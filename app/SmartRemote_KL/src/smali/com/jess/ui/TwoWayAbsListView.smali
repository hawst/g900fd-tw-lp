.class public abstract Lcom/jess/ui/TwoWayAbsListView;
.super Lcom/jess/ui/v;

# interfaces
.implements Landroid/view/ViewTreeObserver$OnTouchModeChangeListener;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/jess/ui/v",
        "<",
        "Landroid/widget/ListAdapter;",
        ">;",
        "Landroid/view/ViewTreeObserver$OnTouchModeChangeListener;"
    }
.end annotation


# instance fields
.field A:I

.field protected B:Z

.field protected C:Z

.field protected D:Lcom/jess/ui/n;

.field final E:[Z

.field a:I

.field private aA:Z

.field private aB:I

.field private af:Landroid/view/VelocityTracker;

.field private ag:Lcom/jess/ui/i;

.field private ah:Z

.field private ai:Landroid/graphics/Rect;

.field private aj:Landroid/view/ContextMenu$ContextMenuInfo;

.field private ak:I

.field private al:Z

.field private am:Z

.field private an:Lcom/jess/ui/c;

.field private ao:Ljava/lang/Runnable;

.field private ap:Lcom/jess/ui/b;

.field private aq:Lcom/jess/ui/j;

.field private ar:I

.field private as:I

.field private at:Z

.field private au:I

.field private av:I

.field private aw:F

.field private ax:Ljava/lang/Runnable;

.field private ay:I

.field private az:Z

.field b:Lcom/jess/ui/y;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jess/ui/v",
            "<",
            "Landroid/widget/ListAdapter;",
            ">.com/jess/ui/y;"
        }
    .end annotation
.end field

.field c:Landroid/widget/ListAdapter;

.field d:Z

.field e:Landroid/graphics/drawable/Drawable;

.field f:Landroid/graphics/Rect;

.field final g:Lcom/jess/ui/k;

.field final h:Z

.field i:I

.field j:I

.field k:I

.field l:I

.field m:Landroid/graphics/Rect;

.field n:I

.field o:Landroid/view/View;

.field p:Landroid/view/View;

.field q:Landroid/view/View;

.field r:Landroid/view/View;

.field s:Z

.field t:I

.field u:I

.field v:I

.field w:I

.field x:I

.field y:Z

.field z:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, -0x1

    const/4 v1, 0x0

    .line 535
    invoke-direct {p0, p1}, Lcom/jess/ui/v;-><init>(Landroid/content/Context;)V

    .line 202
    iput v1, p0, Lcom/jess/ui/TwoWayAbsListView;->a:I

    .line 217
    iput-boolean v1, p0, Lcom/jess/ui/TwoWayAbsListView;->d:Z

    .line 227
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/jess/ui/TwoWayAbsListView;->f:Landroid/graphics/Rect;

    .line 233
    new-instance v0, Lcom/jess/ui/k;

    invoke-direct {v0, p0}, Lcom/jess/ui/k;-><init>(Lcom/jess/ui/TwoWayAbsListView;)V

    iput-object v0, p0, Lcom/jess/ui/TwoWayAbsListView;->g:Lcom/jess/ui/k;

    .line 235
    iput-boolean v1, p0, Lcom/jess/ui/TwoWayAbsListView;->h:Z

    .line 239
    iput v1, p0, Lcom/jess/ui/TwoWayAbsListView;->i:I

    .line 244
    iput v1, p0, Lcom/jess/ui/TwoWayAbsListView;->j:I

    .line 249
    iput v1, p0, Lcom/jess/ui/TwoWayAbsListView;->k:I

    .line 254
    iput v1, p0, Lcom/jess/ui/TwoWayAbsListView;->l:I

    .line 259
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/jess/ui/TwoWayAbsListView;->m:Landroid/graphics/Rect;

    .line 264
    iput v1, p0, Lcom/jess/ui/TwoWayAbsListView;->n:I

    .line 313
    iput v2, p0, Lcom/jess/ui/TwoWayAbsListView;->w:I

    .line 324
    iput v1, p0, Lcom/jess/ui/TwoWayAbsListView;->x:I

    .line 362
    iput-boolean v3, p0, Lcom/jess/ui/TwoWayAbsListView;->ah:Z

    .line 382
    iput v2, p0, Lcom/jess/ui/TwoWayAbsListView;->A:I

    .line 384
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/jess/ui/TwoWayAbsListView;->aj:Landroid/view/ContextMenu$ContextMenuInfo;

    .line 393
    iput v2, p0, Lcom/jess/ui/TwoWayAbsListView;->ak:I

    .line 396
    iput-boolean v1, p0, Lcom/jess/ui/TwoWayAbsListView;->al:Z

    .line 399
    iput-boolean v1, p0, Lcom/jess/ui/TwoWayAbsListView;->am:Z

    .line 441
    iput v1, p0, Lcom/jess/ui/TwoWayAbsListView;->au:I

    .line 467
    new-array v0, v3, [Z

    iput-object v0, p0, Lcom/jess/ui/TwoWayAbsListView;->E:[Z

    .line 477
    iput v2, p0, Lcom/jess/ui/TwoWayAbsListView;->aB:I

    .line 536
    invoke-direct {p0}, Lcom/jess/ui/TwoWayAbsListView;->s()V

    .line 537
    invoke-direct {p0}, Lcom/jess/ui/TwoWayAbsListView;->t()V

    .line 541
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 544
    const v0, 0x101006a

    invoke-direct {p0, p1, p2, v0}, Lcom/jess/ui/TwoWayAbsListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 545
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4

    .prologue
    const/4 v3, -0x1

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 548
    invoke-direct {p0, p1, p2, p3}, Lcom/jess/ui/v;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 202
    iput v2, p0, Lcom/jess/ui/TwoWayAbsListView;->a:I

    .line 217
    iput-boolean v2, p0, Lcom/jess/ui/TwoWayAbsListView;->d:Z

    .line 227
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/jess/ui/TwoWayAbsListView;->f:Landroid/graphics/Rect;

    .line 233
    new-instance v0, Lcom/jess/ui/k;

    invoke-direct {v0, p0}, Lcom/jess/ui/k;-><init>(Lcom/jess/ui/TwoWayAbsListView;)V

    iput-object v0, p0, Lcom/jess/ui/TwoWayAbsListView;->g:Lcom/jess/ui/k;

    .line 235
    iput-boolean v2, p0, Lcom/jess/ui/TwoWayAbsListView;->h:Z

    .line 239
    iput v2, p0, Lcom/jess/ui/TwoWayAbsListView;->i:I

    .line 244
    iput v2, p0, Lcom/jess/ui/TwoWayAbsListView;->j:I

    .line 249
    iput v2, p0, Lcom/jess/ui/TwoWayAbsListView;->k:I

    .line 254
    iput v2, p0, Lcom/jess/ui/TwoWayAbsListView;->l:I

    .line 259
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/jess/ui/TwoWayAbsListView;->m:Landroid/graphics/Rect;

    .line 264
    iput v2, p0, Lcom/jess/ui/TwoWayAbsListView;->n:I

    .line 313
    iput v3, p0, Lcom/jess/ui/TwoWayAbsListView;->w:I

    .line 324
    iput v2, p0, Lcom/jess/ui/TwoWayAbsListView;->x:I

    .line 362
    iput-boolean v1, p0, Lcom/jess/ui/TwoWayAbsListView;->ah:Z

    .line 382
    iput v3, p0, Lcom/jess/ui/TwoWayAbsListView;->A:I

    .line 384
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/jess/ui/TwoWayAbsListView;->aj:Landroid/view/ContextMenu$ContextMenuInfo;

    .line 393
    iput v3, p0, Lcom/jess/ui/TwoWayAbsListView;->ak:I

    .line 396
    iput-boolean v2, p0, Lcom/jess/ui/TwoWayAbsListView;->al:Z

    .line 399
    iput-boolean v2, p0, Lcom/jess/ui/TwoWayAbsListView;->am:Z

    .line 441
    iput v2, p0, Lcom/jess/ui/TwoWayAbsListView;->au:I

    .line 467
    new-array v0, v1, [Z

    iput-object v0, p0, Lcom/jess/ui/TwoWayAbsListView;->E:[Z

    .line 477
    iput v3, p0, Lcom/jess/ui/TwoWayAbsListView;->aB:I

    .line 549
    invoke-direct {p0}, Lcom/jess/ui/TwoWayAbsListView;->s()V

    .line 551
    sget-object v0, Lcom/peel/ui/fv;->TwoWayAbsListView:[I

    invoke-virtual {p1, p2, v0, p3, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v3

    .line 554
    sget v0, Lcom/peel/ui/fv;->TwoWayAbsListView_listSelector:I

    invoke-virtual {v3, v0}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 555
    if-eqz v0, :cond_0

    .line 556
    invoke-virtual {p0, v0}, Lcom/jess/ui/TwoWayAbsListView;->setSelector(Landroid/graphics/drawable/Drawable;)V

    .line 559
    :cond_0
    sget v0, Lcom/peel/ui/fv;->TwoWayAbsListView_drawSelectorOnTop:I

    invoke-virtual {v3, v0, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    iput-boolean v0, p0, Lcom/jess/ui/TwoWayAbsListView;->d:Z

    .line 562
    sget v0, Lcom/peel/ui/fv;->TwoWayAbsListView_stackFromBottom:I

    invoke-virtual {v3, v0, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    .line 563
    invoke-virtual {p0, v0}, Lcom/jess/ui/TwoWayAbsListView;->setStackFromBottom(Z)V

    .line 565
    sget v0, Lcom/peel/ui/fv;->TwoWayAbsListView_scrollingCache:I

    invoke-virtual {v3, v0, v1}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    .line 566
    invoke-virtual {p0, v0}, Lcom/jess/ui/TwoWayAbsListView;->setScrollingCacheEnabled(Z)V

    .line 571
    sget v0, Lcom/peel/ui/fv;->TwoWayAbsListView_transcriptMode:I

    invoke-virtual {v3, v0, v2}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v0

    .line 573
    invoke-virtual {p0, v0}, Lcom/jess/ui/TwoWayAbsListView;->setTranscriptMode(I)V

    .line 575
    sget v0, Lcom/peel/ui/fv;->TwoWayAbsListView_cacheColorHint:I

    invoke-virtual {v3, v0, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v0

    .line 576
    invoke-virtual {p0, v0}, Lcom/jess/ui/TwoWayAbsListView;->setCacheColorHint(I)V

    .line 581
    sget v0, Lcom/peel/ui/fv;->TwoWayAbsListView_smoothScrollbar:I

    invoke-virtual {v3, v0, v1}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    .line 582
    invoke-virtual {p0, v0}, Lcom/jess/ui/TwoWayAbsListView;->setSmoothScrollbarEnabled(Z)V

    .line 584
    sget v0, Lcom/peel/ui/fv;->TwoWayAbsListView_scrollDirectionPortrait:I

    invoke-virtual {v3, v0, v2}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v0

    .line 585
    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/jess/ui/TwoWayAbsListView;->az:Z

    .line 587
    sget v0, Lcom/peel/ui/fv;->TwoWayAbsListView_scrollDirectionLandscape:I

    invoke-virtual {v3, v0, v2}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v0

    .line 588
    if-nez v0, :cond_2

    :goto_1
    iput-boolean v1, p0, Lcom/jess/ui/TwoWayAbsListView;->aA:Z

    .line 590
    invoke-virtual {v3}, Landroid/content/res/TypedArray;->recycle()V

    .line 591
    invoke-direct {p0}, Lcom/jess/ui/TwoWayAbsListView;->t()V

    .line 592
    return-void

    :cond_1
    move v0, v2

    .line 585
    goto :goto_0

    :cond_2
    move v1, v2

    .line 588
    goto :goto_1
.end method

.method static a(Landroid/graphics/Rect;Landroid/graphics/Rect;I)I
    .locals 5

    .prologue
    .line 2513
    sparse-switch p2, :sswitch_data_0

    .line 2546
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "direction must be one of {FOCUS_UP, FOCUS_DOWN, FOCUS_LEFT, FOCUS_RIGHT, FOCUS_FORWARD, FOCUS_BACKWARD}."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2515
    :sswitch_0
    iget v3, p0, Landroid/graphics/Rect;->right:I

    .line 2516
    iget v0, p0, Landroid/graphics/Rect;->top:I

    invoke-virtual {p0}, Landroid/graphics/Rect;->height()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    add-int v2, v0, v1

    .line 2517
    iget v1, p1, Landroid/graphics/Rect;->left:I

    .line 2518
    iget v0, p1, Landroid/graphics/Rect;->top:I

    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    add-int/2addr v0, v4

    .line 2550
    :goto_0
    sub-int/2addr v1, v3

    .line 2551
    sub-int/2addr v0, v2

    .line 2552
    mul-int/2addr v0, v0

    mul-int/2addr v1, v1

    add-int/2addr v0, v1

    return v0

    .line 2521
    :sswitch_1
    iget v0, p0, Landroid/graphics/Rect;->left:I

    invoke-virtual {p0}, Landroid/graphics/Rect;->width()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    add-int v3, v0, v1

    .line 2522
    iget v2, p0, Landroid/graphics/Rect;->bottom:I

    .line 2523
    iget v0, p1, Landroid/graphics/Rect;->left:I

    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    add-int/2addr v1, v0

    .line 2524
    iget v0, p1, Landroid/graphics/Rect;->top:I

    goto :goto_0

    .line 2527
    :sswitch_2
    iget v3, p0, Landroid/graphics/Rect;->left:I

    .line 2528
    iget v0, p0, Landroid/graphics/Rect;->top:I

    invoke-virtual {p0}, Landroid/graphics/Rect;->height()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    add-int v2, v0, v1

    .line 2529
    iget v1, p1, Landroid/graphics/Rect;->right:I

    .line 2530
    iget v0, p1, Landroid/graphics/Rect;->top:I

    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    add-int/2addr v0, v4

    .line 2531
    goto :goto_0

    .line 2533
    :sswitch_3
    iget v0, p0, Landroid/graphics/Rect;->left:I

    invoke-virtual {p0}, Landroid/graphics/Rect;->width()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    add-int v3, v0, v1

    .line 2534
    iget v2, p0, Landroid/graphics/Rect;->top:I

    .line 2535
    iget v0, p1, Landroid/graphics/Rect;->left:I

    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    add-int/2addr v1, v0

    .line 2536
    iget v0, p1, Landroid/graphics/Rect;->bottom:I

    goto :goto_0

    .line 2540
    :sswitch_4
    iget v0, p0, Landroid/graphics/Rect;->right:I

    invoke-virtual {p0}, Landroid/graphics/Rect;->width()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    add-int v3, v0, v1

    .line 2541
    iget v0, p0, Landroid/graphics/Rect;->top:I

    invoke-virtual {p0}, Landroid/graphics/Rect;->height()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    add-int v2, v0, v1

    .line 2542
    iget v0, p1, Landroid/graphics/Rect;->left:I

    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    add-int/2addr v1, v0

    .line 2543
    iget v0, p1, Landroid/graphics/Rect;->top:I

    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    add-int/2addr v0, v4

    .line 2544
    goto :goto_0

    .line 2513
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_4
        0x2 -> :sswitch_4
        0x11 -> :sswitch_2
        0x21 -> :sswitch_3
        0x42 -> :sswitch_0
        0x82 -> :sswitch_1
    .end sparse-switch
.end method

.method static synthetic a(Lcom/jess/ui/TwoWayAbsListView;)I
    .locals 1

    .prologue
    .line 85
    invoke-virtual {p0}, Lcom/jess/ui/TwoWayAbsListView;->getWindowAttachCount()I

    move-result v0

    return v0
.end method

.method static synthetic a(Lcom/jess/ui/TwoWayAbsListView;I)I
    .locals 0

    .prologue
    .line 85
    iput p1, p0, Lcom/jess/ui/TwoWayAbsListView;->ak:I

    return p1
.end method

.method static synthetic a(Lcom/jess/ui/TwoWayAbsListView;Landroid/view/VelocityTracker;)Landroid/view/VelocityTracker;
    .locals 0

    .prologue
    .line 85
    iput-object p1, p0, Lcom/jess/ui/TwoWayAbsListView;->af:Landroid/view/VelocityTracker;

    return-object p1
.end method

.method static synthetic a(Lcom/jess/ui/TwoWayAbsListView;Lcom/jess/ui/c;)Lcom/jess/ui/c;
    .locals 0

    .prologue
    .line 85
    iput-object p1, p0, Lcom/jess/ui/TwoWayAbsListView;->an:Lcom/jess/ui/c;

    return-object p1
.end method

.method static synthetic a(Lcom/jess/ui/TwoWayAbsListView;Lcom/jess/ui/j;)Lcom/jess/ui/j;
    .locals 0

    .prologue
    .line 85
    iput-object p1, p0, Lcom/jess/ui/TwoWayAbsListView;->aq:Lcom/jess/ui/j;

    return-object p1
.end method

.method static synthetic a(Lcom/jess/ui/TwoWayAbsListView;Ljava/lang/Runnable;)Ljava/lang/Runnable;
    .locals 0

    .prologue
    .line 85
    iput-object p1, p0, Lcom/jess/ui/TwoWayAbsListView;->ax:Ljava/lang/Runnable;

    return-object p1
.end method

.method private a(IIII)V
    .locals 5

    .prologue
    .line 1552
    iget-object v0, p0, Lcom/jess/ui/TwoWayAbsListView;->f:Landroid/graphics/Rect;

    iget v1, p0, Lcom/jess/ui/TwoWayAbsListView;->i:I

    sub-int v1, p1, v1

    iget v2, p0, Lcom/jess/ui/TwoWayAbsListView;->j:I

    sub-int v2, p2, v2

    iget v3, p0, Lcom/jess/ui/TwoWayAbsListView;->k:I

    add-int/2addr v3, p3

    iget v4, p0, Lcom/jess/ui/TwoWayAbsListView;->l:I

    add-int/2addr v4, p4

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;->set(IIII)V

    .line 1554
    return-void
.end method

.method private a(Landroid/graphics/Canvas;)V
    .locals 2

    .prologue
    .line 1628
    invoke-virtual {p0}, Lcom/jess/ui/TwoWayAbsListView;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/jess/ui/TwoWayAbsListView;->f:Landroid/graphics/Rect;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/jess/ui/TwoWayAbsListView;->f:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1629
    iget-object v0, p0, Lcom/jess/ui/TwoWayAbsListView;->e:Landroid/graphics/drawable/Drawable;

    .line 1630
    iget-object v1, p0, Lcom/jess/ui/TwoWayAbsListView;->f:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    .line 1631
    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 1633
    :cond_0
    return-void
.end method

.method static synthetic a(Lcom/jess/ui/TwoWayAbsListView;II)V
    .locals 0

    .prologue
    .line 85
    invoke-virtual {p0, p1, p2}, Lcom/jess/ui/TwoWayAbsListView;->detachViewsFromParent(II)V

    return-void
.end method

.method static synthetic a(Lcom/jess/ui/TwoWayAbsListView;Landroid/view/View;Z)V
    .locals 0

    .prologue
    .line 85
    invoke-virtual {p0, p1, p2}, Lcom/jess/ui/TwoWayAbsListView;->removeDetachedView(Landroid/view/View;Z)V

    return-void
.end method

.method static synthetic a(Lcom/jess/ui/TwoWayAbsListView;Z)V
    .locals 0

    .prologue
    .line 85
    invoke-virtual {p0, p1}, Lcom/jess/ui/TwoWayAbsListView;->setChildrenDrawingCacheEnabled(Z)V

    return-void
.end method

.method static synthetic a(Lcom/jess/ui/TwoWayAbsListView;Landroid/view/View;IJ)Z
    .locals 1

    .prologue
    .line 85
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/jess/ui/TwoWayAbsListView;->c(Landroid/view/View;IJ)Z

    move-result v0

    return v0
.end method

.method static synthetic b(Lcom/jess/ui/TwoWayAbsListView;)I
    .locals 1

    .prologue
    .line 85
    invoke-virtual {p0}, Lcom/jess/ui/TwoWayAbsListView;->getWindowAttachCount()I

    move-result v0

    return v0
.end method

.method static synthetic b(Lcom/jess/ui/TwoWayAbsListView;I)I
    .locals 0

    .prologue
    .line 85
    iput p1, p0, Lcom/jess/ui/TwoWayAbsListView;->au:I

    return p1
.end method

.method static synthetic b(Lcom/jess/ui/TwoWayAbsListView;Ljava/lang/Runnable;)Ljava/lang/Runnable;
    .locals 0

    .prologue
    .line 85
    iput-object p1, p0, Lcom/jess/ui/TwoWayAbsListView;->ao:Ljava/lang/Runnable;

    return-object p1
.end method

.method static synthetic b(Lcom/jess/ui/TwoWayAbsListView;II)V
    .locals 0

    .prologue
    .line 85
    invoke-virtual {p0, p1, p2}, Lcom/jess/ui/TwoWayAbsListView;->detachViewsFromParent(II)V

    return-void
.end method

.method static synthetic b(Lcom/jess/ui/TwoWayAbsListView;Landroid/view/View;Z)V
    .locals 0

    .prologue
    .line 85
    invoke-virtual {p0, p1, p2}, Lcom/jess/ui/TwoWayAbsListView;->removeDetachedView(Landroid/view/View;Z)V

    return-void
.end method

.method static synthetic b(Lcom/jess/ui/TwoWayAbsListView;Z)V
    .locals 0

    .prologue
    .line 85
    invoke-virtual {p0, p1}, Lcom/jess/ui/TwoWayAbsListView;->setChildrenDrawnWithCacheEnabled(Z)V

    return-void
.end method

.method static synthetic c(Lcom/jess/ui/TwoWayAbsListView;I)I
    .locals 0

    .prologue
    .line 85
    iput p1, p0, Lcom/jess/ui/TwoWayAbsListView;->aB:I

    return p1
.end method

.method static synthetic c(Lcom/jess/ui/TwoWayAbsListView;)Lcom/jess/ui/c;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lcom/jess/ui/TwoWayAbsListView;->an:Lcom/jess/ui/c;

    return-object v0
.end method

.method static synthetic c(Lcom/jess/ui/TwoWayAbsListView;Landroid/view/View;Z)V
    .locals 0

    .prologue
    .line 85
    invoke-virtual {p0, p1, p2}, Lcom/jess/ui/TwoWayAbsListView;->removeDetachedView(Landroid/view/View;Z)V

    return-void
.end method

.method static synthetic c(Lcom/jess/ui/TwoWayAbsListView;Z)V
    .locals 0

    .prologue
    .line 85
    invoke-virtual {p0, p1}, Lcom/jess/ui/TwoWayAbsListView;->setChildrenDrawingCacheEnabled(Z)V

    return-void
.end method

.method private c(Landroid/view/View;IJ)Z
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 1918
    .line 1920
    iget-object v0, p0, Lcom/jess/ui/TwoWayAbsListView;->R:Lcom/jess/ui/aa;

    if-eqz v0, :cond_2

    .line 1921
    iget-object v0, p0, Lcom/jess/ui/TwoWayAbsListView;->R:Lcom/jess/ui/aa;

    move-object v1, p0

    move-object v2, p1

    move v3, p2

    move-wide v4, p3

    invoke-interface/range {v0 .. v5}, Lcom/jess/ui/aa;->a(Lcom/jess/ui/v;Landroid/view/View;IJ)Z

    move-result v0

    .line 1924
    :goto_0
    if-nez v0, :cond_0

    .line 1925
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/jess/ui/TwoWayAbsListView;->a(Landroid/view/View;IJ)Landroid/view/ContextMenu$ContextMenuInfo;

    move-result-object v0

    iput-object v0, p0, Lcom/jess/ui/TwoWayAbsListView;->aj:Landroid/view/ContextMenu$ContextMenuInfo;

    .line 1926
    invoke-super {p0, p0}, Lcom/jess/ui/v;->showContextMenuForChild(Landroid/view/View;)Z

    move-result v0

    .line 1928
    :cond_0
    if-eqz v0, :cond_1

    .line 1929
    invoke-virtual {p0, v6}, Lcom/jess/ui/TwoWayAbsListView;->performHapticFeedback(I)Z

    .line 1931
    :cond_1
    return v0

    :cond_2
    move v0, v6

    goto :goto_0
.end method

.method static synthetic d(Lcom/jess/ui/TwoWayAbsListView;)I
    .locals 1

    .prologue
    .line 85
    iget v0, p0, Lcom/jess/ui/TwoWayAbsListView;->ak:I

    return v0
.end method

.method static synthetic d(Lcom/jess/ui/TwoWayAbsListView;Landroid/view/View;Z)V
    .locals 0

    .prologue
    .line 85
    invoke-virtual {p0, p1, p2}, Lcom/jess/ui/TwoWayAbsListView;->removeDetachedView(Landroid/view/View;Z)V

    return-void
.end method

.method static synthetic d(Lcom/jess/ui/TwoWayAbsListView;Z)V
    .locals 0

    .prologue
    .line 85
    invoke-virtual {p0, p1}, Lcom/jess/ui/TwoWayAbsListView;->setChildrenDrawnWithCacheEnabled(Z)V

    return-void
.end method

.method static synthetic e(Lcom/jess/ui/TwoWayAbsListView;)I
    .locals 1

    .prologue
    .line 85
    iget v0, p0, Lcom/jess/ui/TwoWayAbsListView;->av:I

    return v0
.end method

.method static synthetic e(Lcom/jess/ui/TwoWayAbsListView;Landroid/view/View;Z)V
    .locals 0

    .prologue
    .line 85
    invoke-virtual {p0, p1, p2}, Lcom/jess/ui/TwoWayAbsListView;->removeDetachedView(Landroid/view/View;Z)V

    return-void
.end method

.method static synthetic e(Lcom/jess/ui/TwoWayAbsListView;Z)V
    .locals 0

    .prologue
    .line 85
    invoke-virtual {p0, p1}, Lcom/jess/ui/TwoWayAbsListView;->setChildrenDrawingCacheEnabled(Z)V

    return-void
.end method

.method static synthetic f(Lcom/jess/ui/TwoWayAbsListView;)I
    .locals 1

    .prologue
    .line 85
    iget v0, p0, Lcom/jess/ui/TwoWayAbsListView;->au:I

    return v0
.end method

.method static synthetic g(Lcom/jess/ui/TwoWayAbsListView;)Lcom/jess/ui/i;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lcom/jess/ui/TwoWayAbsListView;->ag:Lcom/jess/ui/i;

    return-object v0
.end method

.method static synthetic h(Lcom/jess/ui/TwoWayAbsListView;)Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lcom/jess/ui/TwoWayAbsListView;->ax:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic i(Lcom/jess/ui/TwoWayAbsListView;)Landroid/view/VelocityTracker;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lcom/jess/ui/TwoWayAbsListView;->af:Landroid/view/VelocityTracker;

    return-object v0
.end method

.method static synthetic j(Lcom/jess/ui/TwoWayAbsListView;)Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lcom/jess/ui/TwoWayAbsListView;->ao:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic k(Lcom/jess/ui/TwoWayAbsListView;)Lcom/jess/ui/j;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lcom/jess/ui/TwoWayAbsListView;->aq:Lcom/jess/ui/j;

    return-object v0
.end method

.method static synthetic l(Lcom/jess/ui/TwoWayAbsListView;)I
    .locals 1

    .prologue
    .line 85
    iget v0, p0, Lcom/jess/ui/TwoWayAbsListView;->ay:I

    return v0
.end method

.method private s()V
    .locals 4

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 596
    invoke-virtual {p0, v1}, Lcom/jess/ui/TwoWayAbsListView;->setClickable(Z)V

    .line 597
    invoke-virtual {p0, v1}, Lcom/jess/ui/TwoWayAbsListView;->setFocusableInTouchMode(Z)V

    .line 598
    invoke-virtual {p0, v0}, Lcom/jess/ui/TwoWayAbsListView;->setWillNotDraw(Z)V

    .line 599
    invoke-virtual {p0, v0}, Lcom/jess/ui/TwoWayAbsListView;->setAlwaysDrawnWithCacheEnabled(Z)V

    .line 600
    invoke-virtual {p0, v1}, Lcom/jess/ui/TwoWayAbsListView;->setScrollingCacheEnabled(Z)V

    .line 602
    iget-object v2, p0, Lcom/jess/ui/TwoWayAbsListView;->F:Landroid/content/Context;

    invoke-static {v2}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v2

    .line 603
    invoke-virtual {v2}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v3

    iput v3, p0, Lcom/jess/ui/TwoWayAbsListView;->av:I

    .line 604
    invoke-virtual {v2}, Landroid/view/ViewConfiguration;->getScaledMinimumFlingVelocity()I

    move-result v2

    iput v2, p0, Lcom/jess/ui/TwoWayAbsListView;->ay:I

    .line 605
    invoke-virtual {p0}, Lcom/jess/ui/TwoWayAbsListView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v2, v2, Landroid/util/DisplayMetrics;->density:F

    iput v2, p0, Lcom/jess/ui/TwoWayAbsListView;->aw:F

    .line 606
    invoke-virtual {p0}, Lcom/jess/ui/TwoWayAbsListView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    iget v2, v2, Landroid/content/res/Configuration;->orientation:I

    const/4 v3, 0x2

    if-eq v2, v3, :cond_0

    move v0, v1

    :cond_0
    iput-boolean v0, p0, Lcom/jess/ui/TwoWayAbsListView;->C:Z

    .line 608
    iput-boolean v1, p0, Lcom/jess/ui/TwoWayAbsListView;->B:Z

    .line 611
    return-void
.end method

.method private t()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 616
    iget-boolean v0, p0, Lcom/jess/ui/TwoWayAbsListView;->C:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/jess/ui/TwoWayAbsListView;->az:Z

    :goto_0
    iput-boolean v0, p0, Lcom/jess/ui/TwoWayAbsListView;->B:Z

    .line 617
    iget-boolean v0, p0, Lcom/jess/ui/TwoWayAbsListView;->B:Z

    if-eqz v0, :cond_1

    .line 618
    new-instance v0, Lcom/jess/ui/r;

    invoke-direct {v0, p0}, Lcom/jess/ui/r;-><init>(Lcom/jess/ui/TwoWayAbsListView;)V

    iput-object v0, p0, Lcom/jess/ui/TwoWayAbsListView;->D:Lcom/jess/ui/n;

    .line 619
    invoke-virtual {p0, v2}, Lcom/jess/ui/TwoWayAbsListView;->setVerticalScrollBarEnabled(Z)V

    .line 620
    invoke-virtual {p0, v1}, Lcom/jess/ui/TwoWayAbsListView;->setHorizontalScrollBarEnabled(Z)V

    .line 627
    :goto_1
    return-void

    .line 616
    :cond_0
    iget-boolean v0, p0, Lcom/jess/ui/TwoWayAbsListView;->aA:Z

    goto :goto_0

    .line 622
    :cond_1
    new-instance v0, Lcom/jess/ui/e;

    invoke-direct {v0, p0}, Lcom/jess/ui/e;-><init>(Lcom/jess/ui/TwoWayAbsListView;)V

    iput-object v0, p0, Lcom/jess/ui/TwoWayAbsListView;->D:Lcom/jess/ui/n;

    .line 623
    invoke-virtual {p0, v1}, Lcom/jess/ui/TwoWayAbsListView;->setVerticalScrollBarEnabled(Z)V

    .line 624
    invoke-virtual {p0, v2}, Lcom/jess/ui/TwoWayAbsListView;->setHorizontalScrollBarEnabled(Z)V

    goto :goto_1
.end method

.method private u()Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 630
    iget-boolean v3, p0, Lcom/jess/ui/TwoWayAbsListView;->C:Z

    .line 631
    invoke-virtual {p0}, Lcom/jess/ui/TwoWayAbsListView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v4, 0x2

    if-eq v0, v4, :cond_1

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/jess/ui/TwoWayAbsListView;->C:Z

    .line 634
    iget-boolean v0, p0, Lcom/jess/ui/TwoWayAbsListView;->C:Z

    if-eq v3, v0, :cond_2

    .line 635
    :goto_1
    if-eqz v1, :cond_0

    .line 636
    invoke-direct {p0}, Lcom/jess/ui/TwoWayAbsListView;->t()V

    .line 637
    iget-object v0, p0, Lcom/jess/ui/TwoWayAbsListView;->g:Lcom/jess/ui/k;

    invoke-virtual {v0}, Lcom/jess/ui/k;->c()V

    .line 640
    :cond_0
    return v1

    :cond_1
    move v0, v2

    .line 631
    goto :goto_0

    :cond_2
    move v1, v2

    .line 634
    goto :goto_1
.end method

.method private v()V
    .locals 2

    .prologue
    .line 814
    invoke-virtual {p0}, Lcom/jess/ui/TwoWayAbsListView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x1080062

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/jess/ui/TwoWayAbsListView;->setSelector(Landroid/graphics/drawable/Drawable;)V

    .line 816
    return-void
.end method


# virtual methods
.method abstract a(I)I
.end method

.method public a(II)I
    .locals 4

    .prologue
    .line 2007
    iget-object v0, p0, Lcom/jess/ui/TwoWayAbsListView;->ai:Landroid/graphics/Rect;

    .line 2008
    if-nez v0, :cond_0

    .line 2009
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/jess/ui/TwoWayAbsListView;->ai:Landroid/graphics/Rect;

    .line 2010
    iget-object v0, p0, Lcom/jess/ui/TwoWayAbsListView;->ai:Landroid/graphics/Rect;

    .line 2013
    :cond_0
    invoke-virtual {p0}, Lcom/jess/ui/TwoWayAbsListView;->getChildCount()I

    move-result v1

    .line 2014
    add-int/lit8 v1, v1, -0x1

    :goto_0
    if-ltz v1, :cond_2

    .line 2015
    invoke-virtual {p0, v1}, Lcom/jess/ui/TwoWayAbsListView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 2016
    invoke-virtual {v2}, Landroid/view/View;->getVisibility()I

    move-result v3

    if-nez v3, :cond_1

    .line 2017
    invoke-virtual {v2, v0}, Landroid/view/View;->getHitRect(Landroid/graphics/Rect;)V

    .line 2018
    invoke-virtual {v0, p1, p2}, Landroid/graphics/Rect;->contains(II)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2019
    iget v0, p0, Lcom/jess/ui/TwoWayAbsListView;->H:I

    add-int/2addr v0, v1

    .line 2023
    :goto_1
    return v0

    .line 2014
    :cond_1
    add-int/lit8 v1, v1, -0x1

    goto :goto_0

    .line 2023
    :cond_2
    const/4 v0, -0x1

    goto :goto_1
.end method

.method a(Landroid/view/View;IJ)Landroid/view/ContextMenu$ContextMenuInfo;
    .locals 1

    .prologue
    .line 1830
    new-instance v0, Lcom/jess/ui/x;

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/jess/ui/x;-><init>(Landroid/view/View;IJ)V

    return-object v0
.end method

.method a(I[Z)Landroid/view/View;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1492
    aput-boolean v2, p2, v2

    .line 1495
    iget-object v0, p0, Lcom/jess/ui/TwoWayAbsListView;->g:Lcom/jess/ui/k;

    invoke-virtual {v0, p1}, Lcom/jess/ui/k;->d(I)Landroid/view/View;

    move-result-object v1

    .line 1498
    if-eqz v1, :cond_2

    .line 1504
    iget-object v0, p0, Lcom/jess/ui/TwoWayAbsListView;->c:Landroid/widget/ListAdapter;

    invoke-interface {v0, p1, v1, p0}, Landroid/widget/ListAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 1511
    if-eq v0, v1, :cond_1

    .line 1512
    iget-object v2, p0, Lcom/jess/ui/TwoWayAbsListView;->g:Lcom/jess/ui/k;

    invoke-virtual {v2, v1}, Lcom/jess/ui/k;->a(Landroid/view/View;)V

    .line 1513
    iget v1, p0, Lcom/jess/ui/TwoWayAbsListView;->as:I

    if-eqz v1, :cond_0

    .line 1514
    iget v1, p0, Lcom/jess/ui/TwoWayAbsListView;->as:I

    invoke-virtual {v0, v1}, Landroid/view/View;->setDrawingCacheBackgroundColor(I)V

    .line 1535
    :cond_0
    :goto_0
    return-object v0

    .line 1521
    :cond_1
    const/4 v1, 0x1

    aput-boolean v1, p2, v2

    .line 1522
    invoke-virtual {v0}, Landroid/view/View;->onFinishTemporaryDetach()V

    goto :goto_0

    .line 1525
    :cond_2
    iget-object v0, p0, Lcom/jess/ui/TwoWayAbsListView;->c:Landroid/widget/ListAdapter;

    const/4 v1, 0x0

    invoke-interface {v0, p1, v1, p0}, Landroid/widget/ListAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 1526
    iget v1, p0, Lcom/jess/ui/TwoWayAbsListView;->as:I

    if-eqz v1, :cond_0

    .line 1527
    iget v1, p0, Lcom/jess/ui/TwoWayAbsListView;->as:I

    invoke-virtual {v0, v1}, Landroid/view/View;->setDrawingCacheBackgroundColor(I)V

    goto :goto_0
.end method

.method public a(Landroid/util/AttributeSet;)Lcom/jess/ui/h;
    .locals 2

    .prologue
    .line 2828
    new-instance v0, Lcom/jess/ui/h;

    invoke-virtual {p0}, Lcom/jess/ui/TwoWayAbsListView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lcom/jess/ui/h;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-object v0
.end method

.method a()V
    .locals 4

    .prologue
    .line 733
    iget-object v0, p0, Lcom/jess/ui/TwoWayAbsListView;->ag:Lcom/jess/ui/i;

    if-eqz v0, :cond_0

    .line 734
    iget-object v0, p0, Lcom/jess/ui/TwoWayAbsListView;->ag:Lcom/jess/ui/i;

    iget v1, p0, Lcom/jess/ui/TwoWayAbsListView;->H:I

    invoke-virtual {p0}, Lcom/jess/ui/TwoWayAbsListView;->getChildCount()I

    move-result v2

    iget v3, p0, Lcom/jess/ui/TwoWayAbsListView;->aa:I

    invoke-interface {v0, p0, v1, v2, v3}, Lcom/jess/ui/i;->a(Lcom/jess/ui/TwoWayAbsListView;III)V

    .line 736
    :cond_0
    return-void
.end method

.method a(Landroid/view/View;)V
    .locals 5

    .prologue
    .line 1539
    iget-object v0, p0, Lcom/jess/ui/TwoWayAbsListView;->f:Landroid/graphics/Rect;

    .line 1540
    invoke-virtual {p1}, Landroid/view/View;->getLeft()I

    move-result v1

    invoke-virtual {p1}, Landroid/view/View;->getTop()I

    move-result v2

    invoke-virtual {p1}, Landroid/view/View;->getRight()I

    move-result v3

    invoke-virtual {p1}, Landroid/view/View;->getBottom()I

    move-result v4

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;->set(IIII)V

    .line 1541
    iget v1, v0, Landroid/graphics/Rect;->left:I

    iget v2, v0, Landroid/graphics/Rect;->top:I

    iget v3, v0, Landroid/graphics/Rect;->right:I

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    invoke-direct {p0, v1, v2, v3, v0}, Lcom/jess/ui/TwoWayAbsListView;->a(IIII)V

    .line 1544
    iget-boolean v0, p0, Lcom/jess/ui/TwoWayAbsListView;->at:Z

    .line 1545
    invoke-virtual {p1}, Landroid/view/View;->isEnabled()Z

    move-result v1

    if-eq v1, v0, :cond_0

    .line 1546
    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/jess/ui/TwoWayAbsListView;->at:Z

    .line 1547
    invoke-virtual {p0}, Lcom/jess/ui/TwoWayAbsListView;->refreshDrawableState()V

    .line 1549
    :cond_0
    return-void

    .line 1546
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method abstract a(Z)V
.end method

.method public addTouchables(Ljava/util/ArrayList;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2122
    invoke-virtual {p0}, Lcom/jess/ui/TwoWayAbsListView;->getChildCount()I

    move-result v1

    .line 2123
    iget v2, p0, Lcom/jess/ui/TwoWayAbsListView;->H:I

    .line 2124
    iget-object v3, p0, Lcom/jess/ui/TwoWayAbsListView;->c:Landroid/widget/ListAdapter;

    .line 2126
    if-nez v3, :cond_1

    .line 2137
    :cond_0
    return-void

    .line 2130
    :cond_1
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    .line 2131
    invoke-virtual {p0, v0}, Lcom/jess/ui/TwoWayAbsListView;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    .line 2132
    add-int v5, v2, v0

    invoke-interface {v3, v5}, Landroid/widget/ListAdapter;->isEnabled(I)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 2133
    invoke-virtual {p1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2135
    :cond_2
    invoke-virtual {v4, p1}, Landroid/view/View;->addTouchables(Ljava/util/ArrayList;)V

    .line 2130
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method abstract b(I)I
.end method

.method b()V
    .locals 1

    .prologue
    .line 844
    invoke-virtual {p0}, Lcom/jess/ui/TwoWayAbsListView;->getChildCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 845
    invoke-virtual {p0}, Lcom/jess/ui/TwoWayAbsListView;->c()V

    .line 846
    invoke-virtual {p0}, Lcom/jess/ui/TwoWayAbsListView;->requestLayout()V

    .line 847
    invoke-virtual {p0}, Lcom/jess/ui/TwoWayAbsListView;->invalidate()V

    .line 849
    :cond_0
    return-void
.end method

.method c()V
    .locals 4

    .prologue
    const/4 v3, -0x1

    const/4 v2, 0x0

    .line 1064
    invoke-virtual {p0}, Lcom/jess/ui/TwoWayAbsListView;->removeAllViewsInLayout()V

    .line 1065
    iput v2, p0, Lcom/jess/ui/TwoWayAbsListView;->H:I

    .line 1066
    iput-boolean v2, p0, Lcom/jess/ui/TwoWayAbsListView;->S:Z

    .line 1067
    iput-boolean v2, p0, Lcom/jess/ui/TwoWayAbsListView;->M:Z

    .line 1068
    iput v3, p0, Lcom/jess/ui/TwoWayAbsListView;->ac:I

    .line 1069
    const-wide/high16 v0, -0x8000000000000000L

    iput-wide v0, p0, Lcom/jess/ui/TwoWayAbsListView;->ad:J

    .line 1070
    invoke-virtual {p0, v3}, Lcom/jess/ui/TwoWayAbsListView;->setSelectedPositionInt(I)V

    .line 1071
    invoke-virtual {p0, v3}, Lcom/jess/ui/TwoWayAbsListView;->setNextSelectedPositionInt(I)V

    .line 1072
    iput v2, p0, Lcom/jess/ui/TwoWayAbsListView;->x:I

    .line 1073
    iget-object v0, p0, Lcom/jess/ui/TwoWayAbsListView;->f:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->setEmpty()V

    .line 1074
    invoke-virtual {p0}, Lcom/jess/ui/TwoWayAbsListView;->invalidate()V

    .line 1075
    return-void
.end method

.method protected checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z
    .locals 1

    .prologue
    .line 2833
    instance-of v0, p1, Lcom/jess/ui/h;

    return v0
.end method

.method protected computeHorizontalScrollExtent()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1150
    invoke-virtual {p0}, Lcom/jess/ui/TwoWayAbsListView;->getChildCount()I

    move-result v2

    .line 1151
    if-lez v2, :cond_3

    iget-boolean v0, p0, Lcom/jess/ui/TwoWayAbsListView;->B:Z

    if-nez v0, :cond_3

    .line 1152
    iget-boolean v0, p0, Lcom/jess/ui/TwoWayAbsListView;->ah:Z

    if-eqz v0, :cond_2

    .line 1153
    mul-int/lit8 v0, v2, 0x64

    .line 1155
    invoke-virtual {p0, v1}, Lcom/jess/ui/TwoWayAbsListView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 1156
    invoke-virtual {v1}, Landroid/view/View;->getLeft()I

    move-result v3

    .line 1157
    invoke-virtual {v1}, Landroid/view/View;->getWidth()I

    move-result v1

    .line 1158
    if-lez v1, :cond_0

    .line 1159
    mul-int/lit8 v3, v3, 0x64

    div-int v1, v3, v1

    add-int/2addr v0, v1

    .line 1162
    :cond_0
    add-int/lit8 v1, v2, -0x1

    invoke-virtual {p0, v1}, Lcom/jess/ui/TwoWayAbsListView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 1163
    invoke-virtual {v1}, Landroid/view/View;->getRight()I

    move-result v2

    .line 1164
    invoke-virtual {v1}, Landroid/view/View;->getWidth()I

    move-result v1

    .line 1165
    if-lez v1, :cond_1

    .line 1166
    invoke-virtual {p0}, Lcom/jess/ui/TwoWayAbsListView;->getWidth()I

    move-result v3

    sub-int/2addr v2, v3

    mul-int/lit8 v2, v2, 0x64

    div-int v1, v2, v1

    sub-int/2addr v0, v1

    .line 1174
    :cond_1
    :goto_0
    return v0

    .line 1171
    :cond_2
    const/4 v0, 0x1

    goto :goto_0

    :cond_3
    move v0, v1

    .line 1174
    goto :goto_0
.end method

.method protected computeHorizontalScrollOffset()I
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 1179
    iget v2, p0, Lcom/jess/ui/TwoWayAbsListView;->H:I

    .line 1180
    invoke-virtual {p0}, Lcom/jess/ui/TwoWayAbsListView;->getChildCount()I

    move-result v3

    .line 1181
    if-ltz v2, :cond_0

    if-lez v3, :cond_0

    iget-boolean v1, p0, Lcom/jess/ui/TwoWayAbsListView;->B:Z

    if-nez v1, :cond_0

    .line 1182
    iget-boolean v1, p0, Lcom/jess/ui/TwoWayAbsListView;->ah:Z

    if-eqz v1, :cond_1

    .line 1183
    invoke-virtual {p0, v0}, Lcom/jess/ui/TwoWayAbsListView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 1184
    invoke-virtual {v1}, Landroid/view/View;->getLeft()I

    move-result v3

    .line 1185
    invoke-virtual {v1}, Landroid/view/View;->getWidth()I

    move-result v1

    .line 1186
    if-lez v1, :cond_0

    .line 1187
    mul-int/lit8 v2, v2, 0x64

    mul-int/lit8 v3, v3, 0x64

    div-int v1, v3, v1

    sub-int v1, v2, v1

    .line 1188
    invoke-virtual {p0}, Lcom/jess/ui/TwoWayAbsListView;->getScrollX()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {p0}, Lcom/jess/ui/TwoWayAbsListView;->getWidth()I

    move-result v3

    int-to-float v3, v3

    div-float/2addr v2, v3

    iget v3, p0, Lcom/jess/ui/TwoWayAbsListView;->aa:I

    int-to-float v3, v3

    mul-float/2addr v2, v3

    const/high16 v3, 0x42c80000    # 100.0f

    mul-float/2addr v2, v3

    float-to-int v2, v2

    add-int/2addr v1, v2

    .line 1187
    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 1203
    :cond_0
    :goto_0
    return v0

    .line 1192
    :cond_1
    iget v1, p0, Lcom/jess/ui/TwoWayAbsListView;->aa:I

    .line 1193
    if-nez v2, :cond_2

    .line 1200
    :goto_1
    int-to-float v2, v2

    int-to-float v3, v3

    int-to-float v0, v0

    int-to-float v1, v1

    div-float/2addr v0, v1

    mul-float/2addr v0, v3

    add-float/2addr v0, v2

    float-to-int v0, v0

    goto :goto_0

    .line 1195
    :cond_2
    add-int v0, v2, v3

    if-ne v0, v1, :cond_3

    move v0, v1

    .line 1196
    goto :goto_1

    .line 1198
    :cond_3
    div-int/lit8 v0, v3, 0x2

    add-int/2addr v0, v2

    goto :goto_1
.end method

.method protected computeHorizontalScrollRange()I
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1209
    iget-boolean v1, p0, Lcom/jess/ui/TwoWayAbsListView;->B:Z

    if-eqz v1, :cond_0

    .line 1216
    :goto_0
    return v0

    .line 1211
    :cond_0
    iget-boolean v1, p0, Lcom/jess/ui/TwoWayAbsListView;->ah:Z

    if-eqz v1, :cond_1

    .line 1212
    iget v1, p0, Lcom/jess/ui/TwoWayAbsListView;->aa:I

    mul-int/lit8 v1, v1, 0x64

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    goto :goto_0

    .line 1214
    :cond_1
    iget v0, p0, Lcom/jess/ui/TwoWayAbsListView;->aa:I

    goto :goto_0
.end method

.method protected computeVerticalScrollExtent()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1079
    invoke-virtual {p0}, Lcom/jess/ui/TwoWayAbsListView;->getChildCount()I

    move-result v2

    .line 1080
    if-lez v2, :cond_3

    iget-boolean v0, p0, Lcom/jess/ui/TwoWayAbsListView;->B:Z

    if-eqz v0, :cond_3

    .line 1081
    iget-boolean v0, p0, Lcom/jess/ui/TwoWayAbsListView;->ah:Z

    if-eqz v0, :cond_2

    .line 1082
    mul-int/lit8 v0, v2, 0x64

    .line 1084
    invoke-virtual {p0, v1}, Lcom/jess/ui/TwoWayAbsListView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 1085
    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v3

    .line 1086
    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v1

    .line 1087
    if-lez v1, :cond_0

    .line 1088
    mul-int/lit8 v3, v3, 0x64

    div-int v1, v3, v1

    add-int/2addr v0, v1

    .line 1091
    :cond_0
    add-int/lit8 v1, v2, -0x1

    invoke-virtual {p0, v1}, Lcom/jess/ui/TwoWayAbsListView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 1092
    invoke-virtual {v1}, Landroid/view/View;->getBottom()I

    move-result v2

    .line 1093
    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v1

    .line 1094
    if-lez v1, :cond_1

    .line 1095
    invoke-virtual {p0}, Lcom/jess/ui/TwoWayAbsListView;->getHeight()I

    move-result v3

    sub-int/2addr v2, v3

    mul-int/lit8 v2, v2, 0x64

    div-int v1, v2, v1

    sub-int/2addr v0, v1

    .line 1103
    :cond_1
    :goto_0
    return v0

    .line 1100
    :cond_2
    const/4 v0, 0x1

    goto :goto_0

    :cond_3
    move v0, v1

    .line 1103
    goto :goto_0
.end method

.method protected computeVerticalScrollOffset()I
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 1108
    iget v2, p0, Lcom/jess/ui/TwoWayAbsListView;->H:I

    .line 1109
    invoke-virtual {p0}, Lcom/jess/ui/TwoWayAbsListView;->getChildCount()I

    move-result v3

    .line 1110
    if-ltz v2, :cond_0

    if-lez v3, :cond_0

    iget-boolean v1, p0, Lcom/jess/ui/TwoWayAbsListView;->B:Z

    if-eqz v1, :cond_0

    .line 1111
    iget-boolean v1, p0, Lcom/jess/ui/TwoWayAbsListView;->ah:Z

    if-eqz v1, :cond_1

    .line 1112
    invoke-virtual {p0, v0}, Lcom/jess/ui/TwoWayAbsListView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 1113
    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v3

    .line 1114
    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v1

    .line 1115
    if-lez v1, :cond_0

    .line 1116
    mul-int/lit8 v2, v2, 0x64

    mul-int/lit8 v3, v3, 0x64

    div-int v1, v3, v1

    sub-int v1, v2, v1

    .line 1117
    invoke-virtual {p0}, Lcom/jess/ui/TwoWayAbsListView;->getScrollY()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {p0}, Lcom/jess/ui/TwoWayAbsListView;->getHeight()I

    move-result v3

    int-to-float v3, v3

    div-float/2addr v2, v3

    iget v3, p0, Lcom/jess/ui/TwoWayAbsListView;->aa:I

    int-to-float v3, v3

    mul-float/2addr v2, v3

    const/high16 v3, 0x42c80000    # 100.0f

    mul-float/2addr v2, v3

    float-to-int v2, v2

    add-int/2addr v1, v2

    .line 1116
    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 1132
    :cond_0
    :goto_0
    return v0

    .line 1121
    :cond_1
    iget v1, p0, Lcom/jess/ui/TwoWayAbsListView;->aa:I

    .line 1122
    if-nez v2, :cond_2

    .line 1129
    :goto_1
    int-to-float v2, v2

    int-to-float v3, v3

    int-to-float v0, v0

    int-to-float v1, v1

    div-float/2addr v0, v1

    mul-float/2addr v0, v3

    add-float/2addr v0, v2

    float-to-int v0, v0

    goto :goto_0

    .line 1124
    :cond_2
    add-int v0, v2, v3

    if-ne v0, v1, :cond_3

    move v0, v1

    .line 1125
    goto :goto_1

    .line 1127
    :cond_3
    div-int/lit8 v0, v3, 0x2

    add-int/2addr v0, v2

    goto :goto_1
.end method

.method protected computeVerticalScrollRange()I
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1138
    iget-boolean v1, p0, Lcom/jess/ui/TwoWayAbsListView;->B:Z

    if-nez v1, :cond_0

    .line 1145
    :goto_0
    return v0

    .line 1140
    :cond_0
    iget-boolean v1, p0, Lcom/jess/ui/TwoWayAbsListView;->ah:Z

    if-eqz v1, :cond_1

    .line 1141
    iget v1, p0, Lcom/jess/ui/TwoWayAbsListView;->aa:I

    mul-int/lit8 v1, v1, 0x64

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    goto :goto_0

    .line 1143
    :cond_1
    iget v0, p0, Lcom/jess/ui/TwoWayAbsListView;->aa:I

    goto :goto_0
.end method

.method protected d()V
    .locals 0

    .prologue
    .line 1353
    return-void
.end method

.method protected dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 1

    .prologue
    .line 1558
    .line 1572
    iget-boolean v0, p0, Lcom/jess/ui/TwoWayAbsListView;->d:Z

    .line 1573
    if-nez v0, :cond_0

    .line 1574
    invoke-direct {p0, p1}, Lcom/jess/ui/TwoWayAbsListView;->a(Landroid/graphics/Canvas;)V

    .line 1577
    :cond_0
    invoke-super {p0, p1}, Lcom/jess/ui/v;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 1579
    if-eqz v0, :cond_1

    .line 1580
    invoke-direct {p0, p1}, Lcom/jess/ui/TwoWayAbsListView;->a(Landroid/graphics/Canvas;)V

    .line 1587
    :cond_1
    return-void
.end method

.method protected dispatchSetPressed(Z)V
    .locals 0

    .prologue
    .line 1996
    return-void
.end method

.method protected drawableStateChanged()V
    .locals 2

    .prologue
    .line 1736
    invoke-super {p0}, Lcom/jess/ui/v;->drawableStateChanged()V

    .line 1737
    iget-object v0, p0, Lcom/jess/ui/TwoWayAbsListView;->e:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    .line 1738
    iget-object v0, p0, Lcom/jess/ui/TwoWayAbsListView;->e:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0}, Lcom/jess/ui/TwoWayAbsListView;->getDrawableState()[I

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    .line 1740
    :cond_0
    return-void
.end method

.method e()V
    .locals 6

    .prologue
    const/4 v3, 0x4

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1356
    iget-object v0, p0, Lcom/jess/ui/TwoWayAbsListView;->o:Landroid/view/View;

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/jess/ui/TwoWayAbsListView;->B:Z

    if-eqz v0, :cond_1

    .line 1359
    iget v0, p0, Lcom/jess/ui/TwoWayAbsListView;->H:I

    if-lez v0, :cond_7

    move v0, v1

    .line 1362
    :goto_0
    if-nez v0, :cond_0

    .line 1363
    invoke-virtual {p0}, Lcom/jess/ui/TwoWayAbsListView;->getChildCount()I

    move-result v4

    if-lez v4, :cond_0

    .line 1364
    invoke-virtual {p0, v2}, Lcom/jess/ui/TwoWayAbsListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 1365
    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v0

    iget-object v4, p0, Lcom/jess/ui/TwoWayAbsListView;->m:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->top:I

    if-ge v0, v4, :cond_8

    move v0, v1

    .line 1369
    :cond_0
    :goto_1
    iget-object v4, p0, Lcom/jess/ui/TwoWayAbsListView;->o:Landroid/view/View;

    if-eqz v0, :cond_9

    move v0, v2

    :goto_2
    invoke-virtual {v4, v0}, Landroid/view/View;->setVisibility(I)V

    .line 1372
    :cond_1
    iget-object v0, p0, Lcom/jess/ui/TwoWayAbsListView;->p:Landroid/view/View;

    if-eqz v0, :cond_3

    iget-boolean v0, p0, Lcom/jess/ui/TwoWayAbsListView;->B:Z

    if-eqz v0, :cond_3

    .line 1374
    invoke-virtual {p0}, Lcom/jess/ui/TwoWayAbsListView;->getChildCount()I

    move-result v4

    .line 1377
    iget v0, p0, Lcom/jess/ui/TwoWayAbsListView;->H:I

    add-int/2addr v0, v4

    iget v5, p0, Lcom/jess/ui/TwoWayAbsListView;->aa:I

    if-ge v0, v5, :cond_a

    move v0, v1

    .line 1380
    :goto_3
    if-nez v0, :cond_2

    if-lez v4, :cond_2

    .line 1381
    add-int/lit8 v0, v4, -0x1

    invoke-virtual {p0, v0}, Lcom/jess/ui/TwoWayAbsListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 1382
    invoke-virtual {v0}, Landroid/view/View;->getBottom()I

    move-result v0

    invoke-virtual {p0}, Lcom/jess/ui/TwoWayAbsListView;->getBottom()I

    move-result v4

    iget-object v5, p0, Lcom/jess/ui/TwoWayAbsListView;->m:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->bottom:I

    sub-int/2addr v4, v5

    if-le v0, v4, :cond_b

    move v0, v1

    .line 1385
    :cond_2
    :goto_4
    iget-object v4, p0, Lcom/jess/ui/TwoWayAbsListView;->p:Landroid/view/View;

    if-eqz v0, :cond_c

    move v0, v2

    :goto_5
    invoke-virtual {v4, v0}, Landroid/view/View;->setVisibility(I)V

    .line 1388
    :cond_3
    iget-object v0, p0, Lcom/jess/ui/TwoWayAbsListView;->q:Landroid/view/View;

    if-eqz v0, :cond_5

    iget-boolean v0, p0, Lcom/jess/ui/TwoWayAbsListView;->B:Z

    if-nez v0, :cond_5

    .line 1391
    iget v0, p0, Lcom/jess/ui/TwoWayAbsListView;->H:I

    if-lez v0, :cond_d

    move v0, v1

    .line 1394
    :goto_6
    if-nez v0, :cond_4

    .line 1395
    invoke-virtual {p0}, Lcom/jess/ui/TwoWayAbsListView;->getChildCount()I

    move-result v4

    if-lez v4, :cond_4

    .line 1396
    invoke-virtual {p0, v2}, Lcom/jess/ui/TwoWayAbsListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 1397
    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v0

    iget-object v4, p0, Lcom/jess/ui/TwoWayAbsListView;->m:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->left:I

    if-ge v0, v4, :cond_e

    move v0, v1

    .line 1401
    :cond_4
    :goto_7
    iget-object v4, p0, Lcom/jess/ui/TwoWayAbsListView;->q:Landroid/view/View;

    if-eqz v0, :cond_f

    move v0, v2

    :goto_8
    invoke-virtual {v4, v0}, Landroid/view/View;->setVisibility(I)V

    .line 1404
    :cond_5
    iget-object v0, p0, Lcom/jess/ui/TwoWayAbsListView;->r:Landroid/view/View;

    if-eqz v0, :cond_6

    iget-boolean v0, p0, Lcom/jess/ui/TwoWayAbsListView;->B:Z

    if-nez v0, :cond_6

    .line 1406
    invoke-virtual {p0}, Lcom/jess/ui/TwoWayAbsListView;->getChildCount()I

    move-result v4

    .line 1409
    iget v0, p0, Lcom/jess/ui/TwoWayAbsListView;->H:I

    add-int/2addr v0, v4

    iget v5, p0, Lcom/jess/ui/TwoWayAbsListView;->aa:I

    if-ge v0, v5, :cond_10

    move v0, v1

    .line 1412
    :goto_9
    if-nez v0, :cond_13

    if-lez v4, :cond_13

    .line 1413
    add-int/lit8 v0, v4, -0x1

    invoke-virtual {p0, v0}, Lcom/jess/ui/TwoWayAbsListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 1414
    invoke-virtual {v0}, Landroid/view/View;->getRight()I

    move-result v0

    invoke-virtual {p0}, Lcom/jess/ui/TwoWayAbsListView;->getRight()I

    move-result v4

    iget-object v5, p0, Lcom/jess/ui/TwoWayAbsListView;->m:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->right:I

    sub-int/2addr v4, v5

    if-le v0, v4, :cond_11

    .line 1417
    :goto_a
    iget-object v0, p0, Lcom/jess/ui/TwoWayAbsListView;->r:Landroid/view/View;

    if-eqz v1, :cond_12

    :goto_b
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1419
    :cond_6
    return-void

    :cond_7
    move v0, v2

    .line 1359
    goto/16 :goto_0

    :cond_8
    move v0, v2

    .line 1365
    goto/16 :goto_1

    :cond_9
    move v0, v3

    .line 1369
    goto/16 :goto_2

    :cond_a
    move v0, v2

    .line 1377
    goto/16 :goto_3

    :cond_b
    move v0, v2

    .line 1382
    goto :goto_4

    :cond_c
    move v0, v3

    .line 1385
    goto :goto_5

    :cond_d
    move v0, v2

    .line 1391
    goto :goto_6

    :cond_e
    move v0, v2

    .line 1397
    goto :goto_7

    :cond_f
    move v0, v3

    .line 1401
    goto :goto_8

    :cond_10
    move v0, v2

    .line 1409
    goto :goto_9

    :cond_11
    move v1, v2

    .line 1414
    goto :goto_a

    :cond_12
    move v2, v3

    .line 1417
    goto :goto_b

    :cond_13
    move v1, v0

    goto :goto_a
.end method

.method f()Z
    .locals 1

    .prologue
    .line 1607
    iget v0, p0, Lcom/jess/ui/TwoWayAbsListView;->w:I

    packed-switch v0, :pswitch_data_0

    .line 1612
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 1610
    :pswitch_0
    const/4 v0, 0x1

    goto :goto_0

    .line 1607
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method g()Z
    .locals 1

    .prologue
    .line 1624
    invoke-virtual {p0}, Lcom/jess/ui/TwoWayAbsListView;->hasFocus()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/jess/ui/TwoWayAbsListView;->isInTouchMode()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    invoke-virtual {p0}, Lcom/jess/ui/TwoWayAbsListView;->f()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public synthetic generateLayoutParams(Landroid/util/AttributeSet;)Landroid/view/ViewGroup$LayoutParams;
    .locals 1

    .prologue
    .line 85
    invoke-virtual {p0, p1}, Lcom/jess/ui/TwoWayAbsListView;->a(Landroid/util/AttributeSet;)Lcom/jess/ui/h;

    move-result-object v0

    return-object v0
.end method

.method protected generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Landroid/view/ViewGroup$LayoutParams;
    .locals 1

    .prologue
    .line 2823
    new-instance v0, Lcom/jess/ui/h;

    invoke-direct {v0, p1}, Lcom/jess/ui/h;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    return-object v0
.end method

.method protected getBottomFadingEdgeStrength()F
    .locals 6

    .prologue
    .line 1239
    invoke-virtual {p0}, Lcom/jess/ui/TwoWayAbsListView;->getChildCount()I

    move-result v1

    .line 1240
    invoke-super {p0}, Lcom/jess/ui/v;->getBottomFadingEdgeStrength()F

    move-result v0

    .line 1241
    if-eqz v1, :cond_0

    iget-boolean v2, p0, Lcom/jess/ui/TwoWayAbsListView;->B:Z

    if-nez v2, :cond_1

    .line 1252
    :cond_0
    :goto_0
    return v0

    .line 1244
    :cond_1
    iget v2, p0, Lcom/jess/ui/TwoWayAbsListView;->H:I

    add-int/2addr v2, v1

    add-int/lit8 v2, v2, -0x1

    iget v3, p0, Lcom/jess/ui/TwoWayAbsListView;->aa:I

    add-int/lit8 v3, v3, -0x1

    if-ge v2, v3, :cond_2

    .line 1245
    const/high16 v0, 0x3f800000    # 1.0f

    goto :goto_0

    .line 1248
    :cond_2
    add-int/lit8 v1, v1, -0x1

    invoke-virtual {p0, v1}, Lcom/jess/ui/TwoWayAbsListView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getBottom()I

    move-result v1

    .line 1249
    invoke-virtual {p0}, Lcom/jess/ui/TwoWayAbsListView;->getHeight()I

    move-result v2

    .line 1250
    invoke-virtual {p0}, Lcom/jess/ui/TwoWayAbsListView;->getVerticalFadingEdgeLength()I

    move-result v3

    int-to-float v3, v3

    .line 1251
    invoke-virtual {p0}, Lcom/jess/ui/TwoWayAbsListView;->getPaddingBottom()I

    move-result v4

    .line 1252
    sub-int v5, v2, v4

    if-le v1, v5, :cond_0

    sub-int v0, v1, v2

    add-int/2addr v0, v4

    int-to-float v0, v0

    div-float/2addr v0, v3

    goto :goto_0
.end method

.method public getCacheColorHint()I
    .locals 1

    .prologue
    .line 2945
    iget v0, p0, Lcom/jess/ui/TwoWayAbsListView;->as:I

    return v0
.end method

.method protected getContextMenuInfo()Landroid/view/ContextMenu$ContextMenuInfo;
    .locals 1

    .prologue
    .line 1936
    iget-object v0, p0, Lcom/jess/ui/TwoWayAbsListView;->aj:Landroid/view/ContextMenu$ContextMenuInfo;

    return-object v0
.end method

.method public getFocusedRect(Landroid/graphics/Rect;)V
    .locals 2

    .prologue
    .line 801
    invoke-virtual {p0}, Lcom/jess/ui/TwoWayAbsListView;->getSelectedView()Landroid/view/View;

    move-result-object v0

    .line 802
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    if-ne v1, p0, :cond_0

    .line 805
    invoke-virtual {v0, p1}, Landroid/view/View;->getFocusedRect(Landroid/graphics/Rect;)V

    .line 806
    invoke-virtual {p0, v0, p1}, Lcom/jess/ui/TwoWayAbsListView;->offsetDescendantRectToMyCoords(Landroid/view/View;Landroid/graphics/Rect;)V

    .line 811
    :goto_0
    return-void

    .line 809
    :cond_0
    invoke-super {p0, p1}, Lcom/jess/ui/v;->getFocusedRect(Landroid/graphics/Rect;)V

    goto :goto_0
.end method

.method getFooterViewsCount()I
    .locals 1

    .prologue
    .line 2212
    const/4 v0, 0x0

    return v0
.end method

.method getHeaderViewsCount()I
    .locals 1

    .prologue
    .line 2202
    const/4 v0, 0x0

    return v0
.end method

.method protected getLeftFadingEdgeStrength()F
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1259
    invoke-virtual {p0}, Lcom/jess/ui/TwoWayAbsListView;->getChildCount()I

    move-result v1

    .line 1260
    invoke-super {p0}, Lcom/jess/ui/v;->getLeftFadingEdgeStrength()F

    move-result v0

    .line 1261
    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/jess/ui/TwoWayAbsListView;->B:Z

    if-eqz v1, :cond_1

    .line 1271
    :cond_0
    :goto_0
    return v0

    .line 1264
    :cond_1
    iget v1, p0, Lcom/jess/ui/TwoWayAbsListView;->H:I

    if-lez v1, :cond_2

    .line 1265
    const/high16 v0, 0x3f800000    # 1.0f

    goto :goto_0

    .line 1268
    :cond_2
    invoke-virtual {p0, v3}, Lcom/jess/ui/TwoWayAbsListView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getLeft()I

    move-result v1

    .line 1269
    invoke-virtual {p0}, Lcom/jess/ui/TwoWayAbsListView;->getHorizontalFadingEdgeLength()I

    move-result v2

    int-to-float v2, v2

    .line 1271
    if-ge v1, v3, :cond_0

    sub-int v0, v1, v3

    neg-int v0, v0

    int-to-float v0, v0

    div-float/2addr v0, v2

    goto :goto_0
.end method

.method public getListPaddingBottom()I
    .locals 1

    .prologue
    .line 1452
    iget-object v0, p0, Lcom/jess/ui/TwoWayAbsListView;->m:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    return v0
.end method

.method public getListPaddingLeft()I
    .locals 1

    .prologue
    .line 1464
    iget-object v0, p0, Lcom/jess/ui/TwoWayAbsListView;->m:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    return v0
.end method

.method public getListPaddingRight()I
    .locals 1

    .prologue
    .line 1476
    iget-object v0, p0, Lcom/jess/ui/TwoWayAbsListView;->m:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->right:I

    return v0
.end method

.method public getListPaddingTop()I
    .locals 1

    .prologue
    .line 1440
    iget-object v0, p0, Lcom/jess/ui/TwoWayAbsListView;->m:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->top:I

    return v0
.end method

.method protected getRightFadingEdgeStrength()F
    .locals 6

    .prologue
    .line 1277
    invoke-virtual {p0}, Lcom/jess/ui/TwoWayAbsListView;->getChildCount()I

    move-result v1

    .line 1278
    invoke-super {p0}, Lcom/jess/ui/v;->getRightFadingEdgeStrength()F

    move-result v0

    .line 1279
    if-eqz v1, :cond_0

    iget-boolean v2, p0, Lcom/jess/ui/TwoWayAbsListView;->B:Z

    if-eqz v2, :cond_1

    .line 1290
    :cond_0
    :goto_0
    return v0

    .line 1282
    :cond_1
    iget v2, p0, Lcom/jess/ui/TwoWayAbsListView;->H:I

    add-int/2addr v2, v1

    add-int/lit8 v2, v2, -0x1

    iget v3, p0, Lcom/jess/ui/TwoWayAbsListView;->aa:I

    add-int/lit8 v3, v3, -0x1

    if-ge v2, v3, :cond_2

    .line 1283
    const/high16 v0, 0x3f800000    # 1.0f

    goto :goto_0

    .line 1286
    :cond_2
    add-int/lit8 v1, v1, -0x1

    invoke-virtual {p0, v1}, Lcom/jess/ui/TwoWayAbsListView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getRight()I

    move-result v1

    .line 1287
    invoke-virtual {p0}, Lcom/jess/ui/TwoWayAbsListView;->getWidth()I

    move-result v2

    .line 1288
    invoke-virtual {p0}, Lcom/jess/ui/TwoWayAbsListView;->getHorizontalFadingEdgeLength()I

    move-result v3

    int-to-float v3, v3

    .line 1289
    const/4 v4, 0x0

    .line 1290
    sub-int v5, v2, v4

    if-le v1, v5, :cond_0

    sub-int v0, v1, v2

    add-int/2addr v0, v4

    int-to-float v0, v0

    div-float/2addr v0, v3

    goto :goto_0
.end method

.method public getScrollDirectionLandscape()I
    .locals 1

    .prologue
    .line 2913
    iget-boolean v0, p0, Lcom/jess/ui/TwoWayAbsListView;->aA:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public getScrollDirectionPortrait()I
    .locals 1

    .prologue
    .line 2885
    iget-boolean v0, p0, Lcom/jess/ui/TwoWayAbsListView;->az:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public getSelectedView()Landroid/view/View;
    .locals 2
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
    .end annotation

    .prologue
    .line 1424
    iget v0, p0, Lcom/jess/ui/TwoWayAbsListView;->aa:I

    if-lez v0, :cond_0

    iget v0, p0, Lcom/jess/ui/TwoWayAbsListView;->V:I

    if-ltz v0, :cond_0

    .line 1425
    iget v0, p0, Lcom/jess/ui/TwoWayAbsListView;->V:I

    iget v1, p0, Lcom/jess/ui/TwoWayAbsListView;->H:I

    sub-int/2addr v0, v1

    invoke-virtual {p0, v0}, Lcom/jess/ui/TwoWayAbsListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 1427
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getSelector()Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 1682
    iget-object v0, p0, Lcom/jess/ui/TwoWayAbsListView;->e:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method public getSolidColor()I
    .locals 1

    .prologue
    .line 2918
    iget v0, p0, Lcom/jess/ui/TwoWayAbsListView;->as:I

    return v0
.end method

.method protected getTopFadingEdgeStrength()F
    .locals 4

    .prologue
    .line 1221
    invoke-virtual {p0}, Lcom/jess/ui/TwoWayAbsListView;->getChildCount()I

    move-result v1

    .line 1222
    invoke-super {p0}, Lcom/jess/ui/v;->getTopFadingEdgeStrength()F

    move-result v0

    .line 1223
    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/jess/ui/TwoWayAbsListView;->B:Z

    if-nez v1, :cond_1

    .line 1233
    :cond_0
    :goto_0
    return v0

    .line 1226
    :cond_1
    iget v1, p0, Lcom/jess/ui/TwoWayAbsListView;->H:I

    if-lez v1, :cond_2

    .line 1227
    const/high16 v0, 0x3f800000    # 1.0f

    goto :goto_0

    .line 1230
    :cond_2
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/jess/ui/TwoWayAbsListView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v1

    .line 1231
    invoke-virtual {p0}, Lcom/jess/ui/TwoWayAbsListView;->getVerticalFadingEdgeLength()I

    move-result v2

    int-to-float v2, v2

    .line 1232
    invoke-virtual {p0}, Lcom/jess/ui/TwoWayAbsListView;->getPaddingTop()I

    move-result v3

    .line 1233
    if-ge v1, v3, :cond_0

    sub-int v0, v1, v3

    neg-int v0, v0

    int-to-float v0, v0

    div-float/2addr v0, v2

    goto :goto_0
.end method

.method public getTranscriptMode()I
    .locals 1

    .prologue
    .line 2857
    iget v0, p0, Lcom/jess/ui/TwoWayAbsListView;->ar:I

    return v0
.end method

.method h()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 1690
    invoke-virtual {p0}, Lcom/jess/ui/TwoWayAbsListView;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/jess/ui/TwoWayAbsListView;->isClickable()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1725
    :cond_0
    :goto_0
    return-void

    .line 1694
    :cond_1
    iget-object v0, p0, Lcom/jess/ui/TwoWayAbsListView;->e:Landroid/graphics/drawable/Drawable;

    .line 1695
    iget-object v1, p0, Lcom/jess/ui/TwoWayAbsListView;->f:Landroid/graphics/Rect;

    .line 1696
    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/jess/ui/TwoWayAbsListView;->isFocused()Z

    move-result v2

    if-nez v2, :cond_2

    invoke-virtual {p0}, Lcom/jess/ui/TwoWayAbsListView;->f()Z

    move-result v2

    if-eqz v2, :cond_0

    :cond_2
    if-eqz v1, :cond_0

    .line 1697
    invoke-virtual {v1}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1699
    iget v1, p0, Lcom/jess/ui/TwoWayAbsListView;->V:I

    iget v2, p0, Lcom/jess/ui/TwoWayAbsListView;->H:I

    sub-int/2addr v1, v2

    invoke-virtual {p0, v1}, Lcom/jess/ui/TwoWayAbsListView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 1701
    if-eqz v1, :cond_3

    .line 1702
    invoke-virtual {v1}, Landroid/view/View;->hasFocusable()Z

    move-result v2

    if-nez v2, :cond_0

    .line 1703
    invoke-virtual {v1, v3}, Landroid/view/View;->setPressed(Z)V

    .line 1705
    :cond_3
    invoke-virtual {p0, v3}, Lcom/jess/ui/TwoWayAbsListView;->setPressed(Z)V

    .line 1707
    invoke-virtual {p0}, Lcom/jess/ui/TwoWayAbsListView;->isLongClickable()Z

    move-result v1

    .line 1708
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getCurrent()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 1709
    if-eqz v0, :cond_4

    instance-of v2, v0, Landroid/graphics/drawable/TransitionDrawable;

    if-eqz v2, :cond_4

    .line 1710
    if-eqz v1, :cond_6

    .line 1711
    check-cast v0, Landroid/graphics/drawable/TransitionDrawable;

    .line 1712
    invoke-static {}, Landroid/view/ViewConfiguration;->getLongPressTimeout()I

    move-result v2

    .line 1711
    invoke-virtual {v0, v2}, Landroid/graphics/drawable/TransitionDrawable;->startTransition(I)V

    .line 1717
    :cond_4
    :goto_1
    if-eqz v1, :cond_0

    iget-boolean v0, p0, Lcom/jess/ui/TwoWayAbsListView;->S:Z

    if-nez v0, :cond_0

    .line 1718
    iget-object v0, p0, Lcom/jess/ui/TwoWayAbsListView;->ap:Lcom/jess/ui/b;

    if-nez v0, :cond_5

    .line 1719
    new-instance v0, Lcom/jess/ui/b;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/jess/ui/b;-><init>(Lcom/jess/ui/TwoWayAbsListView;Lcom/jess/ui/a;)V

    iput-object v0, p0, Lcom/jess/ui/TwoWayAbsListView;->ap:Lcom/jess/ui/b;

    .line 1721
    :cond_5
    iget-object v0, p0, Lcom/jess/ui/TwoWayAbsListView;->ap:Lcom/jess/ui/b;

    invoke-virtual {v0}, Lcom/jess/ui/b;->a()V

    .line 1722
    iget-object v0, p0, Lcom/jess/ui/TwoWayAbsListView;->ap:Lcom/jess/ui/b;

    invoke-static {}, Landroid/view/ViewConfiguration;->getLongPressTimeout()I

    move-result v1

    int-to-long v2, v1

    invoke-virtual {p0, v0, v2, v3}, Lcom/jess/ui/TwoWayAbsListView;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 1714
    :cond_6
    check-cast v0, Landroid/graphics/drawable/TransitionDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/TransitionDrawable;->resetTransition()V

    goto :goto_1
.end method

.method i()V
    .locals 3

    .prologue
    const/4 v2, -0x1

    .line 2226
    iget v0, p0, Lcom/jess/ui/TwoWayAbsListView;->V:I

    if-eq v0, v2, :cond_2

    .line 2227
    iget v0, p0, Lcom/jess/ui/TwoWayAbsListView;->a:I

    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    .line 2228
    iget v0, p0, Lcom/jess/ui/TwoWayAbsListView;->V:I

    iput v0, p0, Lcom/jess/ui/TwoWayAbsListView;->A:I

    .line 2230
    :cond_0
    iget v0, p0, Lcom/jess/ui/TwoWayAbsListView;->T:I

    if-ltz v0, :cond_1

    iget v0, p0, Lcom/jess/ui/TwoWayAbsListView;->T:I

    iget v1, p0, Lcom/jess/ui/TwoWayAbsListView;->V:I

    if-eq v0, v1, :cond_1

    .line 2231
    iget v0, p0, Lcom/jess/ui/TwoWayAbsListView;->T:I

    iput v0, p0, Lcom/jess/ui/TwoWayAbsListView;->A:I

    .line 2233
    :cond_1
    invoke-virtual {p0, v2}, Lcom/jess/ui/TwoWayAbsListView;->setSelectedPositionInt(I)V

    .line 2234
    invoke-virtual {p0, v2}, Lcom/jess/ui/TwoWayAbsListView;->setNextSelectedPositionInt(I)V

    .line 2235
    const/4 v0, 0x0

    iput v0, p0, Lcom/jess/ui/TwoWayAbsListView;->x:I

    .line 2236
    iget-object v0, p0, Lcom/jess/ui/TwoWayAbsListView;->f:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->setEmpty()V

    .line 2238
    :cond_2
    return-void
.end method

.method j()I
    .locals 2

    .prologue
    .line 2246
    iget v0, p0, Lcom/jess/ui/TwoWayAbsListView;->V:I

    .line 2247
    if-gez v0, :cond_0

    .line 2248
    iget v0, p0, Lcom/jess/ui/TwoWayAbsListView;->A:I

    .line 2250
    :cond_0
    const/4 v1, 0x0

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 2251
    iget v1, p0, Lcom/jess/ui/TwoWayAbsListView;->aa:I

    add-int/lit8 v1, v1, -0x1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 2252
    return v0
.end method

.method k()Z
    .locals 1

    .prologue
    .line 2326
    iget-object v0, p0, Lcom/jess/ui/TwoWayAbsListView;->D:Lcom/jess/ui/n;

    invoke-virtual {v0}, Lcom/jess/ui/n;->a()Z

    move-result v0

    return v0
.end method

.method protected l()V
    .locals 8

    .prologue
    const/4 v7, 0x2

    const/4 v6, -0x1

    const/4 v5, 0x5

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 2331
    iget v3, p0, Lcom/jess/ui/TwoWayAbsListView;->aa:I

    .line 2332
    if-lez v3, :cond_b

    .line 2339
    iget-boolean v0, p0, Lcom/jess/ui/TwoWayAbsListView;->M:Z

    if-eqz v0, :cond_3

    .line 2341
    iput-boolean v1, p0, Lcom/jess/ui/TwoWayAbsListView;->M:Z

    .line 2343
    iget v0, p0, Lcom/jess/ui/TwoWayAbsListView;->ar:I

    if-eq v0, v7, :cond_0

    iget v0, p0, Lcom/jess/ui/TwoWayAbsListView;->ar:I

    if-ne v0, v2, :cond_2

    iget v0, p0, Lcom/jess/ui/TwoWayAbsListView;->H:I

    .line 2345
    invoke-virtual {p0}, Lcom/jess/ui/TwoWayAbsListView;->getChildCount()I

    move-result v4

    add-int/2addr v0, v4

    iget v4, p0, Lcom/jess/ui/TwoWayAbsListView;->ab:I

    if-lt v0, v4, :cond_2

    .line 2346
    :cond_0
    const/4 v0, 0x3

    iput v0, p0, Lcom/jess/ui/TwoWayAbsListView;->a:I

    .line 2442
    :cond_1
    :goto_0
    return-void

    .line 2350
    :cond_2
    iget v0, p0, Lcom/jess/ui/TwoWayAbsListView;->N:I

    packed-switch v0, :pswitch_data_0

    .line 2398
    :cond_3
    invoke-virtual {p0}, Lcom/jess/ui/TwoWayAbsListView;->isInTouchMode()Z

    move-result v0

    if-nez v0, :cond_a

    .line 2400
    invoke-virtual {p0}, Lcom/jess/ui/TwoWayAbsListView;->getSelectedItemPosition()I

    move-result v0

    .line 2403
    if-lt v0, v3, :cond_4

    .line 2404
    add-int/lit8 v0, v3, -0x1

    .line 2406
    :cond_4
    if-gez v0, :cond_5

    move v0, v1

    .line 2411
    :cond_5
    invoke-virtual {p0, v0, v2}, Lcom/jess/ui/TwoWayAbsListView;->a(IZ)I

    move-result v3

    .line 2413
    if-ltz v3, :cond_9

    .line 2414
    invoke-virtual {p0, v3}, Lcom/jess/ui/TwoWayAbsListView;->setNextSelectedPositionInt(I)V

    goto :goto_0

    .line 2352
    :pswitch_0
    invoke-virtual {p0}, Lcom/jess/ui/TwoWayAbsListView;->isInTouchMode()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 2357
    iput v5, p0, Lcom/jess/ui/TwoWayAbsListView;->a:I

    .line 2358
    iget v0, p0, Lcom/jess/ui/TwoWayAbsListView;->J:I

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    add-int/lit8 v1, v3, -0x1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    iput v0, p0, Lcom/jess/ui/TwoWayAbsListView;->J:I

    goto :goto_0

    .line 2364
    :cond_6
    invoke-virtual {p0}, Lcom/jess/ui/TwoWayAbsListView;->q()I

    move-result v4

    .line 2365
    if-ltz v4, :cond_3

    .line 2367
    invoke-virtual {p0, v4, v2}, Lcom/jess/ui/TwoWayAbsListView;->a(IZ)I

    move-result v0

    .line 2368
    if-ne v0, v4, :cond_3

    .line 2370
    iput v4, p0, Lcom/jess/ui/TwoWayAbsListView;->J:I

    .line 2371
    iget-boolean v0, p0, Lcom/jess/ui/TwoWayAbsListView;->G:Z

    if-eqz v0, :cond_7

    invoke-virtual {p0}, Lcom/jess/ui/TwoWayAbsListView;->getHeight()I

    move-result v0

    .line 2372
    :goto_1
    iget-wide v2, p0, Lcom/jess/ui/TwoWayAbsListView;->L:J

    int-to-long v0, v0

    cmp-long v0, v2, v0

    if-nez v0, :cond_8

    .line 2375
    iput v5, p0, Lcom/jess/ui/TwoWayAbsListView;->a:I

    .line 2383
    :goto_2
    invoke-virtual {p0, v4}, Lcom/jess/ui/TwoWayAbsListView;->setNextSelectedPositionInt(I)V

    goto :goto_0

    .line 2371
    :cond_7
    invoke-virtual {p0}, Lcom/jess/ui/TwoWayAbsListView;->getWidth()I

    move-result v0

    goto :goto_1

    .line 2379
    :cond_8
    iput v7, p0, Lcom/jess/ui/TwoWayAbsListView;->a:I

    goto :goto_2

    .line 2391
    :pswitch_1
    iput v5, p0, Lcom/jess/ui/TwoWayAbsListView;->a:I

    .line 2392
    iget v0, p0, Lcom/jess/ui/TwoWayAbsListView;->J:I

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    add-int/lit8 v1, v3, -0x1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    iput v0, p0, Lcom/jess/ui/TwoWayAbsListView;->J:I

    goto :goto_0

    .line 2418
    :cond_9
    invoke-virtual {p0, v0, v1}, Lcom/jess/ui/TwoWayAbsListView;->a(IZ)I

    move-result v0

    .line 2419
    if-ltz v0, :cond_b

    .line 2420
    invoke-virtual {p0, v0}, Lcom/jess/ui/TwoWayAbsListView;->setNextSelectedPositionInt(I)V

    goto :goto_0

    .line 2427
    :cond_a
    iget v0, p0, Lcom/jess/ui/TwoWayAbsListView;->A:I

    if-gez v0, :cond_1

    .line 2435
    :cond_b
    iget-boolean v0, p0, Lcom/jess/ui/TwoWayAbsListView;->y:Z

    if-eqz v0, :cond_c

    const/4 v0, 0x3

    :goto_3
    iput v0, p0, Lcom/jess/ui/TwoWayAbsListView;->a:I

    .line 2436
    iput v6, p0, Lcom/jess/ui/TwoWayAbsListView;->V:I

    .line 2437
    const-wide/high16 v2, -0x8000000000000000L

    iput-wide v2, p0, Lcom/jess/ui/TwoWayAbsListView;->W:J

    .line 2438
    iput v6, p0, Lcom/jess/ui/TwoWayAbsListView;->T:I

    .line 2439
    const-wide/high16 v2, -0x8000000000000000L

    iput-wide v2, p0, Lcom/jess/ui/TwoWayAbsListView;->U:J

    .line 2440
    iput-boolean v1, p0, Lcom/jess/ui/TwoWayAbsListView;->M:Z

    .line 2441
    invoke-virtual {p0}, Lcom/jess/ui/TwoWayAbsListView;->p()V

    goto/16 :goto_0

    :cond_c
    move v0, v2

    .line 2435
    goto :goto_3

    .line 2350
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected onAttachedToWindow()V
    .locals 1

    .prologue
    .line 1783
    invoke-super {p0}, Lcom/jess/ui/v;->onAttachedToWindow()V

    .line 1785
    invoke-virtual {p0}, Lcom/jess/ui/TwoWayAbsListView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    .line 1786
    if-eqz v0, :cond_0

    .line 1787
    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->addOnTouchModeChangeListener(Landroid/view/ViewTreeObserver$OnTouchModeChangeListener;)V

    .line 1793
    :cond_0
    return-void
.end method

.method protected onCreateDrawableState(I)[I
    .locals 5

    .prologue
    .line 1745
    iget-boolean v0, p0, Lcom/jess/ui/TwoWayAbsListView;->at:Z

    if-eqz v0, :cond_1

    .line 1747
    invoke-super {p0, p1}, Lcom/jess/ui/v;->onCreateDrawableState(I)[I

    move-result-object v0

    .line 1773
    :cond_0
    :goto_0
    return-object v0

    .line 1753
    :cond_1
    sget-object v0, Lcom/jess/ui/TwoWayAbsListView;->ENABLED_STATE_SET:[I

    const/4 v1, 0x0

    aget v3, v0, v1

    .line 1758
    add-int/lit8 v0, p1, 0x1

    invoke-super {p0, v0}, Lcom/jess/ui/v;->onCreateDrawableState(I)[I

    move-result-object v0

    .line 1759
    const/4 v2, -0x1

    .line 1760
    array-length v1, v0

    add-int/lit8 v1, v1, -0x1

    :goto_1
    if-ltz v1, :cond_3

    .line 1761
    aget v4, v0, v1

    if-ne v4, v3, :cond_2

    .line 1768
    :goto_2
    if-ltz v1, :cond_0

    .line 1769
    add-int/lit8 v2, v1, 0x1

    array-length v3, v0

    sub-int/2addr v3, v1

    add-int/lit8 v3, v3, -0x1

    invoke-static {v0, v2, v0, v1, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto :goto_0

    .line 1760
    :cond_2
    add-int/lit8 v1, v1, -0x1

    goto :goto_1

    :cond_3
    move v1, v2

    goto :goto_2
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .prologue
    .line 1797
    invoke-super {p0}, Lcom/jess/ui/v;->onDetachedFromWindow()V

    .line 1803
    iget-object v0, p0, Lcom/jess/ui/TwoWayAbsListView;->g:Lcom/jess/ui/k;

    invoke-virtual {v0}, Lcom/jess/ui/k;->b()V

    .line 1805
    invoke-virtual {p0}, Lcom/jess/ui/TwoWayAbsListView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    .line 1806
    if-eqz v0, :cond_0

    .line 1807
    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->removeOnTouchModeChangeListener(Landroid/view/ViewTreeObserver$OnTouchModeChangeListener;)V

    .line 1814
    :cond_0
    return-void
.end method

.method protected onFocusChanged(ZILandroid/graphics/Rect;)V
    .locals 1

    .prologue
    .line 1047
    invoke-super {p0, p1, p2, p3}, Lcom/jess/ui/v;->onFocusChanged(ZILandroid/graphics/Rect;)V

    .line 1048
    if-eqz p1, :cond_0

    iget v0, p0, Lcom/jess/ui/TwoWayAbsListView;->V:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/jess/ui/TwoWayAbsListView;->isInTouchMode()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1049
    invoke-virtual {p0}, Lcom/jess/ui/TwoWayAbsListView;->k()Z

    .line 1051
    :cond_0
    return-void
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 2114
    iget-object v0, p0, Lcom/jess/ui/TwoWayAbsListView;->D:Lcom/jess/ui/n;

    invoke-virtual {v0, p1}, Lcom/jess/ui/n;->a(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1

    .prologue
    .line 1964
    const/4 v0, 0x0

    return v0
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v3, 0x0

    .line 1969
    sparse-switch p1, :sswitch_data_0

    .line 1989
    :cond_0
    invoke-super {p0, p1, p2}, Lcom/jess/ui/v;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    :cond_1
    :goto_0
    return v0

    .line 1972
    :sswitch_0
    invoke-virtual {p0}, Lcom/jess/ui/TwoWayAbsListView;->isEnabled()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1975
    invoke-virtual {p0}, Lcom/jess/ui/TwoWayAbsListView;->isClickable()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/jess/ui/TwoWayAbsListView;->isPressed()Z

    move-result v1

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/jess/ui/TwoWayAbsListView;->V:I

    if-ltz v1, :cond_0

    iget-object v1, p0, Lcom/jess/ui/TwoWayAbsListView;->c:Landroid/widget/ListAdapter;

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/jess/ui/TwoWayAbsListView;->V:I

    iget-object v2, p0, Lcom/jess/ui/TwoWayAbsListView;->c:Landroid/widget/ListAdapter;

    .line 1977
    invoke-interface {v2}, Landroid/widget/ListAdapter;->getCount()I

    move-result v2

    if-ge v1, v2, :cond_0

    .line 1979
    iget v1, p0, Lcom/jess/ui/TwoWayAbsListView;->V:I

    iget v2, p0, Lcom/jess/ui/TwoWayAbsListView;->H:I

    sub-int/2addr v1, v2

    invoke-virtual {p0, v1}, Lcom/jess/ui/TwoWayAbsListView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 1980
    if-eqz v1, :cond_2

    .line 1981
    iget v2, p0, Lcom/jess/ui/TwoWayAbsListView;->V:I

    iget-wide v4, p0, Lcom/jess/ui/TwoWayAbsListView;->W:J

    invoke-virtual {p0, v1, v2, v4, v5}, Lcom/jess/ui/TwoWayAbsListView;->b(Landroid/view/View;IJ)Z

    .line 1982
    invoke-virtual {v1, v3}, Landroid/view/View;->setPressed(Z)V

    .line 1984
    :cond_2
    invoke-virtual {p0, v3}, Lcom/jess/ui/TwoWayAbsListView;->setPressed(Z)V

    goto :goto_0

    .line 1969
    nop

    :sswitch_data_0
    .sparse-switch
        0x17 -> :sswitch_0
        0x42 -> :sswitch_0
    .end sparse-switch
.end method

.method protected onLayout(ZIIII)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1315
    invoke-direct {p0}, Lcom/jess/ui/TwoWayAbsListView;->u()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1316
    invoke-direct {p0}, Lcom/jess/ui/TwoWayAbsListView;->t()V

    .line 1318
    :cond_0
    invoke-super/range {p0 .. p5}, Lcom/jess/ui/v;->onLayout(ZIIII)V

    .line 1319
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/jess/ui/TwoWayAbsListView;->O:Z

    .line 1320
    if-eqz p1, :cond_2

    .line 1321
    invoke-virtual {p0}, Lcom/jess/ui/TwoWayAbsListView;->getChildCount()I

    move-result v2

    move v0, v1

    .line 1322
    :goto_0
    if-ge v0, v2, :cond_1

    .line 1323
    invoke-virtual {p0, v0}, Lcom/jess/ui/TwoWayAbsListView;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->forceLayout()V

    .line 1322
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1325
    :cond_1
    iget-object v0, p0, Lcom/jess/ui/TwoWayAbsListView;->g:Lcom/jess/ui/k;

    invoke-virtual {v0}, Lcom/jess/ui/k;->a()V

    .line 1328
    :cond_2
    invoke-virtual {p0}, Lcom/jess/ui/TwoWayAbsListView;->d()V

    .line 1329
    iput-boolean v1, p0, Lcom/jess/ui/TwoWayAbsListView;->O:Z

    .line 1330
    return-void
.end method

.method protected onMeasure(II)V
    .locals 3

    .prologue
    .line 1297
    invoke-direct {p0}, Lcom/jess/ui/TwoWayAbsListView;->u()Z

    .line 1299
    iget-object v0, p0, Lcom/jess/ui/TwoWayAbsListView;->e:Landroid/graphics/drawable/Drawable;

    if-nez v0, :cond_0

    .line 1300
    invoke-direct {p0}, Lcom/jess/ui/TwoWayAbsListView;->v()V

    .line 1302
    :cond_0
    iget-object v0, p0, Lcom/jess/ui/TwoWayAbsListView;->m:Landroid/graphics/Rect;

    .line 1303
    iget v1, p0, Lcom/jess/ui/TwoWayAbsListView;->i:I

    invoke-virtual {p0}, Lcom/jess/ui/TwoWayAbsListView;->getPaddingLeft()I

    move-result v2

    add-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Rect;->left:I

    .line 1304
    iget v1, p0, Lcom/jess/ui/TwoWayAbsListView;->j:I

    invoke-virtual {p0}, Lcom/jess/ui/TwoWayAbsListView;->getPaddingTop()I

    move-result v2

    add-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Rect;->top:I

    .line 1305
    iget v1, p0, Lcom/jess/ui/TwoWayAbsListView;->k:I

    invoke-virtual {p0}, Lcom/jess/ui/TwoWayAbsListView;->getPaddingRight()I

    move-result v2

    add-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Rect;->right:I

    .line 1306
    iget v1, p0, Lcom/jess/ui/TwoWayAbsListView;->l:I

    invoke-virtual {p0}, Lcom/jess/ui/TwoWayAbsListView;->getPaddingBottom()I

    move-result v2

    add-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Rect;->bottom:I

    .line 1307
    return-void
.end method

.method public onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const/4 v3, -0x1

    const/4 v2, 0x1

    .line 972
    check-cast p1, Lcom/jess/ui/TwoWayAbsListView$SavedState;

    .line 974
    invoke-virtual {p1}, Lcom/jess/ui/TwoWayAbsListView$SavedState;->getSuperState()Landroid/os/Parcelable;

    move-result-object v0

    invoke-super {p0, v0}, Lcom/jess/ui/v;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 975
    iput-boolean v2, p0, Lcom/jess/ui/TwoWayAbsListView;->S:Z

    .line 977
    iget v0, p1, Lcom/jess/ui/TwoWayAbsListView$SavedState;->e:I

    int-to-long v0, v0

    iput-wide v0, p0, Lcom/jess/ui/TwoWayAbsListView;->L:J

    .line 979
    iget-wide v0, p1, Lcom/jess/ui/TwoWayAbsListView$SavedState;->a:J

    cmp-long v0, v0, v4

    if-ltz v0, :cond_1

    .line 980
    iput-boolean v2, p0, Lcom/jess/ui/TwoWayAbsListView;->M:Z

    .line 981
    iget-wide v0, p1, Lcom/jess/ui/TwoWayAbsListView$SavedState;->a:J

    iput-wide v0, p0, Lcom/jess/ui/TwoWayAbsListView;->K:J

    .line 982
    iget v0, p1, Lcom/jess/ui/TwoWayAbsListView$SavedState;->d:I

    iput v0, p0, Lcom/jess/ui/TwoWayAbsListView;->J:I

    .line 983
    iget v0, p1, Lcom/jess/ui/TwoWayAbsListView$SavedState;->c:I

    iput v0, p0, Lcom/jess/ui/TwoWayAbsListView;->I:I

    .line 984
    const/4 v0, 0x0

    iput v0, p0, Lcom/jess/ui/TwoWayAbsListView;->N:I

    .line 998
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/jess/ui/TwoWayAbsListView;->requestLayout()V

    .line 999
    return-void

    .line 985
    :cond_1
    iget-wide v0, p1, Lcom/jess/ui/TwoWayAbsListView$SavedState;->b:J

    cmp-long v0, v0, v4

    if-ltz v0, :cond_0

    .line 986
    invoke-virtual {p0, v3}, Lcom/jess/ui/TwoWayAbsListView;->setSelectedPositionInt(I)V

    .line 988
    invoke-virtual {p0, v3}, Lcom/jess/ui/TwoWayAbsListView;->setNextSelectedPositionInt(I)V

    .line 989
    iput-boolean v2, p0, Lcom/jess/ui/TwoWayAbsListView;->M:Z

    .line 990
    iget-wide v0, p1, Lcom/jess/ui/TwoWayAbsListView$SavedState;->b:J

    iput-wide v0, p0, Lcom/jess/ui/TwoWayAbsListView;->K:J

    .line 991
    iget v0, p1, Lcom/jess/ui/TwoWayAbsListView$SavedState;->d:I

    iput v0, p0, Lcom/jess/ui/TwoWayAbsListView;->J:I

    .line 992
    iget v0, p1, Lcom/jess/ui/TwoWayAbsListView$SavedState;->c:I

    iput v0, p0, Lcom/jess/ui/TwoWayAbsListView;->I:I

    .line 993
    iput v2, p0, Lcom/jess/ui/TwoWayAbsListView;->N:I

    goto :goto_0
.end method

.method public onSaveInstanceState()Landroid/os/Parcelable;
    .locals 10

    .prologue
    const-wide/16 v8, -0x1

    const/4 v1, 0x0

    .line 924
    invoke-super {p0}, Lcom/jess/ui/v;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v0

    .line 926
    new-instance v2, Lcom/jess/ui/TwoWayAbsListView$SavedState;

    invoke-direct {v2, v0}, Lcom/jess/ui/TwoWayAbsListView$SavedState;-><init>(Landroid/os/Parcelable;)V

    .line 928
    invoke-virtual {p0}, Lcom/jess/ui/TwoWayAbsListView;->getChildCount()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    .line 929
    :goto_0
    invoke-virtual {p0}, Lcom/jess/ui/TwoWayAbsListView;->getSelectedItemId()J

    move-result-wide v4

    .line 930
    iput-wide v4, v2, Lcom/jess/ui/TwoWayAbsListView$SavedState;->a:J

    .line 931
    invoke-virtual {p0}, Lcom/jess/ui/TwoWayAbsListView;->getHeight()I

    move-result v3

    iput v3, v2, Lcom/jess/ui/TwoWayAbsListView$SavedState;->e:I

    .line 933
    const-wide/16 v6, 0x0

    cmp-long v3, v4, v6

    if-ltz v3, :cond_1

    .line 935
    iget v0, p0, Lcom/jess/ui/TwoWayAbsListView;->x:I

    iput v0, v2, Lcom/jess/ui/TwoWayAbsListView$SavedState;->c:I

    .line 936
    invoke-virtual {p0}, Lcom/jess/ui/TwoWayAbsListView;->getSelectedItemPosition()I

    move-result v0

    iput v0, v2, Lcom/jess/ui/TwoWayAbsListView$SavedState;->d:I

    .line 937
    iput-wide v8, v2, Lcom/jess/ui/TwoWayAbsListView$SavedState;->b:J

    .line 967
    :goto_1
    return-object v2

    :cond_0
    move v0, v1

    .line 928
    goto :goto_0

    .line 939
    :cond_1
    if-eqz v0, :cond_3

    .line 941
    invoke-virtual {p0, v1}, Lcom/jess/ui/TwoWayAbsListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 942
    iget-boolean v1, p0, Lcom/jess/ui/TwoWayAbsListView;->B:Z

    if-eqz v1, :cond_2

    .line 943
    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v0

    iput v0, v2, Lcom/jess/ui/TwoWayAbsListView$SavedState;->c:I

    .line 947
    :goto_2
    iget v0, p0, Lcom/jess/ui/TwoWayAbsListView;->H:I

    iput v0, v2, Lcom/jess/ui/TwoWayAbsListView$SavedState;->d:I

    .line 948
    iget-object v0, p0, Lcom/jess/ui/TwoWayAbsListView;->c:Landroid/widget/ListAdapter;

    iget v1, p0, Lcom/jess/ui/TwoWayAbsListView;->H:I

    invoke-interface {v0, v1}, Landroid/widget/ListAdapter;->getItemId(I)J

    move-result-wide v0

    iput-wide v0, v2, Lcom/jess/ui/TwoWayAbsListView$SavedState;->b:J

    goto :goto_1

    .line 945
    :cond_2
    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v0

    iput v0, v2, Lcom/jess/ui/TwoWayAbsListView$SavedState;->c:I

    goto :goto_2

    .line 950
    :cond_3
    iput v1, v2, Lcom/jess/ui/TwoWayAbsListView$SavedState;->c:I

    .line 951
    iput-wide v8, v2, Lcom/jess/ui/TwoWayAbsListView$SavedState;->b:J

    .line 952
    iput v1, v2, Lcom/jess/ui/TwoWayAbsListView$SavedState;->d:I

    goto :goto_1
.end method

.method protected onSizeChanged(IIII)V
    .locals 1

    .prologue
    .line 1591
    invoke-virtual {p0}, Lcom/jess/ui/TwoWayAbsListView;->getChildCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 1592
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/jess/ui/TwoWayAbsListView;->S:Z

    .line 1593
    invoke-virtual {p0}, Lcom/jess/ui/TwoWayAbsListView;->r()V

    .line 1599
    :cond_0
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 2100
    iget-object v0, p0, Lcom/jess/ui/TwoWayAbsListView;->D:Lcom/jess/ui/n;

    invoke-virtual {v0, p1}, Lcom/jess/ui/n;->b(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public onTouchModeChanged(Z)V
    .locals 1

    .prologue
    .line 2095
    iget-object v0, p0, Lcom/jess/ui/TwoWayAbsListView;->D:Lcom/jess/ui/n;

    invoke-virtual {v0, p1}, Lcom/jess/ui/n;->b(Z)V

    .line 2096
    return-void
.end method

.method public onWindowFocusChanged(Z)V
    .locals 1

    .prologue
    .line 2158
    invoke-super {p0, p1}, Lcom/jess/ui/v;->onWindowFocusChanged(Z)V

    .line 2159
    iget-object v0, p0, Lcom/jess/ui/TwoWayAbsListView;->D:Lcom/jess/ui/n;

    invoke-virtual {v0, p1}, Lcom/jess/ui/n;->a(Z)V

    .line 2160
    return-void
.end method

.method public requestLayout()V
    .locals 1

    .prologue
    .line 1055
    iget-boolean v0, p0, Lcom/jess/ui/TwoWayAbsListView;->ae:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/jess/ui/TwoWayAbsListView;->O:Z

    if-nez v0, :cond_0

    .line 1056
    invoke-super {p0}, Lcom/jess/ui/v;->requestLayout()V

    .line 1058
    :cond_0
    return-void
.end method

.method public setCacheColorHint(I)V
    .locals 3

    .prologue
    .line 2928
    iget v0, p0, Lcom/jess/ui/TwoWayAbsListView;->as:I

    if-eq p1, v0, :cond_1

    .line 2929
    iput p1, p0, Lcom/jess/ui/TwoWayAbsListView;->as:I

    .line 2930
    invoke-virtual {p0}, Lcom/jess/ui/TwoWayAbsListView;->getChildCount()I

    move-result v1

    .line 2931
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    .line 2932
    invoke-virtual {p0, v0}, Lcom/jess/ui/TwoWayAbsListView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, p1}, Landroid/view/View;->setDrawingCacheBackgroundColor(I)V

    .line 2931
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2934
    :cond_0
    iget-object v0, p0, Lcom/jess/ui/TwoWayAbsListView;->g:Lcom/jess/ui/k;

    invoke-virtual {v0, p1}, Lcom/jess/ui/k;->e(I)V

    .line 2936
    :cond_1
    return-void
.end method

.method public setDrawSelectorOnTop(Z)V
    .locals 0

    .prologue
    .line 1645
    iput-boolean p1, p0, Lcom/jess/ui/TwoWayAbsListView;->d:Z

    .line 1646
    return-void
.end method

.method public setOnScrollListener(Lcom/jess/ui/i;)V
    .locals 0

    .prologue
    .line 722
    iput-object p1, p0, Lcom/jess/ui/TwoWayAbsListView;->ag:Lcom/jess/ui/i;

    .line 723
    invoke-virtual {p0}, Lcom/jess/ui/TwoWayAbsListView;->a()V

    .line 724
    return-void
.end method

.method public setRecyclerListener(Lcom/jess/ui/l;)V
    .locals 1

    .prologue
    .line 3044
    iget-object v0, p0, Lcom/jess/ui/TwoWayAbsListView;->g:Lcom/jess/ui/k;

    invoke-static {v0, p1}, Lcom/jess/ui/k;->a(Lcom/jess/ui/k;Lcom/jess/ui/l;)Lcom/jess/ui/l;

    .line 3045
    return-void
.end method

.method public setScrollDirectionLandscape(I)V
    .locals 2

    .prologue
    .line 2897
    iget-boolean v1, p0, Lcom/jess/ui/TwoWayAbsListView;->aA:Z

    .line 2898
    if-nez p1, :cond_1

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/jess/ui/TwoWayAbsListView;->aA:Z

    .line 2899
    iget-boolean v0, p0, Lcom/jess/ui/TwoWayAbsListView;->aA:Z

    if-eq v1, v0, :cond_0

    .line 2900
    invoke-direct {p0}, Lcom/jess/ui/TwoWayAbsListView;->t()V

    .line 2902
    invoke-virtual {p0}, Lcom/jess/ui/TwoWayAbsListView;->c()V

    .line 2903
    iget-object v0, p0, Lcom/jess/ui/TwoWayAbsListView;->g:Lcom/jess/ui/k;

    invoke-virtual {v0}, Lcom/jess/ui/k;->b()V

    .line 2905
    :cond_0
    return-void

    .line 2898
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setScrollDirectionPortrait(I)V
    .locals 2

    .prologue
    .line 2869
    iget-boolean v1, p0, Lcom/jess/ui/TwoWayAbsListView;->az:Z

    .line 2870
    if-nez p1, :cond_1

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/jess/ui/TwoWayAbsListView;->az:Z

    .line 2871
    iget-boolean v0, p0, Lcom/jess/ui/TwoWayAbsListView;->az:Z

    if-eq v1, v0, :cond_0

    .line 2872
    invoke-direct {p0}, Lcom/jess/ui/TwoWayAbsListView;->t()V

    .line 2874
    invoke-virtual {p0}, Lcom/jess/ui/TwoWayAbsListView;->c()V

    .line 2875
    iget-object v0, p0, Lcom/jess/ui/TwoWayAbsListView;->g:Lcom/jess/ui/k;

    invoke-virtual {v0}, Lcom/jess/ui/k;->b()V

    .line 2877
    :cond_0
    return-void

    .line 2870
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setScrollingCacheEnabled(Z)V
    .locals 1

    .prologue
    .line 766
    iget-boolean v0, p0, Lcom/jess/ui/TwoWayAbsListView;->z:Z

    if-eqz v0, :cond_0

    if-nez p1, :cond_0

    .line 767
    iget-object v0, p0, Lcom/jess/ui/TwoWayAbsListView;->D:Lcom/jess/ui/n;

    invoke-virtual {v0}, Lcom/jess/ui/n;->c()V

    .line 769
    :cond_0
    iput-boolean p1, p0, Lcom/jess/ui/TwoWayAbsListView;->z:Z

    .line 770
    return-void
.end method

.method abstract setSelectionInt(I)V
.end method

.method public setSelector(I)V
    .locals 1

    .prologue
    .line 1656
    invoke-virtual {p0}, Lcom/jess/ui/TwoWayAbsListView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/jess/ui/TwoWayAbsListView;->setSelector(Landroid/graphics/drawable/Drawable;)V

    .line 1657
    return-void
.end method

.method public setSelector(Landroid/graphics/drawable/Drawable;)V
    .locals 2

    .prologue
    .line 1660
    iget-object v0, p0, Lcom/jess/ui/TwoWayAbsListView;->e:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    .line 1661
    iget-object v0, p0, Lcom/jess/ui/TwoWayAbsListView;->e:Landroid/graphics/drawable/Drawable;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    .line 1662
    iget-object v0, p0, Lcom/jess/ui/TwoWayAbsListView;->e:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0, v0}, Lcom/jess/ui/TwoWayAbsListView;->unscheduleDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1664
    :cond_0
    iput-object p1, p0, Lcom/jess/ui/TwoWayAbsListView;->e:Landroid/graphics/drawable/Drawable;

    .line 1665
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    .line 1666
    invoke-virtual {p1, v0}, Landroid/graphics/drawable/Drawable;->getPadding(Landroid/graphics/Rect;)Z

    .line 1667
    iget v1, v0, Landroid/graphics/Rect;->left:I

    iput v1, p0, Lcom/jess/ui/TwoWayAbsListView;->i:I

    .line 1668
    iget v1, v0, Landroid/graphics/Rect;->top:I

    iput v1, p0, Lcom/jess/ui/TwoWayAbsListView;->j:I

    .line 1669
    iget v1, v0, Landroid/graphics/Rect;->right:I

    iput v1, p0, Lcom/jess/ui/TwoWayAbsListView;->k:I

    .line 1670
    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    iput v0, p0, Lcom/jess/ui/TwoWayAbsListView;->l:I

    .line 1671
    invoke-virtual {p1, p0}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    .line 1672
    invoke-virtual {p0}, Lcom/jess/ui/TwoWayAbsListView;->getDrawableState()[I

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    .line 1673
    return-void
.end method

.method public setSmoothScrollbarEnabled(Z)V
    .locals 0

    .prologue
    .line 701
    iput-boolean p1, p0, Lcom/jess/ui/TwoWayAbsListView;->ah:Z

    .line 702
    return-void
.end method

.method public setStackFromBottom(Z)V
    .locals 1

    .prologue
    .line 837
    iget-boolean v0, p0, Lcom/jess/ui/TwoWayAbsListView;->y:Z

    if-eq v0, p1, :cond_0

    .line 838
    iput-boolean p1, p0, Lcom/jess/ui/TwoWayAbsListView;->y:Z

    .line 839
    invoke-virtual {p0}, Lcom/jess/ui/TwoWayAbsListView;->b()V

    .line 841
    :cond_0
    return-void
.end method

.method public setTranscriptMode(I)V
    .locals 0

    .prologue
    .line 2847
    iput p1, p0, Lcom/jess/ui/TwoWayAbsListView;->ar:I

    .line 2848
    return-void
.end method

.method public showContextMenuForChild(Landroid/view/View;)Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 1941
    invoke-virtual {p0, p1}, Lcom/jess/ui/TwoWayAbsListView;->b(Landroid/view/View;)I

    move-result v3

    .line 1942
    if-ltz v3, :cond_1

    .line 1943
    iget-object v1, p0, Lcom/jess/ui/TwoWayAbsListView;->c:Landroid/widget/ListAdapter;

    invoke-interface {v1, v3}, Landroid/widget/ListAdapter;->getItemId(I)J

    move-result-wide v4

    .line 1946
    iget-object v1, p0, Lcom/jess/ui/TwoWayAbsListView;->R:Lcom/jess/ui/aa;

    if-eqz v1, :cond_0

    .line 1947
    iget-object v0, p0, Lcom/jess/ui/TwoWayAbsListView;->R:Lcom/jess/ui/aa;

    move-object v1, p0

    move-object v2, p1

    invoke-interface/range {v0 .. v5}, Lcom/jess/ui/aa;->a(Lcom/jess/ui/v;Landroid/view/View;IJ)Z

    move-result v0

    .line 1950
    :cond_0
    if-nez v0, :cond_1

    .line 1951
    iget v0, p0, Lcom/jess/ui/TwoWayAbsListView;->H:I

    sub-int v0, v3, v0

    .line 1952
    invoke-virtual {p0, v0}, Lcom/jess/ui/TwoWayAbsListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 1951
    invoke-virtual {p0, v0, v3, v4, v5}, Lcom/jess/ui/TwoWayAbsListView;->a(Landroid/view/View;IJ)Landroid/view/ContextMenu$ContextMenuInfo;

    move-result-object v0

    iput-object v0, p0, Lcom/jess/ui/TwoWayAbsListView;->aj:Landroid/view/ContextMenu$ContextMenuInfo;

    .line 1954
    invoke-super {p0, p1}, Lcom/jess/ui/v;->showContextMenuForChild(Landroid/view/View;)Z

    move-result v0

    .line 1959
    :cond_1
    return v0
.end method

.method public verifyDrawable(Landroid/graphics/drawable/Drawable;)Z
    .locals 1

    .prologue
    .line 1778
    iget-object v0, p0, Lcom/jess/ui/TwoWayAbsListView;->e:Landroid/graphics/drawable/Drawable;

    if-eq v0, p1, :cond_0

    invoke-super {p0, p1}, Lcom/jess/ui/v;->verifyDrawable(Landroid/graphics/drawable/Drawable;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
