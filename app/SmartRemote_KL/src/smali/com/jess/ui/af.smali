.class Lcom/jess/ui/af;
.super Lcom/jess/ui/ae;


# instance fields
.field final synthetic b:Lcom/jess/ui/TwoWayGridView;


# direct methods
.method private constructor <init>(Lcom/jess/ui/TwoWayGridView;)V
    .locals 1

    .prologue
    .line 2153
    iput-object p1, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/jess/ui/ae;-><init>(Lcom/jess/ui/TwoWayGridView;Lcom/jess/ui/ad;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/jess/ui/TwoWayGridView;Lcom/jess/ui/ad;)V
    .locals 0

    .prologue
    .line 2153
    invoke-direct {p0, p1}, Lcom/jess/ui/af;-><init>(Lcom/jess/ui/TwoWayGridView;)V

    return-void
.end method

.method private a(IIII)I
    .locals 2

    .prologue
    .line 3064
    .line 3065
    add-int v0, p4, p3

    add-int/lit8 v0, v0, -0x1

    iget-object v1, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    iget v1, v1, Lcom/jess/ui/TwoWayGridView;->aa:I

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_0

    .line 3066
    sub-int/2addr p1, p2

    .line 3068
    :cond_0
    return p1
.end method

.method private a(IIZ)Landroid/view/View;
    .locals 14

    .prologue
    .line 2261
    iget-object v0, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-static {v0}, Lcom/jess/ui/TwoWayGridView;->o(Lcom/jess/ui/TwoWayGridView;)I

    move-result v10

    .line 2262
    iget-object v0, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-static {v0}, Lcom/jess/ui/TwoWayGridView;->b(Lcom/jess/ui/TwoWayGridView;)I

    move-result v7

    .line 2265
    iget-object v0, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    iget-object v0, v0, Lcom/jess/ui/TwoWayGridView;->m:Landroid/graphics/Rect;

    iget v1, v0, Landroid/graphics/Rect;->top:I

    iget-object v0, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    .line 2266
    invoke-static {v0}, Lcom/jess/ui/TwoWayGridView;->f(Lcom/jess/ui/TwoWayGridView;)I

    move-result v0

    const/4 v2, 0x3

    if-ne v0, v2, :cond_1

    move v0, v7

    :goto_0
    add-int/2addr v0, v1

    .line 2268
    iget-object v1, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    iget-boolean v1, v1, Lcom/jess/ui/TwoWayGridView;->y:Z

    if-nez v1, :cond_2

    .line 2269
    iget-object v1, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-static {v1}, Lcom/jess/ui/TwoWayGridView;->n(Lcom/jess/ui/TwoWayGridView;)I

    move-result v1

    add-int/2addr v1, p1

    iget-object v2, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    iget v2, v2, Lcom/jess/ui/TwoWayGridView;->aa:I

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    move v8, v1

    .line 2279
    :goto_1
    const/4 v9, 0x0

    .line 2281
    iget-object v1, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-virtual {v1}, Lcom/jess/ui/TwoWayGridView;->g()Z

    move-result v11

    .line 2282
    iget-object v1, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-virtual {v1}, Lcom/jess/ui/TwoWayGridView;->f()Z

    move-result v12

    .line 2283
    iget-object v1, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    iget v13, v1, Lcom/jess/ui/TwoWayGridView;->V:I

    .line 2285
    const/4 v2, 0x0

    move v1, p1

    move v4, v0

    .line 2286
    :goto_2
    if-ge v1, v8, :cond_5

    .line 2288
    if-ne v1, v13, :cond_3

    const/4 v5, 0x1

    .line 2291
    :goto_3
    if-eqz p3, :cond_4

    const/4 v6, -0x1

    :goto_4
    move-object v0, p0

    move/from16 v2, p2

    move/from16 v3, p3

    .line 2292
    invoke-virtual/range {v0 .. v6}, Lcom/jess/ui/af;->a(IIZIZI)Landroid/view/View;

    move-result-object v2

    .line 2294
    add-int v0, v4, v10

    .line 2295
    add-int/lit8 v3, v8, -0x1

    if-ge v1, v3, :cond_8

    .line 2296
    add-int/2addr v0, v7

    move v3, v0

    .line 2299
    :goto_5
    if-eqz v5, :cond_7

    if-nez v11, :cond_0

    if-eqz v12, :cond_7

    :cond_0
    move-object v0, v2

    .line 2286
    :goto_6
    add-int/lit8 v1, v1, 0x1

    move-object v9, v0

    move v4, v3

    goto :goto_2

    .line 2266
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 2271
    :cond_2
    add-int/lit8 v1, p1, 0x1

    .line 2272
    const/4 v2, 0x0

    iget-object v3, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-static {v3}, Lcom/jess/ui/TwoWayGridView;->n(Lcom/jess/ui/TwoWayGridView;)I

    move-result v3

    sub-int v3, p1, v3

    add-int/lit8 v3, v3, 0x1

    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result p1

    .line 2274
    sub-int v2, v1, p1

    iget-object v3, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-static {v3}, Lcom/jess/ui/TwoWayGridView;->n(Lcom/jess/ui/TwoWayGridView;)I

    move-result v3

    if-ge v2, v3, :cond_9

    .line 2275
    iget-object v2, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-static {v2}, Lcom/jess/ui/TwoWayGridView;->n(Lcom/jess/ui/TwoWayGridView;)I

    move-result v2

    sub-int v3, v1, p1

    sub-int/2addr v2, v3

    add-int v3, v10, v7

    mul-int/2addr v2, v3

    add-int/2addr v0, v2

    move v8, v1

    goto :goto_1

    .line 2288
    :cond_3
    const/4 v5, 0x0

    goto :goto_3

    .line 2291
    :cond_4
    sub-int v6, v1, p1

    goto :goto_4

    .line 2304
    :cond_5
    iget-object v0, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-static {v0, v2}, Lcom/jess/ui/TwoWayGridView;->a(Lcom/jess/ui/TwoWayGridView;Landroid/view/View;)Landroid/view/View;

    .line 2306
    if-eqz v9, :cond_6

    .line 2307
    iget-object v0, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    iget-object v1, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-static {v1}, Lcom/jess/ui/TwoWayGridView;->c(Lcom/jess/ui/TwoWayGridView;)Landroid/view/View;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/jess/ui/TwoWayGridView;->b(Lcom/jess/ui/TwoWayGridView;Landroid/view/View;)Landroid/view/View;

    .line 2310
    :cond_6
    return-object v9

    :cond_7
    move-object v0, v9

    goto :goto_6

    :cond_8
    move v3, v0

    goto :goto_5

    :cond_9
    move v8, v1

    goto/16 :goto_1
.end method

.method private a(III)V
    .locals 4

    .prologue
    .line 2504
    iget-object v0, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    iget v0, v0, Lcom/jess/ui/TwoWayGridView;->H:I

    add-int/2addr v0, p3

    add-int/lit8 v0, v0, -0x1

    .line 2505
    iget-object v1, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    iget v1, v1, Lcom/jess/ui/TwoWayGridView;->aa:I

    add-int/lit8 v1, v1, -0x1

    if-ne v0, v1, :cond_3

    if-lez p3, :cond_3

    .line 2507
    iget-object v0, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    add-int/lit8 v1, p3, -0x1

    invoke-virtual {v0, v1}, Lcom/jess/ui/TwoWayGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 2510
    invoke-virtual {v0}, Landroid/view/View;->getRight()I

    move-result v0

    .line 2512
    iget-object v1, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-virtual {v1}, Lcom/jess/ui/TwoWayGridView;->getRight()I

    move-result v1

    iget-object v2, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-virtual {v2}, Lcom/jess/ui/TwoWayGridView;->getLeft()I

    move-result v2

    sub-int/2addr v1, v2

    iget-object v2, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    iget-object v2, v2, Lcom/jess/ui/TwoWayGridView;->m:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->right:I

    sub-int/2addr v1, v2

    .line 2516
    sub-int v0, v1, v0

    .line 2518
    iget-object v1, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/jess/ui/TwoWayGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 2519
    invoke-virtual {v1}, Landroid/view/View;->getLeft()I

    move-result v2

    .line 2523
    if-lez v0, :cond_3

    iget-object v3, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    iget v3, v3, Lcom/jess/ui/TwoWayGridView;->H:I

    if-gtz v3, :cond_0

    iget-object v3, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    iget-object v3, v3, Lcom/jess/ui/TwoWayGridView;->m:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->left:I

    if-ge v2, v3, :cond_3

    .line 2524
    :cond_0
    iget-object v3, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    iget v3, v3, Lcom/jess/ui/TwoWayGridView;->H:I

    if-nez v3, :cond_1

    .line 2526
    iget-object v3, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    iget-object v3, v3, Lcom/jess/ui/TwoWayGridView;->m:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->left:I

    sub-int v2, v3, v2

    invoke-static {v0, v2}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 2530
    :cond_1
    iget-object v2, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-virtual {v2, v0}, Lcom/jess/ui/TwoWayGridView;->f(I)V

    .line 2531
    iget-object v0, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    iget v0, v0, Lcom/jess/ui/TwoWayGridView;->H:I

    if-lez v0, :cond_3

    .line 2534
    iget-object v0, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    iget v0, v0, Lcom/jess/ui/TwoWayGridView;->H:I

    iget-object v2, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    iget-boolean v2, v2, Lcom/jess/ui/TwoWayGridView;->y:Z

    if-eqz v2, :cond_2

    const/4 p1, 0x1

    :cond_2
    sub-int/2addr v0, p1

    .line 2535
    invoke-virtual {v1}, Landroid/view/View;->getLeft()I

    move-result v1

    sub-int/2addr v1, p2

    .line 2534
    invoke-direct {p0, v0, v1}, Lcom/jess/ui/af;->d(II)Landroid/view/View;

    .line 2537
    invoke-direct {p0}, Lcom/jess/ui/af;->b()V

    .line 2541
    :cond_3
    return-void
.end method

.method private a(Landroid/view/View;II)V
    .locals 2

    .prologue
    .line 3103
    invoke-virtual {p1}, Landroid/view/View;->getRight()I

    move-result v0

    if-le v0, p3, :cond_0

    .line 3107
    invoke-virtual {p1}, Landroid/view/View;->getLeft()I

    move-result v0

    sub-int/2addr v0, p2

    .line 3111
    invoke-virtual {p1}, Landroid/view/View;->getRight()I

    move-result v1

    sub-int/2addr v1, p3

    .line 3112
    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 3115
    iget-object v1, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    neg-int v0, v0

    invoke-virtual {v1, v0}, Lcom/jess/ui/TwoWayGridView;->f(I)V

    .line 3117
    :cond_0
    return-void
.end method

.method private a(Landroid/view/View;IIZIZZI)V
    .locals 10

    .prologue
    .line 3214
    if-eqz p6, :cond_5

    iget-object v1, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-virtual {v1}, Lcom/jess/ui/TwoWayGridView;->g()Z

    move-result v1

    if-eqz v1, :cond_5

    const/4 v1, 0x1

    move v2, v1

    .line 3215
    :goto_0
    invoke-virtual {p1}, Landroid/view/View;->isSelected()Z

    move-result v1

    if-eq v2, v1, :cond_6

    const/4 v1, 0x1

    move v3, v1

    .line 3216
    :goto_1
    iget-object v1, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    iget v1, v1, Lcom/jess/ui/TwoWayGridView;->w:I

    .line 3217
    if-lez v1, :cond_7

    const/4 v4, 0x3

    if-ge v1, v4, :cond_7

    iget-object v1, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    iget v1, v1, Lcom/jess/ui/TwoWayGridView;->t:I

    if-ne v1, p2, :cond_7

    const/4 v1, 0x1

    move v4, v1

    .line 3219
    :goto_2
    invoke-virtual {p1}, Landroid/view/View;->isPressed()Z

    move-result v1

    if-eq v4, v1, :cond_8

    const/4 v1, 0x1

    move v6, v1

    .line 3221
    :goto_3
    if-eqz p7, :cond_0

    if-nez v3, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->isLayoutRequested()Z

    move-result v1

    if-eqz v1, :cond_9

    :cond_0
    const/4 v1, 0x1

    move v5, v1

    .line 3225
    :goto_4
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Lcom/jess/ui/h;

    .line 3226
    if-nez v1, :cond_1

    .line 3227
    new-instance v1, Lcom/jess/ui/h;

    const/4 v7, -0x2

    const/4 v8, -0x1

    const/4 v9, 0x0

    invoke-direct {v1, v7, v8, v9}, Lcom/jess/ui/h;-><init>(III)V

    .line 3230
    :cond_1
    iget-object v7, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    iget-object v7, v7, Lcom/jess/ui/TwoWayGridView;->c:Landroid/widget/ListAdapter;

    invoke-interface {v7, p2}, Landroid/widget/ListAdapter;->getItemViewType(I)I

    move-result v7

    iput v7, v1, Lcom/jess/ui/h;->a:I

    .line 3232
    if-eqz p7, :cond_a

    iget-boolean v7, v1, Lcom/jess/ui/h;->b:Z

    if-nez v7, :cond_a

    .line 3233
    iget-object v7, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    move/from16 v0, p8

    invoke-static {v7, p1, v0, v1}, Lcom/jess/ui/TwoWayGridView;->b(Lcom/jess/ui/TwoWayGridView;Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    .line 3239
    :goto_5
    if-eqz v3, :cond_2

    .line 3240
    invoke-virtual {p1, v2}, Landroid/view/View;->setSelected(Z)V

    .line 3241
    if-eqz v2, :cond_2

    .line 3242
    iget-object v2, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-virtual {v2}, Lcom/jess/ui/TwoWayGridView;->requestFocus()Z

    .line 3246
    :cond_2
    if-eqz v6, :cond_3

    .line 3247
    invoke-virtual {p1, v4}, Landroid/view/View;->setPressed(Z)V

    .line 3250
    :cond_3
    if-eqz v5, :cond_b

    .line 3251
    const/4 v2, 0x0

    const/4 v3, 0x0

    .line 3252
    invoke-static {v2, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    const/4 v3, 0x0

    iget v4, v1, Lcom/jess/ui/h;->width:I

    .line 3251
    invoke-static {v2, v3, v4}, Landroid/view/ViewGroup;->getChildMeasureSpec(III)I

    move-result v2

    .line 3254
    iget-object v3, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    .line 3255
    invoke-static {v3}, Lcom/jess/ui/TwoWayGridView;->o(Lcom/jess/ui/TwoWayGridView;)I

    move-result v3

    const/high16 v4, 0x40000000    # 2.0f

    invoke-static {v3, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    const/4 v4, 0x0

    iget v1, v1, Lcom/jess/ui/h;->height:I

    .line 3254
    invoke-static {v3, v4, v1}, Landroid/view/ViewGroup;->getChildMeasureSpec(III)I

    move-result v1

    .line 3256
    invoke-virtual {p1, v2, v1}, Landroid/view/View;->measure(II)V

    .line 3261
    :goto_6
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    .line 3262
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v2

    .line 3264
    if-eqz p4, :cond_c

    .line 3267
    :goto_7
    iget-object v3, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-static {v3}, Lcom/jess/ui/TwoWayGridView;->m(Lcom/jess/ui/TwoWayGridView;)I

    move-result v3

    and-int/lit8 v3, v3, 0x70

    sparse-switch v3, :sswitch_data_0

    .line 3282
    :goto_8
    :sswitch_0
    if-eqz v5, :cond_d

    .line 3283
    add-int/2addr v1, p3

    .line 3284
    add-int/2addr v2, p5

    .line 3285
    invoke-virtual {p1, p3, p5, v1, v2}, Landroid/view/View;->layout(IIII)V

    .line 3291
    :goto_9
    iget-object v1, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    iget-boolean v1, v1, Lcom/jess/ui/TwoWayGridView;->s:Z

    if-eqz v1, :cond_4

    .line 3292
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, Landroid/view/View;->setDrawingCacheEnabled(Z)V

    .line 3294
    :cond_4
    return-void

    .line 3214
    :cond_5
    const/4 v1, 0x0

    move v2, v1

    goto/16 :goto_0

    .line 3215
    :cond_6
    const/4 v1, 0x0

    move v3, v1

    goto/16 :goto_1

    .line 3217
    :cond_7
    const/4 v1, 0x0

    move v4, v1

    goto/16 :goto_2

    .line 3219
    :cond_8
    const/4 v1, 0x0

    move v6, v1

    goto/16 :goto_3

    .line 3221
    :cond_9
    const/4 v1, 0x0

    move v5, v1

    goto/16 :goto_4

    .line 3235
    :cond_a
    const/4 v7, 0x0

    iput-boolean v7, v1, Lcom/jess/ui/h;->b:Z

    .line 3236
    iget-object v7, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    const/4 v8, 0x1

    move/from16 v0, p8

    invoke-static {v7, p1, v0, v1, v8}, Lcom/jess/ui/TwoWayGridView;->b(Lcom/jess/ui/TwoWayGridView;Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;Z)Z

    goto :goto_5

    .line 3258
    :cond_b
    iget-object v1, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-static {v1, p1}, Lcom/jess/ui/TwoWayGridView;->d(Lcom/jess/ui/TwoWayGridView;Landroid/view/View;)V

    goto :goto_6

    .line 3264
    :cond_c
    sub-int/2addr p3, v1

    goto :goto_7

    .line 3272
    :sswitch_1
    iget-object v3, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-static {v3}, Lcom/jess/ui/TwoWayGridView;->o(Lcom/jess/ui/TwoWayGridView;)I

    move-result v3

    sub-int/2addr v3, v2

    div-int/lit8 v3, v3, 0x2

    add-int/2addr p5, v3

    .line 3273
    goto :goto_8

    .line 3275
    :sswitch_2
    iget-object v3, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-static {v3}, Lcom/jess/ui/TwoWayGridView;->o(Lcom/jess/ui/TwoWayGridView;)I

    move-result v3

    add-int/2addr v3, p5

    sub-int p5, v3, v2

    .line 3276
    goto :goto_8

    .line 3287
    :cond_d
    invoke-virtual {p1}, Landroid/view/View;->getLeft()I

    move-result v1

    sub-int v1, p3, v1

    invoke-virtual {p1, v1}, Landroid/view/View;->offsetLeftAndRight(I)V

    .line 3288
    invoke-virtual {p1}, Landroid/view/View;->getTop()I

    move-result v1

    sub-int v1, p5, v1

    invoke-virtual {p1, v1}, Landroid/view/View;->offsetTopAndBottom(I)V

    goto :goto_9

    .line 3267
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x5 -> :sswitch_2
        0x30 -> :sswitch_0
    .end sparse-switch
.end method

.method private b()V
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 3152
    iget-object v1, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-virtual {v1}, Lcom/jess/ui/TwoWayGridView;->getChildCount()I

    move-result v2

    .line 3154
    if-lez v2, :cond_2

    .line 3158
    iget-object v1, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    iget-boolean v1, v1, Lcom/jess/ui/TwoWayGridView;->y:Z

    if-nez v1, :cond_3

    .line 3161
    iget-object v1, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-virtual {v1, v0}, Lcom/jess/ui/TwoWayGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 3162
    invoke-virtual {v1}, Landroid/view/View;->getLeft()I

    move-result v1

    iget-object v2, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    iget-object v2, v2, Lcom/jess/ui/TwoWayGridView;->m:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->left:I

    sub-int/2addr v1, v2

    .line 3163
    iget-object v2, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    iget v2, v2, Lcom/jess/ui/TwoWayGridView;->H:I

    if-eqz v2, :cond_0

    .line 3166
    iget-object v2, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-static {v2}, Lcom/jess/ui/TwoWayGridView;->e(Lcom/jess/ui/TwoWayGridView;)I

    move-result v2

    sub-int/2addr v1, v2

    .line 3168
    :cond_0
    if-gez v1, :cond_5

    .line 3189
    :cond_1
    :goto_0
    if-eqz v0, :cond_2

    .line 3190
    iget-object v1, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    neg-int v0, v0

    invoke-virtual {v1, v0}, Lcom/jess/ui/TwoWayGridView;->f(I)V

    .line 3193
    :cond_2
    return-void

    .line 3174
    :cond_3
    iget-object v1, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    add-int/lit8 v3, v2, -0x1

    invoke-virtual {v1, v3}, Lcom/jess/ui/TwoWayGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 3175
    invoke-virtual {v1}, Landroid/view/View;->getRight()I

    move-result v1

    iget-object v3, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-virtual {v3}, Lcom/jess/ui/TwoWayGridView;->getWidth()I

    move-result v3

    iget-object v4, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    iget-object v4, v4, Lcom/jess/ui/TwoWayGridView;->m:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->right:I

    sub-int/2addr v3, v4

    sub-int/2addr v1, v3

    .line 3177
    iget-object v3, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    iget v3, v3, Lcom/jess/ui/TwoWayGridView;->H:I

    add-int/2addr v2, v3

    iget-object v3, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    iget v3, v3, Lcom/jess/ui/TwoWayGridView;->aa:I

    if-ge v2, v3, :cond_4

    .line 3180
    iget-object v2, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-static {v2}, Lcom/jess/ui/TwoWayGridView;->e(Lcom/jess/ui/TwoWayGridView;)I

    move-result v2

    add-int/2addr v1, v2

    .line 3183
    :cond_4
    if-gtz v1, :cond_1

    :cond_5
    move v0, v1

    goto :goto_0
.end method

.method private b(III)V
    .locals 6

    .prologue
    .line 2545
    iget-object v0, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    iget v0, v0, Lcom/jess/ui/TwoWayGridView;->H:I

    if-nez v0, :cond_3

    if-lez p3, :cond_3

    .line 2547
    iget-object v0, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/jess/ui/TwoWayGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 2550
    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v0

    .line 2553
    iget-object v1, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    iget-object v1, v1, Lcom/jess/ui/TwoWayGridView;->m:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    .line 2556
    iget-object v2, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-virtual {v2}, Lcom/jess/ui/TwoWayGridView;->getRight()I

    move-result v2

    iget-object v3, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-virtual {v3}, Lcom/jess/ui/TwoWayGridView;->getLeft()I

    move-result v3

    sub-int/2addr v2, v3

    iget-object v3, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    iget-object v3, v3, Lcom/jess/ui/TwoWayGridView;->m:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->right:I

    sub-int/2addr v2, v3

    .line 2560
    sub-int/2addr v0, v1

    .line 2561
    iget-object v1, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    add-int/lit8 v3, p3, -0x1

    invoke-virtual {v1, v3}, Lcom/jess/ui/TwoWayGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 2562
    invoke-virtual {v1}, Landroid/view/View;->getRight()I

    move-result v3

    .line 2563
    iget-object v4, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    iget v4, v4, Lcom/jess/ui/TwoWayGridView;->H:I

    add-int/2addr v4, p3

    add-int/lit8 v4, v4, -0x1

    .line 2567
    if-lez v0, :cond_3

    iget-object v5, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    iget v5, v5, Lcom/jess/ui/TwoWayGridView;->aa:I

    add-int/lit8 v5, v5, -0x1

    if-lt v4, v5, :cond_0

    if-le v3, v2, :cond_3

    .line 2568
    :cond_0
    iget-object v5, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    iget v5, v5, Lcom/jess/ui/TwoWayGridView;->aa:I

    add-int/lit8 v5, v5, -0x1

    if-ne v4, v5, :cond_1

    .line 2570
    sub-int v2, v3, v2

    invoke-static {v0, v2}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 2574
    :cond_1
    iget-object v2, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    neg-int v0, v0

    invoke-virtual {v2, v0}, Lcom/jess/ui/TwoWayGridView;->f(I)V

    .line 2575
    iget-object v0, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    iget v0, v0, Lcom/jess/ui/TwoWayGridView;->aa:I

    add-int/lit8 v0, v0, -0x1

    if-ge v4, v0, :cond_3

    .line 2578
    iget-object v0, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    iget-boolean v0, v0, Lcom/jess/ui/TwoWayGridView;->y:Z

    if-nez v0, :cond_2

    const/4 p1, 0x1

    :cond_2
    add-int v0, v4, p1

    .line 2579
    invoke-virtual {v1}, Landroid/view/View;->getRight()I

    move-result v1

    add-int/2addr v1, p2

    .line 2578
    invoke-direct {p0, v0, v1}, Lcom/jess/ui/af;->c(II)Landroid/view/View;

    .line 2581
    invoke-direct {p0}, Lcom/jess/ui/af;->b()V

    .line 2585
    :cond_3
    return-void
.end method

.method private b(Landroid/view/View;II)V
    .locals 2

    .prologue
    .line 3131
    invoke-virtual {p1}, Landroid/view/View;->getLeft()I

    move-result v0

    if-ge v0, p2, :cond_0

    .line 3134
    invoke-virtual {p1}, Landroid/view/View;->getLeft()I

    move-result v0

    sub-int v0, p2, v0

    .line 3138
    invoke-virtual {p1}, Landroid/view/View;->getRight()I

    move-result v1

    sub-int v1, p3, v1

    .line 3139
    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 3142
    iget-object v1, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-virtual {v1, v0}, Lcom/jess/ui/TwoWayGridView;->f(I)V

    .line 3144
    :cond_0
    return-void
.end method

.method private c(I)Landroid/view/View;
    .locals 4

    .prologue
    .line 2361
    iget-object v0, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    iget-object v1, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    iget v1, v1, Lcom/jess/ui/TwoWayGridView;->H:I

    iget-object v2, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    iget v2, v2, Lcom/jess/ui/TwoWayGridView;->V:I

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    iput v1, v0, Lcom/jess/ui/TwoWayGridView;->H:I

    .line 2362
    iget-object v0, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    iget-object v1, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    iget v1, v1, Lcom/jess/ui/TwoWayGridView;->H:I

    iget-object v2, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    iget v2, v2, Lcom/jess/ui/TwoWayGridView;->aa:I

    add-int/lit8 v2, v2, -0x1

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    iput v1, v0, Lcom/jess/ui/TwoWayGridView;->H:I

    .line 2363
    iget-object v0, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    iget v0, v0, Lcom/jess/ui/TwoWayGridView;->H:I

    if-gez v0, :cond_0

    .line 2364
    iget-object v0, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    const/4 v1, 0x0

    iput v1, v0, Lcom/jess/ui/TwoWayGridView;->H:I

    .line 2366
    :cond_0
    iget-object v0, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    iget v1, v0, Lcom/jess/ui/TwoWayGridView;->H:I

    iget-object v2, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    iget v2, v2, Lcom/jess/ui/TwoWayGridView;->H:I

    iget-object v3, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-static {v3}, Lcom/jess/ui/TwoWayGridView;->n(Lcom/jess/ui/TwoWayGridView;)I

    move-result v3

    rem-int/2addr v2, v3

    sub-int/2addr v1, v2

    iput v1, v0, Lcom/jess/ui/TwoWayGridView;->H:I

    .line 2367
    iget-object v0, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    iget v0, v0, Lcom/jess/ui/TwoWayGridView;->H:I

    invoke-direct {p0, v0, p1}, Lcom/jess/ui/af;->c(II)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method private c(II)Landroid/view/View;
    .locals 4

    .prologue
    .line 2239
    const/4 v1, 0x0

    .line 2241
    iget-object v0, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-virtual {v0}, Lcom/jess/ui/TwoWayGridView;->getRight()I

    move-result v0

    iget-object v2, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-virtual {v2}, Lcom/jess/ui/TwoWayGridView;->getLeft()I

    move-result v2

    sub-int/2addr v0, v2

    add-int/lit8 v2, v0, 0x0

    .line 2243
    :goto_0
    if-ge p2, v2, :cond_0

    iget-object v0, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    iget v0, v0, Lcom/jess/ui/TwoWayGridView;->aa:I

    if-ge p1, v0, :cond_0

    .line 2244
    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, v0}, Lcom/jess/ui/af;->a(IIZ)Landroid/view/View;

    move-result-object v0

    .line 2245
    if-eqz v0, :cond_1

    .line 2251
    :goto_1
    iget-object v1, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-static {v1}, Lcom/jess/ui/TwoWayGridView;->c(Lcom/jess/ui/TwoWayGridView;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getRight()I

    move-result v1

    iget-object v3, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-static {v3}, Lcom/jess/ui/TwoWayGridView;->e(Lcom/jess/ui/TwoWayGridView;)I

    move-result v3

    add-int p2, v1, v3

    .line 2253
    iget-object v1, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-static {v1}, Lcom/jess/ui/TwoWayGridView;->n(Lcom/jess/ui/TwoWayGridView;)I

    move-result v1

    add-int/2addr p1, v1

    move-object v1, v0

    .line 2254
    goto :goto_0

    .line 2256
    :cond_0
    return-object v1

    :cond_1
    move-object v0, v1

    goto :goto_1
.end method

.method private c(III)Landroid/view/View;
    .locals 10

    .prologue
    const/4 v9, 0x1

    const/4 v3, 0x0

    .line 2603
    iget-object v0, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-virtual {v0}, Lcom/jess/ui/TwoWayGridView;->getHorizontalFadingEdgeLength()I

    move-result v4

    .line 2604
    iget-object v0, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    iget v5, v0, Lcom/jess/ui/TwoWayGridView;->V:I

    .line 2605
    iget-object v0, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-static {v0}, Lcom/jess/ui/TwoWayGridView;->n(Lcom/jess/ui/TwoWayGridView;)I

    move-result v6

    .line 2606
    iget-object v0, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-static {v0}, Lcom/jess/ui/TwoWayGridView;->e(Lcom/jess/ui/TwoWayGridView;)I

    move-result v7

    .line 2610
    const/4 v0, -0x1

    .line 2612
    iget-object v1, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    iget-boolean v1, v1, Lcom/jess/ui/TwoWayGridView;->y:Z

    if-nez v1, :cond_0

    .line 2613
    sub-int v1, v5, p1

    sub-int v2, v5, p1

    rem-int/2addr v2, v6

    sub-int v2, v1, v2

    .line 2615
    rem-int v1, v5, v6

    sub-int v1, v5, v1

    .line 2627
    :goto_0
    sub-int v2, v1, v2

    .line 2629
    invoke-direct {p0, p2, v4, v1}, Lcom/jess/ui/af;->e(III)I

    move-result v5

    .line 2630
    invoke-direct {p0, p3, v4, v6, v1}, Lcom/jess/ui/af;->a(IIII)I

    move-result v4

    .line 2634
    iget-object v8, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    iput v1, v8, Lcom/jess/ui/TwoWayGridView;->H:I

    .line 2639
    if-lez v2, :cond_3

    .line 2644
    iget-object v2, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-static {v2}, Lcom/jess/ui/TwoWayGridView;->k(Lcom/jess/ui/TwoWayGridView;)Landroid/view/View;

    move-result-object v2

    if-nez v2, :cond_1

    move v2, v3

    .line 2647
    :goto_1
    iget-object v3, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    iget-boolean v3, v3, Lcom/jess/ui/TwoWayGridView;->y:Z

    if-eqz v3, :cond_2

    :goto_2
    add-int/2addr v2, v7

    invoke-direct {p0, v0, v2, v9}, Lcom/jess/ui/af;->a(IIZ)Landroid/view/View;

    move-result-object v2

    .line 2648
    iget-object v0, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-static {v0}, Lcom/jess/ui/TwoWayGridView;->c(Lcom/jess/ui/TwoWayGridView;)Landroid/view/View;

    move-result-object v0

    .line 2650
    invoke-direct {p0, v0, v5, v4}, Lcom/jess/ui/af;->a(Landroid/view/View;II)V

    .line 2673
    :goto_3
    iget-object v3, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    iget-boolean v3, v3, Lcom/jess/ui/TwoWayGridView;->y:Z

    if-nez v3, :cond_9

    .line 2674
    sub-int v3, v1, v6

    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v4

    sub-int/2addr v4, v7

    invoke-direct {p0, v3, v4}, Lcom/jess/ui/af;->d(II)Landroid/view/View;

    .line 2675
    invoke-direct {p0}, Lcom/jess/ui/af;->b()V

    .line 2676
    add-int/2addr v1, v6

    invoke-virtual {v0}, Landroid/view/View;->getRight()I

    move-result v0

    add-int/2addr v0, v7

    invoke-direct {p0, v1, v0}, Lcom/jess/ui/af;->c(II)Landroid/view/View;

    .line 2683
    :goto_4
    return-object v2

    .line 2617
    :cond_0
    iget-object v0, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    iget v0, v0, Lcom/jess/ui/TwoWayGridView;->aa:I

    add-int/lit8 v0, v0, -0x1

    sub-int/2addr v0, v5

    .line 2619
    iget-object v1, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    iget v1, v1, Lcom/jess/ui/TwoWayGridView;->aa:I

    add-int/lit8 v1, v1, -0x1

    rem-int v2, v0, v6

    sub-int/2addr v0, v2

    sub-int v0, v1, v0

    .line 2620
    sub-int v1, v0, v6

    add-int/lit8 v1, v1, 0x1

    invoke-static {v3, v1}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 2622
    iget-object v2, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    iget v2, v2, Lcom/jess/ui/TwoWayGridView;->aa:I

    add-int/lit8 v2, v2, -0x1

    sub-int/2addr v5, p1

    sub-int/2addr v2, v5

    .line 2623
    iget-object v5, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    iget v5, v5, Lcom/jess/ui/TwoWayGridView;->aa:I

    add-int/lit8 v5, v5, -0x1

    rem-int v8, v2, v6

    sub-int/2addr v2, v8

    sub-int v2, v5, v2

    .line 2624
    sub-int/2addr v2, v6

    add-int/lit8 v2, v2, 0x1

    invoke-static {v3, v2}, Ljava/lang/Math;->max(II)I

    move-result v2

    goto :goto_0

    .line 2644
    :cond_1
    iget-object v2, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    .line 2645
    invoke-static {v2}, Lcom/jess/ui/TwoWayGridView;->k(Lcom/jess/ui/TwoWayGridView;)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getRight()I

    move-result v2

    goto :goto_1

    :cond_2
    move v0, v1

    .line 2647
    goto :goto_2

    .line 2651
    :cond_3
    if-gez v2, :cond_6

    .line 2655
    iget-object v2, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-static {v2}, Lcom/jess/ui/TwoWayGridView;->k(Lcom/jess/ui/TwoWayGridView;)Landroid/view/View;

    move-result-object v2

    if-nez v2, :cond_4

    move v2, v3

    .line 2658
    :goto_5
    iget-object v8, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    iget-boolean v8, v8, Lcom/jess/ui/TwoWayGridView;->y:Z

    if-eqz v8, :cond_5

    :goto_6
    sub-int/2addr v2, v7

    invoke-direct {p0, v0, v2, v3}, Lcom/jess/ui/af;->a(IIZ)Landroid/view/View;

    move-result-object v2

    .line 2659
    iget-object v0, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-static {v0}, Lcom/jess/ui/TwoWayGridView;->c(Lcom/jess/ui/TwoWayGridView;)Landroid/view/View;

    move-result-object v0

    .line 2661
    invoke-direct {p0, v0, v5, v4}, Lcom/jess/ui/af;->b(Landroid/view/View;II)V

    goto :goto_3

    .line 2655
    :cond_4
    iget-object v2, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    .line 2656
    invoke-static {v2}, Lcom/jess/ui/TwoWayGridView;->k(Lcom/jess/ui/TwoWayGridView;)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getLeft()I

    move-result v2

    goto :goto_5

    :cond_5
    move v0, v1

    .line 2658
    goto :goto_6

    .line 2666
    :cond_6
    iget-object v2, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-static {v2}, Lcom/jess/ui/TwoWayGridView;->k(Lcom/jess/ui/TwoWayGridView;)Landroid/view/View;

    move-result-object v2

    if-nez v2, :cond_7

    .line 2669
    :goto_7
    iget-object v2, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    iget-boolean v2, v2, Lcom/jess/ui/TwoWayGridView;->y:Z

    if-eqz v2, :cond_8

    :goto_8
    invoke-direct {p0, v0, v3, v9}, Lcom/jess/ui/af;->a(IIZ)Landroid/view/View;

    move-result-object v2

    .line 2670
    iget-object v0, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-static {v0}, Lcom/jess/ui/TwoWayGridView;->c(Lcom/jess/ui/TwoWayGridView;)Landroid/view/View;

    move-result-object v0

    goto/16 :goto_3

    .line 2666
    :cond_7
    iget-object v2, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    .line 2667
    invoke-static {v2}, Lcom/jess/ui/TwoWayGridView;->k(Lcom/jess/ui/TwoWayGridView;)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getLeft()I

    move-result v3

    goto :goto_7

    :cond_8
    move v0, v1

    .line 2669
    goto :goto_8

    .line 2678
    :cond_9
    add-int v3, v1, v6

    invoke-virtual {v0}, Landroid/view/View;->getRight()I

    move-result v4

    add-int/2addr v4, v7

    invoke-direct {p0, v3, v4}, Lcom/jess/ui/af;->c(II)Landroid/view/View;

    .line 2679
    invoke-direct {p0}, Lcom/jess/ui/af;->b()V

    .line 2680
    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v0

    sub-int/2addr v0, v7

    invoke-direct {p0, v1, v0}, Lcom/jess/ui/af;->d(II)Landroid/view/View;

    goto/16 :goto_4
.end method

.method private d(II)Landroid/view/View;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2326
    const/4 v1, 0x0

    .line 2330
    :goto_0
    if-le p2, v3, :cond_0

    if-ltz p1, :cond_0

    .line 2332
    invoke-direct {p0, p1, p2, v3}, Lcom/jess/ui/af;->a(IIZ)Landroid/view/View;

    move-result-object v0

    .line 2333
    if-eqz v0, :cond_2

    .line 2337
    :goto_1
    iget-object v1, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-static {v1}, Lcom/jess/ui/TwoWayGridView;->c(Lcom/jess/ui/TwoWayGridView;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getLeft()I

    move-result v1

    iget-object v2, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-static {v2}, Lcom/jess/ui/TwoWayGridView;->e(Lcom/jess/ui/TwoWayGridView;)I

    move-result v2

    sub-int p2, v1, v2

    .line 2339
    iget-object v1, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    iput p1, v1, Lcom/jess/ui/TwoWayGridView;->H:I

    .line 2341
    iget-object v1, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-static {v1}, Lcom/jess/ui/TwoWayGridView;->n(Lcom/jess/ui/TwoWayGridView;)I

    move-result v1

    sub-int/2addr p1, v1

    move-object v1, v0

    .line 2342
    goto :goto_0

    .line 2344
    :cond_0
    iget-object v0, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    iget-boolean v0, v0, Lcom/jess/ui/TwoWayGridView;->y:Z

    if-eqz v0, :cond_1

    .line 2345
    iget-object v0, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    add-int/lit8 v2, p1, 0x1

    invoke-static {v3, v2}, Ljava/lang/Math;->max(II)I

    move-result v2

    iput v2, v0, Lcom/jess/ui/TwoWayGridView;->H:I

    .line 2348
    :cond_1
    return-object v1

    :cond_2
    move-object v0, v1

    goto :goto_1
.end method

.method private d(III)Landroid/view/View;
    .locals 8

    .prologue
    .line 3006
    iget-object v0, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-virtual {v0}, Lcom/jess/ui/TwoWayGridView;->getHorizontalFadingEdgeLength()I

    move-result v2

    .line 3007
    iget-object v0, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    iget v1, v0, Lcom/jess/ui/TwoWayGridView;->V:I

    .line 3008
    iget-object v0, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-static {v0}, Lcom/jess/ui/TwoWayGridView;->n(Lcom/jess/ui/TwoWayGridView;)I

    move-result v3

    .line 3009
    iget-object v0, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-static {v0}, Lcom/jess/ui/TwoWayGridView;->e(Lcom/jess/ui/TwoWayGridView;)I

    move-result v4

    .line 3012
    const/4 v0, -0x1

    .line 3014
    iget-object v5, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    iget-boolean v5, v5, Lcom/jess/ui/TwoWayGridView;->y:Z

    if-nez v5, :cond_0

    .line 3015
    rem-int v5, v1, v3

    sub-int/2addr v1, v5

    .line 3026
    :goto_0
    invoke-direct {p0, p2, v2, v1}, Lcom/jess/ui/af;->e(III)I

    move-result v5

    .line 3027
    invoke-direct {p0, p3, v2, v3, v1}, Lcom/jess/ui/af;->a(IIII)I

    move-result v6

    .line 3030
    iget-object v2, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    iget-boolean v2, v2, Lcom/jess/ui/TwoWayGridView;->y:Z

    if-eqz v2, :cond_1

    move v2, v0

    :goto_1
    const/4 v7, 0x1

    invoke-direct {p0, v2, p1, v7}, Lcom/jess/ui/af;->a(IIZ)Landroid/view/View;

    move-result-object v2

    .line 3032
    iget-object v7, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    iput v1, v7, Lcom/jess/ui/TwoWayGridView;->H:I

    .line 3034
    iget-object v7, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-static {v7}, Lcom/jess/ui/TwoWayGridView;->c(Lcom/jess/ui/TwoWayGridView;)Landroid/view/View;

    move-result-object v7

    .line 3035
    invoke-direct {p0, v7, v5, v6}, Lcom/jess/ui/af;->b(Landroid/view/View;II)V

    .line 3036
    invoke-direct {p0, v7, v5, v6}, Lcom/jess/ui/af;->a(Landroid/view/View;II)V

    .line 3038
    iget-object v5, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    iget-boolean v5, v5, Lcom/jess/ui/TwoWayGridView;->y:Z

    if-nez v5, :cond_2

    .line 3039
    sub-int v0, v1, v3

    invoke-virtual {v7}, Landroid/view/View;->getLeft()I

    move-result v5

    sub-int/2addr v5, v4

    invoke-direct {p0, v0, v5}, Lcom/jess/ui/af;->d(II)Landroid/view/View;

    .line 3040
    invoke-direct {p0}, Lcom/jess/ui/af;->b()V

    .line 3041
    add-int v0, v1, v3

    invoke-virtual {v7}, Landroid/view/View;->getRight()I

    move-result v1

    add-int/2addr v1, v4

    invoke-direct {p0, v0, v1}, Lcom/jess/ui/af;->c(II)Landroid/view/View;

    .line 3049
    :goto_2
    return-object v2

    .line 3017
    :cond_0
    iget-object v0, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    iget v0, v0, Lcom/jess/ui/TwoWayGridView;->aa:I

    add-int/lit8 v0, v0, -0x1

    sub-int/2addr v0, v1

    .line 3019
    iget-object v1, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    iget v1, v1, Lcom/jess/ui/TwoWayGridView;->aa:I

    add-int/lit8 v1, v1, -0x1

    rem-int v5, v0, v3

    sub-int/2addr v0, v5

    sub-int v0, v1, v0

    .line 3020
    const/4 v1, 0x0

    sub-int v5, v0, v3

    add-int/lit8 v5, v5, 0x1

    invoke-static {v1, v5}, Ljava/lang/Math;->max(II)I

    move-result v1

    goto :goto_0

    :cond_1
    move v2, v1

    .line 3030
    goto :goto_1

    .line 3043
    :cond_2
    add-int/2addr v0, v3

    invoke-virtual {v7}, Landroid/view/View;->getRight()I

    move-result v3

    add-int/2addr v3, v4

    invoke-direct {p0, v0, v3}, Lcom/jess/ui/af;->c(II)Landroid/view/View;

    .line 3044
    invoke-direct {p0}, Lcom/jess/ui/af;->b()V

    .line 3045
    add-int/lit8 v0, v1, -0x1

    invoke-virtual {v7}, Landroid/view/View;->getLeft()I

    move-result v1

    sub-int/2addr v1, v4

    invoke-direct {p0, v0, v1}, Lcom/jess/ui/af;->d(II)Landroid/view/View;

    goto :goto_2
.end method

.method private d(I)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 2922
    iget-object v0, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-static {v0}, Lcom/jess/ui/TwoWayGridView;->i(Lcom/jess/ui/TwoWayGridView;)I

    move-result v0

    .line 2923
    iget-object v1, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-static {v1}, Lcom/jess/ui/TwoWayGridView;->f(Lcom/jess/ui/TwoWayGridView;)I

    move-result v1

    .line 2924
    iget-object v2, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-static {v2}, Lcom/jess/ui/TwoWayGridView;->r(Lcom/jess/ui/TwoWayGridView;)I

    move-result v2

    .line 2925
    iget-object v3, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    iget-object v4, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-static {v4}, Lcom/jess/ui/TwoWayGridView;->g(Lcom/jess/ui/TwoWayGridView;)I

    move-result v4

    invoke-static {v3, v4}, Lcom/jess/ui/TwoWayGridView;->d(Lcom/jess/ui/TwoWayGridView;I)I

    .line 2927
    iget-object v3, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-static {v3}, Lcom/jess/ui/TwoWayGridView;->s(Lcom/jess/ui/TwoWayGridView;)I

    move-result v3

    const/4 v4, -0x1

    if-ne v3, v4, :cond_2

    .line 2928
    if-lez v2, :cond_1

    .line 2930
    iget-object v3, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    iget-object v4, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-static {v4}, Lcom/jess/ui/TwoWayGridView;->i(Lcom/jess/ui/TwoWayGridView;)I

    move-result v4

    add-int/2addr v4, p1

    iget-object v5, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    .line 2931
    invoke-static {v5}, Lcom/jess/ui/TwoWayGridView;->i(Lcom/jess/ui/TwoWayGridView;)I

    move-result v5

    add-int/2addr v5, v2

    div-int/2addr v4, v5

    .line 2930
    invoke-static {v3, v4}, Lcom/jess/ui/TwoWayGridView;->e(Lcom/jess/ui/TwoWayGridView;I)I

    .line 2941
    :goto_0
    iget-object v3, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-static {v3}, Lcom/jess/ui/TwoWayGridView;->n(Lcom/jess/ui/TwoWayGridView;)I

    move-result v3

    if-gtz v3, :cond_0

    .line 2942
    iget-object v3, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-static {v3, v6}, Lcom/jess/ui/TwoWayGridView;->e(Lcom/jess/ui/TwoWayGridView;I)I

    .line 2945
    :cond_0
    packed-switch v1, :pswitch_data_0

    .line 2954
    packed-switch v1, :pswitch_data_1

    .line 2992
    :goto_1
    return-void

    .line 2934
    :cond_1
    iget-object v3, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    const/4 v4, 0x2

    invoke-static {v3, v4}, Lcom/jess/ui/TwoWayGridView;->e(Lcom/jess/ui/TwoWayGridView;I)I

    goto :goto_0

    .line 2938
    :cond_2
    iget-object v3, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    iget-object v4, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-static {v4}, Lcom/jess/ui/TwoWayGridView;->s(Lcom/jess/ui/TwoWayGridView;)I

    move-result v4

    invoke-static {v3, v4}, Lcom/jess/ui/TwoWayGridView;->e(Lcom/jess/ui/TwoWayGridView;I)I

    goto :goto_0

    .line 2948
    :pswitch_0
    iget-object v0, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    iget-object v1, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-static {v1}, Lcom/jess/ui/TwoWayGridView;->r(Lcom/jess/ui/TwoWayGridView;)I

    move-result v1

    invoke-static {v0, v1}, Lcom/jess/ui/TwoWayGridView;->f(Lcom/jess/ui/TwoWayGridView;I)I

    .line 2949
    iget-object v0, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    iget-object v1, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-static {v1}, Lcom/jess/ui/TwoWayGridView;->i(Lcom/jess/ui/TwoWayGridView;)I

    move-result v1

    invoke-static {v0, v1}, Lcom/jess/ui/TwoWayGridView;->a(Lcom/jess/ui/TwoWayGridView;I)I

    goto :goto_1

    .line 2956
    :pswitch_1
    iget-object v1, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-static {v1}, Lcom/jess/ui/TwoWayGridView;->n(Lcom/jess/ui/TwoWayGridView;)I

    move-result v1

    mul-int/2addr v1, v2

    sub-int v1, p1, v1

    iget-object v3, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    .line 2957
    invoke-static {v3}, Lcom/jess/ui/TwoWayGridView;->n(Lcom/jess/ui/TwoWayGridView;)I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    mul-int/2addr v3, v0

    sub-int/2addr v1, v3

    .line 2959
    iget-object v3, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    iget-object v4, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-static {v4}, Lcom/jess/ui/TwoWayGridView;->n(Lcom/jess/ui/TwoWayGridView;)I

    move-result v4

    div-int/2addr v1, v4

    add-int/2addr v1, v2

    invoke-static {v3, v1}, Lcom/jess/ui/TwoWayGridView;->f(Lcom/jess/ui/TwoWayGridView;I)I

    .line 2960
    iget-object v1, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-static {v1, v0}, Lcom/jess/ui/TwoWayGridView;->a(Lcom/jess/ui/TwoWayGridView;I)I

    goto :goto_1

    .line 2964
    :pswitch_2
    iget-object v1, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-static {v1}, Lcom/jess/ui/TwoWayGridView;->n(Lcom/jess/ui/TwoWayGridView;)I

    move-result v1

    mul-int/2addr v1, v2

    sub-int v1, p1, v1

    iget-object v3, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    .line 2965
    invoke-static {v3}, Lcom/jess/ui/TwoWayGridView;->n(Lcom/jess/ui/TwoWayGridView;)I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    mul-int/2addr v3, v0

    sub-int/2addr v1, v3

    .line 2967
    iget-object v3, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-static {v3, v2}, Lcom/jess/ui/TwoWayGridView;->f(Lcom/jess/ui/TwoWayGridView;I)I

    .line 2968
    iget-object v2, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-static {v2}, Lcom/jess/ui/TwoWayGridView;->n(Lcom/jess/ui/TwoWayGridView;)I

    move-result v2

    if-le v2, v6, :cond_3

    .line 2969
    iget-object v2, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    iget-object v3, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    .line 2970
    invoke-static {v3}, Lcom/jess/ui/TwoWayGridView;->n(Lcom/jess/ui/TwoWayGridView;)I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    div-int/2addr v1, v3

    add-int/2addr v0, v1

    .line 2969
    invoke-static {v2, v0}, Lcom/jess/ui/TwoWayGridView;->a(Lcom/jess/ui/TwoWayGridView;I)I

    goto :goto_1

    .line 2972
    :cond_3
    iget-object v2, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    add-int/2addr v0, v1

    invoke-static {v2, v0}, Lcom/jess/ui/TwoWayGridView;->a(Lcom/jess/ui/TwoWayGridView;I)I

    goto/16 :goto_1

    .line 2978
    :pswitch_3
    iget-object v1, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-static {v1}, Lcom/jess/ui/TwoWayGridView;->n(Lcom/jess/ui/TwoWayGridView;)I

    move-result v1

    mul-int/2addr v1, v2

    sub-int v1, p1, v1

    iget-object v3, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    .line 2979
    invoke-static {v3}, Lcom/jess/ui/TwoWayGridView;->n(Lcom/jess/ui/TwoWayGridView;)I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    mul-int/2addr v3, v0

    sub-int/2addr v1, v3

    .line 2980
    iget-object v3, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-static {v3, v2}, Lcom/jess/ui/TwoWayGridView;->f(Lcom/jess/ui/TwoWayGridView;I)I

    .line 2981
    iget-object v2, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-static {v2}, Lcom/jess/ui/TwoWayGridView;->n(Lcom/jess/ui/TwoWayGridView;)I

    move-result v2

    if-le v2, v6, :cond_4

    .line 2982
    iget-object v2, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    iget-object v3, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-static {v3}, Lcom/jess/ui/TwoWayGridView;->n(Lcom/jess/ui/TwoWayGridView;)I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    div-int/2addr v1, v3

    add-int/2addr v0, v1

    invoke-static {v2, v0}, Lcom/jess/ui/TwoWayGridView;->a(Lcom/jess/ui/TwoWayGridView;I)I

    goto/16 :goto_1

    .line 2984
    :cond_4
    iget-object v2, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    mul-int/lit8 v0, v0, 0x2

    add-int/2addr v0, v1

    div-int/lit8 v0, v0, 0x2

    invoke-static {v2, v0}, Lcom/jess/ui/TwoWayGridView;->a(Lcom/jess/ui/TwoWayGridView;I)I

    goto/16 :goto_1

    .line 2945
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch

    .line 2954
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_3
    .end packed-switch
.end method

.method private e(III)I
    .locals 0

    .prologue
    .line 3081
    .line 3082
    if-lez p3, :cond_0

    .line 3083
    add-int/2addr p1, p2

    .line 3085
    :cond_0
    return p1
.end method

.method private e(II)Landroid/view/View;
    .locals 3

    .prologue
    .line 2372
    iget-object v0, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    iget v0, v0, Lcom/jess/ui/TwoWayGridView;->V:I

    invoke-static {p1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 2373
    iget-object v1, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    iget v1, v1, Lcom/jess/ui/TwoWayGridView;->aa:I

    add-int/lit8 v1, v1, -0x1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 2375
    iget-object v1, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    iget v1, v1, Lcom/jess/ui/TwoWayGridView;->aa:I

    add-int/lit8 v1, v1, -0x1

    sub-int v0, v1, v0

    .line 2376
    iget-object v1, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    iget v1, v1, Lcom/jess/ui/TwoWayGridView;->aa:I

    add-int/lit8 v1, v1, -0x1

    iget-object v2, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-static {v2}, Lcom/jess/ui/TwoWayGridView;->n(Lcom/jess/ui/TwoWayGridView;)I

    move-result v2

    rem-int v2, v0, v2

    sub-int/2addr v0, v2

    sub-int v0, v1, v0

    .line 2378
    invoke-direct {p0, v0, p2}, Lcom/jess/ui/af;->d(II)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method private e(I)V
    .locals 2

    .prologue
    .line 3297
    iget-object v0, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    iget v0, v0, Lcom/jess/ui/TwoWayGridView;->H:I

    if-nez v0, :cond_0

    .line 3298
    iget-object v0, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/jess/ui/TwoWayGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v0

    .line 3299
    sub-int v0, p1, v0

    .line 3300
    if-gez v0, :cond_0

    .line 3301
    iget-object v1, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-virtual {v1, v0}, Lcom/jess/ui/TwoWayGridView;->f(I)V

    .line 3304
    :cond_0
    return-void
.end method

.method private f(II)Landroid/view/View;
    .locals 8

    .prologue
    .line 2383
    iget-object v0, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-virtual {v0}, Lcom/jess/ui/TwoWayGridView;->j()I

    move-result v1

    .line 2384
    iget-object v0, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-static {v0}, Lcom/jess/ui/TwoWayGridView;->n(Lcom/jess/ui/TwoWayGridView;)I

    move-result v3

    .line 2385
    iget-object v0, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-static {v0}, Lcom/jess/ui/TwoWayGridView;->e(Lcom/jess/ui/TwoWayGridView;)I

    move-result v4

    .line 2388
    const/4 v0, -0x1

    .line 2390
    iget-object v2, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    iget-boolean v2, v2, Lcom/jess/ui/TwoWayGridView;->y:Z

    if-nez v2, :cond_0

    .line 2391
    rem-int v2, v1, v3

    sub-int/2addr v1, v2

    .line 2399
    :goto_0
    iget-object v2, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-virtual {v2}, Lcom/jess/ui/TwoWayGridView;->getHorizontalFadingEdgeLength()I

    move-result v5

    .line 2400
    invoke-direct {p0, p1, v5, v1}, Lcom/jess/ui/af;->e(III)I

    move-result v6

    .line 2402
    iget-object v2, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    iget-boolean v2, v2, Lcom/jess/ui/TwoWayGridView;->y:Z

    if-eqz v2, :cond_1

    move v2, v0

    :goto_1
    const/4 v7, 0x1

    invoke-direct {p0, v2, v6, v7}, Lcom/jess/ui/af;->a(IIZ)Landroid/view/View;

    move-result-object v2

    .line 2403
    iget-object v6, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    iput v1, v6, Lcom/jess/ui/TwoWayGridView;->H:I

    .line 2405
    iget-object v6, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-static {v6}, Lcom/jess/ui/TwoWayGridView;->c(Lcom/jess/ui/TwoWayGridView;)Landroid/view/View;

    move-result-object v6

    .line 2407
    iget-object v7, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    iget-boolean v7, v7, Lcom/jess/ui/TwoWayGridView;->y:Z

    if-nez v7, :cond_2

    .line 2408
    add-int v0, v1, v3

    invoke-virtual {v6}, Landroid/view/View;->getRight()I

    move-result v5

    add-int/2addr v5, v4

    invoke-direct {p0, v0, v5}, Lcom/jess/ui/af;->c(II)Landroid/view/View;

    .line 2409
    invoke-direct {p0, p2}, Lcom/jess/ui/af;->f(I)V

    .line 2410
    sub-int v0, v1, v3

    invoke-virtual {v6}, Landroid/view/View;->getLeft()I

    move-result v1

    sub-int/2addr v1, v4

    invoke-direct {p0, v0, v1}, Lcom/jess/ui/af;->d(II)Landroid/view/View;

    .line 2411
    invoke-direct {p0}, Lcom/jess/ui/af;->b()V

    .line 2423
    :goto_2
    return-object v2

    .line 2393
    :cond_0
    iget-object v0, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    iget v0, v0, Lcom/jess/ui/TwoWayGridView;->aa:I

    add-int/lit8 v0, v0, -0x1

    sub-int/2addr v0, v1

    .line 2395
    iget-object v1, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    iget v1, v1, Lcom/jess/ui/TwoWayGridView;->aa:I

    add-int/lit8 v1, v1, -0x1

    rem-int v2, v0, v3

    sub-int/2addr v0, v2

    sub-int v0, v1, v0

    .line 2396
    const/4 v1, 0x0

    sub-int v2, v0, v3

    add-int/lit8 v2, v2, 0x1

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    goto :goto_0

    :cond_1
    move v2, v1

    .line 2402
    goto :goto_1

    .line 2413
    :cond_2
    invoke-direct {p0, p2, v5, v3, v1}, Lcom/jess/ui/af;->a(IIII)I

    move-result v5

    .line 2415
    invoke-virtual {v6}, Landroid/view/View;->getRight()I

    move-result v7

    sub-int/2addr v5, v7

    .line 2416
    iget-object v7, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-virtual {v7, v5}, Lcom/jess/ui/TwoWayGridView;->f(I)V

    .line 2417
    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v6}, Landroid/view/View;->getLeft()I

    move-result v5

    sub-int/2addr v5, v4

    invoke-direct {p0, v1, v5}, Lcom/jess/ui/af;->d(II)Landroid/view/View;

    .line 2418
    invoke-direct {p0, p1}, Lcom/jess/ui/af;->e(I)V

    .line 2419
    add-int/2addr v0, v3

    invoke-virtual {v6}, Landroid/view/View;->getRight()I

    move-result v1

    add-int/2addr v1, v4

    invoke-direct {p0, v0, v1}, Lcom/jess/ui/af;->c(II)Landroid/view/View;

    .line 2420
    invoke-direct {p0}, Lcom/jess/ui/af;->b()V

    goto :goto_2
.end method

.method private f(I)V
    .locals 3

    .prologue
    .line 3307
    iget-object v0, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-virtual {v0}, Lcom/jess/ui/TwoWayGridView;->getChildCount()I

    move-result v0

    .line 3308
    iget-object v1, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    iget v1, v1, Lcom/jess/ui/TwoWayGridView;->H:I

    add-int/2addr v1, v0

    iget-object v2, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    iget v2, v2, Lcom/jess/ui/TwoWayGridView;->aa:I

    if-ne v1, v2, :cond_0

    .line 3309
    iget-object v1, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {v1, v0}, Lcom/jess/ui/TwoWayGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getRight()I

    move-result v0

    .line 3310
    sub-int v0, p1, v0

    .line 3311
    if-lez v0, :cond_0

    .line 3312
    iget-object v1, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-virtual {v1, v0}, Lcom/jess/ui/TwoWayGridView;->f(I)V

    .line 3315
    :cond_0
    return-void
.end method

.method private g(II)Landroid/view/View;
    .locals 8

    .prologue
    .line 2439
    iget-object v0, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-static {v0}, Lcom/jess/ui/TwoWayGridView;->n(Lcom/jess/ui/TwoWayGridView;)I

    move-result v3

    .line 2442
    const/4 v0, -0x1

    .line 2444
    iget-object v1, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    iget-boolean v1, v1, Lcom/jess/ui/TwoWayGridView;->y:Z

    if-nez v1, :cond_1

    .line 2446
    rem-int v1, p1, v3

    sub-int v1, p1, v1

    .line 2455
    :goto_0
    iget-object v2, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    iget-boolean v2, v2, Lcom/jess/ui/TwoWayGridView;->y:Z

    if-eqz v2, :cond_2

    move v2, v0

    :goto_1
    const/4 v4, 0x1

    invoke-direct {p0, v2, p2, v4}, Lcom/jess/ui/af;->a(IIZ)Landroid/view/View;

    move-result-object v2

    .line 2458
    iget-object v4, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    iput v1, v4, Lcom/jess/ui/TwoWayGridView;->H:I

    .line 2460
    iget-object v4, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-static {v4}, Lcom/jess/ui/TwoWayGridView;->c(Lcom/jess/ui/TwoWayGridView;)Landroid/view/View;

    move-result-object v4

    .line 2462
    if-nez v4, :cond_3

    .line 2463
    const/4 v0, 0x0

    .line 2496
    :cond_0
    :goto_2
    return-object v0

    .line 2449
    :cond_1
    iget-object v0, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    iget v0, v0, Lcom/jess/ui/TwoWayGridView;->aa:I

    add-int/lit8 v0, v0, -0x1

    sub-int/2addr v0, p1

    .line 2451
    iget-object v1, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    iget v1, v1, Lcom/jess/ui/TwoWayGridView;->aa:I

    add-int/lit8 v1, v1, -0x1

    rem-int v2, v0, v3

    sub-int/2addr v0, v2

    sub-int v0, v1, v0

    .line 2452
    const/4 v1, 0x0

    sub-int v2, v0, v3

    add-int/lit8 v2, v2, 0x1

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    goto :goto_0

    :cond_2
    move v2, v1

    .line 2455
    goto :goto_1

    .line 2466
    :cond_3
    iget-object v5, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-static {v5}, Lcom/jess/ui/TwoWayGridView;->e(Lcom/jess/ui/TwoWayGridView;)I

    move-result v5

    .line 2471
    iget-object v6, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    iget-boolean v6, v6, Lcom/jess/ui/TwoWayGridView;->y:Z

    if-nez v6, :cond_5

    .line 2472
    sub-int v0, v1, v3

    invoke-virtual {v4}, Landroid/view/View;->getLeft()I

    move-result v6

    sub-int/2addr v6, v5

    invoke-direct {p0, v0, v6}, Lcom/jess/ui/af;->d(II)Landroid/view/View;

    move-result-object v0

    .line 2473
    invoke-direct {p0}, Lcom/jess/ui/af;->b()V

    .line 2474
    add-int/2addr v1, v3

    invoke-virtual {v4}, Landroid/view/View;->getRight()I

    move-result v4

    add-int/2addr v4, v5

    invoke-direct {p0, v1, v4}, Lcom/jess/ui/af;->c(II)Landroid/view/View;

    move-result-object v1

    .line 2476
    iget-object v4, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-virtual {v4}, Lcom/jess/ui/TwoWayGridView;->getChildCount()I

    move-result v4

    .line 2477
    if-lez v4, :cond_4

    .line 2478
    invoke-direct {p0, v3, v5, v4}, Lcom/jess/ui/af;->a(III)V

    .line 2491
    :cond_4
    :goto_3
    if-eqz v2, :cond_7

    move-object v0, v2

    .line 2492
    goto :goto_2

    .line 2481
    :cond_5
    add-int/2addr v0, v3

    invoke-virtual {v4}, Landroid/view/View;->getRight()I

    move-result v6

    add-int/2addr v6, v5

    invoke-direct {p0, v0, v6}, Lcom/jess/ui/af;->c(II)Landroid/view/View;

    move-result-object v0

    .line 2482
    invoke-direct {p0}, Lcom/jess/ui/af;->b()V

    .line 2483
    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v4}, Landroid/view/View;->getLeft()I

    move-result v4

    sub-int/2addr v4, v5

    invoke-direct {p0, v1, v4}, Lcom/jess/ui/af;->d(II)Landroid/view/View;

    move-result-object v1

    .line 2485
    iget-object v4, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-virtual {v4}, Lcom/jess/ui/TwoWayGridView;->getChildCount()I

    move-result v4

    .line 2486
    if-lez v4, :cond_6

    .line 2487
    invoke-direct {p0, v3, v5, v4}, Lcom/jess/ui/af;->b(III)V

    :cond_6
    move-object v7, v0

    move-object v0, v1

    move-object v1, v7

    goto :goto_3

    .line 2493
    :cond_7
    if-nez v0, :cond_0

    move-object v0, v1

    .line 2496
    goto :goto_2
.end method


# virtual methods
.method protected a(IIZIZI)Landroid/view/View;
    .locals 9

    .prologue
    .line 2173
    iget-object v0, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    iget-boolean v0, v0, Lcom/jess/ui/TwoWayGridView;->S:Z

    if-nez v0, :cond_0

    .line 2175
    iget-object v0, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    iget-object v0, v0, Lcom/jess/ui/TwoWayGridView;->g:Lcom/jess/ui/k;

    invoke-virtual {v0, p1}, Lcom/jess/ui/k;->c(I)Landroid/view/View;

    move-result-object v1

    .line 2176
    if-eqz v1, :cond_0

    .line 2179
    const/4 v7, 0x1

    move-object v0, p0

    move v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    move v6, p5

    move v8, p6

    invoke-direct/range {v0 .. v8}, Lcom/jess/ui/af;->a(Landroid/view/View;IIZIZZI)V

    .line 2192
    :goto_0
    return-object v1

    .line 2187
    :cond_0
    iget-object v0, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    iget-object v1, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    iget-object v1, v1, Lcom/jess/ui/TwoWayGridView;->E:[Z

    invoke-virtual {v0, p1, v1}, Lcom/jess/ui/TwoWayGridView;->a(I[Z)Landroid/view/View;

    move-result-object v1

    .line 2190
    iget-object v0, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    iget-object v0, v0, Lcom/jess/ui/TwoWayGridView;->E:[Z

    const/4 v2, 0x0

    aget-boolean v7, v0, v2

    move-object v0, p0

    move v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    move v6, p5

    move v8, p6

    invoke-direct/range {v0 .. v8}, Lcom/jess/ui/af;->a(Landroid/view/View;IIZIZZI)V

    goto :goto_0
.end method

.method protected a()V
    .locals 13

    .prologue
    const/4 v5, -0x1

    const/4 v1, 0x0

    const/4 v4, 0x0

    .line 2689
    iget-object v0, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    iget-object v0, v0, Lcom/jess/ui/TwoWayGridView;->m:Landroid/graphics/Rect;

    iget v6, v0, Landroid/graphics/Rect;->left:I

    .line 2690
    iget-object v0, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-virtual {v0}, Lcom/jess/ui/TwoWayGridView;->getRight()I

    move-result v0

    iget-object v2, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-virtual {v2}, Lcom/jess/ui/TwoWayGridView;->getLeft()I

    move-result v2

    sub-int/2addr v0, v2

    iget-object v2, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    iget-object v2, v2, Lcom/jess/ui/TwoWayGridView;->m:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->right:I

    sub-int v8, v0, v2

    .line 2692
    iget-object v0, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-virtual {v0}, Lcom/jess/ui/TwoWayGridView;->getChildCount()I

    move-result v9

    .line 2702
    iget-object v0, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    iget v0, v0, Lcom/jess/ui/TwoWayGridView;->a:I

    packed-switch v0, :pswitch_data_0

    .line 2721
    iget-object v0, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    iget v0, v0, Lcom/jess/ui/TwoWayGridView;->V:I

    iget-object v2, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    iget v2, v2, Lcom/jess/ui/TwoWayGridView;->H:I

    sub-int/2addr v0, v2

    .line 2722
    if-ltz v0, :cond_13

    if-ge v0, v9, :cond_13

    .line 2723
    iget-object v2, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-virtual {v2, v0}, Lcom/jess/ui/TwoWayGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 2727
    :goto_0
    iget-object v2, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-virtual {v2, v4}, Lcom/jess/ui/TwoWayGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    move v3, v4

    move-object v12, v0

    move-object v0, v2

    move-object v2, v12

    .line 2730
    :goto_1
    iget-object v7, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    iget-boolean v7, v7, Lcom/jess/ui/TwoWayGridView;->S:Z

    .line 2731
    if-eqz v7, :cond_0

    .line 2732
    iget-object v10, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-virtual {v10}, Lcom/jess/ui/TwoWayGridView;->l()V

    .line 2737
    :cond_0
    iget-object v10, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    iget v10, v10, Lcom/jess/ui/TwoWayGridView;->aa:I

    if-nez v10, :cond_1

    .line 2738
    iget-object v0, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-virtual {v0}, Lcom/jess/ui/TwoWayGridView;->c()V

    .line 2739
    iget-object v0, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-virtual {v0}, Lcom/jess/ui/TwoWayGridView;->a()V

    .line 2841
    :goto_2
    return-void

    .line 2704
    :pswitch_0
    iget-object v0, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    iget v0, v0, Lcom/jess/ui/TwoWayGridView;->T:I

    iget-object v2, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    iget v2, v2, Lcom/jess/ui/TwoWayGridView;->H:I

    sub-int/2addr v0, v2

    .line 2705
    if-ltz v0, :cond_14

    if-ge v0, v9, :cond_14

    .line 2706
    iget-object v2, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-virtual {v2, v0}, Lcom/jess/ui/TwoWayGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    move-object v2, v1

    move v3, v4

    move-object v12, v0

    move-object v0, v1

    move-object v1, v12

    goto :goto_1

    :pswitch_1
    move-object v0, v1

    move-object v2, v1

    move v3, v4

    .line 2713
    goto :goto_1

    .line 2715
    :pswitch_2
    iget-object v0, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    iget v0, v0, Lcom/jess/ui/TwoWayGridView;->T:I

    if-ltz v0, :cond_14

    .line 2716
    iget-object v0, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    iget v0, v0, Lcom/jess/ui/TwoWayGridView;->T:I

    iget-object v2, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    iget v2, v2, Lcom/jess/ui/TwoWayGridView;->V:I

    sub-int/2addr v0, v2

    move-object v2, v1

    move v3, v0

    move-object v0, v1

    goto :goto_1

    .line 2743
    :cond_1
    iget-object v10, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    iget-object v11, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    iget v11, v11, Lcom/jess/ui/TwoWayGridView;->T:I

    invoke-virtual {v10, v11}, Lcom/jess/ui/TwoWayGridView;->setSelectedPositionInt(I)V

    .line 2747
    iget-object v10, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    iget v10, v10, Lcom/jess/ui/TwoWayGridView;->H:I

    .line 2748
    iget-object v11, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    iget-object v11, v11, Lcom/jess/ui/TwoWayGridView;->g:Lcom/jess/ui/k;

    .line 2750
    if-eqz v7, :cond_2

    move v7, v4

    .line 2751
    :goto_3
    if-ge v7, v9, :cond_3

    .line 2752
    iget-object v10, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-virtual {v10, v7}, Lcom/jess/ui/TwoWayGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v10

    invoke-virtual {v11, v10}, Lcom/jess/ui/k;->a(Landroid/view/View;)V

    .line 2751
    add-int/lit8 v7, v7, 0x1

    goto :goto_3

    .line 2755
    :cond_2
    invoke-virtual {v11, v9, v10}, Lcom/jess/ui/k;->a(II)V

    .line 2760
    :cond_3
    iget-object v7, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-static {v7}, Lcom/jess/ui/TwoWayGridView;->p(Lcom/jess/ui/TwoWayGridView;)V

    .line 2762
    iget-object v7, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    iget v7, v7, Lcom/jess/ui/TwoWayGridView;->a:I

    packed-switch v7, :pswitch_data_1

    .line 2790
    if-nez v9, :cond_c

    .line 2791
    iget-object v0, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    iget-boolean v0, v0, Lcom/jess/ui/TwoWayGridView;->y:Z

    if-nez v0, :cond_9

    .line 2792
    iget-object v1, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    iget-object v0, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    iget-object v0, v0, Lcom/jess/ui/TwoWayGridView;->c:Landroid/widget/ListAdapter;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-virtual {v0}, Lcom/jess/ui/TwoWayGridView;->isInTouchMode()Z

    move-result v0

    if-eqz v0, :cond_8

    :cond_4
    move v0, v5

    :goto_4
    invoke-virtual {v1, v0}, Lcom/jess/ui/TwoWayGridView;->setSelectedPositionInt(I)V

    .line 2794
    invoke-direct {p0, v6}, Lcom/jess/ui/af;->c(I)Landroid/view/View;

    move-result-object v0

    .line 2816
    :goto_5
    invoke-virtual {v11}, Lcom/jess/ui/k;->c()V

    .line 2818
    if-eqz v0, :cond_11

    .line 2819
    iget-object v1, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-virtual {v1, v0}, Lcom/jess/ui/TwoWayGridView;->a(Landroid/view/View;)V

    .line 2820
    iget-object v1, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v0

    iput v0, v1, Lcom/jess/ui/TwoWayGridView;->x:I

    .line 2829
    :cond_5
    :goto_6
    iget-object v0, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    iput v4, v0, Lcom/jess/ui/TwoWayGridView;->a:I

    .line 2830
    iget-object v0, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    iput-boolean v4, v0, Lcom/jess/ui/TwoWayGridView;->S:Z

    .line 2831
    iget-object v0, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    iput-boolean v4, v0, Lcom/jess/ui/TwoWayGridView;->M:Z

    .line 2832
    iget-object v0, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    iget-object v1, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    iget v1, v1, Lcom/jess/ui/TwoWayGridView;->V:I

    invoke-virtual {v0, v1}, Lcom/jess/ui/TwoWayGridView;->setNextSelectedPositionInt(I)V

    .line 2834
    iget-object v0, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-virtual {v0}, Lcom/jess/ui/TwoWayGridView;->e()V

    .line 2836
    iget-object v0, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    iget v0, v0, Lcom/jess/ui/TwoWayGridView;->aa:I

    if-lez v0, :cond_6

    .line 2837
    iget-object v0, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-virtual {v0}, Lcom/jess/ui/TwoWayGridView;->p()V

    .line 2840
    :cond_6
    iget-object v0, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-virtual {v0}, Lcom/jess/ui/TwoWayGridView;->a()V

    goto/16 :goto_2

    .line 2764
    :pswitch_3
    if-eqz v1, :cond_7

    .line 2765
    invoke-virtual {v1}, Landroid/view/View;->getLeft()I

    move-result v0

    invoke-direct {p0, v0, v6, v8}, Lcom/jess/ui/af;->d(III)Landroid/view/View;

    move-result-object v0

    goto :goto_5

    .line 2767
    :cond_7
    invoke-direct {p0, v6, v8}, Lcom/jess/ui/af;->f(II)Landroid/view/View;

    move-result-object v0

    goto :goto_5

    .line 2771
    :pswitch_4
    iget-object v0, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    iput v4, v0, Lcom/jess/ui/TwoWayGridView;->H:I

    .line 2772
    invoke-direct {p0, v6}, Lcom/jess/ui/af;->c(I)Landroid/view/View;

    move-result-object v0

    .line 2773
    invoke-direct {p0}, Lcom/jess/ui/af;->b()V

    goto :goto_5

    .line 2776
    :pswitch_5
    iget-object v0, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    iget v0, v0, Lcom/jess/ui/TwoWayGridView;->aa:I

    add-int/lit8 v0, v0, -0x1

    invoke-direct {p0, v0, v8}, Lcom/jess/ui/af;->c(II)Landroid/view/View;

    move-result-object v0

    .line 2777
    invoke-direct {p0}, Lcom/jess/ui/af;->b()V

    goto :goto_5

    .line 2780
    :pswitch_6
    iget-object v0, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    iget v0, v0, Lcom/jess/ui/TwoWayGridView;->V:I

    iget-object v1, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    iget v1, v1, Lcom/jess/ui/TwoWayGridView;->I:I

    invoke-direct {p0, v0, v1}, Lcom/jess/ui/af;->g(II)Landroid/view/View;

    move-result-object v0

    goto :goto_5

    .line 2783
    :pswitch_7
    iget-object v0, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    iget v0, v0, Lcom/jess/ui/TwoWayGridView;->J:I

    iget-object v1, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    iget v1, v1, Lcom/jess/ui/TwoWayGridView;->I:I

    invoke-direct {p0, v0, v1}, Lcom/jess/ui/af;->g(II)Landroid/view/View;

    move-result-object v0

    goto/16 :goto_5

    .line 2787
    :pswitch_8
    invoke-direct {p0, v3, v6, v8}, Lcom/jess/ui/af;->c(III)Landroid/view/View;

    move-result-object v0

    goto/16 :goto_5

    :cond_8
    move v0, v4

    .line 2792
    goto/16 :goto_4

    .line 2796
    :cond_9
    iget-object v0, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    iget v0, v0, Lcom/jess/ui/TwoWayGridView;->aa:I

    add-int/lit8 v0, v0, -0x1

    .line 2797
    iget-object v1, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    iget-object v2, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    iget-object v2, v2, Lcom/jess/ui/TwoWayGridView;->c:Landroid/widget/ListAdapter;

    if-eqz v2, :cond_a

    iget-object v2, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-virtual {v2}, Lcom/jess/ui/TwoWayGridView;->isInTouchMode()Z

    move-result v2

    if-eqz v2, :cond_b

    :cond_a
    :goto_7
    invoke-virtual {v1, v5}, Lcom/jess/ui/TwoWayGridView;->setSelectedPositionInt(I)V

    .line 2799
    invoke-direct {p0, v0, v8}, Lcom/jess/ui/af;->e(II)Landroid/view/View;

    move-result-object v0

    goto/16 :goto_5

    :cond_b
    move v5, v0

    .line 2797
    goto :goto_7

    .line 2802
    :cond_c
    iget-object v1, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    iget v1, v1, Lcom/jess/ui/TwoWayGridView;->V:I

    if-ltz v1, :cond_e

    iget-object v1, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    iget v1, v1, Lcom/jess/ui/TwoWayGridView;->V:I

    iget-object v3, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    iget v3, v3, Lcom/jess/ui/TwoWayGridView;->aa:I

    if-ge v1, v3, :cond_e

    .line 2803
    iget-object v0, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    iget v1, v0, Lcom/jess/ui/TwoWayGridView;->V:I

    if-nez v2, :cond_d

    move v0, v6

    :goto_8
    invoke-direct {p0, v1, v0}, Lcom/jess/ui/af;->g(II)Landroid/view/View;

    move-result-object v0

    goto/16 :goto_5

    .line 2804
    :cond_d
    invoke-virtual {v2}, Landroid/view/View;->getLeft()I

    move-result v0

    goto :goto_8

    .line 2805
    :cond_e
    iget-object v1, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    iget v1, v1, Lcom/jess/ui/TwoWayGridView;->H:I

    iget-object v2, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    iget v2, v2, Lcom/jess/ui/TwoWayGridView;->aa:I

    if-ge v1, v2, :cond_10

    .line 2806
    iget-object v1, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    iget v1, v1, Lcom/jess/ui/TwoWayGridView;->H:I

    if-nez v0, :cond_f

    :goto_9
    invoke-direct {p0, v1, v6}, Lcom/jess/ui/af;->g(II)Landroid/view/View;

    move-result-object v0

    goto/16 :goto_5

    .line 2807
    :cond_f
    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v6

    goto :goto_9

    .line 2809
    :cond_10
    invoke-direct {p0, v4, v6}, Lcom/jess/ui/af;->g(II)Landroid/view/View;

    move-result-object v0

    goto/16 :goto_5

    .line 2821
    :cond_11
    iget-object v0, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    iget v0, v0, Lcom/jess/ui/TwoWayGridView;->w:I

    if-lez v0, :cond_12

    iget-object v0, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    iget v0, v0, Lcom/jess/ui/TwoWayGridView;->w:I

    const/4 v1, 0x3

    if-ge v0, v1, :cond_12

    .line 2822
    iget-object v0, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    iget-object v1, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    iget v1, v1, Lcom/jess/ui/TwoWayGridView;->t:I

    iget-object v2, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    iget v2, v2, Lcom/jess/ui/TwoWayGridView;->H:I

    sub-int/2addr v1, v2

    invoke-virtual {v0, v1}, Lcom/jess/ui/TwoWayGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 2823
    if-eqz v0, :cond_5

    iget-object v1, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-virtual {v1, v0}, Lcom/jess/ui/TwoWayGridView;->a(Landroid/view/View;)V

    goto/16 :goto_6

    .line 2825
    :cond_12
    iget-object v0, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    iput v4, v0, Lcom/jess/ui/TwoWayGridView;->x:I

    .line 2826
    iget-object v0, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    iget-object v0, v0, Lcom/jess/ui/TwoWayGridView;->f:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->setEmpty()V

    goto/16 :goto_6

    :cond_13
    move-object v0, v1

    goto/16 :goto_0

    :cond_14
    move-object v0, v1

    move-object v2, v1

    move v3, v4

    goto/16 :goto_1

    .line 2702
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_2
    .end packed-switch

    .line 2762
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_4
        :pswitch_3
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method

.method protected a(I)V
    .locals 3

    .prologue
    .line 3324
    iget-object v0, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    iget v1, v0, Lcom/jess/ui/TwoWayGridView;->T:I

    .line 3326
    iget-object v0, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-virtual {v0, p1}, Lcom/jess/ui/TwoWayGridView;->setNextSelectedPositionInt(I)V

    .line 3327
    iget-object v0, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-virtual {v0}, Lcom/jess/ui/TwoWayGridView;->d()V

    .line 3329
    iget-object v0, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    iget-boolean v0, v0, Lcom/jess/ui/TwoWayGridView;->y:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    iget v0, v0, Lcom/jess/ui/TwoWayGridView;->aa:I

    add-int/lit8 v0, v0, -0x1

    iget-object v2, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    iget v2, v2, Lcom/jess/ui/TwoWayGridView;->T:I

    sub-int/2addr v0, v2

    .line 3331
    :goto_0
    iget-object v2, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    iget-boolean v2, v2, Lcom/jess/ui/TwoWayGridView;->y:Z

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    iget v2, v2, Lcom/jess/ui/TwoWayGridView;->aa:I

    add-int/lit8 v2, v2, -0x1

    sub-int v1, v2, v1

    .line 3334
    :cond_0
    iget-object v2, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-static {v2}, Lcom/jess/ui/TwoWayGridView;->n(Lcom/jess/ui/TwoWayGridView;)I

    move-result v2

    div-int/2addr v0, v2

    .line 3335
    iget-object v2, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-static {v2}, Lcom/jess/ui/TwoWayGridView;->n(Lcom/jess/ui/TwoWayGridView;)I

    move-result v2

    div-int/2addr v1, v2

    .line 3337
    if-eq v0, v1, :cond_1

    .line 3341
    :cond_1
    return-void

    .line 3329
    :cond_2
    iget-object v0, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    iget v0, v0, Lcom/jess/ui/TwoWayGridView;->T:I

    goto :goto_0
.end method

.method protected a(II)V
    .locals 11

    .prologue
    const/4 v2, 0x0

    .line 2847
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v5

    .line 2848
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v1

    .line 2849
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v3

    .line 2850
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 2852
    if-nez v1, :cond_a

    .line 2853
    iget-object v0, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-static {v0}, Lcom/jess/ui/TwoWayGridView;->o(Lcom/jess/ui/TwoWayGridView;)I

    move-result v0

    if-lez v0, :cond_4

    .line 2854
    iget-object v0, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-static {v0}, Lcom/jess/ui/TwoWayGridView;->o(Lcom/jess/ui/TwoWayGridView;)I

    move-result v0

    iget-object v1, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    iget-object v1, v1, Lcom/jess/ui/TwoWayGridView;->m:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->top:I

    add-int/2addr v0, v1

    iget-object v1, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    iget-object v1, v1, Lcom/jess/ui/TwoWayGridView;->m:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v0, v1

    .line 2858
    :goto_0
    iget-object v1, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-static {v1}, Lcom/jess/ui/TwoWayGridView;->q(Lcom/jess/ui/TwoWayGridView;)I

    move-result v1

    add-int/2addr v0, v1

    move v1, v0

    .line 2861
    :goto_1
    iget-object v0, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    iget-object v0, v0, Lcom/jess/ui/TwoWayGridView;->m:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->top:I

    sub-int v0, v1, v0

    iget-object v4, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    iget-object v4, v4, Lcom/jess/ui/TwoWayGridView;->m:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->bottom:I

    sub-int/2addr v0, v4

    .line 2862
    invoke-direct {p0, v0}, Lcom/jess/ui/af;->d(I)V

    .line 2866
    iget-object v4, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    iget-object v0, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    iget-object v0, v0, Lcom/jess/ui/TwoWayGridView;->c:Landroid/widget/ListAdapter;

    if-nez v0, :cond_5

    move v0, v2

    :goto_2
    iput v0, v4, Lcom/jess/ui/TwoWayGridView;->aa:I

    .line 2867
    iget-object v0, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    iget v6, v0, Lcom/jess/ui/TwoWayGridView;->aa:I

    .line 2868
    if-lez v6, :cond_9

    .line 2869
    iget-object v0, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    iget-object v4, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    iget-object v4, v4, Lcom/jess/ui/TwoWayGridView;->E:[Z

    invoke-virtual {v0, v2, v4}, Lcom/jess/ui/TwoWayGridView;->a(I[Z)Landroid/view/View;

    move-result-object v7

    .line 2871
    invoke-virtual {v7}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/jess/ui/h;

    .line 2872
    if-nez v0, :cond_0

    .line 2873
    new-instance v0, Lcom/jess/ui/h;

    const/4 v4, -0x2

    const/4 v8, -0x1

    invoke-direct {v0, v4, v8, v2}, Lcom/jess/ui/h;-><init>(III)V

    .line 2875
    invoke-virtual {v7, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2877
    :cond_0
    iget-object v4, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    iget-object v4, v4, Lcom/jess/ui/TwoWayGridView;->c:Landroid/widget/ListAdapter;

    invoke-interface {v4, v2}, Landroid/widget/ListAdapter;->getItemViewType(I)I

    move-result v4

    iput v4, v0, Lcom/jess/ui/h;->a:I

    .line 2878
    const/4 v4, 0x1

    iput-boolean v4, v0, Lcom/jess/ui/h;->b:Z

    .line 2880
    iget-object v4, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    .line 2881
    invoke-static {v4}, Lcom/jess/ui/TwoWayGridView;->o(Lcom/jess/ui/TwoWayGridView;)I

    move-result v4

    invoke-static {v4, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    iget v8, v0, Lcom/jess/ui/h;->height:I

    .line 2880
    invoke-static {v4, v2, v8}, Landroid/view/ViewGroup;->getChildMeasureSpec(III)I

    move-result v4

    .line 2882
    const/high16 v8, 0x40000000    # 2.0f

    .line 2883
    invoke-static {v2, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v8

    iget v9, v0, Lcom/jess/ui/h;->width:I

    .line 2882
    invoke-static {v8, v2, v9}, Landroid/view/ViewGroup;->getChildMeasureSpec(III)I

    move-result v8

    .line 2884
    invoke-virtual {v7, v8, v4}, Landroid/view/View;->measure(II)V

    .line 2886
    invoke-virtual {v7}, Landroid/view/View;->getMeasuredWidth()I

    move-result v4

    .line 2888
    iget-object v8, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    iget-object v8, v8, Lcom/jess/ui/TwoWayGridView;->g:Lcom/jess/ui/k;

    iget v0, v0, Lcom/jess/ui/h;->a:I

    invoke-virtual {v8, v0}, Lcom/jess/ui/k;->b(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2889
    iget-object v0, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    iget-object v0, v0, Lcom/jess/ui/TwoWayGridView;->g:Lcom/jess/ui/k;

    invoke-virtual {v0, v7}, Lcom/jess/ui/k;->a(Landroid/view/View;)V

    .line 2893
    :cond_1
    :goto_3
    if-nez v5, :cond_8

    .line 2894
    iget-object v0, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    iget-object v0, v0, Lcom/jess/ui/TwoWayGridView;->m:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    iget-object v3, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    iget-object v3, v3, Lcom/jess/ui/TwoWayGridView;->m:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->right:I

    add-int/2addr v0, v3

    add-int/2addr v0, v4

    iget-object v3, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    .line 2895
    invoke-virtual {v3}, Lcom/jess/ui/TwoWayGridView;->getHorizontalFadingEdgeLength()I

    move-result v3

    mul-int/lit8 v3, v3, 0x2

    add-int/2addr v0, v3

    .line 2898
    :goto_4
    const/high16 v3, -0x80000000

    if-ne v5, v3, :cond_3

    .line 2899
    iget-object v3, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    iget-object v3, v3, Lcom/jess/ui/TwoWayGridView;->m:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->left:I

    iget-object v5, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    iget-object v5, v5, Lcom/jess/ui/TwoWayGridView;->m:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->right:I

    add-int/2addr v3, v5

    .line 2901
    iget-object v5, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-static {v5}, Lcom/jess/ui/TwoWayGridView;->n(Lcom/jess/ui/TwoWayGridView;)I

    move-result v5

    move v10, v2

    move v2, v3

    move v3, v10

    .line 2902
    :goto_5
    if-ge v3, v6, :cond_7

    .line 2903
    add-int/2addr v2, v4

    .line 2904
    add-int v7, v3, v5

    if-ge v7, v6, :cond_2

    .line 2905
    iget-object v7, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-static {v7}, Lcom/jess/ui/TwoWayGridView;->e(Lcom/jess/ui/TwoWayGridView;)I

    move-result v7

    add-int/2addr v2, v7

    .line 2907
    :cond_2
    if-lt v2, v0, :cond_6

    .line 2915
    :cond_3
    :goto_6
    iget-object v2, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-static {v2, v0, v1}, Lcom/jess/ui/TwoWayGridView;->b(Lcom/jess/ui/TwoWayGridView;II)V

    .line 2916
    iget-object v0, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    iput p1, v0, Lcom/jess/ui/TwoWayGridView;->n:I

    .line 2918
    return-void

    .line 2856
    :cond_4
    iget-object v0, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    iget-object v0, v0, Lcom/jess/ui/TwoWayGridView;->m:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->top:I

    iget-object v1, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    iget-object v1, v1, Lcom/jess/ui/TwoWayGridView;->m:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v0, v1

    goto/16 :goto_0

    .line 2866
    :cond_5
    iget-object v0, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    iget-object v0, v0, Lcom/jess/ui/TwoWayGridView;->c:Landroid/widget/ListAdapter;

    invoke-interface {v0}, Landroid/widget/ListAdapter;->getCount()I

    move-result v0

    goto/16 :goto_2

    .line 2902
    :cond_6
    add-int/2addr v3, v5

    goto :goto_5

    :cond_7
    move v0, v2

    goto :goto_6

    :cond_8
    move v0, v3

    goto :goto_4

    :cond_9
    move v4, v2

    goto :goto_3

    :cond_a
    move v1, v0

    goto/16 :goto_1
.end method

.method protected a(Z)V
    .locals 5

    .prologue
    .line 2197
    iget-object v0, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-static {v0}, Lcom/jess/ui/TwoWayGridView;->n(Lcom/jess/ui/TwoWayGridView;)I

    move-result v2

    .line 2198
    iget-object v0, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-static {v0}, Lcom/jess/ui/TwoWayGridView;->e(Lcom/jess/ui/TwoWayGridView;)I

    move-result v3

    .line 2200
    iget-object v0, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-virtual {v0}, Lcom/jess/ui/TwoWayGridView;->getChildCount()I

    move-result v1

    .line 2202
    if-eqz p1, :cond_2

    .line 2203
    if-lez v1, :cond_1

    iget-object v0, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    add-int/lit8 v4, v1, -0x1

    .line 2204
    invoke-virtual {v0, v4}, Lcom/jess/ui/TwoWayGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getRight()I

    move-result v0

    add-int/2addr v0, v3

    .line 2205
    :goto_0
    iget-object v4, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    iget v4, v4, Lcom/jess/ui/TwoWayGridView;->H:I

    add-int/2addr v1, v4

    .line 2206
    iget-object v4, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    iget-boolean v4, v4, Lcom/jess/ui/TwoWayGridView;->y:Z

    if-eqz v4, :cond_0

    .line 2207
    add-int/lit8 v4, v2, -0x1

    add-int/2addr v1, v4

    .line 2209
    :cond_0
    invoke-direct {p0, v1, v0}, Lcom/jess/ui/af;->c(II)Landroid/view/View;

    .line 2210
    iget-object v0, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-virtual {v0}, Lcom/jess/ui/TwoWayGridView;->getChildCount()I

    move-result v0

    invoke-direct {p0, v2, v3, v0}, Lcom/jess/ui/af;->a(III)V

    .line 2223
    :goto_1
    return-void

    .line 2204
    :cond_1
    iget-object v0, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-virtual {v0}, Lcom/jess/ui/TwoWayGridView;->getListPaddingLeft()I

    move-result v0

    goto :goto_0

    .line 2212
    :cond_2
    if-lez v1, :cond_3

    iget-object v0, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    const/4 v1, 0x0

    .line 2213
    invoke-virtual {v0, v1}, Lcom/jess/ui/TwoWayGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v0

    sub-int/2addr v0, v3

    .line 2214
    :goto_2
    iget-object v1, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    iget v1, v1, Lcom/jess/ui/TwoWayGridView;->H:I

    .line 2215
    iget-object v4, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    iget-boolean v4, v4, Lcom/jess/ui/TwoWayGridView;->y:Z

    if-nez v4, :cond_4

    .line 2216
    sub-int/2addr v1, v2

    .line 2220
    :goto_3
    invoke-direct {p0, v1, v0}, Lcom/jess/ui/af;->d(II)Landroid/view/View;

    .line 2221
    iget-object v0, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-virtual {v0}, Lcom/jess/ui/TwoWayGridView;->getChildCount()I

    move-result v0

    invoke-direct {p0, v2, v3, v0}, Lcom/jess/ui/af;->b(III)V

    goto :goto_1

    .line 2213
    :cond_3
    iget-object v0, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-virtual {v0}, Lcom/jess/ui/TwoWayGridView;->getWidth()I

    move-result v0

    iget-object v1, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-virtual {v1}, Lcom/jess/ui/TwoWayGridView;->getListPaddingRight()I

    move-result v1

    sub-int/2addr v0, v1

    goto :goto_2

    .line 2218
    :cond_4
    add-int/lit8 v1, v1, -0x1

    goto :goto_3
.end method

.method protected b(I)Z
    .locals 8

    .prologue
    const/4 v7, 0x6

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 3353
    iget-object v2, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    iget v4, v2, Lcom/jess/ui/TwoWayGridView;->V:I

    .line 3354
    iget-object v2, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-static {v2}, Lcom/jess/ui/TwoWayGridView;->n(Lcom/jess/ui/TwoWayGridView;)I

    move-result v5

    .line 3361
    iget-object v2, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    iget-boolean v2, v2, Lcom/jess/ui/TwoWayGridView;->y:Z

    if-nez v2, :cond_3

    .line 3362
    div-int v2, v4, v5

    mul-int v3, v2, v5

    .line 3363
    add-int v2, v3, v5

    add-int/lit8 v2, v2, -0x1

    iget-object v6, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    iget v6, v6, Lcom/jess/ui/TwoWayGridView;->aa:I

    add-int/lit8 v6, v6, -0x1

    invoke-static {v2, v6}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 3370
    :goto_0
    sparse-switch p1, :sswitch_data_0

    :cond_0
    move v0, v1

    .line 3401
    :goto_1
    if-eqz v0, :cond_1

    .line 3402
    iget-object v1, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-static {p1}, Landroid/view/SoundEffectConstants;->getContantForFocusDirection(I)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/jess/ui/TwoWayGridView;->playSoundEffect(I)V

    .line 3403
    iget-object v1, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-virtual {v1}, Lcom/jess/ui/TwoWayGridView;->a()V

    .line 3406
    :cond_1
    if-eqz v0, :cond_2

    .line 3410
    :cond_2
    return v0

    .line 3365
    :cond_3
    iget-object v2, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    iget v2, v2, Lcom/jess/ui/TwoWayGridView;->aa:I

    add-int/lit8 v2, v2, -0x1

    sub-int/2addr v2, v4

    .line 3366
    iget-object v3, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    iget v3, v3, Lcom/jess/ui/TwoWayGridView;->aa:I

    add-int/lit8 v3, v3, -0x1

    div-int/2addr v2, v5

    mul-int/2addr v2, v5

    sub-int v2, v3, v2

    .line 3367
    sub-int v3, v2, v5

    add-int/lit8 v3, v3, 0x1

    invoke-static {v1, v3}, Ljava/lang/Math;->max(II)I

    move-result v3

    goto :goto_0

    .line 3372
    :sswitch_0
    if-lez v3, :cond_0

    .line 3373
    iget-object v2, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    iput v7, v2, Lcom/jess/ui/TwoWayGridView;->a:I

    .line 3374
    sub-int v2, v4, v5

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/jess/ui/af;->a(I)V

    goto :goto_1

    .line 3379
    :sswitch_1
    iget-object v2, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    iget v2, v2, Lcom/jess/ui/TwoWayGridView;->aa:I

    add-int/lit8 v2, v2, -0x1

    if-ge v3, v2, :cond_0

    .line 3380
    iget-object v1, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    iput v7, v1, Lcom/jess/ui/TwoWayGridView;->a:I

    .line 3381
    add-int v1, v4, v5

    iget-object v2, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    iget v2, v2, Lcom/jess/ui/TwoWayGridView;->aa:I

    add-int/lit8 v2, v2, -0x1

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/jess/ui/af;->a(I)V

    goto :goto_1

    .line 3386
    :sswitch_2
    if-le v4, v3, :cond_0

    .line 3387
    iget-object v2, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    iput v7, v2, Lcom/jess/ui/TwoWayGridView;->a:I

    .line 3388
    add-int/lit8 v2, v4, -0x1

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/jess/ui/af;->a(I)V

    goto :goto_1

    .line 3393
    :sswitch_3
    if-ge v4, v2, :cond_0

    .line 3394
    iget-object v1, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    iput v7, v1, Lcom/jess/ui/TwoWayGridView;->a:I

    .line 3395
    add-int/lit8 v1, v4, 0x1

    iget-object v2, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    iget v2, v2, Lcom/jess/ui/TwoWayGridView;->aa:I

    add-int/lit8 v2, v2, -0x1

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/jess/ui/af;->a(I)V

    goto :goto_1

    .line 3370
    :sswitch_data_0
    .sparse-switch
        0x11 -> :sswitch_0
        0x21 -> :sswitch_2
        0x42 -> :sswitch_1
        0x82 -> :sswitch_3
    .end sparse-switch
.end method

.method protected b(II)Z
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 3423
    iget-object v2, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-virtual {v2}, Lcom/jess/ui/TwoWayGridView;->getChildCount()I

    move-result v4

    .line 3424
    add-int/lit8 v2, v4, -0x1

    sub-int/2addr v2, p1

    .line 3425
    iget-object v3, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-static {v3}, Lcom/jess/ui/TwoWayGridView;->n(Lcom/jess/ui/TwoWayGridView;)I

    move-result v5

    .line 3430
    iget-object v3, p0, Lcom/jess/ui/af;->b:Lcom/jess/ui/TwoWayGridView;

    iget-boolean v3, v3, Lcom/jess/ui/TwoWayGridView;->y:Z

    if-nez v3, :cond_0

    .line 3431
    rem-int v2, p1, v5

    sub-int v3, p1, v2

    .line 3432
    add-int v2, v3, v5

    add-int/lit8 v2, v2, -0x1

    invoke-static {v2, v4}, Ljava/lang/Math;->max(II)I

    move-result v2

    .line 3438
    :goto_0
    sparse-switch p2, :sswitch_data_0

    .line 3459
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "direction must be one of {FOCUS_UP, FOCUS_DOWN, FOCUS_LEFT, FOCUS_RIGHT, FOCUS_FORWARD, FOCUS_BACKWARD}."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 3434
    :cond_0
    add-int/lit8 v3, v4, -0x1

    rem-int v6, v2, v5

    sub-int/2addr v2, v6

    sub-int v2, v3, v2

    .line 3435
    sub-int v3, v2, v5

    add-int/lit8 v3, v3, 0x1

    invoke-static {v1, v3}, Ljava/lang/Math;->max(II)I

    move-result v3

    goto :goto_0

    .line 3442
    :sswitch_0
    if-ne p1, v3, :cond_2

    .line 3457
    :cond_1
    :goto_1
    return v0

    :cond_2
    move v0, v1

    .line 3442
    goto :goto_1

    .line 3445
    :sswitch_1
    if-eqz v3, :cond_1

    move v0, v1

    goto :goto_1

    .line 3448
    :sswitch_2
    if-eq p1, v3, :cond_1

    move v0, v1

    goto :goto_1

    .line 3451
    :sswitch_3
    add-int/lit8 v2, v4, -0x1

    if-eq v3, v2, :cond_1

    move v0, v1

    goto :goto_1

    .line 3454
    :sswitch_4
    if-ne p1, v3, :cond_3

    if-eqz v3, :cond_1

    :cond_3
    move v0, v1

    goto :goto_1

    .line 3457
    :sswitch_5
    if-ne p1, v2, :cond_4

    add-int/lit8 v3, v4, -0x1

    if-eq v2, v3, :cond_1

    :cond_4
    move v0, v1

    goto :goto_1

    .line 3438
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_5
        0x2 -> :sswitch_4
        0x11 -> :sswitch_2
        0x21 -> :sswitch_3
        0x42 -> :sswitch_0
        0x82 -> :sswitch_1
    .end sparse-switch
.end method
