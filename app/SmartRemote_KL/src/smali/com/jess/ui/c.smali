.class Lcom/jess/ui/c;
.super Lcom/jess/ui/u;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/jess/ui/TwoWayAbsListView;


# direct methods
.method private constructor <init>(Lcom/jess/ui/TwoWayAbsListView;)V
    .locals 1

    .prologue
    .line 1869
    iput-object p1, p0, Lcom/jess/ui/c;->a:Lcom/jess/ui/TwoWayAbsListView;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/jess/ui/u;-><init>(Lcom/jess/ui/TwoWayAbsListView;Lcom/jess/ui/a;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/jess/ui/TwoWayAbsListView;Lcom/jess/ui/a;)V
    .locals 0

    .prologue
    .line 1869
    invoke-direct {p0, p1}, Lcom/jess/ui/c;-><init>(Lcom/jess/ui/TwoWayAbsListView;)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 1871
    iget-object v0, p0, Lcom/jess/ui/c;->a:Lcom/jess/ui/TwoWayAbsListView;

    iget v0, v0, Lcom/jess/ui/TwoWayAbsListView;->t:I

    .line 1872
    iget-object v2, p0, Lcom/jess/ui/c;->a:Lcom/jess/ui/TwoWayAbsListView;

    iget-object v3, p0, Lcom/jess/ui/c;->a:Lcom/jess/ui/TwoWayAbsListView;

    iget v3, v3, Lcom/jess/ui/TwoWayAbsListView;->H:I

    sub-int/2addr v0, v3

    invoke-virtual {v2, v0}, Lcom/jess/ui/TwoWayAbsListView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 1873
    if-eqz v2, :cond_0

    .line 1874
    iget-object v0, p0, Lcom/jess/ui/c;->a:Lcom/jess/ui/TwoWayAbsListView;

    iget v0, v0, Lcom/jess/ui/TwoWayAbsListView;->t:I

    .line 1875
    iget-object v3, p0, Lcom/jess/ui/c;->a:Lcom/jess/ui/TwoWayAbsListView;

    iget-object v3, v3, Lcom/jess/ui/TwoWayAbsListView;->c:Landroid/widget/ListAdapter;

    iget-object v4, p0, Lcom/jess/ui/c;->a:Lcom/jess/ui/TwoWayAbsListView;

    iget v4, v4, Lcom/jess/ui/TwoWayAbsListView;->t:I

    invoke-interface {v3, v4}, Landroid/widget/ListAdapter;->getItemId(I)J

    move-result-wide v4

    .line 1878
    invoke-virtual {p0}, Lcom/jess/ui/c;->b()Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/jess/ui/c;->a:Lcom/jess/ui/TwoWayAbsListView;

    iget-boolean v3, v3, Lcom/jess/ui/TwoWayAbsListView;->S:Z

    if-nez v3, :cond_2

    .line 1879
    iget-object v3, p0, Lcom/jess/ui/c;->a:Lcom/jess/ui/TwoWayAbsListView;

    invoke-static {v3, v2, v0, v4, v5}, Lcom/jess/ui/TwoWayAbsListView;->a(Lcom/jess/ui/TwoWayAbsListView;Landroid/view/View;IJ)Z

    move-result v0

    .line 1881
    :goto_0
    if-eqz v0, :cond_1

    .line 1882
    iget-object v0, p0, Lcom/jess/ui/c;->a:Lcom/jess/ui/TwoWayAbsListView;

    const/4 v3, -0x1

    iput v3, v0, Lcom/jess/ui/TwoWayAbsListView;->w:I

    .line 1883
    iget-object v0, p0, Lcom/jess/ui/c;->a:Lcom/jess/ui/TwoWayAbsListView;

    invoke-virtual {v0, v1}, Lcom/jess/ui/TwoWayAbsListView;->setPressed(Z)V

    .line 1884
    invoke-virtual {v2, v1}, Landroid/view/View;->setPressed(Z)V

    .line 1890
    :cond_0
    :goto_1
    return-void

    .line 1886
    :cond_1
    iget-object v0, p0, Lcom/jess/ui/c;->a:Lcom/jess/ui/TwoWayAbsListView;

    const/4 v1, 0x2

    iput v1, v0, Lcom/jess/ui/TwoWayAbsListView;->w:I

    goto :goto_1

    :cond_2
    move v0, v1

    goto :goto_0
.end method
