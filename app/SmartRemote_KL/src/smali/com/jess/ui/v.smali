.class public abstract Lcom/jess/ui/v;
.super Landroid/view/ViewGroup;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T::",
        "Landroid/widget/Adapter;",
        ">",
        "Landroid/view/ViewGroup;"
    }
.end annotation


# instance fields
.field protected F:Landroid/content/Context;

.field protected G:Z

.field H:I
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
    .end annotation
.end field

.field I:I

.field J:I

.field K:J

.field L:J

.field M:Z

.field N:I

.field O:Z

.field P:Lcom/jess/ui/ab;

.field Q:Lcom/jess/ui/z;

.field R:Lcom/jess/ui/aa;

.field S:Z

.field T:I
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
    .end annotation
.end field

.field U:J

.field V:I
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
    .end annotation
.end field

.field W:J

.field private a:I

.field aa:I
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
    .end annotation
.end field

.field ab:I

.field ac:I

.field ad:J

.field ae:Z

.field private b:I

.field private c:Landroid/view/View;

.field private d:Z

.field private e:Z

.field private f:Lcom/jess/ui/ac;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jess/ui/v",
            "<TT;>.com/jess/ui/ac;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 5

    .prologue
    const/4 v4, -0x1

    const-wide/high16 v2, -0x8000000000000000L

    const/4 v1, 0x0

    .line 248
    invoke-direct {p0, p1}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;)V

    .line 72
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/jess/ui/v;->F:Landroid/content/Context;

    .line 77
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/jess/ui/v;->G:Z

    .line 82
    iput v1, p0, Lcom/jess/ui/v;->H:I

    .line 99
    iput-wide v2, p0, Lcom/jess/ui/v;->K:J

    .line 109
    iput-boolean v1, p0, Lcom/jess/ui/v;->M:Z

    .line 146
    iput-boolean v1, p0, Lcom/jess/ui/v;->O:Z

    .line 172
    iput v4, p0, Lcom/jess/ui/v;->T:I

    .line 178
    iput-wide v2, p0, Lcom/jess/ui/v;->U:J

    .line 183
    iput v4, p0, Lcom/jess/ui/v;->V:I

    .line 189
    iput-wide v2, p0, Lcom/jess/ui/v;->W:J

    .line 221
    iput v4, p0, Lcom/jess/ui/v;->ac:I

    .line 226
    iput-wide v2, p0, Lcom/jess/ui/v;->ad:J

    .line 245
    iput-boolean v1, p0, Lcom/jess/ui/v;->ae:Z

    .line 249
    iput-object p1, p0, Lcom/jess/ui/v;->F:Landroid/content/Context;

    .line 250
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 5

    .prologue
    const/4 v4, -0x1

    const-wide/high16 v2, -0x8000000000000000L

    const/4 v1, 0x0

    .line 253
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 72
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/jess/ui/v;->F:Landroid/content/Context;

    .line 77
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/jess/ui/v;->G:Z

    .line 82
    iput v1, p0, Lcom/jess/ui/v;->H:I

    .line 99
    iput-wide v2, p0, Lcom/jess/ui/v;->K:J

    .line 109
    iput-boolean v1, p0, Lcom/jess/ui/v;->M:Z

    .line 146
    iput-boolean v1, p0, Lcom/jess/ui/v;->O:Z

    .line 172
    iput v4, p0, Lcom/jess/ui/v;->T:I

    .line 178
    iput-wide v2, p0, Lcom/jess/ui/v;->U:J

    .line 183
    iput v4, p0, Lcom/jess/ui/v;->V:I

    .line 189
    iput-wide v2, p0, Lcom/jess/ui/v;->W:J

    .line 221
    iput v4, p0, Lcom/jess/ui/v;->ac:I

    .line 226
    iput-wide v2, p0, Lcom/jess/ui/v;->ad:J

    .line 245
    iput-boolean v1, p0, Lcom/jess/ui/v;->ae:Z

    .line 254
    iput-object p1, p0, Lcom/jess/ui/v;->F:Landroid/content/Context;

    .line 255
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 5

    .prologue
    const/4 v4, -0x1

    const-wide/high16 v2, -0x8000000000000000L

    const/4 v1, 0x0

    .line 258
    invoke-direct {p0, p1, p2, p3}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 72
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/jess/ui/v;->F:Landroid/content/Context;

    .line 77
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/jess/ui/v;->G:Z

    .line 82
    iput v1, p0, Lcom/jess/ui/v;->H:I

    .line 99
    iput-wide v2, p0, Lcom/jess/ui/v;->K:J

    .line 109
    iput-boolean v1, p0, Lcom/jess/ui/v;->M:Z

    .line 146
    iput-boolean v1, p0, Lcom/jess/ui/v;->O:Z

    .line 172
    iput v4, p0, Lcom/jess/ui/v;->T:I

    .line 178
    iput-wide v2, p0, Lcom/jess/ui/v;->U:J

    .line 183
    iput v4, p0, Lcom/jess/ui/v;->V:I

    .line 189
    iput-wide v2, p0, Lcom/jess/ui/v;->W:J

    .line 221
    iput v4, p0, Lcom/jess/ui/v;->ac:I

    .line 226
    iput-wide v2, p0, Lcom/jess/ui/v;->ad:J

    .line 245
    iput-boolean v1, p0, Lcom/jess/ui/v;->ae:Z

    .line 259
    iput-object p1, p0, Lcom/jess/ui/v;->F:Landroid/content/Context;

    .line 260
    return-void
.end method

.method static synthetic a(Lcom/jess/ui/v;)Landroid/os/Parcelable;
    .locals 1

    .prologue
    .line 58
    invoke-virtual {p0}, Lcom/jess/ui/v;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v0

    return-object v0
.end method

.method private a()V
    .locals 6

    .prologue
    .line 891
    iget-object v0, p0, Lcom/jess/ui/v;->P:Lcom/jess/ui/ab;

    if-nez v0, :cond_0

    .line 902
    :goto_0
    return-void

    .line 894
    :cond_0
    invoke-virtual {p0}, Lcom/jess/ui/v;->getSelectedItemPosition()I

    move-result v3

    .line 895
    if-ltz v3, :cond_1

    .line 896
    invoke-virtual {p0}, Lcom/jess/ui/v;->getSelectedView()Landroid/view/View;

    move-result-object v2

    .line 897
    iget-object v0, p0, Lcom/jess/ui/v;->P:Lcom/jess/ui/ab;

    .line 898
    invoke-virtual {p0}, Lcom/jess/ui/v;->getAdapter()Landroid/widget/Adapter;

    move-result-object v1

    invoke-interface {v1, v3}, Landroid/widget/Adapter;->getItemId(I)J

    move-result-wide v4

    move-object v1, p0

    .line 897
    invoke-interface/range {v0 .. v5}, Lcom/jess/ui/ab;->a(Lcom/jess/ui/v;Landroid/view/View;IJ)V

    goto :goto_0

    .line 900
    :cond_1
    iget-object v0, p0, Lcom/jess/ui/v;->P:Lcom/jess/ui/ab;

    invoke-interface {v0, p0}, Lcom/jess/ui/ab;->a(Lcom/jess/ui/v;)V

    goto :goto_0
.end method

.method static synthetic a(Lcom/jess/ui/v;Landroid/os/Parcelable;)V
    .locals 0

    .prologue
    .line 58
    invoke-virtual {p0, p1}, Lcom/jess/ui/v;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    return-void
.end method

.method private a(Z)V
    .locals 6

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 739
    invoke-virtual {p0}, Lcom/jess/ui/v;->m()Z

    move-result v0

    if-eqz v0, :cond_0

    move p1, v1

    .line 743
    :cond_0
    if-eqz p1, :cond_3

    .line 744
    iget-object v0, p0, Lcom/jess/ui/v;->c:Landroid/view/View;

    if-eqz v0, :cond_2

    .line 745
    iget-object v0, p0, Lcom/jess/ui/v;->c:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 746
    invoke-virtual {p0, v2}, Lcom/jess/ui/v;->setVisibility(I)V

    .line 755
    :goto_0
    iget-boolean v0, p0, Lcom/jess/ui/v;->S:Z

    if-eqz v0, :cond_1

    .line 756
    invoke-virtual {p0}, Lcom/jess/ui/v;->getLeft()I

    move-result v2

    invoke-virtual {p0}, Lcom/jess/ui/v;->getTop()I

    move-result v3

    invoke-virtual {p0}, Lcom/jess/ui/v;->getRight()I

    move-result v4

    invoke-virtual {p0}, Lcom/jess/ui/v;->getBottom()I

    move-result v5

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/jess/ui/v;->onLayout(ZIIII)V

    .line 762
    :cond_1
    :goto_1
    return-void

    .line 749
    :cond_2
    invoke-virtual {p0, v1}, Lcom/jess/ui/v;->setVisibility(I)V

    goto :goto_0

    .line 759
    :cond_3
    iget-object v0, p0, Lcom/jess/ui/v;->c:Landroid/view/View;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/jess/ui/v;->c:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 760
    :cond_4
    invoke-virtual {p0, v1}, Lcom/jess/ui/v;->setVisibility(I)V

    goto :goto_1
.end method

.method static synthetic b(Lcom/jess/ui/v;)V
    .locals 0

    .prologue
    .line 58
    invoke-direct {p0}, Lcom/jess/ui/v;->a()V

    return-void
.end method


# virtual methods
.method a(IZ)I
    .locals 0

    .prologue
    .line 1106
    return p1
.end method

.method public addView(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 466
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string/jumbo v1, "addView(View) is not supported in AdapterView"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public addView(Landroid/view/View;I)V
    .locals 2

    .prologue
    .line 479
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string/jumbo v1, "addView(View, int) is not supported in AdapterView"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V
    .locals 2

    .prologue
    .line 507
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string/jumbo v1, "addView(View, int, LayoutParams) is not supported in AdapterView"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    .locals 2

    .prologue
    .line 492
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string/jumbo v1, "addView(View, LayoutParams) is not supported in AdapterView"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public b(Landroid/view/View;)I
    .locals 4

    .prologue
    const/4 v1, -0x1

    .line 610
    .line 613
    :goto_0
    :try_start_0
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {v0, p0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    if-nez v2, :cond_0

    move-object p1, v0

    .line 614
    goto :goto_0

    .line 616
    :catch_0
    move-exception v0

    move v0, v1

    .line 630
    :goto_1
    return v0

    .line 622
    :cond_0
    invoke-virtual {p0}, Lcom/jess/ui/v;->getChildCount()I

    move-result v2

    .line 623
    const/4 v0, 0x0

    :goto_2
    if-ge v0, v2, :cond_2

    .line 624
    invoke-virtual {p0, v0}, Lcom/jess/ui/v;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 625
    iget v1, p0, Lcom/jess/ui/v;->H:I

    add-int/2addr v0, v1

    goto :goto_1

    .line 623
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    move v0, v1

    .line 630
    goto :goto_1
.end method

.method public b(Landroid/view/View;IJ)Z
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 313
    iget-object v1, p0, Lcom/jess/ui/v;->Q:Lcom/jess/ui/z;

    if-eqz v1, :cond_0

    .line 314
    invoke-virtual {p0, v0}, Lcom/jess/ui/v;->playSoundEffect(I)V

    .line 315
    iget-object v0, p0, Lcom/jess/ui/v;->Q:Lcom/jess/ui/z;

    move-object v1, p0

    move-object v2, p1

    move v3, p2

    move-wide v4, p3

    invoke-interface/range {v0 .. v5}, Lcom/jess/ui/z;->a(Lcom/jess/ui/v;Landroid/view/View;IJ)V

    .line 316
    const/4 v0, 0x1

    .line 319
    :cond_0
    return v0
.end method

.method public c(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 771
    invoke-virtual {p0}, Lcom/jess/ui/v;->getAdapter()Landroid/widget/Adapter;

    move-result-object v0

    .line 772
    if-eqz v0, :cond_0

    if-gez p1, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_1
    invoke-interface {v0, p1}, Landroid/widget/Adapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method protected canAnimate()Z
    .locals 1

    .prologue
    .line 936
    invoke-super {p0}, Landroid/view/ViewGroup;->canAnimate()Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/jess/ui/v;->aa:I

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public d(I)J
    .locals 2

    .prologue
    .line 776
    invoke-virtual {p0}, Lcom/jess/ui/v;->getAdapter()Landroid/widget/Adapter;

    move-result-object v0

    .line 777
    if-eqz v0, :cond_0

    if-gez p1, :cond_1

    :cond_0
    const-wide/high16 v0, -0x8000000000000000L

    :goto_0
    return-wide v0

    :cond_1
    invoke-interface {v0, p1}, Landroid/widget/Adapter;->getItemId(I)J

    move-result-wide v0

    goto :goto_0
.end method

.method protected dispatchRestoreInstanceState(Landroid/util/SparseArray;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/SparseArray",
            "<",
            "Landroid/os/Parcelable;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 799
    invoke-virtual {p0, p1}, Lcom/jess/ui/v;->dispatchThawSelfOnly(Landroid/util/SparseArray;)V

    .line 800
    return-void
.end method

.method protected dispatchSaveInstanceState(Landroid/util/SparseArray;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/SparseArray",
            "<",
            "Landroid/os/Parcelable;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 791
    invoke-virtual {p0, p1}, Lcom/jess/ui/v;->dispatchFreezeSelfOnly(Landroid/util/SparseArray;)V

    .line 792
    return-void
.end method

.method public e(I)V
    .locals 3

    .prologue
    .line 1188
    invoke-virtual {p0}, Lcom/jess/ui/v;->getChildCount()I

    move-result v1

    .line 1190
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    .line 1191
    invoke-virtual {p0, v0}, Lcom/jess/ui/v;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 1192
    invoke-virtual {v2, p1}, Landroid/view/View;->offsetTopAndBottom(I)V

    .line 1190
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1194
    :cond_0
    return-void
.end method

.method public f(I)V
    .locals 3

    .prologue
    .line 1203
    invoke-virtual {p0}, Lcom/jess/ui/v;->getChildCount()I

    move-result v1

    .line 1205
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    .line 1206
    invoke-virtual {p0, v0}, Lcom/jess/ui/v;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 1207
    invoke-virtual {v2, p1}, Landroid/view/View;->offsetLeftAndRight(I)V

    .line 1205
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1209
    :cond_0
    return-void
.end method

.method public abstract getAdapter()Landroid/widget/Adapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation
.end method

.method public getCount()I
    .locals 1
    .annotation runtime Landroid/view/ViewDebug$CapturedViewProperty;
    .end annotation

    .prologue
    .line 597
    iget v0, p0, Lcom/jess/ui/v;->aa:I

    return v0
.end method

.method public getEmptyView()Landroid/view/View;
    .locals 1

    .prologue
    .line 680
    iget-object v0, p0, Lcom/jess/ui/v;->c:Landroid/view/View;

    return-object v0
.end method

.method public getFirstVisiblePosition()I
    .locals 1

    .prologue
    .line 640
    iget v0, p0, Lcom/jess/ui/v;->H:I

    return v0
.end method

.method public getLastVisiblePosition()I
    .locals 2

    .prologue
    .line 650
    iget v0, p0, Lcom/jess/ui/v;->H:I

    invoke-virtual {p0}, Lcom/jess/ui/v;->getChildCount()I

    move-result v1

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, -0x1

    return v0
.end method

.method public final getOnItemClickListener()Lcom/jess/ui/z;
    .locals 1

    .prologue
    .line 300
    iget-object v0, p0, Lcom/jess/ui/v;->Q:Lcom/jess/ui/z;

    return-object v0
.end method

.method public final getOnItemLongClickListener()Lcom/jess/ui/aa;
    .locals 1

    .prologue
    .line 363
    iget-object v0, p0, Lcom/jess/ui/v;->R:Lcom/jess/ui/aa;

    return-object v0
.end method

.method public final getOnItemSelectedListener()Lcom/jess/ui/ab;
    .locals 1

    .prologue
    .line 407
    iget-object v0, p0, Lcom/jess/ui/v;->P:Lcom/jess/ui/ab;

    return-object v0
.end method

.method public getSelectedItem()Ljava/lang/Object;
    .locals 3

    .prologue
    .line 581
    invoke-virtual {p0}, Lcom/jess/ui/v;->getAdapter()Landroid/widget/Adapter;

    move-result-object v0

    .line 582
    invoke-virtual {p0}, Lcom/jess/ui/v;->getSelectedItemPosition()I

    move-result v1

    .line 583
    if-eqz v0, :cond_0

    invoke-interface {v0}, Landroid/widget/Adapter;->getCount()I

    move-result v2

    if-lez v2, :cond_0

    if-ltz v1, :cond_0

    .line 584
    invoke-interface {v0, v1}, Landroid/widget/Adapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    .line 586
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getSelectedItemId()J
    .locals 2
    .annotation runtime Landroid/view/ViewDebug$CapturedViewProperty;
    .end annotation

    .prologue
    .line 567
    iget-wide v0, p0, Lcom/jess/ui/v;->U:J

    return-wide v0
.end method

.method public getSelectedItemPosition()I
    .locals 1
    .annotation runtime Landroid/view/ViewDebug$CapturedViewProperty;
    .end annotation

    .prologue
    .line 558
    iget v0, p0, Lcom/jess/ui/v;->T:I

    return v0
.end method

.method public abstract getSelectedView()Landroid/view/View;
.end method

.method l()V
    .locals 8

    .prologue
    const-wide/high16 v6, -0x8000000000000000L

    const/4 v5, -0x1

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 940
    iget v4, p0, Lcom/jess/ui/v;->aa:I

    .line 943
    if-lez v4, :cond_6

    .line 948
    iget-boolean v0, p0, Lcom/jess/ui/v;->M:Z

    if-eqz v0, :cond_5

    .line 951
    iput-boolean v1, p0, Lcom/jess/ui/v;->M:Z

    .line 955
    invoke-virtual {p0}, Lcom/jess/ui/v;->q()I

    move-result v0

    .line 956
    if-ltz v0, :cond_5

    .line 958
    invoke-virtual {p0, v0, v2}, Lcom/jess/ui/v;->a(IZ)I

    move-result v3

    .line 959
    if-ne v3, v0, :cond_5

    .line 961
    invoke-virtual {p0, v0}, Lcom/jess/ui/v;->setNextSelectedPositionInt(I)V

    move v3, v2

    .line 966
    :goto_0
    if-nez v3, :cond_3

    .line 968
    invoke-virtual {p0}, Lcom/jess/ui/v;->getSelectedItemPosition()I

    move-result v0

    .line 971
    if-lt v0, v4, :cond_0

    .line 972
    add-int/lit8 v0, v4, -0x1

    .line 974
    :cond_0
    if-gez v0, :cond_1

    move v0, v1

    .line 979
    :cond_1
    invoke-virtual {p0, v0, v2}, Lcom/jess/ui/v;->a(IZ)I

    move-result v4

    .line 980
    if-gez v4, :cond_4

    .line 982
    invoke-virtual {p0, v0, v1}, Lcom/jess/ui/v;->a(IZ)I

    move-result v0

    .line 984
    :goto_1
    if-ltz v0, :cond_3

    .line 985
    invoke-virtual {p0, v0}, Lcom/jess/ui/v;->setNextSelectedPositionInt(I)V

    .line 986
    invoke-virtual {p0}, Lcom/jess/ui/v;->p()V

    move v0, v2

    .line 991
    :goto_2
    if-nez v0, :cond_2

    .line 993
    iput v5, p0, Lcom/jess/ui/v;->V:I

    .line 994
    iput-wide v6, p0, Lcom/jess/ui/v;->W:J

    .line 995
    iput v5, p0, Lcom/jess/ui/v;->T:I

    .line 996
    iput-wide v6, p0, Lcom/jess/ui/v;->U:J

    .line 997
    iput-boolean v1, p0, Lcom/jess/ui/v;->M:Z

    .line 998
    invoke-virtual {p0}, Lcom/jess/ui/v;->p()V

    .line 1000
    :cond_2
    return-void

    :cond_3
    move v0, v3

    goto :goto_2

    :cond_4
    move v0, v4

    goto :goto_1

    :cond_5
    move v3, v1

    goto :goto_0

    :cond_6
    move v0, v1

    goto :goto_2
.end method

.method m()Z
    .locals 1

    .prologue
    .line 690
    const/4 v0, 0x0

    return v0
.end method

.method n()V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 720
    invoke-virtual {p0}, Lcom/jess/ui/v;->getAdapter()Landroid/widget/Adapter;

    move-result-object v4

    .line 721
    if-eqz v4, :cond_0

    invoke-interface {v4}, Landroid/widget/Adapter;->getCount()I

    move-result v0

    if-nez v0, :cond_5

    :cond_0
    move v0, v1

    .line 722
    :goto_0
    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/jess/ui/v;->m()Z

    move-result v0

    if-eqz v0, :cond_6

    :cond_1
    move v3, v1

    .line 726
    :goto_1
    if-eqz v3, :cond_7

    iget-boolean v0, p0, Lcom/jess/ui/v;->e:Z

    if-eqz v0, :cond_7

    move v0, v1

    :goto_2
    invoke-super {p0, v0}, Landroid/view/ViewGroup;->setFocusableInTouchMode(Z)V

    .line 727
    if-eqz v3, :cond_8

    iget-boolean v0, p0, Lcom/jess/ui/v;->d:Z

    if-eqz v0, :cond_8

    move v0, v1

    :goto_3
    invoke-super {p0, v0}, Landroid/view/ViewGroup;->setFocusable(Z)V

    .line 728
    iget-object v0, p0, Lcom/jess/ui/v;->c:Landroid/view/View;

    if-eqz v0, :cond_4

    .line 729
    if-eqz v4, :cond_2

    invoke-interface {v4}, Landroid/widget/Adapter;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_2
    move v2, v1

    :cond_3
    invoke-direct {p0, v2}, Lcom/jess/ui/v;->a(Z)V

    .line 731
    :cond_4
    return-void

    :cond_5
    move v0, v2

    .line 721
    goto :goto_0

    :cond_6
    move v3, v2

    .line 722
    goto :goto_1

    :cond_7
    move v0, v2

    .line 726
    goto :goto_2

    :cond_8
    move v0, v2

    .line 727
    goto :goto_3
.end method

.method o()V
    .locals 2

    .prologue
    .line 868
    iget-object v0, p0, Lcom/jess/ui/v;->P:Lcom/jess/ui/ab;

    if-eqz v0, :cond_2

    .line 869
    iget-boolean v0, p0, Lcom/jess/ui/v;->O:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/jess/ui/v;->ae:Z

    if-eqz v0, :cond_3

    .line 874
    :cond_0
    iget-object v0, p0, Lcom/jess/ui/v;->f:Lcom/jess/ui/ac;

    if-nez v0, :cond_1

    .line 875
    new-instance v0, Lcom/jess/ui/ac;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/jess/ui/ac;-><init>(Lcom/jess/ui/v;Lcom/jess/ui/w;)V

    iput-object v0, p0, Lcom/jess/ui/v;->f:Lcom/jess/ui/ac;

    .line 877
    :cond_1
    iget-object v0, p0, Lcom/jess/ui/v;->f:Lcom/jess/ui/ac;

    iget-object v1, p0, Lcom/jess/ui/v;->f:Lcom/jess/ui/ac;

    invoke-virtual {v0, v1}, Lcom/jess/ui/ac;->post(Ljava/lang/Runnable;)Z

    .line 888
    :cond_2
    :goto_0
    return-void

    .line 879
    :cond_3
    invoke-direct {p0}, Lcom/jess/ui/v;->a()V

    goto :goto_0
.end method

.method protected onLayout(ZIIII)V
    .locals 1

    .prologue
    .line 547
    invoke-virtual {p0}, Lcom/jess/ui/v;->getHeight()I

    move-result v0

    iput v0, p0, Lcom/jess/ui/v;->a:I

    .line 548
    invoke-virtual {p0}, Lcom/jess/ui/v;->getWidth()I

    move-result v0

    iput v0, p0, Lcom/jess/ui/v;->b:I

    .line 549
    return-void
.end method

.method p()V
    .locals 4

    .prologue
    .line 1003
    iget v0, p0, Lcom/jess/ui/v;->V:I

    iget v1, p0, Lcom/jess/ui/v;->ac:I

    if-ne v0, v1, :cond_0

    iget-wide v0, p0, Lcom/jess/ui/v;->W:J

    iget-wide v2, p0, Lcom/jess/ui/v;->ad:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    .line 1004
    :cond_0
    invoke-virtual {p0}, Lcom/jess/ui/v;->o()V

    .line 1005
    iget v0, p0, Lcom/jess/ui/v;->V:I

    iput v0, p0, Lcom/jess/ui/v;->ac:I

    .line 1006
    iget-wide v0, p0, Lcom/jess/ui/v;->W:J

    iput-wide v0, p0, Lcom/jess/ui/v;->ad:J

    .line 1008
    :cond_1
    return-void
.end method

.method q()I
    .locals 12

    .prologue
    .line 1019
    iget v6, p0, Lcom/jess/ui/v;->aa:I

    .line 1021
    if-nez v6, :cond_1

    .line 1022
    const/4 v3, -0x1

    .line 1094
    :cond_0
    :goto_0
    return v3

    .line 1025
    :cond_1
    iget-wide v8, p0, Lcom/jess/ui/v;->K:J

    .line 1026
    iget v0, p0, Lcom/jess/ui/v;->J:I

    .line 1029
    const-wide/high16 v2, -0x8000000000000000L

    cmp-long v1, v8, v2

    if-nez v1, :cond_2

    .line 1030
    const/4 v3, -0x1

    goto :goto_0

    .line 1034
    :cond_2
    const/4 v1, 0x0

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 1035
    add-int/lit8 v1, v6, -0x1

    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 1037
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    const-wide/16 v4, 0x64

    add-long v10, v2, v4

    .line 1048
    const/4 v0, 0x0

    .line 1058
    invoke-virtual {p0}, Lcom/jess/ui/v;->getAdapter()Landroid/widget/Adapter;

    move-result-object v7

    .line 1059
    if-nez v7, :cond_b

    .line 1060
    const/4 v3, -0x1

    goto :goto_0

    .line 1078
    :cond_3
    if-nez v4, :cond_4

    if-eqz v0, :cond_9

    if-nez v5, :cond_9

    .line 1080
    :cond_4
    add-int/lit8 v1, v1, 0x1

    .line 1083
    const/4 v0, 0x0

    move v3, v1

    .line 1063
    :cond_5
    :goto_1
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    cmp-long v4, v4, v10

    if-gtz v4, :cond_6

    .line 1064
    invoke-interface {v7, v3}, Landroid/widget/Adapter;->getItemId(I)J

    move-result-wide v4

    .line 1065
    cmp-long v4, v4, v8

    if-eqz v4, :cond_0

    .line 1070
    add-int/lit8 v4, v6, -0x1

    if-ne v1, v4, :cond_7

    const/4 v4, 0x1

    move v5, v4

    .line 1071
    :goto_2
    if-nez v2, :cond_8

    const/4 v4, 0x1

    .line 1073
    :goto_3
    if-eqz v5, :cond_3

    if-eqz v4, :cond_3

    .line 1094
    :cond_6
    const/4 v3, -0x1

    goto :goto_0

    .line 1070
    :cond_7
    const/4 v4, 0x0

    move v5, v4

    goto :goto_2

    .line 1071
    :cond_8
    const/4 v4, 0x0

    goto :goto_3

    .line 1084
    :cond_9
    if-nez v5, :cond_a

    if-nez v0, :cond_5

    if-nez v4, :cond_5

    .line 1086
    :cond_a
    add-int/lit8 v2, v2, -0x1

    .line 1089
    const/4 v0, 0x1

    move v3, v2

    goto :goto_1

    :cond_b
    move v2, v1

    move v3, v1

    goto :goto_1
.end method

.method r()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1139
    invoke-virtual {p0}, Lcom/jess/ui/v;->getChildCount()I

    move-result v0

    if-lez v0, :cond_1

    .line 1140
    iput-boolean v5, p0, Lcom/jess/ui/v;->M:Z

    .line 1141
    iget-boolean v0, p0, Lcom/jess/ui/v;->G:Z

    if-eqz v0, :cond_2

    .line 1142
    iget v0, p0, Lcom/jess/ui/v;->a:I

    int-to-long v0, v0

    iput-wide v0, p0, Lcom/jess/ui/v;->L:J

    .line 1146
    :goto_0
    iget v0, p0, Lcom/jess/ui/v;->V:I

    if-ltz v0, :cond_4

    .line 1148
    iget v0, p0, Lcom/jess/ui/v;->V:I

    iget v1, p0, Lcom/jess/ui/v;->H:I

    sub-int/2addr v0, v1

    invoke-virtual {p0, v0}, Lcom/jess/ui/v;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 1149
    iget-wide v2, p0, Lcom/jess/ui/v;->U:J

    iput-wide v2, p0, Lcom/jess/ui/v;->K:J

    .line 1150
    iget v1, p0, Lcom/jess/ui/v;->T:I

    iput v1, p0, Lcom/jess/ui/v;->J:I

    .line 1151
    if-eqz v0, :cond_0

    .line 1152
    iget-boolean v1, p0, Lcom/jess/ui/v;->G:Z

    if-eqz v1, :cond_3

    .line 1153
    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v0

    iput v0, p0, Lcom/jess/ui/v;->I:I

    .line 1158
    :cond_0
    :goto_1
    iput v4, p0, Lcom/jess/ui/v;->N:I

    .line 1179
    :cond_1
    :goto_2
    return-void

    .line 1144
    :cond_2
    iget v0, p0, Lcom/jess/ui/v;->b:I

    int-to-long v0, v0

    iput-wide v0, p0, Lcom/jess/ui/v;->L:J

    goto :goto_0

    .line 1155
    :cond_3
    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v0

    iput v0, p0, Lcom/jess/ui/v;->I:I

    goto :goto_1

    .line 1161
    :cond_4
    invoke-virtual {p0, v4}, Lcom/jess/ui/v;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 1162
    invoke-virtual {p0}, Lcom/jess/ui/v;->getAdapter()Landroid/widget/Adapter;

    move-result-object v1

    .line 1163
    iget v2, p0, Lcom/jess/ui/v;->H:I

    if-ltz v2, :cond_6

    iget v2, p0, Lcom/jess/ui/v;->H:I

    invoke-interface {v1}, Landroid/widget/Adapter;->getCount()I

    move-result v3

    if-ge v2, v3, :cond_6

    .line 1164
    iget v2, p0, Lcom/jess/ui/v;->H:I

    invoke-interface {v1, v2}, Landroid/widget/Adapter;->getItemId(I)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/jess/ui/v;->K:J

    .line 1168
    :goto_3
    iget v1, p0, Lcom/jess/ui/v;->H:I

    iput v1, p0, Lcom/jess/ui/v;->J:I

    .line 1169
    if-eqz v0, :cond_5

    .line 1170
    iget-boolean v1, p0, Lcom/jess/ui/v;->G:Z

    if-eqz v1, :cond_7

    .line 1171
    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v0

    iput v0, p0, Lcom/jess/ui/v;->I:I

    .line 1176
    :cond_5
    :goto_4
    iput v5, p0, Lcom/jess/ui/v;->N:I

    goto :goto_2

    .line 1166
    :cond_6
    const-wide/16 v2, -0x1

    iput-wide v2, p0, Lcom/jess/ui/v;->K:J

    goto :goto_3

    .line 1173
    :cond_7
    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v0

    iput v0, p0, Lcom/jess/ui/v;->I:I

    goto :goto_4
.end method

.method public removeAllViews()V
    .locals 2

    .prologue
    .line 542
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string/jumbo v1, "removeAllViews() is not supported in AdapterView"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public removeView(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 520
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string/jumbo v1, "removeView(View) is not supported in AdapterView"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public removeViewAt(I)V
    .locals 2

    .prologue
    .line 532
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string/jumbo v1, "removeViewAt(int) is not supported in AdapterView"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public abstract setAdapter(Landroid/widget/Adapter;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation
.end method

.method public setEmptyView(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 665
    iput-object p1, p0, Lcom/jess/ui/v;->c:Landroid/view/View;

    .line 667
    invoke-virtual {p0}, Lcom/jess/ui/v;->getAdapter()Landroid/widget/Adapter;

    move-result-object v0

    .line 668
    if-eqz v0, :cond_0

    invoke-interface {v0}, Landroid/widget/Adapter;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 669
    :goto_0
    invoke-direct {p0, v0}, Lcom/jess/ui/v;->a(Z)V

    .line 670
    return-void

    .line 668
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setFocusable(Z)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 695
    invoke-virtual {p0}, Lcom/jess/ui/v;->getAdapter()Landroid/widget/Adapter;

    move-result-object v0

    .line 696
    if-eqz v0, :cond_0

    invoke-interface {v0}, Landroid/widget/Adapter;->getCount()I

    move-result v0

    if-nez v0, :cond_3

    :cond_0
    move v0, v2

    .line 698
    :goto_0
    iput-boolean p1, p0, Lcom/jess/ui/v;->d:Z

    .line 699
    if-nez p1, :cond_1

    .line 700
    iput-boolean v1, p0, Lcom/jess/ui/v;->e:Z

    .line 703
    :cond_1
    if-eqz p1, :cond_4

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/jess/ui/v;->m()Z

    move-result v0

    if-eqz v0, :cond_4

    :cond_2
    :goto_1
    invoke-super {p0, v2}, Landroid/view/ViewGroup;->setFocusable(Z)V

    .line 704
    return-void

    :cond_3
    move v0, v1

    .line 696
    goto :goto_0

    :cond_4
    move v2, v1

    .line 703
    goto :goto_1
.end method

.method public setFocusableInTouchMode(Z)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 708
    invoke-virtual {p0}, Lcom/jess/ui/v;->getAdapter()Landroid/widget/Adapter;

    move-result-object v0

    .line 709
    if-eqz v0, :cond_0

    invoke-interface {v0}, Landroid/widget/Adapter;->getCount()I

    move-result v0

    if-nez v0, :cond_3

    :cond_0
    move v0, v2

    .line 711
    :goto_0
    iput-boolean p1, p0, Lcom/jess/ui/v;->e:Z

    .line 712
    if-eqz p1, :cond_1

    .line 713
    iput-boolean v2, p0, Lcom/jess/ui/v;->d:Z

    .line 716
    :cond_1
    if-eqz p1, :cond_4

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/jess/ui/v;->m()Z

    move-result v0

    if-eqz v0, :cond_4

    :cond_2
    :goto_1
    invoke-super {p0, v2}, Landroid/view/ViewGroup;->setFocusableInTouchMode(Z)V

    .line 717
    return-void

    :cond_3
    move v0, v1

    .line 709
    goto :goto_0

    :cond_4
    move v2, v1

    .line 716
    goto :goto_1
.end method

.method protected setIsVertical(Z)V
    .locals 0

    .prologue
    .line 1212
    iput-boolean p1, p0, Lcom/jess/ui/v;->G:Z

    .line 1213
    return-void
.end method

.method setNextSelectedPositionInt(I)V
    .locals 2

    .prologue
    .line 1124
    iput p1, p0, Lcom/jess/ui/v;->T:I

    .line 1125
    invoke-virtual {p0, p1}, Lcom/jess/ui/v;->d(I)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/jess/ui/v;->U:J

    .line 1127
    iget-boolean v0, p0, Lcom/jess/ui/v;->M:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/jess/ui/v;->N:I

    if-nez v0, :cond_0

    if-ltz p1, :cond_0

    .line 1128
    iput p1, p0, Lcom/jess/ui/v;->J:I

    .line 1129
    iget-wide v0, p0, Lcom/jess/ui/v;->U:J

    iput-wide v0, p0, Lcom/jess/ui/v;->K:J

    .line 1131
    :cond_0
    return-void
.end method

.method public setOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 2

    .prologue
    .line 782
    new-instance v0, Ljava/lang/RuntimeException;

    const-string/jumbo v1, "Don\'t call setOnClickListener for an AdapterView. You probably want setOnItemClickListener instead"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public setOnItemClickListener(Lcom/jess/ui/z;)V
    .locals 0

    .prologue
    .line 292
    iput-object p1, p0, Lcom/jess/ui/v;->Q:Lcom/jess/ui/z;

    .line 293
    return-void
.end method

.method public setOnItemLongClickListener(Lcom/jess/ui/aa;)V
    .locals 1

    .prologue
    .line 352
    invoke-virtual {p0}, Lcom/jess/ui/v;->isLongClickable()Z

    move-result v0

    if-nez v0, :cond_0

    .line 353
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/jess/ui/v;->setLongClickable(Z)V

    .line 355
    :cond_0
    iput-object p1, p0, Lcom/jess/ui/v;->R:Lcom/jess/ui/aa;

    .line 356
    return-void
.end method

.method public setOnItemSelectedListener(Lcom/jess/ui/ab;)V
    .locals 0

    .prologue
    .line 403
    iput-object p1, p0, Lcom/jess/ui/v;->P:Lcom/jess/ui/ab;

    .line 404
    return-void
.end method

.method setSelectedPositionInt(I)V
    .locals 2

    .prologue
    .line 1114
    iput p1, p0, Lcom/jess/ui/v;->V:I

    .line 1115
    invoke-virtual {p0, p1}, Lcom/jess/ui/v;->d(I)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/jess/ui/v;->W:J

    .line 1116
    return-void
.end method

.method public abstract setSelection(I)V
.end method
