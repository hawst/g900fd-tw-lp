.class public abstract Lcom/jess/ui/p;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field protected final c:Landroid/widget/Scroller;

.field final synthetic d:Lcom/jess/ui/n;


# direct methods
.method constructor <init>(Lcom/jess/ui/n;)V
    .locals 2

    .prologue
    .line 3677
    iput-object p1, p0, Lcom/jess/ui/p;->d:Lcom/jess/ui/n;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3678
    new-instance v0, Landroid/widget/Scroller;

    iget-object v1, p1, Lcom/jess/ui/n;->h:Lcom/jess/ui/TwoWayAbsListView;

    invoke-virtual {v1}, Lcom/jess/ui/TwoWayAbsListView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/Scroller;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/jess/ui/p;->c:Landroid/widget/Scroller;

    .line 3679
    return-void
.end method


# virtual methods
.method protected a()V
    .locals 2

    .prologue
    .line 3686
    iget-object v0, p0, Lcom/jess/ui/p;->d:Lcom/jess/ui/n;

    iget-object v0, v0, Lcom/jess/ui/n;->h:Lcom/jess/ui/TwoWayAbsListView;

    const/4 v1, -0x1

    iput v1, v0, Lcom/jess/ui/TwoWayAbsListView;->w:I

    .line 3688
    iget-object v0, p0, Lcom/jess/ui/p;->d:Lcom/jess/ui/n;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/jess/ui/n;->b(I)V

    .line 3689
    iget-object v0, p0, Lcom/jess/ui/p;->d:Lcom/jess/ui/n;

    invoke-virtual {v0}, Lcom/jess/ui/n;->c()V

    .line 3691
    iget-object v0, p0, Lcom/jess/ui/p;->d:Lcom/jess/ui/n;

    iget-object v0, v0, Lcom/jess/ui/n;->h:Lcom/jess/ui/TwoWayAbsListView;

    invoke-virtual {v0, p0}, Lcom/jess/ui/TwoWayAbsListView;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 3693
    iget-object v0, p0, Lcom/jess/ui/p;->d:Lcom/jess/ui/n;

    iget-object v0, v0, Lcom/jess/ui/n;->e:Lcom/jess/ui/q;

    if-eqz v0, :cond_0

    .line 3694
    iget-object v0, p0, Lcom/jess/ui/p;->d:Lcom/jess/ui/n;

    iget-object v0, v0, Lcom/jess/ui/n;->h:Lcom/jess/ui/TwoWayAbsListView;

    iget-object v1, p0, Lcom/jess/ui/p;->d:Lcom/jess/ui/n;

    iget-object v1, v1, Lcom/jess/ui/n;->e:Lcom/jess/ui/q;

    invoke-virtual {v0, v1}, Lcom/jess/ui/TwoWayAbsListView;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 3696
    :cond_0
    return-void
.end method

.method abstract a(I)V
.end method
