.class Lcom/jess/ui/y;
.super Landroid/database/DataSetObserver;


# instance fields
.field final synthetic a:Lcom/jess/ui/v;

.field private b:Landroid/os/Parcelable;


# direct methods
.method constructor <init>(Lcom/jess/ui/v;)V
    .locals 1

    .prologue
    .line 802
    iput-object p1, p0, Lcom/jess/ui/y;->a:Lcom/jess/ui/v;

    invoke-direct {p0}, Landroid/database/DataSetObserver;-><init>()V

    .line 804
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/jess/ui/y;->b:Landroid/os/Parcelable;

    return-void
.end method


# virtual methods
.method public onChanged()V
    .locals 2

    .prologue
    .line 808
    iget-object v0, p0, Lcom/jess/ui/y;->a:Lcom/jess/ui/v;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/jess/ui/v;->S:Z

    .line 809
    iget-object v0, p0, Lcom/jess/ui/y;->a:Lcom/jess/ui/v;

    iget-object v1, p0, Lcom/jess/ui/y;->a:Lcom/jess/ui/v;

    iget v1, v1, Lcom/jess/ui/v;->aa:I

    iput v1, v0, Lcom/jess/ui/v;->ab:I

    .line 810
    iget-object v0, p0, Lcom/jess/ui/y;->a:Lcom/jess/ui/v;

    iget-object v1, p0, Lcom/jess/ui/y;->a:Lcom/jess/ui/v;

    invoke-virtual {v1}, Lcom/jess/ui/v;->getAdapter()Landroid/widget/Adapter;

    move-result-object v1

    invoke-interface {v1}, Landroid/widget/Adapter;->getCount()I

    move-result v1

    iput v1, v0, Lcom/jess/ui/v;->aa:I

    .line 814
    iget-object v0, p0, Lcom/jess/ui/y;->a:Lcom/jess/ui/v;

    invoke-virtual {v0}, Lcom/jess/ui/v;->getAdapter()Landroid/widget/Adapter;

    move-result-object v0

    invoke-interface {v0}, Landroid/widget/Adapter;->hasStableIds()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/jess/ui/y;->b:Landroid/os/Parcelable;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/jess/ui/y;->a:Lcom/jess/ui/v;

    iget v0, v0, Lcom/jess/ui/v;->ab:I

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/jess/ui/y;->a:Lcom/jess/ui/v;

    iget v0, v0, Lcom/jess/ui/v;->aa:I

    if-lez v0, :cond_0

    .line 816
    iget-object v0, p0, Lcom/jess/ui/y;->a:Lcom/jess/ui/v;

    iget-object v1, p0, Lcom/jess/ui/y;->b:Landroid/os/Parcelable;

    invoke-static {v0, v1}, Lcom/jess/ui/v;->a(Lcom/jess/ui/v;Landroid/os/Parcelable;)V

    .line 817
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/jess/ui/y;->b:Landroid/os/Parcelable;

    .line 821
    :goto_0
    iget-object v0, p0, Lcom/jess/ui/y;->a:Lcom/jess/ui/v;

    invoke-virtual {v0}, Lcom/jess/ui/v;->n()V

    .line 822
    iget-object v0, p0, Lcom/jess/ui/y;->a:Lcom/jess/ui/v;

    invoke-virtual {v0}, Lcom/jess/ui/v;->requestLayout()V

    .line 823
    return-void

    .line 819
    :cond_0
    iget-object v0, p0, Lcom/jess/ui/y;->a:Lcom/jess/ui/v;

    invoke-virtual {v0}, Lcom/jess/ui/v;->r()V

    goto :goto_0
.end method

.method public onInvalidated()V
    .locals 6

    .prologue
    const-wide/high16 v4, -0x8000000000000000L

    const/4 v3, 0x0

    const/4 v2, -0x1

    .line 827
    iget-object v0, p0, Lcom/jess/ui/y;->a:Lcom/jess/ui/v;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/jess/ui/v;->S:Z

    .line 829
    iget-object v0, p0, Lcom/jess/ui/y;->a:Lcom/jess/ui/v;

    invoke-virtual {v0}, Lcom/jess/ui/v;->getAdapter()Landroid/widget/Adapter;

    move-result-object v0

    invoke-interface {v0}, Landroid/widget/Adapter;->hasStableIds()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 832
    iget-object v0, p0, Lcom/jess/ui/y;->a:Lcom/jess/ui/v;

    invoke-static {v0}, Lcom/jess/ui/v;->a(Lcom/jess/ui/v;)Landroid/os/Parcelable;

    move-result-object v0

    iput-object v0, p0, Lcom/jess/ui/y;->b:Landroid/os/Parcelable;

    .line 836
    :cond_0
    iget-object v0, p0, Lcom/jess/ui/y;->a:Lcom/jess/ui/v;

    iget-object v1, p0, Lcom/jess/ui/y;->a:Lcom/jess/ui/v;

    iget v1, v1, Lcom/jess/ui/v;->aa:I

    iput v1, v0, Lcom/jess/ui/v;->ab:I

    .line 837
    iget-object v0, p0, Lcom/jess/ui/y;->a:Lcom/jess/ui/v;

    iput v3, v0, Lcom/jess/ui/v;->aa:I

    .line 838
    iget-object v0, p0, Lcom/jess/ui/y;->a:Lcom/jess/ui/v;

    iput v2, v0, Lcom/jess/ui/v;->V:I

    .line 839
    iget-object v0, p0, Lcom/jess/ui/y;->a:Lcom/jess/ui/v;

    iput-wide v4, v0, Lcom/jess/ui/v;->W:J

    .line 840
    iget-object v0, p0, Lcom/jess/ui/y;->a:Lcom/jess/ui/v;

    iput v2, v0, Lcom/jess/ui/v;->T:I

    .line 841
    iget-object v0, p0, Lcom/jess/ui/y;->a:Lcom/jess/ui/v;

    iput-wide v4, v0, Lcom/jess/ui/v;->U:J

    .line 842
    iget-object v0, p0, Lcom/jess/ui/y;->a:Lcom/jess/ui/v;

    iput-boolean v3, v0, Lcom/jess/ui/v;->M:Z

    .line 843
    iget-object v0, p0, Lcom/jess/ui/y;->a:Lcom/jess/ui/v;

    invoke-virtual {v0}, Lcom/jess/ui/v;->p()V

    .line 845
    iget-object v0, p0, Lcom/jess/ui/y;->a:Lcom/jess/ui/v;

    invoke-virtual {v0}, Lcom/jess/ui/v;->n()V

    .line 846
    iget-object v0, p0, Lcom/jess/ui/y;->a:Lcom/jess/ui/v;

    invoke-virtual {v0}, Lcom/jess/ui/v;->requestLayout()V

    .line 847
    return-void
.end method
