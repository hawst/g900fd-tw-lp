.class Lcom/jess/ui/j;
.super Lcom/jess/ui/u;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field a:Landroid/view/View;

.field b:I

.field final synthetic c:Lcom/jess/ui/TwoWayAbsListView;


# direct methods
.method private constructor <init>(Lcom/jess/ui/TwoWayAbsListView;)V
    .locals 1

    .prologue
    .line 1850
    iput-object p1, p0, Lcom/jess/ui/j;->c:Lcom/jess/ui/TwoWayAbsListView;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/jess/ui/u;-><init>(Lcom/jess/ui/TwoWayAbsListView;Lcom/jess/ui/a;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/jess/ui/TwoWayAbsListView;Lcom/jess/ui/a;)V
    .locals 0

    .prologue
    .line 1850
    invoke-direct {p0, p1}, Lcom/jess/ui/j;-><init>(Lcom/jess/ui/TwoWayAbsListView;)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    .line 1857
    iget-object v0, p0, Lcom/jess/ui/j;->c:Lcom/jess/ui/TwoWayAbsListView;

    iget-boolean v0, v0, Lcom/jess/ui/TwoWayAbsListView;->S:Z

    if-eqz v0, :cond_1

    .line 1866
    :cond_0
    :goto_0
    return-void

    .line 1859
    :cond_1
    iget-object v0, p0, Lcom/jess/ui/j;->c:Lcom/jess/ui/TwoWayAbsListView;

    iget-object v0, v0, Lcom/jess/ui/TwoWayAbsListView;->c:Landroid/widget/ListAdapter;

    .line 1860
    iget v1, p0, Lcom/jess/ui/j;->b:I

    .line 1861
    if-eqz v0, :cond_0

    iget-object v2, p0, Lcom/jess/ui/j;->c:Lcom/jess/ui/TwoWayAbsListView;

    iget v2, v2, Lcom/jess/ui/TwoWayAbsListView;->aa:I

    if-lez v2, :cond_0

    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    .line 1863
    invoke-interface {v0}, Landroid/widget/ListAdapter;->getCount()I

    move-result v2

    if-ge v1, v2, :cond_0

    invoke-virtual {p0}, Lcom/jess/ui/j;->b()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1864
    iget-object v2, p0, Lcom/jess/ui/j;->c:Lcom/jess/ui/TwoWayAbsListView;

    iget-object v3, p0, Lcom/jess/ui/j;->a:Landroid/view/View;

    invoke-interface {v0, v1}, Landroid/widget/ListAdapter;->getItemId(I)J

    move-result-wide v4

    invoke-virtual {v2, v3, v1, v4, v5}, Lcom/jess/ui/TwoWayAbsListView;->b(Landroid/view/View;IJ)Z

    goto :goto_0
.end method
