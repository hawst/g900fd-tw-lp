.class Lcom/jess/ui/g;
.super Lcom/jess/ui/p;


# instance fields
.field protected a:I

.field final synthetic b:Lcom/jess/ui/e;


# direct methods
.method private constructor <init>(Lcom/jess/ui/e;)V
    .locals 0

    .prologue
    .line 5303
    iput-object p1, p0, Lcom/jess/ui/g;->b:Lcom/jess/ui/e;

    invoke-direct {p0, p1}, Lcom/jess/ui/p;-><init>(Lcom/jess/ui/n;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/jess/ui/e;Lcom/jess/ui/a;)V
    .locals 0

    .prologue
    .line 5303
    invoke-direct {p0, p1}, Lcom/jess/ui/g;-><init>(Lcom/jess/ui/e;)V

    return-void
.end method


# virtual methods
.method a(I)V
    .locals 9

    .prologue
    const v6, 0x7fffffff

    const/4 v2, 0x0

    .line 5311
    if-gez p1, :cond_0

    move v1, v6

    .line 5312
    :goto_0
    iput v1, p0, Lcom/jess/ui/g;->a:I

    .line 5313
    iget-object v0, p0, Lcom/jess/ui/g;->c:Landroid/widget/Scroller;

    move v3, p1

    move v4, v2

    move v5, v2

    move v7, v2

    move v8, v6

    invoke-virtual/range {v0 .. v8}, Landroid/widget/Scroller;->fling(IIIIIIII)V

    .line 5315
    iget-object v0, p0, Lcom/jess/ui/g;->b:Lcom/jess/ui/e;

    iget-object v0, v0, Lcom/jess/ui/e;->d:Lcom/jess/ui/TwoWayAbsListView;

    const/4 v1, 0x4

    iput v1, v0, Lcom/jess/ui/TwoWayAbsListView;->w:I

    .line 5316
    iget-object v0, p0, Lcom/jess/ui/g;->b:Lcom/jess/ui/e;

    iget-object v0, v0, Lcom/jess/ui/e;->d:Lcom/jess/ui/TwoWayAbsListView;

    invoke-virtual {v0, p0}, Lcom/jess/ui/TwoWayAbsListView;->post(Ljava/lang/Runnable;)Z

    .line 5324
    return-void

    :cond_0
    move v1, v2

    .line 5311
    goto :goto_0
.end method

.method public run()V
    .locals 6

    .prologue
    .line 5337
    iget-object v0, p0, Lcom/jess/ui/g;->b:Lcom/jess/ui/e;

    iget-object v0, v0, Lcom/jess/ui/e;->d:Lcom/jess/ui/TwoWayAbsListView;

    iget v0, v0, Lcom/jess/ui/TwoWayAbsListView;->w:I

    packed-switch v0, :pswitch_data_0

    .line 5396
    :goto_0
    return-void

    .line 5342
    :pswitch_0
    iget-object v0, p0, Lcom/jess/ui/g;->b:Lcom/jess/ui/e;

    iget-object v0, v0, Lcom/jess/ui/e;->d:Lcom/jess/ui/TwoWayAbsListView;

    iget v0, v0, Lcom/jess/ui/TwoWayAbsListView;->aa:I

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/jess/ui/g;->b:Lcom/jess/ui/e;

    iget-object v0, v0, Lcom/jess/ui/e;->d:Lcom/jess/ui/TwoWayAbsListView;

    invoke-virtual {v0}, Lcom/jess/ui/TwoWayAbsListView;->getChildCount()I

    move-result v0

    if-nez v0, :cond_1

    .line 5343
    :cond_0
    invoke-virtual {p0}, Lcom/jess/ui/g;->a()V

    goto :goto_0

    .line 5347
    :cond_1
    iget-object v0, p0, Lcom/jess/ui/g;->c:Landroid/widget/Scroller;

    .line 5348
    invoke-virtual {v0}, Landroid/widget/Scroller;->computeScrollOffset()Z

    move-result v1

    .line 5349
    invoke-virtual {v0}, Landroid/widget/Scroller;->getCurrX()I

    move-result v2

    .line 5353
    iget v0, p0, Lcom/jess/ui/g;->a:I

    sub-int/2addr v0, v2

    .line 5356
    if-lez v0, :cond_2

    .line 5358
    iget-object v3, p0, Lcom/jess/ui/g;->b:Lcom/jess/ui/e;

    iget-object v3, v3, Lcom/jess/ui/e;->d:Lcom/jess/ui/TwoWayAbsListView;

    iget-object v4, p0, Lcom/jess/ui/g;->b:Lcom/jess/ui/e;

    iget-object v4, v4, Lcom/jess/ui/e;->d:Lcom/jess/ui/TwoWayAbsListView;

    iget v4, v4, Lcom/jess/ui/TwoWayAbsListView;->H:I

    iput v4, v3, Lcom/jess/ui/TwoWayAbsListView;->t:I

    .line 5359
    iget-object v3, p0, Lcom/jess/ui/g;->b:Lcom/jess/ui/e;

    iget-object v3, v3, Lcom/jess/ui/e;->d:Lcom/jess/ui/TwoWayAbsListView;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Lcom/jess/ui/TwoWayAbsListView;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 5360
    iget-object v4, p0, Lcom/jess/ui/g;->b:Lcom/jess/ui/e;

    invoke-virtual {v3}, Landroid/view/View;->getLeft()I

    move-result v3

    iput v3, v4, Lcom/jess/ui/e;->a:I

    .line 5363
    iget-object v3, p0, Lcom/jess/ui/g;->b:Lcom/jess/ui/e;

    iget-object v3, v3, Lcom/jess/ui/e;->d:Lcom/jess/ui/TwoWayAbsListView;

    invoke-virtual {v3}, Lcom/jess/ui/TwoWayAbsListView;->getWidth()I

    move-result v3

    iget-object v4, p0, Lcom/jess/ui/g;->b:Lcom/jess/ui/e;

    iget-object v4, v4, Lcom/jess/ui/e;->d:Lcom/jess/ui/TwoWayAbsListView;

    invoke-virtual {v4}, Lcom/jess/ui/TwoWayAbsListView;->getPaddingRight()I

    move-result v4

    sub-int/2addr v3, v4

    iget-object v4, p0, Lcom/jess/ui/g;->b:Lcom/jess/ui/e;

    iget-object v4, v4, Lcom/jess/ui/e;->d:Lcom/jess/ui/TwoWayAbsListView;

    invoke-virtual {v4}, Lcom/jess/ui/TwoWayAbsListView;->getPaddingLeft()I

    move-result v4

    sub-int/2addr v3, v4

    add-int/lit8 v3, v3, -0x1

    invoke-static {v3, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 5376
    :goto_1
    iget-object v3, p0, Lcom/jess/ui/g;->b:Lcom/jess/ui/e;

    invoke-virtual {v3, v0, v0}, Lcom/jess/ui/e;->a(II)Z

    move-result v0

    .line 5378
    if-eqz v1, :cond_3

    if-nez v0, :cond_3

    .line 5379
    iget-object v0, p0, Lcom/jess/ui/g;->b:Lcom/jess/ui/e;

    iget-object v0, v0, Lcom/jess/ui/e;->d:Lcom/jess/ui/TwoWayAbsListView;

    invoke-virtual {v0}, Lcom/jess/ui/TwoWayAbsListView;->invalidate()V

    .line 5380
    iput v2, p0, Lcom/jess/ui/g;->a:I

    .line 5381
    iget-object v0, p0, Lcom/jess/ui/g;->b:Lcom/jess/ui/e;

    iget-object v0, v0, Lcom/jess/ui/e;->d:Lcom/jess/ui/TwoWayAbsListView;

    invoke-virtual {v0, p0}, Lcom/jess/ui/TwoWayAbsListView;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    .line 5366
    :cond_2
    iget-object v3, p0, Lcom/jess/ui/g;->b:Lcom/jess/ui/e;

    iget-object v3, v3, Lcom/jess/ui/e;->d:Lcom/jess/ui/TwoWayAbsListView;

    invoke-virtual {v3}, Lcom/jess/ui/TwoWayAbsListView;->getChildCount()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    .line 5367
    iget-object v4, p0, Lcom/jess/ui/g;->b:Lcom/jess/ui/e;

    iget-object v4, v4, Lcom/jess/ui/e;->d:Lcom/jess/ui/TwoWayAbsListView;

    iget-object v5, p0, Lcom/jess/ui/g;->b:Lcom/jess/ui/e;

    iget-object v5, v5, Lcom/jess/ui/e;->d:Lcom/jess/ui/TwoWayAbsListView;

    iget v5, v5, Lcom/jess/ui/TwoWayAbsListView;->H:I

    add-int/2addr v5, v3

    iput v5, v4, Lcom/jess/ui/TwoWayAbsListView;->t:I

    .line 5369
    iget-object v4, p0, Lcom/jess/ui/g;->b:Lcom/jess/ui/e;

    iget-object v4, v4, Lcom/jess/ui/e;->d:Lcom/jess/ui/TwoWayAbsListView;

    invoke-virtual {v4, v3}, Lcom/jess/ui/TwoWayAbsListView;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 5370
    iget-object v4, p0, Lcom/jess/ui/g;->b:Lcom/jess/ui/e;

    invoke-virtual {v3}, Landroid/view/View;->getLeft()I

    move-result v3

    iput v3, v4, Lcom/jess/ui/e;->a:I

    .line 5373
    iget-object v3, p0, Lcom/jess/ui/g;->b:Lcom/jess/ui/e;

    iget-object v3, v3, Lcom/jess/ui/e;->d:Lcom/jess/ui/TwoWayAbsListView;

    invoke-virtual {v3}, Lcom/jess/ui/TwoWayAbsListView;->getWidth()I

    move-result v3

    iget-object v4, p0, Lcom/jess/ui/g;->b:Lcom/jess/ui/e;

    iget-object v4, v4, Lcom/jess/ui/e;->d:Lcom/jess/ui/TwoWayAbsListView;

    invoke-virtual {v4}, Lcom/jess/ui/TwoWayAbsListView;->getPaddingRight()I

    move-result v4

    sub-int/2addr v3, v4

    iget-object v4, p0, Lcom/jess/ui/g;->b:Lcom/jess/ui/e;

    iget-object v4, v4, Lcom/jess/ui/e;->d:Lcom/jess/ui/TwoWayAbsListView;

    invoke-virtual {v4}, Lcom/jess/ui/TwoWayAbsListView;->getPaddingLeft()I

    move-result v4

    sub-int/2addr v3, v4

    add-int/lit8 v3, v3, -0x1

    neg-int v3, v3

    invoke-static {v3, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    goto :goto_1

    .line 5383
    :cond_3
    invoke-virtual {p0}, Lcom/jess/ui/g;->a()V

    goto/16 :goto_0

    .line 5337
    nop

    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_0
    .end packed-switch
.end method
