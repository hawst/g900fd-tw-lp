.class Lcom/jess/ui/k;
.super Ljava/lang/Object;


# instance fields
.field final synthetic a:Lcom/jess/ui/TwoWayAbsListView;

.field private b:Lcom/jess/ui/l;

.field private c:I

.field private d:[Landroid/view/View;

.field private e:[Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private f:I

.field private g:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/jess/ui/TwoWayAbsListView;)V
    .locals 1

    .prologue
    .line 3128
    iput-object p1, p0, Lcom/jess/ui/k;->a:Lcom/jess/ui/TwoWayAbsListView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3142
    const/4 v0, 0x0

    new-array v0, v0, [Landroid/view/View;

    iput-object v0, p0, Lcom/jess/ui/k;->d:[Landroid/view/View;

    return-void
.end method

.method static synthetic a(Lcom/jess/ui/k;Lcom/jess/ui/l;)Lcom/jess/ui/l;
    .locals 0

    .prologue
    .line 3128
    iput-object p1, p0, Lcom/jess/ui/k;->b:Lcom/jess/ui/l;

    return-object p1
.end method

.method private d()V
    .locals 11

    .prologue
    const/4 v2, 0x0

    .line 3367
    iget-object v0, p0, Lcom/jess/ui/k;->d:[Landroid/view/View;

    array-length v5, v0

    .line 3368
    iget v6, p0, Lcom/jess/ui/k;->f:I

    .line 3369
    iget-object v7, p0, Lcom/jess/ui/k;->e:[Ljava/util/ArrayList;

    move v4, v2

    .line 3370
    :goto_0
    if-ge v4, v6, :cond_1

    .line 3371
    aget-object v8, v7, v4

    .line 3372
    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 3373
    sub-int v9, v0, v5

    .line 3374
    add-int/lit8 v0, v0, -0x1

    move v1, v2

    .line 3375
    :goto_1
    if-ge v1, v9, :cond_0

    .line 3376
    iget-object v10, p0, Lcom/jess/ui/k;->a:Lcom/jess/ui/TwoWayAbsListView;

    add-int/lit8 v3, v0, -0x1

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-static {v10, v0, v2}, Lcom/jess/ui/TwoWayAbsListView;->e(Lcom/jess/ui/TwoWayAbsListView;Landroid/view/View;Z)V

    .line 3375
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    move v0, v3

    goto :goto_1

    .line 3370
    :cond_0
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_0

    .line 3379
    :cond_1
    return-void
.end method


# virtual methods
.method public a()V
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 3168
    iget v0, p0, Lcom/jess/ui/k;->f:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 3169
    iget-object v1, p0, Lcom/jess/ui/k;->g:Ljava/util/ArrayList;

    .line 3170
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 3171
    :goto_0
    if-ge v2, v3, :cond_2

    .line 3172
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->forceLayout()V

    .line 3171
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 3175
    :cond_0
    iget v4, p0, Lcom/jess/ui/k;->f:I

    move v3, v2

    .line 3176
    :goto_1
    if-ge v3, v4, :cond_2

    .line 3177
    iget-object v0, p0, Lcom/jess/ui/k;->e:[Ljava/util/ArrayList;

    aget-object v5, v0, v3

    .line 3178
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v6

    move v1, v2

    .line 3179
    :goto_2
    if-ge v1, v6, :cond_1

    .line 3180
    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->forceLayout()V

    .line 3179
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 3176
    :cond_1
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    .line 3184
    :cond_2
    return-void
.end method

.method public a(I)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 3154
    const/4 v0, 0x1

    if-ge p1, v0, :cond_0

    .line 3155
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "Can\'t have a viewTypeCount < 1"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 3158
    :cond_0
    new-array v2, p1, [Ljava/util/ArrayList;

    move v0, v1

    .line 3159
    :goto_0
    if-ge v0, p1, :cond_1

    .line 3160
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    aput-object v3, v2, v0

    .line 3159
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 3162
    :cond_1
    iput p1, p0, Lcom/jess/ui/k;->f:I

    .line 3163
    aget-object v0, v2, v1

    iput-object v0, p0, Lcom/jess/ui/k;->g:Ljava/util/ArrayList;

    .line 3164
    iput-object v2, p0, Lcom/jess/ui/k;->e:[Ljava/util/ArrayList;

    .line 3165
    return-void
.end method

.method a(II)V
    .locals 5

    .prologue
    .line 3220
    iget-object v0, p0, Lcom/jess/ui/k;->d:[Landroid/view/View;

    array-length v0, v0

    if-ge v0, p1, :cond_0

    .line 3221
    new-array v0, p1, [Landroid/view/View;

    iput-object v0, p0, Lcom/jess/ui/k;->d:[Landroid/view/View;

    .line 3223
    :cond_0
    iput p2, p0, Lcom/jess/ui/k;->c:I

    .line 3225
    iget-object v2, p0, Lcom/jess/ui/k;->d:[Landroid/view/View;

    .line 3226
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, p1, :cond_2

    .line 3227
    iget-object v0, p0, Lcom/jess/ui/k;->a:Lcom/jess/ui/TwoWayAbsListView;

    invoke-virtual {v0, v1}, Lcom/jess/ui/TwoWayAbsListView;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 3228
    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/jess/ui/h;

    .line 3230
    if-eqz v0, :cond_1

    iget v0, v0, Lcom/jess/ui/h;->a:I

    const/4 v4, -0x2

    if-eq v0, v4, :cond_1

    .line 3233
    aput-object v3, v2, v1

    .line 3226
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 3236
    :cond_2
    return-void
.end method

.method a(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 3288
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/jess/ui/h;

    .line 3289
    if-nez v0, :cond_1

    .line 3314
    :cond_0
    :goto_0
    return-void

    .line 3295
    :cond_1
    iget v0, v0, Lcom/jess/ui/h;->a:I

    .line 3296
    invoke-virtual {p0, v0}, Lcom/jess/ui/k;->b(I)Z

    move-result v1

    if-nez v1, :cond_2

    .line 3297
    const/4 v1, -0x2

    if-eq v0, v1, :cond_0

    .line 3298
    iget-object v0, p0, Lcom/jess/ui/k;->a:Lcom/jess/ui/TwoWayAbsListView;

    const/4 v1, 0x0

    invoke-static {v0, p1, v1}, Lcom/jess/ui/TwoWayAbsListView;->c(Lcom/jess/ui/TwoWayAbsListView;Landroid/view/View;Z)V

    goto :goto_0

    .line 3303
    :cond_2
    iget v1, p0, Lcom/jess/ui/k;->f:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_3

    .line 3304
    invoke-virtual {p1}, Landroid/view/View;->onStartTemporaryDetach()V

    .line 3305
    iget-object v0, p0, Lcom/jess/ui/k;->g:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3311
    :goto_1
    iget-object v0, p0, Lcom/jess/ui/k;->b:Lcom/jess/ui/l;

    if-eqz v0, :cond_0

    .line 3312
    iget-object v0, p0, Lcom/jess/ui/k;->b:Lcom/jess/ui/l;

    invoke-interface {v0, p1}, Lcom/jess/ui/l;->a(Landroid/view/View;)V

    goto :goto_0

    .line 3307
    :cond_3
    invoke-virtual {p1}, Landroid/view/View;->onStartTemporaryDetach()V

    .line 3308
    iget-object v1, p0, Lcom/jess/ui/k;->e:[Ljava/util/ArrayList;

    aget-object v0, v1, v0

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method

.method b()V
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 3194
    iget v0, p0, Lcom/jess/ui/k;->f:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 3195
    iget-object v3, p0, Lcom/jess/ui/k;->g:Ljava/util/ArrayList;

    .line 3196
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v4

    move v1, v2

    .line 3197
    :goto_0
    if-ge v1, v4, :cond_2

    .line 3198
    iget-object v5, p0, Lcom/jess/ui/k;->a:Lcom/jess/ui/TwoWayAbsListView;

    add-int/lit8 v0, v4, -0x1

    sub-int/2addr v0, v1

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-static {v5, v0, v2}, Lcom/jess/ui/TwoWayAbsListView;->a(Lcom/jess/ui/TwoWayAbsListView;Landroid/view/View;Z)V

    .line 3197
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 3201
    :cond_0
    iget v4, p0, Lcom/jess/ui/k;->f:I

    move v3, v2

    .line 3202
    :goto_1
    if-ge v3, v4, :cond_2

    .line 3203
    iget-object v0, p0, Lcom/jess/ui/k;->e:[Ljava/util/ArrayList;

    aget-object v5, v0, v3

    .line 3204
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v6

    move v1, v2

    .line 3205
    :goto_2
    if-ge v1, v6, :cond_1

    .line 3206
    iget-object v7, p0, Lcom/jess/ui/k;->a:Lcom/jess/ui/TwoWayAbsListView;

    add-int/lit8 v0, v6, -0x1

    sub-int/2addr v0, v1

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-static {v7, v0, v2}, Lcom/jess/ui/TwoWayAbsListView;->b(Lcom/jess/ui/TwoWayAbsListView;Landroid/view/View;Z)V

    .line 3205
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 3202
    :cond_1
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    .line 3210
    :cond_2
    return-void
.end method

.method public b(I)Z
    .locals 1

    .prologue
    .line 3187
    if-ltz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method c(I)Landroid/view/View;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 3246
    iget v0, p0, Lcom/jess/ui/k;->c:I

    sub-int v2, p1, v0

    .line 3247
    iget-object v3, p0, Lcom/jess/ui/k;->d:[Landroid/view/View;

    .line 3248
    if-ltz v2, :cond_0

    array-length v0, v3

    if-ge v2, v0, :cond_0

    .line 3249
    aget-object v0, v3, v2

    .line 3250
    aput-object v1, v3, v2

    .line 3253
    :goto_0
    return-object v0

    :cond_0
    move-object v0, v1

    goto :goto_0
.end method

.method c()V
    .locals 9

    .prologue
    const/4 v0, 0x1

    const/4 v2, 0x0

    .line 3320
    iget-object v6, p0, Lcom/jess/ui/k;->d:[Landroid/view/View;

    .line 3321
    iget-object v1, p0, Lcom/jess/ui/k;->b:Lcom/jess/ui/l;

    if-eqz v1, :cond_1

    move v1, v0

    .line 3322
    :goto_0
    iget v3, p0, Lcom/jess/ui/k;->f:I

    if-le v3, v0, :cond_2

    move v3, v0

    .line 3324
    :goto_1
    iget-object v4, p0, Lcom/jess/ui/k;->g:Ljava/util/ArrayList;

    .line 3325
    array-length v0, v6

    .line 3326
    add-int/lit8 v0, v0, -0x1

    move v5, v0

    :goto_2
    if-ltz v5, :cond_5

    .line 3327
    aget-object v7, v6, v5

    .line 3328
    if-eqz v7, :cond_0

    .line 3329
    invoke-virtual {v7}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/jess/ui/h;

    iget v0, v0, Lcom/jess/ui/h;->a:I

    .line 3331
    const/4 v8, 0x0

    aput-object v8, v6, v5

    .line 3333
    invoke-virtual {p0, v0}, Lcom/jess/ui/k;->b(I)Z

    move-result v8

    if-nez v8, :cond_3

    .line 3335
    const/4 v8, -0x2

    if-eq v0, v8, :cond_0

    .line 3336
    iget-object v0, p0, Lcom/jess/ui/k;->a:Lcom/jess/ui/TwoWayAbsListView;

    invoke-static {v0, v7, v2}, Lcom/jess/ui/TwoWayAbsListView;->d(Lcom/jess/ui/TwoWayAbsListView;Landroid/view/View;Z)V

    .line 3326
    :cond_0
    :goto_3
    add-int/lit8 v0, v5, -0x1

    move v5, v0

    goto :goto_2

    :cond_1
    move v1, v2

    .line 3321
    goto :goto_0

    :cond_2
    move v3, v2

    .line 3322
    goto :goto_1

    .line 3341
    :cond_3
    if-eqz v3, :cond_6

    .line 3342
    iget-object v4, p0, Lcom/jess/ui/k;->e:[Ljava/util/ArrayList;

    aget-object v0, v4, v0

    .line 3344
    :goto_4
    invoke-virtual {v7}, Landroid/view/View;->onStartTemporaryDetach()V

    .line 3345
    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3347
    if-eqz v1, :cond_4

    .line 3348
    iget-object v4, p0, Lcom/jess/ui/k;->b:Lcom/jess/ui/l;

    invoke-interface {v4, v7}, Lcom/jess/ui/l;->a(Landroid/view/View;)V

    :cond_4
    move-object v4, v0

    goto :goto_3

    .line 3359
    :cond_5
    invoke-direct {p0}, Lcom/jess/ui/k;->d()V

    .line 3360
    return-void

    :cond_6
    move-object v0, v4

    goto :goto_4
.end method

.method d(I)Landroid/view/View;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 3261
    iget v1, p0, Lcom/jess/ui/k;->f:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    .line 3262
    iget-object v1, p0, Lcom/jess/ui/k;->g:Ljava/util/ArrayList;

    .line 3263
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 3264
    if-lez v2, :cond_0

    .line 3265
    add-int/lit8 v0, v2, -0x1

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 3279
    :cond_0
    :goto_0
    return-object v0

    .line 3270
    :cond_1
    iget-object v1, p0, Lcom/jess/ui/k;->a:Lcom/jess/ui/TwoWayAbsListView;

    iget-object v1, v1, Lcom/jess/ui/TwoWayAbsListView;->c:Landroid/widget/ListAdapter;

    invoke-interface {v1, p1}, Landroid/widget/ListAdapter;->getItemViewType(I)I

    move-result v1

    .line 3271
    if-ltz v1, :cond_0

    iget-object v2, p0, Lcom/jess/ui/k;->e:[Ljava/util/ArrayList;

    array-length v2, v2

    if-ge v1, v2, :cond_0

    .line 3272
    iget-object v2, p0, Lcom/jess/ui/k;->e:[Ljava/util/ArrayList;

    aget-object v1, v2, v1

    .line 3273
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 3274
    if-lez v2, :cond_0

    .line 3275
    add-int/lit8 v0, v2, -0x1

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    goto :goto_0
.end method

.method e(I)V
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 3403
    iget v0, p0, Lcom/jess/ui/k;->f:I

    const/4 v2, 0x1

    if-ne v0, v2, :cond_0

    .line 3404
    iget-object v3, p0, Lcom/jess/ui/k;->g:Ljava/util/ArrayList;

    .line 3405
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v4

    move v2, v1

    .line 3406
    :goto_0
    if-ge v2, v4, :cond_2

    .line 3407
    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setDrawingCacheBackgroundColor(I)V

    .line 3406
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 3410
    :cond_0
    iget v4, p0, Lcom/jess/ui/k;->f:I

    move v3, v1

    .line 3411
    :goto_1
    if-ge v3, v4, :cond_2

    .line 3412
    iget-object v0, p0, Lcom/jess/ui/k;->e:[Ljava/util/ArrayList;

    aget-object v5, v0, v3

    .line 3413
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v6

    move v2, v1

    .line 3414
    :goto_2
    if-ge v2, v6, :cond_1

    .line 3415
    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setDrawingCacheBackgroundColor(I)V

    .line 3414
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    .line 3411
    :cond_1
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    .line 3420
    :cond_2
    iget-object v2, p0, Lcom/jess/ui/k;->d:[Landroid/view/View;

    .line 3421
    array-length v3, v2

    move v0, v1

    .line 3422
    :goto_3
    if-ge v0, v3, :cond_4

    .line 3423
    aget-object v1, v2, v0

    .line 3424
    if-eqz v1, :cond_3

    .line 3425
    invoke-virtual {v1, p1}, Landroid/view/View;->setDrawingCacheBackgroundColor(I)V

    .line 3422
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 3428
    :cond_4
    return-void
.end method
