.class Lcom/jess/ui/e;
.super Lcom/jess/ui/n;


# instance fields
.field a:I

.field b:I

.field c:I

.field final synthetic d:Lcom/jess/ui/TwoWayAbsListView;


# direct methods
.method constructor <init>(Lcom/jess/ui/TwoWayAbsListView;)V
    .locals 0

    .prologue
    .line 4689
    iput-object p1, p0, Lcom/jess/ui/e;->d:Lcom/jess/ui/TwoWayAbsListView;

    invoke-direct {p0, p1}, Lcom/jess/ui/n;-><init>(Lcom/jess/ui/TwoWayAbsListView;)V

    .line 5400
    return-void
.end method


# virtual methods
.method a()Z
    .locals 14

    .prologue
    const/4 v3, 0x1

    const/4 v6, -0x1

    const/4 v4, 0x0

    .line 5060
    iget-object v0, p0, Lcom/jess/ui/e;->d:Lcom/jess/ui/TwoWayAbsListView;

    invoke-virtual {v0}, Lcom/jess/ui/TwoWayAbsListView;->getChildCount()I

    move-result v9

    .line 5062
    if-gtz v9, :cond_0

    .line 5154
    :goto_0
    return v4

    .line 5068
    :cond_0
    iget-object v0, p0, Lcom/jess/ui/e;->d:Lcom/jess/ui/TwoWayAbsListView;

    iget-object v0, v0, Lcom/jess/ui/TwoWayAbsListView;->m:Landroid/graphics/Rect;

    iget v5, v0, Landroid/graphics/Rect;->top:I

    .line 5069
    iget-object v0, p0, Lcom/jess/ui/e;->d:Lcom/jess/ui/TwoWayAbsListView;

    invoke-virtual {v0}, Lcom/jess/ui/TwoWayAbsListView;->getRight()I

    move-result v0

    iget-object v1, p0, Lcom/jess/ui/e;->d:Lcom/jess/ui/TwoWayAbsListView;

    invoke-virtual {v1}, Lcom/jess/ui/TwoWayAbsListView;->getLeft()I

    move-result v1

    sub-int/2addr v0, v1

    iget-object v1, p0, Lcom/jess/ui/e;->d:Lcom/jess/ui/TwoWayAbsListView;

    iget-object v1, v1, Lcom/jess/ui/TwoWayAbsListView;->m:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->right:I

    sub-int v7, v0, v1

    .line 5070
    iget-object v0, p0, Lcom/jess/ui/e;->d:Lcom/jess/ui/TwoWayAbsListView;

    iget v1, v0, Lcom/jess/ui/TwoWayAbsListView;->H:I

    .line 5071
    iget-object v0, p0, Lcom/jess/ui/e;->d:Lcom/jess/ui/TwoWayAbsListView;

    iget v2, v0, Lcom/jess/ui/TwoWayAbsListView;->A:I

    .line 5074
    if-lt v2, v1, :cond_3

    add-int v0, v1, v9

    if-ge v2, v0, :cond_3

    .line 5077
    iget-object v0, p0, Lcom/jess/ui/e;->d:Lcom/jess/ui/TwoWayAbsListView;

    iget-object v8, p0, Lcom/jess/ui/e;->d:Lcom/jess/ui/TwoWayAbsListView;

    iget v8, v8, Lcom/jess/ui/TwoWayAbsListView;->H:I

    sub-int v8, v2, v8

    invoke-virtual {v0, v8}, Lcom/jess/ui/TwoWayAbsListView;->getChildAt(I)Landroid/view/View;

    move-result-object v8

    .line 5078
    invoke-virtual {v8}, Landroid/view/View;->getLeft()I

    move-result v0

    .line 5079
    invoke-virtual {v8}, Landroid/view/View;->getRight()I

    move-result v9

    .line 5082
    if-ge v0, v5, :cond_2

    .line 5083
    iget-object v0, p0, Lcom/jess/ui/e;->d:Lcom/jess/ui/TwoWayAbsListView;

    invoke-virtual {v0}, Lcom/jess/ui/TwoWayAbsListView;->getHorizontalFadingEdgeLength()I

    move-result v0

    add-int/2addr v0, v5

    :cond_1
    :goto_1
    move v5, v0

    move v0, v3

    .line 5139
    :goto_2
    iget-object v7, p0, Lcom/jess/ui/e;->d:Lcom/jess/ui/TwoWayAbsListView;

    iput v6, v7, Lcom/jess/ui/TwoWayAbsListView;->A:I

    .line 5140
    iget-object v7, p0, Lcom/jess/ui/e;->d:Lcom/jess/ui/TwoWayAbsListView;

    iget-object v8, p0, Lcom/jess/ui/e;->f:Lcom/jess/ui/p;

    invoke-virtual {v7, v8}, Lcom/jess/ui/TwoWayAbsListView;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 5141
    iget-object v7, p0, Lcom/jess/ui/e;->d:Lcom/jess/ui/TwoWayAbsListView;

    iput v6, v7, Lcom/jess/ui/TwoWayAbsListView;->w:I

    .line 5142
    invoke-virtual {p0}, Lcom/jess/ui/e;->c()V

    .line 5143
    iget-object v7, p0, Lcom/jess/ui/e;->d:Lcom/jess/ui/TwoWayAbsListView;

    iput v5, v7, Lcom/jess/ui/TwoWayAbsListView;->I:I

    .line 5144
    iget-object v5, p0, Lcom/jess/ui/e;->d:Lcom/jess/ui/TwoWayAbsListView;

    invoke-virtual {v5, v2, v0}, Lcom/jess/ui/TwoWayAbsListView;->a(IZ)I

    move-result v0

    .line 5145
    if-lt v0, v1, :cond_9

    iget-object v1, p0, Lcom/jess/ui/e;->d:Lcom/jess/ui/TwoWayAbsListView;

    invoke-virtual {v1}, Lcom/jess/ui/TwoWayAbsListView;->getLastVisiblePosition()I

    move-result v1

    if-gt v0, v1, :cond_9

    .line 5146
    iget-object v1, p0, Lcom/jess/ui/e;->d:Lcom/jess/ui/TwoWayAbsListView;

    const/4 v2, 0x4

    iput v2, v1, Lcom/jess/ui/TwoWayAbsListView;->a:I

    .line 5147
    iget-object v1, p0, Lcom/jess/ui/e;->d:Lcom/jess/ui/TwoWayAbsListView;

    invoke-virtual {v1, v0}, Lcom/jess/ui/TwoWayAbsListView;->setSelectionInt(I)V

    .line 5148
    iget-object v1, p0, Lcom/jess/ui/e;->d:Lcom/jess/ui/TwoWayAbsListView;

    invoke-virtual {v1}, Lcom/jess/ui/TwoWayAbsListView;->a()V

    .line 5152
    :goto_3
    invoke-virtual {p0, v4}, Lcom/jess/ui/e;->b(I)V

    .line 5154
    if-ltz v0, :cond_a

    :goto_4
    move v4, v3

    goto/16 :goto_0

    .line 5084
    :cond_2
    if-le v9, v7, :cond_1

    .line 5085
    invoke-virtual {v8}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    sub-int v0, v7, v0

    iget-object v5, p0, Lcom/jess/ui/e;->d:Lcom/jess/ui/TwoWayAbsListView;

    .line 5086
    invoke-virtual {v5}, Lcom/jess/ui/TwoWayAbsListView;->getHorizontalFadingEdgeLength()I

    move-result v5

    sub-int/2addr v0, v5

    goto :goto_1

    .line 5089
    :cond_3
    if-ge v2, v1, :cond_6

    move v7, v4

    move v0, v4

    .line 5092
    :goto_5
    if-ge v7, v9, :cond_10

    .line 5093
    iget-object v2, p0, Lcom/jess/ui/e;->d:Lcom/jess/ui/TwoWayAbsListView;

    invoke-virtual {v2, v7}, Lcom/jess/ui/TwoWayAbsListView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 5094
    invoke-virtual {v2}, Landroid/view/View;->getLeft()I

    move-result v2

    .line 5096
    if-nez v7, :cond_f

    .line 5100
    if-gtz v1, :cond_4

    if-ge v2, v5, :cond_e

    .line 5103
    :cond_4
    iget-object v0, p0, Lcom/jess/ui/e;->d:Lcom/jess/ui/TwoWayAbsListView;

    invoke-virtual {v0}, Lcom/jess/ui/TwoWayAbsListView;->getHorizontalFadingEdgeLength()I

    move-result v0

    add-int/2addr v0, v5

    move v5, v2

    .line 5106
    :goto_6
    if-lt v2, v0, :cond_5

    .line 5108
    add-int v0, v1, v7

    :goto_7
    move v5, v2

    move v2, v0

    move v0, v3

    .line 5092
    goto :goto_2

    :cond_5
    add-int/lit8 v2, v7, 0x1

    move v7, v2

    move v13, v0

    move v0, v5

    move v5, v13

    goto :goto_5

    .line 5114
    :cond_6
    iget-object v0, p0, Lcom/jess/ui/e;->d:Lcom/jess/ui/TwoWayAbsListView;

    iget v10, v0, Lcom/jess/ui/TwoWayAbsListView;->aa:I

    .line 5116
    add-int v0, v1, v9

    add-int/lit8 v0, v0, -0x1

    .line 5118
    add-int/lit8 v2, v9, -0x1

    move v8, v2

    move v5, v4

    :goto_8
    if-ltz v8, :cond_d

    .line 5119
    iget-object v2, p0, Lcom/jess/ui/e;->d:Lcom/jess/ui/TwoWayAbsListView;

    invoke-virtual {v2, v8}, Lcom/jess/ui/TwoWayAbsListView;->getChildAt(I)Landroid/view/View;

    move-result-object v11

    .line 5120
    invoke-virtual {v11}, Landroid/view/View;->getLeft()I

    move-result v2

    .line 5121
    invoke-virtual {v11}, Landroid/view/View;->getRight()I

    move-result v11

    .line 5123
    add-int/lit8 v12, v9, -0x1

    if-ne v8, v12, :cond_c

    .line 5125
    add-int v5, v1, v9

    if-lt v5, v10, :cond_7

    if-le v11, v7, :cond_b

    .line 5126
    :cond_7
    iget-object v5, p0, Lcom/jess/ui/e;->d:Lcom/jess/ui/TwoWayAbsListView;

    invoke-virtual {v5}, Lcom/jess/ui/TwoWayAbsListView;->getHorizontalFadingEdgeLength()I

    move-result v5

    sub-int v5, v7, v5

    move v7, v2

    .line 5130
    :goto_9
    if-gt v11, v5, :cond_8

    .line 5131
    add-int v0, v1, v8

    move v5, v2

    move v2, v0

    move v0, v4

    .line 5133
    goto/16 :goto_2

    .line 5118
    :cond_8
    add-int/lit8 v2, v8, -0x1

    move v8, v2

    move v13, v5

    move v5, v7

    move v7, v13

    goto :goto_8

    :cond_9
    move v0, v6

    .line 5150
    goto/16 :goto_3

    :cond_a
    move v3, v4

    .line 5154
    goto/16 :goto_4

    :cond_b
    move v5, v7

    move v7, v2

    goto :goto_9

    :cond_c
    move v13, v7

    move v7, v5

    move v5, v13

    goto :goto_9

    :cond_d
    move v2, v0

    move v0, v4

    goto/16 :goto_2

    :cond_e
    move v0, v5

    move v5, v2

    goto :goto_6

    :cond_f
    move v13, v5

    move v5, v0

    move v0, v13

    goto :goto_6

    :cond_10
    move v2, v0

    move v0, v1

    goto :goto_7
.end method

.method a(II)Z
    .locals 18

    .prologue
    .line 5161
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/jess/ui/e;->d:Lcom/jess/ui/TwoWayAbsListView;

    invoke-virtual {v1}, Lcom/jess/ui/TwoWayAbsListView;->getChildCount()I

    move-result v8

    .line 5162
    if-nez v8, :cond_0

    .line 5163
    const/4 v1, 0x1

    .line 5293
    :goto_0
    return v1

    .line 5166
    :cond_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/jess/ui/e;->d:Lcom/jess/ui/TwoWayAbsListView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/jess/ui/TwoWayAbsListView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getLeft()I

    move-result v2

    .line 5167
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/jess/ui/e;->d:Lcom/jess/ui/TwoWayAbsListView;

    add-int/lit8 v3, v8, -0x1

    invoke-virtual {v1, v3}, Lcom/jess/ui/TwoWayAbsListView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getRight()I

    move-result v3

    .line 5169
    new-instance v6, Landroid/graphics/Rect;

    const/4 v1, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v7, 0x0

    invoke-direct {v6, v1, v4, v5, v7}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 5172
    iget v1, v6, Landroid/graphics/Rect;->left:I

    sub-int v9, v1, v2

    .line 5173
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/jess/ui/e;->d:Lcom/jess/ui/TwoWayAbsListView;

    invoke-virtual {v1}, Lcom/jess/ui/TwoWayAbsListView;->getWidth()I

    move-result v1

    iget v4, v6, Landroid/graphics/Rect;->right:I

    sub-int v4, v1, v4

    .line 5174
    sub-int v10, v3, v4

    .line 5176
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/jess/ui/e;->d:Lcom/jess/ui/TwoWayAbsListView;

    invoke-virtual {v1}, Lcom/jess/ui/TwoWayAbsListView;->getWidth()I

    move-result v1

    add-int/lit8 v5, v1, 0x0

    .line 5177
    if-gez p1, :cond_1

    .line 5178
    add-int/lit8 v1, v5, -0x1

    neg-int v1, v1

    move/from16 v0, p1

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v1

    move v7, v1

    .line 5183
    :goto_1
    if-gez p2, :cond_2

    .line 5184
    add-int/lit8 v1, v5, -0x1

    neg-int v1, v1

    move/from16 v0, p2

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 5189
    :goto_2
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/jess/ui/e;->d:Lcom/jess/ui/TwoWayAbsListView;

    iget v11, v5, Lcom/jess/ui/TwoWayAbsListView;->H:I

    .line 5191
    if-nez v11, :cond_3

    iget v5, v6, Landroid/graphics/Rect;->left:I

    if-lt v2, v5, :cond_3

    if-ltz v7, :cond_3

    .line 5195
    const/4 v1, 0x1

    goto :goto_0

    .line 5180
    :cond_1
    add-int/lit8 v1, v5, -0x1

    move/from16 v0, p1

    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v1

    move v7, v1

    goto :goto_1

    .line 5186
    :cond_2
    add-int/lit8 v1, v5, -0x1

    move/from16 v0, p2

    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v1

    goto :goto_2

    .line 5198
    :cond_3
    add-int v2, v11, v8

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/jess/ui/e;->d:Lcom/jess/ui/TwoWayAbsListView;

    iget v5, v5, Lcom/jess/ui/TwoWayAbsListView;->aa:I

    if-ne v2, v5, :cond_4

    if-gt v3, v4, :cond_4

    if-gtz v7, :cond_4

    .line 5202
    const/4 v1, 0x1

    goto/16 :goto_0

    .line 5205
    :cond_4
    if-gez v1, :cond_c

    const/4 v2, 0x1

    .line 5207
    :goto_3
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/jess/ui/e;->d:Lcom/jess/ui/TwoWayAbsListView;

    invoke-virtual {v3}, Lcom/jess/ui/TwoWayAbsListView;->isInTouchMode()Z

    move-result v12

    .line 5208
    if-eqz v12, :cond_5

    .line 5209
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/jess/ui/e;->d:Lcom/jess/ui/TwoWayAbsListView;

    invoke-virtual {v3}, Lcom/jess/ui/TwoWayAbsListView;->i()V

    .line 5212
    :cond_5
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/jess/ui/e;->d:Lcom/jess/ui/TwoWayAbsListView;

    invoke-virtual {v3}, Lcom/jess/ui/TwoWayAbsListView;->getHeaderViewsCount()I

    move-result v13

    .line 5213
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/jess/ui/e;->d:Lcom/jess/ui/TwoWayAbsListView;

    iget v3, v3, Lcom/jess/ui/TwoWayAbsListView;->aa:I

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/jess/ui/e;->d:Lcom/jess/ui/TwoWayAbsListView;

    invoke-virtual {v4}, Lcom/jess/ui/TwoWayAbsListView;->getFooterViewsCount()I

    move-result v4

    sub-int v14, v3, v4

    .line 5215
    const/4 v4, 0x0

    .line 5216
    const/4 v5, 0x0

    .line 5218
    if-eqz v2, :cond_f

    .line 5219
    iget v3, v6, Landroid/graphics/Rect;->left:I

    sub-int v15, v3, v1

    .line 5220
    const/4 v3, 0x0

    move/from16 v17, v3

    move v3, v5

    move/from16 v5, v17

    :goto_4
    if-ge v5, v8, :cond_6

    .line 5221
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/jess/ui/e;->d:Lcom/jess/ui/TwoWayAbsListView;

    invoke-virtual {v6, v5}, Lcom/jess/ui/TwoWayAbsListView;->getChildAt(I)Landroid/view/View;

    move-result-object v16

    .line 5222
    invoke-virtual/range {v16 .. v16}, Landroid/view/View;->getRight()I

    move-result v6

    if-lt v6, v15, :cond_d

    .line 5261
    :cond_6
    move-object/from16 v0, p0

    iget v5, v0, Lcom/jess/ui/e;->a:I

    add-int/2addr v5, v7

    move-object/from16 v0, p0

    iput v5, v0, Lcom/jess/ui/e;->c:I

    .line 5263
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/jess/ui/e;->d:Lcom/jess/ui/TwoWayAbsListView;

    const/4 v6, 0x1

    iput-boolean v6, v5, Lcom/jess/ui/TwoWayAbsListView;->ae:Z

    .line 5265
    if-lez v3, :cond_7

    .line 5266
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/jess/ui/e;->d:Lcom/jess/ui/TwoWayAbsListView;

    invoke-static {v5, v4, v3}, Lcom/jess/ui/TwoWayAbsListView;->b(Lcom/jess/ui/TwoWayAbsListView;II)V

    .line 5268
    :cond_7
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/jess/ui/e;->d:Lcom/jess/ui/TwoWayAbsListView;

    invoke-virtual {v4, v1}, Lcom/jess/ui/TwoWayAbsListView;->f(I)V

    .line 5270
    if-eqz v2, :cond_8

    .line 5271
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/jess/ui/e;->d:Lcom/jess/ui/TwoWayAbsListView;

    iget v5, v4, Lcom/jess/ui/TwoWayAbsListView;->H:I

    add-int/2addr v3, v5

    iput v3, v4, Lcom/jess/ui/TwoWayAbsListView;->H:I

    .line 5274
    :cond_8
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/jess/ui/e;->d:Lcom/jess/ui/TwoWayAbsListView;

    invoke-virtual {v3}, Lcom/jess/ui/TwoWayAbsListView;->invalidate()V

    .line 5276
    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v1

    .line 5277
    if-lt v9, v1, :cond_9

    if-ge v10, v1, :cond_a

    .line 5278
    :cond_9
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/jess/ui/e;->d:Lcom/jess/ui/TwoWayAbsListView;

    invoke-virtual {v1, v2}, Lcom/jess/ui/TwoWayAbsListView;->a(Z)V

    .line 5281
    :cond_a
    if-nez v12, :cond_b

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/jess/ui/e;->d:Lcom/jess/ui/TwoWayAbsListView;

    iget v1, v1, Lcom/jess/ui/TwoWayAbsListView;->V:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_b

    .line 5282
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/jess/ui/e;->d:Lcom/jess/ui/TwoWayAbsListView;

    iget v1, v1, Lcom/jess/ui/TwoWayAbsListView;->V:I

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/jess/ui/e;->d:Lcom/jess/ui/TwoWayAbsListView;

    iget v2, v2, Lcom/jess/ui/TwoWayAbsListView;->H:I

    sub-int/2addr v1, v2

    .line 5283
    if-ltz v1, :cond_b

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/jess/ui/e;->d:Lcom/jess/ui/TwoWayAbsListView;

    invoke-virtual {v2}, Lcom/jess/ui/TwoWayAbsListView;->getChildCount()I

    move-result v2

    if-ge v1, v2, :cond_b

    .line 5284
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/jess/ui/e;->d:Lcom/jess/ui/TwoWayAbsListView;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/jess/ui/e;->d:Lcom/jess/ui/TwoWayAbsListView;

    invoke-virtual {v3, v1}, Lcom/jess/ui/TwoWayAbsListView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v2, v1}, Lcom/jess/ui/TwoWayAbsListView;->a(Landroid/view/View;)V

    .line 5288
    :cond_b
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/jess/ui/e;->d:Lcom/jess/ui/TwoWayAbsListView;

    const/4 v2, 0x0

    iput-boolean v2, v1, Lcom/jess/ui/TwoWayAbsListView;->ae:Z

    .line 5290
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/jess/ui/e;->d:Lcom/jess/ui/TwoWayAbsListView;

    invoke-virtual {v1}, Lcom/jess/ui/TwoWayAbsListView;->a()V

    .line 5293
    const/4 v1, 0x0

    goto/16 :goto_0

    .line 5205
    :cond_c
    const/4 v2, 0x0

    goto/16 :goto_3

    .line 5225
    :cond_d
    add-int/lit8 v6, v3, 0x1

    .line 5226
    add-int v3, v11, v5

    .line 5227
    if-lt v3, v13, :cond_e

    if-ge v3, v14, :cond_e

    .line 5228
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/jess/ui/e;->d:Lcom/jess/ui/TwoWayAbsListView;

    iget-object v3, v3, Lcom/jess/ui/TwoWayAbsListView;->g:Lcom/jess/ui/k;

    move-object/from16 v0, v16

    invoke-virtual {v3, v0}, Lcom/jess/ui/k;->a(Landroid/view/View;)V

    .line 5220
    :cond_e
    add-int/lit8 v3, v5, 0x1

    move v5, v3

    move v3, v6

    goto/16 :goto_4

    .line 5239
    :cond_f
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/jess/ui/e;->d:Lcom/jess/ui/TwoWayAbsListView;

    invoke-virtual {v3}, Lcom/jess/ui/TwoWayAbsListView;->getWidth()I

    move-result v3

    iget v6, v6, Landroid/graphics/Rect;->right:I

    sub-int/2addr v3, v6

    sub-int v6, v3, v1

    .line 5240
    add-int/lit8 v3, v8, -0x1

    move/from16 v17, v3

    move v3, v5

    move/from16 v5, v17

    :goto_5
    if-ltz v5, :cond_6

    .line 5241
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/jess/ui/e;->d:Lcom/jess/ui/TwoWayAbsListView;

    invoke-virtual {v8, v5}, Lcom/jess/ui/TwoWayAbsListView;->getChildAt(I)Landroid/view/View;

    move-result-object v8

    .line 5242
    invoke-virtual {v8}, Landroid/view/View;->getLeft()I

    move-result v15

    if-le v15, v6, :cond_6

    .line 5246
    add-int/lit8 v4, v3, 0x1

    .line 5247
    add-int v3, v11, v5

    .line 5248
    if-lt v3, v13, :cond_10

    if-ge v3, v14, :cond_10

    .line 5249
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/jess/ui/e;->d:Lcom/jess/ui/TwoWayAbsListView;

    iget-object v3, v3, Lcom/jess/ui/TwoWayAbsListView;->g:Lcom/jess/ui/k;

    invoke-virtual {v3, v8}, Lcom/jess/ui/k;->a(Landroid/view/View;)V

    .line 5240
    :cond_10
    add-int/lit8 v3, v5, -0x1

    move/from16 v17, v3

    move v3, v4

    move v4, v5

    move/from16 v5, v17

    goto :goto_5
.end method

.method public a(Landroid/view/MotionEvent;)Z
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v0, 0x1

    const/4 v3, -0x1

    const/4 v1, 0x0

    .line 4721
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    .line 4732
    packed-switch v2, :pswitch_data_0

    :cond_0
    :goto_0
    move v0, v1

    .line 4779
    :goto_1
    return v0

    .line 4734
    :pswitch_0
    iget-object v2, p0, Lcom/jess/ui/e;->d:Lcom/jess/ui/TwoWayAbsListView;

    iget v2, v2, Lcom/jess/ui/TwoWayAbsListView;->w:I

    .line 4736
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    float-to-int v3, v3

    .line 4737
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    float-to-int v4, v4

    .line 4739
    iget-object v5, p0, Lcom/jess/ui/e;->d:Lcom/jess/ui/TwoWayAbsListView;

    invoke-virtual {v5, v3}, Lcom/jess/ui/TwoWayAbsListView;->b(I)I

    move-result v5

    .line 4740
    if-eq v2, v8, :cond_1

    if-ltz v5, :cond_1

    .line 4743
    iget-object v6, p0, Lcom/jess/ui/e;->d:Lcom/jess/ui/TwoWayAbsListView;

    iget-object v7, p0, Lcom/jess/ui/e;->d:Lcom/jess/ui/TwoWayAbsListView;

    iget v7, v7, Lcom/jess/ui/TwoWayAbsListView;->H:I

    sub-int v7, v5, v7

    invoke-virtual {v6, v7}, Lcom/jess/ui/TwoWayAbsListView;->getChildAt(I)Landroid/view/View;

    move-result-object v6

    .line 4744
    invoke-virtual {v6}, Landroid/view/View;->getLeft()I

    move-result v6

    iput v6, p0, Lcom/jess/ui/e;->a:I

    .line 4745
    iget-object v6, p0, Lcom/jess/ui/e;->d:Lcom/jess/ui/TwoWayAbsListView;

    iput v3, v6, Lcom/jess/ui/TwoWayAbsListView;->u:I

    .line 4746
    iget-object v3, p0, Lcom/jess/ui/e;->d:Lcom/jess/ui/TwoWayAbsListView;

    iput v4, v3, Lcom/jess/ui/TwoWayAbsListView;->v:I

    .line 4747
    iget-object v3, p0, Lcom/jess/ui/e;->d:Lcom/jess/ui/TwoWayAbsListView;

    iput v5, v3, Lcom/jess/ui/TwoWayAbsListView;->t:I

    .line 4748
    iget-object v3, p0, Lcom/jess/ui/e;->d:Lcom/jess/ui/TwoWayAbsListView;

    iput v1, v3, Lcom/jess/ui/TwoWayAbsListView;->w:I

    .line 4749
    invoke-virtual {p0}, Lcom/jess/ui/e;->c()V

    .line 4751
    :cond_1
    const/high16 v3, -0x80000000

    iput v3, p0, Lcom/jess/ui/e;->b:I

    .line 4752
    if-ne v2, v8, :cond_0

    goto :goto_1

    .line 4759
    :pswitch_1
    iget-object v2, p0, Lcom/jess/ui/e;->d:Lcom/jess/ui/TwoWayAbsListView;

    iget v2, v2, Lcom/jess/ui/TwoWayAbsListView;->w:I

    packed-switch v2, :pswitch_data_1

    goto :goto_0

    .line 4761
    :pswitch_2
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    float-to-int v2, v2

    .line 4762
    iget-object v3, p0, Lcom/jess/ui/e;->d:Lcom/jess/ui/TwoWayAbsListView;

    iget v3, v3, Lcom/jess/ui/TwoWayAbsListView;->u:I

    sub-int/2addr v2, v3

    invoke-virtual {p0, v2}, Lcom/jess/ui/e;->a(I)Z

    move-result v2

    if-eqz v2, :cond_0

    goto :goto_1

    .line 4771
    :pswitch_3
    iget-object v0, p0, Lcom/jess/ui/e;->d:Lcom/jess/ui/TwoWayAbsListView;

    iput v3, v0, Lcom/jess/ui/TwoWayAbsListView;->w:I

    .line 4772
    iget-object v0, p0, Lcom/jess/ui/e;->d:Lcom/jess/ui/TwoWayAbsListView;

    invoke-static {v0, v3}, Lcom/jess/ui/TwoWayAbsListView;->c(Lcom/jess/ui/TwoWayAbsListView;I)I

    .line 4773
    invoke-virtual {p0, v1}, Lcom/jess/ui/e;->b(I)V

    goto :goto_0

    .line 4732
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_3
        :pswitch_1
    .end packed-switch

    .line 4759
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_2
    .end packed-switch
.end method

.method public b(Landroid/view/MotionEvent;)Z
    .locals 10

    .prologue
    const/high16 v9, -0x80000000

    const/4 v8, 0x0

    const/4 v2, 0x1

    const/4 v7, -0x1

    const/4 v1, 0x0

    .line 4785
    iget-object v0, p0, Lcom/jess/ui/e;->d:Lcom/jess/ui/TwoWayAbsListView;

    invoke-virtual {v0}, Lcom/jess/ui/TwoWayAbsListView;->isEnabled()Z

    move-result v0

    if-nez v0, :cond_3

    .line 4788
    iget-object v0, p0, Lcom/jess/ui/e;->d:Lcom/jess/ui/TwoWayAbsListView;

    invoke-virtual {v0}, Lcom/jess/ui/TwoWayAbsListView;->isClickable()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/jess/ui/e;->d:Lcom/jess/ui/TwoWayAbsListView;

    invoke-virtual {v0}, Lcom/jess/ui/TwoWayAbsListView;->isLongClickable()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    move v0, v2

    :goto_0
    move v2, v0

    .line 5054
    :cond_1
    :goto_1
    return v2

    :cond_2
    move v0, v1

    .line 4788
    goto :goto_0

    .line 4799
    :cond_3
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    .line 4804
    iget-object v3, p0, Lcom/jess/ui/e;->d:Lcom/jess/ui/TwoWayAbsListView;

    invoke-static {v3}, Lcom/jess/ui/TwoWayAbsListView;->i(Lcom/jess/ui/TwoWayAbsListView;)Landroid/view/VelocityTracker;

    move-result-object v3

    if-nez v3, :cond_4

    .line 4805
    iget-object v3, p0, Lcom/jess/ui/e;->d:Lcom/jess/ui/TwoWayAbsListView;

    invoke-static {}, Landroid/view/VelocityTracker;->obtain()Landroid/view/VelocityTracker;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/jess/ui/TwoWayAbsListView;->a(Lcom/jess/ui/TwoWayAbsListView;Landroid/view/VelocityTracker;)Landroid/view/VelocityTracker;

    .line 4807
    :cond_4
    iget-object v3, p0, Lcom/jess/ui/e;->d:Lcom/jess/ui/TwoWayAbsListView;

    invoke-static {v3}, Lcom/jess/ui/TwoWayAbsListView;->i(Lcom/jess/ui/TwoWayAbsListView;)Landroid/view/VelocityTracker;

    move-result-object v3

    invoke-virtual {v3, p1}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    .line 4809
    packed-switch v0, :pswitch_data_0

    goto :goto_1

    .line 4811
    :pswitch_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    float-to-int v4, v0

    .line 4812
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    float-to-int v5, v0

    .line 4813
    iget-object v0, p0, Lcom/jess/ui/e;->d:Lcom/jess/ui/TwoWayAbsListView;

    invoke-virtual {v0, v4, v5}, Lcom/jess/ui/TwoWayAbsListView;->a(II)I

    move-result v3

    .line 4814
    iget-object v0, p0, Lcom/jess/ui/e;->d:Lcom/jess/ui/TwoWayAbsListView;

    iget-boolean v0, v0, Lcom/jess/ui/TwoWayAbsListView;->S:Z

    if-nez v0, :cond_1f

    .line 4815
    iget-object v0, p0, Lcom/jess/ui/e;->d:Lcom/jess/ui/TwoWayAbsListView;

    iget v0, v0, Lcom/jess/ui/TwoWayAbsListView;->w:I

    const/4 v6, 0x4

    if-eq v0, v6, :cond_7

    if-ltz v3, :cond_7

    iget-object v0, p0, Lcom/jess/ui/e;->d:Lcom/jess/ui/TwoWayAbsListView;

    .line 4816
    invoke-virtual {v0}, Lcom/jess/ui/TwoWayAbsListView;->getAdapter()Landroid/widget/Adapter;

    move-result-object v0

    check-cast v0, Landroid/widget/ListAdapter;

    invoke-interface {v0, v3}, Landroid/widget/ListAdapter;->isEnabled(I)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 4819
    iget-object v0, p0, Lcom/jess/ui/e;->d:Lcom/jess/ui/TwoWayAbsListView;

    iput v1, v0, Lcom/jess/ui/TwoWayAbsListView;->w:I

    .line 4821
    iget-object v0, p0, Lcom/jess/ui/e;->d:Lcom/jess/ui/TwoWayAbsListView;

    invoke-static {v0}, Lcom/jess/ui/TwoWayAbsListView;->j(Lcom/jess/ui/TwoWayAbsListView;)Ljava/lang/Runnable;

    move-result-object v0

    if-nez v0, :cond_5

    .line 4822
    iget-object v0, p0, Lcom/jess/ui/e;->d:Lcom/jess/ui/TwoWayAbsListView;

    new-instance v1, Lcom/jess/ui/d;

    iget-object v6, p0, Lcom/jess/ui/e;->d:Lcom/jess/ui/TwoWayAbsListView;

    invoke-direct {v1, v6}, Lcom/jess/ui/d;-><init>(Lcom/jess/ui/TwoWayAbsListView;)V

    invoke-static {v0, v1}, Lcom/jess/ui/TwoWayAbsListView;->b(Lcom/jess/ui/TwoWayAbsListView;Ljava/lang/Runnable;)Ljava/lang/Runnable;

    .line 4824
    :cond_5
    iget-object v0, p0, Lcom/jess/ui/e;->d:Lcom/jess/ui/TwoWayAbsListView;

    iget-object v1, p0, Lcom/jess/ui/e;->d:Lcom/jess/ui/TwoWayAbsListView;

    invoke-static {v1}, Lcom/jess/ui/TwoWayAbsListView;->j(Lcom/jess/ui/TwoWayAbsListView;)Ljava/lang/Runnable;

    move-result-object v1

    invoke-static {}, Landroid/view/ViewConfiguration;->getTapTimeout()I

    move-result v6

    int-to-long v6, v6

    invoke-virtual {v0, v1, v6, v7}, Lcom/jess/ui/TwoWayAbsListView;->postDelayed(Ljava/lang/Runnable;J)Z

    move v0, v3

    .line 4844
    :goto_2
    if-ltz v0, :cond_6

    .line 4846
    iget-object v1, p0, Lcom/jess/ui/e;->d:Lcom/jess/ui/TwoWayAbsListView;

    iget-object v3, p0, Lcom/jess/ui/e;->d:Lcom/jess/ui/TwoWayAbsListView;

    iget v3, v3, Lcom/jess/ui/TwoWayAbsListView;->H:I

    sub-int v3, v0, v3

    invoke-virtual {v1, v3}, Lcom/jess/ui/TwoWayAbsListView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 4847
    invoke-virtual {v1}, Landroid/view/View;->getLeft()I

    move-result v1

    iput v1, p0, Lcom/jess/ui/e;->a:I

    .line 4849
    :cond_6
    iget-object v1, p0, Lcom/jess/ui/e;->d:Lcom/jess/ui/TwoWayAbsListView;

    iput v4, v1, Lcom/jess/ui/TwoWayAbsListView;->u:I

    .line 4850
    iget-object v1, p0, Lcom/jess/ui/e;->d:Lcom/jess/ui/TwoWayAbsListView;

    iput v5, v1, Lcom/jess/ui/TwoWayAbsListView;->v:I

    .line 4851
    iget-object v1, p0, Lcom/jess/ui/e;->d:Lcom/jess/ui/TwoWayAbsListView;

    iput v0, v1, Lcom/jess/ui/TwoWayAbsListView;->t:I

    .line 4852
    iput v9, p0, Lcom/jess/ui/e;->b:I

    goto/16 :goto_1

    .line 4826
    :cond_7
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEdgeFlags()I

    move-result v0

    if-eqz v0, :cond_8

    if-gez v3, :cond_8

    move v2, v1

    .line 4830
    goto/16 :goto_1

    .line 4833
    :cond_8
    iget-object v0, p0, Lcom/jess/ui/e;->d:Lcom/jess/ui/TwoWayAbsListView;

    iget v0, v0, Lcom/jess/ui/TwoWayAbsListView;->w:I

    const/4 v6, 0x4

    if-ne v0, v6, :cond_1f

    .line 4835
    invoke-virtual {p0}, Lcom/jess/ui/e;->b()V

    .line 4836
    iget-object v0, p0, Lcom/jess/ui/e;->d:Lcom/jess/ui/TwoWayAbsListView;

    const/4 v3, 0x3

    iput v3, v0, Lcom/jess/ui/TwoWayAbsListView;->w:I

    .line 4837
    iput v1, p0, Lcom/jess/ui/e;->g:I

    .line 4838
    iget-object v0, p0, Lcom/jess/ui/e;->d:Lcom/jess/ui/TwoWayAbsListView;

    invoke-virtual {v0, v4}, Lcom/jess/ui/TwoWayAbsListView;->b(I)I

    move-result v0

    .line 4839
    invoke-virtual {p0, v2}, Lcom/jess/ui/e;->b(I)V

    goto :goto_2

    .line 4857
    :pswitch_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    float-to-int v4, v0

    .line 4858
    iget-object v0, p0, Lcom/jess/ui/e;->d:Lcom/jess/ui/TwoWayAbsListView;

    iget v0, v0, Lcom/jess/ui/TwoWayAbsListView;->u:I

    sub-int v0, v4, v0

    .line 4859
    iget-object v3, p0, Lcom/jess/ui/e;->d:Lcom/jess/ui/TwoWayAbsListView;

    iget v3, v3, Lcom/jess/ui/TwoWayAbsListView;->w:I

    packed-switch v3, :pswitch_data_1

    goto/16 :goto_1

    .line 4865
    :pswitch_2
    invoke-virtual {p0, v0}, Lcom/jess/ui/e;->a(I)Z

    goto/16 :goto_1

    .line 4875
    :pswitch_3
    iget v3, p0, Lcom/jess/ui/e;->b:I

    if-eq v4, v3, :cond_1

    .line 4876
    iget v3, p0, Lcom/jess/ui/e;->g:I

    sub-int v3, v0, v3

    .line 4877
    iget v0, p0, Lcom/jess/ui/e;->b:I

    if-eq v0, v9, :cond_c

    iget v0, p0, Lcom/jess/ui/e;->b:I

    sub-int v0, v4, v0

    .line 4881
    :goto_3
    if-eqz v0, :cond_9

    .line 4882
    invoke-virtual {p0, v3, v0}, Lcom/jess/ui/e;->a(II)Z

    move-result v1

    .line 4886
    :cond_9
    if-eqz v1, :cond_b

    iget-object v0, p0, Lcom/jess/ui/e;->d:Lcom/jess/ui/TwoWayAbsListView;

    invoke-virtual {v0}, Lcom/jess/ui/TwoWayAbsListView;->getChildCount()I

    move-result v0

    if-lez v0, :cond_b

    .line 4891
    iget-object v0, p0, Lcom/jess/ui/e;->d:Lcom/jess/ui/TwoWayAbsListView;

    invoke-virtual {v0, v4}, Lcom/jess/ui/TwoWayAbsListView;->b(I)I

    move-result v0

    .line 4892
    if-ltz v0, :cond_a

    .line 4893
    iget-object v1, p0, Lcom/jess/ui/e;->d:Lcom/jess/ui/TwoWayAbsListView;

    iget-object v3, p0, Lcom/jess/ui/e;->d:Lcom/jess/ui/TwoWayAbsListView;

    iget v3, v3, Lcom/jess/ui/TwoWayAbsListView;->H:I

    sub-int v3, v0, v3

    invoke-virtual {v1, v3}, Lcom/jess/ui/TwoWayAbsListView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 4894
    invoke-virtual {v1}, Landroid/view/View;->getLeft()I

    move-result v1

    iput v1, p0, Lcom/jess/ui/e;->a:I

    .line 4896
    :cond_a
    iget-object v1, p0, Lcom/jess/ui/e;->d:Lcom/jess/ui/TwoWayAbsListView;

    iput v4, v1, Lcom/jess/ui/TwoWayAbsListView;->u:I

    .line 4897
    iget-object v1, p0, Lcom/jess/ui/e;->d:Lcom/jess/ui/TwoWayAbsListView;

    iput v0, v1, Lcom/jess/ui/TwoWayAbsListView;->t:I

    .line 4898
    iget-object v0, p0, Lcom/jess/ui/e;->d:Lcom/jess/ui/TwoWayAbsListView;

    invoke-virtual {v0}, Lcom/jess/ui/TwoWayAbsListView;->invalidate()V

    .line 4900
    :cond_b
    iput v4, p0, Lcom/jess/ui/e;->b:I

    goto/16 :goto_1

    :cond_c
    move v0, v3

    .line 4877
    goto :goto_3

    .line 4909
    :pswitch_4
    iget-object v0, p0, Lcom/jess/ui/e;->d:Lcom/jess/ui/TwoWayAbsListView;

    iget v0, v0, Lcom/jess/ui/TwoWayAbsListView;->w:I

    packed-switch v0, :pswitch_data_2

    .line 5004
    :goto_4
    iget-object v0, p0, Lcom/jess/ui/e;->d:Lcom/jess/ui/TwoWayAbsListView;

    invoke-virtual {v0, v1}, Lcom/jess/ui/TwoWayAbsListView;->setPressed(Z)V

    .line 5007
    iget-object v0, p0, Lcom/jess/ui/e;->d:Lcom/jess/ui/TwoWayAbsListView;

    invoke-virtual {v0}, Lcom/jess/ui/TwoWayAbsListView;->invalidate()V

    .line 5009
    iget-object v0, p0, Lcom/jess/ui/e;->d:Lcom/jess/ui/TwoWayAbsListView;

    invoke-virtual {v0}, Lcom/jess/ui/TwoWayAbsListView;->getHandler()Landroid/os/Handler;

    move-result-object v0

    .line 5010
    if-eqz v0, :cond_d

    .line 5011
    iget-object v1, p0, Lcom/jess/ui/e;->d:Lcom/jess/ui/TwoWayAbsListView;

    invoke-static {v1}, Lcom/jess/ui/TwoWayAbsListView;->c(Lcom/jess/ui/TwoWayAbsListView;)Lcom/jess/ui/c;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 5014
    :cond_d
    iget-object v0, p0, Lcom/jess/ui/e;->d:Lcom/jess/ui/TwoWayAbsListView;

    invoke-static {v0}, Lcom/jess/ui/TwoWayAbsListView;->i(Lcom/jess/ui/TwoWayAbsListView;)Landroid/view/VelocityTracker;

    move-result-object v0

    if-eqz v0, :cond_e

    .line 5015
    iget-object v0, p0, Lcom/jess/ui/e;->d:Lcom/jess/ui/TwoWayAbsListView;

    invoke-static {v0}, Lcom/jess/ui/TwoWayAbsListView;->i(Lcom/jess/ui/TwoWayAbsListView;)Landroid/view/VelocityTracker;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->recycle()V

    .line 5016
    iget-object v0, p0, Lcom/jess/ui/e;->d:Lcom/jess/ui/TwoWayAbsListView;

    invoke-static {v0, v8}, Lcom/jess/ui/TwoWayAbsListView;->a(Lcom/jess/ui/TwoWayAbsListView;Landroid/view/VelocityTracker;)Landroid/view/VelocityTracker;

    .line 5019
    :cond_e
    iget-object v0, p0, Lcom/jess/ui/e;->d:Lcom/jess/ui/TwoWayAbsListView;

    invoke-static {v0, v7}, Lcom/jess/ui/TwoWayAbsListView;->c(Lcom/jess/ui/TwoWayAbsListView;I)I

    goto/16 :goto_1

    .line 4913
    :pswitch_5
    iget-object v0, p0, Lcom/jess/ui/e;->d:Lcom/jess/ui/TwoWayAbsListView;

    iget v3, v0, Lcom/jess/ui/TwoWayAbsListView;->t:I

    .line 4914
    iget-object v0, p0, Lcom/jess/ui/e;->d:Lcom/jess/ui/TwoWayAbsListView;

    iget-object v4, p0, Lcom/jess/ui/e;->d:Lcom/jess/ui/TwoWayAbsListView;

    iget v4, v4, Lcom/jess/ui/TwoWayAbsListView;->H:I

    sub-int v4, v3, v4

    invoke-virtual {v0, v4}, Lcom/jess/ui/TwoWayAbsListView;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    .line 4915
    if-eqz v4, :cond_17

    invoke-virtual {v4}, Landroid/view/View;->hasFocusable()Z

    move-result v0

    if-nez v0, :cond_17

    .line 4916
    iget-object v0, p0, Lcom/jess/ui/e;->d:Lcom/jess/ui/TwoWayAbsListView;

    iget v0, v0, Lcom/jess/ui/TwoWayAbsListView;->w:I

    if-eqz v0, :cond_f

    .line 4917
    invoke-virtual {v4, v1}, Landroid/view/View;->setPressed(Z)V

    .line 4920
    :cond_f
    iget-object v0, p0, Lcom/jess/ui/e;->d:Lcom/jess/ui/TwoWayAbsListView;

    invoke-static {v0}, Lcom/jess/ui/TwoWayAbsListView;->k(Lcom/jess/ui/TwoWayAbsListView;)Lcom/jess/ui/j;

    move-result-object v0

    if-nez v0, :cond_10

    .line 4921
    iget-object v0, p0, Lcom/jess/ui/e;->d:Lcom/jess/ui/TwoWayAbsListView;

    new-instance v5, Lcom/jess/ui/j;

    iget-object v6, p0, Lcom/jess/ui/e;->d:Lcom/jess/ui/TwoWayAbsListView;

    invoke-direct {v5, v6, v8}, Lcom/jess/ui/j;-><init>(Lcom/jess/ui/TwoWayAbsListView;Lcom/jess/ui/a;)V

    invoke-static {v0, v5}, Lcom/jess/ui/TwoWayAbsListView;->a(Lcom/jess/ui/TwoWayAbsListView;Lcom/jess/ui/j;)Lcom/jess/ui/j;

    .line 4924
    :cond_10
    iget-object v0, p0, Lcom/jess/ui/e;->d:Lcom/jess/ui/TwoWayAbsListView;

    invoke-static {v0}, Lcom/jess/ui/TwoWayAbsListView;->k(Lcom/jess/ui/TwoWayAbsListView;)Lcom/jess/ui/j;

    move-result-object v5

    .line 4925
    iput-object v4, v5, Lcom/jess/ui/j;->a:Landroid/view/View;

    .line 4926
    iput v3, v5, Lcom/jess/ui/j;->b:I

    .line 4927
    invoke-virtual {v5}, Lcom/jess/ui/j;->a()V

    .line 4929
    iget-object v0, p0, Lcom/jess/ui/e;->d:Lcom/jess/ui/TwoWayAbsListView;

    iput v3, v0, Lcom/jess/ui/TwoWayAbsListView;->A:I

    .line 4931
    iget-object v0, p0, Lcom/jess/ui/e;->d:Lcom/jess/ui/TwoWayAbsListView;

    iget v0, v0, Lcom/jess/ui/TwoWayAbsListView;->w:I

    if-eqz v0, :cond_11

    iget-object v0, p0, Lcom/jess/ui/e;->d:Lcom/jess/ui/TwoWayAbsListView;

    iget v0, v0, Lcom/jess/ui/TwoWayAbsListView;->w:I

    if-ne v0, v2, :cond_16

    .line 4932
    :cond_11
    iget-object v0, p0, Lcom/jess/ui/e;->d:Lcom/jess/ui/TwoWayAbsListView;

    invoke-virtual {v0}, Lcom/jess/ui/TwoWayAbsListView;->getHandler()Landroid/os/Handler;

    move-result-object v6

    .line 4933
    if-eqz v6, :cond_12

    .line 4934
    iget-object v0, p0, Lcom/jess/ui/e;->d:Lcom/jess/ui/TwoWayAbsListView;

    iget v0, v0, Lcom/jess/ui/TwoWayAbsListView;->w:I

    if-nez v0, :cond_14

    iget-object v0, p0, Lcom/jess/ui/e;->d:Lcom/jess/ui/TwoWayAbsListView;

    .line 4935
    invoke-static {v0}, Lcom/jess/ui/TwoWayAbsListView;->j(Lcom/jess/ui/TwoWayAbsListView;)Ljava/lang/Runnable;

    move-result-object v0

    .line 4934
    :goto_5
    invoke-virtual {v6, v0}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 4937
    :cond_12
    iget-object v0, p0, Lcom/jess/ui/e;->d:Lcom/jess/ui/TwoWayAbsListView;

    iput v1, v0, Lcom/jess/ui/TwoWayAbsListView;->a:I

    .line 4938
    iget-object v0, p0, Lcom/jess/ui/e;->d:Lcom/jess/ui/TwoWayAbsListView;

    iget-boolean v0, v0, Lcom/jess/ui/TwoWayAbsListView;->S:Z

    if-nez v0, :cond_15

    iget-object v0, p0, Lcom/jess/ui/e;->d:Lcom/jess/ui/TwoWayAbsListView;

    iget-object v0, v0, Lcom/jess/ui/TwoWayAbsListView;->c:Landroid/widget/ListAdapter;

    invoke-interface {v0, v3}, Landroid/widget/ListAdapter;->isEnabled(I)Z

    move-result v0

    if-eqz v0, :cond_15

    .line 4939
    iget-object v0, p0, Lcom/jess/ui/e;->d:Lcom/jess/ui/TwoWayAbsListView;

    iput v2, v0, Lcom/jess/ui/TwoWayAbsListView;->w:I

    .line 4940
    iget-object v0, p0, Lcom/jess/ui/e;->d:Lcom/jess/ui/TwoWayAbsListView;

    iget-object v1, p0, Lcom/jess/ui/e;->d:Lcom/jess/ui/TwoWayAbsListView;

    iget v1, v1, Lcom/jess/ui/TwoWayAbsListView;->t:I

    invoke-virtual {v0, v1}, Lcom/jess/ui/TwoWayAbsListView;->setSelectedPositionInt(I)V

    .line 4941
    iget-object v0, p0, Lcom/jess/ui/e;->d:Lcom/jess/ui/TwoWayAbsListView;

    invoke-virtual {v0}, Lcom/jess/ui/TwoWayAbsListView;->d()V

    .line 4942
    invoke-virtual {v4, v2}, Landroid/view/View;->setPressed(Z)V

    .line 4943
    iget-object v0, p0, Lcom/jess/ui/e;->d:Lcom/jess/ui/TwoWayAbsListView;

    invoke-virtual {v0, v4}, Lcom/jess/ui/TwoWayAbsListView;->a(Landroid/view/View;)V

    .line 4944
    iget-object v0, p0, Lcom/jess/ui/e;->d:Lcom/jess/ui/TwoWayAbsListView;

    invoke-virtual {v0, v2}, Lcom/jess/ui/TwoWayAbsListView;->setPressed(Z)V

    .line 4945
    iget-object v0, p0, Lcom/jess/ui/e;->d:Lcom/jess/ui/TwoWayAbsListView;

    iget-object v0, v0, Lcom/jess/ui/TwoWayAbsListView;->e:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_13

    .line 4946
    iget-object v0, p0, Lcom/jess/ui/e;->d:Lcom/jess/ui/TwoWayAbsListView;

    iget-object v0, v0, Lcom/jess/ui/TwoWayAbsListView;->e:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getCurrent()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 4947
    if-eqz v0, :cond_13

    instance-of v1, v0, Landroid/graphics/drawable/TransitionDrawable;

    if-eqz v1, :cond_13

    .line 4948
    check-cast v0, Landroid/graphics/drawable/TransitionDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/TransitionDrawable;->resetTransition()V

    .line 4951
    :cond_13
    iget-object v0, p0, Lcom/jess/ui/e;->d:Lcom/jess/ui/TwoWayAbsListView;

    new-instance v1, Lcom/jess/ui/f;

    invoke-direct {v1, p0, v4, v5}, Lcom/jess/ui/f;-><init>(Lcom/jess/ui/e;Landroid/view/View;Lcom/jess/ui/j;)V

    .line 4960
    invoke-static {}, Landroid/view/ViewConfiguration;->getPressedStateDuration()I

    move-result v3

    int-to-long v4, v3

    .line 4951
    invoke-virtual {v0, v1, v4, v5}, Lcom/jess/ui/TwoWayAbsListView;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/16 :goto_1

    .line 4935
    :cond_14
    iget-object v0, p0, Lcom/jess/ui/e;->d:Lcom/jess/ui/TwoWayAbsListView;

    invoke-static {v0}, Lcom/jess/ui/TwoWayAbsListView;->c(Lcom/jess/ui/TwoWayAbsListView;)Lcom/jess/ui/c;

    move-result-object v0

    goto :goto_5

    .line 4962
    :cond_15
    iget-object v0, p0, Lcom/jess/ui/e;->d:Lcom/jess/ui/TwoWayAbsListView;

    iput v7, v0, Lcom/jess/ui/TwoWayAbsListView;->w:I

    goto/16 :goto_1

    .line 4965
    :cond_16
    iget-object v0, p0, Lcom/jess/ui/e;->d:Lcom/jess/ui/TwoWayAbsListView;

    iget-boolean v0, v0, Lcom/jess/ui/TwoWayAbsListView;->S:Z

    if-nez v0, :cond_17

    iget-object v0, p0, Lcom/jess/ui/e;->d:Lcom/jess/ui/TwoWayAbsListView;

    iget-object v0, v0, Lcom/jess/ui/TwoWayAbsListView;->c:Landroid/widget/ListAdapter;

    invoke-interface {v0, v3}, Landroid/widget/ListAdapter;->isEnabled(I)Z

    move-result v0

    if-eqz v0, :cond_17

    .line 4966
    iget-object v0, p0, Lcom/jess/ui/e;->d:Lcom/jess/ui/TwoWayAbsListView;

    invoke-virtual {v0, v5}, Lcom/jess/ui/TwoWayAbsListView;->post(Ljava/lang/Runnable;)Z

    .line 4969
    :cond_17
    iget-object v0, p0, Lcom/jess/ui/e;->d:Lcom/jess/ui/TwoWayAbsListView;

    iput v7, v0, Lcom/jess/ui/TwoWayAbsListView;->w:I

    goto/16 :goto_4

    .line 4972
    :pswitch_6
    iget-object v0, p0, Lcom/jess/ui/e;->d:Lcom/jess/ui/TwoWayAbsListView;

    invoke-virtual {v0}, Lcom/jess/ui/TwoWayAbsListView;->getChildCount()I

    move-result v0

    .line 4973
    if-lez v0, :cond_1b

    .line 4974
    iget-object v3, p0, Lcom/jess/ui/e;->d:Lcom/jess/ui/TwoWayAbsListView;

    iget v3, v3, Lcom/jess/ui/TwoWayAbsListView;->H:I

    if-nez v3, :cond_18

    iget-object v3, p0, Lcom/jess/ui/e;->d:Lcom/jess/ui/TwoWayAbsListView;

    invoke-virtual {v3, v1}, Lcom/jess/ui/TwoWayAbsListView;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->getLeft()I

    move-result v3

    iget-object v4, p0, Lcom/jess/ui/e;->d:Lcom/jess/ui/TwoWayAbsListView;

    iget-object v4, v4, Lcom/jess/ui/TwoWayAbsListView;->m:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->left:I

    if-lt v3, v4, :cond_18

    iget-object v3, p0, Lcom/jess/ui/e;->d:Lcom/jess/ui/TwoWayAbsListView;

    iget v3, v3, Lcom/jess/ui/TwoWayAbsListView;->H:I

    add-int/2addr v3, v0

    iget-object v4, p0, Lcom/jess/ui/e;->d:Lcom/jess/ui/TwoWayAbsListView;

    iget v4, v4, Lcom/jess/ui/TwoWayAbsListView;->aa:I

    if-ge v3, v4, :cond_18

    iget-object v3, p0, Lcom/jess/ui/e;->d:Lcom/jess/ui/TwoWayAbsListView;

    add-int/lit8 v0, v0, -0x1

    .line 4976
    invoke-virtual {v3, v0}, Lcom/jess/ui/TwoWayAbsListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getRight()I

    move-result v0

    iget-object v3, p0, Lcom/jess/ui/e;->d:Lcom/jess/ui/TwoWayAbsListView;

    .line 4977
    invoke-virtual {v3}, Lcom/jess/ui/TwoWayAbsListView;->getWidth()I

    move-result v3

    iget-object v4, p0, Lcom/jess/ui/e;->d:Lcom/jess/ui/TwoWayAbsListView;

    iget-object v4, v4, Lcom/jess/ui/TwoWayAbsListView;->m:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->right:I

    sub-int/2addr v3, v4

    if-gt v0, v3, :cond_18

    .line 4978
    iget-object v0, p0, Lcom/jess/ui/e;->d:Lcom/jess/ui/TwoWayAbsListView;

    iput v7, v0, Lcom/jess/ui/TwoWayAbsListView;->w:I

    .line 4979
    invoke-virtual {p0, v1}, Lcom/jess/ui/e;->b(I)V

    goto/16 :goto_4

    .line 4981
    :cond_18
    iget-object v0, p0, Lcom/jess/ui/e;->d:Lcom/jess/ui/TwoWayAbsListView;

    invoke-static {v0}, Lcom/jess/ui/TwoWayAbsListView;->i(Lcom/jess/ui/TwoWayAbsListView;)Landroid/view/VelocityTracker;

    move-result-object v0

    .line 4982
    const/16 v3, 0x3e8

    invoke-virtual {v0, v3}, Landroid/view/VelocityTracker;->computeCurrentVelocity(I)V

    .line 4983
    invoke-virtual {v0}, Landroid/view/VelocityTracker;->getXVelocity()F

    move-result v0

    float-to-int v0, v0

    .line 4985
    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v3

    iget-object v4, p0, Lcom/jess/ui/e;->d:Lcom/jess/ui/TwoWayAbsListView;

    invoke-static {v4}, Lcom/jess/ui/TwoWayAbsListView;->l(Lcom/jess/ui/TwoWayAbsListView;)I

    move-result v4

    if-le v3, v4, :cond_1a

    .line 4986
    iget-object v3, p0, Lcom/jess/ui/e;->f:Lcom/jess/ui/p;

    if-nez v3, :cond_19

    .line 4987
    new-instance v3, Lcom/jess/ui/g;

    invoke-direct {v3, p0, v8}, Lcom/jess/ui/g;-><init>(Lcom/jess/ui/e;Lcom/jess/ui/a;)V

    iput-object v3, p0, Lcom/jess/ui/e;->f:Lcom/jess/ui/p;

    .line 4989
    :cond_19
    const/4 v3, 0x2

    invoke-virtual {p0, v3}, Lcom/jess/ui/e;->b(I)V

    .line 4991
    iget-object v3, p0, Lcom/jess/ui/e;->f:Lcom/jess/ui/p;

    neg-int v0, v0

    invoke-virtual {v3, v0}, Lcom/jess/ui/p;->a(I)V

    goto/16 :goto_4

    .line 4993
    :cond_1a
    iget-object v0, p0, Lcom/jess/ui/e;->d:Lcom/jess/ui/TwoWayAbsListView;

    iput v7, v0, Lcom/jess/ui/TwoWayAbsListView;->w:I

    .line 4994
    invoke-virtual {p0, v1}, Lcom/jess/ui/e;->b(I)V

    goto/16 :goto_4

    .line 4998
    :cond_1b
    iget-object v0, p0, Lcom/jess/ui/e;->d:Lcom/jess/ui/TwoWayAbsListView;

    iput v7, v0, Lcom/jess/ui/TwoWayAbsListView;->w:I

    .line 4999
    invoke-virtual {p0, v1}, Lcom/jess/ui/e;->b(I)V

    goto/16 :goto_4

    .line 5031
    :pswitch_7
    iget-object v0, p0, Lcom/jess/ui/e;->d:Lcom/jess/ui/TwoWayAbsListView;

    iput v7, v0, Lcom/jess/ui/TwoWayAbsListView;->w:I

    .line 5032
    iget-object v0, p0, Lcom/jess/ui/e;->d:Lcom/jess/ui/TwoWayAbsListView;

    invoke-virtual {v0, v1}, Lcom/jess/ui/TwoWayAbsListView;->setPressed(Z)V

    .line 5033
    iget-object v0, p0, Lcom/jess/ui/e;->d:Lcom/jess/ui/TwoWayAbsListView;

    iget-object v3, p0, Lcom/jess/ui/e;->d:Lcom/jess/ui/TwoWayAbsListView;

    iget v3, v3, Lcom/jess/ui/TwoWayAbsListView;->t:I

    iget-object v4, p0, Lcom/jess/ui/e;->d:Lcom/jess/ui/TwoWayAbsListView;

    iget v4, v4, Lcom/jess/ui/TwoWayAbsListView;->H:I

    sub-int/2addr v3, v4

    invoke-virtual {v0, v3}, Lcom/jess/ui/TwoWayAbsListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 5034
    if-eqz v0, :cond_1c

    .line 5035
    invoke-virtual {v0, v1}, Landroid/view/View;->setPressed(Z)V

    .line 5037
    :cond_1c
    invoke-virtual {p0}, Lcom/jess/ui/e;->c()V

    .line 5039
    iget-object v0, p0, Lcom/jess/ui/e;->d:Lcom/jess/ui/TwoWayAbsListView;

    invoke-virtual {v0}, Lcom/jess/ui/TwoWayAbsListView;->getHandler()Landroid/os/Handler;

    move-result-object v0

    .line 5040
    if-eqz v0, :cond_1d

    .line 5041
    iget-object v1, p0, Lcom/jess/ui/e;->d:Lcom/jess/ui/TwoWayAbsListView;

    invoke-static {v1}, Lcom/jess/ui/TwoWayAbsListView;->c(Lcom/jess/ui/TwoWayAbsListView;)Lcom/jess/ui/c;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 5044
    :cond_1d
    iget-object v0, p0, Lcom/jess/ui/e;->d:Lcom/jess/ui/TwoWayAbsListView;

    invoke-static {v0}, Lcom/jess/ui/TwoWayAbsListView;->i(Lcom/jess/ui/TwoWayAbsListView;)Landroid/view/VelocityTracker;

    move-result-object v0

    if-eqz v0, :cond_1e

    .line 5045
    iget-object v0, p0, Lcom/jess/ui/e;->d:Lcom/jess/ui/TwoWayAbsListView;

    invoke-static {v0}, Lcom/jess/ui/TwoWayAbsListView;->i(Lcom/jess/ui/TwoWayAbsListView;)Landroid/view/VelocityTracker;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->recycle()V

    .line 5046
    iget-object v0, p0, Lcom/jess/ui/e;->d:Lcom/jess/ui/TwoWayAbsListView;

    invoke-static {v0, v8}, Lcom/jess/ui/TwoWayAbsListView;->a(Lcom/jess/ui/TwoWayAbsListView;Landroid/view/VelocityTracker;)Landroid/view/VelocityTracker;

    .line 5049
    :cond_1e
    iget-object v0, p0, Lcom/jess/ui/e;->d:Lcom/jess/ui/TwoWayAbsListView;

    invoke-static {v0, v7}, Lcom/jess/ui/TwoWayAbsListView;->c(Lcom/jess/ui/TwoWayAbsListView;I)I

    goto/16 :goto_1

    :cond_1f
    move v0, v3

    goto/16 :goto_2

    .line 4809
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_4
        :pswitch_1
        :pswitch_7
    .end packed-switch

    .line 4859
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_3
    .end packed-switch

    .line 4909
    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method
