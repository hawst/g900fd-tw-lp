.class Lcom/jess/ui/ag;
.super Lcom/jess/ui/ae;


# instance fields
.field final synthetic b:Lcom/jess/ui/TwoWayGridView;


# direct methods
.method private constructor <init>(Lcom/jess/ui/TwoWayGridView;)V
    .locals 1

    .prologue
    .line 843
    iput-object p1, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/jess/ui/ae;-><init>(Lcom/jess/ui/TwoWayGridView;Lcom/jess/ui/ad;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/jess/ui/TwoWayGridView;Lcom/jess/ui/ad;)V
    .locals 0

    .prologue
    .line 843
    invoke-direct {p0, p1}, Lcom/jess/ui/ag;-><init>(Lcom/jess/ui/TwoWayGridView;)V

    return-void
.end method

.method private a(IIII)I
    .locals 2

    .prologue
    .line 1345
    .line 1346
    add-int v0, p4, p3

    add-int/lit8 v0, v0, -0x1

    iget-object v1, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    iget v1, v1, Lcom/jess/ui/TwoWayGridView;->aa:I

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_0

    .line 1347
    sub-int/2addr p1, p2

    .line 1349
    :cond_0
    return p1
.end method

.method private a(IIZ)Landroid/view/View;
    .locals 14

    .prologue
    .line 952
    iget-object v0, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-static {v0}, Lcom/jess/ui/TwoWayGridView;->d(Lcom/jess/ui/TwoWayGridView;)I

    move-result v10

    .line 953
    iget-object v0, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-static {v0}, Lcom/jess/ui/TwoWayGridView;->e(Lcom/jess/ui/TwoWayGridView;)I

    move-result v7

    .line 956
    iget-object v0, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    iget-object v0, v0, Lcom/jess/ui/TwoWayGridView;->m:Landroid/graphics/Rect;

    iget v1, v0, Landroid/graphics/Rect;->left:I

    iget-object v0, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    .line 957
    invoke-static {v0}, Lcom/jess/ui/TwoWayGridView;->f(Lcom/jess/ui/TwoWayGridView;)I

    move-result v0

    const/4 v2, 0x3

    if-ne v0, v2, :cond_1

    move v0, v7

    :goto_0
    add-int/2addr v0, v1

    .line 959
    iget-object v1, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    iget-boolean v1, v1, Lcom/jess/ui/TwoWayGridView;->y:Z

    if-nez v1, :cond_2

    .line 960
    iget-object v1, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-static {v1}, Lcom/jess/ui/TwoWayGridView;->a(Lcom/jess/ui/TwoWayGridView;)I

    move-result v1

    add-int/2addr v1, p1

    iget-object v2, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    iget v2, v2, Lcom/jess/ui/TwoWayGridView;->aa:I

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    move v8, v1

    .line 970
    :goto_1
    const/4 v9, 0x0

    .line 972
    iget-object v1, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-virtual {v1}, Lcom/jess/ui/TwoWayGridView;->g()Z

    move-result v11

    .line 973
    iget-object v1, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-virtual {v1}, Lcom/jess/ui/TwoWayGridView;->f()Z

    move-result v12

    .line 974
    iget-object v1, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    iget v13, v1, Lcom/jess/ui/TwoWayGridView;->V:I

    .line 976
    const/4 v2, 0x0

    move v1, p1

    move v4, v0

    .line 977
    :goto_2
    if-ge v1, v8, :cond_5

    .line 979
    if-ne v1, v13, :cond_3

    const/4 v5, 0x1

    .line 982
    :goto_3
    if-eqz p3, :cond_4

    const/4 v6, -0x1

    :goto_4
    move-object v0, p0

    move/from16 v2, p2

    move/from16 v3, p3

    .line 983
    invoke-virtual/range {v0 .. v6}, Lcom/jess/ui/ag;->a(IIZIZI)Landroid/view/View;

    move-result-object v2

    .line 985
    add-int v0, v4, v10

    .line 986
    add-int/lit8 v3, v8, -0x1

    if-ge v1, v3, :cond_8

    .line 987
    add-int/2addr v0, v7

    move v3, v0

    .line 990
    :goto_5
    if-eqz v5, :cond_7

    if-nez v11, :cond_0

    if-eqz v12, :cond_7

    :cond_0
    move-object v0, v2

    .line 977
    :goto_6
    add-int/lit8 v1, v1, 0x1

    move-object v9, v0

    move v4, v3

    goto :goto_2

    .line 957
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 962
    :cond_2
    add-int/lit8 v1, p1, 0x1

    .line 963
    const/4 v2, 0x0

    iget-object v3, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-static {v3}, Lcom/jess/ui/TwoWayGridView;->a(Lcom/jess/ui/TwoWayGridView;)I

    move-result v3

    sub-int v3, p1, v3

    add-int/lit8 v3, v3, 0x1

    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result p1

    .line 965
    sub-int v2, v1, p1

    iget-object v3, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-static {v3}, Lcom/jess/ui/TwoWayGridView;->a(Lcom/jess/ui/TwoWayGridView;)I

    move-result v3

    if-ge v2, v3, :cond_9

    .line 966
    iget-object v2, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-static {v2}, Lcom/jess/ui/TwoWayGridView;->a(Lcom/jess/ui/TwoWayGridView;)I

    move-result v2

    sub-int v3, v1, p1

    sub-int/2addr v2, v3

    add-int v3, v10, v7

    mul-int/2addr v2, v3

    add-int/2addr v0, v2

    move v8, v1

    goto :goto_1

    .line 979
    :cond_3
    const/4 v5, 0x0

    goto :goto_3

    .line 982
    :cond_4
    sub-int v6, v1, p1

    goto :goto_4

    .line 995
    :cond_5
    iget-object v0, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-static {v0, v2}, Lcom/jess/ui/TwoWayGridView;->a(Lcom/jess/ui/TwoWayGridView;Landroid/view/View;)Landroid/view/View;

    .line 997
    if-eqz v9, :cond_6

    .line 998
    iget-object v0, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    iget-object v1, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-static {v1}, Lcom/jess/ui/TwoWayGridView;->c(Lcom/jess/ui/TwoWayGridView;)Landroid/view/View;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/jess/ui/TwoWayGridView;->b(Lcom/jess/ui/TwoWayGridView;Landroid/view/View;)Landroid/view/View;

    .line 1001
    :cond_6
    return-object v9

    :cond_7
    move-object v0, v9

    goto :goto_6

    :cond_8
    move v3, v0

    goto :goto_5

    :cond_9
    move v8, v1

    goto/16 :goto_1
.end method

.method private a(III)V
    .locals 4

    .prologue
    .line 1191
    iget-object v0, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    iget v0, v0, Lcom/jess/ui/TwoWayGridView;->H:I

    add-int/2addr v0, p3

    add-int/lit8 v0, v0, -0x1

    .line 1192
    iget-object v1, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    iget v1, v1, Lcom/jess/ui/TwoWayGridView;->aa:I

    add-int/lit8 v1, v1, -0x1

    if-ne v0, v1, :cond_3

    if-lez p3, :cond_3

    .line 1194
    iget-object v0, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    add-int/lit8 v1, p3, -0x1

    invoke-virtual {v0, v1}, Lcom/jess/ui/TwoWayGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 1197
    invoke-virtual {v0}, Landroid/view/View;->getBottom()I

    move-result v0

    .line 1199
    iget-object v1, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-virtual {v1}, Lcom/jess/ui/TwoWayGridView;->getBottom()I

    move-result v1

    iget-object v2, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-virtual {v2}, Lcom/jess/ui/TwoWayGridView;->getTop()I

    move-result v2

    sub-int/2addr v1, v2

    iget-object v2, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    iget-object v2, v2, Lcom/jess/ui/TwoWayGridView;->m:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->bottom:I

    sub-int/2addr v1, v2

    .line 1203
    sub-int v0, v1, v0

    .line 1205
    iget-object v1, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/jess/ui/TwoWayGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 1206
    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v2

    .line 1210
    if-lez v0, :cond_3

    iget-object v3, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    iget v3, v3, Lcom/jess/ui/TwoWayGridView;->H:I

    if-gtz v3, :cond_0

    iget-object v3, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    iget-object v3, v3, Lcom/jess/ui/TwoWayGridView;->m:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->top:I

    if-ge v2, v3, :cond_3

    .line 1211
    :cond_0
    iget-object v3, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    iget v3, v3, Lcom/jess/ui/TwoWayGridView;->H:I

    if-nez v3, :cond_1

    .line 1213
    iget-object v3, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    iget-object v3, v3, Lcom/jess/ui/TwoWayGridView;->m:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->top:I

    sub-int v2, v3, v2

    invoke-static {v0, v2}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 1217
    :cond_1
    iget-object v2, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-virtual {v2, v0}, Lcom/jess/ui/TwoWayGridView;->e(I)V

    .line 1218
    iget-object v0, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    iget v0, v0, Lcom/jess/ui/TwoWayGridView;->H:I

    if-lez v0, :cond_3

    .line 1221
    iget-object v0, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    iget v0, v0, Lcom/jess/ui/TwoWayGridView;->H:I

    iget-object v2, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    iget-boolean v2, v2, Lcom/jess/ui/TwoWayGridView;->y:Z

    if-eqz v2, :cond_2

    const/4 p1, 0x1

    :cond_2
    sub-int/2addr v0, p1

    .line 1222
    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v1

    sub-int/2addr v1, p2

    .line 1221
    invoke-direct {p0, v0, v1}, Lcom/jess/ui/ag;->d(II)Landroid/view/View;

    .line 1224
    invoke-direct {p0}, Lcom/jess/ui/ag;->b()V

    .line 1228
    :cond_3
    return-void
.end method

.method private a(Landroid/view/View;II)V
    .locals 2

    .prologue
    .line 1382
    invoke-virtual {p1}, Landroid/view/View;->getBottom()I

    move-result v0

    if-le v0, p3, :cond_0

    .line 1386
    invoke-virtual {p1}, Landroid/view/View;->getTop()I

    move-result v0

    sub-int/2addr v0, p2

    .line 1390
    invoke-virtual {p1}, Landroid/view/View;->getBottom()I

    move-result v1

    sub-int/2addr v1, p3

    .line 1391
    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 1394
    iget-object v1, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    neg-int v0, v0

    invoke-virtual {v1, v0}, Lcom/jess/ui/TwoWayGridView;->e(I)V

    .line 1396
    :cond_0
    return-void
.end method

.method private a(Landroid/view/View;IIZIZZI)V
    .locals 10

    .prologue
    .line 1894
    if-eqz p6, :cond_5

    iget-object v1, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-virtual {v1}, Lcom/jess/ui/TwoWayGridView;->g()Z

    move-result v1

    if-eqz v1, :cond_5

    const/4 v1, 0x1

    move v2, v1

    .line 1895
    :goto_0
    invoke-virtual {p1}, Landroid/view/View;->isSelected()Z

    move-result v1

    if-eq v2, v1, :cond_6

    const/4 v1, 0x1

    move v3, v1

    .line 1896
    :goto_1
    iget-object v1, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    iget v1, v1, Lcom/jess/ui/TwoWayGridView;->w:I

    .line 1897
    if-lez v1, :cond_7

    const/4 v4, 0x3

    if-ge v1, v4, :cond_7

    iget-object v1, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    iget v1, v1, Lcom/jess/ui/TwoWayGridView;->t:I

    if-ne v1, p2, :cond_7

    const/4 v1, 0x1

    move v4, v1

    .line 1899
    :goto_2
    invoke-virtual {p1}, Landroid/view/View;->isPressed()Z

    move-result v1

    if-eq v4, v1, :cond_8

    const/4 v1, 0x1

    move v6, v1

    .line 1901
    :goto_3
    if-eqz p7, :cond_0

    if-nez v3, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->isLayoutRequested()Z

    move-result v1

    if-eqz v1, :cond_9

    :cond_0
    const/4 v1, 0x1

    move v5, v1

    .line 1905
    :goto_4
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Lcom/jess/ui/h;

    .line 1906
    if-nez v1, :cond_1

    .line 1907
    new-instance v1, Lcom/jess/ui/h;

    const/4 v7, -0x1

    const/4 v8, -0x2

    const/4 v9, 0x0

    invoke-direct {v1, v7, v8, v9}, Lcom/jess/ui/h;-><init>(III)V

    .line 1910
    :cond_1
    iget-object v7, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    iget-object v7, v7, Lcom/jess/ui/TwoWayGridView;->c:Landroid/widget/ListAdapter;

    invoke-interface {v7, p2}, Landroid/widget/ListAdapter;->getItemViewType(I)I

    move-result v7

    iput v7, v1, Lcom/jess/ui/h;->a:I

    .line 1912
    if-eqz p7, :cond_a

    iget-boolean v7, v1, Lcom/jess/ui/h;->b:Z

    if-nez v7, :cond_a

    .line 1913
    iget-object v7, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    move/from16 v0, p8

    invoke-static {v7, p1, v0, v1}, Lcom/jess/ui/TwoWayGridView;->a(Lcom/jess/ui/TwoWayGridView;Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    .line 1919
    :goto_5
    if-eqz v3, :cond_2

    .line 1920
    invoke-virtual {p1, v2}, Landroid/view/View;->setSelected(Z)V

    .line 1921
    if-eqz v2, :cond_2

    .line 1922
    iget-object v2, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-virtual {v2}, Lcom/jess/ui/TwoWayGridView;->requestFocus()Z

    .line 1926
    :cond_2
    if-eqz v6, :cond_3

    .line 1927
    invoke-virtual {p1, v4}, Landroid/view/View;->setPressed(Z)V

    .line 1930
    :cond_3
    if-eqz v5, :cond_b

    .line 1931
    const/4 v2, 0x0

    const/4 v3, 0x0

    .line 1932
    invoke-static {v2, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    const/4 v3, 0x0

    iget v4, v1, Lcom/jess/ui/h;->height:I

    .line 1931
    invoke-static {v2, v3, v4}, Landroid/view/ViewGroup;->getChildMeasureSpec(III)I

    move-result v2

    .line 1934
    iget-object v3, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    .line 1935
    invoke-static {v3}, Lcom/jess/ui/TwoWayGridView;->d(Lcom/jess/ui/TwoWayGridView;)I

    move-result v3

    const/high16 v4, 0x40000000    # 2.0f

    invoke-static {v3, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    const/4 v4, 0x0

    iget v1, v1, Lcom/jess/ui/h;->width:I

    .line 1934
    invoke-static {v3, v4, v1}, Landroid/view/ViewGroup;->getChildMeasureSpec(III)I

    move-result v1

    .line 1936
    invoke-virtual {p1, v1, v2}, Landroid/view/View;->measure(II)V

    .line 1941
    :goto_6
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    .line 1942
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v2

    .line 1945
    if-eqz p4, :cond_c

    .line 1947
    :goto_7
    iget-object v3, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-static {v3}, Lcom/jess/ui/TwoWayGridView;->m(Lcom/jess/ui/TwoWayGridView;)I

    move-result v3

    and-int/lit8 v3, v3, 0x7

    packed-switch v3, :pswitch_data_0

    .line 1962
    :goto_8
    :pswitch_0
    if-eqz v5, :cond_d

    .line 1963
    add-int/2addr v1, p5

    .line 1964
    add-int/2addr v2, p3

    .line 1965
    invoke-virtual {p1, p5, p3, v1, v2}, Landroid/view/View;->layout(IIII)V

    .line 1971
    :goto_9
    iget-object v1, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    iget-boolean v1, v1, Lcom/jess/ui/TwoWayGridView;->s:Z

    if-eqz v1, :cond_4

    .line 1972
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, Landroid/view/View;->setDrawingCacheEnabled(Z)V

    .line 1974
    :cond_4
    return-void

    .line 1894
    :cond_5
    const/4 v1, 0x0

    move v2, v1

    goto/16 :goto_0

    .line 1895
    :cond_6
    const/4 v1, 0x0

    move v3, v1

    goto/16 :goto_1

    .line 1897
    :cond_7
    const/4 v1, 0x0

    move v4, v1

    goto/16 :goto_2

    .line 1899
    :cond_8
    const/4 v1, 0x0

    move v6, v1

    goto/16 :goto_3

    .line 1901
    :cond_9
    const/4 v1, 0x0

    move v5, v1

    goto/16 :goto_4

    .line 1915
    :cond_a
    const/4 v7, 0x0

    iput-boolean v7, v1, Lcom/jess/ui/h;->b:Z

    .line 1916
    iget-object v7, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    const/4 v8, 0x1

    move/from16 v0, p8

    invoke-static {v7, p1, v0, v1, v8}, Lcom/jess/ui/TwoWayGridView;->a(Lcom/jess/ui/TwoWayGridView;Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;Z)Z

    goto :goto_5

    .line 1938
    :cond_b
    iget-object v1, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-static {v1, p1}, Lcom/jess/ui/TwoWayGridView;->c(Lcom/jess/ui/TwoWayGridView;Landroid/view/View;)V

    goto :goto_6

    .line 1945
    :cond_c
    sub-int/2addr p3, v2

    goto :goto_7

    .line 1952
    :pswitch_1
    iget-object v3, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-static {v3}, Lcom/jess/ui/TwoWayGridView;->d(Lcom/jess/ui/TwoWayGridView;)I

    move-result v3

    sub-int/2addr v3, v1

    div-int/lit8 v3, v3, 0x2

    add-int/2addr p5, v3

    .line 1953
    goto :goto_8

    .line 1955
    :pswitch_2
    iget-object v3, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-static {v3}, Lcom/jess/ui/TwoWayGridView;->d(Lcom/jess/ui/TwoWayGridView;)I

    move-result v3

    add-int/2addr v3, p5

    sub-int p5, v3, v1

    .line 1956
    goto :goto_8

    .line 1967
    :cond_d
    invoke-virtual {p1}, Landroid/view/View;->getLeft()I

    move-result v1

    sub-int v1, p5, v1

    invoke-virtual {p1, v1}, Landroid/view/View;->offsetLeftAndRight(I)V

    .line 1968
    invoke-virtual {p1}, Landroid/view/View;->getTop()I

    move-result v1

    sub-int v1, p3, v1

    invoke-virtual {p1, v1}, Landroid/view/View;->offsetTopAndBottom(I)V

    goto :goto_9

    .line 1947
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method private b()V
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 1833
    iget-object v1, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-virtual {v1}, Lcom/jess/ui/TwoWayGridView;->getChildCount()I

    move-result v2

    .line 1835
    if-lez v2, :cond_2

    .line 1839
    iget-object v1, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    iget-boolean v1, v1, Lcom/jess/ui/TwoWayGridView;->y:Z

    if-nez v1, :cond_3

    .line 1842
    iget-object v1, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-virtual {v1, v0}, Lcom/jess/ui/TwoWayGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 1843
    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v1

    iget-object v2, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    iget-object v2, v2, Lcom/jess/ui/TwoWayGridView;->m:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    sub-int/2addr v1, v2

    .line 1844
    iget-object v2, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    iget v2, v2, Lcom/jess/ui/TwoWayGridView;->H:I

    if-eqz v2, :cond_0

    .line 1847
    iget-object v2, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-static {v2}, Lcom/jess/ui/TwoWayGridView;->b(Lcom/jess/ui/TwoWayGridView;)I

    move-result v2

    sub-int/2addr v1, v2

    .line 1849
    :cond_0
    if-gez v1, :cond_5

    .line 1870
    :cond_1
    :goto_0
    if-eqz v0, :cond_2

    .line 1871
    iget-object v1, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    neg-int v0, v0

    invoke-virtual {v1, v0}, Lcom/jess/ui/TwoWayGridView;->e(I)V

    .line 1874
    :cond_2
    return-void

    .line 1855
    :cond_3
    iget-object v1, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    add-int/lit8 v3, v2, -0x1

    invoke-virtual {v1, v3}, Lcom/jess/ui/TwoWayGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 1856
    invoke-virtual {v1}, Landroid/view/View;->getBottom()I

    move-result v1

    iget-object v3, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-virtual {v3}, Lcom/jess/ui/TwoWayGridView;->getHeight()I

    move-result v3

    iget-object v4, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    iget-object v4, v4, Lcom/jess/ui/TwoWayGridView;->m:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->bottom:I

    sub-int/2addr v3, v4

    sub-int/2addr v1, v3

    .line 1858
    iget-object v3, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    iget v3, v3, Lcom/jess/ui/TwoWayGridView;->H:I

    add-int/2addr v2, v3

    iget-object v3, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    iget v3, v3, Lcom/jess/ui/TwoWayGridView;->aa:I

    if-ge v2, v3, :cond_4

    .line 1861
    iget-object v2, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-static {v2}, Lcom/jess/ui/TwoWayGridView;->b(Lcom/jess/ui/TwoWayGridView;)I

    move-result v2

    add-int/2addr v1, v2

    .line 1864
    :cond_4
    if-gtz v1, :cond_1

    :cond_5
    move v0, v1

    goto :goto_0
.end method

.method private b(III)V
    .locals 6

    .prologue
    .line 1232
    iget-object v0, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    iget v0, v0, Lcom/jess/ui/TwoWayGridView;->H:I

    if-nez v0, :cond_3

    if-lez p3, :cond_3

    .line 1234
    iget-object v0, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/jess/ui/TwoWayGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 1237
    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v0

    .line 1240
    iget-object v1, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    iget-object v1, v1, Lcom/jess/ui/TwoWayGridView;->m:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->top:I

    .line 1243
    iget-object v2, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-virtual {v2}, Lcom/jess/ui/TwoWayGridView;->getBottom()I

    move-result v2

    iget-object v3, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-virtual {v3}, Lcom/jess/ui/TwoWayGridView;->getTop()I

    move-result v3

    sub-int/2addr v2, v3

    iget-object v3, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    iget-object v3, v3, Lcom/jess/ui/TwoWayGridView;->m:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->bottom:I

    sub-int/2addr v2, v3

    .line 1247
    sub-int/2addr v0, v1

    .line 1248
    iget-object v1, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    add-int/lit8 v3, p3, -0x1

    invoke-virtual {v1, v3}, Lcom/jess/ui/TwoWayGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 1249
    invoke-virtual {v1}, Landroid/view/View;->getBottom()I

    move-result v3

    .line 1250
    iget-object v4, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    iget v4, v4, Lcom/jess/ui/TwoWayGridView;->H:I

    add-int/2addr v4, p3

    add-int/lit8 v4, v4, -0x1

    .line 1254
    if-lez v0, :cond_3

    iget-object v5, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    iget v5, v5, Lcom/jess/ui/TwoWayGridView;->aa:I

    add-int/lit8 v5, v5, -0x1

    if-lt v4, v5, :cond_0

    if-le v3, v2, :cond_3

    .line 1255
    :cond_0
    iget-object v5, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    iget v5, v5, Lcom/jess/ui/TwoWayGridView;->aa:I

    add-int/lit8 v5, v5, -0x1

    if-ne v4, v5, :cond_1

    .line 1257
    sub-int v2, v3, v2

    invoke-static {v0, v2}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 1261
    :cond_1
    iget-object v2, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    neg-int v0, v0

    invoke-virtual {v2, v0}, Lcom/jess/ui/TwoWayGridView;->e(I)V

    .line 1262
    iget-object v0, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    iget v0, v0, Lcom/jess/ui/TwoWayGridView;->aa:I

    add-int/lit8 v0, v0, -0x1

    if-ge v4, v0, :cond_3

    .line 1265
    iget-object v0, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    iget-boolean v0, v0, Lcom/jess/ui/TwoWayGridView;->y:Z

    if-nez v0, :cond_2

    const/4 p1, 0x1

    :cond_2
    add-int v0, v4, p1

    .line 1266
    invoke-virtual {v1}, Landroid/view/View;->getBottom()I

    move-result v1

    add-int/2addr v1, p2

    .line 1265
    invoke-direct {p0, v0, v1}, Lcom/jess/ui/ag;->c(II)Landroid/view/View;

    .line 1268
    invoke-direct {p0}, Lcom/jess/ui/ag;->b()V

    .line 1272
    :cond_3
    return-void
.end method

.method private b(Landroid/view/View;II)V
    .locals 2

    .prologue
    .line 1410
    invoke-virtual {p1}, Landroid/view/View;->getTop()I

    move-result v0

    if-ge v0, p2, :cond_0

    .line 1413
    invoke-virtual {p1}, Landroid/view/View;->getTop()I

    move-result v0

    sub-int v0, p2, v0

    .line 1417
    invoke-virtual {p1}, Landroid/view/View;->getBottom()I

    move-result v1

    sub-int v1, p3, v1

    .line 1418
    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 1421
    iget-object v1, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-virtual {v1, v0}, Lcom/jess/ui/TwoWayGridView;->e(I)V

    .line 1423
    :cond_0
    return-void
.end method

.method private c(I)Landroid/view/View;
    .locals 4

    .prologue
    .line 1051
    iget-object v0, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    iget-object v1, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    iget v1, v1, Lcom/jess/ui/TwoWayGridView;->H:I

    iget-object v2, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    iget v2, v2, Lcom/jess/ui/TwoWayGridView;->V:I

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    iput v1, v0, Lcom/jess/ui/TwoWayGridView;->H:I

    .line 1052
    iget-object v0, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    iget-object v1, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    iget v1, v1, Lcom/jess/ui/TwoWayGridView;->H:I

    iget-object v2, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    iget v2, v2, Lcom/jess/ui/TwoWayGridView;->aa:I

    add-int/lit8 v2, v2, -0x1

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    iput v1, v0, Lcom/jess/ui/TwoWayGridView;->H:I

    .line 1053
    iget-object v0, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    iget v0, v0, Lcom/jess/ui/TwoWayGridView;->H:I

    if-gez v0, :cond_0

    .line 1054
    iget-object v0, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    const/4 v1, 0x0

    iput v1, v0, Lcom/jess/ui/TwoWayGridView;->H:I

    .line 1056
    :cond_0
    iget-object v0, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    iget v1, v0, Lcom/jess/ui/TwoWayGridView;->H:I

    iget-object v2, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    iget v2, v2, Lcom/jess/ui/TwoWayGridView;->H:I

    iget-object v3, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-static {v3}, Lcom/jess/ui/TwoWayGridView;->a(Lcom/jess/ui/TwoWayGridView;)I

    move-result v3

    rem-int/2addr v2, v3

    sub-int/2addr v1, v2

    iput v1, v0, Lcom/jess/ui/TwoWayGridView;->H:I

    .line 1057
    iget-object v0, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    iget v0, v0, Lcom/jess/ui/TwoWayGridView;->H:I

    invoke-direct {p0, v0, p1}, Lcom/jess/ui/ag;->c(II)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method private c(II)Landroid/view/View;
    .locals 4

    .prologue
    .line 930
    const/4 v1, 0x0

    .line 932
    iget-object v0, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-virtual {v0}, Lcom/jess/ui/TwoWayGridView;->getBottom()I

    move-result v0

    iget-object v2, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-virtual {v2}, Lcom/jess/ui/TwoWayGridView;->getTop()I

    move-result v2

    sub-int/2addr v0, v2

    iget-object v2, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    iget-object v2, v2, Lcom/jess/ui/TwoWayGridView;->m:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->bottom:I

    sub-int v2, v0, v2

    .line 934
    :goto_0
    if-ge p2, v2, :cond_0

    iget-object v0, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    iget v0, v0, Lcom/jess/ui/TwoWayGridView;->aa:I

    if-ge p1, v0, :cond_0

    .line 935
    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, v0}, Lcom/jess/ui/ag;->a(IIZ)Landroid/view/View;

    move-result-object v0

    .line 936
    if-eqz v0, :cond_1

    .line 942
    :goto_1
    iget-object v1, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-static {v1}, Lcom/jess/ui/TwoWayGridView;->c(Lcom/jess/ui/TwoWayGridView;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getBottom()I

    move-result v1

    iget-object v3, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-static {v3}, Lcom/jess/ui/TwoWayGridView;->b(Lcom/jess/ui/TwoWayGridView;)I

    move-result v3

    add-int p2, v1, v3

    .line 944
    iget-object v1, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-static {v1}, Lcom/jess/ui/TwoWayGridView;->a(Lcom/jess/ui/TwoWayGridView;)I

    move-result v1

    add-int/2addr p1, v1

    move-object v1, v0

    .line 945
    goto :goto_0

    .line 947
    :cond_0
    return-object v1

    :cond_1
    move-object v0, v1

    goto :goto_1
.end method

.method private c(III)Landroid/view/View;
    .locals 8

    .prologue
    .line 1286
    iget-object v0, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-virtual {v0}, Lcom/jess/ui/TwoWayGridView;->getVerticalFadingEdgeLength()I

    move-result v2

    .line 1287
    iget-object v0, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    iget v1, v0, Lcom/jess/ui/TwoWayGridView;->V:I

    .line 1288
    iget-object v0, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-static {v0}, Lcom/jess/ui/TwoWayGridView;->a(Lcom/jess/ui/TwoWayGridView;)I

    move-result v3

    .line 1289
    iget-object v0, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-static {v0}, Lcom/jess/ui/TwoWayGridView;->b(Lcom/jess/ui/TwoWayGridView;)I

    move-result v4

    .line 1292
    const/4 v0, -0x1

    .line 1294
    iget-object v5, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    iget-boolean v5, v5, Lcom/jess/ui/TwoWayGridView;->y:Z

    if-nez v5, :cond_0

    .line 1295
    rem-int v5, v1, v3

    sub-int/2addr v1, v5

    .line 1306
    :goto_0
    invoke-direct {p0, p2, v2, v1}, Lcom/jess/ui/ag;->d(III)I

    move-result v5

    .line 1307
    invoke-direct {p0, p3, v2, v3, v1}, Lcom/jess/ui/ag;->a(IIII)I

    move-result v6

    .line 1310
    iget-object v2, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    iget-boolean v2, v2, Lcom/jess/ui/TwoWayGridView;->y:Z

    if-eqz v2, :cond_1

    move v2, v0

    :goto_1
    const/4 v7, 0x1

    invoke-direct {p0, v2, p1, v7}, Lcom/jess/ui/ag;->a(IIZ)Landroid/view/View;

    move-result-object v2

    .line 1312
    iget-object v7, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    iput v1, v7, Lcom/jess/ui/TwoWayGridView;->H:I

    .line 1314
    iget-object v7, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-static {v7}, Lcom/jess/ui/TwoWayGridView;->c(Lcom/jess/ui/TwoWayGridView;)Landroid/view/View;

    move-result-object v7

    .line 1315
    invoke-direct {p0, v7, v5, v6}, Lcom/jess/ui/ag;->b(Landroid/view/View;II)V

    .line 1316
    invoke-direct {p0, v7, v5, v6}, Lcom/jess/ui/ag;->a(Landroid/view/View;II)V

    .line 1318
    iget-object v5, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    iget-boolean v5, v5, Lcom/jess/ui/TwoWayGridView;->y:Z

    if-nez v5, :cond_2

    .line 1319
    sub-int v0, v1, v3

    invoke-virtual {v7}, Landroid/view/View;->getTop()I

    move-result v5

    sub-int/2addr v5, v4

    invoke-direct {p0, v0, v5}, Lcom/jess/ui/ag;->d(II)Landroid/view/View;

    .line 1320
    invoke-direct {p0}, Lcom/jess/ui/ag;->b()V

    .line 1321
    add-int v0, v1, v3

    invoke-virtual {v7}, Landroid/view/View;->getBottom()I

    move-result v1

    add-int/2addr v1, v4

    invoke-direct {p0, v0, v1}, Lcom/jess/ui/ag;->c(II)Landroid/view/View;

    .line 1329
    :goto_2
    return-object v2

    .line 1297
    :cond_0
    iget-object v0, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    iget v0, v0, Lcom/jess/ui/TwoWayGridView;->aa:I

    add-int/lit8 v0, v0, -0x1

    sub-int/2addr v0, v1

    .line 1299
    iget-object v1, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    iget v1, v1, Lcom/jess/ui/TwoWayGridView;->aa:I

    add-int/lit8 v1, v1, -0x1

    rem-int v5, v0, v3

    sub-int/2addr v0, v5

    sub-int v0, v1, v0

    .line 1300
    const/4 v1, 0x0

    sub-int v5, v0, v3

    add-int/lit8 v5, v5, 0x1

    invoke-static {v1, v5}, Ljava/lang/Math;->max(II)I

    move-result v1

    goto :goto_0

    :cond_1
    move v2, v1

    .line 1310
    goto :goto_1

    .line 1323
    :cond_2
    add-int/2addr v0, v3

    invoke-virtual {v7}, Landroid/view/View;->getBottom()I

    move-result v3

    add-int/2addr v3, v4

    invoke-direct {p0, v0, v3}, Lcom/jess/ui/ag;->c(II)Landroid/view/View;

    .line 1324
    invoke-direct {p0}, Lcom/jess/ui/ag;->b()V

    .line 1325
    add-int/lit8 v0, v1, -0x1

    invoke-virtual {v7}, Landroid/view/View;->getTop()I

    move-result v1

    sub-int/2addr v1, v4

    invoke-direct {p0, v0, v1}, Lcom/jess/ui/ag;->d(II)Landroid/view/View;

    goto :goto_2
.end method

.method private d(III)I
    .locals 0

    .prologue
    .line 1362
    .line 1363
    if-lez p3, :cond_0

    .line 1364
    add-int/2addr p1, p2

    .line 1366
    :cond_0
    return p1
.end method

.method private d(II)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1016
    const/4 v1, 0x0

    .line 1018
    iget-object v0, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    iget-object v0, v0, Lcom/jess/ui/TwoWayGridView;->m:Landroid/graphics/Rect;

    iget v2, v0, Landroid/graphics/Rect;->top:I

    .line 1020
    :goto_0
    if-le p2, v2, :cond_0

    if-ltz p1, :cond_0

    .line 1022
    invoke-direct {p0, p1, p2, v4}, Lcom/jess/ui/ag;->a(IIZ)Landroid/view/View;

    move-result-object v0

    .line 1023
    if-eqz v0, :cond_2

    .line 1027
    :goto_1
    iget-object v1, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-static {v1}, Lcom/jess/ui/TwoWayGridView;->c(Lcom/jess/ui/TwoWayGridView;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v1

    iget-object v3, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-static {v3}, Lcom/jess/ui/TwoWayGridView;->b(Lcom/jess/ui/TwoWayGridView;)I

    move-result v3

    sub-int p2, v1, v3

    .line 1029
    iget-object v1, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    iput p1, v1, Lcom/jess/ui/TwoWayGridView;->H:I

    .line 1031
    iget-object v1, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-static {v1}, Lcom/jess/ui/TwoWayGridView;->a(Lcom/jess/ui/TwoWayGridView;)I

    move-result v1

    sub-int/2addr p1, v1

    move-object v1, v0

    .line 1032
    goto :goto_0

    .line 1034
    :cond_0
    iget-object v0, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    iget-boolean v0, v0, Lcom/jess/ui/TwoWayGridView;->y:Z

    if-eqz v0, :cond_1

    .line 1035
    iget-object v0, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    add-int/lit8 v2, p1, 0x1

    invoke-static {v4, v2}, Ljava/lang/Math;->max(II)I

    move-result v2

    iput v2, v0, Lcom/jess/ui/TwoWayGridView;->H:I

    .line 1038
    :cond_1
    return-object v1

    :cond_2
    move-object v0, v1

    goto :goto_1
.end method

.method private d(I)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 1426
    iget-object v0, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-static {v0}, Lcom/jess/ui/TwoWayGridView;->g(Lcom/jess/ui/TwoWayGridView;)I

    move-result v0

    .line 1427
    iget-object v1, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-static {v1}, Lcom/jess/ui/TwoWayGridView;->f(Lcom/jess/ui/TwoWayGridView;)I

    move-result v1

    .line 1428
    iget-object v2, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-static {v2}, Lcom/jess/ui/TwoWayGridView;->h(Lcom/jess/ui/TwoWayGridView;)I

    move-result v2

    .line 1429
    iget-object v3, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    iget-object v4, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-static {v4}, Lcom/jess/ui/TwoWayGridView;->i(Lcom/jess/ui/TwoWayGridView;)I

    move-result v4

    invoke-static {v3, v4}, Lcom/jess/ui/TwoWayGridView;->a(Lcom/jess/ui/TwoWayGridView;I)I

    .line 1431
    iget-object v3, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-static {v3}, Lcom/jess/ui/TwoWayGridView;->j(Lcom/jess/ui/TwoWayGridView;)I

    move-result v3

    const/4 v4, -0x1

    if-ne v3, v4, :cond_2

    .line 1432
    if-lez v2, :cond_1

    .line 1434
    iget-object v3, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    add-int v4, p1, v0

    add-int v5, v2, v0

    div-int/2addr v4, v5

    invoke-static {v3, v4}, Lcom/jess/ui/TwoWayGridView;->b(Lcom/jess/ui/TwoWayGridView;I)I

    .line 1445
    :goto_0
    iget-object v3, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-static {v3}, Lcom/jess/ui/TwoWayGridView;->a(Lcom/jess/ui/TwoWayGridView;)I

    move-result v3

    if-gtz v3, :cond_0

    .line 1446
    iget-object v3, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-static {v3, v6}, Lcom/jess/ui/TwoWayGridView;->b(Lcom/jess/ui/TwoWayGridView;I)I

    .line 1449
    :cond_0
    packed-switch v1, :pswitch_data_0

    .line 1458
    packed-switch v1, :pswitch_data_1

    .line 1496
    :goto_1
    return-void

    .line 1438
    :cond_1
    iget-object v3, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    const/4 v4, 0x2

    invoke-static {v3, v4}, Lcom/jess/ui/TwoWayGridView;->b(Lcom/jess/ui/TwoWayGridView;I)I

    goto :goto_0

    .line 1442
    :cond_2
    iget-object v3, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    iget-object v4, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-static {v4}, Lcom/jess/ui/TwoWayGridView;->j(Lcom/jess/ui/TwoWayGridView;)I

    move-result v4

    invoke-static {v3, v4}, Lcom/jess/ui/TwoWayGridView;->b(Lcom/jess/ui/TwoWayGridView;I)I

    goto :goto_0

    .line 1452
    :pswitch_0
    iget-object v1, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-static {v1, v2}, Lcom/jess/ui/TwoWayGridView;->c(Lcom/jess/ui/TwoWayGridView;I)I

    .line 1453
    iget-object v1, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-static {v1, v0}, Lcom/jess/ui/TwoWayGridView;->d(Lcom/jess/ui/TwoWayGridView;I)I

    goto :goto_1

    .line 1461
    :pswitch_1
    iget-object v1, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-static {v1}, Lcom/jess/ui/TwoWayGridView;->a(Lcom/jess/ui/TwoWayGridView;)I

    move-result v1

    mul-int/2addr v1, v2

    sub-int v1, p1, v1

    iget-object v3, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    .line 1462
    invoke-static {v3}, Lcom/jess/ui/TwoWayGridView;->a(Lcom/jess/ui/TwoWayGridView;)I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    mul-int/2addr v3, v0

    sub-int/2addr v1, v3

    .line 1463
    iget-object v3, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    iget-object v4, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-static {v4}, Lcom/jess/ui/TwoWayGridView;->a(Lcom/jess/ui/TwoWayGridView;)I

    move-result v4

    div-int/2addr v1, v4

    add-int/2addr v1, v2

    invoke-static {v3, v1}, Lcom/jess/ui/TwoWayGridView;->c(Lcom/jess/ui/TwoWayGridView;I)I

    .line 1464
    iget-object v1, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-static {v1, v0}, Lcom/jess/ui/TwoWayGridView;->d(Lcom/jess/ui/TwoWayGridView;I)I

    goto :goto_1

    .line 1469
    :pswitch_2
    iget-object v1, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-static {v1}, Lcom/jess/ui/TwoWayGridView;->a(Lcom/jess/ui/TwoWayGridView;)I

    move-result v1

    mul-int/2addr v1, v2

    sub-int v1, p1, v1

    iget-object v3, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    .line 1470
    invoke-static {v3}, Lcom/jess/ui/TwoWayGridView;->a(Lcom/jess/ui/TwoWayGridView;)I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    mul-int/2addr v3, v0

    sub-int/2addr v1, v3

    .line 1471
    iget-object v3, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-static {v3, v2}, Lcom/jess/ui/TwoWayGridView;->c(Lcom/jess/ui/TwoWayGridView;I)I

    .line 1472
    iget-object v2, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-static {v2}, Lcom/jess/ui/TwoWayGridView;->a(Lcom/jess/ui/TwoWayGridView;)I

    move-result v2

    if-le v2, v6, :cond_3

    .line 1473
    iget-object v2, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    iget-object v3, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    .line 1474
    invoke-static {v3}, Lcom/jess/ui/TwoWayGridView;->a(Lcom/jess/ui/TwoWayGridView;)I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    div-int/2addr v1, v3

    add-int/2addr v0, v1

    .line 1473
    invoke-static {v2, v0}, Lcom/jess/ui/TwoWayGridView;->d(Lcom/jess/ui/TwoWayGridView;I)I

    goto :goto_1

    .line 1476
    :cond_3
    iget-object v2, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    add-int/2addr v0, v1

    invoke-static {v2, v0}, Lcom/jess/ui/TwoWayGridView;->d(Lcom/jess/ui/TwoWayGridView;I)I

    goto :goto_1

    .line 1482
    :pswitch_3
    iget-object v1, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-static {v1}, Lcom/jess/ui/TwoWayGridView;->a(Lcom/jess/ui/TwoWayGridView;)I

    move-result v1

    mul-int/2addr v1, v2

    sub-int v1, p1, v1

    iget-object v3, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    .line 1483
    invoke-static {v3}, Lcom/jess/ui/TwoWayGridView;->a(Lcom/jess/ui/TwoWayGridView;)I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    mul-int/2addr v3, v0

    sub-int/2addr v1, v3

    .line 1484
    iget-object v3, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-static {v3, v2}, Lcom/jess/ui/TwoWayGridView;->c(Lcom/jess/ui/TwoWayGridView;I)I

    .line 1485
    iget-object v2, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-static {v2}, Lcom/jess/ui/TwoWayGridView;->a(Lcom/jess/ui/TwoWayGridView;)I

    move-result v2

    if-le v2, v6, :cond_4

    .line 1486
    iget-object v2, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    iget-object v3, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    .line 1487
    invoke-static {v3}, Lcom/jess/ui/TwoWayGridView;->a(Lcom/jess/ui/TwoWayGridView;)I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    div-int/2addr v1, v3

    add-int/2addr v0, v1

    .line 1486
    invoke-static {v2, v0}, Lcom/jess/ui/TwoWayGridView;->d(Lcom/jess/ui/TwoWayGridView;I)I

    goto/16 :goto_1

    .line 1489
    :cond_4
    iget-object v2, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    mul-int/lit8 v0, v0, 0x2

    add-int/2addr v0, v1

    div-int/lit8 v0, v0, 0x2

    invoke-static {v2, v0}, Lcom/jess/ui/TwoWayGridView;->d(Lcom/jess/ui/TwoWayGridView;I)I

    goto/16 :goto_1

    .line 1449
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch

    .line 1458
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_3
    .end packed-switch
.end method

.method private e(II)Landroid/view/View;
    .locals 3

    .prologue
    .line 1062
    iget-object v0, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    iget v0, v0, Lcom/jess/ui/TwoWayGridView;->V:I

    invoke-static {p1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 1063
    iget-object v1, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    iget v1, v1, Lcom/jess/ui/TwoWayGridView;->aa:I

    add-int/lit8 v1, v1, -0x1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 1065
    iget-object v1, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    iget v1, v1, Lcom/jess/ui/TwoWayGridView;->aa:I

    add-int/lit8 v1, v1, -0x1

    sub-int v0, v1, v0

    .line 1066
    iget-object v1, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    iget v1, v1, Lcom/jess/ui/TwoWayGridView;->aa:I

    add-int/lit8 v1, v1, -0x1

    iget-object v2, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-static {v2}, Lcom/jess/ui/TwoWayGridView;->a(Lcom/jess/ui/TwoWayGridView;)I

    move-result v2

    rem-int v2, v0, v2

    sub-int/2addr v0, v2

    sub-int v0, v1, v0

    .line 1068
    invoke-direct {p0, v0, p2}, Lcom/jess/ui/ag;->d(II)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method private e(III)Landroid/view/View;
    .locals 10

    .prologue
    const/4 v9, 0x1

    const/4 v3, 0x0

    .line 1513
    iget-object v0, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-virtual {v0}, Lcom/jess/ui/TwoWayGridView;->getVerticalFadingEdgeLength()I

    move-result v4

    .line 1514
    iget-object v0, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    iget v5, v0, Lcom/jess/ui/TwoWayGridView;->V:I

    .line 1515
    iget-object v0, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-static {v0}, Lcom/jess/ui/TwoWayGridView;->a(Lcom/jess/ui/TwoWayGridView;)I

    move-result v6

    .line 1516
    iget-object v0, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-static {v0}, Lcom/jess/ui/TwoWayGridView;->b(Lcom/jess/ui/TwoWayGridView;)I

    move-result v7

    .line 1520
    const/4 v0, -0x1

    .line 1522
    iget-object v1, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    iget-boolean v1, v1, Lcom/jess/ui/TwoWayGridView;->y:Z

    if-nez v1, :cond_0

    .line 1523
    sub-int v1, v5, p1

    sub-int v2, v5, p1

    rem-int/2addr v2, v6

    sub-int v2, v1, v2

    .line 1525
    rem-int v1, v5, v6

    sub-int v1, v5, v1

    .line 1537
    :goto_0
    sub-int v2, v1, v2

    .line 1539
    invoke-direct {p0, p2, v4, v1}, Lcom/jess/ui/ag;->d(III)I

    move-result v5

    .line 1540
    invoke-direct {p0, p3, v4, v6, v1}, Lcom/jess/ui/ag;->a(IIII)I

    move-result v8

    .line 1544
    iget-object v4, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    iput v1, v4, Lcom/jess/ui/TwoWayGridView;->H:I

    .line 1549
    if-lez v2, :cond_3

    .line 1554
    iget-object v2, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-static {v2}, Lcom/jess/ui/TwoWayGridView;->k(Lcom/jess/ui/TwoWayGridView;)Landroid/view/View;

    move-result-object v2

    if-nez v2, :cond_1

    move v2, v3

    .line 1557
    :goto_1
    iget-object v3, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    iget-boolean v3, v3, Lcom/jess/ui/TwoWayGridView;->y:Z

    if-eqz v3, :cond_2

    move v3, v0

    :goto_2
    add-int/2addr v2, v7

    invoke-direct {p0, v3, v2, v9}, Lcom/jess/ui/ag;->a(IIZ)Landroid/view/View;

    move-result-object v3

    .line 1558
    iget-object v2, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-static {v2}, Lcom/jess/ui/TwoWayGridView;->c(Lcom/jess/ui/TwoWayGridView;)Landroid/view/View;

    move-result-object v2

    .line 1560
    invoke-direct {p0, v2, v5, v8}, Lcom/jess/ui/ag;->a(Landroid/view/View;II)V

    .line 1583
    :goto_3
    iget-object v4, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    iget-boolean v4, v4, Lcom/jess/ui/TwoWayGridView;->y:Z

    if-nez v4, :cond_9

    .line 1584
    sub-int v0, v1, v6

    invoke-virtual {v2}, Landroid/view/View;->getTop()I

    move-result v4

    sub-int/2addr v4, v7

    invoke-direct {p0, v0, v4}, Lcom/jess/ui/ag;->d(II)Landroid/view/View;

    .line 1585
    invoke-direct {p0}, Lcom/jess/ui/ag;->b()V

    .line 1586
    add-int v0, v1, v6

    invoke-virtual {v2}, Landroid/view/View;->getBottom()I

    move-result v1

    add-int/2addr v1, v7

    invoke-direct {p0, v0, v1}, Lcom/jess/ui/ag;->c(II)Landroid/view/View;

    .line 1593
    :goto_4
    return-object v3

    .line 1527
    :cond_0
    iget-object v0, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    iget v0, v0, Lcom/jess/ui/TwoWayGridView;->aa:I

    add-int/lit8 v0, v0, -0x1

    sub-int/2addr v0, v5

    .line 1529
    iget-object v1, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    iget v1, v1, Lcom/jess/ui/TwoWayGridView;->aa:I

    add-int/lit8 v1, v1, -0x1

    rem-int v2, v0, v6

    sub-int/2addr v0, v2

    sub-int v0, v1, v0

    .line 1530
    sub-int v1, v0, v6

    add-int/lit8 v1, v1, 0x1

    invoke-static {v3, v1}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 1532
    iget-object v2, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    iget v2, v2, Lcom/jess/ui/TwoWayGridView;->aa:I

    add-int/lit8 v2, v2, -0x1

    sub-int/2addr v5, p1

    sub-int/2addr v2, v5

    .line 1533
    iget-object v5, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    iget v5, v5, Lcom/jess/ui/TwoWayGridView;->aa:I

    add-int/lit8 v5, v5, -0x1

    rem-int v8, v2, v6

    sub-int/2addr v2, v8

    sub-int v2, v5, v2

    .line 1534
    sub-int/2addr v2, v6

    add-int/lit8 v2, v2, 0x1

    invoke-static {v3, v2}, Ljava/lang/Math;->max(II)I

    move-result v2

    goto :goto_0

    .line 1554
    :cond_1
    iget-object v2, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    .line 1555
    invoke-static {v2}, Lcom/jess/ui/TwoWayGridView;->k(Lcom/jess/ui/TwoWayGridView;)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getBottom()I

    move-result v2

    goto :goto_1

    :cond_2
    move v3, v1

    .line 1557
    goto :goto_2

    .line 1561
    :cond_3
    if-gez v2, :cond_6

    .line 1565
    iget-object v2, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-static {v2}, Lcom/jess/ui/TwoWayGridView;->k(Lcom/jess/ui/TwoWayGridView;)Landroid/view/View;

    move-result-object v2

    if-nez v2, :cond_4

    move v2, v3

    .line 1568
    :goto_5
    iget-object v4, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    iget-boolean v4, v4, Lcom/jess/ui/TwoWayGridView;->y:Z

    if-eqz v4, :cond_5

    move v4, v0

    :goto_6
    sub-int/2addr v2, v7

    invoke-direct {p0, v4, v2, v3}, Lcom/jess/ui/ag;->a(IIZ)Landroid/view/View;

    move-result-object v3

    .line 1569
    iget-object v2, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-static {v2}, Lcom/jess/ui/TwoWayGridView;->c(Lcom/jess/ui/TwoWayGridView;)Landroid/view/View;

    move-result-object v2

    .line 1571
    invoke-direct {p0, v2, v5, v8}, Lcom/jess/ui/ag;->b(Landroid/view/View;II)V

    goto :goto_3

    .line 1565
    :cond_4
    iget-object v2, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    .line 1566
    invoke-static {v2}, Lcom/jess/ui/TwoWayGridView;->k(Lcom/jess/ui/TwoWayGridView;)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getTop()I

    move-result v2

    goto :goto_5

    :cond_5
    move v4, v1

    .line 1568
    goto :goto_6

    .line 1576
    :cond_6
    iget-object v2, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-static {v2}, Lcom/jess/ui/TwoWayGridView;->k(Lcom/jess/ui/TwoWayGridView;)Landroid/view/View;

    move-result-object v2

    if-nez v2, :cond_7

    .line 1579
    :goto_7
    iget-object v2, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    iget-boolean v2, v2, Lcom/jess/ui/TwoWayGridView;->y:Z

    if-eqz v2, :cond_8

    move v2, v0

    :goto_8
    invoke-direct {p0, v2, v3, v9}, Lcom/jess/ui/ag;->a(IIZ)Landroid/view/View;

    move-result-object v3

    .line 1580
    iget-object v2, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-static {v2}, Lcom/jess/ui/TwoWayGridView;->c(Lcom/jess/ui/TwoWayGridView;)Landroid/view/View;

    move-result-object v2

    goto/16 :goto_3

    .line 1576
    :cond_7
    iget-object v2, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    .line 1577
    invoke-static {v2}, Lcom/jess/ui/TwoWayGridView;->k(Lcom/jess/ui/TwoWayGridView;)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getTop()I

    move-result v3

    goto :goto_7

    :cond_8
    move v2, v1

    .line 1579
    goto :goto_8

    .line 1588
    :cond_9
    add-int/2addr v0, v6

    invoke-virtual {v2}, Landroid/view/View;->getBottom()I

    move-result v4

    add-int/2addr v4, v7

    invoke-direct {p0, v0, v4}, Lcom/jess/ui/ag;->c(II)Landroid/view/View;

    .line 1589
    invoke-direct {p0}, Lcom/jess/ui/ag;->b()V

    .line 1590
    add-int/lit8 v0, v1, -0x1

    invoke-virtual {v2}, Landroid/view/View;->getTop()I

    move-result v1

    sub-int/2addr v1, v7

    invoke-direct {p0, v0, v1}, Lcom/jess/ui/ag;->d(II)Landroid/view/View;

    goto/16 :goto_4
.end method

.method private e(I)V
    .locals 2

    .prologue
    .line 1977
    iget-object v0, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    iget v0, v0, Lcom/jess/ui/TwoWayGridView;->H:I

    if-nez v0, :cond_0

    .line 1978
    iget-object v0, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/jess/ui/TwoWayGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v0

    .line 1979
    sub-int v0, p1, v0

    .line 1980
    if-gez v0, :cond_0

    .line 1981
    iget-object v1, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-virtual {v1, v0}, Lcom/jess/ui/TwoWayGridView;->e(I)V

    .line 1984
    :cond_0
    return-void
.end method

.method private f(II)Landroid/view/View;
    .locals 8

    .prologue
    .line 1073
    iget-object v0, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-virtual {v0}, Lcom/jess/ui/TwoWayGridView;->j()I

    move-result v1

    .line 1074
    iget-object v0, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-static {v0}, Lcom/jess/ui/TwoWayGridView;->a(Lcom/jess/ui/TwoWayGridView;)I

    move-result v3

    .line 1075
    iget-object v0, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-static {v0}, Lcom/jess/ui/TwoWayGridView;->b(Lcom/jess/ui/TwoWayGridView;)I

    move-result v4

    .line 1078
    const/4 v0, -0x1

    .line 1080
    iget-object v2, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    iget-boolean v2, v2, Lcom/jess/ui/TwoWayGridView;->y:Z

    if-nez v2, :cond_0

    .line 1081
    rem-int v2, v1, v3

    sub-int/2addr v1, v2

    .line 1089
    :goto_0
    iget-object v2, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-virtual {v2}, Lcom/jess/ui/TwoWayGridView;->getVerticalFadingEdgeLength()I

    move-result v5

    .line 1090
    invoke-direct {p0, p1, v5, v1}, Lcom/jess/ui/ag;->d(III)I

    move-result v6

    .line 1092
    iget-object v2, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    iget-boolean v2, v2, Lcom/jess/ui/TwoWayGridView;->y:Z

    if-eqz v2, :cond_1

    move v2, v0

    :goto_1
    const/4 v7, 0x1

    invoke-direct {p0, v2, v6, v7}, Lcom/jess/ui/ag;->a(IIZ)Landroid/view/View;

    move-result-object v2

    .line 1093
    iget-object v6, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    iput v1, v6, Lcom/jess/ui/TwoWayGridView;->H:I

    .line 1095
    iget-object v6, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-static {v6}, Lcom/jess/ui/TwoWayGridView;->c(Lcom/jess/ui/TwoWayGridView;)Landroid/view/View;

    move-result-object v6

    .line 1097
    iget-object v7, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    iget-boolean v7, v7, Lcom/jess/ui/TwoWayGridView;->y:Z

    if-nez v7, :cond_2

    .line 1098
    add-int v0, v1, v3

    invoke-virtual {v6}, Landroid/view/View;->getBottom()I

    move-result v5

    add-int/2addr v5, v4

    invoke-direct {p0, v0, v5}, Lcom/jess/ui/ag;->c(II)Landroid/view/View;

    .line 1099
    invoke-direct {p0, p2}, Lcom/jess/ui/ag;->f(I)V

    .line 1100
    sub-int v0, v1, v3

    invoke-virtual {v6}, Landroid/view/View;->getTop()I

    move-result v1

    sub-int/2addr v1, v4

    invoke-direct {p0, v0, v1}, Lcom/jess/ui/ag;->d(II)Landroid/view/View;

    .line 1101
    invoke-direct {p0}, Lcom/jess/ui/ag;->b()V

    .line 1113
    :goto_2
    return-object v2

    .line 1083
    :cond_0
    iget-object v0, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    iget v0, v0, Lcom/jess/ui/TwoWayGridView;->aa:I

    add-int/lit8 v0, v0, -0x1

    sub-int/2addr v0, v1

    .line 1085
    iget-object v1, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    iget v1, v1, Lcom/jess/ui/TwoWayGridView;->aa:I

    add-int/lit8 v1, v1, -0x1

    rem-int v2, v0, v3

    sub-int/2addr v0, v2

    sub-int v0, v1, v0

    .line 1086
    const/4 v1, 0x0

    sub-int v2, v0, v3

    add-int/lit8 v2, v2, 0x1

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    goto :goto_0

    :cond_1
    move v2, v1

    .line 1092
    goto :goto_1

    .line 1103
    :cond_2
    invoke-direct {p0, p2, v5, v3, v1}, Lcom/jess/ui/ag;->a(IIII)I

    move-result v5

    .line 1105
    invoke-virtual {v6}, Landroid/view/View;->getBottom()I

    move-result v7

    sub-int/2addr v5, v7

    .line 1106
    iget-object v7, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-virtual {v7, v5}, Lcom/jess/ui/TwoWayGridView;->e(I)V

    .line 1107
    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v6}, Landroid/view/View;->getTop()I

    move-result v5

    sub-int/2addr v5, v4

    invoke-direct {p0, v1, v5}, Lcom/jess/ui/ag;->d(II)Landroid/view/View;

    .line 1108
    invoke-direct {p0, p1}, Lcom/jess/ui/ag;->e(I)V

    .line 1109
    add-int/2addr v0, v3

    invoke-virtual {v6}, Landroid/view/View;->getBottom()I

    move-result v1

    add-int/2addr v1, v4

    invoke-direct {p0, v0, v1}, Lcom/jess/ui/ag;->c(II)Landroid/view/View;

    .line 1110
    invoke-direct {p0}, Lcom/jess/ui/ag;->b()V

    goto :goto_2
.end method

.method private f(I)V
    .locals 3

    .prologue
    .line 1987
    iget-object v0, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-virtual {v0}, Lcom/jess/ui/TwoWayGridView;->getChildCount()I

    move-result v0

    .line 1988
    iget-object v1, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    iget v1, v1, Lcom/jess/ui/TwoWayGridView;->H:I

    add-int/2addr v1, v0

    iget-object v2, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    iget v2, v2, Lcom/jess/ui/TwoWayGridView;->aa:I

    if-ne v1, v2, :cond_0

    .line 1989
    iget-object v1, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {v1, v0}, Lcom/jess/ui/TwoWayGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getBottom()I

    move-result v0

    .line 1990
    sub-int v0, p1, v0

    .line 1991
    if-lez v0, :cond_0

    .line 1992
    iget-object v1, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-virtual {v1, v0}, Lcom/jess/ui/TwoWayGridView;->e(I)V

    .line 1995
    :cond_0
    return-void
.end method

.method private g(II)Landroid/view/View;
    .locals 8

    .prologue
    .line 1129
    iget-object v0, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-static {v0}, Lcom/jess/ui/TwoWayGridView;->a(Lcom/jess/ui/TwoWayGridView;)I

    move-result v3

    .line 1132
    const/4 v0, -0x1

    .line 1134
    iget-object v1, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    iget-boolean v1, v1, Lcom/jess/ui/TwoWayGridView;->y:Z

    if-nez v1, :cond_1

    .line 1135
    rem-int v1, p1, v3

    sub-int v1, p1, v1

    .line 1143
    :goto_0
    iget-object v2, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    iget-boolean v2, v2, Lcom/jess/ui/TwoWayGridView;->y:Z

    if-eqz v2, :cond_2

    move v2, v0

    :goto_1
    const/4 v4, 0x1

    invoke-direct {p0, v2, p2, v4}, Lcom/jess/ui/ag;->a(IIZ)Landroid/view/View;

    move-result-object v2

    .line 1146
    iget-object v4, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    iput v1, v4, Lcom/jess/ui/TwoWayGridView;->H:I

    .line 1148
    iget-object v4, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-static {v4}, Lcom/jess/ui/TwoWayGridView;->c(Lcom/jess/ui/TwoWayGridView;)Landroid/view/View;

    move-result-object v4

    .line 1150
    if-nez v4, :cond_3

    .line 1151
    const/4 v0, 0x0

    .line 1184
    :cond_0
    :goto_2
    return-object v0

    .line 1137
    :cond_1
    iget-object v0, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    iget v0, v0, Lcom/jess/ui/TwoWayGridView;->aa:I

    add-int/lit8 v0, v0, -0x1

    sub-int/2addr v0, p1

    .line 1139
    iget-object v1, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    iget v1, v1, Lcom/jess/ui/TwoWayGridView;->aa:I

    add-int/lit8 v1, v1, -0x1

    rem-int v2, v0, v3

    sub-int/2addr v0, v2

    sub-int v0, v1, v0

    .line 1140
    const/4 v1, 0x0

    sub-int v2, v0, v3

    add-int/lit8 v2, v2, 0x1

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    goto :goto_0

    :cond_2
    move v2, v1

    .line 1143
    goto :goto_1

    .line 1154
    :cond_3
    iget-object v5, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-static {v5}, Lcom/jess/ui/TwoWayGridView;->b(Lcom/jess/ui/TwoWayGridView;)I

    move-result v5

    .line 1159
    iget-object v6, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    iget-boolean v6, v6, Lcom/jess/ui/TwoWayGridView;->y:Z

    if-nez v6, :cond_5

    .line 1160
    sub-int v0, v1, v3

    invoke-virtual {v4}, Landroid/view/View;->getTop()I

    move-result v6

    sub-int/2addr v6, v5

    invoke-direct {p0, v0, v6}, Lcom/jess/ui/ag;->d(II)Landroid/view/View;

    move-result-object v0

    .line 1161
    invoke-direct {p0}, Lcom/jess/ui/ag;->b()V

    .line 1162
    add-int/2addr v1, v3

    invoke-virtual {v4}, Landroid/view/View;->getBottom()I

    move-result v4

    add-int/2addr v4, v5

    invoke-direct {p0, v1, v4}, Lcom/jess/ui/ag;->c(II)Landroid/view/View;

    move-result-object v1

    .line 1164
    iget-object v4, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-virtual {v4}, Lcom/jess/ui/TwoWayGridView;->getChildCount()I

    move-result v4

    .line 1165
    if-lez v4, :cond_4

    .line 1166
    invoke-direct {p0, v3, v5, v4}, Lcom/jess/ui/ag;->a(III)V

    .line 1179
    :cond_4
    :goto_3
    if-eqz v2, :cond_7

    move-object v0, v2

    .line 1180
    goto :goto_2

    .line 1169
    :cond_5
    add-int/2addr v0, v3

    invoke-virtual {v4}, Landroid/view/View;->getBottom()I

    move-result v6

    add-int/2addr v6, v5

    invoke-direct {p0, v0, v6}, Lcom/jess/ui/ag;->c(II)Landroid/view/View;

    move-result-object v0

    .line 1170
    invoke-direct {p0}, Lcom/jess/ui/ag;->b()V

    .line 1171
    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v4}, Landroid/view/View;->getTop()I

    move-result v4

    sub-int/2addr v4, v5

    invoke-direct {p0, v1, v4}, Lcom/jess/ui/ag;->d(II)Landroid/view/View;

    move-result-object v1

    .line 1173
    iget-object v4, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-virtual {v4}, Lcom/jess/ui/TwoWayGridView;->getChildCount()I

    move-result v4

    .line 1174
    if-lez v4, :cond_6

    .line 1175
    invoke-direct {p0, v3, v5, v4}, Lcom/jess/ui/ag;->b(III)V

    :cond_6
    move-object v7, v0

    move-object v0, v1

    move-object v1, v7

    goto :goto_3

    .line 1181
    :cond_7
    if-nez v0, :cond_0

    move-object v0, v1

    .line 1184
    goto :goto_2
.end method


# virtual methods
.method protected a(IIZIZI)Landroid/view/View;
    .locals 9

    .prologue
    .line 864
    iget-object v0, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    iget-boolean v0, v0, Lcom/jess/ui/TwoWayGridView;->S:Z

    if-nez v0, :cond_0

    .line 866
    iget-object v0, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    iget-object v0, v0, Lcom/jess/ui/TwoWayGridView;->g:Lcom/jess/ui/k;

    invoke-virtual {v0, p1}, Lcom/jess/ui/k;->c(I)Landroid/view/View;

    move-result-object v1

    .line 867
    if-eqz v1, :cond_0

    .line 870
    const/4 v7, 0x1

    move-object v0, p0

    move v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    move v6, p5

    move v8, p6

    invoke-direct/range {v0 .. v8}, Lcom/jess/ui/ag;->a(Landroid/view/View;IIZIZZI)V

    .line 883
    :goto_0
    return-object v1

    .line 878
    :cond_0
    iget-object v0, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    iget-object v1, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    iget-object v1, v1, Lcom/jess/ui/TwoWayGridView;->E:[Z

    invoke-virtual {v0, p1, v1}, Lcom/jess/ui/TwoWayGridView;->a(I[Z)Landroid/view/View;

    move-result-object v1

    .line 881
    iget-object v0, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    iget-object v0, v0, Lcom/jess/ui/TwoWayGridView;->E:[Z

    const/4 v2, 0x0

    aget-boolean v7, v0, v2

    move-object v0, p0

    move v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    move v6, p5

    move v8, p6

    invoke-direct/range {v0 .. v8}, Lcom/jess/ui/ag;->a(Landroid/view/View;IIZIZZI)V

    goto :goto_0
.end method

.method protected a()V
    .locals 13

    .prologue
    const/4 v5, -0x1

    const/4 v1, 0x0

    const/4 v4, 0x0

    .line 1673
    iget-object v0, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    iget-object v0, v0, Lcom/jess/ui/TwoWayGridView;->m:Landroid/graphics/Rect;

    iget v6, v0, Landroid/graphics/Rect;->top:I

    .line 1674
    iget-object v0, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-virtual {v0}, Lcom/jess/ui/TwoWayGridView;->getBottom()I

    move-result v0

    iget-object v2, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-virtual {v2}, Lcom/jess/ui/TwoWayGridView;->getTop()I

    move-result v2

    sub-int/2addr v0, v2

    iget-object v2, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    iget-object v2, v2, Lcom/jess/ui/TwoWayGridView;->m:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->bottom:I

    sub-int v8, v0, v2

    .line 1676
    iget-object v0, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-virtual {v0}, Lcom/jess/ui/TwoWayGridView;->getChildCount()I

    move-result v9

    .line 1686
    iget-object v0, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    iget v0, v0, Lcom/jess/ui/TwoWayGridView;->a:I

    packed-switch v0, :pswitch_data_0

    .line 1705
    iget-object v0, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    iget v0, v0, Lcom/jess/ui/TwoWayGridView;->V:I

    iget-object v2, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    iget v2, v2, Lcom/jess/ui/TwoWayGridView;->H:I

    sub-int/2addr v0, v2

    .line 1706
    if-ltz v0, :cond_13

    if-ge v0, v9, :cond_13

    .line 1707
    iget-object v2, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-virtual {v2, v0}, Lcom/jess/ui/TwoWayGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 1711
    :goto_0
    iget-object v2, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-virtual {v2, v4}, Lcom/jess/ui/TwoWayGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    move v3, v4

    move-object v12, v0

    move-object v0, v2

    move-object v2, v12

    .line 1714
    :goto_1
    iget-object v7, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    iget-boolean v7, v7, Lcom/jess/ui/TwoWayGridView;->S:Z

    .line 1715
    if-eqz v7, :cond_0

    .line 1716
    iget-object v10, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-virtual {v10}, Lcom/jess/ui/TwoWayGridView;->l()V

    .line 1721
    :cond_0
    iget-object v10, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    iget v10, v10, Lcom/jess/ui/TwoWayGridView;->aa:I

    if-nez v10, :cond_1

    .line 1722
    iget-object v0, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-virtual {v0}, Lcom/jess/ui/TwoWayGridView;->c()V

    .line 1723
    iget-object v0, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-virtual {v0}, Lcom/jess/ui/TwoWayGridView;->a()V

    .line 1826
    :goto_2
    return-void

    .line 1688
    :pswitch_0
    iget-object v0, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    iget v0, v0, Lcom/jess/ui/TwoWayGridView;->T:I

    iget-object v2, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    iget v2, v2, Lcom/jess/ui/TwoWayGridView;->H:I

    sub-int/2addr v0, v2

    .line 1689
    if-ltz v0, :cond_14

    if-ge v0, v9, :cond_14

    .line 1690
    iget-object v2, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-virtual {v2, v0}, Lcom/jess/ui/TwoWayGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    move-object v2, v1

    move v3, v4

    move-object v12, v0

    move-object v0, v1

    move-object v1, v12

    goto :goto_1

    :pswitch_1
    move-object v0, v1

    move-object v2, v1

    move v3, v4

    .line 1697
    goto :goto_1

    .line 1699
    :pswitch_2
    iget-object v0, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    iget v0, v0, Lcom/jess/ui/TwoWayGridView;->T:I

    if-ltz v0, :cond_14

    .line 1700
    iget-object v0, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    iget v0, v0, Lcom/jess/ui/TwoWayGridView;->T:I

    iget-object v2, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    iget v2, v2, Lcom/jess/ui/TwoWayGridView;->V:I

    sub-int/2addr v0, v2

    move-object v2, v1

    move v3, v0

    move-object v0, v1

    goto :goto_1

    .line 1727
    :cond_1
    iget-object v10, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    iget-object v11, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    iget v11, v11, Lcom/jess/ui/TwoWayGridView;->T:I

    invoke-virtual {v10, v11}, Lcom/jess/ui/TwoWayGridView;->setSelectedPositionInt(I)V

    .line 1731
    iget-object v10, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    iget v10, v10, Lcom/jess/ui/TwoWayGridView;->H:I

    .line 1732
    iget-object v11, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    iget-object v11, v11, Lcom/jess/ui/TwoWayGridView;->g:Lcom/jess/ui/k;

    .line 1734
    if-eqz v7, :cond_2

    move v7, v4

    .line 1735
    :goto_3
    if-ge v7, v9, :cond_3

    .line 1736
    iget-object v10, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-virtual {v10, v7}, Lcom/jess/ui/TwoWayGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v10

    invoke-virtual {v11, v10}, Lcom/jess/ui/k;->a(Landroid/view/View;)V

    .line 1735
    add-int/lit8 v7, v7, 0x1

    goto :goto_3

    .line 1739
    :cond_2
    invoke-virtual {v11, v9, v10}, Lcom/jess/ui/k;->a(II)V

    .line 1744
    :cond_3
    iget-object v7, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-static {v7}, Lcom/jess/ui/TwoWayGridView;->l(Lcom/jess/ui/TwoWayGridView;)V

    .line 1746
    iget-object v7, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    iget v7, v7, Lcom/jess/ui/TwoWayGridView;->a:I

    packed-switch v7, :pswitch_data_1

    .line 1774
    if-nez v9, :cond_c

    .line 1775
    iget-object v0, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    iget-boolean v0, v0, Lcom/jess/ui/TwoWayGridView;->y:Z

    if-nez v0, :cond_9

    .line 1776
    iget-object v1, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    iget-object v0, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    iget-object v0, v0, Lcom/jess/ui/TwoWayGridView;->c:Landroid/widget/ListAdapter;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-virtual {v0}, Lcom/jess/ui/TwoWayGridView;->isInTouchMode()Z

    move-result v0

    if-eqz v0, :cond_8

    :cond_4
    move v0, v5

    :goto_4
    invoke-virtual {v1, v0}, Lcom/jess/ui/TwoWayGridView;->setSelectedPositionInt(I)V

    .line 1778
    invoke-direct {p0, v6}, Lcom/jess/ui/ag;->c(I)Landroid/view/View;

    move-result-object v0

    .line 1800
    :goto_5
    invoke-virtual {v11}, Lcom/jess/ui/k;->c()V

    .line 1802
    if-eqz v0, :cond_11

    .line 1803
    iget-object v1, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-virtual {v1, v0}, Lcom/jess/ui/TwoWayGridView;->a(Landroid/view/View;)V

    .line 1804
    iget-object v1, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v0

    iput v0, v1, Lcom/jess/ui/TwoWayGridView;->x:I

    .line 1813
    :cond_5
    :goto_6
    iget-object v0, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    iput v4, v0, Lcom/jess/ui/TwoWayGridView;->a:I

    .line 1814
    iget-object v0, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    iput-boolean v4, v0, Lcom/jess/ui/TwoWayGridView;->S:Z

    .line 1815
    iget-object v0, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    iput-boolean v4, v0, Lcom/jess/ui/TwoWayGridView;->M:Z

    .line 1816
    iget-object v0, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    iget-object v1, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    iget v1, v1, Lcom/jess/ui/TwoWayGridView;->V:I

    invoke-virtual {v0, v1}, Lcom/jess/ui/TwoWayGridView;->setNextSelectedPositionInt(I)V

    .line 1818
    iget-object v0, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-virtual {v0}, Lcom/jess/ui/TwoWayGridView;->e()V

    .line 1820
    iget-object v0, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    iget v0, v0, Lcom/jess/ui/TwoWayGridView;->aa:I

    if-lez v0, :cond_6

    .line 1821
    iget-object v0, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-virtual {v0}, Lcom/jess/ui/TwoWayGridView;->p()V

    .line 1824
    :cond_6
    iget-object v0, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-virtual {v0}, Lcom/jess/ui/TwoWayGridView;->a()V

    goto/16 :goto_2

    .line 1748
    :pswitch_3
    if-eqz v1, :cond_7

    .line 1749
    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v0

    invoke-direct {p0, v0, v6, v8}, Lcom/jess/ui/ag;->c(III)Landroid/view/View;

    move-result-object v0

    goto :goto_5

    .line 1751
    :cond_7
    invoke-direct {p0, v6, v8}, Lcom/jess/ui/ag;->f(II)Landroid/view/View;

    move-result-object v0

    goto :goto_5

    .line 1755
    :pswitch_4
    iget-object v0, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    iput v4, v0, Lcom/jess/ui/TwoWayGridView;->H:I

    .line 1756
    invoke-direct {p0, v6}, Lcom/jess/ui/ag;->c(I)Landroid/view/View;

    move-result-object v0

    .line 1757
    invoke-direct {p0}, Lcom/jess/ui/ag;->b()V

    goto :goto_5

    .line 1760
    :pswitch_5
    iget-object v0, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    iget v0, v0, Lcom/jess/ui/TwoWayGridView;->aa:I

    add-int/lit8 v0, v0, -0x1

    invoke-direct {p0, v0, v8}, Lcom/jess/ui/ag;->d(II)Landroid/view/View;

    move-result-object v0

    .line 1761
    invoke-direct {p0}, Lcom/jess/ui/ag;->b()V

    goto :goto_5

    .line 1764
    :pswitch_6
    iget-object v0, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    iget v0, v0, Lcom/jess/ui/TwoWayGridView;->V:I

    iget-object v1, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    iget v1, v1, Lcom/jess/ui/TwoWayGridView;->I:I

    invoke-direct {p0, v0, v1}, Lcom/jess/ui/ag;->g(II)Landroid/view/View;

    move-result-object v0

    goto :goto_5

    .line 1767
    :pswitch_7
    iget-object v0, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    iget v0, v0, Lcom/jess/ui/TwoWayGridView;->J:I

    iget-object v1, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    iget v1, v1, Lcom/jess/ui/TwoWayGridView;->I:I

    invoke-direct {p0, v0, v1}, Lcom/jess/ui/ag;->g(II)Landroid/view/View;

    move-result-object v0

    goto/16 :goto_5

    .line 1771
    :pswitch_8
    invoke-direct {p0, v3, v6, v8}, Lcom/jess/ui/ag;->e(III)Landroid/view/View;

    move-result-object v0

    goto/16 :goto_5

    :cond_8
    move v0, v4

    .line 1776
    goto/16 :goto_4

    .line 1780
    :cond_9
    iget-object v0, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    iget v0, v0, Lcom/jess/ui/TwoWayGridView;->aa:I

    add-int/lit8 v0, v0, -0x1

    .line 1781
    iget-object v1, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    iget-object v2, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    iget-object v2, v2, Lcom/jess/ui/TwoWayGridView;->c:Landroid/widget/ListAdapter;

    if-eqz v2, :cond_a

    iget-object v2, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-virtual {v2}, Lcom/jess/ui/TwoWayGridView;->isInTouchMode()Z

    move-result v2

    if-eqz v2, :cond_b

    :cond_a
    :goto_7
    invoke-virtual {v1, v5}, Lcom/jess/ui/TwoWayGridView;->setSelectedPositionInt(I)V

    .line 1783
    invoke-direct {p0, v0, v8}, Lcom/jess/ui/ag;->e(II)Landroid/view/View;

    move-result-object v0

    goto/16 :goto_5

    :cond_b
    move v5, v0

    .line 1781
    goto :goto_7

    .line 1786
    :cond_c
    iget-object v1, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    iget v1, v1, Lcom/jess/ui/TwoWayGridView;->V:I

    if-ltz v1, :cond_e

    iget-object v1, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    iget v1, v1, Lcom/jess/ui/TwoWayGridView;->V:I

    iget-object v3, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    iget v3, v3, Lcom/jess/ui/TwoWayGridView;->aa:I

    if-ge v1, v3, :cond_e

    .line 1787
    iget-object v0, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    iget v1, v0, Lcom/jess/ui/TwoWayGridView;->V:I

    if-nez v2, :cond_d

    move v0, v6

    :goto_8
    invoke-direct {p0, v1, v0}, Lcom/jess/ui/ag;->g(II)Landroid/view/View;

    move-result-object v0

    goto/16 :goto_5

    .line 1788
    :cond_d
    invoke-virtual {v2}, Landroid/view/View;->getTop()I

    move-result v0

    goto :goto_8

    .line 1789
    :cond_e
    iget-object v1, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    iget v1, v1, Lcom/jess/ui/TwoWayGridView;->H:I

    iget-object v2, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    iget v2, v2, Lcom/jess/ui/TwoWayGridView;->aa:I

    if-ge v1, v2, :cond_10

    .line 1790
    iget-object v1, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    iget v1, v1, Lcom/jess/ui/TwoWayGridView;->H:I

    if-nez v0, :cond_f

    :goto_9
    invoke-direct {p0, v1, v6}, Lcom/jess/ui/ag;->g(II)Landroid/view/View;

    move-result-object v0

    goto/16 :goto_5

    .line 1791
    :cond_f
    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v6

    goto :goto_9

    .line 1793
    :cond_10
    invoke-direct {p0, v4, v6}, Lcom/jess/ui/ag;->g(II)Landroid/view/View;

    move-result-object v0

    goto/16 :goto_5

    .line 1805
    :cond_11
    iget-object v0, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    iget v0, v0, Lcom/jess/ui/TwoWayGridView;->w:I

    if-lez v0, :cond_12

    iget-object v0, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    iget v0, v0, Lcom/jess/ui/TwoWayGridView;->w:I

    const/4 v1, 0x3

    if-ge v0, v1, :cond_12

    .line 1806
    iget-object v0, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    iget-object v1, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    iget v1, v1, Lcom/jess/ui/TwoWayGridView;->t:I

    iget-object v2, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    iget v2, v2, Lcom/jess/ui/TwoWayGridView;->H:I

    sub-int/2addr v1, v2

    invoke-virtual {v0, v1}, Lcom/jess/ui/TwoWayGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 1807
    if-eqz v0, :cond_5

    iget-object v1, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-virtual {v1, v0}, Lcom/jess/ui/TwoWayGridView;->a(Landroid/view/View;)V

    goto/16 :goto_6

    .line 1809
    :cond_12
    iget-object v0, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    iput v4, v0, Lcom/jess/ui/TwoWayGridView;->x:I

    .line 1810
    iget-object v0, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    iget-object v0, v0, Lcom/jess/ui/TwoWayGridView;->f:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->setEmpty()V

    goto/16 :goto_6

    :cond_13
    move-object v0, v1

    goto/16 :goto_0

    :cond_14
    move-object v0, v1

    move-object v2, v1

    move v3, v4

    goto/16 :goto_1

    .line 1686
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_2
    .end packed-switch

    .line 1746
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_4
        :pswitch_3
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method

.method protected a(I)V
    .locals 3

    .prologue
    .line 2004
    iget-object v0, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    iget v1, v0, Lcom/jess/ui/TwoWayGridView;->T:I

    .line 2006
    iget-object v0, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-virtual {v0, p1}, Lcom/jess/ui/TwoWayGridView;->setNextSelectedPositionInt(I)V

    .line 2007
    iget-object v0, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-virtual {v0}, Lcom/jess/ui/TwoWayGridView;->d()V

    .line 2009
    iget-object v0, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    iget-boolean v0, v0, Lcom/jess/ui/TwoWayGridView;->y:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    iget v0, v0, Lcom/jess/ui/TwoWayGridView;->aa:I

    add-int/lit8 v0, v0, -0x1

    iget-object v2, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    iget v2, v2, Lcom/jess/ui/TwoWayGridView;->T:I

    sub-int/2addr v0, v2

    .line 2011
    :goto_0
    iget-object v2, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    iget-boolean v2, v2, Lcom/jess/ui/TwoWayGridView;->y:Z

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    iget v2, v2, Lcom/jess/ui/TwoWayGridView;->aa:I

    add-int/lit8 v2, v2, -0x1

    sub-int v1, v2, v1

    .line 2014
    :cond_0
    iget-object v2, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-static {v2}, Lcom/jess/ui/TwoWayGridView;->a(Lcom/jess/ui/TwoWayGridView;)I

    move-result v2

    div-int/2addr v0, v2

    .line 2015
    iget-object v2, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-static {v2}, Lcom/jess/ui/TwoWayGridView;->a(Lcom/jess/ui/TwoWayGridView;)I

    move-result v2

    div-int/2addr v1, v2

    .line 2017
    if-eq v0, v1, :cond_1

    .line 2021
    :cond_1
    return-void

    .line 2009
    :cond_2
    iget-object v0, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    iget v0, v0, Lcom/jess/ui/TwoWayGridView;->T:I

    goto :goto_0
.end method

.method protected a(II)V
    .locals 11

    .prologue
    const/4 v2, 0x0

    .line 1598
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v1

    .line 1599
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v5

    .line 1600
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 1601
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v3

    .line 1603
    if-nez v1, :cond_a

    .line 1604
    iget-object v0, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-static {v0}, Lcom/jess/ui/TwoWayGridView;->d(Lcom/jess/ui/TwoWayGridView;)I

    move-result v0

    if-lez v0, :cond_4

    .line 1605
    iget-object v0, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-static {v0}, Lcom/jess/ui/TwoWayGridView;->d(Lcom/jess/ui/TwoWayGridView;)I

    move-result v0

    iget-object v1, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    iget-object v1, v1, Lcom/jess/ui/TwoWayGridView;->m:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    add-int/2addr v0, v1

    iget-object v1, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    iget-object v1, v1, Lcom/jess/ui/TwoWayGridView;->m:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->right:I

    add-int/2addr v0, v1

    .line 1609
    :goto_0
    iget-object v1, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-virtual {v1}, Lcom/jess/ui/TwoWayGridView;->getVerticalScrollbarWidth()I

    move-result v1

    add-int/2addr v0, v1

    move v1, v0

    .line 1612
    :goto_1
    iget-object v0, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    iget-object v0, v0, Lcom/jess/ui/TwoWayGridView;->m:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    sub-int v0, v1, v0

    iget-object v4, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    iget-object v4, v4, Lcom/jess/ui/TwoWayGridView;->m:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->right:I

    sub-int/2addr v0, v4

    .line 1613
    invoke-direct {p0, v0}, Lcom/jess/ui/ag;->d(I)V

    .line 1617
    iget-object v4, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    iget-object v0, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    iget-object v0, v0, Lcom/jess/ui/TwoWayGridView;->c:Landroid/widget/ListAdapter;

    if-nez v0, :cond_5

    move v0, v2

    :goto_2
    iput v0, v4, Lcom/jess/ui/TwoWayGridView;->aa:I

    .line 1618
    iget-object v0, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    iget v6, v0, Lcom/jess/ui/TwoWayGridView;->aa:I

    .line 1619
    if-lez v6, :cond_9

    .line 1620
    iget-object v0, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    iget-object v4, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    iget-object v4, v4, Lcom/jess/ui/TwoWayGridView;->E:[Z

    invoke-virtual {v0, v2, v4}, Lcom/jess/ui/TwoWayGridView;->a(I[Z)Landroid/view/View;

    move-result-object v7

    .line 1622
    invoke-virtual {v7}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/jess/ui/h;

    .line 1623
    if-nez v0, :cond_0

    .line 1624
    new-instance v0, Lcom/jess/ui/h;

    const/4 v4, -0x1

    const/4 v8, -0x2

    invoke-direct {v0, v4, v8, v2}, Lcom/jess/ui/h;-><init>(III)V

    .line 1626
    invoke-virtual {v7, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1628
    :cond_0
    iget-object v4, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    iget-object v4, v4, Lcom/jess/ui/TwoWayGridView;->c:Landroid/widget/ListAdapter;

    invoke-interface {v4, v2}, Landroid/widget/ListAdapter;->getItemViewType(I)I

    move-result v4

    iput v4, v0, Lcom/jess/ui/h;->a:I

    .line 1629
    const/4 v4, 0x1

    iput-boolean v4, v0, Lcom/jess/ui/h;->b:Z

    .line 1632
    invoke-static {v2, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    iget v8, v0, Lcom/jess/ui/h;->height:I

    .line 1631
    invoke-static {v4, v2, v8}, Landroid/view/ViewGroup;->getChildMeasureSpec(III)I

    move-result v4

    .line 1633
    iget-object v8, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    .line 1634
    invoke-static {v8}, Lcom/jess/ui/TwoWayGridView;->d(Lcom/jess/ui/TwoWayGridView;)I

    move-result v8

    const/high16 v9, 0x40000000    # 2.0f

    invoke-static {v8, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v8

    iget v9, v0, Lcom/jess/ui/h;->width:I

    .line 1633
    invoke-static {v8, v2, v9}, Landroid/view/ViewGroup;->getChildMeasureSpec(III)I

    move-result v8

    .line 1635
    invoke-virtual {v7, v8, v4}, Landroid/view/View;->measure(II)V

    .line 1637
    invoke-virtual {v7}, Landroid/view/View;->getMeasuredHeight()I

    move-result v4

    .line 1639
    iget-object v8, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    iget-object v8, v8, Lcom/jess/ui/TwoWayGridView;->g:Lcom/jess/ui/k;

    iget v0, v0, Lcom/jess/ui/h;->a:I

    invoke-virtual {v8, v0}, Lcom/jess/ui/k;->b(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1640
    iget-object v0, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    iget-object v0, v0, Lcom/jess/ui/TwoWayGridView;->g:Lcom/jess/ui/k;

    invoke-virtual {v0, v7}, Lcom/jess/ui/k;->a(Landroid/view/View;)V

    .line 1644
    :cond_1
    :goto_3
    if-nez v5, :cond_8

    .line 1645
    iget-object v0, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    iget-object v0, v0, Lcom/jess/ui/TwoWayGridView;->m:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->top:I

    iget-object v3, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    iget-object v3, v3, Lcom/jess/ui/TwoWayGridView;->m:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v0, v3

    add-int/2addr v0, v4

    iget-object v3, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    .line 1646
    invoke-virtual {v3}, Lcom/jess/ui/TwoWayGridView;->getVerticalFadingEdgeLength()I

    move-result v3

    mul-int/lit8 v3, v3, 0x2

    add-int/2addr v0, v3

    .line 1649
    :goto_4
    const/high16 v3, -0x80000000

    if-ne v5, v3, :cond_3

    .line 1650
    iget-object v3, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    iget-object v3, v3, Lcom/jess/ui/TwoWayGridView;->m:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->top:I

    iget-object v5, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    iget-object v5, v5, Lcom/jess/ui/TwoWayGridView;->m:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v3, v5

    .line 1652
    iget-object v5, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-static {v5}, Lcom/jess/ui/TwoWayGridView;->a(Lcom/jess/ui/TwoWayGridView;)I

    move-result v5

    move v10, v2

    move v2, v3

    move v3, v10

    .line 1653
    :goto_5
    if-ge v3, v6, :cond_7

    .line 1654
    add-int/2addr v2, v4

    .line 1655
    add-int v7, v3, v5

    if-ge v7, v6, :cond_2

    .line 1656
    iget-object v7, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-static {v7}, Lcom/jess/ui/TwoWayGridView;->b(Lcom/jess/ui/TwoWayGridView;)I

    move-result v7

    add-int/2addr v2, v7

    .line 1658
    :cond_2
    if-lt v2, v0, :cond_6

    .line 1666
    :cond_3
    :goto_6
    iget-object v2, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-static {v2, v1, v0}, Lcom/jess/ui/TwoWayGridView;->a(Lcom/jess/ui/TwoWayGridView;II)V

    .line 1667
    iget-object v0, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    iput p1, v0, Lcom/jess/ui/TwoWayGridView;->n:I

    .line 1669
    return-void

    .line 1607
    :cond_4
    iget-object v0, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    iget-object v0, v0, Lcom/jess/ui/TwoWayGridView;->m:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    iget-object v1, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    iget-object v1, v1, Lcom/jess/ui/TwoWayGridView;->m:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->right:I

    add-int/2addr v0, v1

    goto/16 :goto_0

    .line 1617
    :cond_5
    iget-object v0, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    iget-object v0, v0, Lcom/jess/ui/TwoWayGridView;->c:Landroid/widget/ListAdapter;

    invoke-interface {v0}, Landroid/widget/ListAdapter;->getCount()I

    move-result v0

    goto/16 :goto_2

    .line 1653
    :cond_6
    add-int/2addr v3, v5

    goto :goto_5

    :cond_7
    move v0, v2

    goto :goto_6

    :cond_8
    move v0, v3

    goto :goto_4

    :cond_9
    move v4, v2

    goto :goto_3

    :cond_a
    move v1, v0

    goto/16 :goto_1
.end method

.method protected a(Z)V
    .locals 5

    .prologue
    .line 889
    iget-object v0, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-static {v0}, Lcom/jess/ui/TwoWayGridView;->a(Lcom/jess/ui/TwoWayGridView;)I

    move-result v2

    .line 890
    iget-object v0, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-static {v0}, Lcom/jess/ui/TwoWayGridView;->b(Lcom/jess/ui/TwoWayGridView;)I

    move-result v3

    .line 892
    iget-object v0, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-virtual {v0}, Lcom/jess/ui/TwoWayGridView;->getChildCount()I

    move-result v1

    .line 894
    if-eqz p1, :cond_2

    .line 895
    if-lez v1, :cond_1

    iget-object v0, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    add-int/lit8 v4, v1, -0x1

    .line 896
    invoke-virtual {v0, v4}, Lcom/jess/ui/TwoWayGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getBottom()I

    move-result v0

    add-int/2addr v0, v3

    .line 897
    :goto_0
    iget-object v4, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    iget v4, v4, Lcom/jess/ui/TwoWayGridView;->H:I

    add-int/2addr v1, v4

    .line 898
    iget-object v4, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    iget-boolean v4, v4, Lcom/jess/ui/TwoWayGridView;->y:Z

    if-eqz v4, :cond_0

    .line 899
    add-int/lit8 v4, v2, -0x1

    add-int/2addr v1, v4

    .line 901
    :cond_0
    invoke-direct {p0, v1, v0}, Lcom/jess/ui/ag;->c(II)Landroid/view/View;

    .line 902
    iget-object v0, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-virtual {v0}, Lcom/jess/ui/TwoWayGridView;->getChildCount()I

    move-result v0

    invoke-direct {p0, v2, v3, v0}, Lcom/jess/ui/ag;->a(III)V

    .line 915
    :goto_1
    return-void

    .line 896
    :cond_1
    iget-object v0, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-virtual {v0}, Lcom/jess/ui/TwoWayGridView;->getListPaddingTop()I

    move-result v0

    goto :goto_0

    .line 904
    :cond_2
    if-lez v1, :cond_3

    iget-object v0, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    const/4 v1, 0x0

    .line 905
    invoke-virtual {v0, v1}, Lcom/jess/ui/TwoWayGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v0

    sub-int/2addr v0, v3

    .line 906
    :goto_2
    iget-object v1, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    iget v1, v1, Lcom/jess/ui/TwoWayGridView;->H:I

    .line 907
    iget-object v4, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    iget-boolean v4, v4, Lcom/jess/ui/TwoWayGridView;->y:Z

    if-nez v4, :cond_4

    .line 908
    sub-int/2addr v1, v2

    .line 912
    :goto_3
    invoke-direct {p0, v1, v0}, Lcom/jess/ui/ag;->d(II)Landroid/view/View;

    .line 913
    iget-object v0, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-virtual {v0}, Lcom/jess/ui/TwoWayGridView;->getChildCount()I

    move-result v0

    invoke-direct {p0, v2, v3, v0}, Lcom/jess/ui/ag;->b(III)V

    goto :goto_1

    .line 905
    :cond_3
    iget-object v0, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-virtual {v0}, Lcom/jess/ui/TwoWayGridView;->getHeight()I

    move-result v0

    iget-object v1, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-virtual {v1}, Lcom/jess/ui/TwoWayGridView;->getListPaddingBottom()I

    move-result v1

    sub-int/2addr v0, v1

    goto :goto_2

    .line 910
    :cond_4
    add-int/lit8 v1, v1, -0x1

    goto :goto_3
.end method

.method protected b(I)Z
    .locals 8

    .prologue
    const/4 v7, 0x6

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2034
    iget-object v2, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    iget v4, v2, Lcom/jess/ui/TwoWayGridView;->V:I

    .line 2035
    iget-object v2, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-static {v2}, Lcom/jess/ui/TwoWayGridView;->a(Lcom/jess/ui/TwoWayGridView;)I

    move-result v5

    .line 2042
    iget-object v2, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    iget-boolean v2, v2, Lcom/jess/ui/TwoWayGridView;->y:Z

    if-nez v2, :cond_3

    .line 2043
    div-int v2, v4, v5

    mul-int v3, v2, v5

    .line 2044
    add-int v2, v3, v5

    add-int/lit8 v2, v2, -0x1

    iget-object v6, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    iget v6, v6, Lcom/jess/ui/TwoWayGridView;->aa:I

    add-int/lit8 v6, v6, -0x1

    invoke-static {v2, v6}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 2051
    :goto_0
    sparse-switch p1, :sswitch_data_0

    :cond_0
    move v0, v1

    .line 2082
    :goto_1
    if-eqz v0, :cond_1

    .line 2083
    iget-object v1, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-static {p1}, Landroid/view/SoundEffectConstants;->getContantForFocusDirection(I)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/jess/ui/TwoWayGridView;->playSoundEffect(I)V

    .line 2084
    iget-object v1, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-virtual {v1}, Lcom/jess/ui/TwoWayGridView;->a()V

    .line 2087
    :cond_1
    if-eqz v0, :cond_2

    .line 2091
    :cond_2
    return v0

    .line 2046
    :cond_3
    iget-object v2, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    iget v2, v2, Lcom/jess/ui/TwoWayGridView;->aa:I

    add-int/lit8 v2, v2, -0x1

    sub-int/2addr v2, v4

    .line 2047
    iget-object v3, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    iget v3, v3, Lcom/jess/ui/TwoWayGridView;->aa:I

    add-int/lit8 v3, v3, -0x1

    div-int/2addr v2, v5

    mul-int/2addr v2, v5

    sub-int v2, v3, v2

    .line 2048
    sub-int v3, v2, v5

    add-int/lit8 v3, v3, 0x1

    invoke-static {v1, v3}, Ljava/lang/Math;->max(II)I

    move-result v3

    goto :goto_0

    .line 2053
    :sswitch_0
    if-lez v3, :cond_0

    .line 2054
    iget-object v2, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    iput v7, v2, Lcom/jess/ui/TwoWayGridView;->a:I

    .line 2055
    sub-int v2, v4, v5

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/jess/ui/ag;->a(I)V

    goto :goto_1

    .line 2060
    :sswitch_1
    iget-object v3, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    iget v3, v3, Lcom/jess/ui/TwoWayGridView;->aa:I

    add-int/lit8 v3, v3, -0x1

    if-ge v2, v3, :cond_0

    .line 2061
    iget-object v1, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    iput v7, v1, Lcom/jess/ui/TwoWayGridView;->a:I

    .line 2062
    add-int v1, v4, v5

    iget-object v2, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    iget v2, v2, Lcom/jess/ui/TwoWayGridView;->aa:I

    add-int/lit8 v2, v2, -0x1

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/jess/ui/ag;->a(I)V

    goto :goto_1

    .line 2067
    :sswitch_2
    if-le v4, v3, :cond_0

    .line 2068
    iget-object v2, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    iput v7, v2, Lcom/jess/ui/TwoWayGridView;->a:I

    .line 2069
    add-int/lit8 v2, v4, -0x1

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/jess/ui/ag;->a(I)V

    goto :goto_1

    .line 2074
    :sswitch_3
    if-ge v4, v2, :cond_0

    .line 2075
    iget-object v1, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    iput v7, v1, Lcom/jess/ui/TwoWayGridView;->a:I

    .line 2076
    add-int/lit8 v1, v4, 0x1

    iget-object v2, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    iget v2, v2, Lcom/jess/ui/TwoWayGridView;->aa:I

    add-int/lit8 v2, v2, -0x1

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/jess/ui/ag;->a(I)V

    goto :goto_1

    .line 2051
    :sswitch_data_0
    .sparse-switch
        0x11 -> :sswitch_2
        0x21 -> :sswitch_0
        0x42 -> :sswitch_3
        0x82 -> :sswitch_1
    .end sparse-switch
.end method

.method protected b(II)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2104
    iget-object v2, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-virtual {v2}, Lcom/jess/ui/TwoWayGridView;->getChildCount()I

    move-result v4

    .line 2105
    add-int/lit8 v2, v4, -0x1

    sub-int/2addr v2, p1

    .line 2110
    iget-object v3, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    iget-boolean v3, v3, Lcom/jess/ui/TwoWayGridView;->y:Z

    if-nez v3, :cond_0

    .line 2111
    iget-object v2, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-static {v2}, Lcom/jess/ui/TwoWayGridView;->a(Lcom/jess/ui/TwoWayGridView;)I

    move-result v2

    rem-int v2, p1, v2

    sub-int v3, p1, v2

    .line 2112
    iget-object v2, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-static {v2}, Lcom/jess/ui/TwoWayGridView;->a(Lcom/jess/ui/TwoWayGridView;)I

    move-result v2

    add-int/2addr v2, v3

    add-int/lit8 v2, v2, -0x1

    invoke-static {v2, v4}, Ljava/lang/Math;->max(II)I

    move-result v2

    .line 2118
    :goto_0
    sparse-switch p2, :sswitch_data_0

    .line 2139
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "direction must be one of {FOCUS_UP, FOCUS_DOWN, FOCUS_LEFT, FOCUS_RIGHT, FOCUS_FORWARD, FOCUS_BACKWARD}"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2114
    :cond_0
    add-int/lit8 v3, v4, -0x1

    iget-object v5, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-static {v5}, Lcom/jess/ui/TwoWayGridView;->a(Lcom/jess/ui/TwoWayGridView;)I

    move-result v5

    rem-int v5, v2, v5

    sub-int/2addr v2, v5

    sub-int v2, v3, v2

    .line 2115
    iget-object v3, p0, Lcom/jess/ui/ag;->b:Lcom/jess/ui/TwoWayGridView;

    invoke-static {v3}, Lcom/jess/ui/TwoWayGridView;->a(Lcom/jess/ui/TwoWayGridView;)I

    move-result v3

    sub-int v3, v2, v3

    add-int/lit8 v3, v3, 0x1

    invoke-static {v1, v3}, Ljava/lang/Math;->max(II)I

    move-result v3

    goto :goto_0

    .line 2122
    :sswitch_0
    if-ne p1, v3, :cond_2

    .line 2137
    :cond_1
    :goto_1
    return v0

    :cond_2
    move v0, v1

    .line 2122
    goto :goto_1

    .line 2125
    :sswitch_1
    if-eqz v3, :cond_1

    move v0, v1

    goto :goto_1

    .line 2128
    :sswitch_2
    if-eq p1, v2, :cond_1

    move v0, v1

    goto :goto_1

    .line 2131
    :sswitch_3
    add-int/lit8 v3, v4, -0x1

    if-eq v2, v3, :cond_1

    move v0, v1

    goto :goto_1

    .line 2134
    :sswitch_4
    if-ne p1, v3, :cond_3

    if-eqz v3, :cond_1

    :cond_3
    move v0, v1

    goto :goto_1

    .line 2137
    :sswitch_5
    if-ne p1, v2, :cond_4

    add-int/lit8 v3, v4, -0x1

    if-eq v2, v3, :cond_1

    :cond_4
    move v0, v1

    goto :goto_1

    .line 2118
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_5
        0x2 -> :sswitch_4
        0x11 -> :sswitch_2
        0x21 -> :sswitch_3
        0x42 -> :sswitch_0
        0x82 -> :sswitch_1
    .end sparse-switch
.end method
