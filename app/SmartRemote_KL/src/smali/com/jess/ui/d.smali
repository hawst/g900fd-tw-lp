.class final Lcom/jess/ui/d;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/jess/ui/TwoWayAbsListView;


# direct methods
.method constructor <init>(Lcom/jess/ui/TwoWayAbsListView;)V
    .locals 0

    .prologue
    .line 2043
    iput-object p1, p0, Lcom/jess/ui/d;->a:Lcom/jess/ui/TwoWayAbsListView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    .line 2045
    iget-object v0, p0, Lcom/jess/ui/d;->a:Lcom/jess/ui/TwoWayAbsListView;

    iget v0, v0, Lcom/jess/ui/TwoWayAbsListView;->w:I

    if-nez v0, :cond_2

    .line 2046
    iget-object v0, p0, Lcom/jess/ui/d;->a:Lcom/jess/ui/TwoWayAbsListView;

    iput v3, v0, Lcom/jess/ui/TwoWayAbsListView;->w:I

    .line 2047
    iget-object v0, p0, Lcom/jess/ui/d;->a:Lcom/jess/ui/TwoWayAbsListView;

    iget-object v1, p0, Lcom/jess/ui/d;->a:Lcom/jess/ui/TwoWayAbsListView;

    iget v1, v1, Lcom/jess/ui/TwoWayAbsListView;->t:I

    iget-object v2, p0, Lcom/jess/ui/d;->a:Lcom/jess/ui/TwoWayAbsListView;

    iget v2, v2, Lcom/jess/ui/TwoWayAbsListView;->H:I

    sub-int/2addr v1, v2

    invoke-virtual {v0, v1}, Lcom/jess/ui/TwoWayAbsListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 2048
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Landroid/view/View;->hasFocusable()Z

    move-result v1

    if-nez v1, :cond_2

    .line 2049
    iget-object v1, p0, Lcom/jess/ui/d;->a:Lcom/jess/ui/TwoWayAbsListView;

    const/4 v2, 0x0

    iput v2, v1, Lcom/jess/ui/TwoWayAbsListView;->a:I

    .line 2051
    iget-object v1, p0, Lcom/jess/ui/d;->a:Lcom/jess/ui/TwoWayAbsListView;

    iget-boolean v1, v1, Lcom/jess/ui/TwoWayAbsListView;->S:Z

    if-nez v1, :cond_5

    .line 2052
    iget-object v1, p0, Lcom/jess/ui/d;->a:Lcom/jess/ui/TwoWayAbsListView;

    invoke-virtual {v1}, Lcom/jess/ui/TwoWayAbsListView;->d()V

    .line 2053
    invoke-virtual {v0, v3}, Landroid/view/View;->setPressed(Z)V

    .line 2054
    iget-object v1, p0, Lcom/jess/ui/d;->a:Lcom/jess/ui/TwoWayAbsListView;

    invoke-virtual {v1, v0}, Lcom/jess/ui/TwoWayAbsListView;->a(Landroid/view/View;)V

    .line 2055
    iget-object v0, p0, Lcom/jess/ui/d;->a:Lcom/jess/ui/TwoWayAbsListView;

    invoke-virtual {v0, v3}, Lcom/jess/ui/TwoWayAbsListView;->setPressed(Z)V

    .line 2057
    invoke-static {}, Landroid/view/ViewConfiguration;->getLongPressTimeout()I

    move-result v1

    .line 2058
    iget-object v0, p0, Lcom/jess/ui/d;->a:Lcom/jess/ui/TwoWayAbsListView;

    invoke-virtual {v0}, Lcom/jess/ui/TwoWayAbsListView;->isLongClickable()Z

    move-result v2

    .line 2060
    iget-object v0, p0, Lcom/jess/ui/d;->a:Lcom/jess/ui/TwoWayAbsListView;

    iget-object v0, v0, Lcom/jess/ui/TwoWayAbsListView;->e:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    .line 2061
    iget-object v0, p0, Lcom/jess/ui/d;->a:Lcom/jess/ui/TwoWayAbsListView;

    iget-object v0, v0, Lcom/jess/ui/TwoWayAbsListView;->e:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getCurrent()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 2062
    if-eqz v0, :cond_0

    instance-of v3, v0, Landroid/graphics/drawable/TransitionDrawable;

    if-eqz v3, :cond_0

    .line 2063
    if-eqz v2, :cond_3

    .line 2064
    check-cast v0, Landroid/graphics/drawable/TransitionDrawable;

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/TransitionDrawable;->startTransition(I)V

    .line 2071
    :cond_0
    :goto_0
    if-eqz v2, :cond_4

    .line 2072
    iget-object v0, p0, Lcom/jess/ui/d;->a:Lcom/jess/ui/TwoWayAbsListView;

    invoke-static {v0}, Lcom/jess/ui/TwoWayAbsListView;->c(Lcom/jess/ui/TwoWayAbsListView;)Lcom/jess/ui/c;

    move-result-object v0

    if-nez v0, :cond_1

    .line 2073
    iget-object v0, p0, Lcom/jess/ui/d;->a:Lcom/jess/ui/TwoWayAbsListView;

    new-instance v2, Lcom/jess/ui/c;

    iget-object v3, p0, Lcom/jess/ui/d;->a:Lcom/jess/ui/TwoWayAbsListView;

    const/4 v4, 0x0

    invoke-direct {v2, v3, v4}, Lcom/jess/ui/c;-><init>(Lcom/jess/ui/TwoWayAbsListView;Lcom/jess/ui/a;)V

    invoke-static {v0, v2}, Lcom/jess/ui/TwoWayAbsListView;->a(Lcom/jess/ui/TwoWayAbsListView;Lcom/jess/ui/c;)Lcom/jess/ui/c;

    .line 2075
    :cond_1
    iget-object v0, p0, Lcom/jess/ui/d;->a:Lcom/jess/ui/TwoWayAbsListView;

    invoke-static {v0}, Lcom/jess/ui/TwoWayAbsListView;->c(Lcom/jess/ui/TwoWayAbsListView;)Lcom/jess/ui/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/jess/ui/c;->a()V

    .line 2076
    iget-object v0, p0, Lcom/jess/ui/d;->a:Lcom/jess/ui/TwoWayAbsListView;

    iget-object v2, p0, Lcom/jess/ui/d;->a:Lcom/jess/ui/TwoWayAbsListView;

    invoke-static {v2}, Lcom/jess/ui/TwoWayAbsListView;->c(Lcom/jess/ui/TwoWayAbsListView;)Lcom/jess/ui/c;

    move-result-object v2

    int-to-long v4, v1

    invoke-virtual {v0, v2, v4, v5}, Lcom/jess/ui/TwoWayAbsListView;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 2085
    :cond_2
    :goto_1
    return-void

    .line 2066
    :cond_3
    check-cast v0, Landroid/graphics/drawable/TransitionDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/TransitionDrawable;->resetTransition()V

    goto :goto_0

    .line 2078
    :cond_4
    iget-object v0, p0, Lcom/jess/ui/d;->a:Lcom/jess/ui/TwoWayAbsListView;

    iput v4, v0, Lcom/jess/ui/TwoWayAbsListView;->w:I

    goto :goto_1

    .line 2081
    :cond_5
    iget-object v0, p0, Lcom/jess/ui/d;->a:Lcom/jess/ui/TwoWayAbsListView;

    iput v4, v0, Lcom/jess/ui/TwoWayAbsListView;->w:I

    goto :goto_1
.end method
