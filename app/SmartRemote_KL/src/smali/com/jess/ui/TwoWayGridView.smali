.class public Lcom/jess/ui/TwoWayGridView;
.super Lcom/jess/ui/TwoWayAbsListView;


# instance fields
.field protected af:Lcom/jess/ui/ae;

.field private ag:I

.field private ah:I

.field private ai:I

.field private aj:I

.field private ak:I

.field private al:I

.field private am:I

.field private an:I

.field private ao:I

.field private ap:I

.field private aq:I

.field private ar:I

.field private as:I

.field private at:Landroid/view/View;

.field private au:Landroid/view/View;

.field private av:I

.field private final aw:Landroid/graphics/Rect;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 95
    invoke-direct {p0, p1}, Lcom/jess/ui/TwoWayAbsListView;-><init>(Landroid/content/Context;)V

    .line 70
    iput v2, p0, Lcom/jess/ui/TwoWayGridView;->ag:I

    .line 71
    iput v2, p0, Lcom/jess/ui/TwoWayGridView;->ah:I

    .line 73
    iput v0, p0, Lcom/jess/ui/TwoWayGridView;->ai:I

    .line 75
    iput v0, p0, Lcom/jess/ui/TwoWayGridView;->ak:I

    .line 77
    const/4 v0, 0x2

    iput v0, p0, Lcom/jess/ui/TwoWayGridView;->am:I

    .line 85
    iput-object v1, p0, Lcom/jess/ui/TwoWayGridView;->at:Landroid/view/View;

    .line 86
    iput-object v1, p0, Lcom/jess/ui/TwoWayGridView;->au:Landroid/view/View;

    .line 88
    const/4 v0, 0x3

    iput v0, p0, Lcom/jess/ui/TwoWayGridView;->av:I

    .line 90
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/jess/ui/TwoWayGridView;->aw:Landroid/graphics/Rect;

    .line 92
    iput-object v1, p0, Lcom/jess/ui/TwoWayGridView;->af:Lcom/jess/ui/ae;

    .line 96
    invoke-direct {p0}, Lcom/jess/ui/TwoWayGridView;->s()V

    .line 97
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 100
    const v0, 0x1010071

    invoke-direct {p0, p1, p2, v0}, Lcom/jess/ui/TwoWayGridView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 101
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v1, 0x0

    const/4 v4, -0x1

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 104
    invoke-direct {p0, p1, p2, p3}, Lcom/jess/ui/TwoWayAbsListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 70
    iput v3, p0, Lcom/jess/ui/TwoWayGridView;->ag:I

    .line 71
    iput v3, p0, Lcom/jess/ui/TwoWayGridView;->ah:I

    .line 73
    iput v2, p0, Lcom/jess/ui/TwoWayGridView;->ai:I

    .line 75
    iput v2, p0, Lcom/jess/ui/TwoWayGridView;->ak:I

    .line 77
    iput v5, p0, Lcom/jess/ui/TwoWayGridView;->am:I

    .line 85
    iput-object v1, p0, Lcom/jess/ui/TwoWayGridView;->at:Landroid/view/View;

    .line 86
    iput-object v1, p0, Lcom/jess/ui/TwoWayGridView;->au:Landroid/view/View;

    .line 88
    const/4 v0, 0x3

    iput v0, p0, Lcom/jess/ui/TwoWayGridView;->av:I

    .line 90
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/jess/ui/TwoWayGridView;->aw:Landroid/graphics/Rect;

    .line 92
    iput-object v1, p0, Lcom/jess/ui/TwoWayGridView;->af:Lcom/jess/ui/ae;

    .line 106
    sget-object v0, Lcom/peel/ui/fv;->TwoWayGridView:[I

    invoke-virtual {p1, p2, v0, p3, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 109
    sget v1, Lcom/peel/ui/fv;->TwoWayGridView_horizontalSpacing:I

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result v1

    .line 111
    invoke-virtual {p0, v1}, Lcom/jess/ui/TwoWayGridView;->setHorizontalSpacing(I)V

    .line 113
    sget v1, Lcom/peel/ui/fv;->TwoWayGridView_verticalSpacing:I

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result v1

    .line 115
    invoke-virtual {p0, v1}, Lcom/jess/ui/TwoWayGridView;->setVerticalSpacing(I)V

    .line 117
    sget v1, Lcom/peel/ui/fv;->TwoWayGridView_stretchMode:I

    invoke-virtual {v0, v1, v5}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    .line 118
    if-ltz v1, :cond_0

    .line 119
    invoke-virtual {p0, v1}, Lcom/jess/ui/TwoWayGridView;->setStretchMode(I)V

    .line 122
    :cond_0
    sget v1, Lcom/peel/ui/fv;->TwoWayGridView_columnWidth:I

    invoke-virtual {v0, v1, v4}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result v1

    .line 123
    if-lez v1, :cond_1

    .line 124
    invoke-virtual {p0, v1}, Lcom/jess/ui/TwoWayGridView;->setColumnWidth(I)V

    .line 127
    :cond_1
    sget v1, Lcom/peel/ui/fv;->TwoWayGridView_rowHeight:I

    invoke-virtual {v0, v1, v4}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result v1

    .line 128
    if-lez v1, :cond_2

    .line 129
    invoke-virtual {p0, v1}, Lcom/jess/ui/TwoWayGridView;->setRowHeight(I)V

    .line 132
    :cond_2
    sget v1, Lcom/peel/ui/fv;->TwoWayGridView_numColumns:I

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    .line 133
    invoke-virtual {p0, v1}, Lcom/jess/ui/TwoWayGridView;->setNumColumns(I)V

    .line 135
    sget v1, Lcom/peel/ui/fv;->TwoWayGridView_numRows:I

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    .line 136
    invoke-virtual {p0, v1}, Lcom/jess/ui/TwoWayGridView;->setNumRows(I)V

    .line 138
    sget v1, Lcom/peel/ui/fv;->TwoWayGridView_gravity:I

    invoke-virtual {v0, v1, v4}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    .line 139
    if-ltz v1, :cond_3

    .line 140
    invoke-virtual {p0, v1}, Lcom/jess/ui/TwoWayGridView;->setGravity(I)V

    .line 143
    :cond_3
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 144
    invoke-direct {p0}, Lcom/jess/ui/TwoWayGridView;->s()V

    .line 145
    return-void
.end method

.method static synthetic a(Lcom/jess/ui/TwoWayGridView;)I
    .locals 1

    .prologue
    .line 59
    iget v0, p0, Lcom/jess/ui/TwoWayGridView;->ag:I

    return v0
.end method

.method static synthetic a(Lcom/jess/ui/TwoWayGridView;I)I
    .locals 0

    .prologue
    .line 59
    iput p1, p0, Lcom/jess/ui/TwoWayGridView;->ak:I

    return p1
.end method

.method static synthetic a(Lcom/jess/ui/TwoWayGridView;Landroid/view/View;)Landroid/view/View;
    .locals 0

    .prologue
    .line 59
    iput-object p1, p0, Lcom/jess/ui/TwoWayGridView;->at:Landroid/view/View;

    return-object p1
.end method

.method static synthetic a(Lcom/jess/ui/TwoWayGridView;II)V
    .locals 0

    .prologue
    .line 59
    invoke-virtual {p0, p1, p2}, Lcom/jess/ui/TwoWayGridView;->setMeasuredDimension(II)V

    return-void
.end method

.method static synthetic a(Lcom/jess/ui/TwoWayGridView;Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V
    .locals 0

    .prologue
    .line 59
    invoke-virtual {p0, p1, p2, p3}, Lcom/jess/ui/TwoWayGridView;->attachViewToParent(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method

.method private a(IILandroid/view/KeyEvent;)Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    const/16 v5, 0x82

    const/16 v4, 0x21

    const/4 v1, 0x1

    .line 391
    iget-object v2, p0, Lcom/jess/ui/TwoWayGridView;->c:Landroid/widget/ListAdapter;

    if-nez v2, :cond_0

    .line 487
    :goto_0
    return v0

    .line 395
    :cond_0
    iget-boolean v2, p0, Lcom/jess/ui/TwoWayGridView;->S:Z

    if-eqz v2, :cond_1

    .line 396
    invoke-virtual {p0}, Lcom/jess/ui/TwoWayGridView;->d()V

    .line 400
    :cond_1
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v3

    .line 402
    if-eq v3, v1, :cond_3

    .line 403
    iget v2, p0, Lcom/jess/ui/TwoWayGridView;->V:I

    if-gez v2, :cond_2

    .line 404
    sparse-switch p1, :sswitch_data_0

    .line 417
    :cond_2
    sparse-switch p1, :sswitch_data_1

    :cond_3
    move v2, v0

    .line 476
    :goto_1
    if-eqz v2, :cond_a

    move v0, v1

    .line 477
    goto :goto_0

    .line 412
    :sswitch_0
    invoke-virtual {p0}, Lcom/jess/ui/TwoWayGridView;->k()Z

    move v0, v1

    .line 413
    goto :goto_0

    .line 419
    :sswitch_1
    invoke-virtual {p3}, Landroid/view/KeyEvent;->isAltPressed()Z

    move-result v2

    if-nez v2, :cond_4

    .line 420
    iget-object v2, p0, Lcom/jess/ui/TwoWayGridView;->af:Lcom/jess/ui/ae;

    const/16 v4, 0x11

    invoke-virtual {v2, v4}, Lcom/jess/ui/ae;->b(I)Z

    move-result v2

    goto :goto_1

    .line 422
    :cond_4
    invoke-virtual {p0, v4}, Lcom/jess/ui/TwoWayGridView;->h(I)Z

    move-result v2

    goto :goto_1

    .line 427
    :sswitch_2
    invoke-virtual {p3}, Landroid/view/KeyEvent;->isAltPressed()Z

    move-result v2

    if-nez v2, :cond_5

    .line 428
    iget-object v2, p0, Lcom/jess/ui/TwoWayGridView;->af:Lcom/jess/ui/ae;

    const/16 v4, 0x42

    invoke-virtual {v2, v4}, Lcom/jess/ui/ae;->b(I)Z

    move-result v2

    goto :goto_1

    .line 430
    :cond_5
    invoke-virtual {p0, v5}, Lcom/jess/ui/TwoWayGridView;->h(I)Z

    move-result v2

    goto :goto_1

    .line 435
    :sswitch_3
    invoke-virtual {p3}, Landroid/view/KeyEvent;->isAltPressed()Z

    move-result v2

    if-nez v2, :cond_6

    .line 436
    iget-object v2, p0, Lcom/jess/ui/TwoWayGridView;->af:Lcom/jess/ui/ae;

    invoke-virtual {v2, v4}, Lcom/jess/ui/ae;->b(I)Z

    move-result v2

    goto :goto_1

    .line 439
    :cond_6
    invoke-virtual {p0, v4}, Lcom/jess/ui/TwoWayGridView;->h(I)Z

    move-result v2

    goto :goto_1

    .line 444
    :sswitch_4
    invoke-virtual {p3}, Landroid/view/KeyEvent;->isAltPressed()Z

    move-result v2

    if-nez v2, :cond_7

    .line 445
    iget-object v2, p0, Lcom/jess/ui/TwoWayGridView;->af:Lcom/jess/ui/ae;

    invoke-virtual {v2, v5}, Lcom/jess/ui/ae;->b(I)Z

    move-result v2

    goto :goto_1

    .line 447
    :cond_7
    invoke-virtual {p0, v5}, Lcom/jess/ui/TwoWayGridView;->h(I)Z

    move-result v2

    goto :goto_1

    .line 453
    :sswitch_5
    invoke-virtual {p0}, Lcom/jess/ui/TwoWayGridView;->getChildCount()I

    move-result v0

    if-lez v0, :cond_8

    invoke-virtual {p3}, Landroid/view/KeyEvent;->getRepeatCount()I

    move-result v0

    if-nez v0, :cond_8

    .line 454
    invoke-virtual {p0}, Lcom/jess/ui/TwoWayGridView;->h()V

    :cond_8
    move v0, v1

    .line 457
    goto :goto_0

    .line 462
    :sswitch_6
    invoke-virtual {p3}, Landroid/view/KeyEvent;->isShiftPressed()Z

    move-result v2

    if-nez v2, :cond_9

    .line 463
    invoke-virtual {p0, v5}, Lcom/jess/ui/TwoWayGridView;->g(I)Z

    move-result v2

    goto :goto_1

    .line 465
    :cond_9
    invoke-virtual {p0, v4}, Lcom/jess/ui/TwoWayGridView;->g(I)Z

    move-result v2

    goto :goto_1

    .line 479
    :cond_a
    packed-switch v3, :pswitch_data_0

    goto/16 :goto_0

    .line 481
    :pswitch_0
    invoke-super {p0, p1, p3}, Lcom/jess/ui/TwoWayAbsListView;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto/16 :goto_0

    .line 483
    :pswitch_1
    invoke-super {p0, p1, p3}, Lcom/jess/ui/TwoWayAbsListView;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto/16 :goto_0

    .line 485
    :pswitch_2
    invoke-super {p0, p1, p2, p3}, Lcom/jess/ui/TwoWayAbsListView;->onKeyMultiple(IILandroid/view/KeyEvent;)Z

    move-result v0

    goto/16 :goto_0

    .line 404
    :sswitch_data_0
    .sparse-switch
        0x13 -> :sswitch_0
        0x14 -> :sswitch_0
        0x15 -> :sswitch_0
        0x16 -> :sswitch_0
        0x17 -> :sswitch_0
        0x3e -> :sswitch_0
        0x42 -> :sswitch_0
    .end sparse-switch

    .line 417
    :sswitch_data_1
    .sparse-switch
        0x13 -> :sswitch_3
        0x14 -> :sswitch_4
        0x15 -> :sswitch_1
        0x16 -> :sswitch_2
        0x17 -> :sswitch_5
        0x3e -> :sswitch_6
        0x42 -> :sswitch_5
    .end sparse-switch

    .line 479
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method static synthetic a(Lcom/jess/ui/TwoWayGridView;Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;Z)Z
    .locals 1

    .prologue
    .line 59
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/jess/ui/TwoWayGridView;->addViewInLayout(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;Z)Z

    move-result v0

    return v0
.end method

.method static synthetic b(Lcom/jess/ui/TwoWayGridView;)I
    .locals 1

    .prologue
    .line 59
    iget v0, p0, Lcom/jess/ui/TwoWayGridView;->ak:I

    return v0
.end method

.method static synthetic b(Lcom/jess/ui/TwoWayGridView;I)I
    .locals 0

    .prologue
    .line 59
    iput p1, p0, Lcom/jess/ui/TwoWayGridView;->ag:I

    return p1
.end method

.method static synthetic b(Lcom/jess/ui/TwoWayGridView;Landroid/view/View;)Landroid/view/View;
    .locals 0

    .prologue
    .line 59
    iput-object p1, p0, Lcom/jess/ui/TwoWayGridView;->au:Landroid/view/View;

    return-object p1
.end method

.method static synthetic b(Lcom/jess/ui/TwoWayGridView;II)V
    .locals 0

    .prologue
    .line 59
    invoke-virtual {p0, p1, p2}, Lcom/jess/ui/TwoWayGridView;->setMeasuredDimension(II)V

    return-void
.end method

.method static synthetic b(Lcom/jess/ui/TwoWayGridView;Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V
    .locals 0

    .prologue
    .line 59
    invoke-virtual {p0, p1, p2, p3}, Lcom/jess/ui/TwoWayGridView;->attachViewToParent(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method

.method static synthetic b(Lcom/jess/ui/TwoWayGridView;Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;Z)Z
    .locals 1

    .prologue
    .line 59
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/jess/ui/TwoWayGridView;->addViewInLayout(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;Z)Z

    move-result v0

    return v0
.end method

.method static synthetic c(Lcom/jess/ui/TwoWayGridView;I)I
    .locals 0

    .prologue
    .line 59
    iput p1, p0, Lcom/jess/ui/TwoWayGridView;->an:I

    return p1
.end method

.method static synthetic c(Lcom/jess/ui/TwoWayGridView;)Landroid/view/View;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/jess/ui/TwoWayGridView;->at:Landroid/view/View;

    return-object v0
.end method

.method static synthetic c(Lcom/jess/ui/TwoWayGridView;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 59
    invoke-virtual {p0, p1}, Lcom/jess/ui/TwoWayGridView;->cleanupLayoutState(Landroid/view/View;)V

    return-void
.end method

.method static synthetic d(Lcom/jess/ui/TwoWayGridView;)I
    .locals 1

    .prologue
    .line 59
    iget v0, p0, Lcom/jess/ui/TwoWayGridView;->an:I

    return v0
.end method

.method static synthetic d(Lcom/jess/ui/TwoWayGridView;I)I
    .locals 0

    .prologue
    .line 59
    iput p1, p0, Lcom/jess/ui/TwoWayGridView;->ai:I

    return p1
.end method

.method static synthetic d(Lcom/jess/ui/TwoWayGridView;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 59
    invoke-virtual {p0, p1}, Lcom/jess/ui/TwoWayGridView;->cleanupLayoutState(Landroid/view/View;)V

    return-void
.end method

.method static synthetic e(Lcom/jess/ui/TwoWayGridView;)I
    .locals 1

    .prologue
    .line 59
    iget v0, p0, Lcom/jess/ui/TwoWayGridView;->ai:I

    return v0
.end method

.method static synthetic e(Lcom/jess/ui/TwoWayGridView;I)I
    .locals 0

    .prologue
    .line 59
    iput p1, p0, Lcom/jess/ui/TwoWayGridView;->ah:I

    return p1
.end method

.method static synthetic f(Lcom/jess/ui/TwoWayGridView;)I
    .locals 1

    .prologue
    .line 59
    iget v0, p0, Lcom/jess/ui/TwoWayGridView;->am:I

    return v0
.end method

.method static synthetic f(Lcom/jess/ui/TwoWayGridView;I)I
    .locals 0

    .prologue
    .line 59
    iput p1, p0, Lcom/jess/ui/TwoWayGridView;->aq:I

    return p1
.end method

.method static synthetic g(Lcom/jess/ui/TwoWayGridView;)I
    .locals 1

    .prologue
    .line 59
    iget v0, p0, Lcom/jess/ui/TwoWayGridView;->aj:I

    return v0
.end method

.method static synthetic h(Lcom/jess/ui/TwoWayGridView;)I
    .locals 1

    .prologue
    .line 59
    iget v0, p0, Lcom/jess/ui/TwoWayGridView;->ao:I

    return v0
.end method

.method static synthetic i(Lcom/jess/ui/TwoWayGridView;)I
    .locals 1

    .prologue
    .line 59
    iget v0, p0, Lcom/jess/ui/TwoWayGridView;->al:I

    return v0
.end method

.method static synthetic j(Lcom/jess/ui/TwoWayGridView;)I
    .locals 1

    .prologue
    .line 59
    iget v0, p0, Lcom/jess/ui/TwoWayGridView;->ap:I

    return v0
.end method

.method static synthetic k(Lcom/jess/ui/TwoWayGridView;)Landroid/view/View;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/jess/ui/TwoWayGridView;->au:Landroid/view/View;

    return-object v0
.end method

.method static synthetic l(Lcom/jess/ui/TwoWayGridView;)V
    .locals 0

    .prologue
    .line 59
    invoke-virtual {p0}, Lcom/jess/ui/TwoWayGridView;->detachAllViewsFromParent()V

    return-void
.end method

.method static synthetic m(Lcom/jess/ui/TwoWayGridView;)I
    .locals 1

    .prologue
    .line 59
    iget v0, p0, Lcom/jess/ui/TwoWayGridView;->av:I

    return v0
.end method

.method static synthetic n(Lcom/jess/ui/TwoWayGridView;)I
    .locals 1

    .prologue
    .line 59
    iget v0, p0, Lcom/jess/ui/TwoWayGridView;->ah:I

    return v0
.end method

.method static synthetic o(Lcom/jess/ui/TwoWayGridView;)I
    .locals 1

    .prologue
    .line 59
    iget v0, p0, Lcom/jess/ui/TwoWayGridView;->aq:I

    return v0
.end method

.method static synthetic p(Lcom/jess/ui/TwoWayGridView;)V
    .locals 0

    .prologue
    .line 59
    invoke-virtual {p0}, Lcom/jess/ui/TwoWayGridView;->detachAllViewsFromParent()V

    return-void
.end method

.method static synthetic q(Lcom/jess/ui/TwoWayGridView;)I
    .locals 1

    .prologue
    .line 59
    invoke-virtual {p0}, Lcom/jess/ui/TwoWayGridView;->getHorizontalScrollbarHeight()I

    move-result v0

    return v0
.end method

.method static synthetic r(Lcom/jess/ui/TwoWayGridView;)I
    .locals 1

    .prologue
    .line 59
    iget v0, p0, Lcom/jess/ui/TwoWayGridView;->ar:I

    return v0
.end method

.method static synthetic s(Lcom/jess/ui/TwoWayGridView;)I
    .locals 1

    .prologue
    .line 59
    iget v0, p0, Lcom/jess/ui/TwoWayGridView;->as:I

    return v0
.end method

.method private s()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 148
    iget-boolean v0, p0, Lcom/jess/ui/TwoWayGridView;->B:Z

    if-eqz v0, :cond_0

    .line 149
    new-instance v0, Lcom/jess/ui/ag;

    invoke-direct {v0, p0, v1}, Lcom/jess/ui/ag;-><init>(Lcom/jess/ui/TwoWayGridView;Lcom/jess/ui/ad;)V

    iput-object v0, p0, Lcom/jess/ui/TwoWayGridView;->af:Lcom/jess/ui/ae;

    .line 153
    :goto_0
    return-void

    .line 151
    :cond_0
    new-instance v0, Lcom/jess/ui/af;

    invoke-direct {v0, p0, v1}, Lcom/jess/ui/af;-><init>(Lcom/jess/ui/TwoWayGridView;Lcom/jess/ui/ad;)V

    iput-object v0, p0, Lcom/jess/ui/TwoWayGridView;->af:Lcom/jess/ui/ae;

    goto :goto_0
.end method


# virtual methods
.method a(I)I
    .locals 4

    .prologue
    .line 231
    invoke-virtual {p0}, Lcom/jess/ui/TwoWayGridView;->getChildCount()I

    move-result v1

    .line 232
    if-lez v1, :cond_3

    .line 234
    iget v2, p0, Lcom/jess/ui/TwoWayGridView;->ag:I

    .line 235
    iget-boolean v0, p0, Lcom/jess/ui/TwoWayGridView;->y:Z

    if-nez v0, :cond_1

    .line 236
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_3

    .line 237
    invoke-virtual {p0, v0}, Lcom/jess/ui/TwoWayGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->getBottom()I

    move-result v3

    if-gt p1, v3, :cond_0

    .line 238
    iget v1, p0, Lcom/jess/ui/TwoWayGridView;->H:I

    add-int/2addr v0, v1

    .line 249
    :goto_1
    return v0

    .line 236
    :cond_0
    add-int/2addr v0, v2

    goto :goto_0

    .line 242
    :cond_1
    add-int/lit8 v0, v1, -0x1

    :goto_2
    if-ltz v0, :cond_3

    .line 243
    invoke-virtual {p0, v0}, Lcom/jess/ui/TwoWayGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v1

    if-lt p1, v1, :cond_2

    .line 244
    iget v1, p0, Lcom/jess/ui/TwoWayGridView;->H:I

    add-int/2addr v0, v1

    goto :goto_1

    .line 242
    :cond_2
    sub-int/2addr v0, v2

    goto :goto_2

    .line 249
    :cond_3
    const/4 v0, -0x1

    goto :goto_1
.end method

.method a(IZ)I
    .locals 2

    .prologue
    const/4 v0, -0x1

    .line 209
    iget-object v1, p0, Lcom/jess/ui/TwoWayGridView;->c:Landroid/widget/ListAdapter;

    .line 210
    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/jess/ui/TwoWayGridView;->isInTouchMode()Z

    move-result v1

    if-eqz v1, :cond_2

    :cond_0
    move p1, v0

    .line 217
    :cond_1
    :goto_0
    return p1

    .line 214
    :cond_2
    if-ltz p1, :cond_3

    iget v1, p0, Lcom/jess/ui/TwoWayGridView;->aa:I

    if-lt p1, v1, :cond_1

    :cond_3
    move p1, v0

    .line 215
    goto :goto_0
.end method

.method a(Z)V
    .locals 1

    .prologue
    .line 226
    iget-object v0, p0, Lcom/jess/ui/TwoWayGridView;->af:Lcom/jess/ui/ae;

    invoke-virtual {v0, p1}, Lcom/jess/ui/ae;->a(Z)V

    .line 227
    return-void
.end method

.method protected attachLayoutAnimationParameters(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;II)V
    .locals 4

    .prologue
    .line 293
    iget-object v0, p2, Landroid/view/ViewGroup$LayoutParams;->layoutAnimationParameters:Landroid/view/animation/LayoutAnimationController$AnimationParameters;

    check-cast v0, Landroid/view/animation/GridLayoutAnimationController$AnimationParameters;

    .line 296
    if-nez v0, :cond_0

    .line 297
    new-instance v0, Landroid/view/animation/GridLayoutAnimationController$AnimationParameters;

    invoke-direct {v0}, Landroid/view/animation/GridLayoutAnimationController$AnimationParameters;-><init>()V

    .line 298
    iput-object v0, p2, Landroid/view/ViewGroup$LayoutParams;->layoutAnimationParameters:Landroid/view/animation/LayoutAnimationController$AnimationParameters;

    .line 301
    :cond_0
    iput p4, v0, Landroid/view/animation/GridLayoutAnimationController$AnimationParameters;->count:I

    .line 302
    iput p3, v0, Landroid/view/animation/GridLayoutAnimationController$AnimationParameters;->index:I

    .line 303
    iget v1, p0, Lcom/jess/ui/TwoWayGridView;->ag:I

    iput v1, v0, Landroid/view/animation/GridLayoutAnimationController$AnimationParameters;->columnsCount:I

    .line 304
    iget v1, p0, Lcom/jess/ui/TwoWayGridView;->ag:I

    div-int v1, p4, v1

    iput v1, v0, Landroid/view/animation/GridLayoutAnimationController$AnimationParameters;->rowsCount:I

    .line 306
    iget-boolean v1, p0, Lcom/jess/ui/TwoWayGridView;->y:Z

    if-nez v1, :cond_1

    .line 307
    iget v1, p0, Lcom/jess/ui/TwoWayGridView;->ag:I

    rem-int v1, p3, v1

    iput v1, v0, Landroid/view/animation/GridLayoutAnimationController$AnimationParameters;->column:I

    .line 308
    iget v1, p0, Lcom/jess/ui/TwoWayGridView;->ag:I

    div-int v1, p3, v1

    iput v1, v0, Landroid/view/animation/GridLayoutAnimationController$AnimationParameters;->row:I

    .line 315
    :goto_0
    return-void

    .line 310
    :cond_1
    add-int/lit8 v1, p4, -0x1

    sub-int/2addr v1, p3

    .line 312
    iget v2, p0, Lcom/jess/ui/TwoWayGridView;->ag:I

    add-int/lit8 v2, v2, -0x1

    iget v3, p0, Lcom/jess/ui/TwoWayGridView;->ag:I

    rem-int v3, v1, v3

    sub-int/2addr v2, v3

    iput v2, v0, Landroid/view/animation/GridLayoutAnimationController$AnimationParameters;->column:I

    .line 313
    iget v2, v0, Landroid/view/animation/GridLayoutAnimationController$AnimationParameters;->rowsCount:I

    add-int/lit8 v2, v2, -0x1

    iget v3, p0, Lcom/jess/ui/TwoWayGridView;->ag:I

    div-int/2addr v1, v3

    sub-int v1, v2, v1

    iput v1, v0, Landroid/view/animation/GridLayoutAnimationController$AnimationParameters;->row:I

    goto :goto_0
.end method

.method b(I)I
    .locals 4

    .prologue
    .line 254
    invoke-virtual {p0}, Lcom/jess/ui/TwoWayGridView;->getChildCount()I

    move-result v1

    .line 255
    if-lez v1, :cond_3

    .line 257
    iget v2, p0, Lcom/jess/ui/TwoWayGridView;->ah:I

    .line 258
    iget-boolean v0, p0, Lcom/jess/ui/TwoWayGridView;->y:Z

    if-nez v0, :cond_1

    .line 259
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_3

    .line 260
    invoke-virtual {p0, v0}, Lcom/jess/ui/TwoWayGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->getRight()I

    move-result v3

    if-gt p1, v3, :cond_0

    .line 261
    iget v1, p0, Lcom/jess/ui/TwoWayGridView;->H:I

    add-int/2addr v0, v1

    .line 272
    :goto_1
    return v0

    .line 259
    :cond_0
    add-int/2addr v0, v2

    goto :goto_0

    .line 265
    :cond_1
    add-int/lit8 v0, v1, -0x1

    :goto_2
    if-ltz v0, :cond_3

    .line 266
    invoke-virtual {p0, v0}, Lcom/jess/ui/TwoWayGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getLeft()I

    move-result v1

    if-lt p1, v1, :cond_2

    .line 267
    iget v1, p0, Lcom/jess/ui/TwoWayGridView;->H:I

    add-int/2addr v0, v1

    goto :goto_1

    .line 265
    :cond_2
    sub-int/2addr v0, v2

    goto :goto_2

    .line 272
    :cond_3
    const/4 v0, -0x1

    goto :goto_1
.end method

.method protected computeHorizontalScrollExtent()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 769
    invoke-virtual {p0}, Lcom/jess/ui/TwoWayGridView;->getChildCount()I

    move-result v2

    .line 770
    if-lez v2, :cond_2

    iget-boolean v0, p0, Lcom/jess/ui/TwoWayGridView;->B:Z

    if-nez v0, :cond_2

    .line 771
    iget v0, p0, Lcom/jess/ui/TwoWayGridView;->ah:I

    .line 772
    add-int v3, v2, v0

    add-int/lit8 v3, v3, -0x1

    div-int v0, v3, v0

    .line 774
    mul-int/lit8 v0, v0, 0x64

    .line 776
    invoke-virtual {p0, v1}, Lcom/jess/ui/TwoWayGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 777
    invoke-virtual {v1}, Landroid/view/View;->getLeft()I

    move-result v3

    .line 778
    invoke-virtual {v1}, Landroid/view/View;->getWidth()I

    move-result v1

    .line 779
    if-lez v1, :cond_0

    .line 780
    mul-int/lit8 v3, v3, 0x64

    div-int v1, v3, v1

    add-int/2addr v0, v1

    .line 783
    :cond_0
    add-int/lit8 v1, v2, -0x1

    invoke-virtual {p0, v1}, Lcom/jess/ui/TwoWayGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 784
    invoke-virtual {v1}, Landroid/view/View;->getRight()I

    move-result v2

    .line 785
    invoke-virtual {v1}, Landroid/view/View;->getWidth()I

    move-result v1

    .line 786
    if-lez v1, :cond_1

    .line 787
    invoke-virtual {p0}, Lcom/jess/ui/TwoWayGridView;->getWidth()I

    move-result v3

    sub-int/2addr v2, v3

    mul-int/lit8 v2, v2, 0x64

    div-int v1, v2, v1

    sub-int/2addr v0, v1

    .line 792
    :cond_1
    :goto_0
    return v0

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method protected computeHorizontalScrollOffset()I
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 797
    iget v1, p0, Lcom/jess/ui/TwoWayGridView;->H:I

    if-ltz v1, :cond_0

    invoke-virtual {p0}, Lcom/jess/ui/TwoWayGridView;->getChildCount()I

    move-result v1

    if-lez v1, :cond_0

    iget-boolean v1, p0, Lcom/jess/ui/TwoWayGridView;->B:Z

    if-nez v1, :cond_0

    .line 798
    invoke-virtual {p0, v0}, Lcom/jess/ui/TwoWayGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 799
    invoke-virtual {v1}, Landroid/view/View;->getLeft()I

    move-result v2

    .line 800
    invoke-virtual {v1}, Landroid/view/View;->getWidth()I

    move-result v1

    .line 801
    if-lez v1, :cond_0

    .line 802
    iget v3, p0, Lcom/jess/ui/TwoWayGridView;->ah:I

    .line 803
    iget v4, p0, Lcom/jess/ui/TwoWayGridView;->H:I

    div-int/2addr v4, v3

    .line 804
    iget v5, p0, Lcom/jess/ui/TwoWayGridView;->aa:I

    add-int/2addr v5, v3

    add-int/lit8 v5, v5, -0x1

    div-int v3, v5, v3

    .line 805
    mul-int/lit8 v4, v4, 0x64

    mul-int/lit8 v2, v2, 0x64

    div-int v1, v2, v1

    sub-int v1, v4, v1

    .line 806
    invoke-virtual {p0}, Lcom/jess/ui/TwoWayGridView;->getScrollX()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {p0}, Lcom/jess/ui/TwoWayGridView;->getWidth()I

    move-result v4

    int-to-float v4, v4

    div-float/2addr v2, v4

    int-to-float v3, v3

    mul-float/2addr v2, v3

    const/high16 v3, 0x42c80000    # 100.0f

    mul-float/2addr v2, v3

    float-to-int v2, v2

    add-int/2addr v1, v2

    .line 805
    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 809
    :cond_0
    return v0
.end method

.method protected computeHorizontalScrollRange()I
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 815
    iget-boolean v1, p0, Lcom/jess/ui/TwoWayGridView;->B:Z

    if-eqz v1, :cond_0

    .line 820
    :goto_0
    return v0

    .line 818
    :cond_0
    iget v1, p0, Lcom/jess/ui/TwoWayGridView;->ah:I

    .line 819
    iget v2, p0, Lcom/jess/ui/TwoWayGridView;->aa:I

    add-int/2addr v2, v1

    add-int/lit8 v2, v2, -0x1

    div-int v1, v2, v1

    .line 820
    mul-int/lit8 v1, v1, 0x64

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    goto :goto_0
.end method

.method protected computeVerticalScrollExtent()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 713
    invoke-virtual {p0}, Lcom/jess/ui/TwoWayGridView;->getChildCount()I

    move-result v2

    .line 714
    if-lez v2, :cond_2

    iget-boolean v0, p0, Lcom/jess/ui/TwoWayGridView;->B:Z

    if-eqz v0, :cond_2

    .line 715
    iget v0, p0, Lcom/jess/ui/TwoWayGridView;->ag:I

    .line 716
    add-int v3, v2, v0

    add-int/lit8 v3, v3, -0x1

    div-int v0, v3, v0

    .line 718
    mul-int/lit8 v0, v0, 0x64

    .line 720
    invoke-virtual {p0, v1}, Lcom/jess/ui/TwoWayGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 721
    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v3

    .line 722
    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v1

    .line 723
    if-lez v1, :cond_0

    .line 724
    mul-int/lit8 v3, v3, 0x64

    div-int v1, v3, v1

    add-int/2addr v0, v1

    .line 727
    :cond_0
    add-int/lit8 v1, v2, -0x1

    invoke-virtual {p0, v1}, Lcom/jess/ui/TwoWayGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 728
    invoke-virtual {v1}, Landroid/view/View;->getBottom()I

    move-result v2

    .line 729
    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v1

    .line 730
    if-lez v1, :cond_1

    .line 731
    invoke-virtual {p0}, Lcom/jess/ui/TwoWayGridView;->getHeight()I

    move-result v3

    sub-int/2addr v2, v3

    mul-int/lit8 v2, v2, 0x64

    div-int v1, v2, v1

    sub-int/2addr v0, v1

    .line 736
    :cond_1
    :goto_0
    return v0

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method protected computeVerticalScrollOffset()I
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 741
    iget v1, p0, Lcom/jess/ui/TwoWayGridView;->H:I

    if-ltz v1, :cond_0

    invoke-virtual {p0}, Lcom/jess/ui/TwoWayGridView;->getChildCount()I

    move-result v1

    if-lez v1, :cond_0

    iget-boolean v1, p0, Lcom/jess/ui/TwoWayGridView;->B:Z

    if-eqz v1, :cond_0

    .line 742
    invoke-virtual {p0, v0}, Lcom/jess/ui/TwoWayGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 743
    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v2

    .line 744
    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v1

    .line 745
    if-lez v1, :cond_0

    .line 746
    iget v3, p0, Lcom/jess/ui/TwoWayGridView;->ag:I

    .line 747
    iget v4, p0, Lcom/jess/ui/TwoWayGridView;->H:I

    div-int/2addr v4, v3

    .line 748
    iget v5, p0, Lcom/jess/ui/TwoWayGridView;->aa:I

    add-int/2addr v5, v3

    add-int/lit8 v5, v5, -0x1

    div-int v3, v5, v3

    .line 749
    mul-int/lit8 v4, v4, 0x64

    mul-int/lit8 v2, v2, 0x64

    div-int v1, v2, v1

    sub-int v1, v4, v1

    .line 750
    invoke-virtual {p0}, Lcom/jess/ui/TwoWayGridView;->getScrollY()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {p0}, Lcom/jess/ui/TwoWayGridView;->getHeight()I

    move-result v4

    int-to-float v4, v4

    div-float/2addr v2, v4

    int-to-float v3, v3

    mul-float/2addr v2, v3

    const/high16 v3, 0x42c80000    # 100.0f

    mul-float/2addr v2, v3

    float-to-int v2, v2

    add-int/2addr v1, v2

    .line 749
    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 753
    :cond_0
    return v0
.end method

.method protected computeVerticalScrollRange()I
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 759
    iget-boolean v1, p0, Lcom/jess/ui/TwoWayGridView;->B:Z

    if-nez v1, :cond_0

    .line 764
    :goto_0
    return v0

    .line 762
    :cond_0
    iget v1, p0, Lcom/jess/ui/TwoWayGridView;->ag:I

    .line 763
    iget v2, p0, Lcom/jess/ui/TwoWayGridView;->aa:I

    add-int/2addr v2, v1

    add-int/lit8 v2, v2, -0x1

    div-int v1, v2, v1

    .line 764
    mul-int/lit8 v1, v1, 0x64

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    goto :goto_0
.end method

.method protected d()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 319
    iget-boolean v1, p0, Lcom/jess/ui/TwoWayGridView;->ae:Z

    .line 320
    if-nez v1, :cond_0

    .line 321
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/jess/ui/TwoWayGridView;->ae:Z

    .line 325
    :cond_0
    :try_start_0
    invoke-super {p0}, Lcom/jess/ui/TwoWayAbsListView;->d()V

    .line 327
    invoke-virtual {p0}, Lcom/jess/ui/TwoWayGridView;->invalidate()V

    .line 329
    iget-object v0, p0, Lcom/jess/ui/TwoWayGridView;->c:Landroid/widget/ListAdapter;

    if-nez v0, :cond_2

    .line 330
    invoke-virtual {p0}, Lcom/jess/ui/TwoWayGridView;->c()V

    .line 331
    invoke-virtual {p0}, Lcom/jess/ui/TwoWayGridView;->a()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 338
    if-nez v1, :cond_1

    .line 339
    iput-boolean v2, p0, Lcom/jess/ui/TwoWayGridView;->ae:Z

    .line 342
    :cond_1
    :goto_0
    return-void

    .line 335
    :cond_2
    :try_start_1
    iget-object v0, p0, Lcom/jess/ui/TwoWayGridView;->af:Lcom/jess/ui/ae;

    invoke-virtual {v0}, Lcom/jess/ui/ae;->a()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 338
    if-nez v1, :cond_1

    .line 339
    iput-boolean v2, p0, Lcom/jess/ui/TwoWayGridView;->ae:Z

    goto :goto_0

    .line 338
    :catchall_0
    move-exception v0

    if-nez v1, :cond_3

    .line 339
    iput-boolean v2, p0, Lcom/jess/ui/TwoWayGridView;->ae:Z

    :cond_3
    throw v0
.end method

.method g(I)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 499
    const/4 v1, -0x1

    .line 501
    const/16 v2, 0x21

    if-ne p1, v2, :cond_2

    .line 502
    iget v1, p0, Lcom/jess/ui/TwoWayGridView;->V:I

    invoke-virtual {p0}, Lcom/jess/ui/TwoWayGridView;->getChildCount()I

    move-result v2

    sub-int/2addr v1, v2

    add-int/lit8 v1, v1, -0x1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 507
    :cond_0
    :goto_0
    if-ltz v1, :cond_1

    .line 508
    invoke-virtual {p0, v1}, Lcom/jess/ui/TwoWayGridView;->setSelectionInt(I)V

    .line 509
    invoke-virtual {p0}, Lcom/jess/ui/TwoWayGridView;->a()V

    .line 511
    const/4 v0, 0x1

    .line 514
    :cond_1
    return v0

    .line 503
    :cond_2
    const/16 v2, 0x82

    if-ne p1, v2, :cond_0

    .line 504
    iget v1, p0, Lcom/jess/ui/TwoWayGridView;->aa:I

    add-int/lit8 v1, v1, -0x1

    iget v2, p0, Lcom/jess/ui/TwoWayGridView;->V:I

    invoke-virtual {p0}, Lcom/jess/ui/TwoWayGridView;->getChildCount()I

    move-result v3

    add-int/2addr v2, v3

    add-int/lit8 v2, v2, -0x1

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    goto :goto_0
.end method

.method public bridge synthetic getAdapter()Landroid/widget/Adapter;
    .locals 1

    .prologue
    .line 59
    invoke-virtual {p0}, Lcom/jess/ui/TwoWayGridView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    return-object v0
.end method

.method public getAdapter()Landroid/widget/ListAdapter;
    .locals 1

    .prologue
    .line 157
    iget-object v0, p0, Lcom/jess/ui/TwoWayGridView;->c:Landroid/widget/ListAdapter;

    return-object v0
.end method

.method public getStretchMode()I
    .locals 1

    .prologue
    .line 650
    iget v0, p0, Lcom/jess/ui/TwoWayGridView;->am:I

    return v0
.end method

.method h(I)Z
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 525
    .line 526
    const/16 v2, 0x21

    if-ne p1, v2, :cond_1

    .line 527
    iput v3, p0, Lcom/jess/ui/TwoWayGridView;->a:I

    .line 528
    invoke-virtual {p0, v1}, Lcom/jess/ui/TwoWayGridView;->setSelectionInt(I)V

    .line 529
    invoke-virtual {p0}, Lcom/jess/ui/TwoWayGridView;->a()V

    .line 538
    :goto_0
    if-eqz v0, :cond_0

    .line 542
    :cond_0
    return v0

    .line 531
    :cond_1
    const/16 v2, 0x82

    if-ne p1, v2, :cond_2

    .line 532
    iput v3, p0, Lcom/jess/ui/TwoWayGridView;->a:I

    .line 533
    iget v1, p0, Lcom/jess/ui/TwoWayGridView;->aa:I

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {p0, v1}, Lcom/jess/ui/TwoWayGridView;->setSelectionInt(I)V

    .line 534
    invoke-virtual {p0}, Lcom/jess/ui/TwoWayGridView;->a()V

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method protected onFocusChanged(ZILandroid/graphics/Rect;)V
    .locals 6

    .prologue
    .line 549
    invoke-super {p0, p1, p2, p3}, Lcom/jess/ui/TwoWayAbsListView;->onFocusChanged(ZILandroid/graphics/Rect;)V

    .line 551
    const/4 v2, -0x1

    .line 552
    if-eqz p1, :cond_1

    if-eqz p3, :cond_1

    .line 553
    invoke-virtual {p0}, Lcom/jess/ui/TwoWayGridView;->getScrollX()I

    move-result v0

    invoke-virtual {p0}, Lcom/jess/ui/TwoWayGridView;->getScrollY()I

    move-result v1

    invoke-virtual {p3, v0, v1}, Landroid/graphics/Rect;->offset(II)V

    .line 557
    iget-object v4, p0, Lcom/jess/ui/TwoWayGridView;->aw:Landroid/graphics/Rect;

    .line 558
    const v0, 0x7fffffff

    .line 559
    invoke-virtual {p0}, Lcom/jess/ui/TwoWayGridView;->getChildCount()I

    move-result v5

    .line 560
    const/4 v3, 0x0

    :goto_0
    if-ge v3, v5, :cond_1

    .line 562
    iget-object v1, p0, Lcom/jess/ui/TwoWayGridView;->af:Lcom/jess/ui/ae;

    invoke-virtual {v1, v3, p2}, Lcom/jess/ui/ae;->b(II)Z

    move-result v1

    if-nez v1, :cond_0

    move v1, v2

    .line 560
    :goto_1
    add-int/lit8 v3, v3, 0x1

    move v2, v1

    goto :goto_0

    .line 566
    :cond_0
    invoke-virtual {p0, v3}, Lcom/jess/ui/TwoWayGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 567
    invoke-virtual {v1, v4}, Landroid/view/View;->getDrawingRect(Landroid/graphics/Rect;)V

    .line 568
    invoke-virtual {p0, v1, v4}, Lcom/jess/ui/TwoWayGridView;->offsetDescendantRectToMyCoords(Landroid/view/View;Landroid/graphics/Rect;)V

    .line 569
    invoke-static {p3, v4, p2}, Lcom/jess/ui/TwoWayGridView;->a(Landroid/graphics/Rect;Landroid/graphics/Rect;I)I

    move-result v1

    .line 571
    if-ge v1, v0, :cond_3

    move v0, v1

    move v1, v3

    .line 573
    goto :goto_1

    .line 578
    :cond_1
    if-ltz v2, :cond_2

    .line 579
    iget v0, p0, Lcom/jess/ui/TwoWayGridView;->H:I

    add-int/2addr v0, v2

    invoke-virtual {p0, v0}, Lcom/jess/ui/TwoWayGridView;->setSelection(I)V

    .line 583
    :goto_2
    return-void

    .line 581
    :cond_2
    invoke-virtual {p0}, Lcom/jess/ui/TwoWayGridView;->requestLayout()V

    goto :goto_2

    :cond_3
    move v1, v2

    goto :goto_1
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1

    .prologue
    .line 377
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0, p2}, Lcom/jess/ui/TwoWayGridView;->a(IILandroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method public onKeyMultiple(IILandroid/view/KeyEvent;)Z
    .locals 1

    .prologue
    .line 382
    invoke-direct {p0, p1, p2, p3}, Lcom/jess/ui/TwoWayGridView;->a(IILandroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 1

    .prologue
    .line 387
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0, p2}, Lcom/jess/ui/TwoWayGridView;->a(IILandroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method protected onMeasure(II)V
    .locals 1

    .prologue
    .line 279
    invoke-super {p0, p1, p2}, Lcom/jess/ui/TwoWayAbsListView;->onMeasure(II)V

    .line 280
    iget-boolean v0, p0, Lcom/jess/ui/TwoWayGridView;->B:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/jess/ui/TwoWayGridView;->af:Lcom/jess/ui/ae;

    instance-of v0, v0, Lcom/jess/ui/ag;

    if-eqz v0, :cond_1

    :cond_0
    iget-boolean v0, p0, Lcom/jess/ui/TwoWayGridView;->B:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/jess/ui/TwoWayGridView;->af:Lcom/jess/ui/ae;

    instance-of v0, v0, Lcom/jess/ui/af;

    if-nez v0, :cond_2

    .line 282
    :cond_1
    invoke-direct {p0}, Lcom/jess/ui/TwoWayGridView;->s()V

    .line 285
    :cond_2
    iget-object v0, p0, Lcom/jess/ui/TwoWayGridView;->af:Lcom/jess/ui/ae;

    invoke-virtual {v0, p1, p2}, Lcom/jess/ui/ae;->a(II)V

    .line 286
    return-void
.end method

.method public bridge synthetic setAdapter(Landroid/widget/Adapter;)V
    .locals 0

    .prologue
    .line 59
    check-cast p1, Landroid/widget/ListAdapter;

    invoke-virtual {p0, p1}, Lcom/jess/ui/TwoWayGridView;->setAdapter(Landroid/widget/ListAdapter;)V

    return-void
.end method

.method public setAdapter(Landroid/widget/ListAdapter;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 167
    iget-object v0, p0, Lcom/jess/ui/TwoWayGridView;->c:Landroid/widget/ListAdapter;

    if-eqz v0, :cond_0

    .line 168
    iget-object v0, p0, Lcom/jess/ui/TwoWayGridView;->c:Landroid/widget/ListAdapter;

    iget-object v1, p0, Lcom/jess/ui/TwoWayGridView;->b:Lcom/jess/ui/y;

    invoke-interface {v0, v1}, Landroid/widget/ListAdapter;->unregisterDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 171
    :cond_0
    invoke-virtual {p0}, Lcom/jess/ui/TwoWayGridView;->c()V

    .line 172
    iget-object v0, p0, Lcom/jess/ui/TwoWayGridView;->g:Lcom/jess/ui/k;

    invoke-virtual {v0}, Lcom/jess/ui/k;->b()V

    .line 173
    iput-object p1, p0, Lcom/jess/ui/TwoWayGridView;->c:Landroid/widget/ListAdapter;

    .line 175
    const/4 v0, -0x1

    iput v0, p0, Lcom/jess/ui/TwoWayGridView;->ac:I

    .line 176
    const-wide/high16 v0, -0x8000000000000000L

    iput-wide v0, p0, Lcom/jess/ui/TwoWayGridView;->ad:J

    .line 178
    iget-object v0, p0, Lcom/jess/ui/TwoWayGridView;->c:Landroid/widget/ListAdapter;

    if-eqz v0, :cond_2

    .line 179
    iget v0, p0, Lcom/jess/ui/TwoWayGridView;->aa:I

    iput v0, p0, Lcom/jess/ui/TwoWayGridView;->ab:I

    .line 180
    iget-object v0, p0, Lcom/jess/ui/TwoWayGridView;->c:Landroid/widget/ListAdapter;

    invoke-interface {v0}, Landroid/widget/ListAdapter;->getCount()I

    move-result v0

    iput v0, p0, Lcom/jess/ui/TwoWayGridView;->aa:I

    .line 181
    iput-boolean v3, p0, Lcom/jess/ui/TwoWayGridView;->S:Z

    .line 182
    invoke-virtual {p0}, Lcom/jess/ui/TwoWayGridView;->n()V

    .line 184
    new-instance v0, Lcom/jess/ui/y;

    invoke-direct {v0, p0}, Lcom/jess/ui/y;-><init>(Lcom/jess/ui/v;)V

    iput-object v0, p0, Lcom/jess/ui/TwoWayGridView;->b:Lcom/jess/ui/y;

    .line 185
    iget-object v0, p0, Lcom/jess/ui/TwoWayGridView;->c:Landroid/widget/ListAdapter;

    iget-object v1, p0, Lcom/jess/ui/TwoWayGridView;->b:Lcom/jess/ui/y;

    invoke-interface {v0, v1}, Landroid/widget/ListAdapter;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 187
    iget-object v0, p0, Lcom/jess/ui/TwoWayGridView;->g:Lcom/jess/ui/k;

    iget-object v1, p0, Lcom/jess/ui/TwoWayGridView;->c:Landroid/widget/ListAdapter;

    invoke-interface {v1}, Landroid/widget/ListAdapter;->getViewTypeCount()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/jess/ui/k;->a(I)V

    .line 190
    iget-boolean v0, p0, Lcom/jess/ui/TwoWayGridView;->y:Z

    if-eqz v0, :cond_1

    .line 191
    iget v0, p0, Lcom/jess/ui/TwoWayGridView;->aa:I

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p0, v0, v2}, Lcom/jess/ui/TwoWayGridView;->a(IZ)I

    move-result v0

    .line 195
    :goto_0
    invoke-virtual {p0, v0}, Lcom/jess/ui/TwoWayGridView;->setSelectedPositionInt(I)V

    .line 196
    invoke-virtual {p0, v0}, Lcom/jess/ui/TwoWayGridView;->setNextSelectedPositionInt(I)V

    .line 197
    invoke-virtual {p0}, Lcom/jess/ui/TwoWayGridView;->p()V

    .line 204
    :goto_1
    invoke-virtual {p0}, Lcom/jess/ui/TwoWayGridView;->requestLayout()V

    .line 205
    return-void

    .line 193
    :cond_1
    invoke-virtual {p0, v2, v3}, Lcom/jess/ui/TwoWayGridView;->a(IZ)I

    move-result v0

    goto :goto_0

    .line 199
    :cond_2
    invoke-virtual {p0}, Lcom/jess/ui/TwoWayGridView;->n()V

    .line 201
    invoke-virtual {p0}, Lcom/jess/ui/TwoWayGridView;->p()V

    goto :goto_1
.end method

.method public setColumnWidth(I)V
    .locals 1

    .prologue
    .line 661
    iget v0, p0, Lcom/jess/ui/TwoWayGridView;->ao:I

    if-eq p1, v0, :cond_0

    .line 662
    iput p1, p0, Lcom/jess/ui/TwoWayGridView;->ao:I

    .line 663
    invoke-virtual {p0}, Lcom/jess/ui/TwoWayGridView;->b()V

    .line 665
    :cond_0
    return-void
.end method

.method public setGravity(I)V
    .locals 1

    .prologue
    .line 595
    iget v0, p0, Lcom/jess/ui/TwoWayGridView;->av:I

    if-eq v0, p1, :cond_0

    .line 596
    iput p1, p0, Lcom/jess/ui/TwoWayGridView;->av:I

    .line 597
    invoke-virtual {p0}, Lcom/jess/ui/TwoWayGridView;->b()V

    .line 599
    :cond_0
    return-void
.end method

.method public setHorizontalSpacing(I)V
    .locals 1

    .prologue
    .line 611
    iget v0, p0, Lcom/jess/ui/TwoWayGridView;->aj:I

    if-eq p1, v0, :cond_0

    .line 612
    iput p1, p0, Lcom/jess/ui/TwoWayGridView;->aj:I

    .line 613
    invoke-virtual {p0}, Lcom/jess/ui/TwoWayGridView;->b()V

    .line 615
    :cond_0
    return-void
.end method

.method public setNumColumns(I)V
    .locals 1

    .prologue
    .line 689
    iget v0, p0, Lcom/jess/ui/TwoWayGridView;->ap:I

    if-eq p1, v0, :cond_0

    .line 690
    iput p1, p0, Lcom/jess/ui/TwoWayGridView;->ap:I

    .line 691
    invoke-virtual {p0}, Lcom/jess/ui/TwoWayGridView;->b()V

    .line 693
    :cond_0
    return-void
.end method

.method public setNumRows(I)V
    .locals 1

    .prologue
    .line 703
    iget v0, p0, Lcom/jess/ui/TwoWayGridView;->as:I

    if-eq p1, v0, :cond_0

    .line 704
    iput p1, p0, Lcom/jess/ui/TwoWayGridView;->as:I

    .line 705
    invoke-virtual {p0}, Lcom/jess/ui/TwoWayGridView;->b()V

    .line 707
    :cond_0
    return-void
.end method

.method public setRowHeight(I)V
    .locals 1

    .prologue
    .line 675
    iget v0, p0, Lcom/jess/ui/TwoWayGridView;->ar:I

    if-eq p1, v0, :cond_0

    .line 676
    iput p1, p0, Lcom/jess/ui/TwoWayGridView;->ar:I

    .line 677
    invoke-virtual {p0}, Lcom/jess/ui/TwoWayGridView;->b()V

    .line 679
    :cond_0
    return-void
.end method

.method public setSelection(I)V
    .locals 1

    .prologue
    .line 356
    invoke-virtual {p0}, Lcom/jess/ui/TwoWayGridView;->isInTouchMode()Z

    move-result v0

    if-nez v0, :cond_0

    .line 357
    invoke-virtual {p0, p1}, Lcom/jess/ui/TwoWayGridView;->setNextSelectedPositionInt(I)V

    .line 361
    :goto_0
    const/4 v0, 0x2

    iput v0, p0, Lcom/jess/ui/TwoWayGridView;->a:I

    .line 362
    invoke-virtual {p0}, Lcom/jess/ui/TwoWayGridView;->requestLayout()V

    .line 363
    return-void

    .line 359
    :cond_0
    iput p1, p0, Lcom/jess/ui/TwoWayGridView;->A:I

    goto :goto_0
.end method

.method setSelectionInt(I)V
    .locals 1

    .prologue
    .line 372
    iget-object v0, p0, Lcom/jess/ui/TwoWayGridView;->af:Lcom/jess/ui/ae;

    invoke-virtual {v0, p1}, Lcom/jess/ui/ae;->a(I)V

    .line 373
    return-void
.end method

.method public setStretchMode(I)V
    .locals 1

    .prologue
    .line 643
    iget v0, p0, Lcom/jess/ui/TwoWayGridView;->am:I

    if-eq p1, v0, :cond_0

    .line 644
    iput p1, p0, Lcom/jess/ui/TwoWayGridView;->am:I

    .line 645
    invoke-virtual {p0}, Lcom/jess/ui/TwoWayGridView;->b()V

    .line 647
    :cond_0
    return-void
.end method

.method public setVerticalSpacing(I)V
    .locals 1

    .prologue
    .line 628
    iget v0, p0, Lcom/jess/ui/TwoWayGridView;->al:I

    if-eq p1, v0, :cond_0

    .line 629
    iput p1, p0, Lcom/jess/ui/TwoWayGridView;->al:I

    .line 630
    invoke-virtual {p0}, Lcom/jess/ui/TwoWayGridView;->b()V

    .line 632
    :cond_0
    return-void
.end method
