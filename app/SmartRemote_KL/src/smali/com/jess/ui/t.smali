.class Lcom/jess/ui/t;
.super Lcom/jess/ui/p;


# instance fields
.field protected a:I

.field final synthetic b:Lcom/jess/ui/r;


# direct methods
.method private constructor <init>(Lcom/jess/ui/r;)V
    .locals 0

    .prologue
    .line 4449
    iput-object p1, p0, Lcom/jess/ui/t;->b:Lcom/jess/ui/r;

    invoke-direct {p0, p1}, Lcom/jess/ui/p;-><init>(Lcom/jess/ui/n;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/jess/ui/r;Lcom/jess/ui/a;)V
    .locals 0

    .prologue
    .line 4449
    invoke-direct {p0, p1}, Lcom/jess/ui/t;-><init>(Lcom/jess/ui/r;)V

    return-void
.end method


# virtual methods
.method a(I)V
    .locals 9

    .prologue
    const v6, 0x7fffffff

    const/4 v1, 0x0

    .line 4457
    if-gez p1, :cond_0

    move v2, v6

    .line 4458
    :goto_0
    iput v2, p0, Lcom/jess/ui/t;->a:I

    .line 4459
    iget-object v0, p0, Lcom/jess/ui/t;->c:Landroid/widget/Scroller;

    move v3, v1

    move v4, p1

    move v5, v1

    move v7, v1

    move v8, v6

    invoke-virtual/range {v0 .. v8}, Landroid/widget/Scroller;->fling(IIIIIIII)V

    .line 4461
    iget-object v0, p0, Lcom/jess/ui/t;->b:Lcom/jess/ui/r;

    iget-object v0, v0, Lcom/jess/ui/r;->d:Lcom/jess/ui/TwoWayAbsListView;

    const/4 v1, 0x4

    iput v1, v0, Lcom/jess/ui/TwoWayAbsListView;->w:I

    .line 4462
    iget-object v0, p0, Lcom/jess/ui/t;->b:Lcom/jess/ui/r;

    iget-object v0, v0, Lcom/jess/ui/r;->d:Lcom/jess/ui/TwoWayAbsListView;

    invoke-virtual {v0, p0}, Lcom/jess/ui/TwoWayAbsListView;->post(Ljava/lang/Runnable;)Z

    .line 4470
    return-void

    :cond_0
    move v2, v1

    .line 4457
    goto :goto_0
.end method

.method public run()V
    .locals 6

    .prologue
    .line 4483
    iget-object v0, p0, Lcom/jess/ui/t;->b:Lcom/jess/ui/r;

    iget-object v0, v0, Lcom/jess/ui/r;->d:Lcom/jess/ui/TwoWayAbsListView;

    iget v0, v0, Lcom/jess/ui/TwoWayAbsListView;->w:I

    packed-switch v0, :pswitch_data_0

    .line 4542
    :goto_0
    return-void

    .line 4488
    :pswitch_0
    iget-object v0, p0, Lcom/jess/ui/t;->b:Lcom/jess/ui/r;

    iget-object v0, v0, Lcom/jess/ui/r;->d:Lcom/jess/ui/TwoWayAbsListView;

    iget v0, v0, Lcom/jess/ui/TwoWayAbsListView;->aa:I

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/jess/ui/t;->b:Lcom/jess/ui/r;

    iget-object v0, v0, Lcom/jess/ui/r;->d:Lcom/jess/ui/TwoWayAbsListView;

    invoke-virtual {v0}, Lcom/jess/ui/TwoWayAbsListView;->getChildCount()I

    move-result v0

    if-nez v0, :cond_1

    .line 4489
    :cond_0
    invoke-virtual {p0}, Lcom/jess/ui/t;->a()V

    goto :goto_0

    .line 4493
    :cond_1
    iget-object v0, p0, Lcom/jess/ui/t;->c:Landroid/widget/Scroller;

    .line 4494
    invoke-virtual {v0}, Landroid/widget/Scroller;->computeScrollOffset()Z

    move-result v1

    .line 4495
    invoke-virtual {v0}, Landroid/widget/Scroller;->getCurrY()I

    move-result v2

    .line 4499
    iget v0, p0, Lcom/jess/ui/t;->a:I

    sub-int/2addr v0, v2

    .line 4502
    if-lez v0, :cond_2

    .line 4504
    iget-object v3, p0, Lcom/jess/ui/t;->b:Lcom/jess/ui/r;

    iget-object v3, v3, Lcom/jess/ui/r;->d:Lcom/jess/ui/TwoWayAbsListView;

    iget-object v4, p0, Lcom/jess/ui/t;->b:Lcom/jess/ui/r;

    iget-object v4, v4, Lcom/jess/ui/r;->d:Lcom/jess/ui/TwoWayAbsListView;

    iget v4, v4, Lcom/jess/ui/TwoWayAbsListView;->H:I

    iput v4, v3, Lcom/jess/ui/TwoWayAbsListView;->t:I

    .line 4505
    iget-object v3, p0, Lcom/jess/ui/t;->b:Lcom/jess/ui/r;

    iget-object v3, v3, Lcom/jess/ui/r;->d:Lcom/jess/ui/TwoWayAbsListView;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Lcom/jess/ui/TwoWayAbsListView;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 4506
    iget-object v4, p0, Lcom/jess/ui/t;->b:Lcom/jess/ui/r;

    invoke-virtual {v3}, Landroid/view/View;->getTop()I

    move-result v3

    iput v3, v4, Lcom/jess/ui/r;->a:I

    .line 4509
    iget-object v3, p0, Lcom/jess/ui/t;->b:Lcom/jess/ui/r;

    iget-object v3, v3, Lcom/jess/ui/r;->d:Lcom/jess/ui/TwoWayAbsListView;

    invoke-virtual {v3}, Lcom/jess/ui/TwoWayAbsListView;->getHeight()I

    move-result v3

    iget-object v4, p0, Lcom/jess/ui/t;->b:Lcom/jess/ui/r;

    iget-object v4, v4, Lcom/jess/ui/r;->d:Lcom/jess/ui/TwoWayAbsListView;

    invoke-virtual {v4}, Lcom/jess/ui/TwoWayAbsListView;->getPaddingBottom()I

    move-result v4

    sub-int/2addr v3, v4

    iget-object v4, p0, Lcom/jess/ui/t;->b:Lcom/jess/ui/r;

    iget-object v4, v4, Lcom/jess/ui/r;->d:Lcom/jess/ui/TwoWayAbsListView;

    invoke-virtual {v4}, Lcom/jess/ui/TwoWayAbsListView;->getPaddingTop()I

    move-result v4

    sub-int/2addr v3, v4

    add-int/lit8 v3, v3, -0x1

    invoke-static {v3, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 4522
    :goto_1
    iget-object v3, p0, Lcom/jess/ui/t;->b:Lcom/jess/ui/r;

    invoke-virtual {v3, v0, v0}, Lcom/jess/ui/r;->a(II)Z

    move-result v0

    .line 4524
    if-eqz v1, :cond_3

    if-nez v0, :cond_3

    .line 4525
    iget-object v0, p0, Lcom/jess/ui/t;->b:Lcom/jess/ui/r;

    iget-object v0, v0, Lcom/jess/ui/r;->d:Lcom/jess/ui/TwoWayAbsListView;

    invoke-virtual {v0}, Lcom/jess/ui/TwoWayAbsListView;->invalidate()V

    .line 4526
    iput v2, p0, Lcom/jess/ui/t;->a:I

    .line 4527
    iget-object v0, p0, Lcom/jess/ui/t;->b:Lcom/jess/ui/r;

    iget-object v0, v0, Lcom/jess/ui/r;->d:Lcom/jess/ui/TwoWayAbsListView;

    invoke-virtual {v0, p0}, Lcom/jess/ui/TwoWayAbsListView;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    .line 4512
    :cond_2
    iget-object v3, p0, Lcom/jess/ui/t;->b:Lcom/jess/ui/r;

    iget-object v3, v3, Lcom/jess/ui/r;->d:Lcom/jess/ui/TwoWayAbsListView;

    invoke-virtual {v3}, Lcom/jess/ui/TwoWayAbsListView;->getChildCount()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    .line 4513
    iget-object v4, p0, Lcom/jess/ui/t;->b:Lcom/jess/ui/r;

    iget-object v4, v4, Lcom/jess/ui/r;->d:Lcom/jess/ui/TwoWayAbsListView;

    iget-object v5, p0, Lcom/jess/ui/t;->b:Lcom/jess/ui/r;

    iget-object v5, v5, Lcom/jess/ui/r;->d:Lcom/jess/ui/TwoWayAbsListView;

    iget v5, v5, Lcom/jess/ui/TwoWayAbsListView;->H:I

    add-int/2addr v5, v3

    iput v5, v4, Lcom/jess/ui/TwoWayAbsListView;->t:I

    .line 4515
    iget-object v4, p0, Lcom/jess/ui/t;->b:Lcom/jess/ui/r;

    iget-object v4, v4, Lcom/jess/ui/r;->d:Lcom/jess/ui/TwoWayAbsListView;

    invoke-virtual {v4, v3}, Lcom/jess/ui/TwoWayAbsListView;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 4516
    iget-object v4, p0, Lcom/jess/ui/t;->b:Lcom/jess/ui/r;

    invoke-virtual {v3}, Landroid/view/View;->getTop()I

    move-result v3

    iput v3, v4, Lcom/jess/ui/r;->a:I

    .line 4519
    iget-object v3, p0, Lcom/jess/ui/t;->b:Lcom/jess/ui/r;

    iget-object v3, v3, Lcom/jess/ui/r;->d:Lcom/jess/ui/TwoWayAbsListView;

    invoke-virtual {v3}, Lcom/jess/ui/TwoWayAbsListView;->getHeight()I

    move-result v3

    iget-object v4, p0, Lcom/jess/ui/t;->b:Lcom/jess/ui/r;

    iget-object v4, v4, Lcom/jess/ui/r;->d:Lcom/jess/ui/TwoWayAbsListView;

    invoke-virtual {v4}, Lcom/jess/ui/TwoWayAbsListView;->getPaddingBottom()I

    move-result v4

    sub-int/2addr v3, v4

    iget-object v4, p0, Lcom/jess/ui/t;->b:Lcom/jess/ui/r;

    iget-object v4, v4, Lcom/jess/ui/r;->d:Lcom/jess/ui/TwoWayAbsListView;

    invoke-virtual {v4}, Lcom/jess/ui/TwoWayAbsListView;->getPaddingTop()I

    move-result v4

    sub-int/2addr v3, v4

    add-int/lit8 v3, v3, -0x1

    neg-int v3, v3

    invoke-static {v3, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    goto :goto_1

    .line 4529
    :cond_3
    invoke-virtual {p0}, Lcom/jess/ui/t;->a()V

    goto/16 :goto_0

    .line 4483
    nop

    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_0
    .end packed-switch
.end method
