.class abstract Lcom/jess/ui/n;
.super Ljava/lang/Object;


# instance fields
.field protected e:Lcom/jess/ui/q;

.field protected f:Lcom/jess/ui/p;

.field g:I

.field final synthetic h:Lcom/jess/ui/TwoWayAbsListView;


# direct methods
.method constructor <init>(Lcom/jess/ui/TwoWayAbsListView;)V
    .locals 0

    .prologue
    .line 3439
    iput-object p1, p0, Lcom/jess/ui/n;->h:Lcom/jess/ui/TwoWayAbsListView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3701
    return-void
.end method


# virtual methods
.method public a(Z)V
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 3457
    iget-object v0, p0, Lcom/jess/ui/n;->h:Lcom/jess/ui/TwoWayAbsListView;

    invoke-virtual {v0}, Lcom/jess/ui/TwoWayAbsListView;->isInTouchMode()Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    .line 3459
    :goto_0
    if-nez p1, :cond_3

    .line 3460
    iget-object v3, p0, Lcom/jess/ui/n;->h:Lcom/jess/ui/TwoWayAbsListView;

    invoke-static {v3, v1}, Lcom/jess/ui/TwoWayAbsListView;->a(Lcom/jess/ui/TwoWayAbsListView;Z)V

    .line 3461
    iget-object v3, p0, Lcom/jess/ui/n;->f:Lcom/jess/ui/p;

    if-eqz v3, :cond_0

    .line 3462
    iget-object v3, p0, Lcom/jess/ui/n;->h:Lcom/jess/ui/TwoWayAbsListView;

    iget-object v4, p0, Lcom/jess/ui/n;->f:Lcom/jess/ui/p;

    invoke-virtual {v3, v4}, Lcom/jess/ui/TwoWayAbsListView;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 3465
    iget-object v3, p0, Lcom/jess/ui/n;->f:Lcom/jess/ui/p;

    invoke-virtual {v3}, Lcom/jess/ui/p;->a()V

    .line 3467
    iget-object v3, p0, Lcom/jess/ui/n;->h:Lcom/jess/ui/TwoWayAbsListView;

    invoke-virtual {v3}, Lcom/jess/ui/TwoWayAbsListView;->getScrollY()I

    move-result v3

    if-eqz v3, :cond_0

    .line 3468
    iget-object v3, p0, Lcom/jess/ui/n;->h:Lcom/jess/ui/TwoWayAbsListView;

    iget-object v4, p0, Lcom/jess/ui/n;->h:Lcom/jess/ui/TwoWayAbsListView;

    invoke-virtual {v4}, Lcom/jess/ui/TwoWayAbsListView;->getScrollX()I

    move-result v4

    invoke-virtual {v3, v4, v1}, Lcom/jess/ui/TwoWayAbsListView;->scrollTo(II)V

    .line 3470
    iget-object v1, p0, Lcom/jess/ui/n;->h:Lcom/jess/ui/TwoWayAbsListView;

    invoke-virtual {v1}, Lcom/jess/ui/TwoWayAbsListView;->invalidate()V

    .line 3476
    :cond_0
    if-ne v0, v2, :cond_1

    .line 3478
    iget-object v1, p0, Lcom/jess/ui/n;->h:Lcom/jess/ui/TwoWayAbsListView;

    iget-object v2, p0, Lcom/jess/ui/n;->h:Lcom/jess/ui/TwoWayAbsListView;

    iget v2, v2, Lcom/jess/ui/TwoWayAbsListView;->V:I

    iput v2, v1, Lcom/jess/ui/TwoWayAbsListView;->A:I

    .line 3502
    :cond_1
    :goto_1
    iget-object v1, p0, Lcom/jess/ui/n;->h:Lcom/jess/ui/TwoWayAbsListView;

    invoke-static {v1, v0}, Lcom/jess/ui/TwoWayAbsListView;->a(Lcom/jess/ui/TwoWayAbsListView;I)I

    .line 3503
    return-void

    :cond_2
    move v0, v2

    .line 3457
    goto :goto_0

    .line 3487
    :cond_3
    iget-object v3, p0, Lcom/jess/ui/n;->h:Lcom/jess/ui/TwoWayAbsListView;

    invoke-static {v3}, Lcom/jess/ui/TwoWayAbsListView;->d(Lcom/jess/ui/TwoWayAbsListView;)I

    move-result v3

    if-eq v0, v3, :cond_1

    iget-object v3, p0, Lcom/jess/ui/n;->h:Lcom/jess/ui/TwoWayAbsListView;

    invoke-static {v3}, Lcom/jess/ui/TwoWayAbsListView;->d(Lcom/jess/ui/TwoWayAbsListView;)I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_1

    .line 3489
    if-ne v0, v2, :cond_4

    .line 3491
    invoke-virtual {p0}, Lcom/jess/ui/n;->a()Z

    goto :goto_1

    .line 3495
    :cond_4
    iget-object v2, p0, Lcom/jess/ui/n;->h:Lcom/jess/ui/TwoWayAbsListView;

    invoke-virtual {v2}, Lcom/jess/ui/TwoWayAbsListView;->i()V

    .line 3496
    iget-object v2, p0, Lcom/jess/ui/n;->h:Lcom/jess/ui/TwoWayAbsListView;

    iput v1, v2, Lcom/jess/ui/TwoWayAbsListView;->a:I

    .line 3497
    iget-object v1, p0, Lcom/jess/ui/n;->h:Lcom/jess/ui/TwoWayAbsListView;

    invoke-virtual {v1}, Lcom/jess/ui/TwoWayAbsListView;->d()V

    goto :goto_1
.end method

.method abstract a()Z
.end method

.method public a(I)Z
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 3509
    invoke-static {p1}, Ljava/lang/Math;->abs(I)I

    move-result v2

    .line 3510
    iget-object v3, p0, Lcom/jess/ui/n;->h:Lcom/jess/ui/TwoWayAbsListView;

    invoke-static {v3}, Lcom/jess/ui/TwoWayAbsListView;->e(Lcom/jess/ui/TwoWayAbsListView;)I

    move-result v3

    if-le v2, v3, :cond_2

    .line 3511
    invoke-virtual {p0}, Lcom/jess/ui/n;->b()V

    .line 3512
    iget-object v2, p0, Lcom/jess/ui/n;->h:Lcom/jess/ui/TwoWayAbsListView;

    const/4 v3, 0x3

    iput v3, v2, Lcom/jess/ui/TwoWayAbsListView;->w:I

    .line 3513
    iput p1, p0, Lcom/jess/ui/n;->g:I

    .line 3514
    iget-object v2, p0, Lcom/jess/ui/n;->h:Lcom/jess/ui/TwoWayAbsListView;

    invoke-virtual {v2}, Lcom/jess/ui/TwoWayAbsListView;->getHandler()Landroid/os/Handler;

    move-result-object v2

    .line 3518
    if-eqz v2, :cond_0

    .line 3519
    iget-object v3, p0, Lcom/jess/ui/n;->h:Lcom/jess/ui/TwoWayAbsListView;

    invoke-static {v3}, Lcom/jess/ui/TwoWayAbsListView;->c(Lcom/jess/ui/TwoWayAbsListView;)Lcom/jess/ui/c;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 3521
    :cond_0
    iget-object v2, p0, Lcom/jess/ui/n;->h:Lcom/jess/ui/TwoWayAbsListView;

    invoke-virtual {v2, v1}, Lcom/jess/ui/TwoWayAbsListView;->setPressed(Z)V

    .line 3522
    iget-object v2, p0, Lcom/jess/ui/n;->h:Lcom/jess/ui/TwoWayAbsListView;

    iget-object v3, p0, Lcom/jess/ui/n;->h:Lcom/jess/ui/TwoWayAbsListView;

    iget v3, v3, Lcom/jess/ui/TwoWayAbsListView;->t:I

    iget-object v4, p0, Lcom/jess/ui/n;->h:Lcom/jess/ui/TwoWayAbsListView;

    iget v4, v4, Lcom/jess/ui/TwoWayAbsListView;->H:I

    sub-int/2addr v3, v4

    invoke-virtual {v2, v3}, Lcom/jess/ui/TwoWayAbsListView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 3523
    if-eqz v2, :cond_1

    .line 3524
    invoke-virtual {v2, v1}, Landroid/view/View;->setPressed(Z)V

    .line 3526
    :cond_1
    invoke-virtual {p0, v0}, Lcom/jess/ui/n;->b(I)V

    .line 3529
    iget-object v1, p0, Lcom/jess/ui/n;->h:Lcom/jess/ui/TwoWayAbsListView;

    invoke-virtual {v1, v0}, Lcom/jess/ui/TwoWayAbsListView;->requestDisallowInterceptTouchEvent(Z)V

    .line 3533
    :goto_0
    return v0

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method abstract a(II)Z
.end method

.method public abstract a(Landroid/view/MotionEvent;)Z
.end method

.method protected b()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 3611
    iget-object v0, p0, Lcom/jess/ui/n;->h:Lcom/jess/ui/TwoWayAbsListView;

    iget-boolean v0, v0, Lcom/jess/ui/TwoWayAbsListView;->z:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/jess/ui/n;->h:Lcom/jess/ui/TwoWayAbsListView;

    iget-boolean v0, v0, Lcom/jess/ui/TwoWayAbsListView;->s:Z

    if-nez v0, :cond_0

    .line 3612
    iget-object v0, p0, Lcom/jess/ui/n;->h:Lcom/jess/ui/TwoWayAbsListView;

    invoke-static {v0, v1}, Lcom/jess/ui/TwoWayAbsListView;->b(Lcom/jess/ui/TwoWayAbsListView;Z)V

    .line 3613
    iget-object v0, p0, Lcom/jess/ui/n;->h:Lcom/jess/ui/TwoWayAbsListView;

    invoke-static {v0, v1}, Lcom/jess/ui/TwoWayAbsListView;->c(Lcom/jess/ui/TwoWayAbsListView;Z)V

    .line 3614
    iget-object v0, p0, Lcom/jess/ui/n;->h:Lcom/jess/ui/TwoWayAbsListView;

    iput-boolean v1, v0, Lcom/jess/ui/TwoWayAbsListView;->s:Z

    .line 3616
    :cond_0
    return-void
.end method

.method b(I)V
    .locals 2

    .prologue
    .line 3560
    iget-object v0, p0, Lcom/jess/ui/n;->h:Lcom/jess/ui/TwoWayAbsListView;

    invoke-static {v0}, Lcom/jess/ui/TwoWayAbsListView;->f(Lcom/jess/ui/TwoWayAbsListView;)I

    move-result v0

    if-eq p1, v0, :cond_0

    .line 3561
    iget-object v0, p0, Lcom/jess/ui/n;->h:Lcom/jess/ui/TwoWayAbsListView;

    invoke-static {v0}, Lcom/jess/ui/TwoWayAbsListView;->g(Lcom/jess/ui/TwoWayAbsListView;)Lcom/jess/ui/i;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 3562
    iget-object v0, p0, Lcom/jess/ui/n;->h:Lcom/jess/ui/TwoWayAbsListView;

    invoke-static {v0}, Lcom/jess/ui/TwoWayAbsListView;->g(Lcom/jess/ui/TwoWayAbsListView;)Lcom/jess/ui/i;

    move-result-object v0

    iget-object v1, p0, Lcom/jess/ui/n;->h:Lcom/jess/ui/TwoWayAbsListView;

    invoke-interface {v0, v1, p1}, Lcom/jess/ui/i;->a(Lcom/jess/ui/TwoWayAbsListView;I)V

    .line 3563
    iget-object v0, p0, Lcom/jess/ui/n;->h:Lcom/jess/ui/TwoWayAbsListView;

    invoke-static {v0, p1}, Lcom/jess/ui/TwoWayAbsListView;->b(Lcom/jess/ui/TwoWayAbsListView;I)I

    .line 3566
    :cond_0
    return-void
.end method

.method public b(Z)V
    .locals 1

    .prologue
    .line 3538
    if-eqz p1, :cond_0

    .line 3540
    iget-object v0, p0, Lcom/jess/ui/n;->h:Lcom/jess/ui/TwoWayAbsListView;

    invoke-virtual {v0}, Lcom/jess/ui/TwoWayAbsListView;->i()V

    .line 3544
    iget-object v0, p0, Lcom/jess/ui/n;->h:Lcom/jess/ui/TwoWayAbsListView;

    invoke-virtual {v0}, Lcom/jess/ui/TwoWayAbsListView;->getHeight()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/jess/ui/n;->h:Lcom/jess/ui/TwoWayAbsListView;

    invoke-virtual {v0}, Lcom/jess/ui/TwoWayAbsListView;->getChildCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 3547
    iget-object v0, p0, Lcom/jess/ui/n;->h:Lcom/jess/ui/TwoWayAbsListView;

    invoke-virtual {v0}, Lcom/jess/ui/TwoWayAbsListView;->d()V

    .line 3550
    :cond_0
    return-void
.end method

.method public abstract b(Landroid/view/MotionEvent;)Z
.end method

.method protected c()V
    .locals 2

    .prologue
    .line 3619
    iget-object v0, p0, Lcom/jess/ui/n;->h:Lcom/jess/ui/TwoWayAbsListView;

    invoke-static {v0}, Lcom/jess/ui/TwoWayAbsListView;->h(Lcom/jess/ui/TwoWayAbsListView;)Ljava/lang/Runnable;

    move-result-object v0

    if-nez v0, :cond_0

    .line 3620
    iget-object v0, p0, Lcom/jess/ui/n;->h:Lcom/jess/ui/TwoWayAbsListView;

    new-instance v1, Lcom/jess/ui/o;

    invoke-direct {v1, p0}, Lcom/jess/ui/o;-><init>(Lcom/jess/ui/n;)V

    invoke-static {v0, v1}, Lcom/jess/ui/TwoWayAbsListView;->a(Lcom/jess/ui/TwoWayAbsListView;Ljava/lang/Runnable;)Ljava/lang/Runnable;

    .line 3635
    :cond_0
    iget-object v0, p0, Lcom/jess/ui/n;->h:Lcom/jess/ui/TwoWayAbsListView;

    iget-object v1, p0, Lcom/jess/ui/n;->h:Lcom/jess/ui/TwoWayAbsListView;

    invoke-static {v1}, Lcom/jess/ui/TwoWayAbsListView;->h(Lcom/jess/ui/TwoWayAbsListView;)Ljava/lang/Runnable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/jess/ui/TwoWayAbsListView;->post(Ljava/lang/Runnable;)Z

    .line 3636
    return-void
.end method
