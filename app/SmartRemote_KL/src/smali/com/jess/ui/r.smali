.class Lcom/jess/ui/r;
.super Lcom/jess/ui/n;


# instance fields
.field a:I

.field b:I

.field c:I

.field final synthetic d:Lcom/jess/ui/TwoWayAbsListView;


# direct methods
.method constructor <init>(Lcom/jess/ui/TwoWayAbsListView;)V
    .locals 0

    .prologue
    .line 3827
    iput-object p1, p0, Lcom/jess/ui/r;->d:Lcom/jess/ui/TwoWayAbsListView;

    invoke-direct {p0, p1}, Lcom/jess/ui/n;-><init>(Lcom/jess/ui/TwoWayAbsListView;)V

    .line 4546
    return-void
.end method


# virtual methods
.method a()Z
    .locals 14

    .prologue
    const/4 v3, 0x1

    const/4 v6, -0x1

    const/4 v4, 0x0

    .line 4333
    iget-object v0, p0, Lcom/jess/ui/r;->d:Lcom/jess/ui/TwoWayAbsListView;

    invoke-virtual {v0}, Lcom/jess/ui/TwoWayAbsListView;->getChildCount()I

    move-result v9

    .line 4335
    if-gtz v9, :cond_0

    .line 4427
    :goto_0
    return v4

    .line 4341
    :cond_0
    iget-object v0, p0, Lcom/jess/ui/r;->d:Lcom/jess/ui/TwoWayAbsListView;

    iget-object v0, v0, Lcom/jess/ui/TwoWayAbsListView;->m:Landroid/graphics/Rect;

    iget v5, v0, Landroid/graphics/Rect;->top:I

    .line 4342
    iget-object v0, p0, Lcom/jess/ui/r;->d:Lcom/jess/ui/TwoWayAbsListView;

    invoke-virtual {v0}, Lcom/jess/ui/TwoWayAbsListView;->getBottom()I

    move-result v0

    iget-object v1, p0, Lcom/jess/ui/r;->d:Lcom/jess/ui/TwoWayAbsListView;

    invoke-virtual {v1}, Lcom/jess/ui/TwoWayAbsListView;->getTop()I

    move-result v1

    sub-int/2addr v0, v1

    iget-object v1, p0, Lcom/jess/ui/r;->d:Lcom/jess/ui/TwoWayAbsListView;

    iget-object v1, v1, Lcom/jess/ui/TwoWayAbsListView;->m:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->bottom:I

    sub-int v7, v0, v1

    .line 4343
    iget-object v0, p0, Lcom/jess/ui/r;->d:Lcom/jess/ui/TwoWayAbsListView;

    iget v1, v0, Lcom/jess/ui/TwoWayAbsListView;->H:I

    .line 4344
    iget-object v0, p0, Lcom/jess/ui/r;->d:Lcom/jess/ui/TwoWayAbsListView;

    iget v2, v0, Lcom/jess/ui/TwoWayAbsListView;->A:I

    .line 4347
    if-lt v2, v1, :cond_3

    add-int v0, v1, v9

    if-ge v2, v0, :cond_3

    .line 4350
    iget-object v0, p0, Lcom/jess/ui/r;->d:Lcom/jess/ui/TwoWayAbsListView;

    iget-object v8, p0, Lcom/jess/ui/r;->d:Lcom/jess/ui/TwoWayAbsListView;

    iget v8, v8, Lcom/jess/ui/TwoWayAbsListView;->H:I

    sub-int v8, v2, v8

    invoke-virtual {v0, v8}, Lcom/jess/ui/TwoWayAbsListView;->getChildAt(I)Landroid/view/View;

    move-result-object v8

    .line 4351
    invoke-virtual {v8}, Landroid/view/View;->getTop()I

    move-result v0

    .line 4352
    invoke-virtual {v8}, Landroid/view/View;->getBottom()I

    move-result v9

    .line 4355
    if-ge v0, v5, :cond_2

    .line 4356
    iget-object v0, p0, Lcom/jess/ui/r;->d:Lcom/jess/ui/TwoWayAbsListView;

    invoke-virtual {v0}, Lcom/jess/ui/TwoWayAbsListView;->getVerticalFadingEdgeLength()I

    move-result v0

    add-int/2addr v0, v5

    :cond_1
    :goto_1
    move v5, v0

    move v0, v3

    .line 4412
    :goto_2
    iget-object v7, p0, Lcom/jess/ui/r;->d:Lcom/jess/ui/TwoWayAbsListView;

    iput v6, v7, Lcom/jess/ui/TwoWayAbsListView;->A:I

    .line 4413
    iget-object v7, p0, Lcom/jess/ui/r;->d:Lcom/jess/ui/TwoWayAbsListView;

    iget-object v8, p0, Lcom/jess/ui/r;->f:Lcom/jess/ui/p;

    invoke-virtual {v7, v8}, Lcom/jess/ui/TwoWayAbsListView;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 4414
    iget-object v7, p0, Lcom/jess/ui/r;->d:Lcom/jess/ui/TwoWayAbsListView;

    iput v6, v7, Lcom/jess/ui/TwoWayAbsListView;->w:I

    .line 4415
    invoke-virtual {p0}, Lcom/jess/ui/r;->c()V

    .line 4416
    iget-object v7, p0, Lcom/jess/ui/r;->d:Lcom/jess/ui/TwoWayAbsListView;

    iput v5, v7, Lcom/jess/ui/TwoWayAbsListView;->I:I

    .line 4417
    iget-object v5, p0, Lcom/jess/ui/r;->d:Lcom/jess/ui/TwoWayAbsListView;

    invoke-virtual {v5, v2, v0}, Lcom/jess/ui/TwoWayAbsListView;->a(IZ)I

    move-result v0

    .line 4418
    if-lt v0, v1, :cond_9

    iget-object v1, p0, Lcom/jess/ui/r;->d:Lcom/jess/ui/TwoWayAbsListView;

    invoke-virtual {v1}, Lcom/jess/ui/TwoWayAbsListView;->getLastVisiblePosition()I

    move-result v1

    if-gt v0, v1, :cond_9

    .line 4419
    iget-object v1, p0, Lcom/jess/ui/r;->d:Lcom/jess/ui/TwoWayAbsListView;

    const/4 v2, 0x4

    iput v2, v1, Lcom/jess/ui/TwoWayAbsListView;->a:I

    .line 4420
    iget-object v1, p0, Lcom/jess/ui/r;->d:Lcom/jess/ui/TwoWayAbsListView;

    invoke-virtual {v1, v0}, Lcom/jess/ui/TwoWayAbsListView;->setSelectionInt(I)V

    .line 4421
    iget-object v1, p0, Lcom/jess/ui/r;->d:Lcom/jess/ui/TwoWayAbsListView;

    invoke-virtual {v1}, Lcom/jess/ui/TwoWayAbsListView;->a()V

    .line 4425
    :goto_3
    invoke-virtual {p0, v4}, Lcom/jess/ui/r;->b(I)V

    .line 4427
    if-ltz v0, :cond_a

    :goto_4
    move v4, v3

    goto/16 :goto_0

    .line 4357
    :cond_2
    if-le v9, v7, :cond_1

    .line 4358
    invoke-virtual {v8}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    sub-int v0, v7, v0

    iget-object v5, p0, Lcom/jess/ui/r;->d:Lcom/jess/ui/TwoWayAbsListView;

    .line 4359
    invoke-virtual {v5}, Lcom/jess/ui/TwoWayAbsListView;->getVerticalFadingEdgeLength()I

    move-result v5

    sub-int/2addr v0, v5

    goto :goto_1

    .line 4362
    :cond_3
    if-ge v2, v1, :cond_6

    move v7, v4

    move v0, v4

    .line 4365
    :goto_5
    if-ge v7, v9, :cond_10

    .line 4366
    iget-object v2, p0, Lcom/jess/ui/r;->d:Lcom/jess/ui/TwoWayAbsListView;

    invoke-virtual {v2, v7}, Lcom/jess/ui/TwoWayAbsListView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 4367
    invoke-virtual {v2}, Landroid/view/View;->getTop()I

    move-result v2

    .line 4369
    if-nez v7, :cond_f

    .line 4373
    if-gtz v1, :cond_4

    if-ge v2, v5, :cond_e

    .line 4376
    :cond_4
    iget-object v0, p0, Lcom/jess/ui/r;->d:Lcom/jess/ui/TwoWayAbsListView;

    invoke-virtual {v0}, Lcom/jess/ui/TwoWayAbsListView;->getVerticalFadingEdgeLength()I

    move-result v0

    add-int/2addr v0, v5

    move v5, v2

    .line 4379
    :goto_6
    if-lt v2, v0, :cond_5

    .line 4381
    add-int v0, v1, v7

    :goto_7
    move v5, v2

    move v2, v0

    move v0, v3

    .line 4365
    goto :goto_2

    :cond_5
    add-int/lit8 v2, v7, 0x1

    move v7, v2

    move v13, v0

    move v0, v5

    move v5, v13

    goto :goto_5

    .line 4387
    :cond_6
    iget-object v0, p0, Lcom/jess/ui/r;->d:Lcom/jess/ui/TwoWayAbsListView;

    iget v10, v0, Lcom/jess/ui/TwoWayAbsListView;->aa:I

    .line 4389
    add-int v0, v1, v9

    add-int/lit8 v0, v0, -0x1

    .line 4391
    add-int/lit8 v2, v9, -0x1

    move v8, v2

    move v5, v4

    :goto_8
    if-ltz v8, :cond_d

    .line 4392
    iget-object v2, p0, Lcom/jess/ui/r;->d:Lcom/jess/ui/TwoWayAbsListView;

    invoke-virtual {v2, v8}, Lcom/jess/ui/TwoWayAbsListView;->getChildAt(I)Landroid/view/View;

    move-result-object v11

    .line 4393
    invoke-virtual {v11}, Landroid/view/View;->getTop()I

    move-result v2

    .line 4394
    invoke-virtual {v11}, Landroid/view/View;->getBottom()I

    move-result v11

    .line 4396
    add-int/lit8 v12, v9, -0x1

    if-ne v8, v12, :cond_c

    .line 4398
    add-int v5, v1, v9

    if-lt v5, v10, :cond_7

    if-le v11, v7, :cond_b

    .line 4399
    :cond_7
    iget-object v5, p0, Lcom/jess/ui/r;->d:Lcom/jess/ui/TwoWayAbsListView;

    invoke-virtual {v5}, Lcom/jess/ui/TwoWayAbsListView;->getVerticalFadingEdgeLength()I

    move-result v5

    sub-int v5, v7, v5

    move v7, v2

    .line 4403
    :goto_9
    if-gt v11, v5, :cond_8

    .line 4404
    add-int v0, v1, v8

    move v5, v2

    move v2, v0

    move v0, v4

    .line 4406
    goto/16 :goto_2

    .line 4391
    :cond_8
    add-int/lit8 v2, v8, -0x1

    move v8, v2

    move v13, v5

    move v5, v7

    move v7, v13

    goto :goto_8

    :cond_9
    move v0, v6

    .line 4423
    goto/16 :goto_3

    :cond_a
    move v3, v4

    .line 4427
    goto/16 :goto_4

    :cond_b
    move v5, v7

    move v7, v2

    goto :goto_9

    :cond_c
    move v13, v7

    move v7, v5

    move v5, v13

    goto :goto_9

    :cond_d
    move v2, v0

    move v0, v4

    goto/16 :goto_2

    :cond_e
    move v0, v5

    move v5, v2

    goto :goto_6

    :cond_f
    move v13, v5

    move v5, v0

    move v0, v13

    goto :goto_6

    :cond_10
    move v2, v0

    move v0, v1

    goto :goto_7
.end method

.method a(II)Z
    .locals 18

    .prologue
    .line 4193
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/jess/ui/r;->d:Lcom/jess/ui/TwoWayAbsListView;

    invoke-virtual {v1}, Lcom/jess/ui/TwoWayAbsListView;->getChildCount()I

    move-result v8

    .line 4194
    if-nez v8, :cond_0

    .line 4195
    const/4 v1, 0x1

    .line 4323
    :goto_0
    return v1

    .line 4198
    :cond_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/jess/ui/r;->d:Lcom/jess/ui/TwoWayAbsListView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/jess/ui/TwoWayAbsListView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v2

    .line 4199
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/jess/ui/r;->d:Lcom/jess/ui/TwoWayAbsListView;

    add-int/lit8 v3, v8, -0x1

    invoke-virtual {v1, v3}, Lcom/jess/ui/TwoWayAbsListView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getBottom()I

    move-result v3

    .line 4201
    new-instance v6, Landroid/graphics/Rect;

    const/4 v1, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v7, 0x0

    invoke-direct {v6, v1, v4, v5, v7}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 4204
    iget v1, v6, Landroid/graphics/Rect;->top:I

    sub-int v9, v1, v2

    .line 4205
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/jess/ui/r;->d:Lcom/jess/ui/TwoWayAbsListView;

    invoke-virtual {v1}, Lcom/jess/ui/TwoWayAbsListView;->getHeight()I

    move-result v1

    iget v4, v6, Landroid/graphics/Rect;->bottom:I

    sub-int v4, v1, v4

    .line 4206
    sub-int v10, v3, v4

    .line 4208
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/jess/ui/r;->d:Lcom/jess/ui/TwoWayAbsListView;

    invoke-virtual {v1}, Lcom/jess/ui/TwoWayAbsListView;->getHeight()I

    move-result v1

    add-int/lit8 v5, v1, 0x0

    .line 4209
    if-gez p1, :cond_1

    .line 4210
    add-int/lit8 v1, v5, -0x1

    neg-int v1, v1

    move/from16 v0, p1

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v1

    move v7, v1

    .line 4215
    :goto_1
    if-gez p2, :cond_2

    .line 4216
    add-int/lit8 v1, v5, -0x1

    neg-int v1, v1

    move/from16 v0, p2

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 4221
    :goto_2
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/jess/ui/r;->d:Lcom/jess/ui/TwoWayAbsListView;

    iget v11, v5, Lcom/jess/ui/TwoWayAbsListView;->H:I

    .line 4223
    if-nez v11, :cond_3

    iget v5, v6, Landroid/graphics/Rect;->top:I

    if-lt v2, v5, :cond_3

    if-ltz v7, :cond_3

    .line 4226
    const/4 v1, 0x1

    goto :goto_0

    .line 4212
    :cond_1
    add-int/lit8 v1, v5, -0x1

    move/from16 v0, p1

    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v1

    move v7, v1

    goto :goto_1

    .line 4218
    :cond_2
    add-int/lit8 v1, v5, -0x1

    move/from16 v0, p2

    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v1

    goto :goto_2

    .line 4229
    :cond_3
    add-int v2, v11, v8

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/jess/ui/r;->d:Lcom/jess/ui/TwoWayAbsListView;

    iget v5, v5, Lcom/jess/ui/TwoWayAbsListView;->aa:I

    if-ne v2, v5, :cond_4

    if-gt v3, v4, :cond_4

    if-gtz v7, :cond_4

    .line 4232
    const/4 v1, 0x1

    goto/16 :goto_0

    .line 4235
    :cond_4
    if-gez v1, :cond_c

    const/4 v2, 0x1

    .line 4237
    :goto_3
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/jess/ui/r;->d:Lcom/jess/ui/TwoWayAbsListView;

    invoke-virtual {v3}, Lcom/jess/ui/TwoWayAbsListView;->isInTouchMode()Z

    move-result v12

    .line 4238
    if-eqz v12, :cond_5

    .line 4239
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/jess/ui/r;->d:Lcom/jess/ui/TwoWayAbsListView;

    invoke-virtual {v3}, Lcom/jess/ui/TwoWayAbsListView;->i()V

    .line 4242
    :cond_5
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/jess/ui/r;->d:Lcom/jess/ui/TwoWayAbsListView;

    invoke-virtual {v3}, Lcom/jess/ui/TwoWayAbsListView;->getHeaderViewsCount()I

    move-result v13

    .line 4243
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/jess/ui/r;->d:Lcom/jess/ui/TwoWayAbsListView;

    iget v3, v3, Lcom/jess/ui/TwoWayAbsListView;->aa:I

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/jess/ui/r;->d:Lcom/jess/ui/TwoWayAbsListView;

    invoke-virtual {v4}, Lcom/jess/ui/TwoWayAbsListView;->getFooterViewsCount()I

    move-result v4

    sub-int v14, v3, v4

    .line 4245
    const/4 v4, 0x0

    .line 4246
    const/4 v5, 0x0

    .line 4248
    if-eqz v2, :cond_f

    .line 4249
    iget v3, v6, Landroid/graphics/Rect;->top:I

    sub-int v15, v3, v1

    .line 4250
    const/4 v3, 0x0

    move/from16 v17, v3

    move v3, v5

    move/from16 v5, v17

    :goto_4
    if-ge v5, v8, :cond_6

    .line 4251
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/jess/ui/r;->d:Lcom/jess/ui/TwoWayAbsListView;

    invoke-virtual {v6, v5}, Lcom/jess/ui/TwoWayAbsListView;->getChildAt(I)Landroid/view/View;

    move-result-object v16

    .line 4252
    invoke-virtual/range {v16 .. v16}, Landroid/view/View;->getBottom()I

    move-result v6

    if-lt v6, v15, :cond_d

    .line 4291
    :cond_6
    move-object/from16 v0, p0

    iget v5, v0, Lcom/jess/ui/r;->a:I

    add-int/2addr v5, v7

    move-object/from16 v0, p0

    iput v5, v0, Lcom/jess/ui/r;->c:I

    .line 4293
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/jess/ui/r;->d:Lcom/jess/ui/TwoWayAbsListView;

    const/4 v6, 0x1

    iput-boolean v6, v5, Lcom/jess/ui/TwoWayAbsListView;->ae:Z

    .line 4295
    if-lez v3, :cond_7

    .line 4296
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/jess/ui/r;->d:Lcom/jess/ui/TwoWayAbsListView;

    invoke-static {v5, v4, v3}, Lcom/jess/ui/TwoWayAbsListView;->a(Lcom/jess/ui/TwoWayAbsListView;II)V

    .line 4298
    :cond_7
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/jess/ui/r;->d:Lcom/jess/ui/TwoWayAbsListView;

    invoke-virtual {v4, v1}, Lcom/jess/ui/TwoWayAbsListView;->e(I)V

    .line 4300
    if-eqz v2, :cond_8

    .line 4301
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/jess/ui/r;->d:Lcom/jess/ui/TwoWayAbsListView;

    iget v5, v4, Lcom/jess/ui/TwoWayAbsListView;->H:I

    add-int/2addr v3, v5

    iput v3, v4, Lcom/jess/ui/TwoWayAbsListView;->H:I

    .line 4304
    :cond_8
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/jess/ui/r;->d:Lcom/jess/ui/TwoWayAbsListView;

    invoke-virtual {v3}, Lcom/jess/ui/TwoWayAbsListView;->invalidate()V

    .line 4306
    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v1

    .line 4307
    if-lt v9, v1, :cond_9

    if-ge v10, v1, :cond_a

    .line 4308
    :cond_9
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/jess/ui/r;->d:Lcom/jess/ui/TwoWayAbsListView;

    invoke-virtual {v1, v2}, Lcom/jess/ui/TwoWayAbsListView;->a(Z)V

    .line 4311
    :cond_a
    if-nez v12, :cond_b

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/jess/ui/r;->d:Lcom/jess/ui/TwoWayAbsListView;

    iget v1, v1, Lcom/jess/ui/TwoWayAbsListView;->V:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_b

    .line 4312
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/jess/ui/r;->d:Lcom/jess/ui/TwoWayAbsListView;

    iget v1, v1, Lcom/jess/ui/TwoWayAbsListView;->V:I

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/jess/ui/r;->d:Lcom/jess/ui/TwoWayAbsListView;

    iget v2, v2, Lcom/jess/ui/TwoWayAbsListView;->H:I

    sub-int/2addr v1, v2

    .line 4313
    if-ltz v1, :cond_b

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/jess/ui/r;->d:Lcom/jess/ui/TwoWayAbsListView;

    invoke-virtual {v2}, Lcom/jess/ui/TwoWayAbsListView;->getChildCount()I

    move-result v2

    if-ge v1, v2, :cond_b

    .line 4314
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/jess/ui/r;->d:Lcom/jess/ui/TwoWayAbsListView;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/jess/ui/r;->d:Lcom/jess/ui/TwoWayAbsListView;

    invoke-virtual {v3, v1}, Lcom/jess/ui/TwoWayAbsListView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v2, v1}, Lcom/jess/ui/TwoWayAbsListView;->a(Landroid/view/View;)V

    .line 4318
    :cond_b
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/jess/ui/r;->d:Lcom/jess/ui/TwoWayAbsListView;

    const/4 v2, 0x0

    iput-boolean v2, v1, Lcom/jess/ui/TwoWayAbsListView;->ae:Z

    .line 4320
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/jess/ui/r;->d:Lcom/jess/ui/TwoWayAbsListView;

    invoke-virtual {v1}, Lcom/jess/ui/TwoWayAbsListView;->a()V

    .line 4323
    const/4 v1, 0x0

    goto/16 :goto_0

    .line 4235
    :cond_c
    const/4 v2, 0x0

    goto/16 :goto_3

    .line 4255
    :cond_d
    add-int/lit8 v6, v3, 0x1

    .line 4256
    add-int v3, v11, v5

    .line 4257
    if-lt v3, v13, :cond_e

    if-ge v3, v14, :cond_e

    .line 4258
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/jess/ui/r;->d:Lcom/jess/ui/TwoWayAbsListView;

    iget-object v3, v3, Lcom/jess/ui/TwoWayAbsListView;->g:Lcom/jess/ui/k;

    move-object/from16 v0, v16

    invoke-virtual {v3, v0}, Lcom/jess/ui/k;->a(Landroid/view/View;)V

    .line 4250
    :cond_e
    add-int/lit8 v3, v5, 0x1

    move v5, v3

    move v3, v6

    goto/16 :goto_4

    .line 4269
    :cond_f
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/jess/ui/r;->d:Lcom/jess/ui/TwoWayAbsListView;

    invoke-virtual {v3}, Lcom/jess/ui/TwoWayAbsListView;->getHeight()I

    move-result v3

    iget v6, v6, Landroid/graphics/Rect;->bottom:I

    sub-int/2addr v3, v6

    sub-int v6, v3, v1

    .line 4270
    add-int/lit8 v3, v8, -0x1

    move/from16 v17, v3

    move v3, v5

    move/from16 v5, v17

    :goto_5
    if-ltz v5, :cond_6

    .line 4271
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/jess/ui/r;->d:Lcom/jess/ui/TwoWayAbsListView;

    invoke-virtual {v8, v5}, Lcom/jess/ui/TwoWayAbsListView;->getChildAt(I)Landroid/view/View;

    move-result-object v8

    .line 4272
    invoke-virtual {v8}, Landroid/view/View;->getTop()I

    move-result v15

    if-le v15, v6, :cond_6

    .line 4276
    add-int/lit8 v4, v3, 0x1

    .line 4277
    add-int v3, v11, v5

    .line 4278
    if-lt v3, v13, :cond_10

    if-ge v3, v14, :cond_10

    .line 4279
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/jess/ui/r;->d:Lcom/jess/ui/TwoWayAbsListView;

    iget-object v3, v3, Lcom/jess/ui/TwoWayAbsListView;->g:Lcom/jess/ui/k;

    invoke-virtual {v3, v8}, Lcom/jess/ui/k;->a(Landroid/view/View;)V

    .line 4270
    :cond_10
    add-int/lit8 v3, v5, -0x1

    move/from16 v17, v3

    move v3, v4

    move v4, v5

    move/from16 v5, v17

    goto :goto_5
.end method

.method public a(Landroid/view/MotionEvent;)Z
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v0, 0x1

    const/4 v3, -0x1

    const/4 v1, 0x0

    .line 4122
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    .line 4133
    packed-switch v2, :pswitch_data_0

    :cond_0
    :goto_0
    move v0, v1

    .line 4179
    :goto_1
    return v0

    .line 4135
    :pswitch_0
    iget-object v2, p0, Lcom/jess/ui/r;->d:Lcom/jess/ui/TwoWayAbsListView;

    iget v2, v2, Lcom/jess/ui/TwoWayAbsListView;->w:I

    .line 4137
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    float-to-int v3, v3

    .line 4138
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    float-to-int v4, v4

    .line 4140
    iget-object v5, p0, Lcom/jess/ui/r;->d:Lcom/jess/ui/TwoWayAbsListView;

    invoke-virtual {v5, v4}, Lcom/jess/ui/TwoWayAbsListView;->a(I)I

    move-result v5

    .line 4141
    if-eq v2, v8, :cond_1

    if-ltz v5, :cond_1

    .line 4144
    iget-object v6, p0, Lcom/jess/ui/r;->d:Lcom/jess/ui/TwoWayAbsListView;

    iget-object v7, p0, Lcom/jess/ui/r;->d:Lcom/jess/ui/TwoWayAbsListView;

    iget v7, v7, Lcom/jess/ui/TwoWayAbsListView;->H:I

    sub-int v7, v5, v7

    invoke-virtual {v6, v7}, Lcom/jess/ui/TwoWayAbsListView;->getChildAt(I)Landroid/view/View;

    move-result-object v6

    .line 4145
    invoke-virtual {v6}, Landroid/view/View;->getTop()I

    move-result v6

    iput v6, p0, Lcom/jess/ui/r;->a:I

    .line 4146
    iget-object v6, p0, Lcom/jess/ui/r;->d:Lcom/jess/ui/TwoWayAbsListView;

    iput v3, v6, Lcom/jess/ui/TwoWayAbsListView;->u:I

    .line 4147
    iget-object v3, p0, Lcom/jess/ui/r;->d:Lcom/jess/ui/TwoWayAbsListView;

    iput v4, v3, Lcom/jess/ui/TwoWayAbsListView;->v:I

    .line 4148
    iget-object v3, p0, Lcom/jess/ui/r;->d:Lcom/jess/ui/TwoWayAbsListView;

    iput v5, v3, Lcom/jess/ui/TwoWayAbsListView;->t:I

    .line 4149
    iget-object v3, p0, Lcom/jess/ui/r;->d:Lcom/jess/ui/TwoWayAbsListView;

    iput v1, v3, Lcom/jess/ui/TwoWayAbsListView;->w:I

    .line 4150
    invoke-virtual {p0}, Lcom/jess/ui/r;->c()V

    .line 4152
    :cond_1
    const/high16 v3, -0x80000000

    iput v3, p0, Lcom/jess/ui/r;->b:I

    .line 4153
    if-ne v2, v8, :cond_0

    goto :goto_1

    .line 4160
    :pswitch_1
    iget-object v2, p0, Lcom/jess/ui/r;->d:Lcom/jess/ui/TwoWayAbsListView;

    iget v2, v2, Lcom/jess/ui/TwoWayAbsListView;->w:I

    packed-switch v2, :pswitch_data_1

    goto :goto_0

    .line 4162
    :pswitch_2
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    float-to-int v2, v2

    .line 4163
    iget-object v3, p0, Lcom/jess/ui/r;->d:Lcom/jess/ui/TwoWayAbsListView;

    iget v3, v3, Lcom/jess/ui/TwoWayAbsListView;->v:I

    sub-int/2addr v2, v3

    invoke-virtual {p0, v2}, Lcom/jess/ui/r;->a(I)Z

    move-result v2

    if-eqz v2, :cond_0

    goto :goto_1

    .line 4172
    :pswitch_3
    iget-object v0, p0, Lcom/jess/ui/r;->d:Lcom/jess/ui/TwoWayAbsListView;

    iput v3, v0, Lcom/jess/ui/TwoWayAbsListView;->w:I

    .line 4173
    iget-object v0, p0, Lcom/jess/ui/r;->d:Lcom/jess/ui/TwoWayAbsListView;

    invoke-static {v0, v3}, Lcom/jess/ui/TwoWayAbsListView;->c(Lcom/jess/ui/TwoWayAbsListView;I)I

    .line 4174
    invoke-virtual {p0, v1}, Lcom/jess/ui/r;->b(I)V

    goto :goto_0

    .line 4133
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_3
        :pswitch_1
    .end packed-switch

    .line 4160
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_2
    .end packed-switch
.end method

.method public b(Landroid/view/MotionEvent;)Z
    .locals 10

    .prologue
    const/high16 v9, -0x80000000

    const/4 v8, 0x0

    const/4 v2, 0x1

    const/4 v7, -0x1

    const/4 v1, 0x0

    .line 3845
    iget-object v0, p0, Lcom/jess/ui/r;->d:Lcom/jess/ui/TwoWayAbsListView;

    invoke-virtual {v0}, Lcom/jess/ui/TwoWayAbsListView;->isEnabled()Z

    move-result v0

    if-nez v0, :cond_3

    .line 3848
    iget-object v0, p0, Lcom/jess/ui/r;->d:Lcom/jess/ui/TwoWayAbsListView;

    invoke-virtual {v0}, Lcom/jess/ui/TwoWayAbsListView;->isClickable()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/jess/ui/r;->d:Lcom/jess/ui/TwoWayAbsListView;

    invoke-virtual {v0}, Lcom/jess/ui/TwoWayAbsListView;->isLongClickable()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    move v0, v2

    :goto_0
    move v2, v0

    .line 4115
    :cond_1
    :goto_1
    return v2

    :cond_2
    move v0, v1

    .line 3848
    goto :goto_0

    .line 3859
    :cond_3
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    .line 3864
    iget-object v3, p0, Lcom/jess/ui/r;->d:Lcom/jess/ui/TwoWayAbsListView;

    invoke-static {v3}, Lcom/jess/ui/TwoWayAbsListView;->i(Lcom/jess/ui/TwoWayAbsListView;)Landroid/view/VelocityTracker;

    move-result-object v3

    if-nez v3, :cond_4

    .line 3865
    iget-object v3, p0, Lcom/jess/ui/r;->d:Lcom/jess/ui/TwoWayAbsListView;

    invoke-static {}, Landroid/view/VelocityTracker;->obtain()Landroid/view/VelocityTracker;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/jess/ui/TwoWayAbsListView;->a(Lcom/jess/ui/TwoWayAbsListView;Landroid/view/VelocityTracker;)Landroid/view/VelocityTracker;

    .line 3867
    :cond_4
    iget-object v3, p0, Lcom/jess/ui/r;->d:Lcom/jess/ui/TwoWayAbsListView;

    invoke-static {v3}, Lcom/jess/ui/TwoWayAbsListView;->i(Lcom/jess/ui/TwoWayAbsListView;)Landroid/view/VelocityTracker;

    move-result-object v3

    invoke-virtual {v3, p1}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    .line 3869
    packed-switch v0, :pswitch_data_0

    goto :goto_1

    .line 3871
    :pswitch_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    float-to-int v4, v0

    .line 3872
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    float-to-int v5, v0

    .line 3873
    iget-object v0, p0, Lcom/jess/ui/r;->d:Lcom/jess/ui/TwoWayAbsListView;

    invoke-virtual {v0, v4, v5}, Lcom/jess/ui/TwoWayAbsListView;->a(II)I

    move-result v3

    .line 3874
    iget-object v0, p0, Lcom/jess/ui/r;->d:Lcom/jess/ui/TwoWayAbsListView;

    iget-boolean v0, v0, Lcom/jess/ui/TwoWayAbsListView;->S:Z

    if-nez v0, :cond_1f

    .line 3875
    iget-object v0, p0, Lcom/jess/ui/r;->d:Lcom/jess/ui/TwoWayAbsListView;

    iget v0, v0, Lcom/jess/ui/TwoWayAbsListView;->w:I

    const/4 v6, 0x4

    if-eq v0, v6, :cond_7

    if-ltz v3, :cond_7

    iget-object v0, p0, Lcom/jess/ui/r;->d:Lcom/jess/ui/TwoWayAbsListView;

    .line 3876
    invoke-virtual {v0}, Lcom/jess/ui/TwoWayAbsListView;->getAdapter()Landroid/widget/Adapter;

    move-result-object v0

    check-cast v0, Landroid/widget/ListAdapter;

    invoke-interface {v0, v3}, Landroid/widget/ListAdapter;->isEnabled(I)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 3879
    iget-object v0, p0, Lcom/jess/ui/r;->d:Lcom/jess/ui/TwoWayAbsListView;

    iput v1, v0, Lcom/jess/ui/TwoWayAbsListView;->w:I

    .line 3881
    iget-object v0, p0, Lcom/jess/ui/r;->d:Lcom/jess/ui/TwoWayAbsListView;

    invoke-static {v0}, Lcom/jess/ui/TwoWayAbsListView;->j(Lcom/jess/ui/TwoWayAbsListView;)Ljava/lang/Runnable;

    move-result-object v0

    if-nez v0, :cond_5

    .line 3882
    iget-object v0, p0, Lcom/jess/ui/r;->d:Lcom/jess/ui/TwoWayAbsListView;

    new-instance v1, Lcom/jess/ui/d;

    iget-object v6, p0, Lcom/jess/ui/r;->d:Lcom/jess/ui/TwoWayAbsListView;

    invoke-direct {v1, v6}, Lcom/jess/ui/d;-><init>(Lcom/jess/ui/TwoWayAbsListView;)V

    invoke-static {v0, v1}, Lcom/jess/ui/TwoWayAbsListView;->b(Lcom/jess/ui/TwoWayAbsListView;Ljava/lang/Runnable;)Ljava/lang/Runnable;

    .line 3884
    :cond_5
    iget-object v0, p0, Lcom/jess/ui/r;->d:Lcom/jess/ui/TwoWayAbsListView;

    iget-object v1, p0, Lcom/jess/ui/r;->d:Lcom/jess/ui/TwoWayAbsListView;

    invoke-static {v1}, Lcom/jess/ui/TwoWayAbsListView;->j(Lcom/jess/ui/TwoWayAbsListView;)Ljava/lang/Runnable;

    move-result-object v1

    invoke-static {}, Landroid/view/ViewConfiguration;->getTapTimeout()I

    move-result v6

    int-to-long v6, v6

    invoke-virtual {v0, v1, v6, v7}, Lcom/jess/ui/TwoWayAbsListView;->postDelayed(Ljava/lang/Runnable;J)Z

    move v0, v3

    .line 3904
    :goto_2
    if-ltz v0, :cond_6

    .line 3906
    iget-object v1, p0, Lcom/jess/ui/r;->d:Lcom/jess/ui/TwoWayAbsListView;

    iget-object v3, p0, Lcom/jess/ui/r;->d:Lcom/jess/ui/TwoWayAbsListView;

    iget v3, v3, Lcom/jess/ui/TwoWayAbsListView;->H:I

    sub-int v3, v0, v3

    invoke-virtual {v1, v3}, Lcom/jess/ui/TwoWayAbsListView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 3907
    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v1

    iput v1, p0, Lcom/jess/ui/r;->a:I

    .line 3909
    :cond_6
    iget-object v1, p0, Lcom/jess/ui/r;->d:Lcom/jess/ui/TwoWayAbsListView;

    iput v4, v1, Lcom/jess/ui/TwoWayAbsListView;->u:I

    .line 3910
    iget-object v1, p0, Lcom/jess/ui/r;->d:Lcom/jess/ui/TwoWayAbsListView;

    iput v5, v1, Lcom/jess/ui/TwoWayAbsListView;->v:I

    .line 3911
    iget-object v1, p0, Lcom/jess/ui/r;->d:Lcom/jess/ui/TwoWayAbsListView;

    iput v0, v1, Lcom/jess/ui/TwoWayAbsListView;->t:I

    .line 3912
    iput v9, p0, Lcom/jess/ui/r;->b:I

    goto/16 :goto_1

    .line 3886
    :cond_7
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEdgeFlags()I

    move-result v0

    if-eqz v0, :cond_8

    if-gez v3, :cond_8

    move v2, v1

    .line 3890
    goto/16 :goto_1

    .line 3893
    :cond_8
    iget-object v0, p0, Lcom/jess/ui/r;->d:Lcom/jess/ui/TwoWayAbsListView;

    iget v0, v0, Lcom/jess/ui/TwoWayAbsListView;->w:I

    const/4 v6, 0x4

    if-ne v0, v6, :cond_1f

    .line 3895
    invoke-virtual {p0}, Lcom/jess/ui/r;->b()V

    .line 3896
    iget-object v0, p0, Lcom/jess/ui/r;->d:Lcom/jess/ui/TwoWayAbsListView;

    const/4 v3, 0x3

    iput v3, v0, Lcom/jess/ui/TwoWayAbsListView;->w:I

    .line 3897
    iput v1, p0, Lcom/jess/ui/r;->g:I

    .line 3898
    iget-object v0, p0, Lcom/jess/ui/r;->d:Lcom/jess/ui/TwoWayAbsListView;

    invoke-virtual {v0, v5}, Lcom/jess/ui/TwoWayAbsListView;->a(I)I

    move-result v0

    .line 3899
    invoke-virtual {p0, v2}, Lcom/jess/ui/r;->b(I)V

    goto :goto_2

    .line 3917
    :pswitch_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    float-to-int v4, v0

    .line 3918
    iget-object v0, p0, Lcom/jess/ui/r;->d:Lcom/jess/ui/TwoWayAbsListView;

    iget v0, v0, Lcom/jess/ui/TwoWayAbsListView;->v:I

    sub-int v0, v4, v0

    .line 3919
    iget-object v3, p0, Lcom/jess/ui/r;->d:Lcom/jess/ui/TwoWayAbsListView;

    iget v3, v3, Lcom/jess/ui/TwoWayAbsListView;->w:I

    packed-switch v3, :pswitch_data_1

    goto/16 :goto_1

    .line 3925
    :pswitch_2
    invoke-virtual {p0, v0}, Lcom/jess/ui/r;->a(I)Z

    goto/16 :goto_1

    .line 3935
    :pswitch_3
    iget v3, p0, Lcom/jess/ui/r;->b:I

    if-eq v4, v3, :cond_1

    .line 3936
    iget v3, p0, Lcom/jess/ui/r;->g:I

    sub-int v3, v0, v3

    .line 3937
    iget v0, p0, Lcom/jess/ui/r;->b:I

    if-eq v0, v9, :cond_c

    iget v0, p0, Lcom/jess/ui/r;->b:I

    sub-int v0, v4, v0

    .line 3941
    :goto_3
    if-eqz v0, :cond_9

    .line 3942
    invoke-virtual {p0, v3, v0}, Lcom/jess/ui/r;->a(II)Z

    move-result v1

    .line 3946
    :cond_9
    if-eqz v1, :cond_b

    iget-object v0, p0, Lcom/jess/ui/r;->d:Lcom/jess/ui/TwoWayAbsListView;

    invoke-virtual {v0}, Lcom/jess/ui/TwoWayAbsListView;->getChildCount()I

    move-result v0

    if-lez v0, :cond_b

    .line 3951
    iget-object v0, p0, Lcom/jess/ui/r;->d:Lcom/jess/ui/TwoWayAbsListView;

    invoke-virtual {v0, v4}, Lcom/jess/ui/TwoWayAbsListView;->a(I)I

    move-result v0

    .line 3952
    if-ltz v0, :cond_a

    .line 3953
    iget-object v1, p0, Lcom/jess/ui/r;->d:Lcom/jess/ui/TwoWayAbsListView;

    iget-object v3, p0, Lcom/jess/ui/r;->d:Lcom/jess/ui/TwoWayAbsListView;

    iget v3, v3, Lcom/jess/ui/TwoWayAbsListView;->H:I

    sub-int v3, v0, v3

    invoke-virtual {v1, v3}, Lcom/jess/ui/TwoWayAbsListView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 3954
    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v1

    iput v1, p0, Lcom/jess/ui/r;->a:I

    .line 3956
    :cond_a
    iget-object v1, p0, Lcom/jess/ui/r;->d:Lcom/jess/ui/TwoWayAbsListView;

    iput v4, v1, Lcom/jess/ui/TwoWayAbsListView;->v:I

    .line 3957
    iget-object v1, p0, Lcom/jess/ui/r;->d:Lcom/jess/ui/TwoWayAbsListView;

    iput v0, v1, Lcom/jess/ui/TwoWayAbsListView;->t:I

    .line 3958
    iget-object v0, p0, Lcom/jess/ui/r;->d:Lcom/jess/ui/TwoWayAbsListView;

    invoke-virtual {v0}, Lcom/jess/ui/TwoWayAbsListView;->invalidate()V

    .line 3960
    :cond_b
    iput v4, p0, Lcom/jess/ui/r;->b:I

    goto/16 :goto_1

    :cond_c
    move v0, v3

    .line 3937
    goto :goto_3

    .line 3969
    :pswitch_4
    iget-object v0, p0, Lcom/jess/ui/r;->d:Lcom/jess/ui/TwoWayAbsListView;

    iget v0, v0, Lcom/jess/ui/TwoWayAbsListView;->w:I

    packed-switch v0, :pswitch_data_2

    .line 4064
    :goto_4
    iget-object v0, p0, Lcom/jess/ui/r;->d:Lcom/jess/ui/TwoWayAbsListView;

    invoke-virtual {v0, v1}, Lcom/jess/ui/TwoWayAbsListView;->setPressed(Z)V

    .line 4067
    iget-object v0, p0, Lcom/jess/ui/r;->d:Lcom/jess/ui/TwoWayAbsListView;

    invoke-virtual {v0}, Lcom/jess/ui/TwoWayAbsListView;->invalidate()V

    .line 4069
    iget-object v0, p0, Lcom/jess/ui/r;->d:Lcom/jess/ui/TwoWayAbsListView;

    invoke-virtual {v0}, Lcom/jess/ui/TwoWayAbsListView;->getHandler()Landroid/os/Handler;

    move-result-object v0

    .line 4070
    if-eqz v0, :cond_d

    .line 4071
    iget-object v1, p0, Lcom/jess/ui/r;->d:Lcom/jess/ui/TwoWayAbsListView;

    invoke-static {v1}, Lcom/jess/ui/TwoWayAbsListView;->c(Lcom/jess/ui/TwoWayAbsListView;)Lcom/jess/ui/c;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 4074
    :cond_d
    iget-object v0, p0, Lcom/jess/ui/r;->d:Lcom/jess/ui/TwoWayAbsListView;

    invoke-static {v0}, Lcom/jess/ui/TwoWayAbsListView;->i(Lcom/jess/ui/TwoWayAbsListView;)Landroid/view/VelocityTracker;

    move-result-object v0

    if-eqz v0, :cond_e

    .line 4075
    iget-object v0, p0, Lcom/jess/ui/r;->d:Lcom/jess/ui/TwoWayAbsListView;

    invoke-static {v0}, Lcom/jess/ui/TwoWayAbsListView;->i(Lcom/jess/ui/TwoWayAbsListView;)Landroid/view/VelocityTracker;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->recycle()V

    .line 4076
    iget-object v0, p0, Lcom/jess/ui/r;->d:Lcom/jess/ui/TwoWayAbsListView;

    invoke-static {v0, v8}, Lcom/jess/ui/TwoWayAbsListView;->a(Lcom/jess/ui/TwoWayAbsListView;Landroid/view/VelocityTracker;)Landroid/view/VelocityTracker;

    .line 4079
    :cond_e
    iget-object v0, p0, Lcom/jess/ui/r;->d:Lcom/jess/ui/TwoWayAbsListView;

    invoke-static {v0, v7}, Lcom/jess/ui/TwoWayAbsListView;->c(Lcom/jess/ui/TwoWayAbsListView;I)I

    goto/16 :goto_1

    .line 3973
    :pswitch_5
    iget-object v0, p0, Lcom/jess/ui/r;->d:Lcom/jess/ui/TwoWayAbsListView;

    iget v3, v0, Lcom/jess/ui/TwoWayAbsListView;->t:I

    .line 3974
    iget-object v0, p0, Lcom/jess/ui/r;->d:Lcom/jess/ui/TwoWayAbsListView;

    iget-object v4, p0, Lcom/jess/ui/r;->d:Lcom/jess/ui/TwoWayAbsListView;

    iget v4, v4, Lcom/jess/ui/TwoWayAbsListView;->H:I

    sub-int v4, v3, v4

    invoke-virtual {v0, v4}, Lcom/jess/ui/TwoWayAbsListView;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    .line 3975
    if-eqz v4, :cond_17

    invoke-virtual {v4}, Landroid/view/View;->hasFocusable()Z

    move-result v0

    if-nez v0, :cond_17

    .line 3976
    iget-object v0, p0, Lcom/jess/ui/r;->d:Lcom/jess/ui/TwoWayAbsListView;

    iget v0, v0, Lcom/jess/ui/TwoWayAbsListView;->w:I

    if-eqz v0, :cond_f

    .line 3977
    invoke-virtual {v4, v1}, Landroid/view/View;->setPressed(Z)V

    .line 3980
    :cond_f
    iget-object v0, p0, Lcom/jess/ui/r;->d:Lcom/jess/ui/TwoWayAbsListView;

    invoke-static {v0}, Lcom/jess/ui/TwoWayAbsListView;->k(Lcom/jess/ui/TwoWayAbsListView;)Lcom/jess/ui/j;

    move-result-object v0

    if-nez v0, :cond_10

    .line 3981
    iget-object v0, p0, Lcom/jess/ui/r;->d:Lcom/jess/ui/TwoWayAbsListView;

    new-instance v5, Lcom/jess/ui/j;

    iget-object v6, p0, Lcom/jess/ui/r;->d:Lcom/jess/ui/TwoWayAbsListView;

    invoke-direct {v5, v6, v8}, Lcom/jess/ui/j;-><init>(Lcom/jess/ui/TwoWayAbsListView;Lcom/jess/ui/a;)V

    invoke-static {v0, v5}, Lcom/jess/ui/TwoWayAbsListView;->a(Lcom/jess/ui/TwoWayAbsListView;Lcom/jess/ui/j;)Lcom/jess/ui/j;

    .line 3984
    :cond_10
    iget-object v0, p0, Lcom/jess/ui/r;->d:Lcom/jess/ui/TwoWayAbsListView;

    invoke-static {v0}, Lcom/jess/ui/TwoWayAbsListView;->k(Lcom/jess/ui/TwoWayAbsListView;)Lcom/jess/ui/j;

    move-result-object v5

    .line 3985
    iput-object v4, v5, Lcom/jess/ui/j;->a:Landroid/view/View;

    .line 3986
    iput v3, v5, Lcom/jess/ui/j;->b:I

    .line 3987
    invoke-virtual {v5}, Lcom/jess/ui/j;->a()V

    .line 3989
    iget-object v0, p0, Lcom/jess/ui/r;->d:Lcom/jess/ui/TwoWayAbsListView;

    iput v3, v0, Lcom/jess/ui/TwoWayAbsListView;->A:I

    .line 3991
    iget-object v0, p0, Lcom/jess/ui/r;->d:Lcom/jess/ui/TwoWayAbsListView;

    iget v0, v0, Lcom/jess/ui/TwoWayAbsListView;->w:I

    if-eqz v0, :cond_11

    iget-object v0, p0, Lcom/jess/ui/r;->d:Lcom/jess/ui/TwoWayAbsListView;

    iget v0, v0, Lcom/jess/ui/TwoWayAbsListView;->w:I

    if-ne v0, v2, :cond_16

    .line 3992
    :cond_11
    iget-object v0, p0, Lcom/jess/ui/r;->d:Lcom/jess/ui/TwoWayAbsListView;

    invoke-virtual {v0}, Lcom/jess/ui/TwoWayAbsListView;->getHandler()Landroid/os/Handler;

    move-result-object v6

    .line 3993
    if-eqz v6, :cond_12

    .line 3994
    iget-object v0, p0, Lcom/jess/ui/r;->d:Lcom/jess/ui/TwoWayAbsListView;

    iget v0, v0, Lcom/jess/ui/TwoWayAbsListView;->w:I

    if-nez v0, :cond_14

    iget-object v0, p0, Lcom/jess/ui/r;->d:Lcom/jess/ui/TwoWayAbsListView;

    .line 3995
    invoke-static {v0}, Lcom/jess/ui/TwoWayAbsListView;->j(Lcom/jess/ui/TwoWayAbsListView;)Ljava/lang/Runnable;

    move-result-object v0

    .line 3994
    :goto_5
    invoke-virtual {v6, v0}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 3997
    :cond_12
    iget-object v0, p0, Lcom/jess/ui/r;->d:Lcom/jess/ui/TwoWayAbsListView;

    iput v1, v0, Lcom/jess/ui/TwoWayAbsListView;->a:I

    .line 3998
    iget-object v0, p0, Lcom/jess/ui/r;->d:Lcom/jess/ui/TwoWayAbsListView;

    iget-boolean v0, v0, Lcom/jess/ui/TwoWayAbsListView;->S:Z

    if-nez v0, :cond_15

    iget-object v0, p0, Lcom/jess/ui/r;->d:Lcom/jess/ui/TwoWayAbsListView;

    iget-object v0, v0, Lcom/jess/ui/TwoWayAbsListView;->c:Landroid/widget/ListAdapter;

    invoke-interface {v0, v3}, Landroid/widget/ListAdapter;->isEnabled(I)Z

    move-result v0

    if-eqz v0, :cond_15

    .line 3999
    iget-object v0, p0, Lcom/jess/ui/r;->d:Lcom/jess/ui/TwoWayAbsListView;

    iput v2, v0, Lcom/jess/ui/TwoWayAbsListView;->w:I

    .line 4000
    iget-object v0, p0, Lcom/jess/ui/r;->d:Lcom/jess/ui/TwoWayAbsListView;

    iget-object v1, p0, Lcom/jess/ui/r;->d:Lcom/jess/ui/TwoWayAbsListView;

    iget v1, v1, Lcom/jess/ui/TwoWayAbsListView;->t:I

    invoke-virtual {v0, v1}, Lcom/jess/ui/TwoWayAbsListView;->setSelectedPositionInt(I)V

    .line 4001
    iget-object v0, p0, Lcom/jess/ui/r;->d:Lcom/jess/ui/TwoWayAbsListView;

    invoke-virtual {v0}, Lcom/jess/ui/TwoWayAbsListView;->d()V

    .line 4002
    invoke-virtual {v4, v2}, Landroid/view/View;->setPressed(Z)V

    .line 4003
    iget-object v0, p0, Lcom/jess/ui/r;->d:Lcom/jess/ui/TwoWayAbsListView;

    invoke-virtual {v0, v4}, Lcom/jess/ui/TwoWayAbsListView;->a(Landroid/view/View;)V

    .line 4004
    iget-object v0, p0, Lcom/jess/ui/r;->d:Lcom/jess/ui/TwoWayAbsListView;

    invoke-virtual {v0, v2}, Lcom/jess/ui/TwoWayAbsListView;->setPressed(Z)V

    .line 4005
    iget-object v0, p0, Lcom/jess/ui/r;->d:Lcom/jess/ui/TwoWayAbsListView;

    iget-object v0, v0, Lcom/jess/ui/TwoWayAbsListView;->e:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_13

    .line 4006
    iget-object v0, p0, Lcom/jess/ui/r;->d:Lcom/jess/ui/TwoWayAbsListView;

    iget-object v0, v0, Lcom/jess/ui/TwoWayAbsListView;->e:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getCurrent()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 4007
    if-eqz v0, :cond_13

    instance-of v1, v0, Landroid/graphics/drawable/TransitionDrawable;

    if-eqz v1, :cond_13

    .line 4008
    check-cast v0, Landroid/graphics/drawable/TransitionDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/TransitionDrawable;->resetTransition()V

    .line 4011
    :cond_13
    iget-object v0, p0, Lcom/jess/ui/r;->d:Lcom/jess/ui/TwoWayAbsListView;

    new-instance v1, Lcom/jess/ui/s;

    invoke-direct {v1, p0, v4, v5}, Lcom/jess/ui/s;-><init>(Lcom/jess/ui/r;Landroid/view/View;Lcom/jess/ui/j;)V

    .line 4020
    invoke-static {}, Landroid/view/ViewConfiguration;->getPressedStateDuration()I

    move-result v3

    int-to-long v4, v3

    .line 4011
    invoke-virtual {v0, v1, v4, v5}, Lcom/jess/ui/TwoWayAbsListView;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/16 :goto_1

    .line 3995
    :cond_14
    iget-object v0, p0, Lcom/jess/ui/r;->d:Lcom/jess/ui/TwoWayAbsListView;

    invoke-static {v0}, Lcom/jess/ui/TwoWayAbsListView;->c(Lcom/jess/ui/TwoWayAbsListView;)Lcom/jess/ui/c;

    move-result-object v0

    goto :goto_5

    .line 4022
    :cond_15
    iget-object v0, p0, Lcom/jess/ui/r;->d:Lcom/jess/ui/TwoWayAbsListView;

    iput v7, v0, Lcom/jess/ui/TwoWayAbsListView;->w:I

    goto/16 :goto_1

    .line 4025
    :cond_16
    iget-object v0, p0, Lcom/jess/ui/r;->d:Lcom/jess/ui/TwoWayAbsListView;

    iget-boolean v0, v0, Lcom/jess/ui/TwoWayAbsListView;->S:Z

    if-nez v0, :cond_17

    iget-object v0, p0, Lcom/jess/ui/r;->d:Lcom/jess/ui/TwoWayAbsListView;

    iget-object v0, v0, Lcom/jess/ui/TwoWayAbsListView;->c:Landroid/widget/ListAdapter;

    invoke-interface {v0, v3}, Landroid/widget/ListAdapter;->isEnabled(I)Z

    move-result v0

    if-eqz v0, :cond_17

    .line 4026
    iget-object v0, p0, Lcom/jess/ui/r;->d:Lcom/jess/ui/TwoWayAbsListView;

    invoke-virtual {v0, v5}, Lcom/jess/ui/TwoWayAbsListView;->post(Ljava/lang/Runnable;)Z

    .line 4029
    :cond_17
    iget-object v0, p0, Lcom/jess/ui/r;->d:Lcom/jess/ui/TwoWayAbsListView;

    iput v7, v0, Lcom/jess/ui/TwoWayAbsListView;->w:I

    goto/16 :goto_4

    .line 4032
    :pswitch_6
    iget-object v0, p0, Lcom/jess/ui/r;->d:Lcom/jess/ui/TwoWayAbsListView;

    invoke-virtual {v0}, Lcom/jess/ui/TwoWayAbsListView;->getChildCount()I

    move-result v0

    .line 4033
    if-lez v0, :cond_1b

    .line 4034
    iget-object v3, p0, Lcom/jess/ui/r;->d:Lcom/jess/ui/TwoWayAbsListView;

    iget v3, v3, Lcom/jess/ui/TwoWayAbsListView;->H:I

    if-nez v3, :cond_18

    iget-object v3, p0, Lcom/jess/ui/r;->d:Lcom/jess/ui/TwoWayAbsListView;

    invoke-virtual {v3, v1}, Lcom/jess/ui/TwoWayAbsListView;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->getTop()I

    move-result v3

    iget-object v4, p0, Lcom/jess/ui/r;->d:Lcom/jess/ui/TwoWayAbsListView;

    iget-object v4, v4, Lcom/jess/ui/TwoWayAbsListView;->m:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->top:I

    if-lt v3, v4, :cond_18

    iget-object v3, p0, Lcom/jess/ui/r;->d:Lcom/jess/ui/TwoWayAbsListView;

    iget v3, v3, Lcom/jess/ui/TwoWayAbsListView;->H:I

    add-int/2addr v3, v0

    iget-object v4, p0, Lcom/jess/ui/r;->d:Lcom/jess/ui/TwoWayAbsListView;

    iget v4, v4, Lcom/jess/ui/TwoWayAbsListView;->aa:I

    if-ge v3, v4, :cond_18

    iget-object v3, p0, Lcom/jess/ui/r;->d:Lcom/jess/ui/TwoWayAbsListView;

    add-int/lit8 v0, v0, -0x1

    .line 4036
    invoke-virtual {v3, v0}, Lcom/jess/ui/TwoWayAbsListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getBottom()I

    move-result v0

    iget-object v3, p0, Lcom/jess/ui/r;->d:Lcom/jess/ui/TwoWayAbsListView;

    .line 4037
    invoke-virtual {v3}, Lcom/jess/ui/TwoWayAbsListView;->getHeight()I

    move-result v3

    iget-object v4, p0, Lcom/jess/ui/r;->d:Lcom/jess/ui/TwoWayAbsListView;

    iget-object v4, v4, Lcom/jess/ui/TwoWayAbsListView;->m:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->bottom:I

    sub-int/2addr v3, v4

    if-gt v0, v3, :cond_18

    .line 4038
    iget-object v0, p0, Lcom/jess/ui/r;->d:Lcom/jess/ui/TwoWayAbsListView;

    iput v7, v0, Lcom/jess/ui/TwoWayAbsListView;->w:I

    .line 4039
    invoke-virtual {p0, v1}, Lcom/jess/ui/r;->b(I)V

    goto/16 :goto_4

    .line 4041
    :cond_18
    iget-object v0, p0, Lcom/jess/ui/r;->d:Lcom/jess/ui/TwoWayAbsListView;

    invoke-static {v0}, Lcom/jess/ui/TwoWayAbsListView;->i(Lcom/jess/ui/TwoWayAbsListView;)Landroid/view/VelocityTracker;

    move-result-object v0

    .line 4042
    const/16 v3, 0x3e8

    invoke-virtual {v0, v3}, Landroid/view/VelocityTracker;->computeCurrentVelocity(I)V

    .line 4043
    invoke-virtual {v0}, Landroid/view/VelocityTracker;->getYVelocity()F

    move-result v0

    float-to-int v0, v0

    .line 4045
    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v3

    iget-object v4, p0, Lcom/jess/ui/r;->d:Lcom/jess/ui/TwoWayAbsListView;

    invoke-static {v4}, Lcom/jess/ui/TwoWayAbsListView;->l(Lcom/jess/ui/TwoWayAbsListView;)I

    move-result v4

    if-le v3, v4, :cond_1a

    .line 4046
    iget-object v3, p0, Lcom/jess/ui/r;->f:Lcom/jess/ui/p;

    if-nez v3, :cond_19

    .line 4047
    new-instance v3, Lcom/jess/ui/t;

    invoke-direct {v3, p0, v8}, Lcom/jess/ui/t;-><init>(Lcom/jess/ui/r;Lcom/jess/ui/a;)V

    iput-object v3, p0, Lcom/jess/ui/r;->f:Lcom/jess/ui/p;

    .line 4049
    :cond_19
    const/4 v3, 0x2

    invoke-virtual {p0, v3}, Lcom/jess/ui/r;->b(I)V

    .line 4051
    iget-object v3, p0, Lcom/jess/ui/r;->f:Lcom/jess/ui/p;

    neg-int v0, v0

    invoke-virtual {v3, v0}, Lcom/jess/ui/p;->a(I)V

    goto/16 :goto_4

    .line 4053
    :cond_1a
    iget-object v0, p0, Lcom/jess/ui/r;->d:Lcom/jess/ui/TwoWayAbsListView;

    iput v7, v0, Lcom/jess/ui/TwoWayAbsListView;->w:I

    .line 4054
    invoke-virtual {p0, v1}, Lcom/jess/ui/r;->b(I)V

    goto/16 :goto_4

    .line 4058
    :cond_1b
    iget-object v0, p0, Lcom/jess/ui/r;->d:Lcom/jess/ui/TwoWayAbsListView;

    iput v7, v0, Lcom/jess/ui/TwoWayAbsListView;->w:I

    .line 4059
    invoke-virtual {p0, v1}, Lcom/jess/ui/r;->b(I)V

    goto/16 :goto_4

    .line 4091
    :pswitch_7
    iget-object v0, p0, Lcom/jess/ui/r;->d:Lcom/jess/ui/TwoWayAbsListView;

    iput v7, v0, Lcom/jess/ui/TwoWayAbsListView;->w:I

    .line 4092
    iget-object v0, p0, Lcom/jess/ui/r;->d:Lcom/jess/ui/TwoWayAbsListView;

    invoke-virtual {v0, v1}, Lcom/jess/ui/TwoWayAbsListView;->setPressed(Z)V

    .line 4093
    iget-object v0, p0, Lcom/jess/ui/r;->d:Lcom/jess/ui/TwoWayAbsListView;

    iget-object v3, p0, Lcom/jess/ui/r;->d:Lcom/jess/ui/TwoWayAbsListView;

    iget v3, v3, Lcom/jess/ui/TwoWayAbsListView;->t:I

    iget-object v4, p0, Lcom/jess/ui/r;->d:Lcom/jess/ui/TwoWayAbsListView;

    iget v4, v4, Lcom/jess/ui/TwoWayAbsListView;->H:I

    sub-int/2addr v3, v4

    invoke-virtual {v0, v3}, Lcom/jess/ui/TwoWayAbsListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 4094
    if-eqz v0, :cond_1c

    .line 4095
    invoke-virtual {v0, v1}, Landroid/view/View;->setPressed(Z)V

    .line 4097
    :cond_1c
    invoke-virtual {p0}, Lcom/jess/ui/r;->c()V

    .line 4099
    iget-object v0, p0, Lcom/jess/ui/r;->d:Lcom/jess/ui/TwoWayAbsListView;

    invoke-virtual {v0}, Lcom/jess/ui/TwoWayAbsListView;->getHandler()Landroid/os/Handler;

    move-result-object v0

    .line 4100
    if-eqz v0, :cond_1d

    .line 4101
    iget-object v1, p0, Lcom/jess/ui/r;->d:Lcom/jess/ui/TwoWayAbsListView;

    invoke-static {v1}, Lcom/jess/ui/TwoWayAbsListView;->c(Lcom/jess/ui/TwoWayAbsListView;)Lcom/jess/ui/c;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 4104
    :cond_1d
    iget-object v0, p0, Lcom/jess/ui/r;->d:Lcom/jess/ui/TwoWayAbsListView;

    invoke-static {v0}, Lcom/jess/ui/TwoWayAbsListView;->i(Lcom/jess/ui/TwoWayAbsListView;)Landroid/view/VelocityTracker;

    move-result-object v0

    if-eqz v0, :cond_1e

    .line 4105
    iget-object v0, p0, Lcom/jess/ui/r;->d:Lcom/jess/ui/TwoWayAbsListView;

    invoke-static {v0}, Lcom/jess/ui/TwoWayAbsListView;->i(Lcom/jess/ui/TwoWayAbsListView;)Landroid/view/VelocityTracker;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->recycle()V

    .line 4106
    iget-object v0, p0, Lcom/jess/ui/r;->d:Lcom/jess/ui/TwoWayAbsListView;

    invoke-static {v0, v8}, Lcom/jess/ui/TwoWayAbsListView;->a(Lcom/jess/ui/TwoWayAbsListView;Landroid/view/VelocityTracker;)Landroid/view/VelocityTracker;

    .line 4109
    :cond_1e
    iget-object v0, p0, Lcom/jess/ui/r;->d:Lcom/jess/ui/TwoWayAbsListView;

    invoke-static {v0, v7}, Lcom/jess/ui/TwoWayAbsListView;->c(Lcom/jess/ui/TwoWayAbsListView;I)I

    goto/16 :goto_1

    :cond_1f
    move v0, v3

    goto/16 :goto_2

    .line 3869
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_4
        :pswitch_1
        :pswitch_7
    .end packed-switch

    .line 3919
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_3
    .end packed-switch

    .line 3969
    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method
