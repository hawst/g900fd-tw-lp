.class Lcom/jess/ui/b;
.super Lcom/jess/ui/u;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/jess/ui/TwoWayAbsListView;


# direct methods
.method private constructor <init>(Lcom/jess/ui/TwoWayAbsListView;)V
    .locals 1

    .prologue
    .line 1893
    iput-object p1, p0, Lcom/jess/ui/b;->a:Lcom/jess/ui/TwoWayAbsListView;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/jess/ui/u;-><init>(Lcom/jess/ui/TwoWayAbsListView;Lcom/jess/ui/a;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/jess/ui/TwoWayAbsListView;Lcom/jess/ui/a;)V
    .locals 0

    .prologue
    .line 1893
    invoke-direct {p0, p1}, Lcom/jess/ui/b;-><init>(Lcom/jess/ui/TwoWayAbsListView;)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 1895
    iget-object v0, p0, Lcom/jess/ui/b;->a:Lcom/jess/ui/TwoWayAbsListView;

    invoke-virtual {v0}, Lcom/jess/ui/TwoWayAbsListView;->isPressed()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/jess/ui/b;->a:Lcom/jess/ui/TwoWayAbsListView;

    iget v0, v0, Lcom/jess/ui/TwoWayAbsListView;->V:I

    if-ltz v0, :cond_0

    .line 1896
    iget-object v0, p0, Lcom/jess/ui/b;->a:Lcom/jess/ui/TwoWayAbsListView;

    iget v0, v0, Lcom/jess/ui/TwoWayAbsListView;->V:I

    iget-object v2, p0, Lcom/jess/ui/b;->a:Lcom/jess/ui/TwoWayAbsListView;

    iget v2, v2, Lcom/jess/ui/TwoWayAbsListView;->H:I

    sub-int/2addr v0, v2

    .line 1897
    iget-object v2, p0, Lcom/jess/ui/b;->a:Lcom/jess/ui/TwoWayAbsListView;

    invoke-virtual {v2, v0}, Lcom/jess/ui/TwoWayAbsListView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 1899
    iget-object v0, p0, Lcom/jess/ui/b;->a:Lcom/jess/ui/TwoWayAbsListView;

    iget-boolean v0, v0, Lcom/jess/ui/TwoWayAbsListView;->S:Z

    if-nez v0, :cond_1

    .line 1901
    invoke-virtual {p0}, Lcom/jess/ui/b;->b()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1902
    iget-object v0, p0, Lcom/jess/ui/b;->a:Lcom/jess/ui/TwoWayAbsListView;

    iget-object v3, p0, Lcom/jess/ui/b;->a:Lcom/jess/ui/TwoWayAbsListView;

    iget v3, v3, Lcom/jess/ui/TwoWayAbsListView;->V:I

    iget-object v4, p0, Lcom/jess/ui/b;->a:Lcom/jess/ui/TwoWayAbsListView;

    iget-wide v4, v4, Lcom/jess/ui/TwoWayAbsListView;->W:J

    invoke-static {v0, v2, v3, v4, v5}, Lcom/jess/ui/TwoWayAbsListView;->a(Lcom/jess/ui/TwoWayAbsListView;Landroid/view/View;IJ)Z

    move-result v0

    .line 1904
    :goto_0
    if-eqz v0, :cond_0

    .line 1905
    iget-object v0, p0, Lcom/jess/ui/b;->a:Lcom/jess/ui/TwoWayAbsListView;

    invoke-virtual {v0, v1}, Lcom/jess/ui/TwoWayAbsListView;->setPressed(Z)V

    .line 1906
    invoke-virtual {v2, v1}, Landroid/view/View;->setPressed(Z)V

    .line 1913
    :cond_0
    :goto_1
    return-void

    .line 1909
    :cond_1
    iget-object v0, p0, Lcom/jess/ui/b;->a:Lcom/jess/ui/TwoWayAbsListView;

    invoke-virtual {v0, v1}, Lcom/jess/ui/TwoWayAbsListView;->setPressed(Z)V

    .line 1910
    if-eqz v2, :cond_0

    invoke-virtual {v2, v1}, Landroid/view/View;->setPressed(Z)V

    goto :goto_1

    :cond_2
    move v0, v1

    goto :goto_0
.end method
