.class Lcom/peel/backup/s;
.super Lcom/peel/util/t;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/peel/util/t",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Landroid/os/Bundle;

.field final synthetic b:Lcom/peel/util/t;

.field final synthetic c:Lcom/peel/backup/c;


# direct methods
.method constructor <init>(Lcom/peel/backup/c;Landroid/os/Bundle;Lcom/peel/util/t;)V
    .locals 0

    .prologue
    .line 968
    iput-object p1, p0, Lcom/peel/backup/s;->c:Lcom/peel/backup/c;

    iput-object p2, p0, Lcom/peel/backup/s;->a:Landroid/os/Bundle;

    iput-object p3, p0, Lcom/peel/backup/s;->b:Lcom/peel/util/t;

    invoke-direct {p0}, Lcom/peel/util/t;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(ZLjava/lang/Object;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 968
    check-cast p2, Ljava/lang/String;

    invoke-virtual {p0, p1, p2, p3}, Lcom/peel/backup/s;->a(ZLjava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public a(ZLjava/lang/String;Ljava/lang/String;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 971
    if-eqz p1, :cond_2

    .line 973
    :try_start_0
    new-instance v1, Lorg/json/JSONArray;

    invoke-direct {v1, p2}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 975
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1}, Lorg/json/JSONArray;->length()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 976
    invoke-virtual {v1, v0}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v2

    .line 978
    const-string/jumbo v3, "countryCode"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/peel/backup/s;->a:Landroid/os/Bundle;

    const-string/jumbo v5, "iso"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 979
    iget-object v0, p0, Lcom/peel/backup/s;->a:Landroid/os/Bundle;

    const-string/jumbo v1, "name"

    const-string/jumbo v3, "countryName"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 980
    iget-object v0, p0, Lcom/peel/backup/s;->a:Landroid/os/Bundle;

    const-string/jumbo v1, "type"

    const-string/jumbo v3, "providers_support_type"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 985
    :cond_0
    iget-object v0, p0, Lcom/peel/backup/s;->b:Lcom/peel/util/t;

    iget-object v1, p0, Lcom/peel/backup/s;->a:Landroid/os/Bundle;

    const/4 v2, 0x0

    invoke-virtual {v0, p1, v1, v2}, Lcom/peel/util/t;->a(ZLjava/lang/Object;Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 993
    :goto_1
    return-void

    .line 975
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 987
    :catch_0
    move-exception v0

    .line 988
    invoke-static {}, Lcom/peel/backup/c;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {}, Lcom/peel/backup/c;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 992
    :cond_2
    iget-object v0, p0, Lcom/peel/backup/s;->b:Lcom/peel/util/t;

    invoke-virtual {v0, p1, v6, v6}, Lcom/peel/util/t;->a(ZLjava/lang/Object;Ljava/lang/String;)V

    goto :goto_1
.end method
