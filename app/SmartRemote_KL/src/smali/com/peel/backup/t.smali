.class Lcom/peel/backup/t;
.super Lcom/peel/util/t;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/peel/util/t",
        "<",
        "Landroid/os/Bundle;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/peel/util/t;

.field final synthetic b:Ljava/lang/String;

.field final synthetic c:Ljava/lang/String;

.field final synthetic d:Lorg/json/JSONObject;

.field final synthetic e:Landroid/os/Bundle;

.field final synthetic f:Lcom/peel/backup/c;


# direct methods
.method constructor <init>(Lcom/peel/backup/c;Lcom/peel/util/t;Ljava/lang/String;Ljava/lang/String;Lorg/json/JSONObject;Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 1009
    iput-object p1, p0, Lcom/peel/backup/t;->f:Lcom/peel/backup/c;

    iput-object p2, p0, Lcom/peel/backup/t;->a:Lcom/peel/util/t;

    iput-object p3, p0, Lcom/peel/backup/t;->b:Ljava/lang/String;

    iput-object p4, p0, Lcom/peel/backup/t;->c:Ljava/lang/String;

    iput-object p5, p0, Lcom/peel/backup/t;->d:Lorg/json/JSONObject;

    iput-object p6, p0, Lcom/peel/backup/t;->e:Landroid/os/Bundle;

    invoke-direct {p0}, Lcom/peel/util/t;-><init>()V

    return-void
.end method


# virtual methods
.method public a(ZLandroid/os/Bundle;Ljava/lang/String;)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/4 v3, 0x0

    .line 1012
    if-nez p1, :cond_1

    .line 1013
    iget-object v0, p0, Lcom/peel/backup/t;->a:Lcom/peel/util/t;

    invoke-virtual {v0, p1, v1, v1}, Lcom/peel/util/t;->a(ZLjava/lang/Object;Ljava/lang/String;)V

    .line 1087
    :cond_0
    :goto_0
    return-void

    .line 1017
    :cond_1
    iget-object v0, p0, Lcom/peel/backup/t;->f:Lcom/peel/backup/c;

    invoke-static {v0}, Lcom/peel/backup/c;->b(Lcom/peel/backup/c;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 1018
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v1, "config_legacy"

    .line 1019
    invoke-static {p2}, Lcom/peel/util/bx;->b(Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v1, "country_ISO"

    iget-object v2, p0, Lcom/peel/backup/t;->b:Ljava/lang/String;

    .line 1020
    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 1021
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 1023
    const-string/jumbo v0, "US"

    iget-object v1, p0, Lcom/peel/backup/t;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1024
    sput-boolean v3, Lcom/peel/content/a;->d:Z

    .line 1031
    :goto_1
    sget-object v0, Lcom/peel/b/a;->a:Ljava/util/Map;

    iget-object v1, p0, Lcom/peel/backup/t;->f:Lcom/peel/backup/c;

    iget-object v2, p0, Lcom/peel/backup/t;->c:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/peel/backup/c;->a(Lcom/peel/backup/c;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    sput-object v0, Lcom/peel/b/a;->e:Ljava/lang/String;

    .line 1032
    sget-object v0, Lcom/peel/b/a;->e:Ljava/lang/String;

    invoke-static {v0}, Lcom/peel/content/a/j;->a(Ljava/lang/String;)V

    .line 1033
    iget-object v0, p0, Lcom/peel/backup/t;->b:Ljava/lang/String;

    sput-object v0, Lcom/peel/content/a;->e:Ljava/lang/String;

    .line 1036
    :try_start_0
    new-instance v0, Lcom/peel/content/user/User;

    iget-object v1, p0, Lcom/peel/backup/t;->d:Lorg/json/JSONObject;

    const-string/jumbo v2, "id"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/peel/backup/t;->e:Landroid/os/Bundle;

    invoke-direct {v0, v1, v2}, Lcom/peel/content/user/User;-><init>(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 1038
    invoke-static {}, Lcom/peel/data/m;->a()Lcom/peel/data/m;

    move-result-object v1

    invoke-virtual {v0}, Lcom/peel/content/user/User;->x()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "legacy"

    invoke-virtual {v0}, Lcom/peel/content/user/User;->t()Landroid/os/Bundle;

    move-result-object v4

    invoke-virtual {v1, v2, v3, v4}, Lcom/peel/data/m;->a(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 1039
    sput-object v0, Lcom/peel/content/a;->f:Lcom/peel/content/user/User;

    .line 1041
    iget-object v1, p0, Lcom/peel/backup/t;->c:Ljava/lang/String;

    const-string/jumbo v2, "XX"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 1043
    const-string/jumbo v1, "program"

    new-instance v2, Lcom/peel/backup/u;

    invoke-direct {v2, p0, v0}, Lcom/peel/backup/u;-><init>(Lcom/peel/backup/t;Lcom/peel/content/user/User;)V

    invoke-static {v1, v2}, Lcom/peel/content/a;->b(Ljava/lang/String;Lcom/peel/util/t;)V

    .line 1061
    const-string/jumbo v1, "sports"

    new-instance v2, Lcom/peel/backup/v;

    invoke-direct {v2, p0, v0}, Lcom/peel/backup/v;-><init>(Lcom/peel/backup/t;Lcom/peel/content/user/User;)V

    invoke-static {v1, v2}, Lcom/peel/content/a;->b(Ljava/lang/String;Lcom/peel/util/t;)V

    .line 1073
    iget-object v1, p0, Lcom/peel/backup/t;->d:Lorg/json/JSONObject;

    invoke-virtual {v0, v1}, Lcom/peel/content/user/User;->a(Lorg/json/JSONObject;)V

    .line 1076
    :cond_2
    iget-object v0, p0, Lcom/peel/backup/t;->d:Lorg/json/JSONObject;

    const-string/jumbo v1, "rooms"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1077
    iget-object v0, p0, Lcom/peel/backup/t;->c:Ljava/lang/String;

    const-string/jumbo v1, "XX"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1078
    iget-object v0, p0, Lcom/peel/backup/t;->f:Lcom/peel/backup/c;

    iget-object v1, p0, Lcom/peel/backup/t;->a:Lcom/peel/util/t;

    invoke-static {v0, v1}, Lcom/peel/backup/c;->a(Lcom/peel/backup/c;Lcom/peel/util/t;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 1083
    :catch_0
    move-exception v0

    .line 1084
    iget-object v1, p0, Lcom/peel/backup/t;->f:Lcom/peel/backup/c;

    iget-object v2, p0, Lcom/peel/backup/t;->a:Lcom/peel/util/t;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v2, v0}, Lcom/peel/backup/c;->a(Lcom/peel/backup/c;Lcom/peel/util/t;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1025
    :cond_3
    const-string/jumbo v0, "CA"

    iget-object v1, p0, Lcom/peel/backup/t;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1026
    sput-boolean v3, Lcom/peel/content/a;->d:Z

    goto/16 :goto_1

    .line 1028
    :cond_4
    sput-boolean v3, Lcom/peel/content/a;->d:Z

    goto/16 :goto_1

    .line 1080
    :cond_5
    :try_start_1
    iget-object v0, p0, Lcom/peel/backup/t;->f:Lcom/peel/backup/c;

    iget-object v1, p0, Lcom/peel/backup/t;->d:Lorg/json/JSONObject;

    const-string/jumbo v2, "rooms"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v1

    iget-object v2, p0, Lcom/peel/backup/t;->c:Ljava/lang/String;

    iget-object v3, p0, Lcom/peel/backup/t;->a:Lcom/peel/util/t;

    invoke-static {v0, v1, v2, v3}, Lcom/peel/backup/c;->a(Lcom/peel/backup/c;Lorg/json/JSONArray;Ljava/lang/String;Lcom/peel/util/t;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0
.end method

.method public bridge synthetic a(ZLjava/lang/Object;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1009
    check-cast p2, Landroid/os/Bundle;

    invoke-virtual {p0, p1, p2, p3}, Lcom/peel/backup/t;->a(ZLandroid/os/Bundle;Ljava/lang/String;)V

    return-void
.end method
