.class Lcom/peel/backup/ad;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/peel/control/RoomControl;

.field final synthetic b:Ljava/util/Map;

.field final synthetic c:Ljava/util/Map;

.field final synthetic d:Lcom/peel/backup/c;


# direct methods
.method constructor <init>(Lcom/peel/backup/c;Lcom/peel/control/RoomControl;Ljava/util/Map;Ljava/util/Map;)V
    .locals 0

    .prologue
    .line 470
    iput-object p1, p0, Lcom/peel/backup/ad;->d:Lcom/peel/backup/c;

    iput-object p2, p0, Lcom/peel/backup/ad;->a:Lcom/peel/control/RoomControl;

    iput-object p3, p0, Lcom/peel/backup/ad;->b:Ljava/util/Map;

    iput-object p4, p0, Lcom/peel/backup/ad;->c:Ljava/util/Map;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 473
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/peel/backup/c;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/backup/ad;->d:Lcom/peel/backup/c;

    invoke-static {v1}, Lcom/peel/backup/c;->a(Lcom/peel/backup/c;)Landroid/content/SharedPreferences;

    move-result-object v1

    const-string/jumbo v2, "device_profile_id"

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "/room/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/backup/ad;->a:Lcom/peel/control/RoomControl;

    invoke-virtual {v1}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/data/at;->g()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/backup/ad;->b:Ljava/util/Map;

    iget-object v2, p0, Lcom/peel/backup/ad;->c:Ljava/util/Map;

    const/4 v3, 0x1

    new-instance v4, Lcom/peel/backup/ae;

    invoke-direct {v4, p0}, Lcom/peel/backup/ae;-><init>(Lcom/peel/backup/ad;)V

    invoke-static {v0, v1, v2, v3, v4}, Lcom/peel/util/b/a;->b(Ljava/lang/String;Ljava/util/Map;Ljava/util/Map;ZLcom/peel/util/t;)V

    .line 480
    return-void
.end method
