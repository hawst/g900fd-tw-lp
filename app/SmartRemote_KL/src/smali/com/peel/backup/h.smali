.class Lcom/peel/backup/h;
.super Lcom/peel/util/t;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/peel/util/t",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/peel/control/RoomControl;

.field final synthetic b:Lcom/peel/content/library/LiveLibrary;

.field final synthetic c:Lcom/peel/backup/g;


# direct methods
.method constructor <init>(Lcom/peel/backup/g;Lcom/peel/control/RoomControl;Lcom/peel/content/library/LiveLibrary;)V
    .locals 0

    .prologue
    .line 148
    iput-object p1, p0, Lcom/peel/backup/h;->c:Lcom/peel/backup/g;

    iput-object p2, p0, Lcom/peel/backup/h;->a:Lcom/peel/control/RoomControl;

    iput-object p3, p0, Lcom/peel/backup/h;->b:Lcom/peel/content/library/LiveLibrary;

    invoke-direct {p0}, Lcom/peel/util/t;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(ZLjava/lang/Object;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 148
    check-cast p2, Ljava/lang/String;

    invoke-virtual {p0, p1, p2, p3}, Lcom/peel/backup/h;->a(ZLjava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public a(ZLjava/lang/String;Ljava/lang/String;)V
    .locals 12

    .prologue
    const/4 v2, 0x0

    .line 151
    if-eqz p1, :cond_2

    .line 152
    iget-object v0, p0, Lcom/peel/backup/h;->a:Lcom/peel/control/RoomControl;

    invoke-virtual {v0}, Lcom/peel/control/RoomControl;->c()[Lcom/peel/control/a;

    move-result-object v4

    .line 153
    if-eqz v4, :cond_2

    .line 154
    array-length v5, v4

    move v3, v2

    :goto_0
    if-ge v3, v5, :cond_2

    aget-object v6, v4, v3

    .line 155
    invoke-virtual {v6}, Lcom/peel/control/a;->e()[Lcom/peel/control/h;

    move-result-object v7

    .line 156
    array-length v8, v7

    move v1, v2

    :goto_1
    if-ge v1, v8, :cond_1

    aget-object v9, v7, v1

    .line 157
    iget-object v0, p0, Lcom/peel/backup/h;->c:Lcom/peel/backup/g;

    iget-object v0, v0, Lcom/peel/backup/g;->b:Lcom/peel/backup/f;

    iget-object v0, v0, Lcom/peel/backup/f;->a:Lcom/peel/backup/e;

    iget-object v0, v0, Lcom/peel/backup/e;->a:Lcom/peel/backup/d;

    iget-object v10, v0, Lcom/peel/backup/d;->a:Lcom/peel/backup/c;

    iget-object v11, p0, Lcom/peel/backup/h;->a:Lcom/peel/control/RoomControl;

    iget-object v0, p0, Lcom/peel/backup/h;->b:Lcom/peel/content/library/LiveLibrary;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_2
    invoke-virtual {v10, v11, v0, v6, v9}, Lcom/peel/backup/c;->a(Lcom/peel/control/RoomControl;Ljava/lang/String;Lcom/peel/control/a;Lcom/peel/control/h;)V

    .line 156
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 157
    :cond_0
    iget-object v0, p0, Lcom/peel/backup/h;->b:Lcom/peel/content/library/LiveLibrary;

    invoke-virtual {v0}, Lcom/peel/content/library/LiveLibrary;->d()Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    .line 154
    :cond_1
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    .line 162
    :cond_2
    return-void
.end method
