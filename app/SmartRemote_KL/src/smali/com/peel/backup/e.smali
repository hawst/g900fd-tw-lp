.class Lcom/peel/backup/e;
.super Lcom/peel/util/t;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/peel/util/t",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/peel/backup/d;


# direct methods
.method constructor <init>(Lcom/peel/backup/d;)V
    .locals 0

    .prologue
    .line 106
    iput-object p1, p0, Lcom/peel/backup/e;->a:Lcom/peel/backup/d;

    invoke-direct {p0}, Lcom/peel/util/t;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(ZLjava/lang/Object;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 106
    check-cast p2, Ljava/lang/String;

    invoke-virtual {p0, p1, p2, p3}, Lcom/peel/backup/e;->a(ZLjava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public a(ZLjava/lang/String;Ljava/lang/String;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 109
    if-eqz p1, :cond_0

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 176
    :cond_0
    :goto_0
    return-void

    .line 112
    :cond_1
    sget-object v1, Lcom/peel/content/a;->f:Lcom/peel/content/user/User;

    invoke-virtual {v1}, Lcom/peel/content/user/User;->x()Ljava/lang/String;

    move-result-object v1

    .line 116
    :try_start_0
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2, p2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 117
    const-string/jumbo v3, "mobileDeviceProfiles"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 118
    const-string/jumbo v3, "mobileDeviceProfiles"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v2

    .line 119
    :goto_1
    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result v3

    if-ge v0, v3, :cond_3

    .line 120
    invoke-virtual {v2, v0}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v3

    .line 122
    const-string/jumbo v4, "legacyUserID"

    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 123
    iget-object v4, p0, Lcom/peel/backup/e;->a:Lcom/peel/backup/d;

    iget-object v4, v4, Lcom/peel/backup/d;->a:Lcom/peel/backup/c;

    const-string/jumbo v5, "ID"

    invoke-virtual {v3, v5}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const/4 v5, 0x0

    invoke-virtual {v4, v3, v5}, Lcom/peel/backup/c;->a(Ljava/lang/String;Lcom/peel/util/t;)V

    .line 119
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 128
    :cond_3
    iget-object v0, p0, Lcom/peel/backup/e;->a:Lcom/peel/backup/d;

    iget-object v0, v0, Lcom/peel/backup/d;->a:Lcom/peel/backup/c;

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    new-instance v4, Lcom/peel/backup/f;

    invoke-direct {v4, p0}, Lcom/peel/backup/f;-><init>(Lcom/peel/backup/e;)V

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/peel/backup/c;->a(ZLjava/lang/String;Ljava/lang/String;Lcom/peel/util/t;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 173
    :catch_0
    move-exception v0

    .line 174
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method
