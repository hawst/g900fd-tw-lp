.class Lcom/peel/backup/y;
.super Lcom/peel/util/t;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/peel/util/t",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Ljava/util/Map;

.field final synthetic b:Ljava/lang/String;

.field final synthetic c:Lcom/peel/backup/MobileDeviceProfile;

.field final synthetic d:Ljava/util/ArrayList;

.field final synthetic e:Ljava/util/concurrent/atomic/AtomicInteger;

.field final synthetic f:Ljava/util/ArrayList;

.field final synthetic g:Lcom/peel/backup/x;


# direct methods
.method constructor <init>(Lcom/peel/backup/x;ILjava/util/Map;Ljava/lang/String;Lcom/peel/backup/MobileDeviceProfile;Ljava/util/ArrayList;Ljava/util/concurrent/atomic/AtomicInteger;Ljava/util/ArrayList;)V
    .locals 0

    .prologue
    .line 267
    iput-object p1, p0, Lcom/peel/backup/y;->g:Lcom/peel/backup/x;

    iput-object p3, p0, Lcom/peel/backup/y;->a:Ljava/util/Map;

    iput-object p4, p0, Lcom/peel/backup/y;->b:Ljava/lang/String;

    iput-object p5, p0, Lcom/peel/backup/y;->c:Lcom/peel/backup/MobileDeviceProfile;

    iput-object p6, p0, Lcom/peel/backup/y;->d:Ljava/util/ArrayList;

    iput-object p7, p0, Lcom/peel/backup/y;->e:Ljava/util/concurrent/atomic/AtomicInteger;

    iput-object p8, p0, Lcom/peel/backup/y;->f:Ljava/util/ArrayList;

    invoke-direct {p0, p2}, Lcom/peel/util/t;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(ZLjava/lang/Object;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 267
    check-cast p2, Ljava/lang/String;

    invoke-virtual {p0, p1, p2, p3}, Lcom/peel/backup/y;->a(ZLjava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public a(ZLjava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 269
    if-eqz p1, :cond_0

    .line 273
    :try_start_0
    const-string/jumbo v0, "null"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 274
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0, p2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 275
    const-string/jumbo v1, "users"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    .line 277
    const-string/jumbo v1, "id"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 278
    iget-object v1, p0, Lcom/peel/backup/y;->a:Ljava/util/Map;

    iget-object v2, p0, Lcom/peel/backup/y;->b:Ljava/lang/String;

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 279
    iget-object v1, p0, Lcom/peel/backup/y;->c:Lcom/peel/backup/MobileDeviceProfile;

    invoke-virtual {v1, v0}, Lcom/peel/backup/MobileDeviceProfile;->a(Ljava/lang/String;)V

    .line 281
    iget-object v1, p0, Lcom/peel/backup/y;->c:Lcom/peel/backup/MobileDeviceProfile;

    invoke-virtual {v1}, Lcom/peel/backup/MobileDeviceProfile;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 282
    iget-object v0, p0, Lcom/peel/backup/y;->d:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/peel/backup/y;->c:Lcom/peel/backup/MobileDeviceProfile;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 298
    :cond_0
    iget-object v0, p0, Lcom/peel/backup/y;->e:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    move-result v0

    iget-object v1, p0, Lcom/peel/backup/y;->f:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ne v0, v1, :cond_1

    .line 299
    iget-object v0, p0, Lcom/peel/backup/y;->g:Lcom/peel/backup/x;

    iget-object v0, v0, Lcom/peel/backup/x;->a:Lcom/peel/backup/w;

    iget-object v1, v0, Lcom/peel/backup/w;->a:Lcom/peel/util/t;

    const/4 v2, 0x1

    iget-object v0, p0, Lcom/peel/backup/y;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_2

    iget-object v0, p0, Lcom/peel/backup/y;->d:Ljava/util/ArrayList;

    :goto_0
    invoke-virtual {v1, v2, v0, v3}, Lcom/peel/util/t;->a(ZLjava/lang/Object;Ljava/lang/String;)V

    .line 303
    :cond_1
    :goto_1
    return-void

    .line 292
    :catch_0
    move-exception v0

    .line 293
    iget-object v1, p0, Lcom/peel/backup/y;->g:Lcom/peel/backup/x;

    iget-object v1, v1, Lcom/peel/backup/x;->a:Lcom/peel/backup/w;

    iget-object v1, v1, Lcom/peel/backup/w;->a:Lcom/peel/util/t;

    const/4 v2, 0x0

    invoke-virtual {v0}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v3, v0}, Lcom/peel/util/t;->a(ZLjava/lang/Object;Ljava/lang/String;)V

    goto :goto_1

    .line 299
    :cond_2
    iget-object v0, p0, Lcom/peel/backup/y;->f:Ljava/util/ArrayList;

    goto :goto_0
.end method
