.class public Lcom/peel/backup/as;
.super Ljava/lang/Object;


# instance fields
.field private a:Ljava/lang/String;

.field private b:Ljava/lang/String;

.field private c:[Lcom/peel/backup/a;

.field private d:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/peel/backup/am;",
            ">;"
        }
    .end annotation
.end field

.field private e:I

.field private f:Lcom/peel/control/RoomControl;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    const/4 v0, 0x0

    iput v0, p0, Lcom/peel/backup/as;->e:I

    return-void
.end method


# virtual methods
.method public a(Lcom/peel/control/RoomControl;)V
    .locals 0

    .prologue
    .line 75
    iput-object p1, p0, Lcom/peel/backup/as;->f:Lcom/peel/control/RoomControl;

    .line 76
    return-void
.end method

.method public a(Lorg/json/JSONObject;)V
    .locals 5

    .prologue
    .line 38
    :try_start_0
    const-string/jumbo v0, "name"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/backup/as;->a:Ljava/lang/String;

    .line 39
    const-string/jumbo v0, "ID"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/backup/as;->b:Ljava/lang/String;

    .line 40
    const-string/jumbo v0, "controlData"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 64
    :cond_0
    :goto_0
    return-void

    .line 41
    :cond_1
    new-instance v0, Lcom/peel/backup/ar;

    invoke-direct {v0}, Lcom/peel/backup/ar;-><init>()V

    const-string/jumbo v1, "controlData"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/peel/backup/ar;->a(Ljava/lang/String;)[Lcom/peel/backup/a;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/backup/as;->c:[Lcom/peel/backup/a;

    .line 44
    const-string/jumbo v0, "devices"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 45
    const-string/jumbo v0, "devices"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v1

    .line 47
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lorg/json/JSONArray;->length()I

    move-result v0

    if-lez v0, :cond_0

    .line 48
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/peel/backup/as;->d:Ljava/util/Map;

    .line 50
    const/4 v0, 0x0

    :goto_1
    invoke-virtual {v1}, Lorg/json/JSONArray;->length()I

    move-result v2

    if-ge v0, v2, :cond_2

    .line 51
    invoke-virtual {v1, v0}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v2

    .line 52
    new-instance v3, Lcom/peel/backup/am;

    invoke-direct {v3}, Lcom/peel/backup/am;-><init>()V

    .line 53
    invoke-virtual {v3, v2}, Lcom/peel/backup/am;->a(Lorg/json/JSONObject;)Lcom/peel/control/h;

    move-result-object v2

    .line 55
    iget-object v4, p0, Lcom/peel/backup/as;->d:Ljava/util/Map;

    invoke-virtual {v2}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v2

    invoke-virtual {v2}, Lcom/peel/data/g;->b()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v4, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 50
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 58
    :cond_2
    iget v0, p0, Lcom/peel/backup/as;->e:I

    iget-object v1, p0, Lcom/peel/backup/as;->d:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->size()I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/peel/backup/as;->e:I
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 61
    :catch_0
    move-exception v0

    .line 62
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method

.method public a()[Lcom/peel/backup/a;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lcom/peel/backup/as;->c:[Lcom/peel/backup/a;

    return-object v0
.end method

.method public b()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/peel/backup/am;",
            ">;"
        }
    .end annotation

    .prologue
    .line 29
    iget-object v0, p0, Lcom/peel/backup/as;->d:Ljava/util/Map;

    return-object v0
.end method

.method public c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/peel/backup/as;->b:Ljava/lang/String;

    return-object v0
.end method

.method public d()I
    .locals 1

    .prologue
    .line 67
    iget v0, p0, Lcom/peel/backup/as;->e:I

    return v0
.end method

.method public e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/peel/backup/as;->a:Ljava/lang/String;

    return-object v0
.end method

.method public f()Lcom/peel/control/RoomControl;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/peel/backup/as;->f:Lcom/peel/control/RoomControl;

    return-object v0
.end method
