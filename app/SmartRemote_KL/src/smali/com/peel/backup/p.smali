.class Lcom/peel/backup/p;
.super Lcom/peel/util/t;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/peel/util/t",
        "<",
        "Ljava/util/Map",
        "<",
        "Ljava/lang/String;",
        "Ljava/util/Map",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Object;",
        ">;>;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/peel/backup/o;


# direct methods
.method constructor <init>(Lcom/peel/backup/o;)V
    .locals 0

    .prologue
    .line 766
    iput-object p1, p0, Lcom/peel/backup/p;->a:Lcom/peel/backup/o;

    invoke-direct {p0}, Lcom/peel/util/t;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(ZLjava/lang/Object;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 766
    check-cast p2, Ljava/util/Map;

    invoke-virtual {p0, p1, p2, p3}, Lcom/peel/backup/p;->a(ZLjava/util/Map;Ljava/lang/String;)V

    return-void
.end method

.method public a(ZLjava/util/Map;Ljava/lang/String;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;>;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 769
    if-eqz p1, :cond_0

    .line 770
    iget-object v0, p0, Lcom/peel/backup/p;->a:Lcom/peel/backup/o;

    iget-object v0, v0, Lcom/peel/backup/o;->b:Ljava/util/Map;

    iget-object v1, p0, Lcom/peel/backup/p;->a:Lcom/peel/backup/o;

    iget-object v1, v1, Lcom/peel/backup/o;->c:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/backup/am;

    invoke-virtual {v0}, Lcom/peel/backup/am;->b()Lcom/peel/control/h;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/backup/p;->a:Lcom/peel/backup/o;

    iget v1, v1, Lcom/peel/backup/o;->a:I

    invoke-virtual {v0, v1, p2}, Lcom/peel/data/g;->a(ILjava/util/Map;)V

    .line 773
    :cond_0
    iget-object v0, p0, Lcom/peel/backup/p;->a:Lcom/peel/backup/o;

    iget-object v0, v0, Lcom/peel/backup/o;->d:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    move-result v0

    iget-object v1, p0, Lcom/peel/backup/p;->a:Lcom/peel/backup/o;

    iget-object v1, v1, Lcom/peel/backup/o;->b:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->size()I

    move-result v1

    if-ne v0, v1, :cond_1

    .line 774
    iget-object v0, p0, Lcom/peel/backup/p;->a:Lcom/peel/backup/o;

    iget-object v0, v0, Lcom/peel/backup/o;->e:Lcom/peel/util/t;

    const/4 v1, 0x1

    invoke-virtual {v0, v1, v2, v2}, Lcom/peel/util/t;->a(ZLjava/lang/Object;Ljava/lang/String;)V

    .line 776
    :cond_1
    return-void
.end method
