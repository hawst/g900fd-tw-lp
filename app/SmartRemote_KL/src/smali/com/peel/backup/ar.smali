.class public Lcom/peel/backup/ar;
.super Ljava/lang/Object;


# instance fields
.field private a:[Lcom/peel/control/a;

.field private b:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    return-void
.end method

.method public constructor <init>([Lcom/peel/control/a;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput-object p1, p0, Lcom/peel/backup/ar;->a:[Lcom/peel/control/a;

    .line 21
    iput-object p2, p0, Lcom/peel/backup/ar;->b:Ljava/lang/String;

    .line 22
    return-void
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 21

    .prologue
    .line 89
    const/4 v3, 0x0

    .line 92
    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/peel/backup/ar;->a:[Lcom/peel/control/a;

    if-eqz v2, :cond_8

    .line 93
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2}, Lorg/json/JSONObject;-><init>()V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 94
    :try_start_1
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/peel/backup/ar;->b:Ljava/lang/String;

    if-eqz v3, :cond_0

    .line 95
    const-string/jumbo v3, "providerName"

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/peel/backup/ar;->b:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 98
    :cond_0
    new-instance v6, Lorg/json/JSONArray;

    invoke-direct {v6}, Lorg/json/JSONArray;-><init>()V

    .line 99
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/peel/backup/ar;->a:[Lcom/peel/control/a;

    array-length v8, v7

    const/4 v3, 0x0

    move v5, v3

    :goto_0
    if-ge v5, v8, :cond_6

    aget-object v9, v7, v5

    .line 100
    new-instance v10, Lorg/json/JSONObject;

    invoke-direct {v10}, Lorg/json/JSONObject;-><init>()V

    .line 102
    const-string/jumbo v3, "id"

    invoke-virtual {v9}, Lcom/peel/control/a;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v10, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 103
    const-string/jumbo v3, "name"

    invoke-virtual {v9}, Lcom/peel/control/a;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v10, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 105
    invoke-virtual {v9}, Lcom/peel/control/a;->e()[Lcom/peel/control/h;

    move-result-object v11

    .line 106
    if-eqz v11, :cond_5

    .line 107
    new-instance v12, Lorg/json/JSONArray;

    invoke-direct {v12}, Lorg/json/JSONArray;-><init>()V

    .line 109
    array-length v13, v11

    const/4 v3, 0x0

    move v4, v3

    :goto_1
    if-ge v4, v13, :cond_4

    aget-object v14, v11, v4

    .line 110
    new-instance v15, Lorg/json/JSONObject;

    invoke-direct {v15}, Lorg/json/JSONObject;-><init>()V

    .line 111
    const-string/jumbo v3, "id"

    invoke-virtual {v14}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Lcom/peel/data/g;->b()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v15, v3, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 112
    invoke-virtual {v9, v14}, Lcom/peel/control/a;->b(Lcom/peel/control/h;)[Ljava/lang/Integer;

    move-result-object v16

    .line 114
    if-eqz v16, :cond_2

    move-object/from16 v0, v16

    array-length v3, v0

    if-lez v3, :cond_2

    .line 115
    new-instance v17, Lorg/json/JSONArray;

    invoke-direct/range {v17 .. v17}, Lorg/json/JSONArray;-><init>()V

    .line 117
    move-object/from16 v0, v16

    array-length v0, v0

    move/from16 v18, v0

    const/4 v3, 0x0

    :goto_2
    move/from16 v0, v18

    if-ge v3, v0, :cond_1

    aget-object v19, v16, v3

    .line 118
    move-object/from16 v0, v17

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    .line 117
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 120
    :cond_1
    const-string/jumbo v3, "mode"

    move-object/from16 v0, v17

    invoke-virtual {v15, v3, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 123
    :cond_2
    invoke-virtual {v9, v14}, Lcom/peel/control/a;->a(Lcom/peel/control/h;)Ljava/lang/String;

    move-result-object v3

    .line 125
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 126
    const-string/jumbo v3, "input"

    invoke-virtual {v9, v14}, Lcom/peel/control/a;->a(Lcom/peel/control/h;)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v15, v3, v14}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 129
    :cond_3
    invoke-virtual {v12, v15}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    .line 109
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    goto :goto_1

    .line 132
    :cond_4
    const-string/jumbo v3, "devices"

    invoke-virtual {v10, v3, v12}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 135
    :cond_5
    invoke-virtual {v6, v10}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    .line 99
    add-int/lit8 v3, v5, 0x1

    move v5, v3

    goto/16 :goto_0

    .line 138
    :cond_6
    const-string/jumbo v3, "activities"

    invoke-virtual {v2, v3, v6}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1

    .line 144
    :goto_3
    if-nez v2, :cond_7

    const/4 v2, 0x0

    :goto_4
    return-object v2

    .line 140
    :catch_0
    move-exception v2

    .line 141
    :goto_5
    invoke-virtual {v2}, Lorg/json/JSONException;->printStackTrace()V

    move-object v2, v3

    goto :goto_3

    .line 144
    :cond_7
    invoke-virtual {v2}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_4

    .line 140
    :catch_1
    move-exception v3

    move-object/from16 v20, v3

    move-object v3, v2

    move-object/from16 v2, v20

    goto :goto_5

    :cond_8
    move-object v2, v3

    goto :goto_3
.end method

.method public a(Ljava/lang/String;)[Lcom/peel/backup/a;
    .locals 13

    .prologue
    .line 33
    const/4 v0, 0x0

    .line 36
    :try_start_0
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 38
    const-string/jumbo v2, "providerName"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 39
    const-string/jumbo v2, "providerName"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/peel/backup/ar;->b:Ljava/lang/String;

    .line 42
    :cond_0
    const-string/jumbo v2, "activities"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 43
    const-string/jumbo v2, "activities"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v6

    .line 45
    if-eqz v6, :cond_4

    invoke-virtual {v6}, Lorg/json/JSONArray;->length()I

    move-result v1

    if-lez v1, :cond_4

    .line 46
    invoke-virtual {v6}, Lorg/json/JSONArray;->length()I

    move-result v1

    new-array v0, v1, [Lcom/peel/backup/a;

    .line 48
    const/4 v1, 0x0

    move v5, v1

    :goto_0
    invoke-virtual {v6}, Lorg/json/JSONArray;->length()I

    move-result v1

    if-ge v5, v1, :cond_4

    .line 49
    invoke-virtual {v6, v5}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v7

    .line 50
    const-string/jumbo v1, "devices"

    invoke-virtual {v7, v1}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v8

    .line 51
    invoke-virtual {v8}, Lorg/json/JSONArray;->length()I

    move-result v1

    new-array v9, v1, [Lcom/peel/backup/b;

    .line 53
    const/4 v1, 0x0

    move v4, v1

    :goto_1
    invoke-virtual {v8}, Lorg/json/JSONArray;->length()I

    move-result v1

    if-ge v4, v1, :cond_3

    .line 54
    const/4 v2, 0x0

    .line 55
    const/4 v1, 0x0

    .line 57
    invoke-virtual {v8, v4}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v10

    .line 59
    const-string/jumbo v3, "mode"

    invoke-virtual {v10, v3}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 60
    const-string/jumbo v3, "mode"

    invoke-virtual {v10, v3}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v11

    .line 62
    if-eqz v11, :cond_1

    invoke-virtual {v11}, Lorg/json/JSONArray;->length()I

    move-result v3

    if-lez v3, :cond_1

    .line 63
    invoke-virtual {v11}, Lorg/json/JSONArray;->length()I

    move-result v2

    new-array v2, v2, [Ljava/lang/Integer;

    .line 65
    const/4 v3, 0x0

    :goto_2
    invoke-virtual {v11}, Lorg/json/JSONArray;->length()I

    move-result v12

    if-ge v3, v12, :cond_1

    .line 66
    invoke-virtual {v11, v3}, Lorg/json/JSONArray;->getInt(I)I

    move-result v12

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v2, v3

    .line 65
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 71
    :cond_1
    const-string/jumbo v3, "input"

    invoke-virtual {v10, v3}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 72
    const-string/jumbo v1, "input"

    invoke-virtual {v10, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 74
    :cond_2
    new-instance v3, Lcom/peel/backup/b;

    const-string/jumbo v11, "id"

    invoke-virtual {v10, v11}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-direct {v3, v10, v2, v1}, Lcom/peel/backup/b;-><init>(Ljava/lang/String;[Ljava/lang/Integer;Ljava/lang/String;)V

    aput-object v3, v9, v4

    .line 53
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    goto :goto_1

    .line 77
    :cond_3
    new-instance v1, Lcom/peel/backup/a;

    const-string/jumbo v2, "id"

    invoke-virtual {v7, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "name"

    invoke-virtual {v7, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3, v9}, Lcom/peel/backup/a;-><init>(Ljava/lang/String;Ljava/lang/String;[Lcom/peel/backup/b;)V

    aput-object v1, v0, v5
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 48
    add-int/lit8 v1, v5, 0x1

    move v5, v1

    goto/16 :goto_0

    .line 81
    :catch_0
    move-exception v1

    .line 82
    invoke-virtual {v1}, Lorg/json/JSONException;->printStackTrace()V

    .line 85
    :cond_4
    return-object v0
.end method
