.class public Lcom/peel/backup/an;
.super Lcom/peel/d/t;

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private al:Landroid/widget/TextView;

.field private am:Landroid/widget/ListView;

.field private an:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/peel/backup/MobileDeviceProfile;",
            ">;"
        }
    .end annotation
.end field

.field private ao:I

.field private ap:Landroid/view/LayoutInflater;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/peel/d/t;-><init>()V

    .line 29
    const/4 v0, 0x0

    iput v0, p0, Lcom/peel/backup/an;->ao:I

    return-void
.end method

.method static synthetic a(Lcom/peel/backup/an;I)I
    .locals 0

    .prologue
    .line 25
    iput p1, p0, Lcom/peel/backup/an;->ao:I

    return p1
.end method

.method static synthetic a(Lcom/peel/backup/an;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lcom/peel/backup/an;->an:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic b(Lcom/peel/backup/an;)Landroid/view/LayoutInflater;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lcom/peel/backup/an;->ap:Landroid/view/LayoutInflater;

    return-object v0
.end method

.method static synthetic c(Lcom/peel/backup/an;)I
    .locals 1

    .prologue
    .line 25
    iget v0, p0, Lcom/peel/backup/an;->ao:I

    return v0
.end method

.method static synthetic d(Lcom/peel/backup/an;)Landroid/widget/ListView;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lcom/peel/backup/an;->am:Landroid/widget/ListView;

    return-object v0
.end method


# virtual methods
.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 40
    sget v0, Lcom/peel/ui/fq;->device_import_options:I

    invoke-virtual {p1, v0, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 41
    sget v0, Lcom/peel/ui/fp;->import_header:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/peel/backup/an;->al:Landroid/widget/TextView;

    .line 42
    sget v0, Lcom/peel/ui/fp;->import_list:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/peel/backup/an;->am:Landroid/widget/ListView;

    .line 43
    sget v0, Lcom/peel/ui/fp;->skip_import:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 44
    sget v0, Lcom/peel/ui/fp;->import_continue:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 46
    iput-object p1, p0, Lcom/peel/backup/an;->ap:Landroid/view/LayoutInflater;

    .line 48
    invoke-virtual {p0, v2}, Lcom/peel/backup/an;->b(Z)V

    .line 49
    invoke-virtual {p0}, Lcom/peel/backup/an;->j_()Landroid/app/Dialog;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/app/Dialog;->setCanceledOnTouchOutside(Z)V

    .line 51
    return-object v1
.end method

.method public a_(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 34
    invoke-super {p0, p1}, Lcom/peel/d/t;->a_(Landroid/os/Bundle;)V

    .line 35
    const/4 v0, 0x0

    const v1, 0x1030071

    invoke-virtual {p0, v0, v1}, Lcom/peel/backup/an;->a(II)V

    .line 36
    return-void
.end method

.method public d(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 103
    invoke-super {p0, p1}, Lcom/peel/d/t;->d(Landroid/os/Bundle;)V

    .line 105
    iget-object v0, p0, Lcom/peel/backup/an;->ak:Landroid/os/Bundle;

    const-string/jumbo v1, "mobile_devices"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/backup/an;->an:Ljava/util/ArrayList;

    .line 107
    iget-object v0, p0, Lcom/peel/backup/an;->an:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 108
    iget-object v0, p0, Lcom/peel/backup/an;->al:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/peel/backup/an;->n()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/peel/ui/fs;->import_header_labels:I

    iget-object v3, p0, Lcom/peel/backup/an;->an:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getQuantityString(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 110
    iget-object v0, p0, Lcom/peel/backup/an;->am:Landroid/widget/ListView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setVisibility(I)V

    .line 111
    iget-object v0, p0, Lcom/peel/backup/an;->am:Landroid/widget/ListView;

    new-instance v1, Lcom/peel/backup/ao;

    invoke-direct {v1, p0}, Lcom/peel/backup/ao;-><init>(Lcom/peel/backup/an;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 160
    iget-object v0, p0, Lcom/peel/backup/an;->am:Landroid/widget/ListView;

    new-instance v1, Lcom/peel/backup/ap;

    invoke-direct {v1, p0}, Lcom/peel/backup/ap;-><init>(Lcom/peel/backup/an;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 168
    :cond_0
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 9

    .prologue
    const/16 v3, 0x7d5

    const/4 v1, 0x1

    const/4 v5, 0x0

    const/4 v8, -0x1

    const/4 v4, 0x0

    .line 56
    new-instance v2, Lcom/peel/backup/c;

    invoke-virtual {p0}, Lcom/peel/backup/an;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/ae;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/peel/backup/c;-><init>(Landroid/content/Context;)V

    .line 58
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    .line 59
    sget v6, Lcom/peel/ui/fp;->skip_import:I

    if-ne v0, v6, :cond_5

    .line 60
    iget-object v0, p0, Lcom/peel/backup/an;->an:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_0
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/backup/MobileDeviceProfile;

    .line 61
    invoke-virtual {v0}, Lcom/peel/backup/MobileDeviceProfile;->e()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0}, Lcom/peel/backup/MobileDeviceProfile;->d()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 62
    invoke-virtual {v0}, Lcom/peel/backup/MobileDeviceProfile;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0, v4}, Lcom/peel/backup/c;->a(Ljava/lang/String;Lcom/peel/util/t;)V

    goto :goto_0

    .line 66
    :cond_1
    invoke-virtual {p0}, Lcom/peel/backup/an;->k()Landroid/support/v4/app/Fragment;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/peel/backup/an;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/peel/backup/an;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/ae;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_2

    .line 67
    invoke-virtual {p0}, Lcom/peel/backup/an;->k()Landroid/support/v4/app/Fragment;

    move-result-object v0

    invoke-virtual {p0}, Lcom/peel/backup/an;->l()I

    move-result v2

    invoke-virtual {v0, v2, v8, v4}, Landroid/support/v4/app/Fragment;->a(IILandroid/content/Intent;)V

    .line 69
    :cond_2
    invoke-static {}, Lcom/peel/util/a/f;->a()Lcom/peel/util/a/f;

    move-result-object v2

    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v0}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v0

    if-nez v0, :cond_4

    move v0, v1

    :goto_1
    const/16 v1, 0xbf8

    invoke-virtual {v2, v0, v1, v3}, Lcom/peel/util/a/f;->a(III)V

    .line 70
    invoke-virtual {p0}, Lcom/peel/backup/an;->i_()V

    .line 99
    :cond_3
    :goto_2
    return-void

    .line 69
    :cond_4
    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v0}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/data/at;->f()I

    move-result v0

    goto :goto_1

    .line 72
    :cond_5
    sget v2, Lcom/peel/ui/fp;->import_continue:I

    if-ne v0, v2, :cond_3

    .line 73
    iget-object v0, p0, Lcom/peel/backup/an;->an:Ljava/util/ArrayList;

    iget v2, p0, Lcom/peel/backup/an;->ao:I

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/peel/backup/MobileDeviceProfile;

    .line 75
    invoke-virtual {p0}, Lcom/peel/backup/an;->k()Landroid/support/v4/app/Fragment;

    move-result-object v0

    if-eqz v0, :cond_6

    invoke-virtual {p0}, Lcom/peel/backup/an;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    if-eqz v0, :cond_6

    invoke-virtual {p0}, Lcom/peel/backup/an;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/ae;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_6

    .line 76
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 77
    const-string/jumbo v2, "mobile_info"

    invoke-virtual {v0, v2, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 79
    invoke-virtual {p0}, Lcom/peel/backup/an;->k()Landroid/support/v4/app/Fragment;

    move-result-object v2

    invoke-virtual {p0}, Lcom/peel/backup/an;->l()I

    move-result v7

    invoke-virtual {v2, v7, v8, v0}, Landroid/support/v4/app/Fragment;->a(IILandroid/content/Intent;)V

    .line 84
    :cond_6
    invoke-virtual {p0}, Lcom/peel/backup/an;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    invoke-static {v0}, Lcom/peel/social/w;->g(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 85
    const-string/jumbo v4, "Google"

    .line 92
    :cond_7
    :goto_3
    invoke-static {}, Lcom/peel/util/a/f;->a()Lcom/peel/util/a/f;

    move-result-object v0

    sget-object v2, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v2}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v2

    if-nez v2, :cond_a

    :goto_4
    const/16 v2, 0xbf7

    .line 94
    invoke-virtual {v6}, Lcom/peel/backup/MobileDeviceProfile;->d()Ljava/lang/String;

    move-result-object v6

    move v7, v5

    .line 92
    invoke-virtual/range {v0 .. v7}, Lcom/peel/util/a/f;->a(IIILjava/lang/String;ILjava/lang/String;I)V

    .line 96
    invoke-virtual {p0}, Lcom/peel/backup/an;->i_()V

    goto :goto_2

    .line 86
    :cond_8
    invoke-virtual {p0}, Lcom/peel/backup/an;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    invoke-static {v0}, Lcom/peel/social/w;->h(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 87
    const-string/jumbo v4, "Facebook"

    goto :goto_3

    .line 88
    :cond_9
    invoke-virtual {p0}, Lcom/peel/backup/an;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    invoke-static {v0}, Lcom/peel/social/w;->i(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 89
    const-string/jumbo v4, "Samsung"

    goto :goto_3

    .line 92
    :cond_a
    sget-object v1, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v1}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/data/at;->f()I

    move-result v1

    goto :goto_4
.end method
