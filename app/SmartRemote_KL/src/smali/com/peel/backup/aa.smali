.class Lcom/peel/backup/aa;
.super Lcom/peel/util/t;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/peel/util/t",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/peel/backup/z;


# direct methods
.method constructor <init>(Lcom/peel/backup/z;)V
    .locals 0

    .prologue
    .line 368
    iput-object p1, p0, Lcom/peel/backup/aa;->a:Lcom/peel/backup/z;

    invoke-direct {p0}, Lcom/peel/util/t;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(ZLjava/lang/Object;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 368
    check-cast p2, Ljava/lang/String;

    invoke-virtual {p0, p1, p2, p3}, Lcom/peel/backup/aa;->a(ZLjava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public a(ZLjava/lang/String;Ljava/lang/String;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 371
    if-eqz p1, :cond_2

    .line 373
    :try_start_0
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0, p2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 374
    const-string/jumbo v1, "ID"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 375
    iget-object v1, p0, Lcom/peel/backup/aa;->a:Lcom/peel/backup/z;

    iget-object v1, v1, Lcom/peel/backup/z;->e:Lcom/peel/backup/c;

    invoke-static {v1}, Lcom/peel/backup/c;->a(Lcom/peel/backup/c;)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string/jumbo v2, "device_profile_id"

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 376
    iget-object v0, p0, Lcom/peel/backup/aa;->a:Lcom/peel/backup/z;

    iget-boolean v0, v0, Lcom/peel/backup/z;->b:Z

    if-eqz v0, :cond_0

    .line 377
    iget-object v0, p0, Lcom/peel/backup/aa;->a:Lcom/peel/backup/z;

    iget-object v0, v0, Lcom/peel/backup/z;->e:Lcom/peel/backup/c;

    sget-object v1, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v1}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v1

    iget-object v2, p0, Lcom/peel/backup/aa;->a:Lcom/peel/backup/z;

    iget-object v2, v2, Lcom/peel/backup/z;->c:Ljava/lang/String;

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lcom/peel/backup/c;->a(Lcom/peel/control/RoomControl;Ljava/lang/String;Lcom/peel/util/t;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 387
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/peel/backup/aa;->a:Lcom/peel/backup/z;

    iget-object v0, v0, Lcom/peel/backup/z;->d:Lcom/peel/util/t;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/peel/backup/aa;->a:Lcom/peel/backup/z;

    iget-boolean v0, v0, Lcom/peel/backup/z;->b:Z

    if-nez v0, :cond_1

    .line 388
    iget-object v0, p0, Lcom/peel/backup/aa;->a:Lcom/peel/backup/z;

    iget-object v0, v0, Lcom/peel/backup/z;->d:Lcom/peel/util/t;

    invoke-virtual {v0, p1, v4, v4}, Lcom/peel/util/t;->a(ZLjava/lang/Object;Ljava/lang/String;)V

    .line 390
    :cond_1
    return-void

    .line 379
    :catch_0
    move-exception v0

    .line 380
    invoke-static {}, Lcom/peel/backup/c;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {}, Lcom/peel/backup/c;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 383
    :cond_2
    invoke-static {}, Lcom/peel/backup/c;->b()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "saveDeviceProfile failed : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
