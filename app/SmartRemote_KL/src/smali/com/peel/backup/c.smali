.class public Lcom/peel/backup/c;
.super Ljava/lang/Object;


# static fields
.field public static final a:Ljava/lang/String;

.field public static final b:Ljava/lang/String;

.field public static c:Z

.field public static final d:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final e:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final f:Ljava/lang/String;


# instance fields
.field private g:Landroid/content/Context;

.field private h:Landroid/content/SharedPreferences;

.field private i:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/peel/backup/as;",
            ">;"
        }
    .end annotation
.end field

.field private j:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 53
    const-class v0, Lcom/peel/backup/c;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/peel/backup/c;->f:Ljava/lang/String;

    .line 55
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/peel/b/a;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "user/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/peel/backup/c;->a:Ljava/lang/String;

    .line 56
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/peel/b/a;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "deviceProfile"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/peel/backup/c;->b:Ljava/lang/String;

    .line 73
    const/4 v0, 0x0

    sput-boolean v0, Lcom/peel/backup/c;->c:Z

    .line 74
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/peel/backup/c;->d:Ljava/util/Map;

    .line 75
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/peel/backup/c;->e:Ljava/util/Map;

    .line 78
    sget-object v0, Lcom/peel/backup/c;->d:Ljava/util/Map;

    const-string/jumbo v1, "usa"

    const-string/jumbo v2, "https://epg-us-samsung.peel.com/"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 79
    sget-object v0, Lcom/peel/backup/c;->d:Ljava/util/Map;

    const-string/jumbo v1, "asia"

    const-string/jumbo v2, "https://epg-asia-samsung.peel.com/"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 80
    sget-object v0, Lcom/peel/backup/c;->d:Ljava/util/Map;

    const-string/jumbo v1, "europe"

    const-string/jumbo v2, "https://epg-eu-samsung.peel.com/"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 81
    sget-object v0, Lcom/peel/backup/c;->d:Ljava/util/Map;

    const-string/jumbo v1, "australia"

    const-string/jumbo v2, "https://epg-asia-samsung.peel.com/"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 82
    sget-object v0, Lcom/peel/backup/c;->d:Ljava/util/Map;

    const-string/jumbo v1, "bramex"

    const-string/jumbo v2, "https://epg-bramex-samsung.peel.com/"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 83
    sget-object v0, Lcom/peel/backup/c;->d:Ljava/util/Map;

    const-string/jumbo v1, "latin"

    const-string/jumbo v2, "https://epg-la-samsung.peel.com/"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 87
    sget-object v0, Lcom/peel/backup/c;->e:Ljava/util/Map;

    const-string/jumbo v1, "usa"

    const-string/jumbo v2, "http://staging.zelfy.com/"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 88
    sget-object v0, Lcom/peel/backup/c;->e:Ljava/util/Map;

    const-string/jumbo v1, "asia"

    const-string/jumbo v2, "http://staging.epg.asia.samsung.peel.com/"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 89
    sget-object v0, Lcom/peel/backup/c;->e:Ljava/util/Map;

    const-string/jumbo v1, "europe"

    const-string/jumbo v2, "http://test.epg.eu.samsung.peel.com/"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 90
    sget-object v0, Lcom/peel/backup/c;->e:Ljava/util/Map;

    const-string/jumbo v1, "australia"

    const-string/jumbo v2, "http://staging.epg.asia.samsung.peel.com/"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 91
    sget-object v0, Lcom/peel/backup/c;->e:Ljava/util/Map;

    const-string/jumbo v1, "bramex"

    const-string/jumbo v2, "http://test.epg.bramex.samsung.peel.com/"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 92
    sget-object v0, Lcom/peel/backup/c;->e:Ljava/util/Map;

    const-string/jumbo v1, "latin"

    const-string/jumbo v2, "http://test.epg.la.samsung.peel.com/"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 93
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 95
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 72
    iput v1, p0, Lcom/peel/backup/c;->j:I

    .line 96
    iput-object p1, p0, Lcom/peel/backup/c;->g:Landroid/content/Context;

    .line 98
    const-string/jumbo v0, "social_accounts_setup"

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/backup/c;->h:Landroid/content/SharedPreferences;

    .line 99
    return-void
.end method

.method static synthetic a(Lcom/peel/backup/c;)Landroid/content/SharedPreferences;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/peel/backup/c;->h:Landroid/content/SharedPreferences;

    return-object v0
.end method

.method static synthetic a(Lcom/peel/backup/c;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 52
    invoke-direct {p0, p1}, Lcom/peel/backup/c;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 338
    const-string/jumbo v0, "AS"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 339
    const-string/jumbo v0, "asia"

    .line 349
    :goto_0
    return-object v0

    .line 340
    :cond_0
    const-string/jumbo v0, "EU"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 341
    const-string/jumbo v0, "europe"

    goto :goto_0

    .line 342
    :cond_1
    const-string/jumbo v0, "AU"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 343
    const-string/jumbo v0, "australia"

    goto :goto_0

    .line 344
    :cond_2
    const-string/jumbo v0, "LA"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 345
    const-string/jumbo v0, "latin"

    goto :goto_0

    .line 346
    :cond_3
    const-string/jumbo v0, "BM"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 347
    const-string/jumbo v0, "bramex"

    goto :goto_0

    .line 349
    :cond_4
    const-string/jumbo v0, "usa"

    goto :goto_0
.end method

.method private a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;Lcom/peel/util/t;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/Bundle;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/peel/util/t",
            "<",
            "Landroid/os/Bundle;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 952
    const-string/jumbo v0, "XX"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 953
    const-string/jumbo v0, "name"

    iget-object v1, p0, Lcom/peel/backup/c;->g:Landroid/content/Context;

    sget v2, Lcom/peel/ui/ft;->other_countries:I

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 954
    const-string/jumbo v0, "iso"

    const-string/jumbo v1, "XX"

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 955
    const-string/jumbo v0, "type"

    const-string/jumbo v1, "none"

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 957
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p4, v0, p1, v1}, Lcom/peel/util/t;->a(ZLjava/lang/Object;Ljava/lang/String;)V

    .line 995
    :goto_0
    return-void

    .line 960
    :cond_0
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string/jumbo v0, "usca"

    invoke-virtual {p3, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 961
    :cond_1
    const-string/jumbo p3, "US"

    .line 964
    :cond_2
    const-string/jumbo v0, "iso"

    invoke-virtual {p1, v0, p3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 967
    const-string/jumbo v0, "endpoint"

    invoke-direct {p0, p2}, Lcom/peel/backup/c;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 968
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "http://countries.epg.samsung.peel.com/countries/all?langcode="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lcom/peel/backup/s;

    invoke-direct {v1, p0, p1, p4}, Lcom/peel/backup/s;-><init>(Lcom/peel/backup/c;Landroid/os/Bundle;Lcom/peel/util/t;)V

    invoke-static {v0, v1}, Lcom/peel/util/b/a;->b(Ljava/lang/String;Lcom/peel/util/t;)V

    goto :goto_0
.end method

.method static synthetic a(Lcom/peel/backup/c;Lcom/peel/control/RoomControl;Ljava/lang/String;Lcom/peel/control/a;Lcom/peel/control/h;)V
    .locals 0

    .prologue
    .line 52
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/peel/backup/c;->b(Lcom/peel/control/RoomControl;Ljava/lang/String;Lcom/peel/control/a;Lcom/peel/control/h;)V

    return-void
.end method

.method static synthetic a(Lcom/peel/backup/c;Lcom/peel/data/ContentRoom;Lcom/peel/util/t;)V
    .locals 0

    .prologue
    .line 52
    invoke-direct {p0, p1, p2}, Lcom/peel/backup/c;->a(Lcom/peel/data/ContentRoom;Lcom/peel/util/t;)V

    return-void
.end method

.method static synthetic a(Lcom/peel/backup/c;Lcom/peel/util/t;)V
    .locals 0

    .prologue
    .line 52
    invoke-direct {p0, p1}, Lcom/peel/backup/c;->b(Lcom/peel/util/t;)V

    return-void
.end method

.method static synthetic a(Lcom/peel/backup/c;Lcom/peel/util/t;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 52
    invoke-direct {p0, p1, p2}, Lcom/peel/backup/c;->a(Lcom/peel/util/t;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic a(Lcom/peel/backup/c;Ljava/util/Map;)V
    .locals 0

    .prologue
    .line 52
    invoke-direct {p0, p1}, Lcom/peel/backup/c;->a(Ljava/util/Map;)V

    return-void
.end method

.method static synthetic a(Lcom/peel/backup/c;Lorg/json/JSONArray;Ljava/lang/String;Lcom/peel/util/t;)V
    .locals 0

    .prologue
    .line 52
    invoke-direct {p0, p1, p2, p3}, Lcom/peel/backup/c;->a(Lorg/json/JSONArray;Ljava/lang/String;Lcom/peel/util/t;)V

    return-void
.end method

.method static synthetic a(Lcom/peel/backup/c;Lorg/json/JSONObject;Ljava/lang/String;Lcom/peel/util/t;)V
    .locals 0

    .prologue
    .line 52
    invoke-direct {p0, p1, p2, p3}, Lcom/peel/backup/c;->a(Lorg/json/JSONObject;Ljava/lang/String;Lcom/peel/util/t;)V

    return-void
.end method

.method private a(Lcom/peel/data/ContentRoom;Lcom/peel/util/t;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 941
    invoke-virtual {p1}, Lcom/peel/data/ContentRoom;->a()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lcom/peel/backup/r;

    invoke-direct {v1, p0, p2}, Lcom/peel/backup/r;-><init>(Lcom/peel/backup/c;Lcom/peel/util/t;)V

    invoke-static {v0, v2, v2, v1}, Lcom/peel/content/a;->a(Ljava/lang/String;ZZLcom/peel/util/t;)V

    .line 949
    return-void
.end method

.method private a(Lcom/peel/util/t;Ljava/lang/String;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 694
    iget-object v0, p0, Lcom/peel/backup/c;->h:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v1, "device_profile_id"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 695
    invoke-static {}, Lcom/peel/control/am;->b()V

    .line 696
    invoke-static {}, Lcom/peel/content/a;->b()V

    .line 697
    iget-object v0, p0, Lcom/peel/backup/c;->g:Landroid/content/Context;

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->clear()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 699
    const/4 v0, 0x0

    invoke-virtual {p1, v0, v2, v2}, Lcom/peel/util/t;->a(ZLjava/lang/Object;Ljava/lang/String;)V

    .line 701
    if-eqz p2, :cond_0

    .line 702
    sget-object v0, Lcom/peel/backup/c;->f:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "error msg : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 704
    :cond_0
    return-void
.end method

.method private a(Ljava/util/Map;)V
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/peel/backup/am;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 787
    iget-object v0, p0, Lcom/peel/backup/c;->i:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/backup/as;

    .line 788
    invoke-virtual {v0}, Lcom/peel/backup/as;->a()[Lcom/peel/backup/a;

    move-result-object v6

    .line 790
    if-eqz v6, :cond_0

    .line 791
    array-length v7, v6

    move v4, v3

    :goto_0
    if-ge v4, v7, :cond_0

    aget-object v1, v6, v4

    .line 792
    invoke-virtual {v1}, Lcom/peel/backup/a;->a()[Lcom/peel/backup/b;

    move-result-object v8

    .line 793
    new-instance v9, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v9, v3}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    .line 794
    invoke-virtual {v1}, Lcom/peel/backup/a;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1}, Lcom/peel/backup/a;->c()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/peel/control/a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/peel/control/a;

    move-result-object v10

    .line 796
    array-length v11, v8

    move v2, v3

    :goto_1
    if-ge v2, v11, :cond_3

    aget-object v12, v8, v2

    .line 797
    invoke-virtual {v12}, Lcom/peel/backup/b;->a()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/peel/backup/am;

    invoke-virtual {v1}, Lcom/peel/backup/am;->b()Lcom/peel/control/h;

    move-result-object v13

    invoke-virtual {v12}, Lcom/peel/backup/b;->c()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_2

    const/4 v1, 0x0

    :goto_2
    invoke-virtual {v12}, Lcom/peel/backup/b;->b()[Ljava/lang/Integer;

    move-result-object v12

    invoke-virtual {v10, v13, v1, v12}, Lcom/peel/control/a;->a(Lcom/peel/control/h;Ljava/lang/String;[Ljava/lang/Integer;)Z

    .line 798
    invoke-virtual {v9}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    move-result v1

    array-length v12, v8

    if-ne v1, v12, :cond_1

    .line 799
    invoke-virtual {v0}, Lcom/peel/backup/as;->f()Lcom/peel/control/RoomControl;

    move-result-object v1

    invoke-virtual {v1, v10}, Lcom/peel/control/RoomControl;->a(Lcom/peel/control/a;)V

    .line 800
    invoke-virtual {v0}, Lcom/peel/backup/as;->f()Lcom/peel/control/RoomControl;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/control/RoomControl;->h()V

    .line 796
    :cond_1
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_1

    .line 797
    :cond_2
    invoke-virtual {v12}, Lcom/peel/backup/b;->c()Ljava/lang/String;

    move-result-object v1

    goto :goto_2

    .line 791
    :cond_3
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    goto :goto_0

    .line 806
    :cond_4
    return-void
.end method

.method private a(Ljava/util/Map;Ljava/util/concurrent/atomic/AtomicInteger;Lcom/peel/util/t;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/peel/backup/am;",
            ">;",
            "Ljava/util/concurrent/atomic/AtomicInteger;",
            "Lcom/peel/util/t;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v10, 0x0

    .line 756
    if-eqz p1, :cond_2

    .line 757
    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_0
    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 758
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 759
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/backup/am;

    .line 760
    invoke-virtual {v0}, Lcom/peel/backup/am;->a()I

    move-result v2

    .line 762
    invoke-virtual {v0}, Lcom/peel/backup/am;->b()Lcom/peel/control/h;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/data/g;->e()I

    move-result v0

    if-nez v0, :cond_1

    .line 763
    const-class v0, Lcom/peel/backup/c;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v8

    const-string/jumbo v9, "get data"

    new-instance v0, Lcom/peel/backup/o;

    move-object v1, p0

    move-object v3, p1

    move-object v5, p2

    move-object v6, p3

    invoke-direct/range {v0 .. v6}, Lcom/peel/backup/o;-><init>(Lcom/peel/backup/c;ILjava/util/Map;Ljava/lang/String;Ljava/util/concurrent/atomic/AtomicInteger;Lcom/peel/util/t;)V

    invoke-static {v8, v9, v0}, Lcom/peel/util/i;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    goto :goto_0

    .line 780
    :cond_1
    invoke-virtual {p2}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    move-result v0

    invoke-interface {p1}, Ljava/util/Map;->size()I

    move-result v1

    if-ne v0, v1, :cond_0

    .line 781
    const/4 v0, 0x1

    invoke-virtual {p3, v0, v10, v10}, Lcom/peel/util/t;->a(ZLjava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 785
    :cond_2
    return-void
.end method

.method private a(Lorg/json/JSONArray;Ljava/lang/String;Lcom/peel/util/t;)V
    .locals 35

    .prologue
    .line 809
    sget-object v21, Lcom/peel/content/a;->f:Lcom/peel/content/user/User;

    .line 810
    const/16 v18, 0x0

    .line 811
    new-instance v22, Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v4, 0x0

    move-object/from16 v0, v22

    invoke-direct {v0, v4}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    .line 812
    new-instance v23, Ljava/util/HashMap;

    invoke-direct/range {v23 .. v23}, Ljava/util/HashMap;-><init>()V

    .line 813
    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    .line 815
    const/4 v4, 0x0

    move/from16 v19, v4

    :goto_0
    invoke-virtual/range {p1 .. p1}, Lorg/json/JSONArray;->length()I

    move-result v4

    move/from16 v0, v19

    if-ge v0, v4, :cond_f

    .line 819
    :try_start_0
    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v24

    .line 820
    const-string/jumbo v4, "roomid"

    move-object/from16 v0, v24

    invoke-virtual {v0, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    .line 822
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/peel/backup/c;->i:Ljava/util/Map;

    if-eqz v4, :cond_0

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/peel/backup/c;->i:Ljava/util/Map;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v4, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    if-nez v4, :cond_2

    :cond_0
    move-object/from16 v4, v18

    .line 815
    :cond_1
    :goto_1
    add-int/lit8 v5, v19, 0x1

    move/from16 v19, v5

    move-object/from16 v18, v4

    goto :goto_0

    .line 824
    :cond_2
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/peel/backup/c;->i:Ljava/util/Map;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v4, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    move-object v0, v4

    check-cast v0, Lcom/peel/backup/as;

    move-object/from16 v17, v0

    .line 825
    new-instance v25, Lcom/peel/control/RoomControl;

    invoke-virtual/range {v17 .. v17}, Lcom/peel/backup/as;->e()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v25

    invoke-direct {v0, v4}, Lcom/peel/control/RoomControl;-><init>(Ljava/lang/String;)V

    .line 826
    invoke-virtual/range {v25 .. v25}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v4

    invoke-virtual/range {v17 .. v17}, Lcom/peel/backup/as;->c()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Lcom/peel/data/at;->d(Ljava/lang/String;)V

    .line 827
    invoke-virtual/range {v25 .. v25}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v4

    invoke-virtual {v4, v5}, Lcom/peel/data/at;->a(I)V

    .line 829
    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-static {v4, v5}, Lcom/peel/control/o;->a(ILjava/lang/String;)Lcom/peel/control/o;

    move-result-object v4

    move-object/from16 v0, v25

    invoke-virtual {v0, v4}, Lcom/peel/control/RoomControl;->a(Lcom/peel/control/o;)V

    .line 830
    sget-object v4, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    move-object/from16 v0, v25

    invoke-virtual {v4, v0}, Lcom/peel/control/am;->a(Lcom/peel/control/RoomControl;)V

    .line 832
    new-instance v4, Lcom/peel/data/ContentRoom;

    invoke-virtual/range {v25 .. v25}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v5

    invoke-virtual {v5}, Lcom/peel/data/at;->b()Ljava/lang/String;

    move-result-object v5

    invoke-virtual/range {v25 .. v25}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v6

    invoke-virtual {v6}, Lcom/peel/data/at;->a()Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    const-string/jumbo v8, "roomid"

    move-object/from16 v0, v24

    invoke-virtual {v0, v8}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v8

    invoke-virtual/range {v25 .. v25}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v9

    invoke-virtual {v9}, Lcom/peel/data/at;->b()Ljava/lang/String;

    move-result-object v9

    invoke-direct/range {v4 .. v9}, Lcom/peel/data/ContentRoom;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;ILjava/lang/String;)V

    .line 834
    new-instance v5, Landroid/os/Bundle;

    invoke-direct {v5}, Landroid/os/Bundle;-><init>()V

    .line 835
    const-string/jumbo v6, "path"

    const-string/jumbo v7, "rooms/add"

    invoke-virtual {v5, v6, v7}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 836
    const-string/jumbo v6, "room"

    invoke-virtual {v5, v6, v4}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 837
    const/4 v6, 0x0

    move-object/from16 v0, v21

    invoke-virtual {v0, v5, v6}, Lcom/peel/content/user/User;->a(Landroid/os/Bundle;Lcom/peel/util/t;)V

    .line 839
    const-string/jumbo v5, "XX"

    move-object/from16 v0, p2

    invoke-virtual {v0, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_e

    .line 841
    const-string/jumbo v5, "name"

    move-object/from16 v0, v24

    invoke-virtual {v0, v5}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v26

    .line 842
    const-string/jumbo v5, "zipcode"

    move-object/from16 v0, v24

    invoke-virtual {v0, v5}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v27

    .line 844
    const-string/jumbo v5, "mso"

    move-object/from16 v0, v24

    invoke-virtual {v0, v5}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v28

    .line 847
    const-string/jumbo v5, "boxtype"

    move-object/from16 v0, v24

    invoke-virtual {v0, v5}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v29

    .line 849
    const-string/jumbo v5, "channels"

    move-object/from16 v0, v24

    invoke-virtual {v0, v5}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v30

    .line 851
    invoke-virtual/range {v30 .. v30}, Lorg/json/JSONArray;->length()I

    move-result v5

    new-array v0, v5, [Lcom/peel/data/Channel;

    move-object/from16 v31, v0

    .line 852
    new-instance v32, Ljava/util/ArrayList;

    invoke-direct/range {v32 .. v32}, Ljava/util/ArrayList;-><init>()V

    .line 854
    const-string/jumbo v5, "headendid"

    move-object/from16 v0, v24

    invoke-virtual {v0, v5}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v33

    .line 855
    const/4 v5, 0x0

    move/from16 v20, v5

    :goto_2
    invoke-virtual/range {v30 .. v30}, Lorg/json/JSONArray;->length()I

    move-result v5

    move/from16 v0, v20

    if-ge v0, v5, :cond_8

    .line 856
    move-object/from16 v0, v30

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v34

    .line 857
    const-string/jumbo v5, "callsign"

    move-object/from16 v0, v34

    invoke-virtual {v0, v5}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 858
    const-string/jumbo v5, "channelnumber"

    move-object/from16 v0, v34

    invoke-virtual {v0, v5}, Lorg/json/JSONObject;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    move-object v0, v5

    check-cast v0, Ljava/lang/String;

    move-object v6, v0

    .line 859
    new-instance v5, Lcom/peel/data/Channel;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v33

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, "^[0]*"

    const-string/jumbo v9, ""

    invoke-virtual {v6, v8, v9}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    const-string/jumbo v7, "callsign"

    move-object/from16 v0, v34

    invoke-virtual {v0, v7}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    const-string/jumbo v8, "prgsvcid"

    move-object/from16 v0, v34

    invoke-virtual {v0, v8}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    const-string/jumbo v9, "name"

    move-object/from16 v0, v34

    invoke-virtual {v0, v9}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    const-string/jumbo v10, "channelnumber"

    move-object/from16 v0, v34

    invoke-virtual {v0, v10}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    const-string/jumbo v11, "imageurl"

    .line 860
    move-object/from16 v0, v34

    invoke-virtual {v0, v11}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_4

    const-string/jumbo v11, "imageurl"

    move-object/from16 v0, v34

    invoke-virtual {v0, v11}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    :goto_3
    const-string/jumbo v12, "type"

    move-object/from16 v0, v34

    invoke-virtual {v0, v12}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    const-string/jumbo v13, "HD"

    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_5

    const/4 v12, 0x1

    :goto_4
    const-string/jumbo v13, "lang"

    move-object/from16 v0, v34

    invoke-virtual {v0, v13}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v13

    if-eqz v13, :cond_6

    const-string/jumbo v13, "lang"

    move-object/from16 v0, v34

    invoke-virtual {v0, v13}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    :goto_5
    const-string/jumbo v14, "tier"

    move-object/from16 v0, v34

    invoke-virtual {v0, v14}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    const-string/jumbo v15, ""

    const-string/jumbo v16, ""

    invoke-direct/range {v5 .. v16}, Lcom/peel/data/Channel;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 861
    const-string/jumbo v6, "userchannel"

    move-object/from16 v0, v34

    invoke-virtual {v0, v6}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    const-string/jumbo v7, "Y"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_7

    const/4 v6, 0x1

    :goto_6
    invoke-virtual {v5, v6}, Lcom/peel/data/Channel;->b(Z)V

    .line 863
    invoke-virtual {v5}, Lcom/peel/data/Channel;->l()Z

    move-result v6

    if-eqz v6, :cond_3

    .line 864
    invoke-virtual {v5}, Lcom/peel/data/Channel;->a()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, v32

    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 866
    :cond_3
    aput-object v5, v31, v20

    .line 855
    add-int/lit8 v5, v20, 0x1

    move/from16 v20, v5

    goto/16 :goto_2

    .line 860
    :cond_4
    const-string/jumbo v11, ""

    goto :goto_3

    :cond_5
    const/4 v12, 0x0

    goto :goto_4

    :cond_6
    const/4 v13, 0x0

    goto :goto_5

    .line 861
    :cond_7
    const/4 v6, 0x0

    goto :goto_6

    .line 869
    :cond_8
    new-instance v5, Lcom/peel/content/library/LiveLibrary;

    move-object/from16 v6, v33

    move-object/from16 v7, v26

    move-object/from16 v8, v27

    move-object/from16 v9, v28

    move-object/from16 v10, v29

    move-object/from16 v11, v31

    invoke-direct/range {v5 .. v11}, Lcom/peel/content/library/LiveLibrary;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Lcom/peel/data/Channel;)V

    .line 871
    invoke-virtual {v4}, Lcom/peel/data/ContentRoom;->a()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/peel/content/a;->a(Lcom/peel/content/library/Library;Ljava/lang/String;)V

    .line 872
    invoke-virtual/range {v21 .. v21}, Lcom/peel/content/user/User;->d()Landroid/os/Bundle;

    move-result-object v7

    .line 873
    const-string/jumbo v6, "langpreference"

    move-object/from16 v0, v24

    invoke-virtual {v0, v6}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 874
    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_a

    .line 875
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 876
    const-string/jumbo v9, ","

    invoke-virtual {v6, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    .line 878
    array-length v10, v9

    const/4 v6, 0x0

    :goto_7
    if-ge v6, v10, :cond_9

    aget-object v11, v9, v6

    .line 879
    invoke-virtual {v8, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 878
    add-int/lit8 v6, v6, 0x1

    goto :goto_7

    .line 881
    :cond_9
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4}, Lcom/peel/data/ContentRoom;->a()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v9, "/"

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v5}, Lcom/peel/content/library/LiveLibrary;->e()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v7, v6, v8}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 883
    :cond_a
    const-string/jumbo v6, "hdpreference"

    move-object/from16 v0, v24

    invoke-virtual {v0, v6}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 885
    invoke-virtual/range {v21 .. v21}, Lcom/peel/content/user/User;->c()Landroid/os/Bundle;

    move-result-object v7

    .line 886
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4}, Lcom/peel/data/ContentRoom;->a()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string/jumbo v9, "/"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v5}, Lcom/peel/content/library/LiveLibrary;->e()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_b

    .line 887
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4}, Lcom/peel/data/ContentRoom;->a()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string/jumbo v9, "/"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v5}, Lcom/peel/content/library/LiveLibrary;->e()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8, v6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 890
    :cond_b
    if-eqz v21, :cond_d

    if-eqz v31, :cond_d

    move-object/from16 v0, v31

    array-length v6, v0

    if-lez v6, :cond_d

    .line 891
    invoke-interface/range {v32 .. v32}, Ljava/util/List;->size()I

    move-result v6

    if-lez v6, :cond_d

    .line 892
    invoke-virtual/range {v21 .. v21}, Lcom/peel/content/user/User;->f()Landroid/os/Bundle;

    move-result-object v8

    .line 893
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4}, Lcom/peel/data/ContentRoom;->a()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, "/"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v5}, Lcom/peel/content/library/LiveLibrary;->e()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v8, v6}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v7

    .line 894
    if-nez v7, :cond_c

    .line 895
    new-instance v7, Ljava/util/ArrayList;

    invoke-interface/range {v32 .. v32}, Ljava/util/List;->size()I

    move-result v6

    invoke-direct {v7, v6}, Ljava/util/ArrayList;-><init>(I)V

    .line 896
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4}, Lcom/peel/data/ContentRoom;->a()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v9, "/"

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v5}, Lcom/peel/content/library/LiveLibrary;->e()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    move-object v0, v7

    check-cast v0, Ljava/util/ArrayList;

    move-object v6, v0

    invoke-virtual {v8, v9, v6}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 898
    :cond_c
    move-object/from16 v0, v32

    invoke-interface {v7, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 901
    :cond_d
    move-object/from16 v0, v21

    move-object/from16 v1, v24

    invoke-virtual {v0, v4, v5, v1}, Lcom/peel/content/user/User;->a(Lcom/peel/data/ContentRoom;Lcom/peel/content/library/Library;Lorg/json/JSONObject;)V

    .line 904
    :cond_e
    sget-object v5, Lcom/peel/content/a;->f:Lcom/peel/content/user/User;

    invoke-virtual {v5}, Lcom/peel/content/user/User;->u()V

    .line 905
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/peel/backup/c;->g:Landroid/content/Context;

    const/4 v6, 0x1

    invoke-static {v5, v6}, Lcom/peel/util/dq;->a(Landroid/content/Context;Z)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_1

    .line 906
    if-nez v19, :cond_11

    .line 910
    :goto_8
    :try_start_1
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/peel/backup/c;->i:Ljava/util/Map;

    if-eqz v5, :cond_1

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/peel/backup/c;->i:Ljava/util/Map;

    invoke-interface {v5}, Ljava/util/Map;->size()I

    move-result v5

    if-lez v5, :cond_1

    .line 911
    if-eqz v17, :cond_1

    .line 912
    move-object/from16 v0, v17

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/peel/backup/as;->a(Lcom/peel/control/RoomControl;)V

    .line 914
    invoke-virtual/range {v17 .. v17}, Lcom/peel/backup/as;->b()Ljava/util/Map;

    move-result-object v5

    if-eqz v5, :cond_1

    invoke-virtual/range {v17 .. v17}, Lcom/peel/backup/as;->b()Ljava/util/Map;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Map;->size()I

    move-result v5

    if-lez v5, :cond_1

    .line 915
    invoke-virtual/range {v17 .. v17}, Lcom/peel/backup/as;->b()Ljava/util/Map;

    move-result-object v5

    move-object/from16 v0, v23

    invoke-interface {v0, v5}, Ljava/util/Map;->putAll(Ljava/util/Map;)V
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_1

    .line 919
    :catch_0
    move-exception v5

    .line 920
    :goto_9
    invoke-virtual {v5}, Lorg/json/JSONException;->printStackTrace()V

    goto/16 :goto_1

    .line 924
    :cond_f
    if-eqz v18, :cond_10

    invoke-interface/range {v23 .. v23}, Ljava/util/Map;->size()I

    move-result v4

    if-nez v4, :cond_10

    .line 925
    move-object/from16 v0, p0

    move-object/from16 v1, v18

    move-object/from16 v2, p3

    invoke-direct {v0, v1, v2}, Lcom/peel/backup/c;->a(Lcom/peel/data/ContentRoom;Lcom/peel/util/t;)V

    .line 938
    :goto_a
    return-void

    .line 928
    :cond_10
    new-instance v4, Lcom/peel/backup/q;

    move-object/from16 v0, p0

    move-object/from16 v1, v23

    move-object/from16 v2, v18

    move-object/from16 v3, p3

    invoke-direct {v4, v0, v1, v2, v3}, Lcom/peel/backup/q;-><init>(Lcom/peel/backup/c;Ljava/util/Map;Lcom/peel/data/ContentRoom;Lcom/peel/util/t;)V

    move-object/from16 v0, p0

    move-object/from16 v1, v23

    move-object/from16 v2, v22

    invoke-direct {v0, v1, v2, v4}, Lcom/peel/backup/c;->a(Ljava/util/Map;Ljava/util/concurrent/atomic/AtomicInteger;Lcom/peel/util/t;)V

    goto :goto_a

    .line 919
    :catch_1
    move-exception v4

    move-object v5, v4

    move-object/from16 v4, v18

    goto :goto_9

    :cond_11
    move-object/from16 v4, v18

    goto :goto_8
.end method

.method private a(Lorg/json/JSONObject;Ljava/lang/String;Lcom/peel/util/t;)V
    .locals 8

    .prologue
    .line 998
    new-instance v6, Landroid/os/Bundle;

    invoke-direct {v6}, Landroid/os/Bundle;-><init>()V

    .line 1000
    const-string/jumbo v0, "age"

    const-string/jumbo v1, "age"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-virtual {v6, v0, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 1002
    const-string/jumbo v0, "sex"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1003
    const-string/jumbo v0, "sex"

    const-string/jumbo v1, "sex"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1006
    :cond_0
    const-string/jumbo v0, "country"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1007
    new-instance v7, Landroid/os/Bundle;

    invoke-direct {v7}, Landroid/os/Bundle;-><init>()V

    .line 1009
    new-instance v0, Lcom/peel/backup/t;

    move-object v1, p0

    move-object v2, p3

    move-object v4, p2

    move-object v5, p1

    invoke-direct/range {v0 .. v6}, Lcom/peel/backup/t;-><init>(Lcom/peel/backup/c;Lcom/peel/util/t;Ljava/lang/String;Ljava/lang/String;Lorg/json/JSONObject;Landroid/os/Bundle;)V

    invoke-direct {p0, v7, p2, v3, v0}, Lcom/peel/backup/c;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;Lcom/peel/util/t;)V

    .line 1089
    return-void
.end method

.method static synthetic b(Lcom/peel/backup/c;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/peel/backup/c;->g:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 52
    sget-object v0, Lcom/peel/backup/c;->f:Ljava/lang/String;

    return-object v0
.end method

.method private b(Lcom/peel/control/RoomControl;Ljava/lang/String;Lcom/peel/control/a;Lcom/peel/control/h;)V
    .locals 10

    .prologue
    .line 486
    invoke-virtual {p3}, Lcom/peel/control/a;->e()[Lcom/peel/control/h;

    move-result-object v0

    .line 488
    if-nez v0, :cond_0

    .line 522
    :goto_0
    return-void

    .line 489
    :cond_0
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    .line 490
    const-string/jumbo v0, "auth"

    iget-object v1, p0, Lcom/peel/backup/c;->h:Landroid/content/SharedPreferences;

    const-string/jumbo v2, "scalos_auth"

    const/4 v4, 0x0

    invoke-interface {v1, v2, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v3, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 491
    const-string/jumbo v0, "deviceTypeID"

    invoke-virtual {p4}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/data/g;->d()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v3, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 492
    const-string/jumbo v0, "brandID"

    invoke-virtual {p4}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/data/g;->f()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v3, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 494
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    .line 495
    new-instance v0, Lcom/peel/backup/am;

    invoke-direct {v0, p4}, Lcom/peel/backup/am;-><init>(Lcom/peel/control/h;)V

    .line 496
    const-string/jumbo v1, "controlData"

    invoke-virtual {v0}, Lcom/peel/backup/am;->c()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v4, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 498
    const-class v0, Lcom/peel/backup/c;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v8

    const-string/jumbo v9, "save device"

    new-instance v0, Lcom/peel/backup/af;

    move-object v1, p0

    move-object v2, p1

    move-object v5, p4

    move-object v6, p3

    move-object v7, p2

    invoke-direct/range {v0 .. v7}, Lcom/peel/backup/af;-><init>(Lcom/peel/backup/c;Lcom/peel/control/RoomControl;Ljava/util/Map;Ljava/util/Map;Lcom/peel/control/h;Lcom/peel/control/a;Ljava/lang/String;)V

    invoke-static {v8, v9, v0}, Lcom/peel/util/i;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    goto :goto_0
.end method

.method private b(Lcom/peel/util/t;)V
    .locals 14

    .prologue
    .line 707
    iget-object v0, p0, Lcom/peel/backup/c;->i:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v9

    .line 708
    sget-object v10, Lcom/peel/content/a;->f:Lcom/peel/content/user/User;

    .line 709
    const/4 v0, 0x0

    .line 710
    const/4 v7, 0x0

    .line 711
    new-instance v11, Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v1, 0x0

    invoke-direct {v11, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    .line 712
    new-instance v12, Ljava/util/HashMap;

    invoke-direct {v12}, Ljava/util/HashMap;-><init>()V

    move v8, v0

    .line 714
    :goto_0
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 715
    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Ljava/lang/Integer;

    .line 716
    iget-object v0, p0, Lcom/peel/backup/c;->i:Ljava/util/Map;

    invoke-interface {v0, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/peel/backup/as;

    .line 718
    new-instance v13, Lcom/peel/control/RoomControl;

    invoke-virtual {v6}, Lcom/peel/backup/as;->e()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v13, v0}, Lcom/peel/control/RoomControl;-><init>(Ljava/lang/String;)V

    .line 719
    invoke-virtual {v13}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v0

    invoke-virtual {v6}, Lcom/peel/backup/as;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/peel/data/at;->d(Ljava/lang/String;)V

    .line 720
    invoke-virtual {v13}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v0

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/peel/data/at;->a(I)V

    .line 722
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/peel/control/o;->a(ILjava/lang/String;)Lcom/peel/control/o;

    move-result-object v0

    invoke-virtual {v13, v0}, Lcom/peel/control/RoomControl;->a(Lcom/peel/control/o;)V

    .line 723
    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v0, v13}, Lcom/peel/control/am;->a(Lcom/peel/control/RoomControl;)V

    .line 725
    new-instance v0, Lcom/peel/data/ContentRoom;

    invoke-virtual {v13}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/data/at;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v13}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v2

    invoke-virtual {v2}, Lcom/peel/data/at;->a()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-virtual {v13}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v5

    invoke-virtual {v5}, Lcom/peel/data/at;->b()Ljava/lang/String;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, Lcom/peel/data/ContentRoom;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;ILjava/lang/String;)V

    .line 727
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 728
    const-string/jumbo v2, "path"

    const-string/jumbo v3, "rooms/add"

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 729
    const-string/jumbo v2, "room"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 730
    const/4 v2, 0x0

    invoke-virtual {v10, v1, v2}, Lcom/peel/content/user/User;->a(Landroid/os/Bundle;Lcom/peel/util/t;)V

    .line 732
    sget-object v1, Lcom/peel/content/a;->f:Lcom/peel/content/user/User;

    invoke-virtual {v1}, Lcom/peel/content/user/User;->u()V

    .line 733
    add-int/lit8 v1, v8, 0x1

    if-nez v8, :cond_1

    .line 737
    :goto_1
    invoke-virtual {v6, v13}, Lcom/peel/backup/as;->a(Lcom/peel/control/RoomControl;)V

    .line 738
    invoke-virtual {v6}, Lcom/peel/backup/as;->b()Ljava/util/Map;

    move-result-object v2

    invoke-interface {v12, v2}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    move-object v7, v0

    move v8, v1

    .line 739
    goto/16 :goto_0

    .line 742
    :cond_0
    new-instance v0, Lcom/peel/backup/n;

    invoke-direct {v0, p0, v12, v7, p1}, Lcom/peel/backup/n;-><init>(Lcom/peel/backup/c;Ljava/util/Map;Lcom/peel/data/ContentRoom;Lcom/peel/util/t;)V

    invoke-direct {p0, v12, v11, v0}, Lcom/peel/backup/c;->a(Ljava/util/Map;Ljava/util/concurrent/atomic/AtomicInteger;Lcom/peel/util/t;)V

    .line 753
    return-void

    :cond_1
    move-object v0, v7

    goto :goto_1
.end method

.method private b(Lorg/json/JSONObject;)V
    .locals 6

    .prologue
    .line 601
    :try_start_0
    const-string/jumbo v0, "rooms"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v1

    .line 602
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/peel/backup/c;->i:Ljava/util/Map;

    .line 605
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1}, Lorg/json/JSONArray;->length()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 606
    invoke-virtual {v1, v0}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v2

    .line 607
    new-instance v3, Lcom/peel/backup/as;

    invoke-direct {v3}, Lcom/peel/backup/as;-><init>()V

    .line 608
    invoke-virtual {v3, v2}, Lcom/peel/backup/as;->a(Lorg/json/JSONObject;)V

    .line 609
    iget-object v4, p0, Lcom/peel/backup/c;->i:Ljava/util/Map;

    const-string/jumbo v5, "epgRoomID"

    invoke-virtual {v2, v5}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v4, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 610
    iget v2, p0, Lcom/peel/backup/c;->j:I

    invoke-virtual {v3}, Lcom/peel/backup/as;->d()I

    move-result v3

    add-int/2addr v2, v3

    iput v2, p0, Lcom/peel/backup/c;->j:I
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 605
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 613
    :catch_0
    move-exception v0

    .line 614
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    .line 616
    :cond_0
    return-void
.end method

.method private c()Ljava/lang/String;
    .locals 4

    .prologue
    .line 323
    iget-object v0, p0, Lcom/peel/backup/c;->g:Landroid/content/Context;

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string/jumbo v1, "config_legacy"

    const-string/jumbo v2, "name|United States Of America|endpoint|usa|iso|US|type|5digitzip"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 324
    invoke-static {v0}, Lcom/peel/util/bx;->b(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    .line 326
    const-string/jumbo v0, "US"

    .line 327
    const-string/jumbo v2, "asia"

    const-string/jumbo v3, "endpoint"

    invoke-virtual {v1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    const-string/jumbo v0, "AS"

    .line 334
    :cond_0
    :goto_0
    return-object v0

    .line 328
    :cond_1
    const-string/jumbo v2, "europe"

    const-string/jumbo v3, "endpoint"

    invoke-virtual {v1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    const-string/jumbo v0, "EU"

    goto :goto_0

    .line 329
    :cond_2
    const-string/jumbo v2, "australia"

    const-string/jumbo v3, "endpoint"

    invoke-virtual {v1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    const-string/jumbo v0, "AU"

    goto :goto_0

    .line 330
    :cond_3
    const-string/jumbo v2, "latin"

    const-string/jumbo v3, "endpoint"

    invoke-virtual {v1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    const-string/jumbo v0, "LA"

    goto :goto_0

    .line 331
    :cond_4
    const-string/jumbo v2, "bramex"

    const-string/jumbo v3, "endpoint"

    invoke-virtual {v1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    const-string/jumbo v0, "BM"

    goto :goto_0

    .line 332
    :cond_5
    const-string/jumbo v1, "XX"

    sget-object v2, Lcom/peel/content/a;->e:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string/jumbo v0, "XX"

    goto :goto_0
.end method


# virtual methods
.method public a()V
    .locals 3

    .prologue
    .line 102
    const-class v0, Lcom/peel/backup/c;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "get user info by legacy id"

    new-instance v2, Lcom/peel/backup/d;

    invoke-direct {v2, p0}, Lcom/peel/backup/d;-><init>(Lcom/peel/backup/c;)V

    invoke-static {v0, v1, v2}, Lcom/peel/util/i;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    .line 180
    return-void
.end method

.method public a(Lcom/peel/control/RoomControl;)V
    .locals 5

    .prologue
    .line 560
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 561
    const-string/jumbo v1, "auth"

    iget-object v2, p0, Lcom/peel/backup/c;->h:Landroid/content/SharedPreferences;

    const-string/jumbo v3, "scalos_auth"

    const/4 v4, 0x0

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 563
    const-class v1, Lcom/peel/backup/c;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "remove room"

    new-instance v3, Lcom/peel/backup/ak;

    invoke-direct {v3, p0, p1, v0}, Lcom/peel/backup/ak;-><init>(Lcom/peel/backup/c;Lcom/peel/control/RoomControl;Ljava/util/Map;)V

    invoke-static {v1, v2, v3}, Lcom/peel/util/i;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    .line 573
    return-void
.end method

.method public a(Lcom/peel/control/RoomControl;Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 459
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 460
    const-string/jumbo v1, "auth"

    iget-object v2, p0, Lcom/peel/backup/c;->h:Landroid/content/SharedPreferences;

    const-string/jumbo v3, "scalos_auth"

    const/4 v4, 0x0

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 461
    invoke-virtual {p1}, Lcom/peel/control/RoomControl;->c()[Lcom/peel/control/a;

    move-result-object v1

    .line 462
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 464
    new-instance v3, Lcom/peel/backup/ar;

    invoke-direct {v3, v1, p2}, Lcom/peel/backup/ar;-><init>([Lcom/peel/control/a;Ljava/lang/String;)V

    .line 465
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 467
    invoke-virtual {v3}, Lcom/peel/backup/ar;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 468
    const-string/jumbo v3, "controlData"

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v3, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 470
    const-class v2, Lcom/peel/backup/c;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "update room info"

    new-instance v4, Lcom/peel/backup/ad;

    invoke-direct {v4, p0, p1, v0, v1}, Lcom/peel/backup/ad;-><init>(Lcom/peel/backup/c;Lcom/peel/control/RoomControl;Ljava/util/Map;Ljava/util/Map;)V

    invoke-static {v2, v3, v4}, Lcom/peel/util/i;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    .line 482
    return-void
.end method

.method public a(Lcom/peel/control/RoomControl;Ljava/lang/String;Lcom/peel/control/a;Lcom/peel/control/h;)V
    .locals 6

    .prologue
    .line 525
    iget-object v0, p0, Lcom/peel/backup/c;->h:Landroid/content/SharedPreferences;

    const-string/jumbo v1, "device_profile_id"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 527
    if-eqz v0, :cond_0

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 528
    invoke-virtual {p1}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/data/at;->g()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    .line 529
    :cond_0
    new-instance v0, Lcom/peel/backup/ah;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/peel/backup/ah;-><init>(Lcom/peel/backup/c;Lcom/peel/control/RoomControl;Ljava/lang/String;Lcom/peel/control/a;Lcom/peel/control/h;)V

    invoke-virtual {p0, p1, p2, v0}, Lcom/peel/backup/c;->a(Lcom/peel/control/RoomControl;Ljava/lang/String;Lcom/peel/util/t;)V

    .line 540
    :goto_0
    return-void

    .line 538
    :cond_1
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/peel/backup/c;->b(Lcom/peel/control/RoomControl;Ljava/lang/String;Lcom/peel/control/a;Lcom/peel/control/h;)V

    goto :goto_0
.end method

.method public a(Lcom/peel/control/RoomControl;Ljava/lang/String;Lcom/peel/control/h;)V
    .locals 8

    .prologue
    .line 543
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    .line 544
    const-string/jumbo v0, "auth"

    iget-object v1, p0, Lcom/peel/backup/c;->h:Landroid/content/SharedPreferences;

    const-string/jumbo v2, "scalos_auth"

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v4, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 546
    const-class v0, Lcom/peel/backup/c;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v6

    const-string/jumbo v7, "remove device"

    new-instance v0, Lcom/peel/backup/ai;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p3

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/peel/backup/ai;-><init>(Lcom/peel/backup/c;Lcom/peel/control/RoomControl;Lcom/peel/control/h;Ljava/util/Map;Ljava/lang/String;)V

    invoke-static {v6, v7, v0}, Lcom/peel/util/i;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    .line 557
    return-void
.end method

.method public a(Lcom/peel/control/RoomControl;Ljava/lang/String;Lcom/peel/util/t;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/peel/control/RoomControl;",
            "Ljava/lang/String;",
            "Lcom/peel/util/t",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 399
    iget-object v0, p0, Lcom/peel/backup/c;->h:Landroid/content/SharedPreferences;

    const-string/jumbo v1, "device_profile_id"

    invoke-interface {v0, v1, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 400
    if-nez v0, :cond_0

    .line 401
    const/4 v0, 0x1

    invoke-virtual {p0, v0, v4, p2, p3}, Lcom/peel/backup/c;->a(ZLjava/lang/String;Ljava/lang/String;Lcom/peel/util/t;)V

    .line 456
    :goto_0
    return-void

    .line 406
    :cond_0
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 407
    const-string/jumbo v0, "auth"

    iget-object v1, p0, Lcom/peel/backup/c;->h:Landroid/content/SharedPreferences;

    const-string/jumbo v3, "scalos_auth"

    invoke-interface {v1, v3, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v2, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 408
    const-string/jumbo v0, "name"

    invoke-virtual {p1}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/data/at;->a()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v2, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 409
    const-string/jumbo v0, "epgRoomID"

    invoke-virtual {p1}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/data/at;->f()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v2, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 411
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    .line 413
    if-eqz p2, :cond_1

    .line 414
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 416
    :try_start_0
    const-string/jumbo v1, "providerName"

    invoke-virtual {v0, v1, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 417
    const-string/jumbo v1, "controlData"

    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v3, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 423
    :cond_1
    :goto_1
    const-class v0, Lcom/peel/backup/c;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v6

    const-string/jumbo v7, "save room"

    new-instance v0, Lcom/peel/backup/ab;

    move-object v1, p0

    move-object v4, p1

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/peel/backup/ab;-><init>(Lcom/peel/backup/c;Ljava/util/Map;Ljava/util/Map;Lcom/peel/control/RoomControl;Lcom/peel/util/t;)V

    invoke-static {v6, v7, v0}, Lcom/peel/util/i;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    goto :goto_0

    .line 418
    :catch_0
    move-exception v0

    .line 419
    sget-object v1, Lcom/peel/backup/c;->f:Ljava/lang/String;

    sget-object v4, Lcom/peel/backup/c;->f:Ljava/lang/String;

    invoke-static {v1, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method public a(Lcom/peel/util/t;)V
    .locals 3

    .prologue
    .line 183
    const-class v0, Lcom/peel/backup/c;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "get user info"

    new-instance v2, Lcom/peel/backup/w;

    invoke-direct {v2, p0, p1}, Lcom/peel/backup/w;-><init>(Lcom/peel/backup/c;Lcom/peel/util/t;)V

    invoke-static {v0, v1, v2}, Lcom/peel/util/i;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    .line 320
    return-void
.end method

.method public a(Ljava/lang/String;Lcom/peel/util/t;)V
    .locals 5

    .prologue
    .line 576
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 577
    const-string/jumbo v1, "auth"

    iget-object v2, p0, Lcom/peel/backup/c;->h:Landroid/content/SharedPreferences;

    const-string/jumbo v3, "scalos_auth"

    const/4 v4, 0x0

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 579
    const-class v1, Lcom/peel/backup/c;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "remove device profile"

    new-instance v3, Lcom/peel/backup/i;

    invoke-direct {v3, p0, p1, v0, p2}, Lcom/peel/backup/i;-><init>(Lcom/peel/backup/c;Ljava/lang/String;Ljava/util/Map;Lcom/peel/util/t;)V

    invoke-static {v1, v2, v3}, Lcom/peel/util/i;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    .line 592
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/peel/util/t;)V
    .locals 9

    .prologue
    .line 619
    const-class v0, Lcom/peel/backup/c;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v7

    const-string/jumbo v8, "grab user by udid"

    new-instance v0, Lcom/peel/backup/k;

    move-object v1, p0

    move-object v2, p4

    move-object v3, p2

    move-object v4, p1

    move-object v5, p3

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/peel/backup/k;-><init>(Lcom/peel/backup/c;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/peel/util/t;)V

    invoke-static {v7, v8, v0}, Lcom/peel/util/i;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    .line 691
    return-void
.end method

.method public a(Lorg/json/JSONObject;)V
    .locals 3

    .prologue
    .line 595
    iget-object v0, p0, Lcom/peel/backup/c;->h:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v1, "device_profile_id"

    const-string/jumbo v2, "ID"

    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 596
    invoke-direct {p0, p1}, Lcom/peel/backup/c;->b(Lorg/json/JSONObject;)V

    .line 597
    return-void
.end method

.method public a(ZLjava/lang/String;Ljava/lang/String;Lcom/peel/util/t;)V
    .locals 8

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 354
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 355
    const-string/jumbo v0, "auth"

    iget-object v1, p0, Lcom/peel/backup/c;->h:Landroid/content/SharedPreferences;

    const-string/jumbo v3, "scalos_auth"

    invoke-interface {v1, v3, v6}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v2, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 356
    const-string/jumbo v0, "legacyUserID"

    sget-object v1, Lcom/peel/content/a;->f:Lcom/peel/content/user/User;

    invoke-virtual {v1}, Lcom/peel/content/user/User;->x()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v2, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 357
    const-string/jumbo v0, "mobileDeviceID"

    iget-object v1, p0, Lcom/peel/backup/c;->g:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string/jumbo v3, "android_id"

    invoke-static {v1, v3}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v2, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 359
    sget-object v0, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    .line 361
    const-string/jumbo v1, "name"

    sget-object v3, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    :goto_0
    invoke-interface {v2, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 362
    const-string/jumbo v0, "regionPrefix"

    invoke-direct {p0}, Lcom/peel/backup/c;->c()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v2, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 364
    iget-object v0, p0, Lcom/peel/backup/c;->h:Landroid/content/SharedPreferences;

    const-string/jumbo v1, "device_profile_id"

    invoke-interface {v0, v1, v6}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    .line 365
    const-class v0, Lcom/peel/backup/c;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v6

    const-string/jumbo v7, "register device profile"

    new-instance v0, Lcom/peel/backup/z;

    move-object v1, p0

    move v3, p1

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/peel/backup/z;-><init>(Lcom/peel/backup/c;Ljava/util/Map;ZLjava/lang/String;Lcom/peel/util/t;)V

    invoke-static {v6, v7, v0}, Lcom/peel/util/i;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    .line 395
    :cond_0
    return-void

    .line 361
    :cond_1
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v4, 0x0

    invoke-virtual {v0, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    invoke-virtual {v0, v5, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v3, " "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v3, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
