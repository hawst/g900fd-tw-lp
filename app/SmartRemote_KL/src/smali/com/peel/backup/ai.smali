.class Lcom/peel/backup/ai;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/peel/control/RoomControl;

.field final synthetic b:Lcom/peel/control/h;

.field final synthetic c:Ljava/util/Map;

.field final synthetic d:Ljava/lang/String;

.field final synthetic e:Lcom/peel/backup/c;


# direct methods
.method constructor <init>(Lcom/peel/backup/c;Lcom/peel/control/RoomControl;Lcom/peel/control/h;Ljava/util/Map;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 546
    iput-object p1, p0, Lcom/peel/backup/ai;->e:Lcom/peel/backup/c;

    iput-object p2, p0, Lcom/peel/backup/ai;->a:Lcom/peel/control/RoomControl;

    iput-object p3, p0, Lcom/peel/backup/ai;->b:Lcom/peel/control/h;

    iput-object p4, p0, Lcom/peel/backup/ai;->c:Ljava/util/Map;

    iput-object p5, p0, Lcom/peel/backup/ai;->d:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 549
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/peel/backup/c;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/backup/ai;->e:Lcom/peel/backup/c;

    invoke-static {v1}, Lcom/peel/backup/c;->a(Lcom/peel/backup/c;)Landroid/content/SharedPreferences;

    move-result-object v1

    const-string/jumbo v2, "device_profile_id"

    invoke-interface {v1, v2, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "/room/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/backup/ai;->a:Lcom/peel/control/RoomControl;

    invoke-virtual {v1}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/data/at;->g()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "/device/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/backup/ai;->b:Lcom/peel/control/h;

    invoke-virtual {v1}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/data/g;->n()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/backup/ai;->c:Ljava/util/Map;

    const/4 v2, 0x1

    new-instance v3, Lcom/peel/backup/aj;

    invoke-direct {v3, p0}, Lcom/peel/backup/aj;-><init>(Lcom/peel/backup/ai;)V

    invoke-static {v0, v1, v4, v2, v3}, Lcom/peel/util/b/a;->a(Ljava/lang/String;Ljava/util/Map;Ljava/util/Map;ZLcom/peel/util/t;)V

    .line 555
    return-void
.end method
