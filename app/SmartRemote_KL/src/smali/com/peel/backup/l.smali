.class Lcom/peel/backup/l;
.super Lcom/peel/util/t;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/peel/util/t",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/peel/backup/k;


# direct methods
.method constructor <init>(Lcom/peel/backup/k;)V
    .locals 0

    .prologue
    .line 635
    iput-object p1, p0, Lcom/peel/backup/l;->a:Lcom/peel/backup/k;

    invoke-direct {p0}, Lcom/peel/util/t;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(ZLjava/lang/Object;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 635
    check-cast p2, Ljava/lang/String;

    invoke-virtual {p0, p1, p2, p3}, Lcom/peel/backup/l;->a(ZLjava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public a(ZLjava/lang/String;Ljava/lang/String;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 638
    if-eqz p1, :cond_4

    .line 641
    :try_start_0
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0, p2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 643
    const-string/jumbo v1, "deviceProfile"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 644
    iget-object v1, p0, Lcom/peel/backup/l;->a:Lcom/peel/backup/k;

    iget-object v1, v1, Lcom/peel/backup/k;->f:Lcom/peel/backup/c;

    const-string/jumbo v2, "deviceProfile"

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/peel/backup/c;->a(Lorg/json/JSONObject;)V

    .line 651
    :cond_0
    iget-object v1, p0, Lcom/peel/backup/l;->a:Lcom/peel/backup/k;

    iget-object v1, v1, Lcom/peel/backup/k;->f:Lcom/peel/backup/c;

    invoke-static {v1}, Lcom/peel/backup/c;->b(Lcom/peel/backup/c;)Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {v1, v2}, Lcom/peel/util/dq;->a(Landroid/content/Context;Z)V

    .line 653
    iget-object v1, p0, Lcom/peel/backup/l;->a:Lcom/peel/backup/k;

    iget-object v1, v1, Lcom/peel/backup/k;->b:Ljava/lang/String;

    const-string/jumbo v2, "XX"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 654
    iget-object v1, p0, Lcom/peel/backup/l;->a:Lcom/peel/backup/k;

    iget-object v1, v1, Lcom/peel/backup/k;->f:Lcom/peel/backup/c;

    invoke-static {v1}, Lcom/peel/backup/c;->b(Lcom/peel/backup/c;)Landroid/content/Context;

    move-result-object v1

    const-string/jumbo v2, "setup_type"

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Lcom/peel/util/dq;->a(Landroid/content/Context;Ljava/lang/String;I)V

    .line 659
    :goto_0
    iget-object v1, p0, Lcom/peel/backup/l;->a:Lcom/peel/backup/k;

    iget-object v1, v1, Lcom/peel/backup/k;->f:Lcom/peel/backup/c;

    iget-object v2, p0, Lcom/peel/backup/l;->a:Lcom/peel/backup/k;

    iget-object v2, v2, Lcom/peel/backup/k;->b:Ljava/lang/String;

    new-instance v3, Lcom/peel/backup/m;

    invoke-direct {v3, p0}, Lcom/peel/backup/m;-><init>(Lcom/peel/backup/l;)V

    invoke-static {v1, v0, v2, v3}, Lcom/peel/backup/c;->a(Lcom/peel/backup/c;Lorg/json/JSONObject;Ljava/lang/String;Lcom/peel/util/t;)V

    .line 687
    :cond_1
    :goto_1
    return-void

    .line 646
    :cond_2
    iget-object v1, p0, Lcom/peel/backup/l;->a:Lcom/peel/backup/k;

    iget-object v1, v1, Lcom/peel/backup/k;->e:Lcom/peel/util/t;

    if-eqz v1, :cond_0

    .line 647
    iget-object v0, p0, Lcom/peel/backup/l;->a:Lcom/peel/backup/k;

    iget-object v0, v0, Lcom/peel/backup/k;->e:Lcom/peel/util/t;

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lcom/peel/util/t;->a(ZLjava/lang/Object;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 673
    :catch_0
    move-exception v0

    .line 674
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 675
    iget-object v1, p0, Lcom/peel/backup/l;->a:Lcom/peel/backup/k;

    iget-object v1, v1, Lcom/peel/backup/k;->e:Lcom/peel/util/t;

    if-eqz v1, :cond_1

    .line 676
    iget-object v1, p0, Lcom/peel/backup/l;->a:Lcom/peel/backup/k;

    iget-object v1, v1, Lcom/peel/backup/k;->f:Lcom/peel/backup/c;

    iget-object v2, p0, Lcom/peel/backup/l;->a:Lcom/peel/backup/k;

    iget-object v2, v2, Lcom/peel/backup/k;->e:Lcom/peel/util/t;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v2, v0}, Lcom/peel/backup/c;->a(Lcom/peel/backup/c;Lcom/peel/util/t;Ljava/lang/String;)V

    goto :goto_1

    .line 656
    :cond_3
    :try_start_1
    iget-object v1, p0, Lcom/peel/backup/l;->a:Lcom/peel/backup/k;

    iget-object v1, v1, Lcom/peel/backup/k;->f:Lcom/peel/backup/c;

    invoke-static {v1}, Lcom/peel/backup/c;->b(Lcom/peel/backup/c;)Landroid/content/Context;

    move-result-object v1

    const-string/jumbo v2, "setup_type"

    const/4 v3, 0x1

    invoke-static {v1, v2, v3}, Lcom/peel/util/dq;->a(Landroid/content/Context;Ljava/lang/String;I)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 681
    :cond_4
    if-eqz p3, :cond_5

    .line 682
    invoke-static {}, Lcom/peel/backup/c;->b()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "error msg : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 684
    :cond_5
    iget-object v0, p0, Lcom/peel/backup/l;->a:Lcom/peel/backup/k;

    iget-object v0, v0, Lcom/peel/backup/k;->e:Lcom/peel/util/t;

    invoke-virtual {v0, v4, v3, v3}, Lcom/peel/util/t;->a(ZLjava/lang/Object;Ljava/lang/String;)V

    goto :goto_1
.end method
