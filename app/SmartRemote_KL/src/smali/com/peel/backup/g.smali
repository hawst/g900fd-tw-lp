.class Lcom/peel/backup/g;
.super Lcom/peel/util/t;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/peel/util/t",
        "<[",
        "Lcom/peel/content/library/Library;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/peel/data/ContentRoom;

.field final synthetic b:Lcom/peel/backup/f;


# direct methods
.method constructor <init>(Lcom/peel/backup/f;ILcom/peel/data/ContentRoom;)V
    .locals 0

    .prologue
    .line 134
    iput-object p1, p0, Lcom/peel/backup/g;->b:Lcom/peel/backup/f;

    iput-object p3, p0, Lcom/peel/backup/g;->a:Lcom/peel/data/ContentRoom;

    invoke-direct {p0, p2}, Lcom/peel/util/t;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(ZLjava/lang/Object;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 134
    check-cast p2, [Lcom/peel/content/library/Library;

    invoke-virtual {p0, p1, p2, p3}, Lcom/peel/backup/g;->a(Z[Lcom/peel/content/library/Library;Ljava/lang/String;)V

    return-void
.end method

.method public a(Z[Lcom/peel/content/library/Library;Ljava/lang/String;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 137
    if-eqz p1, :cond_0

    .line 139
    array-length v3, p2

    const/4 v0, 0x0

    move v2, v0

    :goto_0
    if-ge v2, v3, :cond_3

    aget-object v0, p2, v2

    .line 140
    invoke-virtual {v0}, Lcom/peel/content/library/Library;->f()Ljava/lang/String;

    move-result-object v4

    const-string/jumbo v5, "live"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 141
    check-cast v0, Lcom/peel/content/library/LiveLibrary;

    .line 146
    :goto_1
    sget-object v2, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    iget-object v3, p0, Lcom/peel/backup/g;->a:Lcom/peel/data/ContentRoom;

    invoke-virtual {v3}, Lcom/peel/data/ContentRoom;->d()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/peel/control/am;->a(Ljava/lang/String;)Lcom/peel/control/RoomControl;

    move-result-object v2

    .line 148
    iget-object v3, p0, Lcom/peel/backup/g;->b:Lcom/peel/backup/f;

    iget-object v3, v3, Lcom/peel/backup/f;->a:Lcom/peel/backup/e;

    iget-object v3, v3, Lcom/peel/backup/e;->a:Lcom/peel/backup/d;

    iget-object v3, v3, Lcom/peel/backup/d;->a:Lcom/peel/backup/c;

    if-nez v0, :cond_2

    :goto_2
    new-instance v4, Lcom/peel/backup/h;

    invoke-direct {v4, p0, v2, v0}, Lcom/peel/backup/h;-><init>(Lcom/peel/backup/g;Lcom/peel/control/RoomControl;Lcom/peel/content/library/LiveLibrary;)V

    invoke-virtual {v3, v2, v1, v4}, Lcom/peel/backup/c;->a(Lcom/peel/control/RoomControl;Ljava/lang/String;Lcom/peel/util/t;)V

    .line 165
    :cond_0
    return-void

    .line 139
    :cond_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 148
    :cond_2
    invoke-virtual {v0}, Lcom/peel/content/library/LiveLibrary;->d()Ljava/lang/String;

    move-result-object v1

    goto :goto_2

    :cond_3
    move-object v0, v1

    goto :goto_1
.end method
