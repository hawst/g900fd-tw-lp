.class Lcom/peel/backup/ac;
.super Lcom/peel/util/t;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/peel/util/t",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/peel/backup/ab;


# direct methods
.method constructor <init>(Lcom/peel/backup/ab;)V
    .locals 0

    .prologue
    .line 426
    iput-object p1, p0, Lcom/peel/backup/ac;->a:Lcom/peel/backup/ab;

    invoke-direct {p0}, Lcom/peel/util/t;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(ZLjava/lang/Object;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 426
    check-cast p2, Ljava/lang/String;

    invoke-virtual {p0, p1, p2, p3}, Lcom/peel/backup/ac;->a(ZLjava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public a(ZLjava/lang/String;Ljava/lang/String;)V
    .locals 7

    .prologue
    .line 428
    if-eqz p1, :cond_0

    .line 431
    :try_start_0
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0, p2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 432
    const-string/jumbo v1, "ID"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 433
    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v0}, Lcom/peel/control/am;->c()[Lcom/peel/control/RoomControl;

    move-result-object v2

    .line 435
    array-length v3, v2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_0

    aget-object v4, v2, v0

    .line 436
    invoke-virtual {v4}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v5

    invoke-virtual {v5}, Lcom/peel/data/at;->b()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/peel/backup/ac;->a:Lcom/peel/backup/ab;

    iget-object v6, v6, Lcom/peel/backup/ab;->c:Lcom/peel/control/RoomControl;

    invoke-virtual {v6}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v6

    invoke-virtual {v6}, Lcom/peel/data/at;->b()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 437
    invoke-virtual {v4}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/peel/data/at;->d(Ljava/lang/String;)V

    .line 438
    invoke-static {}, Lcom/peel/data/m;->a()Lcom/peel/data/m;

    move-result-object v0

    invoke-virtual {v4}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/peel/data/m;->c(Lcom/peel/data/at;)V

    .line 440
    iget-object v0, p0, Lcom/peel/backup/ac;->a:Lcom/peel/backup/ab;

    iget-object v0, v0, Lcom/peel/backup/ab;->d:Lcom/peel/util/t;

    if-eqz v0, :cond_0

    .line 441
    iget-object v0, p0, Lcom/peel/backup/ac;->a:Lcom/peel/backup/ab;

    iget-object v0, v0, Lcom/peel/backup/ab;->d:Lcom/peel/util/t;

    const/4 v2, 0x0

    invoke-virtual {v0, p1, v1, v2}, Lcom/peel/util/t;->a(ZLjava/lang/Object;Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 452
    :cond_0
    :goto_1
    return-void

    .line 435
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 446
    :catch_0
    move-exception v0

    .line 447
    invoke-static {}, Lcom/peel/backup/c;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {}, Lcom/peel/backup/c;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method
