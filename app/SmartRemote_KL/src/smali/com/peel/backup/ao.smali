.class Lcom/peel/backup/ao;
.super Landroid/widget/BaseAdapter;


# instance fields
.field final synthetic a:Lcom/peel/backup/an;


# direct methods
.method constructor <init>(Lcom/peel/backup/an;)V
    .locals 0

    .prologue
    .line 111
    iput-object p1, p0, Lcom/peel/backup/ao;->a:Lcom/peel/backup/an;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 114
    iget-object v0, p0, Lcom/peel/backup/ao;->a:Lcom/peel/backup/an;

    invoke-static {v0}, Lcom/peel/backup/an;->a(Lcom/peel/backup/an;)Ljava/util/ArrayList;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/peel/backup/ao;->a:Lcom/peel/backup/an;

    invoke-static {v0}, Lcom/peel/backup/an;->a(Lcom/peel/backup/an;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    goto :goto_0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 119
    iget-object v0, p0, Lcom/peel/backup/ao;->a:Lcom/peel/backup/an;

    invoke-static {v0}, Lcom/peel/backup/an;->a(Lcom/peel/backup/an;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2

    .prologue
    .line 124
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 10

    .prologue
    const/4 v9, 0x0

    .line 129
    if-nez p2, :cond_0

    iget-object v0, p0, Lcom/peel/backup/ao;->a:Lcom/peel/backup/an;

    invoke-static {v0}, Lcom/peel/backup/an;->b(Lcom/peel/backup/an;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lcom/peel/ui/fq;->l17_list_item:I

    invoke-virtual {v0, v1, p3, v9}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 130
    :cond_0
    sget v0, Lcom/peel/ui/fp;->main_item:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 131
    sget v1, Lcom/peel/ui/fp;->sub_item:I

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 132
    sget v2, Lcom/peel/ui/fp;->third_item:I

    invoke-virtual {p2, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 133
    sget v3, Lcom/peel/ui/fp;->selected:I

    invoke-virtual {p2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    .line 134
    invoke-virtual {p0, p1}, Lcom/peel/backup/ao;->getItem(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/peel/backup/MobileDeviceProfile;

    .line 135
    iget-object v5, p0, Lcom/peel/backup/ao;->a:Lcom/peel/backup/an;

    sget v6, Lcom/peel/ui/ft;->import_device_title:I

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    invoke-virtual {v4}, Lcom/peel/backup/MobileDeviceProfile;->b()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v7, v9

    invoke-virtual {v5, v6, v7}, Lcom/peel/backup/an;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 137
    invoke-virtual {v4}, Lcom/peel/backup/MobileDeviceProfile;->g()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v4}, Lcom/peel/backup/MobileDeviceProfile;->f()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_4

    invoke-virtual {v4}, Lcom/peel/backup/MobileDeviceProfile;->g()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 138
    :cond_1
    const/16 v0, 0x8

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 140
    invoke-virtual {v4}, Lcom/peel/backup/MobileDeviceProfile;->f()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_2

    invoke-virtual {v4}, Lcom/peel/backup/MobileDeviceProfile;->g()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 141
    invoke-virtual {v4}, Lcom/peel/backup/MobileDeviceProfile;->g()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 148
    :cond_2
    :goto_0
    invoke-virtual {v4}, Lcom/peel/backup/MobileDeviceProfile;->f()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 149
    invoke-virtual {v4}, Lcom/peel/backup/MobileDeviceProfile;->f()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 152
    :cond_3
    iget-object v0, p0, Lcom/peel/backup/ao;->a:Lcom/peel/backup/an;

    invoke-static {v0}, Lcom/peel/backup/an;->c(Lcom/peel/backup/an;)I

    move-result v0

    if-ne p1, v0, :cond_5

    .line 153
    sget v0, Lcom/peel/ui/fo;->tw_btn_radio_on_holo_dark:I

    invoke-virtual {v3, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 157
    :goto_1
    return-object p2

    .line 144
    :cond_4
    invoke-virtual {v2, v9}, Landroid/widget/TextView;->setVisibility(I)V

    .line 145
    invoke-virtual {v4}, Lcom/peel/backup/MobileDeviceProfile;->g()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 155
    :cond_5
    sget v0, Lcom/peel/ui/fo;->tw_btn_radio_off_holo_dark:I

    invoke-virtual {v3, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_1
.end method
