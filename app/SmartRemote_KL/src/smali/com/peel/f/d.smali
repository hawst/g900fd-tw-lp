.class public Lcom/peel/f/d;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable",
        "<",
        "Lcom/peel/f/d;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Ljava/lang/String;

.field private final c:I


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 9
    iput-object p1, p0, Lcom/peel/f/d;->a:Ljava/lang/String;

    .line 10
    iput-object p2, p0, Lcom/peel/f/d;->b:Ljava/lang/String;

    .line 11
    iput p3, p0, Lcom/peel/f/d;->c:I

    .line 12
    return-void
.end method


# virtual methods
.method public a(Lcom/peel/f/d;)I
    .locals 2

    .prologue
    .line 15
    iget v0, p0, Lcom/peel/f/d;->c:I

    invoke-virtual {p1}, Lcom/peel/f/d;->b()I

    move-result v1

    sub-int/2addr v0, v1

    return v0
.end method

.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 18
    iget-object v0, p0, Lcom/peel/f/d;->a:Ljava/lang/String;

    return-object v0
.end method

.method public b()I
    .locals 1

    .prologue
    .line 25
    iget v0, p0, Lcom/peel/f/d;->c:I

    return v0
.end method

.method public synthetic compareTo(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 3
    check-cast p1, Lcom/peel/f/d;

    invoke-virtual {p0, p1}, Lcom/peel/f/d;->a(Lcom/peel/f/d;)I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lcom/peel/f/d;->b:Ljava/lang/String;

    return-object v0
.end method
