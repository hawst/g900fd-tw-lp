.class public Lcom/peel/f/b;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lcom/peel/f/a;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/peel/f/a;Lcom/peel/f/a;)I
    .locals 2

    .prologue
    .line 10
    invoke-virtual {p1}, Lcom/peel/f/a;->c()I

    move-result v0

    invoke-virtual {p2}, Lcom/peel/f/a;->c()I

    move-result v1

    if-ge v0, v1, :cond_0

    const/4 v0, -0x1

    .line 12
    :goto_0
    return v0

    .line 11
    :cond_0
    invoke-virtual {p1}, Lcom/peel/f/a;->c()I

    move-result v0

    invoke-virtual {p2}, Lcom/peel/f/a;->c()I

    move-result v1

    if-le v0, v1, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    .line 12
    :cond_1
    invoke-virtual {p1}, Lcom/peel/f/a;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2}, Lcom/peel/f/a;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    goto :goto_0
.end method

.method public synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 8
    check-cast p1, Lcom/peel/f/a;

    check-cast p2, Lcom/peel/f/a;

    invoke-virtual {p0, p1, p2}, Lcom/peel/f/b;->a(Lcom/peel/f/a;Lcom/peel/f/a;)I

    move-result v0

    return v0
.end method
