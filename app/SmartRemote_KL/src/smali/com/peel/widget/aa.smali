.class Lcom/peel/widget/aa;
.super Landroid/view/GestureDetector$SimpleOnGestureListener;


# instance fields
.field final synthetic a:Lcom/peel/widget/HorizontalListView;


# direct methods
.method constructor <init>(Lcom/peel/widget/HorizontalListView;)V
    .locals 0

    .prologue
    .line 341
    iput-object p1, p0, Lcom/peel/widget/aa;->a:Lcom/peel/widget/HorizontalListView;

    invoke-direct {p0}, Landroid/view/GestureDetector$SimpleOnGestureListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onDown(Landroid/view/MotionEvent;)Z
    .locals 8

    .prologue
    .line 345
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    .line 346
    iget-object v0, p0, Lcom/peel/widget/aa;->a:Lcom/peel/widget/HorizontalListView;

    invoke-virtual {v0}, Lcom/peel/widget/HorizontalListView;->getChildCount()I

    move-result v3

    .line 347
    const/4 v0, 0x0

    move v4, v0

    :goto_0
    if-ge v4, v3, :cond_0

    .line 348
    iget-object v0, p0, Lcom/peel/widget/aa;->a:Lcom/peel/widget/HorizontalListView;

    invoke-virtual {v0, v4}, Lcom/peel/widget/HorizontalListView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 349
    invoke-virtual {v2}, Landroid/view/View;->getLeft()I

    move-result v0

    .line 350
    invoke-virtual {v2}, Landroid/view/View;->getRight()I

    move-result v5

    .line 351
    invoke-virtual {v2}, Landroid/view/View;->getTop()I

    move-result v6

    .line 352
    invoke-virtual {v2}, Landroid/view/View;->getBottom()I

    move-result v7

    .line 353
    invoke-virtual {v1, v0, v6, v5, v7}, Landroid/graphics/Rect;->set(IIII)V

    .line 354
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    float-to-int v0, v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v5

    float-to-int v5, v5

    invoke-virtual {v1, v0, v5}, Landroid/graphics/Rect;->contains(II)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 355
    iget-object v0, p0, Lcom/peel/widget/aa;->a:Lcom/peel/widget/HorizontalListView;

    invoke-static {v0}, Lcom/peel/widget/HorizontalListView;->b(Lcom/peel/widget/HorizontalListView;)Lcom/peel/widget/ad;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 356
    iget-object v0, p0, Lcom/peel/widget/aa;->a:Lcom/peel/widget/HorizontalListView;

    invoke-static {v0}, Lcom/peel/widget/HorizontalListView;->b(Lcom/peel/widget/HorizontalListView;)Lcom/peel/widget/ad;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/widget/aa;->a:Lcom/peel/widget/HorizontalListView;

    iget-object v3, p0, Lcom/peel/widget/aa;->a:Lcom/peel/widget/HorizontalListView;

    invoke-static {v3}, Lcom/peel/widget/HorizontalListView;->c(Lcom/peel/widget/HorizontalListView;)I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    add-int/2addr v3, v4

    iget-object v5, p0, Lcom/peel/widget/aa;->a:Lcom/peel/widget/HorizontalListView;

    iget-object v5, v5, Lcom/peel/widget/HorizontalListView;->b:Landroid/widget/ListAdapter;

    iget-object v6, p0, Lcom/peel/widget/aa;->a:Lcom/peel/widget/HorizontalListView;

    invoke-static {v6}, Lcom/peel/widget/HorizontalListView;->c(Lcom/peel/widget/HorizontalListView;)I

    move-result v6

    add-int/lit8 v6, v6, 0x1

    add-int/2addr v4, v6

    invoke-interface {v5, v4}, Landroid/widget/ListAdapter;->getItemId(I)J

    move-result-wide v4

    invoke-interface/range {v0 .. v5}, Lcom/peel/widget/ad;->a(Landroid/widget/AdapterView;Landroid/view/View;IJ)V

    .line 361
    :cond_0
    iget-object v0, p0, Lcom/peel/widget/aa;->a:Lcom/peel/widget/HorizontalListView;

    invoke-virtual {v0, p1}, Lcom/peel/widget/HorizontalListView;->a(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0

    .line 347
    :cond_1
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_0
.end method

.method public onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 1

    .prologue
    .line 367
    iget-object v0, p0, Lcom/peel/widget/aa;->a:Lcom/peel/widget/HorizontalListView;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/peel/widget/HorizontalListView;->a(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z

    move-result v0

    return v0
.end method

.method public onLongPress(Landroid/view/MotionEvent;)V
    .locals 8

    .prologue
    .line 426
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    .line 427
    iget-object v0, p0, Lcom/peel/widget/aa;->a:Lcom/peel/widget/HorizontalListView;

    invoke-virtual {v0}, Lcom/peel/widget/HorizontalListView;->getChildCount()I

    move-result v3

    .line 428
    const/4 v0, 0x0

    move v4, v0

    :goto_0
    if-ge v4, v3, :cond_0

    .line 429
    iget-object v0, p0, Lcom/peel/widget/aa;->a:Lcom/peel/widget/HorizontalListView;

    invoke-virtual {v0, v4}, Lcom/peel/widget/HorizontalListView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 430
    invoke-virtual {v2}, Landroid/view/View;->getLeft()I

    move-result v0

    .line 431
    invoke-virtual {v2}, Landroid/view/View;->getRight()I

    move-result v5

    .line 432
    invoke-virtual {v2}, Landroid/view/View;->getTop()I

    move-result v6

    .line 433
    invoke-virtual {v2}, Landroid/view/View;->getBottom()I

    move-result v7

    .line 434
    invoke-virtual {v1, v0, v6, v5, v7}, Landroid/graphics/Rect;->set(IIII)V

    .line 435
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    float-to-int v0, v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v5

    float-to-int v5, v5

    invoke-virtual {v1, v0, v5}, Landroid/graphics/Rect;->contains(II)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 436
    iget-object v0, p0, Lcom/peel/widget/aa;->a:Lcom/peel/widget/HorizontalListView;

    invoke-static {v0}, Lcom/peel/widget/HorizontalListView;->g(Lcom/peel/widget/HorizontalListView;)Landroid/widget/AdapterView$OnItemLongClickListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 437
    iget-object v0, p0, Lcom/peel/widget/aa;->a:Lcom/peel/widget/HorizontalListView;

    invoke-static {v0}, Lcom/peel/widget/HorizontalListView;->g(Lcom/peel/widget/HorizontalListView;)Landroid/widget/AdapterView$OnItemLongClickListener;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/widget/aa;->a:Lcom/peel/widget/HorizontalListView;

    iget-object v3, p0, Lcom/peel/widget/aa;->a:Lcom/peel/widget/HorizontalListView;

    invoke-static {v3}, Lcom/peel/widget/HorizontalListView;->c(Lcom/peel/widget/HorizontalListView;)I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    add-int/2addr v3, v4

    iget-object v5, p0, Lcom/peel/widget/aa;->a:Lcom/peel/widget/HorizontalListView;

    iget-object v5, v5, Lcom/peel/widget/HorizontalListView;->b:Landroid/widget/ListAdapter;

    iget-object v6, p0, Lcom/peel/widget/aa;->a:Lcom/peel/widget/HorizontalListView;

    invoke-static {v6}, Lcom/peel/widget/HorizontalListView;->c(Lcom/peel/widget/HorizontalListView;)I

    move-result v6

    add-int/lit8 v6, v6, 0x1

    add-int/2addr v4, v6

    invoke-interface {v5, v4}, Landroid/widget/ListAdapter;->getItemId(I)J

    move-result-wide v4

    invoke-interface/range {v0 .. v5}, Landroid/widget/AdapterView$OnItemLongClickListener;->onItemLongClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)Z

    .line 443
    :cond_0
    return-void

    .line 428
    :cond_1
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_0
.end method

.method public onScroll(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 9

    .prologue
    const/4 v8, 0x1

    .line 373
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    .line 374
    iget-object v0, p0, Lcom/peel/widget/aa;->a:Lcom/peel/widget/HorizontalListView;

    invoke-virtual {v0}, Lcom/peel/widget/HorizontalListView;->getChildCount()I

    move-result v3

    .line 375
    const/4 v0, 0x0

    move v4, v0

    :goto_0
    if-ge v4, v3, :cond_0

    .line 376
    iget-object v0, p0, Lcom/peel/widget/aa;->a:Lcom/peel/widget/HorizontalListView;

    invoke-virtual {v0, v4}, Lcom/peel/widget/HorizontalListView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 377
    invoke-virtual {v2}, Landroid/view/View;->getLeft()I

    move-result v0

    .line 378
    invoke-virtual {v2}, Landroid/view/View;->getRight()I

    move-result v5

    .line 379
    invoke-virtual {v2}, Landroid/view/View;->getTop()I

    move-result v6

    .line 380
    invoke-virtual {v2}, Landroid/view/View;->getBottom()I

    move-result v7

    .line 381
    invoke-virtual {v1, v0, v6, v5, v7}, Landroid/graphics/Rect;->set(IIII)V

    .line 382
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    float-to-int v0, v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v5

    float-to-int v5, v5

    invoke-virtual {v1, v0, v5}, Landroid/graphics/Rect;->contains(II)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 383
    iget-object v0, p0, Lcom/peel/widget/aa;->a:Lcom/peel/widget/HorizontalListView;

    invoke-static {v0}, Lcom/peel/widget/HorizontalListView;->d(Lcom/peel/widget/HorizontalListView;)Lcom/peel/widget/ab;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 384
    iget-object v0, p0, Lcom/peel/widget/aa;->a:Lcom/peel/widget/HorizontalListView;

    invoke-static {v0}, Lcom/peel/widget/HorizontalListView;->d(Lcom/peel/widget/HorizontalListView;)Lcom/peel/widget/ab;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/widget/aa;->a:Lcom/peel/widget/HorizontalListView;

    iget-object v3, p0, Lcom/peel/widget/aa;->a:Lcom/peel/widget/HorizontalListView;

    invoke-static {v3}, Lcom/peel/widget/HorizontalListView;->c(Lcom/peel/widget/HorizontalListView;)I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    add-int/2addr v3, v4

    iget-object v5, p0, Lcom/peel/widget/aa;->a:Lcom/peel/widget/HorizontalListView;

    iget-object v5, v5, Lcom/peel/widget/HorizontalListView;->b:Landroid/widget/ListAdapter;

    iget-object v6, p0, Lcom/peel/widget/aa;->a:Lcom/peel/widget/HorizontalListView;

    invoke-static {v6}, Lcom/peel/widget/HorizontalListView;->c(Lcom/peel/widget/HorizontalListView;)I

    move-result v6

    add-int/lit8 v6, v6, 0x1

    add-int/2addr v4, v6

    invoke-interface {v5, v4}, Landroid/widget/ListAdapter;->getItemId(I)J

    move-result-wide v4

    invoke-interface/range {v0 .. v5}, Lcom/peel/widget/ab;->a(Landroid/widget/AdapterView;Landroid/view/View;IJ)V

    .line 390
    :cond_0
    iget-object v0, p0, Lcom/peel/widget/aa;->a:Lcom/peel/widget/HorizontalListView;

    invoke-virtual {v0}, Lcom/peel/widget/HorizontalListView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    invoke-interface {v0, v8}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    .line 392
    iget-object v1, p0, Lcom/peel/widget/aa;->a:Lcom/peel/widget/HorizontalListView;

    monitor-enter v1

    .line 393
    :try_start_0
    iget-object v0, p0, Lcom/peel/widget/aa;->a:Lcom/peel/widget/HorizontalListView;

    iget v2, v0, Lcom/peel/widget/HorizontalListView;->d:I

    float-to-int v3, p3

    add-int/2addr v2, v3

    iput v2, v0, Lcom/peel/widget/HorizontalListView;->d:I

    .line 394
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 395
    iget-object v0, p0, Lcom/peel/widget/aa;->a:Lcom/peel/widget/HorizontalListView;

    invoke-virtual {v0}, Lcom/peel/widget/HorizontalListView;->requestLayout()V

    .line 397
    return v8

    .line 375
    :cond_1
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_0

    .line 394
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public onSingleTapConfirmed(Landroid/view/MotionEvent;)Z
    .locals 7

    .prologue
    .line 402
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    .line 403
    const/4 v0, 0x0

    move v6, v0

    :goto_0
    iget-object v0, p0, Lcom/peel/widget/aa;->a:Lcom/peel/widget/HorizontalListView;

    invoke-virtual {v0}, Lcom/peel/widget/HorizontalListView;->getChildCount()I

    move-result v0

    if-ge v6, v0, :cond_1

    .line 404
    iget-object v0, p0, Lcom/peel/widget/aa;->a:Lcom/peel/widget/HorizontalListView;

    invoke-virtual {v0, v6}, Lcom/peel/widget/HorizontalListView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 405
    invoke-virtual {v2}, Landroid/view/View;->getLeft()I

    move-result v0

    .line 406
    invoke-virtual {v2}, Landroid/view/View;->getRight()I

    move-result v3

    .line 407
    invoke-virtual {v2}, Landroid/view/View;->getTop()I

    move-result v4

    .line 408
    invoke-virtual {v2}, Landroid/view/View;->getBottom()I

    move-result v5

    .line 409
    invoke-virtual {v1, v0, v4, v3, v5}, Landroid/graphics/Rect;->set(IIII)V

    .line 410
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    float-to-int v0, v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    float-to-int v3, v3

    invoke-virtual {v1, v0, v3}, Landroid/graphics/Rect;->contains(II)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 411
    iget-object v0, p0, Lcom/peel/widget/aa;->a:Lcom/peel/widget/HorizontalListView;

    invoke-static {v0}, Lcom/peel/widget/HorizontalListView;->e(Lcom/peel/widget/HorizontalListView;)Landroid/widget/AdapterView$OnItemClickListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 412
    iget-object v0, p0, Lcom/peel/widget/aa;->a:Lcom/peel/widget/HorizontalListView;

    invoke-static {v0}, Lcom/peel/widget/HorizontalListView;->e(Lcom/peel/widget/HorizontalListView;)Landroid/widget/AdapterView$OnItemClickListener;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/widget/aa;->a:Lcom/peel/widget/HorizontalListView;

    iget-object v3, p0, Lcom/peel/widget/aa;->a:Lcom/peel/widget/HorizontalListView;

    invoke-static {v3}, Lcom/peel/widget/HorizontalListView;->c(Lcom/peel/widget/HorizontalListView;)I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    add-int/2addr v3, v6

    iget-object v4, p0, Lcom/peel/widget/aa;->a:Lcom/peel/widget/HorizontalListView;

    iget-object v4, v4, Lcom/peel/widget/HorizontalListView;->b:Landroid/widget/ListAdapter;

    iget-object v5, p0, Lcom/peel/widget/aa;->a:Lcom/peel/widget/HorizontalListView;

    invoke-static {v5}, Lcom/peel/widget/HorizontalListView;->c(Lcom/peel/widget/HorizontalListView;)I

    move-result v5

    add-int/lit8 v5, v5, 0x1

    add-int/2addr v5, v6

    invoke-interface {v4, v5}, Landroid/widget/ListAdapter;->getItemId(I)J

    move-result-wide v4

    invoke-interface/range {v0 .. v5}, Landroid/widget/AdapterView$OnItemClickListener;->onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V

    .line 414
    :cond_0
    iget-object v0, p0, Lcom/peel/widget/aa;->a:Lcom/peel/widget/HorizontalListView;

    invoke-static {v0}, Lcom/peel/widget/HorizontalListView;->f(Lcom/peel/widget/HorizontalListView;)Landroid/widget/AdapterView$OnItemSelectedListener;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 415
    iget-object v0, p0, Lcom/peel/widget/aa;->a:Lcom/peel/widget/HorizontalListView;

    invoke-static {v0}, Lcom/peel/widget/HorizontalListView;->f(Lcom/peel/widget/HorizontalListView;)Landroid/widget/AdapterView$OnItemSelectedListener;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/widget/aa;->a:Lcom/peel/widget/HorizontalListView;

    iget-object v3, p0, Lcom/peel/widget/aa;->a:Lcom/peel/widget/HorizontalListView;

    invoke-static {v3}, Lcom/peel/widget/HorizontalListView;->c(Lcom/peel/widget/HorizontalListView;)I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    add-int/2addr v3, v6

    iget-object v4, p0, Lcom/peel/widget/aa;->a:Lcom/peel/widget/HorizontalListView;

    iget-object v4, v4, Lcom/peel/widget/HorizontalListView;->b:Landroid/widget/ListAdapter;

    iget-object v5, p0, Lcom/peel/widget/aa;->a:Lcom/peel/widget/HorizontalListView;

    invoke-static {v5}, Lcom/peel/widget/HorizontalListView;->c(Lcom/peel/widget/HorizontalListView;)I

    move-result v5

    add-int/lit8 v5, v5, 0x1

    add-int/2addr v5, v6

    invoke-interface {v4, v5}, Landroid/widget/ListAdapter;->getItemId(I)J

    move-result-wide v4

    invoke-interface/range {v0 .. v5}, Landroid/widget/AdapterView$OnItemSelectedListener;->onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V

    .line 421
    :cond_1
    const/4 v0, 0x1

    return v0

    .line 403
    :cond_2
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto/16 :goto_0
.end method

.method public onSingleTapUp(Landroid/view/MotionEvent;)Z
    .locals 8

    .prologue
    .line 447
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    .line 448
    iget-object v0, p0, Lcom/peel/widget/aa;->a:Lcom/peel/widget/HorizontalListView;

    invoke-virtual {v0}, Lcom/peel/widget/HorizontalListView;->getChildCount()I

    move-result v3

    .line 449
    const/4 v0, 0x0

    move v4, v0

    :goto_0
    if-ge v4, v3, :cond_0

    .line 450
    iget-object v0, p0, Lcom/peel/widget/aa;->a:Lcom/peel/widget/HorizontalListView;

    invoke-virtual {v0, v4}, Lcom/peel/widget/HorizontalListView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 451
    invoke-virtual {v2}, Landroid/view/View;->getLeft()I

    move-result v0

    .line 452
    invoke-virtual {v2}, Landroid/view/View;->getRight()I

    move-result v5

    .line 453
    invoke-virtual {v2}, Landroid/view/View;->getTop()I

    move-result v6

    .line 454
    invoke-virtual {v2}, Landroid/view/View;->getBottom()I

    move-result v7

    .line 455
    invoke-virtual {v1, v0, v6, v5, v7}, Landroid/graphics/Rect;->set(IIII)V

    .line 456
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    float-to-int v0, v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v5

    float-to-int v5, v5

    invoke-virtual {v1, v0, v5}, Landroid/graphics/Rect;->contains(II)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 457
    iget-object v0, p0, Lcom/peel/widget/aa;->a:Lcom/peel/widget/HorizontalListView;

    invoke-static {v0}, Lcom/peel/widget/HorizontalListView;->h(Lcom/peel/widget/HorizontalListView;)Lcom/peel/widget/ac;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 458
    iget-object v0, p0, Lcom/peel/widget/aa;->a:Lcom/peel/widget/HorizontalListView;

    invoke-static {v0}, Lcom/peel/widget/HorizontalListView;->h(Lcom/peel/widget/HorizontalListView;)Lcom/peel/widget/ac;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/widget/aa;->a:Lcom/peel/widget/HorizontalListView;

    iget-object v3, p0, Lcom/peel/widget/aa;->a:Lcom/peel/widget/HorizontalListView;

    invoke-static {v3}, Lcom/peel/widget/HorizontalListView;->c(Lcom/peel/widget/HorizontalListView;)I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    add-int/2addr v3, v4

    iget-object v5, p0, Lcom/peel/widget/aa;->a:Lcom/peel/widget/HorizontalListView;

    iget-object v5, v5, Lcom/peel/widget/HorizontalListView;->b:Landroid/widget/ListAdapter;

    iget-object v6, p0, Lcom/peel/widget/aa;->a:Lcom/peel/widget/HorizontalListView;

    invoke-static {v6}, Lcom/peel/widget/HorizontalListView;->c(Lcom/peel/widget/HorizontalListView;)I

    move-result v6

    add-int/lit8 v6, v6, 0x1

    add-int/2addr v4, v6

    invoke-interface {v5, v4}, Landroid/widget/ListAdapter;->getItemId(I)J

    move-result-wide v4

    invoke-interface/range {v0 .. v5}, Lcom/peel/widget/ac;->a(Landroid/widget/AdapterView;Landroid/view/View;IJ)V

    .line 463
    :cond_0
    const/4 v0, 0x1

    return v0

    .line 449
    :cond_1
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_0
.end method
