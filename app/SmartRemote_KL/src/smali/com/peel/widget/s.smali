.class Lcom/peel/widget/s;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/widget/AdapterView$OnItemLongClickListener;


# instance fields
.field final synthetic a:Lcom/peel/widget/DynamicListView;


# direct methods
.method constructor <init>(Lcom/peel/widget/DynamicListView;)V
    .locals 0

    .prologue
    .line 135
    iput-object p1, p0, Lcom/peel/widget/s;->a:Lcom/peel/widget/DynamicListView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemLongClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)Z
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)Z"
        }
    .end annotation

    .prologue
    const/4 v6, 0x1

    .line 137
    iget-object v0, p0, Lcom/peel/widget/s;->a:Lcom/peel/widget/DynamicListView;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/peel/widget/DynamicListView;->a(Lcom/peel/widget/DynamicListView;I)I

    .line 139
    iget-object v0, p0, Lcom/peel/widget/s;->a:Lcom/peel/widget/DynamicListView;

    iget-object v1, p0, Lcom/peel/widget/s;->a:Lcom/peel/widget/DynamicListView;

    invoke-static {v1}, Lcom/peel/widget/DynamicListView;->a(Lcom/peel/widget/DynamicListView;)I

    move-result v1

    iget-object v2, p0, Lcom/peel/widget/s;->a:Lcom/peel/widget/DynamicListView;

    invoke-static {v2}, Lcom/peel/widget/DynamicListView;->b(Lcom/peel/widget/DynamicListView;)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/peel/widget/DynamicListView;->pointToPosition(II)I

    move-result v0

    .line 140
    iget-object v1, p0, Lcom/peel/widget/s;->a:Lcom/peel/widget/DynamicListView;

    invoke-virtual {v1}, Lcom/peel/widget/DynamicListView;->getFirstVisiblePosition()I

    move-result v1

    sub-int v1, v0, v1

    .line 142
    iget-object v2, p0, Lcom/peel/widget/s;->a:Lcom/peel/widget/DynamicListView;

    invoke-virtual {v2, v1}, Lcom/peel/widget/DynamicListView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 143
    iget-object v2, p0, Lcom/peel/widget/s;->a:Lcom/peel/widget/DynamicListView;

    iget-object v3, p0, Lcom/peel/widget/s;->a:Lcom/peel/widget/DynamicListView;

    invoke-virtual {v3}, Lcom/peel/widget/DynamicListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v3

    invoke-interface {v3, v0}, Landroid/widget/ListAdapter;->getItemId(I)J

    move-result-wide v4

    invoke-static {v2, v4, v5}, Lcom/peel/widget/DynamicListView;->a(Lcom/peel/widget/DynamicListView;J)J

    .line 144
    iget-object v0, p0, Lcom/peel/widget/s;->a:Lcom/peel/widget/DynamicListView;

    iget-object v2, p0, Lcom/peel/widget/s;->a:Lcom/peel/widget/DynamicListView;

    invoke-static {v2, v1}, Lcom/peel/widget/DynamicListView;->a(Lcom/peel/widget/DynamicListView;Landroid/view/View;)Landroid/graphics/drawable/BitmapDrawable;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/peel/widget/DynamicListView;->a(Lcom/peel/widget/DynamicListView;Landroid/graphics/drawable/BitmapDrawable;)Landroid/graphics/drawable/BitmapDrawable;

    .line 145
    const/4 v0, 0x4

    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 147
    iget-object v0, p0, Lcom/peel/widget/s;->a:Lcom/peel/widget/DynamicListView;

    invoke-static {v0, v6}, Lcom/peel/widget/DynamicListView;->a(Lcom/peel/widget/DynamicListView;Z)Z

    .line 149
    iget-object v0, p0, Lcom/peel/widget/s;->a:Lcom/peel/widget/DynamicListView;

    iget-object v1, p0, Lcom/peel/widget/s;->a:Lcom/peel/widget/DynamicListView;

    invoke-static {v1}, Lcom/peel/widget/DynamicListView;->c(Lcom/peel/widget/DynamicListView;)J

    move-result-wide v2

    invoke-static {v0, v2, v3}, Lcom/peel/widget/DynamicListView;->b(Lcom/peel/widget/DynamicListView;J)V

    .line 151
    return v6
.end method
