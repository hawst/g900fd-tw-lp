.class Lcom/peel/widget/b;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/peel/widget/AdVideoView;


# direct methods
.method constructor <init>(Lcom/peel/widget/AdVideoView;)V
    .locals 0

    .prologue
    .line 289
    iput-object p1, p0, Lcom/peel/widget/b;->a:Lcom/peel/widget/AdVideoView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    .line 293
    :try_start_0
    iget-object v0, p0, Lcom/peel/widget/b;->a:Lcom/peel/widget/AdVideoView;

    invoke-static {v0}, Lcom/peel/widget/AdVideoView;->d(Lcom/peel/widget/AdVideoView;)Ljava/util/Map;

    move-result-object v0

    const-string/jumbo v1, "on_banner_click"

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 294
    iget-object v1, p0, Lcom/peel/widget/b;->a:Lcom/peel/widget/AdVideoView;

    iget-object v0, p0, Lcom/peel/widget/b;->a:Lcom/peel/widget/AdVideoView;

    invoke-static {v0}, Lcom/peel/widget/AdVideoView;->d(Lcom/peel/widget/AdVideoView;)Ljava/util/Map;

    move-result-object v0

    const-string/jumbo v2, "on_banner_click"

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v1, v0}, Lcom/peel/widget/AdVideoView;->a(Lcom/peel/widget/AdVideoView;Ljava/lang/String;)V

    .line 296
    :cond_0
    invoke-static {}, Lcom/peel/util/a/f;->a()Lcom/peel/util/a/f;

    move-result-object v0

    sget-object v1, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v1}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v1

    if-nez v1, :cond_1

    const/4 v1, 0x1

    :goto_0
    const/16 v2, 0x856

    const/16 v3, 0x1770

    iget-object v4, p0, Lcom/peel/widget/b;->a:Lcom/peel/widget/AdVideoView;

    .line 297
    invoke-static {v4}, Lcom/peel/widget/AdVideoView;->k(Lcom/peel/widget/AdVideoView;)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    .line 296
    invoke-virtual/range {v0 .. v5}, Lcom/peel/util/a/f;->a(IIILjava/lang/String;I)V

    .line 298
    iget-object v0, p0, Lcom/peel/widget/b;->a:Lcom/peel/widget/AdVideoView;

    invoke-virtual {v0}, Lcom/peel/widget/AdVideoView;->getContext()Landroid/content/Context;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    const-string/jumbo v2, "android.intent.action.VIEW"

    iget-object v3, p0, Lcom/peel/widget/b;->a:Lcom/peel/widget/AdVideoView;

    invoke-static {v3}, Lcom/peel/widget/AdVideoView;->k(Lcom/peel/widget/AdVideoView;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 302
    :goto_1
    return-void

    .line 296
    :cond_1
    sget-object v1, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v1}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/data/at;->f()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    goto :goto_0

    .line 299
    :catch_0
    move-exception v0

    .line 300
    invoke-static {}, Lcom/peel/widget/AdVideoView;->h()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "Fail to launch banner onClick url"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method
