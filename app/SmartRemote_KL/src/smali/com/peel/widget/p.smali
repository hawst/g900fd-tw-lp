.class Lcom/peel/widget/p;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic b:Lcom/peel/ui/model/DFPAd;

.field final synthetic c:Lcom/peel/widget/m;


# direct methods
.method constructor <init>(Lcom/peel/widget/m;Ljava/lang/String;Lcom/peel/ui/model/DFPAd;)V
    .locals 0

    .prologue
    .line 331
    iput-object p1, p0, Lcom/peel/widget/p;->c:Lcom/peel/widget/m;

    iput-object p2, p0, Lcom/peel/widget/p;->a:Ljava/lang/String;

    iput-object p3, p0, Lcom/peel/widget/p;->b:Lcom/peel/ui/model/DFPAd;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 10

    .prologue
    const/4 v5, 0x0

    const/16 v9, 0xa

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 334
    const-string/jumbo v0, "DFPAdView"

    iget-object v1, p0, Lcom/peel/widget/p;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 336
    iget-object v0, p0, Lcom/peel/widget/p;->c:Lcom/peel/widget/m;

    invoke-static {v0}, Lcom/peel/widget/m;->d(Lcom/peel/widget/m;)Landroid/webkit/WebView;

    move-result-object v0

    invoke-virtual {v0, v8}, Landroid/webkit/WebView;->clearCache(Z)V

    .line 337
    iget-object v0, p0, Lcom/peel/widget/p;->c:Lcom/peel/widget/m;

    invoke-static {v0}, Lcom/peel/widget/m;->d(Lcom/peel/widget/m;)Landroid/webkit/WebView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/webkit/WebView;->clearFormData()V

    .line 338
    iget-object v0, p0, Lcom/peel/widget/p;->c:Lcom/peel/widget/m;

    invoke-static {v0}, Lcom/peel/widget/m;->d(Lcom/peel/widget/m;)Landroid/webkit/WebView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/webkit/WebView;->clearHistory()V

    .line 340
    invoke-static {}, Lcom/peel/widget/m;->c()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    .line 341
    iget-object v0, p0, Lcom/peel/widget/p;->c:Lcom/peel/widget/m;

    invoke-static {v0}, Lcom/peel/widget/m;->d(Lcom/peel/widget/m;)Landroid/webkit/WebView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    invoke-virtual {v0}, Landroid/webkit/WebSettings;->getUserAgentString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/peel/widget/m;->a(Ljava/lang/String;)Ljava/lang/String;

    .line 344
    :cond_0
    iget-object v0, p0, Lcom/peel/widget/p;->b:Lcom/peel/ui/model/DFPAd;

    iget-object v0, v0, Lcom/peel/ui/model/DFPAd;->e:Ljava/util/Map;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/peel/widget/p;->b:Lcom/peel/ui/model/DFPAd;

    iget-object v0, v0, Lcom/peel/ui/model/DFPAd;->e:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    if-lez v0, :cond_2

    .line 345
    const-string/jumbo v0, ""

    .line 347
    iget-object v1, p0, Lcom/peel/widget/p;->b:Lcom/peel/ui/model/DFPAd;

    iget-object v1, v1, Lcom/peel/ui/model/DFPAd;->e:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move-object v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 348
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v3, "\"%1$s\":\"%2$s\","

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v6

    aput-object v6, v4, v7

    .line 349
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    aput-object v0, v4, v8

    .line 348
    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    .line 350
    goto :goto_0

    .line 352
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, " #$#{"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 354
    iget-object v1, p0, Lcom/peel/widget/p;->c:Lcom/peel/widget/m;

    invoke-static {v1}, Lcom/peel/widget/m;->d(Lcom/peel/widget/m;)Landroid/webkit/WebView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/peel/widget/m;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/webkit/WebSettings;->setUserAgentString(Ljava/lang/String;)V

    .line 357
    :cond_2
    iget-object v0, p0, Lcom/peel/widget/p;->c:Lcom/peel/widget/m;

    invoke-static {v0}, Lcom/peel/widget/m;->d(Lcom/peel/widget/m;)Landroid/webkit/WebView;

    move-result-object v0

    const-string/jumbo v1, "http://www.googletagservices.com/tag/js/"

    iget-object v2, p0, Lcom/peel/widget/p;->a:Ljava/lang/String;

    const-string/jumbo v3, "text/html"

    const-string/jumbo v4, "utf-8"

    invoke-virtual/range {v0 .. v5}, Landroid/webkit/WebView;->loadDataWithBaseURL(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 358
    iget-object v0, p0, Lcom/peel/widget/p;->b:Lcom/peel/ui/model/DFPAd;

    iget-object v0, v0, Lcom/peel/ui/model/DFPAd;->f:Ljava/lang/String;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/peel/widget/p;->b:Lcom/peel/ui/model/DFPAd;

    iget-object v0, v0, Lcom/peel/ui/model/DFPAd;->h:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "interstitial"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 360
    new-instance v1, Landroid/widget/ImageButton;

    iget-object v0, p0, Lcom/peel/widget/p;->c:Lcom/peel/widget/m;

    invoke-static {v0}, Lcom/peel/widget/m;->g(Lcom/peel/widget/m;)Landroid/app/Activity;

    move-result-object v0

    invoke-direct {v1, v0}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    .line 361
    iget-object v0, p0, Lcom/peel/widget/p;->c:Lcom/peel/widget/m;

    iget-object v0, v0, Lcom/peel/widget/m;->a:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 362
    invoke-virtual {v1}, Landroid/widget/ImageButton;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 363
    const/16 v2, 0xb

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 364
    const/16 v2, 0xc

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 365
    invoke-virtual {v0, v7, v7, v9, v9}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 366
    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 367
    const/16 v0, 0x8

    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 368
    invoke-virtual {v1, v5}, Landroid/widget/ImageButton;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 370
    :cond_3
    return-void
.end method
