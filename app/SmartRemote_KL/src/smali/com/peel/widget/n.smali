.class Lcom/peel/widget/n;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/view/View$OnTouchListener;


# instance fields
.field final synthetic a:Lcom/peel/widget/m;

.field private b:I


# direct methods
.method constructor <init>(Lcom/peel/widget/m;)V
    .locals 1

    .prologue
    .line 97
    iput-object p1, p0, Lcom/peel/widget/n;->a:Lcom/peel/widget/m;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 104
    const/4 v0, 0x0

    iput v0, p0, Lcom/peel/widget/n;->b:I

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 5

    .prologue
    const/4 v4, 0x3

    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v1, 0x2

    .line 109
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 135
    iput v4, p0, Lcom/peel/widget/n;->b:I

    .line 139
    :goto_0
    return v2

    .line 112
    :pswitch_0
    iget v0, p0, Lcom/peel/widget/n;->b:I

    if-nez v0, :cond_0

    iput v3, p0, Lcom/peel/widget/n;->b:I

    .line 114
    :goto_1
    const-string/jumbo v0, "DFPAdView"

    const-string/jumbo v1, "Ad Action Down"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 115
    iget-object v0, p0, Lcom/peel/widget/n;->a:Lcom/peel/widget/m;

    invoke-static {v0, v3}, Lcom/peel/widget/m;->a(Lcom/peel/widget/m;Z)Z

    goto :goto_0

    .line 113
    :cond_0
    iput v4, p0, Lcom/peel/widget/n;->b:I

    goto :goto_1

    .line 119
    :pswitch_1
    iget v0, p0, Lcom/peel/widget/n;->b:I

    if-eq v0, v1, :cond_1

    .line 120
    iput v2, p0, Lcom/peel/widget/n;->b:I

    .line 122
    const-string/jumbo v0, "DFPAdView"

    const-string/jumbo v1, "Ad Action Up"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 123
    :cond_1
    iget v0, p0, Lcom/peel/widget/n;->b:I

    if-ne v0, v1, :cond_2

    iput v2, p0, Lcom/peel/widget/n;->b:I

    goto :goto_0

    .line 124
    :cond_2
    iput v4, p0, Lcom/peel/widget/n;->b:I

    goto :goto_0

    .line 128
    :pswitch_2
    iget v0, p0, Lcom/peel/widget/n;->b:I

    if-eq v0, v3, :cond_3

    iget v0, p0, Lcom/peel/widget/n;->b:I

    if-ne v0, v1, :cond_4

    .line 129
    :cond_3
    iput v1, p0, Lcom/peel/widget/n;->b:I

    .line 130
    :cond_4
    const-string/jumbo v0, "DFPAdView"

    const-string/jumbo v1, "Ad Action Move"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 109
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
