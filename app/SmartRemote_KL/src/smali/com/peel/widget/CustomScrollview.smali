.class public Lcom/peel/widget/CustomScrollview;
.super Landroid/widget/ScrollView;


# instance fields
.field a:Lcom/peel/widget/j;

.field b:Lcom/peel/widget/l;

.field c:Lcom/peel/widget/k;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0, p1}, Landroid/widget/ScrollView;-><init>(Landroid/content/Context;)V

    .line 29
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0, p1, p2}, Landroid/widget/ScrollView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 25
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ScrollView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 21
    return-void
.end method


# virtual methods
.method public getOnBottomReachedListener()Lcom/peel/widget/j;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/peel/widget/CustomScrollview;->a:Lcom/peel/widget/j;

    return-object v0
.end method

.method public getOnMiddleReachedListener()Lcom/peel/widget/k;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/peel/widget/CustomScrollview;->c:Lcom/peel/widget/k;

    return-object v0
.end method

.method public getOnTopReachedListener()Lcom/peel/widget/l;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lcom/peel/widget/CustomScrollview;->b:Lcom/peel/widget/l;

    return-object v0
.end method

.method protected onScrollChanged(IIII)V
    .locals 3

    .prologue
    .line 34
    invoke-virtual {p0}, Lcom/peel/widget/CustomScrollview;->getChildCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p0, v0}, Lcom/peel/widget/CustomScrollview;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 35
    invoke-virtual {v0}, Landroid/view/View;->getBottom()I

    move-result v0

    invoke-virtual {p0}, Lcom/peel/widget/CustomScrollview;->getHeight()I

    move-result v1

    invoke-virtual {p0}, Lcom/peel/widget/CustomScrollview;->getScrollY()I

    move-result v2

    add-int/2addr v1, v2

    sub-int/2addr v0, v1

    .line 38
    if-nez v0, :cond_1

    .line 40
    iget-object v0, p0, Lcom/peel/widget/CustomScrollview;->a:Lcom/peel/widget/j;

    if-eqz v0, :cond_0

    .line 42
    iget-object v0, p0, Lcom/peel/widget/CustomScrollview;->a:Lcom/peel/widget/j;

    invoke-interface {v0}, Lcom/peel/widget/j;->a()V

    .line 59
    :cond_0
    :goto_0
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/ScrollView;->onScrollChanged(IIII)V

    .line 60
    return-void

    .line 45
    :cond_1
    if-nez p2, :cond_2

    .line 47
    iget-object v0, p0, Lcom/peel/widget/CustomScrollview;->b:Lcom/peel/widget/l;

    if-eqz v0, :cond_0

    .line 49
    iget-object v0, p0, Lcom/peel/widget/CustomScrollview;->b:Lcom/peel/widget/l;

    invoke-interface {v0}, Lcom/peel/widget/l;->a()V

    goto :goto_0

    .line 54
    :cond_2
    iget-object v0, p0, Lcom/peel/widget/CustomScrollview;->c:Lcom/peel/widget/k;

    if-eqz v0, :cond_0

    .line 56
    iget-object v0, p0, Lcom/peel/widget/CustomScrollview;->c:Lcom/peel/widget/k;

    invoke-interface {v0}, Lcom/peel/widget/k;->a()V

    goto :goto_0
.end method

.method public setOnBottomReachedListener(Lcom/peel/widget/j;)V
    .locals 0

    .prologue
    .line 76
    iput-object p1, p0, Lcom/peel/widget/CustomScrollview;->a:Lcom/peel/widget/j;

    .line 77
    return-void
.end method

.method public setOnMiddleReachedListener(Lcom/peel/widget/k;)V
    .locals 0

    .prologue
    .line 85
    iput-object p1, p0, Lcom/peel/widget/CustomScrollview;->c:Lcom/peel/widget/k;

    .line 86
    return-void
.end method

.method public setOnTopReachedListener(Lcom/peel/widget/l;)V
    .locals 0

    .prologue
    .line 80
    iput-object p1, p0, Lcom/peel/widget/CustomScrollview;->b:Lcom/peel/widget/l;

    .line 81
    return-void
.end method
