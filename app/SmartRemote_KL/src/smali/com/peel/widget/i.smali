.class Lcom/peel/widget/i;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/peel/widget/AdVideoView;


# direct methods
.method constructor <init>(Lcom/peel/widget/AdVideoView;)V
    .locals 0

    .prologue
    .line 367
    iput-object p1, p0, Lcom/peel/widget/i;->a:Lcom/peel/widget/AdVideoView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    .line 370
    iget-object v0, p0, Lcom/peel/widget/i;->a:Lcom/peel/widget/AdVideoView;

    invoke-static {v0}, Lcom/peel/widget/AdVideoView;->m(Lcom/peel/widget/AdVideoView;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/peel/widget/i;->a:Lcom/peel/widget/AdVideoView;

    invoke-static {v0}, Lcom/peel/widget/AdVideoView;->m(Lcom/peel/widget/AdVideoView;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 372
    :try_start_0
    iget-object v0, p0, Lcom/peel/widget/i;->a:Lcom/peel/widget/AdVideoView;

    invoke-static {v0}, Lcom/peel/widget/AdVideoView;->d(Lcom/peel/widget/AdVideoView;)Ljava/util/Map;

    move-result-object v0

    const-string/jumbo v2, "on_video_click"

    invoke-interface {v0, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 373
    iget-object v2, p0, Lcom/peel/widget/i;->a:Lcom/peel/widget/AdVideoView;

    iget-object v0, p0, Lcom/peel/widget/i;->a:Lcom/peel/widget/AdVideoView;

    invoke-static {v0}, Lcom/peel/widget/AdVideoView;->d(Lcom/peel/widget/AdVideoView;)Ljava/util/Map;

    move-result-object v0

    const-string/jumbo v3, "on_video_click"

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v2, v0}, Lcom/peel/widget/AdVideoView;->a(Lcom/peel/widget/AdVideoView;Ljava/lang/String;)V

    .line 375
    :cond_0
    invoke-static {}, Lcom/peel/util/a/f;->a()Lcom/peel/util/a/f;

    move-result-object v0

    sget-object v2, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v2}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v2

    if-nez v2, :cond_1

    :goto_0
    const/16 v2, 0x857

    const/16 v3, 0x1770

    iget-object v4, p0, Lcom/peel/widget/i;->a:Lcom/peel/widget/AdVideoView;

    .line 376
    invoke-static {v4}, Lcom/peel/widget/AdVideoView;->m(Lcom/peel/widget/AdVideoView;)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    .line 375
    invoke-virtual/range {v0 .. v5}, Lcom/peel/util/a/f;->a(IIILjava/lang/String;I)V

    .line 377
    iget-object v0, p0, Lcom/peel/widget/i;->a:Lcom/peel/widget/AdVideoView;

    invoke-virtual {v0}, Lcom/peel/widget/AdVideoView;->getContext()Landroid/content/Context;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    const-string/jumbo v2, "android.intent.action.VIEW"

    iget-object v3, p0, Lcom/peel/widget/i;->a:Lcom/peel/widget/AdVideoView;

    invoke-static {v3}, Lcom/peel/widget/AdVideoView;->m(Lcom/peel/widget/AdVideoView;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 384
    :goto_1
    return-void

    .line 375
    :cond_1
    sget-object v1, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v1}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/data/at;->f()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    goto :goto_0

    .line 378
    :catch_0
    move-exception v0

    .line 379
    invoke-static {}, Lcom/peel/widget/AdVideoView;->h()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "Fail to launch video onClick url"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 382
    :cond_2
    iget-object v0, p0, Lcom/peel/widget/i;->a:Lcom/peel/widget/AdVideoView;

    invoke-virtual {v0, v1}, Lcom/peel/widget/AdVideoView;->setFullscreen(Z)V

    goto :goto_1
.end method
