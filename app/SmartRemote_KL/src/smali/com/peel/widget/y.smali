.class Lcom/peel/widget/y;
.super Landroid/database/DataSetObserver;


# instance fields
.field final synthetic a:Lcom/peel/widget/HorizontalListView;


# direct methods
.method constructor <init>(Lcom/peel/widget/HorizontalListView;)V
    .locals 0

    .prologue
    .line 123
    iput-object p1, p0, Lcom/peel/widget/y;->a:Lcom/peel/widget/HorizontalListView;

    invoke-direct {p0}, Landroid/database/DataSetObserver;-><init>()V

    return-void
.end method


# virtual methods
.method public onChanged()V
    .locals 3

    .prologue
    .line 127
    iget-object v1, p0, Lcom/peel/widget/y;->a:Lcom/peel/widget/HorizontalListView;

    monitor-enter v1

    .line 128
    :try_start_0
    iget-object v0, p0, Lcom/peel/widget/y;->a:Lcom/peel/widget/HorizontalListView;

    const/4 v2, 0x1

    invoke-static {v0, v2}, Lcom/peel/widget/HorizontalListView;->a(Lcom/peel/widget/HorizontalListView;Z)Z

    .line 129
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 130
    iget-object v0, p0, Lcom/peel/widget/y;->a:Lcom/peel/widget/HorizontalListView;

    iget-object v1, p0, Lcom/peel/widget/y;->a:Lcom/peel/widget/HorizontalListView;

    invoke-virtual {v1}, Lcom/peel/widget/HorizontalListView;->getEmptyView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/peel/widget/HorizontalListView;->setEmptyView(Landroid/view/View;)V

    .line 131
    iget-object v0, p0, Lcom/peel/widget/y;->a:Lcom/peel/widget/HorizontalListView;

    invoke-virtual {v0}, Lcom/peel/widget/HorizontalListView;->invalidate()V

    .line 132
    iget-object v0, p0, Lcom/peel/widget/y;->a:Lcom/peel/widget/HorizontalListView;

    invoke-virtual {v0}, Lcom/peel/widget/HorizontalListView;->requestLayout()V

    .line 133
    return-void

    .line 129
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public onInvalidated()V
    .locals 1

    .prologue
    .line 137
    iget-object v0, p0, Lcom/peel/widget/y;->a:Lcom/peel/widget/HorizontalListView;

    invoke-static {v0}, Lcom/peel/widget/HorizontalListView;->a(Lcom/peel/widget/HorizontalListView;)V

    .line 138
    iget-object v0, p0, Lcom/peel/widget/y;->a:Lcom/peel/widget/HorizontalListView;

    invoke-virtual {v0}, Lcom/peel/widget/HorizontalListView;->invalidate()V

    .line 139
    iget-object v0, p0, Lcom/peel/widget/y;->a:Lcom/peel/widget/HorizontalListView;

    invoke-virtual {v0}, Lcom/peel/widget/HorizontalListView;->requestLayout()V

    .line 140
    return-void
.end method
