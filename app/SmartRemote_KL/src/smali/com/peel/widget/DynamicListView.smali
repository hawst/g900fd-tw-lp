.class public Lcom/peel/widget/DynamicListView;
.super Landroid/widget/ListView;


# static fields
.field private static final x:Landroid/animation/TypeEvaluator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/animation/TypeEvaluator",
            "<",
            "Landroid/graphics/Rect;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public a:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<*>;"
        }
    .end annotation
.end field

.field private final b:I

.field private final c:I

.field private final d:I

.field private e:I

.field private f:I

.field private g:I

.field private h:I

.field private i:Z

.field private j:Z

.field private k:I

.field private final l:I

.field private m:J

.field private n:J

.field private o:J

.field private p:Landroid/graphics/drawable/BitmapDrawable;

.field private q:Landroid/graphics/Rect;

.field private r:Landroid/graphics/Rect;

.field private final s:I

.field private t:I

.field private u:Z

.field private v:I

.field private w:Landroid/widget/AdapterView$OnItemLongClickListener;

.field private y:Landroid/widget/AbsListView$OnScrollListener;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 494
    new-instance v0, Lcom/peel/widget/w;

    invoke-direct {v0}, Lcom/peel/widget/w;-><init>()V

    sput-object v0, Lcom/peel/widget/DynamicListView;->x:Landroid/animation/TypeEvaluator;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 6

    .prologue
    const/16 v3, 0xf

    const-wide/16 v4, -0x1

    const/4 v2, 0x0

    const/4 v1, -0x1

    .line 109
    invoke-direct {p0, p1}, Landroid/widget/ListView;-><init>(Landroid/content/Context;)V

    .line 66
    iput v3, p0, Lcom/peel/widget/DynamicListView;->b:I

    .line 67
    const/16 v0, 0x96

    iput v0, p0, Lcom/peel/widget/DynamicListView;->c:I

    .line 68
    iput v3, p0, Lcom/peel/widget/DynamicListView;->d:I

    .line 72
    iput v1, p0, Lcom/peel/widget/DynamicListView;->e:I

    .line 74
    iput v1, p0, Lcom/peel/widget/DynamicListView;->f:I

    .line 75
    iput v1, p0, Lcom/peel/widget/DynamicListView;->g:I

    .line 77
    iput v2, p0, Lcom/peel/widget/DynamicListView;->h:I

    .line 79
    iput-boolean v2, p0, Lcom/peel/widget/DynamicListView;->i:Z

    .line 80
    iput-boolean v2, p0, Lcom/peel/widget/DynamicListView;->j:Z

    .line 81
    iput v2, p0, Lcom/peel/widget/DynamicListView;->k:I

    .line 83
    iput v1, p0, Lcom/peel/widget/DynamicListView;->l:I

    .line 84
    iput-wide v4, p0, Lcom/peel/widget/DynamicListView;->m:J

    .line 85
    iput-wide v4, p0, Lcom/peel/widget/DynamicListView;->n:J

    .line 86
    iput-wide v4, p0, Lcom/peel/widget/DynamicListView;->o:J

    .line 92
    iput v1, p0, Lcom/peel/widget/DynamicListView;->s:I

    .line 93
    iput v1, p0, Lcom/peel/widget/DynamicListView;->t:I

    .line 95
    iput-boolean v2, p0, Lcom/peel/widget/DynamicListView;->u:Z

    .line 96
    iput v2, p0, Lcom/peel/widget/DynamicListView;->v:I

    .line 134
    new-instance v0, Lcom/peel/widget/s;

    invoke-direct {v0, p0}, Lcom/peel/widget/s;-><init>(Lcom/peel/widget/DynamicListView;)V

    iput-object v0, p0, Lcom/peel/widget/DynamicListView;->w:Landroid/widget/AdapterView$OnItemLongClickListener;

    .line 552
    new-instance v0, Lcom/peel/widget/x;

    invoke-direct {v0, p0}, Lcom/peel/widget/x;-><init>(Lcom/peel/widget/DynamicListView;)V

    iput-object v0, p0, Lcom/peel/widget/DynamicListView;->y:Landroid/widget/AbsListView$OnScrollListener;

    .line 110
    invoke-virtual {p0, p1}, Lcom/peel/widget/DynamicListView;->a(Landroid/content/Context;)V

    .line 111
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 6

    .prologue
    const/16 v3, 0xf

    const-wide/16 v4, -0x1

    const/4 v2, 0x0

    const/4 v1, -0x1

    .line 119
    invoke-direct {p0, p1, p2}, Landroid/widget/ListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 66
    iput v3, p0, Lcom/peel/widget/DynamicListView;->b:I

    .line 67
    const/16 v0, 0x96

    iput v0, p0, Lcom/peel/widget/DynamicListView;->c:I

    .line 68
    iput v3, p0, Lcom/peel/widget/DynamicListView;->d:I

    .line 72
    iput v1, p0, Lcom/peel/widget/DynamicListView;->e:I

    .line 74
    iput v1, p0, Lcom/peel/widget/DynamicListView;->f:I

    .line 75
    iput v1, p0, Lcom/peel/widget/DynamicListView;->g:I

    .line 77
    iput v2, p0, Lcom/peel/widget/DynamicListView;->h:I

    .line 79
    iput-boolean v2, p0, Lcom/peel/widget/DynamicListView;->i:Z

    .line 80
    iput-boolean v2, p0, Lcom/peel/widget/DynamicListView;->j:Z

    .line 81
    iput v2, p0, Lcom/peel/widget/DynamicListView;->k:I

    .line 83
    iput v1, p0, Lcom/peel/widget/DynamicListView;->l:I

    .line 84
    iput-wide v4, p0, Lcom/peel/widget/DynamicListView;->m:J

    .line 85
    iput-wide v4, p0, Lcom/peel/widget/DynamicListView;->n:J

    .line 86
    iput-wide v4, p0, Lcom/peel/widget/DynamicListView;->o:J

    .line 92
    iput v1, p0, Lcom/peel/widget/DynamicListView;->s:I

    .line 93
    iput v1, p0, Lcom/peel/widget/DynamicListView;->t:I

    .line 95
    iput-boolean v2, p0, Lcom/peel/widget/DynamicListView;->u:Z

    .line 96
    iput v2, p0, Lcom/peel/widget/DynamicListView;->v:I

    .line 134
    new-instance v0, Lcom/peel/widget/s;

    invoke-direct {v0, p0}, Lcom/peel/widget/s;-><init>(Lcom/peel/widget/DynamicListView;)V

    iput-object v0, p0, Lcom/peel/widget/DynamicListView;->w:Landroid/widget/AdapterView$OnItemLongClickListener;

    .line 552
    new-instance v0, Lcom/peel/widget/x;

    invoke-direct {v0, p0}, Lcom/peel/widget/x;-><init>(Lcom/peel/widget/DynamicListView;)V

    iput-object v0, p0, Lcom/peel/widget/DynamicListView;->y:Landroid/widget/AbsListView$OnScrollListener;

    .line 120
    invoke-virtual {p0, p1}, Lcom/peel/widget/DynamicListView;->a(Landroid/content/Context;)V

    .line 121
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 6

    .prologue
    const/16 v3, 0xf

    const-wide/16 v4, -0x1

    const/4 v2, 0x0

    const/4 v1, -0x1

    .line 114
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 66
    iput v3, p0, Lcom/peel/widget/DynamicListView;->b:I

    .line 67
    const/16 v0, 0x96

    iput v0, p0, Lcom/peel/widget/DynamicListView;->c:I

    .line 68
    iput v3, p0, Lcom/peel/widget/DynamicListView;->d:I

    .line 72
    iput v1, p0, Lcom/peel/widget/DynamicListView;->e:I

    .line 74
    iput v1, p0, Lcom/peel/widget/DynamicListView;->f:I

    .line 75
    iput v1, p0, Lcom/peel/widget/DynamicListView;->g:I

    .line 77
    iput v2, p0, Lcom/peel/widget/DynamicListView;->h:I

    .line 79
    iput-boolean v2, p0, Lcom/peel/widget/DynamicListView;->i:Z

    .line 80
    iput-boolean v2, p0, Lcom/peel/widget/DynamicListView;->j:Z

    .line 81
    iput v2, p0, Lcom/peel/widget/DynamicListView;->k:I

    .line 83
    iput v1, p0, Lcom/peel/widget/DynamicListView;->l:I

    .line 84
    iput-wide v4, p0, Lcom/peel/widget/DynamicListView;->m:J

    .line 85
    iput-wide v4, p0, Lcom/peel/widget/DynamicListView;->n:J

    .line 86
    iput-wide v4, p0, Lcom/peel/widget/DynamicListView;->o:J

    .line 92
    iput v1, p0, Lcom/peel/widget/DynamicListView;->s:I

    .line 93
    iput v1, p0, Lcom/peel/widget/DynamicListView;->t:I

    .line 95
    iput-boolean v2, p0, Lcom/peel/widget/DynamicListView;->u:Z

    .line 96
    iput v2, p0, Lcom/peel/widget/DynamicListView;->v:I

    .line 134
    new-instance v0, Lcom/peel/widget/s;

    invoke-direct {v0, p0}, Lcom/peel/widget/s;-><init>(Lcom/peel/widget/DynamicListView;)V

    iput-object v0, p0, Lcom/peel/widget/DynamicListView;->w:Landroid/widget/AdapterView$OnItemLongClickListener;

    .line 552
    new-instance v0, Lcom/peel/widget/x;

    invoke-direct {v0, p0}, Lcom/peel/widget/x;-><init>(Lcom/peel/widget/DynamicListView;)V

    iput-object v0, p0, Lcom/peel/widget/DynamicListView;->y:Landroid/widget/AbsListView$OnScrollListener;

    .line 115
    invoke-virtual {p0, p1}, Lcom/peel/widget/DynamicListView;->a(Landroid/content/Context;)V

    .line 116
    return-void
.end method

.method static synthetic a(Lcom/peel/widget/DynamicListView;)I
    .locals 1

    .prologue
    .line 64
    iget v0, p0, Lcom/peel/widget/DynamicListView;->g:I

    return v0
.end method

.method static synthetic a(Lcom/peel/widget/DynamicListView;I)I
    .locals 0

    .prologue
    .line 64
    iput p1, p0, Lcom/peel/widget/DynamicListView;->h:I

    return p1
.end method

.method static synthetic a(Lcom/peel/widget/DynamicListView;J)J
    .locals 1

    .prologue
    .line 64
    iput-wide p1, p0, Lcom/peel/widget/DynamicListView;->n:J

    return-wide p1
.end method

.method private a(Landroid/view/View;)Landroid/graphics/drawable/BitmapDrawable;
    .locals 7

    .prologue
    .line 162
    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v0

    .line 163
    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v1

    .line 164
    invoke-virtual {p1}, Landroid/view/View;->getTop()I

    move-result v2

    .line 165
    invoke-virtual {p1}, Landroid/view/View;->getLeft()I

    move-result v3

    .line 167
    invoke-direct {p0, p1}, Lcom/peel/widget/DynamicListView;->b(Landroid/view/View;)Landroid/graphics/Bitmap;

    move-result-object v4

    .line 169
    new-instance v5, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {p0}, Lcom/peel/widget/DynamicListView;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-direct {v5, v6, v4}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    .line 171
    new-instance v4, Landroid/graphics/Rect;

    add-int/2addr v0, v3

    add-int/2addr v1, v2

    invoke-direct {v4, v3, v2, v0, v1}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object v4, p0, Lcom/peel/widget/DynamicListView;->r:Landroid/graphics/Rect;

    .line 172
    new-instance v0, Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/peel/widget/DynamicListView;->r:Landroid/graphics/Rect;

    invoke-direct {v0, v1}, Landroid/graphics/Rect;-><init>(Landroid/graphics/Rect;)V

    iput-object v0, p0, Lcom/peel/widget/DynamicListView;->q:Landroid/graphics/Rect;

    .line 174
    iget-object v0, p0, Lcom/peel/widget/DynamicListView;->q:Landroid/graphics/Rect;

    invoke-virtual {v5, v0}, Landroid/graphics/drawable/BitmapDrawable;->setBounds(Landroid/graphics/Rect;)V

    .line 176
    return-object v5
.end method

.method static synthetic a(Lcom/peel/widget/DynamicListView;Landroid/graphics/drawable/BitmapDrawable;)Landroid/graphics/drawable/BitmapDrawable;
    .locals 0

    .prologue
    .line 64
    iput-object p1, p0, Lcom/peel/widget/DynamicListView;->p:Landroid/graphics/drawable/BitmapDrawable;

    return-object p1
.end method

.method static synthetic a(Lcom/peel/widget/DynamicListView;Landroid/view/View;)Landroid/graphics/drawable/BitmapDrawable;
    .locals 1

    .prologue
    .line 64
    invoke-direct {p0, p1}, Lcom/peel/widget/DynamicListView;->a(Landroid/view/View;)Landroid/graphics/drawable/BitmapDrawable;

    move-result-object v0

    return-object v0
.end method

.method private a()V
    .locals 12

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 348
    iget v0, p0, Lcom/peel/widget/DynamicListView;->e:I

    iget v1, p0, Lcom/peel/widget/DynamicListView;->f:I

    sub-int v6, v0, v1

    .line 349
    iget-object v0, p0, Lcom/peel/widget/DynamicListView;->r:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->top:I

    iget v1, p0, Lcom/peel/widget/DynamicListView;->h:I

    add-int/2addr v0, v1

    add-int v4, v0, v6

    .line 353
    iget-wide v0, p0, Lcom/peel/widget/DynamicListView;->o:J

    invoke-virtual {p0, v0, v1}, Lcom/peel/widget/DynamicListView;->a(J)Landroid/view/View;

    move-result-object v0

    .line 354
    iget-wide v8, p0, Lcom/peel/widget/DynamicListView;->n:J

    invoke-virtual {p0, v8, v9}, Lcom/peel/widget/DynamicListView;->a(J)Landroid/view/View;

    move-result-object v8

    .line 355
    iget-wide v10, p0, Lcom/peel/widget/DynamicListView;->m:J

    invoke-virtual {p0, v10, v11}, Lcom/peel/widget/DynamicListView;->a(J)Landroid/view/View;

    move-result-object v1

    .line 357
    if-eqz v0, :cond_3

    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v5

    if-le v4, v5, :cond_3

    move v7, v2

    .line 358
    :goto_0
    if-eqz v1, :cond_4

    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v5

    if-ge v4, v5, :cond_4

    .line 360
    :goto_1
    if-nez v7, :cond_0

    if-eqz v2, :cond_2

    .line 362
    :cond_0
    if-eqz v7, :cond_5

    iget-wide v4, p0, Lcom/peel/widget/DynamicListView;->o:J

    .line 363
    :goto_2
    if-eqz v7, :cond_1

    move-object v1, v0

    .line 364
    :cond_1
    invoke-virtual {p0, v8}, Lcom/peel/widget/DynamicListView;->getPositionForView(Landroid/view/View;)I

    move-result v0

    .line 366
    if-nez v1, :cond_6

    .line 367
    iget-wide v0, p0, Lcom/peel/widget/DynamicListView;->n:J

    invoke-direct {p0, v0, v1}, Lcom/peel/widget/DynamicListView;->c(J)V

    .line 409
    :cond_2
    :goto_3
    return-void

    :cond_3
    move v7, v3

    .line 357
    goto :goto_0

    :cond_4
    move v2, v3

    .line 358
    goto :goto_1

    .line 362
    :cond_5
    iget-wide v4, p0, Lcom/peel/widget/DynamicListView;->m:J

    goto :goto_2

    .line 371
    :cond_6
    iget-object v2, p0, Lcom/peel/widget/DynamicListView;->a:Ljava/util/ArrayList;

    invoke-virtual {p0, v1}, Lcom/peel/widget/DynamicListView;->getPositionForView(Landroid/view/View;)I

    move-result v7

    invoke-direct {p0, v2, v0, v7}, Lcom/peel/widget/DynamicListView;->a(Ljava/util/ArrayList;II)V

    .line 373
    invoke-virtual {p0}, Lcom/peel/widget/DynamicListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Landroid/widget/BaseAdapter;

    invoke-virtual {v0}, Landroid/widget/BaseAdapter;->notifyDataSetChanged()V

    .line 375
    iget v0, p0, Lcom/peel/widget/DynamicListView;->e:I

    iput v0, p0, Lcom/peel/widget/DynamicListView;->f:I

    .line 377
    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v7

    .line 379
    invoke-virtual {v8, v3}, Landroid/view/View;->setVisibility(I)V

    .line 380
    const/4 v0, 0x4

    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 382
    iget-wide v0, p0, Lcom/peel/widget/DynamicListView;->n:J

    invoke-direct {p0, v0, v1}, Lcom/peel/widget/DynamicListView;->c(J)V

    .line 384
    invoke-virtual {p0}, Lcom/peel/widget/DynamicListView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v3

    .line 385
    new-instance v1, Lcom/peel/widget/t;

    move-object v2, p0

    invoke-direct/range {v1 .. v7}, Lcom/peel/widget/t;-><init>(Lcom/peel/widget/DynamicListView;Landroid/view/ViewTreeObserver;JII)V

    invoke-virtual {v3, v1}, Landroid/view/ViewTreeObserver;->addOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    goto :goto_3
.end method

.method private a(Ljava/util/ArrayList;II)V
    .locals 2

    .prologue
    .line 412
    invoke-virtual {p1, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    .line 413
    invoke-virtual {p1, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {p1, p2, v1}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 414
    invoke-virtual {p1, p3, v0}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 415
    return-void
.end method

.method static synthetic a(Lcom/peel/widget/DynamicListView;Z)Z
    .locals 0

    .prologue
    .line 64
    iput-boolean p1, p0, Lcom/peel/widget/DynamicListView;->i:Z

    return p1
.end method

.method static synthetic b(Lcom/peel/widget/DynamicListView;)I
    .locals 1

    .prologue
    .line 64
    iget v0, p0, Lcom/peel/widget/DynamicListView;->f:I

    return v0
.end method

.method static synthetic b(Lcom/peel/widget/DynamicListView;I)I
    .locals 0

    .prologue
    .line 64
    iput p1, p0, Lcom/peel/widget/DynamicListView;->v:I

    return p1
.end method

.method private b(Landroid/view/View;)Landroid/graphics/Bitmap;
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x0

    .line 181
    invoke-direct {p0, p1}, Lcom/peel/widget/DynamicListView;->c(Landroid/view/View;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 182
    new-instance v1, Landroid/graphics/Canvas;

    invoke-direct {v1, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 184
    new-instance v2, Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    invoke-direct {v2, v6, v6, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 186
    new-instance v3, Landroid/graphics/Paint;

    invoke-direct {v3}, Landroid/graphics/Paint;-><init>()V

    .line 187
    sget-object v4, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 188
    const/high16 v4, 0x41700000    # 15.0f

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 189
    const/high16 v4, -0x1000000

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setColor(I)V

    .line 191
    const/4 v4, 0x0

    invoke-virtual {v1, v0, v5, v5, v4}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 192
    invoke-virtual {v1, v2, v3}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 194
    return-object v0
.end method

.method private b()V
    .locals 7

    .prologue
    const/4 v4, 0x1

    const/4 v6, 0x0

    .line 423
    iget-wide v0, p0, Lcom/peel/widget/DynamicListView;->n:J

    invoke-virtual {p0, v0, v1}, Lcom/peel/widget/DynamicListView;->a(J)Landroid/view/View;

    move-result-object v0

    .line 424
    iget-boolean v1, p0, Lcom/peel/widget/DynamicListView;->i:Z

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lcom/peel/widget/DynamicListView;->u:Z

    if-eqz v1, :cond_2

    .line 425
    :cond_0
    iput-boolean v6, p0, Lcom/peel/widget/DynamicListView;->i:Z

    .line 426
    iput-boolean v6, p0, Lcom/peel/widget/DynamicListView;->u:Z

    .line 427
    iput-boolean v6, p0, Lcom/peel/widget/DynamicListView;->j:Z

    .line 428
    const/4 v1, -0x1

    iput v1, p0, Lcom/peel/widget/DynamicListView;->t:I

    .line 433
    iget v1, p0, Lcom/peel/widget/DynamicListView;->v:I

    if-eqz v1, :cond_1

    .line 434
    iput-boolean v4, p0, Lcom/peel/widget/DynamicListView;->u:Z

    .line 469
    :goto_0
    return-void

    .line 438
    :cond_1
    iget-object v1, p0, Lcom/peel/widget/DynamicListView;->q:Landroid/graphics/Rect;

    iget-object v2, p0, Lcom/peel/widget/DynamicListView;->r:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->left:I

    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v3

    invoke-virtual {v1, v2, v3}, Landroid/graphics/Rect;->offsetTo(II)V

    .line 440
    iget-object v1, p0, Lcom/peel/widget/DynamicListView;->p:Landroid/graphics/drawable/BitmapDrawable;

    const-string/jumbo v2, "bounds"

    sget-object v3, Lcom/peel/widget/DynamicListView;->x:Landroid/animation/TypeEvaluator;

    new-array v4, v4, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/peel/widget/DynamicListView;->q:Landroid/graphics/Rect;

    aput-object v5, v4, v6

    invoke-static {v1, v2, v3, v4}, Landroid/animation/ObjectAnimator;->ofObject(Ljava/lang/Object;Ljava/lang/String;Landroid/animation/TypeEvaluator;[Ljava/lang/Object;)Landroid/animation/ObjectAnimator;

    move-result-object v1

    .line 442
    new-instance v2, Lcom/peel/widget/u;

    invoke-direct {v2, p0}, Lcom/peel/widget/u;-><init>(Lcom/peel/widget/DynamicListView;)V

    invoke-virtual {v1, v2}, Landroid/animation/ObjectAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 448
    new-instance v2, Lcom/peel/widget/v;

    invoke-direct {v2, p0, v0}, Lcom/peel/widget/v;-><init>(Lcom/peel/widget/DynamicListView;Landroid/view/View;)V

    invoke-virtual {v1, v2}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 465
    invoke-virtual {v1}, Landroid/animation/ObjectAnimator;->start()V

    goto :goto_0

    .line 467
    :cond_2
    invoke-direct {p0}, Lcom/peel/widget/DynamicListView;->c()V

    goto :goto_0
.end method

.method static synthetic b(Lcom/peel/widget/DynamicListView;J)V
    .locals 1

    .prologue
    .line 64
    invoke-direct {p0, p1, p2}, Lcom/peel/widget/DynamicListView;->c(J)V

    return-void
.end method

.method static synthetic c(Lcom/peel/widget/DynamicListView;)J
    .locals 2

    .prologue
    .line 64
    iget-wide v0, p0, Lcom/peel/widget/DynamicListView;->n:J

    return-wide v0
.end method

.method static synthetic c(Lcom/peel/widget/DynamicListView;J)J
    .locals 1

    .prologue
    .line 64
    iput-wide p1, p0, Lcom/peel/widget/DynamicListView;->m:J

    return-wide p1
.end method

.method private c(Landroid/view/View;)Landroid/graphics/Bitmap;
    .locals 3

    .prologue
    .line 199
    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v0

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v1

    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v1, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 200
    new-instance v1, Landroid/graphics/Canvas;

    invoke-direct {v1, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 201
    invoke-virtual {p1, v1}, Landroid/view/View;->draw(Landroid/graphics/Canvas;)V

    .line 202
    return-object v0
.end method

.method private c()V
    .locals 6

    .prologue
    const-wide/16 v4, -0x1

    const/4 v2, 0x0

    .line 475
    iget-wide v0, p0, Lcom/peel/widget/DynamicListView;->n:J

    invoke-virtual {p0, v0, v1}, Lcom/peel/widget/DynamicListView;->a(J)Landroid/view/View;

    move-result-object v0

    .line 476
    iget-boolean v1, p0, Lcom/peel/widget/DynamicListView;->i:Z

    if-eqz v1, :cond_0

    .line 477
    iput-wide v4, p0, Lcom/peel/widget/DynamicListView;->m:J

    .line 478
    iput-wide v4, p0, Lcom/peel/widget/DynamicListView;->n:J

    .line 479
    iput-wide v4, p0, Lcom/peel/widget/DynamicListView;->o:J

    .line 480
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 481
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/peel/widget/DynamicListView;->p:Landroid/graphics/drawable/BitmapDrawable;

    .line 482
    invoke-virtual {p0}, Lcom/peel/widget/DynamicListView;->invalidate()V

    .line 484
    :cond_0
    iput-boolean v2, p0, Lcom/peel/widget/DynamicListView;->i:Z

    .line 485
    iput-boolean v2, p0, Lcom/peel/widget/DynamicListView;->j:Z

    .line 486
    const/4 v0, -0x1

    iput v0, p0, Lcom/peel/widget/DynamicListView;->t:I

    .line 487
    return-void
.end method

.method private c(J)V
    .locals 5

    .prologue
    .line 212
    invoke-virtual {p0, p1, p2}, Lcom/peel/widget/DynamicListView;->b(J)I

    move-result v1

    .line 213
    invoke-virtual {p0}, Lcom/peel/widget/DynamicListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lcom/peel/widget/ap;

    .line 214
    add-int/lit8 v2, v1, -0x1

    invoke-virtual {v0, v2}, Lcom/peel/widget/ap;->getItemId(I)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/peel/widget/DynamicListView;->m:J

    .line 215
    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v1}, Lcom/peel/widget/ap;->getItemId(I)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/peel/widget/DynamicListView;->o:J

    .line 216
    return-void
.end method

.method static synthetic d(Lcom/peel/widget/DynamicListView;)I
    .locals 1

    .prologue
    .line 64
    iget v0, p0, Lcom/peel/widget/DynamicListView;->h:I

    return v0
.end method

.method static synthetic d(Lcom/peel/widget/DynamicListView;J)J
    .locals 1

    .prologue
    .line 64
    iput-wide p1, p0, Lcom/peel/widget/DynamicListView;->o:J

    return-wide p1
.end method

.method private d()V
    .locals 1

    .prologue
    .line 512
    iget-object v0, p0, Lcom/peel/widget/DynamicListView;->q:Landroid/graphics/Rect;

    invoke-virtual {p0, v0}, Lcom/peel/widget/DynamicListView;->a(Landroid/graphics/Rect;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/peel/widget/DynamicListView;->j:Z

    .line 513
    return-void
.end method

.method static synthetic e(Lcom/peel/widget/DynamicListView;)Z
    .locals 1

    .prologue
    .line 64
    iget-boolean v0, p0, Lcom/peel/widget/DynamicListView;->i:Z

    return v0
.end method

.method static synthetic f(Lcom/peel/widget/DynamicListView;)Z
    .locals 1

    .prologue
    .line 64
    iget-boolean v0, p0, Lcom/peel/widget/DynamicListView;->j:Z

    return v0
.end method

.method static synthetic g(Lcom/peel/widget/DynamicListView;)V
    .locals 0

    .prologue
    .line 64
    invoke-direct {p0}, Lcom/peel/widget/DynamicListView;->d()V

    return-void
.end method

.method static synthetic h(Lcom/peel/widget/DynamicListView;)Z
    .locals 1

    .prologue
    .line 64
    iget-boolean v0, p0, Lcom/peel/widget/DynamicListView;->u:Z

    return v0
.end method

.method static synthetic i(Lcom/peel/widget/DynamicListView;)V
    .locals 0

    .prologue
    .line 64
    invoke-direct {p0}, Lcom/peel/widget/DynamicListView;->b()V

    return-void
.end method

.method static synthetic j(Lcom/peel/widget/DynamicListView;)V
    .locals 0

    .prologue
    .line 64
    invoke-direct {p0}, Lcom/peel/widget/DynamicListView;->a()V

    return-void
.end method


# virtual methods
.method public a(J)Landroid/view/View;
    .locals 7

    .prologue
    .line 220
    invoke-virtual {p0}, Lcom/peel/widget/DynamicListView;->getFirstVisiblePosition()I

    move-result v3

    .line 221
    invoke-virtual {p0}, Lcom/peel/widget/DynamicListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lcom/peel/widget/ap;

    .line 222
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0}, Lcom/peel/widget/DynamicListView;->getChildCount()I

    move-result v2

    if-ge v1, v2, :cond_1

    .line 223
    invoke-virtual {p0, v1}, Lcom/peel/widget/DynamicListView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 224
    add-int v4, v3, v1

    .line 225
    invoke-virtual {v0, v4}, Lcom/peel/widget/ap;->getItemId(I)J

    move-result-wide v4

    .line 226
    cmp-long v4, v4, p1

    if-nez v4, :cond_0

    move-object v0, v2

    .line 230
    :goto_1
    return-object v0

    .line 222
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 230
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public a(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 124
    iget-object v0, p0, Lcom/peel/widget/DynamicListView;->w:Landroid/widget/AdapterView$OnItemLongClickListener;

    invoke-virtual {p0, v0}, Lcom/peel/widget/DynamicListView;->setOnItemLongClickListener(Landroid/widget/AdapterView$OnItemLongClickListener;)V

    .line 125
    iget-object v0, p0, Lcom/peel/widget/DynamicListView;->y:Landroid/widget/AbsListView$OnScrollListener;

    invoke-virtual {p0, v0}, Lcom/peel/widget/DynamicListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 126
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    .line 127
    const/high16 v1, 0x41700000    # 15.0f

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    div-float v0, v1, v0

    float-to-int v0, v0

    iput v0, p0, Lcom/peel/widget/DynamicListView;->k:I

    .line 128
    return-void
.end method

.method public a(Landroid/graphics/Rect;)Z
    .locals 8

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 521
    invoke-virtual {p0}, Lcom/peel/widget/DynamicListView;->computeVerticalScrollOffset()I

    move-result v2

    .line 522
    invoke-virtual {p0}, Lcom/peel/widget/DynamicListView;->getHeight()I

    move-result v3

    .line 523
    invoke-virtual {p0}, Lcom/peel/widget/DynamicListView;->computeVerticalScrollExtent()I

    move-result v4

    .line 524
    invoke-virtual {p0}, Lcom/peel/widget/DynamicListView;->computeVerticalScrollRange()I

    move-result v5

    .line 525
    iget v6, p1, Landroid/graphics/Rect;->top:I

    .line 526
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result v7

    .line 528
    if-gtz v6, :cond_0

    if-lez v2, :cond_0

    .line 529
    iget v2, p0, Lcom/peel/widget/DynamicListView;->k:I

    neg-int v2, v2

    invoke-virtual {p0, v2, v1}, Lcom/peel/widget/DynamicListView;->smoothScrollBy(II)V

    .line 538
    :goto_0
    return v0

    .line 533
    :cond_0
    add-int/2addr v6, v7

    if-lt v6, v3, :cond_1

    add-int/2addr v2, v4

    if-ge v2, v5, :cond_1

    .line 534
    iget v2, p0, Lcom/peel/widget/DynamicListView;->k:I

    invoke-virtual {p0, v2, v1}, Lcom/peel/widget/DynamicListView;->smoothScrollBy(II)V

    goto :goto_0

    :cond_1
    move v0, v1

    .line 538
    goto :goto_0
.end method

.method public b(J)I
    .locals 1

    .prologue
    .line 235
    invoke-virtual {p0, p1, p2}, Lcom/peel/widget/DynamicListView;->a(J)Landroid/view/View;

    move-result-object v0

    .line 236
    if-nez v0, :cond_0

    .line 237
    const/4 v0, -0x1

    .line 239
    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0, v0}, Lcom/peel/widget/DynamicListView;->getPositionForView(Landroid/view/View;)I

    move-result v0

    goto :goto_0
.end method

.method protected dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 1

    .prologue
    .line 250
    invoke-super {p0, p1}, Landroid/widget/ListView;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 251
    iget-object v0, p0, Lcom/peel/widget/DynamicListView;->p:Landroid/graphics/drawable/BitmapDrawable;

    if-eqz v0, :cond_0

    .line 252
    iget-object v0, p0, Lcom/peel/widget/DynamicListView;->p:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/BitmapDrawable;->draw(Landroid/graphics/Canvas;)V

    .line 254
    :cond_0
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 259
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    and-int/lit16 v1, v1, 0xff

    packed-switch v1, :pswitch_data_0

    .line 335
    :cond_0
    :goto_0
    :pswitch_0
    invoke-super {p0, p1}, Landroid/widget/ListView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    :goto_1
    return v0

    .line 261
    :pswitch_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    float-to-int v1, v1

    iput v1, p0, Lcom/peel/widget/DynamicListView;->g:I

    .line 262
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    float-to-int v1, v1

    iput v1, p0, Lcom/peel/widget/DynamicListView;->f:I

    .line 263
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v0

    iput v0, p0, Lcom/peel/widget/DynamicListView;->t:I

    goto :goto_0

    .line 274
    :pswitch_2
    iget v1, p0, Lcom/peel/widget/DynamicListView;->t:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    .line 278
    iget v1, p0, Lcom/peel/widget/DynamicListView;->t:I

    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->findPointerIndex(I)I

    move-result v1

    .line 280
    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->getY(I)F

    move-result v1

    float-to-int v1, v1

    iput v1, p0, Lcom/peel/widget/DynamicListView;->e:I

    .line 281
    iget v1, p0, Lcom/peel/widget/DynamicListView;->e:I

    iget v2, p0, Lcom/peel/widget/DynamicListView;->f:I

    sub-int/2addr v1, v2

    .line 284
    iget-boolean v2, p0, Lcom/peel/widget/DynamicListView;->i:Z

    if-eqz v2, :cond_0

    .line 285
    iget-object v2, p0, Lcom/peel/widget/DynamicListView;->q:Landroid/graphics/Rect;

    iget-object v3, p0, Lcom/peel/widget/DynamicListView;->r:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->left:I

    iget-object v4, p0, Lcom/peel/widget/DynamicListView;->r:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->top:I

    add-int/2addr v1, v4

    iget v4, p0, Lcom/peel/widget/DynamicListView;->h:I

    add-int/2addr v1, v4

    invoke-virtual {v2, v3, v1}, Landroid/graphics/Rect;->offsetTo(II)V

    .line 287
    iget-object v1, p0, Lcom/peel/widget/DynamicListView;->p:Landroid/graphics/drawable/BitmapDrawable;

    iget-object v2, p0, Lcom/peel/widget/DynamicListView;->q:Landroid/graphics/Rect;

    invoke-virtual {v1, v2}, Landroid/graphics/drawable/BitmapDrawable;->setBounds(Landroid/graphics/Rect;)V

    .line 288
    invoke-virtual {p0}, Lcom/peel/widget/DynamicListView;->invalidate()V

    .line 290
    invoke-direct {p0}, Lcom/peel/widget/DynamicListView;->a()V

    .line 292
    iput-boolean v0, p0, Lcom/peel/widget/DynamicListView;->j:Z

    .line 293
    invoke-direct {p0}, Lcom/peel/widget/DynamicListView;->d()V

    goto :goto_1

    .line 308
    :pswitch_3
    invoke-direct {p0}, Lcom/peel/widget/DynamicListView;->b()V

    goto :goto_0

    .line 314
    :pswitch_4
    invoke-direct {p0}, Lcom/peel/widget/DynamicListView;->c()V

    goto :goto_0

    .line 321
    :pswitch_5
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    const v1, 0xff00

    and-int/2addr v0, v1

    shr-int/lit8 v0, v0, 0x8

    .line 323
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v0

    .line 324
    iget v1, p0, Lcom/peel/widget/DynamicListView;->t:I

    if-ne v0, v1, :cond_0

    .line 325
    invoke-direct {p0}, Lcom/peel/widget/DynamicListView;->b()V

    goto :goto_0

    .line 259
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_3
        :pswitch_2
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_5
    .end packed-switch
.end method

.method public setCheeseList(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 542
    iput-object p1, p0, Lcom/peel/widget/DynamicListView;->a:Ljava/util/ArrayList;

    .line 543
    return-void
.end method
