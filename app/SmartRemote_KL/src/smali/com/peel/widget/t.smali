.class Lcom/peel/widget/t;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/view/ViewTreeObserver$OnPreDrawListener;


# instance fields
.field final synthetic a:Landroid/view/ViewTreeObserver;

.field final synthetic b:J

.field final synthetic c:I

.field final synthetic d:I

.field final synthetic e:Lcom/peel/widget/DynamicListView;


# direct methods
.method constructor <init>(Lcom/peel/widget/DynamicListView;Landroid/view/ViewTreeObserver;JII)V
    .locals 1

    .prologue
    .line 385
    iput-object p1, p0, Lcom/peel/widget/t;->e:Lcom/peel/widget/DynamicListView;

    iput-object p2, p0, Lcom/peel/widget/t;->a:Landroid/view/ViewTreeObserver;

    iput-wide p3, p0, Lcom/peel/widget/t;->b:J

    iput p5, p0, Lcom/peel/widget/t;->c:I

    iput p6, p0, Lcom/peel/widget/t;->d:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPreDraw()Z
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 387
    iget-object v0, p0, Lcom/peel/widget/t;->a:Landroid/view/ViewTreeObserver;

    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->removeOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    .line 389
    iget-object v0, p0, Lcom/peel/widget/t;->e:Lcom/peel/widget/DynamicListView;

    iget-wide v2, p0, Lcom/peel/widget/t;->b:J

    invoke-virtual {v0, v2, v3}, Lcom/peel/widget/DynamicListView;->a(J)Landroid/view/View;

    move-result-object v0

    .line 391
    iget-object v1, p0, Lcom/peel/widget/t;->e:Lcom/peel/widget/DynamicListView;

    iget-object v2, p0, Lcom/peel/widget/t;->e:Lcom/peel/widget/DynamicListView;

    invoke-static {v2}, Lcom/peel/widget/DynamicListView;->d(Lcom/peel/widget/DynamicListView;)I

    move-result v2

    iget v3, p0, Lcom/peel/widget/t;->c:I

    add-int/2addr v2, v3

    invoke-static {v1, v2}, Lcom/peel/widget/DynamicListView;->a(Lcom/peel/widget/DynamicListView;I)I

    .line 393
    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v1

    .line 394
    iget v2, p0, Lcom/peel/widget/t;->d:I

    sub-int v1, v2, v1

    .line 396
    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setTranslationY(F)V

    .line 398
    sget-object v1, Landroid/view/View;->TRANSLATION_Y:Landroid/util/Property;

    new-array v2, v5, [F

    const/4 v3, 0x0

    const/4 v4, 0x0

    aput v4, v2, v3

    invoke-static {v0, v1, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 400
    const-wide/16 v2, 0x96

    invoke-virtual {v0, v2, v3}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 401
    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    .line 403
    return v5
.end method
