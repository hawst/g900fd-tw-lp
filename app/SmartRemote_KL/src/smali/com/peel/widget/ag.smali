.class public Lcom/peel/widget/ag;
.super Landroid/app/Dialog;

# interfaces
.implements Landroid/content/DialogInterface;


# static fields
.field private static t:Z

.field private static u:Lcom/peel/widget/ag;


# instance fields
.field private a:Landroid/widget/TextView;

.field private b:Landroid/widget/TextView;

.field private c:Landroid/widget/LinearLayout;

.field private d:Landroid/widget/LinearLayout;

.field private e:Landroid/widget/LinearLayout;

.field private f:Landroid/widget/LinearLayout;

.field private g:Landroid/widget/Button;

.field h:Z

.field i:Z

.field j:Z

.field private k:Landroid/widget/Button;

.field private l:Landroid/widget/Button;

.field private m:Landroid/widget/ImageView;

.field private n:Landroid/widget/ImageView;

.field private o:Landroid/content/DialogInterface$OnClickListener;

.field private p:Landroid/content/DialogInterface$OnClickListener;

.field private q:Landroid/content/DialogInterface$OnClickListener;

.field private r:Z

.field private s:Landroid/app/Activity;

.field private v:Landroid/view/View$OnClickListener;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 32
    const/4 v0, 0x0

    sput-object v0, Lcom/peel/widget/ag;->u:Lcom/peel/widget/ag;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 65
    invoke-direct {p0, p1}, Landroid/app/Dialog;-><init>(Landroid/content/Context;)V

    .line 34
    new-instance v0, Lcom/peel/widget/ah;

    invoke-direct {v0, p0}, Lcom/peel/widget/ah;-><init>(Lcom/peel/widget/ag;)V

    iput-object v0, p0, Lcom/peel/widget/ag;->v:Landroid/view/View$OnClickListener;

    .line 66
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/peel/widget/ag;->requestWindowFeature(I)Z

    .line 67
    check-cast p1, Landroid/app/Activity;

    iput-object p1, p0, Lcom/peel/widget/ag;->s:Landroid/app/Activity;

    .line 68
    sget v0, Lcom/peel/ui/fq;->popup_dialog:I

    invoke-virtual {p0, v0}, Lcom/peel/widget/ag;->setContentView(I)V

    .line 70
    sget v0, Lcom/peel/ui/fp;->topPanel:I

    invoke-virtual {p0, v0}, Lcom/peel/widget/ag;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/peel/widget/ag;->c:Landroid/widget/LinearLayout;

    .line 71
    sget v0, Lcom/peel/ui/fp;->alertTitle:I

    invoke-virtual {p0, v0}, Lcom/peel/widget/ag;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/peel/widget/ag;->a:Landroid/widget/TextView;

    .line 72
    sget v0, Lcom/peel/ui/fp;->message:I

    invoke-virtual {p0, v0}, Lcom/peel/widget/ag;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/peel/widget/ag;->b:Landroid/widget/TextView;

    .line 73
    sget v0, Lcom/peel/ui/fp;->contentPanel:I

    invoke-virtual {p0, v0}, Lcom/peel/widget/ag;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/peel/widget/ag;->e:Landroid/widget/LinearLayout;

    .line 74
    sget v0, Lcom/peel/ui/fp;->customPanel:I

    invoke-virtual {p0, v0}, Lcom/peel/widget/ag;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/peel/widget/ag;->f:Landroid/widget/LinearLayout;

    .line 75
    sget v0, Lcom/peel/ui/fp;->buttonPanel:I

    invoke-virtual {p0, v0}, Lcom/peel/widget/ag;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/peel/widget/ag;->d:Landroid/widget/LinearLayout;

    .line 76
    sget v0, Lcom/peel/ui/fp;->button2:I

    invoke-virtual {p0, v0}, Lcom/peel/widget/ag;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/peel/widget/ag;->g:Landroid/widget/Button;

    .line 77
    sget v0, Lcom/peel/ui/fp;->button3:I

    invoke-virtual {p0, v0}, Lcom/peel/widget/ag;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/peel/widget/ag;->k:Landroid/widget/Button;

    .line 78
    sget v0, Lcom/peel/ui/fp;->button1:I

    invoke-virtual {p0, v0}, Lcom/peel/widget/ag;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/peel/widget/ag;->l:Landroid/widget/Button;

    .line 79
    sget v0, Lcom/peel/ui/fp;->line1:I

    invoke-virtual {p0, v0}, Lcom/peel/widget/ag;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/peel/widget/ag;->m:Landroid/widget/ImageView;

    .line 80
    sget v0, Lcom/peel/ui/fp;->line2:I

    invoke-virtual {p0, v0}, Lcom/peel/widget/ag;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/peel/widget/ag;->n:Landroid/widget/ImageView;

    .line 83
    iget-object v0, p0, Lcom/peel/widget/ag;->g:Landroid/widget/Button;

    iget-object v1, p0, Lcom/peel/widget/ag;->v:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 84
    iget-object v0, p0, Lcom/peel/widget/ag;->k:Landroid/widget/Button;

    iget-object v1, p0, Lcom/peel/widget/ag;->v:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 85
    iget-object v0, p0, Lcom/peel/widget/ag;->l:Landroid/widget/Button;

    iget-object v1, p0, Lcom/peel/widget/ag;->v:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 86
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/peel/widget/ag;->r:Z

    .line 87
    sput-object p0, Lcom/peel/widget/ag;->u:Lcom/peel/widget/ag;

    .line 88
    return-void
.end method

.method private a()V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 268
    .line 269
    iget-boolean v0, p0, Lcom/peel/widget/ag;->h:Z

    if-eqz v0, :cond_4

    const/4 v0, 0x1

    .line 270
    :goto_0
    iget-boolean v2, p0, Lcom/peel/widget/ag;->i:Z

    if-eqz v2, :cond_0

    add-int/lit8 v0, v0, 0x1

    .line 271
    :cond_0
    iget-boolean v2, p0, Lcom/peel/widget/ag;->j:Z

    if-eqz v2, :cond_1

    add-int/lit8 v0, v0, 0x1

    .line 272
    :cond_1
    const/4 v2, 0x2

    if-lt v0, v2, :cond_2

    .line 273
    iget-object v2, p0, Lcom/peel/widget/ag;->m:Landroid/widget/ImageView;

    invoke-virtual {v2, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 275
    :cond_2
    const/4 v2, 0x3

    if-ne v0, v2, :cond_3

    .line 276
    iget-object v0, p0, Lcom/peel/widget/ag;->n:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 278
    :cond_3
    return-void

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method static synthetic a(Lcom/peel/widget/ag;Z)Z
    .locals 0

    .prologue
    .line 20
    iput-boolean p1, p0, Lcom/peel/widget/ag;->r:Z

    return p1
.end method

.method static synthetic b(Lcom/peel/widget/ag;)Z
    .locals 1

    .prologue
    .line 20
    iget-boolean v0, p0, Lcom/peel/widget/ag;->r:Z

    return v0
.end method

.method static synthetic c(Lcom/peel/widget/ag;)Landroid/content/DialogInterface$OnClickListener;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lcom/peel/widget/ag;->q:Landroid/content/DialogInterface$OnClickListener;

    return-object v0
.end method

.method static synthetic d(Lcom/peel/widget/ag;)Landroid/content/DialogInterface$OnClickListener;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lcom/peel/widget/ag;->p:Landroid/content/DialogInterface$OnClickListener;

    return-object v0
.end method

.method static synthetic e(Lcom/peel/widget/ag;)Landroid/content/DialogInterface$OnClickListener;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lcom/peel/widget/ag;->o:Landroid/content/DialogInterface$OnClickListener;

    return-object v0
.end method

.method public static g()Z
    .locals 1

    .prologue
    .line 281
    sget-boolean v0, Lcom/peel/widget/ag;->t:Z

    return v0
.end method

.method public static h()V
    .locals 1

    .prologue
    .line 284
    sget-object v0, Lcom/peel/widget/ag;->u:Lcom/peel/widget/ag;

    if-eqz v0, :cond_0

    .line 285
    sget-object v0, Lcom/peel/widget/ag;->u:Lcom/peel/widget/ag;

    invoke-virtual {v0}, Lcom/peel/widget/ag;->dismiss()V

    .line 286
    const/4 v0, 0x0

    sput-object v0, Lcom/peel/widget/ag;->u:Lcom/peel/widget/ag;

    .line 288
    :cond_0
    return-void
.end method


# virtual methods
.method public a(I)Lcom/peel/widget/ag;
    .locals 2

    .prologue
    .line 91
    iget-object v0, p0, Lcom/peel/widget/ag;->c:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 92
    iget-object v0, p0, Lcom/peel/widget/ag;->a:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(I)V

    .line 93
    return-object p0
.end method

.method public a(ILandroid/content/DialogInterface$OnClickListener;)Lcom/peel/widget/ag;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 143
    iget-object v0, p0, Lcom/peel/widget/ag;->g:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setText(I)V

    .line 144
    iput-object p2, p0, Lcom/peel/widget/ag;->o:Landroid/content/DialogInterface$OnClickListener;

    .line 145
    iget-object v0, p0, Lcom/peel/widget/ag;->d:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 146
    iget-object v0, p0, Lcom/peel/widget/ag;->g:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 147
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/peel/widget/ag;->h:Z

    .line 148
    invoke-direct {p0}, Lcom/peel/widget/ag;->a()V

    .line 149
    return-object p0
.end method

.method public a(Landroid/view/View;)Lcom/peel/widget/ag;
    .locals 2

    .prologue
    .line 120
    iget-object v0, p0, Lcom/peel/widget/ag;->f:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 121
    iget-object v0, p0, Lcom/peel/widget/ag;->f:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 122
    return-object p0
.end method

.method public a(Lcom/peel/widget/ag;)Lcom/peel/widget/ag;
    .locals 2

    .prologue
    .line 259
    if-eqz p1, :cond_0

    .line 260
    invoke-virtual {p1}, Lcom/peel/widget/ag;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    .line 261
    const/16 v1, 0x11

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->gravity:I

    .line 264
    :cond_0
    return-object p1
.end method

.method public a(Ljava/lang/CharSequence;)Lcom/peel/widget/ag;
    .locals 2

    .prologue
    .line 97
    iget-object v0, p0, Lcom/peel/widget/ag;->c:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 98
    iget-object v0, p0, Lcom/peel/widget/ag;->a:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 99
    return-object p0
.end method

.method public a(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lcom/peel/widget/ag;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 152
    iget-object v0, p0, Lcom/peel/widget/ag;->g:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 153
    iput-object p2, p0, Lcom/peel/widget/ag;->o:Landroid/content/DialogInterface$OnClickListener;

    .line 154
    iget-object v0, p0, Lcom/peel/widget/ag;->d:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 155
    iget-object v0, p0, Lcom/peel/widget/ag;->g:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 156
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/peel/widget/ag;->h:Z

    .line 157
    invoke-direct {p0}, Lcom/peel/widget/ag;->a()V

    .line 158
    return-object p0
.end method

.method public a(Z)Lcom/peel/widget/ag;
    .locals 0

    .prologue
    .line 202
    invoke-virtual {p0, p1}, Lcom/peel/widget/ag;->setCancelable(Z)V

    .line 203
    return-object p0
.end method

.method public b()Lcom/peel/widget/ag;
    .locals 2

    .prologue
    .line 103
    iget-object v0, p0, Lcom/peel/widget/ag;->c:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 104
    return-object p0
.end method

.method public b(I)Lcom/peel/widget/ag;
    .locals 2

    .prologue
    .line 108
    iget-object v0, p0, Lcom/peel/widget/ag;->e:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 109
    iget-object v0, p0, Lcom/peel/widget/ag;->b:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(I)V

    .line 110
    return-object p0
.end method

.method public b(ILandroid/content/DialogInterface$OnClickListener;)Lcom/peel/widget/ag;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 162
    iget-object v0, p0, Lcom/peel/widget/ag;->k:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setText(I)V

    .line 163
    iput-object p2, p0, Lcom/peel/widget/ag;->p:Landroid/content/DialogInterface$OnClickListener;

    .line 164
    iget-object v0, p0, Lcom/peel/widget/ag;->d:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 165
    iget-object v0, p0, Lcom/peel/widget/ag;->k:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 166
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/peel/widget/ag;->j:Z

    .line 167
    invoke-direct {p0}, Lcom/peel/widget/ag;->a()V

    .line 168
    return-object p0
.end method

.method public b(Ljava/lang/CharSequence;)Lcom/peel/widget/ag;
    .locals 2

    .prologue
    .line 114
    iget-object v0, p0, Lcom/peel/widget/ag;->e:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 115
    iget-object v0, p0, Lcom/peel/widget/ag;->b:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 116
    return-object p0
.end method

.method public c(ILandroid/content/DialogInterface$OnClickListener;)Lcom/peel/widget/ag;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 181
    invoke-virtual {p0, v1}, Lcom/peel/widget/ag;->setCanceledOnTouchOutside(Z)V

    .line 182
    iget-object v0, p0, Lcom/peel/widget/ag;->l:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setText(I)V

    .line 183
    iput-object p2, p0, Lcom/peel/widget/ag;->q:Landroid/content/DialogInterface$OnClickListener;

    .line 184
    iget-object v0, p0, Lcom/peel/widget/ag;->d:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 185
    iget-object v0, p0, Lcom/peel/widget/ag;->l:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 186
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/peel/widget/ag;->i:Z

    .line 187
    invoke-direct {p0}, Lcom/peel/widget/ag;->a()V

    .line 188
    return-object p0
.end method

.method public c()V
    .locals 2

    .prologue
    .line 126
    iget-object v0, p0, Lcom/peel/widget/ag;->f:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 127
    iget-object v0, p0, Lcom/peel/widget/ag;->f:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 128
    return-void
.end method

.method public d()Lcom/peel/widget/ag;
    .locals 1

    .prologue
    .line 207
    iget-object v0, p0, Lcom/peel/widget/ag;->s:Landroid/app/Activity;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/peel/widget/ag;->s:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 208
    invoke-virtual {p0}, Lcom/peel/widget/ag;->show()V

    .line 210
    :cond_0
    return-object p0
.end method

.method public dismiss()V
    .locals 1

    .prologue
    .line 235
    const/4 v0, 0x0

    sput-boolean v0, Lcom/peel/widget/ag;->t:Z

    .line 236
    invoke-super {p0}, Landroid/app/Dialog;->dismiss()V

    .line 237
    return-void
.end method

.method public e()V
    .locals 2

    .prologue
    .line 214
    iget-object v0, p0, Lcom/peel/widget/ag;->d:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 215
    return-void
.end method

.method public f()Landroid/widget/Button;
    .locals 1

    .prologue
    .line 247
    iget-object v0, p0, Lcom/peel/widget/ag;->g:Landroid/widget/Button;

    return-object v0
.end method

.method public hide()V
    .locals 1

    .prologue
    .line 229
    const/4 v0, 0x0

    sput-boolean v0, Lcom/peel/widget/ag;->t:Z

    .line 230
    invoke-super {p0}, Landroid/app/Dialog;->hide()V

    .line 231
    return-void
.end method

.method protected onStop()V
    .locals 1

    .prologue
    .line 241
    const/4 v0, 0x0

    sput-boolean v0, Lcom/peel/widget/ag;->t:Z

    .line 242
    invoke-super {p0}, Landroid/app/Dialog;->onStop()V

    .line 243
    return-void
.end method

.method public show()V
    .locals 1

    .prologue
    .line 219
    iget-object v0, p0, Lcom/peel/widget/ag;->d:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/peel/widget/ag;->e:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getVisibility()I

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/peel/widget/ag;->e:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getVisibility()I

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/peel/widget/ag;->f:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getVisibility()I

    move-result v0

    if-nez v0, :cond_1

    .line 222
    :cond_1
    const/4 v0, 0x1

    sput-boolean v0, Lcom/peel/widget/ag;->t:Z

    .line 223
    sput-object p0, Lcom/peel/widget/ag;->u:Lcom/peel/widget/ag;

    .line 224
    invoke-super {p0}, Landroid/app/Dialog;->show()V

    .line 225
    return-void
.end method
