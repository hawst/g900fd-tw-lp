.class public Lcom/peel/widget/HorizontalListView;
.super Landroid/widget/AdapterView;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/AdapterView",
        "<",
        "Landroid/widget/ListAdapter;",
        ">;"
    }
.end annotation


# instance fields
.field public a:Z

.field protected b:Landroid/widget/ListAdapter;

.field protected c:I

.field protected d:I

.field protected e:Landroid/widget/Scroller;

.field private f:I

.field private g:I

.field private h:I

.field private i:I

.field private j:Landroid/view/GestureDetector;

.field private k:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private l:Landroid/widget/AdapterView$OnItemSelectedListener;

.field private m:Landroid/widget/AdapterView$OnItemClickListener;

.field private n:Landroid/widget/AdapterView$OnItemLongClickListener;

.field private o:Z

.field private p:Lcom/peel/widget/ad;

.field private q:Lcom/peel/widget/ab;

.field private r:Lcom/peel/widget/ac;

.field private s:Landroid/database/DataSetObserver;

.field private final t:Ljava/lang/Runnable;

.field private u:Landroid/view/GestureDetector$OnGestureListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 91
    invoke-direct {p0, p1, p2}, Landroid/widget/AdapterView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 47
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/peel/widget/HorizontalListView;->a:Z

    .line 49
    const/4 v0, -0x1

    iput v0, p0, Lcom/peel/widget/HorizontalListView;->f:I

    .line 50
    iput v1, p0, Lcom/peel/widget/HorizontalListView;->g:I

    .line 53
    const v0, 0x7fffffff

    iput v0, p0, Lcom/peel/widget/HorizontalListView;->h:I

    .line 54
    iput v1, p0, Lcom/peel/widget/HorizontalListView;->i:I

    .line 57
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/peel/widget/HorizontalListView;->k:Ljava/util/Queue;

    .line 61
    iput-boolean v1, p0, Lcom/peel/widget/HorizontalListView;->o:Z

    .line 123
    new-instance v0, Lcom/peel/widget/y;

    invoke-direct {v0, p0}, Lcom/peel/widget/y;-><init>(Lcom/peel/widget/HorizontalListView;)V

    iput-object v0, p0, Lcom/peel/widget/HorizontalListView;->s:Landroid/database/DataSetObserver;

    .line 187
    new-instance v0, Lcom/peel/widget/z;

    invoke-direct {v0, p0}, Lcom/peel/widget/z;-><init>(Lcom/peel/widget/HorizontalListView;)V

    iput-object v0, p0, Lcom/peel/widget/HorizontalListView;->t:Ljava/lang/Runnable;

    .line 341
    new-instance v0, Lcom/peel/widget/aa;

    invoke-direct {v0, p0}, Lcom/peel/widget/aa;-><init>(Lcom/peel/widget/HorizontalListView;)V

    iput-object v0, p0, Lcom/peel/widget/HorizontalListView;->u:Landroid/view/GestureDetector$OnGestureListener;

    .line 92
    invoke-direct {p0}, Lcom/peel/widget/HorizontalListView;->a()V

    .line 93
    return-void
.end method

.method private declared-synchronized a()V
    .locals 3

    .prologue
    .line 96
    monitor-enter p0

    const/4 v0, -0x1

    :try_start_0
    iput v0, p0, Lcom/peel/widget/HorizontalListView;->f:I

    .line 97
    const/4 v0, 0x0

    iput v0, p0, Lcom/peel/widget/HorizontalListView;->g:I

    .line 98
    const/4 v0, 0x0

    iput v0, p0, Lcom/peel/widget/HorizontalListView;->i:I

    .line 99
    const/4 v0, 0x0

    iput v0, p0, Lcom/peel/widget/HorizontalListView;->c:I

    .line 100
    const/4 v0, 0x0

    iput v0, p0, Lcom/peel/widget/HorizontalListView;->d:I

    .line 101
    const v0, 0x7fffffff

    iput v0, p0, Lcom/peel/widget/HorizontalListView;->h:I

    .line 102
    new-instance v0, Landroid/widget/Scroller;

    invoke-virtual {p0}, Lcom/peel/widget/HorizontalListView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/Scroller;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/peel/widget/HorizontalListView;->e:Landroid/widget/Scroller;

    .line 103
    new-instance v0, Landroid/view/GestureDetector;

    invoke-virtual {p0}, Lcom/peel/widget/HorizontalListView;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/peel/widget/HorizontalListView;->u:Landroid/view/GestureDetector$OnGestureListener;

    invoke-direct {v0, v1, v2}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v0, p0, Lcom/peel/widget/HorizontalListView;->j:Landroid/view/GestureDetector;

    .line 105
    iget-object v0, p0, Lcom/peel/widget/HorizontalListView;->j:Landroid/view/GestureDetector;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/GestureDetector;->setIsLongpressEnabled(Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 106
    monitor-exit p0

    return-void

    .line 96
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private a(I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 234
    .line 235
    invoke-virtual {p0}, Lcom/peel/widget/HorizontalListView;->getChildCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p0, v0}, Lcom/peel/widget/HorizontalListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 236
    if-eqz v0, :cond_1

    .line 237
    invoke-virtual {v0}, Landroid/view/View;->getRight()I

    move-result v0

    .line 239
    :goto_0
    invoke-direct {p0, v0, p1}, Lcom/peel/widget/HorizontalListView;->a(II)V

    .line 242
    invoke-virtual {p0, v1}, Lcom/peel/widget/HorizontalListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 243
    if-eqz v0, :cond_0

    .line 244
    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v1

    .line 246
    :cond_0
    invoke-direct {p0, v1, p1}, Lcom/peel/widget/HorizontalListView;->b(II)V

    .line 249
    return-void

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method private a(II)V
    .locals 3

    .prologue
    .line 252
    :goto_0
    add-int v0, p1, p2

    invoke-virtual {p0}, Lcom/peel/widget/HorizontalListView;->getWidth()I

    move-result v1

    if-ge v0, v1, :cond_2

    iget v0, p0, Lcom/peel/widget/HorizontalListView;->g:I

    iget-object v1, p0, Lcom/peel/widget/HorizontalListView;->b:Landroid/widget/ListAdapter;

    invoke-interface {v1}, Landroid/widget/ListAdapter;->getCount()I

    move-result v1

    if-ge v0, v1, :cond_2

    .line 254
    iget-object v1, p0, Lcom/peel/widget/HorizontalListView;->b:Landroid/widget/ListAdapter;

    iget v2, p0, Lcom/peel/widget/HorizontalListView;->g:I

    iget-object v0, p0, Lcom/peel/widget/HorizontalListView;->k:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-interface {v1, v2, v0, p0}, Landroid/widget/ListAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 255
    const/4 v1, -0x1

    invoke-direct {p0, v0, v1}, Lcom/peel/widget/HorizontalListView;->a(Landroid/view/View;I)V

    .line 256
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    add-int/2addr p1, v0

    .line 258
    iget v0, p0, Lcom/peel/widget/HorizontalListView;->g:I

    iget-object v1, p0, Lcom/peel/widget/HorizontalListView;->b:Landroid/widget/ListAdapter;

    invoke-interface {v1}, Landroid/widget/ListAdapter;->getCount()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-ne v0, v1, :cond_0

    .line 259
    iget v0, p0, Lcom/peel/widget/HorizontalListView;->c:I

    add-int/2addr v0, p1

    invoke-virtual {p0}, Lcom/peel/widget/HorizontalListView;->getWidth()I

    move-result v1

    sub-int/2addr v0, v1

    iput v0, p0, Lcom/peel/widget/HorizontalListView;->h:I

    .line 262
    :cond_0
    iget v0, p0, Lcom/peel/widget/HorizontalListView;->h:I

    if-gez v0, :cond_1

    .line 263
    const/4 v0, 0x0

    iput v0, p0, Lcom/peel/widget/HorizontalListView;->h:I

    .line 265
    :cond_1
    iget v0, p0, Lcom/peel/widget/HorizontalListView;->g:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/peel/widget/HorizontalListView;->g:I

    goto :goto_0

    .line 268
    :cond_2
    return-void
.end method

.method private a(Landroid/view/View;I)V
    .locals 3

    .prologue
    const/4 v1, -0x1

    const/high16 v2, -0x80000000

    .line 177
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 178
    if-nez v0, :cond_0

    .line 179
    new-instance v0, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v0, v1, v1}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    .line 182
    :cond_0
    const/4 v1, 0x1

    invoke-virtual {p0, p1, p2, v0, v1}, Lcom/peel/widget/HorizontalListView;->addViewInLayout(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;Z)Z

    .line 183
    invoke-virtual {p0}, Lcom/peel/widget/HorizontalListView;->getWidth()I

    move-result v0

    invoke-static {v0, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .line 184
    invoke-virtual {p0}, Lcom/peel/widget/HorizontalListView;->getHeight()I

    move-result v1

    invoke-static {v1, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 183
    invoke-virtual {p1, v0, v1}, Landroid/view/View;->measure(II)V

    .line 185
    return-void
.end method

.method static synthetic a(Lcom/peel/widget/HorizontalListView;)V
    .locals 0

    .prologue
    .line 45
    invoke-direct {p0}, Lcom/peel/widget/HorizontalListView;->b()V

    return-void
.end method

.method static synthetic a(Lcom/peel/widget/HorizontalListView;Z)Z
    .locals 0

    .prologue
    .line 45
    iput-boolean p1, p0, Lcom/peel/widget/HorizontalListView;->o:Z

    return p1
.end method

.method static synthetic b(Lcom/peel/widget/HorizontalListView;)Lcom/peel/widget/ad;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/peel/widget/HorizontalListView;->p:Lcom/peel/widget/ad;

    return-object v0
.end method

.method private declared-synchronized b()V
    .locals 1

    .prologue
    .line 166
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/peel/widget/HorizontalListView;->a()V

    .line 167
    invoke-virtual {p0}, Lcom/peel/widget/HorizontalListView;->removeAllViewsInLayout()V

    .line 168
    invoke-virtual {p0}, Lcom/peel/widget/HorizontalListView;->requestLayout()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 169
    monitor-exit p0

    return-void

    .line 166
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private b(I)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 281
    invoke-virtual {p0, v3}, Lcom/peel/widget/HorizontalListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 282
    :goto_0
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/view/View;->getRight()I

    move-result v1

    add-int/2addr v1, p1

    if-gtz v1, :cond_0

    .line 283
    iget v1, p0, Lcom/peel/widget/HorizontalListView;->i:I

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v2

    add-int/2addr v1, v2

    iput v1, p0, Lcom/peel/widget/HorizontalListView;->i:I

    .line 284
    iget-object v1, p0, Lcom/peel/widget/HorizontalListView;->k:Ljava/util/Queue;

    invoke-interface {v1, v0}, Ljava/util/Queue;->offer(Ljava/lang/Object;)Z

    .line 285
    invoke-virtual {p0, v0}, Lcom/peel/widget/HorizontalListView;->removeViewInLayout(Landroid/view/View;)V

    .line 286
    iget v0, p0, Lcom/peel/widget/HorizontalListView;->f:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/peel/widget/HorizontalListView;->f:I

    .line 287
    invoke-virtual {p0, v3}, Lcom/peel/widget/HorizontalListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 291
    :cond_0
    invoke-virtual {p0}, Lcom/peel/widget/HorizontalListView;->getChildCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p0, v0}, Lcom/peel/widget/HorizontalListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 292
    :goto_1
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v1

    add-int/2addr v1, p1

    invoke-virtual {p0}, Lcom/peel/widget/HorizontalListView;->getWidth()I

    move-result v2

    if-lt v1, v2, :cond_1

    .line 293
    iget-object v1, p0, Lcom/peel/widget/HorizontalListView;->k:Ljava/util/Queue;

    invoke-interface {v1, v0}, Ljava/util/Queue;->offer(Ljava/lang/Object;)Z

    .line 294
    invoke-virtual {p0, v0}, Lcom/peel/widget/HorizontalListView;->removeViewInLayout(Landroid/view/View;)V

    .line 295
    iget v0, p0, Lcom/peel/widget/HorizontalListView;->g:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/peel/widget/HorizontalListView;->g:I

    .line 296
    invoke-virtual {p0}, Lcom/peel/widget/HorizontalListView;->getChildCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p0, v0}, Lcom/peel/widget/HorizontalListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    goto :goto_1

    .line 298
    :cond_1
    return-void
.end method

.method private b(II)V
    .locals 3

    .prologue
    .line 271
    :goto_0
    add-int v0, p1, p2

    if-lez v0, :cond_0

    iget v0, p0, Lcom/peel/widget/HorizontalListView;->f:I

    if-ltz v0, :cond_0

    .line 272
    iget-object v1, p0, Lcom/peel/widget/HorizontalListView;->b:Landroid/widget/ListAdapter;

    iget v2, p0, Lcom/peel/widget/HorizontalListView;->f:I

    iget-object v0, p0, Lcom/peel/widget/HorizontalListView;->k:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-interface {v1, v2, v0, p0}, Landroid/widget/ListAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 273
    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/peel/widget/HorizontalListView;->a(Landroid/view/View;I)V

    .line 274
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    sub-int/2addr p1, v1

    .line 275
    iget v1, p0, Lcom/peel/widget/HorizontalListView;->f:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lcom/peel/widget/HorizontalListView;->f:I

    .line 276
    iget v1, p0, Lcom/peel/widget/HorizontalListView;->i:I

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    sub-int v0, v1, v0

    iput v0, p0, Lcom/peel/widget/HorizontalListView;->i:I

    goto :goto_0

    .line 278
    :cond_0
    return-void
.end method

.method static synthetic c(Lcom/peel/widget/HorizontalListView;)I
    .locals 1

    .prologue
    .line 45
    iget v0, p0, Lcom/peel/widget/HorizontalListView;->f:I

    return v0
.end method

.method private c(I)V
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 301
    invoke-virtual {p0}, Lcom/peel/widget/HorizontalListView;->getChildCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 302
    iget v0, p0, Lcom/peel/widget/HorizontalListView;->i:I

    add-int/2addr v0, p1

    iput v0, p0, Lcom/peel/widget/HorizontalListView;->i:I

    .line 303
    iget v0, p0, Lcom/peel/widget/HorizontalListView;->i:I

    move v2, v0

    move v0, v1

    .line 304
    :goto_0
    invoke-virtual {p0}, Lcom/peel/widget/HorizontalListView;->getChildCount()I

    move-result v3

    if-ge v0, v3, :cond_0

    .line 305
    invoke-virtual {p0, v0}, Lcom/peel/widget/HorizontalListView;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 306
    invoke-virtual {v3}, Landroid/view/View;->getMeasuredWidth()I

    move-result v4

    .line 307
    add-int v5, v2, v4

    invoke-virtual {v3}, Landroid/view/View;->getMeasuredHeight()I

    move-result v6

    invoke-virtual {v3, v2, v1, v5, v6}, Landroid/view/View;->layout(IIII)V

    .line 308
    add-int/2addr v2, v4

    .line 304
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 311
    :cond_0
    return-void
.end method

.method static synthetic d(Lcom/peel/widget/HorizontalListView;)Lcom/peel/widget/ab;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/peel/widget/HorizontalListView;->q:Lcom/peel/widget/ab;

    return-object v0
.end method

.method static synthetic e(Lcom/peel/widget/HorizontalListView;)Landroid/widget/AdapterView$OnItemClickListener;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/peel/widget/HorizontalListView;->m:Landroid/widget/AdapterView$OnItemClickListener;

    return-object v0
.end method

.method static synthetic f(Lcom/peel/widget/HorizontalListView;)Landroid/widget/AdapterView$OnItemSelectedListener;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/peel/widget/HorizontalListView;->l:Landroid/widget/AdapterView$OnItemSelectedListener;

    return-object v0
.end method

.method static synthetic g(Lcom/peel/widget/HorizontalListView;)Landroid/widget/AdapterView$OnItemLongClickListener;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/peel/widget/HorizontalListView;->n:Landroid/widget/AdapterView$OnItemLongClickListener;

    return-object v0
.end method

.method static synthetic h(Lcom/peel/widget/HorizontalListView;)Lcom/peel/widget/ac;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/peel/widget/HorizontalListView;->r:Lcom/peel/widget/ac;

    return-object v0
.end method


# virtual methods
.method protected a(Landroid/view/MotionEvent;)Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 337
    iget-object v0, p0, Lcom/peel/widget/HorizontalListView;->e:Landroid/widget/Scroller;

    invoke-virtual {v0, v1}, Landroid/widget/Scroller;->forceFinished(Z)V

    .line 338
    return v1
.end method

.method protected a(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 9

    .prologue
    .line 328
    monitor-enter p0

    .line 329
    :try_start_0
    iget-object v0, p0, Lcom/peel/widget/HorizontalListView;->e:Landroid/widget/Scroller;

    iget v1, p0, Lcom/peel/widget/HorizontalListView;->d:I

    const/4 v2, 0x0

    neg-float v3, p3

    float-to-int v3, v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    iget v6, p0, Lcom/peel/widget/HorizontalListView;->h:I

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual/range {v0 .. v8}, Landroid/widget/Scroller;->fling(IIIIIIII)V

    .line 330
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 331
    invoke-virtual {p0}, Lcom/peel/widget/HorizontalListView;->requestLayout()V

    .line 333
    const/4 v0, 0x1

    return v0

    .line 330
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 320
    iget-object v0, p0, Lcom/peel/widget/HorizontalListView;->j:Landroid/view/GestureDetector;

    invoke-virtual {v0, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    .line 321
    return v0
.end method

.method public bridge synthetic getAdapter()Landroid/widget/Adapter;
    .locals 1

    .prologue
    .line 45
    invoke-virtual {p0}, Lcom/peel/widget/HorizontalListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    return-object v0
.end method

.method public getAdapter()Landroid/widget/ListAdapter;
    .locals 1

    .prologue
    .line 146
    iget-object v0, p0, Lcom/peel/widget/HorizontalListView;->b:Landroid/widget/ListAdapter;

    return-object v0
.end method

.method public getSelectedView()Landroid/view/View;
    .locals 1

    .prologue
    .line 152
    const/4 v0, 0x0

    return-object v0
.end method

.method protected declared-synchronized onLayout(ZIIII)V
    .locals 2

    .prologue
    .line 192
    monitor-enter p0

    :try_start_0
    invoke-super/range {p0 .. p5}, Landroid/widget/AdapterView;->onLayout(ZIIII)V

    .line 194
    iget-object v0, p0, Lcom/peel/widget/HorizontalListView;->b:Landroid/widget/ListAdapter;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_1

    .line 231
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 198
    :cond_1
    :try_start_1
    iget-boolean v0, p0, Lcom/peel/widget/HorizontalListView;->o:Z

    if-eqz v0, :cond_2

    .line 199
    iget v0, p0, Lcom/peel/widget/HorizontalListView;->c:I

    .line 200
    invoke-direct {p0}, Lcom/peel/widget/HorizontalListView;->a()V

    .line 201
    invoke-virtual {p0}, Lcom/peel/widget/HorizontalListView;->removeAllViewsInLayout()V

    .line 202
    iput v0, p0, Lcom/peel/widget/HorizontalListView;->d:I

    .line 203
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/peel/widget/HorizontalListView;->o:Z

    .line 206
    :cond_2
    iget-object v0, p0, Lcom/peel/widget/HorizontalListView;->e:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->computeScrollOffset()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 207
    iget-object v0, p0, Lcom/peel/widget/HorizontalListView;->e:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->getCurrX()I

    move-result v0

    .line 208
    iput v0, p0, Lcom/peel/widget/HorizontalListView;->d:I

    .line 211
    :cond_3
    iget v0, p0, Lcom/peel/widget/HorizontalListView;->d:I

    if-gtz v0, :cond_4

    .line 212
    const/4 v0, 0x0

    iput v0, p0, Lcom/peel/widget/HorizontalListView;->d:I

    .line 213
    iget-object v0, p0, Lcom/peel/widget/HorizontalListView;->e:Landroid/widget/Scroller;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/Scroller;->forceFinished(Z)V

    .line 215
    :cond_4
    iget v0, p0, Lcom/peel/widget/HorizontalListView;->d:I

    iget v1, p0, Lcom/peel/widget/HorizontalListView;->h:I

    if-lt v0, v1, :cond_5

    .line 216
    iget v0, p0, Lcom/peel/widget/HorizontalListView;->h:I

    iput v0, p0, Lcom/peel/widget/HorizontalListView;->d:I

    .line 217
    iget-object v0, p0, Lcom/peel/widget/HorizontalListView;->e:Landroid/widget/Scroller;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/Scroller;->forceFinished(Z)V

    .line 220
    :cond_5
    iget v0, p0, Lcom/peel/widget/HorizontalListView;->c:I

    iget v1, p0, Lcom/peel/widget/HorizontalListView;->d:I

    sub-int/2addr v0, v1

    .line 222
    invoke-direct {p0, v0}, Lcom/peel/widget/HorizontalListView;->b(I)V

    .line 223
    invoke-direct {p0, v0}, Lcom/peel/widget/HorizontalListView;->a(I)V

    .line 224
    invoke-direct {p0, v0}, Lcom/peel/widget/HorizontalListView;->c(I)V

    .line 226
    iget v0, p0, Lcom/peel/widget/HorizontalListView;->d:I

    iput v0, p0, Lcom/peel/widget/HorizontalListView;->c:I

    .line 228
    iget-object v0, p0, Lcom/peel/widget/HorizontalListView;->e:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->isFinished()Z

    move-result v0

    if-nez v0, :cond_0

    .line 229
    iget-object v0, p0, Lcom/peel/widget/HorizontalListView;->t:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/peel/widget/HorizontalListView;->post(Ljava/lang/Runnable;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 192
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public bridge synthetic setAdapter(Landroid/widget/Adapter;)V
    .locals 0

    .prologue
    .line 45
    check-cast p1, Landroid/widget/ListAdapter;

    invoke-virtual {p0, p1}, Lcom/peel/widget/HorizontalListView;->setAdapter(Landroid/widget/ListAdapter;)V

    return-void
.end method

.method public setAdapter(Landroid/widget/ListAdapter;)V
    .locals 2

    .prologue
    .line 157
    iget-object v0, p0, Lcom/peel/widget/HorizontalListView;->b:Landroid/widget/ListAdapter;

    if-eqz v0, :cond_0

    .line 158
    iget-object v0, p0, Lcom/peel/widget/HorizontalListView;->b:Landroid/widget/ListAdapter;

    iget-object v1, p0, Lcom/peel/widget/HorizontalListView;->s:Landroid/database/DataSetObserver;

    invoke-interface {v0, v1}, Landroid/widget/ListAdapter;->unregisterDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 160
    :cond_0
    iput-object p1, p0, Lcom/peel/widget/HorizontalListView;->b:Landroid/widget/ListAdapter;

    .line 161
    iget-object v0, p0, Lcom/peel/widget/HorizontalListView;->b:Landroid/widget/ListAdapter;

    iget-object v1, p0, Lcom/peel/widget/HorizontalListView;->s:Landroid/database/DataSetObserver;

    invoke-interface {v0, v1}, Landroid/widget/ListAdapter;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 162
    invoke-direct {p0}, Lcom/peel/widget/HorizontalListView;->b()V

    .line 163
    return-void
.end method

.method public setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V
    .locals 0

    .prologue
    .line 115
    iput-object p1, p0, Lcom/peel/widget/HorizontalListView;->m:Landroid/widget/AdapterView$OnItemClickListener;

    .line 116
    return-void
.end method

.method public setOnItemLongClickListener(Landroid/widget/AdapterView$OnItemLongClickListener;)V
    .locals 0

    .prologue
    .line 120
    iput-object p1, p0, Lcom/peel/widget/HorizontalListView;->n:Landroid/widget/AdapterView$OnItemLongClickListener;

    .line 121
    return-void
.end method

.method public setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V
    .locals 0

    .prologue
    .line 110
    iput-object p1, p0, Lcom/peel/widget/HorizontalListView;->l:Landroid/widget/AdapterView$OnItemSelectedListener;

    .line 111
    return-void
.end method

.method public setOnPeelItemCanceledListener(Lcom/peel/widget/ab;)V
    .locals 0

    .prologue
    .line 83
    iput-object p1, p0, Lcom/peel/widget/HorizontalListView;->q:Lcom/peel/widget/ab;

    .line 84
    return-void
.end method

.method public setOnPeelItemConfirmedListener(Lcom/peel/widget/ac;)V
    .locals 0

    .prologue
    .line 87
    iput-object p1, p0, Lcom/peel/widget/HorizontalListView;->r:Lcom/peel/widget/ac;

    .line 88
    return-void
.end method

.method public setOnPeelItemPressedListener(Lcom/peel/widget/ad;)V
    .locals 0

    .prologue
    .line 79
    iput-object p1, p0, Lcom/peel/widget/HorizontalListView;->p:Lcom/peel/widget/ad;

    .line 80
    return-void
.end method

.method public setSelection(I)V
    .locals 0

    .prologue
    .line 174
    return-void
.end method
