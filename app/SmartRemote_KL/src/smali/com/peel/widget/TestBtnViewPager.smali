.class public Lcom/peel/widget/TestBtnViewPager;
.super Landroid/support/v4/view/ViewPager;


# instance fields
.field private a:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 12
    invoke-direct {p0, p1}, Landroid/support/v4/view/ViewPager;-><init>(Landroid/content/Context;)V

    .line 9
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/peel/widget/TestBtnViewPager;->a:Z

    .line 13
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 16
    invoke-direct {p0, p1, p2}, Landroid/support/v4/view/ViewPager;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 9
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/peel/widget/TestBtnViewPager;->a:Z

    .line 17
    return-void
.end method


# virtual methods
.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 27
    iget-boolean v0, p0, Lcom/peel/widget/TestBtnViewPager;->a:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    .line 28
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 21
    iget-boolean v0, p0, Lcom/peel/widget/TestBtnViewPager;->a:Z

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 22
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Landroid/support/v4/view/ViewPager;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public setEnabledSwipe(Z)V
    .locals 0

    .prologue
    .line 32
    iput-boolean p1, p0, Lcom/peel/widget/TestBtnViewPager;->a:Z

    .line 33
    return-void
.end method
