.class public Lcom/peel/widget/ObservableListView;
.super Landroid/widget/ListView;

# interfaces
.implements Landroid/widget/AbsListView$OnScrollListener;


# instance fields
.field private a:Lcom/peel/widget/ae;

.field private b:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 17
    invoke-direct {p0, p1, p2}, Landroid/widget/ListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 14
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/peel/widget/ObservableListView;->b:Z

    .line 18
    invoke-virtual {p0, p0}, Lcom/peel/widget/ObservableListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 19
    return-void
.end method


# virtual methods
.method protected layoutChildren()V
    .locals 1

    .prologue
    .line 31
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/peel/widget/ObservableListView;->b:Z

    .line 32
    invoke-super {p0}, Landroid/widget/ListView;->layoutChildren()V

    .line 33
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/peel/widget/ObservableListView;->b:Z

    .line 34
    return-void
.end method

.method public onOverScrolled(IIZZ)V
    .locals 0

    .prologue
    .line 28
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/ListView;->onOverScrolled(IIZZ)V

    return-void
.end method

.method public onScroll(Landroid/widget/AbsListView;III)V
    .locals 0

    .prologue
    .line 36
    return-void
.end method

.method protected onScrollChanged(IIII)V
    .locals 6

    .prologue
    .line 22
    iget-boolean v0, p0, Lcom/peel/widget/ObservableListView;->b:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/peel/widget/ObservableListView;->a:Lcom/peel/widget/ae;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/peel/widget/ObservableListView;->a:Lcom/peel/widget/ae;

    move-object v1, p0

    move v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    invoke-interface/range {v0 .. v5}, Lcom/peel/widget/ae;->a(Lcom/peel/widget/ObservableListView;IIII)V

    .line 23
    :cond_0
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/ListView;->onScrollChanged(IIII)V

    .line 24
    return-void
.end method

.method public onScrollStateChanged(Landroid/widget/AbsListView;I)V
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/peel/widget/ObservableListView;->a:Lcom/peel/widget/ae;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/peel/widget/ObservableListView;->a:Lcom/peel/widget/ae;

    invoke-interface {v0, p0, p2}, Lcom/peel/widget/ae;->a(Lcom/peel/widget/ObservableListView;I)V

    :cond_0
    return-void
.end method

.method public setOnPeelScrollListener(Lcom/peel/widget/ae;)V
    .locals 0

    .prologue
    .line 27
    iput-object p1, p0, Lcom/peel/widget/ObservableListView;->a:Lcom/peel/widget/ae;

    return-void
.end method
