.class public Lcom/peel/widget/m;
.super Ljava/lang/Object;


# static fields
.field private static m:Ljava/lang/String;


# instance fields
.field public a:Landroid/view/ViewGroup;

.field public b:Z

.field private c:Landroid/webkit/WebView;

.field private d:Landroid/app/Activity;

.field private e:Landroid/os/Handler;

.field private f:Z

.field private g:Lcom/peel/ui/model/DFPAd;

.field private h:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/peel/widget/r;",
            ">;"
        }
    .end annotation
.end field

.field private i:Z

.field private j:Z

.field private k:I

.field private l:Z

.field private n:Z

.field private o:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(Landroid/app/Activity;)V
    .locals 5
    .annotation build Landroid/annotation/TargetApi;
        value = 0x11
    .end annotation

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 91
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 420
    new-instance v0, Lcom/peel/widget/q;

    invoke-direct {v0, p0}, Lcom/peel/widget/q;-><init>(Lcom/peel/widget/m;)V

    iput-object v0, p0, Lcom/peel/widget/m;->o:Ljava/lang/Runnable;

    .line 92
    invoke-virtual {p1}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lcom/peel/ui/fq;->dfp_advertisement:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/peel/widget/m;->a:Landroid/view/ViewGroup;

    .line 93
    iget-object v0, p0, Lcom/peel/widget/m;->a:Landroid/view/ViewGroup;

    sget v1, Lcom/peel/ui/fp;->webview:I

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/webkit/WebView;

    iput-object v0, p0, Lcom/peel/widget/m;->c:Landroid/webkit/WebView;

    .line 94
    iput-object p1, p0, Lcom/peel/widget/m;->d:Landroid/app/Activity;

    .line 95
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/peel/widget/m;->e:Landroid/os/Handler;

    .line 96
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/peel/widget/m;->h:Ljava/util/ArrayList;

    .line 97
    iget-object v0, p0, Lcom/peel/widget/m;->c:Landroid/webkit/WebView;

    new-instance v1, Lcom/peel/widget/n;

    invoke-direct {v1, p0}, Lcom/peel/widget/n;-><init>(Lcom/peel/widget/m;)V

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 143
    iget-object v0, p0, Lcom/peel/widget/m;->c:Landroid/webkit/WebView;

    new-instance v1, Lcom/peel/widget/o;

    invoke-direct {v1, p0}, Lcom/peel/widget/o;-><init>(Lcom/peel/widget/m;)V

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 248
    iget-object v0, p0, Lcom/peel/widget/m;->c:Landroid/webkit/WebView;

    invoke-virtual {v0, v3}, Landroid/webkit/WebView;->setInitialScale(I)V

    .line 249
    iget-object v0, p0, Lcom/peel/widget/m;->c:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    .line 250
    invoke-virtual {v0, v4}, Landroid/webkit/WebSettings;->setMediaPlaybackRequiresUserGesture(Z)V

    .line 251
    invoke-virtual {v0, v3}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V

    .line 252
    invoke-virtual {v0, v3}, Landroid/webkit/WebSettings;->setJavaScriptCanOpenWindowsAutomatically(Z)V

    .line 253
    invoke-virtual {v0, v3}, Landroid/webkit/WebSettings;->setUseWideViewPort(Z)V

    .line 254
    invoke-virtual {v0, v3}, Landroid/webkit/WebSettings;->setSupportZoom(Z)V

    .line 255
    invoke-virtual {v0, v3}, Landroid/webkit/WebSettings;->setLoadWithOverviewMode(Z)V

    .line 256
    sget-object v1, Landroid/webkit/WebSettings$PluginState;->ON:Landroid/webkit/WebSettings$PluginState;

    invoke-virtual {v0, v1}, Landroid/webkit/WebSettings;->setPluginState(Landroid/webkit/WebSettings$PluginState;)V

    .line 257
    invoke-virtual {v0, v4}, Landroid/webkit/WebSettings;->setBlockNetworkImage(Z)V

    .line 258
    invoke-virtual {v0, v4}, Landroid/webkit/WebSettings;->setBlockNetworkLoads(Z)V

    .line 260
    return-void
.end method

.method static synthetic a(Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 36
    sput-object p0, Lcom/peel/widget/m;->m:Ljava/lang/String;

    return-object p0
.end method

.method private varargs a([Lcom/peel/ui/model/DFPAd;)Ljava/lang/String;
    .locals 19

    .prologue
    .line 381
    const-string/jumbo v8, ""

    .line 382
    const-string/jumbo v7, ""

    .line 383
    const-string/jumbo v6, ""

    .line 384
    const-string/jumbo v5, ""

    .line 385
    const-string/jumbo v4, ""

    .line 386
    const-string/jumbo v3, ""

    .line 387
    const/4 v2, 0x0

    .line 388
    move-object/from16 v0, p1

    array-length v10, v0

    const/4 v1, 0x0

    move/from16 v18, v1

    move-object v1, v8

    move-object v8, v7

    move-object v7, v6

    move-object v6, v5

    move-object v5, v4

    move/from16 v4, v18

    :goto_0
    if-ge v4, v10, :cond_3

    aget-object v11, p1, v4

    .line 389
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "ad"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    .line 390
    add-int/lit8 v5, v2, 0x1

    .line 392
    const-string/jumbo v3, ""

    .line 393
    const/4 v2, 0x1

    new-array v9, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    iget-object v13, v11, Lcom/peel/ui/model/DFPAd;->c:Ljava/lang/String;

    aput-object v13, v9, v2

    .line 394
    array-length v13, v9

    const/4 v2, 0x0

    :goto_1
    if-ge v2, v13, :cond_0

    aget-object v14, v9, v2

    .line 395
    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v15, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v15, "[%1$s],"

    const/16 v16, 0x1

    move/from16 v0, v16

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v16, v0

    const/16 v17, 0x0

    aput-object v14, v16, v17

    invoke-static/range {v15 .. v16}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v3, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 394
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 398
    :cond_0
    const/4 v2, 0x0

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v9

    add-int/lit8 v9, v9, -0x1

    invoke-virtual {v3, v2, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    .line 399
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "["

    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 401
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v8, "var %1$s = googletag.defineSlot(\'%2$s\', %3$s, \'%4$s\');"

    const/4 v9, 0x4

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v13, 0x0

    aput-object v12, v9, v13

    const/4 v13, 0x1

    iget-object v14, v11, Lcom/peel/ui/model/DFPAd;->a:Ljava/lang/String;

    aput-object v14, v9, v13

    const/4 v13, 0x2

    aput-object v2, v9, v13

    const/4 v2, 0x3

    iget-object v13, v11, Lcom/peel/ui/model/DFPAd;->b:Ljava/lang/String;

    aput-object v13, v9, v2

    invoke-static {v8, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 403
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "%1$s.addService(googletag.pubads());"

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    aput-object v12, v7, v8

    invoke-static {v3, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 405
    iget-object v2, v11, Lcom/peel/ui/model/DFPAd;->d:Ljava/util/Map;

    if-eqz v2, :cond_2

    iget-object v2, v11, Lcom/peel/ui/model/DFPAd;->d:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->size()I

    move-result v2

    if-lez v2, :cond_2

    .line 406
    iget-object v2, v11, Lcom/peel/ui/model/DFPAd;->d:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move-object v2, v1

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 407
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v7, "%1$s.setTargeting(\"%2$s\",\"%3$s\");"

    const/4 v13, 0x3

    new-array v13, v13, [Ljava/lang/Object;

    const/4 v14, 0x0

    aput-object v12, v13, v14

    const/4 v14, 0x1

    aput-object v1, v13, v14

    const/4 v14, 0x2

    iget-object v15, v11, Lcom/peel/ui/model/DFPAd;->d:Ljava/util/Map;

    invoke-interface {v15, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    aput-object v1, v13, v14

    invoke-static {v7, v13}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    move-object v2, v1

    .line 408
    goto :goto_2

    :cond_1
    move-object v1, v2

    .line 410
    :cond_2
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "<div class=\"ad\" id=\'%1$s\'><script type=\'text/javascript\'>googletag.cmd.push(function() { googletag.display(\'%2$s\'); });</script></div>"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    iget-object v12, v11, Lcom/peel/ui/model/DFPAd;->b:Ljava/lang/String;

    aput-object v12, v6, v7

    const/4 v7, 0x1

    iget-object v12, v11, Lcom/peel/ui/model/DFPAd;->b:Ljava/lang/String;

    aput-object v12, v6, v7

    invoke-static {v3, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 412
    const-string/jumbo v2, ""

    const/4 v3, 0x4

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v6, 0x0

    iget-object v12, v11, Lcom/peel/ui/model/DFPAd;->b:Ljava/lang/String;

    aput-object v12, v3, v6

    const/4 v6, 0x1

    iget-object v12, v11, Lcom/peel/ui/model/DFPAd;->b:Ljava/lang/String;

    aput-object v12, v3, v6

    const/4 v6, 0x2

    iget-object v12, v11, Lcom/peel/ui/model/DFPAd;->b:Ljava/lang/String;

    aput-object v12, v3, v6

    const/4 v6, 0x3

    iget-object v11, v11, Lcom/peel/ui/model/DFPAd;->b:Ljava/lang/String;

    aput-object v11, v3, v6

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 413
    const-string/jumbo v2, ""

    const/4 v6, 0x0

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {v2, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    .line 388
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    move v2, v5

    move-object v5, v6

    move-object v6, v7

    move-object v7, v8

    move-object v8, v9

    goto/16 :goto_0

    .line 415
    :cond_3
    const-string/jumbo v2, "<html><meta name = \"viewport\" content = \"initial-scale = 1, user-scalable = no\"><head><title></title><script type=\'text/javascript\'>var googletag = googletag || {};googletag.cmd = googletag.cmd || [];(function() {var gads = document.createElement(\'script\');gads.async = true;gads.type = \'text/javascript\';var useSSL = \'https:\' == document.location.protocol;gads.src = (useSSL ? \'https:\' : \'http:\') + \'//www.googletagservices.com/tag/js/gpt.js\';var node = document.getElementsByTagName(\'script\')[0];node.parentNode.insertBefore(gads, node);})();</script><script type=\'text/javascript\'>googletag.cmd.push(function() {%1$sgoogletag.pubads().enable();googletag.enableServices();});</script><style type=\"text/css\">body{margin:0px!important;padding: 0px !important;background: #111 !important;} .ad{margin: 0 auto;}</style>%2$s</head><body>%3$s</body>%4$s</html>"

    const/4 v4, 0x4

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v9, 0x0

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v9

    const/4 v1, 0x1

    aput-object v5, v4, v1

    const/4 v1, 0x2

    aput-object v6, v4, v1

    const/4 v1, 0x3

    aput-object v3, v4, v1

    invoke-static {v2, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 416
    const-string/jumbo v2, "DFPAdView"

    invoke-static {v2, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 417
    return-object v1
.end method

.method static synthetic a(Lcom/peel/widget/m;)Z
    .locals 1

    .prologue
    .line 36
    iget-boolean v0, p0, Lcom/peel/widget/m;->f:Z

    return v0
.end method

.method static synthetic a(Lcom/peel/widget/m;Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 36
    invoke-direct {p0, p1}, Lcom/peel/widget/m;->c(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method static synthetic a(Lcom/peel/widget/m;Z)Z
    .locals 0

    .prologue
    .line 36
    iput-boolean p1, p0, Lcom/peel/widget/m;->j:Z

    return p1
.end method

.method static synthetic b(Lcom/peel/widget/m;)Lcom/peel/ui/model/DFPAd;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/peel/widget/m;->g:Lcom/peel/ui/model/DFPAd;

    return-object v0
.end method

.method static synthetic b(Lcom/peel/widget/m;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0, p1}, Lcom/peel/widget/m;->b(Ljava/lang/String;)V

    return-void
.end method

.method private b(Ljava/lang/String;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v0, 0x0

    .line 271
    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/String;

    const-string/jumbo v2, "googletagservices"

    aput-object v2, v1, v0

    const-string/jumbo v2, "doubleclick.net/gampad/ads"

    aput-object v2, v1, v5

    const/4 v2, 0x2

    const-string/jumbo v3, "googlesyndication"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string/jumbo v3, "googleadservices"

    aput-object v3, v1, v2

    .line 273
    array-length v2, v1

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 274
    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 282
    :goto_1
    return-void

    .line 273
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 279
    :cond_1
    const-string/jumbo v0, "DFPAdView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "hasLoaded URL: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 281
    iput-boolean v5, p0, Lcom/peel/widget/m;->b:Z

    goto :goto_1
.end method

.method static synthetic b(Lcom/peel/widget/m;Z)Z
    .locals 0

    .prologue
    .line 36
    iput-boolean p1, p0, Lcom/peel/widget/m;->f:Z

    return p1
.end method

.method static synthetic c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 36
    sget-object v0, Lcom/peel/widget/m;->m:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic c(Lcom/peel/widget/m;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0, p1}, Lcom/peel/widget/m;->d(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic c(Lcom/peel/widget/m;)Z
    .locals 1

    .prologue
    .line 36
    iget-boolean v0, p0, Lcom/peel/widget/m;->j:Z

    return v0
.end method

.method static synthetic c(Lcom/peel/widget/m;Z)Z
    .locals 0

    .prologue
    .line 36
    iput-boolean p1, p0, Lcom/peel/widget/m;->n:Z

    return p1
.end method

.method private c(Ljava/lang/String;)Z
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 294
    const/16 v2, 0xa

    new-array v3, v2, [Ljava/lang/String;

    const-string/jumbo v2, ".js"

    aput-object v2, v3, v0

    const-string/jumbo v2, ".css"

    aput-object v2, v3, v1

    const-string/jumbo v2, "/js"

    aput-object v2, v3, v6

    const-string/jumbo v2, ".ico"

    aput-object v2, v3, v7

    const-string/jumbo v2, ".jpg"

    aput-object v2, v3, v8

    const/4 v2, 0x5

    const-string/jumbo v4, ".jpeg"

    aput-object v4, v3, v2

    const/4 v2, 0x6

    const-string/jumbo v4, ".png"

    aput-object v4, v3, v2

    const/4 v2, 0x7

    const-string/jumbo v4, ".gif"

    aput-object v4, v3, v2

    const/16 v2, 0x8

    const-string/jumbo v4, ".svg"

    aput-object v4, v3, v2

    const/16 v2, 0x9

    const-string/jumbo v4, "//templates.html"

    aput-object v4, v3, v2

    .line 295
    new-array v4, v0, [Ljava/lang/String;

    .line 296
    const/4 v2, 0x6

    new-array v5, v2, [Ljava/lang/String;

    const-string/jumbo v2, "googletagservices"

    aput-object v2, v5, v0

    const-string/jumbo v2, "pubads.g.doubleclick.net/gampad"

    aput-object v2, v5, v1

    const-string/jumbo v2, "ad.doubleclick.net"

    aput-object v2, v5, v6

    const-string/jumbo v2, "csi.gstatic."

    aput-object v2, v5, v7

    const-string/jumbo v2, ".css?"

    aput-object v2, v5, v8

    const/4 v2, 0x5

    const-string/jumbo v6, ".js?"

    aput-object v6, v5, v2

    .line 299
    array-length v6, v3

    move v2, v0

    :goto_0
    if-ge v2, v6, :cond_2

    aget-object v7, v3, v2

    .line 300
    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v8, v7}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 316
    :cond_0
    :goto_1
    return v0

    .line 299
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 305
    :cond_2
    array-length v3, v5

    move v2, v0

    :goto_2
    if-ge v2, v3, :cond_3

    aget-object v6, v5, v2

    .line 306
    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v7, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 305
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 311
    :cond_3
    array-length v3, v4

    move v2, v0

    :goto_3
    if-ge v2, v3, :cond_4

    aget-object v5, v4, v2

    .line 312
    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v6, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 311
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    :cond_4
    move v0, v1

    .line 316
    goto :goto_1
.end method

.method static synthetic d(Lcom/peel/widget/m;)Landroid/webkit/WebView;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/peel/widget/m;->c:Landroid/webkit/WebView;

    return-object v0
.end method

.method private d()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 428
    iget-object v0, p0, Lcom/peel/widget/m;->h:Ljava/util/ArrayList;

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/peel/widget/m;->b:Z

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/peel/widget/m;->i:Z

    if-eqz v0, :cond_2

    .line 429
    iput-boolean v2, p0, Lcom/peel/widget/m;->i:Z

    .line 430
    iget-object v0, p0, Lcom/peel/widget/m;->h:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/widget/r;

    .line 431
    invoke-interface {v0}, Lcom/peel/widget/r;->a()V

    goto :goto_0

    .line 433
    :cond_0
    iput-boolean v2, p0, Lcom/peel/widget/m;->b:Z

    .line 434
    iget-object v0, p0, Lcom/peel/widget/m;->g:Lcom/peel/ui/model/DFPAd;

    iget v0, v0, Lcom/peel/ui/model/DFPAd;->s:I

    if-gez v0, :cond_1

    .line 455
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/peel/widget/m;->f:Z

    .line 456
    return-void

    .line 448
    :cond_2
    iget-object v0, p0, Lcom/peel/widget/m;->h:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    .line 449
    iget-object v0, p0, Lcom/peel/widget/m;->h:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/widget/r;

    .line 450
    invoke-interface {v0}, Lcom/peel/widget/r;->c()V

    goto :goto_1
.end method

.method private d(Ljava/lang/String;)V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v0, 0x0

    .line 459
    const/4 v1, 0x3

    new-array v2, v1, [Ljava/lang/String;

    const-string/jumbo v1, "rubiconproject"

    aput-object v1, v2, v0

    const-string/jumbo v1, "Peel_App_"

    aput-object v1, v2, v7

    const/4 v1, 0x2

    const-string/jumbo v3, "peelaction=click"

    aput-object v3, v2, v1

    .line 462
    new-array v3, v7, [Ljava/lang/String;

    const-string/jumbo v1, "peel://"

    aput-object v1, v3, v0

    .line 464
    array-length v4, v2

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_1

    aget-object v5, v2, v1

    .line 465
    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v6, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 466
    iput-boolean v7, p0, Lcom/peel/widget/m;->l:Z

    .line 464
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 471
    :cond_1
    array-length v1, v3

    :goto_1
    if-ge v0, v1, :cond_3

    aget-object v2, v3, v0

    .line 472
    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 473
    iput-boolean v7, p0, Lcom/peel/widget/m;->l:Z

    .line 471
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 477
    :cond_3
    return-void
.end method

.method static synthetic d(Lcom/peel/widget/m;Z)Z
    .locals 0

    .prologue
    .line 36
    iput-boolean p1, p0, Lcom/peel/widget/m;->l:Z

    return p1
.end method

.method static synthetic e(Lcom/peel/widget/m;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/peel/widget/m;->h:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic f(Lcom/peel/widget/m;)I
    .locals 1

    .prologue
    .line 36
    iget v0, p0, Lcom/peel/widget/m;->k:I

    return v0
.end method

.method static synthetic g(Lcom/peel/widget/m;)Landroid/app/Activity;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/peel/widget/m;->d:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic h(Lcom/peel/widget/m;)Z
    .locals 1

    .prologue
    .line 36
    iget-boolean v0, p0, Lcom/peel/widget/m;->n:Z

    return v0
.end method

.method static synthetic i(Lcom/peel/widget/m;)Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/peel/widget/m;->o:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic j(Lcom/peel/widget/m;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/peel/widget/m;->e:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic k(Lcom/peel/widget/m;)Z
    .locals 1

    .prologue
    .line 36
    iget-boolean v0, p0, Lcom/peel/widget/m;->l:Z

    return v0
.end method

.method static synthetic l(Lcom/peel/widget/m;)V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0}, Lcom/peel/widget/m;->d()V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 289
    iget-object v0, p0, Lcom/peel/widget/m;->h:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 290
    return-void
.end method

.method public a(I)V
    .locals 0

    .prologue
    .line 87
    iput p1, p0, Lcom/peel/widget/m;->k:I

    .line 88
    return-void
.end method

.method public a(II)V
    .locals 1

    .prologue
    .line 263
    iget-object v0, p0, Lcom/peel/widget/m;->c:Landroid/webkit/WebView;

    if-eqz v0, :cond_0

    .line 264
    iget-object v0, p0, Lcom/peel/widget/m;->c:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 265
    iput p1, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 266
    iput p2, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 268
    :cond_0
    return-void
.end method

.method public a(Lcom/peel/ui/model/DFPAd;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 320
    iput-object p1, p0, Lcom/peel/widget/m;->g:Lcom/peel/ui/model/DFPAd;

    .line 321
    iput-boolean v1, p0, Lcom/peel/widget/m;->i:Z

    .line 322
    iput-boolean v2, p0, Lcom/peel/widget/m;->n:Z

    .line 323
    const-string/jumbo v3, "DFPAdView"

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "Loading Ad: Webview "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v0, p0, Lcom/peel/widget/m;->c:Landroid/webkit/WebView;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 324
    new-array v0, v1, [Lcom/peel/ui/model/DFPAd;

    aput-object p1, v0, v2

    invoke-direct {p0, v0}, Lcom/peel/widget/m;->a([Lcom/peel/ui/model/DFPAd;)Ljava/lang/String;

    move-result-object v0

    .line 325
    iget-object v1, p0, Lcom/peel/widget/m;->c:Landroid/webkit/WebView;

    if-nez v1, :cond_1

    .line 374
    :goto_1
    return-void

    :cond_0
    move v0, v2

    .line 323
    goto :goto_0

    .line 329
    :cond_1
    iput-boolean v2, p0, Lcom/peel/widget/m;->l:Z

    .line 331
    iget-object v1, p0, Lcom/peel/widget/m;->d:Landroid/app/Activity;

    new-instance v2, Lcom/peel/widget/p;

    invoke-direct {v2, p0, v0, p1}, Lcom/peel/widget/p;-><init>(Lcom/peel/widget/m;Ljava/lang/String;Lcom/peel/ui/model/DFPAd;)V

    invoke-virtual {v1, v2}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_1
.end method

.method public a(Lcom/peel/widget/r;)V
    .locals 1

    .prologue
    .line 285
    iget-object v0, p0, Lcom/peel/widget/m;->h:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 286
    return-void
.end method

.method public b()V
    .locals 2

    .prologue
    .line 480
    iget-object v0, p0, Lcom/peel/widget/m;->c:Landroid/webkit/WebView;

    const-string/jumbo v1, "about:blank"

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 481
    return-void
.end method
