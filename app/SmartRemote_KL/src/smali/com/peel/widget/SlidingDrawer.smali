.class public Lcom/peel/widget/SlidingDrawer;
.super Landroid/view/ViewGroup;


# instance fields
.field private A:Z

.field private B:Z

.field private C:Z

.field private final D:I

.field private final E:I

.field private final F:I

.field private final G:I

.field private final H:I

.field private final I:I

.field private final a:I

.field private final b:I

.field private c:Landroid/view/View;

.field private d:Landroid/view/View;

.field private final e:Landroid/graphics/Rect;

.field private final f:Landroid/graphics/Rect;

.field private g:Z

.field private h:Z

.field private i:Landroid/view/VelocityTracker;

.field private j:Z

.field private k:Z

.field private l:I

.field private m:I

.field private n:I

.field private o:I

.field private p:I

.field private q:Lcom/peel/widget/am;

.field private r:Lcom/peel/widget/al;

.field private s:Lcom/peel/widget/an;

.field private final t:Landroid/os/Handler;

.field private u:F

.field private v:F

.field private w:F

.field private x:J

.field private y:J

.field private z:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 183
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/peel/widget/SlidingDrawer;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 184
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/high16 v4, 0x3f000000    # 0.5f

    .line 194
    invoke-direct {p0, p1, p2, p3}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 104
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/peel/widget/SlidingDrawer;->e:Landroid/graphics/Rect;

    .line 105
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/peel/widget/SlidingDrawer;->f:Landroid/graphics/Rect;

    .line 123
    new-instance v0, Lcom/peel/widget/ao;

    const/4 v3, 0x0

    invoke-direct {v0, p0, v3}, Lcom/peel/widget/ao;-><init>(Lcom/peel/widget/SlidingDrawer;Lcom/peel/widget/aj;)V

    iput-object v0, p0, Lcom/peel/widget/SlidingDrawer;->t:Landroid/os/Handler;

    .line 195
    sget-object v0, Lcom/peel/ui/fv;->SlidingDrawer:[I

    invoke-virtual {p1, p2, v0, p3, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v3

    .line 197
    sget v0, Lcom/peel/ui/fv;->SlidingDrawer_orientation:I

    invoke-virtual {v3, v0, v1}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v0

    .line 198
    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/peel/widget/SlidingDrawer;->j:Z

    .line 199
    sget v0, Lcom/peel/ui/fv;->SlidingDrawer_bottomOffset:I

    invoke-virtual {v3, v0, v5}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/peel/widget/SlidingDrawer;->l:I

    .line 200
    sget v0, Lcom/peel/ui/fv;->SlidingDrawer_topOffset:I

    invoke-virtual {v3, v0, v5}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/peel/widget/SlidingDrawer;->m:I

    .line 201
    sget v0, Lcom/peel/ui/fv;->SlidingDrawer_allowSingleTap:I

    invoke-virtual {v3, v0, v1}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    iput-boolean v0, p0, Lcom/peel/widget/SlidingDrawer;->B:Z

    .line 203
    sget v0, Lcom/peel/ui/fv;->SlidingDrawer_animateOnClick:I

    invoke-virtual {v3, v0, v1}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    iput-boolean v0, p0, Lcom/peel/widget/SlidingDrawer;->C:Z

    .line 205
    sget v0, Lcom/peel/ui/fv;->SlidingDrawer_topDistance:I

    invoke-virtual {v3, v0, v5}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/peel/widget/SlidingDrawer;->p:I

    .line 207
    sget v0, Lcom/peel/ui/fv;->SlidingDrawer_handle:I

    invoke-virtual {v3, v0, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0

    .line 208
    if-nez v0, :cond_1

    .line 209
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "The handle attribute is required and must refer to a valid child."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    move v0, v2

    .line 198
    goto :goto_0

    .line 213
    :cond_1
    sget v1, Lcom/peel/ui/fv;->SlidingDrawer_content:I

    invoke-virtual {v3, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    .line 214
    if-nez v1, :cond_2

    .line 215
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "The content attribute is required and must refer to a valid child."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 219
    :cond_2
    if-ne v0, v1, :cond_3

    .line 220
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "The content and handle attributes must refer to different children."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 224
    :cond_3
    iput v0, p0, Lcom/peel/widget/SlidingDrawer;->a:I

    .line 225
    iput v1, p0, Lcom/peel/widget/SlidingDrawer;->b:I

    .line 227
    invoke-virtual {p0}, Lcom/peel/widget/SlidingDrawer;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    .line 228
    const/high16 v1, 0x40c00000    # 6.0f

    mul-float/2addr v1, v0

    add-float/2addr v1, v4

    float-to-int v1, v1

    iput v1, p0, Lcom/peel/widget/SlidingDrawer;->D:I

    .line 229
    const/high16 v1, 0x42c80000    # 100.0f

    mul-float/2addr v1, v0

    add-float/2addr v1, v4

    float-to-int v1, v1

    iput v1, p0, Lcom/peel/widget/SlidingDrawer;->E:I

    .line 230
    const/high16 v1, 0x43160000    # 150.0f

    mul-float/2addr v1, v0

    add-float/2addr v1, v4

    float-to-int v1, v1

    iput v1, p0, Lcom/peel/widget/SlidingDrawer;->F:I

    .line 231
    const/high16 v1, 0x43480000    # 200.0f

    mul-float/2addr v1, v0

    add-float/2addr v1, v4

    float-to-int v1, v1

    iput v1, p0, Lcom/peel/widget/SlidingDrawer;->G:I

    .line 232
    const/high16 v1, 0x44fa0000    # 2000.0f

    mul-float/2addr v1, v0

    add-float/2addr v1, v4

    float-to-int v1, v1

    iput v1, p0, Lcom/peel/widget/SlidingDrawer;->H:I

    .line 233
    const/high16 v1, 0x447a0000    # 1000.0f

    mul-float/2addr v0, v1

    add-float/2addr v0, v4

    float-to-int v0, v0

    iput v0, p0, Lcom/peel/widget/SlidingDrawer;->I:I

    .line 235
    invoke-virtual {v3}, Landroid/content/res/TypedArray;->recycle()V

    .line 237
    invoke-virtual {p0, v2}, Lcom/peel/widget/SlidingDrawer;->setAlwaysDrawnWithCacheEnabled(Z)V

    .line 238
    return-void
.end method

.method private a(I)V
    .locals 2

    .prologue
    .line 495
    invoke-direct {p0, p1}, Lcom/peel/widget/SlidingDrawer;->c(I)V

    .line 496
    iget v0, p0, Lcom/peel/widget/SlidingDrawer;->H:I

    int-to-float v0, v0

    const/4 v1, 0x1

    invoke-direct {p0, p1, v0, v1}, Lcom/peel/widget/SlidingDrawer;->a(IFZ)V

    .line 497
    return-void
.end method

.method private a(IFZ)V
    .locals 5

    .prologue
    const/16 v4, 0x3e8

    const/4 v2, 0x0

    .line 505
    int-to-float v0, p1

    iput v0, p0, Lcom/peel/widget/SlidingDrawer;->w:F

    .line 506
    iput p2, p0, Lcom/peel/widget/SlidingDrawer;->v:F

    .line 508
    iget-boolean v0, p0, Lcom/peel/widget/SlidingDrawer;->k:Z

    if-eqz v0, :cond_4

    .line 509
    if-nez p3, :cond_0

    iget v0, p0, Lcom/peel/widget/SlidingDrawer;->G:I

    int-to-float v0, v0

    cmpl-float v0, p2, v0

    if-gtz v0, :cond_0

    iget v1, p0, Lcom/peel/widget/SlidingDrawer;->m:I

    iget-boolean v0, p0, Lcom/peel/widget/SlidingDrawer;->j:Z

    if-eqz v0, :cond_2

    iget v0, p0, Lcom/peel/widget/SlidingDrawer;->n:I

    :goto_0
    add-int/2addr v0, v1

    if-le p1, v0, :cond_3

    iget v0, p0, Lcom/peel/widget/SlidingDrawer;->G:I

    neg-int v0, v0

    int-to-float v0, v0

    cmpl-float v0, p2, v0

    if-lez v0, :cond_3

    .line 514
    :cond_0
    iget v0, p0, Lcom/peel/widget/SlidingDrawer;->H:I

    int-to-float v0, v0

    iput v0, p0, Lcom/peel/widget/SlidingDrawer;->u:F

    .line 515
    cmpg-float v0, p2, v2

    if-gez v0, :cond_1

    .line 516
    iput v2, p0, Lcom/peel/widget/SlidingDrawer;->v:F

    .line 544
    :cond_1
    :goto_1
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    .line 545
    iput-wide v0, p0, Lcom/peel/widget/SlidingDrawer;->x:J

    .line 546
    const-wide/16 v2, 0x10

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/peel/widget/SlidingDrawer;->y:J

    .line 547
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/peel/widget/SlidingDrawer;->A:Z

    .line 548
    iget-object v0, p0, Lcom/peel/widget/SlidingDrawer;->t:Landroid/os/Handler;

    invoke-virtual {v0, v4}, Landroid/os/Handler;->removeMessages(I)V

    .line 549
    iget-object v0, p0, Lcom/peel/widget/SlidingDrawer;->t:Landroid/os/Handler;

    iget-object v1, p0, Lcom/peel/widget/SlidingDrawer;->t:Landroid/os/Handler;

    invoke-virtual {v1, v4}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    iget-wide v2, p0, Lcom/peel/widget/SlidingDrawer;->y:J

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageAtTime(Landroid/os/Message;J)Z

    .line 550
    invoke-direct {p0}, Lcom/peel/widget/SlidingDrawer;->h()V

    .line 551
    return-void

    .line 509
    :cond_2
    iget v0, p0, Lcom/peel/widget/SlidingDrawer;->o:I

    goto :goto_0

    .line 520
    :cond_3
    iget v0, p0, Lcom/peel/widget/SlidingDrawer;->H:I

    neg-int v0, v0

    int-to-float v0, v0

    iput v0, p0, Lcom/peel/widget/SlidingDrawer;->u:F

    .line 521
    cmpl-float v0, p2, v2

    if-lez v0, :cond_1

    .line 522
    iput v2, p0, Lcom/peel/widget/SlidingDrawer;->v:F

    goto :goto_1

    .line 526
    :cond_4
    if-nez p3, :cond_7

    iget v0, p0, Lcom/peel/widget/SlidingDrawer;->G:I

    int-to-float v0, v0

    cmpl-float v0, p2, v0

    if-gtz v0, :cond_5

    iget-boolean v0, p0, Lcom/peel/widget/SlidingDrawer;->j:Z

    if-eqz v0, :cond_6

    .line 527
    invoke-virtual {p0}, Lcom/peel/widget/SlidingDrawer;->getHeight()I

    move-result v0

    :goto_2
    div-int/lit8 v0, v0, 0x2

    if-le p1, v0, :cond_7

    iget v0, p0, Lcom/peel/widget/SlidingDrawer;->G:I

    neg-int v0, v0

    int-to-float v0, v0

    cmpl-float v0, p2, v0

    if-lez v0, :cond_7

    .line 530
    :cond_5
    iget v0, p0, Lcom/peel/widget/SlidingDrawer;->H:I

    int-to-float v0, v0

    iput v0, p0, Lcom/peel/widget/SlidingDrawer;->u:F

    .line 531
    cmpg-float v0, p2, v2

    if-gez v0, :cond_1

    .line 532
    iput v2, p0, Lcom/peel/widget/SlidingDrawer;->v:F

    goto :goto_1

    .line 527
    :cond_6
    invoke-virtual {p0}, Lcom/peel/widget/SlidingDrawer;->getWidth()I

    move-result v0

    goto :goto_2

    .line 537
    :cond_7
    iget v0, p0, Lcom/peel/widget/SlidingDrawer;->H:I

    neg-int v0, v0

    int-to-float v0, v0

    iput v0, p0, Lcom/peel/widget/SlidingDrawer;->u:F

    .line 538
    cmpl-float v0, p2, v2

    if-lez v0, :cond_1

    .line 539
    iput v2, p0, Lcom/peel/widget/SlidingDrawer;->v:F

    goto :goto_1
.end method

.method static synthetic a(Lcom/peel/widget/SlidingDrawer;)Z
    .locals 1

    .prologue
    .line 82
    iget-boolean v0, p0, Lcom/peel/widget/SlidingDrawer;->h:Z

    return v0
.end method

.method private b(I)V
    .locals 2

    .prologue
    .line 500
    invoke-direct {p0, p1}, Lcom/peel/widget/SlidingDrawer;->c(I)V

    .line 501
    iget v0, p0, Lcom/peel/widget/SlidingDrawer;->H:I

    neg-int v0, v0

    int-to-float v0, v0

    const/4 v1, 0x1

    invoke-direct {p0, p1, v0, v1}, Lcom/peel/widget/SlidingDrawer;->a(IFZ)V

    .line 502
    return-void
.end method

.method static synthetic b(Lcom/peel/widget/SlidingDrawer;)Z
    .locals 1

    .prologue
    .line 82
    iget-boolean v0, p0, Lcom/peel/widget/SlidingDrawer;->C:Z

    return v0
.end method

.method private c(I)V
    .locals 6

    .prologue
    const/16 v4, 0x3e8

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 554
    iput-boolean v1, p0, Lcom/peel/widget/SlidingDrawer;->g:Z

    .line 555
    invoke-static {}, Landroid/view/VelocityTracker;->obtain()Landroid/view/VelocityTracker;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/widget/SlidingDrawer;->i:Landroid/view/VelocityTracker;

    .line 556
    iget-boolean v0, p0, Lcom/peel/widget/SlidingDrawer;->k:Z

    if-nez v0, :cond_0

    move v0, v1

    .line 557
    :goto_0
    if-eqz v0, :cond_2

    .line 558
    iget v0, p0, Lcom/peel/widget/SlidingDrawer;->H:I

    int-to-float v0, v0

    iput v0, p0, Lcom/peel/widget/SlidingDrawer;->u:F

    .line 559
    iget v0, p0, Lcom/peel/widget/SlidingDrawer;->G:I

    int-to-float v0, v0

    iput v0, p0, Lcom/peel/widget/SlidingDrawer;->v:F

    .line 560
    iget v2, p0, Lcom/peel/widget/SlidingDrawer;->l:I

    iget-boolean v0, p0, Lcom/peel/widget/SlidingDrawer;->j:Z

    if-eqz v0, :cond_1

    .line 561
    invoke-virtual {p0}, Lcom/peel/widget/SlidingDrawer;->getHeight()I

    move-result v0

    iget v3, p0, Lcom/peel/widget/SlidingDrawer;->n:I

    sub-int/2addr v0, v3

    :goto_1
    add-int/2addr v0, v2

    int-to-float v0, v0

    iput v0, p0, Lcom/peel/widget/SlidingDrawer;->w:F

    .line 562
    iget v0, p0, Lcom/peel/widget/SlidingDrawer;->w:F

    float-to-int v0, v0

    invoke-direct {p0, v0}, Lcom/peel/widget/SlidingDrawer;->d(I)V

    .line 563
    iput-boolean v1, p0, Lcom/peel/widget/SlidingDrawer;->A:Z

    .line 564
    iget-object v0, p0, Lcom/peel/widget/SlidingDrawer;->t:Landroid/os/Handler;

    invoke-virtual {v0, v4}, Landroid/os/Handler;->removeMessages(I)V

    .line 565
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    .line 566
    iput-wide v2, p0, Lcom/peel/widget/SlidingDrawer;->x:J

    .line 567
    const-wide/16 v4, 0x10

    add-long/2addr v2, v4

    iput-wide v2, p0, Lcom/peel/widget/SlidingDrawer;->y:J

    .line 568
    iput-boolean v1, p0, Lcom/peel/widget/SlidingDrawer;->A:Z

    .line 576
    :goto_2
    return-void

    :cond_0
    move v0, v2

    .line 556
    goto :goto_0

    .line 561
    :cond_1
    invoke-virtual {p0}, Lcom/peel/widget/SlidingDrawer;->getWidth()I

    move-result v0

    iget v3, p0, Lcom/peel/widget/SlidingDrawer;->o:I

    sub-int/2addr v0, v3

    goto :goto_1

    .line 570
    :cond_2
    iget-boolean v0, p0, Lcom/peel/widget/SlidingDrawer;->A:Z

    if-eqz v0, :cond_3

    .line 571
    iput-boolean v2, p0, Lcom/peel/widget/SlidingDrawer;->A:Z

    .line 572
    iget-object v0, p0, Lcom/peel/widget/SlidingDrawer;->t:Landroid/os/Handler;

    invoke-virtual {v0, v4}, Landroid/os/Handler;->removeMessages(I)V

    .line 574
    :cond_3
    invoke-direct {p0, p1}, Lcom/peel/widget/SlidingDrawer;->d(I)V

    goto :goto_2
.end method

.method static synthetic c(Lcom/peel/widget/SlidingDrawer;)V
    .locals 0

    .prologue
    .line 82
    invoke-direct {p0}, Lcom/peel/widget/SlidingDrawer;->i()V

    return-void
.end method

.method private d(I)V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/16 v3, -0x2711

    const/16 v2, -0x2712

    .line 579
    iget-object v1, p0, Lcom/peel/widget/SlidingDrawer;->c:Landroid/view/View;

    .line 581
    iget-boolean v0, p0, Lcom/peel/widget/SlidingDrawer;->j:Z

    if-eqz v0, :cond_4

    .line 582
    if-ne p1, v3, :cond_0

    .line 583
    iget v0, p0, Lcom/peel/widget/SlidingDrawer;->m:I

    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v2

    sub-int/2addr v0, v2

    invoke-virtual {v1, v0}, Landroid/view/View;->offsetTopAndBottom(I)V

    .line 584
    invoke-virtual {p0}, Lcom/peel/widget/SlidingDrawer;->invalidate()V

    .line 642
    :goto_0
    return-void

    .line 585
    :cond_0
    if-ne p1, v2, :cond_1

    .line 586
    iget v0, p0, Lcom/peel/widget/SlidingDrawer;->l:I

    invoke-virtual {p0}, Lcom/peel/widget/SlidingDrawer;->getBottom()I

    move-result v2

    add-int/2addr v0, v2

    invoke-virtual {p0}, Lcom/peel/widget/SlidingDrawer;->getTop()I

    move-result v2

    sub-int/2addr v0, v2

    iget v2, p0, Lcom/peel/widget/SlidingDrawer;->n:I

    sub-int/2addr v0, v2

    .line 587
    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v2

    sub-int/2addr v0, v2

    .line 586
    invoke-virtual {v1, v0}, Landroid/view/View;->offsetTopAndBottom(I)V

    .line 588
    invoke-virtual {p0}, Lcom/peel/widget/SlidingDrawer;->invalidate()V

    goto :goto_0

    .line 590
    :cond_1
    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v2

    .line 591
    sub-int v0, p1, v2

    .line 592
    iget v3, p0, Lcom/peel/widget/SlidingDrawer;->m:I

    if-ge p1, v3, :cond_3

    .line 593
    iget v0, p0, Lcom/peel/widget/SlidingDrawer;->m:I

    sub-int/2addr v0, v2

    .line 597
    :cond_2
    :goto_1
    invoke-virtual {v1, v0}, Landroid/view/View;->offsetTopAndBottom(I)V

    .line 599
    iget-object v2, p0, Lcom/peel/widget/SlidingDrawer;->e:Landroid/graphics/Rect;

    .line 600
    iget-object v3, p0, Lcom/peel/widget/SlidingDrawer;->f:Landroid/graphics/Rect;

    .line 602
    invoke-virtual {v1, v2}, Landroid/view/View;->getHitRect(Landroid/graphics/Rect;)V

    .line 603
    invoke-virtual {v3, v2}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 605
    iget v1, v2, Landroid/graphics/Rect;->left:I

    iget v4, v2, Landroid/graphics/Rect;->top:I

    sub-int/2addr v4, v0

    iget v5, v2, Landroid/graphics/Rect;->right:I

    iget v6, v2, Landroid/graphics/Rect;->bottom:I

    sub-int/2addr v6, v0

    invoke-virtual {v3, v1, v4, v5, v6}, Landroid/graphics/Rect;->union(IIII)V

    .line 606
    iget v1, v2, Landroid/graphics/Rect;->bottom:I

    sub-int/2addr v1, v0

    invoke-virtual {p0}, Lcom/peel/widget/SlidingDrawer;->getWidth()I

    move-result v4

    iget v2, v2, Landroid/graphics/Rect;->bottom:I

    sub-int v0, v2, v0

    iget-object v2, p0, Lcom/peel/widget/SlidingDrawer;->d:Landroid/view/View;

    .line 607
    invoke-virtual {v2}, Landroid/view/View;->getHeight()I

    move-result v2

    add-int/2addr v0, v2

    .line 606
    invoke-virtual {v3, v7, v1, v4, v0}, Landroid/graphics/Rect;->union(IIII)V

    .line 609
    invoke-virtual {p0, v3}, Lcom/peel/widget/SlidingDrawer;->invalidate(Landroid/graphics/Rect;)V

    goto :goto_0

    .line 594
    :cond_3
    iget v3, p0, Lcom/peel/widget/SlidingDrawer;->l:I

    invoke-virtual {p0}, Lcom/peel/widget/SlidingDrawer;->getBottom()I

    move-result v4

    add-int/2addr v3, v4

    invoke-virtual {p0}, Lcom/peel/widget/SlidingDrawer;->getTop()I

    move-result v4

    sub-int/2addr v3, v4

    iget v4, p0, Lcom/peel/widget/SlidingDrawer;->n:I

    sub-int/2addr v3, v4

    sub-int/2addr v3, v2

    if-le v0, v3, :cond_2

    .line 595
    iget v0, p0, Lcom/peel/widget/SlidingDrawer;->l:I

    invoke-virtual {p0}, Lcom/peel/widget/SlidingDrawer;->getBottom()I

    move-result v3

    add-int/2addr v0, v3

    invoke-virtual {p0}, Lcom/peel/widget/SlidingDrawer;->getTop()I

    move-result v3

    sub-int/2addr v0, v3

    iget v3, p0, Lcom/peel/widget/SlidingDrawer;->n:I

    sub-int/2addr v0, v3

    sub-int/2addr v0, v2

    goto :goto_1

    .line 612
    :cond_4
    if-ne p1, v3, :cond_5

    .line 613
    iget v0, p0, Lcom/peel/widget/SlidingDrawer;->m:I

    invoke-virtual {v1}, Landroid/view/View;->getLeft()I

    move-result v2

    sub-int/2addr v0, v2

    invoke-virtual {v1, v0}, Landroid/view/View;->offsetLeftAndRight(I)V

    .line 614
    invoke-virtual {p0}, Lcom/peel/widget/SlidingDrawer;->invalidate()V

    goto/16 :goto_0

    .line 615
    :cond_5
    if-ne p1, v2, :cond_6

    .line 616
    iget v0, p0, Lcom/peel/widget/SlidingDrawer;->l:I

    invoke-virtual {p0}, Lcom/peel/widget/SlidingDrawer;->getRight()I

    move-result v2

    add-int/2addr v0, v2

    invoke-virtual {p0}, Lcom/peel/widget/SlidingDrawer;->getLeft()I

    move-result v2

    sub-int/2addr v0, v2

    iget v2, p0, Lcom/peel/widget/SlidingDrawer;->o:I

    sub-int/2addr v0, v2

    .line 617
    invoke-virtual {v1}, Landroid/view/View;->getLeft()I

    move-result v2

    sub-int/2addr v0, v2

    .line 616
    invoke-virtual {v1, v0}, Landroid/view/View;->offsetLeftAndRight(I)V

    .line 618
    invoke-virtual {p0}, Lcom/peel/widget/SlidingDrawer;->invalidate()V

    goto/16 :goto_0

    .line 620
    :cond_6
    invoke-virtual {v1}, Landroid/view/View;->getLeft()I

    move-result v2

    .line 621
    sub-int v0, p1, v2

    .line 622
    iget v3, p0, Lcom/peel/widget/SlidingDrawer;->m:I

    if-ge p1, v3, :cond_8

    .line 623
    iget v0, p0, Lcom/peel/widget/SlidingDrawer;->m:I

    sub-int/2addr v0, v2

    .line 627
    :cond_7
    :goto_2
    invoke-virtual {v1, v0}, Landroid/view/View;->offsetLeftAndRight(I)V

    .line 629
    iget-object v2, p0, Lcom/peel/widget/SlidingDrawer;->e:Landroid/graphics/Rect;

    .line 630
    iget-object v3, p0, Lcom/peel/widget/SlidingDrawer;->f:Landroid/graphics/Rect;

    .line 632
    invoke-virtual {v1, v2}, Landroid/view/View;->getHitRect(Landroid/graphics/Rect;)V

    .line 633
    invoke-virtual {v3, v2}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 635
    iget v1, v2, Landroid/graphics/Rect;->left:I

    sub-int/2addr v1, v0

    iget v4, v2, Landroid/graphics/Rect;->top:I

    iget v5, v2, Landroid/graphics/Rect;->right:I

    sub-int/2addr v5, v0

    iget v6, v2, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {v3, v1, v4, v5, v6}, Landroid/graphics/Rect;->union(IIII)V

    .line 636
    iget v1, v2, Landroid/graphics/Rect;->right:I

    sub-int/2addr v1, v0

    iget v2, v2, Landroid/graphics/Rect;->right:I

    sub-int v0, v2, v0

    iget-object v2, p0, Lcom/peel/widget/SlidingDrawer;->d:Landroid/view/View;

    .line 637
    invoke-virtual {v2}, Landroid/view/View;->getWidth()I

    move-result v2

    add-int/2addr v0, v2

    invoke-virtual {p0}, Lcom/peel/widget/SlidingDrawer;->getHeight()I

    move-result v2

    .line 636
    invoke-virtual {v3, v1, v7, v0, v2}, Landroid/graphics/Rect;->union(IIII)V

    .line 639
    invoke-virtual {p0, v3}, Lcom/peel/widget/SlidingDrawer;->invalidate(Landroid/graphics/Rect;)V

    goto/16 :goto_0

    .line 624
    :cond_8
    iget v3, p0, Lcom/peel/widget/SlidingDrawer;->l:I

    invoke-virtual {p0}, Lcom/peel/widget/SlidingDrawer;->getRight()I

    move-result v4

    add-int/2addr v3, v4

    invoke-virtual {p0}, Lcom/peel/widget/SlidingDrawer;->getLeft()I

    move-result v4

    sub-int/2addr v3, v4

    iget v4, p0, Lcom/peel/widget/SlidingDrawer;->o:I

    sub-int/2addr v3, v4

    sub-int/2addr v3, v2

    if-le v0, v3, :cond_7

    .line 625
    iget v0, p0, Lcom/peel/widget/SlidingDrawer;->l:I

    invoke-virtual {p0}, Lcom/peel/widget/SlidingDrawer;->getRight()I

    move-result v3

    add-int/2addr v0, v3

    invoke-virtual {p0}, Lcom/peel/widget/SlidingDrawer;->getLeft()I

    move-result v3

    sub-int/2addr v0, v3

    iget v3, p0, Lcom/peel/widget/SlidingDrawer;->o:I

    sub-int/2addr v0, v3

    sub-int/2addr v0, v2

    goto :goto_2
.end method

.method private g()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/high16 v5, 0x40000000    # 2.0f

    .line 645
    iget-boolean v0, p0, Lcom/peel/widget/SlidingDrawer;->A:Z

    if-eqz v0, :cond_0

    .line 676
    :goto_0
    return-void

    .line 651
    :cond_0
    iget-object v0, p0, Lcom/peel/widget/SlidingDrawer;->d:Landroid/view/View;

    .line 652
    invoke-virtual {v0}, Landroid/view/View;->isLayoutRequested()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 653
    iget-boolean v1, p0, Lcom/peel/widget/SlidingDrawer;->j:Z

    if-eqz v1, :cond_3

    .line 654
    iget v1, p0, Lcom/peel/widget/SlidingDrawer;->n:I

    .line 655
    invoke-virtual {p0}, Lcom/peel/widget/SlidingDrawer;->getBottom()I

    move-result v2

    invoke-virtual {p0}, Lcom/peel/widget/SlidingDrawer;->getTop()I

    move-result v3

    sub-int/2addr v2, v3

    sub-int/2addr v2, v1

    iget v3, p0, Lcom/peel/widget/SlidingDrawer;->m:I

    sub-int/2addr v2, v3

    .line 656
    invoke-virtual {p0}, Lcom/peel/widget/SlidingDrawer;->getRight()I

    move-result v3

    invoke-virtual {p0}, Lcom/peel/widget/SlidingDrawer;->getLeft()I

    move-result v4

    sub-int/2addr v3, v4

    invoke-static {v3, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    .line 657
    invoke-static {v2, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    .line 656
    invoke-virtual {v0, v3, v2}, Landroid/view/View;->measure(II)V

    .line 658
    iget v2, p0, Lcom/peel/widget/SlidingDrawer;->m:I

    add-int/2addr v2, v1

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v3

    iget v4, p0, Lcom/peel/widget/SlidingDrawer;->m:I

    add-int/2addr v1, v4

    .line 659
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v4

    add-int/2addr v1, v4

    .line 658
    invoke-virtual {v0, v6, v2, v3, v1}, Landroid/view/View;->layout(IIII)V

    .line 672
    :cond_1
    :goto_1
    invoke-virtual {v0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/ViewTreeObserver;->dispatchOnPreDraw()Z

    .line 673
    invoke-virtual {v0}, Landroid/view/View;->isHardwareAccelerated()Z

    move-result v1

    if-nez v1, :cond_2

    invoke-virtual {v0}, Landroid/view/View;->buildDrawingCache()V

    .line 675
    :cond_2
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 661
    :cond_3
    iget-object v1, p0, Lcom/peel/widget/SlidingDrawer;->c:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getWidth()I

    move-result v1

    .line 662
    invoke-virtual {p0}, Lcom/peel/widget/SlidingDrawer;->getRight()I

    move-result v2

    invoke-virtual {p0}, Lcom/peel/widget/SlidingDrawer;->getLeft()I

    move-result v3

    sub-int/2addr v2, v3

    sub-int/2addr v2, v1

    iget v3, p0, Lcom/peel/widget/SlidingDrawer;->m:I

    sub-int/2addr v2, v3

    .line 663
    invoke-static {v2, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    .line 664
    invoke-virtual {p0}, Lcom/peel/widget/SlidingDrawer;->getBottom()I

    move-result v3

    invoke-virtual {p0}, Lcom/peel/widget/SlidingDrawer;->getTop()I

    move-result v4

    sub-int/2addr v3, v4

    invoke-static {v3, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    .line 663
    invoke-virtual {v0, v2, v3}, Landroid/view/View;->measure(II)V

    .line 665
    iget v2, p0, Lcom/peel/widget/SlidingDrawer;->m:I

    add-int/2addr v2, v1

    iget v3, p0, Lcom/peel/widget/SlidingDrawer;->m:I

    add-int/2addr v1, v3

    .line 666
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v3

    add-int/2addr v1, v3

    .line 667
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v3

    .line 665
    invoke-virtual {v0, v2, v6, v1, v3}, Landroid/view/View;->layout(IIII)V

    goto :goto_1
.end method

.method private h()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 679
    iget-object v0, p0, Lcom/peel/widget/SlidingDrawer;->c:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setPressed(Z)V

    .line 680
    iput-boolean v1, p0, Lcom/peel/widget/SlidingDrawer;->g:Z

    .line 682
    iget-object v0, p0, Lcom/peel/widget/SlidingDrawer;->s:Lcom/peel/widget/an;

    if-eqz v0, :cond_0

    .line 683
    iget-object v0, p0, Lcom/peel/widget/SlidingDrawer;->s:Lcom/peel/widget/an;

    invoke-interface {v0}, Lcom/peel/widget/an;->b()V

    .line 686
    :cond_0
    iget-object v0, p0, Lcom/peel/widget/SlidingDrawer;->i:Landroid/view/VelocityTracker;

    if-eqz v0, :cond_1

    .line 687
    iget-object v0, p0, Lcom/peel/widget/SlidingDrawer;->i:Landroid/view/VelocityTracker;

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->recycle()V

    .line 688
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/peel/widget/SlidingDrawer;->i:Landroid/view/VelocityTracker;

    .line 690
    :cond_1
    return-void
.end method

.method private i()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 693
    iget-boolean v0, p0, Lcom/peel/widget/SlidingDrawer;->A:Z

    if-eqz v0, :cond_0

    .line 694
    invoke-direct {p0}, Lcom/peel/widget/SlidingDrawer;->j()V

    .line 695
    iget v1, p0, Lcom/peel/widget/SlidingDrawer;->w:F

    iget v2, p0, Lcom/peel/widget/SlidingDrawer;->l:I

    iget-boolean v0, p0, Lcom/peel/widget/SlidingDrawer;->j:Z

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/peel/widget/SlidingDrawer;->getHeight()I

    move-result v0

    :goto_0
    add-int/2addr v0, v2

    add-int/lit8 v0, v0, -0x1

    int-to-float v0, v0

    cmpl-float v0, v1, v0

    if-ltz v0, :cond_2

    .line 696
    iput-boolean v3, p0, Lcom/peel/widget/SlidingDrawer;->A:Z

    .line 697
    invoke-direct {p0}, Lcom/peel/widget/SlidingDrawer;->k()V

    .line 708
    :cond_0
    :goto_1
    return-void

    .line 695
    :cond_1
    invoke-virtual {p0}, Lcom/peel/widget/SlidingDrawer;->getWidth()I

    move-result v0

    goto :goto_0

    .line 698
    :cond_2
    iget v0, p0, Lcom/peel/widget/SlidingDrawer;->w:F

    iget v1, p0, Lcom/peel/widget/SlidingDrawer;->m:I

    int-to-float v1, v1

    cmpg-float v0, v0, v1

    if-gez v0, :cond_3

    .line 699
    iput-boolean v3, p0, Lcom/peel/widget/SlidingDrawer;->A:Z

    .line 700
    invoke-direct {p0}, Lcom/peel/widget/SlidingDrawer;->l()V

    goto :goto_1

    .line 702
    :cond_3
    iget v0, p0, Lcom/peel/widget/SlidingDrawer;->w:F

    float-to-int v0, v0

    invoke-direct {p0, v0}, Lcom/peel/widget/SlidingDrawer;->d(I)V

    .line 703
    iget-wide v0, p0, Lcom/peel/widget/SlidingDrawer;->y:J

    const-wide/16 v2, 0x10

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/peel/widget/SlidingDrawer;->y:J

    .line 704
    iget-object v0, p0, Lcom/peel/widget/SlidingDrawer;->t:Landroid/os/Handler;

    iget-object v1, p0, Lcom/peel/widget/SlidingDrawer;->t:Landroid/os/Handler;

    const/16 v2, 0x3e8

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    iget-wide v2, p0, Lcom/peel/widget/SlidingDrawer;->y:J

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageAtTime(Landroid/os/Message;J)Z

    goto :goto_1
.end method

.method private j()V
    .locals 7

    .prologue
    .line 711
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    .line 712
    iget-wide v2, p0, Lcom/peel/widget/SlidingDrawer;->x:J

    sub-long v2, v0, v2

    long-to-float v2, v2

    const/high16 v3, 0x447a0000    # 1000.0f

    div-float/2addr v2, v3

    .line 713
    iget v3, p0, Lcom/peel/widget/SlidingDrawer;->w:F

    .line 714
    iget v4, p0, Lcom/peel/widget/SlidingDrawer;->v:F

    .line 715
    iget v5, p0, Lcom/peel/widget/SlidingDrawer;->u:F

    .line 716
    mul-float v6, v4, v2

    add-float/2addr v3, v6

    const/high16 v6, 0x3f000000    # 0.5f

    mul-float/2addr v6, v5

    mul-float/2addr v6, v2

    mul-float/2addr v6, v2

    add-float/2addr v3, v6

    iput v3, p0, Lcom/peel/widget/SlidingDrawer;->w:F

    .line 717
    mul-float/2addr v2, v5

    add-float/2addr v2, v4

    iput v2, p0, Lcom/peel/widget/SlidingDrawer;->v:F

    .line 718
    iput-wide v0, p0, Lcom/peel/widget/SlidingDrawer;->x:J

    .line 719
    return-void
.end method

.method private k()V
    .locals 2

    .prologue
    .line 844
    const/16 v0, -0x2712

    invoke-direct {p0, v0}, Lcom/peel/widget/SlidingDrawer;->d(I)V

    .line 845
    iget-object v0, p0, Lcom/peel/widget/SlidingDrawer;->d:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 846
    iget-object v0, p0, Lcom/peel/widget/SlidingDrawer;->d:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->destroyDrawingCache()V

    .line 848
    iget-boolean v0, p0, Lcom/peel/widget/SlidingDrawer;->k:Z

    if-nez v0, :cond_1

    .line 856
    :cond_0
    :goto_0
    return-void

    .line 852
    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/peel/widget/SlidingDrawer;->k:Z

    .line 853
    iget-object v0, p0, Lcom/peel/widget/SlidingDrawer;->r:Lcom/peel/widget/al;

    if-eqz v0, :cond_0

    .line 854
    iget-object v0, p0, Lcom/peel/widget/SlidingDrawer;->r:Lcom/peel/widget/al;

    invoke-interface {v0}, Lcom/peel/widget/al;->a()V

    goto :goto_0
.end method

.method private l()V
    .locals 2

    .prologue
    .line 859
    const/16 v0, -0x2711

    invoke-direct {p0, v0}, Lcom/peel/widget/SlidingDrawer;->d(I)V

    .line 860
    iget-object v0, p0, Lcom/peel/widget/SlidingDrawer;->d:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 862
    iget-boolean v0, p0, Lcom/peel/widget/SlidingDrawer;->k:Z

    if-eqz v0, :cond_1

    .line 871
    :cond_0
    :goto_0
    return-void

    .line 866
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/peel/widget/SlidingDrawer;->k:Z

    .line 868
    iget-object v0, p0, Lcom/peel/widget/SlidingDrawer;->q:Lcom/peel/widget/am;

    if-eqz v0, :cond_0

    .line 869
    iget-object v0, p0, Lcom/peel/widget/SlidingDrawer;->q:Lcom/peel/widget/am;

    invoke-interface {v0}, Lcom/peel/widget/am;->a()V

    goto :goto_0
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 731
    iget-boolean v0, p0, Lcom/peel/widget/SlidingDrawer;->k:Z

    if-nez v0, :cond_0

    .line 732
    invoke-direct {p0}, Lcom/peel/widget/SlidingDrawer;->l()V

    .line 736
    :goto_0
    invoke-virtual {p0}, Lcom/peel/widget/SlidingDrawer;->invalidate()V

    .line 737
    invoke-virtual {p0}, Lcom/peel/widget/SlidingDrawer;->requestLayout()V

    .line 738
    return-void

    .line 734
    :cond_0
    invoke-direct {p0}, Lcom/peel/widget/SlidingDrawer;->k()V

    goto :goto_0
.end method

.method public b()V
    .locals 1

    .prologue
    .line 750
    iget-boolean v0, p0, Lcom/peel/widget/SlidingDrawer;->k:Z

    if-nez v0, :cond_0

    .line 751
    invoke-virtual {p0}, Lcom/peel/widget/SlidingDrawer;->d()V

    .line 755
    :goto_0
    return-void

    .line 753
    :cond_0
    invoke-virtual {p0}, Lcom/peel/widget/SlidingDrawer;->c()V

    goto :goto_0
.end method

.method public c()V
    .locals 2

    .prologue
    .line 795
    invoke-direct {p0}, Lcom/peel/widget/SlidingDrawer;->g()V

    .line 796
    iget-object v1, p0, Lcom/peel/widget/SlidingDrawer;->s:Lcom/peel/widget/an;

    .line 797
    if-eqz v1, :cond_0

    .line 798
    invoke-interface {v1}, Lcom/peel/widget/an;->a()V

    .line 800
    :cond_0
    iget-boolean v0, p0, Lcom/peel/widget/SlidingDrawer;->j:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/peel/widget/SlidingDrawer;->c:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v0

    :goto_0
    invoke-direct {p0, v0}, Lcom/peel/widget/SlidingDrawer;->a(I)V

    .line 802
    if-eqz v1, :cond_1

    .line 803
    invoke-interface {v1}, Lcom/peel/widget/an;->b()V

    .line 805
    :cond_1
    return-void

    .line 800
    :cond_2
    iget-object v0, p0, Lcom/peel/widget/SlidingDrawer;->c:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v0

    goto :goto_0
.end method

.method public d()V
    .locals 2

    .prologue
    .line 817
    invoke-direct {p0}, Lcom/peel/widget/SlidingDrawer;->g()V

    .line 818
    iget-object v1, p0, Lcom/peel/widget/SlidingDrawer;->s:Lcom/peel/widget/an;

    .line 819
    if-eqz v1, :cond_0

    .line 820
    invoke-interface {v1}, Lcom/peel/widget/an;->a()V

    .line 822
    :cond_0
    iget-boolean v0, p0, Lcom/peel/widget/SlidingDrawer;->j:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/peel/widget/SlidingDrawer;->c:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v0

    :goto_0
    invoke-direct {p0, v0}, Lcom/peel/widget/SlidingDrawer;->b(I)V

    .line 824
    const/16 v0, 0x20

    invoke-virtual {p0, v0}, Lcom/peel/widget/SlidingDrawer;->sendAccessibilityEvent(I)V

    .line 826
    if-eqz v1, :cond_1

    .line 827
    invoke-interface {v1}, Lcom/peel/widget/an;->b()V

    .line 829
    :cond_1
    return-void

    .line 822
    :cond_2
    iget-object v0, p0, Lcom/peel/widget/SlidingDrawer;->c:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v0

    goto :goto_0
.end method

.method protected dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v0, 0x0

    .line 288
    invoke-virtual {p0}, Lcom/peel/widget/SlidingDrawer;->getDrawingTime()J

    move-result-wide v2

    .line 289
    iget-object v4, p0, Lcom/peel/widget/SlidingDrawer;->c:Landroid/view/View;

    .line 290
    iget-boolean v5, p0, Lcom/peel/widget/SlidingDrawer;->j:Z

    .line 292
    iget-boolean v1, p0, Lcom/peel/widget/SlidingDrawer;->g:Z

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lcom/peel/widget/SlidingDrawer;->A:Z

    if-eqz v1, :cond_6

    .line 293
    :cond_0
    iget-object v1, p0, Lcom/peel/widget/SlidingDrawer;->d:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getDrawingCache()Landroid/graphics/Bitmap;

    move-result-object v1

    .line 294
    if-eqz v1, :cond_3

    .line 295
    if-eqz v5, :cond_2

    .line 296
    invoke-virtual {v4}, Landroid/view/View;->getBottom()I

    move-result v5

    int-to-float v5, v5

    invoke-virtual {p1, v1, v0, v5, v6}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 311
    :cond_1
    :goto_0
    invoke-virtual {p0, p1, v4, v2, v3}, Lcom/peel/widget/SlidingDrawer;->drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z

    .line 312
    return-void

    .line 298
    :cond_2
    invoke-virtual {v4}, Landroid/view/View;->getRight()I

    move-result v5

    int-to-float v5, v5

    invoke-virtual {p1, v1, v5, v0, v6}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto :goto_0

    .line 301
    :cond_3
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 302
    if-eqz v5, :cond_5

    move v1, v0

    :goto_1
    if-eqz v5, :cond_4

    .line 303
    invoke-virtual {v4}, Landroid/view/View;->getTop()I

    move-result v0

    iget v5, p0, Lcom/peel/widget/SlidingDrawer;->m:I

    sub-int/2addr v0, v5

    int-to-float v0, v0

    .line 302
    :cond_4
    invoke-virtual {p1, v1, v0}, Landroid/graphics/Canvas;->translate(FF)V

    .line 304
    iget-object v0, p0, Lcom/peel/widget/SlidingDrawer;->d:Landroid/view/View;

    invoke-virtual {p0, p1, v0, v2, v3}, Lcom/peel/widget/SlidingDrawer;->drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z

    .line 305
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    goto :goto_0

    .line 302
    :cond_5
    invoke-virtual {v4}, Landroid/view/View;->getLeft()I

    move-result v1

    iget v6, p0, Lcom/peel/widget/SlidingDrawer;->m:I

    sub-int/2addr v1, v6

    int-to-float v1, v1

    goto :goto_1

    .line 307
    :cond_6
    iget-boolean v0, p0, Lcom/peel/widget/SlidingDrawer;->k:Z

    if-eqz v0, :cond_1

    .line 308
    iget-object v0, p0, Lcom/peel/widget/SlidingDrawer;->d:Landroid/view/View;

    invoke-virtual {p0, p1, v0, v2, v3}, Lcom/peel/widget/SlidingDrawer;->drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z

    goto :goto_0
.end method

.method public e()Z
    .locals 1

    .prologue
    .line 947
    iget-boolean v0, p0, Lcom/peel/widget/SlidingDrawer;->k:Z

    return v0
.end method

.method public f()Z
    .locals 1

    .prologue
    .line 956
    iget-boolean v0, p0, Lcom/peel/widget/SlidingDrawer;->g:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/peel/widget/SlidingDrawer;->A:Z

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getContent()Landroid/view/View;
    .locals 1

    .prologue
    .line 920
    iget-object v0, p0, Lcom/peel/widget/SlidingDrawer;->d:Landroid/view/View;

    return-object v0
.end method

.method public getHandle()Landroid/view/View;
    .locals 1

    .prologue
    .line 910
    iget-object v0, p0, Lcom/peel/widget/SlidingDrawer;->c:Landroid/view/View;

    return-object v0
.end method

.method protected onFinishInflate()V
    .locals 3

    .prologue
    .line 242
    iget v0, p0, Lcom/peel/widget/SlidingDrawer;->a:I

    invoke-virtual {p0, v0}, Lcom/peel/widget/SlidingDrawer;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/widget/SlidingDrawer;->c:Landroid/view/View;

    .line 243
    iget-object v0, p0, Lcom/peel/widget/SlidingDrawer;->c:Landroid/view/View;

    if-nez v0, :cond_0

    .line 244
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "The handle attribute is must refer to an existing child."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 247
    :cond_0
    iget-object v0, p0, Lcom/peel/widget/SlidingDrawer;->c:Landroid/view/View;

    new-instance v1, Lcom/peel/widget/ak;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/peel/widget/ak;-><init>(Lcom/peel/widget/SlidingDrawer;Lcom/peel/widget/aj;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 249
    iget v0, p0, Lcom/peel/widget/SlidingDrawer;->b:I

    invoke-virtual {p0, v0}, Lcom/peel/widget/SlidingDrawer;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/widget/SlidingDrawer;->d:Landroid/view/View;

    .line 250
    iget-object v0, p0, Lcom/peel/widget/SlidingDrawer;->d:Landroid/view/View;

    if-nez v0, :cond_1

    .line 251
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "The content attribute is must refer to an existing child."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 254
    :cond_1
    iget-object v0, p0, Lcom/peel/widget/SlidingDrawer;->d:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 255
    return-void
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 10

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 361
    iget-boolean v2, p0, Lcom/peel/widget/SlidingDrawer;->h:Z

    if-eqz v2, :cond_1

    .line 408
    :cond_0
    :goto_0
    return v0

    .line 365
    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    .line 367
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    .line 368
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    .line 370
    iget-object v5, p0, Lcom/peel/widget/SlidingDrawer;->e:Landroid/graphics/Rect;

    .line 371
    iget-object v6, p0, Lcom/peel/widget/SlidingDrawer;->c:Landroid/view/View;

    .line 373
    invoke-virtual {v6, v5}, Landroid/view/View;->getHitRect(Landroid/graphics/Rect;)V

    .line 376
    iget-boolean v7, p0, Lcom/peel/widget/SlidingDrawer;->k:Z

    if-eqz v7, :cond_2

    .line 377
    iget v7, v5, Landroid/graphics/Rect;->top:I

    int-to-float v7, v7

    const/high16 v8, 0x42280000    # 42.0f

    invoke-virtual {p0}, Lcom/peel/widget/SlidingDrawer;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    invoke-virtual {v9}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v9

    iget v9, v9, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v8, v9

    add-float/2addr v7, v8

    float-to-int v7, v7

    iput v7, v5, Landroid/graphics/Rect;->top:I

    .line 380
    :cond_2
    iget-boolean v7, p0, Lcom/peel/widget/SlidingDrawer;->g:Z

    if-nez v7, :cond_3

    float-to-int v7, v3

    float-to-int v8, v4

    invoke-virtual {v5, v7, v8}, Landroid/graphics/Rect;->contains(II)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 384
    :cond_3
    if-eqz v2, :cond_4

    const/4 v0, 0x2

    if-ne v2, v0, :cond_6

    .line 385
    :cond_4
    iput-boolean v1, p0, Lcom/peel/widget/SlidingDrawer;->g:Z

    .line 387
    invoke-virtual {v6, v1}, Landroid/view/View;->setPressed(Z)V

    .line 389
    invoke-direct {p0}, Lcom/peel/widget/SlidingDrawer;->g()V

    .line 392
    iget-object v0, p0, Lcom/peel/widget/SlidingDrawer;->s:Lcom/peel/widget/an;

    if-eqz v0, :cond_5

    .line 393
    iget-object v0, p0, Lcom/peel/widget/SlidingDrawer;->s:Lcom/peel/widget/an;

    invoke-interface {v0}, Lcom/peel/widget/an;->a()V

    .line 396
    :cond_5
    iget-boolean v0, p0, Lcom/peel/widget/SlidingDrawer;->j:Z

    if-eqz v0, :cond_7

    .line 397
    iget-object v0, p0, Lcom/peel/widget/SlidingDrawer;->c:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v0

    .line 398
    float-to-int v2, v4

    sub-int/2addr v2, v0

    iput v2, p0, Lcom/peel/widget/SlidingDrawer;->z:I

    .line 399
    invoke-direct {p0, v0}, Lcom/peel/widget/SlidingDrawer;->c(I)V

    .line 405
    :goto_1
    iget-object v0, p0, Lcom/peel/widget/SlidingDrawer;->i:Landroid/view/VelocityTracker;

    invoke-virtual {v0, p1}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    :cond_6
    move v0, v1

    .line 408
    goto :goto_0

    .line 401
    :cond_7
    iget-object v0, p0, Lcom/peel/widget/SlidingDrawer;->c:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v0

    .line 402
    float-to-int v2, v3

    sub-int/2addr v2, v0

    iput v2, p0, Lcom/peel/widget/SlidingDrawer;->z:I

    .line 403
    invoke-direct {p0, v0}, Lcom/peel/widget/SlidingDrawer;->c(I)V

    goto :goto_1
.end method

.method protected onLayout(ZIIII)V
    .locals 12

    .prologue
    .line 316
    iget-boolean v0, p0, Lcom/peel/widget/SlidingDrawer;->g:Z

    if-eqz v0, :cond_0

    .line 357
    :goto_0
    return-void

    .line 320
    :cond_0
    sub-int v0, p4, p2

    .line 321
    sub-int v2, p5, p3

    .line 323
    iget-object v3, p0, Lcom/peel/widget/SlidingDrawer;->c:Landroid/view/View;

    .line 325
    invoke-virtual {v3}, Landroid/view/View;->getMeasuredWidth()I

    move-result v4

    .line 326
    invoke-virtual {v3}, Landroid/view/View;->getMeasuredHeight()I

    move-result v5

    .line 331
    iget-object v6, p0, Lcom/peel/widget/SlidingDrawer;->d:Landroid/view/View;

    .line 333
    iget-boolean v1, p0, Lcom/peel/widget/SlidingDrawer;->j:Z

    if-eqz v1, :cond_2

    .line 334
    sub-int/2addr v0, v4

    div-int/lit8 v1, v0, 0x2

    .line 335
    iget-boolean v0, p0, Lcom/peel/widget/SlidingDrawer;->k:Z

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/peel/widget/SlidingDrawer;->m:I

    .line 337
    :goto_1
    const/4 v2, 0x0

    iget v7, p0, Lcom/peel/widget/SlidingDrawer;->m:I

    add-int/2addr v7, v5

    invoke-virtual {v6}, Landroid/view/View;->getMeasuredWidth()I

    move-result v8

    iget v9, p0, Lcom/peel/widget/SlidingDrawer;->m:I

    add-int/2addr v9, v5

    .line 338
    invoke-virtual {v6}, Landroid/view/View;->getMeasuredHeight()I

    move-result v10

    add-int/2addr v9, v10

    .line 337
    invoke-virtual {v6, v2, v7, v8, v9}, Landroid/view/View;->layout(IIII)V

    move v11, v0

    move v0, v1

    move v1, v11

    .line 354
    :goto_2
    add-int v2, v0, v4

    add-int v4, v1, v5

    invoke-virtual {v3, v0, v1, v2, v4}, Landroid/view/View;->layout(IIII)V

    .line 355
    invoke-virtual {v3}, Landroid/view/View;->getHeight()I

    move-result v0

    iput v0, p0, Lcom/peel/widget/SlidingDrawer;->n:I

    .line 356
    invoke-virtual {v3}, Landroid/view/View;->getWidth()I

    move-result v0

    iput v0, p0, Lcom/peel/widget/SlidingDrawer;->o:I

    goto :goto_0

    .line 335
    :cond_1
    sub-int v0, v2, v5

    iget v2, p0, Lcom/peel/widget/SlidingDrawer;->l:I

    add-int/2addr v0, v2

    goto :goto_1

    .line 340
    :cond_2
    iget-boolean v1, p0, Lcom/peel/widget/SlidingDrawer;->k:Z

    if-eqz v1, :cond_3

    iget v0, p0, Lcom/peel/widget/SlidingDrawer;->m:I

    .line 341
    :goto_3
    iget v1, p0, Lcom/peel/widget/SlidingDrawer;->p:I

    if-lez v1, :cond_4

    iget v1, p0, Lcom/peel/widget/SlidingDrawer;->p:I

    .line 343
    :goto_4
    iget-boolean v2, p0, Lcom/peel/widget/SlidingDrawer;->k:Z

    if-eqz v2, :cond_5

    .line 344
    iget v2, p0, Lcom/peel/widget/SlidingDrawer;->m:I

    const/4 v7, 0x0

    iget v8, p0, Lcom/peel/widget/SlidingDrawer;->m:I

    add-int/2addr v8, v4

    .line 345
    invoke-virtual {v6}, Landroid/view/View;->getMeasuredWidth()I

    move-result v9

    add-int/2addr v8, v9

    .line 346
    invoke-virtual {v6}, Landroid/view/View;->getMeasuredHeight()I

    move-result v9

    .line 344
    invoke-virtual {v6, v2, v7, v8, v9}, Landroid/view/View;->layout(IIII)V

    goto :goto_2

    .line 340
    :cond_3
    sub-int/2addr v0, v4

    iget v1, p0, Lcom/peel/widget/SlidingDrawer;->l:I

    add-int/2addr v0, v1

    goto :goto_3

    .line 341
    :cond_4
    sub-int v1, v2, v5

    div-int/lit8 v1, v1, 0x2

    goto :goto_4

    .line 348
    :cond_5
    iget v2, p0, Lcom/peel/widget/SlidingDrawer;->m:I

    add-int/2addr v2, v4

    const/4 v7, 0x0

    iget v8, p0, Lcom/peel/widget/SlidingDrawer;->m:I

    add-int/2addr v8, v4

    .line 349
    invoke-virtual {v6}, Landroid/view/View;->getMeasuredWidth()I

    move-result v9

    add-int/2addr v8, v9

    .line 350
    invoke-virtual {v6}, Landroid/view/View;->getMeasuredHeight()I

    move-result v9

    .line 348
    invoke-virtual {v6, v2, v7, v8, v9}, Landroid/view/View;->layout(IIII)V

    goto :goto_2
.end method

.method protected onMeasure(II)V
    .locals 6

    .prologue
    const/high16 v5, 0x40000000    # 2.0f

    .line 259
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v0

    .line 260
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    .line 262
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v2

    .line 263
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v3

    .line 265
    if-eqz v0, :cond_0

    if-nez v2, :cond_1

    .line 266
    :cond_0
    new-instance v0, Ljava/lang/RuntimeException;

    const-string/jumbo v1, "SlidingDrawer cannot have UNSPECIFIED dimensions"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 269
    :cond_1
    iget-object v0, p0, Lcom/peel/widget/SlidingDrawer;->c:Landroid/view/View;

    .line 270
    invoke-virtual {p0, v0, p1, p2}, Lcom/peel/widget/SlidingDrawer;->measureChild(Landroid/view/View;II)V

    .line 272
    iget-boolean v2, p0, Lcom/peel/widget/SlidingDrawer;->j:Z

    if-eqz v2, :cond_2

    .line 273
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    sub-int v0, v3, v0

    iget v2, p0, Lcom/peel/widget/SlidingDrawer;->m:I

    sub-int/2addr v0, v2

    .line 274
    iget-object v2, p0, Lcom/peel/widget/SlidingDrawer;->d:Landroid/view/View;

    invoke-static {v1, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    .line 275
    invoke-static {v0, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .line 274
    invoke-virtual {v2, v4, v0}, Landroid/view/View;->measure(II)V

    .line 283
    :goto_0
    invoke-virtual {p0, v1, v3}, Lcom/peel/widget/SlidingDrawer;->setMeasuredDimension(II)V

    .line 284
    return-void

    .line 278
    :cond_2
    iget v0, p0, Lcom/peel/widget/SlidingDrawer;->m:I

    sub-int v0, v1, v0

    .line 279
    iget-object v2, p0, Lcom/peel/widget/SlidingDrawer;->d:Landroid/view/View;

    invoke-static {v0, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .line 280
    invoke-static {v3, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    .line 279
    invoke-virtual {v2, v0, v4}, Landroid/view/View;->measure(II)V

    goto :goto_0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 11

    .prologue
    const/4 v4, 0x1

    const/4 v6, 0x0

    const/4 v3, 0x0

    .line 413
    iget-boolean v0, p0, Lcom/peel/widget/SlidingDrawer;->h:Z

    if-eqz v0, :cond_0

    .line 491
    :goto_0
    return v4

    .line 417
    :cond_0
    iget-boolean v0, p0, Lcom/peel/widget/SlidingDrawer;->g:Z

    if-eqz v0, :cond_1

    .line 418
    iget-object v0, p0, Lcom/peel/widget/SlidingDrawer;->i:Landroid/view/VelocityTracker;

    invoke-virtual {v0, p1}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    .line 419
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    .line 420
    packed-switch v0, :pswitch_data_0

    .line 491
    :cond_1
    :goto_1
    iget-boolean v0, p0, Lcom/peel/widget/SlidingDrawer;->g:Z

    if-nez v0, :cond_2

    iget-boolean v0, p0, Lcom/peel/widget/SlidingDrawer;->A:Z

    if-nez v0, :cond_2

    invoke-super {p0, p1}, Landroid/view/ViewGroup;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_16

    :cond_2
    move v0, v4

    :goto_2
    move v4, v0

    goto :goto_0

    .line 422
    :pswitch_0
    iget-boolean v0, p0, Lcom/peel/widget/SlidingDrawer;->j:Z

    if-eqz v0, :cond_3

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    :goto_3
    float-to-int v0, v0

    iget v1, p0, Lcom/peel/widget/SlidingDrawer;->z:I

    sub-int/2addr v0, v1

    invoke-direct {p0, v0}, Lcom/peel/widget/SlidingDrawer;->d(I)V

    goto :goto_1

    :cond_3
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    goto :goto_3

    .line 426
    :pswitch_1
    iget-object v0, p0, Lcom/peel/widget/SlidingDrawer;->i:Landroid/view/VelocityTracker;

    .line 427
    iget v1, p0, Lcom/peel/widget/SlidingDrawer;->I:I

    invoke-virtual {v0, v1}, Landroid/view/VelocityTracker;->computeCurrentVelocity(I)V

    .line 429
    invoke-virtual {v0}, Landroid/view/VelocityTracker;->getYVelocity()F

    move-result v1

    .line 430
    invoke-virtual {v0}, Landroid/view/VelocityTracker;->getXVelocity()F

    move-result v0

    .line 433
    iget-boolean v5, p0, Lcom/peel/widget/SlidingDrawer;->j:Z

    .line 434
    if-eqz v5, :cond_8

    .line 435
    cmpg-float v2, v1, v6

    if-gez v2, :cond_7

    move v2, v4

    .line 436
    :goto_4
    cmpg-float v6, v0, v6

    if-gez v6, :cond_4

    .line 437
    neg-float v0, v0

    .line 439
    :cond_4
    iget v6, p0, Lcom/peel/widget/SlidingDrawer;->F:I

    int-to-float v6, v6

    cmpl-float v6, v0, v6

    if-lez v6, :cond_18

    .line 440
    iget v0, p0, Lcom/peel/widget/SlidingDrawer;->F:I

    int-to-float v0, v0

    move v10, v2

    move v2, v1

    move v1, v0

    move v0, v10

    .line 452
    :goto_5
    float-to-double v6, v1

    float-to-double v8, v2

    invoke-static {v6, v7, v8, v9}, Ljava/lang/Math;->hypot(DD)D

    move-result-wide v6

    double-to-float v1, v6

    .line 453
    if-eqz v0, :cond_17

    .line 454
    neg-float v0, v1

    .line 457
    :goto_6
    iget-object v1, p0, Lcom/peel/widget/SlidingDrawer;->c:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v1

    .line 458
    iget-object v2, p0, Lcom/peel/widget/SlidingDrawer;->c:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getLeft()I

    move-result v2

    .line 460
    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v6

    iget v7, p0, Lcom/peel/widget/SlidingDrawer;->E:I

    int-to-float v7, v7

    cmpg-float v6, v6, v7

    if-gez v6, :cond_14

    .line 461
    if-eqz v5, :cond_b

    iget-boolean v6, p0, Lcom/peel/widget/SlidingDrawer;->k:Z

    if-eqz v6, :cond_5

    iget v6, p0, Lcom/peel/widget/SlidingDrawer;->D:I

    iget v7, p0, Lcom/peel/widget/SlidingDrawer;->m:I

    add-int/2addr v6, v7

    if-lt v1, v6, :cond_6

    :cond_5
    iget-boolean v6, p0, Lcom/peel/widget/SlidingDrawer;->k:Z

    if-nez v6, :cond_d

    iget v6, p0, Lcom/peel/widget/SlidingDrawer;->l:I

    .line 462
    invoke-virtual {p0}, Lcom/peel/widget/SlidingDrawer;->getBottom()I

    move-result v7

    add-int/2addr v6, v7

    invoke-virtual {p0}, Lcom/peel/widget/SlidingDrawer;->getTop()I

    move-result v7

    sub-int/2addr v6, v7

    iget v7, p0, Lcom/peel/widget/SlidingDrawer;->n:I

    sub-int/2addr v6, v7

    iget v7, p0, Lcom/peel/widget/SlidingDrawer;->D:I

    sub-int/2addr v6, v7

    if-le v1, v6, :cond_d

    .line 468
    :cond_6
    iget-boolean v6, p0, Lcom/peel/widget/SlidingDrawer;->B:Z

    if-eqz v6, :cond_11

    .line 469
    invoke-virtual {p0, v3}, Lcom/peel/widget/SlidingDrawer;->playSoundEffect(I)V

    .line 471
    iget-boolean v0, p0, Lcom/peel/widget/SlidingDrawer;->k:Z

    if-eqz v0, :cond_f

    .line 472
    if-eqz v5, :cond_e

    move v0, v1

    :goto_7
    invoke-direct {p0, v0}, Lcom/peel/widget/SlidingDrawer;->a(I)V

    goto/16 :goto_1

    :cond_7
    move v2, v3

    .line 435
    goto :goto_4

    .line 443
    :cond_8
    cmpg-float v2, v0, v6

    if-gez v2, :cond_a

    move v2, v4

    .line 444
    :goto_8
    cmpg-float v6, v1, v6

    if-gez v6, :cond_9

    .line 445
    neg-float v1, v1

    .line 447
    :cond_9
    iget v6, p0, Lcom/peel/widget/SlidingDrawer;->F:I

    int-to-float v6, v6

    cmpl-float v6, v1, v6

    if-lez v6, :cond_18

    .line 448
    iget v1, p0, Lcom/peel/widget/SlidingDrawer;->F:I

    int-to-float v1, v1

    move v10, v2

    move v2, v1

    move v1, v0

    move v0, v10

    goto :goto_5

    :cond_a
    move v2, v3

    .line 443
    goto :goto_8

    .line 462
    :cond_b
    iget-boolean v6, p0, Lcom/peel/widget/SlidingDrawer;->k:Z

    if-eqz v6, :cond_c

    iget v6, p0, Lcom/peel/widget/SlidingDrawer;->D:I

    iget v7, p0, Lcom/peel/widget/SlidingDrawer;->m:I

    add-int/2addr v6, v7

    if-lt v2, v6, :cond_6

    :cond_c
    iget-boolean v6, p0, Lcom/peel/widget/SlidingDrawer;->k:Z

    if-nez v6, :cond_d

    iget v6, p0, Lcom/peel/widget/SlidingDrawer;->l:I

    .line 465
    invoke-virtual {p0}, Lcom/peel/widget/SlidingDrawer;->getRight()I

    move-result v7

    add-int/2addr v6, v7

    invoke-virtual {p0}, Lcom/peel/widget/SlidingDrawer;->getLeft()I

    move-result v7

    sub-int/2addr v6, v7

    iget v7, p0, Lcom/peel/widget/SlidingDrawer;->o:I

    sub-int/2addr v6, v7

    iget v7, p0, Lcom/peel/widget/SlidingDrawer;->D:I

    sub-int/2addr v6, v7

    if-gt v2, v6, :cond_6

    .line 481
    :cond_d
    if-eqz v5, :cond_13

    :goto_9
    invoke-direct {p0, v1, v0, v3}, Lcom/peel/widget/SlidingDrawer;->a(IFZ)V

    goto/16 :goto_1

    :cond_e
    move v0, v2

    .line 472
    goto :goto_7

    .line 474
    :cond_f
    if-eqz v5, :cond_10

    :goto_a
    invoke-direct {p0, v1}, Lcom/peel/widget/SlidingDrawer;->b(I)V

    goto/16 :goto_1

    :cond_10
    move v1, v2

    goto :goto_a

    .line 477
    :cond_11
    if-eqz v5, :cond_12

    :goto_b
    invoke-direct {p0, v1, v0, v3}, Lcom/peel/widget/SlidingDrawer;->a(IFZ)V

    goto/16 :goto_1

    :cond_12
    move v1, v2

    goto :goto_b

    :cond_13
    move v1, v2

    .line 481
    goto :goto_9

    .line 484
    :cond_14
    if-eqz v5, :cond_15

    :goto_c
    invoke-direct {p0, v1, v0, v3}, Lcom/peel/widget/SlidingDrawer;->a(IFZ)V

    goto/16 :goto_1

    :cond_15
    move v1, v2

    goto :goto_c

    :cond_16
    move v0, v3

    .line 491
    goto/16 :goto_2

    :cond_17
    move v0, v1

    goto/16 :goto_6

    :cond_18
    move v10, v2

    move v2, v1

    move v1, v0

    move v0, v10

    goto/16 :goto_5

    .line 420
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public setOnDrawerCloseListener(Lcom/peel/widget/al;)V
    .locals 0

    .prologue
    .line 888
    iput-object p1, p0, Lcom/peel/widget/SlidingDrawer;->r:Lcom/peel/widget/al;

    .line 889
    return-void
.end method

.method public setOnDrawerOpenListener(Lcom/peel/widget/am;)V
    .locals 0

    .prologue
    .line 879
    iput-object p1, p0, Lcom/peel/widget/SlidingDrawer;->q:Lcom/peel/widget/am;

    .line 880
    return-void
.end method

.method public setOnDrawerScrollListener(Lcom/peel/widget/an;)V
    .locals 0

    .prologue
    .line 900
    iput-object p1, p0, Lcom/peel/widget/SlidingDrawer;->s:Lcom/peel/widget/an;

    .line 901
    return-void
.end method
