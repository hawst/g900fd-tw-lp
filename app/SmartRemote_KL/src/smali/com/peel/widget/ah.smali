.class Lcom/peel/widget/ah;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/peel/widget/ag;


# direct methods
.method constructor <init>(Lcom/peel/widget/ag;)V
    .locals 0

    .prologue
    .line 34
    iput-object p1, p0, Lcom/peel/widget/ah;->a:Lcom/peel/widget/ag;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    .line 38
    iget-object v0, p0, Lcom/peel/widget/ah;->a:Lcom/peel/widget/ag;

    invoke-static {v0}, Lcom/peel/widget/ag;->b(Lcom/peel/widget/ag;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 39
    iget-object v0, p0, Lcom/peel/widget/ah;->a:Lcom/peel/widget/ag;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/peel/widget/ag;->a(Lcom/peel/widget/ag;Z)Z

    .line 40
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    .line 41
    sget v1, Lcom/peel/ui/fp;->button1:I

    if-ne v0, v1, :cond_3

    .line 42
    iget-object v0, p0, Lcom/peel/widget/ah;->a:Lcom/peel/widget/ag;

    invoke-static {v0}, Lcom/peel/widget/ag;->c(Lcom/peel/widget/ag;)Landroid/content/DialogInterface$OnClickListener;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/peel/widget/ah;->a:Lcom/peel/widget/ag;

    invoke-static {v0}, Lcom/peel/widget/ag;->c(Lcom/peel/widget/ag;)Landroid/content/DialogInterface$OnClickListener;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/widget/ah;->a:Lcom/peel/widget/ag;

    const/4 v2, -0x2

    invoke-interface {v0, v1, v2}, Landroid/content/DialogInterface$OnClickListener;->onClick(Landroid/content/DialogInterface;I)V

    .line 43
    :cond_0
    iget-object v0, p0, Lcom/peel/widget/ah;->a:Lcom/peel/widget/ag;

    invoke-virtual {v0}, Lcom/peel/widget/ag;->dismiss()V

    .line 54
    :cond_1
    :goto_0
    const-class v0, Lcom/peel/widget/ag;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "delay allowing second press"

    new-instance v2, Lcom/peel/widget/ai;

    invoke-direct {v2, p0}, Lcom/peel/widget/ai;-><init>(Lcom/peel/widget/ah;)V

    const-wide/16 v4, 0x1f4

    invoke-static {v0, v1, v2, v4, v5}, Lcom/peel/util/i;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;J)Ljava/lang/Runnable;

    .line 61
    :cond_2
    return-void

    .line 45
    :cond_3
    sget v1, Lcom/peel/ui/fp;->button3:I

    if-ne v0, v1, :cond_5

    .line 46
    iget-object v0, p0, Lcom/peel/widget/ah;->a:Lcom/peel/widget/ag;

    invoke-static {v0}, Lcom/peel/widget/ag;->d(Lcom/peel/widget/ag;)Landroid/content/DialogInterface$OnClickListener;

    move-result-object v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/peel/widget/ah;->a:Lcom/peel/widget/ag;

    invoke-static {v0}, Lcom/peel/widget/ag;->d(Lcom/peel/widget/ag;)Landroid/content/DialogInterface$OnClickListener;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/widget/ah;->a:Lcom/peel/widget/ag;

    const/4 v2, -0x3

    invoke-interface {v0, v1, v2}, Landroid/content/DialogInterface$OnClickListener;->onClick(Landroid/content/DialogInterface;I)V

    .line 47
    :cond_4
    iget-object v0, p0, Lcom/peel/widget/ah;->a:Lcom/peel/widget/ag;

    invoke-virtual {v0}, Lcom/peel/widget/ag;->dismiss()V

    goto :goto_0

    .line 49
    :cond_5
    sget v1, Lcom/peel/ui/fp;->button2:I

    if-ne v0, v1, :cond_1

    .line 50
    iget-object v0, p0, Lcom/peel/widget/ah;->a:Lcom/peel/widget/ag;

    invoke-static {v0}, Lcom/peel/widget/ag;->e(Lcom/peel/widget/ag;)Landroid/content/DialogInterface$OnClickListener;

    move-result-object v0

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/peel/widget/ah;->a:Lcom/peel/widget/ag;

    invoke-static {v0}, Lcom/peel/widget/ag;->e(Lcom/peel/widget/ag;)Landroid/content/DialogInterface$OnClickListener;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/widget/ah;->a:Lcom/peel/widget/ag;

    const/4 v2, -0x1

    invoke-interface {v0, v1, v2}, Landroid/content/DialogInterface$OnClickListener;->onClick(Landroid/content/DialogInterface;I)V

    .line 51
    :cond_6
    iget-object v0, p0, Lcom/peel/widget/ah;->a:Lcom/peel/widget/ag;

    invoke-virtual {v0}, Lcom/peel/widget/ag;->dismiss()V

    goto :goto_0
.end method
