.class Lcom/peel/widget/v;
.super Landroid/animation/AnimatorListenerAdapter;


# instance fields
.field final synthetic a:Landroid/view/View;

.field final synthetic b:Lcom/peel/widget/DynamicListView;


# direct methods
.method constructor <init>(Lcom/peel/widget/DynamicListView;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 448
    iput-object p1, p0, Lcom/peel/widget/v;->b:Lcom/peel/widget/DynamicListView;

    iput-object p2, p0, Lcom/peel/widget/v;->a:Landroid/view/View;

    invoke-direct {p0}, Landroid/animation/AnimatorListenerAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 4

    .prologue
    const-wide/16 v2, -0x1

    .line 456
    iget-object v0, p0, Lcom/peel/widget/v;->b:Lcom/peel/widget/DynamicListView;

    invoke-static {v0, v2, v3}, Lcom/peel/widget/DynamicListView;->c(Lcom/peel/widget/DynamicListView;J)J

    .line 457
    iget-object v0, p0, Lcom/peel/widget/v;->b:Lcom/peel/widget/DynamicListView;

    invoke-static {v0, v2, v3}, Lcom/peel/widget/DynamicListView;->a(Lcom/peel/widget/DynamicListView;J)J

    .line 458
    iget-object v0, p0, Lcom/peel/widget/v;->b:Lcom/peel/widget/DynamicListView;

    invoke-static {v0, v2, v3}, Lcom/peel/widget/DynamicListView;->d(Lcom/peel/widget/DynamicListView;J)J

    .line 459
    iget-object v0, p0, Lcom/peel/widget/v;->a:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 460
    iget-object v0, p0, Lcom/peel/widget/v;->b:Lcom/peel/widget/DynamicListView;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/peel/widget/DynamicListView;->a(Lcom/peel/widget/DynamicListView;Landroid/graphics/drawable/BitmapDrawable;)Landroid/graphics/drawable/BitmapDrawable;

    .line 461
    iget-object v0, p0, Lcom/peel/widget/v;->b:Lcom/peel/widget/DynamicListView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/peel/widget/DynamicListView;->setEnabled(Z)V

    .line 462
    iget-object v0, p0, Lcom/peel/widget/v;->b:Lcom/peel/widget/DynamicListView;

    invoke-virtual {v0}, Lcom/peel/widget/DynamicListView;->invalidate()V

    .line 463
    return-void
.end method

.method public onAnimationStart(Landroid/animation/Animator;)V
    .locals 2

    .prologue
    .line 451
    iget-object v0, p0, Lcom/peel/widget/v;->b:Lcom/peel/widget/DynamicListView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/peel/widget/DynamicListView;->setEnabled(Z)V

    .line 452
    return-void
.end method
