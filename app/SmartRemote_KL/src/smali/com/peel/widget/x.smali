.class Lcom/peel/widget/x;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/widget/AbsListView$OnScrollListener;


# instance fields
.field final synthetic a:Lcom/peel/widget/DynamicListView;

.field private b:I

.field private c:I

.field private d:I

.field private e:I

.field private f:I


# direct methods
.method constructor <init>(Lcom/peel/widget/DynamicListView;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 552
    iput-object p1, p0, Lcom/peel/widget/x;->a:Lcom/peel/widget/DynamicListView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 554
    iput v0, p0, Lcom/peel/widget/x;->b:I

    .line 555
    iput v0, p0, Lcom/peel/widget/x;->c:I

    return-void
.end method

.method private c()V
    .locals 1

    .prologue
    .line 593
    iget v0, p0, Lcom/peel/widget/x;->e:I

    if-lez v0, :cond_0

    iget v0, p0, Lcom/peel/widget/x;->f:I

    if-nez v0, :cond_0

    .line 594
    iget-object v0, p0, Lcom/peel/widget/x;->a:Lcom/peel/widget/DynamicListView;

    invoke-static {v0}, Lcom/peel/widget/DynamicListView;->e(Lcom/peel/widget/DynamicListView;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/peel/widget/x;->a:Lcom/peel/widget/DynamicListView;

    invoke-static {v0}, Lcom/peel/widget/DynamicListView;->f(Lcom/peel/widget/DynamicListView;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 595
    iget-object v0, p0, Lcom/peel/widget/x;->a:Lcom/peel/widget/DynamicListView;

    invoke-static {v0}, Lcom/peel/widget/DynamicListView;->g(Lcom/peel/widget/DynamicListView;)V

    .line 600
    :cond_0
    :goto_0
    return-void

    .line 596
    :cond_1
    iget-object v0, p0, Lcom/peel/widget/x;->a:Lcom/peel/widget/DynamicListView;

    invoke-static {v0}, Lcom/peel/widget/DynamicListView;->h(Lcom/peel/widget/DynamicListView;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 597
    iget-object v0, p0, Lcom/peel/widget/x;->a:Lcom/peel/widget/DynamicListView;

    invoke-static {v0}, Lcom/peel/widget/DynamicListView;->i(Lcom/peel/widget/DynamicListView;)V

    goto :goto_0
.end method


# virtual methods
.method public a()V
    .locals 4

    .prologue
    .line 607
    iget v0, p0, Lcom/peel/widget/x;->d:I

    iget v1, p0, Lcom/peel/widget/x;->b:I

    if-eq v0, v1, :cond_0

    .line 608
    iget-object v0, p0, Lcom/peel/widget/x;->a:Lcom/peel/widget/DynamicListView;

    invoke-static {v0}, Lcom/peel/widget/DynamicListView;->e(Lcom/peel/widget/DynamicListView;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/peel/widget/x;->a:Lcom/peel/widget/DynamicListView;

    invoke-static {v0}, Lcom/peel/widget/DynamicListView;->c(Lcom/peel/widget/DynamicListView;)J

    move-result-wide v0

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 609
    iget-object v0, p0, Lcom/peel/widget/x;->a:Lcom/peel/widget/DynamicListView;

    iget-object v1, p0, Lcom/peel/widget/x;->a:Lcom/peel/widget/DynamicListView;

    invoke-static {v1}, Lcom/peel/widget/DynamicListView;->c(Lcom/peel/widget/DynamicListView;)J

    move-result-wide v2

    invoke-static {v0, v2, v3}, Lcom/peel/widget/DynamicListView;->b(Lcom/peel/widget/DynamicListView;J)V

    .line 610
    iget-object v0, p0, Lcom/peel/widget/x;->a:Lcom/peel/widget/DynamicListView;

    invoke-static {v0}, Lcom/peel/widget/DynamicListView;->j(Lcom/peel/widget/DynamicListView;)V

    .line 613
    :cond_0
    return-void
.end method

.method public b()V
    .locals 4

    .prologue
    .line 620
    iget v0, p0, Lcom/peel/widget/x;->d:I

    iget v1, p0, Lcom/peel/widget/x;->e:I

    add-int/2addr v0, v1

    .line 621
    iget v1, p0, Lcom/peel/widget/x;->b:I

    iget v2, p0, Lcom/peel/widget/x;->c:I

    add-int/2addr v1, v2

    .line 622
    if-eq v0, v1, :cond_0

    .line 623
    iget-object v0, p0, Lcom/peel/widget/x;->a:Lcom/peel/widget/DynamicListView;

    invoke-static {v0}, Lcom/peel/widget/DynamicListView;->e(Lcom/peel/widget/DynamicListView;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/peel/widget/x;->a:Lcom/peel/widget/DynamicListView;

    invoke-static {v0}, Lcom/peel/widget/DynamicListView;->c(Lcom/peel/widget/DynamicListView;)J

    move-result-wide v0

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 624
    iget-object v0, p0, Lcom/peel/widget/x;->a:Lcom/peel/widget/DynamicListView;

    iget-object v1, p0, Lcom/peel/widget/x;->a:Lcom/peel/widget/DynamicListView;

    invoke-static {v1}, Lcom/peel/widget/DynamicListView;->c(Lcom/peel/widget/DynamicListView;)J

    move-result-wide v2

    invoke-static {v0, v2, v3}, Lcom/peel/widget/DynamicListView;->b(Lcom/peel/widget/DynamicListView;J)V

    .line 625
    iget-object v0, p0, Lcom/peel/widget/x;->a:Lcom/peel/widget/DynamicListView;

    invoke-static {v0}, Lcom/peel/widget/DynamicListView;->j(Lcom/peel/widget/DynamicListView;)V

    .line 628
    :cond_0
    return-void
.end method

.method public onScroll(Landroid/widget/AbsListView;III)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 562
    iput p2, p0, Lcom/peel/widget/x;->d:I

    .line 563
    iput p3, p0, Lcom/peel/widget/x;->e:I

    .line 565
    iget v0, p0, Lcom/peel/widget/x;->b:I

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/peel/widget/x;->d:I

    :goto_0
    iput v0, p0, Lcom/peel/widget/x;->b:I

    .line 567
    iget v0, p0, Lcom/peel/widget/x;->c:I

    if-ne v0, v1, :cond_1

    iget v0, p0, Lcom/peel/widget/x;->e:I

    :goto_1
    iput v0, p0, Lcom/peel/widget/x;->c:I

    .line 570
    invoke-virtual {p0}, Lcom/peel/widget/x;->a()V

    .line 571
    invoke-virtual {p0}, Lcom/peel/widget/x;->b()V

    .line 573
    iget v0, p0, Lcom/peel/widget/x;->d:I

    iput v0, p0, Lcom/peel/widget/x;->b:I

    .line 574
    iget v0, p0, Lcom/peel/widget/x;->e:I

    iput v0, p0, Lcom/peel/widget/x;->c:I

    .line 575
    return-void

    .line 565
    :cond_0
    iget v0, p0, Lcom/peel/widget/x;->b:I

    goto :goto_0

    .line 567
    :cond_1
    iget v0, p0, Lcom/peel/widget/x;->c:I

    goto :goto_1
.end method

.method public onScrollStateChanged(Landroid/widget/AbsListView;I)V
    .locals 1

    .prologue
    .line 579
    iput p2, p0, Lcom/peel/widget/x;->f:I

    .line 580
    iget-object v0, p0, Lcom/peel/widget/x;->a:Lcom/peel/widget/DynamicListView;

    invoke-static {v0, p2}, Lcom/peel/widget/DynamicListView;->b(Lcom/peel/widget/DynamicListView;I)I

    .line 581
    invoke-direct {p0}, Lcom/peel/widget/x;->c()V

    .line 582
    return-void
.end method
