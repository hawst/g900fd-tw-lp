.class Lcom/peel/widget/a;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field a:I

.field b:I

.field final synthetic c:Lcom/peel/widget/AdVideoView;


# direct methods
.method constructor <init>(Lcom/peel/widget/AdVideoView;)V
    .locals 0

    .prologue
    .line 112
    iput-object p1, p0, Lcom/peel/widget/a;->c:Lcom/peel/widget/AdVideoView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    const/16 v5, 0x4b

    const/16 v4, 0x32

    const/4 v3, 0x1

    .line 116
    iget-object v0, p0, Lcom/peel/widget/a;->c:Lcom/peel/widget/AdVideoView;

    invoke-static {v0}, Lcom/peel/widget/AdVideoView;->a(Lcom/peel/widget/AdVideoView;)Landroid/media/MediaPlayer;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 117
    iget-object v0, p0, Lcom/peel/widget/a;->c:Lcom/peel/widget/AdVideoView;

    invoke-static {v0}, Lcom/peel/widget/AdVideoView;->a(Lcom/peel/widget/AdVideoView;)Landroid/media/MediaPlayer;

    move-result-object v0

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->getCurrentPosition()I

    move-result v0

    iput v0, p0, Lcom/peel/widget/a;->a:I

    .line 118
    iget-object v0, p0, Lcom/peel/widget/a;->c:Lcom/peel/widget/AdVideoView;

    invoke-static {v0}, Lcom/peel/widget/AdVideoView;->a(Lcom/peel/widget/AdVideoView;)Landroid/media/MediaPlayer;

    move-result-object v0

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->getDuration()I

    move-result v0

    iput v0, p0, Lcom/peel/widget/a;->b:I

    .line 119
    iget v0, p0, Lcom/peel/widget/a;->b:I

    if-lez v0, :cond_4

    .line 120
    iget-object v0, p0, Lcom/peel/widget/a;->c:Lcom/peel/widget/AdVideoView;

    invoke-static {v0}, Lcom/peel/widget/AdVideoView;->b(Lcom/peel/widget/AdVideoView;)Landroid/widget/TextView;

    move-result-object v0

    iget v1, p0, Lcom/peel/widget/a;->a:I

    invoke-static {v1}, Lcom/peel/util/bx;->e(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 121
    iget-object v0, p0, Lcom/peel/widget/a;->c:Lcom/peel/widget/AdVideoView;

    invoke-static {v0}, Lcom/peel/widget/AdVideoView;->c(Lcom/peel/widget/AdVideoView;)Landroid/widget/TextView;

    move-result-object v0

    iget v1, p0, Lcom/peel/widget/a;->b:I

    invoke-static {v1}, Lcom/peel/util/bx;->e(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 122
    iget v0, p0, Lcom/peel/widget/a;->a:I

    mul-int/lit8 v0, v0, 0x64

    iget v1, p0, Lcom/peel/widget/a;->b:I

    div-int v1, v0, v1

    .line 123
    const/16 v0, 0x19

    if-lt v1, v0, :cond_2

    if-ge v1, v4, :cond_2

    iget-object v0, p0, Lcom/peel/widget/a;->c:Lcom/peel/widget/AdVideoView;

    invoke-static {v0}, Lcom/peel/widget/AdVideoView;->d(Lcom/peel/widget/AdVideoView;)Ljava/util/Map;

    move-result-object v0

    const-string/jumbo v2, "on_video_25"

    invoke-interface {v0, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/peel/widget/a;->c:Lcom/peel/widget/AdVideoView;

    invoke-static {v0}, Lcom/peel/widget/AdVideoView;->e(Lcom/peel/widget/AdVideoView;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 124
    iget-object v0, p0, Lcom/peel/widget/a;->c:Lcom/peel/widget/AdVideoView;

    invoke-static {v0, v3}, Lcom/peel/widget/AdVideoView;->a(Lcom/peel/widget/AdVideoView;Z)Z

    .line 125
    iget-object v2, p0, Lcom/peel/widget/a;->c:Lcom/peel/widget/AdVideoView;

    iget-object v0, p0, Lcom/peel/widget/a;->c:Lcom/peel/widget/AdVideoView;

    invoke-static {v0}, Lcom/peel/widget/AdVideoView;->d(Lcom/peel/widget/AdVideoView;)Ljava/util/Map;

    move-result-object v0

    const-string/jumbo v3, "on_video_25"

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v2, v0}, Lcom/peel/widget/AdVideoView;->a(Lcom/peel/widget/AdVideoView;Ljava/lang/String;)V

    .line 135
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/peel/widget/a;->c:Lcom/peel/widget/AdVideoView;

    invoke-static {v0}, Lcom/peel/widget/AdVideoView;->h(Lcom/peel/widget/AdVideoView;)Landroid/widget/SeekBar;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 136
    iget-object v0, p0, Lcom/peel/widget/a;->c:Lcom/peel/widget/AdVideoView;

    invoke-static {v0}, Lcom/peel/widget/AdVideoView;->j(Lcom/peel/widget/AdVideoView;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/widget/a;->c:Lcom/peel/widget/AdVideoView;

    invoke-static {v1}, Lcom/peel/widget/AdVideoView;->i(Lcom/peel/widget/AdVideoView;)Ljava/lang/Runnable;

    move-result-object v1

    const-wide/16 v2, 0xfa

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 143
    :cond_1
    :goto_1
    return-void

    .line 127
    :cond_2
    if-lt v1, v4, :cond_3

    if-ge v1, v5, :cond_3

    iget-object v0, p0, Lcom/peel/widget/a;->c:Lcom/peel/widget/AdVideoView;

    invoke-static {v0}, Lcom/peel/widget/AdVideoView;->d(Lcom/peel/widget/AdVideoView;)Ljava/util/Map;

    move-result-object v0

    const-string/jumbo v2, "on_video_50"

    invoke-interface {v0, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/peel/widget/a;->c:Lcom/peel/widget/AdVideoView;

    invoke-static {v0}, Lcom/peel/widget/AdVideoView;->f(Lcom/peel/widget/AdVideoView;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 128
    iget-object v0, p0, Lcom/peel/widget/a;->c:Lcom/peel/widget/AdVideoView;

    invoke-static {v0, v3}, Lcom/peel/widget/AdVideoView;->b(Lcom/peel/widget/AdVideoView;Z)Z

    .line 129
    iget-object v2, p0, Lcom/peel/widget/a;->c:Lcom/peel/widget/AdVideoView;

    iget-object v0, p0, Lcom/peel/widget/a;->c:Lcom/peel/widget/AdVideoView;

    invoke-static {v0}, Lcom/peel/widget/AdVideoView;->d(Lcom/peel/widget/AdVideoView;)Ljava/util/Map;

    move-result-object v0

    const-string/jumbo v3, "on_video_50"

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v2, v0}, Lcom/peel/widget/AdVideoView;->a(Lcom/peel/widget/AdVideoView;Ljava/lang/String;)V

    goto :goto_0

    .line 131
    :cond_3
    if-lt v1, v5, :cond_0

    iget-object v0, p0, Lcom/peel/widget/a;->c:Lcom/peel/widget/AdVideoView;

    invoke-static {v0}, Lcom/peel/widget/AdVideoView;->d(Lcom/peel/widget/AdVideoView;)Ljava/util/Map;

    move-result-object v0

    const-string/jumbo v2, "on_video_75"

    invoke-interface {v0, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/peel/widget/a;->c:Lcom/peel/widget/AdVideoView;

    invoke-static {v0}, Lcom/peel/widget/AdVideoView;->g(Lcom/peel/widget/AdVideoView;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 132
    iget-object v0, p0, Lcom/peel/widget/a;->c:Lcom/peel/widget/AdVideoView;

    invoke-static {v0, v3}, Lcom/peel/widget/AdVideoView;->c(Lcom/peel/widget/AdVideoView;Z)Z

    .line 133
    iget-object v2, p0, Lcom/peel/widget/a;->c:Lcom/peel/widget/AdVideoView;

    iget-object v0, p0, Lcom/peel/widget/a;->c:Lcom/peel/widget/AdVideoView;

    invoke-static {v0}, Lcom/peel/widget/AdVideoView;->d(Lcom/peel/widget/AdVideoView;)Ljava/util/Map;

    move-result-object v0

    const-string/jumbo v3, "on_video_75"

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v2, v0}, Lcom/peel/widget/AdVideoView;->a(Lcom/peel/widget/AdVideoView;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 138
    :cond_4
    iget-object v0, p0, Lcom/peel/widget/a;->c:Lcom/peel/widget/AdVideoView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/peel/widget/AdVideoView;->setVisibility(I)V

    .line 139
    iget-object v0, p0, Lcom/peel/widget/a;->c:Lcom/peel/widget/AdVideoView;

    invoke-static {v0, v3}, Lcom/peel/widget/AdVideoView;->d(Lcom/peel/widget/AdVideoView;Z)Z

    .line 140
    iget-object v0, p0, Lcom/peel/widget/a;->c:Lcom/peel/widget/AdVideoView;

    invoke-virtual {v0}, Lcom/peel/widget/AdVideoView;->c()V

    goto :goto_1
.end method
