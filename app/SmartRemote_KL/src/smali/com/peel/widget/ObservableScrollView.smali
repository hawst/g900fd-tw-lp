.class public Lcom/peel/widget/ObservableScrollView;
.super Landroid/widget/ScrollView;


# instance fields
.field private a:Lcom/peel/widget/af;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0, p1, p2}, Landroid/widget/ScrollView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method


# virtual methods
.method public onOverScrolled(IIZZ)V
    .locals 0

    .prologue
    .line 25
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/ScrollView;->onOverScrolled(IIZZ)V

    return-void
.end method

.method protected onScrollChanged(IIII)V
    .locals 6

    .prologue
    .line 17
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/ScrollView;->onScrollChanged(IIII)V

    .line 18
    iget-object v0, p0, Lcom/peel/widget/ObservableScrollView;->a:Lcom/peel/widget/af;

    if-eqz v0, :cond_0

    .line 19
    iget-object v0, p0, Lcom/peel/widget/ObservableScrollView;->a:Lcom/peel/widget/af;

    move-object v1, p0

    move v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    invoke-interface/range {v0 .. v5}, Lcom/peel/widget/af;->a(Lcom/peel/widget/ObservableScrollView;IIII)V

    .line 21
    :cond_0
    return-void
.end method

.method public setOnScrollListener(Lcom/peel/widget/af;)V
    .locals 0

    .prologue
    .line 24
    iput-object p1, p0, Lcom/peel/widget/ObservableScrollView;->a:Lcom/peel/widget/af;

    return-void
.end method
