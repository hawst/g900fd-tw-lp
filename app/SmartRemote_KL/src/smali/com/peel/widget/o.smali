.class Lcom/peel/widget/o;
.super Landroid/webkit/WebViewClient;


# instance fields
.field final synthetic a:Lcom/peel/widget/m;


# direct methods
.method constructor <init>(Lcom/peel/widget/m;)V
    .locals 0

    .prologue
    .line 143
    iput-object p1, p0, Lcom/peel/widget/o;->a:Lcom/peel/widget/m;

    invoke-direct {p0}, Landroid/webkit/WebViewClient;-><init>()V

    return-void
.end method


# virtual methods
.method public onLoadResource(Landroid/webkit/WebView;Ljava/lang/String;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 207
    iget-object v0, p0, Lcom/peel/widget/o;->a:Lcom/peel/widget/m;

    invoke-static {v0}, Lcom/peel/widget/m;->h(Lcom/peel/widget/m;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 208
    iget-object v0, p0, Lcom/peel/widget/o;->a:Lcom/peel/widget/m;

    invoke-static {v0}, Lcom/peel/widget/m;->d(Lcom/peel/widget/m;)Landroid/webkit/WebView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/webkit/WebView;->stopLoading()V

    .line 210
    :cond_0
    invoke-super {p0, p1, p2}, Landroid/webkit/WebViewClient;->onLoadResource(Landroid/webkit/WebView;Ljava/lang/String;)V

    .line 211
    const-string/jumbo v0, "DFPAdView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "webview onLoadResource: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 212
    iget-object v0, p0, Lcom/peel/widget/o;->a:Lcom/peel/widget/m;

    invoke-static {v0, p2}, Lcom/peel/widget/m;->b(Lcom/peel/widget/m;Ljava/lang/String;)V

    .line 213
    iget-object v0, p0, Lcom/peel/widget/o;->a:Lcom/peel/widget/m;

    invoke-static {v0, p2}, Lcom/peel/widget/m;->c(Lcom/peel/widget/m;Ljava/lang/String;)V

    .line 215
    iget-object v0, p0, Lcom/peel/widget/o;->a:Lcom/peel/widget/m;

    invoke-static {v0}, Lcom/peel/widget/m;->j(Lcom/peel/widget/m;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/widget/o;->a:Lcom/peel/widget/m;

    invoke-static {v1}, Lcom/peel/widget/m;->i(Lcom/peel/widget/m;)Ljava/lang/Runnable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 216
    iget-object v0, p0, Lcom/peel/widget/o;->a:Lcom/peel/widget/m;

    invoke-static {v0}, Lcom/peel/widget/m;->j(Lcom/peel/widget/m;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/widget/o;->a:Lcom/peel/widget/m;

    invoke-static {v1}, Lcom/peel/widget/m;->i(Lcom/peel/widget/m;)Ljava/lang/Runnable;

    move-result-object v1

    const-wide/16 v2, 0xa8c

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 218
    iget-object v0, p0, Lcom/peel/widget/o;->a:Lcom/peel/widget/m;

    invoke-static {v0}, Lcom/peel/widget/m;->k(Lcom/peel/widget/m;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 219
    iget-object v0, p0, Lcom/peel/widget/o;->a:Lcom/peel/widget/m;

    invoke-static {v0}, Lcom/peel/widget/m;->a(Lcom/peel/widget/m;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/peel/widget/o;->a:Lcom/peel/widget/m;

    invoke-static {v0}, Lcom/peel/widget/m;->c(Lcom/peel/widget/m;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 220
    const-string/jumbo v0, "DFPAdView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "start Load Resource Intent: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 221
    iget-object v0, p0, Lcom/peel/widget/o;->a:Lcom/peel/widget/m;

    invoke-static {v0, v4}, Lcom/peel/widget/m;->b(Lcom/peel/widget/m;Z)Z

    .line 222
    iget-object v0, p0, Lcom/peel/widget/o;->a:Lcom/peel/widget/m;

    invoke-static {v0, v4}, Lcom/peel/widget/m;->a(Lcom/peel/widget/m;Z)Z

    .line 223
    iget-object v0, p0, Lcom/peel/widget/o;->a:Lcom/peel/widget/m;

    invoke-static {v0, v4}, Lcom/peel/widget/m;->d(Lcom/peel/widget/m;Z)Z

    .line 224
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 225
    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 226
    iget-object v1, p0, Lcom/peel/widget/o;->a:Lcom/peel/widget/m;

    invoke-static {v1}, Lcom/peel/widget/m;->g(Lcom/peel/widget/m;)Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 229
    :cond_1
    return-void
.end method

.method public onPageFinished(Landroid/webkit/WebView;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 194
    const-string/jumbo v0, "DFPAdView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "webview onPageFinished: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 195
    invoke-super {p0, p1, p2}, Landroid/webkit/WebViewClient;->onPageFinished(Landroid/webkit/WebView;Ljava/lang/String;)V

    .line 196
    return-void
.end method

.method public onPageStarted(Landroid/webkit/WebView;Ljava/lang/String;Landroid/graphics/Bitmap;)V
    .locals 3

    .prologue
    .line 200
    const-string/jumbo v0, "DFPAdView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "webview onPageStarted: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 201
    invoke-super {p0, p1, p2, p3}, Landroid/webkit/WebViewClient;->onPageStarted(Landroid/webkit/WebView;Ljava/lang/String;Landroid/graphics/Bitmap;)V

    .line 202
    return-void
.end method

.method public onReceivedError(Landroid/webkit/WebView;ILjava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 239
    invoke-super {p0, p1, p2, p3, p4}, Landroid/webkit/WebViewClient;->onReceivedError(Landroid/webkit/WebView;ILjava/lang/String;Ljava/lang/String;)V

    .line 240
    return-void
.end method

.method public shouldInterceptRequest(Landroid/webkit/WebView;Ljava/lang/String;)Landroid/webkit/WebResourceResponse;
    .locals 1

    .prologue
    .line 234
    invoke-super {p0, p1, p2}, Landroid/webkit/WebViewClient;->shouldInterceptRequest(Landroid/webkit/WebView;Ljava/lang/String;)Landroid/webkit/WebResourceResponse;

    move-result-object v0

    return-object v0
.end method

.method public shouldOverrideKeyEvent(Landroid/webkit/WebView;Landroid/view/KeyEvent;)Z
    .locals 1

    .prologue
    .line 244
    invoke-super {p0, p1, p2}, Landroid/webkit/WebViewClient;->shouldOverrideKeyEvent(Landroid/webkit/WebView;Landroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method public shouldOverrideUrlLoading(Landroid/webkit/WebView;Ljava/lang/String;)Z
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v2, 0x1

    .line 147
    const-string/jumbo v0, "DFPAdView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "webview shouldoverrideurlloading: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 148
    iget-object v0, p0, Lcom/peel/widget/o;->a:Lcom/peel/widget/m;

    invoke-static {v0}, Lcom/peel/widget/m;->a(Lcom/peel/widget/m;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/peel/widget/o;->a:Lcom/peel/widget/m;

    invoke-static {v0, p2}, Lcom/peel/widget/m;->a(Lcom/peel/widget/m;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/peel/widget/o;->a:Lcom/peel/widget/m;

    iget-boolean v0, v0, Lcom/peel/widget/m;->b:Z

    if-eqz v0, :cond_3

    .line 149
    iget-object v0, p0, Lcom/peel/widget/o;->a:Lcom/peel/widget/m;

    invoke-static {v0, v5}, Lcom/peel/widget/m;->b(Lcom/peel/widget/m;Z)Z

    .line 150
    new-instance v3, Landroid/content/Intent;

    const-string/jumbo v0, "android.intent.action.VIEW"

    invoke-direct {v3, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 152
    iget-object v0, p0, Lcom/peel/widget/o;->a:Lcom/peel/widget/m;

    invoke-static {v0}, Lcom/peel/widget/m;->b(Lcom/peel/widget/m;)Lcom/peel/ui/model/DFPAd;

    move-result-object v0

    iget-object v0, v0, Lcom/peel/ui/model/DFPAd;->e:Ljava/util/Map;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/peel/widget/o;->a:Lcom/peel/widget/m;

    invoke-static {v0}, Lcom/peel/widget/m;->b(Lcom/peel/widget/m;)Lcom/peel/ui/model/DFPAd;

    move-result-object v0

    iget-object v0, v0, Lcom/peel/ui/model/DFPAd;->e:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 153
    iget-object v0, p0, Lcom/peel/widget/o;->a:Lcom/peel/widget/m;

    invoke-static {v0}, Lcom/peel/widget/m;->b(Lcom/peel/widget/m;)Lcom/peel/ui/model/DFPAd;

    move-result-object v0

    iget-object v0, v0, Lcom/peel/ui/model/DFPAd;->e:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 154
    iget-object v1, p0, Lcom/peel/widget/o;->a:Lcom/peel/widget/m;

    invoke-static {v1}, Lcom/peel/widget/m;->b(Lcom/peel/widget/m;)Lcom/peel/ui/model/DFPAd;

    move-result-object v1

    iget-object v1, v1, Lcom/peel/ui/model/DFPAd;->e:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-static {p2, v0, v1}, Lcom/peel/util/ef;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    goto :goto_0

    .line 158
    :cond_0
    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v3, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 160
    iget-object v0, p0, Lcom/peel/widget/o;->a:Lcom/peel/widget/m;

    invoke-static {v0}, Lcom/peel/widget/m;->c(Lcom/peel/widget/m;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 161
    iget-object v0, p0, Lcom/peel/widget/o;->a:Lcom/peel/widget/m;

    invoke-static {v0}, Lcom/peel/widget/m;->d(Lcom/peel/widget/m;)Landroid/webkit/WebView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/webkit/WebView;->stopLoading()V

    .line 162
    iget-object v0, p0, Lcom/peel/widget/o;->a:Lcom/peel/widget/m;

    invoke-static {v0, v5}, Lcom/peel/widget/m;->a(Lcom/peel/widget/m;Z)Z

    .line 164
    iget-object v0, p0, Lcom/peel/widget/o;->a:Lcom/peel/widget/m;

    invoke-static {v0}, Lcom/peel/widget/m;->e(Lcom/peel/widget/m;)Ljava/util/ArrayList;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 165
    iget-object v0, p0, Lcom/peel/widget/o;->a:Lcom/peel/widget/m;

    invoke-static {v0}, Lcom/peel/widget/m;->e(Lcom/peel/widget/m;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/widget/r;

    .line 166
    invoke-interface {v0}, Lcom/peel/widget/r;->b()V

    goto :goto_1

    .line 170
    :cond_1
    iget-object v0, p0, Lcom/peel/widget/o;->a:Lcom/peel/widget/m;

    invoke-static {v0, v2}, Lcom/peel/widget/m;->c(Lcom/peel/widget/m;Z)Z

    .line 172
    invoke-virtual {p2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, ".mp4"

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 173
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/peel/widget/o;->a:Lcom/peel/widget/m;

    iget-object v1, v1, Lcom/peel/widget/m;->a:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    const-class v3, Lcom/peel/main/WebViewFragmentHTML5Activity;

    invoke-direct {v0, v1, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 174
    const-string/jumbo v1, "url"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 175
    const-string/jumbo v1, "video"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 176
    const-string/jumbo v1, "context_id"

    iget-object v3, p0, Lcom/peel/widget/o;->a:Lcom/peel/widget/m;

    invoke-static {v3}, Lcom/peel/widget/m;->f(Lcom/peel/widget/m;)I

    move-result v3

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 177
    const-string/jumbo v1, "from"

    const-string/jumbo v3, "Launch webview w/ URI"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 178
    const-string/jumbo v1, "DFPAdView"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "start Intent to mp4: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 179
    iget-object v1, p0, Lcom/peel/widget/o;->a:Lcom/peel/widget/m;

    invoke-static {v1}, Lcom/peel/widget/m;->g(Lcom/peel/widget/m;)Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    move v0, v2

    .line 188
    :goto_2
    return v0

    .line 182
    :cond_2
    const-string/jumbo v0, "DFPAdView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "start Intent: "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 183
    iget-object v0, p0, Lcom/peel/widget/o;->a:Lcom/peel/widget/m;

    invoke-static {v0}, Lcom/peel/widget/m;->g(Lcom/peel/widget/m;)Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    move v0, v2

    .line 184
    goto :goto_2

    .line 188
    :cond_3
    invoke-super {p0, p1, p2}, Landroid/webkit/WebViewClient;->shouldOverrideUrlLoading(Landroid/webkit/WebView;Ljava/lang/String;)Z

    move-result v0

    goto :goto_2
.end method
