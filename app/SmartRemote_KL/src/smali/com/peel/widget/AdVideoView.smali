.class public Lcom/peel/widget/AdVideoView;
.super Landroid/widget/RelativeLayout;

# interfaces
.implements Landroid/media/MediaPlayer$OnCompletionListener;
.implements Landroid/media/MediaPlayer$OnErrorListener;
.implements Landroid/view/SurfaceHolder$Callback;


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private A:Ljava/lang/String;

.field private B:Ljava/lang/String;

.field private C:I

.field private D:I

.field private E:I

.field private F:I

.field private G:I

.field private H:I

.field private I:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private J:Z

.field private K:Z

.field private L:Z

.field private M:Z

.field private N:Landroid/os/Handler;

.field private O:Ljava/lang/Runnable;

.field private b:Landroid/view/View;

.field private c:Landroid/view/View;

.field private d:Landroid/view/SurfaceView;

.field private e:Landroid/view/SurfaceHolder;

.field private f:Landroid/widget/TextView;

.field private g:Landroid/widget/ImageView;

.field private h:Landroid/view/View;

.field private i:Landroid/widget/SeekBar;

.field private j:Landroid/widget/ImageButton;

.field private k:Landroid/widget/ImageButton;

.field private l:Landroid/widget/ImageButton;

.field private m:Landroid/widget/ImageButton;

.field private n:Landroid/widget/ImageView;

.field private o:Landroid/widget/TextView;

.field private p:Landroid/widget/TextView;

.field private q:Landroid/media/MediaPlayer;

.field private r:I

.field private s:Z

.field private t:Z

.field private u:Z

.field private v:Z

.field private w:Ljava/lang/String;

.field private x:Z

.field private y:Z

.field private z:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 40
    const-class v0, Lcom/peel/widget/AdVideoView;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/peel/widget/AdVideoView;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 149
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 83
    iput v2, p0, Lcom/peel/widget/AdVideoView;->C:I

    .line 84
    const/16 v0, 0xc8

    iput v0, p0, Lcom/peel/widget/AdVideoView;->D:I

    .line 89
    iput v2, p0, Lcom/peel/widget/AdVideoView;->G:I

    .line 90
    iput v2, p0, Lcom/peel/widget/AdVideoView;->H:I

    .line 91
    new-instance v0, Ljava/util/HashMap;

    const/16 v1, 0x9

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    iput-object v0, p0, Lcom/peel/widget/AdVideoView;->I:Ljava/util/Map;

    .line 96
    iput-boolean v2, p0, Lcom/peel/widget/AdVideoView;->M:Z

    .line 111
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/peel/widget/AdVideoView;->N:Landroid/os/Handler;

    .line 112
    new-instance v0, Lcom/peel/widget/a;

    invoke-direct {v0, p0}, Lcom/peel/widget/a;-><init>(Lcom/peel/widget/AdVideoView;)V

    iput-object v0, p0, Lcom/peel/widget/AdVideoView;->O:Ljava/lang/Runnable;

    .line 150
    invoke-direct {p0}, Lcom/peel/widget/AdVideoView;->i()V

    .line 151
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 154
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 83
    iput v2, p0, Lcom/peel/widget/AdVideoView;->C:I

    .line 84
    const/16 v0, 0xc8

    iput v0, p0, Lcom/peel/widget/AdVideoView;->D:I

    .line 89
    iput v2, p0, Lcom/peel/widget/AdVideoView;->G:I

    .line 90
    iput v2, p0, Lcom/peel/widget/AdVideoView;->H:I

    .line 91
    new-instance v0, Ljava/util/HashMap;

    const/16 v1, 0x9

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    iput-object v0, p0, Lcom/peel/widget/AdVideoView;->I:Ljava/util/Map;

    .line 96
    iput-boolean v2, p0, Lcom/peel/widget/AdVideoView;->M:Z

    .line 111
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/peel/widget/AdVideoView;->N:Landroid/os/Handler;

    .line 112
    new-instance v0, Lcom/peel/widget/a;

    invoke-direct {v0, p0}, Lcom/peel/widget/a;-><init>(Lcom/peel/widget/AdVideoView;)V

    iput-object v0, p0, Lcom/peel/widget/AdVideoView;->O:Ljava/lang/Runnable;

    .line 155
    invoke-direct {p0}, Lcom/peel/widget/AdVideoView;->i()V

    .line 156
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 159
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 83
    iput v2, p0, Lcom/peel/widget/AdVideoView;->C:I

    .line 84
    const/16 v0, 0xc8

    iput v0, p0, Lcom/peel/widget/AdVideoView;->D:I

    .line 89
    iput v2, p0, Lcom/peel/widget/AdVideoView;->G:I

    .line 90
    iput v2, p0, Lcom/peel/widget/AdVideoView;->H:I

    .line 91
    new-instance v0, Ljava/util/HashMap;

    const/16 v1, 0x9

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    iput-object v0, p0, Lcom/peel/widget/AdVideoView;->I:Ljava/util/Map;

    .line 96
    iput-boolean v2, p0, Lcom/peel/widget/AdVideoView;->M:Z

    .line 111
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/peel/widget/AdVideoView;->N:Landroid/os/Handler;

    .line 112
    new-instance v0, Lcom/peel/widget/a;

    invoke-direct {v0, p0}, Lcom/peel/widget/a;-><init>(Lcom/peel/widget/AdVideoView;)V

    iput-object v0, p0, Lcom/peel/widget/AdVideoView;->O:Ljava/lang/Runnable;

    .line 160
    invoke-direct {p0}, Lcom/peel/widget/AdVideoView;->i()V

    .line 161
    return-void
.end method

.method static synthetic a(Lcom/peel/widget/AdVideoView;)Landroid/media/MediaPlayer;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/peel/widget/AdVideoView;->q:Landroid/media/MediaPlayer;

    return-object v0
.end method

.method static synthetic a(Lcom/peel/widget/AdVideoView;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0, p1}, Lcom/peel/widget/AdVideoView;->a(Ljava/lang/String;)V

    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 465
    invoke-static {p1}, Lcom/peel/util/bq;->a(Ljava/lang/String;)V

    .line 466
    return-void
.end method

.method static synthetic a(Lcom/peel/widget/AdVideoView;Z)Z
    .locals 0

    .prologue
    .line 38
    iput-boolean p1, p0, Lcom/peel/widget/AdVideoView;->J:Z

    return p1
.end method

.method static synthetic b(Lcom/peel/widget/AdVideoView;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/peel/widget/AdVideoView;->o:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic b(Lcom/peel/widget/AdVideoView;Z)Z
    .locals 0

    .prologue
    .line 38
    iput-boolean p1, p0, Lcom/peel/widget/AdVideoView;->K:Z

    return p1
.end method

.method static synthetic c(Lcom/peel/widget/AdVideoView;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/peel/widget/AdVideoView;->p:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic c(Lcom/peel/widget/AdVideoView;Z)Z
    .locals 0

    .prologue
    .line 38
    iput-boolean p1, p0, Lcom/peel/widget/AdVideoView;->L:Z

    return p1
.end method

.method static synthetic d(Lcom/peel/widget/AdVideoView;)Ljava/util/Map;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/peel/widget/AdVideoView;->I:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic d(Lcom/peel/widget/AdVideoView;Z)Z
    .locals 0

    .prologue
    .line 38
    iput-boolean p1, p0, Lcom/peel/widget/AdVideoView;->t:Z

    return p1
.end method

.method static synthetic e(Lcom/peel/widget/AdVideoView;)Z
    .locals 1

    .prologue
    .line 38
    iget-boolean v0, p0, Lcom/peel/widget/AdVideoView;->J:Z

    return v0
.end method

.method static synthetic f(Lcom/peel/widget/AdVideoView;)Z
    .locals 1

    .prologue
    .line 38
    iget-boolean v0, p0, Lcom/peel/widget/AdVideoView;->K:Z

    return v0
.end method

.method static synthetic g(Lcom/peel/widget/AdVideoView;)Z
    .locals 1

    .prologue
    .line 38
    iget-boolean v0, p0, Lcom/peel/widget/AdVideoView;->L:Z

    return v0
.end method

.method static synthetic h(Lcom/peel/widget/AdVideoView;)Landroid/widget/SeekBar;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/peel/widget/AdVideoView;->i:Landroid/widget/SeekBar;

    return-object v0
.end method

.method static synthetic h()Ljava/lang/String;
    .locals 1

    .prologue
    .line 38
    sget-object v0, Lcom/peel/widget/AdVideoView;->a:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic i(Lcom/peel/widget/AdVideoView;)Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/peel/widget/AdVideoView;->O:Ljava/lang/Runnable;

    return-object v0
.end method

.method private i()V
    .locals 2

    .prologue
    .line 164
    invoke-virtual {p0}, Lcom/peel/widget/AdVideoView;->getContext()Landroid/content/Context;

    move-result-object v0

    sget v1, Lcom/peel/ui/fq;->advideoview:I

    invoke-static {v0, v1, p0}, Lcom/peel/widget/AdVideoView;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 165
    sget v0, Lcom/peel/ui/fp;->adview_container:I

    invoke-virtual {p0, v0}, Lcom/peel/widget/AdVideoView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/widget/AdVideoView;->b:Landroid/view/View;

    .line 166
    sget v0, Lcom/peel/ui/fp;->video_container_view:I

    invoke-virtual {p0, v0}, Lcom/peel/widget/AdVideoView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/widget/AdVideoView;->c:Landroid/view/View;

    .line 167
    sget v0, Lcom/peel/ui/fp;->video_view:I

    invoke-virtual {p0, v0}, Lcom/peel/widget/AdVideoView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/SurfaceView;

    iput-object v0, p0, Lcom/peel/widget/AdVideoView;->d:Landroid/view/SurfaceView;

    .line 168
    sget v0, Lcom/peel/ui/fp;->campaign_message_text:I

    invoke-virtual {p0, v0}, Lcom/peel/widget/AdVideoView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/peel/widget/AdVideoView;->f:Landroid/widget/TextView;

    .line 169
    sget v0, Lcom/peel/ui/fp;->ad_banner_view:I

    invoke-virtual {p0, v0}, Lcom/peel/widget/AdVideoView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/peel/widget/AdVideoView;->g:Landroid/widget/ImageView;

    .line 170
    sget v0, Lcom/peel/ui/fp;->playback_ctrl_view:I

    invoke-virtual {p0, v0}, Lcom/peel/widget/AdVideoView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/widget/AdVideoView;->h:Landroid/view/View;

    .line 171
    sget v0, Lcom/peel/ui/fp;->video_progressbar:I

    invoke-virtual {p0, v0}, Lcom/peel/widget/AdVideoView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/SeekBar;

    iput-object v0, p0, Lcom/peel/widget/AdVideoView;->i:Landroid/widget/SeekBar;

    .line 172
    sget v0, Lcom/peel/ui/fp;->video_btn_mute:I

    invoke-virtual {p0, v0}, Lcom/peel/widget/AdVideoView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/peel/widget/AdVideoView;->j:Landroid/widget/ImageButton;

    .line 173
    sget v0, Lcom/peel/ui/fp;->video_btn_min_scr:I

    invoke-virtual {p0, v0}, Lcom/peel/widget/AdVideoView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/peel/widget/AdVideoView;->k:Landroid/widget/ImageButton;

    .line 174
    sget v0, Lcom/peel/ui/fp;->video_btn_playpause:I

    invoke-virtual {p0, v0}, Lcom/peel/widget/AdVideoView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/peel/widget/AdVideoView;->n:Landroid/widget/ImageView;

    .line 175
    sget v0, Lcom/peel/ui/fp;->video_original_btn_close:I

    invoke-virtual {p0, v0}, Lcom/peel/widget/AdVideoView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/peel/widget/AdVideoView;->m:Landroid/widget/ImageButton;

    .line 176
    sget v0, Lcom/peel/ui/fp;->video_original_btn_mute:I

    invoke-virtual {p0, v0}, Lcom/peel/widget/AdVideoView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/peel/widget/AdVideoView;->l:Landroid/widget/ImageButton;

    .line 177
    sget v0, Lcom/peel/ui/fp;->time_elapse_text:I

    invoke-virtual {p0, v0}, Lcom/peel/widget/AdVideoView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/peel/widget/AdVideoView;->o:Landroid/widget/TextView;

    .line 178
    sget v0, Lcom/peel/ui/fp;->time_duration_text:I

    invoke-virtual {p0, v0}, Lcom/peel/widget/AdVideoView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/peel/widget/AdVideoView;->p:Landroid/widget/TextView;

    .line 179
    new-instance v0, Landroid/media/MediaPlayer;

    invoke-direct {v0}, Landroid/media/MediaPlayer;-><init>()V

    iput-object v0, p0, Lcom/peel/widget/AdVideoView;->q:Landroid/media/MediaPlayer;

    .line 180
    iget-object v0, p0, Lcom/peel/widget/AdVideoView;->q:Landroid/media/MediaPlayer;

    iget-object v1, p0, Lcom/peel/widget/AdVideoView;->e:Landroid/view/SurfaceHolder;

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setDisplay(Landroid/view/SurfaceHolder;)V

    .line 181
    iget-object v0, p0, Lcom/peel/widget/AdVideoView;->q:Landroid/media/MediaPlayer;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setScreenOnWhilePlaying(Z)V

    .line 182
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/peel/widget/AdVideoView;->setViewEnabled(Z)V

    .line 184
    return-void
.end method

.method static synthetic j(Lcom/peel/widget/AdVideoView;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/peel/widget/AdVideoView;->N:Landroid/os/Handler;

    return-object v0
.end method

.method private j()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 188
    iput v2, p0, Lcom/peel/widget/AdVideoView;->r:I

    .line 189
    iget-object v0, p0, Lcom/peel/widget/AdVideoView;->I:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 190
    iput-boolean v2, p0, Lcom/peel/widget/AdVideoView;->J:Z

    .line 191
    iput-boolean v2, p0, Lcom/peel/widget/AdVideoView;->K:Z

    .line 192
    iput-boolean v2, p0, Lcom/peel/widget/AdVideoView;->L:Z

    .line 193
    invoke-virtual {p0, v2}, Lcom/peel/widget/AdVideoView;->setFullscreen(Z)V

    .line 194
    iget-object v0, p0, Lcom/peel/widget/AdVideoView;->f:Landroid/widget/TextView;

    const-string/jumbo v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 195
    iget-object v0, p0, Lcom/peel/widget/AdVideoView;->g:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 196
    iget-object v0, p0, Lcom/peel/widget/AdVideoView;->o:Landroid/widget/TextView;

    invoke-static {v2}, Lcom/peel/util/bx;->e(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 197
    iget-object v0, p0, Lcom/peel/widget/AdVideoView;->p:Landroid/widget/TextView;

    invoke-static {v2}, Lcom/peel/util/bx;->e(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 198
    iget-object v0, p0, Lcom/peel/widget/AdVideoView;->i:Landroid/widget/SeekBar;

    invoke-virtual {v0, v2}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 199
    iget-object v0, p0, Lcom/peel/widget/AdVideoView;->d:Landroid/view/SurfaceView;

    invoke-virtual {v0, v2}, Landroid/view/SurfaceView;->setBackgroundResource(I)V

    .line 200
    iget-object v0, p0, Lcom/peel/widget/AdVideoView;->d:Landroid/view/SurfaceView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/SurfaceView;->setZOrderMediaOverlay(Z)V

    .line 201
    iget-object v0, p0, Lcom/peel/widget/AdVideoView;->d:Landroid/view/SurfaceView;

    invoke-virtual {v0}, Landroid/view/SurfaceView;->requestFocus()Z

    .line 202
    iget-object v0, p0, Lcom/peel/widget/AdVideoView;->d:Landroid/view/SurfaceView;

    invoke-virtual {v0, v2}, Landroid/view/SurfaceView;->setClickable(Z)V

    .line 203
    iget-object v0, p0, Lcom/peel/widget/AdVideoView;->j:Landroid/widget/ImageButton;

    sget v1, Lcom/peel/ui/fo;->video_vol:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 204
    iget-object v0, p0, Lcom/peel/widget/AdVideoView;->l:Landroid/widget/ImageButton;

    sget v1, Lcom/peel/ui/fo;->btn_ad_video_vol_stateful:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 205
    iget-object v0, p0, Lcom/peel/widget/AdVideoView;->n:Landroid/widget/ImageView;

    sget v1, Lcom/peel/ui/fo;->video_pause_btn:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 206
    invoke-virtual {p0, v2}, Lcom/peel/widget/AdVideoView;->setFullscreen(Z)V

    .line 207
    return-void
.end method

.method static synthetic k(Lcom/peel/widget/AdVideoView;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/peel/widget/AdVideoView;->B:Ljava/lang/String;

    return-object v0
.end method

.method private k()V
    .locals 1

    .prologue
    .line 392
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/peel/widget/AdVideoView;->t:Z

    .line 393
    iget-object v0, p0, Lcom/peel/widget/AdVideoView;->d:Landroid/view/SurfaceView;

    invoke-virtual {v0}, Landroid/view/SurfaceView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/widget/AdVideoView;->e:Landroid/view/SurfaceHolder;

    .line 394
    iget-object v0, p0, Lcom/peel/widget/AdVideoView;->e:Landroid/view/SurfaceHolder;

    invoke-interface {v0, p0}, Landroid/view/SurfaceHolder;->addCallback(Landroid/view/SurfaceHolder$Callback;)V

    .line 395
    iget-object v0, p0, Lcom/peel/widget/AdVideoView;->q:Landroid/media/MediaPlayer;

    invoke-virtual {v0, p0}, Landroid/media/MediaPlayer;->setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V

    .line 396
    return-void
.end method

.method static synthetic l(Lcom/peel/widget/AdVideoView;)Z
    .locals 1

    .prologue
    .line 38
    iget-boolean v0, p0, Lcom/peel/widget/AdVideoView;->M:Z

    return v0
.end method

.method static synthetic m(Lcom/peel/widget/AdVideoView;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/peel/widget/AdVideoView;->z:Ljava/lang/String;

    return-object v0
.end method

.method private setPlaybackCtrlVisible(Z)V
    .locals 2

    .prologue
    .line 461
    iget-object v1, p0, Lcom/peel/widget/AdVideoView;->h:Landroid/view/View;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 462
    return-void

    .line 461
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method


# virtual methods
.method public a()V
    .locals 3

    .prologue
    .line 471
    :try_start_0
    iget-object v0, p0, Lcom/peel/widget/AdVideoView;->q:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/peel/widget/AdVideoView;->t:Z

    if-nez v0, :cond_0

    .line 472
    iget-object v0, p0, Lcom/peel/widget/AdVideoView;->q:Landroid/media/MediaPlayer;

    invoke-virtual {p0}, Lcom/peel/widget/AdVideoView;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/peel/widget/AdVideoView;->w:Ljava/lang/String;

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/media/MediaPlayer;->setDataSource(Landroid/content/Context;Landroid/net/Uri;)V

    .line 473
    iget-object v0, p0, Lcom/peel/widget/AdVideoView;->q:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->prepare()V

    .line 474
    iget-object v0, p0, Lcom/peel/widget/AdVideoView;->q:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->start()V

    .line 475
    iget-object v0, p0, Lcom/peel/widget/AdVideoView;->I:Ljava/util/Map;

    const-string/jumbo v1, "on_video_start"

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 476
    iget-object v0, p0, Lcom/peel/widget/AdVideoView;->I:Ljava/util/Map;

    const-string/jumbo v1, "on_video_start"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/peel/widget/AdVideoView;->a(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 482
    :cond_0
    :goto_0
    return-void

    .line 479
    :catch_0
    move-exception v0

    .line 480
    sget-object v0, Lcom/peel/widget/AdVideoView;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Cannot play video from:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/peel/widget/AdVideoView;->w:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public a(Landroid/os/Bundle;Landroid/os/Bundle;Landroid/os/Bundle;Ljava/lang/String;)V
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 212
    if-eqz p1, :cond_0

    const-string/jumbo v0, "url"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "url"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 213
    :cond_0
    invoke-virtual {p0, v1}, Lcom/peel/widget/AdVideoView;->setViewEnabled(Z)V

    .line 389
    :cond_1
    :goto_0
    return-void

    .line 219
    :cond_2
    const-string/jumbo v0, "url"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/widget/AdVideoView;->w:Ljava/lang/String;

    .line 220
    const-string/jumbo v0, "video_click_url"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/widget/AdVideoView;->z:Ljava/lang/String;

    .line 222
    const-string/jumbo v0, "sound"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_13

    const-string/jumbo v0, "sound"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v3, "on"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_13

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/peel/widget/AdVideoView;->y:Z

    .line 223
    iget-object v3, p0, Lcom/peel/widget/AdVideoView;->j:Landroid/widget/ImageButton;

    iget-boolean v0, p0, Lcom/peel/widget/AdVideoView;->y:Z

    if-eqz v0, :cond_14

    sget v0, Lcom/peel/ui/fo;->video_vol:I

    :goto_2
    invoke-virtual {v3, v0}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 224
    iget-object v3, p0, Lcom/peel/widget/AdVideoView;->l:Landroid/widget/ImageButton;

    iget-boolean v0, p0, Lcom/peel/widget/AdVideoView;->y:Z

    if-eqz v0, :cond_15

    sget v0, Lcom/peel/ui/fo;->btn_ad_video_vol_stateful:I

    :goto_3
    invoke-virtual {v3, v0}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 226
    iput-boolean v2, p0, Lcom/peel/widget/AdVideoView;->x:Z

    .line 227
    const-string/jumbo v0, "autoplay"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    const-string/jumbo v0, "autoplay"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v3, "off"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 228
    iput-boolean v1, p0, Lcom/peel/widget/AdVideoView;->x:Z

    .line 231
    :cond_3
    invoke-virtual {p0, v2}, Lcom/peel/widget/AdVideoView;->setViewEnabled(Z)V

    .line 234
    if-eqz p3, :cond_e

    .line 235
    const-string/jumbo v0, "on_video_start"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    const-string/jumbo v0, "on_video_start"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    .line 236
    iget-object v0, p0, Lcom/peel/widget/AdVideoView;->I:Ljava/util/Map;

    const-string/jumbo v3, "on_video_start"

    const-string/jumbo v4, "on_video_start"

    invoke-virtual {p3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v0, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 239
    :cond_4
    const-string/jumbo v0, "on_video_stop"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    const-string/jumbo v0, "on_video_stop"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_5

    .line 240
    iget-object v0, p0, Lcom/peel/widget/AdVideoView;->I:Ljava/util/Map;

    const-string/jumbo v3, "on_video_stop"

    const-string/jumbo v4, "on_video_stop"

    invoke-virtual {p3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v0, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 243
    :cond_5
    const-string/jumbo v0, "on_video_end"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    const-string/jumbo v0, "on_video_end"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_6

    .line 244
    iget-object v0, p0, Lcom/peel/widget/AdVideoView;->I:Ljava/util/Map;

    const-string/jumbo v3, "on_video_end"

    const-string/jumbo v4, "on_video_end"

    invoke-virtual {p3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v0, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 247
    :cond_6
    const-string/jumbo v0, "on_video_resume"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    const-string/jumbo v0, "on_video_resume"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_7

    .line 248
    iget-object v0, p0, Lcom/peel/widget/AdVideoView;->I:Ljava/util/Map;

    const-string/jumbo v3, "on_video_resume"

    const-string/jumbo v4, "on_video_resume"

    invoke-virtual {p3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v0, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 251
    :cond_7
    const-string/jumbo v0, "on_video_25"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    const-string/jumbo v0, "on_video_25"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_8

    .line 252
    iget-object v0, p0, Lcom/peel/widget/AdVideoView;->I:Ljava/util/Map;

    const-string/jumbo v3, "on_video_25"

    const-string/jumbo v4, "on_video_25"

    invoke-virtual {p3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v0, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 255
    :cond_8
    const-string/jumbo v0, "on_video_50"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    const-string/jumbo v0, "on_video_50"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_9

    .line 256
    iget-object v0, p0, Lcom/peel/widget/AdVideoView;->I:Ljava/util/Map;

    const-string/jumbo v3, "on_video_50"

    const-string/jumbo v4, "on_video_50"

    invoke-virtual {p3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v0, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 259
    :cond_9
    const-string/jumbo v0, "on_video_75"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a

    const-string/jumbo v0, "on_video_75"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_a

    .line 260
    iget-object v0, p0, Lcom/peel/widget/AdVideoView;->I:Ljava/util/Map;

    const-string/jumbo v3, "on_video_75"

    const-string/jumbo v4, "on_video_75"

    invoke-virtual {p3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v0, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 263
    :cond_a
    const-string/jumbo v0, "on_video_mute"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_b

    const-string/jumbo v0, "on_video_mute"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_b

    .line 264
    iget-object v0, p0, Lcom/peel/widget/AdVideoView;->I:Ljava/util/Map;

    const-string/jumbo v3, "on_video_mute"

    const-string/jumbo v4, "on_video_mute"

    invoke-virtual {p3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v0, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 267
    :cond_b
    const-string/jumbo v0, "on_video_unmute"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_c

    const-string/jumbo v0, "on_video_unmute"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_c

    .line 268
    iget-object v0, p0, Lcom/peel/widget/AdVideoView;->I:Ljava/util/Map;

    const-string/jumbo v3, "on_video_unmute"

    const-string/jumbo v4, "on_video_unmute"

    invoke-virtual {p3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v0, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 271
    :cond_c
    const-string/jumbo v0, "on_banner_click"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_d

    const-string/jumbo v0, "on_banner_click"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_d

    .line 272
    iget-object v0, p0, Lcom/peel/widget/AdVideoView;->I:Ljava/util/Map;

    const-string/jumbo v3, "on_banner_click"

    const-string/jumbo v4, "on_banner_click"

    invoke-virtual {p3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v0, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 275
    :cond_d
    const-string/jumbo v0, "on_video_click"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_e

    const-string/jumbo v0, "on_video_click"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_e

    .line 276
    iget-object v0, p0, Lcom/peel/widget/AdVideoView;->I:Ljava/util/Map;

    const-string/jumbo v3, "on_video_click"

    const-string/jumbo v4, "on_video_click"

    invoke-virtual {p3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v0, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 280
    :cond_e
    invoke-static {p4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_f

    .line 281
    iget-object v0, p0, Lcom/peel/widget/AdVideoView;->f:Landroid/widget/TextView;

    invoke-virtual {v0, p4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 285
    :cond_f
    if-eqz p2, :cond_12

    const-string/jumbo v0, "image"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_12

    const-string/jumbo v0, "image"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_12

    .line 286
    const-string/jumbo v0, "image"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/widget/AdVideoView;->A:Ljava/lang/String;

    .line 287
    const-string/jumbo v0, "click_url"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_10

    const-string/jumbo v0, "click_url"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_10

    .line 288
    const-string/jumbo v0, "click_url"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/widget/AdVideoView;->B:Ljava/lang/String;

    .line 289
    iget-object v0, p0, Lcom/peel/widget/AdVideoView;->g:Landroid/widget/ImageView;

    new-instance v3, Lcom/peel/widget/b;

    invoke-direct {v3, p0}, Lcom/peel/widget/b;-><init>(Lcom/peel/widget/AdVideoView;)V

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 305
    :cond_10
    const-string/jumbo v0, "size"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_11

    const-string/jumbo v0, "size"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v3, "\\d,\\d"

    invoke-virtual {v0, v3}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_11

    .line 306
    const-string/jumbo v0, "size"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v3, ","

    invoke-virtual {v0, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 307
    aget-object v3, v0, v1

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    iput v3, p0, Lcom/peel/widget/AdVideoView;->C:I

    .line 308
    aget-object v0, v0, v2

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/peel/widget/AdVideoView;->D:I

    .line 310
    :cond_11
    iget-object v0, p0, Lcom/peel/widget/AdVideoView;->g:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 311
    iget-object v0, p0, Lcom/peel/widget/AdVideoView;->f:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 312
    invoke-virtual {p0}, Lcom/peel/widget/AdVideoView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/peel/util/b/e;->a(Landroid/content/Context;)Lcom/squareup/picasso/Picasso;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/widget/AdVideoView;->A:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/picasso/Picasso;->load(Ljava/lang/String;)Lcom/squareup/picasso/RequestCreator;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/picasso/RequestCreator;->fit()Lcom/squareup/picasso/RequestCreator;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/picasso/RequestCreator;->centerCrop()Lcom/squareup/picasso/RequestCreator;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/widget/AdVideoView;->g:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Lcom/squareup/picasso/RequestCreator;->into(Landroid/widget/ImageView;)V

    .line 316
    :cond_12
    iget-object v0, p0, Lcom/peel/widget/AdVideoView;->i:Landroid/widget/SeekBar;

    new-instance v1, Lcom/peel/widget/c;

    invoke-direct {v1, p0}, Lcom/peel/widget/c;-><init>(Lcom/peel/widget/AdVideoView;)V

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 323
    iget-object v0, p0, Lcom/peel/widget/AdVideoView;->j:Landroid/widget/ImageButton;

    new-instance v1, Lcom/peel/widget/d;

    invoke-direct {v1, p0}, Lcom/peel/widget/d;-><init>(Lcom/peel/widget/AdVideoView;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 330
    iget-object v0, p0, Lcom/peel/widget/AdVideoView;->l:Landroid/widget/ImageButton;

    new-instance v1, Lcom/peel/widget/e;

    invoke-direct {v1, p0}, Lcom/peel/widget/e;-><init>(Lcom/peel/widget/AdVideoView;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 337
    iget-object v0, p0, Lcom/peel/widget/AdVideoView;->m:Landroid/widget/ImageButton;

    new-instance v1, Lcom/peel/widget/f;

    invoke-direct {v1, p0}, Lcom/peel/widget/f;-><init>(Lcom/peel/widget/AdVideoView;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 347
    iget-object v0, p0, Lcom/peel/widget/AdVideoView;->k:Landroid/widget/ImageButton;

    new-instance v1, Lcom/peel/widget/g;

    invoke-direct {v1, p0}, Lcom/peel/widget/g;-><init>(Lcom/peel/widget/AdVideoView;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 354
    iget-object v0, p0, Lcom/peel/widget/AdVideoView;->n:Landroid/widget/ImageView;

    new-instance v1, Lcom/peel/widget/h;

    invoke-direct {v1, p0}, Lcom/peel/widget/h;-><init>(Lcom/peel/widget/AdVideoView;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 367
    iget-object v0, p0, Lcom/peel/widget/AdVideoView;->d:Landroid/view/SurfaceView;

    new-instance v1, Lcom/peel/widget/i;

    invoke-direct {v1, p0}, Lcom/peel/widget/i;-><init>(Lcom/peel/widget/AdVideoView;)V

    invoke-virtual {v0, v1}, Landroid/view/SurfaceView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 387
    invoke-direct {p0}, Lcom/peel/widget/AdVideoView;->k()V

    .line 388
    iget-boolean v0, p0, Lcom/peel/widget/AdVideoView;->x:Z

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/peel/widget/AdVideoView;->a()V

    goto/16 :goto_0

    :cond_13
    move v0, v2

    .line 222
    goto/16 :goto_1

    .line 223
    :cond_14
    sget v0, Lcom/peel/ui/fo;->video_vol_mute:I

    goto/16 :goto_2

    .line 224
    :cond_15
    sget v0, Lcom/peel/ui/fo;->btn_ad_video_mute_stateful:I

    goto/16 :goto_3
.end method

.method public b()V
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 485
    iget-boolean v0, p0, Lcom/peel/widget/AdVideoView;->t:Z

    if-eqz v0, :cond_1

    .line 497
    :cond_0
    :goto_0
    return-void

    .line 486
    :cond_1
    iget-object v0, p0, Lcom/peel/widget/AdVideoView;->q:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/peel/widget/AdVideoView;->q:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 487
    iget-object v0, p0, Lcom/peel/widget/AdVideoView;->q:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->pause()V

    .line 488
    iput-boolean v1, p0, Lcom/peel/widget/AdVideoView;->v:Z

    .line 489
    iget-object v0, p0, Lcom/peel/widget/AdVideoView;->q:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->getCurrentPosition()I

    move-result v0

    iput v0, p0, Lcom/peel/widget/AdVideoView;->r:I

    .line 490
    iget-object v0, p0, Lcom/peel/widget/AdVideoView;->n:Landroid/widget/ImageView;

    sget v2, Lcom/peel/ui/fo;->video_play_btn:I

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 491
    iget-object v0, p0, Lcom/peel/widget/AdVideoView;->I:Ljava/util/Map;

    const-string/jumbo v2, "on_video_stop"

    invoke-interface {v0, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 492
    iget-object v0, p0, Lcom/peel/widget/AdVideoView;->I:Ljava/util/Map;

    const-string/jumbo v2, "on_video_stop"

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/peel/widget/AdVideoView;->a(Ljava/lang/String;)V

    .line 494
    :cond_2
    invoke-static {}, Lcom/peel/util/a/f;->a()Lcom/peel/util/a/f;

    move-result-object v2

    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v0}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v0

    if-nez v0, :cond_3

    move v0, v1

    :goto_1
    const/16 v1, 0x852

    const/16 v3, 0x1770

    invoke-virtual {v2, v0, v1, v3}, Lcom/peel/util/a/f;->a(III)V

    goto :goto_0

    :cond_3
    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v0}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/data/at;->f()I

    move-result v0

    goto :goto_1
.end method

.method public c()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 500
    iget-object v0, p0, Lcom/peel/widget/AdVideoView;->N:Landroid/os/Handler;

    iget-object v1, p0, Lcom/peel/widget/AdVideoView;->O:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 501
    iget-object v0, p0, Lcom/peel/widget/AdVideoView;->q:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/peel/widget/AdVideoView;->t:Z

    if-nez v0, :cond_0

    .line 502
    invoke-virtual {p0, v2}, Lcom/peel/widget/AdVideoView;->setViewEnabled(Z)V

    .line 503
    iget-object v0, p0, Lcom/peel/widget/AdVideoView;->q:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->stop()V

    .line 504
    iget-object v0, p0, Lcom/peel/widget/AdVideoView;->q:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->release()V

    .line 505
    iput-object v3, p0, Lcom/peel/widget/AdVideoView;->q:Landroid/media/MediaPlayer;

    .line 506
    iput-object v3, p0, Lcom/peel/widget/AdVideoView;->e:Landroid/view/SurfaceHolder;

    .line 507
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/peel/widget/AdVideoView;->t:Z

    .line 508
    iput v2, p0, Lcom/peel/widget/AdVideoView;->r:I

    .line 509
    iget-object v0, p0, Lcom/peel/widget/AdVideoView;->I:Ljava/util/Map;

    const-string/jumbo v1, "on_video_end"

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 510
    iget-object v0, p0, Lcom/peel/widget/AdVideoView;->I:Ljava/util/Map;

    const-string/jumbo v1, "on_video_end"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/peel/widget/AdVideoView;->a(Ljava/lang/String;)V

    .line 513
    :cond_0
    return-void
.end method

.method public d()Z
    .locals 1

    .prologue
    .line 516
    iget-boolean v0, p0, Lcom/peel/widget/AdVideoView;->s:Z

    return v0
.end method

.method public e()Z
    .locals 1

    .prologue
    .line 528
    iget-boolean v0, p0, Lcom/peel/widget/AdVideoView;->t:Z

    return v0
.end method

.method public f()Z
    .locals 1

    .prologue
    .line 532
    iget-boolean v0, p0, Lcom/peel/widget/AdVideoView;->v:Z

    return v0
.end method

.method public g()V
    .locals 4

    .prologue
    .line 537
    iget-object v0, p0, Lcom/peel/widget/AdVideoView;->q:Landroid/media/MediaPlayer;

    iget v1, p0, Lcom/peel/widget/AdVideoView;->r:I

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->seekTo(I)V

    .line 538
    iget-object v0, p0, Lcom/peel/widget/AdVideoView;->q:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->start()V

    .line 539
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/peel/widget/AdVideoView;->v:Z

    .line 540
    iget-object v0, p0, Lcom/peel/widget/AdVideoView;->n:Landroid/widget/ImageView;

    sget v1, Lcom/peel/ui/fo;->video_pause_btn:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 542
    iget-object v0, p0, Lcom/peel/widget/AdVideoView;->I:Ljava/util/Map;

    const-string/jumbo v1, "on_video_resume"

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 543
    iget-object v0, p0, Lcom/peel/widget/AdVideoView;->I:Ljava/util/Map;

    const-string/jumbo v1, "on_video_resume"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/peel/widget/AdVideoView;->a(Ljava/lang/String;)V

    .line 545
    :cond_0
    invoke-static {}, Lcom/peel/util/a/f;->a()Lcom/peel/util/a/f;

    move-result-object v1

    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v0}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    const/16 v2, 0x853

    const/16 v3, 0x1770

    invoke-virtual {v1, v0, v2, v3}, Lcom/peel/util/a/f;->a(III)V

    .line 547
    return-void

    .line 545
    :cond_1
    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v0}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/data/at;->f()I

    move-result v0

    goto :goto_0
.end method

.method public onCompletion(Landroid/media/MediaPlayer;)V
    .locals 2

    .prologue
    .line 637
    iget-object v0, p0, Lcom/peel/widget/AdVideoView;->I:Ljava/util/Map;

    const-string/jumbo v1, "on_video_end"

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 638
    iget-object v0, p0, Lcom/peel/widget/AdVideoView;->I:Ljava/util/Map;

    const-string/jumbo v1, "on_video_end"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/peel/widget/AdVideoView;->a(Ljava/lang/String;)V

    .line 640
    :cond_0
    invoke-virtual {p0}, Lcom/peel/widget/AdVideoView;->c()V

    .line 641
    return-void
.end method

.method public onError(Landroid/media/MediaPlayer;II)Z
    .locals 1

    .prologue
    .line 645
    invoke-virtual {p0}, Lcom/peel/widget/AdVideoView;->c()V

    .line 646
    const/4 v0, 0x0

    return v0
.end method

.method public setAudioMute(Z)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 551
    :try_start_0
    iget-object v0, p0, Lcom/peel/widget/AdVideoView;->q:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/peel/widget/AdVideoView;->q:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 552
    if-eqz p1, :cond_3

    .line 553
    iget-object v0, p0, Lcom/peel/widget/AdVideoView;->q:Landroid/media/MediaPlayer;

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/media/MediaPlayer;->setVolume(FF)V

    .line 554
    iget-object v0, p0, Lcom/peel/widget/AdVideoView;->I:Ljava/util/Map;

    const-string/jumbo v2, "on_video_mute"

    invoke-interface {v0, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 555
    iget-object v0, p0, Lcom/peel/widget/AdVideoView;->I:Ljava/util/Map;

    const-string/jumbo v2, "on_video_mute"

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/peel/widget/AdVideoView;->a(Ljava/lang/String;)V

    .line 557
    :cond_0
    invoke-static {}, Lcom/peel/util/a/f;->a()Lcom/peel/util/a/f;

    move-result-object v0

    sget-object v2, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v2}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v2

    if-nez v2, :cond_2

    :goto_0
    const/16 v2, 0x854

    const/16 v3, 0x1770

    invoke-virtual {v0, v1, v2, v3}, Lcom/peel/util/a/f;->a(III)V

    .line 568
    :goto_1
    iget-object v1, p0, Lcom/peel/widget/AdVideoView;->j:Landroid/widget/ImageButton;

    if-eqz p1, :cond_6

    sget v0, Lcom/peel/ui/fo;->video_vol:I

    :goto_2
    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 569
    iget-object v1, p0, Lcom/peel/widget/AdVideoView;->l:Landroid/widget/ImageButton;

    if-eqz p1, :cond_7

    sget v0, Lcom/peel/ui/fo;->btn_ad_video_vol_stateful:I

    :goto_3
    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 570
    iput-boolean p1, p0, Lcom/peel/widget/AdVideoView;->M:Z

    .line 575
    :cond_1
    :goto_4
    return-void

    .line 557
    :cond_2
    sget-object v1, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v1}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/data/at;->f()I

    move-result v1

    goto :goto_0

    .line 561
    :cond_3
    iget-object v0, p0, Lcom/peel/widget/AdVideoView;->q:Landroid/media/MediaPlayer;

    const/high16 v2, 0x3f800000    # 1.0f

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-virtual {v0, v2, v3}, Landroid/media/MediaPlayer;->setVolume(FF)V

    .line 562
    iget-object v0, p0, Lcom/peel/widget/AdVideoView;->I:Ljava/util/Map;

    const-string/jumbo v2, "on_video_unmute"

    invoke-interface {v0, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 563
    iget-object v0, p0, Lcom/peel/widget/AdVideoView;->I:Ljava/util/Map;

    const-string/jumbo v2, "on_video_unmute"

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/peel/widget/AdVideoView;->a(Ljava/lang/String;)V

    .line 565
    :cond_4
    invoke-static {}, Lcom/peel/util/a/f;->a()Lcom/peel/util/a/f;

    move-result-object v2

    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v0}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v0

    if-nez v0, :cond_5

    move v0, v1

    :goto_5
    const/16 v1, 0x855

    const/16 v3, 0x1770

    invoke-virtual {v2, v0, v1, v3}, Lcom/peel/util/a/f;->a(III)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 572
    :catch_0
    move-exception v0

    .line 573
    sget-object v1, Lcom/peel/widget/AdVideoView;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_4

    .line 565
    :cond_5
    :try_start_1
    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v0}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/data/at;->f()I

    move-result v0

    goto :goto_5

    .line 568
    :cond_6
    sget v0, Lcom/peel/ui/fo;->video_vol_mute:I

    goto :goto_2

    .line 569
    :cond_7
    sget v0, Lcom/peel/ui/fo;->btn_ad_video_mute_stateful:I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_3
.end method

.method public setFullscreen(Z)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, -0x1

    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 405
    iget-boolean v0, p0, Lcom/peel/widget/AdVideoView;->s:Z

    if-ne p1, v0, :cond_1

    .line 458
    :cond_0
    :goto_0
    return-void

    .line 406
    :cond_1
    if-nez p1, :cond_4

    .line 407
    iput-boolean v1, p0, Lcom/peel/widget/AdVideoView;->s:Z

    .line 408
    invoke-direct {p0, v1}, Lcom/peel/widget/AdVideoView;->setPlaybackCtrlVisible(Z)V

    .line 410
    iget-object v0, p0, Lcom/peel/widget/AdVideoView;->l:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 411
    iget-object v0, p0, Lcom/peel/widget/AdVideoView;->m:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 413
    iget-object v3, p0, Lcom/peel/widget/AdVideoView;->g:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/peel/widget/AdVideoView;->A:Ljava/lang/String;

    if-eqz v0, :cond_2

    move v0, v1

    :goto_1
    invoke-virtual {v3, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 414
    iget-object v0, p0, Lcom/peel/widget/AdVideoView;->f:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/peel/widget/AdVideoView;->A:Ljava/lang/String;

    if-nez v3, :cond_3

    :goto_2
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 416
    iget-object v0, p0, Lcom/peel/widget/AdVideoView;->c:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 417
    iget v1, p0, Lcom/peel/widget/AdVideoView;->H:I

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 418
    iget-object v1, p0, Lcom/peel/widget/AdVideoView;->c:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 420
    iget-object v0, p0, Lcom/peel/widget/AdVideoView;->d:Landroid/view/SurfaceView;

    invoke-virtual {v0}, Landroid/view/SurfaceView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 421
    iget v1, p0, Lcom/peel/widget/AdVideoView;->G:I

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 422
    iget v1, p0, Lcom/peel/widget/AdVideoView;->H:I

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 423
    iget-object v1, p0, Lcom/peel/widget/AdVideoView;->d:Landroid/view/SurfaceView;

    invoke-virtual {v1, v0}, Landroid/view/SurfaceView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 425
    iget-object v0, p0, Lcom/peel/widget/AdVideoView;->b:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 426
    const/4 v1, -0x2

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 427
    iget-object v1, p0, Lcom/peel/widget/AdVideoView;->b:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 429
    iget-boolean v0, p0, Lcom/peel/widget/AdVideoView;->v:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/peel/widget/AdVideoView;->g()V

    goto :goto_0

    :cond_2
    move v0, v2

    .line 413
    goto :goto_1

    :cond_3
    move v1, v2

    .line 414
    goto :goto_2

    .line 431
    :cond_4
    iput-boolean v4, p0, Lcom/peel/widget/AdVideoView;->s:Z

    .line 432
    invoke-direct {p0, v4}, Lcom/peel/widget/AdVideoView;->setPlaybackCtrlVisible(Z)V

    .line 433
    iget-object v0, p0, Lcom/peel/widget/AdVideoView;->g:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 434
    iget-object v0, p0, Lcom/peel/widget/AdVideoView;->f:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 436
    iget-object v0, p0, Lcom/peel/widget/AdVideoView;->l:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 437
    iget-object v0, p0, Lcom/peel/widget/AdVideoView;->m:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 439
    iget-object v0, p0, Lcom/peel/widget/AdVideoView;->b:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 440
    iput v3, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 441
    iget-object v1, p0, Lcom/peel/widget/AdVideoView;->b:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 443
    iget-object v0, p0, Lcom/peel/widget/AdVideoView;->d:Landroid/view/SurfaceView;

    invoke-virtual {v0}, Landroid/view/SurfaceView;->getWidth()I

    move-result v0

    iput v0, p0, Lcom/peel/widget/AdVideoView;->G:I

    .line 444
    iget-object v0, p0, Lcom/peel/widget/AdVideoView;->d:Landroid/view/SurfaceView;

    invoke-virtual {v0}, Landroid/view/SurfaceView;->getHeight()I

    move-result v0

    iput v0, p0, Lcom/peel/widget/AdVideoView;->H:I

    .line 446
    iget v0, p0, Lcom/peel/widget/AdVideoView;->E:I

    int-to-float v0, v0

    iget v1, p0, Lcom/peel/widget/AdVideoView;->F:I

    int-to-float v1, v1

    div-float/2addr v0, v1

    .line 448
    iget-object v1, p0, Lcom/peel/widget/AdVideoView;->c:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    .line 449
    iget-object v2, p0, Lcom/peel/widget/AdVideoView;->c:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getRootView()Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getWidth()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v2, v0

    float-to-int v2, v2

    iput v2, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 450
    iget-object v2, p0, Lcom/peel/widget/AdVideoView;->c:Landroid/view/View;

    invoke-virtual {v2, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 452
    iget-object v1, p0, Lcom/peel/widget/AdVideoView;->d:Landroid/view/SurfaceView;

    invoke-virtual {v1}, Landroid/view/SurfaceView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    .line 453
    iput v3, v1, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 454
    iget-object v2, p0, Lcom/peel/widget/AdVideoView;->c:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getRootView()Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getWidth()I

    move-result v2

    int-to-float v2, v2

    div-float v0, v2, v0

    float-to-int v0, v0

    iput v0, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 455
    iget-object v0, p0, Lcom/peel/widget/AdVideoView;->d:Landroid/view/SurfaceView;

    invoke-virtual {v0, v1}, Landroid/view/SurfaceView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto/16 :goto_0
.end method

.method public setViewEnabled(Z)V
    .locals 1

    .prologue
    .line 400
    if-eqz p1, :cond_1

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, v0}, Lcom/peel/widget/AdVideoView;->setVisibility(I)V

    .line 401
    if-nez p1, :cond_0

    invoke-direct {p0}, Lcom/peel/widget/AdVideoView;->j()V

    .line 402
    :cond_0
    return-void

    .line 400
    :cond_1
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public surfaceChanged(Landroid/view/SurfaceHolder;III)V
    .locals 0

    .prologue
    .line 628
    return-void
.end method

.method public surfaceCreated(Landroid/view/SurfaceHolder;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 579
    iget-boolean v0, p0, Lcom/peel/widget/AdVideoView;->t:Z

    if-nez v0, :cond_2

    .line 581
    :try_start_0
    iget-object v0, p0, Lcom/peel/widget/AdVideoView;->q:Landroid/media/MediaPlayer;

    invoke-virtual {v0, p1}, Landroid/media/MediaPlayer;->setDisplay(Landroid/view/SurfaceHolder;)V

    .line 582
    iget-boolean v0, p0, Lcom/peel/widget/AdVideoView;->y:Z

    if-eqz v0, :cond_0

    .line 583
    iget-boolean v0, p0, Lcom/peel/widget/AdVideoView;->y:Z

    invoke-virtual {p0, v0}, Lcom/peel/widget/AdVideoView;->setAudioMute(Z)V

    .line 586
    :cond_0
    iget-boolean v0, p0, Lcom/peel/widget/AdVideoView;->u:Z

    if-nez v0, :cond_1

    .line 587
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/peel/widget/AdVideoView;->u:Z

    .line 588
    iget-object v0, p0, Lcom/peel/widget/AdVideoView;->q:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->getVideoWidth()I

    move-result v0

    iput v0, p0, Lcom/peel/widget/AdVideoView;->E:I

    .line 589
    iget-object v0, p0, Lcom/peel/widget/AdVideoView;->q:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->getVideoHeight()I

    move-result v0

    iput v0, p0, Lcom/peel/widget/AdVideoView;->F:I

    .line 590
    iget-object v0, p0, Lcom/peel/widget/AdVideoView;->c:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v0

    int-to-float v0, v0

    .line 591
    iget-object v1, p0, Lcom/peel/widget/AdVideoView;->c:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v1

    int-to-float v1, v1

    .line 592
    iget v2, p0, Lcom/peel/widget/AdVideoView;->E:I

    int-to-float v2, v2

    iget v3, p0, Lcom/peel/widget/AdVideoView;->F:I

    int-to-float v3, v3

    div-float/2addr v2, v3

    .line 593
    iget-object v3, p0, Lcom/peel/widget/AdVideoView;->d:Landroid/view/SurfaceView;

    invoke-virtual {v3}, Landroid/view/SurfaceView;->getHeight()I

    move-result v3

    int-to-float v3, v3

    mul-float/2addr v3, v2

    .line 595
    iget-object v4, p0, Lcom/peel/widget/AdVideoView;->d:Landroid/view/SurfaceView;

    invoke-virtual {v4}, Landroid/view/SurfaceView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    .line 596
    cmpl-float v5, v3, v0

    if-lez v5, :cond_3

    .line 597
    float-to-int v1, v0

    iput v1, v4, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 598
    div-float/2addr v0, v2

    float-to-int v0, v0

    iput v0, v4, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 603
    :goto_0
    iget-object v0, p0, Lcom/peel/widget/AdVideoView;->d:Landroid/view/SurfaceView;

    invoke-virtual {v0, v4}, Landroid/view/SurfaceView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 605
    iget-object v0, p0, Lcom/peel/widget/AdVideoView;->c:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 606
    const/4 v1, -0x2

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 607
    iget-object v1, p0, Lcom/peel/widget/AdVideoView;->c:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 609
    iget-object v0, p0, Lcom/peel/widget/AdVideoView;->b:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 610
    const/4 v1, -0x2

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 611
    iget-object v1, p0, Lcom/peel/widget/AdVideoView;->b:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 613
    :cond_1
    iget-object v0, p0, Lcom/peel/widget/AdVideoView;->d:Landroid/view/SurfaceView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/SurfaceView;->setClickable(Z)V

    .line 614
    iget-object v0, p0, Lcom/peel/widget/AdVideoView;->N:Landroid/os/Handler;

    iget-object v1, p0, Lcom/peel/widget/AdVideoView;->O:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 625
    :cond_2
    :goto_1
    return-void

    .line 600
    :cond_3
    float-to-int v0, v3

    iput v0, v4, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 601
    float-to-int v0, v1

    iput v0, v4, Landroid/view/ViewGroup$LayoutParams;->height:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 616
    :catch_0
    move-exception v0

    .line 617
    sget-object v1, Lcom/peel/widget/AdVideoView;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 618
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/peel/widget/AdVideoView;->setVisibility(I)V

    .line 619
    iget-object v0, p0, Lcom/peel/widget/AdVideoView;->N:Landroid/os/Handler;

    iget-object v1, p0, Lcom/peel/widget/AdVideoView;->O:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 620
    iget-object v0, p0, Lcom/peel/widget/AdVideoView;->q:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->release()V

    .line 621
    iput-object v6, p0, Lcom/peel/widget/AdVideoView;->q:Landroid/media/MediaPlayer;

    .line 622
    iput-object v6, p0, Lcom/peel/widget/AdVideoView;->e:Landroid/view/SurfaceHolder;

    goto :goto_1
.end method

.method public surfaceDestroyed(Landroid/view/SurfaceHolder;)V
    .locals 1

    .prologue
    .line 632
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/peel/widget/AdVideoView;->e:Landroid/view/SurfaceHolder;

    .line 633
    return-void
.end method
