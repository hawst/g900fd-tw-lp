.class Lcom/peel/ui/no;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:I

.field final synthetic b:Lcom/peel/content/listing/Listing;

.field final synthetic c:Z

.field final synthetic d:[Lcom/peel/content/listing/Listing;

.field final synthetic e:Z

.field final synthetic f:Ljava/lang/String;

.field final synthetic g:Lcom/peel/ui/nm;


# direct methods
.method constructor <init>(Lcom/peel/ui/nm;ILcom/peel/content/listing/Listing;Z[Lcom/peel/content/listing/Listing;ZLjava/lang/String;)V
    .locals 0

    .prologue
    .line 150
    iput-object p1, p0, Lcom/peel/ui/no;->g:Lcom/peel/ui/nm;

    iput p2, p0, Lcom/peel/ui/no;->a:I

    iput-object p3, p0, Lcom/peel/ui/no;->b:Lcom/peel/content/listing/Listing;

    iput-boolean p4, p0, Lcom/peel/ui/no;->c:Z

    iput-object p5, p0, Lcom/peel/ui/no;->d:[Lcom/peel/content/listing/Listing;

    iput-boolean p6, p0, Lcom/peel/ui/no;->e:Z

    iput-object p7, p0, Lcom/peel/ui/no;->f:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 12

    .prologue
    const/4 v11, 0x0

    const/4 v10, 0x1

    const/16 v3, 0x7dd

    const/4 v7, 0x0

    .line 153
    iget-object v0, p0, Lcom/peel/ui/no;->g:Lcom/peel/ui/nm;

    iget-object v0, v0, Lcom/peel/ui/nm;->a:Lcom/peel/ui/nl;

    invoke-static {v0}, Lcom/peel/ui/nl;->b(Lcom/peel/ui/nl;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iget-object v1, p0, Lcom/peel/ui/no;->g:Lcom/peel/ui/nm;

    iget-object v1, v1, Lcom/peel/ui/nm;->a:Lcom/peel/ui/nl;

    invoke-static {v1}, Lcom/peel/ui/nl;->b(Lcom/peel/ui/nl;)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/FrameLayout;

    invoke-virtual {v1}, Landroid/widget/FrameLayout;->getChildCount()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->removeViewAt(I)V

    .line 155
    invoke-static {}, Lcom/peel/util/a/f;->a()Lcom/peel/util/a/f;

    move-result-object v0

    sget-object v1, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v1}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v1

    if-nez v1, :cond_0

    move v1, v10

    :goto_0
    const/16 v2, 0x4b1

    const-string/jumbo v4, "favorites"

    iget v5, p0, Lcom/peel/ui/no;->a:I

    iget-object v6, p0, Lcom/peel/ui/no;->b:Lcom/peel/content/listing/Listing;

    .line 157
    invoke-static {v6}, Lcom/peel/util/bx;->b(Lcom/peel/content/listing/Listing;)Ljava/lang/String;

    move-result-object v6

    iget-object v8, p0, Lcom/peel/ui/no;->b:Lcom/peel/content/listing/Listing;

    invoke-virtual {v8}, Lcom/peel/content/listing/Listing;->h()Ljava/lang/String;

    move-result-object v8

    move v9, v7

    .line 155
    invoke-virtual/range {v0 .. v9}, Lcom/peel/util/a/f;->a(IIILjava/lang/String;ILjava/lang/String;ILjava/lang/String;I)V

    .line 161
    iget-boolean v0, p0, Lcom/peel/ui/no;->c:Z

    if-eqz v0, :cond_3

    .line 163
    iget-object v0, p0, Lcom/peel/ui/no;->g:Lcom/peel/ui/nm;

    iget-object v0, v0, Lcom/peel/ui/nm;->a:Lcom/peel/ui/nl;

    invoke-static {v0}, Lcom/peel/ui/nl;->c(Lcom/peel/ui/nl;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 164
    iget-object v0, p0, Lcom/peel/ui/no;->b:Lcom/peel/content/listing/Listing;

    instance-of v0, v0, Lcom/peel/content/listing/LiveListing;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/peel/ui/no;->b:Lcom/peel/content/listing/Listing;

    check-cast v0, Lcom/peel/content/listing/LiveListing;

    .line 165
    invoke-virtual {v0}, Lcom/peel/content/listing/LiveListing;->d()Ljava/lang/String;

    move-result-object v0

    .line 166
    :goto_1
    iget-object v1, p0, Lcom/peel/ui/no;->g:Lcom/peel/ui/nm;

    iget-object v1, v1, Lcom/peel/ui/nm;->a:Lcom/peel/ui/nl;

    invoke-virtual {v1}, Lcom/peel/ui/nl;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/peel/util/bx;->e(Landroid/content/Context;Ljava/lang/String;)V

    .line 168
    iget-object v0, p0, Lcom/peel/ui/no;->b:Lcom/peel/content/listing/Listing;

    invoke-static {v0, v3}, Lcom/peel/util/bx;->b(Lcom/peel/content/listing/Listing;I)V

    .line 170
    iget-object v0, p0, Lcom/peel/ui/no;->d:[Lcom/peel/content/listing/Listing;

    aget-object v0, v0, v7

    check-cast v0, Lcom/peel/content/listing/LiveListing;

    invoke-static {v0}, Lcom/peel/util/bx;->a(Lcom/peel/content/listing/LiveListing;)V

    .line 204
    :goto_2
    return-void

    .line 155
    :cond_0
    sget-object v1, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v1}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/data/at;->f()I

    move-result v1

    goto :goto_0

    :cond_1
    move-object v0, v11

    .line 165
    goto :goto_1

    .line 173
    :cond_2
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 174
    const-string/jumbo v1, "control_only_mode"

    invoke-virtual {v0, v1, v7}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 175
    const-string/jumbo v1, "passback_clazz"

    iget-object v2, p0, Lcom/peel/ui/no;->g:Lcom/peel/ui/nm;

    iget-object v2, v2, Lcom/peel/ui/nm;->a:Lcom/peel/ui/nl;

    invoke-virtual {v2}, Lcom/peel/ui/nl;->m()Landroid/support/v4/app/ae;

    move-result-object v2

    invoke-static {v2}, Lcom/peel/d/e;->a(Landroid/support/v4/app/ae;)Lcom/peel/d/u;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 176
    const-string/jumbo v1, "passback_bundle"

    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 177
    iget-object v1, p0, Lcom/peel/ui/no;->g:Lcom/peel/ui/nm;

    iget-object v1, v1, Lcom/peel/ui/nm;->a:Lcom/peel/ui/nl;

    invoke-virtual {v1}, Lcom/peel/ui/nl;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    const-class v2, Lcom/peel/i/dr;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lcom/peel/d/e;->c(Landroid/support/v4/app/ae;Ljava/lang/String;Landroid/os/Bundle;)V

    goto :goto_2

    .line 182
    :cond_3
    iget-boolean v0, p0, Lcom/peel/ui/no;->e:Z

    if-eqz v0, :cond_6

    .line 183
    sget-object v0, Lcom/peel/content/a;->e:Ljava/lang/String;

    const-string/jumbo v1, "CN"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 184
    iget-object v0, p0, Lcom/peel/ui/no;->g:Lcom/peel/ui/nm;

    iget-object v0, v0, Lcom/peel/ui/nm;->a:Lcom/peel/ui/nl;

    invoke-virtual {v0}, Lcom/peel/ui/nl;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/ui/no;->d:[Lcom/peel/content/listing/Listing;

    aget-object v1, v1, v7

    iget-object v2, p0, Lcom/peel/ui/no;->f:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/peel/util/bx;->a(Landroid/content/Context;Lcom/peel/content/listing/Listing;Ljava/lang/String;)V

    .line 187
    iget-object v0, p0, Lcom/peel/ui/no;->g:Lcom/peel/ui/nm;

    iget-object v0, v0, Lcom/peel/ui/nm;->a:Lcom/peel/ui/nl;

    iget-object v0, v0, Lcom/peel/ui/nl;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "selective"

    invoke-virtual {v0, v1, v10}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 188
    iget-object v0, p0, Lcom/peel/ui/no;->g:Lcom/peel/ui/nm;

    iget-object v0, v0, Lcom/peel/ui/nm;->a:Lcom/peel/ui/nl;

    iget-object v1, p0, Lcom/peel/ui/no;->g:Lcom/peel/ui/nm;

    iget-object v1, v1, Lcom/peel/ui/nm;->a:Lcom/peel/ui/nl;

    iget-object v1, v1, Lcom/peel/ui/nl;->c:Landroid/os/Bundle;

    invoke-virtual {v0, v1}, Lcom/peel/ui/nl;->c(Landroid/os/Bundle;)V

    .line 193
    :goto_3
    invoke-static {}, Lcom/peel/util/a/f;->a()Lcom/peel/util/a/f;

    move-result-object v0

    sget-object v1, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v1}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v1

    if-nez v1, :cond_5

    move v1, v10

    :goto_4
    const/16 v2, 0x4d1

    const-string/jumbo v4, "favorites"

    iget v5, p0, Lcom/peel/ui/no;->a:I

    iget-object v6, p0, Lcom/peel/ui/no;->b:Lcom/peel/content/listing/Listing;

    .line 195
    invoke-static {v6}, Lcom/peel/util/bx;->b(Lcom/peel/content/listing/Listing;)Ljava/lang/String;

    move-result-object v6

    iget-object v8, p0, Lcom/peel/ui/no;->b:Lcom/peel/content/listing/Listing;

    invoke-virtual {v8}, Lcom/peel/content/listing/Listing;->h()Ljava/lang/String;

    move-result-object v8

    move v9, v7

    .line 193
    invoke-virtual/range {v0 .. v9}, Lcom/peel/util/a/f;->a(IIILjava/lang/String;ILjava/lang/String;ILjava/lang/String;I)V

    goto/16 :goto_2

    .line 190
    :cond_4
    iget-object v0, p0, Lcom/peel/ui/no;->g:Lcom/peel/ui/nm;

    iget-object v0, v0, Lcom/peel/ui/nm;->a:Lcom/peel/ui/nl;

    iget-object v1, p0, Lcom/peel/ui/no;->d:[Lcom/peel/content/listing/Listing;

    aget-object v1, v1, v7

    invoke-static {v0, v1}, Lcom/peel/ui/nl;->a(Lcom/peel/ui/nl;Lcom/peel/content/listing/Listing;)V

    goto :goto_3

    .line 193
    :cond_5
    sget-object v1, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v1}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/data/at;->f()I

    move-result v1

    goto :goto_4

    .line 197
    :cond_6
    iget-object v0, p0, Lcom/peel/ui/no;->d:[Lcom/peel/content/listing/Listing;

    aget-object v0, v0, v7

    iget-object v1, p0, Lcom/peel/ui/no;->g:Lcom/peel/ui/nm;

    iget-object v1, v1, Lcom/peel/ui/nm;->a:Lcom/peel/ui/nl;

    invoke-virtual {v1}, Lcom/peel/ui/nl;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    invoke-static {v0, v1, v3, v11, v11}, Lcom/peel/util/bx;->a(Lcom/peel/content/listing/Listing;Landroid/support/v4/app/ae;ILjava/lang/String;Ljava/lang/String;)V

    .line 199
    invoke-static {}, Lcom/peel/util/a/f;->a()Lcom/peel/util/a/f;

    move-result-object v0

    sget-object v1, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v1}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v1

    if-nez v1, :cond_7

    move v1, v10

    :goto_5
    const/16 v2, 0x4cf

    const-string/jumbo v4, "favorites"

    iget v5, p0, Lcom/peel/ui/no;->a:I

    iget-object v6, p0, Lcom/peel/ui/no;->b:Lcom/peel/content/listing/Listing;

    .line 201
    invoke-static {v6}, Lcom/peel/util/bx;->b(Lcom/peel/content/listing/Listing;)Ljava/lang/String;

    move-result-object v6

    iget-object v8, p0, Lcom/peel/ui/no;->b:Lcom/peel/content/listing/Listing;

    invoke-virtual {v8}, Lcom/peel/content/listing/Listing;->h()Ljava/lang/String;

    move-result-object v8

    move v9, v7

    .line 199
    invoke-virtual/range {v0 .. v9}, Lcom/peel/util/a/f;->a(IIILjava/lang/String;ILjava/lang/String;ILjava/lang/String;I)V

    goto/16 :goto_2

    :cond_7
    sget-object v1, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v1}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/data/at;->f()I

    move-result v1

    goto :goto_5
.end method
