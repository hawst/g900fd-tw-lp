.class Lcom/peel/ui/hp;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/peel/ui/ho;


# direct methods
.method constructor <init>(Lcom/peel/ui/ho;)V
    .locals 0

    .prologue
    .line 227
    iput-object p1, p0, Lcom/peel/ui/hp;->a:Lcom/peel/ui/ho;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 229
    iget-object v0, p0, Lcom/peel/ui/hp;->a:Lcom/peel/ui/ho;

    iget-object v0, v0, Lcom/peel/ui/ho;->a:Lcom/peel/ui/gt;

    new-instance v1, Lcom/peel/widget/ag;

    iget-object v2, p0, Lcom/peel/ui/hp;->a:Lcom/peel/ui/ho;

    iget-object v2, v2, Lcom/peel/ui/ho;->a:Lcom/peel/ui/gt;

    invoke-virtual {v2}, Lcom/peel/ui/gt;->m()Landroid/support/v4/app/ae;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/peel/widget/ag;-><init>(Landroid/content/Context;)V

    sget v2, Lcom/peel/ui/ft;->error:I

    .line 230
    invoke-virtual {v1, v2}, Lcom/peel/widget/ag;->a(I)Lcom/peel/widget/ag;

    move-result-object v1

    const-string/jumbo v2, "We can\'t locate your DirecTV box at the moment. Please make sure your device and your DirecTV box are connected to the same Wi-Fi network."

    .line 231
    invoke-virtual {v1, v2}, Lcom/peel/widget/ag;->b(Ljava/lang/CharSequence;)Lcom/peel/widget/ag;

    move-result-object v1

    sget v2, Lcom/peel/ui/ft;->okay:I

    const/4 v3, 0x0

    .line 233
    invoke-virtual {v1, v2, v3}, Lcom/peel/widget/ag;->a(ILandroid/content/DialogInterface$OnClickListener;)Lcom/peel/widget/ag;

    move-result-object v1

    .line 229
    invoke-static {v0, v1}, Lcom/peel/ui/gt;->a(Lcom/peel/ui/gt;Lcom/peel/widget/ag;)Lcom/peel/widget/ag;

    .line 235
    iget-object v0, p0, Lcom/peel/ui/hp;->a:Lcom/peel/ui/ho;

    iget-object v0, v0, Lcom/peel/ui/ho;->a:Lcom/peel/ui/gt;

    invoke-static {v0}, Lcom/peel/ui/gt;->d(Lcom/peel/ui/gt;)Lcom/peel/widget/ag;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/ui/hp;->a:Lcom/peel/ui/ho;

    iget-object v1, v1, Lcom/peel/ui/ho;->a:Lcom/peel/ui/gt;

    invoke-static {v1}, Lcom/peel/ui/gt;->d(Lcom/peel/ui/gt;)Lcom/peel/widget/ag;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/peel/widget/ag;->a(Lcom/peel/widget/ag;)Lcom/peel/widget/ag;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/widget/ag;->show()V

    .line 236
    return-void
.end method
