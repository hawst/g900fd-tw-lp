.class Lcom/peel/ui/ih;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/peel/ui/gt;


# direct methods
.method constructor <init>(Lcom/peel/ui/gt;)V
    .locals 0

    .prologue
    .line 1859
    iput-object p1, p0, Lcom/peel/ui/ih;->a:Lcom/peel/ui/gt;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    .line 1863
    iget-object v0, p0, Lcom/peel/ui/ih;->a:Lcom/peel/ui/gt;

    invoke-static {v0}, Lcom/peel/ui/gt;->A(Lcom/peel/ui/gt;)Landroid/widget/PopupWindow;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1864
    iget-object v0, p0, Lcom/peel/ui/ih;->a:Lcom/peel/ui/gt;

    invoke-static {v0}, Lcom/peel/ui/gt;->A(Lcom/peel/ui/gt;)Landroid/widget/PopupWindow;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->dismiss()V

    .line 1872
    :cond_0
    :goto_0
    return-void

    .line 1866
    :cond_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-object v2, p0, Lcom/peel/ui/ih;->a:Lcom/peel/ui/gt;

    invoke-static {v2}, Lcom/peel/ui/gt;->B(Lcom/peel/ui/gt;)J

    move-result-wide v2

    sub-long/2addr v0, v2

    .line 1868
    const-wide/16 v2, 0xc8

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/peel/ui/ih;->a:Lcom/peel/ui/gt;

    invoke-static {v0}, Lcom/peel/ui/gt;->g(Lcom/peel/ui/gt;)Lcom/peel/widget/SlidingDrawer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/widget/SlidingDrawer;->e()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1869
    iget-object v0, p0, Lcom/peel/ui/ih;->a:Lcom/peel/ui/gt;

    invoke-static {v0}, Lcom/peel/ui/gt;->A(Lcom/peel/ui/gt;)Landroid/widget/PopupWindow;

    move-result-object v0

    const/16 v1, 0x32

    const/16 v2, 0xa

    invoke-virtual {v0, p1, v1, v2}, Landroid/widget/PopupWindow;->showAsDropDown(Landroid/view/View;II)V

    goto :goto_0
.end method
