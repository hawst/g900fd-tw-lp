.class Lcom/peel/ui/jx;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/peel/ui/jw;


# direct methods
.method constructor <init>(Lcom/peel/ui/jw;)V
    .locals 0

    .prologue
    .line 3479
    iput-object p1, p0, Lcom/peel/ui/jx;->a:Lcom/peel/ui/jw;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 3481
    iget-object v0, p0, Lcom/peel/ui/jx;->a:Lcom/peel/ui/jw;

    invoke-static {v0}, Lcom/peel/ui/jw;->a(Lcom/peel/ui/jw;)I

    move-result v0

    if-ne v1, v0, :cond_0

    .line 3482
    iget-object v0, p0, Lcom/peel/ui/jx;->a:Lcom/peel/ui/jw;

    invoke-static {v0}, Lcom/peel/ui/jw;->c(Lcom/peel/ui/jw;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/ui/jx;->a:Lcom/peel/ui/jw;

    invoke-static {v1}, Lcom/peel/ui/jw;->b(Lcom/peel/ui/jw;)Ljava/lang/Runnable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 3495
    :goto_0
    return-void

    .line 3484
    :cond_0
    iget-object v0, p0, Lcom/peel/ui/jx;->a:Lcom/peel/ui/jw;

    invoke-static {v0}, Lcom/peel/ui/jw;->d(Lcom/peel/ui/jw;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ImageView;->performClick()Z

    .line 3485
    iget-object v0, p0, Lcom/peel/ui/jx;->a:Lcom/peel/ui/jw;

    invoke-static {v0}, Lcom/peel/ui/jw;->d(Lcom/peel/ui/jw;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setPressed(Z)V

    .line 3486
    iget-object v0, p0, Lcom/peel/ui/jx;->a:Lcom/peel/ui/jw;

    invoke-static {v0}, Lcom/peel/ui/jw;->d(Lcom/peel/ui/jw;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ImageView;->isSoundEffectsEnabled()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 3487
    iget-object v0, p0, Lcom/peel/ui/jx;->a:Lcom/peel/ui/jw;

    invoke-static {v0}, Lcom/peel/ui/jw;->d(Lcom/peel/ui/jw;)Landroid/widget/ImageView;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setSoundEffectsEnabled(Z)V

    .line 3489
    :cond_1
    iget-object v0, p0, Lcom/peel/ui/jx;->a:Lcom/peel/ui/jw;

    invoke-static {v0}, Lcom/peel/ui/jw;->c(Lcom/peel/ui/jw;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/ui/jx;->a:Lcom/peel/ui/jw;

    invoke-static {v1}, Lcom/peel/ui/jw;->e(Lcom/peel/ui/jw;)J

    move-result-wide v2

    invoke-virtual {v0, p0, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 3490
    iget-object v0, p0, Lcom/peel/ui/jx;->a:Lcom/peel/ui/jw;

    const-wide/16 v2, 0x14

    invoke-static {v0, v2, v3}, Lcom/peel/ui/jw;->a(Lcom/peel/ui/jw;J)J

    .line 3492
    iget-object v0, p0, Lcom/peel/ui/jx;->a:Lcom/peel/ui/jw;

    invoke-static {v0}, Lcom/peel/ui/jw;->c(Lcom/peel/ui/jw;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/ui/jx;->a:Lcom/peel/ui/jw;

    invoke-static {v1}, Lcom/peel/ui/jw;->f(Lcom/peel/ui/jw;)Ljava/lang/Runnable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 3493
    iget-object v0, p0, Lcom/peel/ui/jx;->a:Lcom/peel/ui/jw;

    invoke-static {v0}, Lcom/peel/ui/jw;->c(Lcom/peel/ui/jw;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/ui/jx;->a:Lcom/peel/ui/jw;

    invoke-static {v1}, Lcom/peel/ui/jw;->f(Lcom/peel/ui/jw;)Ljava/lang/Runnable;

    move-result-object v1

    const-wide/16 v2, 0x96

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method
