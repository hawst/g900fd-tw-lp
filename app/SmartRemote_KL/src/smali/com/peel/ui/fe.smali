.class Lcom/peel/ui/fe;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/peel/ui/fh;

.field final synthetic b:I

.field final synthetic c:Ljava/lang/String;

.field final synthetic d:I

.field final synthetic e:Lcom/peel/ui/fd;


# direct methods
.method constructor <init>(Lcom/peel/ui/fd;Lcom/peel/ui/fh;ILjava/lang/String;I)V
    .locals 0

    .prologue
    .line 406
    iput-object p1, p0, Lcom/peel/ui/fe;->e:Lcom/peel/ui/fd;

    iput-object p2, p0, Lcom/peel/ui/fe;->a:Lcom/peel/ui/fh;

    iput p3, p0, Lcom/peel/ui/fe;->b:I

    iput-object p4, p0, Lcom/peel/ui/fe;->c:Ljava/lang/String;

    iput p5, p0, Lcom/peel/ui/fe;->d:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 410
    iget-object v0, p0, Lcom/peel/ui/fe;->a:Lcom/peel/ui/fh;

    iget-object v0, v0, Lcom/peel/ui/fh;->a:Landroid/widget/ImageView;

    sget v1, Lcom/peel/ui/fp;->tag_image:I

    .line 411
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 410
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iget v1, p0, Lcom/peel/ui/fe;->b:I

    sub-int/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    const/4 v1, 0x7

    if-ge v0, v1, :cond_0

    .line 413
    iget-object v0, p0, Lcom/peel/ui/fe;->e:Lcom/peel/ui/fd;

    iget-object v0, v0, Lcom/peel/ui/fd;->a:Lcom/peel/ui/ey;

    invoke-static {v0}, Lcom/peel/ui/ey;->e(Lcom/peel/ui/ey;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/peel/util/b/e;->a(Landroid/content/Context;)Lcom/squareup/picasso/Picasso;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/ui/fe;->c:Ljava/lang/String;

    .line 414
    invoke-virtual {v0, v1}, Lcom/squareup/picasso/Picasso;->load(Ljava/lang/String;)Lcom/squareup/picasso/RequestCreator;

    move-result-object v0

    iget v1, p0, Lcom/peel/ui/fe;->d:I

    .line 415
    invoke-virtual {v0, v1}, Lcom/squareup/picasso/RequestCreator;->placeholder(I)Lcom/squareup/picasso/RequestCreator;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/ui/fe;->a:Lcom/peel/ui/fh;

    iget-object v1, v1, Lcom/peel/ui/fh;->a:Landroid/widget/ImageView;

    new-instance v2, Lcom/peel/ui/ff;

    invoke-direct {v2, p0}, Lcom/peel/ui/ff;-><init>(Lcom/peel/ui/fe;)V

    .line 416
    invoke-virtual {v0, v1, v2}, Lcom/squareup/picasso/RequestCreator;->into(Landroid/widget/ImageView;Lcom/squareup/picasso/Callback;)V

    .line 431
    :cond_0
    return-void
.end method
