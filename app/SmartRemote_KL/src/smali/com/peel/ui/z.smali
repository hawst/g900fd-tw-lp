.class Lcom/peel/ui/z;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/widget/TextView$OnEditorActionListener;


# instance fields
.field final synthetic a:Lcom/peel/ui/v;


# direct methods
.method constructor <init>(Lcom/peel/ui/v;)V
    .locals 0

    .prologue
    .line 300
    iput-object p1, p0, Lcom/peel/ui/z;->a:Lcom/peel/ui/v;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onEditorAction(Landroid/widget/TextView;ILandroid/view/KeyEvent;)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 303
    iget-object v0, p0, Lcom/peel/ui/z;->a:Lcom/peel/ui/v;

    invoke-static {v0}, Lcom/peel/ui/v;->e(Lcom/peel/ui/v;)Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "actionId: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 304
    const/4 v0, 0x6

    if-eq p2, v0, :cond_0

    const/4 v0, 0x3

    if-ne p2, v0, :cond_1

    .line 305
    :cond_0
    iget-object v0, p0, Lcom/peel/ui/z;->a:Lcom/peel/ui/v;

    invoke-virtual {v0}, Lcom/peel/ui/v;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    const-string/jumbo v2, "input_method"

    invoke-virtual {v0, v2}, Landroid/support/v4/app/ae;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 306
    iget-object v2, p0, Lcom/peel/ui/z;->a:Lcom/peel/ui/v;

    invoke-virtual {v2}, Lcom/peel/ui/v;->m()Landroid/support/v4/app/ae;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/ae;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v2

    invoke-virtual {v0, v2, v1}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 307
    const/4 v0, 0x1

    .line 309
    :goto_0
    return v0

    :cond_1
    move v0, v1

    goto :goto_0
.end method
