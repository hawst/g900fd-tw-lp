.class public Lcom/peel/ui/kr;
.super Lcom/peel/d/u;

# interfaces
.implements Lcom/peel/widget/af;


# static fields
.field private static final ap:Ljava/lang/String;

.field private static as:I


# instance fields
.field private aj:Landroid/view/View;

.field private ak:Lcom/peel/widget/ObservableScrollView;

.field private al:I

.field private am:I

.field private an:Landroid/view/ViewGroup;

.field private ao:Landroid/animation/ObjectAnimator;

.field private aq:Lcom/peel/ui/a/p;

.field private ar:Z

.field private final at:Lcom/jess/ui/z;

.field private au:Landroid/content/BroadcastReceiver;

.field final e:Ljava/lang/Runnable;

.field private f:Landroid/view/LayoutInflater;

.field private g:Landroid/widget/LinearLayout;

.field private h:Landroid/view/View;

.field private i:Lcom/jess/ui/TwoWayGridView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 83
    const-class v0, Lcom/peel/ui/kr;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/peel/ui/kr;->ap:Ljava/lang/String;

    .line 87
    const/4 v0, 0x1

    sput v0, Lcom/peel/ui/kr;->as:I

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 73
    invoke-direct {p0}, Lcom/peel/d/u;-><init>()V

    .line 80
    const/4 v0, 0x0

    iput v0, p0, Lcom/peel/ui/kr;->al:I

    .line 89
    new-instance v0, Lcom/peel/ui/ks;

    invoke-direct {v0, p0}, Lcom/peel/ui/ks;-><init>(Lcom/peel/ui/kr;)V

    iput-object v0, p0, Lcom/peel/ui/kr;->at:Lcom/jess/ui/z;

    .line 763
    new-instance v0, Lcom/peel/ui/le;

    invoke-direct {v0, p0}, Lcom/peel/ui/le;-><init>(Lcom/peel/ui/kr;)V

    iput-object v0, p0, Lcom/peel/ui/kr;->e:Ljava/lang/Runnable;

    .line 1090
    new-instance v0, Lcom/peel/ui/kz;

    invoke-direct {v0, p0}, Lcom/peel/ui/kz;-><init>(Lcom/peel/ui/kr;)V

    iput-object v0, p0, Lcom/peel/ui/kr;->au:Landroid/content/BroadcastReceiver;

    return-void
.end method

.method static synthetic S()I
    .locals 1

    .prologue
    .line 73
    sget v0, Lcom/peel/ui/kr;->as:I

    return v0
.end method

.method private T()V
    .locals 12

    .prologue
    .line 423
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/peel/ui/kr;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v5

    .line 425
    invoke-virtual {p0}, Lcom/peel/ui/kr;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 426
    sget v1, Lcom/peel/ui/fq;->onboarding:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v11

    .line 427
    sget v0, Lcom/peel/ui/fp;->tutorials:I

    invoke-virtual {v11, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/support/v4/view/ViewPager;

    .line 429
    sget-object v0, Lcom/peel/util/a;->n:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/peel/util/a;->n:Ljava/lang/String;

    const-string/jumbo v1, "1"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 430
    :cond_0
    const/4 v0, 0x1

    sput v0, Lcom/peel/ui/kr;->as:I

    .line 431
    new-instance v0, Lcom/peel/ui/ll;

    invoke-virtual {p0}, Lcom/peel/ui/kr;->m()Landroid/support/v4/app/ae;

    move-result-object v2

    iget-object v4, p0, Lcom/peel/ui/kr;->b:Lcom/peel/d/i;

    const/4 v6, 0x1

    move-object v1, p0

    invoke-direct/range {v0 .. v6}, Lcom/peel/ui/ll;-><init>(Lcom/peel/ui/kr;Landroid/content/Context;Landroid/support/v4/view/ViewPager;Lcom/peel/d/i;Landroid/app/AlertDialog;I)V

    invoke-virtual {v3, v0}, Landroid/support/v4/view/ViewPager;->setAdapter(Landroid/support/v4/view/av;)V

    .line 440
    :goto_0
    const/4 v0, 0x0

    invoke-virtual {v3, v0}, Landroid/support/v4/view/ViewPager;->setPageMargin(I)V

    .line 441
    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    move-object v6, v11

    invoke-virtual/range {v5 .. v10}, Landroid/app/AlertDialog;->setView(Landroid/view/View;IIII)V

    .line 443
    sget-object v0, Lcom/peel/util/a;->j:Ljava/lang/String;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/peel/util/a;->j:Ljava/lang/String;

    sget-object v1, Lcom/peel/util/a;->c:[Ljava/lang/String;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 444
    :cond_1
    const/4 v0, 0x0

    invoke-virtual {v5, v0}, Landroid/app/AlertDialog;->setCancelable(Z)V

    .line 445
    const/4 v0, 0x0

    invoke-virtual {v5, v0}, Landroid/app/AlertDialog;->setCanceledOnTouchOutside(Z)V

    .line 451
    :cond_2
    :goto_1
    new-instance v0, Lcom/peel/ui/lc;

    invoke-direct {v0, p0}, Lcom/peel/ui/lc;-><init>(Lcom/peel/ui/kr;)V

    invoke-virtual {v5, v0}, Landroid/app/AlertDialog;->setOnShowListener(Landroid/content/DialogInterface$OnShowListener;)V

    .line 463
    new-instance v0, Lcom/peel/ui/ld;

    invoke-direct {v0, p0}, Lcom/peel/ui/ld;-><init>(Lcom/peel/ui/kr;)V

    invoke-virtual {v5, v0}, Landroid/app/AlertDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 474
    invoke-virtual {v5}, Landroid/app/AlertDialog;->show()V

    .line 475
    return-void

    .line 432
    :cond_3
    sget-object v0, Lcom/peel/util/a;->n:Ljava/lang/String;

    const-string/jumbo v1, "2"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 433
    const/4 v0, 0x2

    sput v0, Lcom/peel/ui/kr;->as:I

    .line 434
    new-instance v0, Lcom/peel/ui/ll;

    invoke-virtual {p0}, Lcom/peel/ui/kr;->m()Landroid/support/v4/app/ae;

    move-result-object v2

    iget-object v4, p0, Lcom/peel/ui/kr;->b:Lcom/peel/d/i;

    const/4 v6, 0x3

    move-object v1, p0

    invoke-direct/range {v0 .. v6}, Lcom/peel/ui/ll;-><init>(Lcom/peel/ui/kr;Landroid/content/Context;Landroid/support/v4/view/ViewPager;Lcom/peel/d/i;Landroid/app/AlertDialog;I)V

    invoke-virtual {v3, v0}, Landroid/support/v4/view/ViewPager;->setAdapter(Landroid/support/v4/view/av;)V

    goto :goto_0

    .line 436
    :cond_4
    const/4 v0, 0x3

    sput v0, Lcom/peel/ui/kr;->as:I

    .line 437
    new-instance v0, Lcom/peel/ui/ll;

    invoke-virtual {p0}, Lcom/peel/ui/kr;->m()Landroid/support/v4/app/ae;

    move-result-object v2

    iget-object v4, p0, Lcom/peel/ui/kr;->b:Lcom/peel/d/i;

    const/4 v6, 0x1

    move-object v1, p0

    invoke-direct/range {v0 .. v6}, Lcom/peel/ui/ll;-><init>(Lcom/peel/ui/kr;Landroid/content/Context;Landroid/support/v4/view/ViewPager;Lcom/peel/d/i;Landroid/app/AlertDialog;I)V

    invoke-virtual {v3, v0}, Landroid/support/v4/view/ViewPager;->setAdapter(Landroid/support/v4/view/av;)V

    goto :goto_0

    .line 446
    :cond_5
    sget-object v0, Lcom/peel/util/a;->j:Ljava/lang/String;

    sget-object v1, Lcom/peel/util/a;->c:[Ljava/lang/String;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 447
    const/4 v0, 0x1

    invoke-virtual {v5, v0}, Landroid/app/AlertDialog;->setCancelable(Z)V

    .line 448
    const/4 v0, 0x1

    invoke-virtual {v5, v0}, Landroid/app/AlertDialog;->setCanceledOnTouchOutside(Z)V

    goto :goto_1
.end method

.method private U()V
    .locals 3

    .prologue
    .line 866
    invoke-static {}, Lcom/peel/util/i;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 867
    iget-object v0, p0, Lcom/peel/ui/kr;->e:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 871
    :goto_0
    return-void

    .line 869
    :cond_0
    sget-object v0, Lcom/peel/ui/kr;->ap:Ljava/lang/String;

    const-string/jumbo v1, "offline"

    iget-object v2, p0, Lcom/peel/ui/kr;->e:Ljava/lang/Runnable;

    invoke-static {v0, v1, v2}, Lcom/peel/util/i;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/lang/Runnable;

    goto :goto_0
.end method

.method private V()V
    .locals 10

    .prologue
    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 874
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 875
    const-string/jumbo v0, "path"

    const-string/jumbo v2, "listing/top"

    invoke-virtual {v3, v0, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 876
    const-string/jumbo v0, "window"

    invoke-virtual {v3, v0, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 877
    const-string/jumbo v0, "cacheWindow"

    invoke-virtual {v3, v0, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 880
    :try_start_0
    sget-object v0, Lcom/peel/util/x;->c:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/text/SimpleDateFormat;

    invoke-static {}, Lcom/peel/util/x;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v6

    .line 882
    iget-object v0, p0, Lcom/peel/ui/kr;->c:Landroid/os/Bundle;

    const-string/jumbo v2, "forcetime"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 883
    sget-object v0, Lcom/peel/util/x;->c:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/text/SimpleDateFormat;

    iget-object v2, p0, Lcom/peel/ui/kr;->c:Landroid/os/Bundle;

    const-string/jumbo v4, "forcetime"

    invoke-virtual {v2, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    .line 887
    :goto_0
    const-string/jumbo v0, "start"

    invoke-virtual {v3, v0, v4, v5}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 888
    const-string/jumbo v0, "startofday"

    invoke-virtual {v3, v0, v6, v7}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 892
    :goto_1
    const-string/jumbo v0, "room"

    invoke-static {}, Lcom/peel/content/a;->a()Lcom/peel/data/ContentRoom;

    move-result-object v2

    invoke-virtual {v3, v0, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 893
    const-string/jumbo v0, "country"

    invoke-virtual {p0}, Lcom/peel/ui/kr;->m()Landroid/support/v4/app/ae;

    move-result-object v2

    invoke-static {v2}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v2

    const-string/jumbo v4, "country_ISO"

    const-string/jumbo v5, "US"

    invoke-interface {v2, v4, v5}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v0, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 894
    const-string/jumbo v2, "appversion"

    iget-object v0, p0, Lcom/peel/ui/kr;->b:Lcom/peel/d/i;

    const-string/jumbo v4, "versioncode"

    invoke-virtual {v0, v4}, Lcom/peel/d/i;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_2

    const-string/jumbo v0, "null"

    :goto_2
    invoke-virtual {v3, v2, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 897
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 898
    invoke-static {}, Lcom/peel/content/a;->c()[Lcom/peel/content/library/Library;

    move-result-object v2

    array-length v4, v2

    move v0, v1

    :goto_3
    if-ge v0, v4, :cond_3

    aget-object v6, v2, v0

    .line 900
    const-string/jumbo v7, "live"

    invoke-virtual {v6}, Lcom/peel/content/library/Library;->f()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 901
    invoke-interface {v5, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 898
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 885
    :cond_1
    :try_start_1
    sget-object v0, Lcom/peel/util/x;->c:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/text/SimpleDateFormat;

    iget-object v2, p0, Lcom/peel/ui/kr;->c:Landroid/os/Bundle;

    const-string/jumbo v4, "time"

    invoke-virtual {v2, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Date;->getTime()J
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    move-result-wide v4

    goto :goto_0

    .line 889
    :catch_0
    move-exception v0

    .line 890
    sget-object v2, Lcom/peel/ui/kr;->ap:Ljava/lang/String;

    sget-object v4, Lcom/peel/ui/kr;->ap:Ljava/lang/String;

    invoke-static {v2, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1

    .line 894
    :cond_2
    iget-object v0, p0, Lcom/peel/ui/kr;->b:Lcom/peel/d/i;

    const-string/jumbo v4, "versioncode"

    invoke-virtual {v0, v4}, Lcom/peel/d/i;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    .line 904
    :cond_3
    new-instance v4, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v4, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    .line 905
    new-instance v6, Landroid/os/Bundle;

    invoke-direct {v6}, Landroid/os/Bundle;-><init>()V

    .line 907
    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_4
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/peel/content/library/Library;

    .line 908
    sget-object v8, Lcom/peel/ui/kr;->ap:Ljava/lang/String;

    const-string/jumbo v9, "library.get"

    new-instance v0, Lcom/peel/ui/lj;

    move-object v1, p0

    invoke-direct/range {v0 .. v6}, Lcom/peel/ui/lj;-><init>(Lcom/peel/ui/kr;Lcom/peel/content/library/Library;Landroid/os/Bundle;Ljava/util/concurrent/atomic/AtomicInteger;Ljava/util/List;Landroid/os/Bundle;)V

    invoke-static {v8, v9, v0}, Lcom/peel/util/i;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    goto :goto_4

    .line 953
    :cond_4
    return-void
.end method

.method static synthetic a(Lcom/peel/ui/kr;Landroid/animation/ObjectAnimator;)Landroid/animation/ObjectAnimator;
    .locals 0

    .prologue
    .line 73
    iput-object p1, p0, Lcom/peel/ui/kr;->ao:Landroid/animation/ObjectAnimator;

    return-object p1
.end method

.method static synthetic a(Lcom/peel/ui/kr;)Landroid/view/View;
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/peel/ui/kr;->aj:Landroid/view/View;

    return-object v0
.end method

.method static synthetic a(Lcom/peel/ui/kr;Landroid/view/View;)Landroid/view/View;
    .locals 0

    .prologue
    .line 73
    iput-object p1, p0, Lcom/peel/ui/kr;->aj:Landroid/view/View;

    return-object p1
.end method

.method static synthetic a(Lcom/peel/ui/kr;Lcom/jess/ui/TwoWayGridView;)Lcom/jess/ui/TwoWayGridView;
    .locals 0

    .prologue
    .line 73
    iput-object p1, p0, Lcom/peel/ui/kr;->i:Lcom/jess/ui/TwoWayGridView;

    return-object p1
.end method

.method static synthetic a(Lcom/peel/ui/kr;Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 73
    invoke-direct {p0, p1}, Lcom/peel/ui/kr;->l(Landroid/os/Bundle;)V

    return-void
.end method

.method private a(Ljava/util/List;Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/peel/content/listing/Listing;",
            ">;",
            "Ljava/lang/String;",
            "Landroid/os/Bundle;",
            ")V"
        }
    .end annotation

    .prologue
    .line 957
    new-instance v0, Lcom/peel/ui/kw;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/peel/ui/kw;-><init>(Lcom/peel/ui/kr;Ljava/util/List;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 1027
    invoke-static {}, Lcom/peel/util/i;->c()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1028
    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 1032
    :goto_0
    return-void

    .line 1030
    :cond_0
    sget-object v1, Lcom/peel/ui/kr;->ap:Ljava/lang/String;

    const-string/jumbo v2, "notify"

    invoke-static {v1, v2, v0}, Lcom/peel/util/i;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/lang/Runnable;

    goto :goto_0
.end method

.method private ac()V
    .locals 2

    .prologue
    .line 1069
    invoke-virtual {p0}, Lcom/peel/ui/kr;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    new-instance v1, Lcom/peel/ui/kx;

    invoke-direct {v1, p0}, Lcom/peel/ui/kx;-><init>(Lcom/peel/ui/kr;)V

    invoke-static {v0, v1}, Lcom/peel/social/w;->a(Landroid/content/Context;Lcom/peel/util/t;)V

    .line 1088
    return-void
.end method

.method static synthetic b(Lcom/peel/ui/kr;)Landroid/view/View;
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/peel/ui/kr;->h:Landroid/view/View;

    return-object v0
.end method

.method static synthetic c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 73
    sget-object v0, Lcom/peel/ui/kr;->ap:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic c(Lcom/peel/ui/kr;)V
    .locals 0

    .prologue
    .line 73
    invoke-direct {p0}, Lcom/peel/ui/kr;->T()V

    return-void
.end method

.method static synthetic d(Lcom/peel/ui/kr;)Landroid/animation/ObjectAnimator;
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/peel/ui/kr;->ao:Landroid/animation/ObjectAnimator;

    return-object v0
.end method

.method static synthetic e(Lcom/peel/ui/kr;)Landroid/view/LayoutInflater;
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/peel/ui/kr;->f:Landroid/view/LayoutInflater;

    return-object v0
.end method

.method static synthetic f(Lcom/peel/ui/kr;)V
    .locals 0

    .prologue
    .line 73
    invoke-direct {p0}, Lcom/peel/ui/kr;->V()V

    return-void
.end method

.method static synthetic g(Lcom/peel/ui/kr;)V
    .locals 0

    .prologue
    .line 73
    invoke-direct {p0}, Lcom/peel/ui/kr;->U()V

    return-void
.end method

.method static synthetic h(Lcom/peel/ui/kr;)Landroid/widget/LinearLayout;
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/peel/ui/kr;->g:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic i(Lcom/peel/ui/kr;)Lcom/jess/ui/TwoWayGridView;
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/peel/ui/kr;->i:Lcom/jess/ui/TwoWayGridView;

    return-object v0
.end method

.method static synthetic j(Lcom/peel/ui/kr;)Lcom/jess/ui/z;
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/peel/ui/kr;->at:Lcom/jess/ui/z;

    return-object v0
.end method

.method static synthetic k(Lcom/peel/ui/kr;)I
    .locals 2

    .prologue
    .line 73
    iget v0, p0, Lcom/peel/ui/kr;->am:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/peel/ui/kr;->am:I

    return v0
.end method

.method static synthetic l(Lcom/peel/ui/kr;)I
    .locals 2

    .prologue
    .line 73
    iget v0, p0, Lcom/peel/ui/kr;->al:I

    add-int/lit8 v1, v0, -0x1

    iput v1, p0, Lcom/peel/ui/kr;->al:I

    return v0
.end method

.method private l(Landroid/os/Bundle;)V
    .locals 13

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 808
    const-string/jumbo v0, "libraryIds"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    .line 810
    new-instance v6, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v6}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>()V

    .line 812
    iget-object v0, p0, Lcom/peel/ui/kr;->ao:Landroid/animation/ObjectAnimator;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/peel/ui/kr;->ao:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 813
    sget-object v0, Lcom/peel/ui/kr;->ap:Ljava/lang/String;

    const-string/jumbo v1, "stop animator"

    new-instance v4, Lcom/peel/ui/lg;

    invoke-direct {v4, p0}, Lcom/peel/ui/lg;-><init>(Lcom/peel/ui/kr;)V

    invoke-static {v0, v1, v4}, Lcom/peel/util/i;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/lang/Runnable;

    .line 821
    :cond_0
    iget-object v0, p0, Lcom/peel/ui/kr;->h:Landroid/view/View;

    sget v1, Lcom/peel/ui/fp;->no_content_panel:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 822
    sget-object v0, Lcom/peel/ui/kr;->ap:Ljava/lang/String;

    const-string/jumbo v1, "hide no content"

    new-instance v4, Lcom/peel/ui/lh;

    invoke-direct {v4, p0}, Lcom/peel/ui/lh;-><init>(Lcom/peel/ui/kr;)V

    invoke-static {v0, v1, v4}, Lcom/peel/util/i;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/lang/Runnable;

    .line 830
    :cond_1
    if-eqz v5, :cond_3

    .line 831
    array-length v7, v5

    move v4, v3

    move v0, v3

    :goto_0
    if-ge v4, v7, :cond_4

    aget-object v8, v5, v4

    .line 832
    invoke-virtual {p1, v8}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    .line 834
    if-eqz v1, :cond_2

    .line 835
    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_1
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/content/library/RankCategory;

    .line 836
    new-instance v10, Landroid/os/Bundle;

    invoke-direct {v10}, Landroid/os/Bundle;-><init>()V

    .line 837
    invoke-virtual {v0}, Lcom/peel/content/library/RankCategory;->c()Ljava/util/List;

    move-result-object v1

    .line 838
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v12, "listings/"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    check-cast v1, Ljava/util/ArrayList;

    invoke-virtual {v10, v11, v1}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 839
    const-string/jumbo v1, "libraryIds"

    new-array v11, v2, [Ljava/lang/String;

    aput-object v8, v11, v3

    invoke-virtual {v10, v1, v11}, Landroid/os/Bundle;->putStringArray(Ljava/lang/String;[Ljava/lang/String;)V

    .line 840
    const-string/jumbo v1, "category"

    invoke-virtual {v0}, Lcom/peel/content/library/RankCategory;->b()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v1, v11}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 841
    invoke-virtual {v0}, Lcom/peel/content/library/RankCategory;->c()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0}, Lcom/peel/content/library/RankCategory;->b()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v1, v0, v10}, Lcom/peel/ui/kr;->a(Ljava/util/List;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 842
    invoke-virtual {v6}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    move v0, v2

    .line 844
    goto :goto_1

    .line 831
    :cond_2
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    goto :goto_0

    :cond_3
    move v0, v3

    .line 849
    :cond_4
    if-nez v0, :cond_5

    .line 850
    invoke-direct {p0}, Lcom/peel/ui/kr;->U()V

    .line 862
    :goto_2
    sget-object v0, Lcom/peel/ui/kr;->ap:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/peel/ui/kr;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    invoke-static {v0, v1, v3}, Lcom/peel/util/bx;->a(Ljava/lang/String;Landroid/app/Activity;Z)V

    .line 863
    return-void

    .line 852
    :cond_5
    sget-object v0, Lcom/peel/ui/kr;->ap:Ljava/lang/String;

    const-string/jumbo v1, "insights"

    new-instance v2, Lcom/peel/ui/li;

    invoke-direct {v2, p0, v6}, Lcom/peel/ui/li;-><init>(Lcom/peel/ui/kr;Ljava/util/concurrent/atomic/AtomicInteger;)V

    invoke-static {v0, v1, v2}, Lcom/peel/util/i;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/lang/Runnable;

    goto :goto_2
.end method

.method static synthetic m(Lcom/peel/ui/kr;)I
    .locals 1

    .prologue
    .line 73
    iget v0, p0, Lcom/peel/ui/kr;->al:I

    return v0
.end method

.method static synthetic n(Lcom/peel/ui/kr;)I
    .locals 1

    .prologue
    .line 73
    iget v0, p0, Lcom/peel/ui/kr;->am:I

    return v0
.end method

.method static synthetic o(Lcom/peel/ui/kr;)V
    .locals 0

    .prologue
    .line 73
    invoke-direct {p0}, Lcom/peel/ui/kr;->ac()V

    return-void
.end method


# virtual methods
.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    .line 334
    iput-object p1, p0, Lcom/peel/ui/kr;->f:Landroid/view/LayoutInflater;

    .line 336
    sget v0, Lcom/peel/ui/fq;->tops_layout:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/ui/kr;->h:Landroid/view/View;

    .line 337
    iget-object v0, p0, Lcom/peel/ui/kr;->h:Landroid/view/View;

    sget v1, Lcom/peel/ui/fp;->genre_container:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/peel/ui/kr;->g:Landroid/widget/LinearLayout;

    .line 339
    iget-object v0, p0, Lcom/peel/ui/kr;->h:Landroid/view/View;

    sget v1, Lcom/peel/ui/fp;->tops_scroll:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/peel/widget/ObservableScrollView;

    iput-object v0, p0, Lcom/peel/ui/kr;->ak:Lcom/peel/widget/ObservableScrollView;

    .line 340
    iget-object v0, p0, Lcom/peel/ui/kr;->h:Landroid/view/View;

    sget v1, Lcom/peel/ui/fp;->ads_placeholder:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/peel/ui/kr;->an:Landroid/view/ViewGroup;

    .line 341
    iget-object v0, p0, Lcom/peel/ui/kr;->h:Landroid/view/View;

    return-object v0
.end method

.method public a(Landroid/view/View;Lcom/peel/ui/a/p;)V
    .locals 1

    .prologue
    .line 1062
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/peel/ui/kr;->an:Landroid/view/ViewGroup;

    if-eqz v0, :cond_0

    .line 1063
    iget-object v0, p0, Lcom/peel/ui/kr;->an:Landroid/view/ViewGroup;

    invoke-virtual {v0, p1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 1065
    :cond_0
    iput-object p2, p0, Lcom/peel/ui/kr;->aq:Lcom/peel/ui/a/p;

    .line 1066
    return-void
.end method

.method public a(Lcom/peel/widget/ObservableScrollView;IIII)V
    .locals 3

    .prologue
    .line 1042
    iget-object v0, p0, Lcom/peel/ui/kr;->aj:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/peel/ui/kr;->aj:Landroid/view/View;

    const-string/jumbo v1, "overlay"

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewWithTag(Ljava/lang/Object;)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1043
    iget-object v0, p0, Lcom/peel/ui/kr;->aj:Landroid/view/View;

    check-cast v0, Landroid/widget/FrameLayout;

    iget-object v1, p0, Lcom/peel/ui/kr;->aj:Landroid/view/View;

    const-string/jumbo v2, "overlay"

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewWithTag(Ljava/lang/Object;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->removeView(Landroid/view/View;)V

    .line 1046
    :cond_0
    int-to-float v0, p3

    const/high16 v1, 0x43480000    # 200.0f

    invoke-virtual {p0}, Lcom/peel/ui/kr;->m()Landroid/support/v4/app/ae;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/ae;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v2, v2, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v1, v2

    cmpl-float v0, v0, v1

    if-lez v0, :cond_1

    .line 1047
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/peel/ui/kr;->ar:Z

    .line 1050
    :cond_1
    if-nez p3, :cond_2

    .line 1051
    iget-boolean v0, p0, Lcom/peel/ui/kr;->ar:Z

    if-eqz v0, :cond_2

    .line 1052
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/peel/ui/kr;->ar:Z

    .line 1053
    iget-object v0, p0, Lcom/peel/ui/kr;->aq:Lcom/peel/ui/a/p;

    if-eqz v0, :cond_2

    .line 1054
    iget-object v0, p0, Lcom/peel/ui/kr;->aq:Lcom/peel/ui/a/p;

    invoke-interface {v0}, Lcom/peel/ui/a/p;->a()V

    .line 1055
    sget-object v0, Lcom/peel/ui/kr;->ap:Ljava/lang/String;

    const-string/jumbo v1, "refresh banner ad"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1059
    :cond_2
    return-void
.end method

.method public c(Landroid/os/Bundle;)V
    .locals 8

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 708
    invoke-super {p0, p1}, Lcom/peel/d/u;->c(Landroid/os/Bundle;)V

    .line 710
    invoke-virtual {p0}, Lcom/peel/ui/kr;->v()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/peel/ui/kr;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    if-nez v0, :cond_1

    .line 761
    :cond_0
    :goto_0
    return-void

    .line 712
    :cond_1
    const-string/jumbo v0, "selective"

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 713
    iget-object v0, p0, Lcom/peel/ui/kr;->h:Landroid/view/View;

    check-cast v0, Landroid/view/ViewGroup;

    invoke-static {v0}, Lcom/peel/ui/a/v;->a(Landroid/view/ViewGroup;)V

    .line 714
    iget-object v0, p0, Lcom/peel/ui/kr;->c:Landroid/os/Bundle;

    const-string/jumbo v2, "selective"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    goto :goto_0

    .line 718
    :cond_2
    sget-object v0, Lcom/peel/content/a;->h:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/peel/ui/kr;->b:Lcom/peel/d/i;

    if-eqz v0, :cond_0

    .line 720
    iget-object v0, p0, Lcom/peel/ui/kr;->c:Landroid/os/Bundle;

    const-string/jumbo v2, "refresh"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->remove(Ljava/lang/String;)V

    .line 721
    iget-object v0, p0, Lcom/peel/ui/kr;->c:Landroid/os/Bundle;

    const-string/jumbo v2, "time"

    invoke-static {}, Lcom/peel/util/x;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v2, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 722
    iget-object v0, p0, Lcom/peel/ui/kr;->c:Landroid/os/Bundle;

    const-string/jumbo v2, "failed"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 723
    iget-object v0, p0, Lcom/peel/ui/kr;->c:Landroid/os/Bundle;

    const-string/jumbo v2, "failed"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    goto :goto_0

    .line 727
    :cond_3
    invoke-static {}, Lcom/peel/content/a;->a()Lcom/peel/data/ContentRoom;

    move-result-object v4

    .line 730
    iget-object v0, p0, Lcom/peel/ui/kr;->b:Lcom/peel/d/i;

    const-string/jumbo v2, "libraries"

    invoke-virtual {v0, v2}, Lcom/peel/d/i;->e(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v5

    .line 731
    if-eqz v5, :cond_d

    .line 732
    invoke-static {}, Lcom/peel/content/a;->c()[Lcom/peel/content/library/Library;

    move-result-object v6

    array-length v7, v6

    move v2, v1

    move v0, v1

    :goto_1
    if-ge v2, v7, :cond_4

    aget-object v0, v6, v2

    .line 733
    invoke-virtual {v0}, Lcom/peel/content/library/Library;->e()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_6

    move v0, v3

    .line 734
    :goto_2
    if-eqz v0, :cond_7

    .line 739
    :cond_4
    :goto_3
    iget-object v2, p0, Lcom/peel/ui/kr;->g:Landroid/widget/LinearLayout;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/peel/ui/kr;->g:Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 742
    :cond_5
    const-string/jumbo v2, "forcetime"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_8

    move v2, v3

    .line 743
    :goto_4
    if-nez v0, :cond_9

    iget-object v0, p0, Lcom/peel/ui/kr;->c:Landroid/os/Bundle;

    const-string/jumbo v5, "curatedPayload"

    invoke-virtual {v0, v5}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_9

    if-nez v2, :cond_9

    invoke-virtual {v4}, Lcom/peel/data/ContentRoom;->a()Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/peel/ui/kr;->b:Lcom/peel/d/i;

    const-string/jumbo v5, "top_current_room_id"

    invoke-virtual {v2, v5}, Lcom/peel/d/i;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 744
    iget-object v0, p0, Lcom/peel/ui/kr;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "curatedPayload"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/peel/ui/kr;->l(Landroid/os/Bundle;)V

    goto/16 :goto_0

    :cond_6
    move v0, v1

    .line 733
    goto :goto_2

    .line 732
    :cond_7
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_8
    move v2, v1

    .line 742
    goto :goto_4

    .line 746
    :cond_9
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "\n\n main null? "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v0, p0, Lcom/peel/ui/kr;->b:Lcom/peel/d/i;

    if-nez v0, :cond_a

    const-string/jumbo v0, "NULL"

    :goto_5
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 747
    sget-object v2, Lcom/peel/ui/kr;->ap:Ljava/lang/String;

    invoke-static {v2, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 748
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "\n\n room null? "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    if-nez v4, :cond_b

    const-string/jumbo v0, "NULL"

    :goto_6
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 749
    sget-object v2, Lcom/peel/ui/kr;->ap:Ljava/lang/String;

    invoke-static {v2, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 750
    iget-object v0, p0, Lcom/peel/ui/kr;->b:Lcom/peel/d/i;

    const-string/jumbo v2, "top_current_room_id"

    invoke-virtual {v4}, Lcom/peel/data/ContentRoom;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v2, v4}, Lcom/peel/d/i;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 751
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 752
    invoke-static {}, Lcom/peel/content/a;->c()[Lcom/peel/content/library/Library;

    move-result-object v2

    array-length v4, v2

    :goto_7
    if-ge v1, v4, :cond_c

    aget-object v5, v2, v1

    .line 753
    invoke-virtual {v5}, Lcom/peel/content/library/Library;->e()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 752
    add-int/lit8 v1, v1, 0x1

    goto :goto_7

    .line 746
    :cond_a
    const-string/jumbo v0, "NOT NULL"

    goto :goto_5

    .line 748
    :cond_b
    const-string/jumbo v0, "NOT NULL"

    goto :goto_6

    .line 755
    :cond_c
    iget-object v1, p0, Lcom/peel/ui/kr;->b:Lcom/peel/d/i;

    const-string/jumbo v2, "libraries"

    invoke-virtual {v1, v2, v0}, Lcom/peel/d/i;->a(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 758
    sget-object v0, Lcom/peel/ui/kr;->ap:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/peel/ui/kr;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    invoke-static {v0, v1, v3}, Lcom/peel/util/bx;->a(Ljava/lang/String;Landroid/app/Activity;Z)V

    .line 759
    invoke-direct {p0}, Lcom/peel/ui/kr;->V()V

    goto/16 :goto_0

    :cond_d
    move v0, v1

    goto/16 :goto_3
.end method

.method public d(Landroid/os/Bundle;)V
    .locals 7

    .prologue
    .line 364
    invoke-super {p0, p1}, Lcom/peel/d/u;->d(Landroid/os/Bundle;)V

    .line 366
    if-eqz p1, :cond_0

    .line 367
    iget-object v0, p0, Lcom/peel/ui/kr;->c:Landroid/os/Bundle;

    invoke-virtual {v0, p1}, Landroid/os/Bundle;->putAll(Landroid/os/Bundle;)V

    .line 370
    :cond_0
    iget-object v0, p0, Lcom/peel/ui/kr;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "time"

    invoke-static {}, Lcom/peel/util/x;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 371
    sget-object v0, Lcom/peel/content/a;->h:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/peel/ui/kr;->c:Landroid/os/Bundle;

    invoke-virtual {p0, v0}, Lcom/peel/ui/kr;->c(Landroid/os/Bundle;)V

    .line 373
    :cond_1
    iget-object v0, p0, Lcom/peel/ui/kr;->ak:Lcom/peel/widget/ObservableScrollView;

    invoke-virtual {v0, p0}, Lcom/peel/widget/ObservableScrollView;->setOnScrollListener(Lcom/peel/widget/af;)V

    .line 375
    const-string/jumbo v0, "eng"

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Locale;->getISO3Language()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 377
    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v0}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 378
    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v0}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/control/RoomControl;->c()[Lcom/peel/control/a;

    move-result-object v0

    .line 379
    if-eqz v0, :cond_3

    array-length v0, v0

    if-lez v0, :cond_3

    .line 420
    :cond_2
    :goto_0
    return-void

    .line 382
    :cond_3
    invoke-virtual {p0}, Lcom/peel/ui/kr;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 383
    const-string/jumbo v1, "has_show_tutorial"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    .line 385
    if-nez v1, :cond_2

    .line 386
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string/jumbo v2, "has_show_tutorial"

    const/4 v3, 0x1

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 388
    const-string/jumbo v1, "country_ISO"

    const-string/jumbo v2, "US"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "US"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 389
    const-string/jumbo v0, "RemoteSetupTrigger"

    const-string/jumbo v1, "message"

    const-string/jumbo v2, "1"

    const-string/jumbo v3, "REMOTE_SETUP_MESSAGE"

    const-string/jumbo v4, "remoteSetupMessage"

    .line 394
    invoke-virtual {p0}, Lcom/peel/ui/kr;->m()Landroid/support/v4/app/ae;

    move-result-object v5

    new-instance v6, Lcom/peel/ui/la;

    invoke-direct {v6, p0}, Lcom/peel/ui/la;-><init>(Lcom/peel/ui/kr;)V

    .line 389
    invoke-static/range {v0 .. v6}, Lcom/peel/util/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;Lcom/peel/util/t;)V

    .line 412
    const-string/jumbo v0, "remoteSetupStart"

    invoke-virtual {p0}, Lcom/peel/ui/kr;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/peel/util/a;->a(Ljava/lang/String;Landroid/content/Context;)V

    goto :goto_0

    .line 414
    :cond_4
    invoke-direct {p0}, Lcom/peel/ui/kr;->T()V

    goto :goto_0
.end method

.method public e()V
    .locals 4

    .prologue
    .line 327
    invoke-super {p0}, Lcom/peel/d/u;->e()V

    .line 328
    invoke-virtual {p0}, Lcom/peel/ui/kr;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    invoke-static {v0}, Landroid/support/v4/a/q;->a(Landroid/content/Context;)Landroid/support/v4/a/q;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/ui/kr;->au:Landroid/content/BroadcastReceiver;

    new-instance v2, Landroid/content/IntentFilter;

    const-string/jumbo v3, "reminder_updated"

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/a/q;->a(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)V

    .line 329
    invoke-virtual {p0}, Lcom/peel/ui/kr;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    invoke-static {v0}, Landroid/support/v4/a/q;->a(Landroid/content/Context;)Landroid/support/v4/a/q;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/ui/kr;->au:Landroid/content/BroadcastReceiver;

    new-instance v2, Landroid/content/IntentFilter;

    const-string/jumbo v3, "samsung_account"

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/a/q;->a(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)V

    .line 330
    return-void
.end method

.method public e(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 1036
    iget-object v0, p0, Lcom/peel/ui/kr;->c:Landroid/os/Bundle;

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->putAll(Landroid/os/Bundle;)V

    .line 1037
    invoke-super {p0, p1}, Lcom/peel/d/u;->e(Landroid/os/Bundle;)V

    .line 1038
    return-void
.end method

.method public f()V
    .locals 2

    .prologue
    .line 356
    invoke-super {p0}, Lcom/peel/d/u;->f()V

    .line 357
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "dismiss_toast"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 358
    invoke-virtual {p0}, Lcom/peel/ui/kr;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/ae;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/support/v4/a/q;->a(Landroid/content/Context;)Landroid/support/v4/a/q;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/support/v4/a/q;->a(Landroid/content/Intent;)Z

    .line 359
    invoke-virtual {p0}, Lcom/peel/ui/kr;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    invoke-static {v0}, Landroid/support/v4/a/q;->a(Landroid/content/Context;)Landroid/support/v4/a/q;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/ui/kr;->au:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/support/v4/a/q;->a(Landroid/content/BroadcastReceiver;)V

    .line 360
    return-void
.end method

.method public w()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 346
    invoke-super {p0}, Lcom/peel/d/u;->w()V

    .line 347
    sget-object v0, Lcom/peel/util/bx;->d:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 348
    sget-object v0, Lcom/peel/util/bx;->d:Ljava/util/Map;

    sget v1, Lcom/peel/ui/fq;->tunein_overlay:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 349
    sget-object v0, Lcom/peel/util/bx;->d:Ljava/util/Map;

    sget v1, Lcom/peel/ui/fq;->tunein_overlay:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iget-object v2, p0, Lcom/peel/ui/kr;->f:Landroid/view/LayoutInflater;

    sget v3, Lcom/peel/ui/fq;->tunein_overlay:I

    invoke-virtual {v2, v3, v5, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 350
    :cond_0
    sget-object v0, Lcom/peel/util/bx;->d:Ljava/util/Map;

    sget v1, Lcom/peel/ui/fq;->setreminder_overlay:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 351
    sget-object v0, Lcom/peel/util/bx;->d:Ljava/util/Map;

    sget v1, Lcom/peel/ui/fq;->setreminder_overlay:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iget-object v2, p0, Lcom/peel/ui/kr;->f:Landroid/view/LayoutInflater;

    sget v3, Lcom/peel/ui/fq;->setreminder_overlay:I

    invoke-virtual {v2, v3, v5, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 352
    :cond_1
    return-void
.end method
