.class Lcom/peel/ui/ho;
.super Lcom/peel/util/s;


# instance fields
.field final synthetic a:Lcom/peel/ui/gt;


# direct methods
.method constructor <init>(Lcom/peel/ui/gt;)V
    .locals 0

    .prologue
    .line 215
    iput-object p1, p0, Lcom/peel/ui/ho;->a:Lcom/peel/ui/gt;

    invoke-direct {p0}, Lcom/peel/util/s;-><init>()V

    return-void
.end method


# virtual methods
.method public final varargs a(ILjava/lang/Object;[Ljava/lang/Object;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 218
    aget-object v0, p3, v3

    if-nez v0, :cond_1

    .line 251
    :cond_0
    :goto_0
    return-void

    .line 221
    :cond_1
    packed-switch p1, :pswitch_data_0

    goto :goto_0

    .line 242
    :pswitch_0
    aget-object v0, p3, v3

    check-cast v0, Lcom/peel/control/h;

    invoke-virtual {v0}, Lcom/peel/control/h;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 243
    aget-object v0, p3, v3

    check-cast v0, Lcom/peel/control/h;

    invoke-virtual {v0}, Lcom/peel/control/h;->n()V

    .line 244
    aget-object v0, p3, v3

    check-cast v0, Lcom/peel/control/h;

    invoke-virtual {v0}, Lcom/peel/control/h;->b()V

    .line 245
    invoke-static {}, Lcom/peel/ui/gt;->S()Ljava/lang/String;

    move-result-object v1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "device error, attempting to reconnect: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    aget-object v0, p3, v3

    check-cast v0, Lcom/peel/control/h;

    .line 246
    invoke-virtual {v0}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v0

    .line 247
    invoke-virtual {v0}, Lcom/peel/data/g;->f()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 245
    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 224
    :pswitch_1
    aget-object v0, p3, v3

    instance-of v0, v0, Lcom/peel/control/b/b;

    if-eqz v0, :cond_0

    .line 225
    invoke-static {}, Lcom/peel/ui/gt;->S()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "show device not found information for DirecTV"

    new-instance v2, Lcom/peel/ui/hp;

    invoke-direct {v2, p0}, Lcom/peel/ui/hp;-><init>(Lcom/peel/ui/ho;)V

    invoke-static {v0, v1, v2}, Lcom/peel/util/i;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/lang/Runnable;

    goto :goto_0

    .line 221
    :pswitch_data_0
    .packed-switch 0xa
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
