.class Lcom/peel/ui/av;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Landroid/widget/TextView;

.field final synthetic b:[Ljava/lang/String;

.field final synthetic c:Landroid/widget/Button;

.field final synthetic d:Landroid/widget/Button;

.field final synthetic e:Lcom/peel/ui/ar;


# direct methods
.method constructor <init>(Lcom/peel/ui/ar;Landroid/widget/TextView;[Ljava/lang/String;Landroid/widget/Button;Landroid/widget/Button;)V
    .locals 0

    .prologue
    .line 489
    iput-object p1, p0, Lcom/peel/ui/av;->e:Lcom/peel/ui/ar;

    iput-object p2, p0, Lcom/peel/ui/av;->a:Landroid/widget/TextView;

    iput-object p3, p0, Lcom/peel/ui/av;->b:[Ljava/lang/String;

    iput-object p4, p0, Lcom/peel/ui/av;->c:Landroid/widget/Button;

    iput-object p5, p0, Lcom/peel/ui/av;->d:Landroid/widget/Button;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v4, 0x5

    const/4 v3, 0x1

    .line 492
    iget-object v0, p0, Lcom/peel/ui/av;->e:Lcom/peel/ui/ar;

    sget-object v1, Lcom/peel/ui/aw;->a:Lcom/peel/ui/aw;

    invoke-static {v0, v1}, Lcom/peel/ui/ar;->a(Lcom/peel/ui/ar;Lcom/peel/ui/aw;)V

    .line 493
    iget-object v0, p0, Lcom/peel/ui/av;->e:Lcom/peel/ui/ar;

    invoke-static {v0}, Lcom/peel/ui/ar;->b(Lcom/peel/ui/ar;)I

    move-result v0

    if-ge v0, v4, :cond_0

    .line 494
    iget-object v0, p0, Lcom/peel/ui/av;->e:Lcom/peel/ui/ar;

    invoke-static {v0}, Lcom/peel/ui/ar;->e(Lcom/peel/ui/ar;)I

    .line 495
    iget-object v0, p0, Lcom/peel/ui/av;->a:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/peel/ui/av;->b:[Ljava/lang/String;

    iget-object v2, p0, Lcom/peel/ui/av;->e:Lcom/peel/ui/ar;

    invoke-static {v2}, Lcom/peel/ui/ar;->b(Lcom/peel/ui/ar;)I

    move-result v2

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 496
    iget-object v0, p0, Lcom/peel/ui/av;->e:Lcom/peel/ui/ar;

    invoke-static {v0}, Lcom/peel/ui/ar;->b(Lcom/peel/ui/ar;)I

    move-result v0

    if-ne v0, v4, :cond_1

    .line 497
    iget-object v0, p0, Lcom/peel/ui/av;->c:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 501
    :goto_0
    iget-object v0, p0, Lcom/peel/ui/av;->d:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setEnabled(Z)V

    .line 503
    :cond_0
    return-void

    .line 499
    :cond_1
    iget-object v0, p0, Lcom/peel/ui/av;->c:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_0
.end method
