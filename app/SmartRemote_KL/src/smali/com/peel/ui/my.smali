.class Lcom/peel/ui/my;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/peel/ui/nh;

.field final synthetic b:Lcom/peel/ui/mx;


# direct methods
.method constructor <init>(Lcom/peel/ui/mx;Lcom/peel/ui/nh;)V
    .locals 0

    .prologue
    .line 167
    iput-object p1, p0, Lcom/peel/ui/my;->b:Lcom/peel/ui/mx;

    iput-object p2, p0, Lcom/peel/ui/my;->a:Lcom/peel/ui/nh;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 170
    iget-object v0, p0, Lcom/peel/ui/my;->b:Lcom/peel/ui/mx;

    invoke-static {v0}, Lcom/peel/ui/mx;->a(Lcom/peel/ui/mx;)[Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/peel/ui/my;->b:Lcom/peel/ui/mx;

    invoke-static {v0}, Lcom/peel/ui/mx;->a(Lcom/peel/ui/mx;)[Ljava/lang/String;

    move-result-object v0

    array-length v0, v0

    if-lez v0, :cond_0

    .line 171
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 172
    const-string/jumbo v1, "pre_text"

    iget-object v2, p0, Lcom/peel/ui/my;->a:Lcom/peel/ui/nh;

    iget-object v2, v2, Lcom/peel/ui/nh;->c:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 173
    const-string/jumbo v1, "keyword"

    iget-object v2, p0, Lcom/peel/ui/my;->b:Lcom/peel/ui/mx;

    invoke-static {v2}, Lcom/peel/ui/mx;->a(Lcom/peel/ui/mx;)[Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putStringArray(Ljava/lang/String;[Ljava/lang/String;)V

    .line 174
    const-string/jumbo v1, "context_id"

    iget-object v2, p0, Lcom/peel/ui/my;->b:Lcom/peel/ui/mx;

    invoke-static {v2}, Lcom/peel/ui/mx;->b(Lcom/peel/ui/mx;)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 175
    const-string/jumbo v1, "showid"

    iget-object v2, p0, Lcom/peel/ui/my;->b:Lcom/peel/ui/mx;

    invoke-static {v2}, Lcom/peel/ui/mx;->c(Lcom/peel/ui/mx;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 176
    const-string/jumbo v1, "tmsid"

    iget-object v2, p0, Lcom/peel/ui/my;->b:Lcom/peel/ui/mx;

    invoke-static {v2}, Lcom/peel/ui/mx;->d(Lcom/peel/ui/mx;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 177
    iget-object v1, p0, Lcom/peel/ui/my;->b:Lcom/peel/ui/mx;

    invoke-static {v1}, Lcom/peel/ui/mx;->e(Lcom/peel/ui/mx;)Landroid/support/v4/app/ae;

    move-result-object v1

    const-class v2, Lcom/peel/ui/mo;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lcom/peel/d/e;->c(Landroid/support/v4/app/ae;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 182
    :goto_0
    return-void

    .line 180
    :cond_0
    iget-object v0, p0, Lcom/peel/ui/my;->b:Lcom/peel/ui/mx;

    invoke-static {v0}, Lcom/peel/ui/mx;->f(Lcom/peel/ui/mx;)V

    goto :goto_0
.end method
