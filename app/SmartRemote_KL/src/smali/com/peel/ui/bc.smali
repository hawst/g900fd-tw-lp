.class Lcom/peel/ui/bc;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:I

.field final synthetic b:Landroid/view/View;

.field final synthetic c:Ljava/lang/String;

.field final synthetic d:Lcom/peel/ui/az;


# direct methods
.method constructor <init>(Lcom/peel/ui/az;ILandroid/view/View;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 130
    iput-object p1, p0, Lcom/peel/ui/bc;->d:Lcom/peel/ui/az;

    iput p2, p0, Lcom/peel/ui/bc;->a:I

    iput-object p3, p0, Lcom/peel/ui/bc;->b:Landroid/view/View;

    iput-object p4, p0, Lcom/peel/ui/bc;->c:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 132
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 133
    const-string/jumbo v0, "path"

    const-string/jumbo v2, "listing/channel"

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 134
    const-string/jumbo v0, "user"

    sget-object v2, Lcom/peel/content/a;->f:Lcom/peel/content/user/User;

    invoke-virtual {v2}, Lcom/peel/content/user/User;->x()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 135
    const-string/jumbo v0, "library"

    const-string/jumbo v2, "live"

    invoke-static {v2}, Lcom/peel/content/a;->c(Ljava/lang/String;)Lcom/peel/content/library/Library;

    move-result-object v2

    invoke-virtual {v2}, Lcom/peel/content/library/Library;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 136
    const-string/jumbo v0, "start"

    const-wide/16 v2, 0x1

    invoke-virtual {v1, v0, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 137
    const-string/jumbo v0, "window"

    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 138
    const-string/jumbo v0, "room"

    iget-object v2, p0, Lcom/peel/ui/bc;->d:Lcom/peel/ui/az;

    invoke-static {v2}, Lcom/peel/ui/az;->c(Lcom/peel/ui/az;)Lcom/peel/data/ContentRoom;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 139
    const-string/jumbo v0, "cacheWindow"

    invoke-virtual {v1, v0, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 140
    const-string/jumbo v0, "limit"

    invoke-virtual {v1, v0, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 141
    const-string/jumbo v2, "callsign"

    iget-object v0, p0, Lcom/peel/ui/bc;->d:Lcom/peel/ui/az;

    invoke-static {v0}, Lcom/peel/ui/az;->b(Lcom/peel/ui/az;)Ljava/util/List;

    move-result-object v0

    iget v3, p0, Lcom/peel/ui/bc;->a:I

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/data/Channel;

    invoke-virtual {v0}, Lcom/peel/data/Channel;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 143
    const-string/jumbo v0, "live"

    invoke-static {v0}, Lcom/peel/content/a;->c(Ljava/lang/String;)Lcom/peel/content/library/Library;

    move-result-object v0

    new-instance v2, Lcom/peel/ui/bd;

    invoke-direct {v2, p0, v4}, Lcom/peel/ui/bd;-><init>(Lcom/peel/ui/bc;I)V

    invoke-virtual {v0, v1, v2}, Lcom/peel/content/library/Library;->a(Landroid/os/Bundle;Lcom/peel/util/t;)V

    .line 169
    return-void
.end method
