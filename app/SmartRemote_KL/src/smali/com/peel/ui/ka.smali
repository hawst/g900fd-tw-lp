.class public Lcom/peel/ui/ka;
.super Landroid/widget/ArrayAdapter;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<[",
        "Lcom/peel/content/listing/Listing;",
        ">;"
    }
.end annotation


# static fields
.field private static final g:Ljava/lang/String;


# instance fields
.field final a:F

.field final b:F

.field final c:F

.field final d:F

.field final e:F

.field f:F

.field private final h:Landroid/content/Context;

.field private final i:Landroid/content/res/Resources;

.field private final j:Landroid/view/LayoutInflater;

.field private final k:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 31
    const-class v0, Lcom/peel/ui/ka;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/peel/ui/ka;->g:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;ILjava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Ljava/util/List",
            "<[",
            "Lcom/peel/content/listing/Listing;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 52
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 53
    iput-object p1, p0, Lcom/peel/ui/ka;->h:Landroid/content/Context;

    .line 54
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/ui/ka;->j:Landroid/view/LayoutInflater;

    .line 56
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/peel/ui/ka;->k:J

    .line 58
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/ui/ka;->i:Landroid/content/res/Resources;

    .line 60
    iget-object v0, p0, Lcom/peel/ui/ka;->i:Landroid/content/res/Resources;

    sget v1, Lcom/peel/ui/fn;->tile_height:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    iput v0, p0, Lcom/peel/ui/ka;->a:F

    .line 61
    iget-object v0, p0, Lcom/peel/ui/ka;->i:Landroid/content/res/Resources;

    sget v1, Lcom/peel/ui/fn;->tile_width:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    iput v0, p0, Lcom/peel/ui/ka;->b:F

    .line 62
    iget-object v0, p0, Lcom/peel/ui/ka;->i:Landroid/content/res/Resources;

    sget v1, Lcom/peel/ui/fn;->tile_height_actual:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    iput v0, p0, Lcom/peel/ui/ka;->c:F

    .line 63
    iget-object v0, p0, Lcom/peel/ui/ka;->i:Landroid/content/res/Resources;

    sget v1, Lcom/peel/ui/fn;->tile_width:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    iget-object v1, p0, Lcom/peel/ui/ka;->i:Landroid/content/res/Resources;

    sget v2, Lcom/peel/ui/fn;->tile_view_hspace:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    add-float/2addr v0, v1

    iput v0, p0, Lcom/peel/ui/ka;->f:F

    .line 64
    iget-object v0, p0, Lcom/peel/ui/ka;->i:Landroid/content/res/Resources;

    sget v1, Lcom/peel/ui/fn;->progress_height:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    iput v0, p0, Lcom/peel/ui/ka;->d:F

    .line 65
    iget-object v0, p0, Lcom/peel/ui/ka;->i:Landroid/content/res/Resources;

    sget v1, Lcom/peel/ui/fn;->badge_height:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    iput v0, p0, Lcom/peel/ui/ka;->e:F

    .line 67
    return-void
.end method

.method static synthetic a(Lcom/peel/ui/ka;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/peel/ui/ka;->h:Landroid/content/Context;

    return-object v0
.end method


# virtual methods
.method public final getItemId(I)J
    .locals 2

    .prologue
    .line 72
    int-to-long v0, p1

    return-wide v0
.end method

.method public getItemViewType(I)I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 92
    invoke-virtual {p0, p1}, Lcom/peel/ui/ka;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/peel/content/listing/Listing;

    aget-object v0, v0, v1

    .line 93
    const-string/jumbo v2, "peelad"

    invoke-virtual {v0}, Lcom/peel/content/listing/Listing;->k()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v0, v1

    .line 96
    :goto_0
    return v0

    .line 95
    :cond_0
    const-string/jumbo v1, "movie"

    invoke-virtual {v0}, Lcom/peel/content/listing/Listing;->k()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    goto :goto_0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 18

    .prologue
    .line 105
    invoke-virtual/range {p0 .. p1}, Lcom/peel/ui/ka;->getItem(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Lcom/peel/content/listing/Listing;

    .line 106
    invoke-virtual/range {p0 .. p1}, Lcom/peel/ui/ka;->getItemViewType(I)I

    move-result v5

    .line 107
    if-nez p2, :cond_5

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/peel/ui/ka;->j:Landroid/view/LayoutInflater;

    sget v4, Lcom/peel/ui/fq;->tile_view_new:I

    const/4 v6, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v3, v4, v0, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v4

    .line 109
    :goto_0
    const-string/jumbo v3, "overlay"

    invoke-virtual {v4, v3}, Landroid/view/View;->findViewWithTag(Ljava/lang/Object;)Landroid/view/View;

    move-result-object v6

    .line 110
    if-eqz v6, :cond_0

    move-object v3, v4

    .line 111
    check-cast v3, Landroid/widget/FrameLayout;

    invoke-virtual {v3, v6}, Landroid/widget/FrameLayout;->removeView(Landroid/view/View;)V

    .line 114
    :cond_0
    sget v3, Lcom/peel/ui/fp;->image:I

    invoke-virtual {v4, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    .line 115
    const/4 v6, 0x0

    aget-object v6, v2, v6

    invoke-virtual {v6}, Lcom/peel/content/listing/Listing;->l()Ljava/lang/String;

    move-result-object v8

    .line 118
    const/4 v6, 0x0

    aget-object v6, v2, v6

    const/4 v7, 0x1

    if-ne v7, v5, :cond_6

    const/16 v5, 0x2b

    :goto_1
    invoke-static {v6, v5}, Lcom/peel/util/bx;->a(Lcom/peel/content/listing/Listing;I)I

    move-result v7

    .line 120
    const/4 v5, 0x0

    aget-object v5, v2, v5

    check-cast v5, Lcom/peel/content/listing/LiveListing;

    invoke-virtual {v5}, Lcom/peel/content/listing/LiveListing;->y()Ljava/lang/String;

    move-result-object v5

    .line 122
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_15

    .line 123
    const/4 v6, 0x3

    const/4 v9, 0x4

    const/16 v10, 0x10e

    const/4 v5, 0x0

    aget-object v5, v2, v5

    check-cast v5, Lcom/peel/content/listing/LiveListing;

    invoke-virtual {v5}, Lcom/peel/content/listing/LiveListing;->B()Ljava/util/Map;

    move-result-object v5

    invoke-static {v6, v9, v10, v5}, Lcom/peel/util/bx;->a(IIILjava/util/Map;)Ljava/lang/String;

    move-result-object v5

    move-object v6, v5

    .line 126
    :goto_2
    sget-object v5, Lcom/peel/util/a;->e:[I

    if-eqz v5, :cond_1

    sget-object v5, Lcom/peel/util/a;->e:[I

    const/4 v9, 0x0

    aget v5, v5, v9

    const/4 v9, -0x1

    if-eq v5, v9, :cond_1

    sget-object v5, Lcom/peel/util/a;->e:[I

    const/4 v9, 0x1

    aget v5, v5, v9

    const/4 v9, -0x1

    if-eq v5, v9, :cond_1

    .line 127
    invoke-virtual {v4}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v5

    .line 128
    move-object/from16 v0, p0

    iget v9, v0, Lcom/peel/ui/ka;->b:F

    float-to-int v9, v9

    sget-object v10, Lcom/peel/util/a;->e:[I

    const/4 v11, 0x0

    aget v10, v10, v11

    add-int/2addr v9, v10

    iput v9, v5, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 129
    invoke-virtual {v4, v5}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 132
    :cond_1
    sget v5, Lcom/peel/ui/fp;->tile_title_layout:I

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    sget-boolean v5, Lcom/peel/util/a;->f:Z

    if-eqz v5, :cond_7

    const/4 v5, 0x0

    :goto_3
    invoke-virtual {v9, v5}, Landroid/view/View;->setVisibility(I)V

    .line 134
    sget-object v9, Lcom/peel/ui/ka;->g:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v10, "\n####$$$$$$$$$$#### image_url: "

    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v10, ", "

    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v10, "pictures : "

    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const/4 v5, 0x0

    aget-object v5, v2, v5

    check-cast v5, Lcom/peel/content/listing/LiveListing;

    .line 135
    invoke-virtual {v5}, Lcom/peel/content/listing/LiveListing;->z()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v10, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 134
    invoke-static {v9, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 136
    sget v5, Lcom/peel/ui/fp;->caption_altn:I

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    const/16 v9, 0x8

    invoke-virtual {v5, v9}, Landroid/view/View;->setVisibility(I)V

    .line 138
    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_9

    .line 139
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/peel/ui/ka;->h:Landroid/content/Context;

    invoke-static {v5}, Lcom/peel/util/b/e;->a(Landroid/content/Context;)Lcom/squareup/picasso/Picasso;

    move-result-object v5

    .line 140
    invoke-virtual {v5, v6}, Lcom/squareup/picasso/Picasso;->load(Ljava/lang/String;)Lcom/squareup/picasso/RequestCreator;

    move-result-object v6

    sget-boolean v5, Lcom/peel/util/a;->f:Z

    if-eqz v5, :cond_8

    move v5, v7

    .line 141
    :goto_4
    invoke-virtual {v6, v5}, Lcom/squareup/picasso/RequestCreator;->placeholder(I)Lcom/squareup/picasso/RequestCreator;

    move-result-object v5

    .line 142
    invoke-virtual {v5}, Lcom/squareup/picasso/RequestCreator;->noFade()Lcom/squareup/picasso/RequestCreator;

    move-result-object v5

    new-instance v6, Lcom/peel/ui/kb;

    move-object/from16 v0, p0

    invoke-direct {v6, v0, v3, v4, v8}, Lcom/peel/ui/kb;-><init>(Lcom/peel/ui/ka;Landroid/widget/ImageView;Landroid/view/View;Ljava/lang/String;)V

    .line 143
    invoke-virtual {v5, v3, v6}, Lcom/squareup/picasso/RequestCreator;->into(Landroid/widget/ImageView;Lcom/squareup/picasso/Callback;)V

    .line 166
    :cond_2
    :goto_5
    sget v3, Lcom/peel/ui/fp;->caption:I

    invoke-virtual {v4, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    invoke-virtual {v3, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 168
    const/4 v3, 0x0

    aget-object v3, v2, v3

    invoke-static {v3}, Lcom/peel/util/bx;->c(Lcom/peel/content/listing/Listing;)Ljava/lang/String;

    move-result-object v5

    .line 170
    sget v3, Lcom/peel/ui/fp;->badge_txt:I

    invoke-virtual {v4, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 171
    if-eqz v5, :cond_d

    .line 172
    const-string/jumbo v6, "live"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_b

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "("

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/peel/ui/ka;->h:Landroid/content/Context;

    sget v7, Lcom/peel/ui/ft;->tag_live:I

    invoke-virtual {v6, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, ")"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 175
    :cond_3
    :goto_6
    const/4 v5, 0x4

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 180
    :goto_7
    const-wide/16 v6, 0x0

    .line 181
    array-length v9, v2

    const/4 v3, 0x0

    move v8, v3

    :goto_8
    if-ge v8, v9, :cond_f

    aget-object v5, v2, v8

    .line 182
    const-string/jumbo v3, "live"

    invoke-virtual {v5}, Lcom/peel/content/listing/Listing;->n()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v3, v10}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_e

    .line 181
    :cond_4
    add-int/lit8 v3, v8, 0x1

    move v8, v3

    goto :goto_8

    :cond_5
    move-object/from16 v4, p2

    .line 107
    goto/16 :goto_0

    .line 118
    :cond_6
    const/16 v5, 0xa9

    goto/16 :goto_1

    .line 132
    :cond_7
    const/16 v5, 0x8

    goto/16 :goto_3

    .line 140
    :cond_8
    sget v5, Lcom/peel/ui/fo;->genre_placeholder_02:I

    goto/16 :goto_4

    .line 159
    :cond_9
    sget-boolean v5, Lcom/peel/util/a;->f:Z

    if-eqz v5, :cond_a

    :goto_9
    invoke-virtual {v3, v7}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 160
    sget-boolean v3, Lcom/peel/util/a;->f:Z

    if-nez v3, :cond_2

    .line 161
    sget v3, Lcom/peel/ui/fp;->caption_altn:I

    invoke-virtual {v4, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    const/4 v5, 0x0

    invoke-virtual {v3, v5}, Landroid/view/View;->setVisibility(I)V

    .line 162
    sget v3, Lcom/peel/ui/fp;->caption_altn:I

    invoke-virtual {v4, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    invoke-virtual {v3, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_5

    .line 159
    :cond_a
    sget v7, Lcom/peel/ui/fo;->genre_placeholder_02:I

    goto :goto_9

    .line 173
    :cond_b
    const-string/jumbo v6, "premiere"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_c

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "("

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/peel/ui/ka;->h:Landroid/content/Context;

    sget v7, Lcom/peel/ui/ft;->tag_premiere:I

    invoke-virtual {v6, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, ")"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_6

    .line 174
    :cond_c
    const-string/jumbo v6, "new"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "("

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/peel/ui/ka;->h:Landroid/content/Context;

    sget v7, Lcom/peel/ui/ft;->tag_new:I

    invoke-virtual {v6, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, ")"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_6

    .line 177
    :cond_d
    const/4 v5, 0x4

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_7

    .line 184
    :cond_e
    instance-of v3, v5, Lcom/peel/content/listing/LiveListing;

    if-eqz v3, :cond_4

    move-object v3, v5

    .line 185
    check-cast v3, Lcom/peel/content/listing/LiveListing;

    .line 186
    invoke-virtual {v3}, Lcom/peel/content/listing/LiveListing;->o()J

    move-result-wide v10

    .line 188
    invoke-virtual {v5}, Lcom/peel/content/listing/Listing;->j()J

    move-result-wide v12

    .line 190
    move-object/from16 v0, p0

    iget-wide v14, v0, Lcom/peel/ui/ka;->k:J

    cmp-long v3, v14, v10

    if-lez v3, :cond_10

    move-object/from16 v0, p0

    iget-wide v14, v0, Lcom/peel/ui/ka;->k:J

    add-long v16, v10, v12

    cmp-long v3, v14, v16

    if-gez v3, :cond_10

    .line 191
    const-wide/16 v6, 0x64

    move-object/from16 v0, p0

    iget-wide v8, v0, Lcom/peel/ui/ka;->k:J

    sub-long/2addr v8, v10

    mul-long/2addr v6, v8

    div-long/2addr v6, v12

    .line 200
    :cond_f
    :goto_a
    sget-object v3, Lcom/peel/content/a;->e:Ljava/lang/String;

    const-string/jumbo v5, "CN"

    invoke-virtual {v3, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_12

    .line 201
    const/4 v3, 0x0

    aget-object v3, v2, v3

    invoke-static {v3}, Lcom/peel/util/bx;->d(Lcom/peel/content/listing/Listing;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_11

    const/4 v3, 0x1

    .line 206
    :goto_b
    if-eqz v3, :cond_13

    .line 207
    sget v3, Lcom/peel/ui/fp;->reminder_badge:I

    invoke-virtual {v4, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    const/4 v5, 0x0

    invoke-virtual {v3, v5}, Landroid/view/View;->setVisibility(I)V

    .line 212
    :goto_c
    sget v3, Lcom/peel/ui/fp;->progress_bar:I

    invoke-virtual {v4, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    .line 214
    const-wide/16 v8, 0x0

    cmp-long v5, v6, v8

    if-lez v5, :cond_14

    .line 215
    const/high16 v5, 0x42d20000    # 105.0f

    sget v8, Lcom/peel/b/a;->g:F

    mul-float/2addr v5, v8

    long-to-float v6, v6

    mul-float/2addr v5, v6

    const/high16 v6, 0x42c80000    # 100.0f

    div-float/2addr v5, v6

    invoke-static {v5}, Ljava/lang/Math;->round(F)I

    move-result v5

    .line 216
    new-instance v6, Landroid/widget/FrameLayout$LayoutParams;

    const/high16 v7, 0x40000000    # 2.0f

    sget v8, Lcom/peel/b/a;->g:F

    mul-float/2addr v7, v8

    invoke-static {v7}, Ljava/lang/Math;->round(F)I

    move-result v7

    invoke-direct {v6, v5, v7}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v3, v6}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 221
    :goto_d
    const/4 v3, 0x0

    aget-object v2, v2, v3

    invoke-virtual {v2}, Lcom/peel/content/listing/Listing;->f()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 224
    return-object v4

    .line 193
    :cond_10
    move-object/from16 v0, p0

    iget-wide v14, v0, Lcom/peel/ui/ka;->k:J

    add-long/2addr v10, v12

    cmp-long v3, v14, v10

    if-lez v3, :cond_4

    .line 194
    const-wide/16 v6, 0x64

    .line 195
    goto :goto_a

    .line 201
    :cond_11
    const/4 v3, 0x0

    goto :goto_b

    .line 203
    :cond_12
    const/4 v3, 0x0

    aget-object v3, v2, v3

    invoke-static {v3}, Lcom/peel/util/bx;->e(Lcom/peel/content/listing/Listing;)Z

    move-result v3

    goto :goto_b

    .line 209
    :cond_13
    sget v3, Lcom/peel/ui/fp;->reminder_badge:I

    invoke-virtual {v4, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    const/16 v5, 0x8

    invoke-virtual {v3, v5}, Landroid/view/View;->setVisibility(I)V

    goto :goto_c

    .line 218
    :cond_14
    new-instance v5, Landroid/widget/FrameLayout$LayoutParams;

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-direct {v5, v6, v7}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v3, v5}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_d

    :cond_15
    move-object v6, v5

    goto/16 :goto_2
.end method

.method public getViewTypeCount()I
    .locals 1

    .prologue
    .line 82
    const/4 v0, 0x4

    return v0
.end method

.method public isEnabled(I)Z
    .locals 1

    .prologue
    .line 87
    invoke-super {p0}, Landroid/widget/ArrayAdapter;->getCount()I

    move-result v0

    if-ge p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public notifyDataSetChanged()V
    .locals 0

    .prologue
    .line 69
    invoke-super {p0}, Landroid/widget/ArrayAdapter;->notifyDataSetChanged()V

    return-void
.end method
