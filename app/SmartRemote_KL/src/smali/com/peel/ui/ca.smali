.class Lcom/peel/ui/ca;
.super Lcom/peel/util/t;


# instance fields
.field final synthetic a:Lcom/peel/ui/bz;


# direct methods
.method constructor <init>(Lcom/peel/ui/bz;)V
    .locals 0

    .prologue
    .line 543
    iput-object p1, p0, Lcom/peel/ui/ca;->a:Lcom/peel/ui/bz;

    invoke-direct {p0}, Lcom/peel/util/t;-><init>()V

    return-void
.end method


# virtual methods
.method public a(ZLjava/lang/Object;Ljava/lang/String;)V
    .locals 11

    .prologue
    const/4 v10, 0x1

    .line 546
    if-eqz p1, :cond_0

    .line 548
    :try_start_0
    invoke-static {}, Lcom/peel/util/a/f;->a()Lcom/peel/util/a/f;

    move-result-object v0

    sget-object v1, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v1}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v1

    if-nez v1, :cond_1

    move v1, v10

    :goto_0
    const/16 v2, 0x4d1

    iget-object v3, p0, Lcom/peel/ui/ca;->a:Lcom/peel/ui/bz;

    iget-object v3, v3, Lcom/peel/ui/bz;->h:Lcom/peel/ui/by;

    iget-object v3, v3, Lcom/peel/ui/by;->d:Lcom/peel/ui/bw;

    iget v3, v3, Lcom/peel/ui/bw;->f:I

    iget-object v4, p0, Lcom/peel/ui/ca;->a:Lcom/peel/ui/bz;

    iget-object v4, v4, Lcom/peel/ui/bz;->d:Ljava/lang/String;

    iget-object v5, p0, Lcom/peel/ui/ca;->a:Lcom/peel/ui/bz;

    iget v5, v5, Lcom/peel/ui/bz;->c:I

    iget-object v6, p0, Lcom/peel/ui/ca;->a:Lcom/peel/ui/bz;

    iget-object v6, v6, Lcom/peel/ui/bz;->b:Lcom/peel/content/listing/LiveListing;

    .line 550
    invoke-static {v6}, Lcom/peel/util/bx;->b(Lcom/peel/content/listing/Listing;)Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    iget-object v8, p0, Lcom/peel/ui/ca;->a:Lcom/peel/ui/bz;

    iget-object v8, v8, Lcom/peel/ui/bz;->b:Lcom/peel/content/listing/LiveListing;

    invoke-virtual {v8}, Lcom/peel/content/listing/LiveListing;->h()Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x0

    .line 548
    invoke-virtual/range {v0 .. v9}, Lcom/peel/util/a/f;->a(IIILjava/lang/String;ILjava/lang/String;ILjava/lang/String;I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 556
    :goto_1
    iget-object v0, p0, Lcom/peel/ui/ca;->a:Lcom/peel/ui/bz;

    iget-object v0, v0, Lcom/peel/ui/bz;->h:Lcom/peel/ui/by;

    iget-object v0, v0, Lcom/peel/ui/by;->d:Lcom/peel/ui/bw;

    iget-object v0, v0, Lcom/peel/ui/bw;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "selective"

    invoke-virtual {v0, v1, v10}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 557
    iget-object v0, p0, Lcom/peel/ui/ca;->a:Lcom/peel/ui/bz;

    iget-object v0, v0, Lcom/peel/ui/bz;->h:Lcom/peel/ui/by;

    iget-object v0, v0, Lcom/peel/ui/by;->d:Lcom/peel/ui/bw;

    iget-object v1, p0, Lcom/peel/ui/ca;->a:Lcom/peel/ui/bz;

    iget-object v1, v1, Lcom/peel/ui/bz;->h:Lcom/peel/ui/by;

    iget-object v1, v1, Lcom/peel/ui/by;->d:Lcom/peel/ui/bw;

    iget-object v1, v1, Lcom/peel/ui/bw;->c:Landroid/os/Bundle;

    invoke-virtual {v0, v1}, Lcom/peel/ui/bw;->c(Landroid/os/Bundle;)V

    .line 559
    :cond_0
    return-void

    .line 548
    :cond_1
    :try_start_1
    sget-object v1, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v1}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/data/at;->f()I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    move-result v1

    goto :goto_0

    .line 551
    :catch_0
    move-exception v0

    .line 553
    invoke-static {}, Lcom/peel/ui/bw;->c()Ljava/lang/String;

    move-result-object v1

    invoke-static {}, Lcom/peel/ui/bw;->c()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method
