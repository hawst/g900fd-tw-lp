.class public Lcom/peel/ui/aq;
.super Landroid/widget/ArrayAdapter;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lcom/peel/control/a;",
        ">;"
    }
.end annotation


# instance fields
.field private a:Landroid/view/LayoutInflater;

.field private b:Landroid/content/Context;

.field private c:Lcom/peel/control/a;

.field private d:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;ILjava/util/List;Lcom/peel/control/a;Z)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Ljava/util/List",
            "<",
            "Lcom/peel/control/a;",
            ">;",
            "Lcom/peel/control/a;",
            "Z)V"
        }
    .end annotation

    .prologue
    .line 27
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 28
    iput-object p1, p0, Lcom/peel/ui/aq;->b:Landroid/content/Context;

    .line 29
    iput-object p4, p0, Lcom/peel/ui/aq;->c:Lcom/peel/control/a;

    .line 30
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/ui/aq;->a:Landroid/view/LayoutInflater;

    .line 31
    iput-boolean p5, p0, Lcom/peel/ui/aq;->d:Z

    .line 32
    return-void
.end method


# virtual methods
.method public a(I)Lcom/peel/control/a;
    .locals 1

    .prologue
    .line 40
    invoke-super {p0, p1}, Landroid/widget/ArrayAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/control/a;

    return-object v0
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 34
    invoke-super {p0}, Landroid/widget/ArrayAdapter;->getCount()I

    move-result v0

    return v0
.end method

.method public synthetic getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 20
    invoke-virtual {p0, p1}, Lcom/peel/ui/aq;->a(I)Lcom/peel/control/a;

    move-result-object v0

    return-object v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 11

    .prologue
    const/4 v10, 0x0

    const/4 v9, 0x1

    .line 44
    .line 45
    if-eqz p2, :cond_1

    .line 46
    :goto_0
    const v0, 0x1020014

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckedTextView;

    .line 47
    invoke-virtual {p0, p1}, Lcom/peel/ui/aq;->a(I)Lcom/peel/control/a;

    move-result-object v1

    .line 49
    iget-object v2, p0, Lcom/peel/ui/aq;->c:Lcom/peel/control/a;

    if-eqz v2, :cond_2

    invoke-virtual {v1}, Lcom/peel/control/a;->b()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/peel/ui/aq;->c:Lcom/peel/control/a;

    invoke-virtual {v3}, Lcom/peel/control/a;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 50
    invoke-virtual {v0, v9}, Landroid/widget/CheckedTextView;->setChecked(Z)V

    .line 56
    :goto_1
    invoke-virtual {v1}, Lcom/peel/control/a;->d()[Ljava/lang/String;

    move-result-object v2

    .line 59
    invoke-virtual {v1, v9}, Lcom/peel/control/a;->a(I)Lcom/peel/control/h;

    move-result-object v3

    .line 66
    invoke-virtual {v1}, Lcom/peel/control/a;->a()Ljava/lang/String;

    move-result-object v4

    const-string/jumbo v5, "Player"

    const-string/jumbo v6, ""

    invoke-virtual {v4, v5, v6}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 67
    if-eqz v2, :cond_6

    array-length v5, v2

    if-lez v5, :cond_6

    .line 68
    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    .line 69
    const-string/jumbo v5, "nflx"

    invoke-interface {v2, v5}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 70
    iget-object v2, p0, Lcom/peel/ui/aq;->b:Landroid/content/Context;

    sget v5, Lcom/peel/ui/ft;->watch_fmt:I

    new-array v6, v9, [Ljava/lang/Object;

    iget-object v7, p0, Lcom/peel/ui/aq;->b:Landroid/content/Context;

    sget v8, Lcom/peel/ui/ft;->netflix_label:I

    invoke-virtual {v7, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v10

    invoke-virtual {v2, v5, v6}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/CheckedTextView;->setText(Ljava/lang/CharSequence;)V

    .line 83
    :goto_2
    invoke-virtual {v3}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v2

    invoke-virtual {v2}, Lcom/peel/data/g;->d()I

    move-result v2

    const/4 v5, 0x6

    if-ne v2, v5, :cond_7

    .line 84
    iget-object v2, p0, Lcom/peel/ui/aq;->b:Landroid/content/Context;

    sget v4, Lcom/peel/ui/ft;->watch_fmt:I

    new-array v5, v9, [Ljava/lang/Object;

    invoke-virtual {v3}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v3

    invoke-virtual {v3}, Lcom/peel/data/g;->f()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v5, v10

    invoke-virtual {v2, v4, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/CheckedTextView;->setText(Ljava/lang/CharSequence;)V

    .line 89
    :cond_0
    :goto_3
    invoke-virtual {p2, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 90
    return-object p2

    .line 45
    :cond_1
    iget-object v0, p0, Lcom/peel/ui/aq;->a:Landroid/view/LayoutInflater;

    sget v1, Lcom/peel/ui/fq;->controlpad_spinner_item:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    goto/16 :goto_0

    .line 53
    :cond_2
    invoke-virtual {v0, v10}, Landroid/widget/CheckedTextView;->setChecked(Z)V

    goto :goto_1

    .line 71
    :cond_3
    const-string/jumbo v5, "live"

    invoke-interface {v2, v5}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_4

    const-string/jumbo v5, "dtv"

    invoke-interface {v2, v5}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 74
    :cond_4
    iget-object v2, p0, Lcom/peel/ui/aq;->b:Landroid/content/Context;

    sget v5, Lcom/peel/ui/ft;->watch_fmt:I

    new-array v6, v9, [Ljava/lang/Object;

    invoke-virtual {v1}, Lcom/peel/control/a;->a()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v10

    invoke-virtual {v2, v5, v6}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/CheckedTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2

    .line 76
    :cond_5
    iget-object v2, p0, Lcom/peel/ui/aq;->b:Landroid/content/Context;

    sget v5, Lcom/peel/ui/ft;->watch_fmt:I

    new-array v6, v9, [Ljava/lang/Object;

    aput-object v4, v6, v10

    invoke-virtual {v2, v5, v6}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/CheckedTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2

    .line 79
    :cond_6
    iget-object v2, p0, Lcom/peel/ui/aq;->b:Landroid/content/Context;

    sget v5, Lcom/peel/ui/ft;->watch_fmt:I

    new-array v6, v9, [Ljava/lang/Object;

    aput-object v4, v6, v10

    invoke-virtual {v2, v5, v6}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/CheckedTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2

    .line 85
    :cond_7
    invoke-virtual {v3}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v2

    invoke-virtual {v2}, Lcom/peel/data/g;->d()I

    move-result v2

    const/16 v3, 0x12

    if-ne v2, v3, :cond_0

    .line 86
    invoke-virtual {v0, v4}, Landroid/widget/CheckedTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_3
.end method
