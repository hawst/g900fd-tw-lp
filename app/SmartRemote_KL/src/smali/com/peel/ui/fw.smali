.class public Lcom/peel/ui/fw;
.super Lcom/peel/d/u;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/peel/d/u;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    .line 35
    sget v0, Lcom/peel/ui/fq;->load_error:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public d(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 29
    invoke-super {p0, p1}, Lcom/peel/d/u;->d(Landroid/os/Bundle;)V

    .line 30
    iget-object v0, p0, Lcom/peel/ui/fw;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "category"

    sget v2, Lcom/peel/ui/ft;->load_error_title:I

    invoke-virtual {p0, v2}, Lcom/peel/ui/fw;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 32
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v6, 0x0

    .line 39
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    .line 40
    sget v2, Lcom/peel/ui/fp;->reset:I

    if-ne v0, v2, :cond_5

    .line 41
    invoke-virtual {p0}, Lcom/peel/ui/fw;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v2

    .line 43
    invoke-static {}, Lcom/peel/util/a/f;->a()Lcom/peel/util/a/f;

    move-result-object v3

    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v0}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v0

    if-nez v0, :cond_2

    move v0, v1

    :goto_0
    const/16 v4, 0xbd3

    const/16 v5, 0x7d8

    invoke-virtual {v3, v0, v4, v5}, Lcom/peel/util/a/f;->a(III)V

    .line 46
    invoke-virtual {p0}, Lcom/peel/ui/fw;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->clear()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 49
    invoke-virtual {p0}, Lcom/peel/ui/fw;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    const-string/jumbo v3, "widget_pref"

    invoke-virtual {v0, v3, v6}, Landroid/support/v4/app/ae;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string/jumbo v3, "showquickpanel"

    invoke-interface {v0, v3, v6}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 50
    invoke-virtual {p0}, Lcom/peel/ui/fw;->m()Landroid/support/v4/app/ae;

    move-result-object v3

    const-string/jumbo v4, "peel_private"

    invoke-virtual {v3, v4, v6}, Landroid/support/v4/app/ae;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v3

    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    invoke-interface {v3}, Landroid/content/SharedPreferences$Editor;->clear()Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    invoke-interface {v3}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 51
    invoke-virtual {p0}, Lcom/peel/ui/fw;->m()Landroid/support/v4/app/ae;

    move-result-object v3

    const-string/jumbo v4, "widget_pref"

    invoke-virtual {v3, v4, v6}, Landroid/support/v4/app/ae;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v3

    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    invoke-interface {v3}, Landroid/content/SharedPreferences$Editor;->clear()Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    invoke-interface {v3}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 52
    invoke-virtual {p0}, Lcom/peel/ui/fw;->m()Landroid/support/v4/app/ae;

    move-result-object v3

    const-string/jumbo v4, "places_pref"

    invoke-virtual {v3, v4, v6}, Landroid/support/v4/app/ae;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v3

    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    invoke-interface {v3}, Landroid/content/SharedPreferences$Editor;->clear()Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    invoke-interface {v3}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 53
    invoke-virtual {p0}, Lcom/peel/ui/fw;->m()Landroid/support/v4/app/ae;

    move-result-object v3

    const-string/jumbo v4, "network_setup"

    invoke-virtual {v3, v4, v6}, Landroid/support/v4/app/ae;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v3

    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    invoke-interface {v3}, Landroid/content/SharedPreferences$Editor;->clear()Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    invoke-interface {v3}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 55
    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/peel/ui/fw;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    const-string/jumbo v3, "widget_pref"

    invoke-virtual {v0, v3, v6}, Landroid/support/v4/app/ae;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v3, "notification"

    invoke-interface {v0, v3, v1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 56
    :cond_0
    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v1, "lockscreen_enabled"

    invoke-interface {v0, v1, v6}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 57
    invoke-virtual {p0}, Lcom/peel/ui/fw;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    const-string/jumbo v2, "com.android.internal.policy.impl.keyguard.sec.REMOTE_REMOVED"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/support/v4/app/ae;->sendBroadcast(Landroid/content/Intent;)V

    .line 59
    invoke-static {}, Lcom/peel/control/am;->b()V

    .line 60
    invoke-static {}, Lcom/peel/content/a;->b()V

    .line 64
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 65
    sget-object v0, Lcom/peel/c/a;->b:Lcom/peel/c/e;

    invoke-static {v0}, Lcom/peel/c/a;->a(Lcom/peel/c/e;)Ljava/lang/Object;

    move-result-object v0

    sget-object v2, Lcom/peel/c/b;->c:Lcom/peel/c/b;

    if-ne v0, v2, :cond_3

    const-string/jumbo v0, "tv.peel.settings.RESET"

    :goto_1
    invoke-virtual {v1, v0}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 66
    invoke-virtual {p0}, Lcom/peel/ui/fw;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/support/v4/app/ae;->sendBroadcast(Landroid/content/Intent;)V

    .line 68
    iget-object v0, p0, Lcom/peel/ui/fw;->b:Lcom/peel/d/i;

    if-eqz v0, :cond_4

    .line 69
    iget-object v0, p0, Lcom/peel/ui/fw;->b:Lcom/peel/d/i;

    invoke-virtual {v0}, Lcom/peel/d/i;->d()V

    .line 70
    invoke-virtual {p0}, Lcom/peel/ui/fw;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/ui/fw;->b:Lcom/peel/d/i;

    invoke-static {v0, v1}, Lcom/peel/util/bx;->a(Landroid/content/Context;Lcom/peel/d/i;)V

    .line 71
    invoke-virtual {p0}, Lcom/peel/ui/fw;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    const-class v1, Lcom/peel/i/fv;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/peel/d/e;->a(Landroid/support/v4/app/ae;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 81
    :cond_1
    :goto_2
    return-void

    .line 43
    :cond_2
    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v0}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/data/at;->f()I

    move-result v0

    goto/16 :goto_0

    .line 65
    :cond_3
    const-string/jumbo v0, "tv.peel.samsung.settings.RESET"

    goto :goto_1

    .line 73
    :cond_4
    invoke-virtual {p0}, Lcom/peel/ui/fw;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/ae;->finish()V

    goto :goto_2

    .line 75
    :cond_5
    sget v1, Lcom/peel/ui/fp;->report:I

    if-ne v0, v1, :cond_1

    .line 76
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 77
    const-string/jumbo v1, "feedback_subject"

    sget v2, Lcom/peel/ui/ft;->submitabug:I

    invoke-virtual {p0, v2}, Lcom/peel/ui/fw;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 78
    const-string/jumbo v1, "feedback_desc"

    const-string/jumbo v2, ""

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 79
    invoke-virtual {p0}, Lcom/peel/ui/fw;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    const-class v2, Lcom/peel/h/a/du;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lcom/peel/d/e;->c(Landroid/support/v4/app/ae;Ljava/lang/String;Landroid/os/Bundle;)V

    goto :goto_2
.end method
