.class Lcom/peel/ui/kx;
.super Lcom/peel/util/t;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/peel/util/t",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/peel/ui/kr;


# direct methods
.method constructor <init>(Lcom/peel/ui/kr;)V
    .locals 0

    .prologue
    .line 1069
    iput-object p1, p0, Lcom/peel/ui/kx;->a:Lcom/peel/ui/kr;

    invoke-direct {p0}, Lcom/peel/util/t;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .prologue
    const/4 v1, 0x1

    .line 1072
    iget-boolean v0, p0, Lcom/peel/ui/kx;->i:Z

    if-eqz v0, :cond_1

    .line 1073
    new-instance v2, Lcom/peel/backup/c;

    iget-object v0, p0, Lcom/peel/ui/kx;->a:Lcom/peel/ui/kr;

    invoke-virtual {v0}, Lcom/peel/ui/kr;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/ae;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/peel/backup/c;-><init>(Landroid/content/Context;)V

    .line 1074
    invoke-static {}, Lcom/peel/util/a/f;->a()Lcom/peel/util/a/f;

    move-result-object v3

    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v0}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    const/16 v4, 0xbf5

    const/16 v5, 0x7d5

    sget-object v6, Lcom/peel/social/e;->d:Ljava/lang/String;

    invoke-virtual {v3, v0, v4, v5, v6}, Lcom/peel/util/a/f;->a(IIILjava/lang/String;)V

    .line 1075
    invoke-virtual {v2}, Lcom/peel/backup/c;->a()V

    .line 1076
    iget-object v0, p0, Lcom/peel/ui/kx;->a:Lcom/peel/ui/kr;

    invoke-virtual {v0}, Lcom/peel/ui/kr;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    new-instance v2, Lcom/peel/ui/ky;

    invoke-direct {v2, p0}, Lcom/peel/ui/ky;-><init>(Lcom/peel/ui/kx;)V

    invoke-virtual {v0, v2}, Landroid/support/v4/app/ae;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 1082
    iget-object v0, p0, Lcom/peel/ui/kx;->a:Lcom/peel/ui/kr;

    invoke-virtual {v0}, Lcom/peel/ui/kr;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    iget-object v2, p0, Lcom/peel/ui/kx;->k:Ljava/lang/String;

    invoke-static {v0, v2, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 1086
    :goto_1
    return-void

    .line 1074
    :cond_0
    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v0}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/data/at;->f()I

    move-result v0

    goto :goto_0

    .line 1084
    :cond_1
    iget-object v0, p0, Lcom/peel/ui/kx;->a:Lcom/peel/ui/kr;

    invoke-virtual {v0}, Lcom/peel/ui/kr;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    iget-object v2, p0, Lcom/peel/ui/kx;->k:Ljava/lang/String;

    invoke-static {v0, v2, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_1
.end method
