.class Lcom/peel/ui/gn;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field final synthetic a:Lcom/peel/ui/gm;


# direct methods
.method constructor <init>(Lcom/peel/ui/gm;)V
    .locals 0

    .prologue
    .line 71
    iput-object p1, p0, Lcom/peel/ui/gn;->a:Lcom/peel/ui/gm;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 12

    .prologue
    .line 74
    iget-object v0, p0, Lcom/peel/ui/gn;->a:Lcom/peel/ui/gm;

    invoke-virtual {v0}, Lcom/peel/ui/gm;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    const-string/jumbo v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/ae;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 75
    iget-object v1, p0, Lcom/peel/ui/gn;->a:Lcom/peel/ui/gm;

    invoke-static {v1}, Lcom/peel/ui/gm;->a(Lcom/peel/ui/gm;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 77
    invoke-virtual {p1}, Landroid/widget/AdapterView;->getAdapter()Landroid/widget/Adapter;

    move-result-object v0

    check-cast v0, Lcom/peel/ui/kh;

    invoke-virtual {v0, p3}, Lcom/peel/ui/kh;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    move-object v10, v0

    check-cast v10, [Lcom/peel/content/listing/Listing;

    .line 78
    new-instance v11, Landroid/os/Bundle;

    invoke-direct {v11}, Landroid/os/Bundle;-><init>()V

    .line 79
    if-eqz v10, :cond_0

    const-string/jumbo v0, "selected"

    const/4 v1, 0x0

    aget-object v1, v10, v1

    invoke-virtual {v1}, Lcom/peel/content/listing/Listing;->f()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v11, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 81
    :cond_0
    invoke-static {}, Lcom/peel/util/a/f;->a()Lcom/peel/util/a/f;

    move-result-object v0

    sget-object v1, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v1}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v1

    if-nez v1, :cond_1

    const/4 v1, 0x1

    :goto_0
    const/16 v2, 0x4b1

    const/16 v3, 0x7d4

    const-string/jumbo v4, "search"

    const/4 v5, 0x0

    aget-object v5, v10, v5

    .line 83
    invoke-static {v5}, Lcom/peel/util/bx;->b(Lcom/peel/content/listing/Listing;)Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    const/4 v5, 0x0

    aget-object v5, v10, v5

    invoke-virtual {v5}, Lcom/peel/content/listing/Listing;->h()Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x0

    move v5, p3

    .line 81
    invoke-virtual/range {v0 .. v9}, Lcom/peel/util/a/f;->a(IIILjava/lang/String;ILjava/lang/String;ILjava/lang/String;I)V

    .line 85
    const-string/jumbo v0, "live"

    invoke-static {v0}, Lcom/peel/content/a;->c(Ljava/lang/String;)Lcom/peel/content/library/Library;

    move-result-object v0

    .line 86
    const-string/jumbo v1, "libraryIds"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    invoke-virtual {v0}, Lcom/peel/content/library/Library;->e()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v3

    invoke-virtual {v11, v1, v2}, Landroid/os/Bundle;->putStringArray(Ljava/lang/String;[Ljava/lang/String;)V

    .line 87
    const-string/jumbo v0, "context_id"

    const/16 v1, 0x7d4

    invoke-virtual {v11, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 88
    const-string/jumbo v0, "needsgrouping"

    const/4 v1, 0x0

    invoke-virtual {v11, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 89
    const-string/jumbo v0, "category"

    iget-object v1, p0, Lcom/peel/ui/gn;->a:Lcom/peel/ui/gm;

    iget-object v1, v1, Lcom/peel/ui/gm;->c:Landroid/os/Bundle;

    const-string/jumbo v2, "title"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v11, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 90
    const-string/jumbo v0, "id"

    const/4 v1, 0x0

    aget-object v1, v10, v1

    invoke-static {v1}, Lcom/peel/util/bx;->b(Lcom/peel/content/listing/Listing;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v11, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 91
    const-string/jumbo v0, "show_id"

    const/4 v1, 0x0

    aget-object v1, v10, v1

    invoke-static {v1}, Lcom/peel/util/bx;->b(Lcom/peel/content/listing/Listing;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v11, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 92
    iget-object v0, p0, Lcom/peel/ui/gn;->a:Lcom/peel/ui/gm;

    invoke-virtual {v0}, Lcom/peel/ui/gm;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    const-class v1, Lcom/peel/ui/b/av;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v11}, Lcom/peel/d/e;->c(Landroid/support/v4/app/ae;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 93
    return-void

    .line 81
    :cond_1
    sget-object v1, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v1}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/data/at;->f()I

    move-result v1

    goto/16 :goto_0
.end method
