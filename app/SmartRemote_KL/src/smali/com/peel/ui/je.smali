.class Lcom/peel/ui/je;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field final synthetic a:Lcom/peel/ui/jc;


# direct methods
.method constructor <init>(Lcom/peel/ui/jc;)V
    .locals 0

    .prologue
    .line 3399
    iput-object p1, p0, Lcom/peel/ui/je;->a:Lcom/peel/ui/jc;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 3402
    iget-object v0, p0, Lcom/peel/ui/je;->a:Lcom/peel/ui/jc;

    iget-object v0, v0, Lcom/peel/ui/jc;->a:Lcom/peel/ui/gt;

    invoke-static {v0}, Lcom/peel/ui/gt;->Y(Lcom/peel/ui/gt;)Lcom/peel/widget/ag;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/widget/ag;->dismiss()V

    .line 3403
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 3404
    const-string/jumbo v0, "device_type"

    const/4 v2, 0x5

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 3405
    const-string/jumbo v0, "room"

    sget-object v2, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v2}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v2

    invoke-virtual {v2}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v2

    invoke-virtual {v2}, Lcom/peel/data/at;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 3406
    const-string/jumbo v0, "back_to_clazz"

    invoke-static {}, Lcom/peel/ui/gt;->S()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 3407
    const-string/jumbo v0, "from_audio_setup"

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 3408
    const-string/jumbo v0, "activity_id"

    iget-object v2, p0, Lcom/peel/ui/je;->a:Lcom/peel/ui/jc;

    iget-object v2, v2, Lcom/peel/ui/jc;->a:Lcom/peel/ui/gt;

    invoke-static {v2}, Lcom/peel/ui/gt;->a(Lcom/peel/ui/gt;)Lcom/peel/control/a;

    move-result-object v2

    invoke-virtual {v2}, Lcom/peel/control/a;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 3410
    const-string/jumbo v0, "live"

    invoke-static {v0}, Lcom/peel/content/a;->c(Ljava/lang/String;)Lcom/peel/content/library/Library;

    move-result-object v0

    check-cast v0, Lcom/peel/content/library/LiveLibrary;

    .line 3411
    if-eqz v0, :cond_0

    .line 3412
    const-string/jumbo v2, "providername"

    invoke-virtual {v0}, Lcom/peel/content/library/LiveLibrary;->d()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 3415
    :cond_0
    iget-object v0, p0, Lcom/peel/ui/je;->a:Lcom/peel/ui/jc;

    iget-object v0, v0, Lcom/peel/ui/jc;->a:Lcom/peel/ui/gt;

    invoke-virtual {v0}, Lcom/peel/ui/gt;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    const-class v2, Lcom/peel/h/a/a;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2, v1}, Lcom/peel/d/e;->c(Landroid/support/v4/app/ae;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 3416
    return-void
.end method
