.class public final Lcom/peel/ui/fm;
.super Ljava/lang/Object;


# static fields
.field public static final abc_background_cache_hint_selector_material_dark:I = 0x7f0800aa

.field public static final abc_background_cache_hint_selector_material_light:I = 0x7f0800ab

.field public static final abc_input_method_navigation_guard:I = 0x7f080000

.field public static final abc_next_btn_bg:I = 0x7f080001

.field public static final abc_next_btn_bg_disabled:I = 0x7f080002

.field public static final abc_next_btn_text:I = 0x7f080003

.field public static final abc_next_btn_text_disabled:I = 0x7f080004

.field public static final abc_primary_text_disable_only_material_dark:I = 0x7f0800ac

.field public static final abc_primary_text_disable_only_material_light:I = 0x7f0800ad

.field public static final abc_primary_text_material_dark:I = 0x7f0800ae

.field public static final abc_primary_text_material_light:I = 0x7f0800af

.field public static final abc_search_url_text:I = 0x7f0800b0

.field public static final abc_search_url_text_normal:I = 0x7f080005

.field public static final abc_search_url_text_pressed:I = 0x7f080006

.field public static final abc_search_url_text_selected:I = 0x7f080007

.field public static final abc_secondary_text_material_dark:I = 0x7f0800b1

.field public static final abc_secondary_text_material_light:I = 0x7f0800b2

.field public static final accent_material_dark:I = 0x7f080008

.field public static final accent_material_light:I = 0x7f080009

.field public static final action_bar:I = 0x7f08000a

.field public static final app_background:I = 0x7f08000b

.field public static final background_floating_material_dark:I = 0x7f08000c

.field public static final background_floating_material_light:I = 0x7f08000d

.field public static final background_material_dark:I = 0x7f08000e

.field public static final background_material_light:I = 0x7f08000f

.field public static final blue1:I = 0x7f080010

.field public static final blue2:I = 0x7f080011

.field public static final blue3:I = 0x7f080012

.field public static final blue4:I = 0x7f080013

.field public static final bright_foreground_disabled_material_dark:I = 0x7f080014

.field public static final bright_foreground_disabled_material_light:I = 0x7f080015

.field public static final bright_foreground_inverse_material_dark:I = 0x7f080016

.field public static final bright_foreground_inverse_material_light:I = 0x7f080017

.field public static final bright_foreground_material_dark:I = 0x7f080018

.field public static final bright_foreground_material_light:I = 0x7f080019

.field public static final button_material_dark:I = 0x7f08001a

.field public static final button_material_light:I = 0x7f08001b

.field public static final card_backgroud:I = 0x7f08001c

.field public static final com_facebook_blue:I = 0x7f08001d

.field public static final com_facebook_likeboxcountview_border_color:I = 0x7f08001e

.field public static final com_facebook_likeboxcountview_text_color:I = 0x7f08001f

.field public static final com_facebook_likebutton_text_color:I = 0x7f080020

.field public static final com_facebook_likeview_text_color:I = 0x7f080021

.field public static final com_facebook_loginview_text_color:I = 0x7f080022

.field public static final com_facebook_picker_search_bar_background:I = 0x7f080023

.field public static final com_facebook_picker_search_bar_text:I = 0x7f080024

.field public static final com_facebook_usersettingsfragment_connected_shadow_color:I = 0x7f080025

.field public static final com_facebook_usersettingsfragment_connected_text_color:I = 0x7f080026

.field public static final com_facebook_usersettingsfragment_not_connected_text_color:I = 0x7f080027

.field public static final common_action_bar_splitter:I = 0x7f080028

.field public static final common_signin_btn_dark_text_default:I = 0x7f080029

.field public static final common_signin_btn_dark_text_disabled:I = 0x7f08002a

.field public static final common_signin_btn_dark_text_focused:I = 0x7f08002b

.field public static final common_signin_btn_dark_text_pressed:I = 0x7f08002c

.field public static final common_signin_btn_default_background:I = 0x7f08002d

.field public static final common_signin_btn_light_text_default:I = 0x7f08002e

.field public static final common_signin_btn_light_text_disabled:I = 0x7f08002f

.field public static final common_signin_btn_light_text_focused:I = 0x7f080030

.field public static final common_signin_btn_light_text_pressed:I = 0x7f080031

.field public static final common_signin_btn_text_dark:I = 0x7f0800b3

.field public static final common_signin_btn_text_light:I = 0x7f0800b4

.field public static final control_text:I = 0x7f0800b5

.field public static final controlpad_room_state_pressed:I = 0x7f080032

.field public static final country_region_text_normal:I = 0x7f080033

.field public static final country_text_color:I = 0x7f0800b6

.field public static final countrytextcolor_selector:I = 0x7f0800b7

.field public static final dark_gray:I = 0x7f080034

.field public static final dark_gray_1:I = 0x7f080035

.field public static final dark_gray_2:I = 0x7f080036

.field public static final dark_gray_3:I = 0x7f080037

.field public static final default_circle_indicator_fill_color:I = 0x7f080038

.field public static final default_circle_indicator_page_color:I = 0x7f080039

.field public static final default_circle_indicator_stroke_color:I = 0x7f08003a

.field public static final default_line_indicator_selected_color:I = 0x7f08003b

.field public static final default_line_indicator_unselected_color:I = 0x7f08003c

.field public static final default_title_indicator_footer_color:I = 0x7f08003d

.field public static final default_title_indicator_selected_color:I = 0x7f08003e

.field public static final default_title_indicator_text_color:I = 0x7f08003f

.field public static final default_underline_indicator_selected_color:I = 0x7f080040

.field public static final detail_divider:I = 0x7f080041

.field public static final device_press_color:I = 0x7f080042

.field public static final dim_foreground_disabled_material_dark:I = 0x7f080043

.field public static final dim_foreground_disabled_material_light:I = 0x7f080044

.field public static final dim_foreground_material_dark:I = 0x7f080045

.field public static final dim_foreground_material_light:I = 0x7f080046

.field public static final divider_color:I = 0x7f080047

.field public static final edit_channels_list_row_channel_num:I = 0x7f080048

.field public static final favorites_channel_list_background:I = 0x7f080049

.field public static final favorites_channel_list_divider:I = 0x7f08004a

.field public static final favorites_channel_list_row_bg:I = 0x7f08004b

.field public static final favorites_channel_title:I = 0x7f08004c

.field public static final favorites_channel_top_divider:I = 0x7f08004d

.field public static final favorites_channels_edit_list_bg:I = 0x7f08004e

.field public static final favorites_channels_edit_list_divider:I = 0x7f08004f

.field public static final favorites_shows_bg:I = 0x7f080050

.field public static final favorites_shows_grid_bg:I = 0x7f080051

.field public static final favorites_shows_top_list_divider:I = 0x7f080052

.field public static final gray:I = 0x7f080053

.field public static final highlighted_text_material_dark:I = 0x7f080054

.field public static final highlighted_text_material_light:I = 0x7f080055

.field public static final hint_foreground_material_dark:I = 0x7f080056

.field public static final hint_foreground_material_light:I = 0x7f080057

.field public static final light_gray:I = 0x7f080058

.field public static final link_text_material_dark:I = 0x7f080059

.field public static final link_text_material_light:I = 0x7f08005a

.field public static final list_background:I = 0x7f08005b

.field public static final listitem_normal:I = 0x7f08005c

.field public static final listitem_pressed:I = 0x7f08005d

.field public static final listitem_selected:I = 0x7f08005e

.field public static final listview_headercolor:I = 0x7f08005f

.field public static final material_blue_grey_800:I = 0x7f080060

.field public static final material_blue_grey_900:I = 0x7f080061

.field public static final material_blue_grey_950:I = 0x7f080062

.field public static final material_deep_teal_200:I = 0x7f080063

.field public static final material_deep_teal_500:I = 0x7f080064

.field public static final notification_ctrl_header_bg:I = 0x7f080065

.field public static final notification_ctrl_header_device_btn:I = 0x7f080066

.field public static final notification_ctrl_header_hide_btn:I = 0x7f080067

.field public static final notification_widget_bg:I = 0x7f080068

.field public static final notification_widget_collapsed_appname:I = 0x7f080069

.field public static final notification_widget_collapsed_bg:I = 0x7f08006a

.field public static final notification_widget_collapsed_title:I = 0x7f08006b

.field public static final notification_widget_setup_bg:I = 0x7f08006c

.field public static final notification_widget_setup_btn:I = 0x7f08006d

.field public static final notification_widget_setup_img_bg:I = 0x7f08006e

.field public static final now_playing_widget_bg:I = 0x7f08006f

.field public static final now_playing_widget_bg_sdk_21:I = 0x7f080070

.field public static final now_playing_widget_divider:I = 0x7f080071

.field public static final now_playing_widget_divider_sdk_21:I = 0x7f080072

.field public static final now_playing_widget_text:I = 0x7f080073

.field public static final now_playing_widget_text_sdk21:I = 0x7f080074

.field public static final now_playing_widget_text_sdk21_opacity:I = 0x7f080075

.field public static final off_white:I = 0x7f080076

.field public static final popup_bg_color:I = 0x7f080077

.field public static final power_text_color:I = 0x7f0800b8

.field public static final primary_dark_material_dark:I = 0x7f080078

.field public static final primary_dark_material_light:I = 0x7f080079

.field public static final primary_material_dark:I = 0x7f08007a

.field public static final primary_material_light:I = 0x7f08007b

.field public static final primary_text_default_material_dark:I = 0x7f08007c

.field public static final primary_text_default_material_light:I = 0x7f08007d

.field public static final primary_text_disabled_material_dark:I = 0x7f08007e

.field public static final primary_text_disabled_material_light:I = 0x7f08007f

.field public static final red:I = 0x7f080080

.field public static final remote_ctrl_pad_text:I = 0x7f080081

.field public static final ripple_material_dark:I = 0x7f080082

.field public static final ripple_material_light:I = 0x7f080083

.field public static final secondary_text_default_material_dark:I = 0x7f080084

.field public static final secondary_text_default_material_light:I = 0x7f080085

.field public static final secondary_text_disabled_material_dark:I = 0x7f080086

.field public static final secondary_text_disabled_material_light:I = 0x7f080087

.field public static final settings_add_room_divider:I = 0x7f080088

.field public static final settings_add_room_name_header_text:I = 0x7f080089

.field public static final settings_age_gender_privacy_link:I = 0x7f08008a

.field public static final settings_age_gender_privacy_text:I = 0x7f08008b

.field public static final settings_background:I = 0x7f08008c

.field public static final settings_text:I = 0x7f0800b9

.field public static final shadow_color:I = 0x7f08008d

.field public static final show_details_list_item_bg:I = 0x7f08008e

.field public static final show_details_on_demand_header_bg:I = 0x7f08008f

.field public static final show_details_on_demand_header_unknown_season_text:I = 0x7f080090

.field public static final show_details_synopsis_details_bg:I = 0x7f080091

.field public static final showdetails_bg_color:I = 0x7f080092

.field public static final slate_gray:I = 0x7f080093

.field public static final spinner_state_pressed:I = 0x7f080094

.field public static final sports_bg:I = 0x7f080095

.field public static final switch_thumb_normal_material_dark:I = 0x7f080096

.field public static final switch_thumb_normal_material_light:I = 0x7f080097

.field public static final transparent_50:I = 0x7f080098

.field public static final transparent_70:I = 0x7f080099

.field public static final transparent_80:I = 0x7f08009a

.field public static final transparent_yellow:I = 0x7f08009b

.field public static final twitter_bottom_box:I = 0x7f08009c

.field public static final twitter_link_color:I = 0x7f08009d

.field public static final twitter_login_bg:I = 0x7f08009e

.field public static final twitter_user_name_color:I = 0x7f08009f

.field public static final visual_program_guide_bg:I = 0x7f0800a0

.field public static final vpi__background_holo_dark:I = 0x7f0800a1

.field public static final vpi__background_holo_light:I = 0x7f0800a2

.field public static final vpi__bright_foreground_disabled_holo_dark:I = 0x7f0800a3

.field public static final vpi__bright_foreground_disabled_holo_light:I = 0x7f0800a4

.field public static final vpi__bright_foreground_holo_dark:I = 0x7f0800a5

.field public static final vpi__bright_foreground_holo_light:I = 0x7f0800a6

.field public static final vpi__bright_foreground_inverse_holo_dark:I = 0x7f0800a7

.field public static final vpi__bright_foreground_inverse_holo_light:I = 0x7f0800a8

.field public static final vpi__dark_theme:I = 0x7f0800ba

.field public static final vpi__light_theme:I = 0x7f0800bb

.field public static final white:I = 0x7f0800a9
