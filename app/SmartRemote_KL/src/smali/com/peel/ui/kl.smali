.class Lcom/peel/ui/kl;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field final synthetic a:Lcom/peel/ui/kj;


# direct methods
.method constructor <init>(Lcom/peel/ui/kj;)V
    .locals 0

    .prologue
    .line 143
    iput-object p1, p0, Lcom/peel/ui/kl;->a:Lcom/peel/ui/kj;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    const/4 v1, 0x1

    .line 146
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 147
    sget v0, Lcom/peel/ui/kj;->f:I

    sput v0, Lcom/peel/ui/kj;->e:I

    .line 148
    sput p3, Lcom/peel/ui/kj;->f:I

    .line 149
    sget v0, Lcom/peel/ui/fp;->bar:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    .line 150
    iget-object v0, p0, Lcom/peel/ui/kl;->a:Lcom/peel/ui/kj;

    invoke-static {v0}, Lcom/peel/ui/kj;->f(Lcom/peel/ui/kj;)Lcom/peel/ui/kp;

    move-result-object v0

    sget v2, Lcom/peel/ui/kj;->e:I

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/peel/ui/kl;->a:Lcom/peel/ui/kj;

    invoke-static {v4}, Lcom/peel/ui/kj;->e(Lcom/peel/ui/kj;)Landroid/widget/ListView;

    move-result-object v4

    invoke-virtual {v0, v2, v3, v4}, Lcom/peel/ui/kp;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 151
    sget v2, Lcom/peel/ui/fp;->bar:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v2, 0x4

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 152
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 153
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v2

    .line 154
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 155
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string/jumbo v3, "yyyy-MM-dd HH:mm:ss"

    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v0, v3, v4}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 156
    iget-object v3, p0, Lcom/peel/ui/kl;->a:Lcom/peel/ui/kj;

    invoke-virtual {v2}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Lcom/peel/ui/kj;->a(Lcom/peel/ui/kj;Ljava/lang/String;)Ljava/lang/String;

    .line 157
    iget-object v0, p0, Lcom/peel/ui/kl;->a:Lcom/peel/ui/kj;

    iget-object v0, v0, Lcom/peel/ui/kj;->b:Lcom/peel/d/i;

    const-string/jumbo v2, "time"

    iget-object v3, p0, Lcom/peel/ui/kl;->a:Lcom/peel/ui/kj;

    invoke-static {v3}, Lcom/peel/ui/kj;->g(Lcom/peel/ui/kj;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/peel/d/i;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 158
    iget-object v0, p0, Lcom/peel/ui/kl;->a:Lcom/peel/ui/kj;

    iget-object v0, v0, Lcom/peel/ui/kj;->b:Lcom/peel/d/i;

    const-string/jumbo v2, "newTime"

    const-string/jumbo v3, "true"

    invoke-virtual {v0, v2, v3}, Lcom/peel/d/i;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 160
    invoke-static {}, Lcom/peel/util/a/f;->a()Lcom/peel/util/a/f;

    move-result-object v2

    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v0}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v0

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    const/16 v3, 0x400

    const/16 v4, 0x7db

    iget-object v5, p0, Lcom/peel/ui/kl;->a:Lcom/peel/ui/kj;

    invoke-static {v5}, Lcom/peel/ui/kj;->g(Lcom/peel/ui/kj;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v0, v3, v4, v5}, Lcom/peel/util/a/f;->a(IIILjava/lang/String;)V

    .line 162
    iget-object v0, p0, Lcom/peel/ui/kl;->a:Lcom/peel/ui/kj;

    invoke-virtual {v0}, Lcom/peel/ui/kj;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/ae;->invalidateOptionsMenu()V

    .line 163
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 164
    const-string/jumbo v2, "refresh_fragments"

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/String;

    const-class v4, Lcom/peel/ui/ci;

    invoke-virtual {v4}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v6

    const-class v4, Lcom/peel/ui/cg;

    invoke-virtual {v4}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v1

    const/4 v4, 0x2

    const-class v5, Lcom/peel/ui/ch;

    invoke-virtual {v5}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putStringArray(Ljava/lang/String;[Ljava/lang/String;)V

    .line 165
    const-string/jumbo v2, "selective"

    invoke-virtual {v0, v2, v6}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 166
    const-string/jumbo v2, "refresh"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 167
    sput-boolean v1, Lcom/peel/ui/kj;->g:Z

    .line 168
    iget-object v1, p0, Lcom/peel/ui/kl;->a:Lcom/peel/ui/kj;

    invoke-static {v1}, Lcom/peel/ui/kj;->h(Lcom/peel/ui/kj;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/peel/ui/kl;->a:Lcom/peel/ui/kj;

    invoke-virtual {v2}, Lcom/peel/ui/kj;->m()Landroid/support/v4/app/ae;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/peel/d/e;->a(Ljava/lang/String;Landroid/support/v4/app/ae;)V

    .line 169
    iget-object v1, p0, Lcom/peel/ui/kl;->a:Lcom/peel/ui/kj;

    invoke-virtual {v1}, Lcom/peel/ui/kj;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    const-class v2, Lcom/peel/ui/lq;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lcom/peel/d/e;->d(Landroid/support/v4/app/ae;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 171
    :cond_0
    return-void

    .line 160
    :cond_1
    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v0}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/data/at;->f()I

    move-result v0

    goto :goto_0
.end method
