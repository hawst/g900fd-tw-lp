.class Lcom/peel/ui/mg;
.super Landroid/content/BroadcastReceiver;


# instance fields
.field final synthetic a:Lcom/peel/ui/lq;


# direct methods
.method constructor <init>(Lcom/peel/ui/lq;)V
    .locals 0

    .prologue
    .line 432
    iput-object p1, p0, Lcom/peel/ui/mg;->a:Lcom/peel/ui/lq;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 6

    .prologue
    const/4 v3, -0x2

    const/4 v5, 0x0

    .line 435
    if-eqz p2, :cond_0

    .line 436
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 437
    if-nez v0, :cond_1

    .line 478
    :cond_0
    :goto_0
    return-void

    .line 439
    :cond_1
    const-string/jumbo v1, "dismiss_toast"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 440
    iget-object v0, p0, Lcom/peel/ui/mg;->a:Lcom/peel/ui/lq;

    invoke-static {v0}, Lcom/peel/ui/lq;->a(Lcom/peel/ui/lq;)Landroid/widget/PopupWindow;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/peel/ui/mg;->a:Lcom/peel/ui/lq;

    invoke-static {v0}, Lcom/peel/ui/lq;->a(Lcom/peel/ui/lq;)Landroid/widget/PopupWindow;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 441
    iget-object v0, p0, Lcom/peel/ui/mg;->a:Lcom/peel/ui/lq;

    invoke-static {v0}, Lcom/peel/ui/lq;->a(Lcom/peel/ui/lq;)Landroid/widget/PopupWindow;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->dismiss()V

    goto :goto_0

    .line 443
    :cond_2
    const-string/jumbo v1, "show_toast"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 444
    iget-object v0, p0, Lcom/peel/ui/mg;->a:Lcom/peel/ui/lq;

    invoke-static {v0}, Lcom/peel/ui/lq;->l(Lcom/peel/ui/lq;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lcom/peel/ui/fq;->custom_toast:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 446
    iget-object v1, p0, Lcom/peel/ui/mg;->a:Lcom/peel/ui/lq;

    invoke-static {v1}, Lcom/peel/ui/lq;->a(Lcom/peel/ui/lq;)Landroid/widget/PopupWindow;

    move-result-object v1

    if-nez v1, :cond_3

    .line 447
    iget-object v1, p0, Lcom/peel/ui/mg;->a:Lcom/peel/ui/lq;

    new-instance v2, Landroid/widget/PopupWindow;

    invoke-direct {v2, v0}, Landroid/widget/PopupWindow;-><init>(Landroid/view/View;)V

    invoke-static {v1, v2}, Lcom/peel/ui/lq;->a(Lcom/peel/ui/lq;Landroid/widget/PopupWindow;)Landroid/widget/PopupWindow;

    .line 448
    iget-object v1, p0, Lcom/peel/ui/mg;->a:Lcom/peel/ui/lq;

    invoke-static {v1}, Lcom/peel/ui/lq;->a(Lcom/peel/ui/lq;)Landroid/widget/PopupWindow;

    move-result-object v1

    invoke-virtual {v1, v3, v3}, Landroid/widget/PopupWindow;->setWindowLayoutMode(II)V

    .line 449
    iget-object v1, p0, Lcom/peel/ui/mg;->a:Lcom/peel/ui/lq;

    invoke-static {v1}, Lcom/peel/ui/lq;->a(Lcom/peel/ui/lq;)Landroid/widget/PopupWindow;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/widget/PopupWindow;->setOutsideTouchable(Z)V

    .line 452
    :cond_3
    iget-object v1, p0, Lcom/peel/ui/mg;->a:Lcom/peel/ui/lq;

    invoke-static {v1}, Lcom/peel/ui/lq;->a(Lcom/peel/ui/lq;)Landroid/widget/PopupWindow;

    move-result-object v1

    iget-object v2, p0, Lcom/peel/ui/mg;->a:Lcom/peel/ui/lq;

    invoke-static {v2}, Lcom/peel/ui/lq;->m(Lcom/peel/ui/lq;)Landroid/view/View;

    move-result-object v2

    const/16 v3, 0x30

    const/16 v4, 0x96

    invoke-virtual {v1, v2, v3, v5, v4}, Landroid/widget/PopupWindow;->showAtLocation(Landroid/view/View;III)V

    .line 454
    if-eqz v0, :cond_4

    .line 455
    sget v1, Lcom/peel/ui/fp;->close_rel:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/peel/ui/mh;

    invoke-direct {v1, p0}, Lcom/peel/ui/mh;-><init>(Lcom/peel/ui/mg;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 467
    :cond_4
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    .line 468
    new-instance v1, Lcom/peel/ui/mi;

    invoke-direct {v1, p0}, Lcom/peel/ui/mi;-><init>(Lcom/peel/ui/mg;)V

    const-wide/16 v2, 0x7530

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/16 :goto_0
.end method
