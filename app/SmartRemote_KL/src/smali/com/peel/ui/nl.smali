.class public Lcom/peel/ui/nl;
.super Lcom/peel/d/u;


# static fields
.field private static final e:Ljava/lang/String;


# instance fields
.field private final aj:Lcom/peel/ui/kg;

.field private ak:Landroid/content/BroadcastReceiver;

.field private al:Z

.field private f:Landroid/widget/ListView;

.field private g:Landroid/widget/TextView;

.field private h:Ljava/lang/String;

.field private i:Landroid/view/View;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 53
    const-class v0, Lcom/peel/ui/nl;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/peel/ui/nl;->e:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 52
    invoke-direct {p0}, Lcom/peel/d/u;-><init>()V

    .line 56
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/peel/ui/nl;->h:Ljava/lang/String;

    .line 59
    new-instance v0, Lcom/peel/ui/nm;

    invoke-direct {v0, p0}, Lcom/peel/ui/nm;-><init>(Lcom/peel/ui/nl;)V

    iput-object v0, p0, Lcom/peel/ui/nl;->aj:Lcom/peel/ui/kg;

    .line 244
    new-instance v0, Lcom/peel/ui/nr;

    invoke-direct {v0, p0}, Lcom/peel/ui/nr;-><init>(Lcom/peel/ui/nl;)V

    iput-object v0, p0, Lcom/peel/ui/nl;->ak:Landroid/content/BroadcastReceiver;

    .line 329
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/peel/ui/nl;->al:Z

    return-void
.end method

.method static synthetic a(Lcom/peel/ui/nl;Landroid/view/View;)Landroid/view/View;
    .locals 0

    .prologue
    .line 52
    iput-object p1, p0, Lcom/peel/ui/nl;->i:Landroid/view/View;

    return-object p1
.end method

.method static synthetic a(Lcom/peel/ui/nl;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/peel/ui/nl;->h:Ljava/lang/String;

    return-object v0
.end method

.method private a(Lcom/peel/content/listing/Listing;)V
    .locals 4

    .prologue
    .line 269
    check-cast p1, Lcom/peel/content/listing/LiveListing;

    .line 270
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 271
    invoke-virtual {p0}, Lcom/peel/ui/nl;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    invoke-static {v1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    const-string/jumbo v2, "country_ISO"

    const-string/jumbo v3, "US"

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 272
    const-string/jumbo v2, "listing"

    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 273
    invoke-virtual {p1}, Lcom/peel/content/listing/LiveListing;->k()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "sports"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    invoke-virtual {p1}, Lcom/peel/content/listing/LiveListing;->p()Z

    move-result v2

    if-eqz v2, :cond_1

    const-string/jumbo v2, "us"

    .line 274
    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string/jumbo v2, "CA"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 275
    :cond_0
    invoke-virtual {p1}, Lcom/peel/content/listing/LiveListing;->b()Ljava/lang/String;

    move-result-object v1

    .line 277
    const-string/jumbo v2, "remindertype"

    const-string/jumbo v3, "show"

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 278
    const-string/jumbo v2, "showid"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 285
    :goto_0
    const-string/jumbo v1, "live"

    invoke-static {v1}, Lcom/peel/content/a;->c(Ljava/lang/String;)Lcom/peel/content/library/Library;

    move-result-object v1

    .line 286
    const-string/jumbo v2, "path"

    const-string/jumbo v3, "delete/setreminder"

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 287
    const-string/jumbo v2, "id"

    invoke-virtual {p1}, Lcom/peel/content/listing/LiveListing;->f()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 288
    new-instance v2, Lcom/peel/ui/ns;

    invoke-direct {v2, p0}, Lcom/peel/ui/ns;-><init>(Lcom/peel/ui/nl;)V

    invoke-virtual {v1, v0, v2}, Lcom/peel/content/library/Library;->b(Landroid/os/Bundle;Lcom/peel/util/t;)V

    .line 298
    return-void

    .line 280
    :cond_1
    const-string/jumbo v1, "remindertype"

    const-string/jumbo v2, "schedule"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 281
    const-string/jumbo v1, "episodeid"

    invoke-static {p1}, Lcom/peel/util/bx;->b(Lcom/peel/content/listing/Listing;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 282
    const-string/jumbo v1, "scheduletime"

    invoke-virtual {p1}, Lcom/peel/content/listing/LiveListing;->r()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method static synthetic a(Lcom/peel/ui/nl;Lcom/peel/content/listing/Listing;)V
    .locals 0

    .prologue
    .line 52
    invoke-direct {p0, p1}, Lcom/peel/ui/nl;->a(Lcom/peel/content/listing/Listing;)V

    return-void
.end method

.method static synthetic a(Lcom/peel/ui/nl;Ljava/util/List;)V
    .locals 0

    .prologue
    .line 52
    invoke-direct {p0, p1}, Lcom/peel/ui/nl;->a(Ljava/util/List;)V

    return-void
.end method

.method private a(Ljava/util/List;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<[",
            "Lcom/peel/content/listing/Listing;",
            ">;>;>;)V"
        }
    .end annotation

    .prologue
    const/4 v10, -0x1

    const/4 v6, 0x0

    .line 516
    invoke-virtual {p0}, Lcom/peel/ui/nl;->v()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    if-nez p1, :cond_1

    .line 557
    :cond_0
    :goto_0
    return-void

    .line 518
    :cond_1
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 519
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    .line 521
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    .line 522
    iget-object v4, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 523
    iget-object v4, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    invoke-interface {v3, v4, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 526
    :cond_2
    new-instance v5, Landroid/util/LongSparseArray;

    invoke-direct {v5}, Landroid/util/LongSparseArray;-><init>()V

    .line 527
    iget-object v0, p0, Lcom/peel/ui/nl;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "row_states_keys"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 528
    iget-object v0, p0, Lcom/peel/ui/nl;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "row_states_keys"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLongArray(Ljava/lang/String;)[J

    move-result-object v4

    .line 529
    iget-object v0, p0, Lcom/peel/ui/nl;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "row_states_values"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelableArray(Ljava/lang/String;)[Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, [Landroid/os/Bundle;

    check-cast v0, [Landroid/os/Bundle;

    move v1, v6

    .line 530
    :goto_2
    array-length v7, v4

    if-ge v1, v7, :cond_3

    .line 531
    aget-wide v8, v4, v6

    aget-object v7, v0, v6

    invoke-virtual {v5, v8, v9, v7}, Landroid/util/LongSparseArray;->put(JLjava/lang/Object;)V

    .line 530
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 535
    :cond_3
    iget-object v7, p0, Lcom/peel/ui/nl;->f:Landroid/widget/ListView;

    new-instance v0, Lcom/peel/ui/kc;

    invoke-virtual {p0}, Lcom/peel/ui/nl;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    iget-object v4, p0, Lcom/peel/ui/nl;->aj:Lcom/peel/ui/kg;

    invoke-direct/range {v0 .. v5}, Lcom/peel/ui/kc;-><init>(Landroid/content/Context;Ljava/util/List;Ljava/util/Map;Lcom/peel/ui/kg;Landroid/util/LongSparseArray;)V

    invoke-virtual {v7, v0}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 536
    iget-object v0, p0, Lcom/peel/ui/nl;->f:Landroid/widget/ListView;

    new-instance v1, Lcom/peel/ui/nw;

    invoke-direct {v1, p0}, Lcom/peel/ui/nw;-><init>(Lcom/peel/ui/nl;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 552
    iget-object v0, p0, Lcom/peel/ui/nl;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "firstVisible"

    invoke-virtual {v0, v1, v10}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    if-le v0, v10, :cond_0

    .line 553
    iget-object v0, p0, Lcom/peel/ui/nl;->f:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/peel/ui/nl;->c:Landroid/os/Bundle;

    const-string/jumbo v2, "firstVisible"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    iget-object v2, p0, Lcom/peel/ui/nl;->c:Landroid/os/Bundle;

    const-string/jumbo v3, "firstOffset"

    invoke-virtual {v2, v3, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/widget/ListView;->setSelectionFromTop(II)V

    .line 554
    iget-object v0, p0, Lcom/peel/ui/nl;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "firstVisible"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->remove(Ljava/lang/String;)V

    .line 555
    iget-object v0, p0, Lcom/peel/ui/nl;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "firstOffset"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->remove(Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method static synthetic b(Lcom/peel/ui/nl;)Landroid/view/View;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/peel/ui/nl;->i:Landroid/view/View;

    return-object v0
.end method

.method static synthetic c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 52
    sget-object v0, Lcom/peel/ui/nl;->e:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic c(Lcom/peel/ui/nl;)Z
    .locals 1

    .prologue
    .line 52
    iget-boolean v0, p0, Lcom/peel/ui/nl;->al:Z

    return v0
.end method


# virtual methods
.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 302
    sget v0, Lcom/peel/ui/fq;->favorites_shows_layout:I

    invoke-virtual {p1, v0, p2, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 303
    sget v0, Lcom/peel/ui/fp;->list:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/peel/ui/nl;->f:Landroid/widget/ListView;

    .line 304
    const v0, 0x1020004

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/peel/ui/nl;->g:Landroid/widget/TextView;

    .line 305
    iget-object v0, p0, Lcom/peel/ui/nl;->f:Landroid/widget/ListView;

    iget-object v2, p0, Lcom/peel/ui/nl;->g:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setEmptyView(Landroid/view/View;)V

    .line 307
    sget-object v0, Lcom/peel/util/bx;->d:Ljava/util/Map;

    sget v2, Lcom/peel/ui/fq;->tunein_overlay:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 308
    sget-object v0, Lcom/peel/util/bx;->d:Ljava/util/Map;

    sget v2, Lcom/peel/ui/fq;->tunein_overlay:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    sget v3, Lcom/peel/ui/fq;->tunein_overlay:I

    invoke-virtual {p1, v3, v5, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 309
    :cond_0
    sget-object v0, Lcom/peel/util/bx;->d:Ljava/util/Map;

    sget v2, Lcom/peel/ui/fq;->setreminder_overlay:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 310
    sget-object v0, Lcom/peel/util/bx;->d:Ljava/util/Map;

    sget v2, Lcom/peel/ui/fq;->setreminder_overlay:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    sget v3, Lcom/peel/ui/fq;->setreminder_overlay:I

    invoke-virtual {p1, v3, v5, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 312
    :cond_1
    return-object v1
.end method

.method public c(Landroid/os/Bundle;)V
    .locals 14

    .prologue
    const-wide/16 v4, 0x0

    const/4 v8, 0x1

    const/4 v2, 0x0

    .line 333
    sget-object v0, Lcom/peel/content/a;->h:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/peel/ui/nl;->b:Lcom/peel/d/i;

    if-nez v0, :cond_1

    .line 490
    :cond_0
    :goto_0
    return-void

    .line 335
    :cond_1
    const-string/jumbo v0, "selective"

    invoke-virtual {p1, v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 336
    iget-object v0, p0, Lcom/peel/ui/nl;->f:Landroid/widget/ListView;

    invoke-static {v0}, Lcom/peel/ui/a/v;->a(Landroid/view/ViewGroup;)V

    .line 337
    const-string/jumbo v0, "selective"

    invoke-virtual {p1, v0, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    goto :goto_0

    .line 340
    :cond_2
    invoke-super {p0, p1}, Lcom/peel/d/u;->c(Landroid/os/Bundle;)V

    .line 342
    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v0}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 343
    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v0}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/control/RoomControl;->g()[Ljava/lang/String;

    move-result-object v1

    .line 344
    if-eqz v1, :cond_3

    .line 345
    array-length v3, v1

    move v0, v2

    :goto_1
    if-ge v0, v3, :cond_3

    aget-object v6, v1, v0

    .line 346
    const-string/jumbo v7, "live"

    invoke-virtual {v7, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 347
    iput-boolean v8, p0, Lcom/peel/ui/nl;->al:Z

    .line 354
    :cond_3
    const-string/jumbo v0, "refresh"

    invoke-virtual {p1, v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 355
    const-string/jumbo v0, "refresh"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->remove(Ljava/lang/String;)V

    .line 357
    const-string/jumbo v0, "live"

    invoke-static {v0}, Lcom/peel/content/a;->c(Ljava/lang/String;)Lcom/peel/content/library/Library;

    move-result-object v0

    .line 358
    if-eqz v0, :cond_0

    .line 360
    invoke-virtual {v0}, Lcom/peel/content/library/Library;->e()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/peel/ui/nl;->h:Ljava/lang/String;

    .line 363
    iget-object v1, p0, Lcom/peel/ui/nl;->b:Lcom/peel/d/i;

    const-string/jumbo v3, "favcut_refresh"

    invoke-virtual {v1, v3, v2}, Lcom/peel/d/i;->a(Ljava/lang/String;I)I

    move-result v1

    if-ne v8, v1, :cond_4

    .line 364
    iget-object v1, p0, Lcom/peel/ui/nl;->b:Lcom/peel/d/i;

    const-string/jumbo v3, "favcut_refresh"

    invoke-virtual {v1, v3}, Lcom/peel/d/i;->d(Ljava/lang/String;)V

    .line 365
    const-string/jumbo v1, "weekly"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 366
    const-string/jumbo v1, "weekly"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->remove(Ljava/lang/String;)V

    .line 370
    :cond_4
    const-string/jumbo v1, "weekly"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_c

    .line 372
    const-string/jumbo v0, "weekly"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v3

    .line 373
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v7, v0, [Lcom/peel/content/listing/Listing;

    move v1, v2

    .line 374
    :goto_2
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_6

    .line 375
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/content/listing/Listing;

    aput-object v0, v7, v1

    .line 374
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 345
    :cond_5
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 378
    :cond_6
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 379
    new-instance v9, Ljava/util/LinkedHashMap;

    invoke-direct {v9}, Ljava/util/LinkedHashMap;-><init>()V

    .line 380
    new-instance v10, Ljava/util/TreeMap;

    invoke-direct {v10}, Ljava/util/TreeMap;-><init>()V

    .line 382
    array-length v11, v7

    move v6, v2

    :goto_3
    if-ge v6, v11, :cond_9

    aget-object v1, v7, v6

    .line 384
    instance-of v0, v1, Lcom/peel/content/listing/LiveListing;

    if-eqz v0, :cond_d

    move-object v0, v1

    .line 385
    check-cast v0, Lcom/peel/content/listing/LiveListing;

    invoke-virtual {v0}, Lcom/peel/content/listing/LiveListing;->o()J

    move-result-wide v2

    .line 387
    :goto_4
    cmp-long v0, v2, v4

    if-nez v0, :cond_7

    .line 382
    :goto_5
    add-int/lit8 v2, v6, 0x1

    move v6, v2

    goto :goto_3

    .line 389
    :cond_7
    new-instance v12, Ljava/util/GregorianCalendar;

    invoke-direct {v12}, Ljava/util/GregorianCalendar;-><init>()V

    .line 390
    invoke-virtual {v12, v2, v3}, Ljava/util/GregorianCalendar;->setTimeInMillis(J)V

    .line 391
    invoke-static {v12}, Lcom/peel/util/x;->a(Ljava/util/Calendar;)Ljava/lang/String;

    move-result-object v0

    .line 393
    sget-object v2, Lcom/peel/util/x;->n:[Ljava/lang/String;

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_e

    .line 394
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v2, " ("

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/peel/ui/nl;->m()Landroid/support/v4/app/ae;

    move-result-object v2

    invoke-static {v2}, Landroid/text/format/DateFormat;->getMediumDateFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v2

    invoke-virtual {v12}, Ljava/util/GregorianCalendar;->getTime()Ljava/util/Date;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v2, ")"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    move-object v2, v0

    .line 397
    :goto_6
    invoke-interface {v9, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 398
    if-nez v0, :cond_8

    .line 399
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 400
    invoke-interface {v9, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 401
    invoke-virtual {v12}, Ljava/util/GregorianCalendar;->getTimeInMillis()J

    move-result-wide v12

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {v10, v3, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 403
    :cond_8
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_5

    .line 406
    :cond_9
    invoke-interface {v10}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :cond_a
    :goto_7
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_b

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 407
    invoke-interface {v10, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 408
    invoke-interface {v9, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    .line 409
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    const-wide/16 v4, -0x1

    const-class v6, Lcom/peel/content/listing/c;

    invoke-virtual {v6}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-static {}, Lcom/peel/content/a;->a()Lcom/peel/data/ContentRoom;

    move-result-object v7

    invoke-static/range {v1 .. v7}, Lcom/peel/util/bx;->a(Ljava/util/List;JJLjava/lang/String;Lcom/peel/data/ContentRoom;)Ljava/util/List;

    move-result-object v1

    .line 410
    if-eqz v1, :cond_a

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_a

    .line 411
    invoke-static {v0, v1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_7

    .line 415
    :cond_b
    invoke-direct {p0, v8}, Lcom/peel/ui/nl;->a(Ljava/util/List;)V

    goto/16 :goto_0

    .line 417
    :cond_c
    sget-object v1, Lcom/peel/ui/nl;->e:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/peel/ui/nl;->m()Landroid/support/v4/app/ae;

    move-result-object v2

    invoke-static {v1, v2, v8}, Lcom/peel/util/bx;->a(Ljava/lang/String;Landroid/app/Activity;Z)V

    .line 419
    sget-object v1, Lcom/peel/ui/nl;->e:Ljava/lang/String;

    const-string/jumbo v2, "library.get"

    new-instance v3, Lcom/peel/ui/nt;

    invoke-direct {v3, p0, v0, p1}, Lcom/peel/ui/nt;-><init>(Lcom/peel/ui/nl;Lcom/peel/content/library/Library;Landroid/os/Bundle;)V

    invoke-static {v1, v2, v3}, Lcom/peel/util/i;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    goto/16 :goto_0

    :cond_d
    move-wide v2, v4

    goto/16 :goto_4

    :cond_e
    move-object v2, v0

    goto/16 :goto_6
.end method

.method public e(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 561
    iget-object v0, p0, Lcom/peel/ui/nl;->c:Landroid/os/Bundle;

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->putAll(Landroid/os/Bundle;)V

    .line 562
    invoke-super {p0, p1}, Lcom/peel/d/u;->e(Landroid/os/Bundle;)V

    .line 563
    return-void
.end method

.method public g()V
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 494
    iget-object v0, p0, Lcom/peel/ui/nl;->f:Landroid/widget/ListView;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/peel/ui/nl;->f:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 495
    iget-object v0, p0, Lcom/peel/ui/nl;->f:Landroid/widget/ListView;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 496
    iget-object v2, p0, Lcom/peel/ui/nl;->c:Landroid/os/Bundle;

    const-string/jumbo v3, "firstOffset"

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v2, v3, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 497
    iget-object v0, p0, Lcom/peel/ui/nl;->c:Landroid/os/Bundle;

    const-string/jumbo v2, "firstVisible"

    iget-object v3, p0, Lcom/peel/ui/nl;->f:Landroid/widget/ListView;

    invoke-virtual {v3}, Landroid/widget/ListView;->getFirstVisiblePosition()I

    move-result v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 499
    iget-object v0, p0, Lcom/peel/ui/nl;->f:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lcom/peel/ui/kc;

    .line 500
    invoke-virtual {v0}, Lcom/peel/ui/kc;->a()Landroid/util/LongSparseArray;

    move-result-object v2

    .line 501
    invoke-virtual {v2}, Landroid/util/LongSparseArray;->size()I

    move-result v0

    if-lez v0, :cond_2

    .line 502
    invoke-virtual {v2}, Landroid/util/LongSparseArray;->size()I

    move-result v0

    new-array v3, v0, [J

    .line 503
    invoke-virtual {v2}, Landroid/util/LongSparseArray;->size()I

    move-result v0

    new-array v4, v0, [Landroid/os/Bundle;

    .line 504
    :goto_1
    array-length v0, v3

    if-ge v1, v0, :cond_1

    .line 505
    invoke-virtual {v2, v1}, Landroid/util/LongSparseArray;->keyAt(I)J

    move-result-wide v6

    aput-wide v6, v3, v1

    .line 506
    aget-wide v6, v3, v1

    invoke-virtual {v2, v6, v7}, Landroid/util/LongSparseArray;->get(J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    aput-object v0, v4, v1

    .line 504
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 496
    :cond_0
    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v0

    goto :goto_0

    .line 508
    :cond_1
    iget-object v0, p0, Lcom/peel/ui/nl;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "row_states_keys"

    invoke-virtual {v0, v1, v3}, Landroid/os/Bundle;->putLongArray(Ljava/lang/String;[J)V

    .line 509
    iget-object v0, p0, Lcom/peel/ui/nl;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "row_states_values"

    invoke-virtual {v0, v1, v4}, Landroid/os/Bundle;->putParcelableArray(Ljava/lang/String;[Landroid/os/Parcelable;)V

    .line 512
    :cond_2
    invoke-super {p0}, Lcom/peel/d/u;->g()V

    .line 513
    return-void
.end method

.method public h(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 317
    invoke-super {p0, p1}, Lcom/peel/d/u;->h(Landroid/os/Bundle;)V

    .line 319
    if-eqz p1, :cond_0

    .line 320
    iget-object v0, p0, Lcom/peel/ui/nl;->c:Landroid/os/Bundle;

    invoke-virtual {v0, p1}, Landroid/os/Bundle;->putAll(Landroid/os/Bundle;)V

    .line 323
    :cond_0
    sget-object v0, Lcom/peel/content/a;->h:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 324
    iget-object v0, p0, Lcom/peel/ui/nl;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "refresh"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 325
    iget-object v0, p0, Lcom/peel/ui/nl;->c:Landroid/os/Bundle;

    invoke-virtual {p0, v0}, Lcom/peel/ui/nl;->c(Landroid/os/Bundle;)V

    .line 327
    :cond_1
    return-void
.end method

.method public w()V
    .locals 4

    .prologue
    .line 263
    invoke-super {p0}, Lcom/peel/d/u;->w()V

    .line 265
    invoke-virtual {p0}, Lcom/peel/ui/nl;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    invoke-static {v0}, Landroid/support/v4/a/q;->a(Landroid/content/Context;)Landroid/support/v4/a/q;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/ui/nl;->ak:Landroid/content/BroadcastReceiver;

    new-instance v2, Landroid/content/IntentFilter;

    const-string/jumbo v3, "reminder_updated"

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/a/q;->a(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)V

    .line 266
    return-void
.end method

.method public x()V
    .locals 2

    .prologue
    .line 257
    invoke-super {p0}, Lcom/peel/d/u;->x()V

    .line 258
    invoke-virtual {p0}, Lcom/peel/ui/nl;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    invoke-static {v0}, Landroid/support/v4/a/q;->a(Landroid/content/Context;)Landroid/support/v4/a/q;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/ui/nl;->ak:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/support/v4/a/q;->a(Landroid/content/BroadcastReceiver;)V

    .line 259
    return-void
.end method
