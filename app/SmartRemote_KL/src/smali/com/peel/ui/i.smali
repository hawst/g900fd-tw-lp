.class Lcom/peel/ui/i;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/peel/ui/f;


# direct methods
.method constructor <init>(Lcom/peel/ui/f;)V
    .locals 0

    .prologue
    .line 172
    iput-object p1, p0, Lcom/peel/ui/i;->a:Lcom/peel/ui/f;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    .line 175
    invoke-static {}, Lcom/peel/content/a;->a()Lcom/peel/data/ContentRoom;

    move-result-object v0

    .line 176
    const-string/jumbo v1, "live"

    invoke-static {v1}, Lcom/peel/content/a;->c(Ljava/lang/String;)Lcom/peel/content/library/Library;

    move-result-object v1

    .line 177
    if-eqz v0, :cond_0

    if-nez v1, :cond_1

    .line 185
    :cond_0
    :goto_0
    return-void

    .line 180
    :cond_1
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 181
    const-string/jumbo v3, "room"

    invoke-virtual {v2, v3, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 182
    const-string/jumbo v0, "library"

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 183
    iget-object v0, p0, Lcom/peel/ui/i;->a:Lcom/peel/ui/f;

    invoke-virtual {v0}, Lcom/peel/ui/f;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    const-class v1, Lcom/peel/h/a/bb;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v2}, Lcom/peel/d/e;->b(Landroid/support/v4/app/ae;Ljava/lang/String;Landroid/os/Bundle;)V

    goto :goto_0
.end method
