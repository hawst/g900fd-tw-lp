.class Lcom/peel/ui/lc;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/content/DialogInterface$OnShowListener;


# instance fields
.field final synthetic a:Lcom/peel/ui/kr;


# direct methods
.method constructor <init>(Lcom/peel/ui/kr;)V
    .locals 0

    .prologue
    .line 451
    iput-object p1, p0, Lcom/peel/ui/lc;->a:Lcom/peel/ui/kr;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onShow(Landroid/content/DialogInterface;)V
    .locals 2

    .prologue
    .line 454
    iget-object v0, p0, Lcom/peel/ui/lc;->a:Lcom/peel/ui/kr;

    invoke-virtual {v0}, Lcom/peel/ui/kr;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    const-string/jumbo v1, "edit_channel_popup_trigger"

    invoke-static {v0, v1}, Lcom/peel/util/dq;->b(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 455
    iget-object v0, p0, Lcom/peel/ui/lc;->a:Lcom/peel/ui/kr;

    invoke-static {v0}, Lcom/peel/ui/kr;->b(Lcom/peel/ui/kr;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getRootView()Landroid/view/View;

    move-result-object v0

    sget v1, Lcom/peel/ui/fp;->triggers_placeholder:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 456
    if-eqz v0, :cond_0

    .line 457
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 460
    :cond_0
    return-void
.end method
