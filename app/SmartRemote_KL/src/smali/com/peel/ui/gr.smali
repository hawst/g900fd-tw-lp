.class Lcom/peel/ui/gr;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Z

.field final synthetic b:Ljava/util/List;

.field final synthetic c:Lcom/peel/ui/gq;


# direct methods
.method constructor <init>(Lcom/peel/ui/gq;ZLjava/util/List;)V
    .locals 0

    .prologue
    .line 159
    iput-object p1, p0, Lcom/peel/ui/gr;->c:Lcom/peel/ui/gq;

    iput-boolean p2, p0, Lcom/peel/ui/gr;->a:Z

    iput-object p3, p0, Lcom/peel/ui/gr;->b:Ljava/util/List;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 162
    iget-boolean v0, p0, Lcom/peel/ui/gr;->a:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/peel/ui/gr;->b:Ljava/util/List;

    if-eqz v0, :cond_2

    .line 163
    iget-object v0, p0, Lcom/peel/ui/gr;->c:Lcom/peel/ui/gq;

    iget-object v0, v0, Lcom/peel/ui/gq;->a:Lcom/peel/ui/gp;

    iget-object v0, v0, Lcom/peel/ui/gp;->a:Lcom/peel/ui/go;

    iget-object v0, v0, Lcom/peel/ui/go;->b:Lcom/peel/ui/gm;

    invoke-virtual {v0}, Lcom/peel/ui/gm;->s()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/peel/ui/gr;->b:Ljava/util/List;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/peel/ui/gr;->b:Ljava/util/List;

    .line 164
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_2

    .line 165
    iget-object v0, p0, Lcom/peel/ui/gr;->c:Lcom/peel/ui/gq;

    iget-object v0, v0, Lcom/peel/ui/gq;->a:Lcom/peel/ui/gp;

    iget-object v0, v0, Lcom/peel/ui/gp;->a:Lcom/peel/ui/go;

    iget-object v0, v0, Lcom/peel/ui/go;->b:Lcom/peel/ui/gm;

    iget-object v1, p0, Lcom/peel/ui/gr;->b:Ljava/util/List;

    invoke-static {v0, v1}, Lcom/peel/ui/gm;->a(Lcom/peel/ui/gm;Ljava/util/List;)Ljava/util/List;

    .line 166
    iget-object v0, p0, Lcom/peel/ui/gr;->c:Lcom/peel/ui/gq;

    iget-object v0, v0, Lcom/peel/ui/gq;->a:Lcom/peel/ui/gp;

    iget-object v0, v0, Lcom/peel/ui/gp;->a:Lcom/peel/ui/go;

    iget-object v0, v0, Lcom/peel/ui/go;->b:Lcom/peel/ui/gm;

    .line 167
    invoke-static {v0}, Lcom/peel/ui/gm;->e(Lcom/peel/ui/gm;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x1

    filled-new-array {v0, v1}, [I

    move-result-object v0

    const-class v1, Lcom/peel/content/listing/Listing;

    invoke-static {v1, v0}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [[Lcom/peel/content/listing/Listing;

    move v2, v3

    .line 168
    :goto_0
    iget-object v1, p0, Lcom/peel/ui/gr;->c:Lcom/peel/ui/gq;

    iget-object v1, v1, Lcom/peel/ui/gq;->a:Lcom/peel/ui/gp;

    iget-object v1, v1, Lcom/peel/ui/gp;->a:Lcom/peel/ui/go;

    iget-object v1, v1, Lcom/peel/ui/go;->b:Lcom/peel/ui/gm;

    invoke-static {v1}, Lcom/peel/ui/gm;->e(Lcom/peel/ui/gm;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v2, v1, :cond_0

    .line 169
    aget-object v4, v0, v2

    iget-object v1, p0, Lcom/peel/ui/gr;->c:Lcom/peel/ui/gq;

    iget-object v1, v1, Lcom/peel/ui/gq;->a:Lcom/peel/ui/gp;

    iget-object v1, v1, Lcom/peel/ui/gp;->a:Lcom/peel/ui/go;

    iget-object v1, v1, Lcom/peel/ui/go;->b:Lcom/peel/ui/gm;

    invoke-static {v1}, Lcom/peel/ui/gm;->e(Lcom/peel/ui/gm;)Ljava/util/List;

    move-result-object v1

    .line 170
    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/peel/content/listing/Listing;

    aput-object v1, v4, v3

    .line 168
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    .line 172
    :cond_0
    iget-object v1, p0, Lcom/peel/ui/gr;->c:Lcom/peel/ui/gq;

    iget-object v1, v1, Lcom/peel/ui/gq;->a:Lcom/peel/ui/gp;

    iget-object v1, v1, Lcom/peel/ui/gp;->a:Lcom/peel/ui/go;

    iget-object v1, v1, Lcom/peel/ui/go;->b:Lcom/peel/ui/gm;

    invoke-static {v1}, Lcom/peel/ui/gm;->f(Lcom/peel/ui/gm;)Landroid/widget/ListView;

    move-result-object v1

    new-instance v2, Lcom/peel/ui/kh;

    iget-object v4, p0, Lcom/peel/ui/gr;->c:Lcom/peel/ui/gq;

    iget-object v4, v4, Lcom/peel/ui/gq;->a:Lcom/peel/ui/gp;

    iget-object v4, v4, Lcom/peel/ui/gp;->a:Lcom/peel/ui/go;

    iget-object v4, v4, Lcom/peel/ui/go;->b:Lcom/peel/ui/gm;

    invoke-virtual {v4}, Lcom/peel/ui/gm;->m()Landroid/support/v4/app/ae;

    move-result-object v4

    const/4 v5, -0x1

    invoke-direct {v2, v4, v5, v0}, Lcom/peel/ui/kh;-><init>(Landroid/content/Context;I[[Lcom/peel/content/listing/Listing;)V

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 173
    iget-object v0, p0, Lcom/peel/ui/gr;->c:Lcom/peel/ui/gq;

    iget-object v0, v0, Lcom/peel/ui/gq;->a:Lcom/peel/ui/gp;

    iget-object v0, v0, Lcom/peel/ui/gp;->a:Lcom/peel/ui/go;

    iget-object v0, v0, Lcom/peel/ui/go;->b:Lcom/peel/ui/gm;

    iget-object v0, v0, Lcom/peel/ui/gm;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "grid_state"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 174
    iget-object v0, p0, Lcom/peel/ui/gr;->c:Lcom/peel/ui/gq;

    iget-object v0, v0, Lcom/peel/ui/gq;->a:Lcom/peel/ui/gp;

    iget-object v0, v0, Lcom/peel/ui/gp;->a:Lcom/peel/ui/go;

    iget-object v0, v0, Lcom/peel/ui/go;->b:Lcom/peel/ui/gm;

    invoke-static {v0}, Lcom/peel/ui/gm;->f(Lcom/peel/ui/gm;)Landroid/widget/ListView;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/ui/gr;->c:Lcom/peel/ui/gq;

    iget-object v1, v1, Lcom/peel/ui/gq;->a:Lcom/peel/ui/gp;

    iget-object v1, v1, Lcom/peel/ui/gp;->a:Lcom/peel/ui/go;

    iget-object v1, v1, Lcom/peel/ui/go;->b:Lcom/peel/ui/gm;

    iget-object v1, v1, Lcom/peel/ui/gm;->c:Landroid/os/Bundle;

    const-string/jumbo v2, "grid_state"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 175
    :cond_1
    iget-object v0, p0, Lcom/peel/ui/gr;->c:Lcom/peel/ui/gq;

    iget-object v0, v0, Lcom/peel/ui/gq;->a:Lcom/peel/ui/gp;

    iget-object v0, v0, Lcom/peel/ui/gp;->a:Lcom/peel/ui/go;

    iget-object v0, v0, Lcom/peel/ui/go;->b:Lcom/peel/ui/gm;

    invoke-static {v0}, Lcom/peel/ui/gm;->g(Lcom/peel/ui/gm;)Landroid/widget/TextView;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 176
    iget-object v0, p0, Lcom/peel/ui/gr;->c:Lcom/peel/ui/gq;

    iget-object v0, v0, Lcom/peel/ui/gq;->a:Lcom/peel/ui/gp;

    iget-object v0, v0, Lcom/peel/ui/gp;->a:Lcom/peel/ui/go;

    iget-object v0, v0, Lcom/peel/ui/go;->b:Lcom/peel/ui/gm;

    invoke-static {v0}, Lcom/peel/ui/gm;->h(Lcom/peel/ui/gm;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 180
    :cond_2
    iget-object v0, p0, Lcom/peel/ui/gr;->c:Lcom/peel/ui/gq;

    iget-object v0, v0, Lcom/peel/ui/gq;->a:Lcom/peel/ui/gp;

    iget-object v0, v0, Lcom/peel/ui/gp;->a:Lcom/peel/ui/go;

    iget-object v0, v0, Lcom/peel/ui/go;->b:Lcom/peel/ui/gm;

    invoke-static {v0}, Lcom/peel/ui/gm;->c(Lcom/peel/ui/gm;)V

    .line 181
    return-void
.end method
