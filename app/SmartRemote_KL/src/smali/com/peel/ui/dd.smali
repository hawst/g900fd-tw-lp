.class Lcom/peel/ui/dd;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field final synthetic a:Lcom/peel/ui/cj;

.field private b:Ljava/util/concurrent/Future;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/Future",
            "<*>;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/peel/ui/cj;)V
    .locals 1

    .prologue
    .line 301
    iput-object p1, p0, Lcom/peel/ui/dd;->a:Lcom/peel/ui/cj;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 302
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/peel/ui/dd;->b:Ljava/util/concurrent/Future;

    return-void
.end method

.method static synthetic a(Lcom/peel/ui/dd;Ljava/util/concurrent/Future;)Ljava/util/concurrent/Future;
    .locals 0

    .prologue
    .line 301
    iput-object p1, p0, Lcom/peel/ui/dd;->b:Ljava/util/concurrent/Future;

    return-object p1
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 0

    .prologue
    .line 376
    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 2

    .prologue
    .line 305
    iget-object v0, p0, Lcom/peel/ui/dd;->a:Lcom/peel/ui/cj;

    invoke-static {v0}, Lcom/peel/ui/cj;->c(Lcom/peel/ui/cj;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 306
    iget-object v0, p0, Lcom/peel/ui/dd;->a:Lcom/peel/ui/cj;

    invoke-static {v0}, Lcom/peel/ui/cj;->b(Lcom/peel/ui/cj;)Landroid/widget/ImageView;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 309
    iget-object v0, p0, Lcom/peel/ui/dd;->a:Lcom/peel/ui/cj;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/peel/ui/cj;->a(Lcom/peel/ui/cj;Z)Z

    .line 311
    :cond_0
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x4

    const/4 v2, 0x0

    .line 314
    iget-object v0, p0, Lcom/peel/ui/dd;->a:Lcom/peel/ui/cj;

    invoke-static {v0, v2}, Lcom/peel/ui/cj;->b(Lcom/peel/ui/cj;Z)Z

    .line 315
    iget-object v0, p0, Lcom/peel/ui/dd;->a:Lcom/peel/ui/cj;

    invoke-static {v0}, Lcom/peel/ui/cj;->a(Lcom/peel/ui/cj;)Landroid/widget/AutoCompleteTextView;

    move-result-object v0

    if-nez v0, :cond_0

    .line 373
    :goto_0
    return-void

    .line 318
    :cond_0
    iget-object v0, p0, Lcom/peel/ui/dd;->b:Ljava/util/concurrent/Future;

    if-eqz v0, :cond_1

    .line 319
    iget-object v0, p0, Lcom/peel/ui/dd;->b:Ljava/util/concurrent/Future;

    invoke-interface {v0, v3}, Ljava/util/concurrent/Future;->cancel(Z)Z

    .line 322
    :cond_1
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-lez v0, :cond_3

    .line 323
    iget-object v0, p0, Lcom/peel/ui/dd;->a:Lcom/peel/ui/cj;

    invoke-static {v0}, Lcom/peel/ui/cj;->b(Lcom/peel/ui/cj;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 324
    iget-object v0, p0, Lcom/peel/ui/dd;->a:Lcom/peel/ui/cj;

    invoke-static {v0}, Lcom/peel/ui/cj;->d(Lcom/peel/ui/cj;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ImageView;->getVisibility()I

    move-result v0

    if-ne v0, v1, :cond_2

    .line 325
    iget-object v0, p0, Lcom/peel/ui/dd;->a:Lcom/peel/ui/cj;

    invoke-static {v0}, Lcom/peel/ui/cj;->d(Lcom/peel/ui/cj;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 328
    :cond_2
    iget-object v0, p0, Lcom/peel/ui/dd;->a:Lcom/peel/ui/cj;

    iget-object v0, v0, Lcom/peel/ui/cj;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "keyword"

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 329
    const-class v0, Lcom/peel/ui/cj;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "quick/hint searching..."

    new-instance v2, Lcom/peel/ui/de;

    invoke-direct {v2, p0, p1}, Lcom/peel/ui/de;-><init>(Lcom/peel/ui/dd;Ljava/lang/CharSequence;)V

    invoke-static {v0, v1, v2}, Lcom/peel/util/i;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/lang/Runnable;

    goto :goto_0

    .line 362
    :cond_3
    iget-object v0, p0, Lcom/peel/ui/dd;->a:Lcom/peel/ui/cj;

    invoke-static {v0}, Lcom/peel/ui/cj;->d(Lcom/peel/ui/cj;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ImageView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_4

    .line 363
    iget-object v0, p0, Lcom/peel/ui/dd;->a:Lcom/peel/ui/cj;

    invoke-static {v0}, Lcom/peel/ui/cj;->d(Lcom/peel/ui/cj;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 365
    :cond_4
    sget-object v0, Lcom/peel/c/a;->b:Lcom/peel/c/e;

    invoke-static {v0}, Lcom/peel/c/a;->a(Lcom/peel/c/e;)Ljava/lang/Object;

    move-result-object v0

    sget-object v1, Lcom/peel/c/b;->c:Lcom/peel/c/b;

    if-eq v0, v1, :cond_5

    .line 366
    iget-object v0, p0, Lcom/peel/ui/dd;->a:Lcom/peel/ui/cj;

    invoke-static {v0}, Lcom/peel/ui/cj;->b(Lcom/peel/ui/cj;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 367
    :cond_5
    iget-object v0, p0, Lcom/peel/ui/dd;->a:Lcom/peel/ui/cj;

    iget-object v0, v0, Lcom/peel/ui/cj;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "keyword"

    const-string/jumbo v2, ""

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 370
    iget-object v0, p0, Lcom/peel/ui/dd;->a:Lcom/peel/ui/cj;

    invoke-static {v0, v3}, Lcom/peel/ui/cj;->a(Lcom/peel/ui/cj;Z)Z

    .line 371
    iget-object v0, p0, Lcom/peel/ui/dd;->a:Lcom/peel/ui/cj;

    invoke-static {v0}, Lcom/peel/ui/cj;->a(Lcom/peel/ui/cj;)Landroid/widget/AutoCompleteTextView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/AutoCompleteTextView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lcom/peel/ui/gk;

    invoke-virtual {v0}, Lcom/peel/ui/gk;->clear()V

    goto/16 :goto_0
.end method
