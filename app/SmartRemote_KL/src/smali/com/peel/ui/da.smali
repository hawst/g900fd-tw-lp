.class Lcom/peel/ui/da;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/peel/ui/cj;


# direct methods
.method constructor <init>(Lcom/peel/ui/cj;)V
    .locals 0

    .prologue
    .line 250
    iput-object p1, p0, Lcom/peel/ui/da;->a:Lcom/peel/ui/cj;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 253
    iget-object v0, p0, Lcom/peel/ui/da;->a:Lcom/peel/ui/cj;

    invoke-static {v0}, Lcom/peel/ui/cj;->a(Lcom/peel/ui/cj;)Landroid/widget/AutoCompleteTextView;

    move-result-object v0

    const-string/jumbo v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/AutoCompleteTextView;->setText(Ljava/lang/CharSequence;)V

    .line 254
    iget-object v0, p0, Lcom/peel/ui/da;->a:Lcom/peel/ui/cj;

    iget-object v0, v0, Lcom/peel/ui/cj;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "keyword"

    const-string/jumbo v2, ""

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 255
    iget-object v0, p0, Lcom/peel/ui/da;->a:Lcom/peel/ui/cj;

    invoke-static {v0}, Lcom/peel/ui/cj;->a(Lcom/peel/ui/cj;)Landroid/widget/AutoCompleteTextView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/AutoCompleteTextView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lcom/peel/ui/gk;

    invoke-virtual {v0}, Lcom/peel/ui/gk;->clear()V

    .line 256
    sget-object v0, Lcom/peel/c/a;->b:Lcom/peel/c/e;

    invoke-static {v0}, Lcom/peel/c/a;->a(Lcom/peel/c/e;)Ljava/lang/Object;

    move-result-object v0

    sget-object v1, Lcom/peel/c/b;->c:Lcom/peel/c/b;

    if-eq v0, v1, :cond_0

    .line 257
    iget-object v0, p0, Lcom/peel/ui/da;->a:Lcom/peel/ui/cj;

    invoke-static {v0}, Lcom/peel/ui/cj;->b(Lcom/peel/ui/cj;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 258
    :cond_0
    iget-object v0, p0, Lcom/peel/ui/da;->a:Lcom/peel/ui/cj;

    invoke-static {v0}, Lcom/peel/ui/cj;->a(Lcom/peel/ui/cj;)Landroid/widget/AutoCompleteTextView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/AutoCompleteTextView;->requestFocus()Z

    .line 261
    iget-object v0, p0, Lcom/peel/ui/da;->a:Lcom/peel/ui/cj;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/peel/ui/cj;->a(Lcom/peel/ui/cj;Z)Z

    .line 262
    iget-object v0, p0, Lcom/peel/ui/da;->a:Lcom/peel/ui/cj;

    invoke-virtual {v0}, Lcom/peel/ui/cj;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    const-string/jumbo v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/ae;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 263
    iget-object v1, p0, Lcom/peel/ui/da;->a:Lcom/peel/ui/cj;

    invoke-static {v1}, Lcom/peel/ui/cj;->a(Lcom/peel/ui/cj;)Landroid/widget/AutoCompleteTextView;

    move-result-object v1

    invoke-virtual {v0, v1, v3}, Landroid/view/inputmethod/InputMethodManager;->showSoftInput(Landroid/view/View;I)Z

    .line 264
    return-void
.end method
