.class Lcom/peel/ui/cb;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic b:I

.field final synthetic c:Lcom/peel/content/listing/LiveListing;

.field final synthetic d:Landroid/os/Bundle;

.field final synthetic e:Lcom/peel/ui/by;


# direct methods
.method constructor <init>(Lcom/peel/ui/by;Ljava/lang/String;ILcom/peel/content/listing/LiveListing;Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 574
    iput-object p1, p0, Lcom/peel/ui/cb;->e:Lcom/peel/ui/by;

    iput-object p2, p0, Lcom/peel/ui/cb;->a:Ljava/lang/String;

    iput p3, p0, Lcom/peel/ui/cb;->b:I

    iput-object p4, p0, Lcom/peel/ui/cb;->c:Lcom/peel/content/listing/LiveListing;

    iput-object p5, p0, Lcom/peel/ui/cb;->d:Landroid/os/Bundle;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 10

    .prologue
    const/4 v7, 0x0

    .line 577
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "dismiss_toast"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 578
    iget-object v1, p0, Lcom/peel/ui/cb;->e:Lcom/peel/ui/by;

    iget-object v1, v1, Lcom/peel/ui/by;->d:Lcom/peel/ui/bw;

    invoke-virtual {v1}, Lcom/peel/ui/bw;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/ae;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/support/v4/a/q;->a(Landroid/content/Context;)Landroid/support/v4/a/q;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/support/v4/a/q;->a(Landroid/content/Intent;)Z

    .line 579
    iget-object v0, p0, Lcom/peel/ui/cb;->e:Lcom/peel/ui/by;

    iget-object v0, v0, Lcom/peel/ui/by;->d:Lcom/peel/ui/bw;

    invoke-static {v0}, Lcom/peel/ui/bw;->c(Lcom/peel/ui/bw;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iget-object v1, p0, Lcom/peel/ui/cb;->e:Lcom/peel/ui/by;

    iget-object v1, v1, Lcom/peel/ui/by;->d:Lcom/peel/ui/bw;

    invoke-static {v1}, Lcom/peel/ui/bw;->c(Lcom/peel/ui/bw;)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/FrameLayout;

    invoke-virtual {v1}, Landroid/widget/FrameLayout;->getChildCount()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->removeViewAt(I)V

    .line 582
    invoke-static {}, Lcom/peel/util/a/f;->a()Lcom/peel/util/a/f;

    move-result-object v0

    sget-object v1, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v1}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v1

    if-nez v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    const/16 v2, 0x4b3

    iget-object v3, p0, Lcom/peel/ui/cb;->e:Lcom/peel/ui/by;

    iget-object v3, v3, Lcom/peel/ui/by;->d:Lcom/peel/ui/bw;

    iget v3, v3, Lcom/peel/ui/bw;->f:I

    iget-object v4, p0, Lcom/peel/ui/cb;->a:Ljava/lang/String;

    iget v5, p0, Lcom/peel/ui/cb;->b:I

    iget-object v6, p0, Lcom/peel/ui/cb;->c:Lcom/peel/content/listing/LiveListing;

    .line 584
    invoke-static {v6}, Lcom/peel/util/bx;->b(Lcom/peel/content/listing/Listing;)Ljava/lang/String;

    move-result-object v6

    iget-object v8, p0, Lcom/peel/ui/cb;->c:Lcom/peel/content/listing/LiveListing;

    invoke-virtual {v8}, Lcom/peel/content/listing/LiveListing;->h()Ljava/lang/String;

    move-result-object v8

    move v9, v7

    .line 582
    invoke-virtual/range {v0 .. v9}, Lcom/peel/util/a/f;->a(IIILjava/lang/String;ILjava/lang/String;ILjava/lang/String;I)V

    .line 586
    iget-object v0, p0, Lcom/peel/ui/cb;->e:Lcom/peel/ui/by;

    iget-object v0, v0, Lcom/peel/ui/by;->d:Lcom/peel/ui/bw;

    invoke-virtual {v0}, Lcom/peel/ui/bw;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/ui/cb;->e:Lcom/peel/ui/by;

    iget-object v1, v1, Lcom/peel/ui/by;->d:Lcom/peel/ui/bw;

    invoke-static {v1}, Lcom/peel/ui/bw;->d(Lcom/peel/ui/bw;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/peel/ui/cb;->d:Landroid/os/Bundle;

    invoke-static {v0, v1, v2}, Lcom/peel/d/e;->c(Landroid/support/v4/app/ae;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 587
    return-void

    .line 582
    :cond_0
    sget-object v1, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v1}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/data/at;->f()I

    move-result v1

    goto :goto_0
.end method
