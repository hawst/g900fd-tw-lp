.class public Lcom/peel/ui/bq;
.super Lcom/peel/d/u;


# instance fields
.field private e:Lcom/viewpagerindicator/TabPageIndicator;

.field private f:Landroid/support/v4/view/ViewPager;

.field private g:I

.field private final h:Landroid/support/v4/view/cs;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 23
    invoke-direct {p0}, Lcom/peel/d/u;-><init>()V

    .line 29
    const/4 v0, 0x0

    iput v0, p0, Lcom/peel/ui/bq;->g:I

    .line 37
    new-instance v0, Lcom/peel/ui/br;

    invoke-direct {v0, p0}, Lcom/peel/ui/br;-><init>(Lcom/peel/ui/bq;)V

    iput-object v0, p0, Lcom/peel/ui/bq;->h:Landroid/support/v4/view/cs;

    return-void
.end method

.method static synthetic a(Lcom/peel/ui/bq;I)I
    .locals 0

    .prologue
    .line 23
    iput p1, p0, Lcom/peel/ui/bq;->g:I

    return p1
.end method

.method static synthetic a(Lcom/peel/ui/bq;)Landroid/support/v4/view/ViewPager;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lcom/peel/ui/bq;->f:Landroid/support/v4/view/ViewPager;

    return-object v0
.end method


# virtual methods
.method public X()Z
    .locals 1

    .prologue
    .line 34
    iget v0, p0, Lcom/peel/ui/bq;->g:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public Z()V
    .locals 6

    .prologue
    .line 163
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 164
    sget-boolean v0, Lcom/peel/ui/bh;->f:Z

    if-eqz v0, :cond_3

    iget v0, p0, Lcom/peel/ui/bq;->g:I

    if-nez v0, :cond_3

    .line 165
    sget v0, Lcom/peel/ui/fp;->menu_done:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 174
    :cond_0
    :goto_0
    sget-object v0, Lcom/peel/c/a;->b:Lcom/peel/c/e;

    invoke-static {v0}, Lcom/peel/c/a;->a(Lcom/peel/c/e;)Ljava/lang/Object;

    move-result-object v0

    sget-object v1, Lcom/peel/c/b;->b:Lcom/peel/c/b;

    if-ne v0, v1, :cond_1

    sget v0, Lcom/peel/ui/fp;->menu_done:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v5, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 175
    sget v0, Lcom/peel/ui/fp;->overflow_menu_btn:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 178
    :cond_1
    sget-object v0, Lcom/peel/c/a;->b:Lcom/peel/c/e;

    invoke-static {v0}, Lcom/peel/c/a;->a(Lcom/peel/c/e;)Ljava/lang/Object;

    move-result-object v0

    sget-object v1, Lcom/peel/c/b;->a:Lcom/peel/c/b;

    if-ne v0, v1, :cond_2

    .line 179
    sget v0, Lcom/peel/ui/fp;->menu_change_room:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 180
    sget v0, Lcom/peel/ui/fp;->menu_channels:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 181
    sget v0, Lcom/peel/ui/fp;->menu_settings:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 182
    sget v0, Lcom/peel/ui/fp;->menu_about:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 184
    :cond_2
    new-instance v0, Lcom/peel/d/a;

    sget-object v1, Lcom/peel/d/d;->b:Lcom/peel/d/d;

    sget-object v2, Lcom/peel/d/b;->a:Lcom/peel/d/b;

    sget-object v3, Lcom/peel/d/c;->b:Lcom/peel/d/c;

    sget v4, Lcom/peel/ui/fp;->menu_done:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v5, v4}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    sget v4, Lcom/peel/ui/ft;->edit_fav_channels:I

    invoke-virtual {p0, v4}, Lcom/peel/ui/bq;->a(I)Ljava/lang/String;

    move-result-object v4

    :goto_1
    invoke-direct/range {v0 .. v5}, Lcom/peel/d/a;-><init>(Lcom/peel/d/d;Lcom/peel/d/b;Lcom/peel/d/c;Ljava/lang/String;Ljava/util/List;)V

    iput-object v0, p0, Lcom/peel/ui/bq;->d:Lcom/peel/d/a;

    .line 185
    iget-object v0, p0, Lcom/peel/ui/bq;->b:Lcom/peel/d/i;

    iget-object v1, p0, Lcom/peel/ui/bq;->d:Lcom/peel/d/a;

    invoke-virtual {v0, v1}, Lcom/peel/d/i;->a(Lcom/peel/d/a;)V

    .line 186
    return-void

    .line 167
    :cond_3
    sget v0, Lcom/peel/ui/fp;->menu_remote:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 169
    sget-boolean v0, Lcom/peel/ui/bh;->e:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/peel/ui/bq;->g:I

    if-nez v0, :cond_0

    .line 170
    sget v0, Lcom/peel/ui/fp;->menu_edit:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 184
    :cond_4
    sget v4, Lcom/peel/ui/ft;->favorites:I

    invoke-virtual {p0, v4}, Lcom/peel/ui/bq;->a(I)Ljava/lang/String;

    move-result-object v4

    goto :goto_1
.end method

.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    .line 57
    sget v0, Lcom/peel/ui/fq;->favorites_pager:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 58
    sget v0, Lcom/peel/ui/fp;->pager:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ViewPager;

    iput-object v0, p0, Lcom/peel/ui/bq;->f:Landroid/support/v4/view/ViewPager;

    .line 59
    sget v0, Lcom/peel/ui/fp;->indicator:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/viewpagerindicator/TabPageIndicator;

    iput-object v0, p0, Lcom/peel/ui/bq;->e:Lcom/viewpagerindicator/TabPageIndicator;

    .line 60
    return-object v1
.end method

.method public a(Landroid/view/MenuItem;)Z
    .locals 3

    .prologue
    .line 65
    iget-object v0, p0, Lcom/peel/ui/bq;->f:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->getAdapter()Landroid/support/v4/view/av;

    move-result-object v0

    check-cast v0, Lcom/peel/ui/ds;

    .line 67
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    .line 68
    sget v2, Lcom/peel/ui/fp;->menu_edit:I

    if-ne v1, v2, :cond_2

    .line 69
    if-eqz v0, :cond_0

    .line 70
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/peel/ui/ds;->d(I)Lcom/peel/d/u;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/peel/d/u;->a(Landroid/view/MenuItem;)Z

    move-result v0

    .line 80
    :goto_0
    return v0

    .line 76
    :cond_0
    iget-object v0, p0, Lcom/peel/ui/bq;->a:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/peel/ui/bq;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/peel/d/e;->a(Ljava/lang/String;Landroid/support/v4/app/ae;)V

    .line 80
    :cond_1
    :goto_1
    invoke-super {p0, p1}, Lcom/peel/d/u;->a(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0

    .line 77
    :cond_2
    sget v0, Lcom/peel/ui/fp;->menu_done:I

    if-ne v1, v0, :cond_1

    .line 78
    iget-object v0, p0, Lcom/peel/ui/bq;->a:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/peel/ui/bq;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/peel/d/e;->a(Ljava/lang/String;Landroid/support/v4/app/ae;)V

    goto :goto_1
.end method

.method public b()Z
    .locals 5

    .prologue
    const/4 v1, 0x0

    const v4, 0x800003

    const/4 v2, 0x0

    .line 136
    invoke-virtual {p0}, Lcom/peel/ui/bq;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    sget v3, Lcom/peel/ui/fp;->drawer_layout:I

    invoke-virtual {v0, v3}, Landroid/support/v4/app/ae;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/widget/DrawerLayout;

    invoke-virtual {v0, v4}, Landroid/support/v4/widget/DrawerLayout;->g(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 137
    invoke-virtual {p0}, Lcom/peel/ui/bq;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    sget v1, Lcom/peel/ui/fp;->drawer_layout:I

    invoke-virtual {v0, v1}, Landroid/support/v4/app/ae;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/widget/DrawerLayout;

    invoke-virtual {v0, v4}, Landroid/support/v4/widget/DrawerLayout;->f(I)V

    .line 138
    const/4 v0, 0x1

    .line 145
    :goto_0
    return v0

    .line 140
    :cond_0
    sget-object v0, Lcom/peel/content/a;->h:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/peel/ui/bq;->b:Lcom/peel/d/i;

    if-nez v0, :cond_2

    :cond_1
    move v0, v2

    goto :goto_0

    .line 141
    :cond_2
    iget-object v0, p0, Lcom/peel/ui/bq;->f:Landroid/support/v4/view/ViewPager;

    if-nez v0, :cond_3

    move-object v0, v1

    :goto_1
    check-cast v0, Lcom/peel/ui/ds;

    check-cast v0, Lcom/peel/ui/ds;

    .line 142
    if-nez v0, :cond_4

    .line 143
    :goto_2
    if-eqz v1, :cond_5

    invoke-virtual {v1}, Lcom/peel/d/u;->v()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_5

    invoke-virtual {v1}, Lcom/peel/d/u;->b()Z

    move-result v0

    goto :goto_0

    .line 141
    :cond_3
    iget-object v0, p0, Lcom/peel/ui/bq;->f:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->getAdapter()Landroid/support/v4/view/av;

    move-result-object v0

    goto :goto_1

    .line 142
    :cond_4
    iget-object v1, p0, Lcom/peel/ui/bq;->f:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v1}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/peel/ui/ds;->d(I)Lcom/peel/d/u;

    move-result-object v1

    goto :goto_2

    :cond_5
    move v0, v2

    .line 145
    goto :goto_0
.end method

.method public c(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 120
    invoke-super {p0, p1}, Lcom/peel/d/u;->c(Landroid/os/Bundle;)V

    .line 122
    sget-object v0, Lcom/peel/content/a;->h:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/peel/ui/bq;->b:Lcom/peel/d/i;

    if-nez v0, :cond_1

    .line 124
    :cond_0
    :goto_0
    return-void

    .line 123
    :cond_1
    iget-object v0, p0, Lcom/peel/ui/bq;->e:Lcom/viewpagerindicator/TabPageIndicator;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/viewpagerindicator/TabPageIndicator;->setCurrentItem(I)V

    goto :goto_0
.end method

.method public e(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 156
    iget-object v0, p0, Lcom/peel/ui/bq;->c:Landroid/os/Bundle;

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->putAll(Landroid/os/Bundle;)V

    .line 157
    invoke-super {p0, p1}, Lcom/peel/d/u;->e(Landroid/os/Bundle;)V

    .line 158
    return-void
.end method

.method public g()V
    .locals 3

    .prologue
    .line 150
    iget-object v0, p0, Lcom/peel/ui/bq;->b:Lcom/peel/d/i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/peel/ui/bq;->b:Lcom/peel/d/i;

    const-string/jumbo v1, "selected_forfavs"

    iget v2, p0, Lcom/peel/ui/bq;->g:I

    invoke-virtual {v0, v1, v2}, Lcom/peel/d/i;->b(Ljava/lang/String;I)V

    .line 151
    :cond_0
    invoke-super {p0}, Lcom/peel/d/u;->g()V

    .line 152
    return-void
.end method

.method public h(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 85
    invoke-super {p0, p1}, Lcom/peel/d/u;->h(Landroid/os/Bundle;)V

    .line 87
    if-eqz p1, :cond_0

    .line 88
    iget-object v0, p0, Lcom/peel/ui/bq;->c:Landroid/os/Bundle;

    invoke-virtual {v0, p1}, Landroid/os/Bundle;->putAll(Landroid/os/Bundle;)V

    .line 91
    :cond_0
    iget-object v0, p0, Lcom/peel/ui/bq;->b:Lcom/peel/d/i;

    if-nez v0, :cond_2

    move v0, v1

    :goto_0
    iput v0, p0, Lcom/peel/ui/bq;->g:I

    .line 93
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 94
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 95
    const-string/jumbo v3, "title"

    sget v4, Lcom/peel/ui/ft;->favchannels:I

    invoke-virtual {p0, v4}, Lcom/peel/ui/bq;->a(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 96
    const-string/jumbo v3, "fragment"

    const-class v4, Lcom/peel/ui/bh;

    invoke-virtual {v4}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 97
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 99
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 100
    const-string/jumbo v3, "title"

    sget v4, Lcom/peel/ui/ft;->favshows:I

    invoke-virtual {p0, v4}, Lcom/peel/ui/bq;->a(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 101
    const-string/jumbo v3, "fragment"

    const-class v4, Lcom/peel/ui/nl;

    invoke-virtual {v4}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 102
    const-string/jumbo v3, "netflix"

    invoke-virtual {v2, v3, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 103
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 105
    iget-object v1, p0, Lcom/peel/ui/bq;->f:Landroid/support/v4/view/ViewPager;

    new-instance v2, Lcom/peel/ui/ds;

    invoke-virtual {p0}, Lcom/peel/ui/bq;->p()Landroid/support/v4/app/aj;

    move-result-object v3

    invoke-virtual {p0}, Lcom/peel/ui/bq;->m()Landroid/support/v4/app/ae;

    move-result-object v4

    invoke-direct {v2, v3, v4, v0}, Lcom/peel/ui/ds;-><init>(Landroid/support/v4/app/aj;Landroid/content/Context;Ljava/util/List;)V

    invoke-virtual {v1, v2}, Landroid/support/v4/view/ViewPager;->setAdapter(Landroid/support/v4/view/av;)V

    .line 106
    iget-object v0, p0, Lcom/peel/ui/bq;->e:Lcom/viewpagerindicator/TabPageIndicator;

    iget-object v1, p0, Lcom/peel/ui/bq;->f:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, v1}, Lcom/viewpagerindicator/TabPageIndicator;->setViewPager(Landroid/support/v4/view/ViewPager;)V

    .line 107
    iget-object v0, p0, Lcom/peel/ui/bq;->e:Lcom/viewpagerindicator/TabPageIndicator;

    iget-object v1, p0, Lcom/peel/ui/bq;->h:Landroid/support/v4/view/cs;

    invoke-virtual {v0, v1}, Lcom/viewpagerindicator/TabPageIndicator;->setOnPageChangeListener(Landroid/support/v4/view/cs;)V

    .line 109
    invoke-virtual {p0}, Lcom/peel/ui/bq;->Z()V

    .line 111
    sget-object v0, Lcom/peel/content/a;->h:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 113
    iget-object v0, p0, Lcom/peel/ui/bq;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "refresh"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 114
    iget-object v0, p0, Lcom/peel/ui/bq;->c:Landroid/os/Bundle;

    invoke-virtual {p0, v0}, Lcom/peel/ui/bq;->c(Landroid/os/Bundle;)V

    .line 116
    :cond_1
    return-void

    .line 91
    :cond_2
    iget-object v0, p0, Lcom/peel/ui/bq;->b:Lcom/peel/d/i;

    const-string/jumbo v2, "selected_forfavs"

    invoke-virtual {v0, v2, v1}, Lcom/peel/d/i;->a(Ljava/lang/String;I)I

    move-result v0

    goto/16 :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 128
    sget-object v0, Lcom/peel/content/a;->h:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/peel/ui/bq;->b:Lcom/peel/d/i;

    if-nez v0, :cond_1

    .line 132
    :cond_0
    :goto_0
    return-void

    .line 129
    :cond_1
    iget-object v0, p0, Lcom/peel/ui/bq;->f:Landroid/support/v4/view/ViewPager;

    if-nez v0, :cond_2

    move-object v0, v1

    :goto_1
    check-cast v0, Lcom/peel/ui/ds;

    check-cast v0, Lcom/peel/ui/ds;

    .line 130
    if-nez v0, :cond_3

    .line 131
    :goto_2
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/peel/d/u;->v()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v1, p1}, Lcom/peel/d/u;->onClick(Landroid/view/View;)V

    goto :goto_0

    .line 129
    :cond_2
    iget-object v0, p0, Lcom/peel/ui/bq;->f:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->getAdapter()Landroid/support/v4/view/av;

    move-result-object v0

    goto :goto_1

    .line 130
    :cond_3
    iget-object v1, p0, Lcom/peel/ui/bq;->f:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v1}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/peel/ui/ds;->d(I)Lcom/peel/d/u;

    move-result-object v1

    goto :goto_2
.end method
