.class Lcom/peel/ui/db;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/widget/TextView$OnEditorActionListener;


# instance fields
.field final synthetic a:Lcom/peel/ui/cj;


# direct methods
.method constructor <init>(Lcom/peel/ui/cj;)V
    .locals 0

    .prologue
    .line 279
    iput-object p1, p0, Lcom/peel/ui/db;->a:Lcom/peel/ui/cj;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onEditorAction(Landroid/widget/TextView;ILandroid/view/KeyEvent;)Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 281
    invoke-static {}, Lcom/peel/ui/cj;->S()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "actionId: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 282
    const/4 v2, 0x6

    if-eq p2, v2, :cond_0

    const/4 v2, 0x3

    if-ne p2, v2, :cond_3

    .line 283
    :cond_0
    iget-object v2, p0, Lcom/peel/ui/db;->a:Lcom/peel/ui/cj;

    invoke-static {v2}, Lcom/peel/ui/cj;->a(Lcom/peel/ui/cj;)Landroid/widget/AutoCompleteTextView;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/AutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    .line 284
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    if-lez v3, :cond_1

    .line 285
    iget-object v3, p0, Lcom/peel/ui/db;->a:Lcom/peel/ui/cj;

    invoke-static {v3, v2, v0}, Lcom/peel/ui/cj;->a(Lcom/peel/ui/cj;Ljava/lang/String;Z)V

    .line 286
    invoke-static {}, Lcom/peel/util/a/f;->a()Lcom/peel/util/a/f;

    move-result-object v3

    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v0}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v0

    if-nez v0, :cond_2

    move v0, v1

    :goto_0
    const/16 v4, 0x3f7

    const/16 v5, 0x7d4

    invoke-virtual {v3, v0, v4, v5, v2}, Lcom/peel/util/a/f;->a(IIILjava/lang/String;)V

    .line 290
    :cond_1
    :goto_1
    return v1

    .line 286
    :cond_2
    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v0}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/data/at;->f()I

    move-result v0

    goto :goto_0

    :cond_3
    move v1, v0

    .line 290
    goto :goto_1
.end method
