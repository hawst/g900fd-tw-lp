.class Lcom/peel/ui/be;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:I

.field final synthetic b:Lcom/peel/ui/az;


# direct methods
.method constructor <init>(Lcom/peel/ui/az;I)V
    .locals 0

    .prologue
    .line 188
    iput-object p1, p0, Lcom/peel/ui/be;->b:Lcom/peel/ui/az;

    iput p2, p0, Lcom/peel/ui/be;->a:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    .line 191
    iget-object v0, p0, Lcom/peel/ui/be;->b:Lcom/peel/ui/az;

    invoke-static {v0}, Lcom/peel/ui/az;->b(Lcom/peel/ui/az;)Ljava/util/List;

    move-result-object v0

    iget v1, p0, Lcom/peel/ui/be;->a:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/data/Channel;

    .line 192
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 193
    const-string/jumbo v2, "library"

    iget-object v3, p0, Lcom/peel/ui/be;->b:Lcom/peel/ui/az;

    invoke-static {v3}, Lcom/peel/ui/az;->e(Lcom/peel/ui/az;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 194
    const-string/jumbo v2, "channel"

    invoke-virtual {v0}, Lcom/peel/data/Channel;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 195
    const-string/jumbo v2, "channelNumber"

    invoke-virtual {v0}, Lcom/peel/data/Channel;->e()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 196
    const-string/jumbo v2, "path"

    const-string/jumbo v3, "channel/unfav"

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 197
    const-string/jumbo v2, "room"

    iget-object v3, p0, Lcom/peel/ui/be;->b:Lcom/peel/ui/az;

    invoke-static {v3}, Lcom/peel/ui/az;->c(Lcom/peel/ui/az;)Lcom/peel/data/ContentRoom;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 200
    iget-object v2, p0, Lcom/peel/ui/be;->b:Lcom/peel/ui/az;

    invoke-static {v2}, Lcom/peel/ui/az;->b(Lcom/peel/ui/az;)Ljava/util/List;

    move-result-object v2

    monitor-enter v2

    .line 201
    :try_start_0
    iget-object v3, p0, Lcom/peel/ui/be;->b:Lcom/peel/ui/az;

    invoke-static {v3}, Lcom/peel/ui/az;->b(Lcom/peel/ui/az;)Ljava/util/List;

    move-result-object v3

    iget v4, p0, Lcom/peel/ui/be;->a:I

    invoke-interface {v3, v4}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 202
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 203
    iget-object v2, p0, Lcom/peel/ui/be;->b:Lcom/peel/ui/az;

    invoke-virtual {v2}, Lcom/peel/ui/az;->notifyDataSetChanged()V

    .line 204
    const-string/jumbo v2, "prgsvcid"

    invoke-virtual {v0}, Lcom/peel/data/Channel;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 205
    sget-object v2, Lcom/peel/content/a;->f:Lcom/peel/content/user/User;

    const/4 v3, 0x0

    invoke-virtual {v2, v1, v3}, Lcom/peel/content/user/User;->a(Landroid/os/Bundle;Lcom/peel/util/t;)V

    .line 206
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/peel/data/Channel;->a(Z)V

    .line 207
    iget-object v0, p0, Lcom/peel/ui/be;->b:Lcom/peel/ui/az;

    invoke-static {v0}, Lcom/peel/ui/az;->b(Lcom/peel/ui/az;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_0

    .line 209
    iget-object v0, p0, Lcom/peel/ui/be;->b:Lcom/peel/ui/az;

    iget-object v0, v0, Lcom/peel/ui/az;->a:Lcom/peel/ui/bh;

    invoke-virtual {v0}, Lcom/peel/ui/bh;->b()Z

    .line 211
    :cond_0
    return-void

    .line 202
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method
