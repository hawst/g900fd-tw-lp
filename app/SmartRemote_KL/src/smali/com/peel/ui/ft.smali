.class public final Lcom/peel/ui/ft;
.super Ljava/lang/Object;


# static fields
.field public static final CANDM:I = 0x7f0d049e

.field public static final CJHELLOVISION:I = 0x7f0d049f

.field public static final CMB:I = 0x7f0d04a7

.field public static final DeviceType1:I = 0x7f0d0000

.field public static final DeviceType10:I = 0x7f0d0001

.field public static final DeviceType10_abbr:I = 0x7f0d0002

.field public static final DeviceType10_half:I = 0x7f0d0003

.field public static final DeviceType10_short:I = 0x7f0d0004

.field public static final DeviceType13:I = 0x7f0d0005

.field public static final DeviceType14:I = 0x7f0d0006

.field public static final DeviceType18:I = 0x7f0d0007

.field public static final DeviceType18_abbr:I = 0x7f0d0008

.field public static final DeviceType18_half:I = 0x7f0d0009

.field public static final DeviceType18_short:I = 0x7f0d000a

.field public static final DeviceType1_abbr:I = 0x7f0d000b

.field public static final DeviceType1_half:I = 0x7f0d000c

.field public static final DeviceType2:I = 0x7f0d000d

.field public static final DeviceType20:I = 0x7f0d000e

.field public static final DeviceType20_abbr:I = 0x7f0d000f

.field public static final DeviceType2_abbr:I = 0x7f0d0010

.field public static final DeviceType2_half:I = 0x7f0d0011

.field public static final DeviceType2_short:I = 0x7f0d0012

.field public static final DeviceType3:I = 0x7f0d0013

.field public static final DeviceType3_abbr:I = 0x7f0d0014

.field public static final DeviceType3_half:I = 0x7f0d0015

.field public static final DeviceType3_short:I = 0x7f0d0016

.field public static final DeviceType4:I = 0x7f0d0017

.field public static final DeviceType4_abbr:I = 0x7f0d0018

.field public static final DeviceType4_half:I = 0x7f0d0019

.field public static final DeviceType4_short:I = 0x7f0d001a

.field public static final DeviceType5:I = 0x7f0d001b

.field public static final DeviceType5_abbr:I = 0x7f0d001c

.field public static final DeviceType5_half:I = 0x7f0d001d

.field public static final DeviceType5_short:I = 0x7f0d001e

.field public static final DeviceType6:I = 0x7f0d001f

.field public static final DeviceType6_abbr:I = 0x7f0d0020

.field public static final DeviceType6_half:I = 0x7f0d0021

.field public static final DeviceType6_short:I = 0x7f0d0022

.field public static final HCN:I = 0x7f0d04a0

.field public static final KRFTA:I = 0x7f0d047a

.field public static final KTOLLEHTV:I = 0x7f0d04a1

.field public static final LG:I = 0x7f0d04a8

.field public static final SKBTV:I = 0x7f0d04a9

.field public static final SKYLIFE:I = 0x7f0d04a2

.field public static final String:I = 0x7f0d0491

.field public static final TBROAD:I = 0x7f0d04a3

.field public static final abc_action_bar_home_description:I = 0x7f0d0023

.field public static final abc_action_bar_home_description_format:I = 0x7f0d0024

.field public static final abc_action_bar_home_subtitle_description_format:I = 0x7f0d0025

.field public static final abc_action_bar_up_description:I = 0x7f0d0026

.field public static final abc_action_menu_overflow_description:I = 0x7f0d0027

.field public static final abc_action_mode_done:I = 0x7f0d0028

.field public static final abc_activity_chooser_view_see_all:I = 0x7f0d0029

.field public static final abc_activitychooserview_choose_application:I = 0x7f0d002a

.field public static final abc_searchview_description_clear:I = 0x7f0d002b

.field public static final abc_searchview_description_query:I = 0x7f0d002c

.field public static final abc_searchview_description_search:I = 0x7f0d002d

.field public static final abc_searchview_description_submit:I = 0x7f0d002e

.field public static final abc_searchview_description_voice:I = 0x7f0d002f

.field public static final abc_shareactionprovider_share_with:I = 0x7f0d0030

.field public static final abc_shareactionprovider_share_with_application:I = 0x7f0d0031

.field public static final abc_toolbar_collapse_description:I = 0x7f0d0032

.field public static final about_label:I = 0x7f0d04a6

.field public static final about_peel:I = 0x7f0d04ac

.field public static final about_power:I = 0x7f0d0033

.field public static final about_version_label:I = 0x7f0d0034

.field public static final accessibility_dash:I = 0x7f0d0035

.field public static final accessibility_remote_control:I = 0x7f0d0036

.field public static final action_bar_title_stb:I = 0x7f0d0037

.field public static final actionadventure:I = 0x7f0d0038

.field public static final actionbar_title_tune:I = 0x7f0d0039

.field public static final activity_info_intro:I = 0x7f0d003a

.field public static final add:I = 0x7f0d003b

.field public static final add_channel_list_item:I = 0x7f0d003c

.field public static final add_device:I = 0x7f0d003d

.field public static final add_netflix:I = 0x7f0d003e

.field public static final add_new_room:I = 0x7f0d003f

.field public static final add_other_devices:I = 0x7f0d0040

.field public static final add_roku_title:I = 0x7f0d0041

.field public static final add_room:I = 0x7f0d04a4

.field public static final add_tv_for_inputs:I = 0x7f0d0042

.field public static final add_twitter:I = 0x7f0d0043

.field public static final addchannel:I = 0x7f0d0044

.field public static final adding_channels:I = 0x7f0d0045

.field public static final after_switch_source:I = 0x7f0d0046

.field public static final age:I = 0x7f0d0047

.field public static final age_gender_desc:I = 0x7f0d0048

.field public static final age_gender_female:I = 0x7f0d0049

.field public static final age_gender_male:I = 0x7f0d004a

.field public static final age_gender_title:I = 0x7f0d004b

.field public static final age_opt_out:I = 0x7f0d004c

.field public static final age_title:I = 0x7f0d004d

.field public static final airing_upcoming:I = 0x7f0d004e

.field public static final alert_no_display_name:I = 0x7f0d004f

.field public static final alert_no_item_selected:I = 0x7f0d0050

.field public static final all_cap:I = 0x7f0d0051

.field public static final all_sports:I = 0x7f0d0052

.field public static final always_remote_widget_pin_title:I = 0x7f0d0053

.field public static final animation:I = 0x7f0d0054

.field public static final app_desc:I = 0x7f0d0055

.field public static final app_name:I = 0x7f0d0056

.field public static final apple_tv_unpair_message:I = 0x7f0d0057

.field public static final apple_tv_unpair_title:I = 0x7f0d0058

.field public static final assign_channel_to_key:I = 0x7f0d0059

.field public static final attention_required:I = 0x7f0d005a

.field public static final audio:I = 0x7f0d005b

.field public static final audio_output_msg:I = 0x7f0d005c

.field public static final auth_client_needs_enabling_title:I = 0x7f0d005d

.field public static final auth_client_needs_installation_title:I = 0x7f0d005e

.field public static final auth_client_needs_update_title:I = 0x7f0d005f

.field public static final auth_client_play_services_err_notification_msg:I = 0x7f0d0060

.field public static final auth_client_requested_by_msg:I = 0x7f0d0061

.field public static final auth_client_using_bad_version_title:I = 0x7f0d0062

.field public static final auto_display_lock:I = 0x7f0d0063

.field public static final auto_display_lock_myplace_info:I = 0x7f0d0064

.field public static final auto_display_setmyplace:I = 0x7f0d0065

.field public static final auto_display_title:I = 0x7f0d0066

.field public static final auto_place_desc:I = 0x7f0d0067

.field public static final av_mode_cap:I = 0x7f0d0068

.field public static final baseball:I = 0x7f0d0069

.field public static final basic_profile:I = 0x7f0d006a

.field public static final basketball:I = 0x7f0d006b

.field public static final before_you_start:I = 0x7f0d006c

.field public static final biography:I = 0x7f0d006d

.field public static final bluetooth:I = 0x7f0d006e

.field public static final bluray_popup_menu:I = 0x7f0d006f

.field public static final brand:I = 0x7f0d0070

.field public static final brand_colon:I = 0x7f0d0071

.field public static final brand_supported:I = 0x7f0d0072

.field public static final broadcastchannels:I = 0x7f0d0073

.field public static final browse:I = 0x7f0d0074

.field public static final browseonlinesupport:I = 0x7f0d0075

.field public static final btn_audio:I = 0x7f0d0076

.field public static final btn_av_mode:I = 0x7f0d0077

.field public static final btn_change_country:I = 0x7f0d0078

.field public static final btn_esc:I = 0x7f0d0079

.field public static final btn_fav:I = 0x7f0d007a

.field public static final btn_flash_back:I = 0x7f0d007b

.field public static final btn_home:I = 0x7f0d007c

.field public static final btn_instant_replay:I = 0x7f0d007d

.field public static final btn_last:I = 0x7f0d007e

.field public static final btn_livetv:I = 0x7f0d007f

.field public static final btn_ondemand:I = 0x7f0d0080

.field public static final btn_option:I = 0x7f0d0081

.field public static final btn_options:I = 0x7f0d0082

.field public static final btn_playpause:I = 0x7f0d0083

.field public static final btn_recall:I = 0x7f0d0084

.field public static final btn_return:I = 0x7f0d0085

.field public static final btn_skip_back:I = 0x7f0d0086

.field public static final btn_skip_forward:I = 0x7f0d0087

.field public static final btn_slow:I = 0x7f0d0088

.field public static final btn_star:I = 0x7f0d0089

.field public static final btn_stb:I = 0x7f0d008a

.field public static final btn_sync_menu:I = 0x7f0d008b

.field public static final business:I = 0x7f0d008c

.field public static final button_channel_controller:I = 0x7f0d008d

.field public static final button_channel_down:I = 0x7f0d008e

.field public static final button_channel_up:I = 0x7f0d008f

.field public static final button_direction:I = 0x7f0d0090

.field public static final button_fanspeed_down:I = 0x7f0d0091

.field public static final button_fanspeed_up:I = 0x7f0d0092

.field public static final button_fast_forward:I = 0x7f0d0093

.field public static final button_hide:I = 0x7f0d0094

.field public static final button_home:I = 0x7f0d0095

.field public static final button_list:I = 0x7f0d0096

.field public static final button_logo:I = 0x7f0d0097

.field public static final button_mode:I = 0x7f0d0098

.field public static final button_mute:I = 0x7f0d0099

.field public static final button_navigate_down:I = 0x7f0d009a

.field public static final button_navigate_left:I = 0x7f0d009b

.field public static final button_navigate_right:I = 0x7f0d009c

.field public static final button_navigate_up:I = 0x7f0d009d

.field public static final button_next:I = 0x7f0d009e

.field public static final button_number_pad:I = 0x7f0d009f

.field public static final button_pause:I = 0x7f0d00a0

.field public static final button_play:I = 0x7f0d00a1

.field public static final button_play_pause:I = 0x7f0d00a2

.field public static final button_power:I = 0x7f0d00a3

.field public static final button_previous:I = 0x7f0d00a4

.field public static final button_record:I = 0x7f0d00a5

.field public static final button_rewind:I = 0x7f0d00a6

.field public static final button_show:I = 0x7f0d00a7

.field public static final button_source:I = 0x7f0d00a8

.field public static final button_stop:I = 0x7f0d00a9

.field public static final button_temp_down:I = 0x7f0d00aa

.field public static final button_temp_up:I = 0x7f0d00ab

.field public static final button_thumbs_down:I = 0x7f0d00ac

.field public static final button_thumbs_up:I = 0x7f0d00ad

.field public static final button_volume_controller:I = 0x7f0d00ae

.field public static final button_volume_down:I = 0x7f0d00af

.field public static final button_volume_up:I = 0x7f0d00b0

.field public static final cancel:I = 0x7f0d00b1

.field public static final cannot_set_reminder:I = 0x7f0d00b2

.field public static final cant_find_my_brand:I = 0x7f0d00b3

.field public static final card_recommend:I = 0x7f0d00b4

.field public static final cast:I = 0x7f0d00b5

.field public static final ch_guide_next_format:I = 0x7f0d00b6

.field public static final ch_list:I = 0x7f0d00b7

.field public static final changetochannel:I = 0x7f0d00b8

.field public static final channel:I = 0x7f0d00b9

.field public static final channel_exist:I = 0x7f0d00ba

.field public static final channel_num:I = 0x7f0d00bb

.field public static final channel_playing:I = 0x7f0d00bc

.field public static final channel_remote_none:I = 0x7f0d00bd

.field public static final channels:I = 0x7f0d00be

.field public static final channlel_number:I = 0x7f0d00bf

.field public static final check_atleast_one_language:I = 0x7f0d04a5

.field public static final chlist_cap:I = 0x7f0d00c0

.field public static final choose_channel_changer:I = 0x7f0d00c1

.field public static final choose_device_brand:I = 0x7f0d00c2

.field public static final choose_device_type_msg:I = 0x7f0d00c3

.field public static final choose_pic:I = 0x7f0d00c4

.field public static final choose_stb_brand:I = 0x7f0d0492

.field public static final choose_stereo_brand:I = 0x7f0d00c5

.field public static final choose_tv_tuner_intro:I = 0x7f0d00c6

.field public static final choose_tv_tuner_intro_satellite:I = 0x7f0d00c7

.field public static final choose_tv_tuner_tips:I = 0x7f0d00c8

.field public static final choose_wifi:I = 0x7f0d00c9

.field public static final close:I = 0x7f0d00ca

.field public static final cmenu:I = 0x7f0d00cb

.field public static final coachmark_text_channel:I = 0x7f0d00cc

.field public static final coachmark_text_like:I = 0x7f0d00cd

.field public static final coachmark_text_navigation:I = 0x7f0d00ce

.field public static final coachmark_text_remindar:I = 0x7f0d00cf

.field public static final coachmark_text_remote:I = 0x7f0d00d0

.field public static final coachmark_text_remote_volume:I = 0x7f0d00d1

.field public static final coachmark_text_tile:I = 0x7f0d00d2

.field public static final codes_not_available_desc:I = 0x7f0d00d3

.field public static final com_facebook_choose_friends:I = 0x7f0d00d4

.field public static final com_facebook_dialogloginactivity_ok_button:I = 0x7f0d00d5

.field public static final com_facebook_internet_permission_error_message:I = 0x7f0d00d6

.field public static final com_facebook_internet_permission_error_title:I = 0x7f0d00d7

.field public static final com_facebook_like_button_liked:I = 0x7f0d00d8

.field public static final com_facebook_like_button_not_liked:I = 0x7f0d00d9

.field public static final com_facebook_loading:I = 0x7f0d00da

.field public static final com_facebook_loginview_cancel_action:I = 0x7f0d00db

.field public static final com_facebook_loginview_log_in_button:I = 0x7f0d00dc

.field public static final com_facebook_loginview_log_out_action:I = 0x7f0d00dd

.field public static final com_facebook_loginview_log_out_button:I = 0x7f0d00de

.field public static final com_facebook_loginview_logged_in_as:I = 0x7f0d00df

.field public static final com_facebook_loginview_logged_in_using_facebook:I = 0x7f0d00e0

.field public static final com_facebook_logo_content_description:I = 0x7f0d00e1

.field public static final com_facebook_nearby:I = 0x7f0d00e2

.field public static final com_facebook_picker_done_button_text:I = 0x7f0d00e3

.field public static final com_facebook_placepicker_subtitle_catetory_only_format:I = 0x7f0d00e4

.field public static final com_facebook_placepicker_subtitle_format:I = 0x7f0d00e5

.field public static final com_facebook_placepicker_subtitle_were_here_only_format:I = 0x7f0d00e6

.field public static final com_facebook_requesterror_password_changed:I = 0x7f0d00e7

.field public static final com_facebook_requesterror_permissions:I = 0x7f0d00e8

.field public static final com_facebook_requesterror_reconnect:I = 0x7f0d00e9

.field public static final com_facebook_requesterror_relogin:I = 0x7f0d00ea

.field public static final com_facebook_requesterror_web_login:I = 0x7f0d00eb

.field public static final com_facebook_tooltip_default:I = 0x7f0d00ec

.field public static final com_facebook_usersettingsfragment_log_in_button:I = 0x7f0d00ed

.field public static final com_facebook_usersettingsfragment_logged_in:I = 0x7f0d00ee

.field public static final com_facebook_usersettingsfragment_not_logged_in:I = 0x7f0d00ef

.field public static final comedy:I = 0x7f0d00f0

.field public static final common_google_play_services_enable_button:I = 0x7f0d00f1

.field public static final common_google_play_services_enable_text:I = 0x7f0d00f2

.field public static final common_google_play_services_enable_title:I = 0x7f0d00f3

.field public static final common_google_play_services_install_button:I = 0x7f0d00f4

.field public static final common_google_play_services_install_text_phone:I = 0x7f0d00f5

.field public static final common_google_play_services_install_text_tablet:I = 0x7f0d00f6

.field public static final common_google_play_services_install_title:I = 0x7f0d00f7

.field public static final common_google_play_services_invalid_account_text:I = 0x7f0d00f8

.field public static final common_google_play_services_invalid_account_title:I = 0x7f0d00f9

.field public static final common_google_play_services_network_error_text:I = 0x7f0d00fa

.field public static final common_google_play_services_network_error_title:I = 0x7f0d00fb

.field public static final common_google_play_services_unknown_issue:I = 0x7f0d00fc

.field public static final common_google_play_services_unsupported_date_text:I = 0x7f0d00fd

.field public static final common_google_play_services_unsupported_text:I = 0x7f0d00fe

.field public static final common_google_play_services_unsupported_title:I = 0x7f0d00ff

.field public static final common_google_play_services_update_button:I = 0x7f0d0100

.field public static final common_google_play_services_update_text:I = 0x7f0d0101

.field public static final common_google_play_services_update_title:I = 0x7f0d0102

.field public static final common_signin_button_text:I = 0x7f0d0103

.field public static final common_signin_button_text_long:I = 0x7f0d0104

.field public static final confirm:I = 0x7f0d0105

.field public static final confirm_logout_netflix:I = 0x7f0d0106

.field public static final confirm_logout_twitter:I = 0x7f0d0107

.field public static final confirm_message:I = 0x7f0d0108

.field public static final confirm_title:I = 0x7f0d0109

.field public static final continue_txt:I = 0x7f0d010a

.field public static final control_add_custom_label:I = 0x7f0d010b

.field public static final control_navigation_tutorial:I = 0x7f0d010c

.field public static final control_page_view_tutorial:I = 0x7f0d010d

.field public static final control_tutorial_got_it:I = 0x7f0d010e

.field public static final correctinputnotfound:I = 0x7f0d010f

.field public static final current_episode:I = 0x7f0d0110

.field public static final current_sport:I = 0x7f0d0111

.field public static final current_temp_cap:I = 0x7f0d0112

.field public static final currently_watching:I = 0x7f0d0113

.field public static final customize_time:I = 0x7f0d0114

.field public static final cutprograms:I = 0x7f0d0115

.field public static final day_time_label:I = 0x7f0d0116

.field public static final delay_channel:I = 0x7f0d0117

.field public static final delete:I = 0x7f0d0118

.field public static final delete_device_confirmation:I = 0x7f0d0119

.field public static final delete_room:I = 0x7f0d011a

.field public static final delete_room_confirmation:I = 0x7f0d011b

.field public static final desc_add_fav_channel:I = 0x7f0d011c

.field public static final desc_adjust_delay:I = 0x7f0d011d

.field public static final desc_bad_zipcode:I = 0x7f0d011e

.field public static final desc_ch_confirmation:I = 0x7f0d011f

.field public static final desc_date_time_not_match:I = 0x7f0d0120

.field public static final desc_dtv_setup_successful:I = 0x7f0d0121

.field public static final desc_input_list:I = 0x7f0d0122

.field public static final desc_input_list_devices:I = 0x7f0d0123

.field public static final desc_resolution:I = 0x7f0d04aa

.field public static final desc_resolution_language:I = 0x7f0d0124

.field public static final desc_roku_setup_successful:I = 0x7f0d0125

.field public static final desc_saving_channels:I = 0x7f0d0126

.field public static final desc_setup_country_intro:I = 0x7f0d0127

.field public static final desc_test_channel:I = 0x7f0d0128

.field public static final desc_tunein_check:I = 0x7f0d0129

.field public static final description:I = 0x7f0d012a

.field public static final description_tunein_check:I = 0x7f0d012b

.field public static final details:I = 0x7f0d012c

.field public static final device_add_category:I = 0x7f0d012d

.field public static final device_added:I = 0x7f0d012e

.field public static final device_added_enable:I = 0x7f0d012f

.field public static final device_list_msg:I = 0x7f0d0130

.field public static final device_setup_intro:I = 0x7f0d0131

.field public static final device_test_channel_change_question_msg:I = 0x7f0d0132

.field public static final device_test_question_msg:I = 0x7f0d0493

.field public static final device_test_turn_on_question_msg:I = 0x7f0d0133

.field public static final device_type:I = 0x7f0d0134

.field public static final device_type_dvr:I = 0x7f0d047b

.field public static final devices:I = 0x7f0d0135

.field public static final dialog_title_hdsd:I = 0x7f0d0136

.field public static final dialog_title_language:I = 0x7f0d0137

.field public static final dialog_title_premium_channels:I = 0x7f0d0138

.field public static final did_device_change_channel:I = 0x7f0d0139

.field public static final did_device_turn_on:I = 0x7f0d047c

.field public static final did_device_turn_on_for_tv:I = 0x7f0d013a

.field public static final did_device_turn_on_title:I = 0x7f0d013b

.field public static final did_it_work:I = 0x7f0d013c

.field public static final different_episodes_airing:I = 0x7f0d013d

.field public static final direction:I = 0x7f0d013e

.field public static final directtv_loading:I = 0x7f0d013f

.field public static final directtv_login:I = 0x7f0d0140

.field public static final directtv_password:I = 0x7f0d0141

.field public static final directtv_text1:I = 0x7f0d0142

.field public static final directtv_text2:I = 0x7f0d0143

.field public static final directtv_text3:I = 0x7f0d0144

.field public static final directtv_text4:I = 0x7f0d0145

.field public static final directtv_text5:I = 0x7f0d0146

.field public static final directtv_username:I = 0x7f0d0147

.field public static final disambiguation_intro:I = 0x7f0d0148

.field public static final discover_magic_personalized_tv:I = 0x7f0d0149

.field public static final dislike:I = 0x7f0d014a

.field public static final dislike_confirmation:I = 0x7f0d014b

.field public static final display:I = 0x7f0d014c

.field public static final do_not_switch:I = 0x7f0d014d

.field public static final documentary:I = 0x7f0d014e

.field public static final done:I = 0x7f0d014f

.field public static final dont_get_channel:I = 0x7f0d0150

.field public static final drama:I = 0x7f0d0151

.field public static final drawer_close:I = 0x7f0d0152

.field public static final drawer_open:I = 0x7f0d0153

.field public static final dt_label_available_now:I = 0x7f0d0154

.field public static final dvd:I = 0x7f0d0155

.field public static final dvr:I = 0x7f0d0156

.field public static final dvr_cap:I = 0x7f0d0157

.field public static final easy_remote:I = 0x7f0d0158

.field public static final edit:I = 0x7f0d0159

.field public static final edit_channels_subtitle_premium_default:I = 0x7f0d015a

.field public static final edit_fav_channels:I = 0x7f0d015b

.field public static final edit_ir:I = 0x7f0d015c

.field public static final edit_lineup:I = 0x7f0d015d

.field public static final edit_something:I = 0x7f0d015e

.field public static final editchannels:I = 0x7f0d015f

.field public static final email:I = 0x7f0d0160

.field public static final empty_friends_favs:I = 0x7f0d0495

.field public static final empty_zip_code:I = 0x7f0d0161

.field public static final enable:I = 0x7f0d0162

.field public static final enable_quick_panel_widget:I = 0x7f0d049b

.field public static final engineering_build:I = 0x7f0d0163

.field public static final enter:I = 0x7f0d0164

.field public static final enter_brand_name:I = 0x7f0d0165

.field public static final enter_desc:I = 0x7f0d0166

.field public static final enter_model_name:I = 0x7f0d0167

.field public static final enter_postal_code:I = 0x7f0d0168

.field public static final enter_postal_code_for_providers:I = 0x7f0d0169

.field public static final enter_room_name_below:I = 0x7f0d016a

.field public static final enter_service_provider_name:I = 0x7f0d016b

.field public static final enter_subject:I = 0x7f0d016c

.field public static final enter_us_zip:I = 0x7f0d016d

.field public static final enter_us_zip_for_providers:I = 0x7f0d016e

.field public static final enter_valid_email:I = 0x7f0d016f

.field public static final entermodelnum:I = 0x7f0d0170

.field public static final entertainment:I = 0x7f0d0171

.field public static final enteryouemail:I = 0x7f0d0172

.field public static final episode_name:I = 0x7f0d0173

.field public static final episode_number:I = 0x7f0d0174

.field public static final error:I = 0x7f0d0175

.field public static final error_ip_enter:I = 0x7f0d0176

.field public static final error_login_with:I = 0x7f0d0177

.field public static final event:I = 0x7f0d0178

.field public static final existing_user:I = 0x7f0d0179

.field public static final exit_app:I = 0x7f0d017a

.field public static final exit_cap:I = 0x7f0d017b

.field public static final facebook_label:I = 0x7f0d017c

.field public static final failed_import:I = 0x7f0d017d

.field public static final failed_import_title:I = 0x7f0d017e

.field public static final failed_to_learn:I = 0x7f0d017f

.field public static final fan_label:I = 0x7f0d0180

.field public static final fav_cap:I = 0x7f0d0181

.field public static final favchannels:I = 0x7f0d0182

.field public static final favorite:I = 0x7f0d0183

.field public static final favoriteprograms:I = 0x7f0d0184

.field public static final favorites:I = 0x7f0d0185

.field public static final favorites_not_available:I = 0x7f0d0186

.field public static final favshows:I = 0x7f0d0187

.field public static final feedback_confirm_msg:I = 0x7f0d0188

.field public static final feedback_title_type:I = 0x7f0d0189

.field public static final finalize_device_info:I = 0x7f0d018a

.field public static final find:I = 0x7f0d018b

.field public static final find_your_tv_service:I = 0x7f0d018c

.field public static final firstrun:I = 0x7f0d018d

.field public static final flash_back_cap:I = 0x7f0d018e

.field public static final follow_team_desc:I = 0x7f0d018f

.field public static final football:I = 0x7f0d0190

.field public static final foreign:I = 0x7f0d0191

.field public static final form_model:I = 0x7f0d0192

.field public static final found_roku:I = 0x7f0d0193

.field public static final friday:I = 0x7f0d0194

.field public static final friend_on_peel:I = 0x7f0d0195

.field public static final friend_recommend_label:I = 0x7f0d0196

.field public static final friendsfavshows:I = 0x7f0d0197

.field public static final future_recording:I = 0x7f0d0198

.field public static final game:I = 0x7f0d0199

.field public static final gameshow:I = 0x7f0d019a

.field public static final gender:I = 0x7f0d019b

.field public static final gender_female:I = 0x7f0d019c

.field public static final gender_male:I = 0x7f0d019d

.field public static final gender_opt_out:I = 0x7f0d019e

.field public static final genre_error:I = 0x7f0d019f

.field public static final genres:I = 0x7f0d01a0

.field public static final go_to_tweet_search:I = 0x7f0d01a1

.field public static final google_plus_label:I = 0x7f0d01a2

.field public static final google_tv_locator:I = 0x7f0d01a3

.field public static final google_tv_setup:I = 0x7f0d01a4

.field public static final googletv_devicepicker:I = 0x7f0d01a5

.field public static final googletv_enter_pin:I = 0x7f0d01a6

.field public static final googletv_enter_pin_msg:I = 0x7f0d01a7

.field public static final googletv_help_connected:I = 0x7f0d01a8

.field public static final googletv_help_followsteps:I = 0x7f0d01a9

.field public static final googletv_help_next:I = 0x7f0d01aa

.field public static final googletv_help_nofind:I = 0x7f0d01ab

.field public static final googletv_help_nofind_retry:I = 0x7f0d01ac

.field public static final googletv_help_onyourdevice:I = 0x7f0d01ad

.field public static final googletv_help_onyourmobile:I = 0x7f0d01ae

.field public static final googletv_help_physically:I = 0x7f0d01af

.field public static final googletv_help_proceed:I = 0x7f0d01b0

.field public static final googletv_help_settings:I = 0x7f0d01b1

.field public static final googletv_help_wifi:I = 0x7f0d01b2

.field public static final googletv_id:I = 0x7f0d01b3

.field public static final googletv_setup:I = 0x7f0d01b4

.field public static final googletv_setup_notfound:I = 0x7f0d01b5

.field public static final googletv_supports:I = 0x7f0d01b6

.field public static final gotosetting:I = 0x7f0d01b7

.field public static final guide:I = 0x7f0d01b8

.field public static final guide_cap:I = 0x7f0d01b9

.field public static final header_customize_genres:I = 0x7f0d01ba

.field public static final header_customize_sports:I = 0x7f0d01bb

.field public static final header_edit_channel:I = 0x7f0d01bc

.field public static final header_edit_channel_lineup:I = 0x7f0d01bd

.field public static final header_edit_hdsd:I = 0x7f0d01be

.field public static final header_edit_language:I = 0x7f0d01bf

.field public static final header_input_setting:I = 0x7f0d01c0

.field public static final header_languages:I = 0x7f0d01c1

.field public static final header_resolution:I = 0x7f0d01c2

.field public static final header_resolution_language:I = 0x7f0d01c3

.field public static final header_volume_control_device:I = 0x7f0d047d

.field public static final header_volume_setting:I = 0x7f0d01c4

.field public static final hello_blank_fragment:I = 0x7f0d01c5

.field public static final help:I = 0x7f0d01c6

.field public static final hint_channel_search:I = 0x7f0d01c7

.field public static final hint_search_box:I = 0x7f0d01c8

.field public static final hockey:I = 0x7f0d01c9

.field public static final holiday:I = 0x7f0d01ca

.field public static final howtoremovedisliked:I = 0x7f0d01cb

.field public static final howtoremoveliked:I = 0x7f0d01cc

.field public static final i_am_watching:I = 0x7f0d01cd

.field public static final i_have_a_projector:I = 0x7f0d01ce

.field public static final ignore:I = 0x7f0d01cf

.field public static final ihaveaprojector:I = 0x7f0d01d0

.field public static final import_device_title:I = 0x7f0d01d1

.field public static final import_label:I = 0x7f0d01d2

.field public static final import_profile:I = 0x7f0d01d3

.field public static final importing:I = 0x7f0d01d4

.field public static final incorrect_passcode:I = 0x7f0d01d5

.field public static final incorrect_passcode_msg:I = 0x7f0d01d6

.field public static final info:I = 0x7f0d01d7

.field public static final info_cap:I = 0x7f0d01d8

.field public static final initialfavshows:I = 0x7f0d01d9

.field public static final initialfavshows_nothumb:I = 0x7f0d01da

.field public static final inordertocontrol:I = 0x7f0d01db

.field public static final input:I = 0x7f0d01dc

.field public static final input_cap:I = 0x7f0d01dd

.field public static final input_dont:I = 0x7f0d01de

.field public static final input_setup_confirmation:I = 0x7f0d01df

.field public static final input_setup_for_activity:I = 0x7f0d01e0

.field public static final inputs:I = 0x7f0d01e1

.field public static final instructions:I = 0x7f0d01e2

.field public static final invalid_description:I = 0x7f0d01e3

.field public static final invalid_email_address:I = 0x7f0d01e4

.field public static final invalid_model_title:I = 0x7f0d01e5

.field public static final invalid_subject_title:I = 0x7f0d01e6

.field public static final invalid_tv_service_provider_title:I = 0x7f0d01e7

.field public static final invite:I = 0x7f0d01e8

.field public static final invite_email:I = 0x7f0d01e9

.field public static final invite_friend:I = 0x7f0d01ea

.field public static final invite_message:I = 0x7f0d01eb

.field public static final invite_subject:I = 0x7f0d01ec

.field public static final ip_address:I = 0x7f0d01ed

.field public static final ir_btn_enabled:I = 0x7f0d01ee

.field public static final ir_learning_instruction:I = 0x7f0d01ef

.field public static final ir_no_test_next_code:I = 0x7f0d01f0

.field public static final ir_report_missing_code:I = 0x7f0d01f1

.field public static final ir_retry_current_code:I = 0x7f0d01f2

.field public static final ir_save:I = 0x7f0d01f3

.field public static final ir_send_code:I = 0x7f0d01f4

.field public static final ir_try_test_again:I = 0x7f0d01f5

.field public static final ir_yes_code_works:I = 0x7f0d01f6

.field public static final issue_prompt:I = 0x7f0d01f7

.field public static final issue_type1:I = 0x7f0d01f8

.field public static final issue_type2:I = 0x7f0d01f9

.field public static final issue_type3:I = 0x7f0d01fa

.field public static final issue_type4:I = 0x7f0d01fb

.field public static final issue_type5:I = 0x7f0d01fc

.field public static final issue_type6:I = 0x7f0d01fd

.field public static final issue_type7:I = 0x7f0d01fe

.field public static final issue_type8:I = 0x7f0d01ff

.field public static final jfy:I = 0x7f0d047e

.field public static final key:I = 0x7f0d0200

.field public static final kids:I = 0x7f0d0201

.field public static final label_3digits:I = 0x7f0d0202

.field public static final label_about:I = 0x7f0d0203

.field public static final label_activities:I = 0x7f0d0204

.field public static final label_activity_name:I = 0x7f0d0205

.field public static final label_add_room:I = 0x7f0d0206

.field public static final label_add_room_name:I = 0x7f0d0207

.field public static final label_available_devices:I = 0x7f0d0208

.field public static final label_available_now:I = 0x7f0d0209

.field public static final label_available_rooms:I = 0x7f0d020a

.field public static final label_bs:I = 0x7f0d020b

.field public static final label_cable_satellite_box:I = 0x7f0d020c

.field public static final label_change_remote:I = 0x7f0d020d

.field public static final label_change_room:I = 0x7f0d020e

.field public static final label_clear:I = 0x7f0d020f

.field public static final label_cs:I = 0x7f0d0210

.field public static final label_customize:I = 0x7f0d0211

.field public static final label_device_brand:I = 0x7f0d0212

.field public static final label_device_list:I = 0x7f0d0213

.field public static final label_device_setup_success:I = 0x7f0d0214

.field public static final label_device_type:I = 0x7f0d0215

.field public static final label_direction:I = 0x7f0d0216

.field public static final label_done_found_it:I = 0x7f0d0217

.field public static final label_enable_now:I = 0x7f0d0218

.field public static final label_fast:I = 0x7f0d0219

.field public static final label_get_showtimes:I = 0x7f0d021a

.field public static final label_hd:I = 0x7f0d021b

.field public static final label_help:I = 0x7f0d021c

.field public static final label_hide:I = 0x7f0d021d

.field public static final label_learn:I = 0x7f0d021e

.field public static final label_lld:I = 0x7f0d021f

.field public static final label_mode:I = 0x7f0d0220

.field public static final label_name:I = 0x7f0d0221

.field public static final label_next_input:I = 0x7f0d0222

.field public static final label_no_codes_found:I = 0x7f0d0223

.field public static final label_notification:I = 0x7f0d0224

.field public static final label_optional:I = 0x7f0d0225

.field public static final label_pick_channel:I = 0x7f0d0226

.field public static final label_popular_widget:I = 0x7f0d0227

.field public static final label_post:I = 0x7f0d0228

.field public static final label_prev_input:I = 0x7f0d0229

.field public static final label_prime_time:I = 0x7f0d022a

.field public static final label_quick_panel_settings:I = 0x7f0d022b

.field public static final label_recommend:I = 0x7f0d022c

.field public static final label_remind_me_all_caps:I = 0x7f0d022d

.field public static final label_report:I = 0x7f0d022e

.field public static final label_sd:I = 0x7f0d022f

.field public static final label_select:I = 0x7f0d0230

.field public static final label_select_country_region:I = 0x7f0d0231

.field public static final label_select_region:I = 0x7f0d0232

.field public static final label_select_room:I = 0x7f0d0233

.field public static final label_settings:I = 0x7f0d0234

.field public static final label_settings_social:I = 0x7f0d0235

.field public static final label_setup:I = 0x7f0d047f

.field public static final label_setup_now:I = 0x7f0d0236

.field public static final label_share:I = 0x7f0d0237

.field public static final label_show:I = 0x7f0d0238

.field public static final label_sign_in:I = 0x7f0d0239

.field public static final label_skip:I = 0x7f0d023a

.field public static final label_slow:I = 0x7f0d023b

.field public static final label_social:I = 0x7f0d023c

.field public static final label_stereo_input:I = 0x7f0d023d

.field public static final label_success:I = 0x7f0d023e

.field public static final label_switch_activity:I = 0x7f0d023f

.field public static final label_switch_remote_activity:I = 0x7f0d0240

.field public static final label_test_setup:I = 0x7f0d0241

.field public static final label_try_later:I = 0x7f0d0242

.field public static final label_turn_to_this_channel:I = 0x7f0d0243

.field public static final label_tv_input:I = 0x7f0d0244

.field public static final label_use_input_toggle:I = 0x7f0d0245

.field public static final label_watch_on:I = 0x7f0d0246

.field public static final label_watch_on_gtv:I = 0x7f0d0247

.field public static final label_watch_on_nflx:I = 0x7f0d0248

.field public static final label_watch_on_tv:I = 0x7f0d0249

.field public static final label_watch_on_tv_all_caps:I = 0x7f0d024a

.field public static final last_cap:I = 0x7f0d024b

.field public static final last_show:I = 0x7f0d024c

.field public static final learn:I = 0x7f0d024d

.field public static final learn_code:I = 0x7f0d024e

.field public static final learn_new_code:I = 0x7f0d024f

.field public static final learned_ir_test:I = 0x7f0d0250

.field public static final lifestyle:I = 0x7f0d0251

.field public static final like:I = 0x7f0d0252

.field public static final link_samsung_account:I = 0x7f0d0253

.field public static final link_samsung_question:I = 0x7f0d0254

.field public static final list_cap:I = 0x7f0d0255

.field public static final livetv_cap:I = 0x7f0d0256

.field public static final living_room:I = 0x7f0d0480

.field public static final load_error_desc:I = 0x7f0d0257

.field public static final load_error_emoji:I = 0x7f0d0258

.field public static final load_error_title:I = 0x7f0d0259

.field public static final loading:I = 0x7f0d025a

.field public static final loading_brand_list:I = 0x7f0d025b

.field public static final loading_device_info:I = 0x7f0d025c

.field public static final loading_schedules_for_channel:I = 0x7f0d025d

.field public static final loadingwait:I = 0x7f0d025e

.field public static final location_client_powered_by_google:I = 0x7f0d025f

.field public static final locations_label:I = 0x7f0d0260

.field public static final lock_btn_ch:I = 0x7f0d0261

.field public static final lock_btn_vol:I = 0x7f0d0262

.field public static final lockscreen_fan_speed:I = 0x7f0d0263

.field public static final lockscreen_label:I = 0x7f0d0264

.field public static final lockscreen_temp:I = 0x7f0d0265

.field public static final lockscreen_widget:I = 0x7f0d0266

.field public static final logged_in_with:I = 0x7f0d0267

.field public static final login:I = 0x7f0d0268

.field public static final login_desc:I = 0x7f0d0269

.field public static final login_dialog_instructions_backup:I = 0x7f0d026a

.field public static final login_dialog_title:I = 0x7f0d026b

.field public static final login_fail:I = 0x7f0d026c

.field public static final login_failed:I = 0x7f0d026d

.field public static final login_fb:I = 0x7f0d026e

.field public static final login_gplus:I = 0x7f0d026f

.field public static final login_in:I = 0x7f0d0270

.field public static final login_noti:I = 0x7f0d0271

.field public static final login_pwd_empty:I = 0x7f0d0272

.field public static final login_samsung:I = 0x7f0d0273

.field public static final login_title:I = 0x7f0d0274

.field public static final login_twitter:I = 0x7f0d0275

.field public static final logo_str:I = 0x7f0d0276

.field public static final logout:I = 0x7f0d0277

.field public static final logout_netflix:I = 0x7f0d0278

.field public static final logout_twitter:I = 0x7f0d0279

.field public static final looking_roku:I = 0x7f0d027a

.field public static final mandatoryupdate:I = 0x7f0d027b

.field public static final manual_roku:I = 0x7f0d027c

.field public static final manual_roku_hint:I = 0x7f0d027d

.field public static final menu_cap:I = 0x7f0d027e

.field public static final message:I = 0x7f0d027f

.field public static final missing_ir_input_model:I = 0x7f0d0280

.field public static final missing_ir_input_subject:I = 0x7f0d0281

.field public static final missing_provoder_advice:I = 0x7f0d0282

.field public static final missing_tv_service:I = 0x7f0d0496

.field public static final missing_tv_service_confirmation:I = 0x7f0d0283

.field public static final mode:I = 0x7f0d0481

.field public static final mode_cap:I = 0x7f0d0284

.field public static final model_colon:I = 0x7f0d0285

.field public static final model_number:I = 0x7f0d0286

.field public static final monday:I = 0x7f0d0287

.field public static final more:I = 0x7f0d0288

.field public static final more_in:I = 0x7f0d0497

.field public static final more_info:I = 0x7f0d0289

.field public static final more_like_this:I = 0x7f0d028a

.field public static final more_options:I = 0x7f0d028b

.field public static final movie:I = 0x7f0d028c

.field public static final movies:I = 0x7f0d028d

.field public static final msg_dtv_list:I = 0x7f0d028e

.field public static final msg_dtv_not_found:I = 0x7f0d028f

.field public static final msg_wifi_unavailable:I = 0x7f0d0290

.field public static final multiple_schedules_title:I = 0x7f0d0291

.field public static final musicdance:I = 0x7f0d0292

.field public static final mute:I = 0x7f0d0293

.field public static final my_brand_tv:I = 0x7f0d0294

.field public static final my_device_missing:I = 0x7f0d0482

.field public static final my_device_on_device_input:I = 0x7f0d0295

.field public static final my_room:I = 0x7f0d0296

.field public static final mydevicemissing:I = 0x7f0d0297

.field public static final mytvservicemissing:I = 0x7f0d0298

.field public static final netflix_brand:I = 0x7f0d0299

.field public static final netflix_fail:I = 0x7f0d029a

.field public static final netflix_label:I = 0x7f0d029b

.field public static final netflix_login_error:I = 0x7f0d029c

.field public static final netflix_login_error_title:I = 0x7f0d029d

.field public static final netflix_picks:I = 0x7f0d029e

.field public static final netflix_queue_instructions:I = 0x7f0d029f

.field public static final netflix_queue_instructions_msg:I = 0x7f0d02a0

.field public static final netflix_signup:I = 0x7f0d02a1

.field public static final netflix_signup_error:I = 0x7f0d02a2

.field public static final netflix_signup_message:I = 0x7f0d02a3

.field public static final network_connect:I = 0x7f0d02a4

.field public static final network_connect_to_wlan:I = 0x7f0d02a5

.field public static final network_donot_show:I = 0x7f0d02a6

.field public static final network_extra_charge:I = 0x7f0d02a7

.field public static final network_flight_mode:I = 0x7f0d02a8

.field public static final network_mobile_title:I = 0x7f0d02a9

.field public static final network_mobile_warn:I = 0x7f0d02aa

.field public static final network_nosignal:I = 0x7f0d02ab

.field public static final network_roaming_enabled:I = 0x7f0d02ac

.field public static final network_roaming_title:I = 0x7f0d02ad

.field public static final network_roaming_warn:I = 0x7f0d02ae

.field public static final network_will_connect_to_wlan:I = 0x7f0d02af

.field public static final new_chapter:I = 0x7f0d02b0

.field public static final new_code:I = 0x7f0d02b1

.field public static final new_episode_only:I = 0x7f0d02b2

.field public static final new_episode_rerun:I = 0x7f0d02b3

.field public static final news:I = 0x7f0d02b4

.field public static final newstalk:I = 0x7f0d02b5

.field public static final newversionavailable:I = 0x7f0d02b6

.field public static final next:I = 0x7f0d02b7

.field public static final nextchannel:I = 0x7f0d02b8

.field public static final no:I = 0x7f0d02b9

.field public static final no_content:I = 0x7f0d02ba

.field public static final no_follow_team:I = 0x7f0d02bb

.field public static final no_internet:I = 0x7f0d02bc

.field public static final no_internet_alert:I = 0x7f0d02bd

.field public static final no_network:I = 0x7f0d02be

.field public static final no_preset_channel:I = 0x7f0d02bf

.field public static final no_results_for:I = 0x7f0d02c0

.field public static final no_schedules_for_channel:I = 0x7f0d02c1

.field public static final no_shows:I = 0x7f0d02c2

.field public static final no_shows_airing:I = 0x7f0d02c3

.field public static final no_tweets_available:I = 0x7f0d02c4

.field public static final nocutprogramcurrently:I = 0x7f0d02c5

.field public static final nofavchannel:I = 0x7f0d02c6

.field public static final nofavoriteprogramcurrently:I = 0x7f0d02c7

.field public static final nointernetconnectionalert:I = 0x7f0d02c8

.field public static final none:I = 0x7f0d02c9

.field public static final nonfiction:I = 0x7f0d02ca

.field public static final noschedulesavailable:I = 0x7f0d02cb

.field public static final not_available_title:I = 0x7f0d02cc

.field public static final not_configured:I = 0x7f0d02cd

.field public static final not_found_roku:I = 0x7f0d02ce

.field public static final not_to_be_missed:I = 0x7f0d02cf

.field public static final notapplicable:I = 0x7f0d02d0

.field public static final nothanks:I = 0x7f0d02d1

.field public static final noti_playing_channel:I = 0x7f0d02d2

.field public static final noti_playing_endtime:I = 0x7f0d02d3

.field public static final noti_playing_watchon:I = 0x7f0d02d4

.field public static final notification_panel:I = 0x7f0d02d5

.field public static final notification_widget_setup_btn:I = 0x7f0d02d6

.field public static final notification_widget_setup_title:I = 0x7f0d02d7

.field public static final notifications:I = 0x7f0d02d8

.field public static final notnow:I = 0x7f0d02d9

.field public static final now_airing:I = 0x7f0d02da

.field public static final now_airling_time:I = 0x7f0d02db

.field public static final ok:I = 0x7f0d02dc

.field public static final okay:I = 0x7f0d02dd

.field public static final on_air:I = 0x7f0d02de

.field public static final on_now:I = 0x7f0d02df

.field public static final ondemand:I = 0x7f0d02e0

.field public static final ondemand_cap:I = 0x7f0d02e1

.field public static final online_video:I = 0x7f0d02e2

.field public static final onlyoneweekscheduleavailable:I = 0x7f0d02e3

.field public static final onrightnow:I = 0x7f0d02e4

.field public static final option_cap:I = 0x7f0d02e5

.field public static final option_channel_guide:I = 0x7f0d02e6

.field public static final option_recently_watched:I = 0x7f0d02e7

.field public static final or:I = 0x7f0d02e8

.field public static final other:I = 0x7f0d02e9

.field public static final other_countries:I = 0x7f0d02ea

.field public static final other_device:I = 0x7f0d0483

.field public static final other_device_brand:I = 0x7f0d02eb

.field public static final other_episodes:I = 0x7f0d02ec

.field public static final other_episodes_available:I = 0x7f0d02ed

.field public static final other_shows:I = 0x7f0d02ee

.field public static final other_sports:I = 0x7f0d02ef

.field public static final others_on_peel:I = 0x7f0d02f0

.field public static final othertelevisionbrand:I = 0x7f0d02f1

.field public static final overview:I = 0x7f0d02f2

.field public static final pair:I = 0x7f0d02f3

.field public static final pairing:I = 0x7f0d02f4

.field public static final peel_account:I = 0x7f0d0498

.field public static final peel_disclaimer_body:I = 0x7f0d02f5

.field public static final peel_disclaimer_body1:I = 0x7f0d02f6

.field public static final peel_disclaimer_body2:I = 0x7f0d02f7

.field public static final peel_disclaimer_header:I = 0x7f0d02f8

.field public static final peel_first_screen:I = 0x7f0d02f9

.field public static final peel_has_sent_code:I = 0x7f0d02fa

.field public static final peel_has_switched_input:I = 0x7f0d02fb

.field public static final pending_remote:I = 0x7f0d02fc

.field public static final pending_remote_desc:I = 0x7f0d02fd

.field public static final permdesc_remote_control:I = 0x7f0d02fe

.field public static final personalization:I = 0x7f0d02ff

.field public static final personalize:I = 0x7f0d0300

.field public static final personalize_offer:I = 0x7f0d0301

.field public static final personalize_offer_for_us:I = 0x7f0d0302

.field public static final pick_top_fav_channels:I = 0x7f0d0303

.field public static final pick_top_fav_shows:I = 0x7f0d0304

.field public static final pin_disable_hint:I = 0x7f0d0305

.field public static final pin_your_remote_description:I = 0x7f0d0306

.field public static final pin_your_remote_title:I = 0x7f0d0307

.field public static final playpause:I = 0x7f0d0308

.field public static final please_enter_brand:I = 0x7f0d0309

.field public static final please_enter_model:I = 0x7f0d030a

.field public static final please_wait:I = 0x7f0d030b

.field public static final pleasedescribeproblem:I = 0x7f0d030c

.field public static final pleasedescribewhat:I = 0x7f0d030d

.field public static final pleaseenterroomname:I = 0x7f0d030e

.field public static final pleaseselect:I = 0x7f0d030f

.field public static final point_at_tv_msg:I = 0x7f0d0310

.field public static final popmenu_cap:I = 0x7f0d0311

.field public static final popular_channels:I = 0x7f0d0312

.field public static final popular_tv:I = 0x7f0d0313

.field public static final popular_twitter:I = 0x7f0d0314

.field public static final post_tweet:I = 0x7f0d0315

.field public static final power:I = 0x7f0d0316

.field public static final power_dlg_msg:I = 0x7f0d0317

.field public static final pr_chapter:I = 0x7f0d0318

.field public static final premium_channel_error:I = 0x7f0d0319

.field public static final preset_keys:I = 0x7f0d031a

.field public static final preset_keys_desc:I = 0x7f0d031b

.field public static final preset_keys_question:I = 0x7f0d031c

.field public static final preset_keys_title:I = 0x7f0d031d

.field public static final prism:I = 0x7f0d031e

.field public static final privacy:I = 0x7f0d031f

.field public static final privacy_policy:I = 0x7f0d0320

.field public static final profile:I = 0x7f0d0321

.field public static final provider:I = 0x7f0d0322

.field public static final queue_program:I = 0x7f0d0323

.field public static final rate:I = 0x7f0d0324

.field public static final rate_cancel:I = 0x7f0d0325

.field public static final rate_message:I = 0x7f0d0326

.field public static final rate_title:I = 0x7f0d0327

.field public static final ratepeel:I = 0x7f0d0328

.field public static final reality:I = 0x7f0d0329

.field public static final record_episode:I = 0x7f0d032a

.field public static final record_series:I = 0x7f0d032b

.field public static final recorded_on:I = 0x7f0d032c

.field public static final recording_request:I = 0x7f0d032d

.field public static final refresh:I = 0x7f0d032e

.field public static final related_tags:I = 0x7f0d032f

.field public static final remind_me_later:I = 0x7f0d0330

.field public static final reminder:I = 0x7f0d0331

.field public static final reminder_15min:I = 0x7f0d0332

.field public static final reminder_1day:I = 0x7f0d0333

.field public static final reminder_1hour:I = 0x7f0d0334

.field public static final reminder_1week:I = 0x7f0d0335

.field public static final reminder_2days:I = 0x7f0d0336

.field public static final reminder_30min:I = 0x7f0d0337

.field public static final reminder_5min:I = 0x7f0d0338

.field public static final reminder_customize:I = 0x7f0d0339

.field public static final reminder_is_set:I = 0x7f0d033a

.field public static final reminder_off:I = 0x7f0d033b

.field public static final reminder_ontime:I = 0x7f0d033c

.field public static final reminders:I = 0x7f0d033d

.field public static final remote_channel_guide_trigger:I = 0x7f0d033e

.field public static final remote_pin_lock_screen_title:I = 0x7f0d033f

.field public static final remote_pin_title:I = 0x7f0d0340

.field public static final remote_power_all:I = 0x7f0d0341

.field public static final remotecontrol_caption_popup_capital:I = 0x7f0d0342

.field public static final remotecontrol_other_keypad:I = 0x7f0d0343

.field public static final remove:I = 0x7f0d0344

.field public static final removechannel:I = 0x7f0d0345

.field public static final rename:I = 0x7f0d0346

.field public static final reordergenres:I = 0x7f0d0347

.field public static final reordersports:I = 0x7f0d0348

.field public static final reply:I = 0x7f0d0349

.field public static final report_missing:I = 0x7f0d034a

.field public static final report_missing_brand:I = 0x7f0d034b

.field public static final report_missing_brand_desc:I = 0x7f0d034c

.field public static final report_missing_code:I = 0x7f0d034d

.field public static final report_missing_regions:I = 0x7f0d034e

.field public static final reset:I = 0x7f0d034f

.field public static final reset_peel:I = 0x7f0d0350

.field public static final resetpeelapp:I = 0x7f0d0351

.field public static final retrieve_control_codes:I = 0x7f0d0352

.field public static final retry:I = 0x7f0d0353

.field public static final return_cap:I = 0x7f0d0354

.field public static final retweet:I = 0x7f0d0355

.field public static final rf_stb_pairing_message:I = 0x7f0d0356

.field public static final rf_stb_pairing_title:I = 0x7f0d0357

.field public static final roku_channels:I = 0x7f0d0358

.field public static final roku_find_desc:I = 0x7f0d0359

.field public static final room_name:I = 0x7f0d035a

.field public static final room_name_exists_msg:I = 0x7f0d035b

.field public static final room_name_exists_title:I = 0x7f0d035c

.field public static final rooms:I = 0x7f0d035d

.field public static final samsung_account:I = 0x7f0d035e

.field public static final samsung_disclaimer_body:I = 0x7f0d0484

.field public static final samsung_disclaimer_header:I = 0x7f0d0485

.field public static final samsung_label:I = 0x7f0d035f

.field public static final saturday:I = 0x7f0d0360

.field public static final save_config:I = 0x7f0d0361

.field public static final save_setup:I = 0x7f0d0362

.field public static final saving_device_info:I = 0x7f0d0363

.field public static final schedule_recodring_msg:I = 0x7f0d0364

.field public static final schedule_recording:I = 0x7f0d0365

.field public static final scififantasy:I = 0x7f0d0366

.field public static final search:I = 0x7f0d0367

.field public static final search_peel_hint:I = 0x7f0d0368

.field public static final searching_for:I = 0x7f0d0369

.field public static final season_name:I = 0x7f0d036a

.field public static final season_number:I = 0x7f0d036b

.field public static final select_command:I = 0x7f0d036c

.field public static final select_device:I = 0x7f0d036d

.field public static final select_device_brand:I = 0x7f0d036e

.field public static final select_region:I = 0x7f0d036f

.field public static final select_region_for_providers:I = 0x7f0d0370

.field public static final select_resolution:I = 0x7f0d04ab

.field public static final select_resolution_languages:I = 0x7f0d0371

.field public static final select_service_providers:I = 0x7f0d0372

.field public static final select_subregion:I = 0x7f0d0373

.field public static final send:I = 0x7f0d0374

.field public static final send_feedback:I = 0x7f0d0375

.field public static final send_feedback_dialog_message:I = 0x7f0d0376

.field public static final send_next_code:I = 0x7f0d0377

.field public static final send_prev_code:I = 0x7f0d0378

.field public static final sendcomment:I = 0x7f0d0379

.field public static final sendfeedback:I = 0x7f0d037a

.field public static final sending_ir:I = 0x7f0d037b

.field public static final sent_code_msg:I = 0x7f0d0486

.field public static final service_provider_name:I = 0x7f0d037c

.field public static final set:I = 0x7f0d037d

.field public static final set_input:I = 0x7f0d037e

.field public static final set_time:I = 0x7f0d037f

.field public static final set_up:I = 0x7f0d0380

.field public static final set_up_device_input_now:I = 0x7f0d0381

.field public static final set_up_input_for_device:I = 0x7f0d0382

.field public static final set_up_now:I = 0x7f0d0487

.field public static final set_up_tv_msg:I = 0x7f0d0488

.field public static final set_up_txt:I = 0x7f0d0383

.field public static final setting:I = 0x7f0d0384

.field public static final settings_account:I = 0x7f0d0385

.field public static final settings_adddevice_choose:I = 0x7f0d0386

.field public static final settings_basic_info:I = 0x7f0d0387

.field public static final settings_dislied_programs:I = 0x7f0d0388

.field public static final settings_followed_team:I = 0x7f0d0389

.field public static final settings_genres:I = 0x7f0d038a

.field public static final settings_liked_programs:I = 0x7f0d038b

.field public static final settings_social_accounts:I = 0x7f0d038c

.field public static final settings_sports:I = 0x7f0d038d

.field public static final setup_device_now:I = 0x7f0d038e

.field public static final setup_newdevice:I = 0x7f0d038f

.field public static final setup_now:I = 0x7f0d0489

.field public static final setup_roku_ir:I = 0x7f0d0390

.field public static final setup_sso_save_setup:I = 0x7f0d0391

.field public static final setup_sso_tv_direction:I = 0x7f0d0392

.field public static final setup_sso_tv_hello:I = 0x7f0d0393

.field public static final setup_sso_tv_login:I = 0x7f0d0394

.field public static final setup_sso_tv_skip:I = 0x7f0d0395

.field public static final setup_sso_tv_tagline:I = 0x7f0d0396

.field public static final setup_stb_dialog_msg:I = 0x7f0d0397

.field public static final setup_stb_dialog_title:I = 0x7f0d0398

.field public static final shortdescriptivesub:I = 0x7f0d0399

.field public static final shortlabel_hd:I = 0x7f0d039a

.field public static final shortlabel_sd:I = 0x7f0d039b

.field public static final should_preset_desc:I = 0x7f0d039c

.field public static final signin_samsung_question:I = 0x7f0d039d

.field public static final signup_fail:I = 0x7f0d0499

.field public static final single_import:I = 0x7f0d039e

.field public static final sleep:I = 0x7f0d039f

.field public static final slow_cap:I = 0x7f0d03a0

.field public static final smart_hub:I = 0x7f0d03a1

.field public static final smart_hub_1:I = 0x7f0d03a2

.field public static final soap:I = 0x7f0d03a3

.field public static final soccer:I = 0x7f0d03a4

.field public static final social_activity:I = 0x7f0d03a5

.field public static final social_empty_friend:I = 0x7f0d03a6

.field public static final social_find_friend:I = 0x7f0d03a7

.field public static final social_find_friend_label:I = 0x7f0d03a8

.field public static final social_invite_address:I = 0x7f0d03a9

.field public static final social_invite_fb:I = 0x7f0d03aa

.field public static final social_invite_search:I = 0x7f0d03ab

.field public static final social_logout_confirm:I = 0x7f0d03ac

.field public static final social_pic_update:I = 0x7f0d03ad

.field public static final social_recommend:I = 0x7f0d03ae

.field public static final social_signup:I = 0x7f0d03af

.field public static final social_signup_head:I = 0x7f0d03b0

.field public static final social_signup_message:I = 0x7f0d03b1

.field public static final source:I = 0x7f0d03b2

.field public static final source_cap:I = 0x7f0d03b3

.field public static final special:I = 0x7f0d03b4

.field public static final sports:I = 0x7f0d03b5

.field public static final sports_error:I = 0x7f0d03b6

.field public static final sso_desc:I = 0x7f0d048a

.field public static final sso_title:I = 0x7f0d048b

.field public static final start_txt:I = 0x7f0d03b7

.field public static final stb_validation_msg:I = 0x7f0d03b8

.field public static final stereo_cycle_input_msg:I = 0x7f0d049d

.field public static final stereo_input_msg:I = 0x7f0d049c

.field public static final stereo_list_intro:I = 0x7f0d0494

.field public static final stereo_turn_on_msg:I = 0x7f0d048c

.field public static final stereo_type_msg:I = 0x7f0d048d

.field public static final stms_version:I = 0x7f0d048e

.field public static final streo_input:I = 0x7f0d03b9

.field public static final subject:I = 0x7f0d03ba

.field public static final submitabug:I = 0x7f0d03bb

.field public static final subtitle:I = 0x7f0d03bc

.field public static final summer_shows:I = 0x7f0d03bd

.field public static final sunday:I = 0x7f0d03be

.field public static final switch_to:I = 0x7f0d03bf

.field public static final switch_tv_source:I = 0x7f0d03c0

.field public static final switch_tv_source_msg:I = 0x7f0d03c1

.field public static final sync_menu_cap:I = 0x7f0d03c2

.field public static final synopsis:I = 0x7f0d03c3

.field public static final tag1:I = 0x7f0d03c4

.field public static final tag2:I = 0x7f0d03c5

.field public static final tag3:I = 0x7f0d03c6

.field public static final tag4:I = 0x7f0d03c7

.field public static final tag5:I = 0x7f0d03c8

.field public static final tag6:I = 0x7f0d03c9

.field public static final tag7:I = 0x7f0d03ca

.field public static final tag8:I = 0x7f0d03cb

.field public static final tag_live:I = 0x7f0d03cc

.field public static final tag_new:I = 0x7f0d03cd

.field public static final tag_premiere:I = 0x7f0d03ce

.field public static final talk:I = 0x7f0d03cf

.field public static final tap_here:I = 0x7f0d03d0

.field public static final tap_to_add:I = 0x7f0d03d1

.field public static final tap_to_config:I = 0x7f0d03d2

.field public static final taphere:I = 0x7f0d03d3

.field public static final temp_label:I = 0x7f0d03d4

.field public static final terms_of_use:I = 0x7f0d03d5

.field public static final terrestrial:I = 0x7f0d03d6

.field public static final test_label:I = 0x7f0d03d7

.field public static final test_question_msg:I = 0x7f0d03d8

.field public static final test_sequence_all:I = 0x7f0d03d9

.field public static final test_stb_make_sure_live_tv:I = 0x7f0d03da

.field public static final test_test_channel:I = 0x7f0d03db

.field public static final testing_btn_ch_text:I = 0x7f0d03dc

.field public static final testing_btn_ch_up:I = 0x7f0d03dd

.field public static final testing_btn_number:I = 0x7f0d03de

.field public static final testing_change_channel_msg:I = 0x7f0d048f

.field public static final testing_device:I = 0x7f0d03df

.field public static final testing_key_other:I = 0x7f0d03e0

.field public static final testing_key_power:I = 0x7f0d03e1

.field public static final testing_key_stb:I = 0x7f0d03e2

.field public static final testing_key_tune:I = 0x7f0d03e3

.field public static final testing_key_tune_channel_num:I = 0x7f0d03e4

.field public static final testing_question_stb:I = 0x7f0d03e5

.field public static final testing_question_work:I = 0x7f0d03e6

.field public static final testing_stb_list_no_stb:I = 0x7f0d03e7

.field public static final testing_stb_list_other_brand:I = 0x7f0d03e8

.field public static final testing_stb_list_title:I = 0x7f0d03e9

.field public static final testing_stb_msg:I = 0x7f0d03ea

.field public static final testing_try_number:I = 0x7f0d03eb

.field public static final testing_turn_on_msg:I = 0x7f0d03ec

.field public static final testing_tv_msg:I = 0x7f0d03ed

.field public static final text_hidden_text:I = 0x7f0d03ee

.field public static final theater_sync:I = 0x7f0d03ef

.field public static final thriller:I = 0x7f0d03f0

.field public static final thursday:I = 0x7f0d03f1

.field public static final time_am:I = 0x7f0d03f2

.field public static final time_pattern:I = 0x7f0d03f3

.field public static final time_pm:I = 0x7f0d03f4

.field public static final time_unit_hour:I = 0x7f0d03f5

.field public static final time_unit_min:I = 0x7f0d03f6

.field public static final timer_menu:I = 0x7f0d03f7

.field public static final title_add_device:I = 0x7f0d03f8

.field public static final title_adjust_delay:I = 0x7f0d03f9

.field public static final title_audio_control:I = 0x7f0d03fa

.field public static final title_choose_correct_lineup:I = 0x7f0d03fb

.field public static final title_choose_source:I = 0x7f0d03fc

.field public static final title_configure_inputs:I = 0x7f0d03fd

.field public static final title_confirmation:I = 0x7f0d03fe

.field public static final title_dtv_not_found:I = 0x7f0d03ff

.field public static final title_dtv_setup:I = 0x7f0d0400

.field public static final title_gtv_setup:I = 0x7f0d0401

.field public static final title_optimize_tunein:I = 0x7f0d0402

.field public static final title_set_reminder:I = 0x7f0d0403

.field public static final title_set_up_stereo_input_for_device:I = 0x7f0d0404

.field public static final title_set_up_tv_input_for_device:I = 0x7f0d0405

.field public static final title_wifi_unavailable:I = 0x7f0d0406

.field public static final toast_desc:I = 0x7f0d0407

.field public static final toast_description:I = 0x7f0d0408

.field public static final toast_msg:I = 0x7f0d0409

.field public static final toast_title:I = 0x7f0d040a

.field public static final today:I = 0x7f0d040b

.field public static final tomorrow:I = 0x7f0d040c

.field public static final tonight_on_tv:I = 0x7f0d040d

.field public static final tools:I = 0x7f0d040e

.field public static final tools_cap:I = 0x7f0d040f

.field public static final top_brands_name:I = 0x7f0d0410

.field public static final toppicks:I = 0x7f0d0411

.field public static final trending_tweet_label:I = 0x7f0d0412

.field public static final triggers_edit_channel_label:I = 0x7f0d0413

.field public static final triggers_edit_channel_title:I = 0x7f0d0414

.field public static final troubleshoot_channel:I = 0x7f0d0415

.field public static final try_again:I = 0x7f0d0416

.field public static final try_alternate_code_msg:I = 0x7f0d049a

.field public static final tuesday:I = 0x7f0d0417

.field public static final tune_in:I = 0x7f0d0418

.field public static final tunein_check_question:I = 0x7f0d0419

.field public static final turn_off_tv_msg:I = 0x7f0d041a

.field public static final turn_on_device:I = 0x7f0d041b

.field public static final turn_on_device_msg:I = 0x7f0d041c

.field public static final turn_on_tv_msg:I = 0x7f0d0490

.field public static final tutorial_btn:I = 0x7f0d041d

.field public static final tutorial_msg:I = 0x7f0d041e

.field public static final tutorial_msg_1:I = 0x7f0d041f

.field public static final tutorial_msg_2_step1:I = 0x7f0d0420

.field public static final tutorial_msg_2_step2:I = 0x7f0d0421

.field public static final tutorial_msg_3:I = 0x7f0d0422

.field public static final tutorial_title:I = 0x7f0d0423

.field public static final tutorial_title_1:I = 0x7f0d0424

.field public static final tutorial_title_2_step1:I = 0x7f0d0425

.field public static final tutorial_title_2_step2:I = 0x7f0d0426

.field public static final tv:I = 0x7f0d0427

.field public static final tv_remotecontrol_caption_active:I = 0x7f0d0428

.field public static final tv_remotecontrol_caption_back:I = 0x7f0d0429

.field public static final tv_remotecontrol_caption_capital_back:I = 0x7f0d042a

.field public static final tv_remotecontrol_caption_capital_exit:I = 0x7f0d042b

.field public static final tv_remotecontrol_caption_capital_guide:I = 0x7f0d042c

.field public static final tv_remotecontrol_caption_capital_menu:I = 0x7f0d042d

.field public static final tv_remotecontrol_caption_exit:I = 0x7f0d042e

.field public static final tv_remotecontrol_caption_guide:I = 0x7f0d042f

.field public static final tv_remotecontrol_caption_info:I = 0x7f0d0430

.field public static final tv_remotecontrol_caption_livetv:I = 0x7f0d0431

.field public static final tv_remotecontrol_caption_menu:I = 0x7f0d0432

.field public static final tv_remotecontrol_caption_source:I = 0x7f0d0433

.field public static final tv_remotecontrol_caption_tivo:I = 0x7f0d0434

.field public static final tv_remotecontrol_caption_watch_livetv:I = 0x7f0d0435

.field public static final tv_tuner_have_dvr:I = 0x7f0d0436

.field public static final tv_tuner_no_dvr:I = 0x7f0d0437

.field public static final tv_tuner_tv:I = 0x7f0d0438

.field public static final tvshow:I = 0x7f0d0439

.field public static final tvshows:I = 0x7f0d043a

.field public static final tweet:I = 0x7f0d043b

.field public static final tweet_limit:I = 0x7f0d043c

.field public static final tweet_stream_title:I = 0x7f0d043d

.field public static final twitter_label:I = 0x7f0d043e

.field public static final twitter_login_desc:I = 0x7f0d043f

.field public static final twitter_signup:I = 0x7f0d0440

.field public static final twitter_trending:I = 0x7f0d0441

.field public static final unable_get_lineups:I = 0x7f0d0442

.field public static final undo_reminder:I = 0x7f0d0443

.field public static final universal_remote_control:I = 0x7f0d0444

.field public static final unknown_season:I = 0x7f0d0445

.field public static final unlink_samsung_account:I = 0x7f0d0446

.field public static final unlink_samsung_question:I = 0x7f0d0447

.field public static final unlisted_provider_above:I = 0x7f0d0448

.field public static final unqueue_program:I = 0x7f0d0449

.field public static final upcoming:I = 0x7f0d044a

.field public static final update_channel_number_desc:I = 0x7f0d044b

.field public static final update_channel_number_title:I = 0x7f0d044c

.field public static final updateavailable:I = 0x7f0d044d

.field public static final upgrade:I = 0x7f0d044e

.field public static final use_tv_as_display:I = 0x7f0d044f

.field public static final user_follow:I = 0x7f0d0450

.field public static final user_unfollow:I = 0x7f0d0451

.field public static final using_peel:I = 0x7f0d0452

.field public static final variety:I = 0x7f0d0453

.field public static final view_less:I = 0x7f0d0454

.field public static final view_more:I = 0x7f0d0455

.field public static final vod_episode_list_watch_on:I = 0x7f0d0456

.field public static final vod_view_more:I = 0x7f0d0457

.field public static final volume_change_label:I = 0x7f0d0458

.field public static final volume_controlled_on:I = 0x7f0d0459

.field public static final war:I = 0x7f0d045a

.field public static final warning:I = 0x7f0d045b

.field public static final watch_fmt:I = 0x7f0d045c

.field public static final watch_trailer:I = 0x7f0d045d

.field public static final watching:I = 0x7f0d045e

.field public static final weather:I = 0x7f0d045f

.field public static final wednesday:I = 0x7f0d0460

.field public static final weekend_tv:I = 0x7f0d0461

.field public static final welcome_watchon:I = 0x7f0d0462

.field public static final welovetohear:I = 0x7f0d0463

.field public static final western:I = 0x7f0d0464

.field public static final what_channel_changer:I = 0x7f0d0465

.field public static final what_is_happening:I = 0x7f0d0466

.field public static final which_volume_control:I = 0x7f0d0467

.field public static final widget_setup_intro:I = 0x7f0d0468

.field public static final widget_setup_tap_label:I = 0x7f0d0469

.field public static final widget_setup_text:I = 0x7f0d046a

.field public static final wifi:I = 0x7f0d046b

.field public static final wifi_googletv_msg:I = 0x7f0d046c

.field public static final wifi_googletv_title:I = 0x7f0d046d

.field public static final withinthelast24hoursavailable:I = 0x7f0d046e

.field public static final write_reply:I = 0x7f0d046f

.field public static final yes:I = 0x7f0d0470

.field public static final yes_i_do:I = 0x7f0d0471

.field public static final yes_it_did:I = 0x7f0d0472

.field public static final yesterday:I = 0x7f0d0473

.field public static final you_may_skip:I = 0x7f0d0474

.field public static final your_shows_with_one_tap:I = 0x7f0d0475

.field public static final your_universal_remote_control:I = 0x7f0d0476

.field public static final youremailaddress:I = 0x7f0d0477

.field public static final zipcode_validation_msg:I = 0x7f0d0478

.field public static final zipcode_validation_msg_without_zipcode:I = 0x7f0d0479
