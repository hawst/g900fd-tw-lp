.class final enum Lcom/peel/ui/aw;
.super Ljava/lang/Enum;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/peel/ui/aw;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/peel/ui/aw;

.field public static final enum b:Lcom/peel/ui/aw;

.field private static final synthetic c:[Lcom/peel/ui/aw;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 46
    new-instance v0, Lcom/peel/ui/aw;

    const-string/jumbo v1, "HIDE"

    invoke-direct {v0, v1, v2}, Lcom/peel/ui/aw;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/peel/ui/aw;->a:Lcom/peel/ui/aw;

    new-instance v0, Lcom/peel/ui/aw;

    const-string/jumbo v1, "IGNORE"

    invoke-direct {v0, v1, v3}, Lcom/peel/ui/aw;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/peel/ui/aw;->b:Lcom/peel/ui/aw;

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/peel/ui/aw;

    sget-object v1, Lcom/peel/ui/aw;->a:Lcom/peel/ui/aw;

    aput-object v1, v0, v2

    sget-object v1, Lcom/peel/ui/aw;->b:Lcom/peel/ui/aw;

    aput-object v1, v0, v3

    sput-object v0, Lcom/peel/ui/aw;->c:[Lcom/peel/ui/aw;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 46
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/peel/ui/aw;
    .locals 1

    .prologue
    .line 46
    const-class v0, Lcom/peel/ui/aw;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/peel/ui/aw;

    return-object v0
.end method

.method public static values()[Lcom/peel/ui/aw;
    .locals 1

    .prologue
    .line 46
    sget-object v0, Lcom/peel/ui/aw;->c:[Lcom/peel/ui/aw;

    invoke-virtual {v0}, [Lcom/peel/ui/aw;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/peel/ui/aw;

    return-object v0
.end method
