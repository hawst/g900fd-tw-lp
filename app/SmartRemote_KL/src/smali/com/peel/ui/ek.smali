.class public Lcom/peel/ui/ek;
.super Landroid/support/v4/view/av;


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private b:Landroid/view/LayoutInflater;

.field private c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/os/Bundle;",
            ">;"
        }
    .end annotation
.end field

.field private d:[Lcom/peel/data/Channel;

.field private e:Lcom/peel/content/library/Library;

.field private f:Lcom/peel/data/ContentRoom;

.field private g:Z

.field private h:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private i:Lcom/peel/ui/a/q;

.field private j:I

.field private k:Z

.field private l:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 50
    const-class v0, Lcom/peel/ui/ek;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/peel/ui/ek;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/view/LayoutInflater;ZLcom/peel/ui/a/q;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 65
    invoke-direct {p0}, Landroid/support/v4/view/av;-><init>()V

    .line 57
    iput-boolean v1, p0, Lcom/peel/ui/ek;->g:Z

    .line 58
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/peel/ui/ek;->h:Ljava/util/ArrayList;

    .line 60
    iput v1, p0, Lcom/peel/ui/ek;->j:I

    .line 66
    iput-object p1, p0, Lcom/peel/ui/ek;->b:Landroid/view/LayoutInflater;

    .line 67
    iput-boolean p2, p0, Lcom/peel/ui/ek;->g:Z

    .line 68
    iput-object p3, p0, Lcom/peel/ui/ek;->i:Lcom/peel/ui/a/q;

    .line 69
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/peel/ui/ek;->l:Ljava/util/ArrayList;

    .line 70
    return-void
.end method

.method private a(IJZLcom/peel/util/t;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IJZ",
            "Lcom/peel/util/t",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/peel/content/listing/Listing;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    .line 508
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 509
    const-string/jumbo v1, "path"

    const-string/jumbo v2, "listing/genre"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 510
    const-string/jumbo v1, "window"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 511
    const-string/jumbo v1, "start"

    invoke-virtual {v0, v1, p2, p3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 512
    const-string/jumbo v1, "room"

    iget-object v2, p0, Lcom/peel/ui/ek;->f:Lcom/peel/data/ContentRoom;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 513
    const-string/jumbo v1, "cacheWindow"

    invoke-virtual {v0, v1, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 514
    if-eqz p4, :cond_0

    .line 515
    const-string/jumbo v1, "force_refresh"

    invoke-virtual {v0, v1, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 517
    :cond_0
    iget-object v1, p0, Lcom/peel/ui/ek;->e:Lcom/peel/content/library/Library;

    invoke-virtual {v1, v0, p5}, Lcom/peel/content/library/Library;->a(Landroid/os/Bundle;Lcom/peel/util/t;)V

    .line 518
    return-void
.end method

.method private a(Landroid/os/Bundle;Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/Bundle;",
            "Ljava/util/List",
            "<",
            "Lcom/peel/content/listing/Listing;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 521
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/content/listing/Listing;

    .line 522
    if-nez v0, :cond_0

    .line 523
    sget-object v0, Lcom/peel/ui/ek;->a:Ljava/lang/String;

    const-string/jumbo v1, "null listing"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 526
    :cond_0
    check-cast v0, Lcom/peel/content/listing/LiveListing;

    .line 527
    invoke-virtual {v0}, Lcom/peel/content/listing/LiveListing;->q()Ljava/lang/String;

    move-result-object v3

    .line 528
    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    .line 529
    if-nez v1, :cond_1

    .line 530
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 531
    invoke-virtual {p1, v3, v1}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 533
    :cond_1
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 537
    :cond_2
    const-string/jumbo v0, "has_listings"

    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 538
    return-void
.end method

.method static synthetic a(Lcom/peel/ui/ek;IJZLcom/peel/util/t;)V
    .locals 0

    .prologue
    .line 49
    invoke-direct/range {p0 .. p5}, Lcom/peel/ui/ek;->a(IJZLcom/peel/util/t;)V

    return-void
.end method

.method static synthetic a(Lcom/peel/ui/ek;Landroid/os/Bundle;Ljava/util/List;)V
    .locals 0

    .prologue
    .line 49
    invoke-direct {p0, p1, p2}, Lcom/peel/ui/ek;->a(Landroid/os/Bundle;Ljava/util/List;)V

    return-void
.end method

.method static synthetic a(Lcom/peel/ui/ek;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 49
    invoke-direct {p0, p1}, Lcom/peel/ui/ek;->a(Ljava/lang/String;)V

    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 541
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 542
    const-string/jumbo v1, "library"

    iget-object v2, p0, Lcom/peel/ui/ek;->e:Lcom/peel/content/library/Library;

    invoke-virtual {v2}, Lcom/peel/content/library/Library;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 543
    const-string/jumbo v1, "channel"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 544
    const-string/jumbo v1, "path"

    const-string/jumbo v2, "channel/uncut"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 545
    const-string/jumbo v1, "room"

    invoke-static {}, Lcom/peel/content/a;->a()Lcom/peel/data/ContentRoom;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 546
    sget-object v1, Lcom/peel/content/a;->f:Lcom/peel/content/user/User;

    new-instance v2, Lcom/peel/ui/ep;

    invoke-direct {v2, p0, v0}, Lcom/peel/ui/ep;-><init>(Lcom/peel/ui/ek;Landroid/os/Bundle;)V

    invoke-virtual {v1, v0, v2}, Lcom/peel/content/user/User;->a(Landroid/os/Bundle;Lcom/peel/util/t;)V

    .line 550
    return-void
.end method

.method static synthetic a(Lcom/peel/ui/ek;Z)Z
    .locals 0

    .prologue
    .line 49
    iput-boolean p1, p0, Lcom/peel/ui/ek;->k:Z

    return p1
.end method

.method static synthetic a(Lcom/peel/ui/ek;)[Lcom/peel/data/Channel;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/peel/ui/ek;->d:[Lcom/peel/data/Channel;

    return-object v0
.end method

.method static synthetic b(Lcom/peel/ui/ek;)Landroid/view/LayoutInflater;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/peel/ui/ek;->b:Landroid/view/LayoutInflater;

    return-object v0
.end method

.method static synthetic c(Lcom/peel/ui/ek;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/peel/ui/ek;->l:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic d(Lcom/peel/ui/ek;)I
    .locals 1

    .prologue
    .line 49
    iget v0, p0, Lcom/peel/ui/ek;->j:I

    return v0
.end method

.method static synthetic e(Lcom/peel/ui/ek;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/peel/ui/ek;->h:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic f(Lcom/peel/ui/ek;)Lcom/peel/content/library/Library;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/peel/ui/ek;->e:Lcom/peel/content/library/Library;

    return-object v0
.end method

.method static synthetic g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 49
    sget-object v0, Lcom/peel/ui/ek;->a:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public a(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 146
    const/4 v0, -0x2

    return v0
.end method

.method public a(Landroid/view/ViewGroup;I)Ljava/lang/Object;
    .locals 11

    .prologue
    const/4 v5, 0x0

    const/16 v10, 0xc

    const/4 v9, 0x0

    .line 162
    iget-object v0, p0, Lcom/peel/ui/ek;->h:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-le v0, p2, :cond_1

    .line 163
    iget-object v0, p0, Lcom/peel/ui/ek;->h:Ljava/util/ArrayList;

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 164
    if-eqz v0, :cond_1

    .line 165
    iget v1, p0, Lcom/peel/ui/ek;->j:I

    if-ne p2, v1, :cond_0

    iget-object v2, p0, Lcom/peel/ui/ek;->i:Lcom/peel/ui/a/q;

    sget v1, Lcom/peel/ui/fp;->epg_list:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/peel/widget/ObservableListView;

    invoke-virtual {v2, v1}, Lcom/peel/ui/a/q;->a(Lcom/peel/widget/ObservableListView;)V

    .line 224
    :cond_0
    :goto_0
    return-object v0

    .line 171
    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/peel/ui/ek;->h:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-gt v0, p2, :cond_2

    .line 172
    iget-object v0, p0, Lcom/peel/ui/ek;->h:Ljava/util/ArrayList;

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 174
    :cond_2
    iget-object v0, p0, Lcom/peel/ui/ek;->b:Landroid/view/LayoutInflater;

    sget v1, Lcom/peel/ui/fq;->epg_page:I

    invoke-virtual {v0, v1, p1, v9}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    .line 175
    sget v1, Lcom/peel/ui/fp;->epg_list:I

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/peel/widget/ObservableListView;

    .line 176
    iget v2, p0, Lcom/peel/ui/ek;->j:I

    if-ne p2, v2, :cond_3

    iget-object v2, p0, Lcom/peel/ui/ek;->i:Lcom/peel/ui/a/q;

    invoke-virtual {v2, v1}, Lcom/peel/ui/a/q;->a(Lcom/peel/widget/ObservableListView;)V

    .line 178
    :cond_3
    iget-object v2, p0, Lcom/peel/ui/ek;->c:Ljava/util/List;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/peel/ui/ek;->c:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge p2, v2, :cond_4

    .line 179
    iget-object v2, p0, Lcom/peel/ui/ek;->c:Ljava/util/List;

    invoke-interface {v2, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/os/Bundle;

    .line 180
    const-string/jumbo v3, "start"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v6

    .line 182
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v8

    .line 183
    invoke-virtual {v8, v6, v7}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 185
    iget-boolean v3, p0, Lcom/peel/ui/ek;->g:Z

    if-eqz v3, :cond_5

    .line 186
    sget v3, Lcom/peel/ui/fp;->timeslot_1:I

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    sget-object v4, Lcom/peel/util/x;->g:Ljava/lang/ThreadLocal;

    invoke-virtual {v4}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/text/SimpleDateFormat;

    invoke-virtual {v8}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 187
    const-string/jumbo v3, "window"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v3

    mul-int/lit8 v3, v3, 0x1e

    invoke-virtual {v8, v10, v3}, Ljava/util/Calendar;->add(II)V

    .line 188
    sget v3, Lcom/peel/ui/fp;->timeslot_2:I

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    sget-object v4, Lcom/peel/util/x;->g:Ljava/lang/ThreadLocal;

    invoke-virtual {v4}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/text/SimpleDateFormat;

    invoke-virtual {v8}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 195
    :goto_2
    new-instance v4, Lcom/peel/ui/eq;

    iget-object v3, p0, Lcom/peel/ui/ek;->c:Ljava/util/List;

    if-nez v3, :cond_6

    move-object v3, v5

    :goto_3
    iget-object v5, p0, Lcom/peel/ui/ek;->d:[Lcom/peel/data/Channel;

    invoke-direct {v4, p0, v3, v5}, Lcom/peel/ui/eq;-><init>(Lcom/peel/ui/ek;Landroid/os/Bundle;[Lcom/peel/data/Channel;)V

    invoke-virtual {v1, v4}, Lcom/peel/widget/ObservableListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 198
    const-string/jumbo v3, "has_listings"

    invoke-virtual {v2, v3, v9}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    if-nez v3, :cond_4

    .line 199
    sget-object v3, Lcom/peel/ui/ek;->a:Ljava/lang/String;

    const-string/jumbo v4, "epg library.get()"

    new-instance v5, Lcom/peel/ui/el;

    invoke-direct {v5, p0, v2, v1}, Lcom/peel/ui/el;-><init>(Lcom/peel/ui/ek;Landroid/os/Bundle;Lcom/peel/widget/ObservableListView;)V

    invoke-static {v3, v4, v5}, Lcom/peel/util/i;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setTag(Ljava/lang/Object;)V

    .line 222
    :cond_4
    iget-object v1, p0, Lcom/peel/ui/ek;->h:Ljava/util/ArrayList;

    invoke-virtual {v1, p2, v0}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 223
    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    goto/16 :goto_0

    .line 190
    :cond_5
    sget v3, Lcom/peel/ui/fp;->timeslot_1:I

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    sget-object v4, Lcom/peel/util/x;->f:Ljava/lang/ThreadLocal;

    invoke-virtual {v4}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/text/SimpleDateFormat;

    invoke-virtual {v8}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 191
    const-string/jumbo v3, "window"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v3

    mul-int/lit8 v3, v3, 0x1e

    invoke-virtual {v8, v10, v3}, Ljava/util/Calendar;->add(II)V

    .line 192
    sget v3, Lcom/peel/ui/fp;->timeslot_2:I

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    sget-object v4, Lcom/peel/util/x;->f:Ljava/lang/ThreadLocal;

    invoke-virtual {v4}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/text/SimpleDateFormat;

    invoke-virtual {v8}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2

    .line 195
    :cond_6
    iget-object v3, p0, Lcom/peel/ui/ek;->c:Ljava/util/List;

    invoke-interface {v3, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/os/Bundle;

    goto :goto_3
.end method

.method public a([Lcom/peel/data/Channel;)Ljava/util/ArrayList;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Lcom/peel/data/Channel;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 447
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 448
    array-length v2, p1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, p1, v0

    .line 449
    invoke-virtual {v3}, Lcom/peel/data/Channel;->l()Z

    move-result v4

    if-nez v4, :cond_0

    .line 450
    invoke-virtual {v3}, Lcom/peel/data/Channel;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 448
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 453
    :cond_1
    return-object v1
.end method

.method public a(I)V
    .locals 3

    .prologue
    .line 92
    iget-object v0, p0, Lcom/peel/ui/ek;->h:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt p1, v0, :cond_1

    .line 107
    :cond_0
    :goto_0
    return-void

    .line 94
    :cond_1
    iget-object v0, p0, Lcom/peel/ui/ek;->h:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 95
    if-eqz v0, :cond_0

    .line 97
    iget-object v2, p0, Lcom/peel/ui/ek;->i:Lcom/peel/ui/a/q;

    sget v1, Lcom/peel/ui/fp;->epg_list:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/peel/widget/ObservableListView;

    invoke-virtual {v2, v1}, Lcom/peel/ui/a/q;->b(Lcom/peel/widget/ObservableListView;)V

    .line 99
    iput p1, p0, Lcom/peel/ui/ek;->j:I

    .line 101
    iget-object v1, p0, Lcom/peel/ui/ek;->h:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    .line 102
    if-eqz v1, :cond_0

    .line 104
    iget-object v2, p0, Lcom/peel/ui/ek;->i:Lcom/peel/ui/a/q;

    sget v1, Lcom/peel/ui/fp;->epg_list:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/peel/widget/ObservableListView;

    invoke-virtual {v2, v1}, Lcom/peel/ui/a/q;->a(Lcom/peel/widget/ObservableListView;)V

    .line 105
    sget v1, Lcom/peel/ui/fp;->epg_list:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/peel/widget/ObservableListView;

    invoke-virtual {v0}, Lcom/peel/widget/ObservableListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lcom/peel/ui/eq;

    .line 106
    invoke-virtual {v0}, Lcom/peel/ui/eq;->notifyDataSetChanged()V

    goto :goto_0
.end method

.method public a(Landroid/view/ViewGroup;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 154
    move-object v0, p3

    check-cast v0, Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Future;

    check-cast v0, Ljava/util/concurrent/Future;

    .line 155
    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/concurrent/Future;->isDone()Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Ljava/util/concurrent/Future;->cancel(Z)Z

    .line 156
    :cond_0
    iget-object v0, p0, Lcom/peel/ui/ek;->h:Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-virtual {v0, p2, v1}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    move-object v0, p3

    .line 157
    check-cast v0, Landroid/view/View;

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 158
    iget-object v1, p0, Lcom/peel/ui/ek;->i:Lcom/peel/ui/a/q;

    check-cast p3, Landroid/view/View;

    sget v0, Lcom/peel/ui/fp;->epg_list:I

    invoke-virtual {p3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/peel/widget/ObservableListView;

    invoke-virtual {v1, v0}, Lcom/peel/ui/a/q;->b(Lcom/peel/widget/ObservableListView;)V

    .line 159
    return-void
.end method

.method public a(Lcom/peel/content/library/LiveLibrary;)V
    .locals 2

    .prologue
    .line 73
    sget-object v0, Lcom/peel/ui/ek;->a:Ljava/lang/String;

    const-string/jumbo v1, "Setting Library"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 74
    iput-object p1, p0, Lcom/peel/ui/ek;->e:Lcom/peel/content/library/Library;

    .line 76
    invoke-virtual {p1}, Lcom/peel/content/library/LiveLibrary;->i()[Lcom/peel/data/Channel;

    move-result-object v0

    .line 84
    iput-object v0, p0, Lcom/peel/ui/ek;->d:[Lcom/peel/data/Channel;

    .line 85
    return-void
.end method

.method public a(Ljava/util/Date;Ljava/util/ArrayList;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Date;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/os/Bundle;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 110
    iput-object p2, p0, Lcom/peel/ui/ek;->c:Ljava/util/List;

    .line 112
    invoke-static {}, Lcom/peel/content/a;->a()Lcom/peel/data/ContentRoom;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/ui/ek;->f:Lcom/peel/data/ContentRoom;

    .line 114
    iget-object v0, p0, Lcom/peel/ui/ek;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 115
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v2

    .line 116
    invoke-virtual {v2, p1}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 117
    const/4 v0, 0x5

    const/4 v1, 0x1

    invoke-virtual {v2, v0, v1}, Ljava/util/Calendar;->add(II)V

    .line 118
    const/16 v0, 0xb

    const/4 v1, -0x1

    invoke-virtual {v2, v0, v1}, Ljava/util/Calendar;->set(II)V

    .line 120
    invoke-virtual {p1}, Ljava/util/Date;->getTime()J

    move-result-wide v0

    .line 121
    :goto_0
    invoke-virtual {v2}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v4

    cmp-long v3, v0, v4

    if-gtz v3, :cond_0

    .line 122
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 123
    const-string/jumbo v4, "start"

    invoke-virtual {v3, v4, v0, v1}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 124
    const-string/jumbo v4, "window"

    const/4 v5, 0x2

    invoke-virtual {v3, v4, v5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 125
    iget-object v4, p0, Lcom/peel/ui/ek;->c:Ljava/util/List;

    invoke-interface {v4, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 126
    const-wide/32 v4, 0x36ee80

    add-long/2addr v0, v4

    .line 127
    goto :goto_0

    .line 130
    :cond_0
    invoke-virtual {p0}, Lcom/peel/ui/ek;->c()V

    .line 131
    return-void
.end method

.method public a(Landroid/view/View;Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 142
    invoke-virtual {p1, p2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public b()I
    .locals 1

    .prologue
    .line 138
    iget-object v0, p0, Lcom/peel/ui/ek;->c:Ljava/util/List;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/peel/ui/ek;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    goto :goto_0
.end method

.method public d()[Lcom/peel/data/Channel;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/peel/ui/ek;->d:[Lcom/peel/data/Channel;

    return-object v0
.end method

.method public e()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation

    .prologue
    .line 150
    iget-object v0, p0, Lcom/peel/ui/ek;->h:Ljava/util/ArrayList;

    return-object v0
.end method

.method public f()V
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 458
    iget-boolean v1, p0, Lcom/peel/ui/ek;->k:Z

    if-nez v1, :cond_0

    .line 498
    :goto_0
    return-void

    .line 462
    :cond_0
    iput-boolean v0, p0, Lcom/peel/ui/ek;->k:Z

    .line 465
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 466
    const-string/jumbo v2, "path"

    const-string/jumbo v3, "channels/lineup"

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 467
    const-string/jumbo v2, "library"

    iget-object v3, p0, Lcom/peel/ui/ek;->e:Lcom/peel/content/library/Library;

    invoke-virtual {v3}, Lcom/peel/content/library/Library;->e()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 468
    const-string/jumbo v2, "user"

    sget-object v3, Lcom/peel/content/a;->f:Lcom/peel/content/user/User;

    invoke-virtual {v3}, Lcom/peel/content/user/User;->x()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 469
    const-string/jumbo v2, "content_user"

    sget-object v3, Lcom/peel/content/a;->f:Lcom/peel/content/user/User;

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 470
    const-string/jumbo v2, "channels"

    iget-object v3, p0, Lcom/peel/ui/ek;->d:[Lcom/peel/data/Channel;

    invoke-virtual {p0, v3}, Lcom/peel/ui/ek;->a([Lcom/peel/data/Channel;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 471
    const-string/jumbo v2, "room"

    invoke-static {}, Lcom/peel/content/a;->a()Lcom/peel/data/ContentRoom;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 473
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 474
    iget-object v3, p0, Lcom/peel/ui/ek;->d:[Lcom/peel/data/Channel;

    array-length v4, v3

    :goto_1
    if-ge v0, v4, :cond_2

    aget-object v5, v3, v0

    .line 478
    invoke-virtual {v5}, Lcom/peel/data/Channel;->l()Z

    move-result v6

    if-nez v6, :cond_1

    .line 480
    invoke-virtual {v5}, Lcom/peel/data/Channel;->c()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, ","

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 474
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 484
    :cond_2
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    if-lez v0, :cond_3

    .line 485
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->deleteCharAt(I)Ljava/lang/StringBuilder;

    .line 488
    :cond_3
    const-string/jumbo v0, "prgids"

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v0, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 489
    sget-object v0, Lcom/peel/ui/ek;->a:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "prgids: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 492
    new-instance v0, Lcom/peel/ui/eo;

    invoke-direct {v0, p0}, Lcom/peel/ui/eo;-><init>(Lcom/peel/ui/ek;)V

    invoke-static {v1, v0}, Lcom/peel/content/a/j;->b(Landroid/os/Bundle;Lcom/peel/util/t;)V

    goto/16 :goto_0
.end method
