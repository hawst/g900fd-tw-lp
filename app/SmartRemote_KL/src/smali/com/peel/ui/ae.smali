.class public Lcom/peel/ui/ae;
.super Landroid/widget/ArrayAdapter;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lcom/peel/content/listing/Listing;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private b:Landroid/view/LayoutInflater;

.field private c:Ljava/lang/String;

.field private d:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27
    const-class v0, Lcom/peel/ui/ae;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/peel/ui/ae;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;ILjava/util/List;Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Ljava/util/List",
            "<",
            "Lcom/peel/content/listing/Listing;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 34
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 35
    iput-object p1, p0, Lcom/peel/ui/ae;->d:Landroid/content/Context;

    .line 36
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/ui/ae;->b:Landroid/view/LayoutInflater;

    .line 37
    iput-object p4, p0, Lcom/peel/ui/ae;->c:Ljava/lang/String;

    .line 38
    return-void
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 11

    .prologue
    const/4 v4, 0x1

    const/4 v10, 0x0

    const/4 v3, 0x0

    const v2, 0x54dff86

    .line 42
    if-nez p2, :cond_0

    iget-object v0, p0, Lcom/peel/ui/ae;->b:Landroid/view/LayoutInflater;

    sget v1, Lcom/peel/ui/fq;->program_search_row:I

    invoke-virtual {v0, v1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 43
    :cond_0
    invoke-virtual {p2, v2}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_8

    invoke-virtual {p2, v2}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/ui/ag;

    move-object v7, v0

    .line 44
    :goto_0
    invoke-virtual {p2, v2, v7}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 46
    iget-object v0, v7, Lcom/peel/ui/ag;->e:Ljava/util/concurrent/Future;

    if-eqz v0, :cond_1

    .line 48
    iget-object v0, v7, Lcom/peel/ui/ag;->e:Ljava/util/concurrent/Future;

    invoke-interface {v0, v4}, Ljava/util/concurrent/Future;->cancel(Z)Z

    .line 49
    iput-object v3, v7, Lcom/peel/ui/ag;->e:Ljava/util/concurrent/Future;

    .line 52
    :cond_1
    invoke-virtual {p0, p1}, Lcom/peel/ui/ae;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/peel/content/listing/Listing;

    .line 53
    iget-object v0, v7, Lcom/peel/ui/ag;->b:Landroid/widget/TextView;

    if-nez v0, :cond_2

    sget v0, Lcom/peel/ui/fp;->channel:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v7, Lcom/peel/ui/ag;->b:Landroid/widget/TextView;

    .line 57
    :cond_2
    iget-object v0, v7, Lcom/peel/ui/ag;->c:Landroid/widget/TextView;

    if-nez v0, :cond_3

    sget v0, Lcom/peel/ui/fp;->time:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v7, Lcom/peel/ui/ag;->c:Landroid/widget/TextView;

    :cond_3
    move-object v0, v6

    .line 58
    check-cast v0, Lcom/peel/content/listing/LiveListing;

    invoke-virtual {v0}, Lcom/peel/content/listing/LiveListing;->o()J

    move-result-wide v0

    .line 59
    iget-object v2, p0, Lcom/peel/ui/ae;->c:Ljava/lang/String;

    sget-object v3, Lcom/peel/util/x;->n:[Ljava/lang/String;

    aget-object v3, v3, v4

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 60
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    .line 61
    cmp-long v4, v0, v2

    if-gtz v4, :cond_9

    invoke-virtual {v6}, Lcom/peel/content/listing/Listing;->j()J

    move-result-wide v4

    add-long/2addr v4, v0

    cmp-long v2, v4, v2

    if-lez v2, :cond_9

    .line 62
    iget-object v2, v7, Lcom/peel/ui/ag;->b:Landroid/widget/TextView;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/peel/ui/ae;->d:Landroid/content/Context;

    sget v5, Lcom/peel/ui/ft;->now_airing:I

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 63
    iget-object v2, v7, Lcom/peel/ui/ag;->b:Landroid/widget/TextView;

    invoke-virtual {v2, v10}, Landroid/widget/TextView;->setVisibility(I)V

    .line 68
    :cond_4
    :goto_1
    iget-object v8, v7, Lcom/peel/ui/ag;->c:Landroid/widget/TextView;

    invoke-virtual {v6}, Lcom/peel/content/listing/Listing;->j()J

    move-result-wide v2

    iget-object v4, p0, Lcom/peel/ui/ae;->d:Landroid/content/Context;

    invoke-static {v4}, Landroid/text/format/DateFormat;->is24HourFormat(Landroid/content/Context;)Z

    move-result v4

    iget-object v5, p0, Lcom/peel/ui/ae;->d:Landroid/content/Context;

    sget v9, Lcom/peel/ui/ft;->time_pattern:I

    invoke-virtual {v5, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static/range {v0 .. v5}, Lcom/peel/util/x;->a(JJZLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v8, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 69
    iget-object v0, v7, Lcom/peel/ui/ag;->c:Landroid/widget/TextView;

    invoke-virtual {v0, v10}, Landroid/widget/TextView;->setVisibility(I)V

    .line 71
    iget-object v0, v7, Lcom/peel/ui/ag;->a:Landroid/widget/TextView;

    if-nez v0, :cond_5

    sget v0, Lcom/peel/ui/fp;->title:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v7, Lcom/peel/ui/ag;->a:Landroid/widget/TextView;

    .line 72
    :cond_5
    iget-object v0, v7, Lcom/peel/ui/ag;->a:Landroid/widget/TextView;

    invoke-virtual {v6}, Lcom/peel/content/listing/Listing;->l()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 74
    const v0, 0x54dff70

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/peel/ui/ae;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "|"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v6}, Lcom/peel/content/listing/Listing;->f()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v0, v1}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 76
    iget-object v0, v7, Lcom/peel/ui/ag;->d:Landroid/widget/ImageView;

    if-nez v0, :cond_6

    sget v0, Lcom/peel/ui/fp;->icon:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, v7, Lcom/peel/ui/ag;->d:Landroid/widget/ImageView;

    :cond_6
    move-object v0, v6

    .line 78
    check-cast v0, Lcom/peel/content/listing/LiveListing;

    invoke-virtual {v0}, Lcom/peel/content/listing/LiveListing;->y()Ljava/lang/String;

    move-result-object v0

    .line 80
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 81
    const/4 v0, 0x3

    const/4 v1, 0x4

    const/16 v2, 0x10e

    check-cast v6, Lcom/peel/content/listing/LiveListing;

    invoke-virtual {v6}, Lcom/peel/content/listing/LiveListing;->B()Ljava/util/Map;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, Lcom/peel/util/bx;->a(IIILjava/util/Map;)Ljava/lang/String;

    move-result-object v0

    .line 84
    :cond_7
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_a

    .line 85
    iget-object v1, p0, Lcom/peel/ui/ae;->d:Landroid/content/Context;

    invoke-static {v1}, Lcom/peel/util/b/e;->a(Landroid/content/Context;)Lcom/squareup/picasso/Picasso;

    move-result-object v1

    .line 86
    invoke-virtual {v1, v0}, Lcom/squareup/picasso/Picasso;->load(Ljava/lang/String;)Lcom/squareup/picasso/RequestCreator;

    move-result-object v0

    sget v1, Lcom/peel/ui/fo;->genre_placeholder:I

    .line 87
    invoke-virtual {v0, v1}, Lcom/squareup/picasso/RequestCreator;->placeholder(I)Lcom/squareup/picasso/RequestCreator;

    move-result-object v0

    iget-object v1, v7, Lcom/peel/ui/ag;->d:Landroid/widget/ImageView;

    .line 88
    invoke-virtual {v0, v1}, Lcom/squareup/picasso/RequestCreator;->into(Landroid/widget/ImageView;)V

    .line 93
    :goto_2
    return-object p2

    .line 43
    :cond_8
    new-instance v0, Lcom/peel/ui/ag;

    invoke-direct {v0, v3}, Lcom/peel/ui/ag;-><init>(Lcom/peel/ui/af;)V

    move-object v7, v0

    goto/16 :goto_0

    .line 65
    :cond_9
    iget-object v2, v7, Lcom/peel/ui/ag;->b:Landroid/widget/TextView;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_1

    .line 90
    :cond_a
    iget-object v0, v7, Lcom/peel/ui/ag;->d:Landroid/widget/ImageView;

    sget v1, Lcom/peel/ui/fo;->genre_placeholder:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_2
.end method
