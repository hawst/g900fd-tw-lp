.class public Lcom/peel/ui/f;
.super Lcom/peel/d/u;


# instance fields
.field private aj:Lcom/peel/ui/a/q;

.field private ak:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private al:Lcom/peel/ui/ey;

.field private am:Ljava/util/Date;

.field private an:I

.field private ao:Ljava/util/Timer;

.field private ap:Ljava/util/TimerTask;

.field private aq:J

.field private ar:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final as:Landroid/support/v4/view/cs;

.field private at:Landroid/view/View;

.field private au:Landroid/widget/ListView;

.field private av:Z

.field private aw:Ljava/lang/Runnable;

.field private ax:Landroid/view/View;

.field e:Landroid/os/Handler;

.field private f:Landroid/support/v4/view/ViewPager;

.field private g:Lcom/peel/widget/ObservableListView;

.field private h:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/os/Bundle;",
            ">;"
        }
    .end annotation
.end field

.field private i:Landroid/view/LayoutInflater;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 56
    invoke-direct {p0}, Lcom/peel/d/u;-><init>()V

    .line 59
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/peel/ui/f;->h:Ljava/util/ArrayList;

    .line 65
    iput v1, p0, Lcom/peel/ui/f;->an:I

    .line 71
    new-instance v0, Lcom/peel/ui/g;

    invoke-direct {v0, p0}, Lcom/peel/ui/g;-><init>(Lcom/peel/ui/f;)V

    iput-object v0, p0, Lcom/peel/ui/f;->as:Landroid/support/v4/view/cs;

    .line 228
    iput-boolean v1, p0, Lcom/peel/ui/f;->av:Z

    .line 255
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/peel/ui/f;->e:Landroid/os/Handler;

    .line 256
    new-instance v0, Lcom/peel/ui/l;

    invoke-direct {v0, p0}, Lcom/peel/ui/l;-><init>(Lcom/peel/ui/f;)V

    iput-object v0, p0, Lcom/peel/ui/f;->aw:Ljava/lang/Runnable;

    .line 427
    return-void
.end method

.method static synthetic a(Lcom/peel/ui/f;)I
    .locals 1

    .prologue
    .line 56
    iget v0, p0, Lcom/peel/ui/f;->an:I

    return v0
.end method

.method static synthetic a(Lcom/peel/ui/f;I)I
    .locals 0

    .prologue
    .line 56
    iput p1, p0, Lcom/peel/ui/f;->an:I

    return p1
.end method

.method private a(Landroid/view/View;)V
    .locals 4

    .prologue
    .line 235
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/peel/ui/f;->av:Z

    .line 237
    iput-object p1, p0, Lcom/peel/ui/f;->at:Landroid/view/View;

    .line 238
    iget-object v0, p0, Lcom/peel/ui/f;->at:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/peel/ui/f;->au:Landroid/widget/ListView;

    .line 239
    sget v0, Lcom/peel/ui/fp;->rl_image_parent:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 240
    sget v0, Lcom/peel/ui/fp;->rl_image_parent:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/ui/f;->at:Landroid/view/View;

    .line 243
    :cond_0
    iget-object v0, p0, Lcom/peel/ui/f;->at:Landroid/view/View;

    sget v1, Lcom/peel/ui/fm;->common_signin_btn_dark_text_disabled:I

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    .line 244
    iget-object v0, p0, Lcom/peel/ui/f;->au:Landroid/widget/ListView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setEnabled(Z)V

    .line 246
    iget-object v0, p0, Lcom/peel/ui/f;->e:Landroid/os/Handler;

    iget-object v1, p0, Lcom/peel/ui/f;->aw:Ljava/lang/Runnable;

    const-wide/16 v2, 0xbb8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 247
    return-void
.end method

.method static synthetic a(Lcom/peel/ui/f;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 56
    invoke-direct {p0, p1}, Lcom/peel/ui/f;->a(Landroid/view/View;)V

    return-void
.end method

.method static synthetic a(Lcom/peel/ui/f;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 56
    invoke-direct {p0, p1}, Lcom/peel/ui/f;->a(Ljava/lang/String;)V

    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 568
    iget-object v0, p0, Lcom/peel/ui/f;->ar:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/16 v1, 0x64

    if-ne v0, v1, :cond_0

    .line 569
    iget-object v0, p0, Lcom/peel/ui/f;->ar:Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 571
    :cond_0
    iget-object v0, p0, Lcom/peel/ui/f;->ar:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 572
    iget-object v0, p0, Lcom/peel/ui/f;->ar:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 574
    :cond_1
    iget-object v0, p0, Lcom/peel/ui/f;->ar:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 576
    return-void
.end method

.method static synthetic b(Lcom/peel/ui/f;)Lcom/peel/ui/ey;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/peel/ui/f;->al:Lcom/peel/ui/ey;

    return-object v0
.end method

.method static synthetic c(Lcom/peel/ui/f;)Landroid/support/v4/view/ViewPager;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/peel/ui/f;->f:Landroid/support/v4/view/ViewPager;

    return-object v0
.end method

.method private c()V
    .locals 2

    .prologue
    .line 250
    iget-object v0, p0, Lcom/peel/ui/f;->at:Landroid/view/View;

    sget v1, Lcom/peel/ui/fm;->common_signin_btn_dark_text_default:I

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    .line 251
    iget-object v0, p0, Lcom/peel/ui/f;->au:Landroid/widget/ListView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setEnabled(Z)V

    .line 252
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/peel/ui/f;->av:Z

    .line 253
    return-void
.end method

.method static synthetic d(Lcom/peel/ui/f;)Landroid/support/v4/view/cs;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/peel/ui/f;->as:Landroid/support/v4/view/cs;

    return-object v0
.end method

.method static synthetic e(Lcom/peel/ui/f;)Lcom/peel/widget/ObservableListView;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/peel/ui/f;->g:Lcom/peel/widget/ObservableListView;

    return-object v0
.end method

.method static synthetic f(Lcom/peel/ui/f;)Lcom/peel/ui/a/q;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/peel/ui/f;->aj:Lcom/peel/ui/a/q;

    return-object v0
.end method

.method static synthetic g(Lcom/peel/ui/f;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/peel/ui/f;->a:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic h(Lcom/peel/ui/f;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/peel/ui/f;->a:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic i(Lcom/peel/ui/f;)V
    .locals 0

    .prologue
    .line 56
    invoke-direct {p0}, Lcom/peel/ui/f;->c()V

    return-void
.end method

.method static synthetic j(Lcom/peel/ui/f;)J
    .locals 2

    .prologue
    .line 56
    iget-wide v0, p0, Lcom/peel/ui/f;->aq:J

    return-wide v0
.end method

.method static synthetic k(Lcom/peel/ui/f;)Landroid/view/LayoutInflater;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/peel/ui/f;->i:Landroid/view/LayoutInflater;

    return-object v0
.end method

.method static synthetic l(Lcom/peel/ui/f;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/peel/ui/f;->ar:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic m(Lcom/peel/ui/f;)Z
    .locals 1

    .prologue
    .line 56
    iget-boolean v0, p0, Lcom/peel/ui/f;->av:Z

    return v0
.end method

.method static synthetic n(Lcom/peel/ui/f;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/peel/ui/f;->h:Ljava/util/ArrayList;

    return-object v0
.end method


# virtual methods
.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    .line 272
    sget v0, Lcom/peel/ui/fq;->channel_guide_sub_fragment:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/ui/f;->ax:Landroid/view/View;

    .line 273
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/peel/ui/f;->ar:Ljava/util/ArrayList;

    .line 274
    iget-object v0, p0, Lcom/peel/ui/f;->ax:Landroid/view/View;

    sget v1, Lcom/peel/ui/fp;->channels_list:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/peel/widget/ObservableListView;

    iput-object v0, p0, Lcom/peel/ui/f;->g:Lcom/peel/widget/ObservableListView;

    .line 275
    iget-object v0, p0, Lcom/peel/ui/f;->ax:Landroid/view/View;

    sget v1, Lcom/peel/ui/fp;->epg_pager:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ViewPager;

    iput-object v0, p0, Lcom/peel/ui/f;->f:Landroid/support/v4/view/ViewPager;

    .line 276
    iget-object v0, p0, Lcom/peel/ui/f;->f:Landroid/support/v4/view/ViewPager;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setOffscreenPageLimit(I)V

    .line 278
    iput-object p1, p0, Lcom/peel/ui/f;->i:Landroid/view/LayoutInflater;

    .line 280
    iget-object v0, p0, Lcom/peel/ui/f;->ax:Landroid/view/View;

    return-object v0
.end method

.method public a(Ljava/util/ArrayList;I)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;I)V"
        }
    .end annotation

    .prologue
    const/16 v7, 0x8

    const/4 v2, 0x0

    .line 383
    iput-object p1, p0, Lcom/peel/ui/f;->ak:Ljava/util/ArrayList;

    .line 384
    iget-object v0, p0, Lcom/peel/ui/f;->al:Lcom/peel/ui/ey;

    invoke-virtual {v0, p1, p2}, Lcom/peel/ui/ey;->a(Ljava/util/ArrayList;I)V

    .line 385
    iget-object v0, p0, Lcom/peel/ui/f;->al:Lcom/peel/ui/ey;

    invoke-virtual {v0}, Lcom/peel/ui/ey;->c()V

    .line 388
    iget-object v0, p0, Lcom/peel/ui/f;->g:Lcom/peel/widget/ObservableListView;

    invoke-virtual {v0}, Lcom/peel/widget/ObservableListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 389
    iget-object v0, p0, Lcom/peel/ui/f;->g:Lcom/peel/widget/ObservableListView;

    invoke-virtual {v0}, Lcom/peel/widget/ObservableListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lcom/peel/ui/q;

    invoke-static {v0, p2}, Lcom/peel/ui/q;->a(Lcom/peel/ui/q;I)V

    .line 391
    iget-object v0, p0, Lcom/peel/ui/f;->ak:Ljava/util/ArrayList;

    if-eqz v0, :cond_5

    .line 392
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 393
    iget-object v0, p0, Lcom/peel/ui/f;->al:Lcom/peel/ui/ey;

    invoke-virtual {v0}, Lcom/peel/ui/ey;->d()[Lcom/peel/data/Channel;

    move-result-object v3

    array-length v4, v3

    move v0, v2

    :goto_0
    if-ge v0, v4, :cond_0

    aget-object v5, v3, v0

    .line 394
    invoke-virtual {v5}, Lcom/peel/data/Channel;->a()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v1, v6, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 393
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 397
    :cond_0
    new-instance v3, Ljava/util/LinkedHashSet;

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    invoke-direct {v3, v0}, Ljava/util/LinkedHashSet;-><init>(I)V

    .line 398
    iget-object v0, p0, Lcom/peel/ui/f;->ak:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_1
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 399
    invoke-interface {v1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 400
    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 404
    :cond_2
    iget-object v0, p0, Lcom/peel/ui/f;->g:Lcom/peel/widget/ObservableListView;

    invoke-virtual {v0}, Lcom/peel/widget/ObservableListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lcom/peel/ui/q;

    invoke-interface {v3}, Ljava/util/Set;->size()I

    move-result v1

    new-array v1, v1, [Lcom/peel/data/Channel;

    invoke-interface {v3, v1}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Lcom/peel/data/Channel;

    invoke-virtual {v0, v1}, Lcom/peel/ui/q;->a([Lcom/peel/data/Channel;)V

    .line 405
    iget-object v0, p0, Lcom/peel/ui/f;->al:Lcom/peel/ui/ey;

    invoke-virtual {v0, p1, p2}, Lcom/peel/ui/ey;->a(Ljava/util/ArrayList;I)V

    .line 406
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_4

    .line 407
    iget-object v0, p0, Lcom/peel/ui/f;->ax:Landroid/view/View;

    sget v1, Lcom/peel/ui/fp;->empty:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    sget v1, Lcom/peel/ui/fp;->msg:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 408
    sget v1, Lcom/peel/ui/ft;->channel_remote_none:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 409
    iget-object v0, p0, Lcom/peel/ui/f;->ax:Landroid/view/View;

    sget v1, Lcom/peel/ui/fp;->empty:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 425
    :cond_3
    :goto_2
    return-void

    .line 411
    :cond_4
    iget-object v0, p0, Lcom/peel/ui/f;->ax:Landroid/view/View;

    sget v1, Lcom/peel/ui/fp;->empty:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v7}, Landroid/view/View;->setVisibility(I)V

    .line 412
    iget-object v0, p0, Lcom/peel/ui/f;->aj:Lcom/peel/ui/a/q;

    invoke-virtual {v0, v2}, Lcom/peel/ui/a/q;->b(I)V

    goto :goto_2

    .line 415
    :cond_5
    iget-object v0, p0, Lcom/peel/ui/f;->g:Lcom/peel/widget/ObservableListView;

    invoke-virtual {v0}, Lcom/peel/widget/ObservableListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lcom/peel/ui/q;

    iget-object v1, p0, Lcom/peel/ui/f;->al:Lcom/peel/ui/ey;

    invoke-virtual {v1}, Lcom/peel/ui/ey;->d()[Lcom/peel/data/Channel;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/peel/ui/q;->a([Lcom/peel/data/Channel;)V

    .line 416
    sget-object v0, Lcom/peel/b/a;->h:Ljava/lang/String;

    if-nez v0, :cond_6

    .line 417
    iget-object v0, p0, Lcom/peel/ui/f;->al:Lcom/peel/ui/ey;

    invoke-virtual {v0}, Lcom/peel/ui/ey;->d()[Lcom/peel/data/Channel;

    move-result-object v0

    iget v1, p0, Lcom/peel/ui/f;->an:I

    aget-object v0, v0, v1

    invoke-virtual {v0}, Lcom/peel/data/Channel;->a()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/peel/b/a;->h:Ljava/lang/String;

    .line 422
    :goto_3
    iget-object v0, p0, Lcom/peel/ui/f;->ax:Landroid/view/View;

    sget v1, Lcom/peel/ui/fp;->empty:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v7}, Landroid/view/View;->setVisibility(I)V

    goto :goto_2

    .line 419
    :cond_6
    sget-object v0, Lcom/peel/b/a;->h:Ljava/lang/String;

    iget-object v1, p0, Lcom/peel/ui/f;->al:Lcom/peel/ui/ey;

    invoke-virtual {v1}, Lcom/peel/ui/ey;->d()[Lcom/peel/data/Channel;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/peel/util/bx;->a(Ljava/lang/String;[Lcom/peel/data/Channel;)I

    move-result v0

    .line 420
    iget-object v1, p0, Lcom/peel/ui/f;->aj:Lcom/peel/ui/a/q;

    invoke-virtual {v1, v0}, Lcom/peel/ui/a/q;->b(I)V

    goto :goto_3
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 265
    invoke-super {p0}, Lcom/peel/d/u;->b()Z

    move-result v0

    return v0
.end method

.method public c(Landroid/os/Bundle;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/16 v5, 0xc

    const/4 v4, 0x0

    .line 112
    invoke-super {p0, p1}, Lcom/peel/d/u;->c(Landroid/os/Bundle;)V

    .line 114
    iget-object v0, p0, Lcom/peel/ui/f;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "refresh"

    invoke-virtual {v0, v1, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 116
    new-instance v0, Ljava/util/GregorianCalendar;

    invoke-direct {v0}, Ljava/util/GregorianCalendar;-><init>()V

    .line 117
    invoke-virtual {v0, v5}, Ljava/util/GregorianCalendar;->get(I)I

    move-result v1

    .line 124
    invoke-virtual {v0}, Ljava/util/GregorianCalendar;->getTimeInMillis()J

    move-result-wide v2

    const/16 v0, 0x1e

    if-ge v1, v0, :cond_5

    rsub-int/lit8 v0, v1, 0x1e

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    :goto_0
    mul-int/lit16 v0, v0, 0x3e8

    mul-int/lit8 v0, v0, 0x3c

    int-to-long v0, v0

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/peel/ui/f;->aq:J

    .line 127
    iget-object v0, p0, Lcom/peel/ui/f;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "refresh"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->remove(Ljava/lang/String;)V

    .line 129
    const-class v0, Lcom/peel/ui/du;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "initial selection"

    new-instance v2, Lcom/peel/ui/h;

    invoke-direct {v2, p0}, Lcom/peel/ui/h;-><init>(Lcom/peel/ui/f;)V

    invoke-static {v0, v1, v2}, Lcom/peel/util/i;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/lang/Runnable;

    .line 135
    iget-object v0, p0, Lcom/peel/ui/f;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "date_ms"

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    .line 136
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v2

    .line 138
    invoke-virtual {v2, v0, v1}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 139
    invoke-virtual {v2, v5}, Ljava/util/Calendar;->clear(I)V

    .line 140
    const/16 v0, 0xd

    invoke-virtual {v2, v0}, Ljava/util/Calendar;->clear(I)V

    .line 141
    const/16 v0, 0xe

    invoke-virtual {v2, v0}, Ljava/util/Calendar;->clear(I)V

    .line 143
    iget-object v0, p0, Lcom/peel/ui/f;->am:Ljava/util/Date;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/peel/ui/f;->am:Ljava/util/Date;

    invoke-virtual {v2}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/Date;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 144
    :cond_0
    iget-object v0, p0, Lcom/peel/ui/f;->h:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 147
    :cond_1
    invoke-virtual {v2}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/ui/f;->am:Ljava/util/Date;

    .line 149
    iget-object v1, p0, Lcom/peel/ui/f;->al:Lcom/peel/ui/ey;

    const-string/jumbo v0, "live"

    invoke-static {v0}, Lcom/peel/content/a;->c(Ljava/lang/String;)Lcom/peel/content/library/Library;

    move-result-object v0

    check-cast v0, Lcom/peel/content/library/LiveLibrary;

    invoke-virtual {v1, v0}, Lcom/peel/ui/ey;->a(Lcom/peel/content/library/LiveLibrary;)V

    .line 150
    iget-object v0, p0, Lcom/peel/ui/f;->al:Lcom/peel/ui/ey;

    iget-object v1, p0, Lcom/peel/ui/f;->am:Ljava/util/Date;

    iget-object v2, p0, Lcom/peel/ui/f;->h:Ljava/util/ArrayList;

    invoke-virtual {v0, v1, v2}, Lcom/peel/ui/ey;->a(Ljava/util/Date;Ljava/util/ArrayList;)V

    .line 151
    iget-object v0, p0, Lcom/peel/ui/f;->f:Landroid/support/v4/view/ViewPager;

    iget v1, p0, Lcom/peel/ui/f;->an:I

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    .line 153
    iget-object v0, p0, Lcom/peel/ui/f;->al:Lcom/peel/ui/ey;

    invoke-virtual {v0}, Lcom/peel/ui/ey;->d()[Lcom/peel/data/Channel;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 154
    new-instance v0, Lcom/peel/ui/q;

    invoke-direct {v0, p0, v6}, Lcom/peel/ui/q;-><init>(Lcom/peel/ui/f;Lcom/peel/ui/g;)V

    .line 155
    iget-object v1, p0, Lcom/peel/ui/f;->al:Lcom/peel/ui/ey;

    invoke-virtual {v1}, Lcom/peel/ui/ey;->d()[Lcom/peel/data/Channel;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/peel/ui/q;->a([Lcom/peel/data/Channel;)V

    .line 156
    iget-object v1, p0, Lcom/peel/ui/f;->g:Lcom/peel/widget/ObservableListView;

    invoke-virtual {v1, v0}, Lcom/peel/widget/ObservableListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 157
    iget-object v0, p0, Lcom/peel/ui/f;->ax:Landroid/view/View;

    sget v1, Lcom/peel/ui/fp;->empty:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 165
    :cond_2
    :goto_1
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_3

    .line 166
    iget-object v0, p0, Lcom/peel/ui/f;->g:Lcom/peel/widget/ObservableListView;

    iget-object v1, p0, Lcom/peel/ui/f;->c:Landroid/os/Bundle;

    const-string/jumbo v2, "first_visible"

    invoke-virtual {v1, v2, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    iget-object v2, p0, Lcom/peel/ui/f;->c:Landroid/os/Bundle;

    const-string/jumbo v3, "top"

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/peel/widget/ObservableListView;->setSelectionFromTop(II)V

    .line 169
    :cond_3
    iget-object v0, p0, Lcom/peel/ui/f;->aj:Lcom/peel/ui/a/q;

    iget-object v1, p0, Lcom/peel/ui/f;->g:Lcom/peel/widget/ObservableListView;

    invoke-virtual {v0, v1}, Lcom/peel/ui/a/q;->a(Lcom/peel/widget/ObservableListView;)V

    .line 170
    invoke-virtual {p0}, Lcom/peel/ui/f;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    const-string/jumbo v1, "edit_channel_popup_trigger"

    invoke-static {v0, v1}, Lcom/peel/util/dq;->b(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 171
    invoke-virtual {p0}, Lcom/peel/ui/f;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/ae;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lcom/peel/ui/fq;->trigger_action_edit_channels:I

    invoke-virtual {v0, v1, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 172
    new-instance v1, Lcom/peel/ui/i;

    invoke-direct {v1, p0}, Lcom/peel/ui/i;-><init>(Lcom/peel/ui/f;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 189
    :cond_4
    iget-object v0, p0, Lcom/peel/ui/f;->aj:Lcom/peel/ui/a/q;

    new-instance v1, Lcom/peel/ui/j;

    invoke-direct {v1, p0}, Lcom/peel/ui/j;-><init>(Lcom/peel/ui/f;)V

    invoke-virtual {v0, v1}, Lcom/peel/ui/a/q;->a(Lcom/peel/ui/a/t;)V

    .line 209
    iget-object v0, p0, Lcom/peel/ui/f;->g:Lcom/peel/widget/ObservableListView;

    invoke-virtual {v0}, Lcom/peel/widget/ObservableListView;->clearFocus()V

    .line 210
    iget-object v0, p0, Lcom/peel/ui/f;->g:Lcom/peel/widget/ObservableListView;

    new-instance v1, Lcom/peel/ui/k;

    invoke-direct {v1, p0}, Lcom/peel/ui/k;-><init>(Lcom/peel/ui/f;)V

    invoke-virtual {v0, v1}, Lcom/peel/widget/ObservableListView;->post(Ljava/lang/Runnable;)Z

    .line 224
    return-void

    .line 124
    :cond_5
    rsub-int/lit8 v0, v1, 0x3c

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    goto/16 :goto_0

    .line 159
    :cond_6
    iget-object v0, p0, Lcom/peel/ui/f;->ax:Landroid/view/View;

    sget v1, Lcom/peel/ui/fp;->empty:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    sget v1, Lcom/peel/ui/fp;->msg:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 160
    sget v1, Lcom/peel/ui/ft;->no_content:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 161
    iget-object v0, p0, Lcom/peel/ui/f;->ax:Landroid/view/View;

    sget v1, Lcom/peel/ui/fp;->empty:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_1
.end method

.method public d(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 107
    invoke-super {p0, p1}, Lcom/peel/d/u;->d(Landroid/os/Bundle;)V

    .line 108
    return-void
.end method

.method public h(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 291
    invoke-super {p0, p1}, Lcom/peel/d/u;->h(Landroid/os/Bundle;)V

    .line 292
    if-eqz p1, :cond_0

    const-string/jumbo v0, "bundle"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 293
    iget-object v0, p0, Lcom/peel/ui/f;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "bundle"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->putAll(Landroid/os/Bundle;)V

    .line 295
    iget-object v0, p0, Lcom/peel/ui/f;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "date_ms"

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    .line 296
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v2

    .line 297
    invoke-virtual {v2, v0, v1}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 298
    const/16 v0, 0xc

    invoke-virtual {v2, v0}, Ljava/util/Calendar;->clear(I)V

    .line 299
    const/16 v0, 0xd

    invoke-virtual {v2, v0}, Ljava/util/Calendar;->clear(I)V

    .line 300
    const/16 v0, 0xe

    invoke-virtual {v2, v0}, Ljava/util/Calendar;->clear(I)V

    .line 301
    invoke-virtual {v2}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/ui/f;->am:Ljava/util/Date;

    .line 304
    :cond_0
    iget-object v0, p0, Lcom/peel/ui/f;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "payloads"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 305
    iget-object v0, p0, Lcom/peel/ui/f;->h:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 306
    iget-object v0, p0, Lcom/peel/ui/f;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "payloads"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Parcelable;

    .line 307
    iget-object v2, p0, Lcom/peel/ui/f;->h:Ljava/util/ArrayList;

    check-cast v0, Landroid/os/Bundle;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 311
    :cond_1
    new-instance v0, Lcom/peel/ui/a/q;

    iget-object v1, p0, Lcom/peel/ui/f;->c:Landroid/os/Bundle;

    invoke-direct {v0, v1}, Lcom/peel/ui/a/q;-><init>(Landroid/os/Bundle;)V

    iput-object v0, p0, Lcom/peel/ui/f;->aj:Lcom/peel/ui/a/q;

    .line 312
    iget-object v0, p0, Lcom/peel/ui/f;->aj:Lcom/peel/ui/a/q;

    new-instance v1, Lcom/peel/ui/m;

    invoke-direct {v1, p0}, Lcom/peel/ui/m;-><init>(Lcom/peel/ui/f;)V

    invoke-virtual {v0, v1}, Lcom/peel/ui/a/q;->a(Lcom/peel/ui/a/u;)V

    .line 319
    new-instance v0, Lcom/peel/ui/ey;

    invoke-virtual {p0}, Lcom/peel/ui/f;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    iget-object v2, p0, Lcom/peel/ui/f;->i:Landroid/view/LayoutInflater;

    invoke-virtual {p0}, Lcom/peel/ui/f;->m()Landroid/support/v4/app/ae;

    move-result-object v3

    invoke-static {v3}, Landroid/text/format/DateFormat;->is24HourFormat(Landroid/content/Context;)Z

    move-result v3

    iget-object v4, p0, Lcom/peel/ui/f;->aj:Lcom/peel/ui/a/q;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/peel/ui/ey;-><init>(Landroid/content/Context;Landroid/view/LayoutInflater;ZLcom/peel/ui/a/q;)V

    iput-object v0, p0, Lcom/peel/ui/f;->al:Lcom/peel/ui/ey;

    .line 320
    iget-object v0, p0, Lcom/peel/ui/f;->f:Landroid/support/v4/view/ViewPager;

    iget-object v1, p0, Lcom/peel/ui/f;->al:Lcom/peel/ui/ey;

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setAdapter(Landroid/support/v4/view/av;)V

    .line 321
    iget-object v0, p0, Lcom/peel/ui/f;->f:Landroid/support/v4/view/ViewPager;

    iget-object v1, p0, Lcom/peel/ui/f;->as:Landroid/support/v4/view/cs;

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setOnPageChangeListener(Landroid/support/v4/view/cs;)V

    .line 323
    iget-object v0, p0, Lcom/peel/ui/f;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "selected"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/peel/ui/f;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "selected"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/peel/ui/f;->an:I

    .line 325
    :cond_2
    sget-object v0, Lcom/peel/content/a;->h:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 326
    iget-object v0, p0, Lcom/peel/ui/f;->b:Lcom/peel/d/i;

    if-eqz v0, :cond_3

    .line 327
    iget-object v0, p0, Lcom/peel/ui/f;->b:Lcom/peel/d/i;

    invoke-virtual {v0, v5}, Lcom/peel/d/i;->a(Z)V

    .line 330
    :cond_3
    iget-object v0, p0, Lcom/peel/ui/f;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "refresh"

    invoke-virtual {v0, v1, v5}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 331
    iget-object v0, p0, Lcom/peel/ui/f;->c:Landroid/os/Bundle;

    invoke-virtual {p0, v0}, Lcom/peel/ui/f;->c(Landroid/os/Bundle;)V

    .line 334
    :cond_4
    const-class v0, Lcom/peel/ui/du;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "insights"

    new-instance v2, Lcom/peel/ui/n;

    invoke-direct {v2, p0}, Lcom/peel/ui/n;-><init>(Lcom/peel/ui/f;)V

    invoke-static {v0, v1, v2}, Lcom/peel/util/i;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/lang/Runnable;

    .line 344
    return-void
.end method

.method public w()V
    .locals 6

    .prologue
    .line 348
    invoke-super {p0}, Lcom/peel/d/u;->w()V

    .line 349
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, Lcom/peel/ui/f;->ao:Ljava/util/Timer;

    .line 350
    new-instance v0, Lcom/peel/ui/o;

    invoke-direct {v0, p0}, Lcom/peel/ui/o;-><init>(Lcom/peel/ui/f;)V

    iput-object v0, p0, Lcom/peel/ui/f;->ap:Ljava/util/TimerTask;

    .line 371
    iget-object v0, p0, Lcom/peel/ui/f;->ao:Ljava/util/Timer;

    iget-object v1, p0, Lcom/peel/ui/f;->ap:Ljava/util/TimerTask;

    const-wide/16 v2, 0x3e8

    const-wide/16 v4, 0x2710

    invoke-virtual/range {v0 .. v5}, Ljava/util/Timer;->scheduleAtFixedRate(Ljava/util/TimerTask;JJ)V

    .line 372
    return-void
.end method

.method public x()V
    .locals 1

    .prologue
    .line 376
    invoke-super {p0}, Lcom/peel/d/u;->x()V

    .line 377
    iget-object v0, p0, Lcom/peel/ui/f;->ao:Ljava/util/Timer;

    if-eqz v0, :cond_0

    .line 378
    iget-object v0, p0, Lcom/peel/ui/f;->ao:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 380
    :cond_0
    return-void
.end method
