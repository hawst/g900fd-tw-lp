.class Lcom/peel/ui/ke;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/jess/ui/i;


# instance fields
.field final synthetic a:I

.field final synthetic b:Lcom/peel/ui/kc;


# direct methods
.method constructor <init>(Lcom/peel/ui/kc;I)V
    .locals 0

    .prologue
    .line 91
    iput-object p1, p0, Lcom/peel/ui/ke;->b:Lcom/peel/ui/kc;

    iput p2, p0, Lcom/peel/ui/ke;->a:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/jess/ui/TwoWayAbsListView;I)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 93
    if-nez p2, :cond_1

    .line 94
    iget-object v0, p0, Lcom/peel/ui/ke;->b:Lcom/peel/ui/kc;

    invoke-static {v0}, Lcom/peel/ui/kc;->c(Lcom/peel/ui/kc;)Landroid/util/LongSparseArray;

    move-result-object v0

    iget-object v2, p0, Lcom/peel/ui/ke;->b:Lcom/peel/ui/kc;

    iget v3, p0, Lcom/peel/ui/ke;->a:I

    invoke-virtual {v2, v3}, Lcom/peel/ui/kc;->getItemId(I)J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Landroid/util/LongSparseArray;->get(J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    .line 95
    if-nez v0, :cond_0

    .line 96
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 97
    iget-object v2, p0, Lcom/peel/ui/ke;->b:Lcom/peel/ui/kc;

    invoke-static {v2}, Lcom/peel/ui/kc;->c(Lcom/peel/ui/kc;)Landroid/util/LongSparseArray;

    move-result-object v2

    iget-object v3, p0, Lcom/peel/ui/ke;->b:Lcom/peel/ui/kc;

    iget v4, p0, Lcom/peel/ui/ke;->a:I

    invoke-virtual {v3, v4}, Lcom/peel/ui/kc;->getItemId(I)J

    move-result-wide v4

    invoke-virtual {v2, v4, v5, v0}, Landroid/util/LongSparseArray;->put(JLjava/lang/Object;)V

    .line 99
    :cond_0
    invoke-virtual {p1, v1}, Lcom/jess/ui/TwoWayAbsListView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 100
    const-string/jumbo v3, "firstOffset"

    if-nez v2, :cond_2

    :goto_0
    invoke-virtual {v0, v3, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 101
    const-string/jumbo v1, "firstVisible"

    invoke-virtual {p1}, Lcom/jess/ui/TwoWayAbsListView;->getFirstVisiblePosition()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 102
    iget-object v1, p0, Lcom/peel/ui/ke;->b:Lcom/peel/ui/kc;

    invoke-static {v1}, Lcom/peel/ui/kc;->c(Lcom/peel/ui/kc;)Landroid/util/LongSparseArray;

    move-result-object v1

    iget-object v2, p0, Lcom/peel/ui/ke;->b:Lcom/peel/ui/kc;

    iget v3, p0, Lcom/peel/ui/ke;->a:I

    invoke-virtual {v2, v3}, Lcom/peel/ui/kc;->getItemId(I)J

    move-result-wide v2

    invoke-virtual {v1, v2, v3, v0}, Landroid/util/LongSparseArray;->put(JLjava/lang/Object;)V

    .line 104
    :cond_1
    return-void

    .line 100
    :cond_2
    invoke-virtual {v2}, Landroid/view/View;->getTop()I

    move-result v1

    goto :goto_0
.end method

.method public a(Lcom/jess/ui/TwoWayAbsListView;III)V
    .locals 0

    .prologue
    .line 105
    return-void
.end method
