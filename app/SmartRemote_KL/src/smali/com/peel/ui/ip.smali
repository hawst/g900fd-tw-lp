.class Lcom/peel/ui/ip;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/peel/ui/gt;


# direct methods
.method constructor <init>(Lcom/peel/ui/gt;)V
    .locals 0

    .prologue
    .line 2668
    iput-object p1, p0, Lcom/peel/ui/ip;->a:Lcom/peel/ui/gt;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2672
    iget-object v0, p0, Lcom/peel/ui/ip;->a:Lcom/peel/ui/gt;

    invoke-static {v0}, Lcom/peel/ui/gt;->y(Lcom/peel/ui/gt;)Landroid/widget/ViewFlipper;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/widget/ViewFlipper;->setDisplayedChild(I)V

    .line 2673
    iget-object v0, p0, Lcom/peel/ui/ip;->a:Lcom/peel/ui/gt;

    iget-object v1, p0, Lcom/peel/ui/ip;->a:Lcom/peel/ui/gt;

    invoke-static {v1}, Lcom/peel/ui/gt;->y(Lcom/peel/ui/gt;)Landroid/widget/ViewFlipper;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/ViewFlipper;->getDisplayedChild()I

    move-result v1

    invoke-static {v0, v1, v3}, Lcom/peel/ui/gt;->a(Lcom/peel/ui/gt;IZ)V

    .line 2674
    iget-object v0, p0, Lcom/peel/ui/ip;->a:Lcom/peel/ui/gt;

    invoke-static {v0}, Lcom/peel/ui/gt;->H(Lcom/peel/ui/gt;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    const-string/jumbo v1, "device_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2675
    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/peel/ui/ip;->a:Lcom/peel/ui/gt;

    sget-object v2, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v2, v0}, Lcom/peel/control/am;->c(Ljava/lang/String;)Lcom/peel/control/h;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/peel/ui/gt;->a(Lcom/peel/ui/gt;Lcom/peel/control/h;)Lcom/peel/control/h;

    .line 2676
    :cond_0
    invoke-virtual {p1, v4}, Landroid/view/View;->setSelected(Z)V

    .line 2677
    iget-object v0, p0, Lcom/peel/ui/ip;->a:Lcom/peel/ui/gt;

    invoke-static {v0}, Lcom/peel/ui/gt;->I(Lcom/peel/ui/gt;)Landroid/widget/FrameLayout;

    move-result-object v0

    sget v1, Lcom/peel/ui/fp;->device_tab_1:I

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 2678
    if-eqz v0, :cond_1

    invoke-virtual {v0, v3}, Landroid/view/View;->setSelected(Z)V

    .line 2679
    :cond_1
    iget-object v0, p0, Lcom/peel/ui/ip;->a:Lcom/peel/ui/gt;

    invoke-static {v0}, Lcom/peel/ui/gt;->I(Lcom/peel/ui/gt;)Landroid/widget/FrameLayout;

    move-result-object v0

    sget v1, Lcom/peel/ui/fp;->device_tab_3:I

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 2680
    if-eqz v0, :cond_2

    invoke-virtual {v0, v3}, Landroid/view/View;->setSelected(Z)V

    .line 2681
    :cond_2
    return-void
.end method
