.class Lcom/peel/ui/kw;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Ljava/util/List;

.field final synthetic b:Ljava/lang/String;

.field final synthetic c:Landroid/os/Bundle;

.field final synthetic d:Lcom/peel/ui/kr;


# direct methods
.method constructor <init>(Lcom/peel/ui/kr;Ljava/util/List;Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 957
    iput-object p1, p0, Lcom/peel/ui/kw;->d:Lcom/peel/ui/kr;

    iput-object p2, p0, Lcom/peel/ui/kw;->a:Ljava/util/List;

    iput-object p3, p0, Lcom/peel/ui/kw;->b:Ljava/lang/String;

    iput-object p4, p0, Lcom/peel/ui/kw;->c:Landroid/os/Bundle;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 12

    .prologue
    const/4 v11, 0x1

    const/4 v10, 0x0

    const/4 v9, -0x1

    .line 960
    iget-object v0, p0, Lcom/peel/ui/kw;->d:Lcom/peel/ui/kr;

    invoke-virtual {v0}, Lcom/peel/ui/kr;->v()Landroid/view/View;

    move-result-object v0

    if-nez v0, :cond_1

    .line 1024
    :cond_0
    :goto_0
    return-void

    .line 964
    :cond_1
    :try_start_0
    sget-object v0, Lcom/peel/util/x;->c:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/text/SimpleDateFormat;

    iget-object v1, p0, Lcom/peel/ui/kw;->d:Lcom/peel/ui/kr;

    iget-object v1, v1, Lcom/peel/ui/kr;->c:Landroid/os/Bundle;

    const-string/jumbo v2, "time"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v0

    .line 965
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    .line 966
    const-wide/16 v4, 0x1

    add-long/2addr v4, v0

    .line 968
    iget-object v0, p0, Lcom/peel/ui/kw;->d:Lcom/peel/ui/kr;

    iget-object v0, v0, Lcom/peel/ui/kr;->b:Lcom/peel/d/i;

    const-string/jumbo v1, "top_cache_time"

    iget-object v6, p0, Lcom/peel/ui/kw;->d:Lcom/peel/ui/kr;

    iget-object v6, v6, Lcom/peel/ui/kr;->c:Landroid/os/Bundle;

    const-string/jumbo v7, "time"

    invoke-virtual {v6, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v1, v6}, Lcom/peel/d/i;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 974
    :goto_1
    iget-object v0, p0, Lcom/peel/ui/kw;->d:Lcom/peel/ui/kr;

    invoke-static {v0}, Lcom/peel/ui/kr;->e(Lcom/peel/ui/kr;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lcom/peel/ui/fq;->tile_bar:I

    iget-object v6, p0, Lcom/peel/ui/kw;->d:Lcom/peel/ui/kr;

    .line 975
    invoke-static {v6}, Lcom/peel/ui/kr;->h(Lcom/peel/ui/kr;)Landroid/widget/LinearLayout;

    move-result-object v6

    .line 974
    invoke-virtual {v0, v1, v6, v10}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 976
    iget-object v6, p0, Lcom/peel/ui/kw;->d:Lcom/peel/ui/kr;

    sget v1, Lcom/peel/ui/fp;->tiles_grid:I

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/jess/ui/TwoWayGridView;

    invoke-static {v6, v1}, Lcom/peel/ui/kr;->a(Lcom/peel/ui/kr;Lcom/jess/ui/TwoWayGridView;)Lcom/jess/ui/TwoWayGridView;

    .line 977
    sget v1, Lcom/peel/ui/fp;->title:I

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    move-object v8, v1

    check-cast v8, Landroid/widget/TextView;

    .line 979
    iget-object v1, p0, Lcom/peel/ui/kw;->d:Lcom/peel/ui/kr;

    iget-object v1, v1, Lcom/peel/ui/kr;->c:Landroid/os/Bundle;

    const-string/jumbo v6, "start"

    invoke-virtual {v1, v6, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 980
    iget-object v1, p0, Lcom/peel/ui/kw;->d:Lcom/peel/ui/kr;

    iget-object v1, v1, Lcom/peel/ui/kr;->c:Landroid/os/Bundle;

    const-string/jumbo v6, "end"

    invoke-virtual {v1, v6, v4, v5}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 981
    iget-object v1, p0, Lcom/peel/ui/kw;->a:Ljava/util/List;

    const-class v6, Lcom/peel/content/listing/c;

    invoke-virtual {v6}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-static {}, Lcom/peel/content/a;->a()Lcom/peel/data/ContentRoom;

    move-result-object v7

    invoke-static/range {v1 .. v7}, Lcom/peel/util/bx;->a(Ljava/util/List;JJLjava/lang/String;Lcom/peel/data/ContentRoom;)Ljava/util/List;

    move-result-object v1

    .line 983
    if-eqz v1, :cond_3

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_3

    .line 984
    iget-object v2, p0, Lcom/peel/ui/kw;->d:Lcom/peel/ui/kr;

    invoke-static {v2}, Lcom/peel/ui/kr;->i(Lcom/peel/ui/kr;)Lcom/jess/ui/TwoWayGridView;

    move-result-object v2

    new-instance v3, Lcom/peel/ui/ka;

    iget-object v4, p0, Lcom/peel/ui/kw;->d:Lcom/peel/ui/kr;

    invoke-virtual {v4}, Lcom/peel/ui/kr;->m()Landroid/support/v4/app/ae;

    move-result-object v4

    invoke-direct {v3, v4, v9, v1}, Lcom/peel/ui/ka;-><init>(Landroid/content/Context;ILjava/util/List;)V

    invoke-virtual {v2, v3}, Lcom/jess/ui/TwoWayGridView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 986
    iget-object v1, p0, Lcom/peel/ui/kw;->b:Ljava/lang/String;

    const-string/jumbo v2, "Weekend TV"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 988
    iget-object v1, p0, Lcom/peel/ui/kw;->d:Lcom/peel/ui/kr;

    invoke-virtual {v1}, Lcom/peel/ui/kr;->n()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/peel/ui/ft;->weekend_tv:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v8, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1006
    :goto_2
    iget-object v1, p0, Lcom/peel/ui/kw;->d:Lcom/peel/ui/kr;

    invoke-static {v1}, Lcom/peel/ui/kr;->h(Lcom/peel/ui/kr;)Landroid/widget/LinearLayout;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1008
    iget-object v0, p0, Lcom/peel/ui/kw;->d:Lcom/peel/ui/kr;

    invoke-static {v0}, Lcom/peel/ui/kr;->i(Lcom/peel/ui/kr;)Lcom/jess/ui/TwoWayGridView;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/ui/kw;->c:Landroid/os/Bundle;

    invoke-virtual {v0, v1}, Lcom/jess/ui/TwoWayGridView;->setTag(Ljava/lang/Object;)V

    .line 1009
    iget-object v0, p0, Lcom/peel/ui/kw;->d:Lcom/peel/ui/kr;

    invoke-static {v0}, Lcom/peel/ui/kr;->i(Lcom/peel/ui/kr;)Lcom/jess/ui/TwoWayGridView;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/ui/kw;->d:Lcom/peel/ui/kr;

    invoke-static {v1}, Lcom/peel/ui/kr;->j(Lcom/peel/ui/kr;)Lcom/jess/ui/z;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/jess/ui/TwoWayGridView;->setOnItemClickListener(Lcom/jess/ui/z;)V

    .line 1011
    sget-object v0, Lcom/peel/util/a;->e:[I

    if-eqz v0, :cond_2

    sget-object v0, Lcom/peel/util/a;->e:[I

    aget v0, v0, v10

    if-eq v0, v9, :cond_2

    sget-object v0, Lcom/peel/util/a;->e:[I

    aget v0, v0, v11

    if-eq v0, v9, :cond_2

    .line 1012
    iget-object v0, p0, Lcom/peel/ui/kw;->d:Lcom/peel/ui/kr;

    invoke-static {v0}, Lcom/peel/ui/kr;->i(Lcom/peel/ui/kr;)Lcom/jess/ui/TwoWayGridView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/jess/ui/TwoWayGridView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 1013
    iget-object v1, p0, Lcom/peel/ui/kw;->d:Lcom/peel/ui/kr;

    invoke-virtual {v1}, Lcom/peel/ui/kr;->n()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/peel/ui/fn;->tile_height_actual:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sget-object v2, Lcom/peel/util/a;->e:[I

    aget v2, v2, v11

    add-int/2addr v1, v2

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 1014
    iget-object v1, p0, Lcom/peel/ui/kw;->d:Lcom/peel/ui/kr;

    invoke-static {v1}, Lcom/peel/ui/kr;->i(Lcom/peel/ui/kr;)Lcom/jess/ui/TwoWayGridView;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/jess/ui/TwoWayGridView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1016
    :cond_2
    iget-object v0, p0, Lcom/peel/ui/kw;->d:Lcom/peel/ui/kr;

    invoke-static {v0}, Lcom/peel/ui/kr;->k(Lcom/peel/ui/kr;)I

    .line 1019
    :cond_3
    iget-object v0, p0, Lcom/peel/ui/kw;->d:Lcom/peel/ui/kr;

    invoke-static {v0}, Lcom/peel/ui/kr;->l(Lcom/peel/ui/kr;)I

    .line 1021
    iget-object v0, p0, Lcom/peel/ui/kw;->d:Lcom/peel/ui/kr;

    invoke-static {v0}, Lcom/peel/ui/kr;->m(Lcom/peel/ui/kr;)I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/peel/ui/kw;->d:Lcom/peel/ui/kr;

    invoke-static {v0}, Lcom/peel/ui/kr;->n(Lcom/peel/ui/kr;)I

    move-result v0

    if-nez v0, :cond_0

    .line 1022
    iget-object v0, p0, Lcom/peel/ui/kw;->d:Lcom/peel/ui/kr;

    invoke-static {v0}, Lcom/peel/ui/kr;->g(Lcom/peel/ui/kr;)V

    goto/16 :goto_0

    .line 969
    :catch_0
    move-exception v0

    .line 970
    invoke-static {}, Lcom/peel/ui/kr;->c()Ljava/lang/String;

    move-result-object v1

    invoke-static {}, Lcom/peel/ui/kr;->c()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 971
    const-wide/16 v4, -0x1

    move-wide v2, v4

    goto/16 :goto_1

    .line 989
    :cond_4
    iget-object v1, p0, Lcom/peel/ui/kw;->b:Ljava/lang/String;

    const-string/jumbo v2, "On Now"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 991
    iget-object v1, p0, Lcom/peel/ui/kw;->d:Lcom/peel/ui/kr;

    invoke-virtual {v1}, Lcom/peel/ui/kr;->n()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/peel/ui/ft;->on_now:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v8, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    .line 992
    :cond_5
    iget-object v1, p0, Lcom/peel/ui/kw;->b:Ljava/lang/String;

    const-string/jumbo v2, "Popular Channels"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 994
    iget-object v1, p0, Lcom/peel/ui/kw;->d:Lcom/peel/ui/kr;

    invoke-virtual {v1}, Lcom/peel/ui/kr;->n()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/peel/ui/ft;->popular_channels:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v8, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    .line 995
    :cond_6
    iget-object v1, p0, Lcom/peel/ui/kw;->b:Ljava/lang/String;

    const-string/jumbo v2, "Popular On Twitter"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 997
    iget-object v1, p0, Lcom/peel/ui/kw;->d:Lcom/peel/ui/kr;

    invoke-virtual {v1}, Lcom/peel/ui/kr;->n()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/peel/ui/ft;->popular_twitter:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v8, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    .line 998
    :cond_7
    iget-object v1, p0, Lcom/peel/ui/kw;->b:Ljava/lang/String;

    const-string/jumbo v2, "Tonight On TV"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 1000
    iget-object v1, p0, Lcom/peel/ui/kw;->d:Lcom/peel/ui/kr;

    invoke-virtual {v1}, Lcom/peel/ui/kr;->n()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/peel/ui/ft;->tonight_on_tv:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v8, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    .line 1001
    :cond_8
    iget-object v1, p0, Lcom/peel/ui/kw;->b:Ljava/lang/String;

    const-string/jumbo v2, "Not To Be Missed"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 1002
    iget-object v1, p0, Lcom/peel/ui/kw;->d:Lcom/peel/ui/kr;

    invoke-virtual {v1}, Lcom/peel/ui/kr;->n()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/peel/ui/ft;->not_to_be_missed:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v8, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    .line 1004
    :cond_9
    iget-object v1, p0, Lcom/peel/ui/kw;->b:Ljava/lang/String;

    invoke-virtual {v8, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_2
.end method
