.class Lcom/peel/ui/ez;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Landroid/os/Bundle;

.field final synthetic b:Lcom/peel/widget/ObservableListView;

.field final synthetic c:Lcom/peel/ui/ey;


# direct methods
.method constructor <init>(Lcom/peel/ui/ey;Landroid/os/Bundle;Lcom/peel/widget/ObservableListView;)V
    .locals 0

    .prologue
    .line 217
    iput-object p1, p0, Lcom/peel/ui/ez;->c:Lcom/peel/ui/ey;

    iput-object p2, p0, Lcom/peel/ui/ez;->a:Landroid/os/Bundle;

    iput-object p3, p0, Lcom/peel/ui/ez;->b:Lcom/peel/widget/ObservableListView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 220
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 221
    const-string/jumbo v1, "path"

    const-string/jumbo v2, "listing/genre"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 222
    const-string/jumbo v1, "window"

    iget-object v2, p0, Lcom/peel/ui/ez;->a:Landroid/os/Bundle;

    const-string/jumbo v3, "window"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 223
    const-string/jumbo v1, "start"

    iget-object v2, p0, Lcom/peel/ui/ez;->a:Landroid/os/Bundle;

    const-string/jumbo v3, "start"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 224
    const-string/jumbo v1, "room"

    iget-object v2, p0, Lcom/peel/ui/ez;->c:Lcom/peel/ui/ey;

    invoke-static {v2}, Lcom/peel/ui/ey;->a(Lcom/peel/ui/ey;)Lcom/peel/data/ContentRoom;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 225
    const-string/jumbo v1, "cacheWindow"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 227
    iget-object v1, p0, Lcom/peel/ui/ez;->c:Lcom/peel/ui/ey;

    invoke-static {v1}, Lcom/peel/ui/ey;->c(Lcom/peel/ui/ey;)Lcom/peel/content/library/LiveLibrary;

    move-result-object v1

    new-instance v2, Lcom/peel/ui/fa;

    const/4 v3, 0x2

    invoke-direct {v2, p0, v3}, Lcom/peel/ui/fa;-><init>(Lcom/peel/ui/ez;I)V

    invoke-virtual {v1, v0, v2}, Lcom/peel/content/library/LiveLibrary;->a(Landroid/os/Bundle;Lcom/peel/util/t;)V

    .line 261
    return-void
.end method
