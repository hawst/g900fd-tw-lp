.class public Lcom/peel/ui/AutoResizeTextView;
.super Landroid/widget/TextView;


# instance fields
.field private a:Lcom/peel/ui/a;

.field private b:Z

.field private c:F

.field private d:F

.field private e:F

.field private f:F

.field private g:F

.field private h:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 74
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/peel/ui/AutoResizeTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 75
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 79
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/peel/ui/AutoResizeTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 80
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 84
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 52
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/peel/ui/AutoResizeTextView;->b:Z

    .line 58
    iput v1, p0, Lcom/peel/ui/AutoResizeTextView;->d:F

    .line 61
    const/high16 v0, 0x41000000    # 8.0f

    iput v0, p0, Lcom/peel/ui/AutoResizeTextView;->e:F

    .line 64
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/peel/ui/AutoResizeTextView;->f:F

    .line 67
    iput v1, p0, Lcom/peel/ui/AutoResizeTextView;->g:F

    .line 70
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/peel/ui/AutoResizeTextView;->h:Z

    .line 85
    invoke-virtual {p0}, Lcom/peel/ui/AutoResizeTextView;->getTextSize()F

    move-result v0

    iput v0, p0, Lcom/peel/ui/AutoResizeTextView;->c:F

    .line 86
    return-void
.end method

.method private a(Ljava/lang/CharSequence;Landroid/text/TextPaint;IF)I
    .locals 8

    .prologue
    .line 309
    new-instance v2, Landroid/text/TextPaint;

    invoke-direct {v2, p2}, Landroid/text/TextPaint;-><init>(Landroid/graphics/Paint;)V

    .line 311
    invoke-virtual {v2, p4}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 313
    new-instance v0, Landroid/text/StaticLayout;

    sget-object v4, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    iget v5, p0, Lcom/peel/ui/AutoResizeTextView;->f:F

    iget v6, p0, Lcom/peel/ui/AutoResizeTextView;->g:F

    const/4 v7, 0x1

    move-object v1, p1

    move v3, p3

    invoke-direct/range {v0 .. v7}, Landroid/text/StaticLayout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZ)V

    .line 314
    invoke-virtual {v0}, Landroid/text/StaticLayout;->getHeight()I

    move-result v0

    return v0
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 200
    iget v0, p0, Lcom/peel/ui/AutoResizeTextView;->c:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    .line 201
    const/4 v0, 0x0

    iget v1, p0, Lcom/peel/ui/AutoResizeTextView;->c:F

    invoke-super {p0, v0, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 202
    iget v0, p0, Lcom/peel/ui/AutoResizeTextView;->c:F

    iput v0, p0, Lcom/peel/ui/AutoResizeTextView;->d:F

    .line 204
    :cond_0
    return-void
.end method

.method public a(II)V
    .locals 11

    .prologue
    const/4 v2, 0x0

    const/4 v7, 0x0

    .line 235
    invoke-virtual {p0}, Lcom/peel/ui/AutoResizeTextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    .line 237
    if-eqz v1, :cond_0

    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-eqz v0, :cond_0

    if-lez p2, :cond_0

    if-lez p1, :cond_0

    iget v0, p0, Lcom/peel/ui/AutoResizeTextView;->c:F

    cmpl-float v0, v0, v2

    if-nez v0, :cond_1

    .line 302
    :cond_0
    :goto_0
    return-void

    .line 242
    :cond_1
    invoke-virtual {p0}, Lcom/peel/ui/AutoResizeTextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v9

    .line 245
    invoke-virtual {v9}, Landroid/text/TextPaint;->getTextSize()F

    move-result v10

    .line 247
    iget v0, p0, Lcom/peel/ui/AutoResizeTextView;->d:F

    cmpl-float v0, v0, v2

    if-lez v0, :cond_2

    iget v0, p0, Lcom/peel/ui/AutoResizeTextView;->c:F

    iget v2, p0, Lcom/peel/ui/AutoResizeTextView;->d:F

    invoke-static {v0, v2}, Ljava/lang/Math;->min(FF)F

    move-result v0

    .line 250
    :goto_1
    invoke-direct {p0, v1, v9, p1, v0}, Lcom/peel/ui/AutoResizeTextView;->a(Ljava/lang/CharSequence;Landroid/text/TextPaint;IF)I

    move-result v2

    move v8, v0

    move v0, v2

    .line 253
    :goto_2
    if-le v0, p2, :cond_3

    iget v2, p0, Lcom/peel/ui/AutoResizeTextView;->e:F

    cmpl-float v2, v8, v2

    if-lez v2, :cond_3

    .line 254
    const/high16 v0, 0x40000000    # 2.0f

    sub-float v0, v8, v0

    iget v2, p0, Lcom/peel/ui/AutoResizeTextView;->e:F

    invoke-static {v0, v2}, Ljava/lang/Math;->max(FF)F

    move-result v2

    .line 255
    invoke-direct {p0, v1, v9, p1, v2}, Lcom/peel/ui/AutoResizeTextView;->a(Ljava/lang/CharSequence;Landroid/text/TextPaint;IF)I

    move-result v0

    move v8, v2

    goto :goto_2

    .line 247
    :cond_2
    iget v0, p0, Lcom/peel/ui/AutoResizeTextView;->c:F

    goto :goto_1

    .line 259
    :cond_3
    iget-boolean v2, p0, Lcom/peel/ui/AutoResizeTextView;->h:Z

    if-eqz v2, :cond_4

    iget v2, p0, Lcom/peel/ui/AutoResizeTextView;->e:F

    cmpl-float v2, v8, v2

    if-nez v2, :cond_4

    if-le v0, p2, :cond_4

    .line 262
    new-instance v2, Landroid/text/TextPaint;

    invoke-direct {v2, v9}, Landroid/text/TextPaint;-><init>(Landroid/graphics/Paint;)V

    .line 264
    new-instance v0, Landroid/text/StaticLayout;

    sget-object v4, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    iget v5, p0, Lcom/peel/ui/AutoResizeTextView;->f:F

    iget v6, p0, Lcom/peel/ui/AutoResizeTextView;->g:F

    move v3, p1

    invoke-direct/range {v0 .. v7}, Landroid/text/StaticLayout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZ)V

    .line 266
    invoke-virtual {v0}, Landroid/text/StaticLayout;->getLineCount()I

    move-result v2

    if-lez v2, :cond_4

    .line 269
    invoke-virtual {v0, p2}, Landroid/text/StaticLayout;->getLineForVertical(I)I

    move-result v2

    add-int/lit8 v3, v2, -0x1

    .line 271
    if-gez v3, :cond_6

    .line 272
    const-string/jumbo v0, ""

    invoke-virtual {p0, v0}, Lcom/peel/ui/AutoResizeTextView;->setText(Ljava/lang/CharSequence;)V

    .line 292
    :cond_4
    :goto_3
    invoke-virtual {p0, v7, v8}, Lcom/peel/ui/AutoResizeTextView;->setTextSize(IF)V

    .line 293
    iget v0, p0, Lcom/peel/ui/AutoResizeTextView;->g:F

    iget v1, p0, Lcom/peel/ui/AutoResizeTextView;->f:F

    invoke-virtual {p0, v0, v1}, Lcom/peel/ui/AutoResizeTextView;->setLineSpacing(FF)V

    .line 296
    iget-object v0, p0, Lcom/peel/ui/AutoResizeTextView;->a:Lcom/peel/ui/a;

    if-eqz v0, :cond_5

    .line 297
    iget-object v0, p0, Lcom/peel/ui/AutoResizeTextView;->a:Lcom/peel/ui/a;

    invoke-interface {v0, p0, v10, v8}, Lcom/peel/ui/a;->a(Landroid/widget/TextView;FF)V

    .line 301
    :cond_5
    iput-boolean v7, p0, Lcom/peel/ui/AutoResizeTextView;->b:Z

    goto :goto_0

    .line 276
    :cond_6
    invoke-virtual {v0, v3}, Landroid/text/StaticLayout;->getLineStart(I)I

    move-result v4

    .line 277
    invoke-virtual {v0, v3}, Landroid/text/StaticLayout;->getLineEnd(I)I

    move-result v2

    .line 278
    invoke-virtual {v0, v3}, Landroid/text/StaticLayout;->getLineWidth(I)F

    move-result v0

    .line 279
    const-string/jumbo v3, "..."

    invoke-virtual {v9, v3}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;)F

    move-result v3

    .line 282
    :goto_4
    int-to-float v5, p1

    add-float/2addr v0, v3

    cmpg-float v0, v5, v0

    if-gez v0, :cond_7

    .line 283
    add-int/lit8 v2, v2, -0x1

    add-int/lit8 v0, v2, 0x1

    invoke-interface {v1, v4, v0}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v9, v0}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;)F

    move-result v0

    goto :goto_4

    .line 285
    :cond_7
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {v1, v7, v2}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "..."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/peel/ui/AutoResizeTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_3
.end method

.method public getAddEllipsis()Z
    .locals 1

    .prologue
    .line 193
    iget-boolean v0, p0, Lcom/peel/ui/AutoResizeTextView;->h:Z

    return v0
.end method

.method public getMaxTextSize()F
    .locals 1

    .prologue
    .line 159
    iget v0, p0, Lcom/peel/ui/AutoResizeTextView;->d:F

    return v0
.end method

.method public getMinTextSize()F
    .locals 1

    .prologue
    .line 177
    iget v0, p0, Lcom/peel/ui/AutoResizeTextView;->e:F

    return v0
.end method

.method protected onLayout(ZIIII)V
    .locals 3

    .prologue
    .line 211
    if-nez p1, :cond_0

    iget-boolean v0, p0, Lcom/peel/ui/AutoResizeTextView;->b:Z

    if-eqz v0, :cond_1

    .line 212
    :cond_0
    sub-int v0, p4, p2

    invoke-virtual {p0}, Lcom/peel/ui/AutoResizeTextView;->getCompoundPaddingLeft()I

    move-result v1

    sub-int/2addr v0, v1

    invoke-virtual {p0}, Lcom/peel/ui/AutoResizeTextView;->getCompoundPaddingRight()I

    move-result v1

    sub-int/2addr v0, v1

    .line 213
    sub-int v1, p5, p3

    invoke-virtual {p0}, Lcom/peel/ui/AutoResizeTextView;->getCompoundPaddingBottom()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-virtual {p0}, Lcom/peel/ui/AutoResizeTextView;->getCompoundPaddingTop()I

    move-result v2

    sub-int/2addr v1, v2

    .line 214
    invoke-virtual {p0, v0, v1}, Lcom/peel/ui/AutoResizeTextView;->a(II)V

    .line 216
    :cond_1
    invoke-super/range {p0 .. p5}, Landroid/widget/TextView;->onLayout(ZIIII)V

    .line 217
    return-void
.end method

.method protected onSizeChanged(IIII)V
    .locals 1

    .prologue
    .line 103
    if-ne p1, p3, :cond_0

    if-eq p2, p4, :cond_1

    .line 104
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/peel/ui/AutoResizeTextView;->b:Z

    .line 106
    :cond_1
    return-void
.end method

.method protected onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 1

    .prologue
    .line 93
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/peel/ui/AutoResizeTextView;->b:Z

    .line 95
    invoke-virtual {p0}, Lcom/peel/ui/AutoResizeTextView;->a()V

    .line 96
    return-void
.end method

.method public setAddEllipsis(Z)V
    .locals 0

    .prologue
    .line 185
    iput-boolean p1, p0, Lcom/peel/ui/AutoResizeTextView;->h:Z

    .line 186
    return-void
.end method

.method public setLineSpacing(FF)V
    .locals 0

    .prologue
    .line 139
    invoke-super {p0, p1, p2}, Landroid/widget/TextView;->setLineSpacing(FF)V

    .line 140
    iput p2, p0, Lcom/peel/ui/AutoResizeTextView;->f:F

    .line 141
    iput p1, p0, Lcom/peel/ui/AutoResizeTextView;->g:F

    .line 142
    return-void
.end method

.method public setMaxTextSize(F)V
    .locals 0

    .prologue
    .line 149
    iput p1, p0, Lcom/peel/ui/AutoResizeTextView;->d:F

    .line 150
    invoke-virtual {p0}, Lcom/peel/ui/AutoResizeTextView;->requestLayout()V

    .line 151
    invoke-virtual {p0}, Lcom/peel/ui/AutoResizeTextView;->invalidate()V

    .line 152
    return-void
.end method

.method public setMinTextSize(F)V
    .locals 0

    .prologue
    .line 167
    iput p1, p0, Lcom/peel/ui/AutoResizeTextView;->e:F

    .line 168
    invoke-virtual {p0}, Lcom/peel/ui/AutoResizeTextView;->requestLayout()V

    .line 169
    invoke-virtual {p0}, Lcom/peel/ui/AutoResizeTextView;->invalidate()V

    .line 170
    return-void
.end method

.method public setOnResizeListener(Lcom/peel/ui/a;)V
    .locals 0

    .prologue
    .line 113
    iput-object p1, p0, Lcom/peel/ui/AutoResizeTextView;->a:Lcom/peel/ui/a;

    .line 114
    return-void
.end method

.method public setTextSize(F)V
    .locals 1

    .prologue
    .line 121
    invoke-super {p0, p1}, Landroid/widget/TextView;->setTextSize(F)V

    .line 122
    invoke-virtual {p0}, Lcom/peel/ui/AutoResizeTextView;->getTextSize()F

    move-result v0

    iput v0, p0, Lcom/peel/ui/AutoResizeTextView;->c:F

    .line 123
    return-void
.end method

.method public setTextSize(IF)V
    .locals 1

    .prologue
    .line 130
    invoke-super {p0, p1, p2}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 131
    invoke-virtual {p0}, Lcom/peel/ui/AutoResizeTextView;->getTextSize()F

    move-result v0

    iput v0, p0, Lcom/peel/ui/AutoResizeTextView;->c:F

    .line 132
    return-void
.end method
