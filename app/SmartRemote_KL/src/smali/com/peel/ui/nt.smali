.class Lcom/peel/ui/nt;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/peel/content/library/Library;

.field final synthetic b:Landroid/os/Bundle;

.field final synthetic c:Lcom/peel/ui/nl;


# direct methods
.method constructor <init>(Lcom/peel/ui/nl;Lcom/peel/content/library/Library;Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 419
    iput-object p1, p0, Lcom/peel/ui/nt;->c:Lcom/peel/ui/nl;

    iput-object p2, p0, Lcom/peel/ui/nt;->a:Lcom/peel/content/library/Library;

    iput-object p3, p0, Lcom/peel/ui/nt;->b:Landroid/os/Bundle;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 422
    sget-object v0, Lcom/peel/content/a;->h:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/peel/ui/nt;->c:Lcom/peel/ui/nl;

    iget-object v0, v0, Lcom/peel/ui/nl;->b:Lcom/peel/d/i;

    if-nez v0, :cond_1

    .line 486
    :cond_0
    :goto_0
    return-void

    .line 425
    :cond_1
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 426
    const-string/jumbo v1, "path"

    const-string/jumbo v2, "listing/weeklyfavs"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 427
    const-string/jumbo v1, "room"

    invoke-static {}, Lcom/peel/content/a;->a()Lcom/peel/data/ContentRoom;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 428
    iget-object v1, p0, Lcom/peel/ui/nt;->a:Lcom/peel/content/library/Library;

    new-instance v2, Lcom/peel/ui/nu;

    const/4 v3, 0x2

    invoke-direct {v2, p0, v3}, Lcom/peel/ui/nu;-><init>(Lcom/peel/ui/nt;I)V

    invoke-virtual {v1, v0, v2}, Lcom/peel/content/library/Library;->a(Landroid/os/Bundle;Lcom/peel/util/t;)V

    goto :goto_0
.end method
