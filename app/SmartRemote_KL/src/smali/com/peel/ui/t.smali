.class Lcom/peel/ui/t;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/peel/data/Channel;

.field final synthetic b:Lcom/peel/ui/q;


# direct methods
.method constructor <init>(Lcom/peel/ui/q;Lcom/peel/data/Channel;)V
    .locals 0

    .prologue
    .line 514
    iput-object p1, p0, Lcom/peel/ui/t;->b:Lcom/peel/ui/q;

    iput-object p2, p0, Lcom/peel/ui/t;->a:Lcom/peel/data/Channel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v2, 0x0

    const/16 v4, 0x7e7

    .line 517
    iget-object v0, p0, Lcom/peel/ui/t;->b:Lcom/peel/ui/q;

    iget-object v0, v0, Lcom/peel/ui/q;->a:Lcom/peel/ui/f;

    invoke-virtual {v0}, Lcom/peel/ui/f;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    const-string/jumbo v1, "remote_channel_guide_trigger"

    const/4 v3, 0x1

    invoke-static {v0, v1, v3}, Lcom/peel/util/dq;->a(Landroid/content/Context;Ljava/lang/String;Z)V

    .line 518
    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    sget-object v1, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v1}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/peel/control/am;->c(Lcom/peel/control/RoomControl;)[Lcom/peel/control/h;

    move-result-object v0

    .line 519
    if-eqz v0, :cond_0

    array-length v0, v0

    if-nez v0, :cond_2

    .line 520
    :cond_0
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 521
    const-string/jumbo v1, "passback_clazz"

    iget-object v2, p0, Lcom/peel/ui/t;->b:Lcom/peel/ui/q;

    iget-object v2, v2, Lcom/peel/ui/q;->a:Lcom/peel/ui/f;

    invoke-virtual {v2}, Lcom/peel/ui/f;->m()Landroid/support/v4/app/ae;

    move-result-object v2

    invoke-static {v2}, Lcom/peel/d/e;->a(Landroid/support/v4/app/ae;)Lcom/peel/d/u;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 522
    const-string/jumbo v1, "passback_bundle"

    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 523
    iget-object v1, p0, Lcom/peel/ui/t;->b:Lcom/peel/ui/q;

    iget-object v1, v1, Lcom/peel/ui/q;->a:Lcom/peel/ui/f;

    invoke-virtual {v1}, Lcom/peel/ui/f;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    const-class v2, Lcom/peel/i/dr;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lcom/peel/d/e;->b(Landroid/support/v4/app/ae;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 558
    :cond_1
    :goto_0
    return-void

    .line 525
    :cond_2
    iget-object v0, p0, Lcom/peel/ui/t;->b:Lcom/peel/ui/q;

    iget-object v0, v0, Lcom/peel/ui/q;->a:Lcom/peel/ui/f;

    invoke-static {v0}, Lcom/peel/ui/f;->m(Lcom/peel/ui/f;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 530
    :try_start_0
    iget-object v0, p0, Lcom/peel/ui/t;->b:Lcom/peel/ui/q;

    iget-object v0, v0, Lcom/peel/ui/q;->a:Lcom/peel/ui/f;

    invoke-static {v0}, Lcom/peel/ui/f;->n(Lcom/peel/ui/f;)Ljava/util/ArrayList;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/ui/t;->b:Lcom/peel/ui/q;

    iget-object v1, v1, Lcom/peel/ui/q;->a:Lcom/peel/ui/f;

    .line 531
    invoke-static {v1}, Lcom/peel/ui/f;->b(Lcom/peel/ui/f;)Lcom/peel/ui/ey;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/ui/ey;->f()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    iget-object v1, p0, Lcom/peel/ui/t;->a:Lcom/peel/data/Channel;

    .line 533
    invoke-virtual {v1}, Lcom/peel/data/Channel;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 534
    :try_start_1
    invoke-static {v1}, Lcom/peel/content/listing/LiveListing;->a(Ljava/util/List;)V

    .line 535
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/content/listing/LiveListing;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 540
    :goto_1
    if-eqz v1, :cond_1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_1

    .line 541
    iget-object v1, p0, Lcom/peel/ui/t;->b:Lcom/peel/ui/q;

    invoke-static {v1}, Lcom/peel/ui/q;->a(Lcom/peel/ui/q;)I

    move-result v1

    if-nez v1, :cond_3

    .line 542
    const-string/jumbo v1, "Channel Guide"

    invoke-static {v0, v4, v1}, Lcom/peel/util/bx;->a(Lcom/peel/content/listing/Listing;ILjava/lang/String;)V

    .line 552
    :goto_2
    iget-object v0, p0, Lcom/peel/ui/t;->b:Lcom/peel/ui/q;

    iget-object v0, v0, Lcom/peel/ui/q;->a:Lcom/peel/ui/f;

    invoke-static {v0, p1}, Lcom/peel/ui/f;->a(Lcom/peel/ui/f;Landroid/view/View;)V

    .line 553
    iget-object v0, p0, Lcom/peel/ui/t;->b:Lcom/peel/ui/q;

    iget-object v0, v0, Lcom/peel/ui/q;->a:Lcom/peel/ui/f;

    invoke-virtual {v0}, Lcom/peel/ui/f;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/ui/t;->a:Lcom/peel/data/Channel;

    invoke-virtual {v1}, Lcom/peel/data/Channel;->m()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/peel/util/bx;->e(Landroid/content/Context;Ljava/lang/String;)V

    .line 554
    iget-object v0, p0, Lcom/peel/ui/t;->a:Lcom/peel/data/Channel;

    invoke-static {v0}, Lcom/peel/util/bx;->a(Lcom/peel/data/Channel;)V

    goto :goto_0

    .line 537
    :catch_0
    move-exception v0

    move-object v0, v2

    :goto_3
    move-object v1, v0

    move-object v0, v2

    goto :goto_1

    .line 547
    :cond_3
    const-string/jumbo v1, "Recently Watched"

    invoke-static {v0, v4, v1}, Lcom/peel/util/bx;->a(Lcom/peel/content/listing/Listing;ILjava/lang/String;)V

    goto :goto_2

    .line 537
    :catch_1
    move-exception v0

    move-object v0, v1

    goto :goto_3
.end method
