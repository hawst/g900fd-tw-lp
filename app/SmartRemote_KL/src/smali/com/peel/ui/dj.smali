.class Lcom/peel/ui/dj;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic a:Lcom/peel/ui/dh;


# direct methods
.method constructor <init>(Lcom/peel/ui/dh;)V
    .locals 0

    .prologue
    .line 120
    iput-object p1, p0, Lcom/peel/ui/dj;->a:Lcom/peel/ui/dh;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 13

    .prologue
    const/4 v12, 0x0

    .line 123
    new-instance v0, Lcom/peel/ui/dg;

    iget-object v1, p0, Lcom/peel/ui/dj;->a:Lcom/peel/ui/dh;

    iget-object v1, v1, Lcom/peel/ui/dh;->a:Landroid/content/Context;

    iget-object v2, p0, Lcom/peel/ui/dj;->a:Lcom/peel/ui/dh;

    iget-object v2, v2, Lcom/peel/ui/dh;->b:Ljava/lang/String;

    iget-object v3, p0, Lcom/peel/ui/dj;->a:Lcom/peel/ui/dh;

    iget v3, v3, Lcom/peel/ui/dh;->c:I

    iget-object v4, p0, Lcom/peel/ui/dj;->a:Lcom/peel/ui/dh;

    iget v4, v4, Lcom/peel/ui/dh;->d:I

    iget-object v5, p0, Lcom/peel/ui/dj;->a:Lcom/peel/ui/dh;

    iget-object v5, v5, Lcom/peel/ui/dh;->e:Ljava/util/List;

    iget-object v6, p0, Lcom/peel/ui/dj;->a:Lcom/peel/ui/dh;

    iget-object v6, v6, Lcom/peel/ui/dh;->f:Ljava/util/List;

    iget-object v7, p0, Lcom/peel/ui/dj;->a:Lcom/peel/ui/dh;

    iget-object v7, v7, Lcom/peel/ui/dh;->i:Lcom/peel/ui/dg;

    iget-object v7, v7, Lcom/peel/ui/dg;->b:Landroid/widget/EditText;

    .line 124
    invoke-virtual {v7}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v7

    if-nez v7, :cond_0

    move-object v7, v12

    :goto_0
    iget-object v8, p0, Lcom/peel/ui/dj;->a:Lcom/peel/ui/dh;

    iget-object v8, v8, Lcom/peel/ui/dh;->i:Lcom/peel/ui/dg;

    iget-object v8, v8, Lcom/peel/ui/dg;->c:Landroid/widget/EditText;

    .line 125
    invoke-virtual {v8}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v8

    if-nez v8, :cond_1

    move-object v8, v12

    :goto_1
    iget-object v9, p0, Lcom/peel/ui/dj;->a:Lcom/peel/ui/dh;

    iget-object v9, v9, Lcom/peel/ui/dh;->i:Lcom/peel/ui/dg;

    iget-object v9, v9, Lcom/peel/ui/dg;->d:Landroid/widget/EditText;

    .line 126
    invoke-virtual {v9}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v9

    if-nez v9, :cond_2

    move-object v9, v12

    :goto_2
    const-string/jumbo v10, "brand"

    iget-object v11, p0, Lcom/peel/ui/dj;->a:Lcom/peel/ui/dh;

    iget-object v11, v11, Lcom/peel/ui/dh;->g:Lcom/peel/ui/dr;

    invoke-direct/range {v0 .. v12}, Lcom/peel/ui/dg;-><init>(Landroid/content/Context;Ljava/lang/String;IILjava/util/List;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/peel/ui/dr;Landroid/widget/EditText;)V

    .line 128
    iget-object v1, p0, Lcom/peel/ui/dj;->a:Lcom/peel/ui/dh;

    iget-object v1, v1, Lcom/peel/ui/dh;->i:Lcom/peel/ui/dg;

    iget-object v2, p0, Lcom/peel/ui/dj;->a:Lcom/peel/ui/dh;

    iget-object v2, v2, Lcom/peel/ui/dh;->h:Landroid/widget/EditText;

    invoke-virtual {v1, v0, v2}, Lcom/peel/ui/dg;->a(Lcom/peel/widget/ag;Landroid/widget/EditText;)V

    .line 129
    return-void

    .line 124
    :cond_0
    iget-object v7, p0, Lcom/peel/ui/dj;->a:Lcom/peel/ui/dh;

    iget-object v7, v7, Lcom/peel/ui/dh;->i:Lcom/peel/ui/dg;

    iget-object v7, v7, Lcom/peel/ui/dg;->b:Landroid/widget/EditText;

    invoke-virtual {v7}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v7

    goto :goto_0

    .line 125
    :cond_1
    iget-object v8, p0, Lcom/peel/ui/dj;->a:Lcom/peel/ui/dh;

    iget-object v8, v8, Lcom/peel/ui/dh;->i:Lcom/peel/ui/dg;

    iget-object v8, v8, Lcom/peel/ui/dg;->c:Landroid/widget/EditText;

    invoke-virtual {v8}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v8

    goto :goto_1

    .line 126
    :cond_2
    iget-object v9, p0, Lcom/peel/ui/dj;->a:Lcom/peel/ui/dh;

    iget-object v9, v9, Lcom/peel/ui/dh;->i:Lcom/peel/ui/dg;

    iget-object v9, v9, Lcom/peel/ui/dg;->d:Landroid/widget/EditText;

    invoke-virtual {v9}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v9

    goto :goto_2
.end method
