.class public Lcom/peel/ui/model/DFPAd;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;

.field private static final w:Ljava/lang/String;


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public e:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public f:Ljava/lang/String;

.field public g:Ljava/lang/String;

.field public h:Ljava/lang/String;

.field public i:Ljava/lang/String;

.field public j:Ljava/lang/String;

.field public k:Ljava/lang/String;

.field public l:Ljava/lang/String;

.field public m:Ljava/lang/String;

.field public n:Ljava/lang/String;

.field public o:Ljava/lang/String;

.field public p:Ljava/lang/String;

.field public q:I

.field public r:I

.field public s:I

.field public t:Z

.field public u:Z

.field public v:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21
    const-class v0, Lcom/peel/ui/model/DFPAd;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/peel/ui/model/DFPAd;->w:Ljava/lang/String;

    .line 205
    new-instance v0, Lcom/peel/ui/model/a;

    invoke-direct {v0}, Lcom/peel/ui/model/a;-><init>()V

    sput-object v0, Lcom/peel/ui/model/DFPAd;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/peel/ui/model/DFPAd;->a:Ljava/lang/String;

    .line 24
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/peel/ui/model/DFPAd;->b:Ljava/lang/String;

    .line 25
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/peel/ui/model/DFPAd;->c:Ljava/lang/String;

    .line 27
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/peel/ui/model/DFPAd;->f:Ljava/lang/String;

    .line 28
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/peel/ui/model/DFPAd;->g:Ljava/lang/String;

    .line 29
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/peel/ui/model/DFPAd;->h:Ljava/lang/String;

    .line 30
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/peel/ui/model/DFPAd;->i:Ljava/lang/String;

    .line 31
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/peel/ui/model/DFPAd;->j:Ljava/lang/String;

    .line 32
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/peel/ui/model/DFPAd;->k:Ljava/lang/String;

    .line 33
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/peel/ui/model/DFPAd;->l:Ljava/lang/String;

    .line 34
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/peel/ui/model/DFPAd;->m:Ljava/lang/String;

    .line 35
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/peel/ui/model/DFPAd;->n:Ljava/lang/String;

    .line 36
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/peel/ui/model/DFPAd;->o:Ljava/lang/String;

    .line 37
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/peel/ui/model/DFPAd;->p:Ljava/lang/String;

    .line 38
    const/4 v0, 0x0

    iput v0, p0, Lcom/peel/ui/model/DFPAd;->q:I

    .line 39
    const v0, 0x7fffffff

    iput v0, p0, Lcom/peel/ui/model/DFPAd;->r:I

    .line 42
    const/4 v0, -0x1

    iput v0, p0, Lcom/peel/ui/model/DFPAd;->s:I

    .line 49
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/peel/ui/model/DFPAd;->d:Ljava/util/Map;

    .line 50
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 176
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    const-string/jumbo v1, ""

    iput-object v1, p0, Lcom/peel/ui/model/DFPAd;->a:Ljava/lang/String;

    .line 24
    const-string/jumbo v1, ""

    iput-object v1, p0, Lcom/peel/ui/model/DFPAd;->b:Ljava/lang/String;

    .line 25
    const-string/jumbo v1, ""

    iput-object v1, p0, Lcom/peel/ui/model/DFPAd;->c:Ljava/lang/String;

    .line 27
    const-string/jumbo v1, ""

    iput-object v1, p0, Lcom/peel/ui/model/DFPAd;->f:Ljava/lang/String;

    .line 28
    const-string/jumbo v1, ""

    iput-object v1, p0, Lcom/peel/ui/model/DFPAd;->g:Ljava/lang/String;

    .line 29
    const-string/jumbo v1, ""

    iput-object v1, p0, Lcom/peel/ui/model/DFPAd;->h:Ljava/lang/String;

    .line 30
    const-string/jumbo v1, ""

    iput-object v1, p0, Lcom/peel/ui/model/DFPAd;->i:Ljava/lang/String;

    .line 31
    const-string/jumbo v1, ""

    iput-object v1, p0, Lcom/peel/ui/model/DFPAd;->j:Ljava/lang/String;

    .line 32
    const-string/jumbo v1, ""

    iput-object v1, p0, Lcom/peel/ui/model/DFPAd;->k:Ljava/lang/String;

    .line 33
    const-string/jumbo v1, ""

    iput-object v1, p0, Lcom/peel/ui/model/DFPAd;->l:Ljava/lang/String;

    .line 34
    const-string/jumbo v1, ""

    iput-object v1, p0, Lcom/peel/ui/model/DFPAd;->m:Ljava/lang/String;

    .line 35
    const-string/jumbo v1, ""

    iput-object v1, p0, Lcom/peel/ui/model/DFPAd;->n:Ljava/lang/String;

    .line 36
    const-string/jumbo v1, ""

    iput-object v1, p0, Lcom/peel/ui/model/DFPAd;->o:Ljava/lang/String;

    .line 37
    const-string/jumbo v1, ""

    iput-object v1, p0, Lcom/peel/ui/model/DFPAd;->p:Ljava/lang/String;

    .line 38
    iput v0, p0, Lcom/peel/ui/model/DFPAd;->q:I

    .line 39
    const v1, 0x7fffffff

    iput v1, p0, Lcom/peel/ui/model/DFPAd;->r:I

    .line 42
    const/4 v1, -0x1

    iput v1, p0, Lcom/peel/ui/model/DFPAd;->s:I

    .line 177
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, p0, Lcom/peel/ui/model/DFPAd;->d:Ljava/util/Map;

    .line 179
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/peel/ui/model/DFPAd;->a:Ljava/lang/String;

    .line 180
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/peel/ui/model/DFPAd;->b:Ljava/lang/String;

    .line 181
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/peel/ui/model/DFPAd;->c:Ljava/lang/String;

    .line 182
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/peel/ui/model/DFPAd;->f:Ljava/lang/String;

    .line 183
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/peel/ui/model/DFPAd;->g:Ljava/lang/String;

    .line 184
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/peel/ui/model/DFPAd;->h:Ljava/lang/String;

    .line 185
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/peel/ui/model/DFPAd;->i:Ljava/lang/String;

    .line 186
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/peel/ui/model/DFPAd;->j:Ljava/lang/String;

    .line 187
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/peel/ui/model/DFPAd;->k:Ljava/lang/String;

    .line 188
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/peel/ui/model/DFPAd;->l:Ljava/lang/String;

    .line 189
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/peel/ui/model/DFPAd;->m:Ljava/lang/String;

    .line 190
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/peel/ui/model/DFPAd;->n:Ljava/lang/String;

    .line 191
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/peel/ui/model/DFPAd;->o:Ljava/lang/String;

    .line 192
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/peel/ui/model/DFPAd;->p:Ljava/lang/String;

    .line 193
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, p0, Lcom/peel/ui/model/DFPAd;->q:I

    .line 196
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 197
    :goto_0
    if-ge v0, v1, :cond_0

    .line 198
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 199
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    .line 200
    iget-object v4, p0, Lcom/peel/ui/model/DFPAd;->d:Ljava/util/Map;

    invoke-interface {v4, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 197
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 203
    :cond_0
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;)V
    .locals 8

    .prologue
    const/4 v2, 0x0

    const/4 v1, -0x1

    .line 62
    :try_start_0
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    const-string/jumbo v3, "adunit"

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v3

    .line 63
    const-string/jumbo v0, "id"

    invoke-static {v3, v0}, Lcom/peel/util/bu;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/ui/model/DFPAd;->j:Ljava/lang/String;

    .line 64
    const-string/jumbo v0, "type"

    invoke-static {v3, v0}, Lcom/peel/util/bu;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/ui/model/DFPAd;->h:Ljava/lang/String;

    .line 65
    const-string/jumbo v0, "provider"

    invoke-static {v3, v0}, Lcom/peel/util/bu;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/ui/model/DFPAd;->i:Ljava/lang/String;

    .line 66
    const-string/jumbo v0, "param1"

    invoke-static {v3, v0}, Lcom/peel/util/bu;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/ui/model/DFPAd;->a:Ljava/lang/String;

    .line 67
    const-string/jumbo v0, "param2"

    invoke-static {v3, v0}, Lcom/peel/util/bu;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/ui/model/DFPAd;->c:Ljava/lang/String;

    .line 68
    const-string/jumbo v0, "placement"

    invoke-static {v3, v0}, Lcom/peel/util/bu;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/ui/model/DFPAd;->g:Ljava/lang/String;

    .line 69
    const-string/jumbo v0, "action"

    invoke-static {v3, v0}, Lcom/peel/util/bu;->c(Lorg/json/JSONObject;Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    .line 70
    if-eqz v0, :cond_0

    .line 71
    const-string/jumbo v4, "type"

    invoke-static {v0, v4}, Lcom/peel/util/bu;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/peel/ui/model/DFPAd;->f:Ljava/lang/String;

    .line 72
    const-string/jumbo v4, "episodeid"

    invoke-static {v0, v4}, Lcom/peel/util/bu;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/peel/ui/model/DFPAd;->o:Ljava/lang/String;

    .line 73
    const-string/jumbo v4, "showid"

    invoke-static {v0, v4}, Lcom/peel/util/bu;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/ui/model/DFPAd;->p:Ljava/lang/String;

    .line 75
    :cond_0
    iget-object v0, p0, Lcom/peel/ui/model/DFPAd;->h:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/peel/ui/model/DFPAd;->h:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v4, "banner"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 76
    iget-object v0, p0, Lcom/peel/ui/model/DFPAd;->c:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 77
    iget-object v0, p0, Lcom/peel/ui/model/DFPAd;->c:Ljava/lang/String;

    const-string/jumbo v4, ","

    invoke-virtual {v0, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    const/4 v4, 0x1

    aget-object v0, v0, v4

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/peel/ui/model/DFPAd;->q:I

    .line 80
    :cond_1
    const-string/jumbo v0, "adunittag"

    invoke-static {v3, v0}, Lcom/peel/util/bu;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/ui/model/DFPAd;->b:Ljava/lang/String;

    .line 81
    const-string/jumbo v0, "param3"

    invoke-static {v3, v0}, Lcom/peel/util/bu;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/ui/model/DFPAd;->k:Ljava/lang/String;

    .line 82
    const-string/jumbo v0, "param4"

    invoke-static {v3, v0}, Lcom/peel/util/bu;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/ui/model/DFPAd;->l:Ljava/lang/String;

    .line 83
    const-string/jumbo v0, "param5"

    invoke-static {v3, v0}, Lcom/peel/util/bu;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/ui/model/DFPAd;->m:Ljava/lang/String;

    .line 84
    const-string/jumbo v0, "param6"

    invoke-static {v3, v0}, Lcom/peel/util/bu;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/ui/model/DFPAd;->n:Ljava/lang/String;

    .line 86
    iget-object v0, p0, Lcom/peel/ui/model/DFPAd;->l:Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/peel/ui/model/DFPAd;->l:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/peel/ui/model/DFPAd;->l:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v4, "null"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 87
    :cond_2
    const/4 v0, -0x1

    iput v0, p0, Lcom/peel/ui/model/DFPAd;->s:I

    .line 96
    :goto_0
    const-string/jumbo v0, "tags"

    invoke-static {v3, v0}, Lcom/peel/util/bu;->c(Lorg/json/JSONObject;Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v4

    .line 97
    if-eqz v4, :cond_5

    .line 98
    invoke-virtual {v4}, Lorg/json/JSONObject;->keys()Ljava/util/Iterator;

    move-result-object v5

    .line 99
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/peel/ui/model/DFPAd;->d:Ljava/util/Map;

    .line 100
    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 101
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 102
    iget-object v6, p0, Lcom/peel/ui/model/DFPAd;->d:Ljava/util/Map;

    invoke-virtual {v4, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-interface {v6, v0, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 140
    :catch_0
    move-exception v0

    .line 141
    sget-object v1, Lcom/peel/ui/model/DFPAd;->w:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 143
    :cond_3
    :goto_2
    return-void

    .line 90
    :cond_4
    :try_start_1
    iget-object v0, p0, Lcom/peel/ui/model/DFPAd;->l:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/peel/ui/model/DFPAd;->s:I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 91
    :catch_1
    move-exception v0

    goto :goto_0

    .line 107
    :cond_5
    :try_start_2
    const-string/jumbo v0, "urlparams"

    invoke-static {v3, v0}, Lcom/peel/util/bu;->c(Lorg/json/JSONObject;Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v5

    .line 108
    if-eqz v4, :cond_6

    .line 109
    invoke-virtual {v5}, Lorg/json/JSONObject;->keys()Ljava/util/Iterator;

    move-result-object v4

    .line 110
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/peel/ui/model/DFPAd;->e:Ljava/util/Map;

    .line 111
    :goto_3
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 112
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 113
    iget-object v6, p0, Lcom/peel/ui/model/DFPAd;->e:Ljava/util/Map;

    invoke-virtual {v5, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-interface {v6, v0, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_3

    .line 118
    :cond_6
    iget-object v0, p0, Lcom/peel/ui/model/DFPAd;->b:Ljava/lang/String;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/peel/ui/model/DFPAd;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 119
    :cond_7
    const-string/jumbo v0, "div-gpt-ad-1411508372903-0"

    iput-object v0, p0, Lcom/peel/ui/model/DFPAd;->b:Ljava/lang/String;

    .line 122
    :cond_8
    const-string/jumbo v0, "settings"

    invoke-static {v3, v0}, Lcom/peel/util/bu;->c(Lorg/json/JSONObject;Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v3

    .line 123
    if-eqz v3, :cond_3

    .line 124
    const-string/jumbo v0, "scrollrefresh"

    invoke-static {v3, v0}, Lcom/peel/util/bu;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 125
    if-eqz v0, :cond_9

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_d

    :cond_9
    move v0, v2

    .line 126
    :goto_4
    iput-boolean v0, p0, Lcom/peel/ui/model/DFPAd;->t:Z

    .line 128
    const-string/jumbo v0, "dfpsdk"

    invoke-static {v3, v0}, Lcom/peel/util/bu;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 129
    if-eqz v0, :cond_a

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_e

    :cond_a
    move v0, v2

    .line 130
    :goto_5
    iput-boolean v0, p0, Lcom/peel/ui/model/DFPAd;->u:Z

    .line 132
    const-string/jumbo v0, "delay"

    invoke-static {v3, v0}, Lcom/peel/util/bu;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 133
    if-eqz v0, :cond_b

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_f

    :cond_b
    move v0, v1

    :goto_6
    iput v0, p0, Lcom/peel/ui/model/DFPAd;->v:I

    .line 135
    const-string/jumbo v0, "limit"

    invoke-static {v3, v0}, Lcom/peel/util/bu;->b(Lorg/json/JSONObject;Ljava/lang/String;)I

    move-result v0

    .line 136
    if-nez v0, :cond_c

    const v0, 0x7fffffff

    :cond_c
    iput v0, p0, Lcom/peel/ui/model/DFPAd;->r:I

    goto/16 :goto_2

    .line 126
    :cond_d
    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v4, "true"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_4

    .line 130
    :cond_e
    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v2, "true"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_5

    .line 133
    :cond_f
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    move-result v0

    goto :goto_6
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 147
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 3

    .prologue
    .line 152
    iget-object v0, p0, Lcom/peel/ui/model/DFPAd;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 153
    iget-object v0, p0, Lcom/peel/ui/model/DFPAd;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 154
    iget-object v0, p0, Lcom/peel/ui/model/DFPAd;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 155
    iget-object v0, p0, Lcom/peel/ui/model/DFPAd;->f:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 156
    iget-object v0, p0, Lcom/peel/ui/model/DFPAd;->g:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 157
    iget-object v0, p0, Lcom/peel/ui/model/DFPAd;->h:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 158
    iget-object v0, p0, Lcom/peel/ui/model/DFPAd;->i:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 159
    iget-object v0, p0, Lcom/peel/ui/model/DFPAd;->j:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 160
    iget-object v0, p0, Lcom/peel/ui/model/DFPAd;->k:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 161
    iget-object v0, p0, Lcom/peel/ui/model/DFPAd;->l:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 162
    iget-object v0, p0, Lcom/peel/ui/model/DFPAd;->m:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 163
    iget-object v0, p0, Lcom/peel/ui/model/DFPAd;->n:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 164
    iget-object v0, p0, Lcom/peel/ui/model/DFPAd;->o:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 165
    iget-object v0, p0, Lcom/peel/ui/model/DFPAd;->p:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 166
    iget v0, p0, Lcom/peel/ui/model/DFPAd;->q:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 168
    iget-object v0, p0, Lcom/peel/ui/model/DFPAd;->d:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 169
    iget-object v0, p0, Lcom/peel/ui/model/DFPAd;->d:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 170
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 171
    iget-object v2, p0, Lcom/peel/ui/model/DFPAd;->d:Ljava/util/Map;

    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_0

    .line 174
    :cond_0
    return-void
.end method
