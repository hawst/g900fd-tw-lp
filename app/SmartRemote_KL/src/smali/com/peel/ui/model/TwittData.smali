.class public Lcom/peel/ui/model/TwittData;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/os/Parcelable;
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable;",
        "Ljava/lang/Comparable",
        "<",
        "Lcom/peel/ui/model/TwittData;",
        ">;"
    }
.end annotation


# instance fields
.field a:Ljava/lang/String;

.field b:Ljava/lang/String;

.field c:Ljava/lang/String;

.field d:Ljava/lang/String;

.field e:Ljava/lang/String;

.field f:Ljava/lang/String;

.field g:Ljava/lang/String;

.field h:Z

.field i:Z

.field j:I

.field k:[Lcom/peel/ui/model/TwittData$TwittEntries;

.field l:[Lcom/peel/ui/model/TwittData$TwittEntries;

.field m:[Lcom/peel/ui/model/TwittData$TwittEntries;

.field public n:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/peel/ui/model/TwittData;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 126
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 160
    new-instance v0, Lcom/peel/ui/model/b;

    invoke-direct {v0, p0}, Lcom/peel/ui/model/b;-><init>(Lcom/peel/ui/model/TwittData;)V

    iput-object v0, p0, Lcom/peel/ui/model/TwittData;->n:Landroid/os/Parcelable$Creator;

    .line 126
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 127
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 160
    new-instance v0, Lcom/peel/ui/model/b;

    invoke-direct {v0, p0}, Lcom/peel/ui/model/b;-><init>(Lcom/peel/ui/model/TwittData;)V

    iput-object v0, p0, Lcom/peel/ui/model/TwittData;->n:Landroid/os/Parcelable$Creator;

    .line 128
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/ui/model/TwittData;->a:Ljava/lang/String;

    .line 129
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/ui/model/TwittData;->b:Ljava/lang/String;

    .line 130
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/ui/model/TwittData;->c:Ljava/lang/String;

    .line 131
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/ui/model/TwittData;->d:Ljava/lang/String;

    .line 132
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/ui/model/TwittData;->e:Ljava/lang/String;

    .line 133
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/ui/model/TwittData;->f:Ljava/lang/String;

    .line 134
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/ui/model/TwittData;->g:Ljava/lang/String;

    .line 135
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/peel/ui/model/TwittData;->h:Z

    .line 136
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_1

    :goto_1
    iput-boolean v1, p0, Lcom/peel/ui/model/TwittData;->i:Z

    .line 137
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/peel/ui/model/TwittData;->j:I

    .line 138
    const-class v0, Lcom/peel/ui/model/TwittData;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelableArray(Ljava/lang/ClassLoader;)[Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, [Lcom/peel/ui/model/TwittData$TwittEntries;

    check-cast v0, [Lcom/peel/ui/model/TwittData$TwittEntries;

    iput-object v0, p0, Lcom/peel/ui/model/TwittData;->k:[Lcom/peel/ui/model/TwittData$TwittEntries;

    .line 139
    const-class v0, Lcom/peel/ui/model/TwittData;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelableArray(Ljava/lang/ClassLoader;)[Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, [Lcom/peel/ui/model/TwittData$TwittEntries;

    check-cast v0, [Lcom/peel/ui/model/TwittData$TwittEntries;

    iput-object v0, p0, Lcom/peel/ui/model/TwittData;->l:[Lcom/peel/ui/model/TwittData$TwittEntries;

    .line 140
    const-class v0, Lcom/peel/ui/model/TwittData;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelableArray(Ljava/lang/ClassLoader;)[Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, [Lcom/peel/ui/model/TwittData$TwittEntries;

    check-cast v0, [Lcom/peel/ui/model/TwittData$TwittEntries;

    iput-object v0, p0, Lcom/peel/ui/model/TwittData;->m:[Lcom/peel/ui/model/TwittData$TwittEntries;

    .line 141
    return-void

    :cond_0
    move v0, v2

    .line 135
    goto :goto_0

    :cond_1
    move v1, v2

    .line 136
    goto :goto_1
.end method


# virtual methods
.method public a(Lcom/peel/ui/model/TwittData;)I
    .locals 2

    .prologue
    .line 118
    iget v0, p0, Lcom/peel/ui/model/TwittData;->j:I

    invoke-virtual {p1}, Lcom/peel/ui/model/TwittData;->k()I

    move-result v1

    sub-int/2addr v0, v1

    return v0
.end method

.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 16
    iget-object v0, p0, Lcom/peel/ui/model/TwittData;->a:Ljava/lang/String;

    return-object v0
.end method

.method public a(I)V
    .locals 0

    .prologue
    .line 102
    iput p1, p0, Lcom/peel/ui/model/TwittData;->j:I

    .line 103
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 20
    iput-object p1, p0, Lcom/peel/ui/model/TwittData;->a:Ljava/lang/String;

    .line 21
    return-void
.end method

.method public a(Z)V
    .locals 0

    .prologue
    .line 72
    iput-boolean p1, p0, Lcom/peel/ui/model/TwittData;->h:Z

    .line 73
    return-void
.end method

.method public a([Lcom/peel/ui/model/TwittData$TwittEntries;)V
    .locals 0

    .prologue
    .line 92
    iput-object p1, p0, Lcom/peel/ui/model/TwittData;->k:[Lcom/peel/ui/model/TwittData$TwittEntries;

    .line 93
    return-void
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lcom/peel/ui/model/TwittData;->c:Ljava/lang/String;

    return-object v0
.end method

.method public b(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 28
    iput-object p1, p0, Lcom/peel/ui/model/TwittData;->c:Ljava/lang/String;

    .line 29
    return-void
.end method

.method public b(Z)V
    .locals 0

    .prologue
    .line 76
    iput-boolean p1, p0, Lcom/peel/ui/model/TwittData;->i:Z

    .line 77
    return-void
.end method

.method public b([Lcom/peel/ui/model/TwittData$TwittEntries;)V
    .locals 0

    .prologue
    .line 96
    iput-object p1, p0, Lcom/peel/ui/model/TwittData;->m:[Lcom/peel/ui/model/TwittData$TwittEntries;

    .line 97
    return-void
.end method

.method public c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/peel/ui/model/TwittData;->e:Ljava/lang/String;

    return-object v0
.end method

.method public c(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 36
    iput-object p1, p0, Lcom/peel/ui/model/TwittData;->d:Ljava/lang/String;

    .line 37
    return-void
.end method

.method public c([Lcom/peel/ui/model/TwittData$TwittEntries;)V
    .locals 0

    .prologue
    .line 99
    iput-object p1, p0, Lcom/peel/ui/model/TwittData;->l:[Lcom/peel/ui/model/TwittData$TwittEntries;

    .line 100
    return-void
.end method

.method public synthetic compareTo(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 9
    check-cast p1, Lcom/peel/ui/model/TwittData;

    invoke-virtual {p0, p1}, Lcom/peel/ui/model/TwittData;->a(Lcom/peel/ui/model/TwittData;)I

    move-result v0

    return v0
.end method

.method public d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/peel/ui/model/TwittData;->f:Ljava/lang/String;

    return-object v0
.end method

.method public d(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 44
    iput-object p1, p0, Lcom/peel/ui/model/TwittData;->e:Ljava/lang/String;

    .line 45
    return-void
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 123
    const/4 v0, 0x0

    return v0
.end method

.method public e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/peel/ui/model/TwittData;->b:Ljava/lang/String;

    return-object v0
.end method

.method public e(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 52
    iput-object p1, p0, Lcom/peel/ui/model/TwittData;->f:Ljava/lang/String;

    .line 53
    return-void
.end method

.method public f(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 56
    iput-object p1, p0, Lcom/peel/ui/model/TwittData;->b:Ljava/lang/String;

    .line 57
    return-void
.end method

.method public f()Z
    .locals 1

    .prologue
    .line 64
    iget-boolean v0, p0, Lcom/peel/ui/model/TwittData;->h:Z

    return v0
.end method

.method public g(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 113
    iput-object p1, p0, Lcom/peel/ui/model/TwittData;->g:Ljava/lang/String;

    .line 114
    return-void
.end method

.method public g()Z
    .locals 1

    .prologue
    .line 68
    iget-boolean v0, p0, Lcom/peel/ui/model/TwittData;->i:Z

    return v0
.end method

.method public h()[Lcom/peel/ui/model/TwittData$TwittEntries;
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lcom/peel/ui/model/TwittData;->k:[Lcom/peel/ui/model/TwittData$TwittEntries;

    return-object v0
.end method

.method public i()[Lcom/peel/ui/model/TwittData$TwittEntries;
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lcom/peel/ui/model/TwittData;->m:[Lcom/peel/ui/model/TwittData$TwittEntries;

    return-object v0
.end method

.method public j()[Lcom/peel/ui/model/TwittData$TwittEntries;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/peel/ui/model/TwittData;->l:[Lcom/peel/ui/model/TwittData$TwittEntries;

    return-object v0
.end method

.method public k()I
    .locals 1

    .prologue
    .line 106
    iget v0, p0, Lcom/peel/ui/model/TwittData;->j:I

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 145
    iget-object v0, p0, Lcom/peel/ui/model/TwittData;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 146
    iget-object v0, p0, Lcom/peel/ui/model/TwittData;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 147
    iget-object v0, p0, Lcom/peel/ui/model/TwittData;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 148
    iget-object v0, p0, Lcom/peel/ui/model/TwittData;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 149
    iget-object v0, p0, Lcom/peel/ui/model/TwittData;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 150
    iget-object v0, p0, Lcom/peel/ui/model/TwittData;->f:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 151
    iget-object v0, p0, Lcom/peel/ui/model/TwittData;->g:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 152
    iget-boolean v0, p0, Lcom/peel/ui/model/TwittData;->h:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 153
    iget-boolean v0, p0, Lcom/peel/ui/model/TwittData;->i:Z

    if-eqz v0, :cond_1

    :goto_1
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 154
    iget v0, p0, Lcom/peel/ui/model/TwittData;->j:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 155
    iget-object v0, p0, Lcom/peel/ui/model/TwittData;->k:[Lcom/peel/ui/model/TwittData$TwittEntries;

    invoke-virtual {p1, v0, v2}, Landroid/os/Parcel;->writeParcelableArray([Landroid/os/Parcelable;I)V

    .line 156
    iget-object v0, p0, Lcom/peel/ui/model/TwittData;->l:[Lcom/peel/ui/model/TwittData$TwittEntries;

    invoke-virtual {p1, v0, v2}, Landroid/os/Parcel;->writeParcelableArray([Landroid/os/Parcelable;I)V

    .line 157
    iget-object v0, p0, Lcom/peel/ui/model/TwittData;->m:[Lcom/peel/ui/model/TwittData$TwittEntries;

    invoke-virtual {p1, v0, v2}, Landroid/os/Parcel;->writeParcelableArray([Landroid/os/Parcelable;I)V

    .line 158
    return-void

    :cond_0
    move v0, v2

    .line 152
    goto :goto_0

    :cond_1
    move v1, v2

    .line 153
    goto :goto_1
.end method
