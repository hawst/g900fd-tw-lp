.class Lcom/peel/ui/model/c;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/peel/ui/model/TwittData$TwittEntries;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/peel/ui/model/TwittData$TwittEntries;


# direct methods
.method constructor <init>(Lcom/peel/ui/model/TwittData$TwittEntries;)V
    .locals 0

    .prologue
    .line 209
    iput-object p1, p0, Lcom/peel/ui/model/c;->a:Lcom/peel/ui/model/TwittData$TwittEntries;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/os/Parcel;)Lcom/peel/ui/model/TwittData$TwittEntries;
    .locals 2

    .prologue
    .line 210
    new-instance v0, Lcom/peel/ui/model/TwittData$TwittEntries;

    iget-object v1, p0, Lcom/peel/ui/model/c;->a:Lcom/peel/ui/model/TwittData$TwittEntries;

    iget-object v1, v1, Lcom/peel/ui/model/TwittData$TwittEntries;->e:Lcom/peel/ui/model/TwittData;

    invoke-direct {v0, v1, p1}, Lcom/peel/ui/model/TwittData$TwittEntries;-><init>(Lcom/peel/ui/model/TwittData;Landroid/os/Parcel;)V

    return-object v0
.end method

.method public a(I)[Lcom/peel/ui/model/TwittData$TwittEntries;
    .locals 1

    .prologue
    .line 211
    new-array v0, p1, [Lcom/peel/ui/model/TwittData$TwittEntries;

    return-object v0
.end method

.method public synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 209
    invoke-virtual {p0, p1}, Lcom/peel/ui/model/c;->a(Landroid/os/Parcel;)Lcom/peel/ui/model/TwittData$TwittEntries;

    move-result-object v0

    return-object v0
.end method

.method public synthetic newArray(I)[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 209
    invoke-virtual {p0, p1}, Lcom/peel/ui/model/c;->a(I)[Lcom/peel/ui/model/TwittData$TwittEntries;

    move-result-object v0

    return-object v0
.end method
