.class public Lcom/peel/ui/model/TwittData$TwittEntries;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/os/Parcelable;


# instance fields
.field a:I

.field b:I

.field c:Ljava/lang/String;

.field public d:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/peel/ui/model/TwittData$TwittEntries;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic e:Lcom/peel/ui/model/TwittData;


# direct methods
.method public constructor <init>(Lcom/peel/ui/model/TwittData;IILjava/lang/String;)V
    .locals 1

    .prologue
    .line 169
    iput-object p1, p0, Lcom/peel/ui/model/TwittData$TwittEntries;->e:Lcom/peel/ui/model/TwittData;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 209
    new-instance v0, Lcom/peel/ui/model/c;

    invoke-direct {v0, p0}, Lcom/peel/ui/model/c;-><init>(Lcom/peel/ui/model/TwittData$TwittEntries;)V

    iput-object v0, p0, Lcom/peel/ui/model/TwittData$TwittEntries;->d:Landroid/os/Parcelable$Creator;

    .line 170
    iput p2, p0, Lcom/peel/ui/model/TwittData$TwittEntries;->a:I

    .line 171
    iput p3, p0, Lcom/peel/ui/model/TwittData$TwittEntries;->b:I

    .line 172
    iput-object p4, p0, Lcom/peel/ui/model/TwittData$TwittEntries;->c:Ljava/lang/String;

    .line 173
    return-void
.end method

.method public constructor <init>(Lcom/peel/ui/model/TwittData;Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 175
    iput-object p1, p0, Lcom/peel/ui/model/TwittData$TwittEntries;->e:Lcom/peel/ui/model/TwittData;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 209
    new-instance v0, Lcom/peel/ui/model/c;

    invoke-direct {v0, p0}, Lcom/peel/ui/model/c;-><init>(Lcom/peel/ui/model/TwittData$TwittEntries;)V

    iput-object v0, p0, Lcom/peel/ui/model/TwittData$TwittEntries;->d:Landroid/os/Parcelable$Creator;

    .line 176
    invoke-virtual {p0, p2}, Lcom/peel/ui/model/TwittData$TwittEntries;->a(Landroid/os/Parcel;)V

    .line 177
    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 180
    iget v0, p0, Lcom/peel/ui/model/TwittData$TwittEntries;->a:I

    return v0
.end method

.method public a(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 204
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/peel/ui/model/TwittData$TwittEntries;->a:I

    .line 205
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/peel/ui/model/TwittData$TwittEntries;->b:I

    .line 206
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/ui/model/TwittData$TwittEntries;->c:Ljava/lang/String;

    .line 207
    return-void
.end method

.method public b()I
    .locals 1

    .prologue
    .line 184
    iget v0, p0, Lcom/peel/ui/model/TwittData$TwittEntries;->b:I

    return v0
.end method

.method public c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 188
    iget-object v0, p0, Lcom/peel/ui/model/TwittData$TwittEntries;->c:Ljava/lang/String;

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 193
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 198
    iget v0, p0, Lcom/peel/ui/model/TwittData$TwittEntries;->a:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 199
    iget v0, p0, Lcom/peel/ui/model/TwittData$TwittEntries;->b:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 200
    iget-object v0, p0, Lcom/peel/ui/model/TwittData$TwittEntries;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 201
    return-void
.end method
