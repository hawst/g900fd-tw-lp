.class Lcom/peel/ui/fz;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/peel/ui/fx;


# direct methods
.method constructor <init>(Lcom/peel/ui/fx;)V
    .locals 0

    .prologue
    .line 200
    iput-object p1, p0, Lcom/peel/ui/fz;->a:Lcom/peel/ui/fx;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    .line 204
    iget-object v0, p0, Lcom/peel/ui/fz;->a:Lcom/peel/ui/fx;

    invoke-static {v0}, Lcom/peel/ui/fx;->a(Lcom/peel/ui/fx;)Lcom/peel/content/listing/LiveListing;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/content/listing/LiveListing;->k()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "sports"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 205
    iget-object v0, p0, Lcom/peel/ui/fz;->a:Lcom/peel/ui/fx;

    invoke-static {v0}, Lcom/peel/ui/fx;->b(Lcom/peel/ui/fx;)Landroid/widget/CheckedTextView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/CheckedTextView;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 206
    iget-object v0, p0, Lcom/peel/ui/fz;->a:Lcom/peel/ui/fx;

    iget-object v1, p0, Lcom/peel/ui/fz;->a:Lcom/peel/ui/fx;

    invoke-static {v1}, Lcom/peel/ui/fx;->c(Lcom/peel/ui/fx;)Landroid/os/Bundle;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/peel/ui/fx;->a(Lcom/peel/ui/fx;Landroid/os/Bundle;)V

    .line 209
    :cond_0
    iget-object v0, p0, Lcom/peel/ui/fz;->a:Lcom/peel/ui/fx;

    invoke-static {v0}, Lcom/peel/ui/fx;->d(Lcom/peel/ui/fx;)Landroid/widget/CheckedTextView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/CheckedTextView;->isChecked()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/peel/ui/fz;->a:Lcom/peel/ui/fx;

    invoke-static {v0}, Lcom/peel/ui/fx;->e(Lcom/peel/ui/fx;)Landroid/widget/CheckedTextView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/CheckedTextView;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 210
    :cond_1
    iget-object v0, p0, Lcom/peel/ui/fz;->a:Lcom/peel/ui/fx;

    invoke-static {v0}, Lcom/peel/ui/fx;->a(Lcom/peel/ui/fx;)Lcom/peel/content/listing/LiveListing;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/content/listing/LiveListing;->s()[Lcom/peel/data/SportsTeam;

    move-result-object v0

    .line 211
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 213
    iget-object v2, p0, Lcom/peel/ui/fz;->a:Lcom/peel/ui/fx;

    invoke-static {v2}, Lcom/peel/ui/fx;->e(Lcom/peel/ui/fx;)Landroid/widget/CheckedTextView;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/CheckedTextView;->isChecked()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 214
    const/4 v2, 0x0

    aget-object v2, v0, v2

    invoke-virtual {v2}, Lcom/peel/data/SportsTeam;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 216
    :cond_2
    iget-object v2, p0, Lcom/peel/ui/fz;->a:Lcom/peel/ui/fx;

    invoke-static {v2}, Lcom/peel/ui/fx;->d(Lcom/peel/ui/fx;)Landroid/widget/CheckedTextView;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/CheckedTextView;->isChecked()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 217
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, ","

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/4 v3, 0x1

    aget-object v0, v0, v3

    invoke-virtual {v0}, Lcom/peel/data/SportsTeam;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 220
    :cond_3
    iget-object v0, p0, Lcom/peel/ui/fz;->a:Lcom/peel/ui/fx;

    invoke-static {v0}, Lcom/peel/ui/fx;->c(Lcom/peel/ui/fx;)Landroid/os/Bundle;

    move-result-object v0

    const-string/jumbo v2, "remindertype"

    const-string/jumbo v3, "team"

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 221
    iget-object v0, p0, Lcom/peel/ui/fz;->a:Lcom/peel/ui/fx;

    invoke-static {v0}, Lcom/peel/ui/fx;->c(Lcom/peel/ui/fx;)Landroid/os/Bundle;

    move-result-object v0

    const-string/jumbo v2, "teamids"

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 222
    iget-object v0, p0, Lcom/peel/ui/fz;->a:Lcom/peel/ui/fx;

    iget-object v1, p0, Lcom/peel/ui/fz;->a:Lcom/peel/ui/fx;

    invoke-static {v1}, Lcom/peel/ui/fx;->c(Lcom/peel/ui/fx;)Landroid/os/Bundle;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/peel/ui/fx;->a(Lcom/peel/ui/fx;Landroid/os/Bundle;)V

    .line 234
    :cond_4
    :goto_0
    return-void

    .line 225
    :cond_5
    iget-object v0, p0, Lcom/peel/ui/fz;->a:Lcom/peel/ui/fx;

    invoke-static {v0}, Lcom/peel/ui/fx;->b(Lcom/peel/ui/fx;)Landroid/widget/CheckedTextView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/CheckedTextView;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 226
    iget-object v0, p0, Lcom/peel/ui/fz;->a:Lcom/peel/ui/fx;

    iget-object v1, p0, Lcom/peel/ui/fz;->a:Lcom/peel/ui/fx;

    invoke-static {v1}, Lcom/peel/ui/fx;->c(Lcom/peel/ui/fx;)Landroid/os/Bundle;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/peel/ui/fx;->a(Lcom/peel/ui/fx;Landroid/os/Bundle;)V

    goto :goto_0

    .line 228
    :cond_6
    iget-object v0, p0, Lcom/peel/ui/fz;->a:Lcom/peel/ui/fx;

    invoke-static {v0}, Lcom/peel/ui/fx;->e(Lcom/peel/ui/fx;)Landroid/widget/CheckedTextView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/CheckedTextView;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_7

    const-string/jumbo v0, "new"

    .line 229
    :goto_1
    iget-object v1, p0, Lcom/peel/ui/fz;->a:Lcom/peel/ui/fx;

    invoke-static {v1}, Lcom/peel/ui/fx;->c(Lcom/peel/ui/fx;)Landroid/os/Bundle;

    move-result-object v1

    const-string/jumbo v2, "extra"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 230
    iget-object v0, p0, Lcom/peel/ui/fz;->a:Lcom/peel/ui/fx;

    invoke-static {v0}, Lcom/peel/ui/fx;->c(Lcom/peel/ui/fx;)Landroid/os/Bundle;

    move-result-object v0

    const-string/jumbo v1, "remindertype"

    const-string/jumbo v2, "show"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 231
    iget-object v0, p0, Lcom/peel/ui/fz;->a:Lcom/peel/ui/fx;

    iget-object v1, p0, Lcom/peel/ui/fz;->a:Lcom/peel/ui/fx;

    invoke-static {v1}, Lcom/peel/ui/fx;->c(Lcom/peel/ui/fx;)Landroid/os/Bundle;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/peel/ui/fx;->a(Lcom/peel/ui/fx;Landroid/os/Bundle;)V

    goto :goto_0

    .line 228
    :cond_7
    const-string/jumbo v0, "all"

    goto :goto_1
.end method
