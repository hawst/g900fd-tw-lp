.class Lcom/peel/ui/ha;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/peel/widget/al;


# instance fields
.field final synthetic a:Lcom/peel/ui/gt;


# direct methods
.method constructor <init>(Lcom/peel/ui/gt;)V
    .locals 0

    .prologue
    .line 570
    iput-object p1, p0, Lcom/peel/ui/ha;->a:Lcom/peel/ui/gt;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v4, 0x0

    .line 572
    iget-object v0, p0, Lcom/peel/ui/ha;->a:Lcom/peel/ui/gt;

    invoke-static {v0}, Lcom/peel/ui/gt;->g(Lcom/peel/ui/gt;)Lcom/peel/widget/SlidingDrawer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/widget/SlidingDrawer;->getHandle()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ViewFlipper;

    invoke-virtual {v0, v4}, Landroid/widget/ViewFlipper;->setDisplayedChild(I)V

    .line 573
    iget-object v0, p0, Lcom/peel/ui/ha;->a:Lcom/peel/ui/gt;

    invoke-static {v0, v4, v3}, Lcom/peel/ui/gt;->a(Lcom/peel/ui/gt;IZ)V

    .line 574
    iget-object v0, p0, Lcom/peel/ui/ha;->a:Lcom/peel/ui/gt;

    invoke-static {v0}, Lcom/peel/ui/gt;->n(Lcom/peel/ui/gt;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/peel/ui/ha;->a:Lcom/peel/ui/gt;

    invoke-static {v0}, Lcom/peel/ui/gt;->p(Lcom/peel/ui/gt;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/peel/ui/ha;->a:Lcom/peel/ui/gt;

    invoke-static {v0}, Lcom/peel/ui/gt;->o(Lcom/peel/ui/gt;)Lcom/peel/widget/SlidingDrawer;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/peel/ui/ha;->a:Lcom/peel/ui/gt;

    invoke-static {v0}, Lcom/peel/ui/gt;->o(Lcom/peel/ui/gt;)Lcom/peel/widget/SlidingDrawer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/widget/SlidingDrawer;->f()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/peel/ui/ha;->a:Lcom/peel/ui/gt;

    invoke-static {v0}, Lcom/peel/ui/gt;->o(Lcom/peel/ui/gt;)Lcom/peel/widget/SlidingDrawer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/widget/SlidingDrawer;->e()Z

    move-result v0

    if-nez v0, :cond_0

    .line 575
    const/high16 v0, 0x42480000    # 50.0f

    iget-object v1, p0, Lcom/peel/ui/ha;->a:Lcom/peel/ui/gt;

    invoke-virtual {v1}, Lcom/peel/ui/gt;->n()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    invoke-static {v3, v0, v1}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    .line 577
    new-instance v1, Landroid/view/animation/TranslateAnimation;

    invoke-direct {v1, v2, v2, v0, v2}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    .line 578
    const-wide/16 v2, 0x1f4

    invoke-virtual {v1, v2, v3}, Landroid/view/animation/TranslateAnimation;->setDuration(J)V

    .line 579
    iget-object v0, p0, Lcom/peel/ui/ha;->a:Lcom/peel/ui/gt;

    invoke-static {v0}, Lcom/peel/ui/gt;->o(Lcom/peel/ui/gt;)Lcom/peel/widget/SlidingDrawer;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/peel/widget/SlidingDrawer;->setVisibility(I)V

    .line 580
    iget-object v0, p0, Lcom/peel/ui/ha;->a:Lcom/peel/ui/gt;

    invoke-static {v0}, Lcom/peel/ui/gt;->o(Lcom/peel/ui/gt;)Lcom/peel/widget/SlidingDrawer;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/peel/widget/SlidingDrawer;->startAnimation(Landroid/view/animation/Animation;)V

    .line 581
    iget-object v0, p0, Lcom/peel/ui/ha;->a:Lcom/peel/ui/gt;

    invoke-static {v0, v4}, Lcom/peel/ui/gt;->a(Lcom/peel/ui/gt;Z)Z

    .line 583
    :cond_0
    return-void
.end method
