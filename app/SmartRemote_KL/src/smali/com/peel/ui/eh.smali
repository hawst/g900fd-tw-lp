.class Lcom/peel/ui/eh;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:I

.field final synthetic b:Lcom/peel/data/Channel;

.field final synthetic c:Lcom/peel/ui/ef;


# direct methods
.method constructor <init>(Lcom/peel/ui/ef;ILcom/peel/data/Channel;)V
    .locals 0

    .prologue
    .line 361
    iput-object p1, p0, Lcom/peel/ui/eh;->c:Lcom/peel/ui/ef;

    iput p2, p0, Lcom/peel/ui/eh;->a:I

    iput-object p3, p0, Lcom/peel/ui/eh;->b:Lcom/peel/data/Channel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 364
    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    sget-object v1, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v1}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/peel/control/am;->c(Lcom/peel/control/RoomControl;)[Lcom/peel/control/h;

    move-result-object v0

    .line 365
    if-eqz v0, :cond_0

    array-length v0, v0

    if-nez v0, :cond_2

    .line 366
    :cond_0
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 367
    const-string/jumbo v1, "passback_clazz"

    iget-object v2, p0, Lcom/peel/ui/eh;->c:Lcom/peel/ui/ef;

    iget-object v2, v2, Lcom/peel/ui/ef;->a:Lcom/peel/ui/du;

    invoke-virtual {v2}, Lcom/peel/ui/du;->m()Landroid/support/v4/app/ae;

    move-result-object v2

    invoke-static {v2}, Lcom/peel/d/e;->a(Landroid/support/v4/app/ae;)Lcom/peel/d/u;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 368
    const-string/jumbo v1, "passback_bundle"

    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 369
    iget-object v1, p0, Lcom/peel/ui/eh;->c:Lcom/peel/ui/ef;

    iget-object v1, v1, Lcom/peel/ui/ef;->a:Lcom/peel/ui/du;

    invoke-virtual {v1}, Lcom/peel/ui/du;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    const-class v2, Lcom/peel/i/dr;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lcom/peel/d/e;->b(Landroid/support/v4/app/ae;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 378
    :cond_1
    :goto_0
    return-void

    .line 371
    :cond_2
    iget-object v0, p0, Lcom/peel/ui/eh;->c:Lcom/peel/ui/ef;

    iget-object v0, v0, Lcom/peel/ui/ef;->a:Lcom/peel/ui/du;

    iget-boolean v0, v0, Lcom/peel/ui/du;->h:Z

    if-nez v0, :cond_1

    .line 372
    iget-object v0, p0, Lcom/peel/ui/eh;->c:Lcom/peel/ui/ef;

    iget-object v0, v0, Lcom/peel/ui/ef;->a:Lcom/peel/ui/du;

    invoke-static {v0, p1}, Lcom/peel/ui/du;->a(Lcom/peel/ui/du;Landroid/view/View;)V

    .line 373
    iget-object v0, p0, Lcom/peel/ui/eh;->c:Lcom/peel/ui/ef;

    iget-object v0, v0, Lcom/peel/ui/ef;->a:Lcom/peel/ui/du;

    iget v1, p0, Lcom/peel/ui/eh;->a:I

    invoke-static {v0, v1}, Lcom/peel/ui/du;->b(Lcom/peel/ui/du;I)I

    .line 374
    iget-object v0, p0, Lcom/peel/ui/eh;->c:Lcom/peel/ui/ef;

    iget-object v0, v0, Lcom/peel/ui/ef;->a:Lcom/peel/ui/du;

    invoke-virtual {v0}, Lcom/peel/ui/du;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/ui/eh;->b:Lcom/peel/data/Channel;

    invoke-virtual {v1}, Lcom/peel/data/Channel;->m()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/peel/util/bx;->e(Landroid/content/Context;Ljava/lang/String;)V

    .line 375
    iget-object v0, p0, Lcom/peel/ui/eh;->b:Lcom/peel/data/Channel;

    invoke-virtual {v0}, Lcom/peel/data/Channel;->a()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/ui/eh;->b:Lcom/peel/data/Channel;

    invoke-virtual {v1}, Lcom/peel/data/Channel;->b()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/peel/ui/eh;->b:Lcom/peel/data/Channel;

    invoke-virtual {v2}, Lcom/peel/data/Channel;->e()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/peel/util/bx;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
