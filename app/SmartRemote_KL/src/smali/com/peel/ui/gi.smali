.class Lcom/peel/ui/gi;
.super Lcom/peel/util/t;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/peel/util/t",
        "<",
        "Ljava/util/List",
        "<",
        "Lcom/peel/content/listing/Listing;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/peel/ui/gh;


# direct methods
.method constructor <init>(Lcom/peel/ui/gh;I)V
    .locals 0

    .prologue
    .line 92
    iput-object p1, p0, Lcom/peel/ui/gi;->a:Lcom/peel/ui/gh;

    invoke-direct {p0, p2}, Lcom/peel/util/t;-><init>(I)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 13

    .prologue
    const/16 v10, 0x8

    const/4 v12, 0x1

    const/4 v5, 0x0

    .line 95
    iget-object v0, p0, Lcom/peel/ui/gi;->a:Lcom/peel/ui/gh;

    iget-object v0, v0, Lcom/peel/ui/gh;->a:Lcom/peel/ui/gg;

    invoke-virtual {v0}, Lcom/peel/ui/gg;->v()Landroid/view/View;

    move-result-object v0

    if-nez v0, :cond_0

    .line 157
    :goto_0
    return-void

    .line 97
    :cond_0
    iget-boolean v0, p0, Lcom/peel/ui/gi;->i:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/peel/ui/gi;->j:Ljava/lang/Object;

    if-nez v0, :cond_2

    .line 98
    :cond_1
    invoke-static {}, Lcom/peel/ui/gg;->S()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/ui/gi;->k:Ljava/lang/String;

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 99
    iget-object v0, p0, Lcom/peel/ui/gi;->a:Lcom/peel/ui/gh;

    iget-object v0, v0, Lcom/peel/ui/gh;->a:Lcom/peel/ui/gg;

    invoke-static {v0}, Lcom/peel/ui/gg;->b(Lcom/peel/ui/gg;)Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0, v10}, Landroid/widget/ListView;->setVisibility(I)V

    .line 100
    iget-object v0, p0, Lcom/peel/ui/gi;->a:Lcom/peel/ui/gh;

    iget-object v0, v0, Lcom/peel/ui/gh;->a:Lcom/peel/ui/gg;

    invoke-static {v0}, Lcom/peel/ui/gg;->c(Lcom/peel/ui/gg;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 101
    iget-object v0, p0, Lcom/peel/ui/gi;->a:Lcom/peel/ui/gh;

    iget-object v0, v0, Lcom/peel/ui/gh;->a:Lcom/peel/ui/gg;

    invoke-static {v0}, Lcom/peel/ui/gg;->c(Lcom/peel/ui/gg;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/ui/gi;->a:Lcom/peel/ui/gh;

    iget-object v1, v1, Lcom/peel/ui/gh;->a:Lcom/peel/ui/gg;

    sget v2, Lcom/peel/ui/ft;->no_schedules_for_channel:I

    new-array v3, v12, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/peel/ui/gi;->a:Lcom/peel/ui/gh;

    iget-object v4, v4, Lcom/peel/ui/gh;->a:Lcom/peel/ui/gg;

    invoke-static {v4}, Lcom/peel/ui/gg;->d(Lcom/peel/ui/gg;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-virtual {v1, v2, v3}, Lcom/peel/ui/gg;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 102
    invoke-static {}, Lcom/peel/ui/gg;->S()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/ui/gi;->a:Lcom/peel/ui/gh;

    iget-object v1, v1, Lcom/peel/ui/gh;->a:Lcom/peel/ui/gg;

    invoke-virtual {v1}, Lcom/peel/ui/gg;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    invoke-static {v0, v1, v5}, Lcom/peel/util/bx;->a(Ljava/lang/String;Landroid/app/Activity;Z)V

    goto :goto_0

    .line 104
    :cond_2
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    .line 105
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 108
    iget-object v0, p0, Lcom/peel/ui/gi;->j:Ljava/lang/Object;

    check-cast v0, Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_3
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/content/listing/Listing;

    .line 109
    instance-of v6, v0, Lcom/peel/content/listing/LiveListing;

    if-eqz v6, :cond_3

    .line 110
    check-cast v0, Lcom/peel/content/listing/LiveListing;

    .line 111
    invoke-virtual {v0}, Lcom/peel/content/listing/LiveListing;->o()J

    move-result-wide v6

    invoke-virtual {v0}, Lcom/peel/content/listing/LiveListing;->j()J

    move-result-wide v8

    add-long/2addr v6, v8

    cmp-long v6, v6, v2

    if-lez v6, :cond_3

    .line 112
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 116
    :cond_4
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_5

    .line 117
    invoke-static {}, Lcom/peel/ui/gg;->S()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/ui/gi;->k:Ljava/lang/String;

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 118
    iget-object v0, p0, Lcom/peel/ui/gi;->a:Lcom/peel/ui/gh;

    iget-object v0, v0, Lcom/peel/ui/gh;->a:Lcom/peel/ui/gg;

    invoke-static {v0}, Lcom/peel/ui/gg;->b(Lcom/peel/ui/gg;)Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0, v10}, Landroid/widget/ListView;->setVisibility(I)V

    .line 119
    iget-object v0, p0, Lcom/peel/ui/gi;->a:Lcom/peel/ui/gh;

    iget-object v0, v0, Lcom/peel/ui/gh;->a:Lcom/peel/ui/gg;

    invoke-static {v0}, Lcom/peel/ui/gg;->c(Lcom/peel/ui/gg;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 120
    iget-object v0, p0, Lcom/peel/ui/gi;->a:Lcom/peel/ui/gh;

    iget-object v0, v0, Lcom/peel/ui/gh;->a:Lcom/peel/ui/gg;

    invoke-static {v0}, Lcom/peel/ui/gg;->c(Lcom/peel/ui/gg;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/ui/gi;->a:Lcom/peel/ui/gh;

    iget-object v1, v1, Lcom/peel/ui/gh;->a:Lcom/peel/ui/gg;

    sget v2, Lcom/peel/ui/ft;->no_schedules_for_channel:I

    new-array v3, v12, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/peel/ui/gi;->a:Lcom/peel/ui/gh;

    iget-object v4, v4, Lcom/peel/ui/gh;->a:Lcom/peel/ui/gg;

    invoke-static {v4}, Lcom/peel/ui/gg;->d(Lcom/peel/ui/gg;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-virtual {v1, v2, v3}, Lcom/peel/ui/gg;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 121
    invoke-static {}, Lcom/peel/ui/gg;->S()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/ui/gi;->a:Lcom/peel/ui/gh;

    iget-object v1, v1, Lcom/peel/ui/gh;->a:Lcom/peel/ui/gg;

    invoke-virtual {v1}, Lcom/peel/ui/gg;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    invoke-static {v0, v1, v5}, Lcom/peel/util/bx;->a(Ljava/lang/String;Landroid/app/Activity;Z)V

    goto/16 :goto_0

    .line 125
    :cond_5
    iget-object v0, p0, Lcom/peel/ui/gi;->a:Lcom/peel/ui/gh;

    iget-object v2, v0, Lcom/peel/ui/gh;->a:Lcom/peel/ui/gg;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Lcom/peel/content/listing/Listing;

    invoke-interface {v1, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/peel/content/listing/Listing;

    invoke-static {v0}, Lcom/peel/content/library/LiveLibrary;->a([Lcom/peel/content/listing/Listing;)Ljava/util/Map;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/peel/ui/gg;->a(Lcom/peel/ui/gg;Ljava/util/Map;)Ljava/util/Map;

    .line 126
    invoke-static {}, Lcom/peel/util/x;->a()[Ljava/lang/String;

    move-result-object v6

    .line 127
    iget-object v0, p0, Lcom/peel/ui/gi;->a:Lcom/peel/ui/gh;

    iget-object v0, v0, Lcom/peel/ui/gh;->a:Lcom/peel/ui/gg;

    invoke-static {v0}, Lcom/peel/ui/gg;->b(Lcom/peel/ui/gg;)Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/widget/ListView;->setVisibility(I)V

    .line 128
    iget-object v0, p0, Lcom/peel/ui/gi;->a:Lcom/peel/ui/gh;

    iget-object v0, v0, Lcom/peel/ui/gh;->a:Lcom/peel/ui/gg;

    invoke-static {v0}, Lcom/peel/ui/gg;->c(Lcom/peel/ui/gg;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v10}, Landroid/widget/TextView;->setVisibility(I)V

    .line 129
    iget-object v0, p0, Lcom/peel/ui/gi;->a:Lcom/peel/ui/gh;

    iget-object v0, v0, Lcom/peel/ui/gh;->a:Lcom/peel/ui/gg;

    new-instance v1, Lcom/peel/h/a/fa;

    invoke-direct {v1}, Lcom/peel/h/a/fa;-><init>()V

    invoke-static {v0, v1}, Lcom/peel/ui/gg;->a(Lcom/peel/ui/gg;Lcom/peel/h/a/fa;)Lcom/peel/h/a/fa;

    .line 130
    new-instance v7, Ljava/util/GregorianCalendar;

    invoke-direct {v7}, Ljava/util/GregorianCalendar;-><init>()V

    .line 133
    array-length v8, v6

    move v4, v5

    move v3, v5

    :goto_2
    if-ge v4, v8, :cond_7

    aget-object v1, v6, v4

    .line 134
    iget-object v0, p0, Lcom/peel/ui/gi;->a:Lcom/peel/ui/gh;

    iget-object v0, v0, Lcom/peel/ui/gh;->a:Lcom/peel/ui/gg;

    invoke-static {v0}, Lcom/peel/ui/gg;->e(Lcom/peel/ui/gg;)Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 135
    if-eqz v0, :cond_9

    .line 136
    sget-object v2, Lcom/peel/util/x;->n:[Ljava/lang/String;

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_8

    .line 137
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/peel/ui/gi;->a:Lcom/peel/ui/gh;

    iget-object v2, v2, Lcom/peel/ui/gh;->a:Lcom/peel/ui/gg;

    invoke-virtual {v2}, Lcom/peel/ui/gg;->m()Landroid/support/v4/app/ae;

    move-result-object v2

    invoke-static {v2}, Landroid/text/format/DateFormat;->getMediumDateFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v2

    invoke-virtual {v7}, Ljava/util/GregorianCalendar;->getTime()Ljava/util/Date;

    move-result-object v9

    invoke-virtual {v2, v9}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    move-object v2, v1

    .line 139
    :goto_3
    const/4 v1, 0x5

    invoke-virtual {v7, v1, v12}, Ljava/util/GregorianCalendar;->add(II)V

    .line 140
    iget-object v1, p0, Lcom/peel/ui/gi;->a:Lcom/peel/ui/gh;

    iget-object v1, v1, Lcom/peel/ui/gh;->a:Lcom/peel/ui/gg;

    invoke-static {v1}, Lcom/peel/ui/gg;->f(Lcom/peel/ui/gg;)Landroid/view/LayoutInflater;

    move-result-object v1

    sget v9, Lcom/peel/ui/fq;->sticky_card_header_view:I

    const/4 v10, 0x0

    invoke-virtual {v1, v9, v10, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v9

    .line 141
    sget v1, Lcom/peel/ui/fp;->header:I

    invoke-virtual {v9, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 142
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 143
    iget-object v1, p0, Lcom/peel/ui/gi;->a:Lcom/peel/ui/gh;

    iget-object v1, v1, Lcom/peel/ui/gh;->a:Lcom/peel/ui/gg;

    invoke-static {v1}, Lcom/peel/ui/gg;->g(Lcom/peel/ui/gg;)Lcom/peel/h/a/fa;

    move-result-object v1

    invoke-virtual {v1, v9}, Lcom/peel/h/a/fa;->a(Landroid/view/View;)V

    .line 144
    iget-object v1, p0, Lcom/peel/ui/gi;->a:Lcom/peel/ui/gh;

    iget-object v1, v1, Lcom/peel/ui/gh;->a:Lcom/peel/ui/gg;

    invoke-static {v1}, Lcom/peel/ui/gg;->g(Lcom/peel/ui/gg;)Lcom/peel/h/a/fa;

    move-result-object v1

    new-instance v9, Lcom/peel/ui/ae;

    iget-object v10, p0, Lcom/peel/ui/gi;->a:Lcom/peel/ui/gh;

    iget-object v10, v10, Lcom/peel/ui/gh;->a:Lcom/peel/ui/gg;

    invoke-virtual {v10}, Lcom/peel/ui/gg;->m()Landroid/support/v4/app/ae;

    move-result-object v10

    sget v11, Lcom/peel/ui/fq;->program_search_row:I

    invoke-direct {v9, v10, v11, v0, v2}, Lcom/peel/ui/ae;-><init>(Landroid/content/Context;ILjava/util/List;Ljava/lang/String;)V

    invoke-virtual {v1, v9}, Lcom/peel/h/a/fa;->a(Landroid/widget/ListAdapter;)V

    .line 146
    add-int/lit8 v0, v3, 0x1

    array-length v1, v6

    add-int/lit8 v1, v1, -0x1

    if-ge v3, v1, :cond_6

    .line 147
    new-instance v1, Landroid/view/View;

    iget-object v2, p0, Lcom/peel/ui/gi;->a:Lcom/peel/ui/gh;

    iget-object v2, v2, Lcom/peel/ui/gh;->a:Lcom/peel/ui/gg;

    invoke-virtual {v2}, Lcom/peel/ui/gg;->m()Landroid/support/v4/app/ae;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 148
    const-string/jumbo v2, "#043243"

    invoke-static {v2}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setBackgroundColor(I)V

    .line 149
    iget-object v2, p0, Lcom/peel/ui/gi;->a:Lcom/peel/ui/gh;

    iget-object v2, v2, Lcom/peel/ui/gh;->a:Lcom/peel/ui/gg;

    invoke-static {v2}, Lcom/peel/ui/gg;->g(Lcom/peel/ui/gg;)Lcom/peel/h/a/fa;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/peel/h/a/fa;->a(Landroid/view/View;)V

    .line 133
    :cond_6
    :goto_4
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    move v3, v0

    goto/16 :goto_2

    .line 154
    :cond_7
    iget-object v0, p0, Lcom/peel/ui/gi;->a:Lcom/peel/ui/gh;

    iget-object v0, v0, Lcom/peel/ui/gh;->a:Lcom/peel/ui/gg;

    invoke-static {v0}, Lcom/peel/ui/gg;->b(Lcom/peel/ui/gg;)Landroid/widget/ListView;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/ui/gi;->a:Lcom/peel/ui/gh;

    iget-object v1, v1, Lcom/peel/ui/gh;->a:Lcom/peel/ui/gg;

    invoke-static {v1}, Lcom/peel/ui/gg;->g(Lcom/peel/ui/gg;)Lcom/peel/h/a/fa;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 155
    invoke-static {}, Lcom/peel/ui/gg;->S()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/ui/gi;->a:Lcom/peel/ui/gh;

    iget-object v1, v1, Lcom/peel/ui/gh;->a:Lcom/peel/ui/gg;

    invoke-virtual {v1}, Lcom/peel/ui/gg;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    invoke-static {v0, v1, v5}, Lcom/peel/util/bx;->a(Ljava/lang/String;Landroid/app/Activity;Z)V

    goto/16 :goto_0

    :cond_8
    move-object v2, v1

    goto/16 :goto_3

    :cond_9
    move v0, v3

    goto :goto_4
.end method
