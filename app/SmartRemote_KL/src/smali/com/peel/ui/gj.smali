.class Lcom/peel/ui/gj;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field final synthetic a:Lcom/peel/ui/gg;


# direct methods
.method constructor <init>(Lcom/peel/ui/gg;)V
    .locals 0

    .prologue
    .line 162
    iput-object p1, p0, Lcom/peel/ui/gj;->a:Lcom/peel/ui/gg;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    const/16 v8, 0x7dd

    const/4 v1, 0x1

    const/4 v7, 0x0

    .line 165
    const v0, 0x54dff70

    invoke-virtual {p2, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string/jumbo v2, "\\|"

    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    .line 167
    invoke-static {}, Lcom/peel/ui/gg;->S()Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "parts[0]: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    aget-object v3, v5, v7

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " -- parts[1]: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    aget-object v3, v5, v1

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 169
    iget-object v0, p0, Lcom/peel/ui/gj;->a:Lcom/peel/ui/gg;

    invoke-static {v0}, Lcom/peel/ui/gg;->e(Lcom/peel/ui/gg;)Ljava/util/Map;

    move-result-object v0

    aget-object v2, v5, v7

    const-string/jumbo v3, "\\("

    invoke-virtual {v2, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    aget-object v2, v2, v7

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 171
    const-string/jumbo v2, "live"

    invoke-static {v2}, Lcom/peel/content/a;->c(Ljava/lang/String;)Lcom/peel/content/library/Library;

    move-result-object v2

    invoke-virtual {v2}, Lcom/peel/content/library/Library;->e()Ljava/lang/String;

    move-result-object v2

    .line 172
    iget-object v3, p0, Lcom/peel/ui/gj;->a:Lcom/peel/ui/gg;

    invoke-static {v3}, Lcom/peel/ui/gg;->a(Lcom/peel/ui/gg;)Landroid/os/Bundle;

    move-result-object v3

    const-string/jumbo v4, "libraryIds"

    new-array v6, v1, [Ljava/lang/String;

    aput-object v2, v6, v7

    invoke-virtual {v3, v4, v6}, Landroid/os/Bundle;->putStringArray(Ljava/lang/String;[Ljava/lang/String;)V

    .line 173
    iget-object v3, p0, Lcom/peel/ui/gj;->a:Lcom/peel/ui/gg;

    invoke-static {v3}, Lcom/peel/ui/gg;->a(Lcom/peel/ui/gg;)Landroid/os/Bundle;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "listings/"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-virtual {v3, v2, v4}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 174
    iget-object v0, p0, Lcom/peel/ui/gj;->a:Lcom/peel/ui/gg;

    invoke-static {v0}, Lcom/peel/ui/gg;->a(Lcom/peel/ui/gg;)Landroid/os/Bundle;

    move-result-object v0

    const-string/jumbo v2, "selected"

    aget-object v3, v5, v1

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 175
    iget-object v0, p0, Lcom/peel/ui/gj;->a:Lcom/peel/ui/gg;

    invoke-static {v0}, Lcom/peel/ui/gg;->a(Lcom/peel/ui/gg;)Landroid/os/Bundle;

    move-result-object v0

    const-string/jumbo v2, "needsgrouping"

    invoke-virtual {v0, v2, v7}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 176
    iget-object v0, p0, Lcom/peel/ui/gj;->a:Lcom/peel/ui/gg;

    invoke-static {v0}, Lcom/peel/ui/gg;->a(Lcom/peel/ui/gg;)Landroid/os/Bundle;

    move-result-object v0

    const-string/jumbo v2, "fromschedules"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 177
    iget-object v0, p0, Lcom/peel/ui/gj;->a:Lcom/peel/ui/gg;

    invoke-static {v0}, Lcom/peel/ui/gg;->a(Lcom/peel/ui/gg;)Landroid/os/Bundle;

    move-result-object v0

    const-string/jumbo v2, "context_id"

    invoke-virtual {v0, v2, v8}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 183
    :try_start_0
    invoke-static {}, Lcom/peel/ui/gg;->S()Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, " **** report tmsid: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/4 v3, 0x1

    aget-object v3, v5, v3

    invoke-static {v3}, Lcom/peel/util/bx;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 184
    invoke-static {}, Lcom/peel/util/a/f;->a()Lcom/peel/util/a/f;

    move-result-object v0

    sget-object v2, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v2}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v2

    if-nez v2, :cond_0

    :goto_0
    const/16 v2, 0x3ea

    const/16 v3, 0x7dd

    const-string/jumbo v4, "favorites"

    const/4 v6, 0x1

    aget-object v5, v5, v6

    .line 186
    invoke-static {v5}, Lcom/peel/util/bx;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    const-string/jumbo v8, ""

    const/4 v9, 0x0

    move v5, p3

    .line 184
    invoke-virtual/range {v0 .. v9}, Lcom/peel/util/a/f;->a(IIILjava/lang/String;ILjava/lang/String;ILjava/lang/String;I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 191
    :goto_1
    iget-object v0, p0, Lcom/peel/ui/gj;->a:Lcom/peel/ui/gg;

    invoke-virtual {v0}, Lcom/peel/ui/gg;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    const-class v1, Lcom/peel/ui/b/ag;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/peel/ui/gj;->a:Lcom/peel/ui/gg;

    invoke-static {v2}, Lcom/peel/ui/gg;->a(Lcom/peel/ui/gg;)Landroid/os/Bundle;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/peel/d/e;->c(Landroid/support/v4/app/ae;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 192
    return-void

    .line 184
    :cond_0
    :try_start_1
    sget-object v1, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v1}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/data/at;->f()I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    move-result v1

    goto :goto_0

    .line 187
    :catch_0
    move-exception v0

    .line 188
    invoke-static {}, Lcom/peel/ui/gg;->S()Ljava/lang/String;

    move-result-object v1

    invoke-static {}, Lcom/peel/ui/gg;->S()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method
