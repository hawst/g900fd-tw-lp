.class Lcom/peel/ui/gp;
.super Lcom/peel/util/t;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/peel/util/t",
        "<[",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/peel/ui/go;


# direct methods
.method constructor <init>(Lcom/peel/ui/go;)V
    .locals 0

    .prologue
    .line 136
    iput-object p1, p0, Lcom/peel/ui/gp;->a:Lcom/peel/ui/go;

    invoke-direct {p0}, Lcom/peel/util/t;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 140
    :try_start_0
    iget-object v0, p0, Lcom/peel/ui/gp;->a:Lcom/peel/ui/go;

    iget-object v0, v0, Lcom/peel/ui/go;->b:Lcom/peel/ui/gm;

    invoke-virtual {v0}, Lcom/peel/ui/gm;->Z()V

    .line 141
    invoke-static {}, Lcom/peel/ui/gm;->c()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, " results from search/program "

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 142
    iget-object v0, p0, Lcom/peel/ui/gp;->a:Lcom/peel/ui/go;

    iget-object v0, v0, Lcom/peel/ui/go;->b:Lcom/peel/ui/gm;

    invoke-virtual {v0}, Lcom/peel/ui/gm;->s()Z

    move-result v0

    if-nez v0, :cond_0

    .line 143
    iget-object v0, p0, Lcom/peel/ui/gp;->a:Lcom/peel/ui/go;

    iget-object v0, v0, Lcom/peel/ui/go;->b:Lcom/peel/ui/gm;

    invoke-static {v0}, Lcom/peel/ui/gm;->c(Lcom/peel/ui/gm;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 200
    iget-object v0, p0, Lcom/peel/ui/gp;->a:Lcom/peel/ui/go;

    iget-object v0, v0, Lcom/peel/ui/go;->b:Lcom/peel/ui/gm;

    invoke-static {v0, v3}, Lcom/peel/ui/gm;->a(Lcom/peel/ui/gm;Ljava/util/concurrent/Future;)Ljava/util/concurrent/Future;

    .line 202
    :goto_0
    return-void

    .line 147
    :cond_0
    :try_start_1
    iget-boolean v0, p0, Lcom/peel/ui/gp;->i:Z

    if-eqz v0, :cond_2

    .line 148
    iget-object v0, p0, Lcom/peel/ui/gp;->j:Ljava/lang/Object;

    check-cast v0, [Ljava/lang/String;

    .line 150
    iget-object v1, p0, Lcom/peel/ui/gp;->a:Lcom/peel/ui/go;

    iget-object v1, v1, Lcom/peel/ui/go;->b:Lcom/peel/ui/gm;

    invoke-static {v1}, Lcom/peel/ui/gm;->d(Lcom/peel/ui/gm;)Lcom/peel/content/library/LiveLibrary;

    move-result-object v1

    if-nez v1, :cond_1

    .line 151
    iget-object v0, p0, Lcom/peel/ui/gp;->a:Lcom/peel/ui/go;

    iget-object v0, v0, Lcom/peel/ui/go;->b:Lcom/peel/ui/gm;

    invoke-static {v0}, Lcom/peel/ui/gm;->c(Lcom/peel/ui/gm;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 200
    iget-object v0, p0, Lcom/peel/ui/gp;->a:Lcom/peel/ui/go;

    iget-object v0, v0, Lcom/peel/ui/go;->b:Lcom/peel/ui/gm;

    invoke-static {v0, v3}, Lcom/peel/ui/gm;->a(Lcom/peel/ui/gm;Ljava/util/concurrent/Future;)Ljava/util/concurrent/Future;

    goto :goto_0

    .line 155
    :cond_1
    :try_start_2
    iget-object v1, p0, Lcom/peel/ui/gp;->a:Lcom/peel/ui/go;

    iget-object v1, v1, Lcom/peel/ui/go;->b:Lcom/peel/ui/gm;

    invoke-static {v1}, Lcom/peel/ui/gm;->d(Lcom/peel/ui/gm;)Lcom/peel/content/library/LiveLibrary;

    move-result-object v1

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    new-instance v2, Lcom/peel/ui/gq;

    invoke-direct {v2, p0}, Lcom/peel/ui/gq;-><init>(Lcom/peel/ui/gp;)V

    invoke-virtual {v1, v0, v2}, Lcom/peel/content/library/LiveLibrary;->a(Ljava/util/List;Lcom/peel/util/t;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 200
    :goto_1
    iget-object v0, p0, Lcom/peel/ui/gp;->a:Lcom/peel/ui/go;

    iget-object v0, v0, Lcom/peel/ui/go;->b:Lcom/peel/ui/gm;

    invoke-static {v0, v3}, Lcom/peel/ui/gm;->a(Lcom/peel/ui/gm;Ljava/util/concurrent/Future;)Ljava/util/concurrent/Future;

    goto :goto_0

    .line 186
    :cond_2
    :try_start_3
    invoke-static {}, Lcom/peel/ui/gm;->c()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "show empty search result"

    new-instance v2, Lcom/peel/ui/gs;

    invoke-direct {v2, p0}, Lcom/peel/ui/gs;-><init>(Lcom/peel/ui/gp;)V

    invoke-static {v0, v1, v2}, Lcom/peel/util/i;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/lang/Runnable;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    .line 200
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/peel/ui/gp;->a:Lcom/peel/ui/go;

    iget-object v1, v1, Lcom/peel/ui/go;->b:Lcom/peel/ui/gm;

    invoke-static {v1, v3}, Lcom/peel/ui/gm;->a(Lcom/peel/ui/gm;Ljava/util/concurrent/Future;)Ljava/util/concurrent/Future;

    throw v0
.end method
