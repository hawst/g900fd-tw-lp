.class Lcom/peel/ui/nm;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/peel/ui/kg;


# instance fields
.field final synthetic a:Lcom/peel/ui/nl;


# direct methods
.method constructor <init>(Lcom/peel/ui/nl;)V
    .locals 0

    .prologue
    .line 59
    iput-object p1, p0, Lcom/peel/ui/nm;->a:Lcom/peel/ui/nl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/jess/ui/v;Landroid/view/View;IJLjava/lang/String;)V
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/jess/ui/v",
            "<*>;",
            "Landroid/view/View;",
            "IJ",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 62
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/peel/ui/nm;->a:Lcom/peel/ui/nl;

    invoke-static {v2}, Lcom/peel/ui/nl;->a(Lcom/peel/ui/nl;)Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_1

    .line 241
    :cond_0
    :goto_0
    return-void

    .line 64
    :cond_1
    invoke-virtual/range {p1 .. p1}, Lcom/jess/ui/v;->getAdapter()Landroid/widget/Adapter;

    move-result-object v2

    check-cast v2, Lcom/peel/ui/ka;

    move/from16 v0, p3

    invoke-virtual {v2, v0}, Lcom/peel/ui/ka;->getItem(I)Ljava/lang/Object;

    move-result-object v2

    move-object v12, v2

    check-cast v12, [Lcom/peel/content/listing/Listing;

    move-object/from16 v13, p2

    .line 66
    check-cast v13, Landroid/widget/FrameLayout;

    .line 67
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/peel/ui/nm;->a:Lcom/peel/ui/nl;

    invoke-static {v2}, Lcom/peel/ui/nl;->b(Lcom/peel/ui/nl;)Landroid/view/View;

    move-result-object v2

    if-eqz v2, :cond_2

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/peel/ui/nm;->a:Lcom/peel/ui/nl;

    invoke-static {v2}, Lcom/peel/ui/nl;->b(Lcom/peel/ui/nl;)Landroid/view/View;

    move-result-object v2

    const-string/jumbo v3, "overlay"

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewWithTag(Ljava/lang/Object;)Landroid/view/View;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 68
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/peel/ui/nm;->a:Lcom/peel/ui/nl;

    invoke-static {v2}, Lcom/peel/ui/nl;->b(Lcom/peel/ui/nl;)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/FrameLayout;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/peel/ui/nm;->a:Lcom/peel/ui/nl;

    invoke-static {v3}, Lcom/peel/ui/nl;->b(Lcom/peel/ui/nl;)Landroid/view/View;

    move-result-object v3

    const-string/jumbo v4, "overlay"

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewWithTag(Ljava/lang/Object;)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/FrameLayout;->removeView(Landroid/view/View;)V

    .line 70
    :cond_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/peel/ui/nm;->a:Lcom/peel/ui/nl;

    move-object/from16 v0, p2

    invoke-static {v2, v0}, Lcom/peel/ui/nl;->a(Lcom/peel/ui/nl;Landroid/view/View;)Landroid/view/View;

    .line 72
    const-string/jumbo v2, "overlay"

    invoke-virtual {v13, v2}, Landroid/widget/FrameLayout;->findViewWithTag(Ljava/lang/Object;)Landroid/view/View;

    move-result-object v2

    if-nez v2, :cond_0

    .line 75
    if-eqz v12, :cond_0

    .line 77
    const/4 v2, 0x0

    aget-object v14, v12, v2

    .line 79
    instance-of v2, v14, Lcom/peel/content/listing/LiveListing;

    if-eqz v2, :cond_7

    move-object v2, v14

    check-cast v2, Lcom/peel/content/listing/LiveListing;

    invoke-virtual {v2}, Lcom/peel/content/listing/LiveListing;->o()J

    move-result-wide v2

    .line 80
    :goto_1
    invoke-virtual {v14}, Lcom/peel/content/listing/Listing;->j()J

    move-result-wide v4

    .line 81
    add-long/2addr v4, v2

    .line 82
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v6

    .line 84
    const/4 v15, -0x1

    .line 85
    cmp-long v8, v6, v2

    if-lez v8, :cond_8

    cmp-long v4, v6, v4

    if-gez v4, :cond_8

    .line 86
    sget v2, Lcom/peel/ui/fq;->tunein_overlay:I

    move v3, v2

    .line 110
    :goto_2
    const/4 v2, -0x1

    if-le v3, v2, :cond_0

    .line 111
    sget-object v2, Lcom/peel/util/bx;->d:Ljava/util/Map;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v2, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    move-object v10, v2

    check-cast v10, Landroid/view/View;

    .line 117
    sget v2, Lcom/peel/ui/fq;->tunein_overlay:I

    if-ne v3, v2, :cond_b

    const/4 v6, 0x1

    .line 118
    :goto_3
    const/4 v8, 0x0

    .line 119
    const/4 v9, 0x0

    .line 121
    sget-object v2, Lcom/peel/content/a;->e:Ljava/lang/String;

    const-string/jumbo v3, "CN"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_c

    .line 122
    const/4 v2, 0x0

    aget-object v2, v12, v2

    invoke-static {v2}, Lcom/peel/util/bx;->d(Lcom/peel/content/listing/Listing;)Ljava/lang/String;

    move-result-object v9

    .line 124
    if-eqz v9, :cond_3

    const/4 v8, 0x1

    .line 129
    :cond_3
    :goto_4
    if-nez v6, :cond_d

    if-eqz v8, :cond_d

    .line 130
    sget v2, Lcom/peel/ui/fp;->btn:I

    invoke-virtual {v10, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    .line 132
    if-eqz v2, :cond_4

    .line 133
    sget v3, Lcom/peel/ui/fo;->editreminder_stateful:I

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 136
    :cond_4
    sget v2, Lcom/peel/ui/fp;->remind_me_txt:I

    invoke-virtual {v10, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    sget v3, Lcom/peel/ui/ft;->undo_reminder:I

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    .line 150
    :cond_5
    :goto_5
    sget v2, Lcom/peel/ui/fp;->btn:I

    invoke-virtual {v10, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v11

    new-instance v2, Lcom/peel/ui/no;

    move-object/from16 v3, p0

    move/from16 v4, p3

    move-object v5, v14

    move-object v7, v12

    invoke-direct/range {v2 .. v9}, Lcom/peel/ui/no;-><init>(Lcom/peel/ui/nm;ILcom/peel/content/listing/Listing;Z[Lcom/peel/content/listing/Listing;ZLjava/lang/String;)V

    invoke-virtual {v11, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 207
    sget v2, Lcom/peel/ui/fp;->more_btn:I

    invoke-virtual {v10, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    new-instance v2, Lcom/peel/ui/np;

    move-object/from16 v3, p0

    move-object v4, v14

    move-object v5, v12

    move-object/from16 v6, p6

    move/from16 v7, p3

    invoke-direct/range {v2 .. v7}, Lcom/peel/ui/np;-><init>(Lcom/peel/ui/nm;Lcom/peel/content/listing/Listing;[Lcom/peel/content/listing/Listing;Ljava/lang/String;I)V

    invoke-virtual {v8, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 233
    invoke-virtual {v10}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    if-eqz v2, :cond_6

    .line 234
    invoke-virtual {v10}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    .line 235
    check-cast v2, Landroid/widget/FrameLayout;

    invoke-virtual {v2, v10}, Landroid/widget/FrameLayout;->removeView(Landroid/view/View;)V

    .line 237
    :cond_6
    invoke-virtual {v13, v10}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    goto/16 :goto_0

    .line 79
    :cond_7
    const-wide/16 v2, 0x0

    goto/16 :goto_1

    .line 87
    :cond_8
    const-wide/32 v4, 0x1b7740

    add-long/2addr v4, v6

    cmp-long v2, v2, v4

    if-lez v2, :cond_9

    array-length v2, v12

    const/4 v3, 0x1

    if-lt v2, v3, :cond_9

    .line 88
    sget v2, Lcom/peel/ui/fq;->setreminder_overlay:I

    move v3, v2

    goto/16 :goto_2

    .line 90
    :cond_9
    invoke-virtual/range {p1 .. p1}, Lcom/jess/ui/v;->getAdapter()Landroid/widget/Adapter;

    move-result-object v2

    check-cast v2, Lcom/peel/ui/ka;

    .line 93
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 95
    const-string/jumbo v3, "libraryIds"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/peel/ui/nm;->a:Lcom/peel/ui/nl;

    invoke-static {v6}, Lcom/peel/ui/nl;->a(Lcom/peel/ui/nl;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putStringArray(Ljava/lang/String;[Ljava/lang/String;)V

    .line 96
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "listings/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/peel/ui/nm;->a:Lcom/peel/ui/nl;

    invoke-static {v4}, Lcom/peel/ui/nl;->a(Lcom/peel/ui/nl;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Lcom/peel/ui/nn;

    move-object/from16 v0, p0

    invoke-direct {v4, v0, v14}, Lcom/peel/ui/nn;-><init>(Lcom/peel/ui/nm;Lcom/peel/content/listing/Listing;)V

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 97
    const-string/jumbo v3, "selected"

    const/4 v4, 0x0

    aget-object v4, v12, v4

    invoke-virtual {v4}, Lcom/peel/content/listing/Listing;->f()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 98
    const-string/jumbo v3, "context_id"

    const/16 v4, 0x7dd

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 99
    const-string/jumbo v3, "category"

    move-object/from16 v0, p6

    invoke-virtual {v2, v3, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 100
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/peel/ui/nm;->a:Lcom/peel/ui/nl;

    invoke-virtual {v3}, Lcom/peel/ui/nl;->m()Landroid/support/v4/app/ae;

    move-result-object v3

    const-class v4, Lcom/peel/ui/b/ag;

    invoke-virtual {v4}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v2}, Lcom/peel/d/e;->b(Landroid/support/v4/app/ae;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 105
    invoke-static {}, Lcom/peel/util/a/f;->a()Lcom/peel/util/a/f;

    move-result-object v2

    sget-object v3, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v3}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v3

    if-nez v3, :cond_a

    const/4 v3, 0x1

    :goto_6
    const/16 v4, 0x4b3

    const/16 v5, 0x7dd

    const-string/jumbo v6, "favorites"

    .line 107
    invoke-static {v14}, Lcom/peel/util/bx;->b(Lcom/peel/content/listing/Listing;)Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x0

    invoke-virtual {v14}, Lcom/peel/content/listing/Listing;->h()Ljava/lang/String;

    move-result-object v10

    const/4 v11, 0x0

    move/from16 v7, p3

    .line 105
    invoke-virtual/range {v2 .. v11}, Lcom/peel/util/a/f;->a(IIILjava/lang/String;ILjava/lang/String;ILjava/lang/String;I)V

    move v3, v15

    goto/16 :goto_2

    :cond_a
    sget-object v3, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v3}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v3

    invoke-virtual {v3}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v3

    invoke-virtual {v3}, Lcom/peel/data/at;->f()I

    move-result v3

    goto :goto_6

    .line 117
    :cond_b
    const/4 v6, 0x0

    goto/16 :goto_3

    .line 126
    :cond_c
    const/4 v2, 0x0

    aget-object v2, v12, v2

    invoke-static {v2}, Lcom/peel/util/bx;->e(Lcom/peel/content/listing/Listing;)Z

    move-result v8

    goto/16 :goto_4

    .line 137
    :cond_d
    if-nez v6, :cond_5

    .line 138
    sget v2, Lcom/peel/ui/fp;->btn:I

    invoke-virtual {v10, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    .line 140
    if-eqz v2, :cond_e

    .line 141
    sget v3, Lcom/peel/ui/fo;->setreminder_stateful:I

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 144
    :cond_e
    sget v2, Lcom/peel/ui/fp;->remind_me_txt:I

    invoke-virtual {v10, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    sget v3, Lcom/peel/ui/ft;->label_remind_me_all_caps:I

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    goto/16 :goto_5
.end method
