.class Lcom/peel/ui/bl;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/peel/ui/bh;


# direct methods
.method constructor <init>(Lcom/peel/ui/bh;)V
    .locals 0

    .prologue
    .line 224
    iput-object p1, p0, Lcom/peel/ui/bl;->a:Lcom/peel/ui/bh;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 10

    .prologue
    const/16 v9, 0x8

    const/4 v6, 0x1

    .line 227
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 228
    sget v0, Lcom/peel/ui/fp;->menu_done:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 230
    iget-object v7, p0, Lcom/peel/ui/bl;->a:Lcom/peel/ui/bh;

    new-instance v0, Lcom/peel/d/a;

    sget-object v1, Lcom/peel/d/d;->b:Lcom/peel/d/d;

    sget-object v2, Lcom/peel/d/b;->a:Lcom/peel/d/b;

    sget-object v3, Lcom/peel/d/c;->b:Lcom/peel/d/c;

    iget-object v4, p0, Lcom/peel/ui/bl;->a:Lcom/peel/ui/bh;

    sget v8, Lcom/peel/ui/ft;->favorites:I

    .line 231
    invoke-virtual {v4, v8}, Lcom/peel/ui/bh;->a(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct/range {v0 .. v5}, Lcom/peel/d/a;-><init>(Lcom/peel/d/d;Lcom/peel/d/b;Lcom/peel/d/c;Ljava/lang/String;Ljava/util/List;)V

    iput-object v0, v7, Lcom/peel/ui/bh;->d:Lcom/peel/d/a;

    .line 233
    iget-object v0, p0, Lcom/peel/ui/bl;->a:Lcom/peel/ui/bh;

    invoke-virtual {v0}, Lcom/peel/ui/bh;->Z()V

    .line 234
    iget-object v0, p0, Lcom/peel/ui/bl;->a:Lcom/peel/ui/bh;

    invoke-static {v0}, Lcom/peel/ui/bh;->a(Lcom/peel/ui/bh;)Lcom/peel/ui/az;

    move-result-object v0

    invoke-virtual {v0, v6}, Lcom/peel/ui/az;->a(I)V

    .line 235
    iget-object v0, p0, Lcom/peel/ui/bl;->a:Lcom/peel/ui/bh;

    invoke-static {v0}, Lcom/peel/ui/bh;->d(Lcom/peel/ui/bh;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v9}, Landroid/widget/Button;->setVisibility(I)V

    .line 236
    iget-object v0, p0, Lcom/peel/ui/bl;->a:Lcom/peel/ui/bh;

    invoke-static {v0}, Lcom/peel/ui/bh;->e(Lcom/peel/ui/bh;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v9}, Landroid/widget/Button;->setVisibility(I)V

    .line 237
    sput-boolean v6, Lcom/peel/ui/bh;->f:Z

    .line 238
    iget-object v0, p0, Lcom/peel/ui/bl;->a:Lcom/peel/ui/bh;

    invoke-static {v0}, Lcom/peel/ui/bh;->a(Lcom/peel/ui/bh;)Lcom/peel/ui/az;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/ui/az;->notifyDataSetChanged()V

    .line 239
    sput-boolean v6, Lcom/peel/ui/bh;->f:Z

    .line 240
    invoke-static {}, Lcom/peel/util/a/f;->a()Lcom/peel/util/a/f;

    move-result-object v1

    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v0}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v0

    if-nez v0, :cond_0

    move v0, v6

    :goto_0
    const/16 v2, 0x418

    const/16 v3, 0x7dd

    invoke-virtual {v1, v0, v2, v3}, Lcom/peel/util/a/f;->a(III)V

    .line 243
    return-void

    .line 240
    :cond_0
    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v0}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/data/at;->f()I

    move-result v0

    goto :goto_0
.end method
