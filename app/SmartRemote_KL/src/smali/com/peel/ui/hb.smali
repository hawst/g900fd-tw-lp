.class Lcom/peel/ui/hb;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/peel/widget/am;


# instance fields
.field final synthetic a:Lcom/peel/ui/gt;


# direct methods
.method constructor <init>(Lcom/peel/ui/gt;)V
    .locals 0

    .prologue
    .line 587
    iput-object p1, p0, Lcom/peel/ui/hb;->a:Lcom/peel/ui/gt;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 8

    .prologue
    .line 590
    invoke-static {}, Lcom/peel/util/a/f;->a()Lcom/peel/util/a/f;

    move-result-object v0

    sget-object v1, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v1}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v1

    if-nez v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    const/16 v2, 0x532

    const/16 v3, 0x7e7

    iget-object v4, p0, Lcom/peel/ui/hb;->a:Lcom/peel/ui/gt;

    .line 591
    invoke-static {v4}, Lcom/peel/ui/gt;->q(Lcom/peel/ui/gt;)Z

    move-result v4

    if-eqz v4, :cond_1

    const-string/jumbo v4, "AUTO"

    :goto_1
    const/4 v5, 0x0

    .line 590
    invoke-virtual/range {v0 .. v5}, Lcom/peel/util/a/f;->a(IIILjava/lang/String;I)V

    .line 592
    iget-object v0, p0, Lcom/peel/ui/hb;->a:Lcom/peel/ui/gt;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/peel/ui/gt;->b(Lcom/peel/ui/gt;Z)Z

    .line 594
    new-instance v7, Landroid/view/animation/AlphaAnimation;

    const/4 v0, 0x0

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-direct {v7, v0, v1}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 595
    const-wide/16 v0, 0x1f4

    invoke-virtual {v7, v0, v1}, Landroid/view/animation/AlphaAnimation;->setDuration(J)V

    .line 596
    const/4 v0, 0x1

    invoke-virtual {v7, v0}, Landroid/view/animation/AlphaAnimation;->setFillAfter(Z)V

    .line 598
    new-instance v0, Landroid/view/animation/RotateAnimation;

    const/4 v1, 0x0

    const/high16 v2, 0x43340000    # 180.0f

    const/4 v3, 0x1

    const/high16 v4, 0x3f000000    # 0.5f

    const/4 v5, 0x1

    const/high16 v6, 0x3f000000    # 0.5f

    invoke-direct/range {v0 .. v6}, Landroid/view/animation/RotateAnimation;-><init>(FFIFIF)V

    .line 599
    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/RotateAnimation;->setDuration(J)V

    .line 600
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/animation/RotateAnimation;->setFillAfter(Z)V

    .line 601
    iget-object v1, p0, Lcom/peel/ui/hb;->a:Lcom/peel/ui/gt;

    invoke-static {v1}, Lcom/peel/ui/gt;->r(Lcom/peel/ui/gt;)Landroid/widget/ImageView;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 604
    iget-object v0, p0, Lcom/peel/ui/hb;->a:Lcom/peel/ui/gt;

    invoke-static {v0}, Lcom/peel/ui/gt;->s(Lcom/peel/ui/gt;)Landroid/widget/FrameLayout;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 605
    new-instance v0, Lcom/peel/ui/hc;

    invoke-direct {v0, p0}, Lcom/peel/ui/hc;-><init>(Lcom/peel/ui/hb;)V

    invoke-virtual {v7, v0}, Landroid/view/animation/AlphaAnimation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 627
    iget-object v0, p0, Lcom/peel/ui/hb;->a:Lcom/peel/ui/gt;

    invoke-static {v0}, Lcom/peel/ui/gt;->s(Lcom/peel/ui/gt;)Landroid/widget/FrameLayout;

    move-result-object v0

    invoke-virtual {v0, v7}, Landroid/widget/FrameLayout;->startAnimation(Landroid/view/animation/Animation;)V

    .line 629
    iget-object v0, p0, Lcom/peel/ui/hb;->a:Lcom/peel/ui/gt;

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/peel/ui/gt;->a(Lcom/peel/ui/gt;ZZ)V

    .line 631
    return-void

    .line 590
    :cond_0
    sget-object v1, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v1}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/data/at;->f()I

    move-result v1

    goto :goto_0

    .line 591
    :cond_1
    const-string/jumbo v4, "MANUAL"

    goto :goto_1
.end method
