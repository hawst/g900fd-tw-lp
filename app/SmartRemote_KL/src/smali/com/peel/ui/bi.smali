.class Lcom/peel/ui/bi;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field final synthetic a:Lcom/peel/ui/bh;


# direct methods
.method constructor <init>(Lcom/peel/ui/bh;)V
    .locals 0

    .prologue
    .line 90
    iput-object p1, p0, Lcom/peel/ui/bi;->a:Lcom/peel/ui/bh;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    const/16 v3, 0x7dd

    const/4 v9, 0x1

    const/4 v5, 0x0

    .line 93
    iget-object v0, p0, Lcom/peel/ui/bi;->a:Lcom/peel/ui/bh;

    iget-object v0, v0, Lcom/peel/ui/bh;->b:Lcom/peel/d/i;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/peel/content/a;->h:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/peel/ui/bi;->a:Lcom/peel/ui/bh;

    invoke-static {v0}, Lcom/peel/ui/bh;->a(Lcom/peel/ui/bh;)Lcom/peel/ui/az;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/peel/ui/bi;->a:Lcom/peel/ui/bh;

    invoke-static {v0}, Lcom/peel/ui/bh;->a(Lcom/peel/ui/bh;)Lcom/peel/ui/az;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/ui/az;->a()I

    move-result v0

    if-nez v0, :cond_0

    .line 94
    iget-object v0, p0, Lcom/peel/ui/bi;->a:Lcom/peel/ui/bh;

    invoke-static {v0}, Lcom/peel/ui/bh;->b(Lcom/peel/ui/bh;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lcom/peel/data/Channel;

    .line 95
    if-nez v8, :cond_1

    const/4 v0, 0x0

    .line 96
    :goto_0
    if-nez v0, :cond_2

    .line 112
    :cond_0
    :goto_1
    return-void

    .line 95
    :cond_1
    invoke-virtual {v8}, Lcom/peel/data/Channel;->b()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 99
    :cond_2
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 100
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v0, p0, Lcom/peel/ui/bi;->a:Lcom/peel/ui/bh;

    invoke-static {v0}, Lcom/peel/ui/bh;->b(Lcom/peel/ui/bh;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/data/Channel;

    invoke-virtual {v0}, Lcom/peel/data/Channel;->e()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v2, " - "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v0, p0, Lcom/peel/ui/bi;->a:Lcom/peel/ui/bh;

    invoke-static {v0}, Lcom/peel/ui/bh;->b(Lcom/peel/ui/bh;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/data/Channel;

    invoke-virtual {v0}, Lcom/peel/data/Channel;->d()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 101
    const-string/jumbo v4, "callsign"

    iget-object v0, p0, Lcom/peel/ui/bi;->a:Lcom/peel/ui/bh;

    invoke-static {v0}, Lcom/peel/ui/bh;->b(Lcom/peel/ui/bh;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/data/Channel;

    invoke-virtual {v0}, Lcom/peel/data/Channel;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v4, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 102
    const-string/jumbo v0, "name"

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 104
    iget-object v0, p0, Lcom/peel/ui/bi;->a:Lcom/peel/ui/bh;

    invoke-virtual {v0}, Lcom/peel/ui/bh;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    const-class v2, Lcom/peel/ui/gg;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2, v1}, Lcom/peel/d/e;->b(Landroid/support/v4/app/ae;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 106
    invoke-static {}, Lcom/peel/util/a/f;->a()Lcom/peel/util/a/f;

    move-result-object v0

    sget-object v1, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v1}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v1

    if-nez v1, :cond_3

    move v1, v9

    :goto_2
    const/16 v2, 0x419

    .line 107
    invoke-virtual {v8}, Lcom/peel/data/Channel;->m()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v8}, Lcom/peel/data/Channel;->b()Ljava/lang/String;

    move-result-object v6

    move v7, v5

    .line 106
    invoke-virtual/range {v0 .. v7}, Lcom/peel/util/a/f;->a(IIILjava/lang/String;ILjava/lang/String;I)V

    .line 108
    invoke-static {}, Lcom/peel/util/a/f;->a()Lcom/peel/util/a/f;

    move-result-object v0

    sget-object v1, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v1}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v1

    if-nez v1, :cond_4

    move v1, v9

    :goto_3
    const/16 v2, 0x3f3

    .line 110
    invoke-virtual {v8}, Lcom/peel/data/Channel;->m()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v8}, Lcom/peel/data/Channel;->b()Ljava/lang/String;

    move-result-object v6

    move v7, v5

    .line 108
    invoke-virtual/range {v0 .. v7}, Lcom/peel/util/a/f;->a(IIILjava/lang/String;ILjava/lang/String;I)V

    goto/16 :goto_1

    .line 106
    :cond_3
    sget-object v1, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v1}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/data/at;->f()I

    move-result v1

    goto :goto_2

    .line 108
    :cond_4
    sget-object v1, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v1}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/data/at;->f()I

    move-result v1

    goto :goto_3
.end method
