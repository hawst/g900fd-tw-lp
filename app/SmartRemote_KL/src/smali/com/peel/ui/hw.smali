.class Lcom/peel/ui/hw;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/peel/widget/ac;


# instance fields
.field final synthetic a:Lcom/peel/ui/ht;


# direct methods
.method constructor <init>(Lcom/peel/ui/ht;)V
    .locals 0

    .prologue
    .line 1103
    iput-object p1, p0, Lcom/peel/ui/hw;->a:Lcom/peel/ui/ht;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 1108
    invoke-virtual {p2}, Landroid/view/View;->isPressed()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1109
    invoke-virtual {p2, v5}, Landroid/view/View;->setPressed(Z)V

    .line 1111
    :cond_0
    invoke-virtual {p2, v5}, Landroid/view/View;->playSoundEffect(I)V

    .line 1120
    iget-object v0, p0, Lcom/peel/ui/hw;->a:Lcom/peel/ui/ht;

    iget-object v0, v0, Lcom/peel/ui/ht;->b:Lcom/peel/ui/gt;

    iget-boolean v0, v0, Lcom/peel/ui/gt;->g:Z

    if-nez v0, :cond_1

    .line 1121
    iget-object v0, p0, Lcom/peel/ui/hw;->a:Lcom/peel/ui/ht;

    iget-object v0, v0, Lcom/peel/ui/ht;->b:Lcom/peel/ui/gt;

    invoke-static {v0, p2}, Lcom/peel/ui/gt;->a(Lcom/peel/ui/gt;Landroid/view/View;)V

    .line 1122
    iget-object v0, p0, Lcom/peel/ui/hw;->a:Lcom/peel/ui/ht;

    iget-object v0, v0, Lcom/peel/ui/ht;->b:Lcom/peel/ui/gt;

    invoke-static {v0}, Lcom/peel/ui/gt;->x(Lcom/peel/ui/gt;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/peel/data/Channel;

    .line 1126
    iget-object v0, p0, Lcom/peel/ui/hw;->a:Lcom/peel/ui/ht;

    iget-object v0, v0, Lcom/peel/ui/ht;->b:Lcom/peel/ui/gt;

    .line 1127
    invoke-virtual {v0}, Lcom/peel/ui/gt;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    .line 1126
    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string/jumbo v1, "country"

    const-string/jumbo v2, ""

    .line 1127
    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "Japan"

    .line 1128
    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1129
    invoke-virtual {v6}, Lcom/peel/data/Channel;->m()Ljava/lang/String;

    move-result-object v0

    .line 1130
    iget-object v1, p0, Lcom/peel/ui/hw;->a:Lcom/peel/ui/ht;

    iget-object v1, v1, Lcom/peel/ui/ht;->b:Lcom/peel/ui/gt;

    invoke-virtual {v1}, Lcom/peel/ui/gt;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    iget-object v2, p0, Lcom/peel/ui/hw;->a:Lcom/peel/ui/ht;

    iget-object v2, v2, Lcom/peel/ui/ht;->b:Lcom/peel/ui/gt;

    invoke-static {v2}, Lcom/peel/ui/gt;->a(Lcom/peel/ui/gt;)Lcom/peel/control/a;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v1, v2, v0, v3}, Lcom/peel/util/bx;->a(Landroid/content/Context;Lcom/peel/control/a;Ljava/lang/String;Lcom/peel/util/t;)V

    .line 1139
    :goto_0
    invoke-static {}, Lcom/peel/util/a/f;->a()Lcom/peel/util/a/f;

    move-result-object v0

    sget-object v1, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    .line 1142
    invoke-virtual {v1}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v1

    if-nez v1, :cond_3

    const/4 v1, 0x1

    .line 1143
    :goto_1
    const/16 v2, 0x419

    const/16 v3, 0x7e7

    .line 1146
    invoke-virtual {v6}, Lcom/peel/data/Channel;->m()Ljava/lang/String;

    move-result-object v4

    .line 1147
    invoke-virtual {v6}, Lcom/peel/data/Channel;->b()Ljava/lang/String;

    move-result-object v6

    move v7, v5

    .line 1140
    invoke-virtual/range {v0 .. v7}, Lcom/peel/util/a/f;->a(IIILjava/lang/String;ILjava/lang/String;I)V

    .line 1152
    :cond_1
    return-void

    .line 1132
    :cond_2
    iget-object v0, p0, Lcom/peel/ui/hw;->a:Lcom/peel/ui/ht;

    iget-object v0, v0, Lcom/peel/ui/ht;->b:Lcom/peel/ui/gt;

    invoke-static {v0}, Lcom/peel/ui/gt;->a(Lcom/peel/ui/gt;)Lcom/peel/control/a;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "live://channel/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 1136
    invoke-virtual {v6}, Lcom/peel/data/Channel;->m()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1133
    invoke-static {v1}, Ljava/net/URI;->create(Ljava/lang/String;)Ljava/net/URI;

    move-result-object v1

    .line 1132
    invoke-virtual {v0, v1}, Lcom/peel/control/a;->a(Ljava/net/URI;)Z

    goto :goto_0

    .line 1142
    :cond_3
    sget-object v1, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    .line 1143
    invoke-virtual {v1}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/data/at;->f()I

    move-result v1

    goto :goto_1
.end method
