.class Lcom/peel/ui/iz;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/peel/ui/gt;


# direct methods
.method constructor <init>(Lcom/peel/ui/gt;)V
    .locals 0

    .prologue
    .line 3204
    iput-object p1, p0, Lcom/peel/ui/iz;->a:Lcom/peel/ui/gt;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    .line 3206
    iget-object v0, p0, Lcom/peel/ui/iz;->a:Lcom/peel/ui/gt;

    invoke-static {v0}, Lcom/peel/ui/gt;->U(Lcom/peel/ui/gt;)Lcom/peel/widget/ag;

    move-result-object v0

    if-nez v0, :cond_0

    .line 3207
    iget-object v0, p0, Lcom/peel/ui/iz;->a:Lcom/peel/ui/gt;

    invoke-static {v0}, Lcom/peel/ui/gt;->w(Lcom/peel/ui/gt;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lcom/peel/ui/fq;->control_change_room:I

    const/4 v2, 0x0

    .line 3208
    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 3211
    sget v0, Lcom/peel/ui/fp;->activities_lv:I

    .line 3212
    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    .line 3214
    new-instance v2, Lcom/peel/ui/ja;

    iget-object v3, p0, Lcom/peel/ui/iz;->a:Lcom/peel/ui/gt;

    .line 3216
    invoke-virtual {v3}, Lcom/peel/ui/gt;->m()Landroid/support/v4/app/ae;

    move-result-object v3

    sget v4, Lcom/peel/ui/fq;->settings_adddevice_activity_item:I

    iget-object v5, p0, Lcom/peel/ui/iz;->a:Lcom/peel/ui/gt;

    .line 3218
    invoke-static {v5}, Lcom/peel/ui/gt;->V(Lcom/peel/ui/gt;)Ljava/util/ArrayList;

    move-result-object v5

    invoke-direct {v2, p0, v3, v4, v5}, Lcom/peel/ui/ja;-><init>(Lcom/peel/ui/iz;Landroid/content/Context;ILjava/util/List;)V

    .line 3215
    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 3247
    iget-object v2, p0, Lcom/peel/ui/iz;->a:Lcom/peel/ui/gt;

    new-instance v3, Lcom/peel/widget/ag;

    iget-object v4, p0, Lcom/peel/ui/iz;->a:Lcom/peel/ui/gt;

    invoke-virtual {v4}, Lcom/peel/ui/gt;->m()Landroid/support/v4/app/ae;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/peel/widget/ag;-><init>(Landroid/content/Context;)V

    invoke-static {v2, v3}, Lcom/peel/ui/gt;->b(Lcom/peel/ui/gt;Lcom/peel/widget/ag;)Lcom/peel/widget/ag;

    .line 3248
    new-instance v2, Lcom/peel/ui/jb;

    invoke-direct {v2, p0}, Lcom/peel/ui/jb;-><init>(Lcom/peel/ui/iz;)V

    .line 3249
    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 3344
    iget-object v0, p0, Lcom/peel/ui/iz;->a:Lcom/peel/ui/gt;

    invoke-static {v0}, Lcom/peel/ui/gt;->U(Lcom/peel/ui/gt;)Lcom/peel/widget/ag;

    move-result-object v0

    .line 3345
    invoke-virtual {v0, v1}, Lcom/peel/widget/ag;->a(Landroid/view/View;)Lcom/peel/widget/ag;

    .line 3346
    iget-object v0, p0, Lcom/peel/ui/iz;->a:Lcom/peel/ui/gt;

    invoke-static {v0}, Lcom/peel/ui/gt;->U(Lcom/peel/ui/gt;)Lcom/peel/widget/ag;

    move-result-object v0

    sget v1, Lcom/peel/ui/ft;->button_volume_controller:I

    .line 3347
    invoke-virtual {v0, v1}, Lcom/peel/widget/ag;->a(I)Lcom/peel/widget/ag;

    .line 3348
    iget-object v0, p0, Lcom/peel/ui/iz;->a:Lcom/peel/ui/gt;

    invoke-static {v0}, Lcom/peel/ui/gt;->U(Lcom/peel/ui/gt;)Lcom/peel/widget/ag;

    move-result-object v0

    sget v1, Lcom/peel/ui/ft;->which_volume_control:I

    .line 3349
    invoke-virtual {v0, v1}, Lcom/peel/widget/ag;->b(I)Lcom/peel/widget/ag;

    .line 3354
    :cond_0
    iget-object v0, p0, Lcom/peel/ui/iz;->a:Lcom/peel/ui/gt;

    invoke-static {v0}, Lcom/peel/ui/gt;->U(Lcom/peel/ui/gt;)Lcom/peel/widget/ag;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/widget/ag;->isShowing()Z

    move-result v0

    if-nez v0, :cond_1

    .line 3355
    iget-object v0, p0, Lcom/peel/ui/iz;->a:Lcom/peel/ui/gt;

    invoke-static {v0}, Lcom/peel/ui/gt;->U(Lcom/peel/ui/gt;)Lcom/peel/widget/ag;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/ui/iz;->a:Lcom/peel/ui/gt;

    invoke-static {v1}, Lcom/peel/ui/gt;->U(Lcom/peel/ui/gt;)Lcom/peel/widget/ag;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/peel/widget/ag;->a(Lcom/peel/widget/ag;)Lcom/peel/widget/ag;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/widget/ag;->show()V

    .line 3357
    :cond_1
    return-void
.end method
