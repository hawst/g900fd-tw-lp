.class public Lcom/peel/ui/bt;
.super Landroid/widget/BaseAdapter;


# instance fields
.field private a:Landroid/content/Context;

.field private b:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private c:I

.field private d:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/ArrayList;Z)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 21
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 17
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/peel/ui/bt;->b:Ljava/util/ArrayList;

    .line 22
    iput-object p1, p0, Lcom/peel/ui/bt;->a:Landroid/content/Context;

    .line 23
    iput-boolean p3, p0, Lcom/peel/ui/bt;->d:Z

    .line 24
    if-eqz p2, :cond_0

    .line 25
    iget-object v0, p0, Lcom/peel/ui/bt;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 26
    :cond_0
    const/4 v0, -0x1

    iput v0, p0, Lcom/peel/ui/bt;->c:I

    .line 27
    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 87
    iget v0, p0, Lcom/peel/ui/bt;->c:I

    return v0
.end method

.method public a(I)V
    .locals 0

    .prologue
    .line 91
    iput p1, p0, Lcom/peel/ui/bt;->c:I

    .line 92
    invoke-virtual {p0}, Lcom/peel/ui/bt;->notifyDataSetChanged()V

    .line 93
    return-void
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/peel/ui/bt;->b:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/peel/ui/bt;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    goto :goto_0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lcom/peel/ui/bt;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2

    .prologue
    .line 83
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 31
    iget-object v0, p0, Lcom/peel/ui/bt;->a:Landroid/content/Context;

    const-string/jumbo v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 32
    if-nez p2, :cond_1

    new-instance v1, Lcom/peel/ui/bv;

    invoke-direct {v1, v3}, Lcom/peel/ui/bv;-><init>(Lcom/peel/ui/bu;)V

    .line 34
    :goto_0
    if-nez p2, :cond_0

    .line 35
    sget v2, Lcom/peel/ui/fq;->gender_age_option_layout:I

    invoke-virtual {v0, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 36
    sget v0, Lcom/peel/ui/fp;->gender_image:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, v1, Lcom/peel/ui/bv;->a:Landroid/widget/ImageView;

    .line 37
    sget v0, Lcom/peel/ui/fp;->checked_image:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, v1, Lcom/peel/ui/bv;->b:Landroid/widget/ImageView;

    .line 38
    sget v0, Lcom/peel/ui/fp;->age_text:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lcom/peel/ui/bv;->c:Landroid/widget/TextView;

    .line 39
    invoke-virtual {p2, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 42
    :cond_0
    packed-switch p1, :pswitch_data_0

    .line 65
    :goto_1
    iget-object v2, v1, Lcom/peel/ui/bv;->b:Landroid/widget/ImageView;

    iget v0, p0, Lcom/peel/ui/bt;->c:I

    if-ne p1, v0, :cond_8

    const/4 v0, 0x0

    :goto_2
    invoke-virtual {v2, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 66
    iget-object v1, v1, Lcom/peel/ui/bv;->c:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/peel/ui/bt;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 68
    return-object p2

    .line 32
    :cond_1
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/peel/ui/bv;

    goto :goto_0

    .line 44
    :pswitch_0
    iget-object v2, v1, Lcom/peel/ui/bv;->a:Landroid/widget/ImageView;

    iget-boolean v0, p0, Lcom/peel/ui/bt;->d:Z

    if-eqz v0, :cond_2

    sget v0, Lcom/peel/ui/fo;->male_btn_01:I

    :goto_3
    invoke-virtual {v2, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_1

    :cond_2
    sget v0, Lcom/peel/ui/fo;->female_btn_01:I

    goto :goto_3

    .line 47
    :pswitch_1
    iget-object v2, v1, Lcom/peel/ui/bv;->a:Landroid/widget/ImageView;

    iget-boolean v0, p0, Lcom/peel/ui/bt;->d:Z

    if-eqz v0, :cond_3

    sget v0, Lcom/peel/ui/fo;->male_btn_02:I

    :goto_4
    invoke-virtual {v2, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_1

    :cond_3
    sget v0, Lcom/peel/ui/fo;->female_btn_02:I

    goto :goto_4

    .line 50
    :pswitch_2
    iget-object v2, v1, Lcom/peel/ui/bv;->a:Landroid/widget/ImageView;

    iget-boolean v0, p0, Lcom/peel/ui/bt;->d:Z

    if-eqz v0, :cond_4

    sget v0, Lcom/peel/ui/fo;->male_btn_03:I

    :goto_5
    invoke-virtual {v2, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_1

    :cond_4
    sget v0, Lcom/peel/ui/fo;->female_btn_03:I

    goto :goto_5

    .line 53
    :pswitch_3
    iget-object v2, v1, Lcom/peel/ui/bv;->a:Landroid/widget/ImageView;

    iget-boolean v0, p0, Lcom/peel/ui/bt;->d:Z

    if-eqz v0, :cond_5

    sget v0, Lcom/peel/ui/fo;->male_btn_04:I

    :goto_6
    invoke-virtual {v2, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_1

    :cond_5
    sget v0, Lcom/peel/ui/fo;->female_btn_04:I

    goto :goto_6

    .line 56
    :pswitch_4
    iget-object v2, v1, Lcom/peel/ui/bv;->a:Landroid/widget/ImageView;

    iget-boolean v0, p0, Lcom/peel/ui/bt;->d:Z

    if-eqz v0, :cond_6

    sget v0, Lcom/peel/ui/fo;->male_btn_05:I

    :goto_7
    invoke-virtual {v2, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_1

    :cond_6
    sget v0, Lcom/peel/ui/fo;->female_btn_05:I

    goto :goto_7

    .line 59
    :pswitch_5
    iget-object v2, v1, Lcom/peel/ui/bv;->a:Landroid/widget/ImageView;

    iget-boolean v0, p0, Lcom/peel/ui/bt;->d:Z

    if-eqz v0, :cond_7

    sget v0, Lcom/peel/ui/fo;->male_btn_06:I

    :goto_8
    invoke-virtual {v2, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_1

    :cond_7
    sget v0, Lcom/peel/ui/fo;->female_btn_06:I

    goto :goto_8

    .line 65
    :cond_8
    const/16 v0, 0x8

    goto :goto_2

    .line 42
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method
