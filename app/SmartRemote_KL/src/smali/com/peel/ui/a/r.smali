.class Lcom/peel/ui/a/r;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/peel/widget/ae;


# instance fields
.field final synthetic a:Lcom/peel/ui/a/q;


# direct methods
.method constructor <init>(Lcom/peel/ui/a/q;)V
    .locals 0

    .prologue
    .line 47
    iput-object p1, p0, Lcom/peel/ui/a/r;->a:Lcom/peel/ui/a/q;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/peel/widget/ObservableListView;I)V
    .locals 0

    .prologue
    .line 64
    return-void
.end method

.method public a(Lcom/peel/widget/ObservableListView;IIII)V
    .locals 5

    .prologue
    .line 49
    invoke-virtual {p1}, Lcom/peel/widget/ObservableListView;->getFirstVisiblePosition()I

    move-result v1

    .line 50
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/peel/widget/ObservableListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v2

    .line 52
    iget-object v0, p0, Lcom/peel/ui/a/r;->a:Lcom/peel/ui/a/q;

    invoke-static {v0}, Lcom/peel/ui/a/q;->a(Lcom/peel/ui/a/q;)Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/widget/ObservableListView;

    .line 53
    invoke-virtual {v0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 54
    invoke-virtual {v0, v1, v2}, Lcom/peel/widget/ObservableListView;->setSelectionFromTop(II)V

    goto :goto_0

    .line 56
    :cond_1
    iget-object v0, p0, Lcom/peel/ui/a/r;->a:Lcom/peel/ui/a/q;

    invoke-static {v0}, Lcom/peel/ui/a/q;->b(Lcom/peel/ui/a/q;)Lcom/peel/ui/a/u;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 57
    iget-object v0, p0, Lcom/peel/ui/a/r;->a:Lcom/peel/ui/a/q;

    invoke-static {v0}, Lcom/peel/ui/a/q;->b(Lcom/peel/ui/a/q;)Lcom/peel/ui/a/u;

    move-result-object v0

    invoke-interface {v0, v1, v2}, Lcom/peel/ui/a/u;->a(II)V

    .line 60
    :cond_2
    iget-object v0, p0, Lcom/peel/ui/a/r;->a:Lcom/peel/ui/a/q;

    iget-object v0, v0, Lcom/peel/ui/a/q;->a:Landroid/os/Bundle;

    const-string/jumbo v3, "first_visible"

    invoke-virtual {v0, v3, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 61
    iget-object v0, p0, Lcom/peel/ui/a/r;->a:Lcom/peel/ui/a/q;

    iget-object v0, v0, Lcom/peel/ui/a/q;->a:Landroid/os/Bundle;

    const-string/jumbo v1, "top"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 62
    return-void
.end method
