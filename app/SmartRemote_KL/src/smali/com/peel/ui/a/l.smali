.class Lcom/peel/ui/a/l;
.super Lcom/google/android/gms/ads/AdListener;


# instance fields
.field final synthetic a:Lcom/peel/ui/a/k;


# direct methods
.method constructor <init>(Lcom/peel/ui/a/k;)V
    .locals 0

    .prologue
    .line 361
    iput-object p1, p0, Lcom/peel/ui/a/l;->a:Lcom/peel/ui/a/k;

    invoke-direct {p0}, Lcom/google/android/gms/ads/AdListener;-><init>()V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v5, 0x0

    .line 433
    invoke-super {p0}, Lcom/google/android/gms/ads/AdListener;->a()V

    .line 434
    iget-object v0, p0, Lcom/peel/ui/a/l;->a:Lcom/peel/ui/a/k;

    iget-object v0, v0, Lcom/peel/ui/a/k;->d:Lcom/peel/ui/a/a;

    invoke-static {v0, v1}, Lcom/peel/ui/a/a;->b(Lcom/peel/ui/a/a;Z)Z

    .line 436
    invoke-static {}, Lcom/peel/ui/a/a;->d()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v2, " ############# onAdLoaded"

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 438
    invoke-static {}, Lcom/peel/util/a/f;->a()Lcom/peel/util/a/f;

    move-result-object v0

    sget-object v2, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v2}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v2

    if-nez v2, :cond_2

    :goto_0
    const/16 v2, 0x84e

    iget-object v3, p0, Lcom/peel/ui/a/l;->a:Lcom/peel/ui/a/k;

    iget v3, v3, Lcom/peel/ui/a/k;->b:I

    const-string/jumbo v4, "banner"

    const-string/jumbo v6, "DFP"

    move v7, v5

    invoke-virtual/range {v0 .. v7}, Lcom/peel/util/a/f;->a(IIILjava/lang/String;ILjava/lang/String;I)V

    .line 441
    iget-object v0, p0, Lcom/peel/ui/a/l;->a:Lcom/peel/ui/a/k;

    iget-object v0, v0, Lcom/peel/ui/a/k;->c:Lcom/peel/ui/model/DFPAd;

    iget v0, v0, Lcom/peel/ui/model/DFPAd;->s:I

    if-lez v0, :cond_0

    .line 442
    invoke-static {}, Lcom/peel/ui/a/a;->d()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "hide ad"

    new-instance v2, Lcom/peel/ui/a/n;

    invoke-direct {v2, p0}, Lcom/peel/ui/a/n;-><init>(Lcom/peel/ui/a/l;)V

    iget-object v3, p0, Lcom/peel/ui/a/l;->a:Lcom/peel/ui/a/k;

    iget-object v3, v3, Lcom/peel/ui/a/k;->c:Lcom/peel/ui/model/DFPAd;

    iget v3, v3, Lcom/peel/ui/model/DFPAd;->s:I

    int-to-long v6, v3

    invoke-static {v0, v1, v2, v6, v7}, Lcom/peel/util/i;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;J)Ljava/lang/Runnable;

    .line 449
    :cond_0
    iget-object v0, p0, Lcom/peel/ui/a/l;->a:Lcom/peel/ui/a/k;

    iget-object v0, v0, Lcom/peel/ui/a/k;->d:Lcom/peel/ui/a/a;

    invoke-static {v0}, Lcom/peel/ui/a/a;->c(Lcom/peel/ui/a/a;)Lcom/google/android/gms/ads/doubleclick/PublisherAdView;

    move-result-object v0

    invoke-virtual {v0, v5}, Lcom/google/android/gms/ads/doubleclick/PublisherAdView;->setVisibility(I)V

    .line 450
    iget-object v0, p0, Lcom/peel/ui/a/l;->a:Lcom/peel/ui/a/k;

    iget-object v0, v0, Lcom/peel/ui/a/k;->a:Lcom/peel/widget/r;

    if-eqz v0, :cond_1

    .line 451
    iget-object v0, p0, Lcom/peel/ui/a/l;->a:Lcom/peel/ui/a/k;

    iget-object v0, v0, Lcom/peel/ui/a/k;->a:Lcom/peel/widget/r;

    invoke-interface {v0}, Lcom/peel/widget/r;->a()V

    .line 454
    :cond_1
    return-void

    .line 438
    :cond_2
    sget-object v1, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v1}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/data/at;->f()I

    move-result v1

    goto :goto_0
.end method

.method public a(I)V
    .locals 12

    .prologue
    const/4 v5, 0x0

    .line 371
    invoke-super {p0, p1}, Lcom/google/android/gms/ads/AdListener;->a(I)V

    .line 372
    iget-object v0, p0, Lcom/peel/ui/a/l;->a:Lcom/peel/ui/a/k;

    iget-object v0, v0, Lcom/peel/ui/a/k;->d:Lcom/peel/ui/a/a;

    invoke-static {v0, v5}, Lcom/peel/ui/a/a;->b(Lcom/peel/ui/a/a;Z)Z

    .line 374
    invoke-static {}, Lcom/peel/ui/a/a;->d()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, " ############# onAdFailedToLoad"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 377
    packed-switch p1, :pswitch_data_0

    .line 391
    const-string/jumbo v10, "Unknown"

    .line 394
    :goto_0
    invoke-static {}, Lcom/peel/ui/a/a;->d()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "onAdFailedToLoad hide ad"

    new-instance v2, Lcom/peel/ui/a/m;

    invoke-direct {v2, p0}, Lcom/peel/ui/a/m;-><init>(Lcom/peel/ui/a/l;)V

    invoke-static {v0, v1, v2}, Lcom/peel/util/i;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/lang/Runnable;

    .line 401
    invoke-static {}, Lcom/peel/util/a/f;->a()Lcom/peel/util/a/f;

    move-result-object v0

    sget-object v1, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v1}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v1

    if-nez v1, :cond_1

    const/4 v1, 0x1

    :goto_1
    const/16 v2, 0x84f

    const/16 v3, 0x7e7

    const-string/jumbo v4, "banner"

    const-string/jumbo v6, "DFP"

    const-string/jumbo v8, ""

    move v7, v5

    move v9, v5

    move v11, v5

    invoke-virtual/range {v0 .. v11}, Lcom/peel/util/a/f;->a(IIILjava/lang/String;ILjava/lang/String;ILjava/lang/String;ILjava/lang/String;I)V

    .line 403
    iget-object v0, p0, Lcom/peel/ui/a/l;->a:Lcom/peel/ui/a/k;

    iget-object v0, v0, Lcom/peel/ui/a/k;->a:Lcom/peel/widget/r;

    if-eqz v0, :cond_0

    .line 404
    iget-object v0, p0, Lcom/peel/ui/a/l;->a:Lcom/peel/ui/a/k;

    iget-object v0, v0, Lcom/peel/ui/a/k;->a:Lcom/peel/widget/r;

    invoke-interface {v0}, Lcom/peel/widget/r;->c()V

    .line 406
    :cond_0
    return-void

    .line 379
    :pswitch_0
    const-string/jumbo v10, "Internal error"

    goto :goto_0

    .line 382
    :pswitch_1
    const-string/jumbo v10, "Invalid request"

    goto :goto_0

    .line 385
    :pswitch_2
    const-string/jumbo v10, "Network Error"

    goto :goto_0

    .line 388
    :pswitch_3
    const-string/jumbo v10, "No fill"

    goto :goto_0

    .line 401
    :cond_1
    sget-object v1, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v1}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/data/at;->f()I

    move-result v1

    goto :goto_1

    .line 377
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public b()V
    .locals 10

    .prologue
    const/4 v5, 0x0

    .line 418
    invoke-super {p0}, Lcom/google/android/gms/ads/AdListener;->b()V

    .line 420
    invoke-static {}, Lcom/peel/ui/a/a;->d()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, " ############# onAdOpened"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 422
    invoke-static {}, Lcom/peel/util/a/f;->a()Lcom/peel/util/a/f;

    move-result-object v0

    sget-object v1, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v1}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v1

    if-nez v1, :cond_1

    const/4 v1, 0x1

    :goto_0
    const/16 v2, 0x850

    iget-object v3, p0, Lcom/peel/ui/a/l;->a:Lcom/peel/ui/a/k;

    iget v3, v3, Lcom/peel/ui/a/k;->b:I

    const-string/jumbo v4, "banner"

    const-string/jumbo v6, "DFP"

    const-string/jumbo v8, ""

    move v7, v5

    move v9, v5

    invoke-virtual/range {v0 .. v9}, Lcom/peel/util/a/f;->a(IIILjava/lang/String;ILjava/lang/String;ILjava/lang/String;I)V

    .line 426
    iget-object v0, p0, Lcom/peel/ui/a/l;->a:Lcom/peel/ui/a/k;

    iget-object v0, v0, Lcom/peel/ui/a/k;->a:Lcom/peel/widget/r;

    if-eqz v0, :cond_0

    .line 427
    iget-object v0, p0, Lcom/peel/ui/a/l;->a:Lcom/peel/ui/a/k;

    iget-object v0, v0, Lcom/peel/ui/a/k;->a:Lcom/peel/widget/r;

    invoke-interface {v0}, Lcom/peel/widget/r;->b()V

    .line 429
    :cond_0
    return-void

    .line 422
    :cond_1
    sget-object v1, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v1}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/data/at;->f()I

    move-result v1

    goto :goto_0
.end method

.method public c()V
    .locals 2

    .prologue
    .line 364
    invoke-super {p0}, Lcom/google/android/gms/ads/AdListener;->c()V

    .line 366
    invoke-static {}, Lcom/peel/ui/a/a;->d()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, " ############# onAdClosed"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 367
    return-void
.end method

.method public d()V
    .locals 2

    .prologue
    .line 410
    invoke-super {p0}, Lcom/google/android/gms/ads/AdListener;->d()V

    .line 412
    invoke-static {}, Lcom/peel/ui/a/a;->d()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, " ############# onAdLeftApplication"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 413
    iget-object v0, p0, Lcom/peel/ui/a/l;->a:Lcom/peel/ui/a/k;

    iget-object v0, v0, Lcom/peel/ui/a/k;->d:Lcom/peel/ui/a/a;

    invoke-static {v0}, Lcom/peel/ui/a/a;->c(Lcom/peel/ui/a/a;)Lcom/google/android/gms/ads/doubleclick/PublisherAdView;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/google/android/gms/ads/doubleclick/PublisherAdView;->setVisibility(I)V

    .line 414
    return-void
.end method
