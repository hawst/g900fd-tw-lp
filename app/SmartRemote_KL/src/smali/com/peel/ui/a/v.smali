.class public Lcom/peel/ui/a/v;
.super Ljava/lang/Object;


# static fields
.field private static final a:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 28
    const-class v0, Lcom/peel/ui/a/v;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/peel/ui/a/v;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 27
    sget-object v0, Lcom/peel/ui/a/v;->a:Ljava/lang/String;

    return-object v0
.end method

.method public static a(Landroid/content/Context;Landroid/view/View;Lcom/peel/content/listing/Listing;ILjava/lang/String;I)V
    .locals 10

    .prologue
    const/4 v7, 0x0

    .line 87
    invoke-virtual {p2}, Lcom/peel/content/listing/Listing;->n()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "live"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    move-object v3, p2

    .line 88
    check-cast v3, Lcom/peel/content/listing/LiveListing;

    .line 89
    invoke-virtual {v3}, Lcom/peel/content/listing/LiveListing;->d()Ljava/lang/String;

    move-result-object v0

    .line 90
    invoke-virtual {v3}, Lcom/peel/content/listing/LiveListing;->c()Ljava/lang/String;

    move-result-object v1

    .line 89
    invoke-static {p0, p1, v0, v1, p5}, Lcom/peel/util/bx;->a(Landroid/content/Context;Landroid/view/View;Ljava/lang/String;Ljava/lang/String;I)V

    .line 92
    invoke-static {}, Lcom/peel/util/a/f;->a()Lcom/peel/util/a/f;

    move-result-object v0

    sget-object v1, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v1}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v1

    if-nez v1, :cond_1

    const/4 v1, 0x1

    :goto_0
    const/16 v2, 0x4b2

    .line 94
    invoke-static {v3}, Lcom/peel/util/bx;->b(Lcom/peel/content/listing/Listing;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3}, Lcom/peel/content/listing/LiveListing;->h()Ljava/lang/String;

    move-result-object v8

    move v3, p5

    move-object v4, p4

    move v5, p3

    move v9, v7

    .line 92
    invoke-virtual/range {v0 .. v9}, Lcom/peel/util/a/f;->a(IIILjava/lang/String;ILjava/lang/String;ILjava/lang/String;I)V

    .line 97
    :cond_0
    invoke-static {p2, p5}, Lcom/peel/util/bx;->b(Lcom/peel/content/listing/Listing;I)V

    .line 98
    return-void

    .line 92
    :cond_1
    sget-object v1, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v1}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/data/at;->f()I

    move-result v1

    goto :goto_0
.end method

.method public static a(Landroid/view/ViewGroup;)V
    .locals 3

    .prologue
    .line 31
    sget-object v0, Lcom/peel/ui/a/v;->a:Ljava/lang/String;

    const-string/jumbo v1, "get child view"

    new-instance v2, Lcom/peel/ui/a/w;

    invoke-direct {v2, p0}, Lcom/peel/ui/a/w;-><init>(Landroid/view/ViewGroup;)V

    invoke-static {v0, v1, v2}, Lcom/peel/util/i;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/lang/Runnable;

    .line 83
    return-void
.end method
