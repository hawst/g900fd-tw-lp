.class public Lcom/peel/ui/a/a;
.super Ljava/lang/Object;


# static fields
.field private static final a:Ljava/lang/String;

.field private static j:I

.field private static k:D


# instance fields
.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:Lcom/peel/widget/m;

.field private e:Lcom/google/android/gms/ads/doubleclick/PublisherAdView;

.field private f:F

.field private g:Z

.field private h:Z

.field private i:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 40
    const-class v0, Lcom/peel/ui/a/a;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/peel/ui/a/a;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/app/Activity;)V
    .locals 3

    .prologue
    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 62
    invoke-static {p1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string/jumbo v1, "country_ISO"

    const-string/jumbo v2, "US"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/ui/a/a;->b:Ljava/lang/String;

    .line 63
    invoke-virtual {p1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    iput v0, p0, Lcom/peel/ui/a/a;->f:F

    .line 64
    new-instance v0, Lcom/google/android/gms/ads/doubleclick/PublisherAdView;

    invoke-direct {v0, p1}, Lcom/google/android/gms/ads/doubleclick/PublisherAdView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/peel/ui/a/a;->e:Lcom/google/android/gms/ads/doubleclick/PublisherAdView;

    .line 67
    :try_start_0
    invoke-virtual {p1}, Landroid/app/Activity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-virtual {p1}, Landroid/app/Activity;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v0

    iget-object v0, v0, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    iput-object v0, p0, Lcom/peel/ui/a/a;->c:Ljava/lang/String;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 71
    :goto_0
    new-instance v0, Lcom/peel/widget/m;

    invoke-direct {v0, p1}, Lcom/peel/widget/m;-><init>(Landroid/app/Activity;)V

    iput-object v0, p0, Lcom/peel/ui/a/a;->d:Lcom/peel/widget/m;

    .line 72
    return-void

    .line 68
    :catch_0
    move-exception v0

    .line 69
    sget-object v1, Lcom/peel/ui/a/a;->a:Ljava/lang/String;

    invoke-virtual {v0}, Landroid/content/pm/PackageManager$NameNotFoundException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method static synthetic a(D)D
    .locals 0

    .prologue
    .line 38
    sput-wide p0, Lcom/peel/ui/a/a;->k:D

    return-wide p0
.end method

.method static synthetic a(Lcom/peel/ui/a/a;)F
    .locals 1

    .prologue
    .line 38
    iget v0, p0, Lcom/peel/ui/a/a;->f:F

    return v0
.end method

.method private a(ILcom/peel/ui/model/DFPAd;Lcom/peel/widget/r;)V
    .locals 3

    .prologue
    .line 357
    const-class v0, Lcom/peel/ui/a/a;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "show dfp standard banner ad"

    new-instance v2, Lcom/peel/ui/a/k;

    invoke-direct {v2, p0, p3, p1, p2}, Lcom/peel/ui/a/k;-><init>(Lcom/peel/ui/a/a;Lcom/peel/widget/r;ILcom/peel/ui/model/DFPAd;)V

    invoke-static {v0, v1, v2}, Lcom/peel/util/i;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/lang/Runnable;

    .line 476
    return-void
.end method

.method private a(Landroid/app/Activity;ILcom/peel/ui/model/DFPAd;)V
    .locals 2

    .prologue
    .line 479
    new-instance v0, Lcom/google/android/gms/ads/doubleclick/PublisherInterstitialAd;

    invoke-direct {v0, p1}, Lcom/google/android/gms/ads/doubleclick/PublisherInterstitialAd;-><init>(Landroid/content/Context;)V

    .line 480
    iget-object v1, p3, Lcom/peel/ui/model/DFPAd;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/ads/doubleclick/PublisherInterstitialAd;->a(Ljava/lang/String;)V

    .line 483
    new-instance v1, Lcom/google/android/gms/ads/doubleclick/PublisherAdRequest$Builder;

    invoke-direct {v1}, Lcom/google/android/gms/ads/doubleclick/PublisherAdRequest$Builder;-><init>()V

    invoke-virtual {v1}, Lcom/google/android/gms/ads/doubleclick/PublisherAdRequest$Builder;->a()Lcom/google/android/gms/ads/doubleclick/PublisherAdRequest;

    move-result-object v1

    .line 486
    invoke-virtual {v0, v1}, Lcom/google/android/gms/ads/doubleclick/PublisherInterstitialAd;->a(Lcom/google/android/gms/ads/doubleclick/PublisherAdRequest;)V

    .line 488
    new-instance v1, Lcom/peel/ui/a/o;

    invoke-direct {v1, p0, p2, v0}, Lcom/peel/ui/a/o;-><init>(Lcom/peel/ui/a/a;ILcom/google/android/gms/ads/doubleclick/PublisherInterstitialAd;)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/ads/doubleclick/PublisherInterstitialAd;->a(Lcom/google/android/gms/ads/AdListener;)V

    .line 553
    return-void
.end method

.method static synthetic a(Lcom/peel/ui/a/a;ILcom/peel/ui/model/DFPAd;Lcom/peel/widget/r;)V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0, p1, p2, p3}, Lcom/peel/ui/a/a;->a(ILcom/peel/ui/model/DFPAd;Lcom/peel/widget/r;)V

    return-void
.end method

.method static synthetic a(Lcom/peel/ui/a/a;Landroid/app/Activity;ILcom/peel/ui/model/DFPAd;)V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0, p1, p2, p3}, Lcom/peel/ui/a/a;->a(Landroid/app/Activity;ILcom/peel/ui/model/DFPAd;)V

    return-void
.end method

.method static synthetic a(Lcom/peel/ui/a/a;Z)Z
    .locals 0

    .prologue
    .line 38
    iput-boolean p1, p0, Lcom/peel/ui/a/a;->h:Z

    return p1
.end method

.method static synthetic b(Lcom/peel/ui/a/a;)Lcom/peel/widget/m;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/peel/ui/a/a;->d:Lcom/peel/widget/m;

    return-object v0
.end method

.method static synthetic b(Lcom/peel/ui/a/a;Z)Z
    .locals 0

    .prologue
    .line 38
    iput-boolean p1, p0, Lcom/peel/ui/a/a;->g:Z

    return p1
.end method

.method static synthetic c(Lcom/peel/ui/a/a;)Lcom/google/android/gms/ads/doubleclick/PublisherAdView;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/peel/ui/a/a;->e:Lcom/google/android/gms/ads/doubleclick/PublisherAdView;

    return-object v0
.end method

.method static synthetic c(Lcom/peel/ui/a/a;Z)Z
    .locals 0

    .prologue
    .line 38
    iput-boolean p1, p0, Lcom/peel/ui/a/a;->i:Z

    return p1
.end method

.method static synthetic d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 38
    sget-object v0, Lcom/peel/ui/a/a;->a:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic d(Lcom/peel/ui/a/a;)Z
    .locals 1

    .prologue
    .line 38
    iget-boolean v0, p0, Lcom/peel/ui/a/a;->i:Z

    return v0
.end method

.method static synthetic e()I
    .locals 2

    .prologue
    .line 38
    sget v0, Lcom/peel/ui/a/a;->j:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/peel/ui/a/a;->j:I

    return v0
.end method

.method static synthetic f()I
    .locals 1

    .prologue
    .line 38
    sget v0, Lcom/peel/ui/a/a;->j:I

    return v0
.end method


# virtual methods
.method public a(Landroid/app/Activity;Ljava/lang/String;I)V
    .locals 12

    .prologue
    .line 219
    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v0}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    move v7, v0

    .line 223
    :goto_0
    sget-object v0, Lcom/peel/content/a;->f:Lcom/peel/content/user/User;

    if-eqz v0, :cond_2

    sget-object v0, Lcom/peel/content/a;->f:Lcom/peel/content/user/User;

    invoke-virtual {v0}, Lcom/peel/content/user/User;->x()Ljava/lang/String;

    move-result-object v0

    move-object v8, v0

    .line 224
    :goto_1
    iget-object v0, p0, Lcom/peel/ui/a/a;->d:Lcom/peel/widget/m;

    if-eqz v0, :cond_0

    .line 225
    iget-object v0, p0, Lcom/peel/ui/a/a;->d:Lcom/peel/widget/m;

    invoke-virtual {v0, p3}, Lcom/peel/widget/m;->a(I)V

    .line 226
    iget-object v0, p0, Lcom/peel/ui/a/a;->d:Lcom/peel/widget/m;

    invoke-virtual {v0}, Lcom/peel/widget/m;->a()V

    .line 229
    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    long-to-double v4, v0

    .line 230
    sget-wide v0, Lcom/peel/ui/a/a;->k:D

    sub-double v0, v4, v0

    const-wide v2, 0x41124f8000000000L    # 300000.0

    cmpg-double v0, v0, v2

    if-gez v0, :cond_3

    .line 327
    :goto_2
    return-void

    .line 219
    :cond_1
    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    .line 220
    invoke-virtual {v0}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v0

    .line 221
    invoke-virtual {v0}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/data/at;->f()I

    move-result v0

    move v7, v0

    goto :goto_0

    .line 223
    :cond_2
    const/4 v0, 0x0

    move-object v8, v0

    goto :goto_1

    .line 235
    :cond_3
    iget-object v9, p0, Lcom/peel/ui/a/a;->b:Ljava/lang/String;

    const/16 v10, 0x7d0

    iget-object v11, p0, Lcom/peel/ui/a/a;->c:Ljava/lang/String;

    const-string/jumbo v6, "prestitial"

    new-instance v0, Lcom/peel/ui/a/f;

    move-object v1, p0

    move-object v2, p1

    move v3, p3

    invoke-direct/range {v0 .. v5}, Lcom/peel/ui/a/f;-><init>(Lcom/peel/ui/a/a;Landroid/app/Activity;ID)V

    move v1, v7

    move-object v2, v8

    move-object v3, v9

    move v4, v10

    move-object v5, v11

    move-object v7, p2

    move-object v8, v0

    invoke-static/range {v1 .. v8}, Lcom/peel/util/d;->a(ILjava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/peel/util/t;)V

    goto :goto_2
.end method

.method public a(Ljava/lang/String;ILcom/peel/widget/r;)V
    .locals 8

    .prologue
    .line 104
    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v0}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    .line 108
    :goto_0
    sget-object v1, Lcom/peel/content/a;->f:Lcom/peel/content/user/User;

    if-eqz v1, :cond_2

    sget-object v1, Lcom/peel/content/a;->f:Lcom/peel/content/user/User;

    invoke-virtual {v1}, Lcom/peel/content/user/User;->x()Ljava/lang/String;

    move-result-object v1

    .line 110
    :goto_1
    iget-object v2, p0, Lcom/peel/ui/a/a;->d:Lcom/peel/widget/m;

    if-eqz v2, :cond_0

    .line 111
    iget-object v2, p0, Lcom/peel/ui/a/a;->d:Lcom/peel/widget/m;

    invoke-virtual {v2, p2}, Lcom/peel/widget/m;->a(I)V

    .line 112
    iget-object v2, p0, Lcom/peel/ui/a/a;->d:Lcom/peel/widget/m;

    invoke-virtual {v2}, Lcom/peel/widget/m;->a()V

    .line 115
    :cond_0
    iget-object v2, p0, Lcom/peel/ui/a/a;->b:Ljava/lang/String;

    const/16 v3, 0x7d0

    iget-object v4, p0, Lcom/peel/ui/a/a;->c:Ljava/lang/String;

    const-string/jumbo v5, "banner"

    new-instance v7, Lcom/peel/ui/a/b;

    invoke-direct {v7, p0, p2, p3}, Lcom/peel/ui/a/b;-><init>(Lcom/peel/ui/a/a;ILcom/peel/widget/r;)V

    move-object v6, p1

    invoke-static/range {v0 .. v7}, Lcom/peel/util/d;->a(ILjava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/peel/util/t;)V

    .line 213
    return-void

    .line 104
    :cond_1
    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    .line 105
    invoke-virtual {v0}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v0

    .line 106
    invoke-virtual {v0}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/data/at;->f()I

    move-result v0

    goto :goto_0

    .line 108
    :cond_2
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 54
    iget-boolean v0, p0, Lcom/peel/ui/a/a;->g:Z

    return v0
.end method

.method public b()Landroid/view/View;
    .locals 1

    .prologue
    .line 75
    iget-boolean v0, p0, Lcom/peel/ui/a/a;->h:Z

    if-eqz v0, :cond_0

    .line 76
    iget-object v0, p0, Lcom/peel/ui/a/a;->e:Lcom/google/android/gms/ads/doubleclick/PublisherAdView;

    .line 81
    :goto_0
    return-object v0

    .line 78
    :cond_0
    iget-object v0, p0, Lcom/peel/ui/a/a;->d:Lcom/peel/widget/m;

    if-nez v0, :cond_1

    .line 79
    const/4 v0, 0x0

    goto :goto_0

    .line 81
    :cond_1
    iget-object v0, p0, Lcom/peel/ui/a/a;->d:Lcom/peel/widget/m;

    iget-object v0, v0, Lcom/peel/widget/m;->a:Landroid/view/ViewGroup;

    goto :goto_0
.end method

.method public b(Ljava/lang/String;ILcom/peel/widget/r;)V
    .locals 8

    .prologue
    .line 331
    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v0}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    .line 336
    :goto_0
    sget-object v1, Lcom/peel/content/a;->f:Lcom/peel/content/user/User;

    if-eqz v1, :cond_1

    sget-object v1, Lcom/peel/content/a;->f:Lcom/peel/content/user/User;

    invoke-virtual {v1}, Lcom/peel/content/user/User;->x()Ljava/lang/String;

    move-result-object v1

    .line 338
    :goto_1
    iget-object v2, p0, Lcom/peel/ui/a/a;->b:Ljava/lang/String;

    const/16 v3, 0x7d0

    iget-object v4, p0, Lcom/peel/ui/a/a;->c:Ljava/lang/String;

    const-string/jumbo v5, "banner"

    new-instance v7, Lcom/peel/ui/a/j;

    invoke-direct {v7, p0, p2, p3}, Lcom/peel/ui/a/j;-><init>(Lcom/peel/ui/a/a;ILcom/peel/widget/r;)V

    move-object v6, p1

    invoke-static/range {v0 .. v7}, Lcom/peel/util/d;->a(ILjava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/peel/util/t;)V

    .line 354
    return-void

    .line 331
    :cond_0
    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    .line 332
    invoke-virtual {v0}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v0

    .line 333
    invoke-virtual {v0}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/data/at;->f()I

    move-result v0

    goto :goto_0

    .line 336
    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public c()V
    .locals 2

    .prologue
    .line 85
    iget-boolean v0, p0, Lcom/peel/ui/a/a;->h:Z

    if-eqz v0, :cond_1

    .line 86
    iget-object v0, p0, Lcom/peel/ui/a/a;->e:Lcom/google/android/gms/ads/doubleclick/PublisherAdView;

    if-eqz v0, :cond_0

    .line 87
    iget-object v0, p0, Lcom/peel/ui/a/a;->e:Lcom/google/android/gms/ads/doubleclick/PublisherAdView;

    invoke-virtual {v0}, Lcom/google/android/gms/ads/doubleclick/PublisherAdView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    .line 88
    if-eqz v0, :cond_0

    .line 89
    check-cast v0, Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/peel/ui/a/a;->e:Lcom/google/android/gms/ads/doubleclick/PublisherAdView;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 100
    :cond_0
    :goto_0
    return-void

    .line 93
    :cond_1
    iget-object v0, p0, Lcom/peel/ui/a/a;->d:Lcom/peel/widget/m;

    if-eqz v0, :cond_0

    .line 94
    iget-object v0, p0, Lcom/peel/ui/a/a;->d:Lcom/peel/widget/m;

    iget-object v0, v0, Lcom/peel/widget/m;->a:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    .line 95
    if-eqz v0, :cond_0

    .line 96
    check-cast v0, Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/peel/ui/a/a;->d:Lcom/peel/widget/m;

    iget-object v1, v1, Lcom/peel/widget/m;->a:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    goto :goto_0
.end method
