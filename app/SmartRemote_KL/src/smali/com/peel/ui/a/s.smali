.class Lcom/peel/ui/a/s;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/peel/widget/ObservableListView;

.field final synthetic b:Lcom/peel/ui/a/q;


# direct methods
.method constructor <init>(Lcom/peel/ui/a/q;Lcom/peel/widget/ObservableListView;)V
    .locals 0

    .prologue
    .line 75
    iput-object p1, p0, Lcom/peel/ui/a/s;->b:Lcom/peel/ui/a/q;

    iput-object p2, p0, Lcom/peel/ui/a/s;->a:Lcom/peel/widget/ObservableListView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/16 v2, -0x3e8

    .line 78
    iget-object v0, p0, Lcom/peel/ui/a/s;->a:Lcom/peel/widget/ObservableListView;

    iget-object v1, p0, Lcom/peel/ui/a/s;->b:Lcom/peel/ui/a/q;

    invoke-static {v1}, Lcom/peel/ui/a/q;->c(Lcom/peel/ui/a/q;)Lcom/peel/widget/ae;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/peel/widget/ObservableListView;->setOnPeelScrollListener(Lcom/peel/widget/ae;)V

    .line 79
    iget-object v0, p0, Lcom/peel/ui/a/s;->b:Lcom/peel/ui/a/q;

    invoke-static {v0}, Lcom/peel/ui/a/q;->d(Lcom/peel/ui/a/q;)Lcom/peel/ui/a/t;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 80
    iget-object v0, p0, Lcom/peel/ui/a/s;->b:Lcom/peel/ui/a/q;

    invoke-static {v0}, Lcom/peel/ui/a/q;->d(Lcom/peel/ui/a/q;)Lcom/peel/ui/a/t;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/ui/a/s;->b:Lcom/peel/ui/a/q;

    invoke-virtual {v1}, Lcom/peel/ui/a/q;->a()I

    move-result v1

    invoke-interface {v0, v1}, Lcom/peel/ui/a/t;->a(I)V

    .line 82
    :cond_0
    iget-object v0, p0, Lcom/peel/ui/a/s;->b:Lcom/peel/ui/a/q;

    invoke-static {v0}, Lcom/peel/ui/a/q;->d(Lcom/peel/ui/a/q;)Lcom/peel/ui/a/t;

    move-result-object v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/peel/ui/a/s;->b:Lcom/peel/ui/a/q;

    invoke-virtual {v0}, Lcom/peel/ui/a/q;->a()I

    move-result v0

    if-le v0, v3, :cond_4

    .line 84
    iget-object v0, p0, Lcom/peel/ui/a/s;->b:Lcom/peel/ui/a/q;

    invoke-static {v0}, Lcom/peel/ui/a/q;->a(Lcom/peel/ui/a/q;)Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v2

    :cond_1
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/widget/ObservableListView;

    .line 86
    if-ne v1, v2, :cond_2

    .line 87
    :try_start_0
    invoke-virtual {v0}, Lcom/peel/widget/ObservableListView;->getSelectedItemPosition()I

    move-result v0

    move v1, v0

    .line 88
    goto :goto_0

    .line 90
    :cond_2
    invoke-virtual {v0}, Lcom/peel/widget/ObservableListView;->getSelectedItemPosition()I

    move-result v0

    if-eq v0, v1, :cond_1

    .line 91
    iget-object v0, p0, Lcom/peel/ui/a/s;->b:Lcom/peel/ui/a/q;

    invoke-static {v0}, Lcom/peel/ui/a/q;->d(Lcom/peel/ui/a/q;)Lcom/peel/ui/a/t;

    move-result-object v0

    const/4 v4, 0x1

    invoke-interface {v0, v4}, Lcom/peel/ui/a/t;->a(Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 93
    :catch_0
    move-exception v0

    goto :goto_0

    .line 96
    :cond_3
    iget-object v0, p0, Lcom/peel/ui/a/s;->b:Lcom/peel/ui/a/q;

    invoke-static {v0}, Lcom/peel/ui/a/q;->d(Lcom/peel/ui/a/q;)Lcom/peel/ui/a/t;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/peel/ui/a/t;->a(Z)V

    .line 98
    :cond_4
    return-void
.end method
