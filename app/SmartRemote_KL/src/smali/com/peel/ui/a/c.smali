.class Lcom/peel/ui/a/c;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/peel/widget/r;


# instance fields
.field final synthetic a:Lcom/peel/ui/model/DFPAd;

.field final synthetic b:Lcom/peel/ui/a/b;


# direct methods
.method constructor <init>(Lcom/peel/ui/a/b;Lcom/peel/ui/model/DFPAd;)V
    .locals 0

    .prologue
    .line 142
    iput-object p1, p0, Lcom/peel/ui/a/c;->b:Lcom/peel/ui/a/b;

    iput-object p2, p0, Lcom/peel/ui/a/c;->a:Lcom/peel/ui/model/DFPAd;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v5, 0x0

    .line 145
    iget-object v0, p0, Lcom/peel/ui/a/c;->b:Lcom/peel/ui/a/b;

    iget-object v0, v0, Lcom/peel/ui/a/b;->c:Lcom/peel/ui/a/a;

    invoke-static {v0, v1}, Lcom/peel/ui/a/a;->b(Lcom/peel/ui/a/a;Z)Z

    .line 146
    iget-object v0, p0, Lcom/peel/ui/a/c;->b:Lcom/peel/ui/a/b;

    iget-object v0, v0, Lcom/peel/ui/a/b;->c:Lcom/peel/ui/a/a;

    invoke-static {v0}, Lcom/peel/ui/a/a;->b(Lcom/peel/ui/a/a;)Lcom/peel/widget/m;

    move-result-object v0

    iget-object v0, v0, Lcom/peel/widget/m;->a:Landroid/view/ViewGroup;

    invoke-virtual {v0, v5}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 147
    iget-object v0, p0, Lcom/peel/ui/a/c;->a:Lcom/peel/ui/model/DFPAd;

    iget v0, v0, Lcom/peel/ui/model/DFPAd;->s:I

    if-lez v0, :cond_0

    .line 148
    invoke-static {}, Lcom/peel/ui/a/a;->d()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v2, "hide ad"

    new-instance v3, Lcom/peel/ui/a/d;

    invoke-direct {v3, p0}, Lcom/peel/ui/a/d;-><init>(Lcom/peel/ui/a/c;)V

    iget-object v4, p0, Lcom/peel/ui/a/c;->a:Lcom/peel/ui/model/DFPAd;

    iget v4, v4, Lcom/peel/ui/model/DFPAd;->s:I

    int-to-long v6, v4

    invoke-static {v0, v2, v3, v6, v7}, Lcom/peel/util/i;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;J)Ljava/lang/Runnable;

    .line 156
    :cond_0
    iget-object v0, p0, Lcom/peel/ui/a/c;->b:Lcom/peel/ui/a/b;

    iget-object v0, v0, Lcom/peel/ui/a/b;->b:Lcom/peel/widget/r;

    if-eqz v0, :cond_1

    .line 157
    iget-object v0, p0, Lcom/peel/ui/a/c;->b:Lcom/peel/ui/a/b;

    iget-object v0, v0, Lcom/peel/ui/a/b;->b:Lcom/peel/widget/r;

    invoke-interface {v0}, Lcom/peel/widget/r;->a()V

    .line 160
    :cond_1
    invoke-static {}, Lcom/peel/util/a/f;->a()Lcom/peel/util/a/f;

    move-result-object v0

    sget-object v2, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v2}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v2

    if-nez v2, :cond_2

    :goto_0
    const/16 v2, 0x84e

    iget-object v3, p0, Lcom/peel/ui/a/c;->b:Lcom/peel/ui/a/b;

    iget v3, v3, Lcom/peel/ui/a/b;->a:I

    const-string/jumbo v4, "banner"

    const-string/jumbo v6, "DFP"

    move v7, v5

    invoke-virtual/range {v0 .. v7}, Lcom/peel/util/a/f;->a(IIILjava/lang/String;ILjava/lang/String;I)V

    .line 163
    return-void

    .line 160
    :cond_2
    sget-object v1, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v1}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/data/at;->f()I

    move-result v1

    goto :goto_0
.end method

.method public b()V
    .locals 10

    .prologue
    const/4 v5, 0x0

    .line 167
    invoke-static {}, Lcom/peel/util/a/f;->a()Lcom/peel/util/a/f;

    move-result-object v0

    sget-object v1, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v1}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v1

    if-nez v1, :cond_1

    const/4 v1, 0x1

    :goto_0
    const/16 v2, 0x850

    iget-object v3, p0, Lcom/peel/ui/a/c;->b:Lcom/peel/ui/a/b;

    iget v3, v3, Lcom/peel/ui/a/b;->a:I

    const-string/jumbo v4, "banner"

    const-string/jumbo v6, "DFP"

    const-string/jumbo v8, ""

    move v7, v5

    move v9, v5

    invoke-virtual/range {v0 .. v9}, Lcom/peel/util/a/f;->a(IIILjava/lang/String;ILjava/lang/String;ILjava/lang/String;I)V

    .line 171
    iget-object v0, p0, Lcom/peel/ui/a/c;->b:Lcom/peel/ui/a/b;

    iget-object v0, v0, Lcom/peel/ui/a/b;->b:Lcom/peel/widget/r;

    if-eqz v0, :cond_0

    .line 172
    iget-object v0, p0, Lcom/peel/ui/a/c;->b:Lcom/peel/ui/a/b;

    iget-object v0, v0, Lcom/peel/ui/a/b;->b:Lcom/peel/widget/r;

    invoke-interface {v0}, Lcom/peel/widget/r;->b()V

    .line 175
    :cond_0
    const-class v0, Lcom/peel/ui/a/a;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "delay post for reloading a banner ad after click"

    new-instance v2, Lcom/peel/ui/a/e;

    invoke-direct {v2, p0}, Lcom/peel/ui/a/e;-><init>(Lcom/peel/ui/a/c;)V

    const-wide/16 v4, 0x7d0

    invoke-static {v0, v1, v2, v4, v5}, Lcom/peel/util/i;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;J)Ljava/lang/Runnable;

    .line 185
    return-void

    .line 167
    :cond_1
    sget-object v1, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v1}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/data/at;->f()I

    move-result v1

    goto :goto_0
.end method

.method public c()V
    .locals 12

    .prologue
    const/4 v5, 0x0

    .line 194
    iget-object v0, p0, Lcom/peel/ui/a/c;->b:Lcom/peel/ui/a/b;

    iget-object v0, v0, Lcom/peel/ui/a/b;->c:Lcom/peel/ui/a/a;

    invoke-static {v0, v5}, Lcom/peel/ui/a/a;->b(Lcom/peel/ui/a/a;Z)Z

    .line 195
    iget-object v0, p0, Lcom/peel/ui/a/c;->b:Lcom/peel/ui/a/b;

    iget-object v0, v0, Lcom/peel/ui/a/b;->c:Lcom/peel/ui/a/a;

    invoke-static {v0}, Lcom/peel/ui/a/a;->b(Lcom/peel/ui/a/a;)Lcom/peel/widget/m;

    move-result-object v0

    iget-object v0, v0, Lcom/peel/widget/m;->a:Landroid/view/ViewGroup;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 197
    iget-object v0, p0, Lcom/peel/ui/a/c;->b:Lcom/peel/ui/a/b;

    iget-object v0, v0, Lcom/peel/ui/a/b;->b:Lcom/peel/widget/r;

    if-eqz v0, :cond_0

    .line 198
    iget-object v0, p0, Lcom/peel/ui/a/c;->b:Lcom/peel/ui/a/b;

    iget-object v0, v0, Lcom/peel/ui/a/b;->b:Lcom/peel/widget/r;

    invoke-interface {v0}, Lcom/peel/widget/r;->c()V

    .line 201
    :cond_0
    invoke-static {}, Lcom/peel/util/a/f;->a()Lcom/peel/util/a/f;

    move-result-object v0

    sget-object v1, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v1}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v1

    if-nez v1, :cond_1

    const/4 v1, 0x1

    :goto_0
    const/16 v2, 0x84f

    iget-object v3, p0, Lcom/peel/ui/a/c;->b:Lcom/peel/ui/a/b;

    iget v3, v3, Lcom/peel/ui/a/b;->a:I

    const-string/jumbo v4, "banner"

    const-string/jumbo v6, "DFP"

    const-string/jumbo v8, ""

    const-string/jumbo v10, "No fill"

    move v7, v5

    move v9, v5

    move v11, v5

    invoke-virtual/range {v0 .. v11}, Lcom/peel/util/a/f;->a(IIILjava/lang/String;ILjava/lang/String;ILjava/lang/String;ILjava/lang/String;I)V

    .line 205
    return-void

    .line 201
    :cond_1
    sget-object v1, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v1}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/data/at;->f()I

    move-result v1

    goto :goto_0
.end method
