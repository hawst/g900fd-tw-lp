.class Lcom/peel/ui/a/h;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/peel/widget/r;


# instance fields
.field final synthetic a:Lcom/peel/ui/model/DFPAd;

.field final synthetic b:Lcom/peel/ui/a/f;


# direct methods
.method constructor <init>(Lcom/peel/ui/a/f;Lcom/peel/ui/model/DFPAd;)V
    .locals 0

    .prologue
    .line 259
    iput-object p1, p0, Lcom/peel/ui/a/h;->b:Lcom/peel/ui/a/f;

    iput-object p2, p0, Lcom/peel/ui/a/h;->a:Lcom/peel/ui/model/DFPAd;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 10

    .prologue
    const/4 v1, 0x1

    const/4 v5, 0x0

    .line 262
    iget-object v0, p0, Lcom/peel/ui/a/h;->b:Lcom/peel/ui/a/f;

    iget-object v0, v0, Lcom/peel/ui/a/f;->d:Lcom/peel/ui/a/a;

    invoke-static {v0, v1}, Lcom/peel/ui/a/a;->b(Lcom/peel/ui/a/a;Z)Z

    .line 263
    iget-object v0, p0, Lcom/peel/ui/a/h;->b:Lcom/peel/ui/a/f;

    iget-object v0, v0, Lcom/peel/ui/a/f;->d:Lcom/peel/ui/a/a;

    invoke-static {v0}, Lcom/peel/ui/a/a;->b(Lcom/peel/ui/a/a;)Lcom/peel/widget/m;

    move-result-object v0

    iget-object v0, v0, Lcom/peel/widget/m;->a:Landroid/view/ViewGroup;

    invoke-virtual {v0, v5}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 264
    invoke-static {}, Lcom/peel/ui/a/a;->d()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v2, "Full Page Ad Loaded"

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 265
    iget-object v0, p0, Lcom/peel/ui/a/h;->b:Lcom/peel/ui/a/f;

    iget-object v0, v0, Lcom/peel/ui/a/f;->d:Lcom/peel/ui/a/a;

    invoke-static {v0}, Lcom/peel/ui/a/a;->b(Lcom/peel/ui/a/a;)Lcom/peel/widget/m;

    move-result-object v0

    invoke-static {v0}, Lcom/peel/main/DFPWebActivity;->a(Lcom/peel/widget/m;)V

    .line 267
    invoke-static {}, Lcom/peel/widget/ag;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 268
    const-class v0, Lcom/peel/ui/a/a;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v2, "delay full page ad"

    new-instance v3, Lcom/peel/ui/a/i;

    invoke-direct {v3, p0}, Lcom/peel/ui/a/i;-><init>(Lcom/peel/ui/a/h;)V

    const-wide/16 v6, 0x1388

    invoke-static {v0, v2, v3, v6, v7}, Lcom/peel/util/i;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;J)Ljava/lang/Runnable;

    .line 285
    :goto_0
    invoke-static {}, Lcom/peel/ui/a/a;->e()I

    .line 287
    invoke-static {}, Lcom/peel/util/a/f;->a()Lcom/peel/util/a/f;

    move-result-object v0

    sget-object v2, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v2}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v2

    if-nez v2, :cond_1

    :goto_1
    const/16 v2, 0x84e

    iget-object v3, p0, Lcom/peel/ui/a/h;->b:Lcom/peel/ui/a/f;

    iget v3, v3, Lcom/peel/ui/a/f;->b:I

    const-string/jumbo v4, "prestitial"

    const-string/jumbo v6, "DFP"

    const-string/jumbo v8, ""

    move v7, v5

    move v9, v5

    invoke-virtual/range {v0 .. v9}, Lcom/peel/util/a/f;->a(IIILjava/lang/String;ILjava/lang/String;ILjava/lang/String;I)V

    .line 291
    iget-object v0, p0, Lcom/peel/ui/a/h;->b:Lcom/peel/ui/a/f;

    iget-wide v0, v0, Lcom/peel/ui/a/f;->c:D

    invoke-static {v0, v1}, Lcom/peel/ui/a/a;->a(D)D

    .line 292
    return-void

    .line 279
    :cond_0
    new-instance v0, Landroid/content/Intent;

    iget-object v2, p0, Lcom/peel/ui/a/h;->b:Lcom/peel/ui/a/f;

    iget-object v2, v2, Lcom/peel/ui/a/f;->a:Landroid/app/Activity;

    const-class v3, Lcom/peel/main/DFPWebActivity;

    invoke-direct {v0, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 280
    const-string/jumbo v2, "context"

    iget-object v3, p0, Lcom/peel/ui/a/h;->b:Lcom/peel/ui/a/f;

    iget v3, v3, Lcom/peel/ui/a/f;->b:I

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 281
    const-string/jumbo v2, "adtimeout"

    iget-object v3, p0, Lcom/peel/ui/a/h;->a:Lcom/peel/ui/model/DFPAd;

    iget v3, v3, Lcom/peel/ui/model/DFPAd;->s:I

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 282
    iget-object v2, p0, Lcom/peel/ui/a/h;->b:Lcom/peel/ui/a/f;

    iget-object v2, v2, Lcom/peel/ui/a/f;->a:Landroid/app/Activity;

    invoke-virtual {v2, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 287
    :cond_1
    sget-object v1, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v1}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/data/at;->f()I

    move-result v1

    goto :goto_1
.end method

.method public b()V
    .locals 10

    .prologue
    const/4 v5, 0x0

    .line 296
    invoke-static {}, Lcom/peel/util/a/f;->a()Lcom/peel/util/a/f;

    move-result-object v0

    sget-object v1, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v1}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v1

    if-nez v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    const/16 v2, 0x850

    iget-object v3, p0, Lcom/peel/ui/a/h;->b:Lcom/peel/ui/a/f;

    iget v3, v3, Lcom/peel/ui/a/f;->b:I

    const-string/jumbo v4, "prestitial"

    const-string/jumbo v6, "DFP"

    const-string/jumbo v8, ""

    move v7, v5

    move v9, v5

    invoke-virtual/range {v0 .. v9}, Lcom/peel/util/a/f;->a(IIILjava/lang/String;ILjava/lang/String;ILjava/lang/String;I)V

    .line 300
    return-void

    .line 296
    :cond_0
    sget-object v1, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v1}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/data/at;->f()I

    move-result v1

    goto :goto_0
.end method

.method public c()V
    .locals 12

    .prologue
    const/4 v5, 0x0

    .line 309
    iget-object v0, p0, Lcom/peel/ui/a/h;->b:Lcom/peel/ui/a/f;

    iget-object v0, v0, Lcom/peel/ui/a/f;->d:Lcom/peel/ui/a/a;

    invoke-static {v0, v5}, Lcom/peel/ui/a/a;->b(Lcom/peel/ui/a/a;Z)Z

    .line 310
    invoke-static {}, Lcom/peel/util/a/f;->a()Lcom/peel/util/a/f;

    move-result-object v0

    sget-object v1, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v1}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v1

    if-nez v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    const/16 v2, 0x84f

    iget-object v3, p0, Lcom/peel/ui/a/h;->b:Lcom/peel/ui/a/f;

    iget v3, v3, Lcom/peel/ui/a/f;->b:I

    const-string/jumbo v4, "prestitial"

    const-string/jumbo v6, "DFP"

    const-string/jumbo v8, ""

    const-string/jumbo v10, "No fill"

    move v7, v5

    move v9, v5

    move v11, v5

    invoke-virtual/range {v0 .. v11}, Lcom/peel/util/a/f;->a(IIILjava/lang/String;ILjava/lang/String;ILjava/lang/String;ILjava/lang/String;I)V

    .line 314
    return-void

    .line 310
    :cond_0
    sget-object v1, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v1}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/data/at;->f()I

    move-result v1

    goto :goto_0
.end method
