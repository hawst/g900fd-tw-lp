.class Lcom/peel/ui/a/o;
.super Lcom/google/android/gms/ads/AdListener;


# instance fields
.field final synthetic a:I

.field final synthetic b:Lcom/google/android/gms/ads/doubleclick/PublisherInterstitialAd;

.field final synthetic c:Lcom/peel/ui/a/a;


# direct methods
.method constructor <init>(Lcom/peel/ui/a/a;ILcom/google/android/gms/ads/doubleclick/PublisherInterstitialAd;)V
    .locals 0

    .prologue
    .line 488
    iput-object p1, p0, Lcom/peel/ui/a/o;->c:Lcom/peel/ui/a/a;

    iput p2, p0, Lcom/peel/ui/a/o;->a:I

    iput-object p3, p0, Lcom/peel/ui/a/o;->b:Lcom/google/android/gms/ads/doubleclick/PublisherInterstitialAd;

    invoke-direct {p0}, Lcom/google/android/gms/ads/AdListener;-><init>()V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 10

    .prologue
    const/4 v5, 0x0

    .line 543
    invoke-super {p0}, Lcom/google/android/gms/ads/AdListener;->a()V

    .line 545
    invoke-static {}, Lcom/peel/util/a/f;->a()Lcom/peel/util/a/f;

    move-result-object v0

    sget-object v1, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v1}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v1

    if-nez v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    const/16 v2, 0x84e

    iget v3, p0, Lcom/peel/ui/a/o;->a:I

    const-string/jumbo v4, "prestitial"

    const-string/jumbo v6, "DFP"

    const-string/jumbo v8, ""

    move v7, v5

    move v9, v5

    invoke-virtual/range {v0 .. v9}, Lcom/peel/util/a/f;->a(IIILjava/lang/String;ILjava/lang/String;ILjava/lang/String;I)V

    .line 548
    invoke-static {}, Lcom/peel/ui/a/a;->d()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, " ############# onAdLoaded"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 549
    invoke-static {}, Lcom/peel/ui/a/a;->e()I

    .line 550
    iget-object v0, p0, Lcom/peel/ui/a/o;->b:Lcom/google/android/gms/ads/doubleclick/PublisherInterstitialAd;

    invoke-virtual {v0}, Lcom/google/android/gms/ads/doubleclick/PublisherInterstitialAd;->a()V

    .line 551
    return-void

    .line 545
    :cond_0
    sget-object v1, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v1}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/data/at;->f()I

    move-result v1

    goto :goto_0
.end method

.method public a(I)V
    .locals 12

    .prologue
    const/4 v5, 0x0

    .line 497
    invoke-super {p0, p1}, Lcom/google/android/gms/ads/AdListener;->a(I)V

    .line 500
    packed-switch p1, :pswitch_data_0

    .line 514
    const-string/jumbo v10, "Unknown"

    .line 517
    :goto_0
    invoke-static {}, Lcom/peel/util/a/f;->a()Lcom/peel/util/a/f;

    move-result-object v0

    sget-object v1, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v1}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v1

    if-nez v1, :cond_0

    const/4 v1, 0x1

    :goto_1
    const/16 v2, 0x84f

    iget v3, p0, Lcom/peel/ui/a/o;->a:I

    const-string/jumbo v4, "prestitial"

    const-string/jumbo v6, "DFP"

    const-string/jumbo v8, ""

    move v7, v5

    move v9, v5

    move v11, v5

    invoke-virtual/range {v0 .. v11}, Lcom/peel/util/a/f;->a(IIILjava/lang/String;ILjava/lang/String;ILjava/lang/String;ILjava/lang/String;I)V

    .line 522
    invoke-static {}, Lcom/peel/ui/a/a;->d()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, " ############# onAdFailedToLoad: error code: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " -- reason: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 523
    return-void

    .line 502
    :pswitch_0
    const-string/jumbo v10, "Internal error"

    goto :goto_0

    .line 505
    :pswitch_1
    const-string/jumbo v10, "Invalid request"

    goto :goto_0

    .line 508
    :pswitch_2
    const-string/jumbo v10, "Network Error"

    goto :goto_0

    .line 511
    :pswitch_3
    const-string/jumbo v10, "No fill"

    goto :goto_0

    .line 517
    :cond_0
    sget-object v1, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v1}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/data/at;->f()I

    move-result v1

    goto :goto_1

    .line 500
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public b()V
    .locals 10

    .prologue
    const/4 v5, 0x0

    .line 533
    invoke-super {p0}, Lcom/google/android/gms/ads/AdListener;->b()V

    .line 535
    invoke-static {}, Lcom/peel/util/a/f;->a()Lcom/peel/util/a/f;

    move-result-object v0

    sget-object v1, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v1}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v1

    if-nez v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    const/16 v2, 0x850

    iget v3, p0, Lcom/peel/ui/a/o;->a:I

    const-string/jumbo v4, "prestitial"

    const-string/jumbo v6, "DFP"

    const-string/jumbo v8, ""

    move v7, v5

    move v9, v5

    invoke-virtual/range {v0 .. v9}, Lcom/peel/util/a/f;->a(IIILjava/lang/String;ILjava/lang/String;ILjava/lang/String;I)V

    .line 538
    invoke-static {}, Lcom/peel/ui/a/a;->d()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, " ############# onAdOpened"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 539
    return-void

    .line 535
    :cond_0
    sget-object v1, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v1}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/data/at;->f()I

    move-result v1

    goto :goto_0
.end method

.method public c()V
    .locals 2

    .prologue
    .line 491
    invoke-super {p0}, Lcom/google/android/gms/ads/AdListener;->c()V

    .line 492
    invoke-static {}, Lcom/peel/ui/a/a;->d()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, " ############# onAdClosed"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 493
    return-void
.end method

.method public d()V
    .locals 2

    .prologue
    .line 527
    invoke-super {p0}, Lcom/google/android/gms/ads/AdListener;->d()V

    .line 528
    invoke-static {}, Lcom/peel/ui/a/a;->d()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, " ############# onAdLeftApplication"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 529
    return-void
.end method
