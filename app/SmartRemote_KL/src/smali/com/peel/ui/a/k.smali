.class Lcom/peel/ui/a/k;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/peel/widget/r;

.field final synthetic b:I

.field final synthetic c:Lcom/peel/ui/model/DFPAd;

.field final synthetic d:Lcom/peel/ui/a/a;


# direct methods
.method constructor <init>(Lcom/peel/ui/a/a;Lcom/peel/widget/r;ILcom/peel/ui/model/DFPAd;)V
    .locals 0

    .prologue
    .line 358
    iput-object p1, p0, Lcom/peel/ui/a/k;->d:Lcom/peel/ui/a/a;

    iput-object p2, p0, Lcom/peel/ui/a/k;->a:Lcom/peel/widget/r;

    iput p3, p0, Lcom/peel/ui/a/k;->b:I

    iput-object p4, p0, Lcom/peel/ui/a/k;->c:Lcom/peel/ui/model/DFPAd;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    const/4 v2, 0x1

    .line 361
    iget-object v0, p0, Lcom/peel/ui/a/k;->d:Lcom/peel/ui/a/a;

    invoke-static {v0}, Lcom/peel/ui/a/a;->c(Lcom/peel/ui/a/a;)Lcom/google/android/gms/ads/doubleclick/PublisherAdView;

    move-result-object v0

    new-instance v1, Lcom/peel/ui/a/l;

    invoke-direct {v1, p0}, Lcom/peel/ui/a/l;-><init>(Lcom/peel/ui/a/k;)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/ads/doubleclick/PublisherAdView;->setAdListener(Lcom/google/android/gms/ads/AdListener;)V

    .line 457
    iget-object v0, p0, Lcom/peel/ui/a/k;->d:Lcom/peel/ui/a/a;

    invoke-static {v0}, Lcom/peel/ui/a/a;->d(Lcom/peel/ui/a/a;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 458
    iget-object v0, p0, Lcom/peel/ui/a/k;->d:Lcom/peel/ui/a/a;

    invoke-static {v0, v2}, Lcom/peel/ui/a/a;->c(Lcom/peel/ui/a/a;Z)Z

    .line 459
    iget-object v0, p0, Lcom/peel/ui/a/k;->d:Lcom/peel/ui/a/a;

    invoke-static {v0}, Lcom/peel/ui/a/a;->c(Lcom/peel/ui/a/a;)Lcom/google/android/gms/ads/doubleclick/PublisherAdView;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/ui/a/k;->c:Lcom/peel/ui/model/DFPAd;

    iget-object v1, v1, Lcom/peel/ui/model/DFPAd;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/ads/doubleclick/PublisherAdView;->setAdUnitId(Ljava/lang/String;)V

    .line 460
    iget-object v0, p0, Lcom/peel/ui/a/k;->c:Lcom/peel/ui/model/DFPAd;

    iget-object v0, v0, Lcom/peel/ui/model/DFPAd;->c:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/peel/ui/a/k;->c:Lcom/peel/ui/model/DFPAd;

    iget-object v0, v0, Lcom/peel/ui/model/DFPAd;->c:Ljava/lang/String;

    const-string/jumbo v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 462
    :try_start_0
    iget-object v0, p0, Lcom/peel/ui/a/k;->c:Lcom/peel/ui/model/DFPAd;

    iget-object v0, v0, Lcom/peel/ui/model/DFPAd;->c:Ljava/lang/String;

    const-string/jumbo v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    aget-object v0, v0, v1

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 463
    iget-object v1, p0, Lcom/peel/ui/a/k;->c:Lcom/peel/ui/model/DFPAd;

    iget-object v1, v1, Lcom/peel/ui/model/DFPAd;->c:Ljava/lang/String;

    const-string/jumbo v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 464
    iget-object v2, p0, Lcom/peel/ui/a/k;->d:Lcom/peel/ui/a/a;

    invoke-static {v2}, Lcom/peel/ui/a/a;->c(Lcom/peel/ui/a/a;)Lcom/google/android/gms/ads/doubleclick/PublisherAdView;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [Lcom/google/android/gms/ads/AdSize;

    const/4 v4, 0x0

    new-instance v5, Lcom/google/android/gms/ads/AdSize;

    invoke-direct {v5, v1, v0}, Lcom/google/android/gms/ads/AdSize;-><init>(II)V

    aput-object v5, v3, v4

    invoke-virtual {v2, v3}, Lcom/google/android/gms/ads/doubleclick/PublisherAdView;->setAdSizes([Lcom/google/android/gms/ads/AdSize;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 471
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/peel/ui/a/k;->d:Lcom/peel/ui/a/a;

    invoke-static {v0}, Lcom/peel/ui/a/a;->c(Lcom/peel/ui/a/a;)Lcom/google/android/gms/ads/doubleclick/PublisherAdView;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/ads/doubleclick/PublisherAdRequest$Builder;

    invoke-direct {v1}, Lcom/google/android/gms/ads/doubleclick/PublisherAdRequest$Builder;-><init>()V

    invoke-virtual {v1}, Lcom/google/android/gms/ads/doubleclick/PublisherAdRequest$Builder;->a()Lcom/google/android/gms/ads/doubleclick/PublisherAdRequest;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/ads/doubleclick/PublisherAdView;->a(Lcom/google/android/gms/ads/doubleclick/PublisherAdRequest;)V

    .line 472
    return-void

    .line 465
    :catch_0
    move-exception v0

    .line 466
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method
