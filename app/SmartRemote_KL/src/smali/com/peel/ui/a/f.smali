.class Lcom/peel/ui/a/f;
.super Lcom/peel/util/t;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/peel/util/t",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Landroid/app/Activity;

.field final synthetic b:I

.field final synthetic c:D

.field final synthetic d:Lcom/peel/ui/a/a;


# direct methods
.method constructor <init>(Lcom/peel/ui/a/a;Landroid/app/Activity;ID)V
    .locals 0

    .prologue
    .line 236
    iput-object p1, p0, Lcom/peel/ui/a/f;->d:Lcom/peel/ui/a/a;

    iput-object p2, p0, Lcom/peel/ui/a/f;->a:Landroid/app/Activity;

    iput p3, p0, Lcom/peel/ui/a/f;->b:I

    iput-wide p4, p0, Lcom/peel/ui/a/f;->c:D

    invoke-direct {p0}, Lcom/peel/util/t;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 239
    iget-boolean v0, p0, Lcom/peel/ui/a/f;->i:Z

    if-eqz v0, :cond_0

    .line 240
    new-instance v1, Lcom/peel/ui/model/DFPAd;

    invoke-direct {v1}, Lcom/peel/ui/model/DFPAd;-><init>()V

    .line 241
    iget-object v0, p0, Lcom/peel/ui/a/f;->j:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Lcom/peel/ui/model/DFPAd;->a(Ljava/lang/String;)V

    .line 249
    iget-boolean v0, v1, Lcom/peel/ui/model/DFPAd;->u:Z

    if-eqz v0, :cond_1

    .line 250
    const-class v0, Lcom/peel/ui/a/a;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v2, "request dfp interstitial"

    new-instance v3, Lcom/peel/ui/a/g;

    invoke-direct {v3, p0, v1}, Lcom/peel/ui/a/g;-><init>(Lcom/peel/ui/a/f;Lcom/peel/ui/model/DFPAd;)V

    invoke-static {v0, v2, v3}, Lcom/peel/util/i;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/lang/Runnable;

    .line 325
    :cond_0
    :goto_0
    return-void

    .line 259
    :cond_1
    iget-object v0, p0, Lcom/peel/ui/a/f;->d:Lcom/peel/ui/a/a;

    invoke-static {v0}, Lcom/peel/ui/a/a;->b(Lcom/peel/ui/a/a;)Lcom/peel/widget/m;

    move-result-object v0

    new-instance v2, Lcom/peel/ui/a/h;

    invoke-direct {v2, p0, v1}, Lcom/peel/ui/a/h;-><init>(Lcom/peel/ui/a/f;Lcom/peel/ui/model/DFPAd;)V

    invoke-virtual {v0, v2}, Lcom/peel/widget/m;->a(Lcom/peel/widget/r;)V

    .line 317
    invoke-static {}, Lcom/peel/ui/a/a;->f()I

    move-result v0

    iget v2, v1, Lcom/peel/ui/model/DFPAd;->r:I

    if-ge v0, v2, :cond_0

    .line 318
    iget-object v0, p0, Lcom/peel/ui/a/f;->d:Lcom/peel/ui/a/a;

    invoke-static {v0}, Lcom/peel/ui/a/a;->b(Lcom/peel/ui/a/a;)Lcom/peel/widget/m;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/peel/widget/m;->a(Lcom/peel/ui/model/DFPAd;)V

    goto :goto_0
.end method
