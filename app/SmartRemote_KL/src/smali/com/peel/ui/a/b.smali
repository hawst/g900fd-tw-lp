.class Lcom/peel/ui/a/b;
.super Lcom/peel/util/t;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/peel/util/t",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:I

.field final synthetic b:Lcom/peel/widget/r;

.field final synthetic c:Lcom/peel/ui/a/a;


# direct methods
.method constructor <init>(Lcom/peel/ui/a/a;ILcom/peel/widget/r;)V
    .locals 0

    .prologue
    .line 116
    iput-object p1, p0, Lcom/peel/ui/a/b;->c:Lcom/peel/ui/a/a;

    iput p2, p0, Lcom/peel/ui/a/b;->a:I

    iput-object p3, p0, Lcom/peel/ui/a/b;->b:Lcom/peel/widget/r;

    invoke-direct {p0}, Lcom/peel/util/t;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 119
    iget-boolean v0, p0, Lcom/peel/ui/a/b;->i:Z

    if-eqz v0, :cond_0

    .line 120
    new-instance v2, Lcom/peel/ui/model/DFPAd;

    invoke-direct {v2}, Lcom/peel/ui/model/DFPAd;-><init>()V

    .line 122
    iget-object v0, p0, Lcom/peel/ui/a/b;->j:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v2, v0}, Lcom/peel/ui/model/DFPAd;->a(Ljava/lang/String;)V

    .line 126
    iget-boolean v0, v2, Lcom/peel/ui/model/DFPAd;->u:Z

    if-eqz v0, :cond_1

    .line 127
    iget-object v0, p0, Lcom/peel/ui/a/b;->c:Lcom/peel/ui/a/a;

    invoke-static {v0, v1}, Lcom/peel/ui/a/a;->a(Lcom/peel/ui/a/a;Z)Z

    .line 128
    iget-object v0, p0, Lcom/peel/ui/a/b;->c:Lcom/peel/ui/a/a;

    iget v1, p0, Lcom/peel/ui/a/b;->a:I

    iget-object v3, p0, Lcom/peel/ui/a/b;->b:Lcom/peel/widget/r;

    invoke-static {v0, v1, v2, v3}, Lcom/peel/ui/a/a;->a(Lcom/peel/ui/a/a;ILcom/peel/ui/model/DFPAd;Lcom/peel/widget/r;)V

    .line 211
    :cond_0
    :goto_0
    return-void

    .line 132
    :cond_1
    :try_start_0
    iget-object v0, v2, Lcom/peel/ui/model/DFPAd;->c:Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-object v0, v2, Lcom/peel/ui/model/DFPAd;->c:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_4

    :cond_2
    const/high16 v0, 0x431e0000    # 158.0f

    iget-object v1, p0, Lcom/peel/ui/a/b;->c:Lcom/peel/ui/a/a;

    invoke-static {v1}, Lcom/peel/ui/a/a;->a(Lcom/peel/ui/a/a;)F

    move-result v1

    mul-float/2addr v0, v1

    float-to-int v0, v0

    move v1, v0

    .line 134
    :goto_1
    iget-object v0, v2, Lcom/peel/ui/model/DFPAd;->c:Ljava/lang/String;

    if-eqz v0, :cond_3

    iget-object v0, v2, Lcom/peel/ui/model/DFPAd;->c:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_5

    :cond_3
    const/high16 v0, 0x43b10000    # 354.0f

    iget-object v3, p0, Lcom/peel/ui/a/b;->c:Lcom/peel/ui/a/a;

    .line 135
    invoke-static {v3}, Lcom/peel/ui/a/a;->a(Lcom/peel/ui/a/a;)F

    move-result v3

    mul-float/2addr v0, v3

    float-to-int v0, v0

    .line 137
    :goto_2
    iget-object v3, p0, Lcom/peel/ui/a/b;->c:Lcom/peel/ui/a/a;

    invoke-static {v3}, Lcom/peel/ui/a/a;->b(Lcom/peel/ui/a/a;)Lcom/peel/widget/m;

    move-result-object v3

    invoke-virtual {v3, v0, v1}, Lcom/peel/widget/m;->a(II)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 142
    :goto_3
    iget-object v0, p0, Lcom/peel/ui/a/b;->c:Lcom/peel/ui/a/a;

    invoke-static {v0}, Lcom/peel/ui/a/a;->b(Lcom/peel/ui/a/a;)Lcom/peel/widget/m;

    move-result-object v0

    new-instance v1, Lcom/peel/ui/a/c;

    invoke-direct {v1, p0, v2}, Lcom/peel/ui/a/c;-><init>(Lcom/peel/ui/a/b;Lcom/peel/ui/model/DFPAd;)V

    invoke-virtual {v0, v1}, Lcom/peel/widget/m;->a(Lcom/peel/widget/r;)V

    .line 207
    iget-object v0, p0, Lcom/peel/ui/a/b;->c:Lcom/peel/ui/a/a;

    invoke-static {v0}, Lcom/peel/ui/a/a;->b(Lcom/peel/ui/a/a;)Lcom/peel/widget/m;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/peel/widget/m;->a(Lcom/peel/ui/model/DFPAd;)V

    goto :goto_0

    .line 132
    :cond_4
    :try_start_1
    iget-object v0, v2, Lcom/peel/ui/model/DFPAd;->c:Ljava/lang/String;

    const-string/jumbo v1, ","

    .line 133
    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    aget-object v0, v0, v1

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    int-to-float v0, v0

    iget-object v1, p0, Lcom/peel/ui/a/b;->c:Lcom/peel/ui/a/a;

    invoke-static {v1}, Lcom/peel/ui/a/a;->a(Lcom/peel/ui/a/a;)F

    move-result v1

    mul-float/2addr v0, v1

    float-to-int v0, v0

    move v1, v0

    goto :goto_1

    .line 135
    :cond_5
    iget-object v0, v2, Lcom/peel/ui/model/DFPAd;->c:Ljava/lang/String;

    const-string/jumbo v3, ","

    .line 136
    invoke-virtual {v0, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    const/4 v3, 0x0

    aget-object v0, v0, v3

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    int-to-float v0, v0

    iget-object v3, p0, Lcom/peel/ui/a/b;->c:Lcom/peel/ui/a/a;

    invoke-static {v3}, Lcom/peel/ui/a/a;->a(Lcom/peel/ui/a/a;)F
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    move-result v3

    mul-float/2addr v0, v3

    float-to-int v0, v0

    goto :goto_2

    .line 138
    :catch_0
    move-exception v0

    .line 139
    invoke-static {}, Lcom/peel/ui/a/a;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3
.end method
