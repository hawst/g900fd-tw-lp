.class public final Lcom/peel/ui/a/q;
.super Ljava/lang/Object;


# instance fields
.field a:Landroid/os/Bundle;

.field private final b:Ljava/lang/String;

.field private final c:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/peel/widget/ObservableListView;",
            ">;"
        }
    .end annotation
.end field

.field private d:Lcom/peel/ui/a/u;

.field private e:Lcom/peel/ui/a/t;

.field private final f:Lcom/peel/widget/ae;


# direct methods
.method public constructor <init>(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/ui/a/q;->b:Ljava/lang/String;

    .line 24
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/peel/ui/a/q;->c:Ljava/util/Set;

    .line 47
    new-instance v0, Lcom/peel/ui/a/r;

    invoke-direct {v0, p0}, Lcom/peel/ui/a/r;-><init>(Lcom/peel/ui/a/q;)V

    iput-object v0, p0, Lcom/peel/ui/a/q;->f:Lcom/peel/widget/ae;

    .line 21
    iput-object p1, p0, Lcom/peel/ui/a/q;->a:Landroid/os/Bundle;

    .line 22
    return-void
.end method

.method static synthetic a(Lcom/peel/ui/a/q;)Ljava/util/Set;
    .locals 1

    .prologue
    .line 14
    iget-object v0, p0, Lcom/peel/ui/a/q;->c:Ljava/util/Set;

    return-object v0
.end method

.method static synthetic b(Lcom/peel/ui/a/q;)Lcom/peel/ui/a/u;
    .locals 1

    .prologue
    .line 14
    iget-object v0, p0, Lcom/peel/ui/a/q;->d:Lcom/peel/ui/a/u;

    return-object v0
.end method

.method static synthetic c(Lcom/peel/ui/a/q;)Lcom/peel/widget/ae;
    .locals 1

    .prologue
    .line 14
    iget-object v0, p0, Lcom/peel/ui/a/q;->f:Lcom/peel/widget/ae;

    return-object v0
.end method

.method static synthetic d(Lcom/peel/ui/a/q;)Lcom/peel/ui/a/t;
    .locals 1

    .prologue
    .line 14
    iget-object v0, p0, Lcom/peel/ui/a/q;->e:Lcom/peel/ui/a/t;

    return-object v0
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 128
    iget-object v0, p0, Lcom/peel/ui/a/q;->c:Ljava/util/Set;

    if-eqz v0, :cond_0

    .line 129
    iget-object v0, p0, Lcom/peel/ui/a/q;->c:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    .line 131
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(I)V
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/peel/ui/a/q;->e:Lcom/peel/ui/a/t;

    if-eqz v0, :cond_0

    .line 69
    iget-object v0, p0, Lcom/peel/ui/a/q;->e:Lcom/peel/ui/a/t;

    invoke-interface {v0, p1}, Lcom/peel/ui/a/t;->b(I)V

    .line 71
    :cond_0
    return-void
.end method

.method public a(II)V
    .locals 2

    .prologue
    .line 119
    iget-object v0, p0, Lcom/peel/ui/a/q;->c:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/widget/ObservableListView;

    .line 121
    :try_start_0
    invoke-virtual {v0, p1, p2}, Lcom/peel/widget/ObservableListView;->setSelectionFromTop(II)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 122
    :catch_0
    move-exception v0

    goto :goto_0

    .line 125
    :cond_0
    return-void
.end method

.method public a(Lcom/peel/ui/a/t;)V
    .locals 0

    .prologue
    .line 44
    iput-object p1, p0, Lcom/peel/ui/a/q;->e:Lcom/peel/ui/a/t;

    .line 45
    return-void
.end method

.method public a(Lcom/peel/ui/a/u;)V
    .locals 0

    .prologue
    .line 40
    iput-object p1, p0, Lcom/peel/ui/a/q;->d:Lcom/peel/ui/a/u;

    .line 41
    return-void
.end method

.method public a(Lcom/peel/widget/ObservableListView;)V
    .locals 3

    .prologue
    .line 74
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/peel/ui/a/q;->c:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 75
    const-class v0, Lcom/peel/ui/du;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "delay initial scroll position"

    new-instance v2, Lcom/peel/ui/a/s;

    invoke-direct {v2, p0, p1}, Lcom/peel/ui/a/s;-><init>(Lcom/peel/ui/a/q;Lcom/peel/widget/ObservableListView;)V

    invoke-static {v0, v1, v2}, Lcom/peel/util/i;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/lang/Runnable;

    .line 102
    :cond_0
    return-void
.end method

.method public b(I)V
    .locals 5

    .prologue
    .line 109
    iget-object v0, p0, Lcom/peel/ui/a/q;->c:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/widget/ObservableListView;

    .line 111
    :try_start_0
    iget-object v2, p0, Lcom/peel/ui/a/q;->b:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "Force selection: in syncscroller to: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 112
    invoke-virtual {v0, p1}, Lcom/peel/widget/ObservableListView;->setSelection(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 113
    :catch_0
    move-exception v0

    goto :goto_0

    .line 116
    :cond_0
    return-void
.end method

.method public b(Lcom/peel/widget/ObservableListView;)V
    .locals 1

    .prologue
    .line 105
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/peel/ui/a/q;->c:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/peel/widget/ObservableListView;->setOnPeelScrollListener(Lcom/peel/widget/ae;)V

    .line 106
    :cond_0
    return-void
.end method
