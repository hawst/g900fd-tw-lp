.class Lcom/peel/ui/a/x;
.super Lcom/peel/util/t;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/peel/util/t",
        "<",
        "Ljava/util/List",
        "<",
        "Lcom/peel/content/listing/Listing;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Landroid/view/View;

.field final synthetic b:Lcom/peel/ui/a/w;


# direct methods
.method constructor <init>(Lcom/peel/ui/a/w;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 45
    iput-object p1, p0, Lcom/peel/ui/a/x;->b:Lcom/peel/ui/a/w;

    iput-object p2, p0, Lcom/peel/ui/a/x;->a:Landroid/view/View;

    invoke-direct {p0}, Lcom/peel/util/t;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(ZLjava/lang/Object;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 45
    check-cast p2, Ljava/util/List;

    invoke-virtual {p0, p1, p2, p3}, Lcom/peel/ui/a/x;->a(ZLjava/util/List;Ljava/lang/String;)V

    return-void
.end method

.method public a(ZLjava/util/List;Ljava/lang/String;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Ljava/util/List",
            "<",
            "Lcom/peel/content/listing/Listing;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 49
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 50
    new-instance v2, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v2, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    .line 51
    sget-object v0, Lcom/peel/content/a;->e:Ljava/lang/String;

    const-string/jumbo v3, "CN"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 53
    invoke-interface {p2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/content/listing/Listing;

    invoke-static {v0}, Lcom/peel/util/bx;->d(Lcom/peel/content/listing/Listing;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    .line 52
    :goto_0
    invoke-virtual {v2, v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 60
    :goto_1
    invoke-static {}, Lcom/peel/ui/a/v;->a()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "refresh badge"

    new-instance v3, Lcom/peel/ui/a/y;

    invoke-direct {v3, p0, v2}, Lcom/peel/ui/a/y;-><init>(Lcom/peel/ui/a/x;Ljava/util/concurrent/atomic/AtomicBoolean;)V

    invoke-static {v0, v1, v3}, Lcom/peel/util/i;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/lang/Runnable;

    .line 71
    :cond_0
    return-void

    :cond_1
    move v0, v1

    .line 53
    goto :goto_0

    .line 57
    :cond_2
    invoke-interface {p2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/content/listing/Listing;

    .line 56
    invoke-static {v0}, Lcom/peel/util/bx;->e(Lcom/peel/content/listing/Listing;)Z

    move-result v0

    invoke-virtual {v2, v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    goto :goto_1
.end method
