.class Lcom/peel/ui/lk;
.super Lcom/peel/util/t;


# instance fields
.field final synthetic a:Lcom/peel/ui/lj;


# direct methods
.method constructor <init>(Lcom/peel/ui/lj;I)V
    .locals 0

    .prologue
    .line 911
    iput-object p1, p0, Lcom/peel/ui/lk;->a:Lcom/peel/ui/lj;

    invoke-direct {p0, p2}, Lcom/peel/util/t;-><init>(I)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 914
    iget-object v0, p0, Lcom/peel/ui/lk;->a:Lcom/peel/ui/lj;

    iget-object v0, v0, Lcom/peel/ui/lj;->c:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0, v2}, Ljava/util/concurrent/atomic/AtomicInteger;->addAndGet(I)I

    move-result v0

    iget-object v1, p0, Lcom/peel/ui/lk;->a:Lcom/peel/ui/lj;

    iget-object v1, v1, Lcom/peel/ui/lj;->d:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ne v0, v1, :cond_2

    move v1, v2

    .line 917
    :goto_0
    if-nez v1, :cond_3

    iget-boolean v0, p0, Lcom/peel/ui/lk;->i:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/peel/ui/lk;->j:Ljava/lang/Object;

    if-nez v0, :cond_3

    .line 918
    :cond_0
    invoke-static {}, Lcom/peel/ui/kr;->c()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/peel/ui/lk;->a:Lcom/peel/ui/lj;

    iget-object v2, v2, Lcom/peel/ui/lj;->a:Lcom/peel/content/library/Library;

    invoke-virtual {v2}, Lcom/peel/content/library/Library;->f()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "no data returned from library"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 919
    iget-object v0, p0, Lcom/peel/ui/lk;->a:Lcom/peel/ui/lj;

    iget-object v0, v0, Lcom/peel/ui/lj;->f:Lcom/peel/ui/kr;

    invoke-static {v0}, Lcom/peel/ui/kr;->g(Lcom/peel/ui/kr;)V

    .line 948
    :cond_1
    :goto_1
    return-void

    :cond_2
    move v1, v3

    .line 914
    goto :goto_0

    .line 925
    :cond_3
    iget-object v0, p0, Lcom/peel/ui/lk;->j:Ljava/lang/Object;

    if-eqz v0, :cond_6

    iget-boolean v0, p0, Lcom/peel/ui/lk;->i:Z

    if-eqz v0, :cond_6

    .line 926
    iget-object v0, p0, Lcom/peel/ui/lk;->j:Ljava/lang/Object;

    check-cast v0, Ljava/util/ArrayList;

    .line 928
    if-eqz v0, :cond_4

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-lez v4, :cond_4

    .line 929
    iget-object v4, p0, Lcom/peel/ui/lk;->a:Lcom/peel/ui/lj;

    iget-object v4, v4, Lcom/peel/ui/lj;->e:Landroid/os/Bundle;

    const-string/jumbo v5, "libraryIds"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 930
    if-nez v4, :cond_5

    .line 931
    iget-object v4, p0, Lcom/peel/ui/lk;->a:Lcom/peel/ui/lj;

    iget-object v4, v4, Lcom/peel/ui/lj;->e:Landroid/os/Bundle;

    const-string/jumbo v5, "libraryIds"

    new-array v2, v2, [Ljava/lang/String;

    iget-object v6, p0, Lcom/peel/ui/lk;->a:Lcom/peel/ui/lj;

    iget-object v6, v6, Lcom/peel/ui/lj;->a:Lcom/peel/content/library/Library;

    invoke-virtual {v6}, Lcom/peel/content/library/Library;->e()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v2, v3

    invoke-virtual {v4, v5, v2}, Landroid/os/Bundle;->putStringArray(Ljava/lang/String;[Ljava/lang/String;)V

    .line 939
    :goto_2
    iget-object v2, p0, Lcom/peel/ui/lk;->a:Lcom/peel/ui/lj;

    iget-object v2, v2, Lcom/peel/ui/lj;->e:Landroid/os/Bundle;

    iget-object v3, p0, Lcom/peel/ui/lk;->a:Lcom/peel/ui/lj;

    iget-object v3, v3, Lcom/peel/ui/lj;->a:Lcom/peel/content/library/Library;

    invoke-virtual {v3}, Lcom/peel/content/library/Library;->e()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3, v0}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 945
    :cond_4
    :goto_3
    if-eqz v1, :cond_1

    .line 946
    iget-object v0, p0, Lcom/peel/ui/lk;->a:Lcom/peel/ui/lj;

    iget-object v0, v0, Lcom/peel/ui/lj;->f:Lcom/peel/ui/kr;

    iget-object v0, v0, Lcom/peel/ui/kr;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "curatedPayload"

    iget-object v2, p0, Lcom/peel/ui/lk;->a:Lcom/peel/ui/lj;

    iget-object v2, v2, Lcom/peel/ui/lj;->e:Landroid/os/Bundle;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 947
    iget-object v0, p0, Lcom/peel/ui/lk;->a:Lcom/peel/ui/lj;

    iget-object v0, v0, Lcom/peel/ui/lj;->f:Lcom/peel/ui/kr;

    iget-object v1, p0, Lcom/peel/ui/lk;->a:Lcom/peel/ui/lj;

    iget-object v1, v1, Lcom/peel/ui/lj;->e:Landroid/os/Bundle;

    invoke-static {v0, v1}, Lcom/peel/ui/kr;->a(Lcom/peel/ui/kr;Landroid/os/Bundle;)V

    goto :goto_1

    .line 933
    :cond_5
    array-length v2, v4

    add-int/lit8 v2, v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    .line 934
    array-length v5, v4

    invoke-static {v4, v3, v2, v3, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 935
    array-length v3, v4

    iget-object v4, p0, Lcom/peel/ui/lk;->a:Lcom/peel/ui/lj;

    iget-object v4, v4, Lcom/peel/ui/lj;->a:Lcom/peel/content/library/Library;

    invoke-virtual {v4}, Lcom/peel/content/library/Library;->e()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    .line 936
    iget-object v3, p0, Lcom/peel/ui/lk;->a:Lcom/peel/ui/lj;

    iget-object v3, v3, Lcom/peel/ui/lj;->e:Landroid/os/Bundle;

    const-string/jumbo v4, "libraryIds"

    invoke-virtual {v3, v4, v2}, Landroid/os/Bundle;->putStringArray(Ljava/lang/String;[Ljava/lang/String;)V

    goto :goto_2

    .line 942
    :cond_6
    invoke-static {}, Lcom/peel/ui/kr;->c()Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/peel/ui/lk;->a:Lcom/peel/ui/lj;

    iget-object v3, v3, Lcom/peel/ui/lj;->a:Lcom/peel/content/library/Library;

    invoke-virtual {v3}, Lcom/peel/content/library/Library;->f()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "no data returned from library"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3
.end method
