.class Lcom/peel/ui/is;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/peel/ui/gt;


# direct methods
.method constructor <init>(Lcom/peel/ui/gt;)V
    .locals 0

    .prologue
    .line 2700
    iput-object p1, p0, Lcom/peel/ui/is;->a:Lcom/peel/ui/gt;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    .line 2703
    invoke-static {}, Lcom/peel/ui/gt;->S()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, " ******* deviceCmdClickListener: view.getId(): "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " -- tag: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2705
    iget-object v0, p0, Lcom/peel/ui/is;->a:Lcom/peel/ui/gt;

    invoke-static {v0}, Lcom/peel/ui/gt;->J(Lcom/peel/ui/gt;)Lcom/peel/util/p;

    move-result-object v0

    invoke-static {v0}, Lcom/peel/util/i;->a(Ljava/lang/Runnable;)V

    .line 2706
    iget-object v0, p0, Lcom/peel/ui/is;->a:Lcom/peel/ui/gt;

    invoke-static {v0}, Lcom/peel/ui/gt;->J(Lcom/peel/ui/gt;)Lcom/peel/util/p;

    move-result-object v0

    const-wide/16 v2, 0x12c

    invoke-static {v0, v2, v3}, Lcom/peel/util/i;->a(Lcom/peel/util/p;J)Lcom/peel/util/p;

    .line 2707
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2708
    if-nez v0, :cond_0

    .line 2709
    invoke-static {}, Lcom/peel/ui/gt;->S()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "in deviceCmdClickListener onclick, command null, early return"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2734
    :goto_0
    return-void

    .line 2713
    :cond_0
    const-string/jumbo v1, "Play|Pause"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2714
    iget-object v0, p0, Lcom/peel/ui/is;->a:Lcom/peel/ui/gt;

    const-string/jumbo v1, "Play"

    invoke-static {v0, v1}, Lcom/peel/ui/gt;->b(Lcom/peel/ui/gt;Ljava/lang/String;)V

    .line 2715
    const-string/jumbo v0, "Pause|Play"

    invoke-virtual {p1, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    goto :goto_0

    .line 2716
    :cond_1
    const-string/jumbo v1, "Pause|Play"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2717
    iget-object v0, p0, Lcom/peel/ui/is;->a:Lcom/peel/ui/gt;

    const-string/jumbo v1, "Pause"

    invoke-static {v0, v1}, Lcom/peel/ui/gt;->b(Lcom/peel/ui/gt;Ljava/lang/String;)V

    .line 2718
    const-string/jumbo v0, "Play|Pause"

    invoke-virtual {p1, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    goto :goto_0

    .line 2719
    :cond_2
    const-string/jumbo v1, "Yellow|A"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 2720
    iget-object v0, p0, Lcom/peel/ui/is;->a:Lcom/peel/ui/gt;

    const-string/jumbo v1, "Yellow"

    invoke-static {v0, v1}, Lcom/peel/ui/gt;->b(Lcom/peel/ui/gt;Ljava/lang/String;)V

    .line 2721
    iget-object v0, p0, Lcom/peel/ui/is;->a:Lcom/peel/ui/gt;

    const-string/jumbo v1, "A"

    invoke-static {v0, v1}, Lcom/peel/ui/gt;->b(Lcom/peel/ui/gt;Ljava/lang/String;)V

    goto :goto_0

    .line 2722
    :cond_3
    const-string/jumbo v1, "Blue|B"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 2723
    iget-object v0, p0, Lcom/peel/ui/is;->a:Lcom/peel/ui/gt;

    const-string/jumbo v1, "Blue"

    invoke-static {v0, v1}, Lcom/peel/ui/gt;->b(Lcom/peel/ui/gt;Ljava/lang/String;)V

    .line 2724
    iget-object v0, p0, Lcom/peel/ui/is;->a:Lcom/peel/ui/gt;

    const-string/jumbo v1, "B"

    invoke-static {v0, v1}, Lcom/peel/ui/gt;->b(Lcom/peel/ui/gt;Ljava/lang/String;)V

    goto :goto_0

    .line 2725
    :cond_4
    const-string/jumbo v1, "Red|C"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 2726
    iget-object v0, p0, Lcom/peel/ui/is;->a:Lcom/peel/ui/gt;

    const-string/jumbo v1, "Red"

    invoke-static {v0, v1}, Lcom/peel/ui/gt;->b(Lcom/peel/ui/gt;Ljava/lang/String;)V

    .line 2727
    iget-object v0, p0, Lcom/peel/ui/is;->a:Lcom/peel/ui/gt;

    const-string/jumbo v1, "C"

    invoke-static {v0, v1}, Lcom/peel/ui/gt;->b(Lcom/peel/ui/gt;Ljava/lang/String;)V

    goto :goto_0

    .line 2728
    :cond_5
    const-string/jumbo v1, "Green|D"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 2729
    iget-object v0, p0, Lcom/peel/ui/is;->a:Lcom/peel/ui/gt;

    const-string/jumbo v1, "Green"

    invoke-static {v0, v1}, Lcom/peel/ui/gt;->b(Lcom/peel/ui/gt;Ljava/lang/String;)V

    .line 2730
    iget-object v0, p0, Lcom/peel/ui/is;->a:Lcom/peel/ui/gt;

    const-string/jumbo v1, "D"

    invoke-static {v0, v1}, Lcom/peel/ui/gt;->b(Lcom/peel/ui/gt;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 2732
    :cond_6
    iget-object v1, p0, Lcom/peel/ui/is;->a:Lcom/peel/ui/gt;

    invoke-static {v1, v0}, Lcom/peel/ui/gt;->b(Lcom/peel/ui/gt;Ljava/lang/String;)V

    goto/16 :goto_0
.end method
