.class Lcom/peel/ui/mc;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/peel/ui/lq;


# direct methods
.method constructor <init>(Lcom/peel/ui/lq;)V
    .locals 0

    .prologue
    .line 365
    iput-object p1, p0, Lcom/peel/ui/mc;->a:Lcom/peel/ui/lq;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 8

    .prologue
    const-wide/16 v4, 0x32

    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 368
    iget-object v2, p0, Lcom/peel/ui/mc;->a:Lcom/peel/ui/lq;

    invoke-static {v2}, Lcom/peel/ui/lq;->i(Lcom/peel/ui/lq;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 369
    const-class v0, Lcom/peel/ui/lq;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v2, "launch yosemite"

    new-instance v3, Lcom/peel/ui/md;

    invoke-direct {v3, p0}, Lcom/peel/ui/md;-><init>(Lcom/peel/ui/mc;)V

    invoke-static {v0, v2, v3, v4, v5}, Lcom/peel/util/i;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;J)Ljava/lang/Runnable;

    .line 407
    :cond_0
    :goto_0
    :try_start_0
    invoke-static {}, Lcom/peel/util/a/f;->a()Lcom/peel/util/a/f;

    move-result-object v0

    sget-object v2, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v2}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v2

    if-nez v2, :cond_4

    :goto_1
    const/16 v2, 0x1f4a

    const/16 v3, 0x7db

    iget-object v4, p0, Lcom/peel/ui/mc;->a:Lcom/peel/ui/lq;

    invoke-virtual {v4}, Lcom/peel/ui/lq;->m()Landroid/support/v4/app/ae;

    move-result-object v4

    invoke-static {v4}, Lcom/peel/util/bx;->d(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_5

    const-string/jumbo v4, "XX"

    :goto_2
    const/4 v5, 0x0

    sget-object v6, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const/4 v7, 0x0

    invoke-virtual/range {v0 .. v7}, Lcom/peel/util/a/f;->a(IIILjava/lang/String;ILjava/lang/String;I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    .line 411
    :goto_3
    return-void

    .line 377
    :cond_1
    iget-object v2, p0, Lcom/peel/ui/mc;->a:Lcom/peel/ui/lq;

    invoke-static {v2}, Lcom/peel/ui/lq;->j(Lcom/peel/ui/lq;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 379
    :try_start_1
    iget-object v2, p0, Lcom/peel/ui/mc;->a:Lcom/peel/ui/lq;

    iget-object v3, p0, Lcom/peel/ui/mc;->a:Lcom/peel/ui/lq;

    invoke-virtual {v3}, Lcom/peel/ui/lq;->m()Landroid/support/v4/app/ae;

    move-result-object v3

    const-string/jumbo v4, "com.sec.yosemite.phone"

    invoke-static {v3, v4}, Lcom/peel/util/bx;->c(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v3

    invoke-static {v2, v3}, Lcom/peel/ui/lq;->a(Lcom/peel/ui/lq;Z)Z

    .line 381
    iget-object v2, p0, Lcom/peel/ui/mc;->a:Lcom/peel/ui/lq;

    invoke-static {v2}, Lcom/peel/ui/lq;->i(Lcom/peel/ui/lq;)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-result v2

    if-nez v2, :cond_2

    .line 383
    :try_start_2
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v2, "android.intent.action.VIEW"

    const-string/jumbo v3, "market://details?id=com.sec.yosemite.phone"

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-direct {v0, v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 384
    const/high16 v2, 0x40080000    # 2.125f

    invoke-virtual {v0, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 385
    iget-object v2, p0, Lcom/peel/ui/mc;->a:Lcom/peel/ui/lq;

    invoke-virtual {v2, v0}, Lcom/peel/ui/lq;->a(Landroid/content/Intent;)V
    :try_end_2
    .catch Landroid/content/ActivityNotFoundException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_0

    .line 386
    :catch_0
    move-exception v0

    .line 387
    :try_start_3
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v2, "android.intent.action.VIEW"

    const-string/jumbo v3, "http://play.google.com/store/apps/details?id=com.sec.yosemite.phone"

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-direct {v0, v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 388
    const/high16 v2, 0x40080000    # 2.125f

    invoke-virtual {v0, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 389
    iget-object v2, p0, Lcom/peel/ui/mc;->a:Lcom/peel/ui/lq;

    invoke-virtual {v2, v0}, Lcom/peel/ui/lq;->a(Landroid/content/Intent;)V

    goto :goto_0

    .line 404
    :catch_1
    move-exception v0

    goto :goto_0

    .line 392
    :cond_2
    iget-object v2, p0, Lcom/peel/ui/mc;->a:Lcom/peel/ui/lq;

    invoke-virtual {v2}, Lcom/peel/ui/lq;->m()Landroid/support/v4/app/ae;

    move-result-object v2

    invoke-static {v2}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string/jumbo v3, "yosemite_enabled"

    iget-object v4, p0, Lcom/peel/ui/mc;->a:Lcom/peel/ui/lq;

    invoke-static {v4}, Lcom/peel/ui/lq;->i(Lcom/peel/ui/lq;)Z

    move-result v4

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 393
    iget-object v2, p0, Lcom/peel/ui/mc;->a:Lcom/peel/ui/lq;

    iget-object v2, v2, Lcom/peel/ui/lq;->b:Lcom/peel/d/i;

    const-string/jumbo v3, "yosemite_enabled"

    iget-object v4, p0, Lcom/peel/ui/mc;->a:Lcom/peel/ui/lq;

    invoke-static {v4}, Lcom/peel/ui/lq;->i(Lcom/peel/ui/lq;)Z

    move-result v4

    if-eqz v4, :cond_3

    move v0, v1

    :cond_3
    invoke-virtual {v2, v3, v0}, Lcom/peel/d/i;->b(Ljava/lang/String;I)V

    .line 395
    const-class v0, Lcom/peel/ui/lq;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v2, "launch yosemite"

    new-instance v3, Lcom/peel/ui/me;

    invoke-direct {v3, p0}, Lcom/peel/ui/me;-><init>(Lcom/peel/ui/mc;)V

    const-wide/16 v4, 0x32

    invoke-static {v0, v2, v3, v4, v5}, Lcom/peel/util/i;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;J)Ljava/lang/Runnable;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    goto/16 :goto_0

    .line 407
    :cond_4
    :try_start_4
    sget-object v1, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v1}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/data/at;->f()I

    move-result v1

    goto/16 :goto_1

    :cond_5
    iget-object v4, p0, Lcom/peel/ui/mc;->a:Lcom/peel/ui/lq;

    invoke-virtual {v4}, Lcom/peel/ui/lq;->m()Landroid/support/v4/app/ae;

    move-result-object v4

    invoke-static {v4}, Lcom/peel/util/bx;->d(Landroid/content/Context;)Ljava/lang/String;
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_2

    move-result-object v4

    goto/16 :goto_2

    .line 409
    :catch_2
    move-exception v0

    goto/16 :goto_3
.end method
