.class Lcom/peel/ui/ii;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/peel/ui/gt;


# direct methods
.method constructor <init>(Lcom/peel/ui/gt;)V
    .locals 0

    .prologue
    .line 1882
    iput-object p1, p0, Lcom/peel/ui/ii;->a:Lcom/peel/ui/gt;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    .line 1886
    iget-object v0, p0, Lcom/peel/ui/ii;->a:Lcom/peel/ui/gt;

    invoke-static {v0}, Lcom/peel/ui/gt;->n(Lcom/peel/ui/gt;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/peel/ui/ii;->a:Lcom/peel/ui/gt;

    invoke-static {v0}, Lcom/peel/ui/gt;->o(Lcom/peel/ui/gt;)Lcom/peel/widget/SlidingDrawer;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/peel/ui/ii;->a:Lcom/peel/ui/gt;

    invoke-static {v0}, Lcom/peel/ui/gt;->o(Lcom/peel/ui/gt;)Lcom/peel/widget/SlidingDrawer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/widget/SlidingDrawer;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/peel/ui/ii;->a:Lcom/peel/ui/gt;

    invoke-static {v0}, Lcom/peel/ui/gt;->o(Lcom/peel/ui/gt;)Lcom/peel/widget/SlidingDrawer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/widget/SlidingDrawer;->f()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1887
    iget-object v0, p0, Lcom/peel/ui/ii;->a:Lcom/peel/ui/gt;

    invoke-static {v0}, Lcom/peel/ui/gt;->o(Lcom/peel/ui/gt;)Lcom/peel/widget/SlidingDrawer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/widget/SlidingDrawer;->c()V

    .line 1901
    :cond_0
    :goto_0
    return-void

    .line 1891
    :cond_1
    iget-object v0, p0, Lcom/peel/ui/ii;->a:Lcom/peel/ui/gt;

    invoke-static {v0}, Lcom/peel/ui/gt;->C(Lcom/peel/ui/gt;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1892
    iget-object v0, p0, Lcom/peel/ui/ii;->a:Lcom/peel/ui/gt;

    invoke-virtual {v0}, Lcom/peel/ui/gt;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/ae;->finish()V

    goto :goto_0

    .line 1894
    :cond_2
    iget-object v0, p0, Lcom/peel/ui/ii;->a:Lcom/peel/ui/gt;

    iget-object v0, v0, Lcom/peel/ui/gt;->b:Lcom/peel/d/i;

    if-eqz v0, :cond_0

    .line 1895
    invoke-static {}, Lcom/peel/ui/gt;->S()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/ui/ii;->a:Lcom/peel/ui/gt;

    invoke-virtual {v1}, Lcom/peel/ui/gt;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/peel/d/e;->a(Ljava/lang/String;Landroid/support/v4/app/ae;)V

    .line 1896
    invoke-static {}, Lcom/peel/util/a/f;->a()Lcom/peel/util/a/f;

    move-result-object v1

    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v0}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v0

    if-nez v0, :cond_3

    const/4 v0, 0x1

    :goto_1
    const/16 v2, 0x445

    const/16 v3, 0x7e7

    const-string/jumbo v4, "in-app back"

    invoke-virtual {v1, v0, v2, v3, v4}, Lcom/peel/util/a/f;->a(IIILjava/lang/String;)V

    goto :goto_0

    :cond_3
    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v0}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/data/at;->f()I

    move-result v0

    goto :goto_1
.end method
