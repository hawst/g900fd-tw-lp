.class public Lcom/peel/ui/gt;
.super Lcom/peel/d/u;


# static fields
.field private static aA:I

.field private static aB:I

.field private static aC:I

.field private static aP:Landroid/app/Dialog;

.field private static final ak:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final al:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final ar:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final at:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final au:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static av:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static aw:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static ax:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final i:Ljava/lang/String;


# instance fields
.field private final aD:Landroid/os/Bundle;

.field private final aE:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/peel/data/Channel;",
            ">;"
        }
    .end annotation
.end field

.field private final aF:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/os/Bundle;",
            ">;"
        }
    .end annotation
.end field

.field private aG:J

.field private final aH:Lcom/peel/util/s;

.field private final aI:Lcom/peel/util/s;

.field private final aJ:Lcom/peel/util/p;

.field private final aK:Ljava/lang/Runnable;

.field private aL:Lcom/peel/control/a;

.field private aM:Lcom/peel/control/h;

.field private aN:Ljava/lang/String;

.field private aO:Landroid/view/LayoutInflater;

.field private aQ:Landroid/widget/LinearLayout;

.field private aR:Landroid/widget/FrameLayout;

.field private aS:Landroid/widget/FrameLayout;

.field private aT:Landroid/widget/FrameLayout;

.field private aU:Lcom/peel/widget/SlidingDrawer;

.field private aV:Lcom/peel/widget/SlidingDrawer;

.field private aW:Landroid/widget/ViewFlipper;

.field private aX:Lcom/peel/widget/ag;

.field private aY:Lcom/peel/widget/ag;

.field private aZ:Z

.field private aj:[Ljava/lang/String;

.field private am:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/peel/control/h;",
            ">;"
        }
    .end annotation
.end field

.field private an:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private ao:Z

.field private ap:Ljava/lang/String;

.field private aq:Lcom/peel/ui/ai;

.field private as:Landroid/widget/ListView;

.field private ay:Lcom/peel/widget/ag;

.field private az:Lcom/peel/widget/ag;

.field private bA:I

.field private bB:Ljava/lang/String;

.field private bC:I

.field private bD:Lcom/peel/ui/a/a;

.field private bE:Landroid/view/ViewGroup;

.field private bF:Z

.field private bG:Lcom/peel/content/library/LiveLibrary;

.field private bH:Z

.field private bI:Landroid/view/View$OnClickListener;

.field private bJ:Landroid/view/View$OnClickListener;

.field private bK:Landroid/view/View$OnClickListener;

.field private bL:Landroid/view/View$OnClickListener;

.field private bM:Landroid/view/View$OnClickListener;

.field private bN:Landroid/view/View$OnClickListener;

.field private bO:Landroid/widget/PopupWindow;

.field private bP:Landroid/widget/PopupWindow;

.field private ba:Landroid/widget/TextView;

.field private bb:Landroid/widget/TextView;

.field private bc:Landroid/widget/ImageButton;

.field private bd:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/peel/control/h;",
            ">;"
        }
    .end annotation
.end field

.field private be:Landroid/content/SharedPreferences;

.field private bf:Z

.field private bg:Z

.field private bh:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private bi:Ljava/lang/String;

.field private bj:Ljava/lang/String;

.field private bk:Landroid/widget/TextView;

.field private bl:Landroid/widget/TextView;

.field private bm:Ljava/lang/StringBuilder;

.field private bn:J

.field private bo:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private bp:Lcom/peel/widget/ag;

.field private bq:Lcom/peel/widget/ag;

.field private br:Lcom/peel/ui/f;

.field private bs:Landroid/widget/ImageView;

.field private bt:Landroid/widget/RelativeLayout;

.field private bu:Landroid/widget/ImageView;

.field private bv:Landroid/widget/TextView;

.field private bw:[Ljava/lang/String;

.field private bx:Z

.field private by:Z

.field private bz:Z

.field e:Landroid/view/View;

.field f:Landroid/view/View;

.field g:Z

.field h:Landroid/os/Handler;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 114
    const-class v0, Lcom/peel/ui/gt;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/peel/ui/gt;->i:Ljava/lang/String;

    .line 117
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    sput-object v0, Lcom/peel/ui/gt;->ak:Landroid/util/SparseArray;

    .line 118
    new-instance v0, Landroid/util/SparseArray;

    const/16 v1, 0xc

    invoke-direct {v0, v1}, Landroid/util/SparseArray;-><init>(I)V

    sput-object v0, Lcom/peel/ui/gt;->al:Landroid/util/SparseArray;

    .line 124
    new-instance v0, Ljava/util/HashSet;

    const/16 v1, 0x12

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(I)V

    sput-object v0, Lcom/peel/ui/gt;->ar:Ljava/util/Set;

    .line 128
    sget-object v0, Lcom/peel/ui/gt;->ar:Ljava/util/Set;

    const-string/jumbo v1, "PowerOff"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 129
    sget-object v0, Lcom/peel/ui/gt;->ar:Ljava/util/Set;

    const-string/jumbo v1, "PowerOn"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 130
    sget-object v0, Lcom/peel/ui/gt;->ar:Ljava/util/Set;

    const-string/jumbo v1, "1"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 131
    sget-object v0, Lcom/peel/ui/gt;->ar:Ljava/util/Set;

    const-string/jumbo v1, "2"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 132
    sget-object v0, Lcom/peel/ui/gt;->ar:Ljava/util/Set;

    const-string/jumbo v1, "3"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 133
    sget-object v0, Lcom/peel/ui/gt;->ar:Ljava/util/Set;

    const-string/jumbo v1, "4"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 134
    sget-object v0, Lcom/peel/ui/gt;->ar:Ljava/util/Set;

    const-string/jumbo v1, "5"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 135
    sget-object v0, Lcom/peel/ui/gt;->ar:Ljava/util/Set;

    const-string/jumbo v1, "6"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 136
    sget-object v0, Lcom/peel/ui/gt;->ar:Ljava/util/Set;

    const-string/jumbo v1, "7"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 137
    sget-object v0, Lcom/peel/ui/gt;->ar:Ljava/util/Set;

    const-string/jumbo v1, "8"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 138
    sget-object v0, Lcom/peel/ui/gt;->ar:Ljava/util/Set;

    const-string/jumbo v1, "9"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 139
    sget-object v0, Lcom/peel/ui/gt;->ar:Ljava/util/Set;

    const-string/jumbo v1, "0"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 140
    sget-object v0, Lcom/peel/ui/gt;->ar:Ljava/util/Set;

    const-string/jumbo v1, "Enter"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 141
    sget-object v0, Lcom/peel/ui/gt;->ar:Ljava/util/Set;

    const-string/jumbo v1, "."

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 142
    sget-object v0, Lcom/peel/ui/gt;->ar:Ljava/util/Set;

    const-string/jumbo v1, "Dot_DASh"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 143
    sget-object v0, Lcom/peel/ui/gt;->ar:Ljava/util/Set;

    const-string/jumbo v1, "Delay"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 144
    sget-object v0, Lcom/peel/ui/gt;->ar:Ljava/util/Set;

    const-string/jumbo v1, "Power_Off"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 145
    sget-object v0, Lcom/peel/ui/gt;->ar:Ljava/util/Set;

    const-string/jumbo v1, "Power"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 148
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/peel/ui/gt;->at:Ljava/util/List;

    .line 151
    sget-object v0, Lcom/peel/ui/gt;->at:Ljava/util/List;

    const-string/jumbo v1, "0"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 152
    sget-object v0, Lcom/peel/ui/gt;->at:Ljava/util/List;

    const-string/jumbo v1, "1"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 153
    sget-object v0, Lcom/peel/ui/gt;->at:Ljava/util/List;

    const-string/jumbo v1, "2"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 154
    sget-object v0, Lcom/peel/ui/gt;->at:Ljava/util/List;

    const-string/jumbo v1, "3"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 155
    sget-object v0, Lcom/peel/ui/gt;->at:Ljava/util/List;

    const-string/jumbo v1, "4"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 156
    sget-object v0, Lcom/peel/ui/gt;->at:Ljava/util/List;

    const-string/jumbo v1, "5"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 157
    sget-object v0, Lcom/peel/ui/gt;->at:Ljava/util/List;

    const-string/jumbo v1, "6"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 158
    sget-object v0, Lcom/peel/ui/gt;->at:Ljava/util/List;

    const-string/jumbo v1, "7"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 159
    sget-object v0, Lcom/peel/ui/gt;->at:Ljava/util/List;

    const-string/jumbo v1, "8"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 160
    sget-object v0, Lcom/peel/ui/gt;->at:Ljava/util/List;

    const-string/jumbo v1, "9"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 161
    sget-object v0, Lcom/peel/ui/gt;->at:Ljava/util/List;

    const-string/jumbo v1, "10"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 162
    sget-object v0, Lcom/peel/ui/gt;->at:Ljava/util/List;

    const-string/jumbo v1, "11"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 163
    sget-object v0, Lcom/peel/ui/gt;->at:Ljava/util/List;

    const-string/jumbo v1, "12"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 167
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    sput-object v0, Lcom/peel/ui/gt;->au:Ljava/util/Set;

    .line 170
    sget-object v0, Lcom/peel/ui/gt;->au:Ljava/util/Set;

    const-string/jumbo v1, "Volume_Up"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 171
    sget-object v0, Lcom/peel/ui/gt;->au:Ljava/util/Set;

    const-string/jumbo v1, "Volume_Down"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 172
    sget-object v0, Lcom/peel/ui/gt;->au:Ljava/util/Set;

    const-string/jumbo v1, "Mute"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 177
    sput v2, Lcom/peel/ui/gt;->aA:I

    sput v2, Lcom/peel/ui/gt;->aB:I

    sput v2, Lcom/peel/ui/gt;->aC:I

    return-void
.end method

.method public constructor <init>()V
    .locals 5

    .prologue
    const/4 v0, 0x0

    const/4 v4, 0x0

    .line 113
    invoke-direct {p0}, Lcom/peel/d/u;-><init>()V

    .line 116
    iput-object v0, p0, Lcom/peel/ui/gt;->aj:[Ljava/lang/String;

    .line 121
    iput-boolean v4, p0, Lcom/peel/ui/gt;->ao:Z

    .line 176
    iput-object v0, p0, Lcom/peel/ui/gt;->ay:Lcom/peel/widget/ag;

    iput-object v0, p0, Lcom/peel/ui/gt;->az:Lcom/peel/widget/ag;

    .line 178
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, Lcom/peel/ui/gt;->aD:Landroid/os/Bundle;

    .line 179
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/peel/ui/gt;->aE:Ljava/util/List;

    .line 180
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/peel/ui/gt;->aF:Ljava/util/List;

    .line 181
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/peel/ui/gt;->aG:J

    .line 183
    new-instance v0, Lcom/peel/ui/gu;

    invoke-direct {v0, p0}, Lcom/peel/ui/gu;-><init>(Lcom/peel/ui/gt;)V

    iput-object v0, p0, Lcom/peel/ui/gt;->aH:Lcom/peel/util/s;

    .line 215
    new-instance v0, Lcom/peel/ui/ho;

    invoke-direct {v0, p0}, Lcom/peel/ui/ho;-><init>(Lcom/peel/ui/gt;)V

    iput-object v0, p0, Lcom/peel/ui/gt;->aI:Lcom/peel/util/s;

    .line 253
    new-instance v0, Lcom/peel/util/p;

    sget-object v1, Lcom/peel/ui/gt;->i:Ljava/lang/String;

    const-string/jumbo v2, "clear fruit"

    new-instance v3, Lcom/peel/ui/id;

    invoke-direct {v3, p0}, Lcom/peel/ui/id;-><init>(Lcom/peel/ui/gt;)V

    invoke-direct {v0, v1, v2, v3}, Lcom/peel/util/p;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)V

    iput-object v0, p0, Lcom/peel/ui/gt;->aJ:Lcom/peel/util/p;

    .line 262
    new-instance v0, Lcom/peel/ui/iq;

    invoke-direct {v0, p0}, Lcom/peel/ui/iq;-><init>(Lcom/peel/ui/gt;)V

    iput-object v0, p0, Lcom/peel/ui/gt;->aK:Ljava/lang/Runnable;

    .line 270
    iput-boolean v4, p0, Lcom/peel/ui/gt;->g:Z

    .line 271
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/peel/ui/gt;->h:Landroid/os/Handler;

    .line 283
    iput-boolean v4, p0, Lcom/peel/ui/gt;->aZ:Z

    .line 286
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/peel/ui/gt;->bd:Ljava/util/ArrayList;

    .line 289
    iput-boolean v4, p0, Lcom/peel/ui/gt;->bf:Z

    iput-boolean v4, p0, Lcom/peel/ui/gt;->bg:Z

    .line 293
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v0, p0, Lcom/peel/ui/gt;->bm:Ljava/lang/StringBuilder;

    .line 294
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/peel/ui/gt;->bn:J

    .line 296
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/peel/ui/gt;->bo:Ljava/util/Map;

    .line 321
    iput v4, p0, Lcom/peel/ui/gt;->bA:I

    .line 324
    const/4 v0, -0x1

    iput v0, p0, Lcom/peel/ui/gt;->bC:I

    .line 2570
    new-instance v0, Lcom/peel/ui/il;

    invoke-direct {v0, p0}, Lcom/peel/ui/il;-><init>(Lcom/peel/ui/gt;)V

    iput-object v0, p0, Lcom/peel/ui/gt;->bI:Landroid/view/View$OnClickListener;

    .line 2653
    new-instance v0, Lcom/peel/ui/io;

    invoke-direct {v0, p0}, Lcom/peel/ui/io;-><init>(Lcom/peel/ui/gt;)V

    iput-object v0, p0, Lcom/peel/ui/gt;->bJ:Landroid/view/View$OnClickListener;

    .line 2668
    new-instance v0, Lcom/peel/ui/ip;

    invoke-direct {v0, p0}, Lcom/peel/ui/ip;-><init>(Lcom/peel/ui/gt;)V

    iput-object v0, p0, Lcom/peel/ui/gt;->bK:Landroid/view/View$OnClickListener;

    .line 2684
    new-instance v0, Lcom/peel/ui/ir;

    invoke-direct {v0, p0}, Lcom/peel/ui/ir;-><init>(Lcom/peel/ui/gt;)V

    iput-object v0, p0, Lcom/peel/ui/gt;->bL:Landroid/view/View$OnClickListener;

    .line 2700
    new-instance v0, Lcom/peel/ui/is;

    invoke-direct {v0, p0}, Lcom/peel/ui/is;-><init>(Lcom/peel/ui/gt;)V

    iput-object v0, p0, Lcom/peel/ui/gt;->bM:Landroid/view/View$OnClickListener;

    .line 2737
    new-instance v0, Lcom/peel/ui/it;

    invoke-direct {v0, p0}, Lcom/peel/ui/it;-><init>(Lcom/peel/ui/gt;)V

    iput-object v0, p0, Lcom/peel/ui/gt;->bN:Landroid/view/View$OnClickListener;

    .line 3475
    return-void
.end method

.method static synthetic A(Lcom/peel/ui/gt;)Landroid/widget/PopupWindow;
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lcom/peel/ui/gt;->bO:Landroid/widget/PopupWindow;

    return-object v0
.end method

.method static synthetic B(Lcom/peel/ui/gt;)J
    .locals 2

    .prologue
    .line 113
    iget-wide v0, p0, Lcom/peel/ui/gt;->aG:J

    return-wide v0
.end method

.method static synthetic C(Lcom/peel/ui/gt;)Z
    .locals 1

    .prologue
    .line 113
    iget-boolean v0, p0, Lcom/peel/ui/gt;->aZ:Z

    return v0
.end method

.method static synthetic D(Lcom/peel/ui/gt;)Landroid/widget/LinearLayout;
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lcom/peel/ui/gt;->aQ:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic E(Lcom/peel/ui/gt;)Landroid/content/SharedPreferences;
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lcom/peel/ui/gt;->be:Landroid/content/SharedPreferences;

    return-object v0
.end method

.method static synthetic F(Lcom/peel/ui/gt;)Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lcom/peel/ui/gt;->bN:Landroid/view/View$OnClickListener;

    return-object v0
.end method

.method static synthetic G(Lcom/peel/ui/gt;)V
    .locals 0

    .prologue
    .line 113
    invoke-direct {p0}, Lcom/peel/ui/gt;->ao()V

    return-void
.end method

.method static synthetic H(Lcom/peel/ui/gt;)Ljava/util/List;
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lcom/peel/ui/gt;->aF:Ljava/util/List;

    return-object v0
.end method

.method static synthetic I(Lcom/peel/ui/gt;)Landroid/widget/FrameLayout;
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lcom/peel/ui/gt;->aS:Landroid/widget/FrameLayout;

    return-object v0
.end method

.method static synthetic J(Lcom/peel/ui/gt;)Lcom/peel/util/p;
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lcom/peel/ui/gt;->aJ:Lcom/peel/util/p;

    return-object v0
.end method

.method static synthetic K(Lcom/peel/ui/gt;)Lcom/peel/control/h;
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lcom/peel/ui/gt;->aM:Lcom/peel/control/h;

    return-object v0
.end method

.method static synthetic L(Lcom/peel/ui/gt;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lcom/peel/ui/gt;->bl:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic M(Lcom/peel/ui/gt;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lcom/peel/ui/gt;->bk:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic N(Lcom/peel/ui/gt;)Z
    .locals 1

    .prologue
    .line 113
    iget-boolean v0, p0, Lcom/peel/ui/gt;->bf:Z

    return v0
.end method

.method static synthetic O(Lcom/peel/ui/gt;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lcom/peel/ui/gt;->bj:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic P(Lcom/peel/ui/gt;)Z
    .locals 1

    .prologue
    .line 113
    iget-boolean v0, p0, Lcom/peel/ui/gt;->bg:Z

    return v0
.end method

.method static synthetic Q(Lcom/peel/ui/gt;)V
    .locals 0

    .prologue
    .line 113
    invoke-direct {p0}, Lcom/peel/ui/gt;->au()V

    return-void
.end method

.method static synthetic R(Lcom/peel/ui/gt;)J
    .locals 2

    .prologue
    .line 113
    iget-wide v0, p0, Lcom/peel/ui/gt;->bn:J

    return-wide v0
.end method

.method static synthetic S()Ljava/lang/String;
    .locals 1

    .prologue
    .line 113
    sget-object v0, Lcom/peel/ui/gt;->i:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic S(Lcom/peel/ui/gt;)Ljava/lang/StringBuilder;
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lcom/peel/ui/gt;->bm:Ljava/lang/StringBuilder;

    return-object v0
.end method

.method static synthetic T(Lcom/peel/ui/gt;)I
    .locals 1

    .prologue
    .line 113
    iget v0, p0, Lcom/peel/ui/gt;->bA:I

    return v0
.end method

.method static synthetic T()Landroid/app/Dialog;
    .locals 1

    .prologue
    .line 113
    sget-object v0, Lcom/peel/ui/gt;->aP:Landroid/app/Dialog;

    return-object v0
.end method

.method static synthetic U(Lcom/peel/ui/gt;)Lcom/peel/widget/ag;
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lcom/peel/ui/gt;->aX:Lcom/peel/widget/ag;

    return-object v0
.end method

.method static synthetic U()Ljava/util/List;
    .locals 1

    .prologue
    .line 113
    sget-object v0, Lcom/peel/ui/gt;->av:Ljava/util/List;

    return-object v0
.end method

.method static synthetic V()I
    .locals 2

    .prologue
    .line 113
    sget v0, Lcom/peel/ui/gt;->aA:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/peel/ui/gt;->aA:I

    return v0
.end method

.method static synthetic V(Lcom/peel/ui/gt;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lcom/peel/ui/gt;->bd:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic W(Lcom/peel/ui/gt;)V
    .locals 0

    .prologue
    .line 113
    invoke-direct {p0}, Lcom/peel/ui/gt;->aq()V

    return-void
.end method

.method static synthetic X(Lcom/peel/ui/gt;)V
    .locals 0

    .prologue
    .line 113
    invoke-direct {p0}, Lcom/peel/ui/gt;->an()V

    return-void
.end method

.method static synthetic Y(Lcom/peel/ui/gt;)Lcom/peel/widget/ag;
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lcom/peel/ui/gt;->aY:Lcom/peel/widget/ag;

    return-object v0
.end method

.method static synthetic Z(Lcom/peel/ui/gt;)Lcom/peel/ui/ai;
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lcom/peel/ui/gt;->aq:Lcom/peel/ui/ai;

    return-object v0
.end method

.method static synthetic a(Lcom/peel/ui/gt;I)I
    .locals 0

    .prologue
    .line 113
    iput p1, p0, Lcom/peel/ui/gt;->bC:I

    return p1
.end method

.method static synthetic a(Lcom/peel/ui/gt;J)J
    .locals 1

    .prologue
    .line 113
    iput-wide p1, p0, Lcom/peel/ui/gt;->bn:J

    return-wide p1
.end method

.method static synthetic a(Landroid/app/Dialog;)Landroid/app/Dialog;
    .locals 0

    .prologue
    .line 113
    sput-object p0, Lcom/peel/ui/gt;->aP:Landroid/app/Dialog;

    return-object p0
.end method

.method static synthetic a(Lcom/peel/ui/gt;Landroid/widget/LinearLayout;)Landroid/widget/LinearLayout;
    .locals 0

    .prologue
    .line 113
    iput-object p1, p0, Lcom/peel/ui/gt;->aQ:Landroid/widget/LinearLayout;

    return-object p1
.end method

.method static synthetic a(Lcom/peel/ui/gt;)Lcom/peel/control/a;
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lcom/peel/ui/gt;->aL:Lcom/peel/control/a;

    return-object v0
.end method

.method static synthetic a(Lcom/peel/ui/gt;Lcom/peel/control/a;)Lcom/peel/control/a;
    .locals 0

    .prologue
    .line 113
    iput-object p1, p0, Lcom/peel/ui/gt;->aL:Lcom/peel/control/a;

    return-object p1
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)Lcom/peel/control/a;
    .locals 12

    .prologue
    const/4 v0, 0x0

    const/4 v3, 0x0

    .line 2479
    iget-object v1, p0, Lcom/peel/ui/gt;->aL:Lcom/peel/control/a;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/peel/ui/gt;->aL:Lcom/peel/control/a;

    invoke-virtual {v1}, Lcom/peel/control/a;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2480
    iget-object v0, p0, Lcom/peel/ui/gt;->aL:Lcom/peel/control/a;

    .line 2551
    :goto_0
    return-object v0

    .line 2482
    :cond_0
    sget-object v1, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v1}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v5

    .line 2483
    if-nez v5, :cond_1

    .line 2484
    sget-object v1, Lcom/peel/ui/gt;->i:Ljava/lang/String;

    const-string/jumbo v2, "no current room"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 2488
    :cond_1
    invoke-virtual {v5}, Lcom/peel/control/RoomControl;->c()[Lcom/peel/control/a;

    move-result-object v6

    .line 2489
    if-nez v6, :cond_2

    .line 2490
    sget-object v1, Lcom/peel/ui/gt;->i:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "no activities for room "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v5}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v3

    invoke-virtual {v3}, Lcom/peel/data/at;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 2494
    :cond_2
    if-eqz p1, :cond_4

    .line 2495
    array-length v4, v6

    move v2, v3

    :goto_1
    if-ge v2, v4, :cond_4

    aget-object v1, v6, v2

    .line 2497
    invoke-virtual {v1}, Lcom/peel/control/a;->b()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    move-object v0, v1

    .line 2498
    goto :goto_0

    .line 2495
    :cond_3
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_1

    .line 2503
    :cond_4
    if-eqz p2, :cond_8

    .line 2504
    const/16 v1, 0x3a

    invoke-virtual {p2, v1}, Ljava/lang/String;->indexOf(I)I

    move-result v1

    invoke-virtual {p2, v3, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    .line 2505
    array-length v8, v6

    move v4, v3

    :goto_2
    if-ge v4, v8, :cond_8

    aget-object v1, v6, v4

    .line 2506
    invoke-virtual {v1}, Lcom/peel/control/a;->d()[Ljava/lang/String;

    move-result-object v9

    .line 2507
    if-nez v9, :cond_6

    .line 2505
    :cond_5
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    goto :goto_2

    .line 2510
    :cond_6
    array-length v10, v9

    move v2, v3

    :goto_3
    if-ge v2, v10, :cond_5

    aget-object v11, v9, v2

    .line 2512
    invoke-virtual {v7, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_7

    move-object v0, v1

    .line 2513
    goto :goto_0

    .line 2510
    :cond_7
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 2519
    :cond_8
    invoke-virtual {v5}, Lcom/peel/control/RoomControl;->d()Lcom/peel/control/a;

    move-result-object v1

    .line 2520
    if-eqz v1, :cond_9

    const/4 v2, 0x2

    invoke-virtual {v1}, Lcom/peel/control/a;->f()I

    move-result v4

    if-ne v2, v4, :cond_9

    .line 2521
    sget-object v0, Lcom/peel/ui/gt;->i:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "activity already started"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v1}, Lcom/peel/control/a;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v1

    .line 2523
    goto/16 :goto_0

    .line 2527
    :cond_9
    iget-object v1, p0, Lcom/peel/ui/gt;->be:Landroid/content/SharedPreferences;

    const-string/jumbo v2, "last_activity"

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 2528
    if-eqz v4, :cond_b

    .line 2530
    invoke-virtual {v5}, Lcom/peel/control/RoomControl;->c()[Lcom/peel/control/a;

    move-result-object v5

    array-length v7, v5

    move v2, v3

    :goto_4
    if-ge v2, v7, :cond_b

    aget-object v1, v5, v2

    .line 2531
    invoke-virtual {v1}, Lcom/peel/control/a;->b()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_a

    move-object v0, v1

    .line 2532
    goto/16 :goto_0

    .line 2530
    :cond_a
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_4

    .line 2537
    :cond_b
    array-length v5, v6

    move v4, v3

    :goto_5
    if-ge v4, v5, :cond_f

    aget-object v1, v6, v4

    .line 2538
    invoke-virtual {v1}, Lcom/peel/control/a;->d()[Ljava/lang/String;

    move-result-object v7

    .line 2539
    if-nez v7, :cond_d

    .line 2537
    :cond_c
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    goto :goto_5

    .line 2541
    :cond_d
    array-length v8, v7

    move v2, v3

    :goto_6
    if-ge v2, v8, :cond_c

    aget-object v9, v7, v2

    .line 2542
    const-string/jumbo v10, "live"

    invoke-virtual {v10, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_e

    move-object v0, v1

    .line 2543
    goto/16 :goto_0

    .line 2541
    :cond_e
    add-int/lit8 v2, v2, 0x1

    goto :goto_6

    .line 2547
    :cond_f
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "no live activity returning first entry"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    aget-object v2, v6, v3

    if-nez v2, :cond_10

    .line 2549
    :goto_7
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2550
    sget-object v1, Lcom/peel/ui/gt;->i:Ljava/lang/String;

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2551
    aget-object v0, v6, v3

    goto/16 :goto_0

    .line 2547
    :cond_10
    aget-object v0, v6, v3

    .line 2549
    invoke-virtual {v0}, Lcom/peel/control/a;->a()Ljava/lang/String;

    move-result-object v0

    goto :goto_7
.end method

.method static synthetic a(Lcom/peel/ui/gt;Lcom/peel/control/h;)Lcom/peel/control/h;
    .locals 0

    .prologue
    .line 113
    iput-object p1, p0, Lcom/peel/ui/gt;->aM:Lcom/peel/control/h;

    return-object p1
.end method

.method static synthetic a(Lcom/peel/ui/gt;Lcom/peel/widget/ag;)Lcom/peel/widget/ag;
    .locals 0

    .prologue
    .line 113
    iput-object p1, p0, Lcom/peel/ui/gt;->bp:Lcom/peel/widget/ag;

    return-object p1
.end method

.method static synthetic a(Lcom/peel/ui/gt;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 113
    iput-object p1, p0, Lcom/peel/ui/gt;->bB:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic a(Lcom/peel/ui/gt;Ljava/lang/StringBuilder;)Ljava/lang/StringBuilder;
    .locals 0

    .prologue
    .line 113
    iput-object p1, p0, Lcom/peel/ui/gt;->bm:Ljava/lang/StringBuilder;

    return-object p1
.end method

.method private a(IZ)V
    .locals 5

    .prologue
    const/16 v4, 0x21

    .line 2958
    if-eqz p2, :cond_1

    .line 2959
    iget-object v0, p0, Lcom/peel/ui/gt;->aU:Lcom/peel/widget/SlidingDrawer;

    invoke-virtual {v0}, Lcom/peel/widget/SlidingDrawer;->getContent()Landroid/view/View;

    move-result-object v0

    sget v1, Lcom/peel/ui/fp;->devices_flipper:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ViewFlipper;

    invoke-virtual {v0}, Landroid/widget/ViewFlipper;->getChildCount()I

    move-result v2

    .line 2961
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_2

    .line 2962
    iget-object v0, p0, Lcom/peel/ui/gt;->aU:Lcom/peel/widget/SlidingDrawer;

    invoke-virtual {v0}, Lcom/peel/widget/SlidingDrawer;->getContent()Landroid/view/View;

    move-result-object v0

    sget v3, Lcom/peel/ui/fp;->devices_flipper:I

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ViewFlipper;

    invoke-virtual {v0, v1}, Landroid/widget/ViewFlipper;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    sget v3, Lcom/peel/ui/fp;->controlpad_scrollview:I

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/peel/ui/gt;->aU:Lcom/peel/widget/SlidingDrawer;

    .line 2963
    invoke-virtual {v0}, Lcom/peel/widget/SlidingDrawer;->getContent()Landroid/view/View;

    move-result-object v0

    sget v3, Lcom/peel/ui/fp;->devices_flipper:I

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ViewFlipper;

    invoke-virtual {v0, v1}, Landroid/widget/ViewFlipper;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    sget v3, Lcom/peel/ui/fp;->controlpad_scrollview:I

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    instance-of v0, v0, Landroid/widget/ScrollView;

    if-eqz v0, :cond_0

    .line 2964
    iget-object v0, p0, Lcom/peel/ui/gt;->aU:Lcom/peel/widget/SlidingDrawer;

    invoke-virtual {v0}, Lcom/peel/widget/SlidingDrawer;->getContent()Landroid/view/View;

    move-result-object v0

    sget v3, Lcom/peel/ui/fp;->devices_flipper:I

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ViewFlipper;

    invoke-virtual {v0, v1}, Landroid/widget/ViewFlipper;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    sget v3, Lcom/peel/ui/fp;->controlpad_scrollview:I

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ScrollView;

    .line 2965
    invoke-virtual {v0, v4}, Landroid/widget/ScrollView;->pageScroll(I)Z

    .line 2961
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2969
    :cond_1
    iget-object v0, p0, Lcom/peel/ui/gt;->aW:Landroid/widget/ViewFlipper;

    invoke-virtual {v0, p1}, Landroid/widget/ViewFlipper;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    sget v1, Lcom/peel/ui/fp;->controlpad_scrollview:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/peel/ui/gt;->aW:Landroid/widget/ViewFlipper;

    invoke-virtual {v0, p1}, Landroid/widget/ViewFlipper;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    sget v1, Lcom/peel/ui/fp;->controlpad_scrollview:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    instance-of v0, v0, Landroid/widget/ScrollView;

    if-eqz v0, :cond_2

    .line 2970
    iget-object v0, p0, Lcom/peel/ui/gt;->aW:Landroid/widget/ViewFlipper;

    invoke-virtual {v0, p1}, Landroid/widget/ViewFlipper;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    sget v1, Lcom/peel/ui/fp;->controlpad_scrollview:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ScrollView;

    .line 2971
    invoke-virtual {v0, v4}, Landroid/widget/ScrollView;->pageScroll(I)Z

    .line 2974
    :cond_2
    return-void
.end method

.method private a(Landroid/view/View;)V
    .locals 6

    .prologue
    .line 1038
    sget v0, Lcom/peel/ui/fp;->fav_ch_listview:I

    .line 1039
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Lcom/peel/widget/HorizontalListView;

    .line 1040
    if-eqz v5, :cond_0

    .line 1041
    new-instance v4, Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/peel/ui/gt;->aD:Landroid/os/Bundle;

    invoke-virtual {v0}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-direct {v4, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 1043
    sget-object v0, Lcom/peel/ui/gt;->aP:Landroid/app/Dialog;

    sget v1, Lcom/peel/ui/fp;->favorite_channels_textview:I

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/peel/ui/gt;->ba:Landroid/widget/TextView;

    .line 1044
    sget-object v0, Lcom/peel/ui/gt;->aP:Landroid/app/Dialog;

    sget v1, Lcom/peel/ui/fp;->add_favorite_channel_btn:I

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/peel/ui/gt;->bc:Landroid/widget/ImageButton;

    .line 1046
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_1

    .line 1048
    sget v0, Lcom/peel/ui/fp;->favorite_channels_textview:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1056
    :goto_0
    new-instance v0, Lcom/peel/ui/ht;

    invoke-virtual {p0}, Lcom/peel/ui/gt;->m()Landroid/support/v4/app/ae;

    move-result-object v2

    sget v3, Lcom/peel/ui/fq;->controlpad_channel:I

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/peel/ui/ht;-><init>(Lcom/peel/ui/gt;Landroid/content/Context;ILjava/util/List;Lcom/peel/widget/HorizontalListView;)V

    invoke-virtual {v5, v0}, Lcom/peel/widget/HorizontalListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 1159
    :cond_0
    return-void

    .line 1051
    :cond_1
    sget v0, Lcom/peel/ui/fp;->favorite_channels_textview:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method private a(Landroid/view/ViewGroup;Lcom/peel/data/g;)V
    .locals 15

    .prologue
    .line 1951
    invoke-virtual {p0}, Lcom/peel/ui/gt;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    iget-boolean v2, p0, Lcom/peel/ui/gt;->bH:Z

    invoke-static {v1, v2}, Lcom/peel/util/dw;->a(Landroid/content/Context;Z)V

    .line 1953
    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v4, 0x0

    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 1954
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 1955
    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    .line 1956
    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12}, Ljava/util/ArrayList;-><init>()V

    .line 1958
    iget-object v1, p0, Lcom/peel/ui/gt;->aU:Lcom/peel/widget/SlidingDrawer;

    const/4 v8, 0x0

    invoke-virtual {v1, v8}, Lcom/peel/widget/SlidingDrawer;->setVisibility(I)V

    .line 1960
    invoke-virtual/range {p2 .. p2}, Lcom/peel/data/g;->d()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 2230
    :pswitch_0
    sget-object v1, Lcom/peel/ui/gt;->i:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "Error: no layout information for device type: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual/range {p2 .. p2}, Lcom/peel/data/g;->d()I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v1, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    move v9, v3

    move v1, v4

    move v3, v5

    .line 2233
    :goto_0
    const-string/jumbo v4, "Play"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Lcom/peel/data/g;->a(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1f

    .line 2234
    const/4 v4, 0x1

    .line 2238
    :goto_1
    invoke-interface {v12}, Ljava/util/List;->size()I

    move-result v5

    if-nez v5, :cond_1e

    const-string/jumbo v5, "Red"

    move-object/from16 v0, p2

    invoke-virtual {v0, v5}, Lcom/peel/data/g;->a(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1e

    .line 2239
    const-string/jumbo v1, "Yellow"

    invoke-interface {v12, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2240
    const-string/jumbo v1, "Blue"

    invoke-interface {v12, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2241
    const-string/jumbo v1, "Red"

    invoke-interface {v12, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2242
    const-string/jumbo v1, "Green"

    invoke-interface {v12, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2243
    const/4 v1, 0x0

    move v10, v1

    .line 2246
    :goto_2
    iget-object v1, p0, Lcom/peel/ui/gt;->aO:Landroid/view/LayoutInflater;

    sget v5, Lcom/peel/ui/fq;->controlpad_device_layout_container:I

    const/4 v6, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v1, v5, v0, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v13

    .line 2247
    sget v1, Lcom/peel/ui/fp;->controlpad_device_container:I

    invoke-virtual {v13, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    move-object v8, v1

    check-cast v8, Landroid/widget/LinearLayout;

    .line 2262
    new-instance v5, Landroid/widget/LinearLayout;

    invoke-virtual {p0}, Lcom/peel/ui/gt;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    invoke-direct {v5, v1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 2263
    const/4 v1, 0x0

    invoke-virtual {v5, v1}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 2264
    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v6, -0x1

    const/4 v14, -0x2

    invoke-direct {v1, v6, v14}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 2266
    invoke-virtual {p0}, Lcom/peel/ui/gt;->n()Landroid/content/res/Resources;

    move-result-object v6

    sget v14, Lcom/peel/ui/fn;->controlpad_side_margin:I

    invoke-virtual {v6, v14}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    iput v6, v1, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    .line 2267
    invoke-virtual {p0}, Lcom/peel/ui/gt;->n()Landroid/content/res/Resources;

    move-result-object v6

    sget v14, Lcom/peel/ui/fn;->controlpad_side_margin:I

    invoke-virtual {v6, v14}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    iput v6, v1, Landroid/widget/LinearLayout$LayoutParams;->rightMargin:I

    .line 2268
    const/16 v6, 0x11

    invoke-static {v6}, Lcom/peel/util/dw;->a(I)I

    move-result v6

    iput v6, v1, Landroid/widget/LinearLayout$LayoutParams;->topMargin:I

    .line 2269
    const/16 v6, 0x11

    invoke-static {v6}, Lcom/peel/util/dw;->a(I)I

    move-result v6

    iput v6, v1, Landroid/widget/LinearLayout$LayoutParams;->bottomMargin:I

    .line 2270
    invoke-virtual {v5, v1}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2272
    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_1
    :goto_3
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_f

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 2273
    move-object/from16 v0, p2

    invoke-virtual {v0, v1}, Lcom/peel/data/g;->a(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 2274
    invoke-virtual {p0}, Lcom/peel/ui/gt;->m()Landroid/support/v4/app/ae;

    move-result-object v7

    iget-object v14, p0, Lcom/peel/ui/gt;->bM:Landroid/view/View$OnClickListener;

    invoke-static {v7, v1, v14}, Lcom/peel/util/dw;->b(Landroid/content/Context;Ljava/lang/String;Landroid/view/View$OnClickListener;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v5, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    goto :goto_3

    .line 1963
    :pswitch_1
    iget-object v1, p0, Lcom/peel/ui/gt;->aU:Lcom/peel/widget/SlidingDrawer;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Lcom/peel/widget/SlidingDrawer;->setVisibility(I)V

    .line 2422
    :goto_4
    return-void

    .line 1966
    :pswitch_2
    const-string/jumbo v1, "Menu"

    invoke-interface {v7, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1967
    const-string/jumbo v1, "Back"

    invoke-interface {v7, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1969
    const-string/jumbo v1, "Info"

    invoke-interface {v11, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1970
    const-string/jumbo v1, "Exit"

    invoke-interface {v11, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move v9, v3

    move v1, v4

    move v3, v5

    .line 1971
    goto/16 :goto_0

    .line 1974
    :pswitch_3
    const-string/jumbo v1, "Menu"

    invoke-interface {v7, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1975
    const-string/jumbo v1, "Back"

    invoke-interface {v7, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1977
    const-string/jumbo v1, "PopMenu"

    invoke-interface {v11, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1978
    const-string/jumbo v1, "Exit"

    invoke-interface {v11, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1981
    sget-object v1, Lcom/peel/ui/gt;->i:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "\n\n device: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual/range {p2 .. p2}, Lcom/peel/data/g;->f()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string/jumbo v9, " -- "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual/range {p2 .. p2}, Lcom/peel/data/g;->d()I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string/jumbo v9, "\n"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v1, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1982
    invoke-virtual/range {p2 .. p2}, Lcom/peel/data/g;->a()Ljava/util/Map;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_5
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 1983
    sget-object v9, Lcom/peel/ui/gt;->i:Ljava/lang/String;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v13, "*** cmd: "

    invoke-virtual {v10, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v9, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_5

    .line 1987
    :cond_2
    const-string/jumbo v1, "Red"

    move-object/from16 v0, p2

    invoke-virtual {v0, v1}, Lcom/peel/data/g;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1988
    const-string/jumbo v1, "Red"

    invoke-interface {v12, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1989
    const-string/jumbo v1, "Green"

    invoke-interface {v12, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1990
    const-string/jumbo v1, "Yellow"

    invoke-interface {v12, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1991
    const-string/jumbo v1, "Blue"

    invoke-interface {v12, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1995
    :cond_3
    const-string/jumbo v1, "1"

    move-object/from16 v0, p2

    invoke-virtual {v0, v1}, Lcom/peel/data/g;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1996
    const/4 v1, 0x1

    move v9, v1

    move v3, v5

    move v1, v4

    goto/16 :goto_0

    .line 2001
    :pswitch_4
    const-string/jumbo v1, "roku"

    invoke-virtual/range {p2 .. p2}, Lcom/peel/data/g;->f()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v1, v8}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 2002
    const-string/jumbo v1, "Back"

    invoke-interface {v7, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2003
    const-string/jumbo v1, "Home"

    invoke-interface {v7, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2005
    const-string/jumbo v1, "InstantReplay"

    invoke-interface {v11, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2006
    const-string/jumbo v1, "Options"

    invoke-interface {v11, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2007
    const/4 v1, 0x1

    move v9, v3

    move v3, v1

    move v1, v4

    goto/16 :goto_0

    .line 2009
    :cond_4
    const-string/jumbo v1, "Menu"

    invoke-interface {v11, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2010
    const-string/jumbo v1, "Play"

    invoke-interface {v11, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move v9, v3

    move v1, v4

    move v3, v5

    .line 2012
    goto/16 :goto_0

    .line 2014
    :pswitch_5
    const/4 v3, 0x1

    .line 2016
    const-string/jumbo v1, "sony"

    invoke-virtual/range {p2 .. p2}, Lcom/peel/data/g;->f()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v1, v8}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 2017
    const-string/jumbo v1, "Home"

    invoke-interface {v7, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2018
    const-string/jumbo v1, "Guide"

    invoke-interface {v7, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2019
    const-string/jumbo v1, "Options"

    invoke-interface {v7, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2020
    const-string/jumbo v1, "Menu"

    invoke-interface {v7, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2022
    const-string/jumbo v1, "Input"

    invoke-interface {v11, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2023
    const-string/jumbo v1, "Fav"

    invoke-interface {v11, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2024
    const-string/jumbo v1, "Return"

    invoke-interface {v11, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2025
    const-string/jumbo v1, "Exit"

    invoke-interface {v11, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move v9, v3

    move v1, v4

    move v3, v5

    goto/16 :goto_0

    .line 2026
    :cond_5
    const-string/jumbo v1, "sharp"

    invoke-virtual/range {p2 .. p2}, Lcom/peel/data/g;->f()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v1, v8}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 2027
    const-string/jumbo v1, "Menu"

    invoke-interface {v7, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2028
    const-string/jumbo v1, "Info"

    invoke-interface {v7, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2029
    const-string/jumbo v1, "Input"

    invoke-interface {v7, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2031
    const-string/jumbo v1, "AvMode"

    invoke-interface {v11, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2032
    const-string/jumbo v1, "FLASH BACK"

    invoke-interface {v11, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2033
    const-string/jumbo v1, "Return"

    invoke-interface {v11, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2034
    const-string/jumbo v1, "Exit"

    invoke-interface {v11, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move v9, v3

    move v1, v4

    move v3, v5

    goto/16 :goto_0

    .line 2035
    :cond_6
    const-string/jumbo v1, "samsung"

    invoke-virtual/range {p2 .. p2}, Lcom/peel/data/g;->f()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v1, v8}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 2036
    const/4 v1, 0x1

    .line 2038
    const-string/jumbo v2, "Menu"

    invoke-interface {v7, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2039
    const-string/jumbo v2, "Smart_Hub"

    invoke-interface {v7, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2040
    const-string/jumbo v2, "Input^Input"

    invoke-interface {v7, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2041
    const-string/jumbo v2, "Info"

    invoke-interface {v7, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2043
    const-string/jumbo v2, "Back"

    invoke-interface {v11, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2044
    const-string/jumbo v2, "Exit"

    invoke-interface {v11, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move v2, v1

    move v9, v3

    move v1, v4

    move v3, v5

    goto/16 :goto_0

    .line 2046
    :cond_7
    const-string/jumbo v1, "Menu"

    invoke-interface {v7, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2047
    const-string/jumbo v1, "Guide"

    invoke-interface {v7, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2048
    const-string/jumbo v1, "Info"

    invoke-interface {v7, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2049
    const-string/jumbo v1, "Input"

    invoke-interface {v7, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2051
    const-string/jumbo v1, "Back"

    invoke-interface {v11, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2052
    const-string/jumbo v1, "Exit"

    invoke-interface {v11, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move v9, v3

    move v1, v4

    move v3, v5

    .line 2054
    goto/16 :goto_0

    .line 2056
    :pswitch_6
    const/4 v1, 0x1

    .line 2058
    const-string/jumbo v3, "Menu"

    invoke-interface {v7, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2059
    const-string/jumbo v3, "Info"

    invoke-interface {v7, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2060
    const-string/jumbo v3, "Input"

    invoke-interface {v7, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2062
    const-string/jumbo v3, "Back"

    invoke-interface {v11, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2063
    const-string/jumbo v3, "Exit"

    invoke-interface {v11, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move v9, v1

    move v3, v5

    move v1, v4

    .line 2065
    goto/16 :goto_0

    .line 2068
    :pswitch_7
    const/4 v1, 0x1

    .line 2070
    invoke-virtual/range {p2 .. p2}, Lcom/peel/data/g;->f()Ljava/lang/String;

    move-result-object v3

    .line 2077
    const-string/jumbo v8, "Directv"

    invoke-virtual {v8, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_8

    .line 2078
    const-string/jumbo v3, "Menu"

    invoke-interface {v7, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2079
    const-string/jumbo v3, "Guide"

    invoke-interface {v7, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2080
    const-string/jumbo v3, "Info"

    invoke-interface {v7, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2081
    const-string/jumbo v3, "DVR^List"

    invoke-interface {v7, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2083
    const-string/jumbo v3, "Skip_Back"

    invoke-interface {v11, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2084
    const-string/jumbo v3, "Skip_Forward"

    invoke-interface {v11, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2085
    const-string/jumbo v3, "Back"

    invoke-interface {v11, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2086
    const-string/jumbo v3, "Exit"

    invoke-interface {v11, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2089
    const-string/jumbo v3, "Red"

    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Lcom/peel/data/g;->a(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_20

    .line 2090
    const-string/jumbo v3, "Blue"

    invoke-interface {v12, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2091
    const-string/jumbo v3, "Yellow"

    invoke-interface {v12, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2092
    const-string/jumbo v3, "Green"

    invoke-interface {v12, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2093
    const-string/jumbo v3, "Red"

    invoke-interface {v12, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move v9, v1

    move v3, v5

    move v1, v4

    goto/16 :goto_0

    .line 2095
    :cond_8
    const-string/jumbo v8, "AT&T Uverse"

    invoke-virtual {v8, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_9

    .line 2096
    const-string/jumbo v3, "Menu"

    invoke-interface {v7, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2097
    const-string/jumbo v3, "Guide"

    invoke-interface {v7, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2098
    const-string/jumbo v3, "OnDemand"

    invoke-interface {v7, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2099
    const-string/jumbo v3, "DVR"

    invoke-interface {v7, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2101
    const-string/jumbo v3, "Skip_Back"

    invoke-interface {v11, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2102
    const-string/jumbo v3, "Skip_Forward"

    invoke-interface {v11, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2103
    const-string/jumbo v3, "LiveTV"

    invoke-interface {v11, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2104
    const-string/jumbo v3, "Exit"

    invoke-interface {v11, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2107
    const-string/jumbo v3, "Red"

    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Lcom/peel/data/g;->a(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_20

    .line 2108
    const-string/jumbo v3, "Yellow"

    invoke-interface {v12, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2109
    const-string/jumbo v3, "Green"

    invoke-interface {v12, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2110
    const-string/jumbo v3, "Red"

    invoke-interface {v12, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2111
    const/4 v3, 0x1

    move v9, v1

    move v1, v3

    move v3, v5

    goto/16 :goto_0

    .line 2113
    :cond_9
    const-string/jumbo v8, "Cox"

    invoke-virtual {v8, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_a

    .line 2114
    const-string/jumbo v3, "Menu"

    invoke-interface {v7, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2115
    const-string/jumbo v3, "Guide"

    invoke-interface {v7, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2116
    const-string/jumbo v3, "Info"

    invoke-interface {v7, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2117
    const-string/jumbo v3, "OnDemand"

    invoke-interface {v7, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2119
    const-string/jumbo v3, "LiveTV"

    invoke-interface {v11, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2120
    const-string/jumbo v3, "Skip_Back"

    invoke-interface {v11, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2121
    const-string/jumbo v3, "Last"

    invoke-interface {v11, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2122
    const-string/jumbo v3, "Exit"

    invoke-interface {v11, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2125
    const-string/jumbo v3, "Red"

    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Lcom/peel/data/g;->a(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_20

    .line 2126
    const-string/jumbo v3, "Yellow"

    invoke-interface {v12, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2127
    const-string/jumbo v3, "Blue"

    invoke-interface {v12, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2128
    const-string/jumbo v3, "Red"

    invoke-interface {v12, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2129
    const/4 v3, 0x1

    move v9, v1

    move v1, v3

    move v3, v5

    goto/16 :goto_0

    .line 2131
    :cond_a
    const-string/jumbo v8, "Dish"

    invoke-virtual {v8, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_b

    .line 2132
    const-string/jumbo v3, "Menu"

    invoke-interface {v7, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2133
    const-string/jumbo v3, "Info"

    invoke-interface {v7, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2134
    const-string/jumbo v3, "OnDemand"

    invoke-interface {v7, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2135
    const-string/jumbo v3, "DVR"

    invoke-interface {v7, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2137
    const-string/jumbo v3, "Skip_Back"

    invoke-interface {v11, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2138
    const-string/jumbo v3, "Skip_Forward"

    invoke-interface {v11, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2139
    const-string/jumbo v3, "Back"

    invoke-interface {v11, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2140
    const-string/jumbo v3, "Exit"

    invoke-interface {v11, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2143
    const-string/jumbo v3, "Red"

    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Lcom/peel/data/g;->a(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_20

    .line 2144
    const-string/jumbo v3, "Red"

    invoke-interface {v12, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2145
    const-string/jumbo v3, "Green"

    invoke-interface {v12, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2146
    const-string/jumbo v3, "Yellow"

    invoke-interface {v12, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2147
    const-string/jumbo v3, "Blue"

    invoke-interface {v12, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2148
    const/4 v3, 0x0

    move v9, v1

    move v1, v3

    move v3, v5

    goto/16 :goto_0

    .line 2150
    :cond_b
    const-string/jumbo v8, "TimeWarner"

    invoke-virtual {v8, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_c

    .line 2151
    const-string/jumbo v3, "Menu"

    invoke-interface {v7, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2152
    const-string/jumbo v3, "Info"

    invoke-interface {v7, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2153
    const-string/jumbo v3, "OnDemand"

    invoke-interface {v7, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2154
    const-string/jumbo v3, "Guide"

    invoke-interface {v7, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2156
    const-string/jumbo v3, "list"

    invoke-interface {v11, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2157
    const-string/jumbo v3, "Last"

    invoke-interface {v11, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2158
    const-string/jumbo v3, "Back"

    invoke-interface {v11, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2159
    const-string/jumbo v3, "Exit"

    invoke-interface {v11, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2162
    const-string/jumbo v3, "Red"

    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Lcom/peel/data/g;->a(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_20

    .line 2163
    const-string/jumbo v3, "Yellow"

    invoke-interface {v12, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2164
    const-string/jumbo v3, "Blue"

    invoke-interface {v12, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2165
    const-string/jumbo v3, "Red"

    invoke-interface {v12, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2166
    const-string/jumbo v3, "Green"

    invoke-interface {v12, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2167
    const/4 v3, 0x1

    move v9, v1

    move v1, v3

    move v3, v5

    goto/16 :goto_0

    .line 2169
    :cond_c
    const-string/jumbo v8, "Fios TV"

    invoke-virtual {v8, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_d

    .line 2170
    const-string/jumbo v3, "Menu"

    invoke-interface {v7, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2171
    const-string/jumbo v3, "Guide"

    invoke-interface {v7, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2172
    const-string/jumbo v3, "OnDemand"

    invoke-interface {v7, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2173
    const-string/jumbo v3, "DVR"

    invoke-interface {v7, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2175
    const-string/jumbo v3, "Info"

    invoke-interface {v11, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2176
    const-string/jumbo v3, "Last"

    invoke-interface {v11, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2177
    const-string/jumbo v3, "Back"

    invoke-interface {v11, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2178
    const-string/jumbo v3, "Exit"

    invoke-interface {v11, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2181
    const-string/jumbo v3, "Red"

    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Lcom/peel/data/g;->a(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_20

    .line 2182
    const-string/jumbo v3, "Yellow"

    invoke-interface {v12, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2183
    const-string/jumbo v3, "Blue"

    invoke-interface {v12, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2184
    const-string/jumbo v3, "Red"

    invoke-interface {v12, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2185
    const-string/jumbo v3, "Green"

    invoke-interface {v12, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2186
    const/4 v3, 0x1

    move v9, v1

    move v1, v3

    move v3, v5

    goto/16 :goto_0

    .line 2188
    :cond_d
    const-string/jumbo v8, "TiVo"

    invoke-virtual {v8, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_e

    .line 2189
    const-string/jumbo v3, "TIVO"

    invoke-interface {v7, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2190
    const-string/jumbo v3, "Info"

    invoke-interface {v7, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2191
    const-string/jumbo v3, "LiveTV"

    invoke-interface {v7, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2192
    const-string/jumbo v3, "slow"

    invoke-interface {v7, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2194
    const-string/jumbo v3, "ThumbsUp"

    invoke-interface {v11, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2195
    const-string/jumbo v3, "ThumbsDown"

    invoke-interface {v11, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2196
    const-string/jumbo v3, "Skip_Back"

    invoke-interface {v11, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2197
    const-string/jumbo v3, "Back"

    invoke-interface {v11, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2200
    const-string/jumbo v3, "Red"

    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Lcom/peel/data/g;->a(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_20

    .line 2201
    const-string/jumbo v3, "Yellow"

    invoke-interface {v12, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2202
    const-string/jumbo v3, "Blue"

    invoke-interface {v12, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2203
    const-string/jumbo v3, "Red"

    invoke-interface {v12, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2204
    const-string/jumbo v3, "Green"

    invoke-interface {v12, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2205
    const/4 v3, 0x1

    move v9, v1

    move v1, v3

    move v3, v5

    goto/16 :goto_0

    .line 2208
    :cond_e
    const-string/jumbo v3, "Menu"

    invoke-interface {v7, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2209
    const-string/jumbo v3, "Guide"

    invoke-interface {v7, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2210
    const-string/jumbo v3, "Info"

    invoke-interface {v7, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2211
    const-string/jumbo v3, "DVR"

    invoke-interface {v7, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2213
    const-string/jumbo v3, "Skip_Back"

    invoke-interface {v11, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2214
    const-string/jumbo v3, "Skip_Forward"

    invoke-interface {v11, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2215
    const-string/jumbo v3, "Back"

    invoke-interface {v11, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2216
    const-string/jumbo v3, "Exit"

    invoke-interface {v11, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2219
    const-string/jumbo v3, "Red"

    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Lcom/peel/data/g;->a(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_20

    .line 2220
    const-string/jumbo v3, "Yellow"

    invoke-interface {v12, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2221
    const-string/jumbo v3, "Blue"

    invoke-interface {v12, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2222
    const-string/jumbo v3, "Red"

    invoke-interface {v12, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2223
    const-string/jumbo v3, "Green"

    invoke-interface {v12, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2224
    const/4 v3, 0x1

    move v9, v1

    move v1, v3

    move v3, v5

    goto/16 :goto_0

    .line 2276
    :cond_f
    invoke-virtual {v8, v5}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 2279
    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v5, -0x1

    const/4 v6, -0x2

    invoke-direct {v1, v5, v6}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 2280
    const/16 v5, 0xd3

    invoke-static {v5}, Lcom/peel/util/dw;->a(I)I

    move-result v5

    iput v5, v1, Landroid/widget/LinearLayout$LayoutParams;->width:I

    .line 2281
    const/16 v5, 0xd3

    invoke-static {v5}, Lcom/peel/util/dw;->a(I)I

    move-result v5

    iput v5, v1, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 2282
    const/4 v5, 0x1

    iput v5, v1, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    .line 2285
    iget-boolean v5, p0, Lcom/peel/ui/gt;->bH:Z

    if-eqz v5, :cond_11

    .line 2287
    invoke-virtual {p0}, Lcom/peel/ui/gt;->m()Landroid/support/v4/app/ae;

    move-result-object v5

    const v6, 0x186a9

    iget-object v7, p0, Lcom/peel/ui/gt;->bM:Landroid/view/View$OnClickListener;

    .line 2286
    invoke-static {v5, v6, v7, v1}, Lcom/peel/util/dw;->b(Landroid/content/Context;ILandroid/view/View$OnClickListener;Landroid/view/ViewGroup$LayoutParams;)Landroid/view/View;

    move-result-object v1

    .line 2295
    :goto_6
    invoke-virtual {v8, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 2298
    new-instance v5, Landroid/widget/LinearLayout;

    invoke-virtual {p0}, Lcom/peel/ui/gt;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    invoke-direct {v5, v1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 2299
    const/4 v1, 0x0

    invoke-virtual {v5, v1}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 2300
    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v6, -0x1

    const/4 v7, -0x2

    invoke-direct {v1, v6, v7}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 2302
    invoke-virtual {p0}, Lcom/peel/ui/gt;->n()Landroid/content/res/Resources;

    move-result-object v6

    sget v7, Lcom/peel/ui/fn;->controlpad_side_margin:I

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    iput v6, v1, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    .line 2303
    invoke-virtual {p0}, Lcom/peel/ui/gt;->n()Landroid/content/res/Resources;

    move-result-object v6

    sget v7, Lcom/peel/ui/fn;->controlpad_side_margin:I

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    iput v6, v1, Landroid/widget/LinearLayout$LayoutParams;->rightMargin:I

    .line 2304
    const/16 v6, 0x11

    invoke-static {v6}, Lcom/peel/util/dw;->a(I)I

    move-result v6

    iput v6, v1, Landroid/widget/LinearLayout$LayoutParams;->topMargin:I

    .line 2305
    invoke-virtual {v5, v1}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2307
    invoke-interface {v11}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_10
    :goto_7
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_12

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 2308
    move-object/from16 v0, p2

    invoke-virtual {v0, v1}, Lcom/peel/data/g;->a(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_10

    .line 2309
    invoke-virtual {p0}, Lcom/peel/ui/gt;->m()Landroid/support/v4/app/ae;

    move-result-object v7

    iget-object v11, p0, Lcom/peel/ui/gt;->bM:Landroid/view/View$OnClickListener;

    invoke-static {v7, v1, v11}, Lcom/peel/util/dw;->b(Landroid/content/Context;Ljava/lang/String;Landroid/view/View$OnClickListener;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v5, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    goto :goto_7

    .line 2291
    :cond_11
    invoke-virtual {p0}, Lcom/peel/ui/gt;->m()Landroid/support/v4/app/ae;

    move-result-object v5

    const v6, 0x186a9

    iget-object v7, p0, Lcom/peel/ui/gt;->bM:Landroid/view/View$OnClickListener;

    .line 2290
    invoke-static {v5, v6, v7, v1}, Lcom/peel/util/dw;->a(Landroid/content/Context;ILandroid/view/View$OnClickListener;Landroid/view/ViewGroup$LayoutParams;)Landroid/view/View;

    move-result-object v1

    goto :goto_6

    .line 2311
    :cond_12
    invoke-virtual {v8, v5}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 2314
    if-eqz v2, :cond_13

    .line 2315
    new-instance v1, Landroid/widget/LinearLayout;

    invoke-virtual {p0}, Lcom/peel/ui/gt;->m()Landroid/support/v4/app/ae;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 2316
    const/4 v2, 0x0

    invoke-virtual {v5, v2}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 2317
    new-instance v2, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v5, -0x1

    const/4 v6, -0x2

    invoke-direct {v2, v5, v6}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 2318
    invoke-virtual {p0}, Lcom/peel/ui/gt;->n()Landroid/content/res/Resources;

    move-result-object v5

    sget v6, Lcom/peel/ui/fn;->controlpad_side_margin:I

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    iput v5, v2, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    .line 2319
    invoke-virtual {p0}, Lcom/peel/ui/gt;->n()Landroid/content/res/Resources;

    move-result-object v5

    sget v6, Lcom/peel/ui/fn;->controlpad_side_margin:I

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    iput v5, v2, Landroid/widget/LinearLayout$LayoutParams;->rightMargin:I

    .line 2320
    const/16 v5, 0x11

    invoke-static {v5}, Lcom/peel/util/dw;->a(I)I

    move-result v5

    iput v5, v2, Landroid/widget/LinearLayout$LayoutParams;->topMargin:I

    .line 2321
    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2323
    invoke-virtual {p0}, Lcom/peel/ui/gt;->n()Landroid/content/res/Resources;

    move-result-object v2

    sget v5, Lcom/peel/ui/fn;->controlpad_side_margin:I

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    const/4 v5, 0x0

    .line 2324
    invoke-virtual {p0}, Lcom/peel/ui/gt;->n()Landroid/content/res/Resources;

    move-result-object v6

    sget v7, Lcom/peel/ui/fn;->controlpad_side_margin:I

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    const/4 v7, 0x0

    .line 2323
    invoke-virtual {v1, v2, v5, v6, v7}, Landroid/widget/LinearLayout;->setPadding(IIII)V

    .line 2326
    invoke-virtual {p0}, Lcom/peel/ui/gt;->m()Landroid/support/v4/app/ae;

    move-result-object v2

    const-string/jumbo v5, "CHList"

    iget-object v6, p0, Lcom/peel/ui/gt;->bM:Landroid/view/View$OnClickListener;

    invoke-static {v2, v5, v6}, Lcom/peel/util/dw;->a(Landroid/content/Context;Ljava/lang/String;Landroid/view/View$OnClickListener;)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 2329
    new-instance v2, Landroid/view/View;

    invoke-virtual {p0}, Lcom/peel/ui/gt;->m()Landroid/support/v4/app/ae;

    move-result-object v5

    invoke-direct {v2, v5}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 2330
    new-instance v5, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v6, -0x2

    const/4 v7, -0x2

    const/high16 v11, 0x3f800000    # 1.0f

    invoke-direct {v5, v6, v7, v11}, Landroid/widget/LinearLayout$LayoutParams;-><init>(IIF)V

    .line 2331
    invoke-virtual {v2, v5}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2333
    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 2335
    invoke-virtual {p0}, Lcom/peel/ui/gt;->m()Landroid/support/v4/app/ae;

    move-result-object v2

    const-string/jumbo v5, "Tools"

    iget-object v6, p0, Lcom/peel/ui/gt;->bM:Landroid/view/View$OnClickListener;

    invoke-static {v2, v5, v6}, Lcom/peel/util/dw;->a(Landroid/content/Context;Ljava/lang/String;Landroid/view/View$OnClickListener;)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 2337
    invoke-virtual {v8, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 2341
    :cond_13
    if-eqz v3, :cond_14

    .line 2342
    invoke-virtual {p0}, Lcom/peel/ui/gt;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    iget-object v2, p0, Lcom/peel/ui/gt;->bM:Landroid/view/View$OnClickListener;

    invoke-static {v1, v2}, Lcom/peel/util/dw;->a(Landroid/content/Context;Landroid/view/View$OnClickListener;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v8, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 2346
    :cond_14
    if-eqz v4, :cond_15

    .line 2347
    invoke-virtual {p0}, Lcom/peel/ui/gt;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    const/4 v2, 0x1

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/16 v5, 0x1a

    const/16 v6, 0x10

    iget-object v7, p0, Lcom/peel/ui/gt;->bM:Landroid/view/View$OnClickListener;

    invoke-static/range {v1 .. v7}, Lcom/peel/util/dw;->a(Landroid/content/Context;IIIIILandroid/view/View$OnClickListener;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v8, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 2351
    :cond_15
    invoke-interface {v12}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_16

    .line 2352
    invoke-virtual {p0}, Lcom/peel/ui/gt;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    iget-object v2, p0, Lcom/peel/ui/gt;->bM:Landroid/view/View$OnClickListener;

    invoke-static {v1, v12, v10, v2}, Lcom/peel/util/dw;->a(Landroid/content/Context;Ljava/util/List;ZLandroid/view/View$OnClickListener;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v8, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 2356
    :cond_16
    if-eqz v9, :cond_1d

    .line 2357
    iget-object v1, p0, Lcom/peel/ui/gt;->be:Landroid/content/SharedPreferences;

    const-string/jumbo v2, "country"

    const-string/jumbo v3, ""

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "Japan"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_17

    iget-object v1, p0, Lcom/peel/ui/gt;->be:Landroid/content/SharedPreferences;

    const-string/jumbo v2, "country"

    const-string/jumbo v3, ""

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "jp"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_19

    :cond_17
    const/4 v2, 0x1

    .line 2358
    :goto_8
    const-string/jumbo v1, "---"

    move-object/from16 v0, p2

    invoke-virtual {v0, v1}, Lcom/peel/data/g;->a(Ljava/lang/String;)Z

    move-result v3

    .line 2359
    const-string/jumbo v1, "."

    move-object/from16 v0, p2

    invoke-virtual {v0, v1}, Lcom/peel/data/g;->a(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_18

    const-string/jumbo v1, "Dot_DASh"

    move-object/from16 v0, p2

    invoke-virtual {v0, v1}, Lcom/peel/data/g;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1a

    :cond_18
    const/4 v4, 0x1

    .line 2360
    :goto_9
    const-string/jumbo v1, "Enter"

    move-object/from16 v0, p2

    invoke-virtual {v0, v1}, Lcom/peel/data/g;->a(Ljava/lang/String;)Z

    move-result v5

    .line 2362
    iget-boolean v1, p0, Lcom/peel/ui/gt;->bH:Z

    if-eqz v1, :cond_1b

    .line 2364
    invoke-virtual {p0}, Lcom/peel/ui/gt;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    const/4 v6, 0x0

    iget-object v7, p0, Lcom/peel/ui/gt;->bM:Landroid/view/View$OnClickListener;

    .line 2363
    invoke-static/range {v1 .. v7}, Lcom/peel/util/dw;->b(Landroid/content/Context;ZZZZILandroid/view/View$OnClickListener;)Ljava/util/List;

    move-result-object v1

    .line 2372
    :goto_a
    new-instance v2, Landroid/widget/LinearLayout;

    invoke-virtual {p0}, Lcom/peel/ui/gt;->m()Landroid/support/v4/app/ae;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 2373
    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 2374
    new-instance v3, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v4, -0x2

    const/4 v5, -0x2

    invoke-direct {v3, v4, v5}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 2375
    const/16 v4, 0x1a

    invoke-static {v4}, Lcom/peel/util/dw;->a(I)I

    move-result v4

    iput v4, v3, Landroid/widget/LinearLayout$LayoutParams;->topMargin:I

    .line 2376
    const/16 v4, 0x1a

    invoke-static {v4}, Lcom/peel/util/dw;->a(I)I

    move-result v4

    iput v4, v3, Landroid/widget/LinearLayout$LayoutParams;->bottomMargin:I

    .line 2377
    const/4 v4, 0x1

    iput v4, v3, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    .line 2378
    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2380
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_b
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1c

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    invoke-virtual {v2, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    goto :goto_b

    .line 2357
    :cond_19
    const/4 v2, 0x0

    goto :goto_8

    .line 2359
    :cond_1a
    const/4 v4, 0x0

    goto :goto_9

    .line 2368
    :cond_1b
    invoke-virtual {p0}, Lcom/peel/ui/gt;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    const/4 v6, 0x0

    iget-object v7, p0, Lcom/peel/ui/gt;->bM:Landroid/view/View$OnClickListener;

    .line 2367
    invoke-static/range {v1 .. v7}, Lcom/peel/util/dw;->a(Landroid/content/Context;ZZZZILandroid/view/View$OnClickListener;)Ljava/util/List;

    move-result-object v1

    goto :goto_a

    .line 2382
    :cond_1c
    invoke-virtual {v8, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 2386
    :cond_1d
    new-instance v1, Landroid/view/View;

    invoke-virtual {p0}, Lcom/peel/ui/gt;->m()Landroid/support/v4/app/ae;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 2387
    new-instance v2, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v3, -0x1

    const/4 v4, -0x2

    const/high16 v5, 0x3f800000    # 1.0f

    invoke-direct {v2, v3, v4, v5}, Landroid/widget/LinearLayout$LayoutParams;-><init>(IIF)V

    .line 2388
    const/4 v3, 0x1

    iput v3, v2, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 2389
    invoke-virtual {v1, v2}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2391
    invoke-virtual {v8, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 2393
    new-instance v1, Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/peel/ui/gt;->m()Landroid/support/v4/app/ae;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 2394
    new-instance v2, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v3, -0x2

    const/4 v4, -0x2

    invoke-direct {v2, v3, v4}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 2395
    const v3, 0x800055

    iput v3, v2, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    .line 2396
    const/4 v3, 0x0

    const/4 v4, 0x0

    const/16 v5, 0x12

    invoke-static {v5}, Lcom/peel/util/dw;->a(I)I

    move-result v5

    const/16 v6, 0xa

    invoke-static {v6}, Lcom/peel/util/dw;->a(I)I

    move-result v6

    invoke-virtual {v2, v3, v4, v5, v6}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    .line 2398
    const/4 v3, 0x1

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setClickable(Z)V

    .line 2399
    sget v3, Lcom/peel/ui/ft;->logo_str:I

    invoke-virtual {p0, v3}, Lcom/peel/ui/gt;->a(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2400
    invoke-virtual {p0}, Lcom/peel/ui/gt;->m()Landroid/support/v4/app/ae;

    move-result-object v3

    sget v4, Lcom/peel/ui/fu;->Logo_Font:I

    invoke-virtual {v1, v3, v4}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 2401
    const/4 v3, 0x0

    const/4 v4, 0x0

    sget v5, Lcom/peel/ui/fo;->peel_icon_small:I

    const/4 v6, 0x0

    invoke-virtual {v1, v3, v4, v5, v6}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    .line 2402
    const/4 v3, 0x5

    invoke-static {v3}, Lcom/peel/util/dw;->a(I)I

    move-result v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setCompoundDrawablePadding(I)V

    .line 2404
    const/16 v3, 0x10

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setGravity(I)V

    .line 2405
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2407
    new-instance v2, Lcom/peel/ui/ij;

    invoke-direct {v2, p0}, Lcom/peel/ui/ij;-><init>(Lcom/peel/ui/gt;)V

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2414
    invoke-virtual {v8, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 2418
    sget-object v1, Lcom/peel/ui/gt;->i:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "\n\ndevice: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual/range {p2 .. p2}, Lcom/peel/data/g;->f()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " -- "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual/range {p2 .. p2}, Lcom/peel/data/g;->d()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2419
    sget-object v1, Lcom/peel/ui/gt;->i:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "\n\nflipper child count before adding: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual/range {p1 .. p1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2421
    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    goto/16 :goto_4

    :cond_1e
    move v10, v1

    goto/16 :goto_2

    :cond_1f
    move v4, v6

    goto/16 :goto_1

    :cond_20
    move v9, v1

    move v3, v5

    move v1, v4

    goto/16 :goto_0

    .line 1960
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_5
        :pswitch_7
        :pswitch_3
        :pswitch_3
        :pswitch_2
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_6
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_7
    .end packed-switch
.end method

.method private a(Landroid/view/ViewGroup;Lcom/peel/util/bw;ZZZZZZZ)V
    .locals 36

    .prologue
    .line 1452
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/peel/ui/gt;->aO:Landroid/view/LayoutInflater;

    sget v3, Lcom/peel/ui/fq;->controlpad_activity_layout_container:I

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v2, v3, v0, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    move-object/from16 v35, v2

    check-cast v35, Landroid/widget/RelativeLayout;

    .line 1454
    invoke-virtual/range {p0 .. p0}, Lcom/peel/ui/gt;->m()Landroid/support/v4/app/ae;

    move-result-object v2

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/peel/ui/gt;->bH:Z

    invoke-static {v2, v3}, Lcom/peel/util/dw;->a(Landroid/content/Context;Z)V

    .line 1456
    invoke-direct/range {p0 .. p0}, Lcom/peel/ui/gt;->aq()V

    .line 1459
    sget-object v2, Lcom/peel/c/a;->b:Lcom/peel/c/e;

    invoke-static {v2}, Lcom/peel/c/a;->a(Lcom/peel/c/e;)Ljava/lang/Object;

    move-result-object v2

    sget-object v3, Lcom/peel/c/b;->c:Lcom/peel/c/b;

    if-ne v2, v3, :cond_e

    .line 1460
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/peel/ui/gt;->am:Ljava/util/List;

    if-eqz v2, :cond_1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/peel/ui/gt;->am:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_1

    .line 1461
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/peel/ui/gt;->aO:Landroid/view/LayoutInflater;

    sget v3, Lcom/peel/ui/fq;->three_button_row:I

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v9

    .line 1462
    new-instance v2, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v3, -0x1

    const/4 v4, -0x2

    invoke-direct {v2, v3, v4}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1463
    const/16 v3, 0x50

    invoke-static {v3}, Lcom/peel/util/dw;->a(I)I

    move-result v3

    iput v3, v2, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 1464
    const/16 v3, 0xe

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 1465
    invoke-virtual/range {p0 .. p0}, Lcom/peel/ui/gt;->n()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/peel/ui/fn;->controlpad_side_margin:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    iput v3, v2, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    .line 1466
    invoke-virtual/range {p0 .. p0}, Lcom/peel/ui/gt;->n()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/peel/ui/fn;->controlpad_side_margin:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    iput v3, v2, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 1468
    invoke-virtual {v9, v2}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1470
    sget v2, Lcom/peel/ui/fp;->btn1:I

    invoke-virtual {v9, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageButton;

    .line 1471
    sget v3, Lcom/peel/ui/fp;->btn2:I

    invoke-virtual {v9, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageButton;

    .line 1472
    sget v4, Lcom/peel/ui/fp;->btn3:I

    invoke-virtual {v9, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageButton;

    .line 1474
    sget v5, Lcom/peel/ui/fo;->common_btn_stateful:I

    invoke-virtual {v2, v5}, Landroid/widget/ImageButton;->setBackgroundResource(I)V

    .line 1475
    sget v5, Lcom/peel/ui/fo;->remote_icon_pw:I

    invoke-virtual {v2, v5}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 1476
    sget v5, Lcom/peel/ui/fo;->common_btn_stateful:I

    invoke-virtual {v3, v5}, Landroid/widget/ImageButton;->setBackgroundResource(I)V

    .line 1477
    sget v5, Lcom/peel/ui/fo;->remote_icon_pw:I

    invoke-virtual {v3, v5}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 1478
    sget v5, Lcom/peel/ui/fo;->common_btn_stateful:I

    invoke-virtual {v4, v5}, Landroid/widget/ImageButton;->setBackgroundResource(I)V

    .line 1479
    sget v5, Lcom/peel/ui/fo;->remote_icon_pw:I

    invoke-virtual {v4, v5}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 1481
    sget v5, Lcom/peel/ui/fp;->txt1:I

    invoke-virtual {v9, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    .line 1482
    sget v6, Lcom/peel/ui/fp;->txt2:I

    invoke-virtual {v9, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    .line 1483
    sget v7, Lcom/peel/ui/fp;->txt3:I

    invoke-virtual {v9, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    .line 1485
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/peel/ui/gt;->am:Ljava/util/List;

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v8

    const/4 v10, 0x1

    if-ne v8, v10, :cond_c

    .line 1486
    const/4 v8, 0x4

    invoke-virtual {v2, v8}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 1487
    const/4 v2, 0x0

    invoke-virtual {v3, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 1488
    const/4 v2, 0x4

    invoke-virtual {v4, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 1490
    const-string/jumbo v2, ""

    invoke-virtual {v5, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1491
    invoke-virtual/range {p0 .. p0}, Lcom/peel/ui/gt;->m()Landroid/support/v4/app/ae;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/peel/ui/gt;->am:Ljava/util/List;

    const/4 v5, 0x0

    invoke-interface {v2, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/peel/control/h;

    invoke-virtual {v2}, Lcom/peel/control/h;->e()I

    move-result v2

    invoke-static {v4, v2}, Lcom/peel/util/bx;->b(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v6, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1492
    const-string/jumbo v2, ""

    invoke-virtual {v7, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1494
    new-instance v2, Lcom/peel/ui/hz;

    move-object/from16 v0, p0

    invoke-direct {v2, v0}, Lcom/peel/ui/hz;-><init>(Lcom/peel/ui/gt;)V

    invoke-virtual {v3, v2}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1561
    :cond_0
    :goto_0
    move-object/from16 v0, v35

    invoke-virtual {v0, v9}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 1570
    :cond_1
    :goto_1
    new-instance v15, Ljava/util/ArrayList;

    invoke-direct {v15}, Ljava/util/ArrayList;-><init>()V

    .line 1572
    invoke-virtual/range {p0 .. p0}, Lcom/peel/ui/gt;->n()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/peel/ui/fn;->controlpad_side_margin:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v7

    .line 1574
    sget-object v2, Lcom/peel/ui/gt;->i:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "\n\n ******** controlpadMargin : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "\n\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1576
    if-eqz p3, :cond_2

    .line 1578
    invoke-interface {v15}, Ljava/util/List;->clear()V

    .line 1579
    const-string/jumbo v2, "9"

    invoke-interface {v15, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1581
    invoke-virtual/range {p0 .. p0}, Lcom/peel/ui/gt;->m()Landroid/support/v4/app/ae;

    move-result-object v2

    const v3, 0x186a1

    const v4, 0x30d46

    sget v5, Lcom/peel/ui/fo;->vol_015:I

    sget v6, Lcom/peel/ui/ft;->button_volume_controller:I

    const/4 v8, 0x0

    const/16 v9, 0xb8

    const/4 v10, 0x0

    const v11, 0x493e1

    sget v12, Lcom/peel/ui/ft;->button_volume_up:I

    const v13, 0x493e2

    sget v14, Lcom/peel/ui/ft;->button_volume_down:I

    sget v16, Lcom/peel/ui/fo;->volume_up:I

    sget v17, Lcom/peel/ui/fo;->volume_dn:I

    const-string/jumbo v18, "Volume_Up"

    const-string/jumbo v19, "Volume_Down"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/peel/ui/gt;->bN:Landroid/view/View$OnClickListener;

    move-object/from16 v20, v0

    invoke-static/range {v2 .. v20}, Lcom/peel/util/dw;->a(Landroid/content/Context;IIIIIIIIIIIILjava/util/List;IILjava/lang/String;Ljava/lang/String;Landroid/view/View$OnClickListener;)Landroid/view/View;

    move-result-object v2

    .line 1590
    move-object/from16 v0, v35

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 1593
    :cond_2
    if-eqz p4, :cond_3

    .line 1595
    invoke-interface {v15}, Ljava/util/List;->clear()V

    .line 1596
    const-string/jumbo v2, "11"

    invoke-interface {v15, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1597
    invoke-virtual/range {p0 .. p0}, Lcom/peel/ui/gt;->m()Landroid/support/v4/app/ae;

    move-result-object v16

    const v17, 0x186a2

    const v18, 0x30d47

    sget v19, Lcom/peel/ui/fo;->ch_015:I

    sget v20, Lcom/peel/ui/ft;->button_channel_controller:I

    const/16 v21, 0x0

    const/16 v23, 0xb8

    const/16 v24, 0x0

    const v25, 0x493e3

    sget v26, Lcom/peel/ui/ft;->button_channel_up:I

    const v27, 0x493e4

    sget v28, Lcom/peel/ui/ft;->button_channel_down:I

    sget v30, Lcom/peel/ui/fo;->channel_up:I

    sget v31, Lcom/peel/ui/fo;->channel_dn:I

    const-string/jumbo v32, "Channel_Up"

    const-string/jumbo v33, "Channel_Down"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/peel/ui/gt;->bN:Landroid/view/View$OnClickListener;

    move-object/from16 v34, v0

    move/from16 v22, v7

    move-object/from16 v29, v15

    invoke-static/range {v16 .. v34}, Lcom/peel/util/dw;->a(Landroid/content/Context;IIIIIIIIIIIILjava/util/List;IILjava/lang/String;Ljava/lang/String;Landroid/view/View$OnClickListener;)Landroid/view/View;

    move-result-object v2

    .line 1606
    move-object/from16 v0, v35

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 1609
    :cond_3
    if-eqz p6, :cond_4

    .line 1611
    invoke-interface {v15}, Ljava/util/List;->clear()V

    .line 1612
    const-string/jumbo v2, "9"

    invoke-interface {v15, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1614
    invoke-virtual/range {p0 .. p0}, Lcom/peel/ui/gt;->m()Landroid/support/v4/app/ae;

    move-result-object v2

    const v3, 0x186a6

    const v4, 0x30d48

    sget v5, Lcom/peel/ui/fo;->btn_temp:I

    sget v6, Lcom/peel/ui/ft;->temp_label:I

    const/4 v8, 0x0

    const/16 v9, 0xb8

    const/4 v10, 0x0

    const v11, 0x493e5

    sget v12, Lcom/peel/ui/ft;->button_temp_up:I

    const v13, 0x493e6

    sget v14, Lcom/peel/ui/ft;->button_temp_down:I

    sget v16, Lcom/peel/ui/fo;->btn_temp_plus_stateful:I

    sget v17, Lcom/peel/ui/fo;->btn_temp_minus_stateful:I

    const-string/jumbo v18, "UP"

    const-string/jumbo v19, "Down"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/peel/ui/gt;->bN:Landroid/view/View$OnClickListener;

    move-object/from16 v20, v0

    invoke-static/range {v2 .. v20}, Lcom/peel/util/dw;->a(Landroid/content/Context;IIIIIIIIIIIILjava/util/List;IILjava/lang/String;Ljava/lang/String;Landroid/view/View$OnClickListener;)Landroid/view/View;

    move-result-object v2

    .line 1623
    move-object/from16 v0, v35

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 1626
    new-instance v2, Landroid/widget/TextView;

    invoke-virtual/range {p0 .. p0}, Lcom/peel/ui/gt;->m()Landroid/support/v4/app/ae;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/peel/ui/gt;->bl:Landroid/widget/TextView;

    .line 1627
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/peel/ui/gt;->bl:Landroid/widget/TextView;

    sget v3, Lcom/peel/ui/fp;->temperature_txt_id:I

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setId(I)V

    .line 1628
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/peel/ui/gt;->bl:Landroid/widget/TextView;

    invoke-virtual/range {p0 .. p0}, Lcom/peel/ui/gt;->n()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/peel/ui/fm;->remote_ctrl_pad_text:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1629
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/peel/ui/gt;->bl:Landroid/widget/TextView;

    const/high16 v3, 0x42000000    # 32.0f

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTextSize(F)V

    .line 1630
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/peel/ui/gt;->bl:Landroid/widget/TextView;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setGravity(I)V

    .line 1631
    new-instance v2, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v3, -0x2

    const/4 v4, -0x2

    invoke-direct {v2, v3, v4}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1632
    const/16 v3, 0xd

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 1633
    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/16 v6, 0x78

    invoke-virtual {v2, v3, v4, v5, v6}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 1634
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/peel/ui/gt;->bl:Landroid/widget/TextView;

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1636
    sget-object v2, Lcom/peel/ui/gt;->av:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    const/4 v3, 0x2

    if-gt v2, v3, :cond_f

    .line 1637
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/peel/ui/gt;->bl:Landroid/widget/TextView;

    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1652
    :goto_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/peel/ui/gt;->bl:Landroid/widget/TextView;

    move-object/from16 v0, v35

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 1655
    :cond_4
    if-eqz p7, :cond_5

    .line 1657
    invoke-interface {v15}, Ljava/util/List;->clear()V

    .line 1658
    const-string/jumbo v2, "11"

    invoke-interface {v15, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1659
    invoke-virtual/range {p0 .. p0}, Lcom/peel/ui/gt;->m()Landroid/support/v4/app/ae;

    move-result-object v16

    const v17, 0x186a7

    const v18, 0x30d49

    sget v19, Lcom/peel/ui/fo;->btn_fan:I

    sget v20, Lcom/peel/ui/ft;->fan_label:I

    const/16 v21, 0x0

    const/16 v23, 0xb8

    const/16 v24, 0x0

    const v25, 0x493e7

    sget v26, Lcom/peel/ui/ft;->button_channel_up:I

    const v27, 0x493e8

    sget v28, Lcom/peel/ui/ft;->button_channel_down:I

    sget v30, Lcom/peel/ui/fo;->btn_fan_up:I

    sget v31, Lcom/peel/ui/fo;->btn_fan_down:I

    const-string/jumbo v32, "FAN_HIGH"

    const-string/jumbo v33, "FAN_LOW"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/peel/ui/gt;->bN:Landroid/view/View$OnClickListener;

    move-object/from16 v34, v0

    move/from16 v22, v7

    move-object/from16 v29, v15

    invoke-static/range {v16 .. v34}, Lcom/peel/util/dw;->a(Landroid/content/Context;IIIIIIIIIIIILjava/util/List;IILjava/lang/String;Ljava/lang/String;Landroid/view/View$OnClickListener;)Landroid/view/View;

    move-result-object v2

    .line 1668
    move-object/from16 v0, v35

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 1671
    :cond_5
    if-eqz p8, :cond_6

    .line 1673
    invoke-interface {v15}, Ljava/util/List;->clear()V

    .line 1674
    const-string/jumbo v2, "11"

    invoke-interface {v15, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1675
    invoke-virtual/range {p0 .. p0}, Lcom/peel/ui/gt;->m()Landroid/support/v4/app/ae;

    move-result-object v14

    const/16 v16, 0x0

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/peel/ui/gt;->bH:Z

    if-eqz v2, :cond_11

    int-to-float v2, v7

    const v3, 0x3fa66666    # 1.3f

    mul-float/2addr v2, v3

    .line 1676
    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v17

    :goto_3
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/peel/ui/gt;->bH:Z

    if-eqz v2, :cond_12

    const/16 v18, 0xb8

    :goto_4
    const/16 v19, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/peel/ui/gt;->bN:Landroid/view/View$OnClickListener;

    move-object/from16 v20, v0

    .line 1675
    invoke-static/range {v14 .. v20}, Lcom/peel/util/dw;->a(Landroid/content/Context;Ljava/util/List;IIIILandroid/view/View$OnClickListener;)Landroid/view/View;

    move-result-object v2

    .line 1678
    move-object/from16 v0, v35

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 1681
    :cond_6
    if-eqz p5, :cond_7

    .line 1683
    new-instance v2, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v3, -0x2

    const/4 v4, -0x2

    invoke-direct {v2, v3, v4}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1684
    const/16 v3, 0xd3

    invoke-static {v3}, Lcom/peel/util/dw;->a(I)I

    move-result v3

    iput v3, v2, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    .line 1685
    const/16 v3, 0xd3

    invoke-static {v3}, Lcom/peel/util/dw;->a(I)I

    move-result v3

    iput v3, v2, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    .line 1686
    invoke-virtual/range {p0 .. p0}, Lcom/peel/ui/gt;->n()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/peel/ui/fn;->controlpad_side_margin:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    iput v3, v2, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    .line 1687
    const/16 v3, 0xcc

    invoke-static {v3}, Lcom/peel/util/dw;->a(I)I

    move-result v3

    iput v3, v2, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 1688
    const/16 v3, 0xb

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 1691
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/peel/ui/gt;->bH:Z

    if-eqz v3, :cond_13

    .line 1693
    invoke-virtual/range {p0 .. p0}, Lcom/peel/ui/gt;->m()Landroid/support/v4/app/ae;

    move-result-object v3

    const v4, 0x186a9

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/peel/ui/gt;->bN:Landroid/view/View$OnClickListener;

    .line 1692
    invoke-static {v3, v4, v5, v2}, Lcom/peel/util/dw;->b(Landroid/content/Context;ILandroid/view/View$OnClickListener;Landroid/view/ViewGroup$LayoutParams;)Landroid/view/View;

    move-result-object v2

    .line 1701
    :goto_5
    move-object/from16 v0, v35

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 1705
    :cond_7
    new-instance v4, Landroid/widget/LinearLayout;

    invoke-virtual/range {p0 .. p0}, Lcom/peel/ui/gt;->m()Landroid/support/v4/app/ae;

    move-result-object v2

    invoke-direct {v4, v2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 1706
    new-instance v2, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v3, -0x1

    const/4 v5, -0x2

    invoke-direct {v2, v3, v5}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1707
    const/16 v3, 0x2d

    invoke-static {v3}, Lcom/peel/util/dw;->a(I)I

    move-result v3

    iput v3, v2, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    .line 1708
    const/16 v3, 0x2d

    invoke-static {v3}, Lcom/peel/util/dw;->a(I)I

    move-result v3

    iput v3, v2, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 1709
    const/16 v3, 0x1c7

    invoke-static {v3}, Lcom/peel/util/dw;->a(I)I

    move-result v3

    iput v3, v2, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 1711
    invoke-virtual {v4, v2}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1713
    sget-object v2, Lcom/peel/ui/jr;->a:[I

    invoke-virtual/range {p2 .. p2}, Lcom/peel/util/bw;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 1855
    :cond_8
    :goto_6
    move-object/from16 v0, v35

    invoke-virtual {v0, v4}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 1857
    const/4 v2, 0x0

    :try_start_0
    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/peel/ui/gt;->a(Z)V

    .line 1858
    sget v2, Lcom/peel/ui/fp;->overflow_menu_btn:I

    move-object/from16 v0, v35

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    if-eqz v2, :cond_9

    .line 1859
    sget v2, Lcom/peel/ui/fp;->overflow_menu_btn:I

    move-object/from16 v0, v35

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    new-instance v3, Lcom/peel/ui/ih;

    move-object/from16 v0, p0

    invoke-direct {v3, v0}, Lcom/peel/ui/ih;-><init>(Lcom/peel/ui/gt;)V

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1876
    :cond_9
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/peel/ui/gt;->aZ:Z

    if-eqz v2, :cond_17

    .line 1877
    sget v2, Lcom/peel/ui/fp;->controlpad_back_btn:I

    move-object/from16 v0, v35

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    if-eqz v2, :cond_a

    .line 1878
    sget v2, Lcom/peel/ui/fp;->controlpad_back_btn:I

    move-object/from16 v0, v35

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 1910
    :cond_a
    :goto_7
    if-eqz p9, :cond_b

    .line 1911
    sget v2, Lcom/peel/ui/fp;->controlpad_room_state:I

    move-object/from16 v0, v35

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 1912
    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    .line 1913
    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/peel/ui/gt;->c(Landroid/view/View;)V

    .line 1940
    :cond_b
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/peel/ui/gt;->aq:Lcom/peel/ui/ai;

    if-nez v2, :cond_18

    .line 1941
    new-instance v2, Lcom/peel/ui/ai;

    invoke-virtual/range {p0 .. p0}, Lcom/peel/ui/gt;->m()Landroid/support/v4/app/ae;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/peel/ui/gt;->b:Lcom/peel/d/i;

    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/peel/ui/gt;->aZ:Z

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-direct/range {v2 .. v7}, Lcom/peel/ui/ai;-><init>(Landroid/support/v4/app/ae;Lcom/peel/d/i;ZZLjava/lang/String;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/peel/ui/gt;->aq:Lcom/peel/ui/ai;

    .line 1946
    :goto_8
    move-object/from16 v0, p1

    move-object/from16 v1, v35

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 1947
    return-void

    .line 1501
    :cond_c
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/peel/ui/gt;->am:Ljava/util/List;

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v8

    const/4 v10, 0x2

    if-ne v8, v10, :cond_d

    .line 1502
    const/4 v8, 0x0

    invoke-virtual {v2, v8}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 1503
    const/4 v8, 0x0

    invoke-virtual {v3, v8}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 1504
    const/4 v8, 0x0

    invoke-virtual {v4, v8}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 1506
    sget v8, Lcom/peel/ui/ft;->all_cap:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v8}, Lcom/peel/ui/gt;->a(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1507
    invoke-virtual/range {p0 .. p0}, Lcom/peel/ui/gt;->m()Landroid/support/v4/app/ae;

    move-result-object v8

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/peel/ui/gt;->am:Ljava/util/List;

    const/4 v10, 0x1

    invoke-interface {v5, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/peel/control/h;

    invoke-virtual {v5}, Lcom/peel/control/h;->e()I

    move-result v5

    invoke-static {v8, v5}, Lcom/peel/util/bx;->b(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v6, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1508
    invoke-virtual/range {p0 .. p0}, Lcom/peel/ui/gt;->m()Landroid/support/v4/app/ae;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/peel/ui/gt;->am:Ljava/util/List;

    const/4 v8, 0x0

    invoke-interface {v5, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/peel/control/h;

    invoke-virtual {v5}, Lcom/peel/control/h;->e()I

    move-result v5

    invoke-static {v6, v5}, Lcom/peel/util/bx;->b(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v7, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1510
    new-instance v5, Lcom/peel/ui/ia;

    move-object/from16 v0, p0

    invoke-direct {v5, v0}, Lcom/peel/ui/ia;-><init>(Lcom/peel/ui/gt;)V

    invoke-virtual {v2, v5}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1518
    new-instance v2, Lcom/peel/ui/ib;

    move-object/from16 v0, p0

    invoke-direct {v2, v0}, Lcom/peel/ui/ib;-><init>(Lcom/peel/ui/gt;)V

    invoke-virtual {v3, v2}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1525
    new-instance v2, Lcom/peel/ui/ic;

    move-object/from16 v0, p0

    invoke-direct {v2, v0}, Lcom/peel/ui/ic;-><init>(Lcom/peel/ui/gt;)V

    invoke-virtual {v4, v2}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_0

    .line 1531
    :cond_d
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/peel/ui/gt;->am:Ljava/util/List;

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v8

    const/4 v10, 0x3

    if-ne v8, v10, :cond_0

    .line 1532
    const/4 v8, 0x0

    invoke-virtual {v2, v8}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 1533
    const/4 v8, 0x0

    invoke-virtual {v3, v8}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 1534
    const/4 v8, 0x0

    invoke-virtual {v4, v8}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 1536
    invoke-virtual/range {p0 .. p0}, Lcom/peel/ui/gt;->m()Landroid/support/v4/app/ae;

    move-result-object v10

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/peel/ui/gt;->am:Ljava/util/List;

    const/4 v11, 0x2

    invoke-interface {v8, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/peel/control/h;

    invoke-virtual {v8}, Lcom/peel/control/h;->e()I

    move-result v8

    invoke-static {v10, v8}, Lcom/peel/util/bx;->b(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1537
    invoke-virtual/range {p0 .. p0}, Lcom/peel/ui/gt;->m()Landroid/support/v4/app/ae;

    move-result-object v8

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/peel/ui/gt;->am:Ljava/util/List;

    const/4 v10, 0x1

    invoke-interface {v5, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/peel/control/h;

    invoke-virtual {v5}, Lcom/peel/control/h;->e()I

    move-result v5

    invoke-static {v8, v5}, Lcom/peel/util/bx;->b(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v6, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1538
    invoke-virtual/range {p0 .. p0}, Lcom/peel/ui/gt;->m()Landroid/support/v4/app/ae;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/peel/ui/gt;->am:Ljava/util/List;

    const/4 v8, 0x0

    invoke-interface {v5, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/peel/control/h;

    invoke-virtual {v5}, Lcom/peel/control/h;->e()I

    move-result v5

    invoke-static {v6, v5}, Lcom/peel/util/bx;->b(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v7, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1540
    new-instance v5, Lcom/peel/ui/ie;

    move-object/from16 v0, p0

    invoke-direct {v5, v0}, Lcom/peel/ui/ie;-><init>(Lcom/peel/ui/gt;)V

    invoke-virtual {v2, v5}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1547
    new-instance v2, Lcom/peel/ui/if;

    move-object/from16 v0, p0

    invoke-direct {v2, v0}, Lcom/peel/ui/if;-><init>(Lcom/peel/ui/gt;)V

    invoke-virtual {v3, v2}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1554
    new-instance v2, Lcom/peel/ui/ig;

    move-object/from16 v0, p0

    invoke-direct {v2, v0}, Lcom/peel/ui/ig;-><init>(Lcom/peel/ui/gt;)V

    invoke-virtual {v4, v2}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_0

    .line 1565
    :cond_e
    invoke-virtual/range {p0 .. p0}, Lcom/peel/ui/gt;->m()Landroid/support/v4/app/ae;

    move-result-object v2

    const v3, 0x30d45

    sget v4, Lcom/peel/ui/fo;->btn_onoff:I

    sget v5, Lcom/peel/ui/fo;->btn_onoff_press:I

    sget v6, Lcom/peel/ui/ft;->button_power:I

    const-string/jumbo v7, "Power"

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/peel/ui/gt;->bN:Landroid/view/View$OnClickListener;

    invoke-static/range {v2 .. v8}, Lcom/peel/util/dw;->a(Landroid/content/Context;IIIILjava/lang/String;Landroid/view/View$OnClickListener;)Landroid/view/View;

    move-result-object v2

    .line 1567
    move-object/from16 v0, v35

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    goto/16 :goto_1

    .line 1640
    :cond_f
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/peel/ui/gt;->be:Landroid/content/SharedPreferences;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v4, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v4}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v4

    invoke-virtual {v4}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v4

    invoke-virtual {v4}, Lcom/peel/data/at;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "_"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "ac_last_temp_idx"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v2

    sput v2, Lcom/peel/ui/gt;->aA:I

    .line 1643
    sget v2, Lcom/peel/ui/gt;->aA:I

    sget-object v3, Lcom/peel/ui/gt;->av:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-lt v2, v3, :cond_10

    const/4 v2, 0x0

    sput v2, Lcom/peel/ui/gt;->aA:I

    .line 1645
    :cond_10
    :try_start_1
    sget-object v2, Lcom/peel/ui/gt;->av:Ljava/util/List;

    sget v3, Lcom/peel/ui/gt;->aA:I

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    const-string/jumbo v3, "_"

    invoke-virtual {v2, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 1646
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/peel/ui/gt;->bl:Landroid/widget/TextView;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v5, 0x1

    aget-object v2, v2, v5

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/16 v4, 0xb0

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v4, "C"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_2

    .line 1647
    :catch_0
    move-exception v2

    .line 1648
    sget-object v3, Lcom/peel/ui/gt;->i:Ljava/lang/String;

    sget-object v4, Lcom/peel/ui/gt;->i:Ljava/lang/String;

    invoke-static {v3, v4, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_2

    :cond_11
    move/from16 v17, v7

    .line 1676
    goto/16 :goto_3

    :cond_12
    const/16 v18, 0xcc

    goto/16 :goto_4

    .line 1697
    :cond_13
    invoke-virtual/range {p0 .. p0}, Lcom/peel/ui/gt;->m()Landroid/support/v4/app/ae;

    move-result-object v3

    const v4, 0x186a9

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/peel/ui/gt;->bN:Landroid/view/View$OnClickListener;

    .line 1696
    invoke-static {v3, v4, v5, v2}, Lcom/peel/util/dw;->a(Landroid/content/Context;ILandroid/view/View$OnClickListener;Landroid/view/ViewGroup$LayoutParams;)Landroid/view/View;

    move-result-object v2

    goto/16 :goto_5

    .line 1716
    :pswitch_0
    sget-object v2, Lcom/peel/ui/gt;->ax:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_15

    .line 1717
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/peel/ui/gt;->be:Landroid/content/SharedPreferences;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v5, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v5}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v5

    invoke-virtual {v5}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v5

    invoke-virtual {v5}, Lcom/peel/data/at;->b()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v5, "_"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v5, "ac_last_mode_idx"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v5, 0x0

    invoke-interface {v2, v3, v5}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v2

    sput v2, Lcom/peel/ui/gt;->aC:I

    .line 1719
    sget v2, Lcom/peel/ui/gt;->aC:I

    sget-object v3, Lcom/peel/ui/gt;->ax:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-lt v2, v3, :cond_14

    .line 1720
    const/4 v2, 0x0

    sput v2, Lcom/peel/ui/gt;->aC:I

    .line 1721
    :cond_14
    sget-object v2, Lcom/peel/ui/gt;->ax:Ljava/util/List;

    sget v3, Lcom/peel/ui/gt;->aC:I

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 1728
    invoke-virtual/range {p0 .. p0}, Lcom/peel/ui/gt;->m()Landroid/support/v4/app/ae;

    move-result-object v3

    const-string/jumbo v5, "MODE"

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/peel/ui/gt;->bN:Landroid/view/View$OnClickListener;

    invoke-static {v3, v5, v6}, Lcom/peel/util/dw;->b(Landroid/content/Context;Ljava/lang/String;Landroid/view/View$OnClickListener;)Landroid/view/View;

    move-result-object v5

    .line 1730
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/peel/ui/gt;->bH:Z

    if-eqz v3, :cond_16

    .line 1736
    :goto_9
    invoke-virtual {v4, v5}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1744
    :cond_15
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/peel/ui/gt;->aL:Lcom/peel/control/a;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/peel/control/a;->a(I)Lcom/peel/control/h;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/peel/ui/gt;->aM:Lcom/peel/control/h;

    .line 1745
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/peel/ui/gt;->aM:Lcom/peel/control/h;

    const-string/jumbo v3, "VANE"

    invoke-virtual {v2, v3}, Lcom/peel/control/h;->a(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 1746
    invoke-virtual/range {p0 .. p0}, Lcom/peel/ui/gt;->m()Landroid/support/v4/app/ae;

    move-result-object v2

    const-string/jumbo v3, "VANE"

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/peel/ui/gt;->bN:Landroid/view/View$OnClickListener;

    invoke-static {v2, v3, v5}, Lcom/peel/util/dw;->b(Landroid/content/Context;Ljava/lang/String;Landroid/view/View$OnClickListener;)Landroid/view/View;

    move-result-object v2

    .line 1748
    invoke-virtual {v4, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    goto/16 :goto_6

    .line 1733
    :cond_16
    sget v3, Lcom/peel/ui/fp;->btn_txt_id:I

    invoke-virtual {v5, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    const/16 v6, 0x5f

    invoke-virtual {v2, v6}, Ljava/lang/String;->indexOf(I)I

    move-result v6

    add-int/lit8 v6, v6, 0x1

    invoke-virtual {v2, v6}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_9

    .line 1754
    :pswitch_1
    invoke-virtual/range {p0 .. p0}, Lcom/peel/ui/gt;->m()Landroid/support/v4/app/ae;

    move-result-object v2

    const-string/jumbo v3, "Mute"

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/peel/ui/gt;->bN:Landroid/view/View$OnClickListener;

    invoke-static {v2, v3, v5}, Lcom/peel/util/dw;->b(Landroid/content/Context;Ljava/lang/String;Landroid/view/View$OnClickListener;)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v4, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1757
    invoke-virtual/range {p0 .. p0}, Lcom/peel/ui/gt;->m()Landroid/support/v4/app/ae;

    move-result-object v2

    const-string/jumbo v3, "Menu"

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/peel/ui/gt;->bN:Landroid/view/View$OnClickListener;

    invoke-static {v2, v3, v5}, Lcom/peel/util/dw;->b(Landroid/content/Context;Ljava/lang/String;Landroid/view/View$OnClickListener;)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v4, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1760
    invoke-virtual/range {p0 .. p0}, Lcom/peel/ui/gt;->m()Landroid/support/v4/app/ae;

    move-result-object v2

    const-string/jumbo v3, "Play"

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/peel/ui/gt;->bN:Landroid/view/View$OnClickListener;

    invoke-static {v2, v3, v5}, Lcom/peel/util/dw;->b(Landroid/content/Context;Ljava/lang/String;Landroid/view/View$OnClickListener;)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v4, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    goto/16 :goto_6

    .line 1767
    :pswitch_2
    invoke-virtual/range {p0 .. p0}, Lcom/peel/ui/gt;->m()Landroid/support/v4/app/ae;

    move-result-object v2

    const-string/jumbo v3, "Mute"

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/peel/ui/gt;->bN:Landroid/view/View$OnClickListener;

    invoke-static {v2, v3, v5}, Lcom/peel/util/dw;->b(Landroid/content/Context;Ljava/lang/String;Landroid/view/View$OnClickListener;)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v4, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1770
    invoke-virtual/range {p0 .. p0}, Lcom/peel/ui/gt;->m()Landroid/support/v4/app/ae;

    move-result-object v2

    const-string/jumbo v3, "Input"

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/peel/ui/gt;->bN:Landroid/view/View$OnClickListener;

    invoke-static {v2, v3, v5}, Lcom/peel/util/dw;->b(Landroid/content/Context;Ljava/lang/String;Landroid/view/View$OnClickListener;)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v4, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    goto/16 :goto_6

    .line 1777
    :pswitch_3
    invoke-virtual/range {p0 .. p0}, Lcom/peel/ui/gt;->m()Landroid/support/v4/app/ae;

    move-result-object v2

    const-string/jumbo v3, "Mute"

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/peel/ui/gt;->bN:Landroid/view/View$OnClickListener;

    invoke-static {v2, v3, v5}, Lcom/peel/util/dw;->b(Landroid/content/Context;Ljava/lang/String;Landroid/view/View$OnClickListener;)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v4, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1780
    invoke-virtual/range {p0 .. p0}, Lcom/peel/ui/gt;->m()Landroid/support/v4/app/ae;

    move-result-object v2

    const-string/jumbo v3, "keypad"

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/peel/ui/gt;->bI:Landroid/view/View$OnClickListener;

    invoke-static {v2, v3, v5}, Lcom/peel/util/dw;->b(Landroid/content/Context;Ljava/lang/String;Landroid/view/View$OnClickListener;)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v4, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1783
    invoke-virtual/range {p0 .. p0}, Lcom/peel/ui/gt;->m()Landroid/support/v4/app/ae;

    move-result-object v2

    const-string/jumbo v3, "Play"

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/peel/ui/gt;->bN:Landroid/view/View$OnClickListener;

    invoke-static {v2, v3, v5}, Lcom/peel/util/dw;->b(Landroid/content/Context;Ljava/lang/String;Landroid/view/View$OnClickListener;)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v4, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    goto/16 :goto_6

    .line 1788
    :pswitch_4
    invoke-virtual/range {p0 .. p0}, Lcom/peel/ui/gt;->m()Landroid/support/v4/app/ae;

    move-result-object v2

    const-string/jumbo v3, "Mute"

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/peel/ui/gt;->bN:Landroid/view/View$OnClickListener;

    invoke-static {v2, v3, v5}, Lcom/peel/util/dw;->b(Landroid/content/Context;Ljava/lang/String;Landroid/view/View$OnClickListener;)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v4, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1791
    invoke-virtual/range {p0 .. p0}, Lcom/peel/ui/gt;->m()Landroid/support/v4/app/ae;

    move-result-object v2

    const-string/jumbo v3, "Play"

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/peel/ui/gt;->bN:Landroid/view/View$OnClickListener;

    invoke-static {v2, v3, v5}, Lcom/peel/util/dw;->b(Landroid/content/Context;Ljava/lang/String;Landroid/view/View$OnClickListener;)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v4, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1795
    invoke-virtual/range {p0 .. p0}, Lcom/peel/ui/gt;->m()Landroid/support/v4/app/ae;

    move-result-object v2

    const-string/jumbo v3, "ThumbsUp"

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/peel/ui/gt;->bN:Landroid/view/View$OnClickListener;

    .line 1794
    invoke-static {v2, v3, v5}, Lcom/peel/util/dw;->b(Landroid/content/Context;Ljava/lang/String;Landroid/view/View$OnClickListener;)Landroid/view/View;

    move-result-object v2

    .line 1797
    invoke-virtual {v4, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1801
    invoke-virtual/range {p0 .. p0}, Lcom/peel/ui/gt;->m()Landroid/support/v4/app/ae;

    move-result-object v2

    const-string/jumbo v3, "ThumbsDown"

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/peel/ui/gt;->bN:Landroid/view/View$OnClickListener;

    .line 1800
    invoke-static {v2, v3, v5}, Lcom/peel/util/dw;->b(Landroid/content/Context;Ljava/lang/String;Landroid/view/View$OnClickListener;)Landroid/view/View;

    move-result-object v2

    .line 1803
    invoke-virtual {v4, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    goto/16 :goto_6

    .line 1808
    :pswitch_5
    invoke-virtual/range {p0 .. p0}, Lcom/peel/ui/gt;->m()Landroid/support/v4/app/ae;

    move-result-object v2

    const-string/jumbo v3, "Mute"

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/peel/ui/gt;->bN:Landroid/view/View$OnClickListener;

    invoke-static {v2, v3, v5}, Lcom/peel/util/dw;->b(Landroid/content/Context;Ljava/lang/String;Landroid/view/View$OnClickListener;)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v4, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1811
    invoke-virtual/range {p0 .. p0}, Lcom/peel/ui/gt;->m()Landroid/support/v4/app/ae;

    move-result-object v2

    const-string/jumbo v3, "Home"

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/peel/ui/gt;->bN:Landroid/view/View$OnClickListener;

    invoke-static {v2, v3, v5}, Lcom/peel/util/dw;->b(Landroid/content/Context;Ljava/lang/String;Landroid/view/View$OnClickListener;)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v4, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1814
    invoke-virtual/range {p0 .. p0}, Lcom/peel/ui/gt;->m()Landroid/support/v4/app/ae;

    move-result-object v2

    const-string/jumbo v3, "Back"

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/peel/ui/gt;->bN:Landroid/view/View$OnClickListener;

    invoke-static {v2, v3, v5}, Lcom/peel/util/dw;->b(Landroid/content/Context;Ljava/lang/String;Landroid/view/View$OnClickListener;)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v4, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1817
    invoke-virtual/range {p0 .. p0}, Lcom/peel/ui/gt;->m()Landroid/support/v4/app/ae;

    move-result-object v2

    const-string/jumbo v3, "Play"

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/peel/ui/gt;->bN:Landroid/view/View$OnClickListener;

    invoke-static {v2, v3, v5}, Lcom/peel/util/dw;->b(Landroid/content/Context;Ljava/lang/String;Landroid/view/View$OnClickListener;)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v4, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    goto/16 :goto_6

    .line 1822
    :pswitch_6
    invoke-virtual/range {p0 .. p0}, Lcom/peel/ui/gt;->m()Landroid/support/v4/app/ae;

    move-result-object v2

    const-string/jumbo v3, "Mute"

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/peel/ui/gt;->bN:Landroid/view/View$OnClickListener;

    invoke-static {v2, v3, v5}, Lcom/peel/util/dw;->b(Landroid/content/Context;Ljava/lang/String;Landroid/view/View$OnClickListener;)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v4, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1825
    invoke-virtual/range {p0 .. p0}, Lcom/peel/ui/gt;->m()Landroid/support/v4/app/ae;

    move-result-object v2

    const-string/jumbo v3, "keypad"

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/peel/ui/gt;->bI:Landroid/view/View$OnClickListener;

    invoke-static {v2, v3, v5}, Lcom/peel/util/dw;->b(Landroid/content/Context;Ljava/lang/String;Landroid/view/View$OnClickListener;)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v4, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1828
    invoke-virtual/range {p0 .. p0}, Lcom/peel/ui/gt;->m()Landroid/support/v4/app/ae;

    move-result-object v2

    const-string/jumbo v3, "Input"

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/peel/ui/gt;->bN:Landroid/view/View$OnClickListener;

    invoke-static {v2, v3, v5}, Lcom/peel/util/dw;->b(Landroid/content/Context;Ljava/lang/String;Landroid/view/View$OnClickListener;)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v4, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    goto/16 :goto_6

    .line 1833
    :pswitch_7
    invoke-virtual/range {p0 .. p0}, Lcom/peel/ui/gt;->m()Landroid/support/v4/app/ae;

    move-result-object v2

    const-string/jumbo v3, "Mute"

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/peel/ui/gt;->bN:Landroid/view/View$OnClickListener;

    invoke-static {v2, v3, v5}, Lcom/peel/util/dw;->b(Landroid/content/Context;Ljava/lang/String;Landroid/view/View$OnClickListener;)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v4, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1836
    invoke-virtual/range {p0 .. p0}, Lcom/peel/ui/gt;->m()Landroid/support/v4/app/ae;

    move-result-object v2

    const-string/jumbo v3, "keypad"

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/peel/ui/gt;->bI:Landroid/view/View$OnClickListener;

    invoke-static {v2, v3, v5}, Lcom/peel/util/dw;->b(Landroid/content/Context;Ljava/lang/String;Landroid/view/View$OnClickListener;)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v4, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1839
    invoke-virtual/range {p0 .. p0}, Lcom/peel/ui/gt;->m()Landroid/support/v4/app/ae;

    move-result-object v2

    const-string/jumbo v3, "Input^Input"

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/peel/ui/gt;->bN:Landroid/view/View$OnClickListener;

    invoke-static {v2, v3, v5}, Lcom/peel/util/dw;->b(Landroid/content/Context;Ljava/lang/String;Landroid/view/View$OnClickListener;)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v4, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    goto/16 :goto_6

    .line 1844
    :pswitch_8
    invoke-virtual/range {p0 .. p0}, Lcom/peel/ui/gt;->m()Landroid/support/v4/app/ae;

    move-result-object v2

    const-string/jumbo v3, "Mute"

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/peel/ui/gt;->bN:Landroid/view/View$OnClickListener;

    invoke-static {v2, v3, v5}, Lcom/peel/util/dw;->b(Landroid/content/Context;Ljava/lang/String;Landroid/view/View$OnClickListener;)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v4, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1847
    invoke-virtual/range {p0 .. p0}, Lcom/peel/ui/gt;->m()Landroid/support/v4/app/ae;

    move-result-object v2

    const-string/jumbo v3, "keypad"

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/peel/ui/gt;->bI:Landroid/view/View$OnClickListener;

    invoke-static {v2, v3, v5}, Lcom/peel/util/dw;->b(Landroid/content/Context;Ljava/lang/String;Landroid/view/View$OnClickListener;)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v4, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1850
    invoke-virtual/range {p0 .. p0}, Lcom/peel/ui/gt;->m()Landroid/support/v4/app/ae;

    move-result-object v2

    const-string/jumbo v3, "Input"

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/peel/ui/gt;->bN:Landroid/view/View$OnClickListener;

    invoke-static {v2, v3, v5}, Lcom/peel/util/dw;->b(Landroid/content/Context;Ljava/lang/String;Landroid/view/View$OnClickListener;)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v4, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    goto/16 :goto_6

    .line 1881
    :cond_17
    :try_start_2
    sget v2, Lcom/peel/ui/fp;->controlpad_back_btn:I

    move-object/from16 v0, v35

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    if-eqz v2, :cond_a

    .line 1882
    sget v2, Lcom/peel/ui/fp;->controlpad_back_btn:I

    move-object/from16 v0, v35

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    new-instance v3, Lcom/peel/ui/ii;

    move-object/from16 v0, p0

    invoke-direct {v3, v0}, Lcom/peel/ui/ii;-><init>(Lcom/peel/ui/gt;)V

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    goto/16 :goto_7

    .line 1906
    :catch_1
    move-exception v2

    .line 1907
    sget-object v3, Lcom/peel/ui/gt;->i:Ljava/lang/String;

    sget-object v4, Lcom/peel/ui/gt;->i:Ljava/lang/String;

    invoke-static {v3, v4, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_7

    .line 1943
    :cond_18
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/peel/ui/gt;->aq:Lcom/peel/ui/ai;

    invoke-virtual {v2}, Lcom/peel/ui/ai;->notifyDataSetChanged()V

    goto/16 :goto_8

    .line 1713
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method

.method private a(Lcom/peel/control/a;)V
    .locals 12

    .prologue
    const/4 v3, 0x0

    const/16 v11, 0x8

    const/4 v10, 0x2

    const/4 v2, 0x0

    const/4 v9, 0x1

    .line 1328
    iget-object v0, p0, Lcom/peel/ui/gt;->aS:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->removeAllViews()V

    .line 1329
    iget-object v0, p0, Lcom/peel/ui/gt;->aW:Landroid/widget/ViewFlipper;

    invoke-virtual {v0}, Landroid/widget/ViewFlipper;->removeAllViews()V

    .line 1330
    iget-object v0, p0, Lcom/peel/ui/gt;->aF:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 1332
    if-nez p1, :cond_0

    move-object v4, v3

    .line 1333
    :goto_0
    if-nez v4, :cond_1

    .line 1334
    iget-object v0, p0, Lcom/peel/ui/gt;->aU:Lcom/peel/widget/SlidingDrawer;

    invoke-virtual {v0, v11}, Lcom/peel/widget/SlidingDrawer;->setVisibility(I)V

    .line 1444
    :goto_1
    return-void

    .line 1332
    :cond_0
    invoke-virtual {p1}, Lcom/peel/control/a;->e()[Lcom/peel/control/h;

    move-result-object v0

    move-object v4, v0

    goto :goto_0

    .line 1337
    :cond_1
    iget-object v0, p0, Lcom/peel/ui/gt;->aU:Lcom/peel/widget/SlidingDrawer;

    invoke-virtual {v0, v2}, Lcom/peel/widget/SlidingDrawer;->setVisibility(I)V

    move v0, v2

    .line 1340
    :goto_2
    array-length v1, v4

    if-ge v0, v1, :cond_4

    .line 1341
    aget-object v5, v4, v0

    .line 1342
    new-instance v6, Landroid/os/Bundle;

    invoke-direct {v6}, Landroid/os/Bundle;-><init>()V

    .line 1344
    invoke-virtual {v5}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/data/g;->d()I

    move-result v1

    .line 1345
    const/16 v7, 0x14

    if-eq v1, v7, :cond_2

    if-eq v1, v10, :cond_2

    const/4 v7, 0x6

    if-ne v1, v7, :cond_b

    .line 1346
    :cond_2
    invoke-virtual {v5}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/data/g;->f()Ljava/lang/String;

    move-result-object v1

    .line 1348
    :goto_3
    const-string/jumbo v7, "title"

    if-nez v1, :cond_3

    invoke-virtual {p0}, Lcom/peel/ui/gt;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    .line 1349
    invoke-virtual {v5}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v8

    invoke-virtual {v8}, Lcom/peel/data/g;->d()I

    move-result v8

    .line 1348
    invoke-static {v1, v8}, Lcom/peel/util/bx;->a(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v1

    :cond_3
    invoke-virtual {v6, v7, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1350
    const-string/jumbo v1, "device_id"

    invoke-virtual {v5}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v7

    invoke-virtual {v7}, Lcom/peel/data/g;->b()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v1, v7}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1352
    iget-object v1, p0, Lcom/peel/ui/gt;->aF:Ljava/util/List;

    invoke-interface {v1, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1358
    sget-object v1, Lcom/peel/ui/gt;->i:Ljava/lang/String;

    const-string/jumbo v6, "inflate views for device controls"

    new-instance v7, Lcom/peel/ui/hx;

    invoke-direct {v7, p0, v5}, Lcom/peel/ui/hx;-><init>(Lcom/peel/ui/gt;Lcom/peel/control/h;)V

    invoke-static {v1, v6, v7}, Lcom/peel/util/i;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/lang/Runnable;

    .line 1340
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 1365
    :cond_4
    iget-object v0, p0, Lcom/peel/ui/gt;->aF:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_5

    .line 1366
    iget-object v0, p0, Lcom/peel/ui/gt;->aU:Lcom/peel/widget/SlidingDrawer;

    invoke-virtual {v0}, Lcom/peel/widget/SlidingDrawer;->getHandle()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v11}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1

    .line 1370
    :cond_5
    invoke-virtual {p1, v9}, Lcom/peel/control/a;->a(I)Lcom/peel/control/h;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/ui/gt;->aM:Lcom/peel/control/h;

    move v0, v2

    .line 1372
    :goto_4
    array-length v1, v4

    if-ge v0, v1, :cond_a

    .line 1373
    aget-object v1, v4, v0

    invoke-virtual {v1}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/data/g;->b()Ljava/lang/String;

    move-result-object v1

    iget-object v3, p0, Lcom/peel/ui/gt;->aM:Lcom/peel/control/h;

    invoke-virtual {v3}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v3

    invoke-virtual {v3}, Lcom/peel/data/g;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_6

    move v5, v0

    .line 1379
    :goto_5
    iget-object v0, p0, Lcom/peel/ui/gt;->aU:Lcom/peel/widget/SlidingDrawer;

    invoke-virtual {v0}, Lcom/peel/widget/SlidingDrawer;->getHandle()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1380
    iget-object v0, p0, Lcom/peel/ui/gt;->aF:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 1408
    iget-object v0, p0, Lcom/peel/ui/gt;->aO:Landroid/view/LayoutInflater;

    sget v1, Lcom/peel/ui/fq;->device_tab_triple:I

    iget-object v3, p0, Lcom/peel/ui/gt;->aS:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v1, v3, v9}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 1409
    iget-object v0, p0, Lcom/peel/ui/gt;->aS:Landroid/widget/FrameLayout;

    sget v1, Lcom/peel/ui/fp;->device_tab_1:I

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 1410
    iget-object v0, p0, Lcom/peel/ui/gt;->aS:Landroid/widget/FrameLayout;

    sget v3, Lcom/peel/ui/fp;->device_tab_2:I

    invoke-virtual {v0, v3}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    .line 1411
    iget-object v0, p0, Lcom/peel/ui/gt;->aS:Landroid/widget/FrameLayout;

    sget v4, Lcom/peel/ui/fp;->device_tab_3:I

    invoke-virtual {v0, v4}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v4

    move-object v0, v1

    .line 1413
    check-cast v0, Landroid/widget/TextView;

    iget-object v6, p0, Lcom/peel/ui/gt;->aF:Ljava/util/List;

    invoke-interface {v6, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/os/Bundle;

    const-string/jumbo v6, "title"

    invoke-virtual {v2, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    move-object v0, v3

    .line 1414
    check-cast v0, Landroid/widget/TextView;

    iget-object v2, p0, Lcom/peel/ui/gt;->aF:Ljava/util/List;

    invoke-interface {v2, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/os/Bundle;

    const-string/jumbo v6, "title"

    invoke-virtual {v2, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    move-object v0, v4

    .line 1415
    check-cast v0, Landroid/widget/TextView;

    iget-object v2, p0, Lcom/peel/ui/gt;->aF:Ljava/util/List;

    invoke-interface {v2, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/os/Bundle;

    const-string/jumbo v6, "title"

    invoke-virtual {v2, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1417
    iget-object v0, p0, Lcom/peel/ui/gt;->bJ:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1418
    iget-object v0, p0, Lcom/peel/ui/gt;->bK:Landroid/view/View$OnClickListener;

    invoke-virtual {v3, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1419
    iget-object v0, p0, Lcom/peel/ui/gt;->bL:Landroid/view/View$OnClickListener;

    invoke-virtual {v4, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1421
    if-nez v5, :cond_8

    .line 1422
    iget-object v0, p0, Lcom/peel/ui/gt;->aS:Landroid/widget/FrameLayout;

    sget v1, Lcom/peel/ui/fp;->device_tab_1:I

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v9}, Landroid/view/View;->setSelected(Z)V

    .line 1436
    :goto_6
    sget-object v0, Lcom/peel/ui/gt;->i:Ljava/lang/String;

    const-string/jumbo v1, "display device controls"

    new-instance v2, Lcom/peel/ui/hy;

    invoke-direct {v2, p0, v5}, Lcom/peel/ui/hy;-><init>(Lcom/peel/ui/gt;I)V

    invoke-static {v0, v1, v2}, Lcom/peel/util/i;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/lang/Runnable;

    goto/16 :goto_1

    .line 1372
    :cond_6
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_4

    .line 1382
    :pswitch_0
    iget-object v0, p0, Lcom/peel/ui/gt;->aO:Landroid/view/LayoutInflater;

    sget v1, Lcom/peel/ui/fq;->device_tab_single:I

    iget-object v3, p0, Lcom/peel/ui/gt;->aS:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v1, v3, v9}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 1383
    iget-object v0, p0, Lcom/peel/ui/gt;->aS:Landroid/widget/FrameLayout;

    sget v1, Lcom/peel/ui/fp;->device_tab_1:I

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    move-object v0, v1

    .line 1385
    check-cast v0, Landroid/widget/TextView;

    iget-object v3, p0, Lcom/peel/ui/gt;->aF:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/os/Bundle;

    const-string/jumbo v3, "title"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1386
    iget-object v0, p0, Lcom/peel/ui/gt;->bJ:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1387
    invoke-virtual {v1, v9}, Landroid/view/View;->setSelected(Z)V

    goto :goto_6

    .line 1391
    :pswitch_1
    iget-object v0, p0, Lcom/peel/ui/gt;->aO:Landroid/view/LayoutInflater;

    sget v1, Lcom/peel/ui/fq;->device_tab_double:I

    iget-object v3, p0, Lcom/peel/ui/gt;->aS:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v1, v3, v9}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 1392
    iget-object v0, p0, Lcom/peel/ui/gt;->aS:Landroid/widget/FrameLayout;

    sget v1, Lcom/peel/ui/fp;->device_tab_1:I

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 1393
    iget-object v0, p0, Lcom/peel/ui/gt;->aS:Landroid/widget/FrameLayout;

    sget v3, Lcom/peel/ui/fp;->device_tab_2:I

    invoke-virtual {v0, v3}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    move-object v0, v1

    .line 1394
    check-cast v0, Landroid/widget/TextView;

    iget-object v4, p0, Lcom/peel/ui/gt;->aF:Ljava/util/List;

    invoke-interface {v4, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/os/Bundle;

    const-string/jumbo v4, "title"

    invoke-virtual {v2, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    move-object v0, v3

    .line 1395
    check-cast v0, Landroid/widget/TextView;

    iget-object v2, p0, Lcom/peel/ui/gt;->aF:Ljava/util/List;

    invoke-interface {v2, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/os/Bundle;

    const-string/jumbo v4, "title"

    invoke-virtual {v2, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1397
    iget-object v0, p0, Lcom/peel/ui/gt;->bJ:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1398
    iget-object v0, p0, Lcom/peel/ui/gt;->bK:Landroid/view/View$OnClickListener;

    invoke-virtual {v3, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1400
    if-nez v5, :cond_7

    .line 1401
    invoke-virtual {v1, v9}, Landroid/view/View;->setSelected(Z)V

    goto/16 :goto_6

    .line 1403
    :cond_7
    invoke-virtual {v3, v9}, Landroid/view/View;->setSelected(Z)V

    goto/16 :goto_6

    .line 1423
    :cond_8
    if-ne v9, v5, :cond_9

    .line 1424
    iget-object v0, p0, Lcom/peel/ui/gt;->aS:Landroid/widget/FrameLayout;

    sget v1, Lcom/peel/ui/fp;->device_tab_2:I

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v9}, Landroid/view/View;->setSelected(Z)V

    goto/16 :goto_6

    .line 1426
    :cond_9
    iget-object v0, p0, Lcom/peel/ui/gt;->aS:Landroid/widget/FrameLayout;

    sget v1, Lcom/peel/ui/fp;->device_tab_3:I

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v9}, Landroid/view/View;->setSelected(Z)V

    goto/16 :goto_6

    :cond_a
    move v5, v2

    goto/16 :goto_5

    :cond_b
    move-object v1, v3

    goto/16 :goto_3

    .line 1380
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method static synthetic a(Lcom/peel/ui/gt;IZ)V
    .locals 0

    .prologue
    .line 113
    invoke-direct {p0, p1, p2}, Lcom/peel/ui/gt;->a(IZ)V

    return-void
.end method

.method static synthetic a(Lcom/peel/ui/gt;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 113
    invoke-direct {p0, p1}, Lcom/peel/ui/gt;->b(Landroid/view/View;)V

    return-void
.end method

.method static synthetic a(Lcom/peel/ui/gt;Landroid/view/ViewGroup;Lcom/peel/data/g;)V
    .locals 0

    .prologue
    .line 113
    invoke-direct {p0, p1, p2}, Lcom/peel/ui/gt;->a(Landroid/view/ViewGroup;Lcom/peel/data/g;)V

    return-void
.end method

.method static synthetic a(Lcom/peel/ui/gt;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 113
    invoke-direct {p0, p1, p2}, Lcom/peel/ui/gt;->c(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic a(Lcom/peel/ui/gt;ZZ)V
    .locals 0

    .prologue
    .line 113
    invoke-direct {p0, p1, p2}, Lcom/peel/ui/gt;->a(ZZ)V

    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 3000
    iget-object v0, p0, Lcom/peel/ui/gt;->aM:Lcom/peel/control/h;

    if-nez v0, :cond_0

    .line 3001
    sget-object v0, Lcom/peel/ui/gt;->i:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "device is null cmd("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3034
    :goto_0
    return-void

    .line 3005
    :cond_0
    if-nez p1, :cond_1

    .line 3006
    sget-object v0, Lcom/peel/ui/gt;->i:Ljava/lang/String;

    const-string/jumbo v1, "null command not sent"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 3010
    :cond_1
    sget-object v0, Lcom/peel/ui/gt;->i:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "sending command to "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/peel/ui/gt;->aM:Lcom/peel/control/h;

    .line 3011
    invoke-virtual {v2}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v2

    invoke-virtual {v2}, Lcom/peel/data/g;->f()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/peel/ui/gt;->aM:Lcom/peel/control/h;

    .line 3013
    invoke-virtual {v2}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v2

    .line 3014
    invoke-virtual {v2}, Lcom/peel/data/g;->d()I

    move-result v2

    .line 3013
    invoke-static {v2}, Lcom/peel/util/bx;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 3010
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3017
    iget-object v0, p0, Lcom/peel/ui/gt;->be:Landroid/content/SharedPreferences;

    const-string/jumbo v1, "country"

    const-string/jumbo v2, ""

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "Japan"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 3018
    invoke-virtual {p0}, Lcom/peel/ui/gt;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/ui/gt;->aL:Lcom/peel/control/a;

    iget-object v2, p0, Lcom/peel/ui/gt;->aM:Lcom/peel/control/h;

    invoke-static {v0, v1, v2, p1}, Lcom/peel/util/bx;->a(Landroid/content/Context;Lcom/peel/control/a;Lcom/peel/control/h;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 3023
    :cond_2
    invoke-static {}, Lcom/peel/util/i;->c()Z

    move-result v0

    if-nez v0, :cond_3

    .line 3024
    iget-object v0, p0, Lcom/peel/ui/gt;->aM:Lcom/peel/control/h;

    invoke-virtual {v0, p1}, Lcom/peel/control/h;->c(Ljava/lang/String;)Z

    goto :goto_0

    .line 3026
    :cond_3
    sget-object v0, Lcom/peel/ui/gt;->i:Ljava/lang/String;

    const-string/jumbo v1, "sendCommand"

    new-instance v2, Lcom/peel/ui/iv;

    invoke-direct {v2, p0, p1}, Lcom/peel/ui/iv;-><init>(Lcom/peel/ui/gt;Ljava/lang/String;)V

    invoke-static {v0, v1, v2}, Lcom/peel/util/i;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/lang/Runnable;

    goto/16 :goto_0
.end method

.method private a(Z)V
    .locals 6

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 3561
    iget-object v0, p0, Lcom/peel/ui/gt;->bO:Landroid/widget/PopupWindow;

    if-nez v0, :cond_0

    .line 3562
    new-instance v0, Landroid/widget/PopupWindow;

    invoke-virtual {p0}, Lcom/peel/ui/gt;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/PopupWindow;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/peel/ui/gt;->bO:Landroid/widget/PopupWindow;

    .line 3563
    iget-object v0, p0, Lcom/peel/ui/gt;->bO:Landroid/widget/PopupWindow;

    invoke-virtual {v0, v3}, Landroid/widget/PopupWindow;->setOutsideTouchable(Z)V

    .line 3564
    iget-object v0, p0, Lcom/peel/ui/gt;->bO:Landroid/widget/PopupWindow;

    invoke-virtual {v0, v3}, Landroid/widget/PopupWindow;->setFocusable(Z)V

    .line 3566
    invoke-virtual {p0}, Lcom/peel/ui/gt;->n()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/peel/ui/fn;->overflow_menu_width:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    invoke-virtual {p0}, Lcom/peel/ui/gt;->n()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    invoke-static {v2, v0, v1}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    float-to-int v0, v0

    .line 3567
    iget-object v1, p0, Lcom/peel/ui/gt;->bO:Landroid/widget/PopupWindow;

    invoke-virtual {v1, v0}, Landroid/widget/PopupWindow;->setWidth(I)V

    .line 3568
    iget-object v0, p0, Lcom/peel/ui/gt;->bO:Landroid/widget/PopupWindow;

    const/4 v1, -0x2

    invoke-virtual {v0, v1}, Landroid/widget/PopupWindow;->setHeight(I)V

    .line 3570
    iget-object v0, p0, Lcom/peel/ui/gt;->bO:Landroid/widget/PopupWindow;

    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {v1, v2}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v0, v1}, Landroid/widget/PopupWindow;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 3578
    :cond_0
    iget-object v0, p0, Lcom/peel/ui/gt;->aO:Landroid/view/LayoutInflater;

    sget v1, Lcom/peel/ui/fq;->overflow_menu:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 3579
    sget v0, Lcom/peel/ui/fp;->list:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    .line 3580
    new-instance v2, Landroid/widget/ArrayAdapter;

    invoke-virtual {p0}, Lcom/peel/ui/gt;->m()Landroid/support/v4/app/ae;

    move-result-object v3

    sget v4, Lcom/peel/ui/fq;->menu_row:I

    iget-object v5, p0, Lcom/peel/ui/gt;->aj:[Ljava/lang/String;

    invoke-direct {v2, v3, v4, v5}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 3582
    new-instance v2, Lcom/peel/ui/jg;

    invoke-direct {v2, p0}, Lcom/peel/ui/jg;-><init>(Lcom/peel/ui/gt;)V

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 3626
    iget-object v0, p0, Lcom/peel/ui/gt;->bO:Landroid/widget/PopupWindow;

    invoke-virtual {v0, v1}, Landroid/widget/PopupWindow;->setContentView(Landroid/view/View;)V

    .line 3627
    new-instance v0, Lcom/peel/ui/jh;

    invoke-direct {v0, p0}, Lcom/peel/ui/jh;-><init>(Lcom/peel/ui/gt;)V

    invoke-virtual {v1, v0}, Landroid/view/View;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 3640
    iget-object v0, p0, Lcom/peel/ui/gt;->bO:Landroid/widget/PopupWindow;

    new-instance v1, Lcom/peel/ui/ji;

    invoke-direct {v1, p0}, Lcom/peel/ui/ji;-><init>(Lcom/peel/ui/gt;)V

    invoke-virtual {v0, v1}, Landroid/widget/PopupWindow;->setOnDismissListener(Landroid/widget/PopupWindow$OnDismissListener;)V

    .line 3646
    return-void
.end method

.method private a(ZZ)V
    .locals 10

    .prologue
    const-wide/16 v8, 0x15e

    const/high16 v2, 0x3f800000    # 1.0f

    const/high16 v4, -0x3cea0000    # -150.0f

    const/4 v6, 0x1

    const/4 v3, 0x0

    .line 703
    if-eqz p1, :cond_1

    .line 704
    invoke-virtual {p0}, Lcom/peel/ui/gt;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    const-string/jumbo v1, "remote_channel_guide_trigger"

    invoke-static {v0, v1}, Lcom/peel/util/dq;->b(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 705
    new-instance v0, Landroid/view/animation/AnimationSet;

    invoke-direct {v0, v6}, Landroid/view/animation/AnimationSet;-><init>(Z)V

    .line 706
    new-instance v1, Landroid/view/animation/AlphaAnimation;

    invoke-direct {v1, v3, v2}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 707
    invoke-virtual {v1, v8, v9}, Landroid/view/animation/AlphaAnimation;->setDuration(J)V

    .line 708
    new-instance v2, Landroid/view/animation/TranslateAnimation;

    invoke-direct {v2, v3, v3, v4, v3}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    .line 709
    const-wide/16 v4, 0x190

    invoke-virtual {v2, v4, v5}, Landroid/view/animation/TranslateAnimation;->setDuration(J)V

    .line 710
    invoke-virtual {v0, v1}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 711
    invoke-virtual {v0, v2}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 712
    new-instance v1, Lcom/peel/ui/hi;

    invoke-direct {v1, p0}, Lcom/peel/ui/hi;-><init>(Lcom/peel/ui/gt;)V

    invoke-virtual {v0, v1}, Landroid/view/animation/AnimationSet;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 732
    iget-object v1, p0, Lcom/peel/ui/gt;->bt:Landroid/widget/RelativeLayout;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 733
    iget-object v1, p0, Lcom/peel/ui/gt;->bt:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v0}, Landroid/widget/RelativeLayout;->startAnimation(Landroid/view/animation/Animation;)V

    .line 734
    iget-object v0, p0, Lcom/peel/ui/gt;->bu:Landroid/widget/ImageView;

    new-instance v1, Lcom/peel/ui/hk;

    invoke-direct {v1, p0}, Lcom/peel/ui/hk;-><init>(Lcom/peel/ui/gt;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 780
    :cond_0
    :goto_0
    return-void

    .line 744
    :cond_1
    if-eqz p2, :cond_2

    .line 745
    invoke-virtual {p0}, Lcom/peel/ui/gt;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    const-string/jumbo v1, "remote_channel_guide_trigger"

    invoke-static {v0, v1, v6}, Lcom/peel/util/dq;->a(Landroid/content/Context;Ljava/lang/String;Z)V

    .line 747
    :cond_2
    iget-object v0, p0, Lcom/peel/ui/gt;->bt:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 748
    new-instance v0, Landroid/view/animation/AnimationSet;

    invoke-direct {v0, v6}, Landroid/view/animation/AnimationSet;-><init>(Z)V

    .line 749
    new-instance v1, Landroid/view/animation/AlphaAnimation;

    invoke-direct {v1, v2, v3}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 750
    invoke-virtual {v1, v8, v9}, Landroid/view/animation/AlphaAnimation;->setDuration(J)V

    .line 751
    new-instance v2, Landroid/view/animation/TranslateAnimation;

    invoke-direct {v2, v3, v3, v3, v4}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    .line 752
    const-wide/16 v4, 0x190

    invoke-virtual {v2, v4, v5}, Landroid/view/animation/TranslateAnimation;->setDuration(J)V

    .line 753
    invoke-virtual {v0, v1}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 754
    invoke-virtual {v0, v2}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 755
    invoke-virtual {v0, v6}, Landroid/view/animation/AnimationSet;->setFillAfter(Z)V

    .line 756
    new-instance v1, Lcom/peel/ui/hl;

    invoke-direct {v1, p0}, Lcom/peel/ui/hl;-><init>(Lcom/peel/ui/gt;)V

    invoke-virtual {v0, v1}, Landroid/view/animation/AnimationSet;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 777
    iget-object v1, p0, Lcom/peel/ui/gt;->bt:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v0}, Landroid/widget/RelativeLayout;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0
.end method

.method static synthetic a(Lcom/peel/ui/gt;Z)Z
    .locals 0

    .prologue
    .line 113
    iput-boolean p1, p0, Lcom/peel/ui/gt;->bx:Z

    return p1
.end method

.method static synthetic aa(Lcom/peel/ui/gt;)[Ljava/lang/String;
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lcom/peel/ui/gt;->aj:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic ab(Lcom/peel/ui/gt;)Lcom/peel/widget/ag;
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lcom/peel/ui/gt;->ay:Lcom/peel/widget/ag;

    return-object v0
.end method

.method static synthetic ac()I
    .locals 1

    .prologue
    .line 113
    sget v0, Lcom/peel/ui/gt;->aA:I

    return v0
.end method

.method static synthetic ac(Lcom/peel/ui/gt;)Lcom/peel/widget/ag;
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lcom/peel/ui/gt;->bq:Lcom/peel/widget/ag;

    return-object v0
.end method

.method static synthetic ad()I
    .locals 2

    .prologue
    .line 113
    sget v0, Lcom/peel/ui/gt;->aA:I

    add-int/lit8 v1, v0, -0x1

    sput v1, Lcom/peel/ui/gt;->aA:I

    return v0
.end method

.method static synthetic ad(Lcom/peel/ui/gt;)Landroid/widget/PopupWindow;
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lcom/peel/ui/gt;->bP:Landroid/widget/PopupWindow;

    return-object v0
.end method

.method static synthetic ae()Ljava/util/List;
    .locals 1

    .prologue
    .line 113
    sget-object v0, Lcom/peel/ui/gt;->aw:Ljava/util/List;

    return-object v0
.end method

.method static synthetic af()I
    .locals 2

    .prologue
    .line 113
    sget v0, Lcom/peel/ui/gt;->aB:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/peel/ui/gt;->aB:I

    return v0
.end method

.method static synthetic ag()I
    .locals 1

    .prologue
    .line 113
    sget v0, Lcom/peel/ui/gt;->aB:I

    return v0
.end method

.method static synthetic ah()I
    .locals 2

    .prologue
    .line 113
    sget v0, Lcom/peel/ui/gt;->aB:I

    add-int/lit8 v1, v0, -0x1

    sput v1, Lcom/peel/ui/gt;->aB:I

    return v0
.end method

.method static synthetic ai()Ljava/util/List;
    .locals 1

    .prologue
    .line 113
    sget-object v0, Lcom/peel/ui/gt;->ax:Ljava/util/List;

    return-object v0
.end method

.method static synthetic aj()I
    .locals 2

    .prologue
    .line 113
    sget v0, Lcom/peel/ui/gt;->aC:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/peel/ui/gt;->aC:I

    return v0
.end method

.method static synthetic ak()I
    .locals 1

    .prologue
    .line 113
    sget v0, Lcom/peel/ui/gt;->aC:I

    return v0
.end method

.method static synthetic al()Ljava/util/List;
    .locals 1

    .prologue
    .line 113
    sget-object v0, Lcom/peel/ui/gt;->at:Ljava/util/List;

    return-object v0
.end method

.method private am()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1180
    iget-object v0, p0, Lcom/peel/ui/gt;->e:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setBackgroundResource(I)V

    .line 1182
    iget-object v0, p0, Lcom/peel/ui/gt;->f:Landroid/view/View;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    .line 1184
    iput-boolean v2, p0, Lcom/peel/ui/gt;->g:Z

    .line 1185
    return-void
.end method

.method private an()V
    .locals 10

    .prologue
    const/4 v8, 0x0

    const/4 v9, 0x1

    .line 1188
    iget-object v0, p0, Lcom/peel/ui/gt;->aL:Lcom/peel/control/a;

    if-nez v0, :cond_1

    .line 1325
    :cond_0
    :goto_0
    return-void

    .line 1197
    :cond_1
    const/4 v2, 0x0

    .line 1199
    iget-object v0, p0, Lcom/peel/ui/gt;->aL:Lcom/peel/control/a;

    invoke-virtual {v0, v9}, Lcom/peel/control/a;->a(I)Lcom/peel/control/h;

    move-result-object v1

    .line 1200
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1203
    invoke-virtual {v1}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/data/g;->a()Ljava/util/Map;

    move-result-object v3

    .line 1205
    invoke-virtual {v1}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/data/g;->f()Ljava/lang/String;

    move-result-object v0

    .line 1207
    invoke-virtual {v1}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v4

    invoke-virtual {v4}, Lcom/peel/data/g;->d()I

    move-result v4

    packed-switch v4, :pswitch_data_0

    :pswitch_0
    move v5, v8

    move v7, v8

    move v6, v8

    move v3, v9

    move v4, v8

    .line 1318
    :goto_1
    iget-object v0, p0, Lcom/peel/ui/gt;->aR:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->removeAllViews()V

    .line 1320
    if-eqz v2, :cond_16

    .line 1321
    iget-object v1, p0, Lcom/peel/ui/gt;->aR:Landroid/widget/FrameLayout;

    move-object v0, p0

    invoke-direct/range {v0 .. v9}, Lcom/peel/ui/gt;->a(Landroid/view/ViewGroup;Lcom/peel/util/bw;ZZZZZZZ)V

    goto :goto_0

    .line 1210
    :pswitch_1
    sget-object v2, Lcom/peel/util/bw;->k:Lcom/peel/util/bw;

    .line 1214
    const-string/jumbo v3, "Samsung"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_17

    .line 1215
    sget-object v2, Lcom/peel/util/bw;->l:Lcom/peel/util/bw;

    move v5, v8

    move v7, v8

    move v6, v8

    move v3, v9

    move v4, v9

    goto :goto_1

    .line 1221
    :pswitch_2
    sget-object v2, Lcom/peel/util/bw;->g:Lcom/peel/util/bw;

    move v5, v8

    move v7, v8

    move v6, v8

    move v3, v9

    move v4, v9

    .line 1224
    goto :goto_1

    .line 1230
    :pswitch_3
    sget-object v2, Lcom/peel/util/bw;->d:Lcom/peel/util/bw;

    move v5, v8

    move v7, v8

    move v6, v8

    move v3, v9

    move v4, v8

    move v8, v9

    .line 1234
    goto :goto_1

    .line 1239
    :pswitch_4
    const-string/jumbo v2, "roku"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1240
    sget-object v2, Lcom/peel/util/bw;->i:Lcom/peel/util/bw;

    move v5, v9

    move v7, v8

    move v6, v8

    move v3, v9

    move v4, v8

    goto :goto_1

    .line 1241
    :cond_2
    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v2, "apple"

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1242
    sget-object v2, Lcom/peel/util/bw;->b:Lcom/peel/util/bw;

    move v5, v9

    move v7, v8

    move v6, v8

    move v3, v9

    move v4, v8

    goto :goto_1

    .line 1244
    :cond_3
    sget-object v2, Lcom/peel/util/bw;->j:Lcom/peel/util/bw;

    move v5, v9

    move v7, v8

    move v6, v8

    move v3, v9

    move v4, v8

    .line 1246
    goto :goto_1

    .line 1251
    :pswitch_5
    sget-object v2, Lcom/peel/util/bw;->m:Lcom/peel/util/bw;

    move v5, v9

    move v7, v8

    move v6, v8

    move v3, v9

    move v4, v8

    .line 1252
    goto :goto_1

    .line 1254
    :pswitch_6
    sget-object v2, Lcom/peel/util/bw;->a:Lcom/peel/util/bw;

    .line 1261
    :try_start_0
    const-string/jumbo v0, "T_16"

    invoke-interface {v3, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    const-string/jumbo v4, "type"

    invoke-interface {v0, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/peel/ui/gt;->bi:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1266
    :goto_2
    iget-object v0, p0, Lcom/peel/ui/gt;->bi:Ljava/lang/String;

    const-string/jumbo v4, "+"

    invoke-virtual {v0, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/peel/ui/gt;->bf:Z

    .line 1267
    invoke-virtual {v1}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/data/g;->a()Ljava/util/Map;

    move-result-object v0

    const-string/jumbo v4, "16_F_A_C"

    invoke-interface {v0, v4}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1268
    iput-boolean v9, p0, Lcom/peel/ui/gt;->bg:Z

    .line 1270
    :cond_4
    sget-object v0, Lcom/peel/ui/gt;->i:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "\ncombo code rule: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/peel/ui/gt;->bi:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, " -- use combo codes: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-boolean v5, p0, Lcom/peel/ui/gt;->bf:Z

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "use brute force: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-boolean v5, p0, Lcom/peel/ui/gt;->bg:Z

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1272
    iget-boolean v0, p0, Lcom/peel/ui/gt;->bf:Z

    if-nez v0, :cond_5

    iget-boolean v0, p0, Lcom/peel/ui/gt;->bg:Z

    if-eqz v0, :cond_6

    .line 1273
    :cond_5
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/peel/ui/gt;->bh:Ljava/util/Map;

    .line 1278
    :cond_6
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/peel/ui/gt;->av:Ljava/util/List;

    .line 1279
    const/16 v0, 0x10

    :goto_3
    const/16 v4, 0x1e

    if-gt v0, v4, :cond_9

    .line 1280
    iget-boolean v4, p0, Lcom/peel/ui/gt;->bg:Z

    if-nez v4, :cond_7

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "T_"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_8

    .line 1281
    :cond_7
    sget-object v4, Lcom/peel/ui/gt;->av:Ljava/util/List;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "T_"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1279
    :cond_8
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 1262
    :catch_0
    move-exception v0

    .line 1263
    const-string/jumbo v0, "Full_Repeat"

    iput-object v0, p0, Lcom/peel/ui/gt;->bi:Ljava/lang/String;

    goto/16 :goto_2

    .line 1286
    :cond_9
    sget-object v0, Lcom/peel/ui/gt;->av:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_b

    .line 1287
    const-string/jumbo v0, "UP"

    invoke-interface {v3, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 1288
    sget-object v0, Lcom/peel/ui/gt;->av:Ljava/util/List;

    const-string/jumbo v4, "UP"

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1289
    :cond_a
    const-string/jumbo v0, "Down"

    invoke-interface {v3, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 1290
    sget-object v0, Lcom/peel/ui/gt;->av:Ljava/util/List;

    const-string/jumbo v4, "Down"

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1294
    :cond_b
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/peel/ui/gt;->aw:Ljava/util/List;

    .line 1295
    iget-boolean v0, p0, Lcom/peel/ui/gt;->bg:Z

    if-nez v0, :cond_c

    const-string/jumbo v0, "FAN_LOW"

    invoke-interface {v3, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 1296
    :cond_c
    sget-object v0, Lcom/peel/ui/gt;->aw:Ljava/util/List;

    const-string/jumbo v4, "FAN_LOW"

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1297
    :cond_d
    iget-boolean v0, p0, Lcom/peel/ui/gt;->bg:Z

    if-nez v0, :cond_e

    const-string/jumbo v0, "FAN_MED"

    invoke-interface {v3, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 1298
    :cond_e
    sget-object v0, Lcom/peel/ui/gt;->aw:Ljava/util/List;

    const-string/jumbo v4, "FAN_MED"

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1299
    :cond_f
    iget-boolean v0, p0, Lcom/peel/ui/gt;->bg:Z

    if-nez v0, :cond_10

    const-string/jumbo v0, "FAN_HIGH"

    invoke-interface {v3, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_11

    .line 1300
    :cond_10
    sget-object v0, Lcom/peel/ui/gt;->aw:Ljava/util/List;

    const-string/jumbo v4, "FAN_HIGH"

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1302
    :cond_11
    iget-boolean v0, p0, Lcom/peel/ui/gt;->bg:Z

    if-eqz v0, :cond_12

    sget-object v0, Lcom/peel/ui/gt;->aw:Ljava/util/List;

    const-string/jumbo v4, "FAN_AUTO"

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1305
    :cond_12
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/peel/ui/gt;->ax:Ljava/util/List;

    .line 1306
    invoke-interface {v3}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    .line 1307
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_13
    :goto_4
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_14

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1308
    const-string/jumbo v4, "Mode_"

    invoke-virtual {v0, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_13

    .line 1309
    sget-object v4, Lcom/peel/ui/gt;->ax:Ljava/util/List;

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 1311
    :cond_14
    iget-boolean v0, p0, Lcom/peel/ui/gt;->bg:Z

    if-eqz v0, :cond_15

    .line 1312
    sget-object v0, Lcom/peel/ui/gt;->ax:Ljava/util/List;

    const-string/jumbo v3, "Mode_Cool"

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1313
    sget-object v0, Lcom/peel/ui/gt;->ax:Ljava/util/List;

    const-string/jumbo v3, "Mode_Heat"

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_15
    move v5, v8

    move v7, v9

    move v6, v9

    move v3, v8

    move v4, v8

    goto/16 :goto_1

    .line 1323
    :cond_16
    sget-object v0, Lcom/peel/ui/gt;->i:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "no suitable layout for this device type: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v1}, Lcom/peel/control/h;->e()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v1}, Lcom/peel/control/h;->f()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_17
    move v5, v8

    move v7, v8

    move v6, v8

    move v3, v9

    move v4, v9

    goto/16 :goto_1

    .line 1207
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_3
        :pswitch_5
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_6
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method private ao()V
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 2555
    sget-object v1, Lcom/peel/ui/gt;->aP:Landroid/app/Dialog;

    if-eqz v1, :cond_0

    sget-object v1, Lcom/peel/ui/gt;->aP:Landroid/app/Dialog;

    invoke-virtual {v1}, Landroid/app/Dialog;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2556
    sget-object v1, Lcom/peel/ui/gt;->aP:Landroid/app/Dialog;

    invoke-virtual {v1}, Landroid/app/Dialog;->dismiss()V

    .line 2558
    :cond_0
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 2559
    iget-object v2, p0, Lcom/peel/ui/gt;->c:Landroid/os/Bundle;

    const-string/jumbo v3, "fav_ch_bundle"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v2

    .line 2560
    if-eqz v2, :cond_1

    invoke-virtual {v2}, Landroid/os/Bundle;->size()I

    move-result v3

    if-lez v3, :cond_1

    .line 2561
    const-string/jumbo v3, "favChannelIds"

    new-instance v4, Ljava/util/ArrayList;

    invoke-virtual {v2}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-direct {v4, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-virtual {v1, v3, v4}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 2562
    :cond_1
    iget-object v2, p0, Lcom/peel/ui/gt;->c:Landroid/os/Bundle;

    const-string/jumbo v3, "refresh"

    invoke-virtual {v2, v3, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2563
    invoke-virtual {p0}, Lcom/peel/ui/gt;->m()Landroid/support/v4/app/ae;

    move-result-object v2

    const-class v3, Lcom/peel/ui/v;

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v1}, Lcom/peel/d/e;->c(Landroid/support/v4/app/ae;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 2565
    iget-object v1, p0, Lcom/peel/ui/gt;->c:Landroid/os/Bundle;

    const-string/jumbo v2, "start_numberpad"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2567
    invoke-static {}, Lcom/peel/util/a/f;->a()Lcom/peel/util/a/f;

    move-result-object v1

    sget-object v2, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v2}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v2

    if-nez v2, :cond_2

    :goto_0
    const/16 v2, 0x417

    const/16 v3, 0x7e7

    invoke-virtual {v1, v0, v2, v3}, Lcom/peel/util/a/f;->a(III)V

    .line 2568
    return-void

    .line 2567
    :cond_2
    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v0}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/data/at;->f()I

    move-result v0

    goto :goto_0
.end method

.method private ap()V
    .locals 3

    .prologue
    .line 3203
    sget-object v0, Lcom/peel/ui/gt;->i:Ljava/lang/String;

    const-string/jumbo v1, ""

    new-instance v2, Lcom/peel/ui/iz;

    invoke-direct {v2, p0}, Lcom/peel/ui/iz;-><init>(Lcom/peel/ui/gt;)V

    invoke-static {v0, v1, v2}, Lcom/peel/util/i;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/lang/Runnable;

    .line 3359
    return-void
.end method

.method private aq()V
    .locals 4

    .prologue
    .line 3362
    iget-object v0, p0, Lcom/peel/ui/gt;->aL:Lcom/peel/control/a;

    invoke-static {v0}, Lcom/peel/util/bx;->a(Lcom/peel/control/a;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/ui/gt;->am:Ljava/util/List;

    .line 3364
    iget-object v0, p0, Lcom/peel/ui/gt;->an:Landroid/util/SparseArray;

    if-nez v0, :cond_1

    .line 3365
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/peel/ui/gt;->an:Landroid/util/SparseArray;

    .line 3370
    :goto_0
    const/4 v0, 0x0

    move v2, v0

    :goto_1
    iget-object v0, p0, Lcom/peel/ui/gt;->am:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_3

    .line 3371
    iget-object v0, p0, Lcom/peel/ui/gt;->am:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/control/h;

    .line 3372
    iget-object v1, p0, Lcom/peel/ui/gt;->an:Landroid/util/SparseArray;

    invoke-virtual {v1, v2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 3374
    if-nez v1, :cond_0

    .line 3375
    iget-object v1, p0, Lcom/peel/ui/gt;->an:Landroid/util/SparseArray;

    const-string/jumbo v3, "Power"

    invoke-static {v0, v3}, Lcom/peel/util/bx;->a(Lcom/peel/control/h;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string/jumbo v0, "Power"

    :goto_2
    invoke-virtual {v1, v2, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 3370
    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 3367
    :cond_1
    iget-object v0, p0, Lcom/peel/ui/gt;->an:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->clear()V

    goto :goto_0

    .line 3375
    :cond_2
    const-string/jumbo v0, "PowerOn"

    goto :goto_2

    .line 3378
    :cond_3
    return-void
.end method

.method private ar()V
    .locals 3

    .prologue
    .line 3381
    sget-object v0, Lcom/peel/ui/gt;->i:Ljava/lang/String;

    const-string/jumbo v1, ""

    new-instance v2, Lcom/peel/ui/jc;

    invoke-direct {v2, p0}, Lcom/peel/ui/jc;-><init>(Lcom/peel/ui/gt;)V

    invoke-static {v0, v1, v2}, Lcom/peel/util/i;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/lang/Runnable;

    .line 3428
    return-void
.end method

.method private as()V
    .locals 7

    .prologue
    .line 3463
    iget-object v0, p0, Lcom/peel/ui/gt;->bd:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 3465
    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v0}, Lcom/peel/control/am;->e()[Lcom/peel/control/h;

    move-result-object v1

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_2

    aget-object v3, v1, v0

    .line 3466
    const/4 v4, 0x5

    invoke-virtual {v3}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v5

    invoke-virtual {v5}, Lcom/peel/data/g;->d()I

    move-result v5

    if-eq v4, v5, :cond_0

    const/4 v4, 0x1

    invoke-virtual {v3}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v5

    invoke-virtual {v5}, Lcom/peel/data/g;->d()I

    move-result v5

    if-ne v4, v5, :cond_1

    .line 3467
    :cond_0
    sget-object v4, Lcom/peel/ui/gt;->i:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "\n\nadding valid audio device: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v3}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v6

    invoke-virtual {v6}, Lcom/peel/data/g;->f()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, " -- "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v3}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v6

    invoke-virtual {v6}, Lcom/peel/data/g;->d()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3468
    iget-object v4, p0, Lcom/peel/ui/gt;->bd:Ljava/util/ArrayList;

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3465
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 3471
    :cond_2
    return-void
.end method

.method private at()V
    .locals 12

    .prologue
    const/4 v11, 0x0

    const/4 v5, 0x1

    .line 3649
    invoke-virtual {p0}, Lcom/peel/ui/gt;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    const-string/jumbo v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/ae;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 3650
    if-nez v0, :cond_1

    .line 3702
    :cond_0
    :goto_0
    return-void

    .line 3653
    :cond_1
    sget v1, Lcom/peel/ui/fq;->control_change_room:I

    invoke-virtual {v0, v1, v11}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v7

    .line 3654
    sget v0, Lcom/peel/ui/fp;->activities_lv:I

    invoke-virtual {v7, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Landroid/widget/ListView;

    .line 3656
    invoke-static {}, Lcom/peel/content/a;->a()Lcom/peel/data/ContentRoom;

    move-result-object v8

    .line 3657
    if-eqz v8, :cond_0

    .line 3660
    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v8}, Lcom/peel/data/ContentRoom;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/peel/control/am;->a(Ljava/lang/String;)Lcom/peel/control/RoomControl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/control/RoomControl;->c()[Lcom/peel/control/a;

    move-result-object v1

    .line 3661
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 3662
    array-length v2, v1

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v2, :cond_4

    aget-object v4, v1, v0

    .line 3663
    if-eqz v4, :cond_2

    invoke-virtual {v4, v5}, Lcom/peel/control/a;->a(I)Lcom/peel/control/h;

    move-result-object v9

    if-eqz v9, :cond_2

    const/4 v9, 0x5

    invoke-virtual {v4, v5}, Lcom/peel/control/a;->a(I)Lcom/peel/control/h;

    move-result-object v10

    invoke-virtual {v10}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v10

    invoke-virtual {v10}, Lcom/peel/data/g;->d()I

    move-result v10

    if-ne v9, v10, :cond_3

    .line 3662
    :cond_2
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 3665
    :cond_3
    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 3668
    :cond_4
    new-instance v0, Lcom/peel/ui/ah;

    invoke-virtual {p0}, Lcom/peel/ui/gt;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    sget v2, Lcom/peel/ui/fq;->device_power_row:I

    sget-object v4, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v4}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v4

    invoke-virtual {v4}, Lcom/peel/control/RoomControl;->d()Lcom/peel/control/a;

    move-result-object v4

    invoke-direct/range {v0 .. v5}, Lcom/peel/ui/ah;-><init>(Landroid/content/Context;ILjava/util/List;Lcom/peel/control/a;Z)V

    invoke-virtual {v6, v0}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 3670
    new-instance v0, Lcom/peel/widget/ag;

    invoke-virtual {p0}, Lcom/peel/ui/gt;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/peel/widget/ag;-><init>(Landroid/content/Context;)V

    sget v1, Lcom/peel/ui/ft;->label_switch_remote_activity:I

    invoke-virtual {v0, v1}, Lcom/peel/widget/ag;->a(I)Lcom/peel/widget/ag;

    move-result-object v0

    invoke-virtual {v0, v7}, Lcom/peel/widget/ag;->a(Landroid/view/View;)Lcom/peel/widget/ag;

    move-result-object v0

    sget v1, Lcom/peel/ui/ft;->cancel:I

    invoke-virtual {v0, v1, v11}, Lcom/peel/widget/ag;->c(ILandroid/content/DialogInterface$OnClickListener;)Lcom/peel/widget/ag;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/ui/gt;->bq:Lcom/peel/widget/ag;

    .line 3672
    new-instance v0, Lcom/peel/ui/jj;

    invoke-direct {v0, p0, v8}, Lcom/peel/ui/jj;-><init>(Lcom/peel/ui/gt;Lcom/peel/data/ContentRoom;)V

    invoke-virtual {v6, v0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 3701
    iget-object v0, p0, Lcom/peel/ui/gt;->bq:Lcom/peel/widget/ag;

    iget-object v1, p0, Lcom/peel/ui/gt;->bq:Lcom/peel/widget/ag;

    invoke-virtual {v0, v1}, Lcom/peel/widget/ag;->a(Lcom/peel/widget/ag;)Lcom/peel/widget/ag;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/widget/ag;->show()V

    goto/16 :goto_0
.end method

.method private au()V
    .locals 9

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 3706
    iget-object v0, p0, Lcom/peel/ui/gt;->az:Lcom/peel/widget/ag;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/peel/ui/gt;->az:Lcom/peel/widget/ag;

    invoke-virtual {v0}, Lcom/peel/widget/ag;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3707
    iget-object v0, p0, Lcom/peel/ui/gt;->az:Lcom/peel/widget/ag;

    invoke-virtual {v0}, Lcom/peel/widget/ag;->dismiss()V

    .line 3710
    :cond_0
    iget-object v0, p0, Lcom/peel/ui/gt;->az:Lcom/peel/widget/ag;

    if-nez v0, :cond_1

    .line 3711
    new-instance v0, Lcom/peel/widget/ag;

    invoke-virtual {p0}, Lcom/peel/ui/gt;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/peel/widget/ag;-><init>(Landroid/content/Context;)V

    sget v1, Lcom/peel/ui/ft;->label_device_list:I

    .line 3712
    invoke-virtual {v0, v1}, Lcom/peel/widget/ag;->a(I)Lcom/peel/widget/ag;

    move-result-object v0

    sget v1, Lcom/peel/ui/ft;->done:I

    .line 3713
    invoke-virtual {v0, v1, v3}, Lcom/peel/widget/ag;->c(ILandroid/content/DialogInterface$OnClickListener;)Lcom/peel/widget/ag;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/ui/gt;->az:Lcom/peel/widget/ag;

    .line 3718
    :goto_0
    iget-object v0, p0, Lcom/peel/ui/gt;->aO:Landroid/view/LayoutInflater;

    sget v1, Lcom/peel/ui/fq;->settings_adddevice_activities_power:I

    invoke-virtual {v0, v1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .line 3719
    sget v0, Lcom/peel/ui/fp;->activities_lv:I

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    .line 3720
    sget v1, Lcom/peel/ui/fp;->msg:I

    invoke-virtual {v3, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 3721
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 3722
    sget v4, Lcom/peel/ui/ft;->power_dlg_msg:I

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setText(I)V

    .line 3724
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 3725
    iget-object v1, p0, Lcom/peel/ui/gt;->aL:Lcom/peel/control/a;

    invoke-virtual {v1}, Lcom/peel/control/a;->e()[Lcom/peel/control/h;

    move-result-object v5

    array-length v6, v5

    move v1, v2

    :goto_1
    if-ge v1, v6, :cond_3

    aget-object v2, v5, v1

    .line 3727
    invoke-virtual {v2}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v7

    invoke-virtual {v7}, Lcom/peel/data/g;->d()I

    move-result v7

    const/4 v8, 0x6

    if-ne v7, v8, :cond_2

    .line 3725
    :goto_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 3715
    :cond_1
    iget-object v0, p0, Lcom/peel/ui/gt;->az:Lcom/peel/widget/ag;

    invoke-virtual {v0}, Lcom/peel/widget/ag;->c()V

    goto :goto_0

    .line 3729
    :cond_2
    invoke-interface {v4, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 3732
    :cond_3
    new-instance v1, Lcom/peel/ui/jl;

    invoke-virtual {p0}, Lcom/peel/ui/gt;->m()Landroid/support/v4/app/ae;

    move-result-object v2

    sget v5, Lcom/peel/ui/fq;->device_power_row:I

    invoke-direct {v1, p0, v2, v5, v4}, Lcom/peel/ui/jl;-><init>(Lcom/peel/ui/gt;Landroid/content/Context;ILjava/util/List;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 3753
    new-instance v1, Lcom/peel/ui/jm;

    invoke-direct {v1, p0}, Lcom/peel/ui/jm;-><init>(Lcom/peel/ui/gt;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 3773
    iget-object v0, p0, Lcom/peel/ui/gt;->az:Lcom/peel/widget/ag;

    invoke-virtual {v0, v3}, Lcom/peel/widget/ag;->a(Landroid/view/View;)Lcom/peel/widget/ag;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/ui/gt;->az:Lcom/peel/widget/ag;

    invoke-virtual {v0, v1}, Lcom/peel/widget/ag;->a(Lcom/peel/widget/ag;)Lcom/peel/widget/ag;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/widget/ag;->show()V

    .line 3774
    return-void
.end method

.method static synthetic b(I)I
    .locals 0

    .prologue
    .line 113
    sput p0, Lcom/peel/ui/gt;->aA:I

    return p0
.end method

.method static synthetic b(Lcom/peel/ui/gt;J)J
    .locals 1

    .prologue
    .line 113
    iput-wide p1, p0, Lcom/peel/ui/gt;->aG:J

    return-wide p1
.end method

.method static synthetic b(Lcom/peel/ui/gt;Lcom/peel/widget/ag;)Lcom/peel/widget/ag;
    .locals 0

    .prologue
    .line 113
    iput-object p1, p0, Lcom/peel/ui/gt;->aX:Lcom/peel/widget/ag;

    return-object p1
.end method

.method static synthetic b(Lcom/peel/ui/gt;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lcom/peel/ui/gt;->aN:Ljava/lang/String;

    return-object v0
.end method

.method private b(Landroid/view/View;)V
    .locals 4

    .prologue
    .line 1169
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/peel/ui/gt;->g:Z

    .line 1170
    iput-object p1, p0, Lcom/peel/ui/gt;->e:Landroid/view/View;

    .line 1171
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    iput-object v0, p0, Lcom/peel/ui/gt;->f:Landroid/view/View;

    .line 1173
    iget-object v0, p0, Lcom/peel/ui/gt;->f:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    .line 1174
    iget-object v0, p0, Lcom/peel/ui/gt;->e:Landroid/view/View;

    sget v1, Lcom/peel/ui/fm;->common_signin_btn_dark_text_disabled:I

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    .line 1176
    iget-object v0, p0, Lcom/peel/ui/gt;->h:Landroid/os/Handler;

    iget-object v1, p0, Lcom/peel/ui/gt;->aK:Ljava/lang/Runnable;

    const-wide/16 v2, 0xbb8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 1177
    return-void
.end method

.method static synthetic b(Lcom/peel/ui/gt;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 113
    invoke-direct {p0, p1}, Lcom/peel/ui/gt;->d(Landroid/view/View;)V

    return-void
.end method

.method static synthetic b(Lcom/peel/ui/gt;Lcom/peel/control/a;)V
    .locals 0

    .prologue
    .line 113
    invoke-direct {p0, p1}, Lcom/peel/ui/gt;->a(Lcom/peel/control/a;)V

    return-void
.end method

.method static synthetic b(Lcom/peel/ui/gt;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 113
    invoke-direct {p0, p1}, Lcom/peel/ui/gt;->a(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic b(Lcom/peel/ui/gt;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 113
    invoke-direct {p0, p1, p2}, Lcom/peel/ui/gt;->d(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private b(Ljava/lang/String;)V
    .locals 8

    .prologue
    const/16 v7, 0xa

    const/4 v1, 0x1

    const/4 v3, 0x0

    .line 3037
    sget-object v0, Lcom/peel/ui/gt;->i:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "in sendCommand: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3038
    iget-object v0, p0, Lcom/peel/ui/gt;->aL:Lcom/peel/control/a;

    if-nez v0, :cond_1

    .line 3039
    sget-object v0, Lcom/peel/ui/gt;->i:Ljava/lang/String;

    const-string/jumbo v1, "activity NULL"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3143
    :cond_0
    :goto_0
    return-void

    .line 3043
    :cond_1
    if-nez p1, :cond_2

    .line 3044
    sget-object v0, Lcom/peel/ui/gt;->i:Ljava/lang/String;

    const-string/jumbo v1, "null command not sent"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 3049
    :cond_2
    iget-object v0, p0, Lcom/peel/ui/gt;->be:Landroid/content/SharedPreferences;

    const-string/jumbo v2, "country"

    const-string/jumbo v4, ""

    invoke-interface {v0, v2, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v2, "Japan"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 3050
    invoke-virtual {p0}, Lcom/peel/ui/gt;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    iget-object v2, p0, Lcom/peel/ui/gt;->aL:Lcom/peel/control/a;

    invoke-static {v0, v2, p1}, Lcom/peel/util/bx;->a(Landroid/content/Context;Lcom/peel/control/a;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 3055
    :cond_3
    const-string/jumbo v0, "Input"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 3056
    iget-object v0, p0, Lcom/peel/ui/gt;->aL:Lcom/peel/control/a;

    invoke-virtual {v0}, Lcom/peel/control/a;->e()[Lcom/peel/control/h;

    move-result-object v2

    .line 3057
    if-eqz v2, :cond_7

    .line 3058
    array-length v4, v2

    move v0, v3

    :goto_1
    if-ge v0, v4, :cond_7

    aget-object v5, v2, v0

    .line 3059
    invoke-virtual {v5}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v6

    invoke-virtual {v6}, Lcom/peel/data/g;->d()I

    move-result v6

    .line 3060
    if-eq v7, v6, :cond_4

    if-ne v1, v6, :cond_6

    .line 3061
    :cond_4
    invoke-static {}, Lcom/peel/util/i;->c()Z

    move-result v0

    if-nez v0, :cond_5

    .line 3062
    const-string/jumbo v0, "Input"

    invoke-virtual {v5, v0}, Lcom/peel/control/h;->c(Ljava/lang/String;)Z

    goto :goto_0

    .line 3064
    :cond_5
    sget-object v0, Lcom/peel/ui/gt;->i:Ljava/lang/String;

    const-string/jumbo v1, "sendCommand"

    new-instance v2, Lcom/peel/ui/iw;

    invoke-direct {v2, p0, v5, p1}, Lcom/peel/ui/iw;-><init>(Lcom/peel/ui/gt;Lcom/peel/control/h;Ljava/lang/String;)V

    invoke-static {v0, v1, v2}, Lcom/peel/util/i;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/lang/Runnable;

    goto :goto_0

    .line 3058
    :cond_6
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 3076
    :cond_7
    sget-object v0, Lcom/peel/ui/gt;->au:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_f

    iget-object v0, p0, Lcom/peel/ui/gt;->aL:Lcom/peel/control/a;

    invoke-virtual {v0, v3}, Lcom/peel/control/a;->a(I)Lcom/peel/control/h;

    move-result-object v0

    if-nez v0, :cond_f

    .line 3077
    invoke-direct {p0}, Lcom/peel/ui/gt;->as()V

    .line 3078
    iget-object v0, p0, Lcom/peel/ui/gt;->bd:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-le v0, v1, :cond_8

    .line 3079
    invoke-direct {p0}, Lcom/peel/ui/gt;->ap()V

    goto/16 :goto_0

    .line 3080
    :cond_8
    iget-object v0, p0, Lcom/peel/ui/gt;->bd:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ne v0, v1, :cond_d

    .line 3081
    iget-object v2, p0, Lcom/peel/ui/gt;->aL:Lcom/peel/control/a;

    iget-object v0, p0, Lcom/peel/ui/gt;->bd:Ljava/util/ArrayList;

    .line 3082
    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/control/h;

    invoke-virtual {v2, v0}, Lcom/peel/control/a;->b(Lcom/peel/control/h;)[Ljava/lang/Integer;

    move-result-object v0

    .line 3084
    if-eqz v0, :cond_9

    array-length v0, v0

    if-nez v0, :cond_a

    .line 3085
    :cond_9
    new-array v0, v1, [Ljava/lang/Integer;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v3

    move-object v2, v0

    .line 3094
    :goto_2
    iget-object v0, p0, Lcom/peel/ui/gt;->aL:Lcom/peel/control/a;

    invoke-virtual {v0}, Lcom/peel/control/a;->e()[Lcom/peel/control/h;

    move-result-object v5

    array-length v6, v5

    move v4, v3

    :goto_3
    if-ge v4, v6, :cond_12

    aget-object v0, v5, v4

    .line 3095
    invoke-virtual {v0}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/data/g;->b()Ljava/lang/String;

    move-result-object v7

    iget-object v0, p0, Lcom/peel/ui/gt;->bd:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/control/h;

    invoke-virtual {v0}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/data/g;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 3100
    :goto_4
    if-eqz v1, :cond_c

    .line 3101
    iget-object v4, p0, Lcom/peel/ui/gt;->aL:Lcom/peel/control/a;

    iget-object v0, p0, Lcom/peel/ui/gt;->bd:Ljava/util/ArrayList;

    .line 3102
    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/control/h;

    iget-object v5, p0, Lcom/peel/ui/gt;->aL:Lcom/peel/control/a;

    iget-object v1, p0, Lcom/peel/ui/gt;->bd:Ljava/util/ArrayList;

    .line 3103
    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/peel/control/h;

    invoke-virtual {v5, v1}, Lcom/peel/control/a;->a(Lcom/peel/control/h;)Ljava/lang/String;

    move-result-object v1

    .line 3101
    invoke-virtual {v4, v0, v1, v2}, Lcom/peel/control/a;->b(Lcom/peel/control/h;Ljava/lang/String;[Ljava/lang/Integer;)V

    .line 3109
    :goto_5
    invoke-virtual {p0}, Lcom/peel/ui/gt;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    sget-object v1, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v1}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/peel/util/bx;->a(Landroid/content/Context;Lcom/peel/control/RoomControl;)V

    .line 3111
    sget-object v0, Lcom/peel/ui/gt;->i:Ljava/lang/String;

    const-string/jumbo v1, "sendCommand"

    new-instance v2, Lcom/peel/ui/ix;

    invoke-direct {v2, p0, p1}, Lcom/peel/ui/ix;-><init>(Lcom/peel/ui/gt;Ljava/lang/String;)V

    invoke-static {v0, v1, v2}, Lcom/peel/util/i;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/lang/Runnable;

    goto/16 :goto_0

    .line 3087
    :cond_a
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Integer;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v3

    .line 3088
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    move-object v2, v0

    goto :goto_2

    .line 3094
    :cond_b
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_3

    .line 3106
    :cond_c
    iget-object v1, p0, Lcom/peel/ui/gt;->aL:Lcom/peel/control/a;

    iget-object v0, p0, Lcom/peel/ui/gt;->bd:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/control/h;

    const/4 v3, 0x0

    invoke-virtual {v1, v0, v3, v2}, Lcom/peel/control/a;->a(Lcom/peel/control/h;Ljava/lang/String;[Ljava/lang/Integer;)Z

    goto :goto_5

    .line 3120
    :cond_d
    iget-object v0, p0, Lcom/peel/ui/gt;->aL:Lcom/peel/control/a;

    invoke-virtual {v0}, Lcom/peel/control/a;->e()[Lcom/peel/control/h;

    move-result-object v2

    array-length v4, v2

    move v0, v3

    :goto_6
    if-ge v0, v4, :cond_11

    aget-object v5, v2, v0

    .line 3121
    invoke-virtual {v5}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v5

    invoke-virtual {v5}, Lcom/peel/data/g;->d()I

    move-result v5

    if-ne v5, v7, :cond_e

    move v0, v1

    .line 3126
    :goto_7
    if-eqz v0, :cond_0

    .line 3127
    invoke-direct {p0}, Lcom/peel/ui/gt;->ar()V

    goto/16 :goto_0

    .line 3120
    :cond_e
    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    .line 3132
    :cond_f
    invoke-static {}, Lcom/peel/util/i;->c()Z

    move-result v0

    if-nez v0, :cond_10

    .line 3133
    iget-object v0, p0, Lcom/peel/ui/gt;->aL:Lcom/peel/control/a;

    invoke-virtual {v0, p1}, Lcom/peel/control/a;->b(Ljava/lang/String;)Z

    goto/16 :goto_0

    .line 3135
    :cond_10
    sget-object v0, Lcom/peel/ui/gt;->i:Ljava/lang/String;

    const-string/jumbo v1, "sendCommand"

    new-instance v2, Lcom/peel/ui/iy;

    invoke-direct {v2, p0, p1}, Lcom/peel/ui/iy;-><init>(Lcom/peel/ui/gt;Ljava/lang/String;)V

    invoke-static {v0, v1, v2}, Lcom/peel/util/i;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/lang/Runnable;

    goto/16 :goto_0

    :cond_11
    move v0, v3

    goto :goto_7

    :cond_12
    move v1, v3

    goto/16 :goto_4
.end method

.method private b(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 2977
    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v0, p2}, Lcom/peel/control/am;->c(Ljava/lang/String;)Lcom/peel/control/h;

    move-result-object v0

    .line 2980
    iget-object v1, p0, Lcom/peel/ui/gt;->be:Landroid/content/SharedPreferences;

    const-string/jumbo v2, "country"

    const-string/jumbo v3, ""

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "Japan"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2981
    invoke-virtual {p0}, Lcom/peel/ui/gt;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    iget-object v2, p0, Lcom/peel/ui/gt;->aL:Lcom/peel/control/a;

    invoke-static {v1, v2, v0, p1}, Lcom/peel/util/bx;->a(Landroid/content/Context;Lcom/peel/control/a;Lcom/peel/control/h;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 2985
    :cond_0
    invoke-static {}, Lcom/peel/util/i;->c()Z

    move-result v1

    if-nez v1, :cond_1

    .line 2986
    invoke-virtual {v0, p1}, Lcom/peel/control/h;->c(Ljava/lang/String;)Z

    .line 2997
    :goto_0
    return-void

    .line 2988
    :cond_1
    sget-object v1, Lcom/peel/ui/gt;->i:Ljava/lang/String;

    const-string/jumbo v2, "sendCommand"

    new-instance v3, Lcom/peel/ui/iu;

    invoke-direct {v3, p0, v0, p1}, Lcom/peel/ui/iu;-><init>(Lcom/peel/ui/gt;Lcom/peel/control/h;Ljava/lang/String;)V

    invoke-static {v1, v2, v3}, Lcom/peel/util/i;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/lang/Runnable;

    goto :goto_0
.end method

.method static synthetic b(Lcom/peel/ui/gt;Z)Z
    .locals 0

    .prologue
    .line 113
    iput-boolean p1, p0, Lcom/peel/ui/gt;->bz:Z

    return p1
.end method

.method static synthetic c(I)I
    .locals 0

    .prologue
    .line 113
    sput p0, Lcom/peel/ui/gt;->aB:I

    return p0
.end method

.method static synthetic c(Lcom/peel/ui/gt;)Lcom/peel/util/s;
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lcom/peel/ui/gt;->aI:Lcom/peel/util/s;

    return-object v0
.end method

.method static synthetic c(Lcom/peel/ui/gt;Lcom/peel/widget/ag;)Lcom/peel/widget/ag;
    .locals 0

    .prologue
    .line 113
    iput-object p1, p0, Lcom/peel/ui/gt;->aY:Lcom/peel/widget/ag;

    return-object p1
.end method

.method private c(Landroid/view/View;)V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 2425
    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v0}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/control/RoomControl;->d()Lcom/peel/control/a;

    move-result-object v3

    .line 2426
    sget v0, Lcom/peel/ui/fp;->providerName:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 2427
    sget v1, Lcom/peel/ui/fp;->roomname:I

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 2428
    sget v2, Lcom/peel/ui/fp;->layout_spinner:I

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    .line 2429
    iget-object v4, p0, Lcom/peel/ui/gt;->aL:Lcom/peel/control/a;

    invoke-virtual {v4, v6}, Lcom/peel/control/a;->a(I)Lcom/peel/control/h;

    move-result-object v4

    iput-object v4, p0, Lcom/peel/ui/gt;->aM:Lcom/peel/control/h;

    .line 2430
    iget-object v4, p0, Lcom/peel/ui/gt;->aM:Lcom/peel/control/h;

    invoke-virtual {v4}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v4

    invoke-virtual {v4}, Lcom/peel/data/g;->d()I

    move-result v4

    const/4 v5, 0x6

    if-ne v4, v5, :cond_0

    .line 2431
    sget v3, Lcom/peel/ui/ft;->watching:I

    new-array v4, v6, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/peel/ui/gt;->aM:Lcom/peel/control/h;

    invoke-virtual {v5}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v5

    invoke-virtual {v5}, Lcom/peel/data/g;->f()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v7

    invoke-virtual {p0, v3, v4}, Lcom/peel/ui/gt;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2439
    :goto_0
    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v0}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/data/at;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2441
    new-instance v0, Lcom/peel/ui/ik;

    invoke-direct {v0, p0}, Lcom/peel/ui/ik;-><init>(Lcom/peel/ui/gt;)V

    invoke-virtual {v2, v0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2448
    return-void

    .line 2432
    :cond_0
    iget-object v4, p0, Lcom/peel/ui/gt;->aM:Lcom/peel/control/h;

    invoke-virtual {v4}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v4

    invoke-virtual {v4}, Lcom/peel/data/g;->d()I

    move-result v4

    const/16 v5, 0x12

    if-ne v4, v5, :cond_1

    .line 2433
    invoke-virtual {v3}, Lcom/peel/control/a;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 2436
    :cond_1
    sget v4, Lcom/peel/ui/ft;->watching:I

    new-array v5, v6, [Ljava/lang/Object;

    invoke-virtual {v3}, Lcom/peel/control/a;->a()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v5, v7

    invoke-virtual {p0, v4, v5}, Lcom/peel/ui/gt;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method static synthetic c(Lcom/peel/ui/gt;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 113
    invoke-direct {p0, p1}, Lcom/peel/ui/gt;->a(Landroid/view/View;)V

    return-void
.end method

.method static synthetic c(Lcom/peel/ui/gt;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 113
    invoke-direct {p0, p1}, Lcom/peel/ui/gt;->b(Ljava/lang/String;)V

    return-void
.end method

.method private c(Ljava/lang/String;Ljava/lang/String;)V
    .locals 7

    .prologue
    .line 3146
    iget-boolean v0, p0, Lcom/peel/ui/gt;->bf:Z

    if-eqz v0, :cond_3

    .line 3150
    iget-object v0, p0, Lcom/peel/ui/gt;->aM:Lcom/peel/control/h;

    .line 3151
    invoke-virtual {v0}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/data/g;->a()Ljava/util/Map;

    move-result-object v3

    .line 3153
    invoke-interface {v3, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    .line 3157
    if-eqz p1, :cond_0

    const-string/jumbo v1, "PowerOn"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string/jumbo v1, "PowerOff"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 3158
    iget-object v1, p0, Lcom/peel/ui/gt;->bh:Ljava/util/Map;

    invoke-interface {v1, p2, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 3161
    :cond_0
    iget-object v1, p0, Lcom/peel/ui/gt;->bo:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->size()I

    move-result v1

    if-lez v1, :cond_1

    .line 3162
    iget-object v1, p0, Lcom/peel/ui/gt;->bo:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 3163
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v3, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map;

    .line 3164
    const-string/jumbo v5, "ir"

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v6

    invoke-interface {v2, v5, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 3165
    iget-object v5, p0, Lcom/peel/ui/gt;->aM:Lcom/peel/control/h;

    invoke-virtual {v5}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v5

    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v5, v1, v2}, Lcom/peel/data/g;->a(Ljava/lang/String;Ljava/util/Map;)V

    goto :goto_0

    .line 3169
    :cond_1
    iget-object v1, p0, Lcom/peel/ui/gt;->bo:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 3171
    iget-object v1, p0, Lcom/peel/ui/gt;->bo:Ljava/util/Map;

    const-string/jumbo v2, "ir"

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    .line 3172
    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    .line 3171
    invoke-interface {v1, p1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 3178
    :cond_2
    iget-object v1, p0, Lcom/peel/ui/gt;->bi:Ljava/lang/String;

    iget-object v2, p0, Lcom/peel/ui/gt;->bh:Ljava/util/Map;

    invoke-static {v1, v3, v2}, Lcom/peel/util/ay;->a(Ljava/lang/String;Ljava/util/Map;Ljava/util/Map;)Ljava/lang/String;

    move-result-object v1

    .line 3181
    const-string/jumbo v2, "ir"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 3183
    iget-object v1, p0, Lcom/peel/ui/gt;->aM:Lcom/peel/control/h;

    invoke-virtual {v1}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v1

    invoke-virtual {v1, p1, v0}, Lcom/peel/data/g;->a(Ljava/lang/String;Ljava/util/Map;)V

    .line 3186
    :cond_3
    invoke-direct {p0, p1}, Lcom/peel/ui/gt;->a(Ljava/lang/String;)V

    .line 3187
    return-void
.end method

.method static synthetic d(I)I
    .locals 0

    .prologue
    .line 113
    sput p0, Lcom/peel/ui/gt;->aC:I

    return p0
.end method

.method static synthetic d(Lcom/peel/ui/gt;)Lcom/peel/widget/ag;
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lcom/peel/ui/gt;->bp:Lcom/peel/widget/ag;

    return-object v0
.end method

.method static synthetic d(Lcom/peel/ui/gt;Lcom/peel/widget/ag;)Lcom/peel/widget/ag;
    .locals 0

    .prologue
    .line 113
    iput-object p1, p0, Lcom/peel/ui/gt;->ay:Lcom/peel/widget/ag;

    return-object p1
.end method

.method static synthetic d(Lcom/peel/ui/gt;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 113
    iput-object p1, p0, Lcom/peel/ui/gt;->bj:Ljava/lang/String;

    return-object p1
.end method

.method private d(Landroid/view/View;)V
    .locals 9

    .prologue
    const/4 v2, 0x0

    .line 3817
    :try_start_0
    invoke-virtual {p0}, Lcom/peel/ui/gt;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    const-string/jumbo v3, "layout_inflater"

    invoke-virtual {v1, v3}, Landroid/support/v4/app/ae;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/LayoutInflater;

    .line 3818
    sget v3, Lcom/peel/ui/fq;->controlpad_spinner:I

    const/4 v4, 0x0

    invoke-virtual {v1, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .line 3820
    invoke-static {}, Lcom/peel/content/a;->a()Lcom/peel/data/ContentRoom;

    move-result-object v4

    .line 3821
    sget v1, Lcom/peel/ui/fp;->activities_lv:I

    invoke-virtual {v3, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    move-object v0, v1

    check-cast v0, Landroid/widget/ListView;

    move-object v7, v0

    .line 3826
    new-instance v1, Landroid/widget/PopupWindow;

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v5

    const/4 v6, -0x2

    const/4 v8, 0x1

    invoke-direct {v1, v3, v5, v6, v8}, Landroid/widget/PopupWindow;-><init>(Landroid/view/View;IIZ)V

    iput-object v1, p0, Lcom/peel/ui/gt;->bP:Landroid/widget/PopupWindow;

    .line 3827
    iget-object v1, p0, Lcom/peel/ui/gt;->bP:Landroid/widget/PopupWindow;

    new-instance v5, Landroid/graphics/drawable/BitmapDrawable;

    invoke-direct {v5}, Landroid/graphics/drawable/BitmapDrawable;-><init>()V

    invoke-virtual {v1, v5}, Landroid/widget/PopupWindow;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 3828
    iget-object v1, p0, Lcom/peel/ui/gt;->bP:Landroid/widget/PopupWindow;

    const/4 v5, 0x1

    invoke-virtual {v1, v5}, Landroid/widget/PopupWindow;->setOutsideTouchable(Z)V

    .line 3829
    iget-object v1, p0, Lcom/peel/ui/gt;->bP:Landroid/widget/PopupWindow;

    const/4 v5, 0x0

    const/16 v6, 0xa

    invoke-virtual {v1, p1, v5, v6}, Landroid/widget/PopupWindow;->showAsDropDown(Landroid/view/View;II)V

    .line 3841
    sget v1, Lcom/peel/ui/fp;->add_devices:I

    invoke-virtual {v3, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    new-instance v3, Lcom/peel/ui/jo;

    invoke-direct {v3, p0}, Lcom/peel/ui/jo;-><init>(Lcom/peel/ui/gt;)V

    invoke-virtual {v1, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 3851
    sget-object v1, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v4}, Lcom/peel/data/ContentRoom;->d()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/peel/control/am;->a(Ljava/lang/String;)Lcom/peel/control/RoomControl;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/control/RoomControl;->c()[Lcom/peel/control/a;

    move-result-object v3

    .line 3852
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 3853
    array-length v5, v3

    move v1, v2

    :goto_0
    if-ge v1, v5, :cond_2

    aget-object v2, v3, v1

    .line 3854
    if-eqz v2, :cond_0

    const/4 v6, 0x1

    invoke-virtual {v2, v6}, Lcom/peel/control/a;->a(I)Lcom/peel/control/h;

    move-result-object v6

    if-eqz v6, :cond_0

    const/4 v6, 0x5

    const/4 v8, 0x1

    invoke-virtual {v2, v8}, Lcom/peel/control/a;->a(I)Lcom/peel/control/h;

    move-result-object v8

    invoke-virtual {v8}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v8

    invoke-virtual {v8}, Lcom/peel/data/g;->d()I

    move-result v8

    if-ne v6, v8, :cond_1

    .line 3853
    :cond_0
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 3856
    :cond_1
    invoke-interface {v4, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 3881
    :catch_0
    move-exception v1

    .line 3882
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    .line 3884
    :goto_2
    return-void

    .line 3860
    :cond_2
    :try_start_1
    new-instance v1, Lcom/peel/ui/aq;

    invoke-virtual {p0}, Lcom/peel/ui/gt;->m()Landroid/support/v4/app/ae;

    move-result-object v2

    sget v3, Lcom/peel/ui/fq;->controlpad_spinner_item:I

    sget-object v5, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v5}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v5

    invoke-virtual {v5}, Lcom/peel/control/RoomControl;->d()Lcom/peel/control/a;

    move-result-object v5

    const/4 v6, 0x1

    invoke-direct/range {v1 .. v6}, Lcom/peel/ui/aq;-><init>(Landroid/content/Context;ILjava/util/List;Lcom/peel/control/a;Z)V

    invoke-virtual {v7, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 3862
    invoke-static {v7}, Lcom/peel/util/bx;->a(Landroid/widget/ListView;)V

    .line 3863
    new-instance v1, Lcom/peel/ui/jp;

    invoke-direct {v1, p0}, Lcom/peel/ui/jp;-><init>(Lcom/peel/ui/gt;)V

    invoke-virtual {v7, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2
.end method

.method private d(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 3190
    iget-object v0, p0, Lcom/peel/ui/gt;->aM:Lcom/peel/control/h;

    if-nez v0, :cond_0

    .line 3191
    iget-object v0, p0, Lcom/peel/ui/gt;->aL:Lcom/peel/control/a;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/peel/control/a;->a(I)Lcom/peel/control/h;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/ui/gt;->aM:Lcom/peel/control/h;

    .line 3195
    :cond_0
    iget-object v0, p0, Lcom/peel/ui/gt;->bh:Ljava/util/Map;

    invoke-interface {v0, p2, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 3197
    iget-object v0, p0, Lcom/peel/ui/gt;->bh:Ljava/util/Map;

    invoke-static {v0}, Lcom/peel/util/ay;->a(Ljava/util/Map;)Ljava/lang/String;

    move-result-object v0

    .line 3199
    iget-object v1, p0, Lcom/peel/ui/gt;->aM:Lcom/peel/control/h;

    invoke-virtual {v1}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/data/g;->b()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/peel/ui/gt;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 3200
    return-void
.end method

.method static synthetic e(Lcom/peel/ui/gt;)V
    .locals 0

    .prologue
    .line 113
    invoke-direct {p0}, Lcom/peel/ui/gt;->am()V

    return-void
.end method

.method static synthetic f(Lcom/peel/ui/gt;)V
    .locals 0

    .prologue
    .line 113
    invoke-direct {p0}, Lcom/peel/ui/gt;->at()V

    return-void
.end method

.method static synthetic g(Lcom/peel/ui/gt;)Lcom/peel/widget/SlidingDrawer;
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lcom/peel/ui/gt;->aU:Lcom/peel/widget/SlidingDrawer;

    return-object v0
.end method

.method static synthetic h(Lcom/peel/ui/gt;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lcom/peel/ui/gt;->bB:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic i(Lcom/peel/ui/gt;)[Ljava/lang/String;
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lcom/peel/ui/gt;->bw:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic j(Lcom/peel/ui/gt;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lcom/peel/ui/gt;->bv:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic k(Lcom/peel/ui/gt;)Landroid/widget/ListView;
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lcom/peel/ui/gt;->as:Landroid/widget/ListView;

    return-object v0
.end method

.method static synthetic l(Lcom/peel/ui/gt;)Lcom/peel/ui/f;
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lcom/peel/ui/gt;->br:Lcom/peel/ui/f;

    return-object v0
.end method

.method static synthetic m(Lcom/peel/ui/gt;)I
    .locals 1

    .prologue
    .line 113
    iget v0, p0, Lcom/peel/ui/gt;->bC:I

    return v0
.end method

.method static synthetic n(Lcom/peel/ui/gt;)Z
    .locals 1

    .prologue
    .line 113
    iget-boolean v0, p0, Lcom/peel/ui/gt;->by:Z

    return v0
.end method

.method static synthetic o(Lcom/peel/ui/gt;)Lcom/peel/widget/SlidingDrawer;
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lcom/peel/ui/gt;->aV:Lcom/peel/widget/SlidingDrawer;

    return-object v0
.end method

.method static synthetic p(Lcom/peel/ui/gt;)Z
    .locals 1

    .prologue
    .line 113
    iget-boolean v0, p0, Lcom/peel/ui/gt;->bx:Z

    return v0
.end method

.method static synthetic q(Lcom/peel/ui/gt;)Z
    .locals 1

    .prologue
    .line 113
    iget-boolean v0, p0, Lcom/peel/ui/gt;->bz:Z

    return v0
.end method

.method static synthetic r(Lcom/peel/ui/gt;)Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lcom/peel/ui/gt;->bs:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic s(Lcom/peel/ui/gt;)Landroid/widget/FrameLayout;
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lcom/peel/ui/gt;->aT:Landroid/widget/FrameLayout;

    return-object v0
.end method

.method static synthetic t(Lcom/peel/ui/gt;)Landroid/widget/RelativeLayout;
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lcom/peel/ui/gt;->bt:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method static synthetic u(Lcom/peel/ui/gt;)Lcom/peel/ui/a/a;
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lcom/peel/ui/gt;->bD:Lcom/peel/ui/a/a;

    return-object v0
.end method

.method static synthetic v(Lcom/peel/ui/gt;)Landroid/view/ViewGroup;
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lcom/peel/ui/gt;->bE:Landroid/view/ViewGroup;

    return-object v0
.end method

.method static synthetic w(Lcom/peel/ui/gt;)Landroid/view/LayoutInflater;
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lcom/peel/ui/gt;->aO:Landroid/view/LayoutInflater;

    return-object v0
.end method

.method static synthetic x(Lcom/peel/ui/gt;)Ljava/util/List;
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lcom/peel/ui/gt;->aE:Ljava/util/List;

    return-object v0
.end method

.method static synthetic y(Lcom/peel/ui/gt;)Landroid/widget/ViewFlipper;
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lcom/peel/ui/gt;->aW:Landroid/widget/ViewFlipper;

    return-object v0
.end method

.method static synthetic z(Lcom/peel/ui/gt;)Ljava/util/List;
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lcom/peel/ui/gt;->am:Ljava/util/List;

    return-object v0
.end method


# virtual methods
.method public Z()V
    .locals 6

    .prologue
    const/4 v4, 0x0

    .line 3549
    iget-object v0, p0, Lcom/peel/ui/gt;->d:Lcom/peel/d/a;

    if-nez v0, :cond_0

    .line 3550
    new-instance v0, Lcom/peel/d/a;

    sget-object v1, Lcom/peel/d/d;->a:Lcom/peel/d/d;

    sget-object v2, Lcom/peel/d/b;->b:Lcom/peel/d/b;

    sget-object v3, Lcom/peel/d/c;->b:Lcom/peel/d/c;

    move-object v5, v4

    invoke-direct/range {v0 .. v5}, Lcom/peel/d/a;-><init>(Lcom/peel/d/d;Lcom/peel/d/b;Lcom/peel/d/c;Ljava/lang/String;Ljava/util/List;)V

    iput-object v0, p0, Lcom/peel/ui/gt;->d:Lcom/peel/d/a;

    .line 3552
    :cond_0
    iget-object v0, p0, Lcom/peel/ui/gt;->b:Lcom/peel/d/i;

    iget-object v1, p0, Lcom/peel/ui/gt;->d:Lcom/peel/d/a;

    invoke-virtual {v0, v1}, Lcom/peel/d/i;->a(Lcom/peel/d/a;)V

    .line 3554
    iget-object v0, p0, Lcom/peel/ui/gt;->c:Landroid/os/Bundle;

    invoke-virtual {p0, v0}, Lcom/peel/ui/gt;->c(Landroid/os/Bundle;)V

    .line 3555
    return-void
.end method

.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 8

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 378
    sget v0, Lcom/peel/ui/fq;->controlpad_new:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v6

    .line 379
    iput-object p1, p0, Lcom/peel/ui/gt;->aO:Landroid/view/LayoutInflater;

    .line 381
    sget v0, Lcom/peel/ui/fp;->content_new:I

    invoke-virtual {v6, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    sget v1, Lcom/peel/ui/fo;->controller_bg:I

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    .line 384
    sget v0, Lcom/peel/ui/fp;->activity_btn:I

    invoke-virtual {v6, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    .line 386
    iget-boolean v1, p0, Lcom/peel/ui/gt;->bH:Z

    if-eqz v1, :cond_0

    .line 387
    sget v1, Lcom/peel/ui/fo;->change_remote_bg_stateful:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setBackgroundResource(I)V

    .line 393
    :goto_0
    sget v0, Lcom/peel/ui/fp;->activity_btn:I

    invoke-virtual {v6, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/peel/ui/jf;

    invoke-direct {v1, p0}, Lcom/peel/ui/jf;-><init>(Lcom/peel/ui/gt;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 401
    sget v0, Lcom/peel/ui/fp;->vhandle_close:I

    invoke-virtual {v6, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/peel/ui/js;

    invoke-direct {v1, p0}, Lcom/peel/ui/js;-><init>(Lcom/peel/ui/gt;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 409
    new-instance v0, Lcom/peel/ui/jt;

    invoke-direct {v0, p0, v5, v6}, Lcom/peel/ui/jt;-><init>(Lcom/peel/ui/gt;ILandroid/view/View;)V

    invoke-static {v0}, Lcom/peel/util/bx;->b(Lcom/peel/util/t;)V

    .line 431
    sget v0, Lcom/peel/ui/fp;->activity_content:I

    invoke-virtual {v6, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/peel/ui/gt;->aR:Landroid/widget/FrameLayout;

    .line 432
    sget v0, Lcom/peel/ui/fp;->devices_drawer:I

    invoke-virtual {v6, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/peel/widget/SlidingDrawer;

    iput-object v0, p0, Lcom/peel/ui/gt;->aU:Lcom/peel/widget/SlidingDrawer;

    .line 433
    sget v0, Lcom/peel/ui/fp;->devices_flipper:I

    invoke-virtual {v6, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ViewFlipper;

    iput-object v0, p0, Lcom/peel/ui/gt;->aW:Landroid/widget/ViewFlipper;

    .line 434
    sget v0, Lcom/peel/ui/fp;->devices_tabs:I

    invoke-virtual {v6, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/peel/ui/gt;->aS:Landroid/widget/FrameLayout;

    .line 435
    sget v0, Lcom/peel/ui/fp;->room_name:I

    invoke-virtual {v6, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/peel/ui/gt;->bb:Landroid/widget/TextView;

    .line 436
    sget v0, Lcom/peel/ui/fp;->channel_guide_drawer:I

    invoke-virtual {v6, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/peel/widget/SlidingDrawer;

    iput-object v0, p0, Lcom/peel/ui/gt;->aV:Lcom/peel/widget/SlidingDrawer;

    .line 437
    sget v0, Lcom/peel/ui/fp;->view_block:I

    invoke-virtual {v6, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/peel/ui/gt;->aT:Landroid/widget/FrameLayout;

    .line 438
    sget v0, Lcom/peel/ui/fp;->iv_handle_flipper:I

    invoke-virtual {v6, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/peel/ui/gt;->bs:Landroid/widget/ImageView;

    .line 439
    sget v0, Lcom/peel/ui/fp;->message_popup:I

    invoke-virtual {v6, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/peel/ui/gt;->bt:Landroid/widget/RelativeLayout;

    .line 440
    sget v0, Lcom/peel/ui/fp;->tv_channel_guide_text:I

    invoke-virtual {v6, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/peel/ui/gt;->bv:Landroid/widget/TextView;

    .line 442
    sget v0, Lcom/peel/ui/fp;->ad_placeholder:I

    invoke-virtual {v6, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/peel/ui/gt;->bE:Landroid/view/ViewGroup;

    .line 444
    invoke-virtual {p0}, Lcom/peel/ui/gt;->n()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/peel/ui/fk;->channel_guide_remote_options:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/ui/gt;->bw:[Ljava/lang/String;

    .line 446
    iget-object v0, p0, Lcom/peel/ui/gt;->bs:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setClickable(Z)V

    .line 447
    iget-object v0, p0, Lcom/peel/ui/gt;->bs:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setFocusable(Z)V

    .line 449
    sget v0, Lcom/peel/ui/fp;->lv_drawer_option:I

    invoke-virtual {v6, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/peel/ui/gt;->as:Landroid/widget/ListView;

    .line 451
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 452
    iget-object v0, p0, Lcom/peel/ui/gt;->bw:[Ljava/lang/String;

    aget-object v0, v0, v4

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 453
    iget-object v7, p0, Lcom/peel/ui/gt;->as:Landroid/widget/ListView;

    new-instance v0, Lcom/peel/ui/ay;

    invoke-virtual {p0}, Lcom/peel/ui/gt;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    iget-object v2, p0, Lcom/peel/ui/gt;->bw:[Ljava/lang/String;

    invoke-static {v2}, Lcom/peel/util/bu;->a([Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-direct/range {v0 .. v5}, Lcom/peel/ui/ay;-><init>(Landroid/content/Context;Ljava/util/ArrayList;Ljava/util/ArrayList;ZZ)V

    invoke-virtual {v7, v0}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 454
    iget-object v0, p0, Lcom/peel/ui/gt;->as:Landroid/widget/ListView;

    new-instance v1, Lcom/peel/ui/ju;

    invoke-direct {v1, p0}, Lcom/peel/ui/ju;-><init>(Lcom/peel/ui/gt;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 508
    iget-object v0, p0, Lcom/peel/ui/gt;->as:Landroid/widget/ListView;

    invoke-virtual {v0, v4, v5}, Landroid/widget/ListView;->setItemChecked(IZ)V

    .line 510
    sget v0, Lcom/peel/ui/fp;->iv_close:I

    invoke-virtual {v6, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/peel/ui/gt;->bu:Landroid/widget/ImageView;

    .line 512
    iget-object v0, p0, Lcom/peel/ui/gt;->aT:Landroid/widget/FrameLayout;

    new-instance v1, Lcom/peel/ui/jv;

    invoke-direct {v1, p0}, Lcom/peel/ui/jv;-><init>(Lcom/peel/ui/gt;)V

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 520
    iget-object v0, p0, Lcom/peel/ui/gt;->bv:Landroid/widget/TextView;

    new-instance v1, Lcom/peel/ui/gw;

    invoke-direct {v1, p0}, Lcom/peel/ui/gw;-><init>(Lcom/peel/ui/gt;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 537
    iget-object v0, p0, Lcom/peel/ui/gt;->aU:Lcom/peel/widget/SlidingDrawer;

    new-instance v1, Lcom/peel/ui/gx;

    invoke-direct {v1, p0}, Lcom/peel/ui/gx;-><init>(Lcom/peel/ui/gt;)V

    invoke-virtual {v0, v1}, Lcom/peel/widget/SlidingDrawer;->setOnDrawerOpenListener(Lcom/peel/widget/am;)V

    .line 570
    iget-object v0, p0, Lcom/peel/ui/gt;->aU:Lcom/peel/widget/SlidingDrawer;

    new-instance v1, Lcom/peel/ui/ha;

    invoke-direct {v1, p0}, Lcom/peel/ui/ha;-><init>(Lcom/peel/ui/gt;)V

    invoke-virtual {v0, v1}, Lcom/peel/widget/SlidingDrawer;->setOnDrawerCloseListener(Lcom/peel/widget/al;)V

    .line 586
    iget-boolean v0, p0, Lcom/peel/ui/gt;->by:Z

    if-eqz v0, :cond_1

    .line 587
    iget-object v0, p0, Lcom/peel/ui/gt;->aV:Lcom/peel/widget/SlidingDrawer;

    new-instance v1, Lcom/peel/ui/hb;

    invoke-direct {v1, p0}, Lcom/peel/ui/hb;-><init>(Lcom/peel/ui/gt;)V

    invoke-virtual {v0, v1}, Lcom/peel/widget/SlidingDrawer;->setOnDrawerOpenListener(Lcom/peel/widget/am;)V

    .line 634
    iget-object v0, p0, Lcom/peel/ui/gt;->aV:Lcom/peel/widget/SlidingDrawer;

    new-instance v1, Lcom/peel/ui/he;

    invoke-direct {v1, p0}, Lcom/peel/ui/he;-><init>(Lcom/peel/ui/gt;)V

    invoke-virtual {v0, v1}, Lcom/peel/widget/SlidingDrawer;->setOnDrawerScrollListener(Lcom/peel/widget/an;)V

    .line 648
    iget-object v0, p0, Lcom/peel/ui/gt;->aV:Lcom/peel/widget/SlidingDrawer;

    new-instance v1, Lcom/peel/ui/hf;

    invoke-direct {v1, p0}, Lcom/peel/ui/hf;-><init>(Lcom/peel/ui/gt;)V

    invoke-virtual {v0, v1}, Lcom/peel/widget/SlidingDrawer;->setOnDrawerCloseListener(Lcom/peel/widget/al;)V

    .line 691
    :goto_1
    sget-object v0, Lcom/peel/control/am;->a:Lcom/peel/control/aq;

    iget-object v1, p0, Lcom/peel/ui/gt;->aH:Lcom/peel/util/s;

    invoke-virtual {v0, v1}, Lcom/peel/control/aq;->a(Ljava/lang/Object;)V

    .line 693
    return-object v6

    .line 389
    :cond_0
    sget v1, Lcom/peel/ui/fo;->change_remote_bg_stateful:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setBackgroundResource(I)V

    .line 390
    sget v1, Lcom/peel/ui/fo;->change_remote:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageResource(I)V

    goto/16 :goto_0

    .line 688
    :cond_1
    iget-object v0, p0, Lcom/peel/ui/gt;->aV:Lcom/peel/widget/SlidingDrawer;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/peel/widget/SlidingDrawer;->setVisibility(I)V

    goto :goto_1
.end method

.method public a()Ljava/lang/String;
    .locals 2

    .prologue
    .line 3431
    iget-object v0, p0, Lcom/peel/ui/gt;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "goback_clazz"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 0

    .prologue
    .line 797
    if-eqz p1, :cond_0

    .line 798
    invoke-interface {p1}, Landroid/view/Menu;->clear()V

    .line 799
    :cond_0
    return-void
.end method

.method public a_(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 338
    invoke-super {p0, p1}, Lcom/peel/d/u;->a_(Landroid/os/Bundle;)V

    .line 340
    new-instance v0, Lcom/peel/ui/a/a;

    invoke-virtual {p0}, Lcom/peel/ui/gt;->m()Landroid/support/v4/app/ae;

    move-result-object v3

    invoke-direct {v0, v3}, Lcom/peel/ui/a/a;-><init>(Landroid/app/Activity;)V

    iput-object v0, p0, Lcom/peel/ui/gt;->bD:Lcom/peel/ui/a/a;

    .line 342
    sget-object v0, Lcom/peel/c/a;->b:Lcom/peel/c/e;

    invoke-static {v0}, Lcom/peel/c/a;->a(Lcom/peel/c/e;)Ljava/lang/Object;

    move-result-object v0

    sget-object v3, Lcom/peel/c/b;->c:Lcom/peel/c/b;

    if-ne v0, v3, :cond_1

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/peel/ui/gt;->bH:Z

    .line 344
    invoke-virtual {p0}, Lcom/peel/ui/gt;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    const-string/jumbo v3, "remotechannelguide"

    invoke-static {v0, v3}, Lcom/peel/util/dq;->c(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 346
    if-eqz v0, :cond_0

    .line 347
    sget-object v3, Lcom/peel/util/a;->d:[Ljava/lang/String;

    aget-object v3, v3, v2

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 348
    iput v2, p0, Lcom/peel/ui/gt;->bA:I

    .line 356
    :cond_0
    :goto_1
    const-string/jumbo v0, "live"

    invoke-static {v0}, Lcom/peel/content/a;->c(Ljava/lang/String;)Lcom/peel/content/library/Library;

    move-result-object v0

    check-cast v0, Lcom/peel/content/library/LiveLibrary;

    iput-object v0, p0, Lcom/peel/ui/gt;->bG:Lcom/peel/content/library/LiveLibrary;

    .line 359
    sget-object v3, Lcom/peel/ui/gt;->i:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "\n\n************* liveLibrary: "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v0, p0, Lcom/peel/ui/gt;->bG:Lcom/peel/content/library/LiveLibrary;

    if-nez v0, :cond_4

    const-string/jumbo v0, "NULLLLL"

    :goto_2
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v4, "\n\n"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 362
    iget-object v0, p0, Lcom/peel/ui/gt;->bG:Lcom/peel/content/library/LiveLibrary;

    if-eqz v0, :cond_5

    :goto_3
    iput-boolean v1, p0, Lcom/peel/ui/gt;->by:Z

    .line 363
    return-void

    :cond_1
    move v0, v2

    .line 342
    goto :goto_0

    .line 349
    :cond_2
    sget-object v3, Lcom/peel/util/a;->d:[Ljava/lang/String;

    aget-object v3, v3, v1

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 350
    iput v1, p0, Lcom/peel/ui/gt;->bA:I

    goto :goto_1

    .line 351
    :cond_3
    sget-object v3, Lcom/peel/util/a;->d:[Ljava/lang/String;

    aget-object v3, v3, v4

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 352
    iput v4, p0, Lcom/peel/ui/gt;->bA:I

    goto :goto_1

    .line 359
    :cond_4
    iget-object v0, p0, Lcom/peel/ui/gt;->bG:Lcom/peel/content/library/LiveLibrary;

    invoke-virtual {v0}, Lcom/peel/content/library/LiveLibrary;->d()Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    :cond_5
    move v1, v2

    .line 362
    goto :goto_3
.end method

.method public aa()Z
    .locals 1

    .prologue
    .line 1034
    const/4 v0, 0x1

    return v0
.end method

.method public b()Z
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 3436
    iget-object v2, p0, Lcom/peel/ui/gt;->aU:Lcom/peel/widget/SlidingDrawer;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/peel/ui/gt;->aU:Lcom/peel/widget/SlidingDrawer;

    invoke-virtual {v2}, Lcom/peel/widget/SlidingDrawer;->e()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 3437
    iget-object v1, p0, Lcom/peel/ui/gt;->aU:Lcom/peel/widget/SlidingDrawer;

    invoke-virtual {v1}, Lcom/peel/widget/SlidingDrawer;->a()V

    .line 3459
    :goto_0
    return v0

    .line 3439
    :cond_0
    iget-object v2, p0, Lcom/peel/ui/gt;->aV:Lcom/peel/widget/SlidingDrawer;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/peel/ui/gt;->aV:Lcom/peel/widget/SlidingDrawer;

    invoke-virtual {v2}, Lcom/peel/widget/SlidingDrawer;->e()Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/peel/ui/gt;->aV:Lcom/peel/widget/SlidingDrawer;

    invoke-virtual {v2}, Lcom/peel/widget/SlidingDrawer;->f()Z

    move-result v2

    if-nez v2, :cond_1

    .line 3440
    iget-object v1, p0, Lcom/peel/ui/gt;->aV:Lcom/peel/widget/SlidingDrawer;

    invoke-virtual {v1}, Lcom/peel/widget/SlidingDrawer;->c()V

    goto :goto_0

    .line 3442
    :cond_1
    iget-object v2, p0, Lcom/peel/ui/gt;->b:Lcom/peel/d/i;

    if-eqz v2, :cond_4

    .line 3443
    iget-boolean v2, p0, Lcom/peel/ui/gt;->aZ:Z

    if-eqz v2, :cond_3

    .line 3444
    invoke-virtual {p0}, Lcom/peel/ui/gt;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/ae;->finish()V

    :cond_2
    move v0, v1

    .line 3454
    goto :goto_0

    .line 3446
    :cond_3
    iget-object v2, p0, Lcom/peel/ui/gt;->ap:Ljava/lang/String;

    invoke-static {}, Lcom/peel/content/a;->a()Lcom/peel/data/ContentRoom;

    move-result-object v3

    invoke-virtual {v3}, Lcom/peel/data/ContentRoom;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 3447
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 3448
    const-string/jumbo v3, "type"

    invoke-virtual {v2, v3, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 3449
    invoke-virtual {p0}, Lcom/peel/ui/gt;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    const-class v3, Lcom/peel/ui/lq;

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3, v2}, Lcom/peel/d/e;->a(Landroid/support/v4/app/ae;Ljava/lang/String;Landroid/os/Bundle;)V

    goto :goto_0

    .line 3456
    :cond_4
    invoke-static {}, Lcom/peel/util/a/f;->a()Lcom/peel/util/a/f;

    move-result-object v1

    sget-object v2, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v2}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v2

    if-nez v2, :cond_5

    :goto_1
    const/16 v2, 0x445

    const/16 v3, 0x7e7

    const-string/jumbo v4, "physical back"

    invoke-virtual {v1, v0, v2, v3, v4}, Lcom/peel/util/a/f;->a(IIILjava/lang/String;)V

    .line 3459
    invoke-super {p0}, Lcom/peel/d/u;->b()Z

    move-result v0

    goto :goto_0

    .line 3456
    :cond_5
    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v0}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/data/at;->f()I

    move-result v0

    goto :goto_1
.end method

.method public c()V
    .locals 1

    .prologue
    .line 3777
    iget-object v0, p0, Lcom/peel/ui/gt;->ay:Lcom/peel/widget/ag;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/peel/ui/gt;->ay:Lcom/peel/widget/ag;

    invoke-virtual {v0}, Lcom/peel/widget/ag;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3778
    iget-object v0, p0, Lcom/peel/ui/gt;->ay:Lcom/peel/widget/ag;

    invoke-virtual {v0}, Lcom/peel/widget/ag;->dismiss()V

    .line 3780
    :cond_0
    iget-object v0, p0, Lcom/peel/ui/gt;->aX:Lcom/peel/widget/ag;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/peel/ui/gt;->aX:Lcom/peel/widget/ag;

    invoke-virtual {v0}, Lcom/peel/widget/ag;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 3781
    iget-object v0, p0, Lcom/peel/ui/gt;->aX:Lcom/peel/widget/ag;

    invoke-virtual {v0}, Lcom/peel/widget/ag;->dismiss()V

    .line 3783
    :cond_1
    iget-object v0, p0, Lcom/peel/ui/gt;->aY:Lcom/peel/widget/ag;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/peel/ui/gt;->aY:Lcom/peel/widget/ag;

    invoke-virtual {v0}, Lcom/peel/widget/ag;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 3784
    iget-object v0, p0, Lcom/peel/ui/gt;->aY:Lcom/peel/widget/ag;

    invoke-virtual {v0}, Lcom/peel/widget/ag;->dismiss()V

    .line 3786
    :cond_2
    sget-object v0, Lcom/peel/ui/gt;->aP:Landroid/app/Dialog;

    if-eqz v0, :cond_3

    sget-object v0, Lcom/peel/ui/gt;->aP:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 3787
    sget-object v0, Lcom/peel/ui/gt;->aP:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    .line 3789
    :cond_3
    iget-object v0, p0, Lcom/peel/ui/gt;->az:Lcom/peel/widget/ag;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/peel/ui/gt;->az:Lcom/peel/widget/ag;

    invoke-virtual {v0}, Lcom/peel/widget/ag;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 3790
    iget-object v0, p0, Lcom/peel/ui/gt;->az:Lcom/peel/widget/ag;

    invoke-virtual {v0}, Lcom/peel/widget/ag;->dismiss()V

    .line 3792
    :cond_4
    iget-object v0, p0, Lcom/peel/ui/gt;->aY:Lcom/peel/widget/ag;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/peel/ui/gt;->aY:Lcom/peel/widget/ag;

    invoke-virtual {v0}, Lcom/peel/widget/ag;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 3793
    iget-object v0, p0, Lcom/peel/ui/gt;->aY:Lcom/peel/widget/ag;

    invoke-virtual {v0}, Lcom/peel/widget/ag;->dismiss()V

    .line 3795
    :cond_5
    iget-object v0, p0, Lcom/peel/ui/gt;->bp:Lcom/peel/widget/ag;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/peel/ui/gt;->bp:Lcom/peel/widget/ag;

    invoke-virtual {v0}, Lcom/peel/widget/ag;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 3796
    iget-object v0, p0, Lcom/peel/ui/gt;->bp:Lcom/peel/widget/ag;

    invoke-virtual {v0}, Lcom/peel/widget/ag;->dismiss()V

    .line 3798
    :cond_6
    iget-object v0, p0, Lcom/peel/ui/gt;->bq:Lcom/peel/widget/ag;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/peel/ui/gt;->bq:Lcom/peel/widget/ag;

    invoke-virtual {v0}, Lcom/peel/widget/ag;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 3799
    iget-object v0, p0, Lcom/peel/ui/gt;->bq:Lcom/peel/widget/ag;

    invoke-virtual {v0}, Lcom/peel/widget/ag;->dismiss()V

    .line 3801
    :cond_7
    iget-object v0, p0, Lcom/peel/ui/gt;->bO:Landroid/widget/PopupWindow;

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/peel/ui/gt;->bO:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 3802
    iget-object v0, p0, Lcom/peel/ui/gt;->bO:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->dismiss()V

    .line 3804
    :cond_8
    return-void
.end method

.method public c(Landroid/os/Bundle;)V
    .locals 10

    .prologue
    const/16 v8, 0xa

    const/4 v9, 0x2

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 820
    invoke-super {p0, p1}, Lcom/peel/d/u;->c(Landroid/os/Bundle;)V

    .line 821
    iget-object v0, p0, Lcom/peel/ui/gt;->b:Lcom/peel/d/i;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/peel/content/a;->h:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1030
    :cond_0
    :goto_0
    return-void

    .line 823
    :cond_1
    invoke-static {}, Lcom/peel/content/a;->a()Lcom/peel/data/ContentRoom;

    move-result-object v4

    .line 825
    sget-object v0, Lcom/peel/ui/gt;->al:Landroid/util/SparseArray;

    sget v3, Lcom/peel/ui/ft;->DeviceType1:I

    invoke-virtual {p0, v3}, Lcom/peel/ui/gt;->a(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 826
    sget-object v0, Lcom/peel/ui/gt;->al:Landroid/util/SparseArray;

    sget v3, Lcom/peel/ui/ft;->DeviceType2:I

    invoke-virtual {p0, v3}, Lcom/peel/ui/gt;->a(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v9, v3}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 827
    sget-object v0, Lcom/peel/ui/gt;->al:Landroid/util/SparseArray;

    sget v3, Lcom/peel/ui/ft;->DeviceType10:I

    invoke-virtual {p0, v3}, Lcom/peel/ui/gt;->a(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v8, v3}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 829
    sget-object v0, Lcom/peel/ui/gt;->al:Landroid/util/SparseArray;

    const/4 v3, 0x3

    sget v5, Lcom/peel/ui/ft;->DeviceType3:I

    invoke-virtual {p0, v5}, Lcom/peel/ui/gt;->a(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v3, v5}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 830
    sget-object v0, Lcom/peel/ui/gt;->al:Landroid/util/SparseArray;

    const/4 v3, 0x4

    sget v5, Lcom/peel/ui/ft;->DeviceType4:I

    invoke-virtual {p0, v5}, Lcom/peel/ui/gt;->a(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v3, v5}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 831
    sget-object v0, Lcom/peel/ui/gt;->al:Landroid/util/SparseArray;

    const/4 v3, 0x5

    sget v5, Lcom/peel/ui/ft;->DeviceType5:I

    invoke-virtual {p0, v5}, Lcom/peel/ui/gt;->a(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v3, v5}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 832
    sget-object v0, Lcom/peel/ui/gt;->al:Landroid/util/SparseArray;

    const/4 v3, 0x6

    sget v5, Lcom/peel/ui/ft;->DeviceType6:I

    invoke-virtual {p0, v5}, Lcom/peel/ui/gt;->a(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v3, v5}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    move v0, v1

    .line 835
    :goto_1
    sget-object v3, Lcom/peel/ui/gt;->al:Landroid/util/SparseArray;

    invoke-virtual {v3}, Landroid/util/SparseArray;->size()I

    move-result v3

    if-ge v0, v3, :cond_2

    .line 836
    sget-object v3, Lcom/peel/ui/gt;->al:Landroid/util/SparseArray;

    invoke-virtual {v3, v0}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v3

    .line 837
    sget-object v5, Lcom/peel/ui/gt;->ak:Landroid/util/SparseArray;

    sget-object v6, Lcom/peel/ui/gt;->al:Landroid/util/SparseArray;

    invoke-virtual {v6, v3}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v5, v3, v6}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 835
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 842
    :cond_2
    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v0}, Lcom/peel/control/am;->e()[Lcom/peel/control/h;

    move-result-object v3

    .line 843
    if-eqz v3, :cond_3

    array-length v0, v3

    if-lez v0, :cond_3

    .line 844
    array-length v5, v3

    move v0, v1

    :goto_2
    if-ge v0, v5, :cond_3

    aget-object v6, v3, v0

    .line 845
    invoke-virtual {v6}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v7

    invoke-virtual {v7}, Lcom/peel/data/g;->d()I

    move-result v7

    sparse-switch v7, :sswitch_data_0

    .line 860
    sget-object v7, Lcom/peel/ui/gt;->ak:Landroid/util/SparseArray;

    invoke-virtual {v6}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v6

    invoke-virtual {v6}, Lcom/peel/data/g;->d()I

    move-result v6

    invoke-virtual {v7, v6}, Landroid/util/SparseArray;->remove(I)V

    .line 844
    :goto_3
    :sswitch_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 851
    :sswitch_1
    sget-object v6, Lcom/peel/ui/gt;->ak:Landroid/util/SparseArray;

    invoke-virtual {v6, v9}, Landroid/util/SparseArray;->remove(I)V

    .line 852
    sget-object v6, Lcom/peel/ui/gt;->ak:Landroid/util/SparseArray;

    const/16 v7, 0x14

    invoke-virtual {v6, v7}, Landroid/util/SparseArray;->remove(I)V

    goto :goto_3

    .line 856
    :sswitch_2
    sget-object v6, Lcom/peel/ui/gt;->ak:Landroid/util/SparseArray;

    invoke-virtual {v6, v2}, Landroid/util/SparseArray;->remove(I)V

    .line 857
    sget-object v6, Lcom/peel/ui/gt;->ak:Landroid/util/SparseArray;

    invoke-virtual {v6, v8}, Landroid/util/SparseArray;->remove(I)V

    goto :goto_3

    .line 865
    :cond_3
    invoke-direct {p0}, Lcom/peel/ui/gt;->as()V

    .line 867
    const-string/jumbo v0, "refresh"

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 868
    const-string/jumbo v0, "refresh"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->remove(Ljava/lang/String;)V

    .line 870
    iget-object v0, p0, Lcom/peel/ui/gt;->aU:Lcom/peel/widget/SlidingDrawer;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/peel/ui/gt;->aU:Lcom/peel/widget/SlidingDrawer;

    invoke-virtual {v0}, Lcom/peel/widget/SlidingDrawer;->e()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 871
    iget-object v0, p0, Lcom/peel/ui/gt;->aU:Lcom/peel/widget/SlidingDrawer;

    invoke-virtual {v0}, Lcom/peel/widget/SlidingDrawer;->a()V

    .line 874
    :cond_4
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 875
    sget-object v0, Lcom/peel/content/a;->f:Lcom/peel/content/user/User;

    invoke-virtual {v0}, Lcom/peel/content/user/User;->h()Landroid/os/Bundle;

    move-result-object v0

    .line 877
    if-eqz v0, :cond_5

    if-eqz v4, :cond_5

    iget-object v3, p0, Lcom/peel/ui/gt;->bG:Lcom/peel/content/library/LiveLibrary;

    if-eqz v3, :cond_5

    .line 879
    :try_start_0
    const-string/jumbo v3, "favchannels"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v3

    if-eqz v3, :cond_5

    .line 880
    const-string/jumbo v3, "favchannels"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_4
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 881
    const-string/jumbo v6, "#"

    invoke-virtual {v0, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    const/4 v6, 0x0

    aget-object v0, v0, v6

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_4

    .line 884
    :catch_0
    move-exception v0

    .line 885
    sget-object v3, Lcom/peel/ui/gt;->i:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/NullPointerException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 888
    :cond_5
    invoke-virtual {v5}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_6

    move v0, v2

    .line 891
    :goto_5
    const-string/jumbo v3, "activityId"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v6, "command"

    invoke-virtual {p1, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {p0, v3, v6}, Lcom/peel/ui/gt;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/peel/control/a;

    move-result-object v6

    .line 892
    if-nez v6, :cond_7

    .line 893
    sget-object v0, Lcom/peel/ui/gt;->i:Ljava/lang/String;

    const-string/jumbo v1, "no suggested activities"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 894
    sget-object v0, Lcom/peel/ui/gt;->i:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/peel/ui/gt;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/peel/d/e;->a(Ljava/lang/String;Landroid/support/v4/app/ae;)V

    goto/16 :goto_0

    :cond_6
    move v0, v1

    .line 888
    goto :goto_5

    .line 898
    :cond_7
    invoke-virtual {v6}, Lcom/peel/control/a;->b()Ljava/lang/String;

    move-result-object v3

    iget-object v7, p0, Lcom/peel/ui/gt;->aN:Ljava/lang/String;

    invoke-virtual {v3, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_8

    .line 900
    :try_start_1
    iget-object v3, p0, Lcom/peel/ui/gt;->aI:Lcom/peel/util/s;

    invoke-virtual {v6, v3}, Lcom/peel/control/a;->a(Lcom/peel/util/s;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 904
    :goto_6
    invoke-virtual {v6}, Lcom/peel/control/a;->b()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/peel/ui/gt;->aN:Ljava/lang/String;

    .line 908
    :cond_8
    invoke-virtual {v6}, Lcom/peel/control/a;->f()I

    move-result v3

    if-eq v9, v3, :cond_9

    .line 909
    sget-object v3, Lcom/peel/ui/gt;->i:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "start activity "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v6}, Lcom/peel/control/a;->c()Lcom/peel/data/d;

    move-result-object v8

    invoke-virtual {v8}, Lcom/peel/data/d;->a()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    new-instance v8, Lcom/peel/ui/hn;

    invoke-direct {v8, p0, v6}, Lcom/peel/ui/hn;-><init>(Lcom/peel/ui/gt;Lcom/peel/control/a;)V

    invoke-static {v3, v7, v8}, Lcom/peel/util/i;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/lang/Runnable;

    .line 921
    :cond_9
    iput-object v6, p0, Lcom/peel/ui/gt;->aL:Lcom/peel/control/a;

    .line 922
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/peel/ui/gt;->aM:Lcom/peel/control/h;

    .line 923
    const-string/jumbo v3, "activityId"

    iget-object v6, p0, Lcom/peel/ui/gt;->aL:Lcom/peel/control/a;

    invoke-virtual {v6}, Lcom/peel/control/a;->b()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v3, v6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 926
    iget-boolean v3, p0, Lcom/peel/ui/gt;->aZ:Z

    if-nez v3, :cond_f

    .line 927
    iget-object v3, p0, Lcom/peel/ui/gt;->aL:Lcom/peel/control/a;

    invoke-virtual {v3}, Lcom/peel/control/a;->d()[Ljava/lang/String;

    move-result-object v3

    .line 928
    if-eqz v3, :cond_b

    .line 929
    invoke-static {v3}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v3

    .line 930
    const-string/jumbo v6, "live"

    invoke-interface {v3, v6}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_a

    const-string/jumbo v6, "dtv"

    invoke-interface {v3, v6}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_d

    :cond_a
    move v3, v2

    :goto_7
    iput-boolean v3, p0, Lcom/peel/ui/gt;->ao:Z

    .line 933
    :cond_b
    iget-object v3, p0, Lcom/peel/ui/gt;->aD:Landroid/os/Bundle;

    invoke-virtual {v3}, Landroid/os/Bundle;->clear()V

    .line 934
    iget-object v3, p0, Lcom/peel/ui/gt;->aE:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->clear()V

    .line 935
    if-eqz v0, :cond_f

    iget-boolean v0, p0, Lcom/peel/ui/gt;->ao:Z

    if-eqz v0, :cond_f

    .line 936
    if-eqz v5, :cond_e

    invoke-virtual {v5}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_e

    .line 937
    iget-object v0, p0, Lcom/peel/ui/gt;->bG:Lcom/peel/content/library/LiveLibrary;

    invoke-virtual {v0}, Lcom/peel/content/library/LiveLibrary;->i()[Lcom/peel/data/Channel;

    move-result-object v3

    .line 939
    array-length v6, v3

    move v0, v1

    :goto_8
    if-ge v0, v6, :cond_e

    aget-object v7, v3, v0

    .line 940
    invoke-virtual {v7}, Lcom/peel/data/Channel;->a()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v8}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_c

    .line 941
    iget-object v8, p0, Lcom/peel/ui/gt;->aD:Landroid/os/Bundle;

    invoke-virtual {v7}, Lcom/peel/data/Channel;->a()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9, v7}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 942
    iget-object v8, p0, Lcom/peel/ui/gt;->aE:Ljava/util/List;

    invoke-interface {v8, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 939
    :cond_c
    add-int/lit8 v0, v0, 0x1

    goto :goto_8

    .line 901
    :catch_1
    move-exception v3

    .line 902
    sget-object v7, Lcom/peel/ui/gt;->i:Ljava/lang/String;

    sget-object v8, Lcom/peel/ui/gt;->i:Ljava/lang/String;

    invoke-static {v7, v8, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_6

    :cond_d
    move v3, v1

    .line 930
    goto :goto_7

    .line 953
    :cond_e
    :try_start_2
    iget-object v0, p0, Lcom/peel/ui/gt;->aE:Ljava/util/List;

    new-instance v3, Lcom/peel/ui/hq;

    invoke-direct {v3, p0}, Lcom/peel/ui/hq;-><init>(Lcom/peel/ui/gt;)V

    invoke-static {v0, v3}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    .line 973
    :cond_f
    :goto_9
    invoke-direct {p0}, Lcom/peel/ui/gt;->an()V

    .line 974
    iget-object v0, p0, Lcom/peel/ui/gt;->aL:Lcom/peel/control/a;

    invoke-direct {p0, v0}, Lcom/peel/ui/gt;->a(Lcom/peel/control/a;)V

    .line 976
    :cond_10
    if-eqz v4, :cond_11

    .line 977
    iget-object v0, p0, Lcom/peel/ui/gt;->bb:Landroid/widget/TextView;

    invoke-virtual {v4}, Lcom/peel/data/ContentRoom;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 978
    :cond_11
    const-string/jumbo v0, "start_numberpad"

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_12

    .line 979
    const-string/jumbo v0, "start_numberpad"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->remove(Ljava/lang/String;)V

    .line 990
    :cond_12
    iget-boolean v0, p0, Lcom/peel/ui/gt;->by:Z

    if-nez v0, :cond_13

    .line 991
    sget-object v0, Lcom/peel/ui/gt;->i:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "\n ##### control_only_mode: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v3, p0, Lcom/peel/ui/gt;->aZ:Z

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v3, " -- shouldShowChannelDrawer: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v3, p0, Lcom/peel/ui/gt;->by:Z

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v3, " -- hide channel guide drawer"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 993
    iget-object v0, p0, Lcom/peel/ui/gt;->aV:Lcom/peel/widget/SlidingDrawer;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/peel/widget/SlidingDrawer;->setVisibility(I)V

    .line 1006
    :goto_a
    iget-boolean v0, p0, Lcom/peel/ui/gt;->bF:Z

    if-nez v0, :cond_0

    .line 1007
    iput-boolean v2, p0, Lcom/peel/ui/gt;->bF:Z

    .line 1008
    iget-object v0, p0, Lcom/peel/ui/gt;->bD:Lcom/peel/ui/a/a;

    const-string/jumbo v1, "remote"

    const/16 v2, 0x7e7

    new-instance v3, Lcom/peel/ui/hs;

    invoke-direct {v3, p0}, Lcom/peel/ui/hs;-><init>(Lcom/peel/ui/gt;)V

    invoke-virtual {v0, v1, v2, v3}, Lcom/peel/ui/a/a;->b(Ljava/lang/String;ILcom/peel/widget/r;)V

    goto/16 :goto_0

    .line 961
    :catch_2
    move-exception v0

    .line 962
    iget-object v0, p0, Lcom/peel/ui/gt;->aE:Ljava/util/List;

    new-instance v3, Lcom/peel/ui/hr;

    invoke-direct {v3, p0}, Lcom/peel/ui/hr;-><init>(Lcom/peel/ui/gt;)V

    invoke-static {v0, v3}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    goto :goto_9

    .line 995
    :cond_13
    sget-object v0, Lcom/peel/ui/gt;->i:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "\n ##### control_only_mode: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v3, p0, Lcom/peel/ui/gt;->aZ:Z

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v3, " -- shouldShowChannelDrawer: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v3, p0, Lcom/peel/ui/gt;->by:Z

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v3, " -- adding channel guide drawer"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 997
    invoke-virtual {p0}, Lcom/peel/ui/gt;->p()Landroid/support/v4/app/aj;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/aj;->a()Landroid/support/v4/app/ax;

    move-result-object v0

    .line 999
    new-instance v1, Lcom/peel/ui/f;

    invoke-direct {v1}, Lcom/peel/ui/f;-><init>()V

    iput-object v1, p0, Lcom/peel/ui/gt;->br:Lcom/peel/ui/f;

    .line 1000
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 1001
    iget-object v3, p0, Lcom/peel/ui/gt;->br:Lcom/peel/ui/f;

    invoke-virtual {v3, v1}, Lcom/peel/ui/f;->g(Landroid/os/Bundle;)V

    .line 1002
    sget v1, Lcom/peel/ui/fp;->channel_guide_content:I

    iget-object v3, p0, Lcom/peel/ui/gt;->br:Lcom/peel/ui/f;

    invoke-virtual {v0, v1, v3}, Landroid/support/v4/app/ax;->b(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/ax;

    .line 1003
    invoke-virtual {v0}, Landroid/support/v4/app/ax;->b()I

    goto :goto_a

    .line 845
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_2
        0x2 -> :sswitch_1
        0x6 -> :sswitch_0
        0xa -> :sswitch_2
        0x14 -> :sswitch_1
    .end sparse-switch
.end method

.method public d(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 367
    invoke-super {p0, p1}, Lcom/peel/d/u;->d(Landroid/os/Bundle;)V

    .line 368
    invoke-virtual {p0}, Lcom/peel/ui/gt;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/ui/gt;->be:Landroid/content/SharedPreferences;

    .line 369
    iget-object v0, p0, Lcom/peel/ui/gt;->be:Landroid/content/SharedPreferences;

    const-string/jumbo v3, "setup_type"

    invoke-interface {v0, v3, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    if-ne v1, v0, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/peel/ui/gt;->aZ:Z

    .line 370
    iget-boolean v0, p0, Lcom/peel/ui/gt;->aZ:Z

    if-eqz v0, :cond_1

    .line 371
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    invoke-virtual {p0}, Lcom/peel/ui/gt;->n()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/peel/ui/ft;->label_change_room:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v0, v2

    invoke-virtual {p0}, Lcom/peel/ui/gt;->n()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/peel/ui/ft;->label_settings:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-virtual {p0}, Lcom/peel/ui/gt;->n()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/peel/ui/ft;->label_about:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v5

    iput-object v0, p0, Lcom/peel/ui/gt;->aj:[Ljava/lang/String;

    .line 375
    :goto_1
    return-void

    :cond_0
    move v0, v2

    .line 369
    goto :goto_0

    .line 373
    :cond_1
    new-array v0, v5, [Ljava/lang/String;

    invoke-virtual {p0}, Lcom/peel/ui/gt;->n()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/peel/ui/ft;->label_change_room:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v0, v2

    invoke-virtual {p0}, Lcom/peel/ui/gt;->n()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/peel/ui/ft;->label_settings:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/peel/ui/gt;->aj:[Ljava/lang/String;

    goto :goto_1
.end method

.method public f()V
    .locals 3

    .prologue
    .line 784
    invoke-super {p0}, Lcom/peel/d/u;->f()V

    .line 786
    invoke-virtual {p0}, Lcom/peel/ui/gt;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    const-string/jumbo v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/ae;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 787
    invoke-virtual {p0}, Lcom/peel/ui/gt;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/ae;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 789
    invoke-virtual {p0}, Lcom/peel/ui/gt;->c()V

    .line 790
    iget-object v0, p0, Lcom/peel/ui/gt;->aq:Lcom/peel/ui/ai;

    if-eqz v0, :cond_0

    .line 791
    iget-object v0, p0, Lcom/peel/ui/gt;->aq:Lcom/peel/ui/ai;

    invoke-virtual {v0}, Lcom/peel/ui/ai;->d()V

    .line 793
    :cond_0
    return-void
.end method

.method public g()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2451
    sget-object v0, Lcom/peel/control/am;->a:Lcom/peel/control/aq;

    iget-object v1, p0, Lcom/peel/ui/gt;->aH:Lcom/peel/util/s;

    invoke-virtual {v0, v1}, Lcom/peel/control/aq;->b(Ljava/lang/Object;)V

    .line 2452
    iget-object v0, p0, Lcom/peel/ui/gt;->aL:Lcom/peel/control/a;

    if-eqz v0, :cond_0

    .line 2453
    iget-object v0, p0, Lcom/peel/ui/gt;->aL:Lcom/peel/control/a;

    iget-object v1, p0, Lcom/peel/ui/gt;->aI:Lcom/peel/util/s;

    invoke-virtual {v0, v1}, Lcom/peel/control/a;->b(Lcom/peel/util/s;)V

    .line 2454
    iput-object v2, p0, Lcom/peel/ui/gt;->aN:Ljava/lang/String;

    .line 2457
    :cond_0
    iput-object v2, p0, Lcom/peel/ui/gt;->aL:Lcom/peel/control/a;

    .line 2474
    invoke-super {p0}, Lcom/peel/d/u;->g()V

    .line 2475
    return-void
.end method

.method public h(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 802
    invoke-super {p0, p1}, Lcom/peel/d/u;->h(Landroid/os/Bundle;)V

    .line 803
    sget-object v0, Lcom/peel/c/a;->b:Lcom/peel/c/e;

    invoke-static {v0}, Lcom/peel/c/a;->a(Lcom/peel/c/e;)Ljava/lang/Object;

    move-result-object v0

    sget-object v1, Lcom/peel/c/b;->c:Lcom/peel/c/b;

    if-ne v0, v1, :cond_2

    .line 804
    invoke-virtual {p0}, Lcom/peel/ui/gt;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    invoke-static {v0}, Lcom/peel/util/bb;->a(Landroid/content/Context;)V

    .line 810
    :cond_0
    :goto_0
    invoke-static {}, Lcom/peel/content/a;->a()Lcom/peel/data/ContentRoom;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/data/ContentRoom;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/ui/gt;->ap:Ljava/lang/String;

    .line 811
    iget-object v0, p0, Lcom/peel/ui/gt;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "refresh"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 813
    sget-object v0, Lcom/peel/content/a;->h:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 814
    iget-object v0, p0, Lcom/peel/ui/gt;->b:Lcom/peel/d/i;

    if-eqz v0, :cond_1

    .line 815
    iget-object v0, p0, Lcom/peel/ui/gt;->b:Lcom/peel/d/i;

    invoke-virtual {v0, v2}, Lcom/peel/d/i;->a(Z)V

    .line 817
    :cond_1
    return-void

    .line 806
    :cond_2
    invoke-virtual {p0}, Lcom/peel/ui/gt;->n()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget-object v0, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "en"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 807
    invoke-virtual {p0}, Lcom/peel/ui/gt;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    invoke-static {v0}, Lcom/peel/util/bb;->c(Landroid/content/Context;)V

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 2954
    sget-object v0, Lcom/peel/ui/gt;->i:Ljava/lang/String;

    const-string/jumbo v1, "\n\n $$$$$$$$$$$$$$$$$$$$$$$ uh-oh... u shouldn\'t be hitting me... *************** \n\n"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2955
    return-void
.end method

.method public w()V
    .locals 0

    .prologue
    .line 698
    invoke-super {p0}, Lcom/peel/d/u;->w()V

    .line 699
    invoke-virtual {p0}, Lcom/peel/ui/gt;->Z()V

    .line 700
    return-void
.end method

.method public y()V
    .locals 1

    .prologue
    .line 3808
    invoke-super {p0}, Lcom/peel/d/u;->y()V

    .line 3809
    const/4 v0, 0x0

    sput-object v0, Lcom/peel/ui/gt;->aP:Landroid/app/Dialog;

    .line 3810
    return-void
.end method
