.class public Lcom/peel/ui/fx;
.super Lcom/peel/d/t;


# static fields
.field private static final al:Ljava/lang/String;


# instance fields
.field private aA:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private aB:Landroid/os/Bundle;

.field private am:Lcom/peel/content/listing/LiveListing;

.field private an:Landroid/widget/ImageView;

.field private ao:Landroid/widget/TextView;

.field private ap:Landroid/widget/TextView;

.field private aq:Landroid/widget/TextView;

.field private ar:Landroid/widget/TextView;

.field private as:Landroid/widget/TextView;

.field private at:Landroid/widget/TextView;

.field private au:Landroid/widget/CheckedTextView;

.field private av:Landroid/widget/CheckedTextView;

.field private aw:Landroid/widget/CheckedTextView;

.field private ax:Landroid/widget/Button;

.field private ay:Landroid/view/View;

.field private az:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 29
    const-class v0, Lcom/peel/ui/fx;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/peel/ui/fx;->al:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Lcom/peel/d/t;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/peel/ui/fx;)Lcom/peel/content/listing/LiveListing;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lcom/peel/ui/fx;->am:Lcom/peel/content/listing/LiveListing;

    return-object v0
.end method

.method static synthetic a(Lcom/peel/ui/fx;Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0, p1}, Lcom/peel/ui/fx;->m(Landroid/os/Bundle;)V

    return-void
.end method

.method static synthetic b(Lcom/peel/ui/fx;)Landroid/widget/CheckedTextView;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lcom/peel/ui/fx;->aw:Landroid/widget/CheckedTextView;

    return-object v0
.end method

.method static synthetic c(Lcom/peel/ui/fx;)Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lcom/peel/ui/fx;->aB:Landroid/os/Bundle;

    return-object v0
.end method

.method static synthetic d(Lcom/peel/ui/fx;)Landroid/widget/CheckedTextView;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lcom/peel/ui/fx;->av:Landroid/widget/CheckedTextView;

    return-object v0
.end method

.method static synthetic e(Lcom/peel/ui/fx;)Landroid/widget/CheckedTextView;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lcom/peel/ui/fx;->au:Landroid/widget/CheckedTextView;

    return-object v0
.end method

.method private m(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 280
    invoke-virtual {p0}, Lcom/peel/ui/fx;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/ae;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const/16 v1, 0x7d0

    new-instance v2, Lcom/peel/ui/gd;

    invoke-direct {v2, p0}, Lcom/peel/ui/gd;-><init>(Lcom/peel/ui/fx;)V

    invoke-static {v0, p1, v1, v2}, Lcom/peel/util/bx;->a(Landroid/content/Context;Landroid/os/Bundle;ILcom/peel/util/t;)V

    .line 296
    return-void
.end method


# virtual methods
.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    .line 300
    sget v0, Lcom/peel/ui/fq;->reminder_syndicate_layout:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 301
    sget v0, Lcom/peel/ui/fp;->show_image:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/peel/ui/fx;->an:Landroid/widget/ImageView;

    .line 302
    sget v0, Lcom/peel/ui/fp;->show_title:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/peel/ui/fx;->ao:Landroid/widget/TextView;

    .line 303
    sget v0, Lcom/peel/ui/fp;->episode_name:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/peel/ui/fx;->ap:Landroid/widget/TextView;

    .line 304
    sget v0, Lcom/peel/ui/fp;->genre:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/peel/ui/fx;->aq:Landroid/widget/TextView;

    .line 305
    sget v0, Lcom/peel/ui/fp;->showtime:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/peel/ui/fx;->ar:Landroid/widget/TextView;

    .line 306
    sget v0, Lcom/peel/ui/fp;->channel:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/peel/ui/fx;->as:Landroid/widget/TextView;

    .line 307
    sget v0, Lcom/peel/ui/fp;->done_btn:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/peel/ui/fx;->ax:Landroid/widget/Button;

    .line 308
    sget v0, Lcom/peel/ui/fp;->current_episode_only:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckedTextView;

    iput-object v0, p0, Lcom/peel/ui/fx;->aw:Landroid/widget/CheckedTextView;

    .line 309
    sget v0, Lcom/peel/ui/fp;->new_episode_only:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckedTextView;

    iput-object v0, p0, Lcom/peel/ui/fx;->au:Landroid/widget/CheckedTextView;

    .line 310
    sget v0, Lcom/peel/ui/fp;->new_episode_rerun:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckedTextView;

    iput-object v0, p0, Lcom/peel/ui/fx;->av:Landroid/widget/CheckedTextView;

    .line 311
    sget v0, Lcom/peel/ui/fp;->header:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/peel/ui/fx;->at:Landroid/widget/TextView;

    .line 312
    sget v0, Lcom/peel/ui/fp;->option_container:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/ui/fx;->ay:Landroid/view/View;

    .line 314
    return-object v1
.end method

.method public a_(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 43
    invoke-super {p0, p1}, Lcom/peel/d/t;->a_(Landroid/os/Bundle;)V

    .line 44
    const/4 v0, 0x0

    const v1, 0x1030071

    invoke-virtual {p0, v0, v1}, Lcom/peel/ui/fx;->a(II)V

    .line 45
    return-void
.end method

.method public d(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 49
    invoke-super {p0, p1}, Lcom/peel/d/t;->d(Landroid/os/Bundle;)V

    .line 51
    iget-object v0, p0, Lcom/peel/ui/fx;->ak:Landroid/os/Bundle;

    const-string/jumbo v1, "listing"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/peel/content/listing/LiveListing;

    iput-object v0, p0, Lcom/peel/ui/fx;->am:Lcom/peel/content/listing/LiveListing;

    .line 52
    iget-object v0, p0, Lcom/peel/ui/fx;->ak:Landroid/os/Bundle;

    invoke-virtual {p0, v0}, Lcom/peel/ui/fx;->l(Landroid/os/Bundle;)V

    .line 53
    return-void
.end method

.method public l(Landroid/os/Bundle;)V
    .locals 12

    .prologue
    const-wide/16 v10, 0x0

    const/16 v9, 0x8

    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 56
    iget-object v0, p0, Lcom/peel/ui/fx;->am:Lcom/peel/content/listing/LiveListing;

    invoke-virtual {v0}, Lcom/peel/content/listing/LiveListing;->m()[Ljava/lang/String;

    move-result-object v0

    .line 57
    if-nez v0, :cond_7

    .line 58
    iget-object v0, p0, Lcom/peel/ui/fx;->aq:Landroid/widget/TextView;

    invoke-virtual {v0, v9}, Landroid/widget/TextView;->setVisibility(I)V

    .line 63
    :goto_0
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, Lcom/peel/ui/fx;->aB:Landroid/os/Bundle;

    .line 65
    invoke-virtual {p0}, Lcom/peel/ui/fx;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string/jumbo v1, "country_ISO"

    const-string/jumbo v2, "US"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 67
    const-string/jumbo v1, "showoption"

    invoke-virtual {p1, v1, v6}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_e

    const-string/jumbo v1, "US"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string/jumbo v1, "CA"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 68
    :cond_0
    iget-object v0, p0, Lcom/peel/ui/fx;->at:Landroid/widget/TextView;

    sget v1, Lcom/peel/ui/ft;->title_set_reminder:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 69
    iget-object v0, p0, Lcom/peel/ui/fx;->ay:Landroid/view/View;

    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    .line 70
    const-string/jumbo v0, "action"

    const-string/jumbo v1, "none"

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "new"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 71
    iget-object v0, p0, Lcom/peel/ui/fx;->ay:Landroid/view/View;

    invoke-virtual {v0, v9}, Landroid/view/View;->setVisibility(I)V

    .line 72
    iget-object v0, p0, Lcom/peel/ui/fx;->am:Lcom/peel/content/listing/LiveListing;

    invoke-virtual {v0}, Lcom/peel/content/listing/LiveListing;->k()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "sports"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 73
    iget-object v0, p0, Lcom/peel/ui/fx;->am:Lcom/peel/content/listing/LiveListing;

    invoke-virtual {v0}, Lcom/peel/content/listing/LiveListing;->s()[Lcom/peel/data/SportsTeam;

    move-result-object v0

    .line 74
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 76
    aget-object v0, v0, v6

    invoke-virtual {v0}, Lcom/peel/data/SportsTeam;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 78
    iget-object v0, p0, Lcom/peel/ui/fx;->aB:Landroid/os/Bundle;

    const-string/jumbo v2, "remindertype"

    const-string/jumbo v3, "team"

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 79
    iget-object v0, p0, Lcom/peel/ui/fx;->aB:Landroid/os/Bundle;

    const-string/jumbo v2, "teamids"

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 80
    iget-object v0, p0, Lcom/peel/ui/fx;->aB:Landroid/os/Bundle;

    const-string/jumbo v1, "listing"

    iget-object v2, p0, Lcom/peel/ui/fx;->am:Lcom/peel/content/listing/LiveListing;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 81
    iget-object v0, p0, Lcom/peel/ui/fx;->aB:Landroid/os/Bundle;

    invoke-direct {p0, v0}, Lcom/peel/ui/fx;->m(Landroid/os/Bundle;)V

    .line 92
    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/peel/ui/fx;->am:Lcom/peel/content/listing/LiveListing;

    invoke-virtual {v0}, Lcom/peel/content/listing/LiveListing;->k()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "sports"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 93
    iget-object v0, p0, Lcom/peel/ui/fx;->aw:Landroid/widget/CheckedTextView;

    sget v1, Lcom/peel/ui/fo;->btn_white_checkbox_states:I

    invoke-virtual {v0, v1}, Landroid/widget/CheckedTextView;->setCheckMarkDrawable(I)V

    .line 94
    iget-object v0, p0, Lcom/peel/ui/fx;->au:Landroid/widget/CheckedTextView;

    sget v1, Lcom/peel/ui/fo;->btn_white_checkbox_states:I

    invoke-virtual {v0, v1}, Landroid/widget/CheckedTextView;->setCheckMarkDrawable(I)V

    .line 95
    iget-object v0, p0, Lcom/peel/ui/fx;->av:Landroid/widget/CheckedTextView;

    sget v1, Lcom/peel/ui/fo;->btn_white_checkbox_states:I

    invoke-virtual {v0, v1}, Landroid/widget/CheckedTextView;->setCheckMarkDrawable(I)V

    .line 96
    iget-object v0, p0, Lcom/peel/ui/fx;->aw:Landroid/widget/CheckedTextView;

    sget v1, Lcom/peel/ui/ft;->current_sport:I

    invoke-virtual {v0, v1}, Landroid/widget/CheckedTextView;->setText(I)V

    .line 97
    sget-object v0, Lcom/peel/content/a;->f:Lcom/peel/content/user/User;

    invoke-virtual {v0}, Lcom/peel/content/user/User;->p()Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/ui/fx;->az:Ljava/util/Map;

    .line 99
    iget-object v0, p0, Lcom/peel/ui/fx;->am:Lcom/peel/content/listing/LiveListing;

    invoke-virtual {v0}, Lcom/peel/content/listing/LiveListing;->s()[Lcom/peel/data/SportsTeam;

    move-result-object v0

    .line 101
    if-eqz v0, :cond_3

    .line 102
    iget-object v1, p0, Lcom/peel/ui/fx;->au:Landroid/widget/CheckedTextView;

    sget v2, Lcom/peel/ui/ft;->all_sports:I

    invoke-virtual {p0, v2}, Lcom/peel/ui/fx;->a(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, v5, [Ljava/lang/Object;

    aget-object v4, v0, v6

    invoke-virtual {v4}, Lcom/peel/data/SportsTeam;->b()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/CheckedTextView;->setText(Ljava/lang/CharSequence;)V

    .line 103
    iget-object v1, p0, Lcom/peel/ui/fx;->av:Landroid/widget/CheckedTextView;

    sget v2, Lcom/peel/ui/ft;->all_sports:I

    invoke-virtual {p0, v2}, Lcom/peel/ui/fx;->a(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, v5, [Ljava/lang/Object;

    aget-object v4, v0, v5

    invoke-virtual {v4}, Lcom/peel/data/SportsTeam;->b()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/CheckedTextView;->setText(Ljava/lang/CharSequence;)V

    .line 105
    iget-object v1, p0, Lcom/peel/ui/fx;->az:Ljava/util/Map;

    if-eqz v1, :cond_3

    .line 106
    iget-object v1, p0, Lcom/peel/ui/fx;->az:Ljava/util/Map;

    aget-object v2, v0, v6

    invoke-virtual {v2}, Lcom/peel/data/SportsTeam;->a()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 107
    iget-object v1, p0, Lcom/peel/ui/fx;->au:Landroid/widget/CheckedTextView;

    invoke-virtual {v1, v5}, Landroid/widget/CheckedTextView;->setChecked(Z)V

    .line 110
    :cond_2
    iget-object v1, p0, Lcom/peel/ui/fx;->az:Ljava/util/Map;

    aget-object v0, v0, v5

    invoke-virtual {v0}, Lcom/peel/data/SportsTeam;->a()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 111
    iget-object v0, p0, Lcom/peel/ui/fx;->av:Landroid/widget/CheckedTextView;

    invoke-virtual {v0, v5}, Landroid/widget/CheckedTextView;->setChecked(Z)V

    .line 116
    :cond_3
    iget-object v0, p0, Lcom/peel/ui/fx;->au:Landroid/widget/CheckedTextView;

    invoke-virtual {v0}, Landroid/widget/CheckedTextView;->isChecked()Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/peel/ui/fx;->av:Landroid/widget/CheckedTextView;

    invoke-virtual {v0}, Landroid/widget/CheckedTextView;->isChecked()Z

    move-result v0

    if-nez v0, :cond_4

    .line 117
    iget-object v0, p0, Lcom/peel/ui/fx;->aw:Landroid/widget/CheckedTextView;

    invoke-virtual {v0, v5}, Landroid/widget/CheckedTextView;->setChecked(Z)V

    .line 156
    :cond_4
    :goto_2
    iget-object v0, p0, Lcom/peel/ui/fx;->am:Lcom/peel/content/listing/LiveListing;

    invoke-virtual {v0}, Lcom/peel/content/listing/LiveListing;->o()J

    move-result-wide v2

    .line 157
    iget-object v0, p0, Lcom/peel/ui/fx;->am:Lcom/peel/content/listing/LiveListing;

    invoke-virtual {v0}, Lcom/peel/content/listing/LiveListing;->j()J

    move-result-wide v4

    .line 158
    add-long v0, v2, v4

    .line 160
    iget-object v6, p0, Lcom/peel/ui/fx;->am:Lcom/peel/content/listing/LiveListing;

    invoke-virtual {v6}, Lcom/peel/content/listing/LiveListing;->c()Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_5

    .line 161
    iget-object v6, p0, Lcom/peel/ui/fx;->as:Landroid/widget/TextView;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v8, p0, Lcom/peel/ui/fx;->am:Lcom/peel/content/listing/LiveListing;

    invoke-virtual {v8}, Lcom/peel/content/listing/LiveListing;->c()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, " - "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/peel/ui/fx;->am:Lcom/peel/content/listing/LiveListing;

    invoke-virtual {v8}, Lcom/peel/content/listing/LiveListing;->d()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 163
    :cond_5
    cmp-long v6, v2, v10

    if-lez v6, :cond_10

    cmp-long v0, v0, v10

    if-lez v0, :cond_10

    .line 164
    iget-object v1, p0, Lcom/peel/ui/fx;->ar:Landroid/widget/TextView;

    sget-object v0, Lcom/peel/util/x;->c:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/text/SimpleDateFormat;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/peel/ui/fx;->m()Landroid/support/v4/app/ae;

    move-result-object v2

    invoke-static {v2}, Landroid/text/format/DateFormat;->is24HourFormat(Landroid/content/Context;)Z

    move-result v2

    sget v3, Lcom/peel/ui/ft;->time_pattern:I

    invoke-virtual {p0, v3}, Lcom/peel/ui/fx;->a(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v4, v5, v2, v3}, Lcom/peel/util/x;->a(Ljava/lang/String;JZLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 169
    :goto_3
    iget-object v0, p0, Lcom/peel/ui/fx;->am:Lcom/peel/content/listing/LiveListing;

    const/16 v1, 0xa9

    invoke-static {v0, v1}, Lcom/peel/util/bx;->a(Lcom/peel/content/listing/Listing;I)I

    move-result v1

    .line 170
    iget-object v0, p0, Lcom/peel/ui/fx;->ao:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/peel/ui/fx;->am:Lcom/peel/content/listing/LiveListing;

    invoke-virtual {v2}, Lcom/peel/content/listing/LiveListing;->l()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 171
    iget-object v0, p0, Lcom/peel/ui/fx;->ap:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/peel/ui/fx;->am:Lcom/peel/content/listing/LiveListing;

    invoke-virtual {v2}, Lcom/peel/content/listing/LiveListing;->v()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 173
    const-string/jumbo v0, "image_url"

    const/4 v2, 0x0

    invoke-virtual {p1, v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 174
    iget-object v2, p0, Lcom/peel/ui/fx;->an:Landroid/widget/ImageView;

    invoke-virtual {v2, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 176
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_11

    .line 177
    invoke-virtual {p0}, Lcom/peel/ui/fx;->m()Landroid/support/v4/app/ae;

    move-result-object v2

    invoke-static {v2}, Lcom/peel/util/b/e;->a(Landroid/content/Context;)Lcom/squareup/picasso/Picasso;

    move-result-object v2

    .line 178
    invoke-virtual {v2, v0}, Lcom/squareup/picasso/Picasso;->load(Ljava/lang/String;)Lcom/squareup/picasso/RequestCreator;

    move-result-object v2

    .line 179
    invoke-virtual {v2, v1}, Lcom/squareup/picasso/RequestCreator;->placeholder(I)Lcom/squareup/picasso/RequestCreator;

    move-result-object v2

    .line 180
    invoke-virtual {v2}, Lcom/squareup/picasso/RequestCreator;->noFade()Lcom/squareup/picasso/RequestCreator;

    move-result-object v2

    iget-object v3, p0, Lcom/peel/ui/fx;->an:Landroid/widget/ImageView;

    .line 181
    invoke-virtual {v2, v3}, Lcom/squareup/picasso/RequestCreator;->into(Landroid/widget/ImageView;)V

    .line 187
    :goto_4
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_6

    .line 188
    invoke-virtual {p0}, Lcom/peel/ui/fx;->m()Landroid/support/v4/app/ae;

    move-result-object v2

    invoke-static {v2}, Lcom/peel/util/b/e;->a(Landroid/content/Context;)Lcom/squareup/picasso/Picasso;

    move-result-object v2

    .line 189
    invoke-virtual {v2, v0}, Lcom/squareup/picasso/Picasso;->load(Ljava/lang/String;)Lcom/squareup/picasso/RequestCreator;

    move-result-object v0

    .line 190
    invoke-virtual {v0, v1}, Lcom/squareup/picasso/RequestCreator;->placeholder(I)Lcom/squareup/picasso/RequestCreator;

    move-result-object v0

    .line 191
    invoke-virtual {v0}, Lcom/squareup/picasso/RequestCreator;->noFade()Lcom/squareup/picasso/RequestCreator;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/ui/fx;->an:Landroid/widget/ImageView;

    .line 192
    invoke-virtual {v0, v1}, Lcom/squareup/picasso/RequestCreator;->into(Landroid/widget/ImageView;)V

    .line 195
    :cond_6
    iget-object v0, p0, Lcom/peel/ui/fx;->aB:Landroid/os/Bundle;

    const-string/jumbo v1, "remindertype"

    const-string/jumbo v2, "schedule"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 196
    iget-object v0, p0, Lcom/peel/ui/fx;->aB:Landroid/os/Bundle;

    const-string/jumbo v1, "listing"

    iget-object v2, p0, Lcom/peel/ui/fx;->am:Lcom/peel/content/listing/LiveListing;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 200
    iget-object v0, p0, Lcom/peel/ui/fx;->ax:Landroid/widget/Button;

    new-instance v1, Lcom/peel/ui/fz;

    invoke-direct {v1, p0}, Lcom/peel/ui/fz;-><init>(Lcom/peel/ui/fx;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 237
    iget-object v0, p0, Lcom/peel/ui/fx;->au:Landroid/widget/CheckedTextView;

    new-instance v1, Lcom/peel/ui/ga;

    invoke-direct {v1, p0}, Lcom/peel/ui/ga;-><init>(Lcom/peel/ui/fx;)V

    invoke-virtual {v0, v1}, Landroid/widget/CheckedTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 250
    iget-object v0, p0, Lcom/peel/ui/fx;->av:Landroid/widget/CheckedTextView;

    new-instance v1, Lcom/peel/ui/gb;

    invoke-direct {v1, p0}, Lcom/peel/ui/gb;-><init>(Lcom/peel/ui/fx;)V

    invoke-virtual {v0, v1}, Landroid/widget/CheckedTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 264
    iget-object v0, p0, Lcom/peel/ui/fx;->aw:Landroid/widget/CheckedTextView;

    new-instance v1, Lcom/peel/ui/gc;

    invoke-direct {v1, p0}, Lcom/peel/ui/gc;-><init>(Lcom/peel/ui/fx;)V

    invoke-virtual {v0, v1}, Landroid/widget/CheckedTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 277
    return-void

    .line 60
    :cond_7
    iget-object v1, p0, Lcom/peel/ui/fx;->aq:Landroid/widget/TextView;

    const-string/jumbo v2, ", "

    invoke-static {v2, v0}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 84
    :cond_8
    iget-object v0, p0, Lcom/peel/ui/fx;->aB:Landroid/os/Bundle;

    const-string/jumbo v1, "extra"

    const-string/jumbo v2, "new"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 85
    iget-object v0, p0, Lcom/peel/ui/fx;->aB:Landroid/os/Bundle;

    const-string/jumbo v1, "remindertype"

    const-string/jumbo v2, "show"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 86
    iget-object v0, p0, Lcom/peel/ui/fx;->aB:Landroid/os/Bundle;

    const-string/jumbo v1, "listing"

    iget-object v2, p0, Lcom/peel/ui/fx;->am:Lcom/peel/content/listing/LiveListing;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 87
    iget-object v0, p0, Lcom/peel/ui/fx;->aB:Landroid/os/Bundle;

    invoke-direct {p0, v0}, Lcom/peel/ui/fx;->m(Landroid/os/Bundle;)V

    goto/16 :goto_1

    .line 120
    :cond_9
    sget-object v0, Lcom/peel/content/a;->f:Lcom/peel/content/user/User;

    invoke-virtual {v0}, Lcom/peel/content/user/User;->q()Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/ui/fx;->aA:Ljava/util/Map;

    .line 122
    iget-object v0, p0, Lcom/peel/ui/fx;->aA:Ljava/util/Map;

    if-eqz v0, :cond_d

    .line 123
    iget-object v0, p0, Lcom/peel/ui/fx;->aA:Ljava/util/Map;

    iget-object v1, p0, Lcom/peel/ui/fx;->am:Lcom/peel/content/listing/LiveListing;

    invoke-virtual {v1}, Lcom/peel/content/listing/LiveListing;->f()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 124
    iget-object v0, p0, Lcom/peel/ui/fx;->aA:Ljava/util/Map;

    iget-object v1, p0, Lcom/peel/ui/fx;->am:Lcom/peel/content/listing/LiveListing;

    invoke-virtual {v1}, Lcom/peel/content/listing/LiveListing;->f()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 125
    if-eqz v0, :cond_a

    const-string/jumbo v1, "all"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 126
    iget-object v0, p0, Lcom/peel/ui/fx;->av:Landroid/widget/CheckedTextView;

    invoke-virtual {v0, v5}, Landroid/widget/CheckedTextView;->setChecked(Z)V

    goto/16 :goto_2

    .line 127
    :cond_a
    if-eqz v0, :cond_b

    const-string/jumbo v1, "new"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_b

    .line 128
    iget-object v0, p0, Lcom/peel/ui/fx;->au:Landroid/widget/CheckedTextView;

    invoke-virtual {v0, v5}, Landroid/widget/CheckedTextView;->setChecked(Z)V

    goto/16 :goto_2

    .line 129
    :cond_b
    if-eqz v0, :cond_4

    const-string/jumbo v1, "null"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 130
    iget-object v0, p0, Lcom/peel/ui/fx;->aw:Landroid/widget/CheckedTextView;

    invoke-virtual {v0, v5}, Landroid/widget/CheckedTextView;->setChecked(Z)V

    goto/16 :goto_2

    .line 133
    :cond_c
    iget-object v0, p0, Lcom/peel/ui/fx;->aw:Landroid/widget/CheckedTextView;

    invoke-virtual {v0, v5}, Landroid/widget/CheckedTextView;->setChecked(Z)V

    goto/16 :goto_2

    .line 136
    :cond_d
    iget-object v0, p0, Lcom/peel/ui/fx;->aw:Landroid/widget/CheckedTextView;

    invoke-virtual {v0, v5}, Landroid/widget/CheckedTextView;->setChecked(Z)V

    goto/16 :goto_2

    .line 141
    :cond_e
    const-string/jumbo v0, "action"

    const-string/jumbo v1, "none"

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "new"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 142
    iget-object v0, p0, Lcom/peel/ui/fx;->au:Landroid/widget/CheckedTextView;

    invoke-virtual {v0, v5}, Landroid/widget/CheckedTextView;->setChecked(Z)V

    .line 144
    :cond_f
    iget-object v0, p0, Lcom/peel/ui/fx;->at:Landroid/widget/TextView;

    sget v1, Lcom/peel/ui/ft;->reminder_is_set:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 145
    iget-object v0, p0, Lcom/peel/ui/fx;->ay:Landroid/view/View;

    invoke-virtual {v0, v9}, Landroid/view/View;->setVisibility(I)V

    .line 147
    sget-object v0, Lcom/peel/ui/fx;->al:Ljava/lang/String;

    const-string/jumbo v1, "dismiss dialog"

    new-instance v2, Lcom/peel/ui/fy;

    invoke-direct {v2, p0}, Lcom/peel/ui/fy;-><init>(Lcom/peel/ui/fx;)V

    const-wide/16 v4, 0xce4

    invoke-static {v0, v1, v2, v4, v5}, Lcom/peel/util/i;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;J)Ljava/lang/Runnable;

    goto/16 :goto_2

    .line 166
    :cond_10
    iget-object v0, p0, Lcom/peel/ui/fx;->ar:Landroid/widget/TextView;

    invoke-virtual {v0, v9}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_3

    .line 183
    :cond_11
    iget-object v0, p0, Lcom/peel/ui/fx;->am:Lcom/peel/content/listing/LiveListing;

    invoke-virtual {v0}, Lcom/peel/content/listing/LiveListing;->y()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_12

    const/4 v0, 0x3

    const/4 v2, 0x4

    const/16 v3, 0x10e

    iget-object v4, p0, Lcom/peel/ui/fx;->am:Lcom/peel/content/listing/LiveListing;

    .line 184
    invoke-virtual {v4}, Lcom/peel/content/listing/LiveListing;->B()Ljava/util/Map;

    move-result-object v4

    .line 183
    invoke-static {v0, v2, v3, v4}, Lcom/peel/util/bx;->a(IIILjava/util/Map;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_4

    :cond_12
    iget-object v0, p0, Lcom/peel/ui/fx;->am:Lcom/peel/content/listing/LiveListing;

    .line 184
    invoke-virtual {v0}, Lcom/peel/content/listing/LiveListing;->y()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_4
.end method
