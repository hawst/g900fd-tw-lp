.class Lcom/peel/ui/jt;
.super Lcom/peel/util/t;


# instance fields
.field final synthetic a:Landroid/view/View;

.field final synthetic b:Lcom/peel/ui/gt;


# direct methods
.method constructor <init>(Lcom/peel/ui/gt;ILandroid/view/View;)V
    .locals 0

    .prologue
    .line 409
    iput-object p1, p0, Lcom/peel/ui/jt;->b:Lcom/peel/ui/gt;

    iput-object p3, p0, Lcom/peel/ui/jt;->a:Landroid/view/View;

    invoke-direct {p0, p2}, Lcom/peel/util/t;-><init>(I)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 413
    sget-boolean v0, Lcom/peel/util/b/a;->a:Z

    if-eqz v0, :cond_1

    .line 428
    :cond_0
    :goto_0
    return-void

    .line 416
    :cond_1
    iget-boolean v0, p0, Lcom/peel/ui/jt;->i:Z

    if-nez v0, :cond_2

    .line 417
    invoke-static {}, Lcom/peel/ui/gt;->S()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "get sponsored remote ctrl theme: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/peel/ui/jt;->k:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 421
    :cond_2
    iget-object v1, p0, Lcom/peel/ui/jt;->b:Lcom/peel/ui/gt;

    iget-object v0, p0, Lcom/peel/ui/jt;->j:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    invoke-static {v1, v0}, Lcom/peel/ui/gt;->a(Lcom/peel/ui/gt;Ljava/lang/String;)Ljava/lang/String;

    .line 423
    iget-object v0, p0, Lcom/peel/ui/jt;->b:Lcom/peel/ui/gt;

    invoke-static {v0}, Lcom/peel/ui/gt;->h(Lcom/peel/ui/gt;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/peel/ui/jt;->b:Lcom/peel/ui/gt;

    invoke-static {v0}, Lcom/peel/ui/gt;->h(Lcom/peel/ui/gt;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 424
    iget-object v0, p0, Lcom/peel/ui/jt;->b:Lcom/peel/ui/gt;

    invoke-virtual {v0}, Lcom/peel/ui/gt;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    invoke-static {v0}, Lcom/peel/util/b/e;->a(Landroid/content/Context;)Lcom/squareup/picasso/Picasso;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/ui/jt;->b:Lcom/peel/ui/gt;

    invoke-static {v1}, Lcom/peel/ui/gt;->h(Lcom/peel/ui/gt;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/picasso/Picasso;->load(Ljava/lang/String;)Lcom/squareup/picasso/RequestCreator;

    move-result-object v0

    sget v1, Lcom/peel/ui/fo;->controller_bg:I

    invoke-virtual {v0, v1}, Lcom/squareup/picasso/RequestCreator;->placeholder(I)Lcom/squareup/picasso/RequestCreator;

    move-result-object v1

    iget-object v0, p0, Lcom/peel/ui/jt;->a:Landroid/view/View;

    sget v2, Lcom/peel/ui/fp;->img_controlpad:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Lcom/squareup/picasso/RequestCreator;->into(Landroid/widget/ImageView;)V

    .line 425
    iget-object v0, p0, Lcom/peel/ui/jt;->b:Lcom/peel/ui/gt;

    invoke-virtual {v0}, Lcom/peel/ui/gt;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    invoke-static {v0}, Lcom/peel/util/b/e;->a(Landroid/content/Context;)Lcom/squareup/picasso/Picasso;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/ui/jt;->b:Lcom/peel/ui/gt;

    invoke-static {v1}, Lcom/peel/ui/gt;->h(Lcom/peel/ui/gt;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/picasso/Picasso;->load(Ljava/lang/String;)Lcom/squareup/picasso/RequestCreator;

    move-result-object v0

    sget v1, Lcom/peel/ui/fo;->controller_bg:I

    invoke-virtual {v0, v1}, Lcom/squareup/picasso/RequestCreator;->placeholder(I)Lcom/squareup/picasso/RequestCreator;

    move-result-object v1

    iget-object v0, p0, Lcom/peel/ui/jt;->a:Landroid/view/View;

    sget v2, Lcom/peel/ui/fp;->img_device_controlpad:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Lcom/squareup/picasso/RequestCreator;->into(Landroid/widget/ImageView;)V

    .line 426
    iget-object v0, p0, Lcom/peel/ui/jt;->a:Landroid/view/View;

    sget v1, Lcom/peel/ui/fp;->content_new:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_0
.end method
