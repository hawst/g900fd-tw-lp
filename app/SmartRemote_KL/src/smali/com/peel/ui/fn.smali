.class public final Lcom/peel/ui/fn;
.super Ljava/lang/Object;


# static fields
.field public static final ab_indeterminate_progress_bar_size:I = 0x7f090000

.field public static final abc_action_bar_default_height_material:I = 0x7f090001

.field public static final abc_action_bar_default_padding_material:I = 0x7f090002

.field public static final abc_action_bar_icon_vertical_padding_material:I = 0x7f090003

.field public static final abc_action_bar_progress_bar_size:I = 0x7f090004

.field public static final abc_action_bar_stacked_max_height:I = 0x7f090005

.field public static final abc_action_bar_stacked_tab_max_width:I = 0x7f090006

.field public static final abc_action_bar_subtitle_bottom_margin_material:I = 0x7f090007

.field public static final abc_action_bar_subtitle_top_margin_material:I = 0x7f090008

.field public static final abc_action_button_min_height_material:I = 0x7f090009

.field public static final abc_action_button_min_width_material:I = 0x7f09000a

.field public static final abc_action_button_min_width_overflow_material:I = 0x7f09000b

.field public static final abc_config_prefDialogWidth:I = 0x7f09000c

.field public static final abc_control_inset_material:I = 0x7f09000d

.field public static final abc_control_padding_material:I = 0x7f09000e

.field public static final abc_dropdownitem_icon_width:I = 0x7f09000f

.field public static final abc_dropdownitem_text_padding_left:I = 0x7f090010

.field public static final abc_dropdownitem_text_padding_right:I = 0x7f090011

.field public static final abc_panel_menu_list_width:I = 0x7f090012

.field public static final abc_search_view_preferred_width:I = 0x7f090013

.field public static final abc_search_view_text_min_width:I = 0x7f090014

.field public static final abc_text_size_body_1_material:I = 0x7f090015

.field public static final abc_text_size_body_2_material:I = 0x7f090016

.field public static final abc_text_size_button_material:I = 0x7f090017

.field public static final abc_text_size_caption_material:I = 0x7f090018

.field public static final abc_text_size_display_1_material:I = 0x7f090019

.field public static final abc_text_size_display_2_material:I = 0x7f09001a

.field public static final abc_text_size_display_3_material:I = 0x7f09001b

.field public static final abc_text_size_display_4_material:I = 0x7f09001c

.field public static final abc_text_size_headline_material:I = 0x7f09001d

.field public static final abc_text_size_large_material:I = 0x7f09001e

.field public static final abc_text_size_medium_material:I = 0x7f09001f

.field public static final abc_text_size_menu_material:I = 0x7f090020

.field public static final abc_text_size_small_material:I = 0x7f090021

.field public static final abc_text_size_subhead_material:I = 0x7f090022

.field public static final abc_text_size_subtitle_material_toolbar:I = 0x7f090023

.field public static final abc_text_size_title_material:I = 0x7f090024

.field public static final abc_text_size_title_material_toolbar:I = 0x7f090025

.field public static final add_device_padding:I = 0x7f090026

.field public static final always_widget_ac_ctrl_btn_textsize:I = 0x7f090027

.field public static final always_widget_ac_mode_btn_drawable_padding:I = 0x7f090028

.field public static final always_widget_btn_height:I = 0x7f090029

.field public static final always_widget_btn_margin:I = 0x7f09002a

.field public static final always_widget_btn_width:I = 0x7f09002b

.field public static final always_widget_command_holder_height:I = 0x7f09002c

.field public static final always_widget_command_holder_margin_left:I = 0x7f09002d

.field public static final always_widget_command_holder_margin_right:I = 0x7f09002e

.field public static final always_widget_command_holder_margin_top:I = 0x7f09002f

.field public static final always_widget_device_selector_height:I = 0x7f090030

.field public static final always_widget_device_selector_icon_height:I = 0x7f090031

.field public static final always_widget_device_selector_icon_margin_left:I = 0x7f090032

.field public static final always_widget_device_selector_icon_margin_top:I = 0x7f090033

.field public static final always_widget_device_selector_icon_width:I = 0x7f090034

.field public static final always_widget_device_selector_margin:I = 0x7f090035

.field public static final always_widget_device_selector_name_margin_left:I = 0x7f090036

.field public static final always_widget_device_selector_name_margin_right:I = 0x7f090037

.field public static final always_widget_device_selector_name_textsize:I = 0x7f090038

.field public static final always_widget_device_selector_next_btn_width:I = 0x7f090039

.field public static final always_widget_dvr_container_margin_left:I = 0x7f09003a

.field public static final always_widget_dvr_container_margin_right:I = 0x7f09003b

.field public static final always_widget_pw_btn_drawable_left:I = 0x7f09003c

.field public static final always_widget_pw_btn_drawable_padding:I = 0x7f09003d

.field public static final always_widget_pw_btn_height:I = 0x7f09003e

.field public static final always_widget_pw_btn_textsize:I = 0x7f09003f

.field public static final always_widget_pw_btn_width:I = 0x7f090040

.field public static final always_widget_pw_container_height:I = 0x7f090041

.field public static final always_widget_pw_container_width:I = 0x7f090042

.field public static final always_widget_tv_holder_height:I = 0x7f090043

.field public static final always_widget_tv_holder_width:I = 0x7f090044

.field public static final always_widget_vol_btn_height:I = 0x7f090045

.field public static final always_widget_vol_btn_width:I = 0x7f090046

.field public static final always_widget_vol_container_height:I = 0x7f090047

.field public static final always_widget_vol_container_width:I = 0x7f090048

.field public static final badge_height:I = 0x7f090049

.field public static final badge_width:I = 0x7f09004a

.field public static final button_title_text_size:I = 0x7f09004b

.field public static final button_title_text_size_hdpi:I = 0x7f09004c

.field public static final channel_search_width:I = 0x7f09004d

.field public static final channelchanger_top:I = 0x7f09004e

.field public static final com_facebook_likeboxcountview_border_radius:I = 0x7f09004f

.field public static final com_facebook_likeboxcountview_border_width:I = 0x7f090050

.field public static final com_facebook_likeboxcountview_caret_height:I = 0x7f090051

.field public static final com_facebook_likeboxcountview_caret_width:I = 0x7f090052

.field public static final com_facebook_likeboxcountview_text_padding:I = 0x7f090053

.field public static final com_facebook_likeboxcountview_text_size:I = 0x7f090054

.field public static final com_facebook_likebutton_compound_drawable_padding:I = 0x7f090055

.field public static final com_facebook_likebutton_padding_bottom:I = 0x7f090056

.field public static final com_facebook_likebutton_padding_left:I = 0x7f090057

.field public static final com_facebook_likebutton_padding_right:I = 0x7f090058

.field public static final com_facebook_likebutton_padding_top:I = 0x7f090059

.field public static final com_facebook_likebutton_text_size:I = 0x7f09005a

.field public static final com_facebook_likeview_edge_padding:I = 0x7f09005b

.field public static final com_facebook_likeview_internal_padding:I = 0x7f09005c

.field public static final com_facebook_likeview_text_size:I = 0x7f09005d

.field public static final com_facebook_loginview_compound_drawable_padding:I = 0x7f09005e

.field public static final com_facebook_loginview_padding_bottom:I = 0x7f09005f

.field public static final com_facebook_loginview_padding_left:I = 0x7f090060

.field public static final com_facebook_loginview_padding_right:I = 0x7f090061

.field public static final com_facebook_loginview_padding_top:I = 0x7f090062

.field public static final com_facebook_loginview_text_size:I = 0x7f090063

.field public static final com_facebook_picker_divider_width:I = 0x7f0900c9

.field public static final com_facebook_picker_place_image_size:I = 0x7f090064

.field public static final com_facebook_profilepictureview_preset_size_large:I = 0x7f090065

.field public static final com_facebook_profilepictureview_preset_size_normal:I = 0x7f090066

.field public static final com_facebook_profilepictureview_preset_size_small:I = 0x7f090067

.field public static final com_facebook_tooltip_horizontal_padding:I = 0x7f090068

.field public static final com_facebook_usersettingsfragment_profile_picture_height:I = 0x7f090069

.field public static final com_facebook_usersettingsfragment_profile_picture_width:I = 0x7f09006a

.field public static final controlpad_side_margin:I = 0x7f09006b

.field public static final default_circle_indicator_radius:I = 0x7f09006c

.field public static final default_circle_indicator_stroke_width:I = 0x7f09006d

.field public static final default_line_indicator_gap_width:I = 0x7f09006e

.field public static final default_line_indicator_line_width:I = 0x7f09006f

.field public static final default_line_indicator_stroke_width:I = 0x7f090070

.field public static final default_title_indicator_clip_padding:I = 0x7f090071

.field public static final default_title_indicator_footer_indicator_height:I = 0x7f090072

.field public static final default_title_indicator_footer_indicator_underline_padding:I = 0x7f090073

.field public static final default_title_indicator_footer_line_height:I = 0x7f090074

.field public static final default_title_indicator_footer_padding:I = 0x7f090075

.field public static final default_title_indicator_text_size:I = 0x7f090076

.field public static final default_title_indicator_title_padding:I = 0x7f090077

.field public static final default_title_indicator_top_padding:I = 0x7f090078

.field public static final detail_top_scroll_low_limit:I = 0x7f090079

.field public static final detail_top_scroll_max_limit:I = 0x7f09007a

.field public static final dialog_fixed_height_major:I = 0x7f09007b

.field public static final dialog_fixed_height_minor:I = 0x7f09007c

.field public static final dialog_fixed_width_major:I = 0x7f09007d

.field public static final dialog_fixed_width_minor:I = 0x7f09007e

.field public static final disabled_alpha_material_dark:I = 0x7f09007f

.field public static final disabled_alpha_material_light:I = 0x7f090080

.field public static final divider_height:I = 0x7f090081

.field public static final divider_width:I = 0x7f090082

.field public static final footer_height:I = 0x7f090083

.field public static final handle_total_height:I = 0x7f090084

.field public static final lockscreen_controller_area_height:I = 0x7f090085

.field public static final lockscreen_controller_area_height_hdpi:I = 0x7f090086

.field public static final lockscreen_controller_area_height_xxhdpi:I = 0x7f090087

.field public static final lockscreen_controller_collapsed_height_swhdpi:I = 0x7f090088

.field public static final lockscreen_controller_collapsed_height_xxhdpi:I = 0x7f090089

.field public static final lockscreen_device_title_text_size:I = 0x7f09008a

.field public static final lockscreen_min_expanded_height:I = 0x7f09008b

.field public static final lockscreen_xxhdpi_height:I = 0x7f09008c

.field public static final lockscreen_xxhdpi_width:I = 0x7f09008d

.field public static final navigate_down_hit_bottom:I = 0x7f09008e

.field public static final navigate_down_hit_left:I = 0x7f09008f

.field public static final navigate_down_hit_right:I = 0x7f090090

.field public static final navigate_down_hit_top:I = 0x7f090091

.field public static final navigate_left_hit_bottom:I = 0x7f090092

.field public static final navigate_left_hit_left:I = 0x7f090093

.field public static final navigate_left_hit_right:I = 0x7f090094

.field public static final navigate_left_hit_top:I = 0x7f090095

.field public static final navigate_right_hit_bottom:I = 0x7f090096

.field public static final navigate_right_hit_left:I = 0x7f090097

.field public static final navigate_right_hit_right:I = 0x7f090098

.field public static final navigate_right_hit_top:I = 0x7f090099

.field public static final navigate_up_hit_bottom:I = 0x7f09009a

.field public static final navigate_up_hit_left:I = 0x7f09009b

.field public static final navigate_up_hit_right:I = 0x7f09009c

.field public static final navigate_up_hit_top:I = 0x7f09009d

.field public static final notification_placeholder_sdk21_margin_left:I = 0x7f09009e

.field public static final notification_placeholder_sdk21_margin_right:I = 0x7f09009f

.field public static final notiremote_controllerarea_height:I = 0x7f0900a0

.field public static final notiremote_controllerarea_height_hdpi:I = 0x7f0900a1

.field public static final overflow_menu_width:I = 0x7f0900a2

.field public static final profile_photo_length:I = 0x7f0900a3

.field public static final progress_dialog_text:I = 0x7f0900a4

.field public static final progress_height:I = 0x7f0900a5

.field public static final remote_line_height:I = 0x7f0900a6

.field public static final remote_plus_size_height:I = 0x7f0900a7

.field public static final remote_plus_size_width:I = 0x7f0900a8

.field public static final remote_power_height:I = 0x7f0900a9

.field public static final remote_power_width:I = 0x7f0900aa

.field public static final remote_show_height:I = 0x7f0900ab

.field public static final remote_show_width:I = 0x7f0900ac

.field public static final resolution_lang_textsize:I = 0x7f0900ad

.field public static final settings_tile_height:I = 0x7f0900ae

.field public static final settings_tile_width:I = 0x7f0900af

.field public static final setup_show_limit:I = 0x7f0900b0

.field public static final shadow_width:I = 0x7f0900b1

.field public static final slidemenu_item_height:I = 0x7f0900b2

.field public static final slidemenu_width:I = 0x7f0900b3

.field public static final slidingmenu_offset:I = 0x7f0900b4

.field public static final spacer_top_padding:I = 0x7f0900b5

.field public static final spinner_height:I = 0x7f0900b6

.field public static final spinner_left:I = 0x7f0900b7

.field public static final stepview_bottom:I = 0x7f0900b8

.field public static final stepview_top:I = 0x7f0900b9

.field public static final test_btn_pager_margin:I = 0x7f0900ba

.field public static final tile_height:I = 0x7f0900bb

.field public static final tile_height_actual:I = 0x7f0900bc

.field public static final tile_view_hspace:I = 0x7f0900bd

.field public static final tile_view_vspace:I = 0x7f0900be

.field public static final tile_width:I = 0x7f0900bf

.field public static final toast_textsize:I = 0x7f0900ca

.field public static final topDistance:I = 0x7f0900c0

.field public static final topOffset:I = 0x7f0900c1

.field public static final turnonmessage_top:I = 0x7f0900c2

.field public static final tvsignal_top:I = 0x7f0900c3

.field public static final tvview_top:I = 0x7f0900c4

.field public static final twitt_image_height:I = 0x7f0900c5

.field public static final twitt_image_width:I = 0x7f0900c6

.field public static final width_remote_channel_option:I = 0x7f0900c7

.field public static final zip_layout_height:I = 0x7f0900c8
