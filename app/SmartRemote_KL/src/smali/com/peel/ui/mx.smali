.class public Lcom/peel/ui/mx;
.super Landroid/widget/BaseAdapter;


# instance fields
.field private a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/peel/ui/model/TwittData;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Landroid/view/LayoutInflater;

.field private final c:Landroid/support/v4/app/ae;

.field private final d:Lcom/peel/d/v;

.field private final e:[Ljava/lang/String;

.field private final f:I

.field private final g:Ljava/lang/String;

.field private final h:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/support/v4/app/ae;ILjava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 48
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 49
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/ui/mx;->b:Landroid/view/LayoutInflater;

    .line 50
    iput-object p1, p0, Lcom/peel/ui/mx;->c:Landroid/support/v4/app/ae;

    .line 51
    invoke-static {p1}, Lcom/peel/d/v;->a(Landroid/content/Context;)Lcom/peel/d/v;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/ui/mx;->d:Lcom/peel/d/v;

    .line 52
    iput p2, p0, Lcom/peel/ui/mx;->f:I

    .line 53
    iput-object p3, p0, Lcom/peel/ui/mx;->g:Ljava/lang/String;

    .line 54
    iput-object p4, p0, Lcom/peel/ui/mx;->h:Ljava/lang/String;

    .line 55
    iput-object p5, p0, Lcom/peel/ui/mx;->e:[Ljava/lang/String;

    .line 56
    return-void
.end method

.method private a()V
    .locals 3

    .prologue
    .line 257
    new-instance v0, Lcom/peel/widget/ag;

    iget-object v1, p0, Lcom/peel/ui/mx;->c:Landroid/support/v4/app/ae;

    invoke-direct {v0, v1}, Lcom/peel/widget/ag;-><init>(Landroid/content/Context;)V

    sget v1, Lcom/peel/ui/ft;->login_twitter:I

    invoke-virtual {v0, v1}, Lcom/peel/widget/ag;->a(I)Lcom/peel/widget/ag;

    move-result-object v0

    sget v1, Lcom/peel/ui/ft;->twitter_login_desc:I

    invoke-virtual {v0, v1}, Lcom/peel/widget/ag;->b(I)Lcom/peel/widget/ag;

    move-result-object v0

    sget v1, Lcom/peel/ui/ft;->login:I

    new-instance v2, Lcom/peel/ui/ng;

    invoke-direct {v2, p0}, Lcom/peel/ui/ng;-><init>(Lcom/peel/ui/mx;)V

    invoke-virtual {v0, v1, v2}, Lcom/peel/widget/ag;->a(ILandroid/content/DialogInterface$OnClickListener;)Lcom/peel/widget/ag;

    move-result-object v0

    sget v1, Lcom/peel/ui/ft;->cancel:I

    new-instance v2, Lcom/peel/ui/nf;

    invoke-direct {v2, p0}, Lcom/peel/ui/nf;-><init>(Lcom/peel/ui/mx;)V

    .line 266
    invoke-virtual {v0, v1, v2}, Lcom/peel/widget/ag;->c(ILandroid/content/DialogInterface$OnClickListener;)Lcom/peel/widget/ag;

    move-result-object v0

    .line 271
    invoke-virtual {v0}, Lcom/peel/widget/ag;->d()Lcom/peel/widget/ag;

    .line 272
    return-void
.end method

.method static synthetic a(Lcom/peel/ui/mx;)[Ljava/lang/String;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/peel/ui/mx;->e:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic b(Lcom/peel/ui/mx;)I
    .locals 1

    .prologue
    .line 39
    iget v0, p0, Lcom/peel/ui/mx;->f:I

    return v0
.end method

.method static synthetic c(Lcom/peel/ui/mx;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/peel/ui/mx;->g:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic d(Lcom/peel/ui/mx;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/peel/ui/mx;->h:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic e(Lcom/peel/ui/mx;)Landroid/support/v4/app/ae;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/peel/ui/mx;->c:Landroid/support/v4/app/ae;

    return-object v0
.end method

.method static synthetic f(Lcom/peel/ui/mx;)V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0}, Lcom/peel/ui/mx;->a()V

    return-void
.end method

.method static synthetic g(Lcom/peel/ui/mx;)Lcom/peel/d/v;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/peel/ui/mx;->d:Lcom/peel/d/v;

    return-object v0
.end method


# virtual methods
.method public a(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/peel/ui/model/TwittData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 74
    iput-object p1, p0, Lcom/peel/ui/mx;->a:Ljava/util/List;

    .line 75
    return-void
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/peel/ui/mx;->a:Ljava/util/List;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/peel/ui/mx;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    goto :goto_0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/peel/ui/mx;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2

    .prologue
    .line 70
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 10

    .prologue
    .line 79
    if-nez p2, :cond_3

    new-instance v0, Lcom/peel/ui/nh;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/peel/ui/nh;-><init>(Lcom/peel/ui/mx;Lcom/peel/ui/my;)V

    move-object v2, v0

    .line 81
    :goto_0
    if-nez p2, :cond_0

    .line 82
    iget-object v0, p0, Lcom/peel/ui/mx;->b:Landroid/view/LayoutInflater;

    sget v1, Lcom/peel/ui/fq;->tweet_list_item:I

    const/4 v3, 0x0

    invoke-virtual {v0, v1, p3, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 83
    sget v0, Lcom/peel/ui/fp;->contents:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v2, Lcom/peel/ui/nh;->a:Landroid/widget/TextView;

    .line 84
    sget v0, Lcom/peel/ui/fp;->userid:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v2, Lcom/peel/ui/nh;->c:Landroid/widget/TextView;

    .line 85
    sget v0, Lcom/peel/ui/fp;->username:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v2, Lcom/peel/ui/nh;->b:Landroid/widget/TextView;

    .line 86
    sget v0, Lcom/peel/ui/fp;->media_image:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, v2, Lcom/peel/ui/nh;->h:Landroid/widget/ImageView;

    .line 87
    sget v0, Lcom/peel/ui/fp;->user_image:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, v2, Lcom/peel/ui/nh;->i:Landroid/widget/ImageView;

    .line 88
    sget v0, Lcom/peel/ui/fp;->timestamp:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v2, Lcom/peel/ui/nh;->d:Landroid/widget/TextView;

    .line 89
    sget v0, Lcom/peel/ui/fp;->retweet_btn:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v2, Lcom/peel/ui/nh;->f:Landroid/widget/TextView;

    .line 90
    sget v0, Lcom/peel/ui/fp;->favorite_btn:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v2, Lcom/peel/ui/nh;->g:Landroid/widget/TextView;

    .line 91
    sget v0, Lcom/peel/ui/fp;->margin_view:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, v2, Lcom/peel/ui/nh;->j:Landroid/view/View;

    .line 92
    sget v0, Lcom/peel/ui/fp;->reply_btn:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v2, Lcom/peel/ui/nh;->e:Landroid/widget/TextView;

    .line 94
    invoke-virtual {p2, v2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 97
    :cond_0
    invoke-virtual {p0, p1}, Lcom/peel/ui/mx;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/ui/model/TwittData;

    .line 98
    iget-object v1, v2, Lcom/peel/ui/nh;->b:Landroid/widget/TextView;

    invoke-virtual {v0}, Lcom/peel/ui/model/TwittData;->e()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 100
    invoke-virtual {v0}, Lcom/peel/ui/model/TwittData;->d()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 101
    iget-object v1, p0, Lcom/peel/ui/mx;->c:Landroid/support/v4/app/ae;

    invoke-static {v1}, Lcom/peel/util/b/e;->a(Landroid/content/Context;)Lcom/squareup/picasso/Picasso;

    move-result-object v1

    invoke-virtual {v0}, Lcom/peel/ui/model/TwittData;->d()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/squareup/picasso/Picasso;->load(Ljava/lang/String;)Lcom/squareup/picasso/RequestCreator;

    move-result-object v1

    iget-object v3, v2, Lcom/peel/ui/nh;->i:Landroid/widget/ImageView;

    invoke-virtual {v1, v3}, Lcom/squareup/picasso/RequestCreator;->into(Landroid/widget/ImageView;)V

    .line 102
    :cond_1
    new-instance v3, Landroid/text/SpannableStringBuilder;

    invoke-virtual {v0}, Lcom/peel/ui/model/TwittData;->a()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v3, v1}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 104
    invoke-virtual {v0}, Lcom/peel/ui/model/TwittData;->h()[Lcom/peel/ui/model/TwittData$TwittEntries;

    move-result-object v4

    .line 105
    if-eqz v4, :cond_4

    array-length v1, v4

    if-lez v1, :cond_4

    .line 106
    array-length v5, v4

    const/4 v1, 0x0

    :goto_1
    if-ge v1, v5, :cond_4

    aget-object v6, v4, v1

    .line 107
    invoke-virtual {v3}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v7

    invoke-virtual {v6}, Lcom/peel/ui/model/TwittData$TwittEntries;->a()I

    move-result v8

    add-int/lit8 v8, v8, 0x1

    if-le v7, v8, :cond_2

    invoke-virtual {v3}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v7

    invoke-virtual {v6}, Lcom/peel/ui/model/TwittData$TwittEntries;->b()I

    move-result v8

    add-int/lit8 v8, v8, 0x1

    if-le v7, v8, :cond_2

    .line 108
    new-instance v7, Landroid/text/style/StyleSpan;

    const/4 v8, 0x1

    invoke-direct {v7, v8}, Landroid/text/style/StyleSpan;-><init>(I)V

    invoke-virtual {v6}, Lcom/peel/ui/model/TwittData$TwittEntries;->a()I

    move-result v8

    invoke-virtual {v6}, Lcom/peel/ui/model/TwittData$TwittEntries;->b()I

    move-result v6

    const/16 v9, 0x12

    invoke-virtual {v3, v7, v8, v6, v9}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 106
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 79
    :cond_3
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/ui/nh;

    move-object v2, v0

    goto/16 :goto_0

    .line 112
    :cond_4
    invoke-virtual {v0}, Lcom/peel/ui/model/TwittData;->i()[Lcom/peel/ui/model/TwittData$TwittEntries;

    move-result-object v4

    .line 113
    if-eqz v4, :cond_5

    array-length v1, v4

    if-lez v1, :cond_5

    .line 114
    array-length v5, v4

    const/4 v1, 0x0

    :goto_2
    if-ge v1, v5, :cond_5

    aget-object v6, v4, v1

    .line 115
    new-instance v7, Lcom/peel/util/URLSpanNodeUnderline;

    new-instance v8, Landroid/text/style/URLSpan;

    invoke-virtual {v6}, Lcom/peel/ui/model/TwittData$TwittEntries;->c()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Landroid/text/style/URLSpan;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8}, Landroid/text/style/URLSpan;->getURL()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Lcom/peel/util/URLSpanNodeUnderline;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6}, Lcom/peel/ui/model/TwittData$TwittEntries;->a()I

    move-result v8

    invoke-virtual {v6}, Lcom/peel/ui/model/TwittData$TwittEntries;->b()I

    move-result v6

    const/16 v9, 0x12

    invoke-virtual {v3, v7, v8, v6, v9}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 114
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 119
    :cond_5
    iget-object v1, v2, Lcom/peel/ui/nh;->a:Landroid/widget/TextView;

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 120
    iget-object v1, v2, Lcom/peel/ui/nh;->a:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 122
    invoke-virtual {v0}, Lcom/peel/ui/model/TwittData;->k()I

    move-result v1

    .line 125
    const/16 v3, 0x3c

    if-ge v1, v3, :cond_8

    .line 126
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v3, "s"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 135
    :goto_3
    iget-object v3, v2, Lcom/peel/ui/nh;->d:Landroid/widget/TextView;

    invoke-virtual {v3, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 136
    invoke-virtual {v0}, Lcom/peel/ui/model/TwittData;->j()[Lcom/peel/ui/model/TwittData$TwittEntries;

    move-result-object v1

    .line 137
    if-eqz v1, :cond_b

    array-length v3, v1

    if-lez v3, :cond_b

    .line 138
    iget-object v3, v2, Lcom/peel/ui/nh;->h:Landroid/widget/ImageView;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 139
    iget-object v3, p0, Lcom/peel/ui/mx;->c:Landroid/support/v4/app/ae;

    invoke-static {v3}, Lcom/peel/util/b/e;->a(Landroid/content/Context;)Lcom/squareup/picasso/Picasso;

    move-result-object v3

    const/4 v4, 0x0

    aget-object v1, v1, v4

    invoke-virtual {v1}, Lcom/peel/ui/model/TwittData$TwittEntries;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Lcom/squareup/picasso/Picasso;->load(Ljava/lang/String;)Lcom/squareup/picasso/RequestCreator;

    move-result-object v1

    iget-object v3, v2, Lcom/peel/ui/nh;->h:Landroid/widget/ImageView;

    invoke-virtual {v1, v3}, Lcom/squareup/picasso/RequestCreator;->into(Landroid/widget/ImageView;)V

    .line 140
    iget-object v1, v2, Lcom/peel/ui/nh;->j:Landroid/view/View;

    const/16 v3, 0x8

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 146
    :goto_4
    invoke-virtual {v0}, Lcom/peel/ui/model/TwittData;->b()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_7

    .line 147
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Lcom/peel/ui/model/TwittData;->b()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 149
    invoke-virtual {v0}, Lcom/peel/ui/model/TwittData;->b()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Ljava/lang/String;->charAt(I)C

    move-result v3

    const/16 v4, 0x40

    if-eq v3, v4, :cond_6

    .line 150
    const/4 v3, 0x0

    const-string/jumbo v4, "@"

    invoke-virtual {v1, v3, v4}, Ljava/lang/StringBuilder;->insert(ILjava/lang/String;)Ljava/lang/StringBuilder;

    .line 152
    :cond_6
    iget-object v3, v2, Lcom/peel/ui/nh;->c:Landroid/widget/TextView;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 155
    :cond_7
    invoke-virtual {v0}, Lcom/peel/ui/model/TwittData;->g()Z

    move-result v1

    if-eqz v1, :cond_c

    .line 156
    iget-object v1, v2, Lcom/peel/ui/nh;->g:Landroid/widget/TextView;

    sget v3, Lcom/peel/ui/fo;->favorite_icon_press:I

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual {v1, v3, v4, v5, v6}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    .line 161
    :goto_5
    invoke-virtual {v0}, Lcom/peel/ui/model/TwittData;->f()Z

    move-result v1

    if-eqz v1, :cond_d

    .line 162
    iget-object v1, v2, Lcom/peel/ui/nh;->f:Landroid/widget/TextView;

    sget v3, Lcom/peel/ui/fo;->retweet_icon_press:I

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual {v1, v3, v4, v5, v6}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    .line 167
    :goto_6
    iget-object v1, v2, Lcom/peel/ui/nh;->e:Landroid/widget/TextView;

    new-instance v3, Lcom/peel/ui/my;

    invoke-direct {v3, p0, v2}, Lcom/peel/ui/my;-><init>(Lcom/peel/ui/mx;Lcom/peel/ui/nh;)V

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 185
    iget-object v1, v2, Lcom/peel/ui/nh;->g:Landroid/widget/TextView;

    new-instance v3, Lcom/peel/ui/mz;

    invoke-direct {v3, p0, v0, v2}, Lcom/peel/ui/mz;-><init>(Lcom/peel/ui/mx;Lcom/peel/ui/model/TwittData;Lcom/peel/ui/nh;)V

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 219
    iget-object v1, v2, Lcom/peel/ui/nh;->f:Landroid/widget/TextView;

    new-instance v3, Lcom/peel/ui/nc;

    invoke-direct {v3, p0, v0, v2}, Lcom/peel/ui/nc;-><init>(Lcom/peel/ui/mx;Lcom/peel/ui/model/TwittData;Lcom/peel/ui/nh;)V

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 253
    return-object p2

    .line 127
    :cond_8
    const/16 v3, 0x3c

    if-lt v1, v3, :cond_9

    const/16 v3, 0xe10

    if-ge v1, v3, :cond_9

    .line 128
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    div-int/lit8 v1, v1, 0x3c

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v3, "m"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_3

    .line 129
    :cond_9
    const/16 v3, 0xe10

    if-lt v1, v3, :cond_a

    const v3, 0x15180

    if-ge v1, v3, :cond_a

    .line 130
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    div-int/lit16 v1, v1, 0xe10

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v3, "h"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_3

    .line 132
    :cond_a
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const v4, 0x15180

    div-int/2addr v1, v4

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v3, "d"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_3

    .line 142
    :cond_b
    iget-object v1, v2, Lcom/peel/ui/nh;->h:Landroid/widget/ImageView;

    const/16 v3, 0x8

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 143
    iget-object v1, v2, Lcom/peel/ui/nh;->j:Landroid/view/View;

    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_4

    .line 158
    :cond_c
    iget-object v1, v2, Lcom/peel/ui/nh;->g:Landroid/widget/TextView;

    sget v3, Lcom/peel/ui/fo;->favorite_icon_normal:I

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual {v1, v3, v4, v5, v6}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    goto/16 :goto_5

    .line 164
    :cond_d
    iget-object v1, v2, Lcom/peel/ui/nh;->f:Landroid/widget/TextView;

    sget v3, Lcom/peel/ui/fo;->retweet_icon_normal:I

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual {v1, v3, v4, v5, v6}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    goto/16 :goto_6
.end method
