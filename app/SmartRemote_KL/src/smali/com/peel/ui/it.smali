.class Lcom/peel/ui/it;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/peel/ui/gt;


# direct methods
.method constructor <init>(Lcom/peel/ui/gt;)V
    .locals 0

    .prologue
    .line 2737
    iput-object p1, p0, Lcom/peel/ui/it;->a:Lcom/peel/ui/gt;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 10

    .prologue
    const/16 v9, 0x13a6

    const/16 v8, 0x7e7

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 2740
    invoke-static {}, Lcom/peel/ui/gt;->S()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, " ******* onClick: view.getId(): "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " -- tag: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2742
    iget-object v0, p0, Lcom/peel/ui/it;->a:Lcom/peel/ui/gt;

    invoke-static {v0}, Lcom/peel/ui/gt;->J(Lcom/peel/ui/gt;)Lcom/peel/util/p;

    move-result-object v0

    invoke-static {v0}, Lcom/peel/util/i;->a(Ljava/lang/Runnable;)V

    .line 2743
    iget-object v0, p0, Lcom/peel/ui/it;->a:Lcom/peel/ui/gt;

    invoke-static {v0}, Lcom/peel/ui/gt;->J(Lcom/peel/ui/gt;)Lcom/peel/util/p;

    move-result-object v0

    const-wide/16 v2, 0x12c

    invoke-static {v0, v2, v3}, Lcom/peel/util/i;->a(Lcom/peel/util/p;J)Lcom/peel/util/p;

    .line 2744
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2745
    if-nez v0, :cond_1

    .line 2746
    invoke-static {}, Lcom/peel/ui/gt;->S()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "in onclick, command null, early return"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2949
    :cond_0
    :goto_0
    return-void

    .line 2750
    :cond_1
    invoke-static {}, Lcom/peel/ui/gt;->S()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "in onclick, command: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2752
    invoke-static {}, Lcom/peel/ui/gt;->T()Landroid/app/Dialog;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-static {}, Lcom/peel/ui/gt;->T()Landroid/app/Dialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Dialog;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_2

    const-string/jumbo v1, "Enter"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-static {}, Lcom/peel/ui/gt;->T()Landroid/app/Dialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Dialog;->dismiss()V

    .line 2753
    :cond_2
    const-string/jumbo v1, "|"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 2754
    invoke-virtual {v0, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 2755
    const-string/jumbo v1, "Play|Pause"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 2756
    iget-object v0, p0, Lcom/peel/ui/it;->a:Lcom/peel/ui/gt;

    const-string/jumbo v1, "Play"

    invoke-static {v0, v1}, Lcom/peel/ui/gt;->b(Lcom/peel/ui/gt;Ljava/lang/String;)V

    .line 2757
    const-string/jumbo v0, "|Pause|Play"

    invoke-virtual {p1, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    goto :goto_0

    .line 2758
    :cond_3
    const-string/jumbo v1, "Pause|Play"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 2760
    iget-object v0, p0, Lcom/peel/ui/it;->a:Lcom/peel/ui/gt;

    const-string/jumbo v1, "Pause"

    invoke-static {v0, v1}, Lcom/peel/ui/gt;->b(Lcom/peel/ui/gt;Ljava/lang/String;)V

    .line 2761
    const-string/jumbo v0, "|Play|Pause"

    invoke-virtual {p1, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    goto :goto_0

    .line 2762
    :cond_4
    const-string/jumbo v1, "Yellow|A"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 2763
    iget-object v0, p0, Lcom/peel/ui/it;->a:Lcom/peel/ui/gt;

    const-string/jumbo v1, "Yellow"

    invoke-static {v0, v1}, Lcom/peel/ui/gt;->b(Lcom/peel/ui/gt;Ljava/lang/String;)V

    .line 2764
    iget-object v0, p0, Lcom/peel/ui/it;->a:Lcom/peel/ui/gt;

    const-string/jumbo v1, "A"

    invoke-static {v0, v1}, Lcom/peel/ui/gt;->b(Lcom/peel/ui/gt;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 2765
    :cond_5
    const-string/jumbo v1, "Blue|B"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 2766
    iget-object v0, p0, Lcom/peel/ui/it;->a:Lcom/peel/ui/gt;

    const-string/jumbo v1, "Blue"

    invoke-static {v0, v1}, Lcom/peel/ui/gt;->b(Lcom/peel/ui/gt;Ljava/lang/String;)V

    .line 2767
    iget-object v0, p0, Lcom/peel/ui/it;->a:Lcom/peel/ui/gt;

    const-string/jumbo v1, "B"

    invoke-static {v0, v1}, Lcom/peel/ui/gt;->b(Lcom/peel/ui/gt;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 2768
    :cond_6
    const-string/jumbo v1, "Red|C"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 2769
    iget-object v0, p0, Lcom/peel/ui/it;->a:Lcom/peel/ui/gt;

    const-string/jumbo v1, "Red"

    invoke-static {v0, v1}, Lcom/peel/ui/gt;->b(Lcom/peel/ui/gt;Ljava/lang/String;)V

    .line 2770
    iget-object v0, p0, Lcom/peel/ui/it;->a:Lcom/peel/ui/gt;

    const-string/jumbo v1, "C"

    invoke-static {v0, v1}, Lcom/peel/ui/gt;->b(Lcom/peel/ui/gt;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 2771
    :cond_7
    const-string/jumbo v1, "Green|D"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 2772
    iget-object v0, p0, Lcom/peel/ui/it;->a:Lcom/peel/ui/gt;

    const-string/jumbo v1, "Green"

    invoke-static {v0, v1}, Lcom/peel/ui/gt;->b(Lcom/peel/ui/gt;Ljava/lang/String;)V

    .line 2773
    iget-object v0, p0, Lcom/peel/ui/it;->a:Lcom/peel/ui/gt;

    const-string/jumbo v1, "D"

    invoke-static {v0, v1}, Lcom/peel/ui/gt;->b(Lcom/peel/ui/gt;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 2775
    :cond_8
    iget-object v1, p0, Lcom/peel/ui/it;->a:Lcom/peel/ui/gt;

    invoke-static {v1, v0}, Lcom/peel/ui/gt;->b(Lcom/peel/ui/gt;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 2784
    :cond_9
    iget-object v1, p0, Lcom/peel/ui/it;->a:Lcom/peel/ui/gt;

    invoke-static {v1}, Lcom/peel/ui/gt;->K(Lcom/peel/ui/gt;)Lcom/peel/control/h;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/data/g;->d()I

    move-result v1

    const/16 v2, 0x12

    if-ne v1, v2, :cond_21

    .line 2785
    const-string/jumbo v1, "PowerOn|PowerOff"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_b

    .line 2786
    const-string/jumbo v0, "PowerOn"

    .line 2787
    const-string/jumbo v1, "PowerOff|PowerOn"

    invoke-virtual {p1, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 2882
    :cond_a
    :goto_1
    iget-object v1, p0, Lcom/peel/ui/it;->a:Lcom/peel/ui/gt;

    invoke-static {v1}, Lcom/peel/ui/gt;->N(Lcom/peel/ui/gt;)Z

    move-result v1

    if-eqz v1, :cond_20

    const-string/jumbo v1, "Power"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_20

    .line 2883
    iget-object v1, p0, Lcom/peel/ui/it;->a:Lcom/peel/ui/gt;

    iget-object v2, p0, Lcom/peel/ui/it;->a:Lcom/peel/ui/gt;

    invoke-static {v2}, Lcom/peel/ui/gt;->O(Lcom/peel/ui/gt;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v0, v2}, Lcom/peel/ui/gt;->a(Lcom/peel/ui/gt;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 2788
    :cond_b
    const-string/jumbo v1, "PowerOff|PowerOn"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_c

    .line 2789
    const-string/jumbo v0, "PowerOff"

    .line 2790
    const-string/jumbo v1, "PowerOn|PowerOff"

    invoke-virtual {p1, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 2792
    iget-object v1, p0, Lcom/peel/ui/it;->a:Lcom/peel/ui/gt;

    invoke-static {v1, v0}, Lcom/peel/ui/gt;->c(Lcom/peel/ui/gt;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 2794
    :cond_c
    const-string/jumbo v1, "UP"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_10

    .line 2795
    invoke-static {}, Lcom/peel/ui/gt;->U()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-nez v1, :cond_d

    .line 2796
    invoke-static {}, Lcom/peel/ui/gt;->S()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "No Temperature commands for AC "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/peel/ui/it;->a:Lcom/peel/ui/gt;

    invoke-static {v3}, Lcom/peel/ui/gt;->K(Lcom/peel/ui/gt;)Lcom/peel/control/h;

    move-result-object v3

    invoke-virtual {v3}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v3

    invoke-virtual {v3}, Lcom/peel/data/g;->f()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " -- code set: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/peel/ui/it;->a:Lcom/peel/ui/gt;

    .line 2797
    invoke-static {v3}, Lcom/peel/ui/gt;->K(Lcom/peel/ui/gt;)Lcom/peel/control/h;

    move-result-object v3

    invoke-virtual {v3}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v3

    invoke-virtual {v3}, Lcom/peel/data/g;->h()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 2796
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2814
    :goto_2
    iget-object v1, p0, Lcom/peel/ui/it;->a:Lcom/peel/ui/gt;

    const-string/jumbo v2, "T"

    invoke-static {v1, v2}, Lcom/peel/ui/gt;->d(Lcom/peel/ui/gt;Ljava/lang/String;)Ljava/lang/String;

    goto/16 :goto_1

    .line 2798
    :cond_d
    invoke-static {}, Lcom/peel/ui/gt;->U()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ne v0, v5, :cond_e

    .line 2799
    invoke-static {}, Lcom/peel/ui/gt;->U()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2800
    invoke-static {}, Lcom/peel/ui/gt;->S()Ljava/lang/String;

    move-result-object v2

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "sending the only 1 temperature command: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {}, Lcom/peel/ui/gt;->U()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 2802
    :cond_e
    invoke-static {}, Lcom/peel/ui/gt;->V()I

    .line 2803
    invoke-static {}, Lcom/peel/ui/gt;->ac()I

    move-result v0

    invoke-static {}, Lcom/peel/ui/gt;->U()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lt v0, v1, :cond_f

    invoke-static {}, Lcom/peel/ui/gt;->U()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-static {v0}, Lcom/peel/ui/gt;->b(I)I

    .line 2804
    :cond_f
    invoke-static {}, Lcom/peel/ui/gt;->U()Ljava/util/List;

    move-result-object v0

    invoke-static {}, Lcom/peel/ui/gt;->ac()I

    move-result v1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2806
    :try_start_0
    invoke-static {}, Lcom/peel/ui/gt;->U()Ljava/util/List;

    move-result-object v1

    invoke-static {}, Lcom/peel/ui/gt;->ac()I

    move-result v2

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    const-string/jumbo v2, "_"

    invoke-virtual {v1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 2807
    iget-object v2, p0, Lcom/peel/ui/it;->a:Lcom/peel/ui/gt;

    invoke-static {v2}, Lcom/peel/ui/gt;->L(Lcom/peel/ui/gt;)Landroid/widget/TextView;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v4, 0x1

    aget-object v1, v1, v4

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/16 v3, 0xb0

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v3, "C"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2808
    iget-object v1, p0, Lcom/peel/ui/it;->a:Lcom/peel/ui/gt;

    invoke-static {v1}, Lcom/peel/ui/gt;->E(Lcom/peel/ui/gt;)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v3}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v3

    invoke-virtual {v3}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v3

    invoke-virtual {v3}, Lcom/peel/data/at;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "_"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "ac_last_temp_idx"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {}, Lcom/peel/ui/gt;->ac()I

    move-result v3

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->apply()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2812
    :goto_3
    invoke-static {}, Lcom/peel/ui/gt;->S()Ljava/lang/String;

    move-result-object v2

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "sending the Temperature command (idx: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lcom/peel/ui/gt;->ac()I

    move-result v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v3, "): "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {}, Lcom/peel/ui/gt;->U()Ljava/util/List;

    move-result-object v1

    invoke-static {}, Lcom/peel/ui/gt;->ac()I

    move-result v4

    invoke-interface {v1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2

    .line 2809
    :catch_0
    move-exception v1

    .line 2810
    invoke-static {}, Lcom/peel/ui/gt;->S()Ljava/lang/String;

    move-result-object v2

    invoke-static {}, Lcom/peel/ui/gt;->S()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_3

    .line 2815
    :cond_10
    const-string/jumbo v1, "Down"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_14

    .line 2816
    invoke-static {}, Lcom/peel/ui/gt;->U()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-nez v1, :cond_11

    .line 2817
    invoke-static {}, Lcom/peel/ui/gt;->S()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "No Temperature commands for AC "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/peel/ui/it;->a:Lcom/peel/ui/gt;

    invoke-static {v3}, Lcom/peel/ui/gt;->K(Lcom/peel/ui/gt;)Lcom/peel/control/h;

    move-result-object v3

    invoke-virtual {v3}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v3

    invoke-virtual {v3}, Lcom/peel/data/g;->f()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " -- code set: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/peel/ui/it;->a:Lcom/peel/ui/gt;

    invoke-static {v3}, Lcom/peel/ui/gt;->K(Lcom/peel/ui/gt;)Lcom/peel/control/h;

    move-result-object v3

    invoke-virtual {v3}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v3

    invoke-virtual {v3}, Lcom/peel/data/g;->h()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2834
    :goto_4
    iget-object v1, p0, Lcom/peel/ui/it;->a:Lcom/peel/ui/gt;

    const-string/jumbo v2, "T"

    invoke-static {v1, v2}, Lcom/peel/ui/gt;->d(Lcom/peel/ui/gt;Ljava/lang/String;)Ljava/lang/String;

    goto/16 :goto_1

    .line 2818
    :cond_11
    invoke-static {}, Lcom/peel/ui/gt;->U()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ne v0, v5, :cond_12

    .line 2819
    invoke-static {}, Lcom/peel/ui/gt;->U()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2820
    invoke-static {}, Lcom/peel/ui/gt;->S()Ljava/lang/String;

    move-result-object v2

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "sending the only 1 temperature command: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {}, Lcom/peel/ui/gt;->U()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_4

    .line 2822
    :cond_12
    invoke-static {}, Lcom/peel/ui/gt;->ad()I

    .line 2823
    invoke-static {}, Lcom/peel/ui/gt;->ac()I

    move-result v0

    if-gez v0, :cond_13

    invoke-static {v4}, Lcom/peel/ui/gt;->b(I)I

    .line 2824
    :cond_13
    invoke-static {}, Lcom/peel/ui/gt;->U()Ljava/util/List;

    move-result-object v0

    invoke-static {}, Lcom/peel/ui/gt;->ac()I

    move-result v1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2826
    :try_start_1
    invoke-static {}, Lcom/peel/ui/gt;->U()Ljava/util/List;

    move-result-object v1

    invoke-static {}, Lcom/peel/ui/gt;->ac()I

    move-result v2

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    const-string/jumbo v2, "_"

    invoke-virtual {v1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 2827
    iget-object v2, p0, Lcom/peel/ui/it;->a:Lcom/peel/ui/gt;

    invoke-static {v2}, Lcom/peel/ui/gt;->L(Lcom/peel/ui/gt;)Landroid/widget/TextView;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v4, 0x1

    aget-object v1, v1, v4

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/16 v3, 0xb0

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v3, "C"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2828
    iget-object v1, p0, Lcom/peel/ui/it;->a:Lcom/peel/ui/gt;

    invoke-static {v1}, Lcom/peel/ui/gt;->E(Lcom/peel/ui/gt;)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v3}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v3

    invoke-virtual {v3}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v3

    invoke-virtual {v3}, Lcom/peel/data/at;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "_"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "ac_last_temp_idx"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {}, Lcom/peel/ui/gt;->ac()I

    move-result v3

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->apply()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 2832
    :goto_5
    invoke-static {}, Lcom/peel/ui/gt;->S()Ljava/lang/String;

    move-result-object v2

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "sending the Temperature command (idx: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lcom/peel/ui/gt;->ac()I

    move-result v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v3, "): "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {}, Lcom/peel/ui/gt;->U()Ljava/util/List;

    move-result-object v1

    invoke-static {}, Lcom/peel/ui/gt;->ac()I

    move-result v4

    invoke-interface {v1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_4

    .line 2829
    :catch_1
    move-exception v1

    .line 2830
    invoke-static {}, Lcom/peel/ui/gt;->S()Ljava/lang/String;

    move-result-object v2

    invoke-static {}, Lcom/peel/ui/gt;->S()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_5

    .line 2835
    :cond_14
    const-string/jumbo v1, "FAN_HIGH"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_18

    .line 2836
    invoke-static {}, Lcom/peel/ui/gt;->ae()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-nez v1, :cond_15

    .line 2837
    invoke-static {}, Lcom/peel/ui/gt;->S()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "No Fan Speed commands for AC: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/peel/ui/it;->a:Lcom/peel/ui/gt;

    invoke-static {v3}, Lcom/peel/ui/gt;->K(Lcom/peel/ui/gt;)Lcom/peel/control/h;

    move-result-object v3

    invoke-virtual {v3}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v3

    invoke-virtual {v3}, Lcom/peel/data/g;->f()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " -- code set: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/peel/ui/it;->a:Lcom/peel/ui/gt;

    invoke-static {v3}, Lcom/peel/ui/gt;->K(Lcom/peel/ui/gt;)Lcom/peel/control/h;

    move-result-object v3

    invoke-virtual {v3}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v3

    invoke-virtual {v3}, Lcom/peel/data/g;->h()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2848
    :goto_6
    iget-object v1, p0, Lcom/peel/ui/it;->a:Lcom/peel/ui/gt;

    const-string/jumbo v2, "F"

    invoke-static {v1, v2}, Lcom/peel/ui/gt;->d(Lcom/peel/ui/gt;Ljava/lang/String;)Ljava/lang/String;

    goto/16 :goto_1

    .line 2838
    :cond_15
    invoke-static {}, Lcom/peel/ui/gt;->ae()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ne v0, v5, :cond_16

    .line 2839
    invoke-static {}, Lcom/peel/ui/gt;->ae()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2840
    invoke-static {}, Lcom/peel/ui/gt;->S()Ljava/lang/String;

    move-result-object v2

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "sending the only 1 Fan command: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {}, Lcom/peel/ui/gt;->ae()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_6

    .line 2842
    :cond_16
    invoke-static {}, Lcom/peel/ui/gt;->af()I

    .line 2843
    invoke-static {}, Lcom/peel/ui/gt;->ag()I

    move-result v0

    invoke-static {}, Lcom/peel/ui/gt;->ae()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lt v0, v1, :cond_17

    invoke-static {}, Lcom/peel/ui/gt;->ae()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-static {v0}, Lcom/peel/ui/gt;->c(I)I

    .line 2844
    :cond_17
    invoke-static {}, Lcom/peel/ui/gt;->ae()Ljava/util/List;

    move-result-object v0

    invoke-static {}, Lcom/peel/ui/gt;->ag()I

    move-result v1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2845
    iget-object v1, p0, Lcom/peel/ui/it;->a:Lcom/peel/ui/gt;

    invoke-static {v1}, Lcom/peel/ui/gt;->E(Lcom/peel/ui/gt;)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v3}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v3

    invoke-virtual {v3}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v3

    invoke-virtual {v3}, Lcom/peel/data/at;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "_"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "ac_last_fanspeed_idx"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {}, Lcom/peel/ui/gt;->ag()I

    move-result v3

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 2846
    invoke-static {}, Lcom/peel/ui/gt;->S()Ljava/lang/String;

    move-result-object v2

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "sending the Fan command (idx: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lcom/peel/ui/gt;->ag()I

    move-result v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v3, "): "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {}, Lcom/peel/ui/gt;->ae()Ljava/util/List;

    move-result-object v1

    invoke-static {}, Lcom/peel/ui/gt;->ag()I

    move-result v4

    invoke-interface {v1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_6

    .line 2849
    :cond_18
    const-string/jumbo v1, "FAN_LOW"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1c

    .line 2850
    invoke-static {}, Lcom/peel/ui/gt;->ae()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-nez v1, :cond_19

    .line 2851
    invoke-static {}, Lcom/peel/ui/gt;->S()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "No Fan Speed commands for AC: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/peel/ui/it;->a:Lcom/peel/ui/gt;

    invoke-static {v3}, Lcom/peel/ui/gt;->K(Lcom/peel/ui/gt;)Lcom/peel/control/h;

    move-result-object v3

    invoke-virtual {v3}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v3

    invoke-virtual {v3}, Lcom/peel/data/g;->f()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " -- code set: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/peel/ui/it;->a:Lcom/peel/ui/gt;

    invoke-static {v3}, Lcom/peel/ui/gt;->K(Lcom/peel/ui/gt;)Lcom/peel/control/h;

    move-result-object v3

    invoke-virtual {v3}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v3

    invoke-virtual {v3}, Lcom/peel/data/g;->h()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2862
    :goto_7
    iget-object v1, p0, Lcom/peel/ui/it;->a:Lcom/peel/ui/gt;

    const-string/jumbo v2, "F"

    invoke-static {v1, v2}, Lcom/peel/ui/gt;->d(Lcom/peel/ui/gt;Ljava/lang/String;)Ljava/lang/String;

    goto/16 :goto_1

    .line 2852
    :cond_19
    invoke-static {}, Lcom/peel/ui/gt;->ae()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ne v0, v5, :cond_1a

    .line 2853
    invoke-static {}, Lcom/peel/ui/gt;->ae()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2854
    invoke-static {}, Lcom/peel/ui/gt;->S()Ljava/lang/String;

    move-result-object v2

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "sending the only 1 Fan command: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {}, Lcom/peel/ui/gt;->ae()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_7

    .line 2856
    :cond_1a
    invoke-static {}, Lcom/peel/ui/gt;->ah()I

    .line 2857
    invoke-static {}, Lcom/peel/ui/gt;->ag()I

    move-result v0

    if-gez v0, :cond_1b

    invoke-static {v4}, Lcom/peel/ui/gt;->c(I)I

    .line 2858
    :cond_1b
    invoke-static {}, Lcom/peel/ui/gt;->ae()Ljava/util/List;

    move-result-object v0

    invoke-static {}, Lcom/peel/ui/gt;->ag()I

    move-result v1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2859
    iget-object v1, p0, Lcom/peel/ui/it;->a:Lcom/peel/ui/gt;

    invoke-static {v1}, Lcom/peel/ui/gt;->E(Lcom/peel/ui/gt;)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v3}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v3

    invoke-virtual {v3}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v3

    invoke-virtual {v3}, Lcom/peel/data/at;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "_"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "ac_last_fanspeed_idx"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {}, Lcom/peel/ui/gt;->ag()I

    move-result v3

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 2860
    invoke-static {}, Lcom/peel/ui/gt;->S()Ljava/lang/String;

    move-result-object v2

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "sending the Fan command (idx: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lcom/peel/ui/gt;->ag()I

    move-result v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v3, "): "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {}, Lcom/peel/ui/gt;->ae()Ljava/util/List;

    move-result-object v1

    invoke-static {}, Lcom/peel/ui/gt;->ag()I

    move-result v4

    invoke-interface {v1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_7

    .line 2863
    :cond_1c
    const-string/jumbo v1, "MODE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 2864
    invoke-static {}, Lcom/peel/ui/gt;->ai()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-nez v1, :cond_1e

    .line 2865
    invoke-static {}, Lcom/peel/ui/gt;->S()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "No Mode commands for AC: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/peel/ui/it;->a:Lcom/peel/ui/gt;

    invoke-static {v3}, Lcom/peel/ui/gt;->K(Lcom/peel/ui/gt;)Lcom/peel/control/h;

    move-result-object v3

    invoke-virtual {v3}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v3

    invoke-virtual {v3}, Lcom/peel/data/g;->f()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " -- code set: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/peel/ui/it;->a:Lcom/peel/ui/gt;

    invoke-static {v3}, Lcom/peel/ui/gt;->K(Lcom/peel/ui/gt;)Lcom/peel/control/h;

    move-result-object v3

    invoke-virtual {v3}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v3

    invoke-virtual {v3}, Lcom/peel/data/g;->h()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2876
    :goto_8
    iget-object v1, p0, Lcom/peel/ui/it;->a:Lcom/peel/ui/gt;

    invoke-static {v1}, Lcom/peel/ui/gt;->M(Lcom/peel/ui/gt;)Landroid/widget/TextView;

    move-result-object v1

    if-eqz v1, :cond_1d

    .line 2877
    iget-object v1, p0, Lcom/peel/ui/it;->a:Lcom/peel/ui/gt;

    invoke-static {v1}, Lcom/peel/ui/gt;->M(Lcom/peel/ui/gt;)Landroid/widget/TextView;

    move-result-object v1

    const/16 v2, 0x5f

    invoke-virtual {v0, v2}, Ljava/lang/String;->indexOf(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v0, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2879
    :cond_1d
    iget-object v1, p0, Lcom/peel/ui/it;->a:Lcom/peel/ui/gt;

    const-string/jumbo v2, "M"

    invoke-static {v1, v2}, Lcom/peel/ui/gt;->d(Lcom/peel/ui/gt;Ljava/lang/String;)Ljava/lang/String;

    goto/16 :goto_1

    .line 2866
    :cond_1e
    invoke-static {}, Lcom/peel/ui/gt;->ai()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ne v0, v5, :cond_1f

    .line 2867
    invoke-static {}, Lcom/peel/ui/gt;->ai()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2868
    invoke-static {}, Lcom/peel/ui/gt;->S()Ljava/lang/String;

    move-result-object v2

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "sending the only 1 direction command: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {}, Lcom/peel/ui/gt;->ai()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_8

    .line 2870
    :cond_1f
    invoke-static {}, Lcom/peel/ui/gt;->aj()I

    .line 2871
    invoke-static {}, Lcom/peel/ui/gt;->ak()I

    move-result v0

    invoke-static {}, Lcom/peel/ui/gt;->ai()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    rem-int/2addr v0, v1

    invoke-static {v0}, Lcom/peel/ui/gt;->d(I)I

    .line 2872
    invoke-static {}, Lcom/peel/ui/gt;->ai()Ljava/util/List;

    move-result-object v0

    invoke-static {}, Lcom/peel/ui/gt;->ak()I

    move-result v1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2873
    invoke-static {}, Lcom/peel/ui/gt;->S()Ljava/lang/String;

    move-result-object v2

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "sending the direction command (idx: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lcom/peel/ui/gt;->ak()I

    move-result v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v3, "): "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {}, Lcom/peel/ui/gt;->ai()Ljava/util/List;

    move-result-object v1

    invoke-static {}, Lcom/peel/ui/gt;->ak()I

    move-result v4

    invoke-interface {v1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2874
    iget-object v1, p0, Lcom/peel/ui/it;->a:Lcom/peel/ui/gt;

    invoke-static {v1}, Lcom/peel/ui/gt;->E(Lcom/peel/ui/gt;)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v3}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v3

    invoke-virtual {v3}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v3

    invoke-virtual {v3}, Lcom/peel/data/at;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "_"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "ac_last_mode_idx"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {}, Lcom/peel/ui/gt;->ak()I

    move-result v3

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->apply()V

    goto/16 :goto_8

    .line 2885
    :cond_20
    iget-object v1, p0, Lcom/peel/ui/it;->a:Lcom/peel/ui/gt;

    invoke-static {v1}, Lcom/peel/ui/gt;->P(Lcom/peel/ui/gt;)Z

    move-result v1

    if-eqz v1, :cond_22

    const-string/jumbo v1, "Power"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_22

    .line 2886
    iget-object v1, p0, Lcom/peel/ui/it;->a:Lcom/peel/ui/gt;

    iget-object v2, p0, Lcom/peel/ui/it;->a:Lcom/peel/ui/gt;

    invoke-static {v2}, Lcom/peel/ui/gt;->O(Lcom/peel/ui/gt;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v0, v2}, Lcom/peel/ui/gt;->b(Lcom/peel/ui/gt;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 2889
    :cond_21
    const-string/jumbo v1, "Play|Pause"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_23

    .line 2890
    const-string/jumbo v0, "Play"

    .line 2891
    const-string/jumbo v1, "Pause|Play"

    invoke-virtual {p1, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 2898
    :cond_22
    :goto_9
    const-string/jumbo v1, "Power"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_24

    iget-object v1, p0, Lcom/peel/ui/it;->a:Lcom/peel/ui/gt;

    invoke-static {v1}, Lcom/peel/ui/gt;->H(Lcom/peel/ui/gt;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-le v1, v5, :cond_24

    .line 2899
    iget-object v0, p0, Lcom/peel/ui/it;->a:Lcom/peel/ui/gt;

    invoke-static {v0}, Lcom/peel/ui/gt;->Q(Lcom/peel/ui/gt;)V

    goto/16 :goto_0

    .line 2892
    :cond_23
    const-string/jumbo v1, "Pause|Play"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_22

    .line 2893
    const-string/jumbo v0, "Pause"

    .line 2894
    const-string/jumbo v1, "Play|Pause"

    invoke-virtual {p1, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    goto :goto_9

    .line 2902
    :cond_24
    invoke-static {}, Lcom/peel/ui/gt;->al()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_28

    .line 2903
    invoke-static {}, Lcom/peel/ui/gt;->S()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "\n\n######## NUMBER CMD: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2904
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 2905
    iget-object v1, p0, Lcom/peel/ui/it;->a:Lcom/peel/ui/gt;

    invoke-static {v1}, Lcom/peel/ui/gt;->R(Lcom/peel/ui/gt;)J

    move-result-wide v4

    sub-long v4, v2, v4

    const-wide/16 v6, 0x7d0

    cmp-long v1, v4, v6

    if-gez v1, :cond_26

    .line 2907
    invoke-static {}, Lcom/peel/ui/gt;->S()Ljava/lang/String;

    move-result-object v1

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "**** append CMD"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2908
    iget-object v1, p0, Lcom/peel/ui/it;->a:Lcom/peel/ui/gt;

    invoke-static {v1}, Lcom/peel/ui/gt;->S(Lcom/peel/ui/gt;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2920
    :goto_a
    iget-object v1, p0, Lcom/peel/ui/it;->a:Lcom/peel/ui/gt;

    invoke-static {v1, v2, v3}, Lcom/peel/ui/gt;->a(Lcom/peel/ui/gt;J)J

    .line 2946
    :cond_25
    :goto_b
    iget-object v1, p0, Lcom/peel/ui/it;->a:Lcom/peel/ui/gt;

    invoke-static {v1, v0}, Lcom/peel/ui/gt;->c(Lcom/peel/ui/gt;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 2910
    :cond_26
    iget-object v1, p0, Lcom/peel/ui/it;->a:Lcom/peel/ui/gt;

    invoke-static {v1}, Lcom/peel/ui/gt;->S(Lcom/peel/ui/gt;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    if-lez v1, :cond_27

    .line 2911
    invoke-static {}, Lcom/peel/ui/gt;->S()Ljava/lang/String;

    move-result-object v1

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "******** flush CMD"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/peel/ui/it;->a:Lcom/peel/ui/gt;

    invoke-static {v5}, Lcom/peel/ui/gt;->S(Lcom/peel/ui/gt;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2912
    invoke-static {}, Lcom/peel/util/a/f;->a()Lcom/peel/util/a/f;

    move-result-object v1

    sget-object v4, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v4}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v4

    invoke-virtual {v4}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v4

    invoke-virtual {v4}, Lcom/peel/data/at;->f()I

    move-result v4

    iget-object v5, p0, Lcom/peel/ui/it;->a:Lcom/peel/ui/gt;

    .line 2914
    invoke-static {v5}, Lcom/peel/ui/gt;->S(Lcom/peel/ui/gt;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 2912
    invoke-virtual {v1, v4, v9, v8, v5}, Lcom/peel/util/a/f;->a(IIILjava/lang/String;)V

    .line 2915
    iget-object v1, p0, Lcom/peel/ui/it;->a:Lcom/peel/ui/gt;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v1, v4}, Lcom/peel/ui/gt;->a(Lcom/peel/ui/gt;Ljava/lang/StringBuilder;)Ljava/lang/StringBuilder;

    .line 2917
    :cond_27
    invoke-static {}, Lcom/peel/ui/gt;->S()Ljava/lang/String;

    move-result-object v1

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "**** append CMD"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2918
    iget-object v1, p0, Lcom/peel/ui/it;->a:Lcom/peel/ui/gt;

    invoke-static {v1}, Lcom/peel/ui/gt;->S(Lcom/peel/ui/gt;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_a

    .line 2921
    :cond_28
    const-string/jumbo v1, "Enter"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2a

    .line 2922
    invoke-static {}, Lcom/peel/ui/gt;->S()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "\n\n######## ENTER CMD: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2923
    iget-object v1, p0, Lcom/peel/ui/it;->a:Lcom/peel/ui/gt;

    invoke-static {v1}, Lcom/peel/ui/gt;->S(Lcom/peel/ui/gt;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    if-lez v1, :cond_29

    .line 2925
    invoke-static {}, Lcom/peel/ui/gt;->S()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "******** flush CMD after ENTER"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/peel/ui/it;->a:Lcom/peel/ui/gt;

    invoke-static {v3}, Lcom/peel/ui/gt;->S(Lcom/peel/ui/gt;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2926
    invoke-static {}, Lcom/peel/util/a/f;->a()Lcom/peel/util/a/f;

    move-result-object v1

    sget-object v2, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v2}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v2

    invoke-virtual {v2}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v2

    invoke-virtual {v2}, Lcom/peel/data/at;->f()I

    move-result v2

    iget-object v3, p0, Lcom/peel/ui/it;->a:Lcom/peel/ui/gt;

    .line 2929
    invoke-static {v3}, Lcom/peel/ui/gt;->S(Lcom/peel/ui/gt;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 2926
    invoke-virtual {v1, v2, v9, v8, v3}, Lcom/peel/util/a/f;->a(IIILjava/lang/String;)V

    .line 2930
    iget-object v1, p0, Lcom/peel/ui/it;->a:Lcom/peel/ui/gt;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v1, v2}, Lcom/peel/ui/gt;->a(Lcom/peel/ui/gt;Ljava/lang/StringBuilder;)Ljava/lang/StringBuilder;

    .line 2932
    :cond_29
    iget-object v1, p0, Lcom/peel/ui/it;->a:Lcom/peel/ui/gt;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/peel/ui/gt;->a(Lcom/peel/ui/gt;J)J

    goto/16 :goto_b

    .line 2933
    :cond_2a
    const-string/jumbo v1, "Channel_Down"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2b

    const-string/jumbo v1, "Channel_Up"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_25

    .line 2934
    :cond_2b
    iget-object v1, p0, Lcom/peel/ui/it;->a:Lcom/peel/ui/gt;

    invoke-static {v1}, Lcom/peel/ui/gt;->T(Lcom/peel/ui/gt;)I

    move-result v1

    if-eq v1, v5, :cond_2c

    iget-object v1, p0, Lcom/peel/ui/it;->a:Lcom/peel/ui/gt;

    invoke-static {v1}, Lcom/peel/ui/gt;->T(Lcom/peel/ui/gt;)I

    move-result v1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_2d

    .line 2935
    :cond_2c
    iget-object v1, p0, Lcom/peel/ui/it;->a:Lcom/peel/ui/gt;

    invoke-static {v1}, Lcom/peel/ui/gt;->o(Lcom/peel/ui/gt;)Lcom/peel/widget/SlidingDrawer;

    move-result-object v1

    if-eqz v1, :cond_2d

    iget-object v1, p0, Lcom/peel/ui/it;->a:Lcom/peel/ui/gt;

    invoke-static {v1}, Lcom/peel/ui/gt;->o(Lcom/peel/ui/gt;)Lcom/peel/widget/SlidingDrawer;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/widget/SlidingDrawer;->f()Z

    move-result v1

    if-nez v1, :cond_2d

    iget-object v1, p0, Lcom/peel/ui/it;->a:Lcom/peel/ui/gt;

    invoke-static {v1}, Lcom/peel/ui/gt;->o(Lcom/peel/ui/gt;)Lcom/peel/widget/SlidingDrawer;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/widget/SlidingDrawer;->e()Z

    move-result v1

    if-nez v1, :cond_2d

    .line 2936
    iget-object v1, p0, Lcom/peel/ui/it;->a:Lcom/peel/ui/gt;

    invoke-static {v1, v5}, Lcom/peel/ui/gt;->b(Lcom/peel/ui/gt;Z)Z

    .line 2937
    iget-object v1, p0, Lcom/peel/ui/it;->a:Lcom/peel/ui/gt;

    invoke-static {v1}, Lcom/peel/ui/gt;->o(Lcom/peel/ui/gt;)Lcom/peel/widget/SlidingDrawer;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/widget/SlidingDrawer;->d()V

    .line 2941
    :cond_2d
    iget-object v1, p0, Lcom/peel/ui/it;->a:Lcom/peel/ui/gt;

    invoke-static {v1}, Lcom/peel/ui/gt;->T(Lcom/peel/ui/gt;)I

    move-result v1

    if-eqz v1, :cond_2e

    iget-object v1, p0, Lcom/peel/ui/it;->a:Lcom/peel/ui/gt;

    invoke-static {v1}, Lcom/peel/ui/gt;->T(Lcom/peel/ui/gt;)I

    move-result v1

    if-ne v1, v5, :cond_0

    .line 2942
    :cond_2e
    iget-object v1, p0, Lcom/peel/ui/it;->a:Lcom/peel/ui/gt;

    invoke-static {v1, v0}, Lcom/peel/ui/gt;->c(Lcom/peel/ui/gt;Ljava/lang/String;)V

    goto/16 :goto_0
.end method
