.class Lcom/peel/ui/b/y;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Landroid/widget/TextView;

.field final synthetic b:Lcom/peel/ui/b/r;


# direct methods
.method constructor <init>(Lcom/peel/ui/b/r;Landroid/widget/TextView;)V
    .locals 0

    .prologue
    .line 226
    iput-object p1, p0, Lcom/peel/ui/b/y;->b:Lcom/peel/ui/b/r;

    iput-object p2, p0, Lcom/peel/ui/b/y;->a:Landroid/widget/TextView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 229
    iget-object v0, p0, Lcom/peel/ui/b/y;->b:Lcom/peel/ui/b/r;

    invoke-static {v0}, Lcom/peel/ui/b/r;->a(Lcom/peel/ui/b/r;)Lcom/peel/d/v;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/d/v;->b()Lcom/peel/d/af;

    move-result-object v0

    if-nez v0, :cond_0

    .line 230
    iget-object v0, p0, Lcom/peel/ui/b/y;->b:Lcom/peel/ui/b/r;

    invoke-static {v0}, Lcom/peel/ui/b/r;->b(Lcom/peel/ui/b/r;)V

    .line 239
    :goto_0
    return-void

    .line 232
    :cond_0
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 233
    const-string/jumbo v1, "pre_text"

    iget-object v2, p0, Lcom/peel/ui/b/y;->a:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 234
    const-string/jumbo v1, "keyword"

    iget-object v2, p0, Lcom/peel/ui/b/y;->b:Lcom/peel/ui/b/r;

    invoke-static {v2}, Lcom/peel/ui/b/r;->d(Lcom/peel/ui/b/r;)[Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putStringArray(Ljava/lang/String;[Ljava/lang/String;)V

    .line 235
    const-string/jumbo v1, "context_id"

    iget-object v2, p0, Lcom/peel/ui/b/y;->b:Lcom/peel/ui/b/r;

    invoke-static {v2}, Lcom/peel/ui/b/r;->c(Lcom/peel/ui/b/r;)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 236
    iget-object v1, p0, Lcom/peel/ui/b/y;->b:Lcom/peel/ui/b/r;

    invoke-static {v1}, Lcom/peel/ui/b/r;->e(Lcom/peel/ui/b/r;)Landroid/support/v4/app/ae;

    move-result-object v1

    const-class v2, Lcom/peel/ui/mo;

    .line 237
    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    .line 236
    invoke-static {v1, v2, v0}, Lcom/peel/d/e;->c(Landroid/support/v4/app/ae;Ljava/lang/String;Landroid/os/Bundle;)V

    goto :goto_0
.end method
