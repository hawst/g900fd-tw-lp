.class public Lcom/peel/ui/b/l;
.super Landroid/widget/BaseAdapter;


# instance fields
.field private a:Landroid/content/Context;

.field private b:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/peel/content/model/OVD;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/ArrayList;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/peel/content/model/OVD;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 28
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 18
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/peel/ui/b/l;->b:Ljava/util/ArrayList;

    .line 29
    iput-object p1, p0, Lcom/peel/ui/b/l;->a:Landroid/content/Context;

    .line 30
    if-eqz p2, :cond_0

    .line 31
    iget-object v0, p0, Lcom/peel/ui/b/l;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 32
    :cond_0
    return-void
.end method


# virtual methods
.method public a(Ljava/util/ArrayList;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/peel/content/model/OVD;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 78
    iget-object v0, p0, Lcom/peel/ui/b/l;->b:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    .line 79
    iget-object v0, p0, Lcom/peel/ui/b/l;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 80
    if-eqz p1, :cond_0

    .line 81
    iget-object v0, p0, Lcom/peel/ui/b/l;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 82
    :cond_0
    invoke-virtual {p0}, Lcom/peel/ui/b/l;->notifyDataSetChanged()V

    .line 84
    :cond_1
    return-void
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/peel/ui/b/l;->b:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/peel/ui/b/l;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    goto :goto_0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/peel/ui/b/l;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2

    .prologue
    .line 74
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 36
    iget-object v0, p0, Lcom/peel/ui/b/l;->a:Landroid/content/Context;

    const-string/jumbo v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 37
    if-nez p2, :cond_1

    new-instance v1, Lcom/peel/ui/b/n;

    invoke-direct {v1, v3}, Lcom/peel/ui/b/n;-><init>(Lcom/peel/ui/b/m;)V

    .line 39
    :goto_0
    if-nez p2, :cond_0

    .line 40
    sget v2, Lcom/peel/ui/fq;->ondemandvideo_layout:I

    invoke-virtual {v0, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 41
    sget v0, Lcom/peel/ui/fp;->ondemand_video_host_icon:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, v1, Lcom/peel/ui/b/n;->a:Landroid/widget/ImageView;

    .line 42
    invoke-virtual {p2, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 45
    :cond_0
    iget-object v0, p0, Lcom/peel/ui/b/l;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/content/model/OVD;

    invoke-virtual {v0}, Lcom/peel/content/model/OVD;->a()Ljava/lang/String;

    move-result-object v2

    .line 46
    iget-object v0, p0, Lcom/peel/ui/b/l;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/content/model/OVD;

    invoke-virtual {v0}, Lcom/peel/content/model/OVD;->c()Ljava/lang/String;

    move-result-object v0

    .line 47
    const-string/jumbo v3, "amazon.com"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 48
    iget-object v1, v1, Lcom/peel/ui/b/n;->a:Landroid/widget/ImageView;

    const-string/jumbo v2, "hd"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    sget v0, Lcom/peel/ui/fo;->ondemand_amazon_logo_hd:I

    :goto_1
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 59
    :goto_2
    return-object p2

    .line 37
    :cond_1
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/peel/ui/b/n;

    goto :goto_0

    .line 48
    :cond_2
    sget v0, Lcom/peel/ui/fo;->ondemand_amazon_logo:I

    goto :goto_1

    .line 49
    :cond_3
    const-string/jumbo v3, "hulu"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 50
    iget-object v1, v1, Lcom/peel/ui/b/n;->a:Landroid/widget/ImageView;

    const-string/jumbo v2, "hd"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    sget v0, Lcom/peel/ui/fo;->ondemand_hulu_logo_hd:I

    :goto_3
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_2

    :cond_4
    sget v0, Lcom/peel/ui/fo;->ondemand_hulu_logo:I

    goto :goto_3

    .line 51
    :cond_5
    const-string/jumbo v3, "youtube"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 52
    iget-object v0, v1, Lcom/peel/ui/b/n;->a:Landroid/widget/ImageView;

    sget v1, Lcom/peel/ui/fo;->ondemand_youtube_logo:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_2

    .line 53
    :cond_6
    const-string/jumbo v3, "netflix"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 54
    iget-object v1, v1, Lcom/peel/ui/b/n;->a:Landroid/widget/ImageView;

    const-string/jumbo v2, "hd"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    sget v0, Lcom/peel/ui/fo;->ondemand_netflix_logo_hd:I

    :goto_4
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_2

    :cond_7
    sget v0, Lcom/peel/ui/fo;->ondemand_netflix_logo:I

    goto :goto_4

    .line 57
    :cond_8
    iget-object v0, v1, Lcom/peel/ui/b/n;->a:Landroid/widget/ImageView;

    sget v1, Lcom/peel/ui/fo;->ondemand_youtube_logo:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_2
.end method
