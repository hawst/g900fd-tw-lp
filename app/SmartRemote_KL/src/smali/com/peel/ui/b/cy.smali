.class Lcom/peel/ui/b/cy;
.super Landroid/widget/BaseAdapter;


# instance fields
.field final synthetic a:Lcom/peel/ui/b/cv;


# direct methods
.method private constructor <init>(Lcom/peel/ui/b/cv;)V
    .locals 0

    .prologue
    .line 75
    iput-object p1, p0, Lcom/peel/ui/b/cy;->a:Lcom/peel/ui/b/cv;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 164
    return-void
.end method

.method synthetic constructor <init>(Lcom/peel/ui/b/cv;Lcom/peel/ui/b/cw;)V
    .locals 0

    .prologue
    .line 75
    invoke-direct {p0, p1}, Lcom/peel/ui/b/cy;-><init>(Lcom/peel/ui/b/cv;)V

    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lcom/peel/ui/b/cy;->a:Lcom/peel/ui/b/cv;

    invoke-static {v0}, Lcom/peel/ui/b/cv;->b(Lcom/peel/ui/b/cv;)Ljava/util/List;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/peel/ui/b/cy;->a:Lcom/peel/ui/b/cv;

    invoke-static {v0}, Lcom/peel/ui/b/cv;->b(Lcom/peel/ui/b/cv;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    goto :goto_0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lcom/peel/ui/b/cy;->a:Lcom/peel/ui/b/cv;

    invoke-static {v0}, Lcom/peel/ui/b/cv;->b(Lcom/peel/ui/b/cv;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2

    .prologue
    .line 88
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 10

    .prologue
    .line 93
    if-nez p2, :cond_4

    new-instance v0, Lcom/peel/ui/b/cz;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/peel/ui/b/cz;-><init>(Lcom/peel/ui/b/cy;Lcom/peel/ui/b/cw;)V

    move-object v2, v0

    .line 94
    :goto_0
    invoke-virtual {p0, p1}, Lcom/peel/ui/b/cy;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/content/listing/LiveListing;

    .line 96
    if-nez p2, :cond_0

    .line 97
    iget-object v1, p0, Lcom/peel/ui/b/cy;->a:Lcom/peel/ui/b/cv;

    invoke-static {v1}, Lcom/peel/ui/b/cv;->c(Lcom/peel/ui/b/cv;)Landroid/view/LayoutInflater;

    move-result-object v1

    sget v3, Lcom/peel/ui/fq;->show_details_upcoming_listitem:I

    const/4 v4, 0x0

    invoke-virtual {v1, v3, p3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 98
    sget v1, Lcom/peel/ui/fp;->reminder_icon:I

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, v2, Lcom/peel/ui/b/cz;->a:Landroid/widget/ImageView;

    .line 99
    sget v1, Lcom/peel/ui/fp;->day:I

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, v2, Lcom/peel/ui/b/cz;->b:Landroid/widget/TextView;

    .line 100
    sget v1, Lcom/peel/ui/fp;->channel:I

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, v2, Lcom/peel/ui/b/cz;->f:Landroid/widget/TextView;

    .line 101
    sget v1, Lcom/peel/ui/fp;->month:I

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, v2, Lcom/peel/ui/b/cz;->c:Landroid/widget/TextView;

    .line 102
    sget v1, Lcom/peel/ui/fp;->title:I

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, v2, Lcom/peel/ui/b/cz;->d:Landroid/widget/TextView;

    .line 103
    sget v1, Lcom/peel/ui/fp;->season_title:I

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, v2, Lcom/peel/ui/b/cz;->e:Landroid/widget/TextView;

    .line 105
    invoke-virtual {p2, v2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 108
    :cond_0
    invoke-static {v0}, Lcom/peel/util/bx;->e(Lcom/peel/content/listing/Listing;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 109
    iget-object v1, v2, Lcom/peel/ui/b/cz;->a:Landroid/widget/ImageView;

    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 110
    iget-object v1, v2, Lcom/peel/ui/b/cz;->a:Landroid/widget/ImageView;

    sget v3, Lcom/peel/ui/fo;->reminder_icon_selected:I

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 120
    :goto_1
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v1

    .line 121
    invoke-virtual {v0}, Lcom/peel/content/listing/LiveListing;->o()J

    move-result-wide v4

    invoke-virtual {v1, v4, v5}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 123
    iget-object v3, v2, Lcom/peel/ui/b/cz;->c:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/peel/ui/b/cy;->a:Lcom/peel/ui/b/cv;

    invoke-static {v4}, Lcom/peel/ui/b/cv;->d(Lcom/peel/ui/b/cv;)Ljava/text/DateFormatSymbols;

    move-result-object v4

    invoke-virtual {v4}, Ljava/text/DateFormatSymbols;->getShortMonths()[Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x2

    invoke-virtual {v1, v5}, Ljava/util/Calendar;->get(I)I

    move-result v5

    aget-object v4, v4, v5

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 124
    iget-object v3, v2, Lcom/peel/ui/b/cz;->b:Landroid/widget/TextView;

    const/4 v4, 0x5

    invoke-virtual {v1, v4}, Ljava/util/Calendar;->get(I)I

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 126
    invoke-virtual {v0}, Lcom/peel/content/listing/LiveListing;->v()Ljava/lang/String;

    move-result-object v1

    .line 129
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 130
    invoke-virtual {v0}, Lcom/peel/content/listing/LiveListing;->w()Ljava/lang/String;

    move-result-object v4

    .line 131
    if-eqz v4, :cond_1

    const-string/jumbo v5, "0"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 132
    iget-object v5, p0, Lcom/peel/ui/b/cy;->a:Lcom/peel/ui/b/cv;

    sget v6, Lcom/peel/ui/ft;->episode_number:I

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    aput-object v4, v7, v8

    invoke-virtual {v5, v6, v7}, Lcom/peel/ui/b/cv;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 135
    :cond_1
    invoke-virtual {v0}, Lcom/peel/content/listing/LiveListing;->A()Ljava/lang/String;

    move-result-object v4

    .line 136
    if-eqz v4, :cond_3

    const-string/jumbo v5, "0"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_3

    .line 137
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->length()I

    move-result v5

    if-lez v5, :cond_2

    const-string/jumbo v5, ", "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 138
    :cond_2
    iget-object v5, p0, Lcom/peel/ui/b/cy;->a:Lcom/peel/ui/b/cv;

    sget v6, Lcom/peel/ui/ft;->season_number:I

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    aput-object v4, v7, v8

    invoke-virtual {v5, v6, v7}, Lcom/peel/ui/b/cv;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 141
    :cond_3
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->length()I

    move-result v4

    if-lez v4, :cond_7

    .line 142
    iget-object v4, v2, Lcom/peel/ui/b/cz;->e:Landroid/widget/TextView;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 147
    :goto_2
    iget-object v3, v2, Lcom/peel/ui/b/cz;->d:Landroid/widget/TextView;

    if-eqz v1, :cond_8

    :goto_3
    invoke-virtual {v3, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 149
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 151
    invoke-virtual {v0}, Lcom/peel/content/listing/LiveListing;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v4, " "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Lcom/peel/content/listing/LiveListing;->d()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 152
    const-string/jumbo v1, " - "

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 153
    sget-object v1, Lcom/peel/util/x;->c:Ljava/lang/ThreadLocal;

    invoke-virtual {v1}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/text/SimpleDateFormat;

    invoke-virtual {v0}, Lcom/peel/content/listing/LiveListing;->o()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lcom/peel/content/listing/LiveListing;->j()J

    move-result-wide v4

    iget-object v0, p0, Lcom/peel/ui/b/cy;->a:Lcom/peel/ui/b/cv;

    invoke-virtual {v0}, Lcom/peel/ui/b/cv;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    invoke-static {v0}, Landroid/text/format/DateFormat;->is24HourFormat(Landroid/content/Context;)Z

    move-result v0

    iget-object v6, p0, Lcom/peel/ui/b/cy;->a:Lcom/peel/ui/b/cv;

    sget v7, Lcom/peel/ui/ft;->time_pattern:I

    invoke-virtual {v6, v7}, Lcom/peel/ui/b/cv;->a(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {v1, v4, v5, v0, v6}, Lcom/peel/util/x;->a(Ljava/lang/String;JZLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 155
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    if-lez v0, :cond_9

    .line 156
    iget-object v0, v2, Lcom/peel/ui/b/cz;->f:Landroid/widget/TextView;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 161
    :goto_4
    return-object p2

    .line 93
    :cond_4
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/ui/b/cz;

    move-object v2, v0

    goto/16 :goto_0

    .line 112
    :cond_5
    invoke-virtual {v0}, Lcom/peel/content/listing/LiveListing;->o()J

    move-result-wide v4

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    const-wide/32 v8, 0x1b7740

    add-long/2addr v6, v8

    cmp-long v1, v4, v6

    if-gtz v1, :cond_6

    .line 113
    iget-object v1, v2, Lcom/peel/ui/b/cz;->a:Landroid/widget/ImageView;

    const/4 v3, 0x4

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_1

    .line 115
    :cond_6
    iget-object v1, v2, Lcom/peel/ui/b/cz;->a:Landroid/widget/ImageView;

    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 116
    iget-object v1, v2, Lcom/peel/ui/b/cz;->a:Landroid/widget/ImageView;

    sget v3, Lcom/peel/ui/fo;->reminder_icon:I

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_1

    .line 144
    :cond_7
    iget-object v3, v2, Lcom/peel/ui/b/cz;->e:Landroid/widget/TextView;

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_2

    .line 147
    :cond_8
    invoke-virtual {v0}, Lcom/peel/content/listing/LiveListing;->l()Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_3

    .line 158
    :cond_9
    iget-object v0, v2, Lcom/peel/ui/b/cz;->f:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_4
.end method
