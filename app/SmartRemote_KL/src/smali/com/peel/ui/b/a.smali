.class public Lcom/peel/ui/b/a;
.super Landroid/widget/Button;


# instance fields
.field private a:Lcom/peel/ui/b/b;

.field private b:Z

.field private c:F

.field private d:F

.field private e:F

.field private f:F

.field private g:F

.field private h:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 49
    invoke-direct {p0, p1}, Landroid/widget/Button;-><init>(Landroid/content/Context;)V

    .line 28
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/peel/ui/b/a;->b:Z

    .line 34
    iput v1, p0, Lcom/peel/ui/b/a;->d:F

    .line 37
    const/high16 v0, 0x41000000    # 8.0f

    iput v0, p0, Lcom/peel/ui/b/a;->e:F

    .line 40
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/peel/ui/b/a;->f:F

    .line 43
    iput v1, p0, Lcom/peel/ui/b/a;->g:F

    .line 46
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/peel/ui/b/a;->h:Z

    .line 50
    return-void
.end method

.method private a(Ljava/lang/CharSequence;Landroid/text/TextPaint;IF)I
    .locals 8

    .prologue
    .line 282
    new-instance v2, Landroid/text/TextPaint;

    invoke-direct {v2, p2}, Landroid/text/TextPaint;-><init>(Landroid/graphics/Paint;)V

    .line 284
    invoke-virtual {v2, p4}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 286
    new-instance v0, Landroid/text/StaticLayout;

    sget-object v4, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    iget v5, p0, Lcom/peel/ui/b/a;->f:F

    iget v6, p0, Lcom/peel/ui/b/a;->g:F

    const/4 v7, 0x1

    move-object v1, p1

    move v3, p3

    invoke-direct/range {v0 .. v7}, Landroid/text/StaticLayout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZ)V

    .line 287
    invoke-virtual {v0}, Landroid/text/StaticLayout;->getHeight()I

    move-result v0

    return v0
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 173
    iget v0, p0, Lcom/peel/ui/b/a;->c:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    .line 174
    const/4 v0, 0x0

    iget v1, p0, Lcom/peel/ui/b/a;->c:F

    invoke-super {p0, v0, v1}, Landroid/widget/Button;->setTextSize(IF)V

    .line 175
    iget v0, p0, Lcom/peel/ui/b/a;->c:F

    iput v0, p0, Lcom/peel/ui/b/a;->d:F

    .line 177
    :cond_0
    return-void
.end method

.method public a(II)V
    .locals 11

    .prologue
    const/4 v2, 0x0

    const/4 v7, 0x0

    .line 208
    invoke-virtual {p0}, Lcom/peel/ui/b/a;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    .line 210
    if-eqz v1, :cond_0

    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-eqz v0, :cond_0

    if-lez p2, :cond_0

    if-lez p1, :cond_0

    iget v0, p0, Lcom/peel/ui/b/a;->c:F

    cmpl-float v0, v0, v2

    if-nez v0, :cond_1

    .line 275
    :cond_0
    :goto_0
    return-void

    .line 215
    :cond_1
    invoke-virtual {p0}, Lcom/peel/ui/b/a;->getPaint()Landroid/text/TextPaint;

    move-result-object v9

    .line 218
    invoke-virtual {v9}, Landroid/text/TextPaint;->getTextSize()F

    move-result v10

    .line 220
    iget v0, p0, Lcom/peel/ui/b/a;->d:F

    cmpl-float v0, v0, v2

    if-lez v0, :cond_2

    iget v0, p0, Lcom/peel/ui/b/a;->c:F

    iget v2, p0, Lcom/peel/ui/b/a;->d:F

    invoke-static {v0, v2}, Ljava/lang/Math;->min(FF)F

    move-result v0

    .line 223
    :goto_1
    invoke-direct {p0, v1, v9, p1, v0}, Lcom/peel/ui/b/a;->a(Ljava/lang/CharSequence;Landroid/text/TextPaint;IF)I

    move-result v2

    move v8, v0

    move v0, v2

    .line 226
    :goto_2
    if-le v0, p2, :cond_3

    iget v2, p0, Lcom/peel/ui/b/a;->e:F

    cmpl-float v2, v8, v2

    if-lez v2, :cond_3

    .line 227
    const/high16 v0, 0x40000000    # 2.0f

    sub-float v0, v8, v0

    iget v2, p0, Lcom/peel/ui/b/a;->e:F

    invoke-static {v0, v2}, Ljava/lang/Math;->max(FF)F

    move-result v2

    .line 228
    invoke-direct {p0, v1, v9, p1, v2}, Lcom/peel/ui/b/a;->a(Ljava/lang/CharSequence;Landroid/text/TextPaint;IF)I

    move-result v0

    move v8, v2

    goto :goto_2

    .line 220
    :cond_2
    iget v0, p0, Lcom/peel/ui/b/a;->c:F

    goto :goto_1

    .line 232
    :cond_3
    iget-boolean v2, p0, Lcom/peel/ui/b/a;->h:Z

    if-eqz v2, :cond_4

    iget v2, p0, Lcom/peel/ui/b/a;->e:F

    cmpl-float v2, v8, v2

    if-nez v2, :cond_4

    if-le v0, p2, :cond_4

    .line 235
    new-instance v2, Landroid/text/TextPaint;

    invoke-direct {v2, v9}, Landroid/text/TextPaint;-><init>(Landroid/graphics/Paint;)V

    .line 237
    new-instance v0, Landroid/text/StaticLayout;

    sget-object v4, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    iget v5, p0, Lcom/peel/ui/b/a;->f:F

    iget v6, p0, Lcom/peel/ui/b/a;->g:F

    move v3, p1

    invoke-direct/range {v0 .. v7}, Landroid/text/StaticLayout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZ)V

    .line 239
    invoke-virtual {v0}, Landroid/text/StaticLayout;->getLineCount()I

    move-result v2

    if-lez v2, :cond_4

    .line 242
    invoke-virtual {v0, p2}, Landroid/text/StaticLayout;->getLineForVertical(I)I

    move-result v2

    add-int/lit8 v3, v2, -0x1

    .line 244
    if-gez v3, :cond_6

    .line 245
    const-string/jumbo v0, ""

    invoke-virtual {p0, v0}, Lcom/peel/ui/b/a;->setText(Ljava/lang/CharSequence;)V

    .line 265
    :cond_4
    :goto_3
    invoke-virtual {p0, v7, v8}, Lcom/peel/ui/b/a;->setTextSize(IF)V

    .line 266
    iget v0, p0, Lcom/peel/ui/b/a;->g:F

    iget v1, p0, Lcom/peel/ui/b/a;->f:F

    invoke-virtual {p0, v0, v1}, Lcom/peel/ui/b/a;->setLineSpacing(FF)V

    .line 269
    iget-object v0, p0, Lcom/peel/ui/b/a;->a:Lcom/peel/ui/b/b;

    if-eqz v0, :cond_5

    .line 270
    iget-object v0, p0, Lcom/peel/ui/b/a;->a:Lcom/peel/ui/b/b;

    invoke-interface {v0, p0, v10, v8}, Lcom/peel/ui/b/b;->a(Landroid/widget/TextView;FF)V

    .line 274
    :cond_5
    iput-boolean v7, p0, Lcom/peel/ui/b/a;->b:Z

    goto :goto_0

    .line 249
    :cond_6
    invoke-virtual {v0, v3}, Landroid/text/StaticLayout;->getLineStart(I)I

    move-result v4

    .line 250
    invoke-virtual {v0, v3}, Landroid/text/StaticLayout;->getLineEnd(I)I

    move-result v2

    .line 251
    invoke-virtual {v0, v3}, Landroid/text/StaticLayout;->getLineWidth(I)F

    move-result v0

    .line 252
    const-string/jumbo v3, "..."

    invoke-virtual {v9, v3}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;)F

    move-result v3

    .line 255
    :goto_4
    int-to-float v5, p1

    add-float/2addr v0, v3

    cmpg-float v0, v5, v0

    if-gez v0, :cond_7

    .line 256
    add-int/lit8 v2, v2, -0x1

    add-int/lit8 v0, v2, 0x1

    invoke-interface {v1, v4, v0}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v9, v0}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;)F

    move-result v0

    goto :goto_4

    .line 258
    :cond_7
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {v1, v7, v2}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "..."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/peel/ui/b/a;->setText(Ljava/lang/CharSequence;)V

    goto :goto_3
.end method

.method public getAddEllipsis()Z
    .locals 1

    .prologue
    .line 166
    iget-boolean v0, p0, Lcom/peel/ui/b/a;->h:Z

    return v0
.end method

.method public getMaxTextSize()F
    .locals 1

    .prologue
    .line 132
    iget v0, p0, Lcom/peel/ui/b/a;->d:F

    return v0
.end method

.method public getMinTextSize()F
    .locals 1

    .prologue
    .line 150
    iget v0, p0, Lcom/peel/ui/b/a;->e:F

    return v0
.end method

.method protected onLayout(ZIIII)V
    .locals 3

    .prologue
    .line 184
    if-nez p1, :cond_0

    iget-boolean v0, p0, Lcom/peel/ui/b/a;->b:Z

    if-eqz v0, :cond_1

    .line 185
    :cond_0
    sub-int v0, p4, p2

    invoke-virtual {p0}, Lcom/peel/ui/b/a;->getCompoundPaddingLeft()I

    move-result v1

    sub-int/2addr v0, v1

    invoke-virtual {p0}, Lcom/peel/ui/b/a;->getCompoundPaddingRight()I

    move-result v1

    sub-int/2addr v0, v1

    .line 186
    sub-int v1, p5, p3

    invoke-virtual {p0}, Lcom/peel/ui/b/a;->getCompoundPaddingBottom()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-virtual {p0}, Lcom/peel/ui/b/a;->getCompoundPaddingTop()I

    move-result v2

    sub-int/2addr v1, v2

    .line 187
    invoke-virtual {p0, v0, v1}, Lcom/peel/ui/b/a;->a(II)V

    .line 189
    :cond_1
    invoke-super/range {p0 .. p5}, Landroid/widget/Button;->onLayout(ZIIII)V

    .line 190
    return-void
.end method

.method protected onSizeChanged(IIII)V
    .locals 1

    .prologue
    .line 76
    if-ne p1, p3, :cond_0

    if-eq p2, p4, :cond_1

    .line 77
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/peel/ui/b/a;->b:Z

    .line 79
    :cond_1
    return-void
.end method

.method protected onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 1

    .prologue
    .line 66
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/peel/ui/b/a;->b:Z

    .line 68
    invoke-virtual {p0}, Lcom/peel/ui/b/a;->a()V

    .line 69
    return-void
.end method

.method public setAddEllipsis(Z)V
    .locals 0

    .prologue
    .line 158
    iput-boolean p1, p0, Lcom/peel/ui/b/a;->h:Z

    .line 159
    return-void
.end method

.method public setLineSpacing(FF)V
    .locals 0

    .prologue
    .line 112
    invoke-super {p0, p1, p2}, Landroid/widget/Button;->setLineSpacing(FF)V

    .line 113
    iput p2, p0, Lcom/peel/ui/b/a;->f:F

    .line 114
    iput p1, p0, Lcom/peel/ui/b/a;->g:F

    .line 115
    return-void
.end method

.method public setMaxTextSize(F)V
    .locals 0

    .prologue
    .line 122
    iput p1, p0, Lcom/peel/ui/b/a;->d:F

    .line 123
    invoke-virtual {p0}, Lcom/peel/ui/b/a;->requestLayout()V

    .line 124
    invoke-virtual {p0}, Lcom/peel/ui/b/a;->invalidate()V

    .line 125
    return-void
.end method

.method public setMinTextSize(F)V
    .locals 0

    .prologue
    .line 140
    iput p1, p0, Lcom/peel/ui/b/a;->e:F

    .line 141
    invoke-virtual {p0}, Lcom/peel/ui/b/a;->requestLayout()V

    .line 142
    invoke-virtual {p0}, Lcom/peel/ui/b/a;->invalidate()V

    .line 143
    return-void
.end method

.method public setOnResizeListener(Lcom/peel/ui/b/b;)V
    .locals 0

    .prologue
    .line 86
    iput-object p1, p0, Lcom/peel/ui/b/a;->a:Lcom/peel/ui/b/b;

    .line 87
    return-void
.end method

.method public setTextSize(F)V
    .locals 1

    .prologue
    .line 94
    invoke-super {p0, p1}, Landroid/widget/Button;->setTextSize(F)V

    .line 95
    invoke-virtual {p0}, Lcom/peel/ui/b/a;->getTextSize()F

    move-result v0

    iput v0, p0, Lcom/peel/ui/b/a;->c:F

    .line 96
    return-void
.end method

.method public setTextSize(IF)V
    .locals 1

    .prologue
    .line 103
    invoke-super {p0, p1, p2}, Landroid/widget/Button;->setTextSize(IF)V

    .line 104
    invoke-virtual {p0}, Lcom/peel/ui/b/a;->getTextSize()F

    move-result v0

    iput v0, p0, Lcom/peel/ui/b/a;->c:F

    .line 105
    return-void
.end method
