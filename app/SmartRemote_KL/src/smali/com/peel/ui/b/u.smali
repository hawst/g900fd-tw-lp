.class Lcom/peel/ui/b/u;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/peel/ui/b/t;


# direct methods
.method constructor <init>(Lcom/peel/ui/b/t;)V
    .locals 0

    .prologue
    .line 178
    iput-object p1, p0, Lcom/peel/ui/b/u;->a:Lcom/peel/ui/b/t;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 181
    iget-object v0, p0, Lcom/peel/ui/b/u;->a:Lcom/peel/ui/b/t;

    iget-object v0, v0, Lcom/peel/ui/b/t;->a:Lcom/peel/ui/b/s;

    iget-object v0, v0, Lcom/peel/ui/b/s;->a:Lcom/peel/ui/model/TwittData;

    invoke-virtual {v0}, Lcom/peel/ui/model/TwittData;->f()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 182
    iget-object v0, p0, Lcom/peel/ui/b/u;->a:Lcom/peel/ui/b/t;

    iget-object v0, v0, Lcom/peel/ui/b/t;->a:Lcom/peel/ui/b/s;

    iget-object v0, v0, Lcom/peel/ui/b/s;->b:Landroid/widget/TextView;

    sget v1, Lcom/peel/ui/fo;->retweet_icon_press:I

    invoke-virtual {v0, v1, v2, v2, v2}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    .line 183
    invoke-static {}, Lcom/peel/util/a/f;->a()Lcom/peel/util/a/f;

    move-result-object v1

    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v0}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const/16 v2, 0x4c0

    iget-object v3, p0, Lcom/peel/ui/b/u;->a:Lcom/peel/ui/b/t;

    iget-object v3, v3, Lcom/peel/ui/b/t;->a:Lcom/peel/ui/b/s;

    iget-object v3, v3, Lcom/peel/ui/b/s;->c:Lcom/peel/ui/b/r;

    .line 184
    invoke-static {v3}, Lcom/peel/ui/b/r;->c(Lcom/peel/ui/b/r;)I

    move-result v3

    .line 183
    invoke-virtual {v1, v0, v2, v3}, Lcom/peel/util/a/f;->a(III)V

    .line 188
    :goto_1
    return-void

    .line 183
    :cond_0
    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v0}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/data/at;->f()I

    move-result v0

    goto :goto_0

    .line 186
    :cond_1
    iget-object v0, p0, Lcom/peel/ui/b/u;->a:Lcom/peel/ui/b/t;

    iget-object v0, v0, Lcom/peel/ui/b/t;->a:Lcom/peel/ui/b/s;

    iget-object v0, v0, Lcom/peel/ui/b/s;->b:Landroid/widget/TextView;

    sget v1, Lcom/peel/ui/fo;->retweet_icon_normal:I

    invoke-virtual {v0, v1, v2, v2, v2}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    goto :goto_1
.end method
