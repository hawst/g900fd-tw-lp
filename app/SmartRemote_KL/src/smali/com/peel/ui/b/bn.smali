.class public Lcom/peel/ui/b/bn;
.super Lcom/peel/ui/b/af;


# static fields
.field private static final c:Ljava/lang/String;


# instance fields
.field private final d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/peel/content/listing/Listing;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Landroid/support/v4/app/ae;

.field private f:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 29
    const-class v0, Lcom/peel/ui/b/bn;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/peel/ui/b/bn;->c:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/support/v4/app/ae;Ljava/util/List;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/app/ae;",
            "Ljava/util/List",
            "<",
            "Lcom/peel/content/listing/Listing;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .line 34
    invoke-direct {p0}, Lcom/peel/ui/b/af;-><init>()V

    .line 35
    iput-object p1, p0, Lcom/peel/ui/b/bn;->e:Landroid/support/v4/app/ae;

    .line 36
    iput-object p2, p0, Lcom/peel/ui/b/bn;->d:Ljava/util/List;

    .line 37
    iput p3, p0, Lcom/peel/ui/b/bn;->f:I

    .line 38
    return-void
.end method

.method static synthetic a(Lcom/peel/ui/b/bn;)Landroid/support/v4/app/ae;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lcom/peel/ui/b/bn;->e:Landroid/support/v4/app/ae;

    return-object v0
.end method

.method static synthetic b(Lcom/peel/ui/b/bn;)I
    .locals 1

    .prologue
    .line 28
    iget v0, p0, Lcom/peel/ui/b/bn;->f:I

    return v0
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 42
    const/4 v0, 0x6

    return v0
.end method

.method public a(Landroid/view/LayoutInflater;Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 10

    .prologue
    const/4 v9, 0x0

    const/4 v8, 0x0

    .line 47
    if-nez p2, :cond_2

    new-instance v0, Lcom/peel/ui/b/bp;

    invoke-direct {v0, v9}, Lcom/peel/ui/b/bp;-><init>(Lcom/peel/ui/b/bo;)V

    move-object v1, v0

    .line 49
    :goto_0
    if-nez p2, :cond_0

    .line 50
    sget v0, Lcom/peel/ui/fq;->more_like_this_layout:I

    invoke-virtual {p1, v0, p3, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 51
    sget v0, Lcom/peel/ui/fp;->more_like_this_list:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/HorizontalScrollView;

    invoke-static {v1, v0}, Lcom/peel/ui/b/bp;->a(Lcom/peel/ui/b/bp;Landroid/widget/HorizontalScrollView;)Landroid/widget/HorizontalScrollView;

    .line 53
    invoke-virtual {p2, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 56
    :cond_0
    invoke-static {v1}, Lcom/peel/ui/b/bp;->a(Lcom/peel/ui/b/bp;)Landroid/widget/HorizontalScrollView;

    move-result-object v0

    sget v1, Lcom/peel/ui/fp;->item_container:I

    invoke-virtual {v0, v1}, Landroid/widget/HorizontalScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 57
    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 59
    iget-object v1, p0, Lcom/peel/ui/b/bn;->d:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/peel/content/listing/Listing;

    .line 60
    check-cast v1, Lcom/peel/content/listing/LiveListing;

    .line 61
    sget v2, Lcom/peel/ui/fq;->more_like_this_item:I

    invoke-virtual {p1, v2, v9, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v4

    .line 64
    :try_start_0
    invoke-virtual {v1}, Lcom/peel/content/listing/LiveListing;->y()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {v1}, Lcom/peel/content/listing/LiveListing;->y()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_1

    .line 65
    iget-object v2, p0, Lcom/peel/ui/b/bn;->e:Landroid/support/v4/app/ae;

    .line 66
    invoke-static {v2}, Lcom/peel/util/b/e;->a(Landroid/content/Context;)Lcom/squareup/picasso/Picasso;

    move-result-object v2

    .line 67
    invoke-virtual {v1}, Lcom/peel/content/listing/LiveListing;->y()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Lcom/squareup/picasso/Picasso;->load(Ljava/lang/String;)Lcom/squareup/picasso/RequestCreator;

    move-result-object v2

    .line 68
    invoke-virtual {v1}, Lcom/peel/content/listing/LiveListing;->m()[Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1}, Lcom/peel/content/listing/LiveListing;->k()Ljava/lang/String;

    move-result-object v6

    const/16 v7, 0x2b

    invoke-static {v5, v6, v7}, Lcom/peel/util/bx;->a([Ljava/lang/String;Ljava/lang/String;I)I

    move-result v5

    invoke-virtual {v2, v5}, Lcom/squareup/picasso/RequestCreator;->placeholder(I)Lcom/squareup/picasso/RequestCreator;

    move-result-object v5

    sget v2, Lcom/peel/ui/fp;->show_image:I

    .line 69
    invoke-virtual {v4, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    invoke-virtual {v5, v2}, Lcom/squareup/picasso/RequestCreator;->into(Landroid/widget/ImageView;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 75
    :cond_1
    :goto_2
    sget v2, Lcom/peel/ui/fp;->show_title:I

    invoke-virtual {v4, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    invoke-virtual {v1}, Lcom/peel/content/listing/LiveListing;->l()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 77
    new-instance v2, Lcom/peel/ui/b/bo;

    invoke-direct {v2, p0, v1}, Lcom/peel/ui/b/bo;-><init>(Lcom/peel/ui/b/bn;Lcom/peel/content/listing/LiveListing;)V

    invoke-virtual {v4, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 86
    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    goto :goto_1

    .line 47
    :cond_2
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/ui/b/bp;

    move-object v1, v0

    goto/16 :goto_0

    .line 71
    :catch_0
    move-exception v2

    .line 72
    sget-object v5, Lcom/peel/ui/b/bn;->c:Ljava/lang/String;

    sget-object v6, Lcom/peel/ui/b/bn;->c:Ljava/lang/String;

    invoke-static {v5, v6, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_2

    .line 89
    :cond_3
    return-object p2
.end method
