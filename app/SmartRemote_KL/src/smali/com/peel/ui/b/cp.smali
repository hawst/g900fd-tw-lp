.class public Lcom/peel/ui/b/cp;
.super Lcom/peel/d/u;


# instance fields
.field private aj:Ljava/lang/String;

.field private ak:Landroid/widget/EditText;

.field private al:[Ljava/lang/String;

.field private am:Landroid/view/View;

.field private an:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/peel/ui/model/TwittData;",
            ">;"
        }
    .end annotation
.end field

.field private ao:Landroid/view/View;

.field private ap:Lcom/peel/ui/b/a/a;

.field e:Lcom/peel/util/t;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/peel/util/t",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/peel/ui/model/TwittData;",
            ">;>;"
        }
    .end annotation
.end field

.field private f:Lcom/peel/ui/mx;

.field private g:Landroid/widget/ListView;

.field private h:I

.field private i:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 36
    invoke-direct {p0}, Lcom/peel/d/u;-><init>()V

    .line 100
    new-instance v0, Lcom/peel/ui/b/cr;

    invoke-direct {v0, p0}, Lcom/peel/ui/b/cr;-><init>(Lcom/peel/ui/b/cp;)V

    iput-object v0, p0, Lcom/peel/ui/b/cp;->e:Lcom/peel/util/t;

    return-void
.end method

.method static synthetic a(Lcom/peel/ui/b/cp;)Landroid/view/View;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/peel/ui/b/cp;->ao:Landroid/view/View;

    return-object v0
.end method

.method static synthetic a(Lcom/peel/ui/b/cp;Lcom/peel/ui/mx;)Lcom/peel/ui/mx;
    .locals 0

    .prologue
    .line 36
    iput-object p1, p0, Lcom/peel/ui/b/cp;->f:Lcom/peel/ui/mx;

    return-object p1
.end method

.method static synthetic a(Lcom/peel/ui/b/cp;Ljava/util/List;)Ljava/util/List;
    .locals 0

    .prologue
    .line 36
    iput-object p1, p0, Lcom/peel/ui/b/cp;->an:Ljava/util/List;

    return-object p1
.end method

.method static synthetic b(Lcom/peel/ui/b/cp;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/peel/ui/b/cp;->i:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic c(Lcom/peel/ui/b/cp;)Lcom/peel/ui/b/a/a;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/peel/ui/b/cp;->ap:Lcom/peel/ui/b/a/a;

    return-object v0
.end method

.method private c()V
    .locals 3

    .prologue
    .line 156
    new-instance v0, Lcom/peel/widget/ag;

    invoke-virtual {p0}, Lcom/peel/ui/b/cp;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/peel/widget/ag;-><init>(Landroid/content/Context;)V

    sget v1, Lcom/peel/ui/ft;->login_twitter:I

    invoke-virtual {v0, v1}, Lcom/peel/widget/ag;->a(I)Lcom/peel/widget/ag;

    move-result-object v0

    sget v1, Lcom/peel/ui/ft;->twitter_login_desc:I

    invoke-virtual {v0, v1}, Lcom/peel/widget/ag;->b(I)Lcom/peel/widget/ag;

    move-result-object v0

    sget v1, Lcom/peel/ui/ft;->login:I

    new-instance v2, Lcom/peel/ui/b/cu;

    invoke-direct {v2, p0}, Lcom/peel/ui/b/cu;-><init>(Lcom/peel/ui/b/cp;)V

    invoke-virtual {v0, v1, v2}, Lcom/peel/widget/ag;->a(ILandroid/content/DialogInterface$OnClickListener;)Lcom/peel/widget/ag;

    move-result-object v0

    sget v1, Lcom/peel/ui/ft;->cancel:I

    new-instance v2, Lcom/peel/ui/b/ct;

    invoke-direct {v2, p0}, Lcom/peel/ui/b/ct;-><init>(Lcom/peel/ui/b/cp;)V

    .line 165
    invoke-virtual {v0, v1, v2}, Lcom/peel/widget/ag;->c(ILandroid/content/DialogInterface$OnClickListener;)Lcom/peel/widget/ag;

    move-result-object v0

    .line 170
    invoke-virtual {v0}, Lcom/peel/widget/ag;->d()Lcom/peel/widget/ag;

    .line 171
    return-void
.end method

.method static synthetic d(Lcom/peel/ui/b/cp;)Lcom/peel/ui/mx;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/peel/ui/b/cp;->f:Lcom/peel/ui/mx;

    return-object v0
.end method

.method static synthetic e(Lcom/peel/ui/b/cp;)Ljava/util/List;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/peel/ui/b/cp;->an:Ljava/util/List;

    return-object v0
.end method

.method static synthetic f(Lcom/peel/ui/b/cp;)I
    .locals 1

    .prologue
    .line 36
    iget v0, p0, Lcom/peel/ui/b/cp;->h:I

    return v0
.end method

.method static synthetic g(Lcom/peel/ui/b/cp;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/peel/ui/b/cp;->aj:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic h(Lcom/peel/ui/b/cp;)[Ljava/lang/String;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/peel/ui/b/cp;->al:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic i(Lcom/peel/ui/b/cp;)Landroid/widget/ListView;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/peel/ui/b/cp;->g:Landroid/widget/ListView;

    return-object v0
.end method


# virtual methods
.method public Z()V
    .locals 6

    .prologue
    .line 194
    iget-object v0, p0, Lcom/peel/ui/b/cp;->d:Lcom/peel/d/a;

    if-nez v0, :cond_0

    .line 195
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 196
    sget v0, Lcom/peel/ui/fp;->menu_tweet_refresh:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 197
    new-instance v0, Lcom/peel/d/a;

    sget-object v1, Lcom/peel/d/d;->b:Lcom/peel/d/d;

    sget-object v2, Lcom/peel/d/b;->a:Lcom/peel/d/b;

    sget-object v3, Lcom/peel/d/c;->b:Lcom/peel/d/c;

    sget v4, Lcom/peel/ui/ft;->twitter_label:I

    invoke-virtual {p0, v4}, Lcom/peel/ui/b/cp;->a(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct/range {v0 .. v5}, Lcom/peel/d/a;-><init>(Lcom/peel/d/d;Lcom/peel/d/b;Lcom/peel/d/c;Ljava/lang/String;Ljava/util/List;)V

    iput-object v0, p0, Lcom/peel/ui/b/cp;->d:Lcom/peel/d/a;

    .line 200
    :cond_0
    iget-object v0, p0, Lcom/peel/ui/b/cp;->b:Lcom/peel/d/i;

    iget-object v1, p0, Lcom/peel/ui/b/cp;->d:Lcom/peel/d/a;

    invoke-virtual {v0, v1}, Lcom/peel/d/i;->a(Lcom/peel/d/a;)V

    .line 201
    return-void
.end method

.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 51
    sget v0, Lcom/peel/ui/fq;->show_details_twitter_overview:I

    invoke-virtual {p1, v0, p2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 52
    sget v0, Lcom/peel/ui/fp;->tweets_listview:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/peel/ui/b/cp;->g:Landroid/widget/ListView;

    .line 53
    sget v0, Lcom/peel/ui/fp;->fake_edit:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/peel/ui/b/cp;->ak:Landroid/widget/EditText;

    .line 54
    sget v0, Lcom/peel/ui/fp;->compose_tweet:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/ui/b/cp;->am:Landroid/view/View;

    .line 55
    iget-object v0, p0, Lcom/peel/ui/b/cp;->c:Landroid/os/Bundle;

    const-string/jumbo v2, "contextId"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/peel/ui/b/cp;->h:I

    .line 56
    iget-object v0, p0, Lcom/peel/ui/b/cp;->c:Landroid/os/Bundle;

    const-string/jumbo v2, "showid"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/ui/b/cp;->i:Ljava/lang/String;

    .line 57
    iget-object v0, p0, Lcom/peel/ui/b/cp;->c:Landroid/os/Bundle;

    const-string/jumbo v2, "tmsid"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/ui/b/cp;->aj:Ljava/lang/String;

    .line 58
    iget-object v0, p0, Lcom/peel/ui/b/cp;->c:Landroid/os/Bundle;

    const-string/jumbo v2, "keyword"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/ui/b/cp;->al:[Ljava/lang/String;

    .line 59
    iget-object v0, p0, Lcom/peel/ui/b/cp;->c:Landroid/os/Bundle;

    const-string/jumbo v2, "twittList"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/ui/b/cp;->an:Ljava/util/List;

    .line 61
    iget-object v0, p0, Lcom/peel/ui/b/cp;->al:[Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 62
    iget-object v0, p0, Lcom/peel/ui/b/cp;->ak:Landroid/widget/EditText;

    iget-object v2, p0, Lcom/peel/ui/b/cp;->al:[Ljava/lang/String;

    aget-object v2, v2, v3

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 65
    :cond_0
    sget v0, Lcom/peel/ui/fq;->tweet_refresh_layout:I

    const/4 v2, 0x0

    invoke-virtual {p1, v0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/ui/b/cp;->ao:Landroid/view/View;

    .line 66
    iget-object v0, p0, Lcom/peel/ui/b/cp;->ao:Landroid/view/View;

    new-instance v2, Lcom/peel/ui/b/cq;

    invoke-direct {v2, p0}, Lcom/peel/ui/b/cq;-><init>(Lcom/peel/ui/b/cp;)V

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 75
    return-object v1
.end method

.method public a(IILandroid/content/Intent;)V
    .locals 2

    .prologue
    .line 150
    const/16 v0, 0x65

    if-ne p1, v0, :cond_0

    .line 151
    iget-object v0, p0, Lcom/peel/ui/b/cp;->ap:Lcom/peel/ui/b/a/a;

    iget-object v1, p0, Lcom/peel/ui/b/cp;->i:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/peel/ui/b/a/a;->a(Ljava/lang/String;)V

    .line 153
    :cond_0
    return-void
.end method

.method public a(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 128
    invoke-interface {p1}, Landroid/view/Menu;->clear()V

    .line 130
    sget v0, Lcom/peel/ui/fr;->menu:I

    invoke-virtual {p2, v0, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    move v0, v1

    .line 132
    :goto_0
    invoke-interface {p1}, Landroid/view/Menu;->size()I

    move-result v2

    if-ge v0, v2, :cond_1

    .line 133
    invoke-interface {p1, v0}, Landroid/view/Menu;->getItem(I)Landroid/view/MenuItem;

    move-result-object v2

    .line 134
    invoke-interface {v2}, Landroid/view/MenuItem;->getItemId()I

    move-result v3

    sget v4, Lcom/peel/ui/fp;->menu_tweet_refresh:I

    if-ne v3, v4, :cond_0

    .line 135
    iget-object v3, p0, Lcom/peel/ui/b/cp;->ao:Landroid/view/View;

    invoke-interface {v2, v3}, Landroid/view/MenuItem;->setActionView(Landroid/view/View;)Landroid/view/MenuItem;

    .line 136
    const/4 v3, 0x1

    invoke-interface {v2, v3}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 132
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 138
    :cond_0
    invoke-interface {v2, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_1

    .line 141
    :cond_1
    return-void
.end method

.method public aa()Z
    .locals 1

    .prologue
    .line 145
    const/4 v0, 0x1

    return v0
.end method

.method public b()Z
    .locals 2

    .prologue
    .line 205
    iget-object v0, p0, Lcom/peel/ui/b/cp;->ao:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 206
    iget-object v0, p0, Lcom/peel/ui/b/cp;->ao:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->clearAnimation()V

    .line 208
    :cond_0
    iget-object v0, p0, Lcom/peel/ui/b/cp;->ao:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 209
    invoke-super {p0}, Lcom/peel/d/u;->b()Z

    move-result v0

    return v0
.end method

.method public d(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    .line 80
    invoke-super {p0, p1}, Lcom/peel/d/u;->d(Landroid/os/Bundle;)V

    .line 82
    new-instance v0, Lcom/peel/ui/b/a/a;

    invoke-virtual {p0}, Lcom/peel/ui/b/cp;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    invoke-virtual {p0}, Lcom/peel/ui/b/cp;->v()Landroid/view/View;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/peel/ui/b/a/a;-><init>(Landroid/support/v4/app/ae;Landroid/view/View;)V

    iput-object v0, p0, Lcom/peel/ui/b/cp;->ap:Lcom/peel/ui/b/a/a;

    .line 83
    iget-object v0, p0, Lcom/peel/ui/b/cp;->ap:Lcom/peel/ui/b/a/a;

    iget-object v1, p0, Lcom/peel/ui/b/cp;->e:Lcom/peel/util/t;

    invoke-virtual {v0, v1}, Lcom/peel/ui/b/a/a;->a(Lcom/peel/util/t;)V

    .line 84
    iget-object v0, p0, Lcom/peel/ui/b/cp;->f:Lcom/peel/ui/mx;

    if-nez v0, :cond_0

    .line 85
    new-instance v0, Lcom/peel/ui/mx;

    invoke-virtual {p0}, Lcom/peel/ui/b/cp;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    iget v2, p0, Lcom/peel/ui/b/cp;->h:I

    iget-object v3, p0, Lcom/peel/ui/b/cp;->i:Ljava/lang/String;

    iget-object v4, p0, Lcom/peel/ui/b/cp;->aj:Ljava/lang/String;

    iget-object v5, p0, Lcom/peel/ui/b/cp;->al:[Ljava/lang/String;

    invoke-direct/range {v0 .. v5}, Lcom/peel/ui/mx;-><init>(Landroid/support/v4/app/ae;ILjava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V

    iput-object v0, p0, Lcom/peel/ui/b/cp;->f:Lcom/peel/ui/mx;

    .line 86
    iget-object v0, p0, Lcom/peel/ui/b/cp;->f:Lcom/peel/ui/mx;

    iget-object v1, p0, Lcom/peel/ui/b/cp;->an:Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/peel/ui/mx;->a(Ljava/util/List;)V

    .line 87
    iget-object v0, p0, Lcom/peel/ui/b/cp;->g:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/peel/ui/b/cp;->f:Lcom/peel/ui/mx;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 92
    :goto_0
    return-void

    .line 89
    :cond_0
    iget-object v0, p0, Lcom/peel/ui/b/cp;->f:Lcom/peel/ui/mx;

    iget-object v1, p0, Lcom/peel/ui/b/cp;->an:Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/peel/ui/mx;->a(Ljava/util/List;)V

    .line 90
    iget-object v0, p0, Lcom/peel/ui/b/cp;->f:Lcom/peel/ui/mx;

    invoke-virtual {v0}, Lcom/peel/ui/mx;->notifyDataSetChanged()V

    goto :goto_0
.end method

.method public h(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 96
    invoke-super {p0, p1}, Lcom/peel/d/u;->h(Landroid/os/Bundle;)V

    .line 98
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 175
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    .line 176
    sget v1, Lcom/peel/ui/fp;->compose_tweet:I

    if-eq v0, v1, :cond_0

    sget v1, Lcom/peel/ui/fp;->fake_edit:I

    if-eq v0, v1, :cond_0

    sget v1, Lcom/peel/ui/fp;->gallery_icon:I

    if-eq v0, v1, :cond_0

    sget v1, Lcom/peel/ui/fp;->camera_icon:I

    if-ne v0, v1, :cond_1

    .line 177
    :cond_0
    invoke-virtual {p0}, Lcom/peel/ui/b/cp;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    invoke-static {v0}, Lcom/peel/d/v;->a(Landroid/content/Context;)Lcom/peel/d/v;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/d/v;->b()Lcom/peel/d/af;

    move-result-object v0

    if-nez v0, :cond_2

    .line 178
    invoke-direct {p0}, Lcom/peel/ui/b/cp;->c()V

    .line 190
    :cond_1
    :goto_0
    return-void

    .line 180
    :cond_2
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 181
    const-string/jumbo v1, "passback_clazz"

    invoke-virtual {p0}, Lcom/peel/ui/b/cp;->m()Landroid/support/v4/app/ae;

    move-result-object v2

    invoke-static {v2}, Lcom/peel/d/e;->a(Landroid/support/v4/app/ae;)Lcom/peel/d/u;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 182
    const-string/jumbo v1, "keyword"

    iget-object v2, p0, Lcom/peel/ui/b/cp;->al:[Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putStringArray(Ljava/lang/String;[Ljava/lang/String;)V

    .line 183
    const-string/jumbo v1, "context_id"

    iget v2, p0, Lcom/peel/ui/b/cp;->h:I

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 184
    const-string/jumbo v1, "showid"

    iget-object v2, p0, Lcom/peel/ui/b/cp;->i:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 185
    const-string/jumbo v1, "tmsid"

    iget-object v2, p0, Lcom/peel/ui/b/cp;->aj:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 186
    invoke-virtual {p0}, Lcom/peel/ui/b/cp;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    const-class v2, Lcom/peel/ui/mo;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lcom/peel/d/e;->c(Landroid/support/v4/app/ae;Ljava/lang/String;Landroid/os/Bundle;)V

    goto :goto_0
.end method
