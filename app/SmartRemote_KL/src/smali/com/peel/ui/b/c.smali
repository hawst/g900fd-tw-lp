.class public Lcom/peel/ui/b/c;
.super Landroid/widget/BaseExpandableListAdapter;


# static fields
.field private static final a:Ljava/lang/String;

.field private static final f:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final g:Lcom/peel/util/bo;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/peel/util/bo",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/peel/content/model/OVD;",
            ">;>;"
        }
    .end annotation
.end field


# instance fields
.field private b:Landroid/content/Context;

.field private c:Landroid/view/LayoutInflater;

.field private d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/peel/content/model/Episode;",
            ">;"
        }
    .end annotation
.end field

.field private e:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 40
    const-class v0, Lcom/peel/ui/b/c;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/peel/ui/b/c;->a:Ljava/lang/String;

    .line 54
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/peel/ui/b/c;->f:Ljava/util/HashMap;

    .line 55
    new-instance v0, Lcom/peel/util/bo;

    const/16 v1, 0x100

    invoke-direct {v0, v1}, Lcom/peel/util/bo;-><init>(I)V

    sput-object v0, Lcom/peel/ui/b/c;->g:Lcom/peel/util/bo;

    .line 59
    sget-object v0, Lcom/peel/ui/b/c;->f:Ljava/util/HashMap;

    const-string/jumbo v1, "netflix"

    const-string/jumbo v2, "com.netflix.mediaclient"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 60
    sget-object v0, Lcom/peel/ui/b/c;->f:Ljava/util/HashMap;

    const-string/jumbo v1, "youtube"

    const-string/jumbo v2, "com.google.android.youtube"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 61
    sget-object v0, Lcom/peel/ui/b/c;->f:Ljava/util/HashMap;

    const-string/jumbo v1, "hulu"

    const-string/jumbo v2, "com.hulu.plus"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 63
    sget-object v0, Lcom/peel/ui/b/c;->f:Ljava/util/HashMap;

    const-string/jumbo v1, "amazon.com"

    const-string/jumbo v2, "com.amazon.avod"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 65
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 67
    invoke-direct {p0}, Landroid/widget/BaseExpandableListAdapter;-><init>()V

    .line 68
    iput-object p1, p0, Lcom/peel/ui/b/c;->b:Landroid/content/Context;

    .line 69
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/ui/b/c;->c:Landroid/view/LayoutInflater;

    .line 70
    return-void
.end method

.method static synthetic a()Lcom/peel/util/bo;
    .locals 1

    .prologue
    .line 39
    sget-object v0, Lcom/peel/ui/b/c;->g:Lcom/peel/util/bo;

    return-object v0
.end method

.method static synthetic a(Lcom/peel/ui/b/c;)Ljava/util/List;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/peel/ui/b/c;->d:Ljava/util/List;

    return-object v0
.end method

.method static synthetic a(Lcom/peel/ui/b/c;Lcom/peel/ui/b/i;Ljava/util/ArrayList;)V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0, p1, p2}, Lcom/peel/ui/b/c;->a(Lcom/peel/ui/b/i;Ljava/util/ArrayList;)V

    return-void
.end method

.method private a(Lcom/peel/ui/b/i;Ljava/util/ArrayList;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/peel/ui/b/i;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/peel/content/model/OVD;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 209
    sget-object v0, Lcom/peel/ui/b/c;->a:Ljava/lang/String;

    const-string/jumbo v1, "render episode video options"

    new-instance v2, Lcom/peel/ui/b/h;

    invoke-direct {v2, p0, p1, p2}, Lcom/peel/ui/b/h;-><init>(Lcom/peel/ui/b/c;Lcom/peel/ui/b/i;Ljava/util/ArrayList;)V

    invoke-static {v0, v1, v2}, Lcom/peel/util/i;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/lang/Runnable;

    .line 225
    return-void
.end method

.method static synthetic b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 39
    sget-object v0, Lcom/peel/ui/b/c;->a:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic b(Lcom/peel/ui/b/c;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/peel/ui/b/c;->e:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic c(Lcom/peel/ui/b/c;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/peel/ui/b/c;->b:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic c()Ljava/util/HashMap;
    .locals 1

    .prologue
    .line 39
    sget-object v0, Lcom/peel/ui/b/c;->f:Ljava/util/HashMap;

    return-object v0
.end method


# virtual methods
.method public a(Ljava/lang/String;Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/peel/content/model/Episode;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 73
    iput-object p1, p0, Lcom/peel/ui/b/c;->e:Ljava/lang/String;

    .line 74
    iput-object p2, p0, Lcom/peel/ui/b/c;->d:Ljava/util/List;

    .line 75
    return-void
.end method

.method public getChild(II)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 94
    const/4 v0, 0x0

    return-object v0
.end method

.method public getChildId(II)J
    .locals 2

    .prologue
    .line 104
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public getChildView(IIZLandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 5

    .prologue
    const/16 v4, 0x8

    .line 136
    if-nez p4, :cond_1

    new-instance v0, Lcom/peel/ui/b/i;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/peel/ui/b/i;-><init>(Lcom/peel/ui/b/d;)V

    move-object v1, v0

    .line 137
    :goto_0
    if-nez p4, :cond_0

    .line 138
    iget-object v0, p0, Lcom/peel/ui/b/c;->c:Landroid/view/LayoutInflater;

    sget v2, Lcom/peel/ui/fq;->episode_list_item:I

    const/4 v3, 0x0

    invoke-virtual {v0, v2, p5, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p4

    .line 139
    sget v0, Lcom/peel/ui/fp;->synopsis:I

    invoke-virtual {p4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lcom/peel/ui/b/i;->a:Landroid/widget/TextView;

    .line 140
    sget v0, Lcom/peel/ui/fp;->vod_selection_title:I

    invoke-virtual {p4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lcom/peel/ui/b/i;->b:Landroid/widget/TextView;

    .line 141
    sget v0, Lcom/peel/ui/fp;->vod_selection_grid:I

    invoke-virtual {p4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/peel/ui/AutoHeightGridView;

    iput-object v0, v1, Lcom/peel/ui/b/i;->d:Lcom/peel/ui/AutoHeightGridView;

    .line 142
    sget v0, Lcom/peel/ui/fp;->more_txt:I

    invoke-virtual {p4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lcom/peel/ui/b/i;->c:Landroid/widget/TextView;

    .line 144
    invoke-virtual {p4, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 147
    :cond_0
    iget-object v0, v1, Lcom/peel/ui/b/i;->c:Landroid/widget/TextView;

    iget-object v2, v1, Lcom/peel/ui/b/i;->c:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getPaintFlags()I

    move-result v2

    or-int/lit8 v2, v2, 0x8

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setPaintFlags(I)V

    .line 148
    iget-object v0, v1, Lcom/peel/ui/b/i;->c:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 149
    iget-object v0, v1, Lcom/peel/ui/b/i;->c:Landroid/widget/TextView;

    new-instance v2, Lcom/peel/ui/b/d;

    invoke-direct {v2, p0, v1, p1}, Lcom/peel/ui/b/d;-><init>(Lcom/peel/ui/b/c;Lcom/peel/ui/b/i;I)V

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 157
    iget-object v0, v1, Lcom/peel/ui/b/i;->b:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 158
    iget-object v2, v1, Lcom/peel/ui/b/i;->a:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/peel/ui/b/c;->d:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/content/model/Episode;

    invoke-virtual {v0}, Lcom/peel/content/model/Episode;->d()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 160
    sget-object v2, Lcom/peel/ui/b/c;->g:Lcom/peel/util/bo;

    iget-object v0, p0, Lcom/peel/ui/b/c;->d:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/content/model/Episode;

    invoke-virtual {v0}, Lcom/peel/content/model/Episode;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/peel/util/bo;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 162
    if-eqz v0, :cond_2

    .line 163
    invoke-direct {p0, v1, v0}, Lcom/peel/ui/b/c;->a(Lcom/peel/ui/b/i;Ljava/util/ArrayList;)V

    .line 189
    :goto_1
    iget-object v0, v1, Lcom/peel/ui/b/i;->a:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    .line 190
    new-instance v2, Lcom/peel/ui/b/g;

    invoke-direct {v2, p0, v1}, Lcom/peel/ui/b/g;-><init>(Lcom/peel/ui/b/c;Lcom/peel/ui/b/i;)V

    invoke-virtual {v0, v2}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 205
    return-object p4

    .line 136
    :cond_1
    invoke-virtual {p4}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/ui/b/i;

    move-object v1, v0

    goto/16 :goto_0

    .line 165
    :cond_2
    sget-object v0, Lcom/peel/ui/b/c;->a:Ljava/lang/String;

    const-string/jumbo v2, "get on Demand Video options of the episode"

    new-instance v3, Lcom/peel/ui/b/e;

    invoke-direct {v3, p0, p1, v1}, Lcom/peel/ui/b/e;-><init>(Lcom/peel/ui/b/c;ILcom/peel/ui/b/i;)V

    invoke-static {v0, v2, v3}, Lcom/peel/util/i;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    goto :goto_1
.end method

.method public getChildrenCount(I)I
    .locals 1

    .prologue
    .line 84
    const/4 v0, 0x1

    return v0
.end method

.method public getGroup(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 89
    const/4 v0, 0x0

    return-object v0
.end method

.method public getGroupCount()I
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/peel/ui/b/c;->d:Ljava/util/List;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/peel/ui/b/c;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    goto :goto_0
.end method

.method public getGroupId(I)J
    .locals 2

    .prologue
    .line 99
    int-to-long v0, p1

    return-wide v0
.end method

.method public getGroupView(IZLandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4

    .prologue
    .line 115
    if-nez p3, :cond_1

    new-instance v0, Lcom/peel/ui/b/j;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/peel/ui/b/j;-><init>(Lcom/peel/ui/b/d;)V

    move-object v1, v0

    .line 117
    :goto_0
    if-nez p3, :cond_0

    .line 118
    iget-object v0, p0, Lcom/peel/ui/b/c;->c:Landroid/view/LayoutInflater;

    sget v2, Lcom/peel/ui/fq;->show_details_ondemand_listitem:I

    const/4 v3, 0x0

    invoke-virtual {v0, v2, p4, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p3

    .line 119
    sget v0, Lcom/peel/ui/fp;->episode_number:I

    invoke-virtual {p3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lcom/peel/ui/b/j;->a:Landroid/widget/TextView;

    .line 120
    sget v0, Lcom/peel/ui/fp;->episode_title:I

    invoke-virtual {p3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lcom/peel/ui/b/j;->b:Landroid/widget/TextView;

    .line 121
    sget v0, Lcom/peel/ui/fp;->group_indicator:I

    invoke-virtual {p3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, v1, Lcom/peel/ui/b/j;->c:Landroid/widget/ImageView;

    .line 123
    invoke-virtual {p3, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 126
    :cond_0
    iget-object v0, p0, Lcom/peel/ui/b/c;->d:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/content/model/Episode;

    .line 128
    iget-object v2, v1, Lcom/peel/ui/b/j;->a:Landroid/widget/TextView;

    invoke-virtual {v0}, Lcom/peel/content/model/Episode;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 129
    iget-object v1, v1, Lcom/peel/ui/b/j;->b:Landroid/widget/TextView;

    invoke-virtual {v0}, Lcom/peel/content/model/Episode;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 131
    return-object p3

    .line 115
    :cond_1
    invoke-virtual {p3}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/ui/b/j;

    move-object v1, v0

    goto :goto_0
.end method

.method public hasStableIds()Z
    .locals 1

    .prologue
    .line 109
    const/4 v0, 0x0

    return v0
.end method

.method public isChildSelectable(II)Z
    .locals 1

    .prologue
    .line 229
    const/4 v0, 0x0

    return v0
.end method
