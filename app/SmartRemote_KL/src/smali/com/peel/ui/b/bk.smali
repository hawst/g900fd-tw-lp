.class Lcom/peel/ui/b/bk;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/peel/content/model/OVDWrapper;

.field final synthetic b:Landroid/os/Bundle;

.field final synthetic c:Lcom/peel/ui/b/bj;


# direct methods
.method constructor <init>(Lcom/peel/ui/b/bj;Lcom/peel/content/model/OVDWrapper;Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 576
    iput-object p1, p0, Lcom/peel/ui/b/bk;->c:Lcom/peel/ui/b/bj;

    iput-object p2, p0, Lcom/peel/ui/b/bk;->a:Lcom/peel/content/model/OVDWrapper;

    iput-object p3, p0, Lcom/peel/ui/b/bk;->b:Landroid/os/Bundle;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 579
    iget-object v0, p0, Lcom/peel/ui/b/bk;->a:Lcom/peel/content/model/OVDWrapper;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/peel/ui/b/bk;->a:Lcom/peel/content/model/OVDWrapper;

    invoke-virtual {v0}, Lcom/peel/content/model/OVDWrapper;->b()Ljava/util/Map;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/peel/ui/b/bk;->a:Lcom/peel/content/model/OVDWrapper;

    .line 580
    invoke-virtual {v0}, Lcom/peel/content/model/OVDWrapper;->b()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 581
    iget-object v0, p0, Lcom/peel/ui/b/bk;->a:Lcom/peel/content/model/OVDWrapper;

    invoke-virtual {v0}, Lcom/peel/content/model/OVDWrapper;->b()Ljava/util/Map;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/ui/b/bk;->b:Landroid/os/Bundle;

    const-string/jumbo v2, "selectedSeason"

    .line 582
    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 581
    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/content/model/Season;

    .line 583
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 584
    const-string/jumbo v2, "showId"

    iget-object v3, p0, Lcom/peel/ui/b/bk;->c:Lcom/peel/ui/b/bj;

    iget-object v3, v3, Lcom/peel/ui/b/bj;->a:Lcom/peel/ui/b/av;

    invoke-static {v3}, Lcom/peel/ui/b/av;->a(Lcom/peel/ui/b/av;)Lcom/peel/content/listing/LiveListing;

    move-result-object v3

    invoke-virtual {v3}, Lcom/peel/content/listing/LiveListing;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 585
    const-string/jumbo v2, "season"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 586
    iget-object v0, p0, Lcom/peel/ui/b/bk;->c:Lcom/peel/ui/b/bj;

    iget-object v0, v0, Lcom/peel/ui/b/bj;->a:Lcom/peel/ui/b/av;

    invoke-static {v0}, Lcom/peel/ui/b/av;->g(Lcom/peel/ui/b/av;)Lcom/peel/ui/b/q;

    move-result-object v0

    new-instance v2, Lcom/peel/ui/b/bz;

    iget-object v3, p0, Lcom/peel/ui/b/bk;->b:Landroid/os/Bundle;

    const-string/jumbo v4, "selectedSeason"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/peel/ui/b/bk;->c:Lcom/peel/ui/b/bj;

    iget-object v4, v4, Lcom/peel/ui/b/bj;->a:Lcom/peel/ui/b/av;

    invoke-static {v4}, Lcom/peel/ui/b/av;->h(Lcom/peel/ui/b/av;)I

    move-result v4

    invoke-direct {v2, v1, v3, v4}, Lcom/peel/ui/b/bz;-><init>(Landroid/os/Bundle;Ljava/lang/String;I)V

    invoke-virtual {v0, v2}, Lcom/peel/ui/b/q;->a(Lcom/peel/ui/b/af;)V

    .line 587
    iget-object v0, p0, Lcom/peel/ui/b/bk;->c:Lcom/peel/ui/b/bj;

    iget-object v0, v0, Lcom/peel/ui/b/bj;->a:Lcom/peel/ui/b/av;

    invoke-static {v0}, Lcom/peel/ui/b/av;->g(Lcom/peel/ui/b/av;)Lcom/peel/ui/b/q;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/ui/b/q;->notifyDataSetChanged()V

    .line 589
    :cond_0
    return-void
.end method
