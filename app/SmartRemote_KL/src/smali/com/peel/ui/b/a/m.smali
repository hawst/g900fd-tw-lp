.class Lcom/peel/ui/b/a/m;
.super Lcom/peel/util/t;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/peel/util/t",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/peel/ui/b/a/l;


# direct methods
.method constructor <init>(Lcom/peel/ui/b/a/l;)V
    .locals 0

    .prologue
    .line 167
    iput-object p1, p0, Lcom/peel/ui/b/a/m;->a:Lcom/peel/ui/b/a/l;

    invoke-direct {p0}, Lcom/peel/util/t;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(ZLjava/lang/Object;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 167
    check-cast p2, Ljava/lang/String;

    invoke-virtual {p0, p1, p2, p3}, Lcom/peel/ui/b/a/m;->a(ZLjava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public a(ZLjava/lang/String;Ljava/lang/String;)V
    .locals 16

    .prologue
    .line 170
    if-eqz p1, :cond_6

    if-eqz p2, :cond_6

    .line 171
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/peel/ui/b/a/m;->a:Lcom/peel/ui/b/a/l;

    iget-object v2, v2, Lcom/peel/ui/b/a/l;->c:Lcom/peel/ui/b/a/a;

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    invoke-static {v2, v3}, Lcom/peel/ui/b/a/a;->a(Lcom/peel/ui/b/a/a;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    .line 173
    :try_start_0
    new-instance v5, Lorg/json/JSONArray;

    move-object/from16 v0, p2

    invoke-direct {v5, v0}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 175
    invoke-virtual {v5}, Lorg/json/JSONArray;->length()I

    move-result v2

    if-lez v2, :cond_6

    .line 176
    const/4 v2, 0x0

    move v4, v2

    :goto_0
    invoke-virtual {v5}, Lorg/json/JSONArray;->length()I

    move-result v2

    if-ge v4, v2, :cond_6

    .line 177
    invoke-virtual {v5, v4}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v2

    .line 178
    new-instance v6, Lcom/peel/ui/model/TwittData;

    invoke-direct {v6}, Lcom/peel/ui/model/TwittData;-><init>()V

    .line 179
    const-string/jumbo v3, "id"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v6, v3}, Lcom/peel/ui/model/TwittData;->d(Ljava/lang/String;)V

    .line 181
    const-string/jumbo v3, "senderEntity"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v3

    .line 182
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/peel/ui/b/a/m;->a:Lcom/peel/ui/b/a/l;

    iget-wide v8, v7, Lcom/peel/ui/b/a/l;->b:J

    const-string/jumbo v7, "date"

    invoke-virtual {v2, v7}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v10

    sub-long/2addr v8, v10

    long-to-int v7, v8

    div-int/lit16 v7, v7, 0x3e8

    invoke-virtual {v6, v7}, Lcom/peel/ui/model/TwittData;->a(I)V

    .line 184
    const-string/jumbo v7, "entityName"

    invoke-virtual {v3, v7}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/peel/ui/model/TwittData;->b(Ljava/lang/String;)V

    .line 185
    const-string/jumbo v7, "displayName"

    invoke-virtual {v3, v7}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/peel/ui/model/TwittData;->f(Ljava/lang/String;)V

    .line 187
    const-string/jumbo v7, "picUrl"

    invoke-virtual {v3, v7}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 188
    const-string/jumbo v7, "picUrl"

    invoke-virtual {v3, v7}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v6, v3}, Lcom/peel/ui/model/TwittData;->e(Ljava/lang/String;)V

    .line 191
    :cond_0
    const-string/jumbo v3, "message"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 192
    const-string/jumbo v3, "message"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v6, v3}, Lcom/peel/ui/model/TwittData;->a(Ljava/lang/String;)V

    .line 195
    :cond_1
    const-string/jumbo v3, "entities"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v7

    .line 197
    if-eqz v7, :cond_c

    invoke-virtual {v7}, Lorg/json/JSONArray;->length()I

    move-result v2

    if-lez v2, :cond_c

    .line 198
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 199
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 200
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    .line 202
    const/4 v2, 0x0

    move v3, v2

    :goto_1
    invoke-virtual {v7}, Lorg/json/JSONArray;->length()I

    move-result v2

    if-ge v3, v2, :cond_9

    .line 203
    invoke-virtual {v7, v3}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v11

    .line 204
    const-string/jumbo v2, "systems"

    invoke-virtual {v11, v2}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v2

    .line 206
    if-eqz v2, :cond_3

    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result v12

    if-lez v12, :cond_3

    const/4 v12, 0x0

    invoke-virtual {v2, v12}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v12, "twitter"

    invoke-virtual {v2, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 207
    const-string/jumbo v2, "type"

    invoke-virtual {v11, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    .line 209
    const-string/jumbo v2, "indices"

    invoke-virtual {v11, v2}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 210
    const-string/jumbo v2, "indices"

    invoke-virtual {v11, v2}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v13

    .line 211
    const-string/jumbo v2, "entityName"

    invoke-virtual {v11, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 212
    const-string/jumbo v14, "PHOTO"

    invoke-virtual {v12, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_4

    const-string/jumbo v14, "photo:"

    invoke-virtual {v2, v14}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v14

    if-eqz v14, :cond_4

    .line 213
    const/4 v11, 0x6

    invoke-virtual {v2, v11}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    .line 218
    :cond_2
    :goto_2
    new-instance v11, Lcom/peel/ui/model/TwittData$TwittEntries;

    invoke-virtual {v6}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v14, 0x0

    invoke-virtual {v13, v14}, Lorg/json/JSONArray;->getInt(I)I

    move-result v14

    const/4 v15, 0x1

    invoke-virtual {v13, v15}, Lorg/json/JSONArray;->getInt(I)I

    move-result v13

    invoke-direct {v11, v6, v14, v13, v2}, Lcom/peel/ui/model/TwittData$TwittEntries;-><init>(Lcom/peel/ui/model/TwittData;IILjava/lang/String;)V

    .line 220
    const-string/jumbo v2, "URL"

    invoke-virtual {v12, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 221
    invoke-virtual {v8, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 202
    :cond_3
    :goto_3
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_1

    .line 214
    :cond_4
    const-string/jumbo v14, "URL"

    invoke-virtual {v12, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_2

    const-string/jumbo v14, "url:"

    invoke-virtual {v2, v14}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v14

    if-eqz v14, :cond_2

    .line 215
    const-string/jumbo v2, "url"

    invoke-virtual {v11, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    goto :goto_2

    .line 222
    :cond_5
    const-string/jumbo v2, "HASHTAG"

    invoke-virtual {v12, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 223
    invoke-virtual {v9, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_3

    .line 247
    :catch_0
    move-exception v2

    .line 248
    invoke-static {}, Lcom/peel/ui/b/a/a;->h()Ljava/lang/String;

    move-result-object v3

    invoke-static {}, Lcom/peel/ui/b/a/a;->h()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 251
    :cond_6
    const-class v2, Lcom/peel/ui/b/a/a;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "final"

    new-instance v4, Lcom/peel/ui/b/a/n;

    move-object/from16 v0, p0

    invoke-direct {v4, v0}, Lcom/peel/ui/b/a/n;-><init>(Lcom/peel/ui/b/a/m;)V

    invoke-static {v2, v3, v4}, Lcom/peel/util/i;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/lang/Runnable;

    .line 260
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/peel/ui/b/a/m;->a:Lcom/peel/ui/b/a/l;

    iget-object v2, v2, Lcom/peel/ui/b/a/l;->c:Lcom/peel/ui/b/a/a;

    invoke-static {v2}, Lcom/peel/ui/b/a/a;->c(Lcom/peel/ui/b/a/a;)Lcom/peel/util/t;

    move-result-object v2

    if-eqz v2, :cond_7

    .line 261
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/peel/ui/b/a/m;->a:Lcom/peel/ui/b/a/l;

    iget-object v2, v2, Lcom/peel/ui/b/a/l;->c:Lcom/peel/ui/b/a/a;

    invoke-static {v2}, Lcom/peel/ui/b/a/a;->c(Lcom/peel/ui/b/a/a;)Lcom/peel/util/t;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/peel/ui/b/a/m;->a:Lcom/peel/ui/b/a/l;

    iget-object v3, v3, Lcom/peel/ui/b/a/l;->c:Lcom/peel/ui/b/a/a;

    invoke-static {v3}, Lcom/peel/ui/b/a/a;->a(Lcom/peel/ui/b/a/a;)Ljava/util/ArrayList;

    move-result-object v3

    move/from16 v0, p1

    move-object/from16 v1, p3

    invoke-virtual {v2, v0, v3, v1}, Lcom/peel/util/t;->a(ZLjava/lang/Object;Ljava/lang/String;)V

    .line 263
    :cond_7
    return-void

    .line 224
    :cond_8
    :try_start_1
    const-string/jumbo v2, "PHOTO"

    invoke-virtual {v12, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 225
    invoke-virtual {v10, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 231
    :cond_9
    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_a

    .line 232
    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v2

    new-array v2, v2, [Lcom/peel/ui/model/TwittData$TwittEntries;

    invoke-virtual {v10, v2}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Lcom/peel/ui/model/TwittData$TwittEntries;

    invoke-virtual {v6, v2}, Lcom/peel/ui/model/TwittData;->c([Lcom/peel/ui/model/TwittData$TwittEntries;)V

    .line 235
    :cond_a
    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_b

    .line 236
    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v2

    new-array v2, v2, [Lcom/peel/ui/model/TwittData$TwittEntries;

    invoke-virtual {v8, v2}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Lcom/peel/ui/model/TwittData$TwittEntries;

    invoke-virtual {v6, v2}, Lcom/peel/ui/model/TwittData;->b([Lcom/peel/ui/model/TwittData$TwittEntries;)V

    .line 239
    :cond_b
    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_c

    .line 240
    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v2

    new-array v2, v2, [Lcom/peel/ui/model/TwittData$TwittEntries;

    invoke-virtual {v9, v2}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Lcom/peel/ui/model/TwittData$TwittEntries;

    invoke-virtual {v6, v2}, Lcom/peel/ui/model/TwittData;->a([Lcom/peel/ui/model/TwittData$TwittEntries;)V

    .line 244
    :cond_c
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/peel/ui/b/a/m;->a:Lcom/peel/ui/b/a/l;

    iget-object v2, v2, Lcom/peel/ui/b/a/l;->c:Lcom/peel/ui/b/a/a;

    invoke-static {v2}, Lcom/peel/ui/b/a/a;->a(Lcom/peel/ui/b/a/a;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    .line 176
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto/16 :goto_0
.end method
