.class Lcom/peel/ui/b/cs;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/peel/ui/b/cr;


# direct methods
.method constructor <init>(Lcom/peel/ui/b/cr;)V
    .locals 0

    .prologue
    .line 106
    iput-object p1, p0, Lcom/peel/ui/b/cs;->a:Lcom/peel/ui/b/cr;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .prologue
    .line 109
    iget-object v0, p0, Lcom/peel/ui/b/cs;->a:Lcom/peel/ui/b/cr;

    iget-object v0, v0, Lcom/peel/ui/b/cr;->a:Lcom/peel/ui/b/cp;

    invoke-static {v0}, Lcom/peel/ui/b/cp;->d(Lcom/peel/ui/b/cp;)Lcom/peel/ui/mx;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 110
    iget-object v0, p0, Lcom/peel/ui/b/cs;->a:Lcom/peel/ui/b/cr;

    iget-object v0, v0, Lcom/peel/ui/b/cr;->a:Lcom/peel/ui/b/cp;

    invoke-static {v0}, Lcom/peel/ui/b/cp;->d(Lcom/peel/ui/b/cp;)Lcom/peel/ui/mx;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/ui/b/cs;->a:Lcom/peel/ui/b/cr;

    iget-object v1, v1, Lcom/peel/ui/b/cr;->a:Lcom/peel/ui/b/cp;

    invoke-static {v1}, Lcom/peel/ui/b/cp;->e(Lcom/peel/ui/b/cp;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/peel/ui/mx;->a(Ljava/util/List;)V

    .line 111
    iget-object v0, p0, Lcom/peel/ui/b/cs;->a:Lcom/peel/ui/b/cr;

    iget-object v0, v0, Lcom/peel/ui/b/cr;->a:Lcom/peel/ui/b/cp;

    invoke-static {v0}, Lcom/peel/ui/b/cp;->d(Lcom/peel/ui/b/cp;)Lcom/peel/ui/mx;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/ui/mx;->notifyDataSetChanged()V

    .line 118
    :goto_0
    iget-object v0, p0, Lcom/peel/ui/b/cs;->a:Lcom/peel/ui/b/cr;

    iget-object v0, v0, Lcom/peel/ui/b/cr;->a:Lcom/peel/ui/b/cp;

    invoke-static {v0}, Lcom/peel/ui/b/cp;->a(Lcom/peel/ui/b/cp;)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 119
    iget-object v0, p0, Lcom/peel/ui/b/cs;->a:Lcom/peel/ui/b/cr;

    iget-object v0, v0, Lcom/peel/ui/b/cr;->a:Lcom/peel/ui/b/cp;

    invoke-static {v0}, Lcom/peel/ui/b/cp;->a(Lcom/peel/ui/b/cp;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->clearAnimation()V

    .line 121
    :cond_0
    return-void

    .line 113
    :cond_1
    iget-object v0, p0, Lcom/peel/ui/b/cs;->a:Lcom/peel/ui/b/cr;

    iget-object v6, v0, Lcom/peel/ui/b/cr;->a:Lcom/peel/ui/b/cp;

    new-instance v0, Lcom/peel/ui/mx;

    iget-object v1, p0, Lcom/peel/ui/b/cs;->a:Lcom/peel/ui/b/cr;

    iget-object v1, v1, Lcom/peel/ui/b/cr;->a:Lcom/peel/ui/b/cp;

    invoke-virtual {v1}, Lcom/peel/ui/b/cp;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    iget-object v2, p0, Lcom/peel/ui/b/cs;->a:Lcom/peel/ui/b/cr;

    iget-object v2, v2, Lcom/peel/ui/b/cr;->a:Lcom/peel/ui/b/cp;

    invoke-static {v2}, Lcom/peel/ui/b/cp;->f(Lcom/peel/ui/b/cp;)I

    move-result v2

    iget-object v3, p0, Lcom/peel/ui/b/cs;->a:Lcom/peel/ui/b/cr;

    iget-object v3, v3, Lcom/peel/ui/b/cr;->a:Lcom/peel/ui/b/cp;

    invoke-static {v3}, Lcom/peel/ui/b/cp;->b(Lcom/peel/ui/b/cp;)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/peel/ui/b/cs;->a:Lcom/peel/ui/b/cr;

    iget-object v4, v4, Lcom/peel/ui/b/cr;->a:Lcom/peel/ui/b/cp;

    invoke-static {v4}, Lcom/peel/ui/b/cp;->g(Lcom/peel/ui/b/cp;)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/peel/ui/b/cs;->a:Lcom/peel/ui/b/cr;

    iget-object v5, v5, Lcom/peel/ui/b/cr;->a:Lcom/peel/ui/b/cp;

    invoke-static {v5}, Lcom/peel/ui/b/cp;->h(Lcom/peel/ui/b/cp;)[Ljava/lang/String;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, Lcom/peel/ui/mx;-><init>(Landroid/support/v4/app/ae;ILjava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V

    invoke-static {v6, v0}, Lcom/peel/ui/b/cp;->a(Lcom/peel/ui/b/cp;Lcom/peel/ui/mx;)Lcom/peel/ui/mx;

    .line 114
    iget-object v0, p0, Lcom/peel/ui/b/cs;->a:Lcom/peel/ui/b/cr;

    iget-object v0, v0, Lcom/peel/ui/b/cr;->a:Lcom/peel/ui/b/cp;

    invoke-static {v0}, Lcom/peel/ui/b/cp;->d(Lcom/peel/ui/b/cp;)Lcom/peel/ui/mx;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/ui/b/cs;->a:Lcom/peel/ui/b/cr;

    iget-object v1, v1, Lcom/peel/ui/b/cr;->a:Lcom/peel/ui/b/cp;

    invoke-static {v1}, Lcom/peel/ui/b/cp;->e(Lcom/peel/ui/b/cp;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/peel/ui/mx;->a(Ljava/util/List;)V

    .line 115
    iget-object v0, p0, Lcom/peel/ui/b/cs;->a:Lcom/peel/ui/b/cr;

    iget-object v0, v0, Lcom/peel/ui/b/cr;->a:Lcom/peel/ui/b/cp;

    invoke-static {v0}, Lcom/peel/ui/b/cp;->i(Lcom/peel/ui/b/cp;)Landroid/widget/ListView;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/ui/b/cs;->a:Lcom/peel/ui/b/cr;

    iget-object v1, v1, Lcom/peel/ui/b/cr;->a:Lcom/peel/ui/b/cp;

    invoke-static {v1}, Lcom/peel/ui/b/cp;->d(Lcom/peel/ui/b/cp;)Lcom/peel/ui/mx;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    goto :goto_0
.end method
