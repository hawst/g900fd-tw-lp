.class Lcom/peel/ui/b/bs;
.super Landroid/widget/ArrayAdapter;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/peel/ui/b/bq;


# direct methods
.method constructor <init>(Lcom/peel/ui/b/bq;Landroid/content/Context;ILjava/util/List;)V
    .locals 0

    .prologue
    .line 90
    iput-object p1, p0, Lcom/peel/ui/b/bs;->a:Lcom/peel/ui/b/bq;

    invoke-direct {p0, p2, p3, p4}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    return-void
.end method


# virtual methods
.method public getDropDownView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 93
    if-eqz p2, :cond_0

    move-object v0, p2

    :goto_0
    check-cast v0, Landroid/widget/TextView;

    check-cast v0, Landroid/widget/TextView;

    .line 94
    iget-object v1, p0, Lcom/peel/ui/b/bs;->a:Lcom/peel/ui/b/bq;

    sget v2, Lcom/peel/ui/ft;->season_name:I

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {p0, p1}, Lcom/peel/ui/b/bs;->getItem(I)Ljava/lang/Object;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-virtual {v1, v2, v3}, Lcom/peel/ui/b/bq;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 95
    return-object v0

    .line 93
    :cond_0
    iget-object v0, p0, Lcom/peel/ui/b/bs;->a:Lcom/peel/ui/b/bq;

    invoke-static {v0}, Lcom/peel/ui/b/bq;->a(Lcom/peel/ui/b/bq;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lcom/peel/ui/fq;->show_details_ondemand_spinneritem:I

    invoke-virtual {v0, v1, p3, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    goto :goto_0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 100
    if-eqz p2, :cond_0

    move-object v0, p2

    :goto_0
    check-cast v0, Landroid/widget/TextView;

    check-cast v0, Landroid/widget/TextView;

    .line 101
    const v1, 0x106000d

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setBackgroundResource(I)V

    .line 102
    iget-object v1, p0, Lcom/peel/ui/b/bs;->a:Lcom/peel/ui/b/bq;

    sget v2, Lcom/peel/ui/ft;->season_name:I

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {p0, p1}, Lcom/peel/ui/b/bs;->getItem(I)Ljava/lang/Object;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-virtual {v1, v2, v3}, Lcom/peel/ui/b/bq;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 103
    return-object v0

    .line 100
    :cond_0
    iget-object v0, p0, Lcom/peel/ui/b/bs;->a:Lcom/peel/ui/b/bq;

    invoke-static {v0}, Lcom/peel/ui/b/bq;->a(Lcom/peel/ui/b/bq;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lcom/peel/ui/fq;->show_details_ondemand_spinneritem:I

    invoke-virtual {v0, v1, p3, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    goto :goto_0
.end method
