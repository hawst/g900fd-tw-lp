.class Lcom/peel/ui/b/cb;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:I

.field final synthetic b:Lcom/peel/ui/b/bz;


# direct methods
.method constructor <init>(Lcom/peel/ui/b/bz;I)V
    .locals 0

    .prologue
    .line 84
    iput-object p1, p0, Lcom/peel/ui/b/cb;->b:Lcom/peel/ui/b/bz;

    iput p2, p0, Lcom/peel/ui/b/cb;->a:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 87
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 88
    const-string/jumbo v1, "showId"

    iget-object v2, p0, Lcom/peel/ui/b/cb;->b:Lcom/peel/ui/b/bz;

    invoke-static {v2}, Lcom/peel/ui/b/bz;->a(Lcom/peel/ui/b/bz;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 89
    const-string/jumbo v1, "seasons"

    iget-object v2, p0, Lcom/peel/ui/b/cb;->b:Lcom/peel/ui/b/bz;

    invoke-static {v2}, Lcom/peel/ui/b/bz;->b(Lcom/peel/ui/b/bz;)Landroid/os/Bundle;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 90
    const-string/jumbo v1, "selectedSeason"

    iget-object v2, p0, Lcom/peel/ui/b/cb;->b:Lcom/peel/ui/b/bz;

    invoke-static {v2}, Lcom/peel/ui/b/bz;->c(Lcom/peel/ui/b/bz;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 91
    const-string/jumbo v1, "contextId"

    iget-object v2, p0, Lcom/peel/ui/b/cb;->b:Lcom/peel/ui/b/bz;

    invoke-static {v2}, Lcom/peel/ui/b/bz;->d(Lcom/peel/ui/b/bz;)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 92
    const-string/jumbo v1, "selected_episode"

    iget v2, p0, Lcom/peel/ui/b/cb;->a:I

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 93
    iget-object v1, p0, Lcom/peel/ui/b/cb;->b:Lcom/peel/ui/b/bz;

    iget-object v1, v1, Lcom/peel/ui/b/bz;->a:Lcom/peel/d/i;

    invoke-virtual {v1}, Lcom/peel/d/i;->c()Landroid/support/v4/app/ae;

    move-result-object v1

    const-class v2, Lcom/peel/ui/b/bq;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lcom/peel/d/e;->c(Landroid/support/v4/app/ae;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 94
    return-void
.end method
