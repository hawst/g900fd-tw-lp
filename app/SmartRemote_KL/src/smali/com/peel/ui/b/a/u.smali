.class Lcom/peel/ui/b/a/u;
.super Lcom/peel/util/t;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/peel/util/t",
        "<",
        "Lcom/peel/content/model/OVDWrapper;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/peel/ui/b/a/t;


# direct methods
.method constructor <init>(Lcom/peel/ui/b/a/t;)V
    .locals 0

    .prologue
    .line 389
    iput-object p1, p0, Lcom/peel/ui/b/a/u;->a:Lcom/peel/ui/b/a/t;

    invoke-direct {p0}, Lcom/peel/util/t;-><init>()V

    return-void
.end method


# virtual methods
.method public a(ZLcom/peel/content/model/OVDWrapper;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 392
    if-eqz p1, :cond_0

    .line 393
    iget-object v0, p0, Lcom/peel/ui/b/a/u;->a:Lcom/peel/ui/b/a/t;

    iget-object v0, v0, Lcom/peel/ui/b/a/t;->c:Lcom/peel/ui/b/a/a;

    invoke-static {v0}, Lcom/peel/ui/b/a/a;->d(Lcom/peel/ui/b/a/a;)Lcom/peel/util/t;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 394
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 395
    const-string/jumbo v1, "showId"

    iget-object v2, p0, Lcom/peel/ui/b/a/u;->a:Lcom/peel/ui/b/a/t;

    iget-object v2, v2, Lcom/peel/ui/b/a/t;->c:Lcom/peel/ui/b/a/a;

    .line 396
    invoke-virtual {v2}, Lcom/peel/ui/b/a/a;->b()Lcom/peel/content/listing/LiveListing;

    move-result-object v2

    invoke-virtual {v2}, Lcom/peel/content/listing/LiveListing;->b()Ljava/lang/String;

    move-result-object v2

    .line 395
    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 397
    const-string/jumbo v1, "selectedSeason"

    iget-object v2, p0, Lcom/peel/ui/b/a/u;->a:Lcom/peel/ui/b/a/t;

    iget-object v2, v2, Lcom/peel/ui/b/a/t;->b:Lcom/peel/content/model/Season;

    .line 398
    invoke-virtual {v2}, Lcom/peel/content/model/Season;->a()Ljava/lang/String;

    move-result-object v2

    .line 397
    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 400
    iget-object v1, p0, Lcom/peel/ui/b/a/u;->a:Lcom/peel/ui/b/a/t;

    iget-object v1, v1, Lcom/peel/ui/b/a/t;->c:Lcom/peel/ui/b/a/a;

    invoke-static {v1}, Lcom/peel/ui/b/a/a;->d(Lcom/peel/ui/b/a/a;)Lcom/peel/util/t;

    move-result-object v1

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v0, v3}, Lcom/peel/util/t;->a(ZLjava/lang/Object;Ljava/lang/String;)V

    .line 403
    :cond_0
    return-void
.end method

.method public bridge synthetic a(ZLjava/lang/Object;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 389
    check-cast p2, Lcom/peel/content/model/OVDWrapper;

    invoke-virtual {p0, p1, p2, p3}, Lcom/peel/ui/b/a/u;->a(ZLcom/peel/content/model/OVDWrapper;Ljava/lang/String;)V

    return-void
.end method
