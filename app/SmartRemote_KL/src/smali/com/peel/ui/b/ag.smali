.class public Lcom/peel/ui/b/ag;
.super Lcom/peel/d/u;


# static fields
.field private static av:Lcom/peel/d/a;

.field private static final e:Ljava/lang/String;

.field private static g:I


# instance fields
.field private aj:Landroid/view/View;

.field private ak:Lcom/peel/ui/ScrollPositionListView;

.field private al:Landroid/widget/ImageView;

.field private am:Landroid/view/View;

.field private an:Lcom/peel/ui/b/q;

.field private ao:J

.field private ap:Lcom/peel/ui/b/a/a;

.field private aq:Landroid/widget/TextView;

.field private ar:Landroid/widget/TextView;

.field private as:Landroid/widget/TextView;

.field private at:Landroid/content/BroadcastReceiver;

.field private au:Lcom/peel/util/t;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/peel/util/t",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/peel/ui/model/TwittData;",
            ">;>;"
        }
    .end annotation
.end field

.field private f:I

.field private h:I

.field private i:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 55
    const-class v0, Lcom/peel/ui/b/ag;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/peel/ui/b/ag;->e:Ljava/lang/String;

    .line 59
    const/4 v0, 0x0

    sput v0, Lcom/peel/ui/b/ag;->g:I

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 54
    invoke-direct {p0}, Lcom/peel/d/u;-><init>()V

    .line 58
    const/4 v0, 0x0

    iput v0, p0, Lcom/peel/ui/b/ag;->f:I

    .line 66
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/peel/ui/b/ag;->ao:J

    .line 181
    new-instance v0, Lcom/peel/ui/b/ah;

    invoke-direct {v0, p0}, Lcom/peel/ui/b/ah;-><init>(Lcom/peel/ui/b/ag;)V

    iput-object v0, p0, Lcom/peel/ui/b/ag;->at:Landroid/content/BroadcastReceiver;

    .line 560
    new-instance v0, Lcom/peel/ui/b/at;

    invoke-direct {v0, p0}, Lcom/peel/ui/b/at;-><init>(Lcom/peel/ui/b/ag;)V

    iput-object v0, p0, Lcom/peel/ui/b/ag;->au:Lcom/peel/util/t;

    return-void
.end method

.method static synthetic S()Ljava/lang/String;
    .locals 1

    .prologue
    .line 54
    sget-object v0, Lcom/peel/ui/b/ag;->e:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic T()I
    .locals 1

    .prologue
    .line 54
    sget v0, Lcom/peel/ui/b/ag;->g:I

    return v0
.end method

.method private U()V
    .locals 10

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 153
    iget-object v0, p0, Lcom/peel/ui/b/ag;->ap:Lcom/peel/ui/b/a/a;

    invoke-virtual {v0}, Lcom/peel/ui/b/a/a;->b()Lcom/peel/content/listing/LiveListing;

    move-result-object v3

    .line 156
    invoke-virtual {v3}, Lcom/peel/content/listing/LiveListing;->o()J

    move-result-wide v4

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    const-wide/32 v8, 0x1b7740

    add-long/2addr v6, v8

    cmp-long v0, v4, v6

    if-lez v0, :cond_5

    move v0, v1

    .line 161
    :goto_0
    sget-object v4, Lcom/peel/content/a;->e:Ljava/lang/String;

    const-string/jumbo v5, "CN"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-static {v3}, Lcom/peel/util/bx;->d(Lcom/peel/content/listing/Listing;)Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_1

    :cond_0
    sget-object v4, Lcom/peel/content/a;->e:Ljava/lang/String;

    const-string/jumbo v5, "CN"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_2

    invoke-static {v3}, Lcom/peel/util/bx;->e(Lcom/peel/content/listing/Listing;)Z

    move-result v3

    if-eqz v3, :cond_2

    :cond_1
    move v3, v1

    .line 163
    :goto_1
    if-eqz v3, :cond_3

    .line 164
    iget-object v0, p0, Lcom/peel/ui/b/ag;->ar:Landroid/widget/TextView;

    sget v1, Lcom/peel/ui/fo;->undo_reminder_stateful:I

    invoke-virtual {v0, v2, v1, v2, v2}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    .line 165
    iget-object v0, p0, Lcom/peel/ui/b/ag;->ar:Landroid/widget/TextView;

    sget v1, Lcom/peel/ui/ft;->undo_reminder:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 179
    :goto_2
    return-void

    :cond_2
    move v3, v2

    .line 161
    goto :goto_1

    .line 167
    :cond_3
    if-eqz v0, :cond_4

    .line 169
    iget-object v0, p0, Lcom/peel/ui/b/ag;->ar:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 170
    iget-object v0, p0, Lcom/peel/ui/b/ag;->ar:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getCompoundDrawables()[Landroid/graphics/drawable/Drawable;

    move-result-object v0

    aget-object v0, v0, v1

    const/16 v1, 0xff

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 171
    iget-object v0, p0, Lcom/peel/ui/b/ag;->ar:Landroid/widget/TextView;

    sget v1, Lcom/peel/ui/ft;->title_set_reminder:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 172
    iget-object v0, p0, Lcom/peel/ui/b/ag;->ar:Landroid/widget/TextView;

    sget v1, Lcom/peel/ui/fo;->remind_me_btn_stateful:I

    invoke-virtual {v0, v2, v1, v2, v2}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    goto :goto_2

    .line 175
    :cond_4
    iget-object v0, p0, Lcom/peel/ui/b/ag;->ar:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 176
    iget-object v0, p0, Lcom/peel/ui/b/ag;->ar:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getCompoundDrawables()[Landroid/graphics/drawable/Drawable;

    move-result-object v0

    aget-object v0, v0, v1

    const/16 v1, 0x40

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    goto :goto_2

    :cond_5
    move v0, v2

    goto :goto_0
.end method

.method static synthetic a(Lcom/peel/ui/b/ag;J)J
    .locals 1

    .prologue
    .line 54
    iput-wide p1, p0, Lcom/peel/ui/b/ag;->ao:J

    return-wide p1
.end method

.method private static a(Lcom/peel/d/i;Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 578
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 579
    sget v0, Lcom/peel/ui/fp;->menu_remote:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 580
    sget v0, Lcom/peel/ui/fp;->menu_share:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 581
    new-instance v0, Lcom/peel/d/a;

    sget-object v1, Lcom/peel/d/d;->b:Lcom/peel/d/d;

    sget-object v2, Lcom/peel/d/b;->a:Lcom/peel/d/b;

    sget-object v3, Lcom/peel/d/c;->b:Lcom/peel/d/c;

    move-object v4, p1

    invoke-direct/range {v0 .. v5}, Lcom/peel/d/a;-><init>(Lcom/peel/d/d;Lcom/peel/d/b;Lcom/peel/d/c;Ljava/lang/String;Ljava/util/List;)V

    sput-object v0, Lcom/peel/ui/b/ag;->av:Lcom/peel/d/a;

    .line 582
    sget-object v0, Lcom/peel/ui/b/ag;->av:Lcom/peel/d/a;

    invoke-virtual {p0, v0}, Lcom/peel/d/i;->a(Lcom/peel/d/a;)V

    .line 583
    return-void
.end method

.method static synthetic a(Lcom/peel/ui/b/ag;)V
    .locals 0

    .prologue
    .line 54
    invoke-direct {p0}, Lcom/peel/ui/b/ag;->U()V

    return-void
.end method

.method private a(Ljava/util/List;JJZLjava/lang/String;)V
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/peel/content/listing/Listing;",
            ">;JJZ",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 255
    sget-object v2, Lcom/peel/ui/b/ag;->e:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "\n\n ***** groupListings: \nlistings size: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 256
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->size()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "\nstart: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-wide/from16 v0, p2

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, " -- end: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-wide/from16 v0, p4

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, " -- grouping? "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, p6

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, " -- comparator: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p7

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 255
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 257
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    .line 258
    if-eqz p6, :cond_4

    .line 259
    sget-object v2, Lcom/peel/ui/b/ag;->e:Ljava/lang/String;

    const-string/jumbo v3, "\n\n ***** needs grouping path:"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 260
    invoke-static {}, Lcom/peel/content/a;->a()Lcom/peel/data/ContentRoom;

    move-result-object v9

    move-object/from16 v3, p1

    move-wide/from16 v4, p2

    move-wide/from16 v6, p4

    move-object/from16 v8, p7

    invoke-static/range {v3 .. v9}, Lcom/peel/util/bx;->a(Ljava/util/List;JJLjava/lang/String;Lcom/peel/data/ContentRoom;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Lcom/peel/content/listing/Listing;

    .line 261
    new-instance v7, Landroid/os/Bundle;

    invoke-direct {v7}, Landroid/os/Bundle;-><init>()V

    .line 263
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 265
    array-length v3, v2

    const/4 v4, 0x1

    if-le v3, v4, :cond_3

    .line 266
    sget-object v3, Lcom/peel/ui/b/ag;->e:Ljava/lang/String;

    const-string/jumbo v4, "\n\n ***** group has more than 1 listing, filter out dupes"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 268
    new-instance v3, Lcom/peel/content/listing/a;

    invoke-direct {v3}, Lcom/peel/content/listing/a;-><init>()V

    invoke-static {v3}, Ljava/util/Collections;->reverseOrder(Ljava/util/Comparator;)Ljava/util/Comparator;

    move-result-object v3

    invoke-static {v2, v3}, Ljava/util/Arrays;->sort([Ljava/lang/Object;Ljava/util/Comparator;)V

    .line 270
    new-instance v9, Ljava/util/HashSet;

    invoke-direct {v9}, Ljava/util/HashSet;-><init>()V

    .line 271
    array-length v11, v2

    const/4 v3, 0x0

    move v5, v3

    :goto_1
    if-ge v5, v11, :cond_2

    aget-object v4, v2, v5

    move-object v3, v4

    .line 272
    check-cast v3, Lcom/peel/content/listing/LiveListing;

    .line 274
    invoke-virtual {v3}, Lcom/peel/content/listing/LiveListing;->o()J

    move-result-wide v12

    const-wide/16 v14, 0x0

    cmp-long v12, v12, v14

    if-lez v12, :cond_1

    invoke-virtual {v3}, Lcom/peel/content/listing/LiveListing;->d()Ljava/lang/String;

    move-result-object v12

    if-eqz v12, :cond_1

    .line 275
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3}, Lcom/peel/content/listing/LiveListing;->d()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string/jumbo v13, "/"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v3}, Lcom/peel/content/listing/LiveListing;->o()J

    move-result-wide v14

    invoke-static {v14, v15}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-interface {v9, v12}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_0

    .line 271
    :goto_2
    add-int/lit8 v3, v5, 0x1

    move v5, v3

    goto :goto_1

    .line 279
    :cond_0
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3}, Lcom/peel/content/listing/LiveListing;->d()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string/jumbo v13, "/"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v3}, Lcom/peel/content/listing/LiveListing;->o()J

    move-result-wide v14

    invoke-static {v14, v15}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v12, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v9, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 282
    :cond_1
    invoke-interface {v8, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 285
    :cond_2
    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v3

    if-lez v3, :cond_3

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v3

    array-length v4, v2

    if-eq v3, v4, :cond_3

    .line 286
    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v2

    new-array v2, v2, [Lcom/peel/content/listing/Listing;

    invoke-interface {v8, v2}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Lcom/peel/content/listing/Listing;

    .line 290
    :cond_3
    const-string/jumbo v3, "group"

    new-instance v4, Ljava/util/ArrayList;

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    invoke-direct {v4, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-virtual {v7, v3, v4}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 292
    const-string/jumbo v3, "group_id"

    const/4 v2, 0x0

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/peel/content/listing/LiveListing;

    invoke-virtual {v2}, Lcom/peel/content/listing/LiveListing;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v7, v3, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 293
    invoke-virtual {v10, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 296
    :cond_4
    sget-object v2, Lcom/peel/ui/b/ag;->e:Ljava/lang/String;

    const-string/jumbo v3, "\n\n ***** no grouping needed path:"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 297
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_3
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/peel/content/listing/Listing;

    .line 298
    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    .line 299
    sget-object v5, Lcom/peel/ui/b/ag;->e:Ljava/lang/String;

    const-string/jumbo v6, "\n\n ***** putting in single array content listing as group... nice... @@"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 300
    const-string/jumbo v5, "group"

    new-instance v6, Lcom/peel/ui/b/ai;

    move-object/from16 v0, p0

    invoke-direct {v6, v0, v2}, Lcom/peel/ui/b/ai;-><init>(Lcom/peel/ui/b/ag;Lcom/peel/content/listing/Listing;)V

    invoke-virtual {v4, v5, v6}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 301
    const-string/jumbo v5, "group_id"

    invoke-virtual {v2}, Lcom/peel/content/listing/Listing;->h()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v5, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 302
    invoke-virtual {v10, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 307
    :cond_5
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/peel/ui/b/ag;->c:Landroid/os/Bundle;

    const-string/jumbo v3, "selected"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 308
    if-eqz v5, :cond_9

    .line 309
    const/4 v2, 0x0

    .line 310
    invoke-virtual {v10}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v6

    move v4, v2

    :goto_4
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/os/Bundle;

    .line 311
    if-eqz v4, :cond_7

    .line 325
    :cond_6
    :goto_5
    sget-object v2, Lcom/peel/ui/b/ag;->e:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "\n\n ***** selected_id: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, " -- selected_group: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget v4, v0, Lcom/peel/ui/b/ag;->f:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 327
    invoke-static {}, Lcom/peel/util/i;->c()Z

    move-result v2

    if-eqz v2, :cond_a

    .line 328
    move-object/from16 v0, p0

    invoke-virtual {v0, v10}, Lcom/peel/ui/b/ag;->a(Ljava/util/ArrayList;)V

    .line 337
    :goto_6
    return-void

    .line 313
    :cond_7
    const-string/jumbo v3, "group"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_8
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_b

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/os/Parcelable;

    .line 314
    check-cast v3, Lcom/peel/content/listing/Listing;

    invoke-virtual {v3}, Lcom/peel/content/listing/Listing;->f()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v5, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_8

    .line 315
    invoke-virtual {v10, v2}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/peel/ui/b/ag;->f:I

    .line 316
    const/4 v2, 0x1

    :goto_7
    move v4, v2

    .line 320
    goto :goto_4

    .line 322
    :cond_9
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, Lcom/peel/ui/b/ag;->f:I

    goto :goto_5

    .line 330
    :cond_a
    sget-object v2, Lcom/peel/ui/b/ag;->e:Ljava/lang/String;

    const-string/jumbo v3, "renderCard"

    new-instance v4, Lcom/peel/ui/b/aj;

    move-object/from16 v0, p0

    invoke-direct {v4, v0, v10}, Lcom/peel/ui/b/aj;-><init>(Lcom/peel/ui/b/ag;Ljava/util/ArrayList;)V

    invoke-static {v2, v3, v4}, Lcom/peel/util/i;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/lang/Runnable;

    goto :goto_6

    :cond_b
    move v2, v4

    goto :goto_7
.end method

.method static synthetic b(Lcom/peel/ui/b/ag;)Lcom/peel/ui/b/q;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/peel/ui/b/ag;->an:Lcom/peel/ui/b/q;

    return-object v0
.end method

.method static synthetic c(Lcom/peel/ui/b/ag;)J
    .locals 2

    .prologue
    .line 54
    iget-wide v0, p0, Lcom/peel/ui/b/ag;->ao:J

    return-wide v0
.end method

.method static synthetic d(Lcom/peel/ui/b/ag;)Landroid/view/View;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/peel/ui/b/ag;->am:Landroid/view/View;

    return-object v0
.end method

.method static synthetic e(Lcom/peel/ui/b/ag;)Lcom/peel/ui/ScrollPositionListView;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/peel/ui/b/ag;->ak:Lcom/peel/ui/ScrollPositionListView;

    return-object v0
.end method

.method static synthetic f(Lcom/peel/ui/b/ag;)I
    .locals 1

    .prologue
    .line 54
    iget v0, p0, Lcom/peel/ui/b/ag;->h:I

    return v0
.end method

.method static synthetic g(Lcom/peel/ui/b/ag;)I
    .locals 1

    .prologue
    .line 54
    iget v0, p0, Lcom/peel/ui/b/ag;->i:I

    return v0
.end method

.method static synthetic h(Lcom/peel/ui/b/ag;)Lcom/peel/ui/b/a/a;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/peel/ui/b/ag;->ap:Lcom/peel/ui/b/a/a;

    return-object v0
.end method


# virtual methods
.method public Z()V
    .locals 2

    .prologue
    .line 612
    sget-object v0, Lcom/peel/ui/b/ag;->av:Lcom/peel/d/a;

    if-eqz v0, :cond_0

    .line 613
    iget-object v0, p0, Lcom/peel/ui/b/ag;->b:Lcom/peel/d/i;

    sget-object v1, Lcom/peel/ui/b/ag;->av:Lcom/peel/d/a;

    invoke-virtual {v0, v1}, Lcom/peel/d/i;->a(Lcom/peel/d/a;)V

    .line 615
    :cond_0
    return-void
.end method

.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    .line 72
    sget v0, Lcom/peel/ui/fq;->card_view_listview:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/ui/b/ag;->aj:Landroid/view/View;

    .line 73
    iget-object v0, p0, Lcom/peel/ui/b/ag;->aj:Landroid/view/View;

    sget v1, Lcom/peel/ui/fp;->shadow_btn_area:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/ui/b/ag;->am:Landroid/view/View;

    .line 74
    iget-object v0, p0, Lcom/peel/ui/b/ag;->am:Landroid/view/View;

    sget v1, Lcom/peel/ui/fp;->watch_on_tv:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/peel/ui/b/ag;->aq:Landroid/widget/TextView;

    .line 75
    iget-object v0, p0, Lcom/peel/ui/b/ag;->am:Landroid/view/View;

    sget v1, Lcom/peel/ui/fp;->set_reminder:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/peel/ui/b/ag;->ar:Landroid/widget/TextView;

    .line 76
    iget-object v0, p0, Lcom/peel/ui/b/ag;->am:Landroid/view/View;

    sget v1, Lcom/peel/ui/fp;->record_btn:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/peel/ui/b/ag;->as:Landroid/widget/TextView;

    .line 77
    iget-object v0, p0, Lcom/peel/ui/b/ag;->aj:Landroid/view/View;

    sget v1, Lcom/peel/ui/fp;->detail_list:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/peel/ui/ScrollPositionListView;

    iput-object v0, p0, Lcom/peel/ui/b/ag;->ak:Lcom/peel/ui/ScrollPositionListView;

    .line 78
    iget-object v0, p0, Lcom/peel/ui/b/ag;->aj:Landroid/view/View;

    sget v1, Lcom/peel/ui/fp;->epg_image:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/peel/ui/b/ag;->al:Landroid/widget/ImageView;

    .line 80
    iget-object v0, p0, Lcom/peel/ui/b/ag;->aj:Landroid/view/View;

    return-object v0
.end method

.method public a(IILandroid/content/Intent;)V
    .locals 2

    .prologue
    .line 555
    const/16 v0, 0x65

    if-ne p1, v0, :cond_0

    .line 556
    iget-object v0, p0, Lcom/peel/ui/b/ag;->ap:Lcom/peel/ui/b/a/a;

    iget-object v1, p0, Lcom/peel/ui/b/ag;->ap:Lcom/peel/ui/b/a/a;

    invoke-virtual {v1}, Lcom/peel/ui/b/a/a;->b()Lcom/peel/content/listing/LiveListing;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/content/listing/LiveListing;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/peel/ui/b/a/a;->a(Ljava/lang/String;)V

    .line 558
    :cond_0
    return-void
.end method

.method public a(Ljava/util/ArrayList;)V
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/os/Bundle;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    const/16 v13, 0xff

    const/16 v12, 0x40

    const/4 v11, 0x0

    const/4 v10, 0x1

    .line 341
    sget-object v0, Lcom/peel/ui/b/ag;->e:Ljava/lang/String;

    const-string/jumbo v2, "\n\n ***** in renderPager()"

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 342
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_1

    .line 551
    :cond_0
    :goto_0
    return-void

    .line 344
    :cond_1
    iget-object v0, p0, Lcom/peel/ui/b/ag;->c:Landroid/os/Bundle;

    const-string/jumbo v2, "group_bundles"

    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 346
    iget v0, p0, Lcom/peel/ui/b/ag;->f:I

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    const-string/jumbo v2, "group"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    .line 347
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 349
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Parcelable;

    .line 350
    check-cast v0, Lcom/peel/content/listing/Listing;

    invoke-interface {v9, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 353
    :cond_2
    sget-object v0, Lcom/peel/ui/b/ag;->e:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "listings len : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 354
    sget-object v0, Lcom/peel/ui/b/ag;->e:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "\n\n ***** selected_group Parcelable[] size: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 356
    iget-object v0, p0, Lcom/peel/ui/b/ag;->ap:Lcom/peel/ui/b/a/a;

    invoke-virtual {v0, v9}, Lcom/peel/ui/b/a/a;->a(Ljava/util/List;)V

    .line 358
    iget-object v0, p0, Lcom/peel/ui/b/ag;->al:Landroid/widget/ImageView;

    if-eqz v0, :cond_3

    .line 359
    invoke-interface {v2, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/content/listing/Listing;

    invoke-static {v0}, Lcom/peel/util/bx;->a(Lcom/peel/content/listing/Listing;)I

    move-result v3

    .line 360
    const/4 v4, 0x3

    const/4 v5, 0x4

    const/16 v6, 0x21c

    invoke-interface {v9, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/content/listing/LiveListing;

    .line 361
    invoke-virtual {v0}, Lcom/peel/content/listing/LiveListing;->B()Ljava/util/Map;

    move-result-object v0

    .line 360
    invoke-static {v4, v5, v6, v0}, Lcom/peel/util/bx;->a(IIILjava/util/Map;)Ljava/lang/String;

    move-result-object v0

    .line 363
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_b

    .line 364
    invoke-virtual {p0}, Lcom/peel/ui/b/ag;->m()Landroid/support/v4/app/ae;

    move-result-object v4

    invoke-static {v4}, Lcom/peel/util/b/e;->a(Landroid/content/Context;)Lcom/squareup/picasso/Picasso;

    move-result-object v4

    .line 365
    invoke-virtual {v4, v0}, Lcom/squareup/picasso/Picasso;->load(Ljava/lang/String;)Lcom/squareup/picasso/RequestCreator;

    move-result-object v0

    .line 366
    invoke-virtual {v0, v3}, Lcom/squareup/picasso/RequestCreator;->placeholder(I)Lcom/squareup/picasso/RequestCreator;

    move-result-object v0

    .line 367
    invoke-virtual {v0}, Lcom/squareup/picasso/RequestCreator;->noFade()Lcom/squareup/picasso/RequestCreator;

    move-result-object v0

    .line 368
    invoke-virtual {v0}, Lcom/squareup/picasso/RequestCreator;->fit()Lcom/squareup/picasso/RequestCreator;

    move-result-object v0

    iget-object v3, p0, Lcom/peel/ui/b/ag;->al:Landroid/widget/ImageView;

    .line 369
    invoke-virtual {v0, v3}, Lcom/squareup/picasso/RequestCreator;->into(Landroid/widget/ImageView;)V

    .line 375
    :cond_3
    :goto_2
    invoke-interface {v2, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lcom/peel/content/listing/LiveListing;

    .line 376
    invoke-virtual {v8}, Lcom/peel/content/listing/LiveListing;->o()J

    move-result-wide v2

    .line 378
    iget-object v0, p0, Lcom/peel/ui/b/ag;->ap:Lcom/peel/ui/b/a/a;

    iget-object v4, p0, Lcom/peel/ui/b/ag;->c:Landroid/os/Bundle;

    const-string/jumbo v5, "end"

    const-wide/16 v6, -0x1

    invoke-virtual {v4, v5, v6, v7}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v4

    invoke-virtual {v0, v2, v3, v4, v5}, Lcom/peel/ui/b/a/a;->a(JJ)I

    move-result v0

    .line 380
    if-nez v0, :cond_c

    .line 382
    iget-object v0, p0, Lcom/peel/ui/b/ag;->aq:Landroid/widget/TextView;

    invoke-virtual {v0, v11}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 383
    iget-object v0, p0, Lcom/peel/ui/b/ag;->aq:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getCompoundDrawables()[Landroid/graphics/drawable/Drawable;

    move-result-object v0

    aget-object v0, v0, v10

    invoke-virtual {v0, v12}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 390
    :goto_3
    invoke-direct {p0}, Lcom/peel/ui/b/ag;->U()V

    .line 392
    iget-object v0, p0, Lcom/peel/ui/b/ag;->ap:Lcom/peel/ui/b/a/a;

    invoke-virtual {v0, v2, v3}, Lcom/peel/ui/b/a/a;->a(J)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 394
    iget-object v0, p0, Lcom/peel/ui/b/ag;->as:Landroid/widget/TextView;

    invoke-virtual {v0, v10}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 395
    iget-object v0, p0, Lcom/peel/ui/b/ag;->as:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getCompoundDrawables()[Landroid/graphics/drawable/Drawable;

    move-result-object v0

    aget-object v0, v0, v10

    invoke-virtual {v0, v13}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 402
    :goto_4
    new-instance v0, Lcom/peel/ui/b/q;

    invoke-virtual {p0}, Lcom/peel/ui/b/ag;->m()Landroid/support/v4/app/ae;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/peel/ui/b/q;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/peel/ui/b/ag;->an:Lcom/peel/ui/b/q;

    .line 403
    new-instance v0, Lcom/a/a/b/a/a;

    iget-object v2, p0, Lcom/peel/ui/b/ag;->an:Lcom/peel/ui/b/q;

    invoke-direct {v0, v2}, Lcom/a/a/b/a/a;-><init>(Landroid/widget/BaseAdapter;)V

    .line 404
    const-wide/16 v2, 0x12c

    invoke-virtual {v0, v2, v3}, Lcom/a/a/b/a/a;->a(J)V

    .line 405
    iget-object v2, p0, Lcom/peel/ui/b/ag;->ak:Lcom/peel/ui/ScrollPositionListView;

    invoke-virtual {v0, v2}, Lcom/a/a/b/a/a;->a(Landroid/widget/AbsListView;)V

    .line 408
    iget-object v2, p0, Lcom/peel/ui/b/ag;->ak:Lcom/peel/ui/ScrollPositionListView;

    invoke-virtual {v2, v0}, Lcom/peel/ui/ScrollPositionListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 409
    iget-object v0, p0, Lcom/peel/ui/b/ag;->ak:Lcom/peel/ui/ScrollPositionListView;

    invoke-virtual {v0, v10}, Lcom/peel/ui/ScrollPositionListView;->setHorizontalScrollBarEnabled(Z)V

    .line 410
    iget-object v0, p0, Lcom/peel/ui/b/ag;->ak:Lcom/peel/ui/ScrollPositionListView;

    new-instance v2, Lcom/peel/ui/b/ak;

    invoke-direct {v2, p0}, Lcom/peel/ui/b/ak;-><init>(Lcom/peel/ui/b/ag;)V

    invoke-virtual {v0, v2}, Lcom/peel/ui/ScrollPositionListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 434
    iget-object v0, p0, Lcom/peel/ui/b/ag;->an:Lcom/peel/ui/b/q;

    new-instance v2, Lcom/peel/ui/b/cd;

    iget-object v3, p0, Lcom/peel/ui/b/ag;->ap:Lcom/peel/ui/b/a/a;

    invoke-virtual {p0}, Lcom/peel/ui/b/ag;->m()Landroid/support/v4/app/ae;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lcom/peel/ui/b/cd;-><init>(Lcom/peel/ui/b/a/a;Landroid/content/Context;)V

    invoke-virtual {v0, v2}, Lcom/peel/ui/b/q;->a(Lcom/peel/ui/b/af;)V

    .line 435
    iget-object v0, p0, Lcom/peel/ui/b/ag;->an:Lcom/peel/ui/b/q;

    new-instance v2, Lcom/peel/ui/b/o;

    iget-object v3, p0, Lcom/peel/ui/b/ag;->ap:Lcom/peel/ui/b/a/a;

    invoke-direct {v2, v3}, Lcom/peel/ui/b/o;-><init>(Lcom/peel/ui/b/a/a;)V

    invoke-virtual {v0, v2}, Lcom/peel/ui/b/q;->a(Lcom/peel/ui/b/af;)V

    .line 437
    invoke-virtual {p0}, Lcom/peel/ui/b/ag;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    invoke-static {v0}, Lcom/peel/util/bx;->c(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 439
    if-eqz v0, :cond_4

    const-string/jumbo v2, "china"

    .line 440
    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_5

    const-string/jumbo v2, "cn"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 441
    :cond_4
    iget-object v10, p0, Lcom/peel/ui/b/ag;->an:Lcom/peel/ui/b/q;

    new-instance v0, Lcom/peel/ui/b/r;

    invoke-virtual {p0}, Lcom/peel/ui/b/ag;->m()Landroid/support/v4/app/ae;

    move-result-object v2

    iget-object v3, p0, Lcom/peel/ui/b/ag;->ap:Lcom/peel/ui/b/a/a;

    .line 442
    invoke-virtual {v3}, Lcom/peel/ui/b/a/a;->a()[Ljava/lang/String;

    move-result-object v3

    sget v4, Lcom/peel/ui/b/ag;->g:I

    invoke-virtual {v8}, Lcom/peel/content/listing/LiveListing;->b()Ljava/lang/String;

    move-result-object v5

    invoke-static {v8}, Lcom/peel/util/bx;->b(Lcom/peel/content/listing/Listing;)Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lcom/peel/ui/b/ag;->ap:Lcom/peel/ui/b/a/a;

    invoke-direct/range {v0 .. v7}, Lcom/peel/ui/b/r;-><init>(Ljava/util/List;Landroid/support/v4/app/ae;[Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Lcom/peel/ui/b/a/a;)V

    .line 441
    invoke-virtual {v10, v0}, Lcom/peel/ui/b/q;->a(Lcom/peel/ui/b/af;)V

    .line 444
    iget-object v0, p0, Lcom/peel/ui/b/ag;->ap:Lcom/peel/ui/b/a/a;

    iget-object v2, p0, Lcom/peel/ui/b/ag;->au:Lcom/peel/util/t;

    invoke-virtual {v0, v2}, Lcom/peel/ui/b/a/a;->a(Lcom/peel/util/t;)V

    .line 445
    iget-object v0, p0, Lcom/peel/ui/b/ag;->ap:Lcom/peel/ui/b/a/a;

    invoke-virtual {v8}, Lcom/peel/content/listing/LiveListing;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/peel/ui/b/a/a;->a(Ljava/lang/String;)V

    .line 448
    :cond_5
    invoke-interface {v9, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/content/listing/LiveListing;

    check-cast v0, Lcom/peel/content/listing/LiveListing;

    invoke-virtual {v0}, Lcom/peel/content/listing/LiveListing;->x()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 449
    iget-object v0, p0, Lcom/peel/ui/b/ag;->an:Lcom/peel/ui/b/q;

    new-instance v2, Lcom/peel/ui/b/cm;

    iget-object v3, p0, Lcom/peel/ui/b/ag;->ap:Lcom/peel/ui/b/a/a;

    invoke-virtual {p0}, Lcom/peel/ui/b/ag;->m()Landroid/support/v4/app/ae;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lcom/peel/ui/b/cm;-><init>(Lcom/peel/ui/b/a/a;Landroid/support/v4/app/ae;)V

    invoke-virtual {v0, v2}, Lcom/peel/ui/b/q;->a(Lcom/peel/ui/b/af;)V

    .line 452
    :cond_6
    iget-object v0, p0, Lcom/peel/ui/b/ag;->an:Lcom/peel/ui/b/q;

    invoke-virtual {v0}, Lcom/peel/ui/b/q;->notifyDataSetChanged()V

    .line 454
    sget-object v0, Lcom/peel/content/a;->e:Ljava/lang/String;

    const-string/jumbo v2, "US"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_7

    sget-object v0, Lcom/peel/content/a;->e:Ljava/lang/String;

    const-string/jumbo v2, "ca"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 455
    :cond_7
    iget-object v0, p0, Lcom/peel/ui/b/ag;->ap:Lcom/peel/ui/b/a/a;

    new-instance v2, Lcom/peel/ui/b/al;

    invoke-direct {v2, p0}, Lcom/peel/ui/b/al;-><init>(Lcom/peel/ui/b/ag;)V

    invoke-virtual {v0, v2}, Lcom/peel/ui/b/a/a;->d(Lcom/peel/util/t;)V

    .line 476
    :cond_8
    invoke-virtual {v8}, Lcom/peel/content/listing/LiveListing;->b()Ljava/lang/String;

    move-result-object v0

    .line 478
    iget-object v2, p0, Lcom/peel/ui/b/ag;->ap:Lcom/peel/ui/b/a/a;

    new-instance v3, Lcom/peel/ui/b/an;

    invoke-direct {v3, p0}, Lcom/peel/ui/b/an;-><init>(Lcom/peel/ui/b/ag;)V

    invoke-virtual {v2, v3}, Lcom/peel/ui/b/a/a;->c(Lcom/peel/util/t;)V

    .line 498
    sget-object v2, Lcom/peel/content/a;->e:Ljava/lang/String;

    const-string/jumbo v3, "US"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 499
    new-instance v2, Lcom/peel/ui/b/ap;

    invoke-direct {v2, p0, v9, v8}, Lcom/peel/ui/b/ap;-><init>(Lcom/peel/ui/b/ag;Ljava/util/List;Lcom/peel/content/listing/LiveListing;)V

    .line 524
    sget-object v3, Lcom/peel/ui/b/ag;->e:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "get graph node: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Lcom/peel/ui/b/ar;

    invoke-direct {v5, p0, v0, v2}, Lcom/peel/ui/b/ar;-><init>(Lcom/peel/ui/b/ag;Ljava/lang/String;Lcom/peel/util/t;)V

    invoke-static {v3, v4, v5}, Lcom/peel/util/i;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    .line 540
    :cond_9
    invoke-virtual {v8}, Lcom/peel/content/listing/LiveListing;->k()Ljava/lang/String;

    move-result-object v0

    .line 543
    const-string/jumbo v2, "movie"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_e

    .line 544
    sget v0, Lcom/peel/ui/ft;->movie:I

    invoke-virtual {p0, v0}, Lcom/peel/ui/b/ag;->a(I)Ljava/lang/String;

    move-result-object v1

    .line 550
    :cond_a
    :goto_5
    iget-object v0, p0, Lcom/peel/ui/b/ag;->b:Lcom/peel/d/i;

    invoke-static {v0, v1}, Lcom/peel/ui/b/ag;->a(Lcom/peel/d/i;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 371
    :cond_b
    iget-object v0, p0, Lcom/peel/ui/b/ag;->al:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_2

    .line 386
    :cond_c
    iget-object v0, p0, Lcom/peel/ui/b/ag;->aq:Landroid/widget/TextView;

    invoke-virtual {v0, v10}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 387
    iget-object v0, p0, Lcom/peel/ui/b/ag;->aq:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getCompoundDrawables()[Landroid/graphics/drawable/Drawable;

    move-result-object v0

    aget-object v0, v0, v10

    invoke-virtual {v0, v13}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    goto/16 :goto_3

    .line 398
    :cond_d
    iget-object v0, p0, Lcom/peel/ui/b/ag;->as:Landroid/widget/TextView;

    invoke-virtual {v0, v11}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 399
    iget-object v0, p0, Lcom/peel/ui/b/ag;->as:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getCompoundDrawables()[Landroid/graphics/drawable/Drawable;

    move-result-object v0

    aget-object v0, v0, v10

    invoke-virtual {v0, v12}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    goto/16 :goto_4

    .line 545
    :cond_e
    const-string/jumbo v2, "program"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_f

    .line 546
    sget v0, Lcom/peel/ui/ft;->tvshow:I

    invoke-virtual {p0, v0}, Lcom/peel/ui/b/ag;->a(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_5

    .line 547
    :cond_f
    const-string/jumbo v2, "sports"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 548
    sget v0, Lcom/peel/ui/ft;->sports:I

    invoke-virtual {p0, v0}, Lcom/peel/ui/b/ag;->a(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_5
.end method

.method public a(Landroid/view/MenuItem;)Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 587
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    .line 588
    sget v3, Lcom/peel/ui/fp;->menu_share:I

    if-ne v0, v3, :cond_0

    .line 589
    iget-object v0, p0, Lcom/peel/ui/b/ag;->c:Landroid/os/Bundle;

    const-string/jumbo v3, "group_bundles"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    .line 590
    iget v3, p0, Lcom/peel/ui/b/ag;->f:I

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    const-string/jumbo v3, "group"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    .line 591
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/content/listing/Listing;

    .line 592
    invoke-virtual {v0}, Lcom/peel/content/listing/Listing;->l()Ljava/lang/String;

    move-result-object v0

    .line 593
    new-instance v3, Ljava/lang/StringBuilder;

    sget v4, Lcom/peel/ui/ft;->i_am_watching:I

    new-array v5, v1, [Ljava/lang/Object;

    aput-object v0, v5, v2

    invoke-virtual {p0, v4, v5}, Lcom/peel/ui/b/ag;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 594
    const-string/jumbo v2, " #"

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v4, "(\\W)+"

    const-string/jumbo v5, ""

    invoke-virtual {v0, v4, v5}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 595
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v2, "android.intent.action.SEND"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 596
    const-string/jumbo v2, "text/plain"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 597
    const-string/jumbo v2, "android.intent.extra.TEXT"

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 598
    sget v2, Lcom/peel/ui/ft;->card_recommend:I

    invoke-virtual {p0, v2}, Lcom/peel/ui/b/ag;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/content/Intent;->createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/peel/ui/b/ag;->a(Landroid/content/Intent;)V

    move v0, v1

    .line 601
    :goto_0
    return v0

    :cond_0
    move v0, v2

    goto :goto_0
.end method

.method public a_(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 85
    invoke-super {p0, p1}, Lcom/peel/d/u;->a_(Landroid/os/Bundle;)V

    .line 86
    return-void
.end method

.method public c()V
    .locals 17

    .prologue
    .line 195
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/peel/ui/b/ag;->c:Landroid/os/Bundle;

    const-string/jumbo v3, "context_id"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v2

    sput v2, Lcom/peel/ui/b/ag;->g:I

    .line 196
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/peel/ui/b/ag;->c:Landroid/os/Bundle;

    const-string/jumbo v3, "start"

    const-wide/16 v4, -0x1

    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v4

    .line 197
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/peel/ui/b/ag;->c:Landroid/os/Bundle;

    const-string/jumbo v3, "end"

    const-wide/16 v6, -0x1

    invoke-virtual {v2, v3, v6, v7}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v6

    .line 198
    new-instance v11, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v2, 0x0

    invoke-direct {v11, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    .line 200
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/peel/ui/b/ag;->ap:Lcom/peel/ui/b/a/a;

    sget v3, Lcom/peel/ui/b/ag;->g:I

    invoke-virtual {v2, v3}, Lcom/peel/ui/b/a/a;->a(I)V

    .line 201
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 202
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/peel/ui/b/ag;->c:Landroid/os/Bundle;

    const-string/jumbo v8, "listing"

    invoke-virtual {v2, v8}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 203
    sget-object v2, Lcom/peel/ui/b/ag;->e:Ljava/lang/String;

    const-string/jumbo v3, "\n\n ***** "

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 204
    sget-object v2, Lcom/peel/ui/b/ag;->e:Ljava/lang/String;

    const-string/jumbo v3, "\n\n ***** bundle contains listing path"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 205
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 206
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/peel/ui/b/ag;->c:Landroid/os/Bundle;

    const-string/jumbo v8, "listing"

    invoke-virtual {v2, v8}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Lcom/peel/content/listing/Listing;

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 207
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/peel/ui/b/ag;->c:Landroid/os/Bundle;

    const-string/jumbo v8, "needsgrouping"

    const/4 v9, 0x1

    invoke-virtual {v2, v8, v9}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v8

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/peel/ui/b/ag;->c:Landroid/os/Bundle;

    const-string/jumbo v9, "comparator"

    invoke-virtual {v2, v9}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    move-object/from16 v2, p0

    invoke-direct/range {v2 .. v9}, Lcom/peel/ui/b/ag;->a(Ljava/util/List;JJZLjava/lang/String;)V

    .line 251
    :cond_0
    :goto_0
    return-void

    .line 209
    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/peel/ui/b/ag;->c:Landroid/os/Bundle;

    const-string/jumbo v8, "libraryIds"

    invoke-virtual {v2, v8}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v12

    .line 211
    if-eqz v12, :cond_0

    .line 212
    new-instance v13, Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v2, 0x0

    invoke-direct {v13, v2}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    .line 214
    array-length v14, v12

    const/4 v2, 0x0

    move v10, v2

    :goto_1
    if-ge v10, v14, :cond_5

    aget-object v8, v12, v10

    .line 215
    sget-object v2, Lcom/peel/ui/b/ag;->e:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v15, "\n\n ***** bundle lib: "

    invoke-virtual {v9, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v2, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 217
    :try_start_0
    invoke-static {v8}, Lcom/peel/content/a;->b(Ljava/lang/String;)Lcom/peel/content/library/Library;

    move-result-object v2

    .line 218
    if-nez v2, :cond_2

    .line 219
    const/4 v2, 0x1

    invoke-virtual {v13, v2}, Ljava/util/concurrent/atomic/AtomicInteger;->addAndGet(I)I

    .line 214
    :goto_2
    add-int/lit8 v2, v10, 0x1

    move v10, v2

    goto :goto_1

    .line 223
    :cond_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/peel/ui/b/ag;->c:Landroid/os/Bundle;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v15, "listings/"

    invoke-virtual {v9, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v2, v9}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    .line 225
    if-nez v2, :cond_3

    .line 226
    sget-object v2, Lcom/peel/ui/b/ag;->e:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v15, " ************ listings/"

    invoke-virtual {v9, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string/jumbo v9, " -- null listings!"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v2, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 227
    const/4 v2, 0x1

    invoke-virtual {v13, v2}, Ljava/util/concurrent/atomic/AtomicInteger;->addAndGet(I)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    .line 240
    :catch_0
    move-exception v2

    .line 241
    sget-object v8, Lcom/peel/ui/b/ag;->e:Ljava/lang/String;

    sget-object v9, Lcom/peel/ui/b/ag;->e:Ljava/lang/String;

    invoke-static {v8, v9, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 242
    const/4 v2, 0x1

    invoke-virtual {v13, v2}, Ljava/util/concurrent/atomic/AtomicInteger;->addAndGet(I)I

    goto :goto_2

    .line 231
    :cond_3
    :try_start_1
    check-cast v2, Ljava/util/ArrayList;

    .line 233
    const/4 v8, 0x0

    .line 234
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v15

    :goto_3
    invoke-interface {v15}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface {v15}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/os/Parcelable;

    .line 235
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v16, "\n\n ***** Parcelable["

    move-object/from16 v0, v16

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    add-int/lit8 v9, v8, 0x1

    move-object/from16 v0, v16

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string/jumbo v16, "]: "

    move-object/from16 v0, v16

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object v0, v2

    check-cast v0, Lcom/peel/content/listing/Listing;

    move-object v8, v0

    invoke-virtual {v8}, Lcom/peel/content/listing/Listing;->f()Ljava/lang/String;

    move-result-object v8

    move-object/from16 v0, v16

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 236
    sget-object v16, Lcom/peel/ui/b/ag;->e:Ljava/lang/String;

    move-object/from16 v0, v16

    invoke-static {v0, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 237
    check-cast v2, Lcom/peel/content/listing/Listing;

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move v8, v9

    .line 238
    goto :goto_3

    .line 239
    :cond_4
    const/4 v2, 0x1

    invoke-virtual {v13, v2}, Ljava/util/concurrent/atomic/AtomicInteger;->addAndGet(I)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_2

    .line 245
    :cond_5
    invoke-virtual {v11}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {v13}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v2

    array-length v8, v12

    if-ne v2, v8, :cond_0

    .line 246
    const/4 v2, 0x1

    invoke-virtual {v11, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 247
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/peel/ui/b/ag;->c:Landroid/os/Bundle;

    const-string/jumbo v8, "needsgrouping"

    const/4 v9, 0x1

    invoke-virtual {v2, v8, v9}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v8

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/peel/ui/b/ag;->c:Landroid/os/Bundle;

    const-string/jumbo v9, "comparator"

    invoke-virtual {v2, v9}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    move-object/from16 v2, p0

    invoke-direct/range {v2 .. v9}, Lcom/peel/ui/b/ag;->a(Ljava/util/List;JJZLjava/lang/String;)V

    goto/16 :goto_0
.end method

.method public c(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 130
    invoke-super {p0, p1}, Lcom/peel/d/u;->c(Landroid/os/Bundle;)V

    .line 131
    sget-object v0, Lcom/peel/content/a;->h:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/peel/ui/b/ag;->b:Lcom/peel/d/i;

    if-nez v0, :cond_1

    .line 138
    :cond_0
    :goto_0
    return-void

    .line 137
    :cond_1
    invoke-virtual {p0}, Lcom/peel/ui/b/ag;->c()V

    goto :goto_0
.end method

.method public d(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 99
    invoke-super {p0, p1}, Lcom/peel/d/u;->d(Landroid/os/Bundle;)V

    .line 101
    new-instance v0, Lcom/peel/ui/b/a/a;

    invoke-virtual {p0}, Lcom/peel/ui/b/ag;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    iget-object v2, p0, Lcom/peel/ui/b/ag;->aj:Landroid/view/View;

    invoke-direct {v0, v1, v2}, Lcom/peel/ui/b/a/a;-><init>(Landroid/support/v4/app/ae;Landroid/view/View;)V

    iput-object v0, p0, Lcom/peel/ui/b/ag;->ap:Lcom/peel/ui/b/a/a;

    .line 102
    iget-object v0, p0, Lcom/peel/ui/b/ag;->aq:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/peel/ui/b/ag;->ap:Lcom/peel/ui/b/a/a;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 103
    iget-object v0, p0, Lcom/peel/ui/b/ag;->ar:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/peel/ui/b/ag;->ap:Lcom/peel/ui/b/a/a;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 104
    iget-object v0, p0, Lcom/peel/ui/b/ag;->as:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/peel/ui/b/ag;->ap:Lcom/peel/ui/b/a/a;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 108
    if-eqz p1, :cond_0

    .line 109
    const-string/jumbo v0, "bundle"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 110
    iget-object v0, p0, Lcom/peel/ui/b/ag;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "bundle"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->putAll(Landroid/os/Bundle;)V

    .line 114
    :cond_0
    iget-object v0, p0, Lcom/peel/ui/b/ag;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "group_bundles"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 115
    iget-object v0, p0, Lcom/peel/ui/b/ag;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "group_bundles"

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 118
    :cond_1
    invoke-virtual {p0}, Lcom/peel/ui/b/ag;->n()Landroid/content/res/Resources;

    move-result-object v0

    .line 120
    sget v1, Lcom/peel/ui/fn;->detail_top_scroll_low_limit:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    invoke-static {v3, v1, v2}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v1

    float-to-int v1, v1

    iput v1, p0, Lcom/peel/ui/b/ag;->h:I

    .line 121
    sget v1, Lcom/peel/ui/fn;->detail_top_scroll_max_limit:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    invoke-static {v3, v1, v0}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/peel/ui/b/ag;->i:I

    .line 123
    sget-object v0, Lcom/peel/content/a;->h:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 124
    iget-object v0, p0, Lcom/peel/ui/b/ag;->c:Landroid/os/Bundle;

    invoke-virtual {p0, v0}, Lcom/peel/ui/b/ag;->c(Landroid/os/Bundle;)V

    .line 126
    :cond_2
    return-void
.end method

.method public e(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 606
    invoke-virtual {p1, p1}, Landroid/os/Bundle;->putAll(Landroid/os/Bundle;)V

    .line 607
    invoke-super {p0, p1}, Lcom/peel/d/u;->e(Landroid/os/Bundle;)V

    .line 608
    return-void
.end method

.method public f()V
    .locals 3

    .prologue
    .line 90
    invoke-super {p0}, Lcom/peel/d/u;->f()V

    .line 93
    invoke-virtual {p0}, Lcom/peel/ui/b/ag;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    const-string/jumbo v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/ae;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 94
    invoke-virtual {p0}, Lcom/peel/ui/b/ag;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/ae;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 95
    return-void
.end method

.method public w()V
    .locals 4

    .prologue
    .line 148
    invoke-super {p0}, Lcom/peel/d/u;->w()V

    .line 149
    invoke-virtual {p0}, Lcom/peel/ui/b/ag;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    invoke-static {v0}, Landroid/support/v4/a/q;->a(Landroid/content/Context;)Landroid/support/v4/a/q;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/ui/b/ag;->at:Landroid/content/BroadcastReceiver;

    new-instance v2, Landroid/content/IntentFilter;

    const-string/jumbo v3, "reminder_updated"

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/a/q;->a(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)V

    .line 150
    return-void
.end method

.method public x()V
    .locals 2

    .prologue
    .line 142
    invoke-super {p0}, Lcom/peel/d/u;->x()V

    .line 143
    invoke-virtual {p0}, Lcom/peel/ui/b/ag;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    invoke-static {v0}, Landroid/support/v4/a/q;->a(Landroid/content/Context;)Landroid/support/v4/a/q;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/ui/b/ag;->at:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/support/v4/a/q;->a(Landroid/content/BroadcastReceiver;)V

    .line 144
    return-void
.end method
