.class public Lcom/peel/ui/b/cm;
.super Lcom/peel/ui/b/af;


# instance fields
.field private final c:Lcom/peel/ui/b/a/a;

.field private final d:Landroid/support/v4/app/ae;


# direct methods
.method public constructor <init>(Lcom/peel/ui/b/a/a;Landroid/support/v4/app/ae;)V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Lcom/peel/ui/b/af;-><init>()V

    .line 21
    iput-object p1, p0, Lcom/peel/ui/b/cm;->c:Lcom/peel/ui/b/a/a;

    .line 22
    iput-object p2, p0, Lcom/peel/ui/b/cm;->d:Landroid/support/v4/app/ae;

    .line 23
    return-void
.end method

.method static synthetic a(Lcom/peel/ui/b/cm;)Landroid/support/v4/app/ae;
    .locals 1

    .prologue
    .line 16
    iget-object v0, p0, Lcom/peel/ui/b/cm;->d:Landroid/support/v4/app/ae;

    return-object v0
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 27
    const/4 v0, 0x2

    return v0
.end method

.method public a(Landroid/view/LayoutInflater;Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 32
    if-nez p2, :cond_3

    new-instance v0, Lcom/peel/ui/b/co;

    invoke-direct {v0, p0}, Lcom/peel/ui/b/co;-><init>(Lcom/peel/ui/b/cm;)V

    move-object v1, v0

    .line 34
    :goto_0
    if-nez p2, :cond_0

    .line 35
    sget v0, Lcom/peel/ui/fq;->show_details_overview_synopsis:I

    invoke-virtual {p1, v0, p3, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 36
    sget v0, Lcom/peel/ui/fp;->synopsis:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lcom/peel/ui/b/co;->a:Landroid/widget/TextView;

    .line 37
    sget v0, Lcom/peel/ui/fp;->more_bottom:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, v1, Lcom/peel/ui/b/co;->b:Landroid/view/View;

    .line 38
    invoke-virtual {p2, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 41
    :cond_0
    iget-object v0, p0, Lcom/peel/ui/b/cm;->c:Lcom/peel/ui/b/a/a;

    invoke-virtual {v0}, Lcom/peel/ui/b/a/a;->b()Lcom/peel/content/listing/LiveListing;

    move-result-object v0

    .line 42
    invoke-virtual {v0}, Lcom/peel/content/listing/LiveListing;->x()Ljava/lang/String;

    move-result-object v0

    .line 44
    iget-object v2, v1, Lcom/peel/ui/b/co;->a:Landroid/widget/TextView;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 45
    iget-object v0, v1, Lcom/peel/ui/b/co;->a:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getLayout()Landroid/text/Layout;

    move-result-object v0

    .line 46
    if-eqz v0, :cond_1

    .line 47
    invoke-virtual {v0}, Landroid/text/Layout;->getLineCount()I

    move-result v2

    .line 48
    if-lez v2, :cond_1

    .line 49
    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v0, v2}, Landroid/text/Layout;->getEllipsisCount(I)I

    move-result v0

    if-lez v0, :cond_1

    .line 50
    iget-object v0, v1, Lcom/peel/ui/b/co;->b:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 55
    :cond_1
    iget-object v0, v1, Lcom/peel/ui/b/co;->b:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_2

    .line 56
    iget-object v0, v1, Lcom/peel/ui/b/co;->b:Landroid/view/View;

    new-instance v2, Lcom/peel/ui/b/cn;

    invoke-direct {v2, p0, v1}, Lcom/peel/ui/b/cn;-><init>(Lcom/peel/ui/b/cm;Lcom/peel/ui/b/co;)V

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 67
    :cond_2
    return-object p2

    .line 32
    :cond_3
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/ui/b/co;

    move-object v1, v0

    goto :goto_0
.end method
