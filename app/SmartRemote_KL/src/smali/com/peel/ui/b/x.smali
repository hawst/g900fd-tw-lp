.class Lcom/peel/ui/b/x;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/peel/ui/b/w;


# direct methods
.method constructor <init>(Lcom/peel/ui/b/w;)V
    .locals 0

    .prologue
    .line 208
    iput-object p1, p0, Lcom/peel/ui/b/x;->a:Lcom/peel/ui/b/w;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 211
    iget-object v0, p0, Lcom/peel/ui/b/x;->a:Lcom/peel/ui/b/w;

    iget-object v0, v0, Lcom/peel/ui/b/w;->a:Lcom/peel/ui/b/v;

    iget-object v0, v0, Lcom/peel/ui/b/v;->a:Lcom/peel/ui/model/TwittData;

    invoke-virtual {v0}, Lcom/peel/ui/model/TwittData;->g()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 212
    iget-object v0, p0, Lcom/peel/ui/b/x;->a:Lcom/peel/ui/b/w;

    iget-object v0, v0, Lcom/peel/ui/b/w;->a:Lcom/peel/ui/b/v;

    iget-object v0, v0, Lcom/peel/ui/b/v;->b:Landroid/widget/TextView;

    sget v1, Lcom/peel/ui/fo;->favorite_icon_press:I

    invoke-virtual {v0, v1, v2, v2, v2}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    .line 213
    invoke-static {}, Lcom/peel/util/a/f;->a()Lcom/peel/util/a/f;

    move-result-object v1

    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v0}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const/16 v2, 0x4c1

    iget-object v3, p0, Lcom/peel/ui/b/x;->a:Lcom/peel/ui/b/w;

    iget-object v3, v3, Lcom/peel/ui/b/w;->a:Lcom/peel/ui/b/v;

    iget-object v3, v3, Lcom/peel/ui/b/v;->c:Lcom/peel/ui/b/r;

    .line 214
    invoke-static {v3}, Lcom/peel/ui/b/r;->c(Lcom/peel/ui/b/r;)I

    move-result v3

    .line 213
    invoke-virtual {v1, v0, v2, v3}, Lcom/peel/util/a/f;->a(III)V

    .line 218
    :goto_1
    return-void

    .line 213
    :cond_0
    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v0}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/data/at;->f()I

    move-result v0

    goto :goto_0

    .line 216
    :cond_1
    iget-object v0, p0, Lcom/peel/ui/b/x;->a:Lcom/peel/ui/b/w;

    iget-object v0, v0, Lcom/peel/ui/b/w;->a:Lcom/peel/ui/b/v;

    iget-object v0, v0, Lcom/peel/ui/b/v;->b:Landroid/widget/TextView;

    sget v1, Lcom/peel/ui/fo;->favorite_icon_normal:I

    invoke-virtual {v0, v1, v2, v2, v2}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    goto :goto_1
.end method
