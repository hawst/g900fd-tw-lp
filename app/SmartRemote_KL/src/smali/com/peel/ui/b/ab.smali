.class Lcom/peel/ui/b/ab;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/peel/ui/b/r;


# direct methods
.method constructor <init>(Lcom/peel/ui/b/r;)V
    .locals 0

    .prologue
    .line 299
    iput-object p1, p0, Lcom/peel/ui/b/ab;->a:Lcom/peel/ui/b/r;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 302
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 303
    const-string/jumbo v1, "passback_clazz"

    iget-object v2, p0, Lcom/peel/ui/b/ab;->a:Lcom/peel/ui/b/r;

    invoke-static {v2}, Lcom/peel/ui/b/r;->e(Lcom/peel/ui/b/r;)Landroid/support/v4/app/ae;

    move-result-object v2

    invoke-static {v2}, Lcom/peel/d/e;->a(Landroid/support/v4/app/ae;)Lcom/peel/d/u;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 304
    const-string/jumbo v1, "keyword"

    iget-object v2, p0, Lcom/peel/ui/b/ab;->a:Lcom/peel/ui/b/r;

    invoke-static {v2}, Lcom/peel/ui/b/r;->d(Lcom/peel/ui/b/r;)[Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putStringArray(Ljava/lang/String;[Ljava/lang/String;)V

    .line 305
    const-string/jumbo v1, "context_id"

    iget-object v2, p0, Lcom/peel/ui/b/ab;->a:Lcom/peel/ui/b/r;

    invoke-static {v2}, Lcom/peel/ui/b/r;->c(Lcom/peel/ui/b/r;)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 306
    const-string/jumbo v1, "showid"

    iget-object v2, p0, Lcom/peel/ui/b/ab;->a:Lcom/peel/ui/b/r;

    invoke-static {v2}, Lcom/peel/ui/b/r;->g(Lcom/peel/ui/b/r;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 307
    const-string/jumbo v1, "tmsid"

    iget-object v2, p0, Lcom/peel/ui/b/ab;->a:Lcom/peel/ui/b/r;

    invoke-static {v2}, Lcom/peel/ui/b/r;->h(Lcom/peel/ui/b/r;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 308
    iget-object v1, p0, Lcom/peel/ui/b/ab;->a:Lcom/peel/ui/b/r;

    .line 309
    invoke-static {v1}, Lcom/peel/ui/b/r;->e(Lcom/peel/ui/b/r;)Landroid/support/v4/app/ae;

    move-result-object v1

    const-class v2, Lcom/peel/ui/mo;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    .line 308
    invoke-static {v1, v2, v0}, Lcom/peel/d/e;->c(Landroid/support/v4/app/ae;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 310
    return-void
.end method
