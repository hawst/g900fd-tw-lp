.class Lcom/peel/ui/b/e;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:I

.field final synthetic b:Lcom/peel/ui/b/i;

.field final synthetic c:Lcom/peel/ui/b/c;


# direct methods
.method constructor <init>(Lcom/peel/ui/b/c;ILcom/peel/ui/b/i;)V
    .locals 0

    .prologue
    .line 165
    iput-object p1, p0, Lcom/peel/ui/b/e;->c:Lcom/peel/ui/b/c;

    iput p2, p0, Lcom/peel/ui/b/e;->a:I

    iput-object p3, p0, Lcom/peel/ui/b/e;->b:Lcom/peel/ui/b/i;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 168
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 169
    const-string/jumbo v2, "showId"

    iget-object v0, p0, Lcom/peel/ui/b/e;->c:Lcom/peel/ui/b/c;

    invoke-static {v0}, Lcom/peel/ui/b/c;->a(Lcom/peel/ui/b/c;)Ljava/util/List;

    move-result-object v0

    iget v3, p0, Lcom/peel/ui/b/e;->a:I

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/content/model/Episode;

    invoke-virtual {v0}, Lcom/peel/content/model/Episode;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 170
    const-string/jumbo v2, "showName"

    iget-object v0, p0, Lcom/peel/ui/b/e;->c:Lcom/peel/ui/b/c;

    invoke-static {v0}, Lcom/peel/ui/b/c;->a(Lcom/peel/ui/b/c;)Ljava/util/List;

    move-result-object v0

    iget v3, p0, Lcom/peel/ui/b/e;->a:I

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/content/model/Episode;

    invoke-virtual {v0}, Lcom/peel/content/model/Episode;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 171
    const-string/jumbo v0, "season"

    iget-object v2, p0, Lcom/peel/ui/b/e;->c:Lcom/peel/ui/b/c;

    invoke-static {v2}, Lcom/peel/ui/b/c;->b(Lcom/peel/ui/b/c;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 173
    new-instance v0, Lcom/peel/ui/b/f;

    invoke-direct {v0, p0}, Lcom/peel/ui/b/f;-><init>(Lcom/peel/ui/b/e;)V

    invoke-static {v1, v0}, Lcom/peel/content/a/bw;->a(Landroid/os/Bundle;Lcom/peel/util/t;)V

    .line 185
    return-void
.end method
