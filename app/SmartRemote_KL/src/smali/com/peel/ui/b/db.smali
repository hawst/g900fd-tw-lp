.class Lcom/peel/ui/b/db;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/peel/ui/b/da;


# direct methods
.method constructor <init>(Lcom/peel/ui/b/da;)V
    .locals 0

    .prologue
    .line 63
    iput-object p1, p0, Lcom/peel/ui/b/db;->a:Lcom/peel/ui/b/da;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 66
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 67
    const-string/jumbo v2, "upcoming_list"

    iget-object v0, p0, Lcom/peel/ui/b/db;->a:Lcom/peel/ui/b/da;

    invoke-static {v0}, Lcom/peel/ui/b/da;->a(Lcom/peel/ui/b/da;)Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 68
    const-string/jumbo v0, "contextId"

    iget-object v2, p0, Lcom/peel/ui/b/db;->a:Lcom/peel/ui/b/da;

    iget v2, v2, Lcom/peel/ui/b/da;->b:I

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 69
    iget-object v0, p0, Lcom/peel/ui/b/db;->a:Lcom/peel/ui/b/da;

    .line 70
    invoke-static {v0}, Lcom/peel/ui/b/da;->b(Lcom/peel/ui/b/da;)Landroid/support/v4/app/ae;

    move-result-object v0

    const-class v2, Lcom/peel/ui/b/cv;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    .line 69
    invoke-static {v0, v2, v1}, Lcom/peel/d/e;->c(Landroid/support/v4/app/ae;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 71
    return-void
.end method
