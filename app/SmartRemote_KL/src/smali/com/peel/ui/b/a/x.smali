.class Lcom/peel/ui/b/a/x;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field final synthetic a:Landroid/widget/ListView;

.field final synthetic b:Ljava/util/concurrent/atomic/AtomicInteger;

.field final synthetic c:Ljava/util/List;

.field final synthetic d:Landroid/content/Context;

.field final synthetic e:Lcom/peel/ui/b/a/a;


# direct methods
.method constructor <init>(Lcom/peel/ui/b/a/a;Landroid/widget/ListView;Ljava/util/concurrent/atomic/AtomicInteger;Ljava/util/List;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 592
    iput-object p1, p0, Lcom/peel/ui/b/a/x;->e:Lcom/peel/ui/b/a/a;

    iput-object p2, p0, Lcom/peel/ui/b/a/x;->a:Landroid/widget/ListView;

    iput-object p3, p0, Lcom/peel/ui/b/a/x;->b:Ljava/util/concurrent/atomic/AtomicInteger;

    iput-object p4, p0, Lcom/peel/ui/b/a/x;->c:Ljava/util/List;

    iput-object p5, p0, Lcom/peel/ui/b/a/x;->d:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 597
    iget-object v0, p0, Lcom/peel/ui/b/a/x;->a:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/peel/ui/b/a/x;->b:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 598
    if-eqz v0, :cond_0

    .line 599
    sget v1, Lcom/peel/ui/fp;->checkbox:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckedTextView;

    invoke-virtual {v0, v2}, Landroid/widget/CheckedTextView;->setChecked(Z)V

    .line 602
    :cond_0
    sget v0, Lcom/peel/ui/fp;->checkbox:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckedTextView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/CheckedTextView;->setChecked(Z)V

    .line 603
    iget-object v0, p0, Lcom/peel/ui/b/a/x;->b:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0, p3}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V

    .line 605
    invoke-static {}, Lcom/peel/content/a;->e()Ljava/util/Map;

    move-result-object v1

    iget-object v0, p0, Lcom/peel/ui/b/a/x;->c:Ljava/util/List;

    .line 606
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/content/listing/LiveListing;

    invoke-virtual {v0}, Lcom/peel/content/listing/LiveListing;->d()Ljava/lang/String;

    move-result-object v0

    .line 605
    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 607
    iget-object v1, p0, Lcom/peel/ui/b/a/x;->d:Landroid/content/Context;

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/peel/ui/b/a/x;->c:Ljava/util/List;

    invoke-interface {v0, p3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/content/listing/LiveListing;

    .line 608
    invoke-virtual {v0}, Lcom/peel/content/listing/LiveListing;->d()Ljava/lang/String;

    move-result-object v0

    .line 607
    :cond_1
    invoke-static {v1, v0}, Lcom/peel/util/bx;->e(Landroid/content/Context;Ljava/lang/String;)V

    .line 609
    iget-object v1, p0, Lcom/peel/ui/b/a/x;->e:Lcom/peel/ui/b/a/a;

    iget-object v0, p0, Lcom/peel/ui/b/a/x;->c:Ljava/util/List;

    invoke-interface {v0, p3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/content/listing/LiveListing;

    invoke-static {v1, v0}, Lcom/peel/ui/b/a/a;->a(Lcom/peel/ui/b/a/a;Lcom/peel/content/listing/LiveListing;)V

    .line 611
    iget-object v0, p0, Lcom/peel/ui/b/a/x;->c:Ljava/util/List;

    invoke-interface {v0, p3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/content/listing/LiveListing;

    invoke-static {v0}, Lcom/peel/util/bx;->a(Lcom/peel/content/listing/LiveListing;)V

    .line 613
    iget-object v0, p0, Lcom/peel/ui/b/a/x;->e:Lcom/peel/ui/b/a/a;

    iget-object v0, v0, Lcom/peel/ui/b/a/a;->d:Lcom/peel/widget/ag;

    invoke-virtual {v0}, Lcom/peel/widget/ag;->dismiss()V

    .line 614
    return-void
.end method
