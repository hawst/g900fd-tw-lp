.class public Lcom/peel/ui/b/cl;
.super Lcom/peel/d/u;


# instance fields
.field private e:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Lcom/peel/d/u;-><init>()V

    return-void
.end method


# virtual methods
.method public Z()V
    .locals 6

    .prologue
    .line 41
    iget-object v0, p0, Lcom/peel/ui/b/cl;->d:Lcom/peel/d/a;

    if-nez v0, :cond_0

    .line 42
    new-instance v0, Lcom/peel/d/a;

    sget-object v1, Lcom/peel/d/d;->b:Lcom/peel/d/d;

    sget-object v2, Lcom/peel/d/b;->a:Lcom/peel/d/b;

    sget-object v3, Lcom/peel/d/c;->b:Lcom/peel/d/c;

    sget v4, Lcom/peel/ui/ft;->synopsis:I

    invoke-virtual {p0, v4}, Lcom/peel/ui/b/cl;->a(I)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, Lcom/peel/d/a;-><init>(Lcom/peel/d/d;Lcom/peel/d/b;Lcom/peel/d/c;Ljava/lang/String;Ljava/util/List;)V

    iput-object v0, p0, Lcom/peel/ui/b/cl;->d:Lcom/peel/d/a;

    .line 45
    :cond_0
    iget-object v0, p0, Lcom/peel/ui/b/cl;->b:Lcom/peel/d/i;

    iget-object v1, p0, Lcom/peel/ui/b/cl;->d:Lcom/peel/d/a;

    invoke-virtual {v0, v1}, Lcom/peel/d/i;->a(Lcom/peel/d/a;)V

    .line 46
    return-void
.end method

.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    .line 27
    sget v0, Lcom/peel/ui/fq;->show_synopsis_layout:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 28
    sget v0, Lcom/peel/ui/fp;->desc:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/peel/ui/b/cl;->e:Landroid/widget/TextView;

    .line 29
    return-object v1
.end method

.method public d(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 22
    invoke-super {p0, p1}, Lcom/peel/d/u;->d(Landroid/os/Bundle;)V

    .line 23
    return-void
.end method

.method public h(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 34
    invoke-super {p0, p1}, Lcom/peel/d/u;->h(Landroid/os/Bundle;)V

    .line 36
    iget-object v0, p0, Lcom/peel/ui/b/cl;->e:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/peel/ui/b/cl;->c:Landroid/os/Bundle;

    const-string/jumbo v2, "desc"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 37
    return-void
.end method
