.class public Lcom/peel/ui/b/a/a;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/view/View$OnClickListener;


# static fields
.field private static final l:Ljava/lang/String;


# instance fields
.field public a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/peel/content/listing/Listing;",
            ">;"
        }
    .end annotation
.end field

.field public b:Lcom/peel/d/i;

.field protected c:Landroid/support/v4/app/ae;

.field public d:Lcom/peel/widget/ag;

.field public e:Lcom/peel/widget/ag;

.field public f:Lcom/peel/widget/ag;

.field public g:Lcom/peel/widget/ag;

.field public h:I

.field public i:Ljava/lang/String;

.field public j:Landroid/view/LayoutInflater;

.field protected k:Lcom/peel/content/library/LiveLibrary;

.field private m:Landroid/view/View;

.field private n:Landroid/app/ProgressDialog;

.field private o:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/peel/ui/model/TwittData;",
            ">;"
        }
    .end annotation
.end field

.field private p:Landroid/animation/ObjectAnimator;

.field private q:Lcom/peel/util/t;

.field private r:Lcom/peel/util/ak;

.field private s:Landroid/widget/Toast;

.field private t:Lcom/peel/util/t;

.field private u:Z

.field private v:[Ljava/lang/String;

.field private w:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 85
    const-class v0, Lcom/peel/ui/b/a/a;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/peel/ui/b/a/a;->l:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/support/v4/app/ae;Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 117
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 118
    invoke-static {}, Lcom/peel/d/i;->a()Lcom/peel/d/i;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/ui/b/a/a;->b:Lcom/peel/d/i;

    .line 119
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/ui/b/a/a;->j:Landroid/view/LayoutInflater;

    .line 120
    iput-object p1, p0, Lcom/peel/ui/b/a/a;->c:Landroid/support/v4/app/ae;

    .line 121
    iput-object p2, p0, Lcom/peel/ui/b/a/a;->m:Landroid/view/View;

    .line 123
    invoke-static {p1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string/jumbo v1, "config_legacy"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/peel/util/bx;->b(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    .line 125
    const-string/jumbo v0, "live"

    invoke-static {v0}, Lcom/peel/content/a;->c(Ljava/lang/String;)Lcom/peel/content/library/Library;

    move-result-object v0

    check-cast v0, Lcom/peel/content/library/LiveLibrary;

    iput-object v0, p0, Lcom/peel/ui/b/a/a;->k:Lcom/peel/content/library/LiveLibrary;

    .line 126
    const-string/jumbo v0, "iso"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/ui/b/a/a;->i:Ljava/lang/String;

    .line 127
    iget-object v0, p0, Lcom/peel/ui/b/a/a;->k:Lcom/peel/content/library/LiveLibrary;

    invoke-virtual {v0}, Lcom/peel/content/library/LiveLibrary;->e()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/peel/util/al;->a(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/peel/ui/b/a/a;->u:Z

    .line 129
    iget-object v0, p0, Lcom/peel/ui/b/a/a;->k:Lcom/peel/content/library/LiveLibrary;

    invoke-virtual {v0}, Lcom/peel/content/library/LiveLibrary;->e()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/peel/util/al;->b(Ljava/lang/String;)Lcom/peel/util/ak;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/ui/b/a/a;->r:Lcom/peel/util/ak;

    .line 130
    sget v0, Lcom/peel/ui/ft;->recording_request:I

    const/4 v1, 0x1

    invoke-static {p1, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/ui/b/a/a;->s:Landroid/widget/Toast;

    .line 131
    iget-object v0, p0, Lcom/peel/ui/b/a/a;->s:Landroid/widget/Toast;

    const/16 v1, 0x11

    invoke-virtual {v0, v1, v3, v3}, Landroid/widget/Toast;->setGravity(III)V

    .line 133
    const-string/jumbo v0, "US"

    iget-object v1, p0, Lcom/peel/ui/b/a/a;->i:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 134
    iget-object v0, p0, Lcom/peel/ui/b/a/a;->r:Lcom/peel/util/ak;

    if-nez v0, :cond_0

    .line 135
    iget-object v0, p0, Lcom/peel/ui/b/a/a;->k:Lcom/peel/content/library/LiveLibrary;

    invoke-virtual {v0}, Lcom/peel/content/library/LiveLibrary;->e()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lcom/peel/ui/b/a/b;

    invoke-direct {v1, p0}, Lcom/peel/ui/b/a/b;-><init>(Lcom/peel/ui/b/a/a;)V

    invoke-static {v0, v1}, Lcom/peel/util/al;->a(Ljava/lang/String;Lcom/peel/util/t;)V

    .line 146
    :cond_0
    return-void
.end method

.method static synthetic a(Lcom/peel/ui/b/a/a;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lcom/peel/ui/b/a/a;->o:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic a(Lcom/peel/ui/b/a/a;Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 0

    .prologue
    .line 84
    iput-object p1, p0, Lcom/peel/ui/b/a/a;->o:Ljava/util/ArrayList;

    return-object p1
.end method

.method private a(JLcom/peel/content/listing/Listing;Ljava/lang/String;Ljava/lang/String;)V
    .locals 13

    .prologue
    .line 815
    sget-object v0, Lcom/peel/ui/b/a/a;->l:Ljava/lang/String;

    const-string/jumbo v1, "future show, use cloud recording API"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 816
    sget-object v0, Lcom/peel/util/x;->j:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/text/SimpleDateFormat;

    new-instance v1, Ljava/util/Date;

    invoke-direct {v1, p1, p2}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v0, v1}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v2, p3

    .line 817
    check-cast v2, Lcom/peel/content/listing/LiveListing;

    .line 819
    new-instance v0, Lcom/peel/widget/ag;

    iget-object v1, p0, Lcom/peel/ui/b/a/a;->c:Landroid/support/v4/app/ae;

    invoke-direct {v0, v1}, Lcom/peel/widget/ag;-><init>(Landroid/content/Context;)V

    sget v1, Lcom/peel/ui/ft;->schedule_recording:I

    .line 820
    invoke-virtual {v0, v1}, Lcom/peel/widget/ag;->a(I)Lcom/peel/widget/ag;

    move-result-object v0

    sget v1, Lcom/peel/ui/ft;->schedule_recodring_msg:I

    .line 821
    invoke-virtual {v0, v1}, Lcom/peel/widget/ag;->b(I)Lcom/peel/widget/ag;

    move-result-object v7

    sget v8, Lcom/peel/ui/ft;->record_series:I

    new-instance v0, Lcom/peel/ui/b/a/f;

    move-object v1, p0

    move-object/from16 v3, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/peel/ui/b/a/f;-><init>(Lcom/peel/ui/b/a/a;Lcom/peel/content/listing/LiveListing;Lcom/peel/content/listing/Listing;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 822
    invoke-virtual {v7, v8, v0}, Lcom/peel/widget/ag;->a(ILandroid/content/DialogInterface$OnClickListener;)Lcom/peel/widget/ag;

    move-result-object v0

    sget v1, Lcom/peel/ui/ft;->record_episode:I

    new-instance v5, Lcom/peel/ui/b/a/c;

    move-object v6, p0

    move-object/from16 v7, p3

    move-object v8, v4

    move-object/from16 v9, p4

    move-object/from16 v10, p5

    move-object v11, v2

    invoke-direct/range {v5 .. v11}, Lcom/peel/ui/b/a/c;-><init>(Lcom/peel/ui/b/a/a;Lcom/peel/content/listing/Listing;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/peel/content/listing/LiveListing;)V

    .line 847
    invoke-virtual {v0, v1, v5}, Lcom/peel/widget/ag;->c(ILandroid/content/DialogInterface$OnClickListener;)Lcom/peel/widget/ag;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/ui/b/a/a;->g:Lcom/peel/widget/ag;

    .line 874
    iget-object v0, p0, Lcom/peel/ui/b/a/a;->g:Lcom/peel/widget/ag;

    iget-object v1, p0, Lcom/peel/ui/b/a/a;->g:Lcom/peel/widget/ag;

    invoke-virtual {v0, v1}, Lcom/peel/widget/ag;->a(Lcom/peel/widget/ag;)Lcom/peel/widget/ag;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/widget/ag;->d()Lcom/peel/widget/ag;

    .line 875
    return-void
.end method

.method private a(Landroid/content/Context;Ljava/util/List;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Lcom/peel/content/listing/Listing;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 528
    new-instance v0, Lcom/peel/widget/ag;

    invoke-direct {v0, p1}, Lcom/peel/widget/ag;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/peel/ui/b/a/a;->d:Lcom/peel/widget/ag;

    .line 529
    iget-object v0, p0, Lcom/peel/ui/b/a/a;->j:Landroid/view/LayoutInflater;

    sget v1, Lcom/peel/ui/fq;->show_details_watchons:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v10

    .line 530
    sget v0, Lcom/peel/ui/fp;->watchons:I

    invoke-virtual {v10, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v9, v0

    check-cast v9, Landroid/widget/ListView;

    .line 532
    new-instance v6, Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v0, 0x0

    invoke-direct {v6, v0}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    .line 533
    new-instance v0, Lcom/peel/ui/b/a/w;

    sget v3, Lcom/peel/ui/fq;->show_details_watchon_item:I

    move-object v1, p0

    move-object v2, p1

    move-object v4, p2

    move-object v5, p1

    invoke-direct/range {v0 .. v6}, Lcom/peel/ui/b/a/w;-><init>(Lcom/peel/ui/b/a/a;Landroid/content/Context;ILjava/util/List;Landroid/content/Context;Ljava/util/concurrent/atomic/AtomicInteger;)V

    invoke-virtual {v9, v0}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 592
    new-instance v3, Lcom/peel/ui/b/a/x;

    move-object v4, p0

    move-object v5, v9

    move-object v7, p2

    move-object v8, p1

    invoke-direct/range {v3 .. v8}, Lcom/peel/ui/b/a/x;-><init>(Lcom/peel/ui/b/a/a;Landroid/widget/ListView;Ljava/util/concurrent/atomic/AtomicInteger;Ljava/util/List;Landroid/content/Context;)V

    invoke-virtual {v9, v3}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 616
    iget-object v0, p0, Lcom/peel/ui/b/a/a;->d:Lcom/peel/widget/ag;

    invoke-virtual {v0, v10}, Lcom/peel/widget/ag;->a(Landroid/view/View;)Lcom/peel/widget/ag;

    move-result-object v0

    sget v1, Lcom/peel/ui/ft;->different_episodes_airing:I

    invoke-virtual {v0, v1}, Lcom/peel/widget/ag;->a(I)Lcom/peel/widget/ag;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/widget/ag;->show()V

    .line 617
    return-void
.end method

.method private a(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 687
    invoke-direct {p0}, Lcom/peel/ui/b/a/a;->i()Ljava/util/List;

    move-result-object v0

    .line 688
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    .line 689
    invoke-static {}, Lcom/peel/content/a;->e()Ljava/util/Map;

    move-result-object v1

    const/4 v2, 0x0

    .line 690
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/content/listing/LiveListing;

    invoke-virtual {v0}, Lcom/peel/content/listing/LiveListing;->d()Ljava/lang/String;

    move-result-object v0

    .line 689
    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 691
    sget-object v1, Lcom/peel/ui/b/a/a;->l:Ljava/lang/String;

    const/16 v2, 0xbb8

    invoke-static {p1, v1, v2}, Lcom/peel/util/bx;->a(Landroid/view/View;Ljava/lang/String;I)V

    .line 692
    iget-object v1, p0, Lcom/peel/ui/b/a/a;->c:Landroid/support/v4/app/ae;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/peel/ui/b/a/a;->b()Lcom/peel/content/listing/LiveListing;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/content/listing/LiveListing;->e()Ljava/net/URI;

    move-result-object v0

    .line 693
    invoke-virtual {v0}, Ljava/net/URI;->toString()Ljava/lang/String;

    move-result-object v0

    .line 692
    :goto_0
    invoke-static {v1, v0}, Lcom/peel/util/bx;->e(Landroid/content/Context;Ljava/lang/String;)V

    .line 694
    invoke-virtual {p0}, Lcom/peel/ui/b/a/a;->b()Lcom/peel/content/listing/LiveListing;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/peel/ui/b/a/a;->a(Lcom/peel/content/listing/LiveListing;)V

    .line 695
    invoke-virtual {p0}, Lcom/peel/ui/b/a/a;->b()Lcom/peel/content/listing/LiveListing;

    move-result-object v0

    invoke-static {v0}, Lcom/peel/util/bx;->a(Lcom/peel/content/listing/LiveListing;)V

    .line 699
    :goto_1
    return-void

    .line 693
    :cond_0
    invoke-virtual {p0}, Lcom/peel/ui/b/a/a;->b()Lcom/peel/content/listing/LiveListing;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/peel/content/listing/LiveListing;->a(Ljava/lang/String;)Ljava/net/URI;

    move-result-object v0

    invoke-virtual {v0}, Ljava/net/URI;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 697
    :cond_1
    iget-object v1, p0, Lcom/peel/ui/b/a/a;->c:Landroid/support/v4/app/ae;

    invoke-direct {p0, v1, v0}, Lcom/peel/ui/b/a/a;->a(Landroid/content/Context;Ljava/util/List;)V

    goto :goto_1
.end method

.method private a(Lcom/peel/content/listing/Listing;)V
    .locals 11

    .prologue
    const/4 v6, 0x0

    .line 729
    instance-of v0, p1, Lcom/peel/content/listing/LiveListing;

    if-eqz v0, :cond_1

    move-object v0, p1

    check-cast v0, Lcom/peel/content/listing/LiveListing;

    invoke-virtual {v0}, Lcom/peel/content/listing/LiveListing;->o()J

    move-result-wide v2

    .line 730
    :goto_0
    invoke-virtual {p1}, Lcom/peel/content/listing/Listing;->j()J

    move-result-wide v0

    .line 731
    add-long/2addr v0, v2

    .line 733
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    cmp-long v4, v4, v0

    if-lez v4, :cond_2

    .line 812
    :cond_0
    :goto_1
    return-void

    .line 729
    :cond_1
    const-wide/16 v2, 0x0

    goto :goto_0

    .line 735
    :cond_2
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    cmp-long v4, v4, v2

    if-lez v4, :cond_3

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    cmp-long v0, v4, v0

    if-ltz v0, :cond_0

    .line 751
    :cond_3
    iget-boolean v0, p0, Lcom/peel/ui/b/a/a;->u:Z

    if-eqz v0, :cond_0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    cmp-long v0, v2, v0

    if-lez v0, :cond_0

    .line 753
    iget-object v0, p0, Lcom/peel/ui/b/a/a;->c:Landroid/support/v4/app/ae;

    const-string/jumbo v1, "private_prefs"

    const/4 v4, 0x0

    invoke-virtual {v0, v1, v4}, Landroid/support/v4/app/ae;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 756
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/peel/ui/b/a/a;->k:Lcom/peel/content/library/LiveLibrary;

    invoke-virtual {v4}, Lcom/peel/content/library/LiveLibrary;->e()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v4, "_username"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/peel/ui/b/a/a;->k:Lcom/peel/content/library/LiveLibrary;

    invoke-virtual {v4}, Lcom/peel/content/library/LiveLibrary;->e()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v4, "_password"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 758
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/peel/ui/b/a/a;->k:Lcom/peel/content/library/LiveLibrary;

    invoke-virtual {v4}, Lcom/peel/content/library/LiveLibrary;->e()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v4, "_username"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, v6}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/peel/ui/b/a/a;->k:Lcom/peel/content/library/LiveLibrary;

    invoke-virtual {v4}, Lcom/peel/content/library/LiveLibrary;->e()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v4, "_password"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, v6}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    move-object v1, p0

    move-object v4, p1

    invoke-direct/range {v1 .. v6}, Lcom/peel/ui/b/a/a;->b(JLcom/peel/content/listing/Listing;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 760
    :cond_4
    iget-object v0, p0, Lcom/peel/ui/b/a/a;->j:Landroid/view/LayoutInflater;

    sget v1, Lcom/peel/ui/fq;->dtv_login_form:I

    invoke-virtual {v0, v1, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v7

    .line 761
    new-instance v0, Lcom/peel/widget/ag;

    iget-object v1, p0, Lcom/peel/ui/b/a/a;->c:Landroid/support/v4/app/ae;

    invoke-direct {v0, v1}, Lcom/peel/widget/ag;-><init>(Landroid/content/Context;)V

    iget-object v1, p0, Lcom/peel/ui/b/a/a;->c:Landroid/support/v4/app/ae;

    invoke-virtual {v1}, Landroid/support/v4/app/ae;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v4, Lcom/peel/ui/ft;->future_recording:I

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/peel/widget/ag;->a(Ljava/lang/CharSequence;)Lcom/peel/widget/ag;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/ui/b/a/a;->e:Lcom/peel/widget/ag;

    .line 762
    iget-object v0, p0, Lcom/peel/ui/b/a/a;->e:Lcom/peel/widget/ag;

    invoke-virtual {v0, v7}, Lcom/peel/widget/ag;->a(Landroid/view/View;)Lcom/peel/widget/ag;

    .line 763
    iget-object v0, p0, Lcom/peel/ui/b/a/a;->e:Lcom/peel/widget/ag;

    iget-object v1, p0, Lcom/peel/ui/b/a/a;->e:Lcom/peel/widget/ag;

    invoke-virtual {v0, v1}, Lcom/peel/widget/ag;->a(Lcom/peel/widget/ag;)Lcom/peel/widget/ag;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/widget/ag;->d()Lcom/peel/widget/ag;

    .line 764
    iget-object v0, p0, Lcom/peel/ui/b/a/a;->e:Lcom/peel/widget/ag;

    sget v1, Lcom/peel/ui/ft;->directtv_login:I

    new-instance v5, Lcom/peel/ui/b/a/y;

    move-object v6, p0

    move-wide v8, v2

    move-object v10, p1

    invoke-direct/range {v5 .. v10}, Lcom/peel/ui/b/a/y;-><init>(Lcom/peel/ui/b/a/a;Landroid/view/View;JLcom/peel/content/listing/Listing;)V

    invoke-virtual {v0, v1, v5}, Lcom/peel/widget/ag;->a(ILandroid/content/DialogInterface$OnClickListener;)Lcom/peel/widget/ag;

    goto/16 :goto_1
.end method

.method private a(Lcom/peel/content/listing/LiveListing;)V
    .locals 5

    .prologue
    .line 703
    invoke-virtual {p1}, Lcom/peel/content/listing/LiveListing;->n()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "live"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 704
    iget-object v0, p0, Lcom/peel/ui/b/a/a;->c:Landroid/support/v4/app/ae;

    iget-object v1, p0, Lcom/peel/ui/b/a/a;->m:Landroid/view/View;

    invoke-virtual {p1}, Lcom/peel/content/listing/LiveListing;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/peel/content/listing/LiveListing;->c()Ljava/lang/String;

    move-result-object v3

    iget v4, p0, Lcom/peel/ui/b/a/a;->h:I

    invoke-static {v0, v1, v2, v3, v4}, Lcom/peel/util/bx;->a(Landroid/content/Context;Landroid/view/View;Ljava/lang/String;Ljava/lang/String;I)V

    .line 707
    :cond_0
    iget v0, p0, Lcom/peel/ui/b/a/a;->h:I

    invoke-static {p1, v0}, Lcom/peel/util/bx;->b(Lcom/peel/content/listing/Listing;I)V

    .line 708
    return-void
.end method

.method static synthetic a(Lcom/peel/ui/b/a/a;JLcom/peel/content/listing/Listing;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 84
    invoke-direct/range {p0 .. p5}, Lcom/peel/ui/b/a/a;->b(JLcom/peel/content/listing/Listing;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic a(Lcom/peel/ui/b/a/a;Lcom/peel/content/listing/LiveListing;)V
    .locals 0

    .prologue
    .line 84
    invoke-direct {p0, p1}, Lcom/peel/ui/b/a/a;->a(Lcom/peel/content/listing/LiveListing;)V

    return-void
.end method

.method public static a(Ljava/lang/String;Landroid/support/v4/app/ae;)V
    .locals 3

    .prologue
    .line 956
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 957
    const-string/jumbo v1, "id"

    invoke-virtual {v0, v1, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 958
    const-string/jumbo v1, "show_id"

    invoke-virtual {v0, v1, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 959
    const-string/jumbo v1, "movetotop"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 960
    const-class v1, Lcom/peel/ui/b/av;

    .line 961
    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    .line 960
    invoke-static {p1, v1, v0}, Lcom/peel/d/e;->b(Landroid/support/v4/app/ae;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 962
    return-void
.end method

.method static synthetic a(Lcom/peel/ui/b/a/a;Z)Z
    .locals 0

    .prologue
    .line 84
    iput-boolean p1, p0, Lcom/peel/ui/b/a/a;->u:Z

    return p1
.end method

.method static synthetic a(Lcom/peel/ui/b/a/a;[Ljava/lang/String;)[Ljava/lang/String;
    .locals 0

    .prologue
    .line 84
    iput-object p1, p0, Lcom/peel/ui/b/a/a;->v:[Ljava/lang/String;

    return-object p1
.end method

.method static synthetic b(Lcom/peel/ui/b/a/a;)Landroid/animation/ObjectAnimator;
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lcom/peel/ui/b/a/a;->p:Landroid/animation/ObjectAnimator;

    return-object v0
.end method

.method private b(JLcom/peel/content/listing/Listing;Ljava/lang/String;Ljava/lang/String;)V
    .locals 9

    .prologue
    .line 882
    move-object v2, p3

    check-cast v2, Lcom/peel/content/listing/LiveListing;

    .line 885
    iget-object v0, p0, Lcom/peel/ui/b/a/a;->r:Lcom/peel/util/ak;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/peel/ui/b/a/a;->r:Lcom/peel/util/ak;

    iget-boolean v0, v0, Lcom/peel/util/ak;->a:Z

    if-eqz v0, :cond_1

    .line 886
    invoke-virtual {v2}, Lcom/peel/content/listing/LiveListing;->w()Ljava/lang/String;

    move-result-object v0

    .line 885
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 886
    invoke-virtual {v2}, Lcom/peel/content/listing/LiveListing;->A()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 887
    :cond_0
    invoke-direct/range {p0 .. p5}, Lcom/peel/ui/b/a/a;->a(JLcom/peel/content/listing/Listing;Ljava/lang/String;Ljava/lang/String;)V

    .line 913
    :goto_0
    return-void

    .line 889
    :cond_1
    sget-object v0, Lcom/peel/util/x;->j:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/text/SimpleDateFormat;

    new-instance v1, Ljava/util/Date;

    invoke-direct {v1, p1, p2}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v0, v1}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v4

    .line 890
    iget-object v0, p0, Lcom/peel/ui/b/a/a;->s:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 891
    sget-object v7, Lcom/peel/ui/b/a/a;->l:Ljava/lang/String;

    const-string/jumbo v8, "recording episode"

    new-instance v0, Lcom/peel/ui/b/a/i;

    move-object v1, p0

    move-object v3, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/peel/ui/b/a/i;-><init>(Lcom/peel/ui/b/a/a;Lcom/peel/content/listing/LiveListing;Lcom/peel/content/listing/Listing;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v7, v8, v0}, Lcom/peel/util/i;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    goto :goto_0
.end method

.method static synthetic c(Lcom/peel/ui/b/a/a;)Lcom/peel/util/t;
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lcom/peel/ui/b/a/a;->q:Lcom/peel/util/t;

    return-object v0
.end method

.method static synthetic d(Lcom/peel/ui/b/a/a;)Lcom/peel/util/t;
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lcom/peel/ui/b/a/a;->t:Lcom/peel/util/t;

    return-object v0
.end method

.method static synthetic e(Lcom/peel/ui/b/a/a;)Landroid/widget/Toast;
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lcom/peel/ui/b/a/a;->s:Landroid/widget/Toast;

    return-object v0
.end method

.method static synthetic h()Ljava/lang/String;
    .locals 1

    .prologue
    .line 84
    sget-object v0, Lcom/peel/ui/b/a/a;->l:Ljava/lang/String;

    return-object v0
.end method

.method private i()Ljava/util/List;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/peel/content/listing/Listing;",
            ">;"
        }
    .end annotation

    .prologue
    .line 620
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 624
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    .line 625
    new-instance v6, Ljava/util/HashMap;

    invoke-direct {v6}, Ljava/util/HashMap;-><init>()V

    .line 627
    iget-object v0, p0, Lcom/peel/ui/b/a/a;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/content/listing/Listing;

    move-object v1, v0

    .line 628
    check-cast v1, Lcom/peel/content/listing/LiveListing;

    .line 630
    invoke-virtual {v1}, Lcom/peel/content/listing/LiveListing;->o()J

    move-result-wide v8

    invoke-virtual {v1}, Lcom/peel/content/listing/LiveListing;->o()J

    move-result-wide v10

    .line 631
    invoke-virtual {v1}, Lcom/peel/content/listing/LiveListing;->j()J

    move-result-wide v12

    add-long/2addr v10, v12

    .line 630
    invoke-virtual {p0, v8, v9, v10, v11}, Lcom/peel/ui/b/a/a;->a(JJ)I

    move-result v4

    if-eqz v4, :cond_0

    .line 635
    invoke-virtual {v1}, Lcom/peel/content/listing/LiveListing;->d()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 638
    invoke-virtual {v1}, Lcom/peel/content/listing/LiveListing;->d()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 641
    invoke-virtual {v1}, Lcom/peel/content/listing/LiveListing;->g()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v6, v4}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 642
    invoke-virtual {v1}, Lcom/peel/content/listing/LiveListing;->g()Ljava/lang/String;

    move-result-object v4

    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v6, v4, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 645
    :cond_1
    invoke-virtual {v1}, Lcom/peel/content/listing/LiveListing;->g()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v6, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 648
    :cond_2
    invoke-interface {v6}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_3
    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 649
    invoke-interface {v6, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    .line 651
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_8

    .line 652
    iget-object v0, p0, Lcom/peel/ui/b/a/a;->k:Lcom/peel/content/library/LiveLibrary;

    invoke-virtual {v0}, Lcom/peel/content/library/LiveLibrary;->i()[Lcom/peel/data/Channel;

    move-result-object v8

    .line 653
    const/4 v2, -0x1

    const/4 v3, -0x1

    .line 655
    array-length v9, v8

    const/4 v0, 0x0

    move v4, v0

    :goto_2
    if-ge v4, v9, :cond_3

    aget-object v10, v8, v4

    .line 656
    invoke-virtual {v10}, Lcom/peel/data/Channel;->a()Ljava/lang/String;

    move-result-object v11

    const/4 v0, 0x0

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/content/listing/LiveListing;

    invoke-virtual {v0}, Lcom/peel/content/listing/LiveListing;->q()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v11, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 657
    invoke-virtual {v10}, Lcom/peel/data/Channel;->i()I

    move-result v0

    move v2, v0

    .line 660
    :cond_4
    invoke-virtual {v10}, Lcom/peel/data/Channel;->a()Ljava/lang/String;

    move-result-object v11

    const/4 v0, 0x1

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/content/listing/LiveListing;

    invoke-virtual {v0}, Lcom/peel/content/listing/LiveListing;->q()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v11, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 661
    invoke-virtual {v10}, Lcom/peel/data/Channel;->i()I

    move-result v0

    .line 664
    :goto_3
    const/4 v3, -0x1

    if-le v2, v3, :cond_7

    const/4 v3, -0x1

    if-le v0, v3, :cond_7

    .line 665
    if-eq v2, v0, :cond_6

    .line 666
    const/4 v0, 0x1

    if-ne v2, v0, :cond_5

    .line 667
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 669
    :cond_5
    const/4 v0, 0x1

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 672
    :cond_6
    invoke-interface {v5, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_1

    .line 655
    :cond_7
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    move v3, v0

    goto :goto_2

    .line 679
    :cond_8
    invoke-interface {v6, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    invoke-interface {v5, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_1

    .line 683
    :cond_9
    return-object v5

    :cond_a
    move v0, v3

    goto :goto_3
.end method


# virtual methods
.method public a(JJ)I
    .locals 7

    .prologue
    const-wide/16 v4, 0x0

    const/4 v0, 0x0

    .line 715
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 717
    cmp-long v1, p1, v4

    if-eqz v1, :cond_0

    cmp-long v1, p3, v4

    if-nez v1, :cond_1

    .line 724
    :cond_0
    :goto_0
    return v0

    .line 718
    :cond_1
    cmp-long v1, v2, p1

    if-lez v1, :cond_2

    cmp-long v1, v2, p3

    if-gez v1, :cond_2

    .line 719
    const/4 v0, 0x1

    goto :goto_0

    .line 720
    :cond_2
    const-wide/32 v4, 0x1b7740

    add-long/2addr v2, v4

    cmp-long v1, p1, v2

    if-gtz v1, :cond_0

    .line 721
    const/4 v0, 0x2

    goto :goto_0
.end method

.method public a(I)V
    .locals 0

    .prologue
    .line 435
    iput p1, p0, Lcom/peel/ui/b/a/a;->h:I

    .line 436
    return-void
.end method

.method public a(Lcom/peel/util/t;)V
    .locals 0

    .prologue
    .line 153
    iput-object p1, p0, Lcom/peel/ui/b/a/a;->q:Lcom/peel/util/t;

    .line 154
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 160
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 162
    iget-object v2, p0, Lcom/peel/ui/b/a/a;->c:Landroid/support/v4/app/ae;

    invoke-static {v2}, Lcom/peel/d/v;->a(Landroid/content/Context;)Lcom/peel/d/v;

    move-result-object v2

    invoke-virtual {v2}, Lcom/peel/d/v;->b()Lcom/peel/d/af;

    move-result-object v2

    if-nez v2, :cond_0

    .line 163
    sget-object v2, Lcom/peel/ui/b/a/a;->l:Ljava/lang/String;

    const-string/jumbo v3, "get twitter list"

    new-instance v4, Lcom/peel/ui/b/a/l;

    invoke-direct {v4, p0, p1, v0, v1}, Lcom/peel/ui/b/a/l;-><init>(Lcom/peel/ui/b/a/a;Ljava/lang/String;J)V

    invoke-static {v2, v3, v4}, Lcom/peel/util/i;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    .line 348
    :goto_0
    return-void

    .line 269
    :cond_0
    new-instance v0, Lretrofit/client/OkClient;

    invoke-direct {v0}, Lretrofit/client/OkClient;-><init>()V

    invoke-static {v0}, Lcom/peel/social/z;->a(Lretrofit/client/Client;)Lcom/peel/social/c;

    move-result-object v0

    .line 271
    new-instance v1, Lcom/peel/ui/b/a/o;

    invoke-direct {v1, p0, p1, v0}, Lcom/peel/ui/b/a/o;-><init>(Lcom/peel/ui/b/a/a;Ljava/lang/String;Lcom/peel/social/c;)V

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Void;

    .line 346
    invoke-virtual {v1, v0}, Lcom/peel/ui/b/a/o;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method

.method public a(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/peel/content/listing/Listing;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 149
    iput-object p1, p0, Lcom/peel/ui/b/a/a;->a:Ljava/util/List;

    .line 150
    return-void
.end method

.method public a(J)Z
    .locals 3

    .prologue
    .line 878
    iget-boolean v0, p0, Lcom/peel/ui/b/a/a;->u:Z

    if-eqz v0, :cond_0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    cmp-long v0, p1, v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 351
    iget-object v0, p0, Lcom/peel/ui/b/a/a;->v:[Ljava/lang/String;

    return-object v0
.end method

.method public b()Lcom/peel/content/listing/LiveListing;
    .locals 2

    .prologue
    .line 355
    iget-object v0, p0, Lcom/peel/ui/b/a/a;->a:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/content/listing/LiveListing;

    return-object v0
.end method

.method public b(Lcom/peel/util/t;)V
    .locals 0

    .prologue
    .line 156
    iput-object p1, p0, Lcom/peel/ui/b/a/a;->t:Lcom/peel/util/t;

    .line 157
    return-void
.end method

.method public b(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 711
    iput-object p1, p0, Lcom/peel/ui/b/a/a;->w:Ljava/lang/String;

    .line 712
    return-void
.end method

.method public c()V
    .locals 5

    .prologue
    .line 359
    sget-object v0, Lcom/peel/content/a;->a:Lcom/peel/util/bo;

    invoke-virtual {p0}, Lcom/peel/ui/b/a/a;->b()Lcom/peel/content/listing/LiveListing;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/content/listing/LiveListing;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/peel/util/bo;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/content/model/OVDWrapper;

    .line 361
    if-nez v0, :cond_1

    .line 417
    :cond_0
    :goto_0
    return-void

    .line 363
    :cond_1
    invoke-virtual {v0}, Lcom/peel/content/model/OVDWrapper;->b()Ljava/util/Map;

    move-result-object v1

    .line 365
    if-eqz v1, :cond_0

    invoke-interface {v1}, Ljava/util/Map;->size()I

    move-result v2

    if-lez v2, :cond_0

    .line 366
    new-instance v2, Ljava/util/ArrayList;

    invoke-interface {v1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 368
    new-instance v3, Lcom/peel/ui/b/a/s;

    invoke-direct {v3, p0}, Lcom/peel/ui/b/a/s;-><init>(Lcom/peel/ui/b/a/a;)V

    invoke-static {v2, v3}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 381
    const/4 v3, 0x0

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/peel/content/model/Season;

    .line 383
    invoke-virtual {v1}, Lcom/peel/content/model/Season;->b()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-nez v2, :cond_2

    .line 385
    sget-object v2, Lcom/peel/ui/b/a/a;->l:Ljava/lang/String;

    const-string/jumbo v3, "get episodes from season"

    new-instance v4, Lcom/peel/ui/b/a/t;

    invoke-direct {v4, p0, v0, v1}, Lcom/peel/ui/b/a/t;-><init>(Lcom/peel/ui/b/a/a;Lcom/peel/content/model/OVDWrapper;Lcom/peel/content/model/Season;)V

    invoke-static {v2, v3, v4}, Lcom/peel/util/i;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    goto :goto_0

    .line 408
    :cond_2
    iget-object v0, p0, Lcom/peel/ui/b/a/a;->t:Lcom/peel/util/t;

    if-eqz v0, :cond_0

    .line 409
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 410
    const-string/jumbo v2, "showId"

    invoke-virtual {p0}, Lcom/peel/ui/b/a/a;->b()Lcom/peel/content/listing/LiveListing;

    move-result-object v3

    invoke-virtual {v3}, Lcom/peel/content/listing/LiveListing;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 411
    const-string/jumbo v2, "selectedSeason"

    invoke-virtual {v1}, Lcom/peel/content/model/Season;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 413
    iget-object v1, p0, Lcom/peel/ui/b/a/a;->t:Lcom/peel/util/t;

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v0, v3}, Lcom/peel/util/t;->a(ZLjava/lang/Object;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public c(Lcom/peel/util/t;)V
    .locals 7

    .prologue
    .line 916
    invoke-virtual {p0}, Lcom/peel/ui/b/a/a;->b()Lcom/peel/content/listing/LiveListing;

    move-result-object v0

    .line 917
    if-eqz v0, :cond_0

    .line 918
    sget-object v1, Lcom/peel/content/a;->f:Lcom/peel/content/user/User;

    invoke-virtual {v1}, Lcom/peel/content/user/User;->x()Ljava/lang/String;

    move-result-object v1

    .line 919
    iget-object v2, p0, Lcom/peel/ui/b/a/a;->k:Lcom/peel/content/library/LiveLibrary;

    invoke-virtual {v2}, Lcom/peel/content/library/LiveLibrary;->e()Ljava/lang/String;

    move-result-object v2

    .line 920
    sget-object v3, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v3}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v3

    invoke-virtual {v3}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v3

    invoke-virtual {v3}, Lcom/peel/data/at;->f()I

    move-result v3

    .line 922
    invoke-virtual {v0}, Lcom/peel/content/listing/LiveListing;->b()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Lcom/peel/ui/b/a/k;

    const/4 v6, 0x1

    invoke-direct {v5, p0, v6, v0, p1}, Lcom/peel/ui/b/a/k;-><init>(Lcom/peel/ui/b/a/a;ILcom/peel/content/listing/LiveListing;Lcom/peel/util/t;)V

    invoke-static {v4, v2, v1, v3, v5}, Lcom/peel/content/a/j;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILcom/peel/util/t;)V

    .line 946
    :cond_0
    return-void
.end method

.method public d()V
    .locals 3

    .prologue
    .line 420
    iget-object v0, p0, Lcom/peel/ui/b/a/a;->n:Landroid/app/ProgressDialog;

    if-nez v0, :cond_1

    .line 421
    new-instance v0, Landroid/app/ProgressDialog;

    iget-object v1, p0, Lcom/peel/ui/b/a/a;->c:Landroid/support/v4/app/ae;

    sget v2, Lcom/peel/ui/fu;->DialogTheme:I

    invoke-direct {v0, v1, v2}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/peel/ui/b/a/a;->n:Landroid/app/ProgressDialog;

    .line 422
    iget-object v0, p0, Lcom/peel/ui/b/a/a;->n:Landroid/app/ProgressDialog;

    iget-object v1, p0, Lcom/peel/ui/b/a/a;->c:Landroid/support/v4/app/ae;

    sget v2, Lcom/peel/ui/ft;->please_wait:I

    invoke-virtual {v1, v2}, Landroid/support/v4/app/ae;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 423
    iget-object v0, p0, Lcom/peel/ui/b/a/a;->n:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    .line 427
    :cond_0
    :goto_0
    return-void

    .line 424
    :cond_1
    iget-object v0, p0, Lcom/peel/ui/b/a/a;->n:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 425
    iget-object v0, p0, Lcom/peel/ui/b/a/a;->n:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    goto :goto_0
.end method

.method public d(Lcom/peel/util/t;)V
    .locals 3

    .prologue
    .line 949
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 950
    const-string/jumbo v1, "country"

    iget-object v2, p0, Lcom/peel/ui/b/a/a;->i:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 951
    const-string/jumbo v1, "showId"

    invoke-virtual {p0}, Lcom/peel/ui/b/a/a;->b()Lcom/peel/content/listing/LiveListing;

    move-result-object v2

    invoke-virtual {v2}, Lcom/peel/content/listing/LiveListing;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 952
    iget-object v1, p0, Lcom/peel/ui/b/a/a;->k:Lcom/peel/content/library/LiveLibrary;

    const/16 v2, 0x9

    invoke-virtual {v1, v0, v2, p1}, Lcom/peel/content/library/LiveLibrary;->a(Landroid/os/Bundle;ILcom/peel/util/t;)V

    .line 953
    return-void
.end method

.method public e()V
    .locals 1

    .prologue
    .line 431
    iget-object v0, p0, Lcom/peel/ui/b/a/a;->n:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/peel/ui/b/a/a;->n:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/peel/ui/b/a/a;->n:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 432
    :cond_0
    return-void
.end method

.method public f()V
    .locals 1

    .prologue
    .line 439
    iget-object v0, p0, Lcom/peel/ui/b/a/a;->d:Lcom/peel/widget/ag;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/peel/ui/b/a/a;->d:Lcom/peel/widget/ag;

    invoke-virtual {v0}, Lcom/peel/widget/ag;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 440
    iget-object v0, p0, Lcom/peel/ui/b/a/a;->d:Lcom/peel/widget/ag;

    invoke-virtual {v0}, Lcom/peel/widget/ag;->dismiss()V

    .line 443
    :cond_0
    iget-object v0, p0, Lcom/peel/ui/b/a/a;->f:Lcom/peel/widget/ag;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/peel/ui/b/a/a;->f:Lcom/peel/widget/ag;

    invoke-virtual {v0}, Lcom/peel/widget/ag;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 444
    iget-object v0, p0, Lcom/peel/ui/b/a/a;->f:Lcom/peel/widget/ag;

    invoke-virtual {v0}, Lcom/peel/widget/ag;->dismiss()V

    .line 446
    :cond_1
    iget-object v0, p0, Lcom/peel/ui/b/a/a;->e:Lcom/peel/widget/ag;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/peel/ui/b/a/a;->e:Lcom/peel/widget/ag;

    invoke-virtual {v0}, Lcom/peel/widget/ag;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 447
    iget-object v0, p0, Lcom/peel/ui/b/a/a;->e:Lcom/peel/widget/ag;

    invoke-virtual {v0}, Lcom/peel/widget/ag;->dismiss()V

    .line 449
    :cond_2
    iget-object v0, p0, Lcom/peel/ui/b/a/a;->g:Lcom/peel/widget/ag;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/peel/ui/b/a/a;->g:Lcom/peel/widget/ag;

    invoke-virtual {v0}, Lcom/peel/widget/ag;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 450
    iget-object v0, p0, Lcom/peel/ui/b/a/a;->g:Lcom/peel/widget/ag;

    invoke-virtual {v0}, Lcom/peel/widget/ag;->dismiss()V

    .line 452
    :cond_3
    return-void
.end method

.method public g()V
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v5, 0x0

    .line 498
    sget-object v0, Lcom/peel/content/a;->e:Ljava/lang/String;

    const-string/jumbo v1, "CN"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 499
    iget-object v2, p0, Lcom/peel/ui/b/a/a;->c:Landroid/support/v4/app/ae;

    iget-object v0, p0, Lcom/peel/ui/b/a/a;->a:Ljava/util/List;

    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/content/listing/Listing;

    iget-object v1, p0, Lcom/peel/ui/b/a/a;->a:Ljava/util/List;

    invoke-interface {v1, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/peel/content/listing/Listing;

    invoke-static {v1}, Lcom/peel/util/bx;->d(Lcom/peel/content/listing/Listing;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v0, v1}, Lcom/peel/util/bx;->a(Landroid/content/Context;Lcom/peel/content/listing/Listing;Ljava/lang/String;)V

    .line 501
    invoke-static {}, Lcom/peel/util/a/f;->a()Lcom/peel/util/a/f;

    move-result-object v0

    sget-object v1, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v1}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v1

    if-nez v1, :cond_0

    move v1, v8

    :goto_0
    const/16 v2, 0x4d5

    iget v3, p0, Lcom/peel/ui/b/a/a;->h:I

    iget-object v4, p0, Lcom/peel/ui/b/a/a;->a:Ljava/util/List;

    .line 503
    invoke-interface {v4, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/peel/content/listing/Listing;

    invoke-static {v4}, Lcom/peel/util/bx;->b(Lcom/peel/content/listing/Listing;)Ljava/lang/String;

    move-result-object v4

    iget-object v6, p0, Lcom/peel/ui/b/a/a;->a:Ljava/util/List;

    invoke-interface {v6, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/peel/content/listing/Listing;

    invoke-virtual {v6}, Lcom/peel/content/listing/Listing;->h()Ljava/lang/String;

    move-result-object v6

    move v7, v5

    .line 501
    invoke-virtual/range {v0 .. v7}, Lcom/peel/util/a/f;->a(IIILjava/lang/String;ILjava/lang/String;I)V

    .line 505
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "reminder_updated"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 506
    const-string/jumbo v1, "selective"

    invoke-virtual {v0, v1, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 507
    iget-object v1, p0, Lcom/peel/ui/b/a/a;->c:Landroid/support/v4/app/ae;

    invoke-static {v1}, Landroid/support/v4/a/q;->a(Landroid/content/Context;)Landroid/support/v4/a/q;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/support/v4/a/q;->a(Landroid/content/Intent;)Z

    .line 525
    :goto_1
    return-void

    .line 501
    :cond_0
    sget-object v1, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v1}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/data/at;->f()I

    move-result v1

    goto :goto_0

    .line 509
    :cond_1
    iget-object v0, p0, Lcom/peel/ui/b/a/a;->a:Ljava/util/List;

    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/content/listing/Listing;

    iget-object v1, p0, Lcom/peel/ui/b/a/a;->c:Landroid/support/v4/app/ae;

    new-instance v2, Lcom/peel/ui/b/a/v;

    invoke-direct {v2, p0}, Lcom/peel/ui/b/a/v;-><init>(Lcom/peel/ui/b/a/a;)V

    invoke-static {v0, v1, v2}, Lcom/peel/util/bx;->a(Lcom/peel/content/listing/Listing;Landroid/content/Context;Lcom/peel/util/t;)V

    goto :goto_1
.end method

.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v1, 0x0

    .line 456
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    .line 458
    sget v2, Lcom/peel/ui/fp;->refresh_btn:I

    if-ne v0, v2, :cond_1

    .line 459
    invoke-virtual {p1, v1}, Landroid/view/View;->setEnabled(Z)V

    .line 461
    const-string/jumbo v0, "rotation"

    const/4 v1, 0x2

    new-array v1, v1, [F

    fill-array-data v1, :array_0

    invoke-static {p1, v0, v1}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/ui/b/a/a;->p:Landroid/animation/ObjectAnimator;

    .line 462
    iget-object v0, p0, Lcom/peel/ui/b/a/a;->p:Landroid/animation/ObjectAnimator;

    const-wide/16 v2, 0x12c

    invoke-virtual {v0, v2, v3}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 463
    iget-object v0, p0, Lcom/peel/ui/b/a/a;->p:Landroid/animation/ObjectAnimator;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->setRepeatCount(I)V

    .line 464
    iget-object v0, p0, Lcom/peel/ui/b/a/a;->p:Landroid/animation/ObjectAnimator;

    new-instance v1, Landroid/view/animation/LinearInterpolator;

    invoke-direct {v1}, Landroid/view/animation/LinearInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 465
    iget-object v0, p0, Lcom/peel/ui/b/a/a;->p:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    .line 467
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/peel/ui/b/a/a;->a(Ljava/lang/String;)V

    .line 495
    :cond_0
    :goto_0
    return-void

    .line 468
    :cond_1
    sget v2, Lcom/peel/ui/fp;->watch_on_tv:I

    if-ne v0, v2, :cond_3

    .line 469
    invoke-static {}, Lcom/peel/util/bx;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 470
    invoke-direct {p0, p1}, Lcom/peel/ui/b/a/a;->a(Landroid/view/View;)V

    .line 471
    iget-object v0, p0, Lcom/peel/ui/b/a/a;->w:Ljava/lang/String;

    invoke-static {v0}, Lcom/peel/util/bq;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 473
    :cond_2
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 474
    const-string/jumbo v2, "control_only_mode"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 475
    const-string/jumbo v1, "passback_clazz"

    iget-object v2, p0, Lcom/peel/ui/b/a/a;->c:Landroid/support/v4/app/ae;

    invoke-static {v2}, Lcom/peel/d/e;->a(Landroid/support/v4/app/ae;)Lcom/peel/d/u;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 476
    const-string/jumbo v1, "passback_bundle"

    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 477
    iget-object v1, p0, Lcom/peel/ui/b/a/a;->b:Lcom/peel/d/i;

    invoke-virtual {v1}, Lcom/peel/d/i;->c()Landroid/support/v4/app/ae;

    move-result-object v1

    const-class v2, Lcom/peel/i/dr;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lcom/peel/d/e;->c(Landroid/support/v4/app/ae;Ljava/lang/String;Landroid/os/Bundle;)V

    goto :goto_0

    .line 479
    :cond_3
    sget v2, Lcom/peel/ui/fp;->set_reminder:I

    if-ne v0, v2, :cond_8

    .line 480
    sget-object v0, Lcom/peel/content/a;->e:Ljava/lang/String;

    const-string/jumbo v2, "CN"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/peel/ui/b/a/a;->a:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/content/listing/Listing;

    invoke-static {v0}, Lcom/peel/util/bx;->d(Lcom/peel/content/listing/Listing;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_5

    :cond_4
    sget-object v0, Lcom/peel/content/a;->e:Ljava/lang/String;

    const-string/jumbo v2, "CN"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_6

    iget-object v0, p0, Lcom/peel/ui/b/a/a;->a:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/content/listing/Listing;

    invoke-static {v0}, Lcom/peel/util/bx;->e(Lcom/peel/content/listing/Listing;)Z

    move-result v0

    if-eqz v0, :cond_6

    :cond_5
    const/4 v0, 0x1

    .line 482
    :goto_1
    if-eqz v0, :cond_7

    .line 483
    invoke-virtual {p0}, Lcom/peel/ui/b/a/a;->g()V

    goto/16 :goto_0

    :cond_6
    move v0, v1

    .line 480
    goto :goto_1

    .line 485
    :cond_7
    iget-object v0, p0, Lcom/peel/ui/b/a/a;->a:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/content/listing/Listing;

    iget-object v1, p0, Lcom/peel/ui/b/a/a;->b:Lcom/peel/d/i;

    invoke-virtual {v1}, Lcom/peel/d/i;->c()Landroid/support/v4/app/ae;

    move-result-object v1

    iget v2, p0, Lcom/peel/ui/b/a/a;->h:I

    invoke-static {v0, v1, v2, v3, v3}, Lcom/peel/util/bx;->a(Lcom/peel/content/listing/Listing;Landroid/support/v4/app/ae;ILjava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 488
    :cond_8
    sget v1, Lcom/peel/ui/fp;->record_btn:I

    if-ne v0, v1, :cond_0

    .line 489
    invoke-virtual {p0}, Lcom/peel/ui/b/a/a;->b()Lcom/peel/content/listing/LiveListing;

    move-result-object v0

    .line 491
    if-eqz v0, :cond_0

    .line 492
    invoke-direct {p0, v0}, Lcom/peel/ui/b/a/a;->a(Lcom/peel/content/listing/Listing;)V

    goto/16 :goto_0

    .line 461
    nop

    :array_0
    .array-data 4
        0x0
        0x43340000    # 180.0f
    .end array-data
.end method
