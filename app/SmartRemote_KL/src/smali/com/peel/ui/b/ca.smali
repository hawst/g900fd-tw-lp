.class Lcom/peel/ui/b/ca;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/peel/ui/b/bz;


# direct methods
.method constructor <init>(Lcom/peel/ui/b/bz;)V
    .locals 0

    .prologue
    .line 59
    iput-object p1, p0, Lcom/peel/ui/b/ca;->a:Lcom/peel/ui/b/bz;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 62
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 63
    const-string/jumbo v1, "showId"

    iget-object v2, p0, Lcom/peel/ui/b/ca;->a:Lcom/peel/ui/b/bz;

    invoke-static {v2}, Lcom/peel/ui/b/bz;->a(Lcom/peel/ui/b/bz;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 64
    const-string/jumbo v1, "seasons"

    iget-object v2, p0, Lcom/peel/ui/b/ca;->a:Lcom/peel/ui/b/bz;

    invoke-static {v2}, Lcom/peel/ui/b/bz;->b(Lcom/peel/ui/b/bz;)Landroid/os/Bundle;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 65
    const-string/jumbo v1, "selectedSeason"

    iget-object v2, p0, Lcom/peel/ui/b/ca;->a:Lcom/peel/ui/b/bz;

    invoke-static {v2}, Lcom/peel/ui/b/bz;->c(Lcom/peel/ui/b/bz;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 66
    iget-object v1, p0, Lcom/peel/ui/b/ca;->a:Lcom/peel/ui/b/bz;

    iget-object v1, v1, Lcom/peel/ui/b/bz;->a:Lcom/peel/d/i;

    invoke-virtual {v1}, Lcom/peel/d/i;->c()Landroid/support/v4/app/ae;

    move-result-object v1

    const-class v2, Lcom/peel/ui/b/bq;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lcom/peel/d/e;->c(Landroid/support/v4/app/ae;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 67
    return-void
.end method
