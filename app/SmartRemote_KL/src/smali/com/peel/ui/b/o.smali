.class public Lcom/peel/ui/b/o;
.super Lcom/peel/ui/b/af;


# instance fields
.field private c:Lcom/peel/ui/b/a/a;


# direct methods
.method public constructor <init>(Lcom/peel/ui/b/a/a;)V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Lcom/peel/ui/b/af;-><init>()V

    .line 25
    iput-object p1, p0, Lcom/peel/ui/b/o;->c:Lcom/peel/ui/b/a/a;

    .line 26
    return-void
.end method

.method private a(Lcom/peel/ui/b/p;)V
    .locals 12

    .prologue
    const/16 v11, 0xff

    const/16 v10, 0x40

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 58
    iget-object v0, p0, Lcom/peel/ui/b/o;->c:Lcom/peel/ui/b/a/a;

    invoke-virtual {v0}, Lcom/peel/ui/b/a/a;->b()Lcom/peel/content/listing/LiveListing;

    move-result-object v3

    .line 59
    invoke-virtual {v3}, Lcom/peel/content/listing/LiveListing;->o()J

    move-result-wide v4

    .line 60
    invoke-virtual {v3}, Lcom/peel/content/listing/LiveListing;->j()J

    move-result-wide v6

    .line 61
    add-long/2addr v6, v4

    .line 63
    iget-object v0, p0, Lcom/peel/ui/b/o;->c:Lcom/peel/ui/b/a/a;

    invoke-virtual {v0, v4, v5, v6, v7}, Lcom/peel/ui/b/a/a;->a(JJ)I

    move-result v0

    .line 65
    if-nez v0, :cond_2

    .line 66
    iget-object v0, p1, Lcom/peel/ui/b/p;->a:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 67
    iget-object v0, p1, Lcom/peel/ui/b/p;->a:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getCompoundDrawables()[Landroid/graphics/drawable/Drawable;

    move-result-object v0

    aget-object v0, v0, v1

    invoke-virtual {v0, v10}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 74
    :goto_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    const-wide/32 v8, 0x1b7740

    add-long/2addr v6, v8

    cmp-long v0, v4, v6

    if-lez v0, :cond_7

    move v0, v1

    .line 78
    :goto_1
    sget-object v6, Lcom/peel/content/a;->e:Ljava/lang/String;

    const-string/jumbo v7, "CN"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-static {v3}, Lcom/peel/util/bx;->d(Lcom/peel/content/listing/Listing;)Ljava/lang/String;

    move-result-object v6

    if-nez v6, :cond_1

    :cond_0
    sget-object v6, Lcom/peel/content/a;->e:Ljava/lang/String;

    const-string/jumbo v7, "CN"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_3

    invoke-static {v3}, Lcom/peel/util/bx;->e(Lcom/peel/content/listing/Listing;)Z

    move-result v3

    if-eqz v3, :cond_3

    :cond_1
    move v3, v1

    .line 80
    :goto_2
    if-eqz v3, :cond_4

    .line 81
    iget-object v0, p1, Lcom/peel/ui/b/p;->b:Landroid/widget/TextView;

    sget v3, Lcom/peel/ui/fo;->undo_reminder_stateful:I

    invoke-virtual {v0, v2, v3, v2, v2}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    .line 82
    iget-object v0, p1, Lcom/peel/ui/b/p;->b:Landroid/widget/TextView;

    sget v3, Lcom/peel/ui/ft;->undo_reminder:I

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(I)V

    .line 95
    :goto_3
    iget-object v0, p0, Lcom/peel/ui/b/o;->c:Lcom/peel/ui/b/a/a;

    invoke-virtual {v0, v4, v5}, Lcom/peel/ui/b/a/a;->a(J)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 96
    iget-object v0, p1, Lcom/peel/ui/b/p;->c:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 97
    iget-object v0, p1, Lcom/peel/ui/b/p;->c:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getCompoundDrawables()[Landroid/graphics/drawable/Drawable;

    move-result-object v0

    aget-object v0, v0, v1

    invoke-virtual {v0, v11}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 102
    :goto_4
    return-void

    .line 69
    :cond_2
    iget-object v0, p1, Lcom/peel/ui/b/p;->a:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 70
    iget-object v0, p1, Lcom/peel/ui/b/p;->a:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getCompoundDrawables()[Landroid/graphics/drawable/Drawable;

    move-result-object v0

    aget-object v0, v0, v1

    invoke-virtual {v0, v11}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    goto :goto_0

    :cond_3
    move v3, v2

    .line 78
    goto :goto_2

    .line 84
    :cond_4
    iget-object v3, p1, Lcom/peel/ui/b/p;->b:Landroid/widget/TextView;

    sget v6, Lcom/peel/ui/ft;->title_set_reminder:I

    invoke-virtual {v3, v6}, Landroid/widget/TextView;->setText(I)V

    .line 85
    if-eqz v0, :cond_5

    .line 86
    iget-object v0, p1, Lcom/peel/ui/b/p;->b:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 87
    iget-object v0, p1, Lcom/peel/ui/b/p;->b:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getCompoundDrawables()[Landroid/graphics/drawable/Drawable;

    move-result-object v0

    aget-object v0, v0, v1

    invoke-virtual {v0, v11}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 88
    iget-object v0, p1, Lcom/peel/ui/b/p;->b:Landroid/widget/TextView;

    sget v3, Lcom/peel/ui/fo;->remind_me_btn_stateful:I

    invoke-virtual {v0, v2, v3, v2, v2}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    goto :goto_3

    .line 90
    :cond_5
    iget-object v0, p1, Lcom/peel/ui/b/p;->b:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 91
    iget-object v0, p1, Lcom/peel/ui/b/p;->b:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getCompoundDrawables()[Landroid/graphics/drawable/Drawable;

    move-result-object v0

    aget-object v0, v0, v1

    invoke-virtual {v0, v10}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    goto :goto_3

    .line 99
    :cond_6
    iget-object v0, p1, Lcom/peel/ui/b/p;->c:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 100
    iget-object v0, p1, Lcom/peel/ui/b/p;->c:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getCompoundDrawables()[Landroid/graphics/drawable/Drawable;

    move-result-object v0

    aget-object v0, v0, v1

    invoke-virtual {v0, v10}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    goto :goto_4

    :cond_7
    move v0, v2

    goto/16 :goto_1
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 21
    const/4 v0, 0x1

    return v0
.end method

.method public a(Landroid/view/LayoutInflater;Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 5

    .prologue
    const/16 v4, 0xff

    const/4 v3, 0x1

    .line 30
    if-nez p2, :cond_1

    new-instance v0, Lcom/peel/ui/b/p;

    invoke-direct {v0, p0}, Lcom/peel/ui/b/p;-><init>(Lcom/peel/ui/b/o;)V

    move-object v1, v0

    .line 32
    :goto_0
    if-nez p2, :cond_0

    .line 33
    sget v0, Lcom/peel/ui/fq;->overview_btn_layout:I

    const/4 v2, 0x0

    invoke-virtual {p1, v0, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 34
    sget v0, Lcom/peel/ui/fp;->watch_on_tv:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lcom/peel/ui/b/p;->a:Landroid/widget/TextView;

    .line 35
    sget v0, Lcom/peel/ui/fp;->set_reminder:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lcom/peel/ui/b/p;->b:Landroid/widget/TextView;

    .line 36
    sget v0, Lcom/peel/ui/fp;->record_btn:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lcom/peel/ui/b/p;->c:Landroid/widget/TextView;

    .line 38
    invoke-virtual {p2, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 41
    :cond_0
    iget-object v0, v1, Lcom/peel/ui/b/p;->a:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 42
    iget-object v0, v1, Lcom/peel/ui/b/p;->b:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 43
    iget-object v0, v1, Lcom/peel/ui/b/p;->c:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 44
    iget-object v0, v1, Lcom/peel/ui/b/p;->a:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getCompoundDrawables()[Landroid/graphics/drawable/Drawable;

    move-result-object v0

    aget-object v0, v0, v3

    invoke-virtual {v0, v4}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 45
    iget-object v0, v1, Lcom/peel/ui/b/p;->b:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getCompoundDrawables()[Landroid/graphics/drawable/Drawable;

    move-result-object v0

    aget-object v0, v0, v3

    invoke-virtual {v0, v4}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 46
    iget-object v0, v1, Lcom/peel/ui/b/p;->c:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getCompoundDrawables()[Landroid/graphics/drawable/Drawable;

    move-result-object v0

    aget-object v0, v0, v3

    invoke-virtual {v0, v4}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 48
    invoke-direct {p0, v1}, Lcom/peel/ui/b/o;->a(Lcom/peel/ui/b/p;)V

    .line 50
    iget-object v0, v1, Lcom/peel/ui/b/p;->a:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/peel/ui/b/o;->c:Lcom/peel/ui/b/a/a;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 51
    iget-object v0, v1, Lcom/peel/ui/b/p;->b:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/peel/ui/b/o;->c:Lcom/peel/ui/b/a/a;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 52
    iget-object v0, v1, Lcom/peel/ui/b/p;->c:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/peel/ui/b/o;->c:Lcom/peel/ui/b/a/a;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 54
    return-object p2

    .line 30
    :cond_1
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/ui/b/p;

    move-object v1, v0

    goto :goto_0
.end method
