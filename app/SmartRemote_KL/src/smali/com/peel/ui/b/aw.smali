.class Lcom/peel/ui/b/aw;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/peel/ui/b/av;


# direct methods
.method constructor <init>(Lcom/peel/ui/b/av;)V
    .locals 0

    .prologue
    .line 160
    iput-object p1, p0, Lcom/peel/ui/b/aw;->a:Lcom/peel/ui/b/av;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 10

    .prologue
    const/4 v1, 0x0

    const/16 v9, 0x40

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 163
    iget-object v0, p0, Lcom/peel/ui/b/aw;->a:Lcom/peel/ui/b/av;

    invoke-static {v0}, Lcom/peel/ui/b/av;->a(Lcom/peel/ui/b/av;)Lcom/peel/content/listing/LiveListing;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/content/listing/LiveListing;->k()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 165
    iget-object v0, p0, Lcom/peel/ui/b/aw;->a:Lcom/peel/ui/b/av;

    invoke-static {v0}, Lcom/peel/ui/b/av;->a(Lcom/peel/ui/b/av;)Lcom/peel/content/listing/LiveListing;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/content/listing/LiveListing;->k()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v2, "movie"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 166
    iget-object v0, p0, Lcom/peel/ui/b/aw;->a:Lcom/peel/ui/b/av;

    sget v2, Lcom/peel/ui/ft;->movie:I

    invoke-virtual {v0, v2}, Lcom/peel/ui/b/av;->a(I)Ljava/lang/String;

    move-result-object v0

    .line 172
    :goto_0
    if-eqz v0, :cond_0

    iget-object v2, p0, Lcom/peel/ui/b/aw;->a:Lcom/peel/ui/b/av;

    iget-object v3, p0, Lcom/peel/ui/b/aw;->a:Lcom/peel/ui/b/av;

    iget-object v3, v3, Lcom/peel/ui/b/av;->b:Lcom/peel/d/i;

    invoke-static {v2, v3, v0}, Lcom/peel/ui/b/av;->a(Lcom/peel/ui/b/av;Lcom/peel/d/i;Ljava/lang/String;)V

    .line 175
    :cond_0
    iget-object v0, p0, Lcom/peel/ui/b/aw;->a:Lcom/peel/ui/b/av;

    invoke-static {v0}, Lcom/peel/ui/b/av;->b(Lcom/peel/ui/b/av;)Landroid/widget/ImageView;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 176
    iget-object v0, p0, Lcom/peel/ui/b/aw;->a:Lcom/peel/ui/b/av;

    invoke-static {v0}, Lcom/peel/ui/b/av;->a(Lcom/peel/ui/b/av;)Lcom/peel/content/listing/LiveListing;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/content/listing/LiveListing;->m()[Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/peel/ui/b/aw;->a:Lcom/peel/ui/b/av;

    .line 177
    invoke-static {v2}, Lcom/peel/ui/b/av;->a(Lcom/peel/ui/b/av;)Lcom/peel/content/listing/LiveListing;

    move-result-object v2

    invoke-virtual {v2}, Lcom/peel/content/listing/LiveListing;->k()Ljava/lang/String;

    move-result-object v2

    .line 176
    invoke-static {v0, v2}, Lcom/peel/util/bx;->a([Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 179
    iget-object v2, p0, Lcom/peel/ui/b/aw;->a:Lcom/peel/ui/b/av;

    invoke-static {v2}, Lcom/peel/ui/b/av;->a(Lcom/peel/ui/b/av;)Lcom/peel/content/listing/LiveListing;

    move-result-object v2

    invoke-virtual {v2}, Lcom/peel/content/listing/LiveListing;->z()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_9

    .line 180
    iget-object v2, p0, Lcom/peel/ui/b/aw;->a:Lcom/peel/ui/b/av;

    invoke-virtual {v2}, Lcom/peel/ui/b/av;->m()Landroid/support/v4/app/ae;

    move-result-object v2

    invoke-static {v2}, Lcom/peel/util/b/e;->a(Landroid/content/Context;)Lcom/squareup/picasso/Picasso;

    move-result-object v2

    const/4 v3, 0x3

    const/4 v4, 0x4

    const/16 v5, 0x21c

    iget-object v6, p0, Lcom/peel/ui/b/aw;->a:Lcom/peel/ui/b/av;

    .line 181
    invoke-static {v6}, Lcom/peel/ui/b/av;->a(Lcom/peel/ui/b/av;)Lcom/peel/content/listing/LiveListing;

    move-result-object v6

    invoke-virtual {v6}, Lcom/peel/content/listing/LiveListing;->B()Ljava/util/Map;

    move-result-object v6

    invoke-static {v3, v4, v5, v6}, Lcom/peel/util/bx;->a(IIILjava/util/Map;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/squareup/picasso/Picasso;->load(Ljava/lang/String;)Lcom/squareup/picasso/RequestCreator;

    move-result-object v2

    .line 182
    invoke-virtual {v2, v0}, Lcom/squareup/picasso/RequestCreator;->placeholder(I)Lcom/squareup/picasso/RequestCreator;

    move-result-object v0

    .line 183
    invoke-virtual {v0}, Lcom/squareup/picasso/RequestCreator;->noFade()Lcom/squareup/picasso/RequestCreator;

    move-result-object v0

    .line 184
    invoke-virtual {v0}, Lcom/squareup/picasso/RequestCreator;->fit()Lcom/squareup/picasso/RequestCreator;

    move-result-object v0

    iget-object v2, p0, Lcom/peel/ui/b/aw;->a:Lcom/peel/ui/b/av;

    .line 185
    invoke-static {v2}, Lcom/peel/ui/b/av;->b(Lcom/peel/ui/b/av;)Landroid/widget/ImageView;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/squareup/picasso/RequestCreator;->into(Landroid/widget/ImageView;)V

    .line 191
    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/peel/ui/b/aw;->a:Lcom/peel/ui/b/av;

    invoke-static {v0}, Lcom/peel/ui/b/av;->c(Lcom/peel/ui/b/av;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 192
    iget-object v0, p0, Lcom/peel/ui/b/aw;->a:Lcom/peel/ui/b/av;

    invoke-static {v0}, Lcom/peel/ui/b/av;->c(Lcom/peel/ui/b/av;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/TextView;->getCompoundDrawables()[Landroid/graphics/drawable/Drawable;

    move-result-object v0

    aget-object v0, v0, v8

    invoke-virtual {v0, v9}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 193
    iget-object v0, p0, Lcom/peel/ui/b/aw;->a:Lcom/peel/ui/b/av;

    invoke-static {v0}, Lcom/peel/ui/b/av;->d(Lcom/peel/ui/b/av;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 194
    iget-object v0, p0, Lcom/peel/ui/b/aw;->a:Lcom/peel/ui/b/av;

    invoke-static {v0}, Lcom/peel/ui/b/av;->d(Lcom/peel/ui/b/av;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/TextView;->getCompoundDrawables()[Landroid/graphics/drawable/Drawable;

    move-result-object v0

    aget-object v0, v0, v8

    invoke-virtual {v0, v9}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 195
    iget-object v0, p0, Lcom/peel/ui/b/aw;->a:Lcom/peel/ui/b/av;

    invoke-static {v0}, Lcom/peel/ui/b/av;->e(Lcom/peel/ui/b/av;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 196
    iget-object v0, p0, Lcom/peel/ui/b/aw;->a:Lcom/peel/ui/b/av;

    invoke-static {v0}, Lcom/peel/ui/b/av;->e(Lcom/peel/ui/b/av;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/TextView;->getCompoundDrawables()[Landroid/graphics/drawable/Drawable;

    move-result-object v0

    aget-object v0, v0, v8

    invoke-virtual {v0, v9}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 198
    iget-object v0, p0, Lcom/peel/ui/b/aw;->a:Lcom/peel/ui/b/av;

    invoke-virtual {v0}, Lcom/peel/ui/b/av;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    invoke-static {v0}, Lcom/peel/util/bx;->c(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 199
    iget-object v2, p0, Lcom/peel/ui/b/aw;->a:Lcom/peel/ui/b/av;

    invoke-static {v2}, Lcom/peel/ui/b/av;->f(Lcom/peel/ui/b/av;)Lcom/peel/ui/b/a/a;

    move-result-object v2

    invoke-virtual {v2}, Lcom/peel/ui/b/a/a;->b()Lcom/peel/content/listing/LiveListing;

    move-result-object v2

    invoke-virtual {v2}, Lcom/peel/content/listing/LiveListing;->b()Ljava/lang/String;

    move-result-object v5

    .line 201
    iget-object v2, p0, Lcom/peel/ui/b/aw;->a:Lcom/peel/ui/b/av;

    invoke-static {v2}, Lcom/peel/ui/b/av;->g(Lcom/peel/ui/b/av;)Lcom/peel/ui/b/q;

    move-result-object v2

    if-eqz v2, :cond_5

    .line 202
    iget-object v2, p0, Lcom/peel/ui/b/aw;->a:Lcom/peel/ui/b/av;

    invoke-static {v2}, Lcom/peel/ui/b/av;->g(Lcom/peel/ui/b/av;)Lcom/peel/ui/b/q;

    move-result-object v2

    new-instance v3, Lcom/peel/ui/b/cd;

    iget-object v4, p0, Lcom/peel/ui/b/aw;->a:Lcom/peel/ui/b/av;

    invoke-static {v4}, Lcom/peel/ui/b/av;->f(Lcom/peel/ui/b/av;)Lcom/peel/ui/b/a/a;

    move-result-object v4

    iget-object v6, p0, Lcom/peel/ui/b/aw;->a:Lcom/peel/ui/b/av;

    invoke-virtual {v6}, Lcom/peel/ui/b/av;->m()Landroid/support/v4/app/ae;

    move-result-object v6

    invoke-direct {v3, v4, v6}, Lcom/peel/ui/b/cd;-><init>(Lcom/peel/ui/b/a/a;Landroid/content/Context;)V

    invoke-virtual {v2, v3}, Lcom/peel/ui/b/q;->a(Lcom/peel/ui/b/af;)V

    .line 204
    iget-object v2, p0, Lcom/peel/ui/b/aw;->a:Lcom/peel/ui/b/av;

    invoke-static {v2}, Lcom/peel/ui/b/av;->a(Lcom/peel/ui/b/av;)Lcom/peel/content/listing/LiveListing;

    move-result-object v2

    invoke-virtual {v2}, Lcom/peel/content/listing/LiveListing;->x()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 205
    iget-object v2, p0, Lcom/peel/ui/b/aw;->a:Lcom/peel/ui/b/av;

    invoke-static {v2}, Lcom/peel/ui/b/av;->g(Lcom/peel/ui/b/av;)Lcom/peel/ui/b/q;

    move-result-object v2

    new-instance v3, Lcom/peel/ui/b/cm;

    iget-object v4, p0, Lcom/peel/ui/b/aw;->a:Lcom/peel/ui/b/av;

    invoke-static {v4}, Lcom/peel/ui/b/av;->f(Lcom/peel/ui/b/av;)Lcom/peel/ui/b/a/a;

    move-result-object v4

    iget-object v6, p0, Lcom/peel/ui/b/aw;->a:Lcom/peel/ui/b/av;

    invoke-virtual {v6}, Lcom/peel/ui/b/av;->m()Landroid/support/v4/app/ae;

    move-result-object v6

    invoke-direct {v3, v4, v6}, Lcom/peel/ui/b/cm;-><init>(Lcom/peel/ui/b/a/a;Landroid/support/v4/app/ae;)V

    invoke-virtual {v2, v3}, Lcom/peel/ui/b/q;->a(Lcom/peel/ui/b/af;)V

    .line 208
    :cond_2
    iget-object v2, p0, Lcom/peel/ui/b/aw;->a:Lcom/peel/ui/b/av;

    invoke-static {v2}, Lcom/peel/ui/b/av;->g(Lcom/peel/ui/b/av;)Lcom/peel/ui/b/q;

    move-result-object v2

    new-instance v3, Lcom/peel/ui/b/o;

    iget-object v4, p0, Lcom/peel/ui/b/aw;->a:Lcom/peel/ui/b/av;

    invoke-static {v4}, Lcom/peel/ui/b/av;->f(Lcom/peel/ui/b/av;)Lcom/peel/ui/b/a/a;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/peel/ui/b/o;-><init>(Lcom/peel/ui/b/a/a;)V

    invoke-virtual {v2, v3}, Lcom/peel/ui/b/q;->a(Lcom/peel/ui/b/af;)V

    .line 210
    if-eqz v0, :cond_3

    const-string/jumbo v2, "china"

    .line 211
    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_4

    const-string/jumbo v2, "cn"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 212
    :cond_3
    iget-object v0, p0, Lcom/peel/ui/b/aw;->a:Lcom/peel/ui/b/av;

    invoke-static {v0}, Lcom/peel/ui/b/av;->g(Lcom/peel/ui/b/av;)Lcom/peel/ui/b/q;

    move-result-object v8

    new-instance v0, Lcom/peel/ui/b/r;

    iget-object v2, p0, Lcom/peel/ui/b/aw;->a:Lcom/peel/ui/b/av;

    invoke-virtual {v2}, Lcom/peel/ui/b/av;->m()Landroid/support/v4/app/ae;

    move-result-object v2

    iget-object v3, p0, Lcom/peel/ui/b/aw;->a:Lcom/peel/ui/b/av;

    .line 213
    invoke-static {v3}, Lcom/peel/ui/b/av;->f(Lcom/peel/ui/b/av;)Lcom/peel/ui/b/a/a;

    move-result-object v3

    invoke-virtual {v3}, Lcom/peel/ui/b/a/a;->a()[Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/peel/ui/b/aw;->a:Lcom/peel/ui/b/av;

    invoke-static {v4}, Lcom/peel/ui/b/av;->h(Lcom/peel/ui/b/av;)I

    move-result v4

    iget-object v6, p0, Lcom/peel/ui/b/aw;->a:Lcom/peel/ui/b/av;

    .line 214
    invoke-static {v6}, Lcom/peel/ui/b/av;->f(Lcom/peel/ui/b/av;)Lcom/peel/ui/b/a/a;

    move-result-object v6

    invoke-virtual {v6}, Lcom/peel/ui/b/a/a;->b()Lcom/peel/content/listing/LiveListing;

    move-result-object v6

    invoke-virtual {v6}, Lcom/peel/content/listing/LiveListing;->g()Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lcom/peel/ui/b/aw;->a:Lcom/peel/ui/b/av;

    invoke-static {v7}, Lcom/peel/ui/b/av;->f(Lcom/peel/ui/b/av;)Lcom/peel/ui/b/a/a;

    move-result-object v7

    invoke-direct/range {v0 .. v7}, Lcom/peel/ui/b/r;-><init>(Ljava/util/List;Landroid/support/v4/app/ae;[Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Lcom/peel/ui/b/a/a;)V

    .line 212
    invoke-virtual {v8, v0}, Lcom/peel/ui/b/q;->a(Lcom/peel/ui/b/af;)V

    .line 216
    iget-object v0, p0, Lcom/peel/ui/b/aw;->a:Lcom/peel/ui/b/av;

    invoke-static {v0}, Lcom/peel/ui/b/av;->f(Lcom/peel/ui/b/av;)Lcom/peel/ui/b/a/a;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/ui/b/aw;->a:Lcom/peel/ui/b/av;

    iget-object v1, v1, Lcom/peel/ui/b/av;->e:Lcom/peel/util/t;

    invoke-virtual {v0, v1}, Lcom/peel/ui/b/a/a;->a(Lcom/peel/util/t;)V

    .line 217
    iget-object v0, p0, Lcom/peel/ui/b/aw;->a:Lcom/peel/ui/b/av;

    invoke-static {v0}, Lcom/peel/ui/b/av;->f(Lcom/peel/ui/b/av;)Lcom/peel/ui/b/a/a;

    move-result-object v0

    invoke-virtual {v0, v5}, Lcom/peel/ui/b/a/a;->a(Ljava/lang/String;)V

    .line 220
    :cond_4
    iget-object v0, p0, Lcom/peel/ui/b/aw;->a:Lcom/peel/ui/b/av;

    invoke-static {v0}, Lcom/peel/ui/b/av;->g(Lcom/peel/ui/b/av;)Lcom/peel/ui/b/q;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/ui/b/q;->notifyDataSetChanged()V

    .line 223
    :cond_5
    iget-object v0, p0, Lcom/peel/ui/b/aw;->a:Lcom/peel/ui/b/av;

    invoke-static {v0}, Lcom/peel/ui/b/av;->i(Lcom/peel/ui/b/av;)V

    .line 224
    iget-object v0, p0, Lcom/peel/ui/b/aw;->a:Lcom/peel/ui/b/av;

    invoke-static {v0}, Lcom/peel/ui/b/av;->j(Lcom/peel/ui/b/av;)V

    .line 227
    iget-object v0, p0, Lcom/peel/ui/b/aw;->a:Lcom/peel/ui/b/av;

    invoke-static {v0}, Lcom/peel/ui/b/av;->k(Lcom/peel/ui/b/av;)Lcom/peel/ui/ScrollPositionListView;

    move-result-object v0

    new-instance v1, Lcom/peel/ui/b/ax;

    invoke-direct {v1, p0}, Lcom/peel/ui/b/ax;-><init>(Lcom/peel/ui/b/aw;)V

    invoke-virtual {v0, v1}, Lcom/peel/ui/ScrollPositionListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 251
    return-void

    .line 167
    :cond_6
    iget-object v0, p0, Lcom/peel/ui/b/aw;->a:Lcom/peel/ui/b/av;

    invoke-static {v0}, Lcom/peel/ui/b/av;->a(Lcom/peel/ui/b/av;)Lcom/peel/content/listing/LiveListing;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/content/listing/LiveListing;->k()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v2, "program"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_7

    iget-object v0, p0, Lcom/peel/ui/b/aw;->a:Lcom/peel/ui/b/av;

    invoke-static {v0}, Lcom/peel/ui/b/av;->a(Lcom/peel/ui/b/av;)Lcom/peel/content/listing/LiveListing;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/content/listing/LiveListing;->k()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v2, "TVSHOW"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 168
    :cond_7
    iget-object v0, p0, Lcom/peel/ui/b/aw;->a:Lcom/peel/ui/b/av;

    sget v2, Lcom/peel/ui/ft;->tvshow:I

    invoke-virtual {v0, v2}, Lcom/peel/ui/b/av;->a(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 169
    :cond_8
    iget-object v0, p0, Lcom/peel/ui/b/aw;->a:Lcom/peel/ui/b/av;

    invoke-static {v0}, Lcom/peel/ui/b/av;->a(Lcom/peel/ui/b/av;)Lcom/peel/content/listing/LiveListing;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/content/listing/LiveListing;->k()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v2, "sports"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 170
    iget-object v0, p0, Lcom/peel/ui/b/aw;->a:Lcom/peel/ui/b/av;

    sget v2, Lcom/peel/ui/ft;->sports:I

    invoke-virtual {v0, v2}, Lcom/peel/ui/b/av;->a(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 187
    :cond_9
    iget-object v2, p0, Lcom/peel/ui/b/aw;->a:Lcom/peel/ui/b/av;

    invoke-static {v2}, Lcom/peel/ui/b/av;->b(Lcom/peel/ui/b/av;)Landroid/widget/ImageView;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_1

    :cond_a
    move-object v0, v1

    goto/16 :goto_0
.end method
