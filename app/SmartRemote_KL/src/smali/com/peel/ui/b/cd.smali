.class public Lcom/peel/ui/b/cd;
.super Lcom/peel/ui/b/af;

# interfaces
.implements Landroid/view/View$OnClickListener;


# static fields
.field private static final c:Ljava/lang/String;


# instance fields
.field private d:Lcom/peel/ui/b/a/a;

.field private e:Landroid/content/Context;

.field private f:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private g:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private h:Ljava/lang/String;

.field private i:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 24
    const-class v0, Lcom/peel/ui/b/cd;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/peel/ui/b/cd;->c:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/peel/ui/b/a/a;Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/peel/ui/b/af;-><init>()V

    .line 31
    iput-object p1, p0, Lcom/peel/ui/b/cd;->d:Lcom/peel/ui/b/a/a;

    .line 32
    iput-object p2, p0, Lcom/peel/ui/b/cd;->e:Landroid/content/Context;

    .line 33
    sget-object v0, Lcom/peel/content/a;->f:Lcom/peel/content/user/User;

    invoke-virtual {v0}, Lcom/peel/content/user/User;->v()Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/ui/b/cd;->f:Ljava/util/Map;

    .line 34
    sget-object v0, Lcom/peel/content/a;->f:Lcom/peel/content/user/User;

    invoke-virtual {v0}, Lcom/peel/content/user/User;->w()Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/ui/b/cd;->g:Ljava/util/Map;

    .line 35
    return-void
.end method

.method static synthetic a(Lcom/peel/ui/b/cd;Ljava/util/Map;)Ljava/util/Map;
    .locals 0

    .prologue
    .line 23
    iput-object p1, p0, Lcom/peel/ui/b/cd;->g:Ljava/util/Map;

    return-object p1
.end method

.method private a(Lcom/peel/ui/b/ck;)V
    .locals 12

    .prologue
    .line 83
    iget-object v0, p0, Lcom/peel/ui/b/cd;->d:Lcom/peel/ui/b/a/a;

    invoke-virtual {v0}, Lcom/peel/ui/b/a/a;->b()Lcom/peel/content/listing/LiveListing;

    move-result-object v2

    .line 84
    invoke-virtual {v2}, Lcom/peel/content/listing/LiveListing;->o()J

    move-result-wide v4

    .line 85
    invoke-virtual {v2}, Lcom/peel/content/listing/LiveListing;->j()J

    move-result-wide v6

    .line 86
    add-long v8, v4, v6

    .line 89
    invoke-virtual {v2}, Lcom/peel/content/listing/LiveListing;->c()Ljava/lang/String;

    move-result-object v3

    .line 91
    if-eqz v3, :cond_1

    .line 92
    invoke-virtual {v2}, Lcom/peel/content/listing/LiveListing;->d()Ljava/lang/String;

    move-result-object v1

    .line 93
    invoke-static {}, Lcom/peel/content/a;->e()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 94
    iget-object v10, p1, Lcom/peel/ui/b/ck;->e:Landroid/widget/TextView;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v11, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v11, " - "

    invoke-virtual {v3, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    if-nez v0, :cond_0

    move-object v0, v1

    :cond_0
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v10, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 95
    iget-object v0, p1, Lcom/peel/ui/b/ck;->e:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 98
    :cond_1
    const-wide/16 v0, 0x0

    cmp-long v0, v4, v0

    if-lez v0, :cond_9

    const-wide/16 v0, 0x0

    cmp-long v0, v8, v0

    if-lez v0, :cond_9

    .line 99
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0, v4, v5}, Ljava/util/Date;-><init>(J)V

    .line 100
    invoke-static {v0}, Lcom/peel/util/x;->a(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    .line 101
    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    .line 102
    const-string/jumbo v3, "Yesterday"

    invoke-virtual {v0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 103
    const-string/jumbo v1, "Yesterday"

    iget-object v3, p0, Lcom/peel/ui/b/cd;->e:Landroid/content/Context;

    sget v8, Lcom/peel/ui/ft;->yesterday:I

    invoke-virtual {v3, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    .line 110
    :cond_2
    :goto_0
    iget-object v1, p1, Lcom/peel/ui/b/ck;->d:Landroid/widget/TextView;

    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 111
    const-string/jumbo v1, ""

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 112
    iget-object v1, p1, Lcom/peel/ui/b/ck;->d:Landroid/widget/TextView;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/peel/ui/b/cd;->e:Landroid/content/Context;

    sget v8, Lcom/peel/ui/ft;->today:I

    invoke-virtual {v3, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v3, " "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v0, Lcom/peel/util/x;->c:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/text/SimpleDateFormat;

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iget-object v4, p0, Lcom/peel/ui/b/cd;->e:Landroid/content/Context;

    invoke-static {v4}, Landroid/text/format/DateFormat;->is24HourFormat(Landroid/content/Context;)Z

    move-result v4

    iget-object v5, p0, Lcom/peel/ui/b/cd;->e:Landroid/content/Context;

    sget v8, Lcom/peel/ui/ft;->time_pattern:I

    invoke-virtual {v5, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v0, v6, v7, v4, v5}, Lcom/peel/util/x;->a(Ljava/lang/String;JZLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 120
    :goto_1
    invoke-virtual {v2}, Lcom/peel/content/listing/LiveListing;->v()Ljava/lang/String;

    move-result-object v0

    .line 121
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 122
    iget-object v0, p1, Lcom/peel/ui/b/ck;->b:Landroid/widget/TextView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 128
    :goto_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 129
    invoke-virtual {v2}, Lcom/peel/content/listing/LiveListing;->w()Ljava/lang/String;

    move-result-object v1

    .line 130
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_3

    const-string/jumbo v3, "0"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 131
    iget-object v3, p0, Lcom/peel/ui/b/cd;->e:Landroid/content/Context;

    sget v4, Lcom/peel/ui/ft;->episode_number:I

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v1, v5, v6

    invoke-virtual {v3, v4, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 134
    :cond_3
    invoke-virtual {v2}, Lcom/peel/content/listing/LiveListing;->A()Ljava/lang/String;

    move-result-object v1

    .line 135
    if-eqz v1, :cond_5

    const-string/jumbo v3, "0"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_5

    .line 136
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v3

    if-lez v3, :cond_4

    const-string/jumbo v3, ", "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 138
    :cond_4
    iget-object v3, p0, Lcom/peel/ui/b/cd;->e:Landroid/content/Context;

    sget v4, Lcom/peel/ui/ft;->season_number:I

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v1, v5, v6

    invoke-virtual {v3, v4, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 141
    :cond_5
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    if-lez v1, :cond_b

    .line 142
    iget-object v1, p1, Lcom/peel/ui/b/ck;->f:Landroid/widget/TextView;

    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 143
    iget-object v1, p1, Lcom/peel/ui/b/ck;->f:Landroid/widget/TextView;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 148
    :goto_3
    invoke-virtual {v2}, Lcom/peel/content/listing/LiveListing;->m()[Ljava/lang/String;

    move-result-object v1

    .line 149
    if-nez v1, :cond_c

    .line 150
    iget-object v0, p1, Lcom/peel/ui/b/ck;->c:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 161
    :goto_4
    invoke-virtual {v2}, Lcom/peel/content/listing/LiveListing;->l()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/ui/b/cd;->i:Ljava/lang/String;

    .line 162
    iget-object v0, p1, Lcom/peel/ui/b/ck;->a:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/peel/ui/b/cd;->i:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 164
    invoke-virtual {v2}, Lcom/peel/content/listing/LiveListing;->b()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/ui/b/cd;->h:Ljava/lang/String;

    .line 165
    return-void

    .line 104
    :cond_6
    const-string/jumbo v3, "Tomorrow"

    invoke-virtual {v0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 105
    const-string/jumbo v1, "Tomorrow"

    iget-object v3, p0, Lcom/peel/ui/b/cd;->e:Landroid/content/Context;

    sget v8, Lcom/peel/ui/ft;->tomorrow:I

    invoke-virtual {v3, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    goto/16 :goto_0

    .line 106
    :cond_7
    sget-object v3, Lcom/peel/util/bx;->p:Ljava/util/Map;

    invoke-interface {v3, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 107
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v8, p0, Lcom/peel/ui/b/cd;->e:Landroid/content/Context;

    sget-object v0, Lcom/peel/util/bx;->p:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v8, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 114
    :cond_8
    iget-object v1, p1, Lcom/peel/ui/b/ck;->d:Landroid/widget/TextView;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v3, " "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v0, Lcom/peel/util/x;->c:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/text/SimpleDateFormat;

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iget-object v4, p0, Lcom/peel/ui/b/cd;->e:Landroid/content/Context;

    invoke-static {v4}, Landroid/text/format/DateFormat;->is24HourFormat(Landroid/content/Context;)Z

    move-result v4

    iget-object v5, p0, Lcom/peel/ui/b/cd;->e:Landroid/content/Context;

    sget v8, Lcom/peel/ui/ft;->time_pattern:I

    invoke-virtual {v5, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v0, v6, v7, v4, v5}, Lcom/peel/util/x;->a(Ljava/lang/String;JZLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 117
    :cond_9
    iget-object v0, p1, Lcom/peel/ui/b/ck;->d:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_1

    .line 124
    :cond_a
    iget-object v1, p1, Lcom/peel/ui/b/ck;->b:Landroid/widget/TextView;

    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 125
    iget-object v1, p1, Lcom/peel/ui/b/ck;->b:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    .line 145
    :cond_b
    iget-object v0, p1, Lcom/peel/ui/b/ck;->f:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_3

    .line 152
    :cond_c
    iget-object v0, p1, Lcom/peel/ui/b/ck;->c:Landroid/widget/TextView;

    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 153
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 154
    const/4 v0, 0x0

    :goto_5
    array-length v4, v1

    if-ge v0, v4, :cond_e

    .line 155
    aget-object v4, v1, v0

    iget-object v5, p0, Lcom/peel/ui/b/cd;->e:Landroid/content/Context;

    invoke-static {v4, v5}, Lcom/peel/util/bx;->a(Ljava/lang/String;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 156
    array-length v4, v1

    add-int/lit8 v4, v4, -0x1

    if-ge v0, v4, :cond_d

    const-string/jumbo v4, ", "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 154
    :cond_d
    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    .line 158
    :cond_e
    iget-object v0, p1, Lcom/peel/ui/b/ck;->c:Landroid/widget/TextView;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_4
.end method

.method static synthetic b(Lcom/peel/ui/b/cd;Ljava/util/Map;)Ljava/util/Map;
    .locals 0

    .prologue
    .line 23
    iput-object p1, p0, Lcom/peel/ui/b/cd;->f:Ljava/util/Map;

    return-object p1
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 39
    const/4 v0, 0x0

    return v0
.end method

.method public a(Landroid/view/LayoutInflater;Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4

    .prologue
    const/16 v2, 0x8

    const/4 v3, 0x0

    .line 44
    if-nez p2, :cond_1

    new-instance v0, Lcom/peel/ui/b/ck;

    invoke-direct {v0, p0}, Lcom/peel/ui/b/ck;-><init>(Lcom/peel/ui/b/cd;)V

    move-object v1, v0

    .line 46
    :goto_0
    if-nez p2, :cond_0

    .line 47
    sget v0, Lcom/peel/ui/fq;->show_details_overview_actions:I

    invoke-virtual {p1, v0, p3, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 48
    sget v0, Lcom/peel/ui/fp;->show_title:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lcom/peel/ui/b/ck;->a:Landroid/widget/TextView;

    .line 49
    sget v0, Lcom/peel/ui/fp;->episode_title:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lcom/peel/ui/b/ck;->b:Landroid/widget/TextView;

    .line 50
    sget v0, Lcom/peel/ui/fp;->genres:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lcom/peel/ui/b/ck;->c:Landroid/widget/TextView;

    .line 51
    sget v0, Lcom/peel/ui/fp;->time:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lcom/peel/ui/b/ck;->d:Landroid/widget/TextView;

    .line 52
    sget v0, Lcom/peel/ui/fp;->channel:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lcom/peel/ui/b/ck;->e:Landroid/widget/TextView;

    .line 53
    sget v0, Lcom/peel/ui/fp;->episode_season:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lcom/peel/ui/b/ck;->f:Landroid/widget/TextView;

    .line 54
    sget v0, Lcom/peel/ui/fp;->like_btn:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lcom/peel/ui/b/ck;->g:Landroid/widget/TextView;

    .line 55
    sget v0, Lcom/peel/ui/fp;->dislike_btn:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lcom/peel/ui/b/ck;->h:Landroid/widget/TextView;

    .line 57
    invoke-virtual {p2, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 60
    :cond_0
    invoke-direct {p0, v1}, Lcom/peel/ui/b/cd;->a(Lcom/peel/ui/b/ck;)V

    .line 62
    iget-object v0, p0, Lcom/peel/ui/b/cd;->h:Ljava/lang/String;

    if-nez v0, :cond_2

    .line 63
    iget-object v0, v1, Lcom/peel/ui/b/ck;->g:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 64
    iget-object v0, v1, Lcom/peel/ui/b/ck;->h:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 79
    :goto_1
    return-object p2

    .line 44
    :cond_1
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/ui/b/ck;

    move-object v1, v0

    goto :goto_0

    .line 66
    :cond_2
    iget-object v0, p0, Lcom/peel/ui/b/cd;->f:Ljava/util/Map;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/peel/ui/b/cd;->f:Ljava/util/Map;

    iget-object v2, p0, Lcom/peel/ui/b/cd;->h:Ljava/lang/String;

    invoke-interface {v0, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 67
    iget-object v0, v1, Lcom/peel/ui/b/ck;->g:Landroid/widget/TextView;

    sget v2, Lcom/peel/ui/fo;->like_btn_select:I

    invoke-virtual {v0, v3, v2, v3, v3}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    .line 72
    :cond_3
    :goto_2
    iget-object v0, v1, Lcom/peel/ui/b/ck;->g:Landroid/widget/TextView;

    iget-object v2, v1, Lcom/peel/ui/b/ck;->h:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTag(Ljava/lang/Object;)V

    .line 73
    iget-object v0, v1, Lcom/peel/ui/b/ck;->h:Landroid/widget/TextView;

    iget-object v2, v1, Lcom/peel/ui/b/ck;->g:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTag(Ljava/lang/Object;)V

    .line 75
    iget-object v0, v1, Lcom/peel/ui/b/ck;->g:Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 76
    iget-object v0, v1, Lcom/peel/ui/b/ck;->h:Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_1

    .line 68
    :cond_4
    iget-object v0, p0, Lcom/peel/ui/b/cd;->g:Ljava/util/Map;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/peel/ui/b/cd;->g:Ljava/util/Map;

    iget-object v2, p0, Lcom/peel/ui/b/cd;->h:Ljava/lang/String;

    invoke-interface {v0, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 69
    iget-object v0, v1, Lcom/peel/ui/b/ck;->h:Landroid/widget/TextView;

    sget v2, Lcom/peel/ui/fo;->dislike_btn_select:I

    invoke-virtual {v0, v3, v2, v3, v3}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    goto :goto_2
.end method

.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 169
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    .line 170
    sget v1, Lcom/peel/ui/fp;->dislike_btn:I

    if-ne v0, v1, :cond_3

    .line 171
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 172
    const-string/jumbo v0, "show"

    iget-object v2, p0, Lcom/peel/ui/b/cd;->h:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 173
    const-string/jumbo v0, "title"

    iget-object v2, p0, Lcom/peel/ui/b/cd;->i:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 174
    const-string/jumbo v0, "tmsid"

    iget-object v2, p0, Lcom/peel/ui/b/cd;->h:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 175
    const-string/jumbo v0, "contextId"

    iget v2, p0, Lcom/peel/ui/b/cd;->b:I

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 177
    iget-object v0, p0, Lcom/peel/ui/b/cd;->g:Ljava/util/Map;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/peel/ui/b/cd;->g:Ljava/util/Map;

    iget-object v2, p0, Lcom/peel/ui/b/cd;->h:Ljava/lang/String;

    invoke-interface {v0, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 178
    const-string/jumbo v0, "path"

    const-string/jumbo v2, "show/undislike"

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 179
    check-cast p1, Landroid/widget/TextView;

    sget v0, Lcom/peel/ui/fo;->dislike_btn_normal:I

    invoke-virtual {p1, v3, v0, v3, v3}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    .line 180
    sget-object v0, Lcom/peel/content/a;->f:Lcom/peel/content/user/User;

    new-instance v2, Lcom/peel/ui/b/ce;

    invoke-direct {v2, p0}, Lcom/peel/ui/b/ce;-><init>(Lcom/peel/ui/b/cd;)V

    invoke-virtual {v0, v1, v2}, Lcom/peel/content/user/User;->a(Landroid/os/Bundle;Lcom/peel/util/t;)V

    .line 214
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/peel/ui/b/cd;->a:Lcom/peel/d/i;

    const-string/jumbo v1, "favcut_refresh"

    invoke-virtual {v0, v1, v4}, Lcom/peel/d/i;->b(Ljava/lang/String;I)V

    .line 264
    :cond_1
    :goto_1
    return-void

    .line 189
    :cond_2
    const-string/jumbo v0, "path"

    const-string/jumbo v2, "show/dislike"

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, p1

    .line 190
    check-cast v0, Landroid/widget/TextView;

    sget v2, Lcom/peel/ui/fo;->dislike_btn_select:I

    invoke-virtual {v0, v3, v2, v3, v3}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    .line 191
    sget-object v0, Lcom/peel/content/a;->f:Lcom/peel/content/user/User;

    new-instance v2, Lcom/peel/ui/b/cf;

    invoke-direct {v2, p0}, Lcom/peel/ui/b/cf;-><init>(Lcom/peel/ui/b/cd;)V

    invoke-virtual {v0, v1, v2}, Lcom/peel/content/user/User;->a(Landroid/os/Bundle;Lcom/peel/util/t;)V

    .line 200
    iget-object v0, p0, Lcom/peel/ui/b/cd;->f:Ljava/util/Map;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/peel/ui/b/cd;->f:Ljava/util/Map;

    iget-object v2, p0, Lcom/peel/ui/b/cd;->h:Ljava/lang/String;

    invoke-interface {v0, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 201
    const-string/jumbo v0, "path"

    const-string/jumbo v2, "show/unfav"

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 202
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    sget v2, Lcom/peel/ui/fo;->like_btn_normal:I

    invoke-virtual {v0, v3, v2, v3, v3}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    .line 203
    sget-object v0, Lcom/peel/content/a;->f:Lcom/peel/content/user/User;

    new-instance v2, Lcom/peel/ui/b/cg;

    invoke-direct {v2, p0}, Lcom/peel/ui/b/cg;-><init>(Lcom/peel/ui/b/cd;)V

    invoke-virtual {v0, v1, v2}, Lcom/peel/content/user/User;->a(Landroid/os/Bundle;Lcom/peel/util/t;)V

    goto :goto_0

    .line 216
    :cond_3
    sget v1, Lcom/peel/ui/fp;->like_btn:I

    if-ne v0, v1, :cond_1

    .line 217
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 218
    const-string/jumbo v0, "show"

    iget-object v2, p0, Lcom/peel/ui/b/cd;->h:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 219
    const-string/jumbo v0, "title"

    iget-object v2, p0, Lcom/peel/ui/b/cd;->i:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 220
    const-string/jumbo v0, "tmsid"

    iget-object v2, p0, Lcom/peel/ui/b/cd;->h:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 221
    const-string/jumbo v0, "contextId"

    iget v2, p0, Lcom/peel/ui/b/cd;->b:I

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 223
    iget-object v0, p0, Lcom/peel/ui/b/cd;->f:Ljava/util/Map;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/peel/ui/b/cd;->f:Ljava/util/Map;

    iget-object v2, p0, Lcom/peel/ui/b/cd;->h:Ljava/lang/String;

    invoke-interface {v0, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 224
    const-string/jumbo v0, "path"

    const-string/jumbo v2, "show/unfav"

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 225
    check-cast p1, Landroid/widget/TextView;

    sget v0, Lcom/peel/ui/fo;->like_btn_normal:I

    invoke-virtual {p1, v3, v0, v3, v3}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    .line 226
    sget-object v0, Lcom/peel/content/a;->f:Lcom/peel/content/user/User;

    new-instance v2, Lcom/peel/ui/b/ch;

    invoke-direct {v2, p0}, Lcom/peel/ui/b/ch;-><init>(Lcom/peel/ui/b/cd;)V

    invoke-virtual {v0, v1, v2}, Lcom/peel/content/user/User;->a(Landroid/os/Bundle;Lcom/peel/util/t;)V

    .line 261
    :cond_4
    :goto_2
    iget-object v0, p0, Lcom/peel/ui/b/cd;->a:Lcom/peel/d/i;

    const-string/jumbo v1, "favcut_refresh"

    invoke-virtual {v0, v1, v4}, Lcom/peel/d/i;->b(Ljava/lang/String;I)V

    goto/16 :goto_1

    .line 235
    :cond_5
    const-string/jumbo v0, "path"

    const-string/jumbo v2, "show/fav"

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, p1

    .line 236
    check-cast v0, Landroid/widget/TextView;

    sget v2, Lcom/peel/ui/fo;->like_btn_select:I

    invoke-virtual {v0, v3, v2, v3, v3}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    .line 237
    sget-object v0, Lcom/peel/content/a;->f:Lcom/peel/content/user/User;

    new-instance v2, Lcom/peel/ui/b/ci;

    invoke-direct {v2, p0}, Lcom/peel/ui/b/ci;-><init>(Lcom/peel/ui/b/cd;)V

    invoke-virtual {v0, v1, v2}, Lcom/peel/content/user/User;->a(Landroid/os/Bundle;Lcom/peel/util/t;)V

    .line 246
    iget-object v0, p0, Lcom/peel/ui/b/cd;->g:Ljava/util/Map;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/peel/ui/b/cd;->g:Ljava/util/Map;

    iget-object v2, p0, Lcom/peel/ui/b/cd;->h:Ljava/lang/String;

    invoke-interface {v0, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 247
    const-string/jumbo v0, "path"

    const-string/jumbo v2, "show/undislike"

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 248
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    sget v2, Lcom/peel/ui/fo;->dislike_btn_normal:I

    invoke-virtual {v0, v3, v2, v3, v3}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    .line 250
    sget-object v0, Lcom/peel/content/a;->f:Lcom/peel/content/user/User;

    new-instance v2, Lcom/peel/ui/b/cj;

    invoke-direct {v2, p0}, Lcom/peel/ui/b/cj;-><init>(Lcom/peel/ui/b/cd;)V

    invoke-virtual {v0, v1, v2}, Lcom/peel/content/user/User;->a(Landroid/os/Bundle;Lcom/peel/util/t;)V

    goto :goto_2
.end method
