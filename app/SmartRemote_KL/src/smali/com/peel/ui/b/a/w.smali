.class Lcom/peel/ui/b/a/w;
.super Landroid/widget/ArrayAdapter;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lcom/peel/content/listing/Listing;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Landroid/content/Context;

.field final synthetic b:Ljava/util/concurrent/atomic/AtomicInteger;

.field final synthetic c:Lcom/peel/ui/b/a/a;


# direct methods
.method constructor <init>(Lcom/peel/ui/b/a/a;Landroid/content/Context;ILjava/util/List;Landroid/content/Context;Ljava/util/concurrent/atomic/AtomicInteger;)V
    .locals 0

    .prologue
    .line 533
    iput-object p1, p0, Lcom/peel/ui/b/a/w;->c:Lcom/peel/ui/b/a/a;

    iput-object p5, p0, Lcom/peel/ui/b/a/w;->a:Landroid/content/Context;

    iput-object p6, p0, Lcom/peel/ui/b/a/w;->b:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {p0, p2, p3, p4}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    return-void
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 12

    .prologue
    const/16 v11, 0x8

    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 536
    if-eqz p2, :cond_2

    .line 537
    :goto_0
    invoke-virtual {p0, p1}, Lcom/peel/ui/b/a/w;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/content/listing/LiveListing;

    .line 539
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v1

    .line 540
    invoke-virtual {v0}, Lcom/peel/content/listing/LiveListing;->o()J

    move-result-wide v6

    invoke-virtual {v1, v6, v7}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 542
    sget v1, Lcom/peel/ui/fp;->title:I

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 543
    invoke-virtual {v0}, Lcom/peel/content/listing/LiveListing;->v()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_3

    invoke-virtual {v0}, Lcom/peel/content/listing/LiveListing;->l()Ljava/lang/String;

    move-result-object v2

    :goto_1
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 546
    sget v1, Lcom/peel/ui/fp;->info:I

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 548
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 551
    invoke-virtual {v0}, Lcom/peel/content/listing/LiveListing;->A()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_8

    invoke-virtual {v0}, Lcom/peel/content/listing/LiveListing;->A()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v6, "0"

    .line 552
    invoke-virtual {v2, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_8

    .line 553
    iget-object v2, p0, Lcom/peel/ui/b/a/w;->a:Landroid/content/Context;

    sget v6, Lcom/peel/ui/ft;->season_name:I

    new-array v7, v3, [Ljava/lang/Object;

    invoke-virtual {v0}, Lcom/peel/content/listing/LiveListing;->A()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v7, v4

    invoke-virtual {v2, v6, v7}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move v2, v3

    .line 557
    :goto_2
    invoke-virtual {v0}, Lcom/peel/content/listing/LiveListing;->w()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_0

    invoke-virtual {v0}, Lcom/peel/content/listing/LiveListing;->w()Ljava/lang/String;

    move-result-object v6

    const-string/jumbo v7, "0"

    .line 558
    invoke-virtual {v6, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 559
    if-eqz v2, :cond_4

    const-string/jumbo v2, ", "

    :goto_3
    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v6, p0, Lcom/peel/ui/b/a/w;->a:Landroid/content/Context;

    sget v7, Lcom/peel/ui/ft;->episode_name:I

    new-array v8, v3, [Ljava/lang/Object;

    invoke-virtual {v0}, Lcom/peel/content/listing/LiveListing;->w()Ljava/lang/String;

    move-result-object v9

    aput-object v9, v8, v4

    invoke-virtual {v6, v7, v8}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 562
    :cond_0
    const-string/jumbo v2, " / "

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 563
    sget-object v2, Lcom/peel/util/x;->c:Ljava/lang/ThreadLocal;

    invoke-virtual {v2}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/text/SimpleDateFormat;

    invoke-virtual {v0}, Lcom/peel/content/listing/LiveListing;->o()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v2, v6}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 564
    invoke-virtual {v0}, Lcom/peel/content/listing/LiveListing;->j()J

    move-result-wide v6

    iget-object v8, p0, Lcom/peel/ui/b/a/w;->a:Landroid/content/Context;

    invoke-static {v8}, Landroid/text/format/DateFormat;->is24HourFormat(Landroid/content/Context;)Z

    move-result v8

    iget-object v9, p0, Lcom/peel/ui/b/a/w;->a:Landroid/content/Context;

    sget v10, Lcom/peel/ui/ft;->time_pattern:I

    invoke-virtual {v9, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 563
    invoke-static {v2, v6, v7, v8, v9}, Lcom/peel/util/x;->a(Ljava/lang/String;JZLjava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 566
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    if-lez v2, :cond_5

    .line 567
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 572
    :goto_4
    sget v1, Lcom/peel/ui/fp;->channel:I

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 573
    invoke-virtual {v0}, Lcom/peel/content/listing/LiveListing;->d()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_6

    .line 574
    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 576
    invoke-static {}, Lcom/peel/content/a;->e()Ljava/util/Map;

    move-result-object v2

    invoke-virtual {v0}, Lcom/peel/content/listing/LiveListing;->d()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v2, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 577
    if-nez v2, :cond_1

    invoke-virtual {v0}, Lcom/peel/content/listing/LiveListing;->d()Ljava/lang/String;

    move-result-object v2

    :cond_1
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 582
    :goto_5
    iget-object v0, p0, Lcom/peel/ui/b/a/w;->b:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v0

    if-ne v0, p1, :cond_7

    .line 583
    sget v0, Lcom/peel/ui/fp;->checkbox:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckedTextView;

    invoke-virtual {v0, v3}, Landroid/widget/CheckedTextView;->setChecked(Z)V

    .line 588
    :goto_6
    return-object p2

    .line 536
    :cond_2
    iget-object v0, p0, Lcom/peel/ui/b/a/w;->c:Lcom/peel/ui/b/a/a;

    iget-object v0, v0, Lcom/peel/ui/b/a/a;->j:Landroid/view/LayoutInflater;

    sget v1, Lcom/peel/ui/fq;->show_details_watchon_item:I

    invoke-virtual {v0, v1, p3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    goto/16 :goto_0

    .line 544
    :cond_3
    invoke-virtual {v0}, Lcom/peel/content/listing/LiveListing;->v()Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_1

    .line 559
    :cond_4
    const-string/jumbo v2, ""

    goto/16 :goto_3

    .line 569
    :cond_5
    invoke-virtual {v1, v11}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_4

    .line 579
    :cond_6
    invoke-virtual {v1, v11}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_5

    .line 585
    :cond_7
    sget v0, Lcom/peel/ui/fp;->checkbox:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckedTextView;

    invoke-virtual {v0, v4}, Landroid/widget/CheckedTextView;->setChecked(Z)V

    goto :goto_6

    :cond_8
    move v2, v4

    goto/16 :goto_2
.end method
