.class Lcom/peel/ui/b/ba;
.super Lcom/peel/util/t;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/peel/util/t",
        "<",
        "Ljava/util/List",
        "<",
        "Lcom/peel/content/listing/Listing;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/peel/ui/b/av;


# direct methods
.method constructor <init>(Lcom/peel/ui/b/av;)V
    .locals 0

    .prologue
    .line 320
    iput-object p1, p0, Lcom/peel/ui/b/ba;->a:Lcom/peel/ui/b/av;

    invoke-direct {p0}, Lcom/peel/util/t;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(ZLjava/lang/Object;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 320
    check-cast p2, Ljava/util/List;

    invoke-virtual {p0, p1, p2, p3}, Lcom/peel/ui/b/ba;->a(ZLjava/util/List;Ljava/lang/String;)V

    return-void
.end method

.method public a(ZLjava/util/List;Ljava/lang/String;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Ljava/util/List",
            "<",
            "Lcom/peel/content/listing/Listing;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v4, 0x0

    .line 323
    if-eqz p1, :cond_6

    if-eqz p2, :cond_6

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_6

    .line 324
    iget-object v0, p0, Lcom/peel/ui/b/ba;->a:Lcom/peel/ui/b/av;

    invoke-static {v0, p2}, Lcom/peel/ui/b/av;->a(Lcom/peel/ui/b/av;Ljava/util/List;)Ljava/util/List;

    .line 326
    new-instance v0, Lcom/peel/ui/b/bb;

    invoke-direct {v0, p0}, Lcom/peel/ui/b/bb;-><init>(Lcom/peel/ui/b/ba;)V

    invoke-static {p2, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 340
    const/4 v2, 0x0

    .line 343
    iget-object v0, p0, Lcom/peel/ui/b/ba;->a:Lcom/peel/ui/b/av;

    invoke-static {v0}, Lcom/peel/ui/b/av;->q(Lcom/peel/ui/b/av;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/peel/ui/b/ba;->a:Lcom/peel/ui/b/av;

    invoke-static {v0}, Lcom/peel/ui/b/av;->r(Lcom/peel/ui/b/av;)J

    move-result-wide v6

    const-wide/16 v8, 0x0

    cmp-long v0, v6, v8

    if-lez v0, :cond_8

    .line 344
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/content/listing/Listing;

    .line 345
    check-cast v0, Lcom/peel/content/listing/LiveListing;

    .line 346
    iget-object v5, p0, Lcom/peel/ui/b/ba;->a:Lcom/peel/ui/b/av;

    invoke-static {v5}, Lcom/peel/ui/b/av;->s(Lcom/peel/ui/b/av;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0}, Lcom/peel/content/listing/LiveListing;->g()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-virtual {v0}, Lcom/peel/content/listing/LiveListing;->c()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/peel/ui/b/ba;->a:Lcom/peel/ui/b/av;

    .line 347
    invoke-static {v6}, Lcom/peel/ui/b/av;->q(Lcom/peel/ui/b/av;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-virtual {v0}, Lcom/peel/content/listing/LiveListing;->o()J

    move-result-wide v6

    iget-object v5, p0, Lcom/peel/ui/b/ba;->a:Lcom/peel/ui/b/av;

    invoke-static {v5}, Lcom/peel/ui/b/av;->r(Lcom/peel/ui/b/av;)J

    move-result-wide v8

    cmp-long v5, v6, v8

    if-nez v5, :cond_0

    .line 350
    invoke-static {}, Lcom/peel/ui/b/av;->c()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, " ****** found matching piw w/ channel and time: "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v5, p0, Lcom/peel/ui/b/ba;->a:Lcom/peel/ui/b/av;

    invoke-static {v5}, Lcom/peel/ui/b/av;->q(Lcom/peel/ui/b/av;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v5, " -- "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v5, p0, Lcom/peel/ui/b/ba;->a:Lcom/peel/ui/b/av;

    invoke-static {v5}, Lcom/peel/ui/b/av;->r(Lcom/peel/ui/b/av;)J

    move-result-wide v6

    invoke-virtual {v3, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v5, "\n"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move-object v2, v0

    move v3, v1

    .line 357
    :goto_0
    if-nez v3, :cond_7

    .line 358
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/content/listing/Listing;

    .line 359
    check-cast v0, Lcom/peel/content/listing/LiveListing;

    .line 360
    invoke-static {}, Lcom/peel/ui/b/av;->c()Ljava/lang/String;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, " ##################### id : episodeid -- "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/peel/ui/b/ba;->a:Lcom/peel/ui/b/av;

    .line 361
    invoke-static {v8}, Lcom/peel/ui/b/av;->s(Lcom/peel/ui/b/av;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, " -- "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v0}, Lcom/peel/content/listing/LiveListing;->g()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 360
    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 362
    iget-object v6, p0, Lcom/peel/ui/b/ba;->a:Lcom/peel/ui/b/av;

    invoke-static {v6}, Lcom/peel/ui/b/av;->s(Lcom/peel/ui/b/av;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0}, Lcom/peel/content/listing/LiveListing;->g()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    move-object v2, v0

    move v0, v1

    .line 371
    :goto_1
    if-nez v0, :cond_2

    .line 372
    invoke-interface {p2, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/content/listing/Listing;

    move-object v2, v0

    .line 375
    :cond_2
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 377
    if-eqz v2, :cond_3

    .line 378
    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 381
    :cond_3
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_4
    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/content/listing/Listing;

    .line 382
    invoke-virtual {v0}, Lcom/peel/content/listing/Listing;->g()Ljava/lang/String;

    move-result-object v1

    .line 383
    invoke-virtual {v2}, Lcom/peel/content/listing/Listing;->g()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    move-object v1, v0

    check-cast v1, Lcom/peel/content/listing/LiveListing;

    .line 384
    invoke-virtual {v1}, Lcom/peel/content/listing/LiveListing;->o()J

    move-result-wide v6

    move-object v1, v2

    check-cast v1, Lcom/peel/content/listing/LiveListing;

    invoke-virtual {v1}, Lcom/peel/content/listing/LiveListing;->o()J

    move-result-wide v8

    cmp-long v1, v6, v8

    if-nez v1, :cond_4

    move-object v1, v0

    check-cast v1, Lcom/peel/content/listing/LiveListing;

    .line 385
    invoke-virtual {v1}, Lcom/peel/content/listing/LiveListing;->c()Ljava/lang/String;

    move-result-object v5

    move-object v1, v2

    check-cast v1, Lcom/peel/content/listing/LiveListing;

    .line 386
    invoke-virtual {v1}, Lcom/peel/content/listing/LiveListing;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 387
    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 391
    :cond_5
    iget-object v0, p0, Lcom/peel/ui/b/ba;->a:Lcom/peel/ui/b/av;

    invoke-static {v0}, Lcom/peel/ui/b/av;->f(Lcom/peel/ui/b/av;)Lcom/peel/ui/b/a/a;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/peel/ui/b/a/a;->a(Ljava/util/List;)V

    .line 393
    invoke-static {}, Lcom/peel/ui/b/av;->c()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "render full views"

    new-instance v2, Lcom/peel/ui/b/bc;

    invoke-direct {v2, p0}, Lcom/peel/ui/b/bc;-><init>(Lcom/peel/ui/b/ba;)V

    invoke-static {v0, v1, v2}, Lcom/peel/util/i;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/lang/Runnable;

    .line 400
    :cond_6
    return-void

    :cond_7
    move v0, v3

    goto :goto_1

    :cond_8
    move v3, v4

    goto/16 :goto_0
.end method
