.class Lcom/peel/ui/b/a/z;
.super Lcom/peel/util/t;


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic b:Ljava/lang/String;

.field final synthetic c:Lcom/peel/ui/b/a/y;


# direct methods
.method constructor <init>(Lcom/peel/ui/b/a/y;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 778
    iput-object p1, p0, Lcom/peel/ui/b/a/z;->c:Lcom/peel/ui/b/a/y;

    iput-object p2, p0, Lcom/peel/ui/b/a/z;->a:Ljava/lang/String;

    iput-object p3, p0, Lcom/peel/ui/b/a/z;->b:Ljava/lang/String;

    invoke-direct {p0}, Lcom/peel/util/t;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .prologue
    .line 781
    iget-object v0, p0, Lcom/peel/ui/b/a/z;->c:Lcom/peel/ui/b/a/y;

    iget-object v0, v0, Lcom/peel/ui/b/a/y;->d:Lcom/peel/ui/b/a/a;

    invoke-virtual {v0}, Lcom/peel/ui/b/a/a;->e()V

    .line 782
    iget-boolean v0, p0, Lcom/peel/ui/b/a/z;->i:Z

    if-eqz v0, :cond_1

    .line 783
    iget-object v0, p0, Lcom/peel/ui/b/a/z;->c:Lcom/peel/ui/b/a/y;

    iget-object v0, v0, Lcom/peel/ui/b/a/y;->d:Lcom/peel/ui/b/a/a;

    iget-object v0, v0, Lcom/peel/ui/b/a/a;->e:Lcom/peel/widget/ag;

    invoke-virtual {v0}, Lcom/peel/widget/ag;->dismiss()V

    .line 785
    invoke-static {}, Lcom/peel/util/a/f;->a()Lcom/peel/util/a/f;

    move-result-object v1

    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    .line 786
    invoke-virtual {v0}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const/16 v2, 0x441

    iget-object v3, p0, Lcom/peel/ui/b/a/z;->c:Lcom/peel/ui/b/a/y;

    iget-object v3, v3, Lcom/peel/ui/b/a/y;->d:Lcom/peel/ui/b/a/a;

    iget v3, v3, Lcom/peel/ui/b/a/a;->h:I

    const-string/jumbo v4, "login success"

    .line 785
    invoke-virtual {v1, v0, v2, v3, v4}, Lcom/peel/util/a/f;->a(IIILjava/lang/String;)V

    .line 789
    iget-object v0, p0, Lcom/peel/ui/b/a/z;->c:Lcom/peel/ui/b/a/y;

    iget-object v1, v0, Lcom/peel/ui/b/a/y;->d:Lcom/peel/ui/b/a/a;

    iget-object v0, p0, Lcom/peel/ui/b/a/z;->c:Lcom/peel/ui/b/a/y;

    iget-wide v2, v0, Lcom/peel/ui/b/a/y;->b:J

    iget-object v0, p0, Lcom/peel/ui/b/a/z;->c:Lcom/peel/ui/b/a/y;

    iget-object v4, v0, Lcom/peel/ui/b/a/y;->c:Lcom/peel/content/listing/Listing;

    iget-object v5, p0, Lcom/peel/ui/b/a/z;->a:Ljava/lang/String;

    iget-object v6, p0, Lcom/peel/ui/b/a/z;->b:Ljava/lang/String;

    invoke-static/range {v1 .. v6}, Lcom/peel/ui/b/a/a;->a(Lcom/peel/ui/b/a/a;JLcom/peel/content/listing/Listing;Ljava/lang/String;Ljava/lang/String;)V

    .line 804
    :goto_1
    return-void

    .line 786
    :cond_0
    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v0}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/data/at;->f()I

    move-result v0

    goto :goto_0

    .line 792
    :cond_1
    invoke-static {}, Lcom/peel/ui/b/a/a;->h()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "\n login failed: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/peel/ui/b/a/z;->k:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " -- "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/peel/ui/b/a/z;->j:Ljava/lang/Object;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 793
    invoke-static {}, Lcom/peel/ui/b/a/a;->h()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "show login failed dialog"

    new-instance v2, Lcom/peel/ui/b/a/aa;

    invoke-direct {v2, p0}, Lcom/peel/ui/b/a/aa;-><init>(Lcom/peel/ui/b/a/z;)V

    invoke-static {v0, v1, v2}, Lcom/peel/util/i;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/lang/Runnable;

    goto :goto_1
.end method
