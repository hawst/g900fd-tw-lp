.class Lcom/peel/ui/b/a/y;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic a:Landroid/view/View;

.field final synthetic b:J

.field final synthetic c:Lcom/peel/content/listing/Listing;

.field final synthetic d:Lcom/peel/ui/b/a/a;


# direct methods
.method constructor <init>(Lcom/peel/ui/b/a/a;Landroid/view/View;JLcom/peel/content/listing/Listing;)V
    .locals 1

    .prologue
    .line 764
    iput-object p1, p0, Lcom/peel/ui/b/a/y;->d:Lcom/peel/ui/b/a/a;

    iput-object p2, p0, Lcom/peel/ui/b/a/y;->a:Landroid/view/View;

    iput-wide p3, p0, Lcom/peel/ui/b/a/y;->b:J

    iput-object p5, p0, Lcom/peel/ui/b/a/y;->c:Lcom/peel/content/listing/Listing;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 767
    iget-object v0, p0, Lcom/peel/ui/b/a/y;->d:Lcom/peel/ui/b/a/a;

    invoke-virtual {v0}, Lcom/peel/ui/b/a/a;->d()V

    .line 768
    iget-object v0, p0, Lcom/peel/ui/b/a/y;->a:Landroid/view/View;

    sget v1, Lcom/peel/ui/fp;->username:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    .line 769
    iget-object v0, p0, Lcom/peel/ui/b/a/y;->a:Landroid/view/View;

    sget v1, Lcom/peel/ui/fp;->password:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v4

    .line 771
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_1

    .line 772
    :cond_0
    iget-object v0, p0, Lcom/peel/ui/b/a/y;->d:Lcom/peel/ui/b/a/a;

    invoke-virtual {v0}, Lcom/peel/ui/b/a/a;->e()V

    .line 773
    iget-object v0, p0, Lcom/peel/ui/b/a/y;->d:Lcom/peel/ui/b/a/a;

    iget-object v0, v0, Lcom/peel/ui/b/a/a;->c:Landroid/support/v4/app/ae;

    iget-object v1, p0, Lcom/peel/ui/b/a/y;->d:Lcom/peel/ui/b/a/a;

    iget-object v1, v1, Lcom/peel/ui/b/a/a;->c:Landroid/support/v4/app/ae;

    invoke-virtual {v1}, Landroid/support/v4/app/ae;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/peel/ui/ft;->login_pwd_empty:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    .line 774
    const/16 v1, 0x11

    invoke-virtual {v0, v1, v5, v5}, Landroid/widget/Toast;->setGravity(III)V

    .line 775
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 808
    :goto_0
    return-void

    .line 777
    :cond_1
    iget-object v0, p0, Lcom/peel/ui/b/a/y;->d:Lcom/peel/ui/b/a/a;

    iget-object v0, v0, Lcom/peel/ui/b/a/a;->c:Landroid/support/v4/app/ae;

    sget-object v1, Lcom/peel/content/a;->f:Lcom/peel/content/user/User;

    invoke-virtual {v1}, Lcom/peel/content/user/User;->x()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/peel/ui/b/a/y;->d:Lcom/peel/ui/b/a/a;

    iget-object v2, v2, Lcom/peel/ui/b/a/a;->k:Lcom/peel/content/library/LiveLibrary;

    invoke-virtual {v2}, Lcom/peel/content/library/LiveLibrary;->e()Ljava/lang/String;

    move-result-object v2

    new-instance v5, Lcom/peel/ui/b/a/z;

    invoke-direct {v5, p0, v3, v4}, Lcom/peel/ui/b/a/z;-><init>(Lcom/peel/ui/b/a/y;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static/range {v0 .. v5}, Lcom/peel/util/al;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/peel/util/t;)V

    goto :goto_0
.end method
