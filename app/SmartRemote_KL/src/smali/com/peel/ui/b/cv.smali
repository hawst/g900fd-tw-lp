.class public Lcom/peel/ui/b/cv;
.super Lcom/peel/d/u;


# static fields
.field private static final aj:Ljava/lang/String;


# instance fields
.field private ak:Landroid/content/BroadcastReceiver;

.field private e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/peel/content/listing/Listing;",
            ">;"
        }
    .end annotation
.end field

.field private f:Landroid/view/LayoutInflater;

.field private g:Ljava/text/DateFormatSymbols;

.field private h:Landroid/widget/ListView;

.field private i:Lcom/peel/ui/b/cy;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 43
    const-class v0, Lcom/peel/ui/b/cv;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/peel/ui/b/cv;->aj:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 37
    invoke-direct {p0}, Lcom/peel/d/u;-><init>()V

    .line 182
    new-instance v0, Lcom/peel/ui/b/cx;

    invoke-direct {v0, p0}, Lcom/peel/ui/b/cx;-><init>(Lcom/peel/ui/b/cv;)V

    iput-object v0, p0, Lcom/peel/ui/b/cv;->ak:Landroid/content/BroadcastReceiver;

    return-void
.end method

.method static synthetic a(Lcom/peel/ui/b/cv;)Lcom/peel/ui/b/cy;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/peel/ui/b/cv;->i:Lcom/peel/ui/b/cy;

    return-object v0
.end method

.method static synthetic b(Lcom/peel/ui/b/cv;)Ljava/util/List;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/peel/ui/b/cv;->e:Ljava/util/List;

    return-object v0
.end method

.method static synthetic c(Lcom/peel/ui/b/cv;)Landroid/view/LayoutInflater;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/peel/ui/b/cv;->f:Landroid/view/LayoutInflater;

    return-object v0
.end method

.method static synthetic d(Lcom/peel/ui/b/cv;)Ljava/text/DateFormatSymbols;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/peel/ui/b/cv;->g:Ljava/text/DateFormatSymbols;

    return-object v0
.end method


# virtual methods
.method public Z()V
    .locals 6

    .prologue
    .line 195
    iget-object v0, p0, Lcom/peel/ui/b/cv;->d:Lcom/peel/d/a;

    if-nez v0, :cond_0

    .line 196
    new-instance v0, Lcom/peel/d/a;

    sget-object v1, Lcom/peel/d/d;->b:Lcom/peel/d/d;

    sget-object v2, Lcom/peel/d/b;->a:Lcom/peel/d/b;

    sget-object v3, Lcom/peel/d/c;->b:Lcom/peel/d/c;

    sget v4, Lcom/peel/ui/ft;->upcoming:I

    invoke-virtual {p0, v4}, Lcom/peel/ui/b/cv;->a(I)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, Lcom/peel/d/a;-><init>(Lcom/peel/d/d;Lcom/peel/d/b;Lcom/peel/d/c;Ljava/lang/String;Ljava/util/List;)V

    iput-object v0, p0, Lcom/peel/ui/b/cv;->d:Lcom/peel/d/a;

    .line 199
    :cond_0
    iget-object v0, p0, Lcom/peel/ui/b/cv;->b:Lcom/peel/d/i;

    iget-object v1, p0, Lcom/peel/ui/b/cv;->d:Lcom/peel/d/a;

    invoke-virtual {v0, v1}, Lcom/peel/d/i;->a(Lcom/peel/d/a;)V

    .line 200
    return-void
.end method

.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    .line 69
    sget v0, Lcom/peel/ui/fq;->common_listview_layout:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 70
    iput-object p1, p0, Lcom/peel/ui/b/cv;->f:Landroid/view/LayoutInflater;

    .line 71
    sget v0, Lcom/peel/ui/fp;->list:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/peel/ui/b/cv;->h:Landroid/widget/ListView;

    .line 72
    return-object v1
.end method

.method public d(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 47
    invoke-super {p0, p1}, Lcom/peel/d/u;->d(Landroid/os/Bundle;)V

    .line 48
    return-void
.end method

.method public h(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 52
    invoke-super {p0, p1}, Lcom/peel/d/u;->h(Landroid/os/Bundle;)V

    .line 53
    iget-object v0, p0, Lcom/peel/ui/b/cv;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "upcoming_list"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/ui/b/cv;->e:Ljava/util/List;

    .line 54
    new-instance v0, Ljava/text/DateFormatSymbols;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/text/DateFormatSymbols;-><init>(Ljava/util/Locale;)V

    iput-object v0, p0, Lcom/peel/ui/b/cv;->g:Ljava/text/DateFormatSymbols;

    .line 55
    new-instance v0, Lcom/peel/ui/b/cy;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/peel/ui/b/cy;-><init>(Lcom/peel/ui/b/cv;Lcom/peel/ui/b/cw;)V

    iput-object v0, p0, Lcom/peel/ui/b/cv;->i:Lcom/peel/ui/b/cy;

    .line 57
    iget-object v0, p0, Lcom/peel/ui/b/cv;->h:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/peel/ui/b/cv;->i:Lcom/peel/ui/b/cy;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 58
    iget-object v0, p0, Lcom/peel/ui/b/cv;->h:Landroid/widget/ListView;

    new-instance v1, Lcom/peel/ui/b/cw;

    invoke-direct {v1, p0}, Lcom/peel/ui/b/cw;-><init>(Lcom/peel/ui/b/cv;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 65
    return-void
.end method

.method public w()V
    .locals 4

    .prologue
    .line 178
    invoke-super {p0}, Lcom/peel/d/u;->w()V

    .line 179
    invoke-virtual {p0}, Lcom/peel/ui/b/cv;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    invoke-static {v0}, Landroid/support/v4/a/q;->a(Landroid/content/Context;)Landroid/support/v4/a/q;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/ui/b/cv;->ak:Landroid/content/BroadcastReceiver;

    new-instance v2, Landroid/content/IntentFilter;

    const-string/jumbo v3, "reminder_updated"

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/a/q;->a(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)V

    .line 180
    return-void
.end method

.method public x()V
    .locals 2

    .prologue
    .line 172
    invoke-super {p0}, Lcom/peel/d/u;->x()V

    .line 173
    invoke-virtual {p0}, Lcom/peel/ui/b/cv;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    invoke-static {v0}, Landroid/support/v4/a/q;->a(Landroid/content/Context;)Landroid/support/v4/a/q;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/ui/b/cv;->ak:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/support/v4/a/q;->a(Landroid/content/BroadcastReceiver;)V

    .line 174
    return-void
.end method
