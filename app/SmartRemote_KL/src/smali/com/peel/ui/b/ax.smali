.class Lcom/peel/ui/b/ax;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/widget/AbsListView$OnScrollListener;


# instance fields
.field final synthetic a:Lcom/peel/ui/b/aw;


# direct methods
.method constructor <init>(Lcom/peel/ui/b/aw;)V
    .locals 0

    .prologue
    .line 227
    iput-object p1, p0, Lcom/peel/ui/b/ax;->a:Lcom/peel/ui/b/aw;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onScroll(Landroid/widget/AbsListView;III)V
    .locals 8

    .prologue
    .line 234
    if-eqz p1, :cond_1

    const/4 v0, 0x1

    if-le p4, v0, :cond_1

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/widget/AbsListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_1

    if-nez p2, :cond_1

    .line 236
    iget-object v0, p0, Lcom/peel/ui/b/ax;->a:Lcom/peel/ui/b/aw;

    iget-object v0, v0, Lcom/peel/ui/b/aw;->a:Lcom/peel/ui/b/av;

    invoke-static {v0}, Lcom/peel/ui/b/av;->k(Lcom/peel/ui/b/av;)Lcom/peel/ui/ScrollPositionListView;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/peel/ui/ScrollPositionListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 237
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iget-object v1, p0, Lcom/peel/ui/b/ax;->a:Lcom/peel/ui/b/aw;

    iget-object v1, v1, Lcom/peel/ui/b/aw;->a:Lcom/peel/ui/b/av;

    invoke-static {v1}, Lcom/peel/ui/b/av;->l(Lcom/peel/ui/b/av;)J

    move-result-wide v4

    sub-long/2addr v2, v4

    .line 238
    iget-object v1, p0, Lcom/peel/ui/b/ax;->a:Lcom/peel/ui/b/aw;

    iget-object v1, v1, Lcom/peel/ui/b/aw;->a:Lcom/peel/ui/b/av;

    invoke-static {v1}, Lcom/peel/ui/b/av;->m(Lcom/peel/ui/b/av;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v1

    const/16 v4, 0x8

    if-ne v1, v4, :cond_2

    iget-object v1, p0, Lcom/peel/ui/b/ax;->a:Lcom/peel/ui/b/aw;

    iget-object v1, v1, Lcom/peel/ui/b/aw;->a:Lcom/peel/ui/b/av;

    invoke-static {v1}, Lcom/peel/ui/b/av;->k(Lcom/peel/ui/b/av;)Lcom/peel/ui/ScrollPositionListView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/ui/ScrollPositionListView;->getHeight()I

    move-result v1

    const/16 v4, 0x3e8

    if-le v1, v4, :cond_2

    const/4 v1, 0x1

    invoke-virtual {p1, v1}, Landroid/widget/AbsListView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v1

    iget-object v4, p0, Lcom/peel/ui/b/ax;->a:Lcom/peel/ui/b/aw;

    iget-object v4, v4, Lcom/peel/ui/b/aw;->a:Lcom/peel/ui/b/av;

    invoke-static {v4}, Lcom/peel/ui/b/av;->m(Lcom/peel/ui/b/av;)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/View;->getHeight()I

    move-result v4

    iget-object v5, p0, Lcom/peel/ui/b/ax;->a:Lcom/peel/ui/b/aw;

    iget-object v5, v5, Lcom/peel/ui/b/aw;->a:Lcom/peel/ui/b/av;

    invoke-static {v5}, Lcom/peel/ui/b/av;->n(Lcom/peel/ui/b/av;)I

    move-result v5

    add-int/2addr v4, v5

    if-ge v1, v4, :cond_2

    iget-object v1, p0, Lcom/peel/ui/b/ax;->a:Lcom/peel/ui/b/aw;

    iget-object v1, v1, Lcom/peel/ui/b/aw;->a:Lcom/peel/ui/b/av;

    invoke-static {v1}, Lcom/peel/ui/b/av;->l(Lcom/peel/ui/b/av;)J

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmp-long v1, v4, v6

    if-eqz v1, :cond_0

    const-wide/16 v4, 0x82

    cmp-long v1, v2, v4

    if-lez v1, :cond_2

    .line 239
    :cond_0
    iget-object v1, p0, Lcom/peel/ui/b/ax;->a:Lcom/peel/ui/b/aw;

    iget-object v1, v1, Lcom/peel/ui/b/aw;->a:Lcom/peel/ui/b/av;

    invoke-static {v1}, Lcom/peel/ui/b/av;->k(Lcom/peel/ui/b/av;)Lcom/peel/ui/ScrollPositionListView;

    move-result-object v1

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Lcom/peel/ui/ScrollPositionListView;->smoothScrollToPositionFromTop(II)V

    .line 240
    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 241
    iget-object v0, p0, Lcom/peel/ui/b/ax;->a:Lcom/peel/ui/b/aw;

    iget-object v0, v0, Lcom/peel/ui/b/aw;->a:Lcom/peel/ui/b/av;

    invoke-static {v0}, Lcom/peel/ui/b/av;->m(Lcom/peel/ui/b/av;)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 242
    iget-object v0, p0, Lcom/peel/ui/b/ax;->a:Lcom/peel/ui/b/aw;

    iget-object v0, v0, Lcom/peel/ui/b/aw;->a:Lcom/peel/ui/b/av;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-static {v0, v2, v3}, Lcom/peel/ui/b/av;->a(Lcom/peel/ui/b/av;J)J

    .line 249
    :cond_1
    :goto_0
    return-void

    .line 243
    :cond_2
    iget-object v1, p0, Lcom/peel/ui/b/ax;->a:Lcom/peel/ui/b/aw;

    iget-object v1, v1, Lcom/peel/ui/b/aw;->a:Lcom/peel/ui/b/av;

    invoke-static {v1}, Lcom/peel/ui/b/av;->m(Lcom/peel/ui/b/av;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-nez v1, :cond_1

    const/4 v1, 0x1

    invoke-virtual {p1, v1}, Landroid/widget/AbsListView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v1

    iget-object v4, p0, Lcom/peel/ui/b/ax;->a:Lcom/peel/ui/b/aw;

    iget-object v4, v4, Lcom/peel/ui/b/aw;->a:Lcom/peel/ui/b/av;

    invoke-static {v4}, Lcom/peel/ui/b/av;->m(Lcom/peel/ui/b/av;)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/View;->getHeight()I

    move-result v4

    iget-object v5, p0, Lcom/peel/ui/b/ax;->a:Lcom/peel/ui/b/aw;

    iget-object v5, v5, Lcom/peel/ui/b/aw;->a:Lcom/peel/ui/b/av;

    invoke-static {v5}, Lcom/peel/ui/b/av;->o(Lcom/peel/ui/b/av;)I

    move-result v5

    add-int/2addr v4, v5

    if-le v1, v4, :cond_1

    const-wide/16 v4, 0x82

    cmp-long v1, v2, v4

    if-gtz v1, :cond_3

    iget-object v1, p0, Lcom/peel/ui/b/ax;->a:Lcom/peel/ui/b/aw;

    iget-object v1, v1, Lcom/peel/ui/b/aw;->a:Lcom/peel/ui/b/av;

    invoke-static {v1}, Lcom/peel/ui/b/av;->l(Lcom/peel/ui/b/av;)J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-nez v1, :cond_1

    .line 244
    :cond_3
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 245
    iget-object v0, p0, Lcom/peel/ui/b/ax;->a:Lcom/peel/ui/b/aw;

    iget-object v0, v0, Lcom/peel/ui/b/aw;->a:Lcom/peel/ui/b/av;

    invoke-static {v0}, Lcom/peel/ui/b/av;->m(Lcom/peel/ui/b/av;)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 246
    iget-object v0, p0, Lcom/peel/ui/b/ax;->a:Lcom/peel/ui/b/aw;

    iget-object v0, v0, Lcom/peel/ui/b/aw;->a:Lcom/peel/ui/b/av;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-static {v0, v2, v3}, Lcom/peel/ui/b/av;->a(Lcom/peel/ui/b/av;J)J

    goto :goto_0
.end method

.method public onScrollStateChanged(Landroid/widget/AbsListView;I)V
    .locals 0

    .prologue
    .line 230
    return-void
.end method
