.class Lcom/peel/ui/b/g;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;


# instance fields
.field final synthetic a:Lcom/peel/ui/b/i;

.field final synthetic b:Lcom/peel/ui/b/c;


# direct methods
.method constructor <init>(Lcom/peel/ui/b/c;Lcom/peel/ui/b/i;)V
    .locals 0

    .prologue
    .line 190
    iput-object p1, p0, Lcom/peel/ui/b/g;->b:Lcom/peel/ui/b/c;

    iput-object p2, p0, Lcom/peel/ui/b/g;->a:Lcom/peel/ui/b/i;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onGlobalLayout()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 194
    iget-object v0, p0, Lcom/peel/ui/b/g;->a:Lcom/peel/ui/b/i;

    iget-object v0, v0, Lcom/peel/ui/b/i;->a:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    .line 195
    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->removeOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 196
    iget-object v0, p0, Lcom/peel/ui/b/g;->a:Lcom/peel/ui/b/i;

    iget-object v0, v0, Lcom/peel/ui/b/i;->a:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getLineCount()I

    move-result v0

    const/4 v1, 0x3

    if-le v0, v1, :cond_0

    iget-object v0, p0, Lcom/peel/ui/b/g;->a:Lcom/peel/ui/b/i;

    iget-object v0, v0, Lcom/peel/ui/b/i;->c:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_0

    .line 197
    iget-object v0, p0, Lcom/peel/ui/b/g;->a:Lcom/peel/ui/b/i;

    iget-object v0, v0, Lcom/peel/ui/b/i;->a:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getLayout()Landroid/text/Layout;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/text/Layout;->getLineEnd(I)I

    move-result v0

    .line 198
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/peel/ui/b/g;->a:Lcom/peel/ui/b/i;

    iget-object v2, v2, Lcom/peel/ui/b/i;->a:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    add-int/lit8 v0, v0, -0x3

    invoke-interface {v2, v3, v0}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "..."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 199
    iget-object v1, p0, Lcom/peel/ui/b/g;->a:Lcom/peel/ui/b/i;

    iget-object v1, v1, Lcom/peel/ui/b/i;->a:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 200
    iget-object v0, p0, Lcom/peel/ui/b/g;->a:Lcom/peel/ui/b/i;

    iget-object v0, v0, Lcom/peel/ui/b/i;->c:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 202
    :cond_0
    return-void
.end method
