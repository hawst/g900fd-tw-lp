.class Lcom/peel/ui/b/aq;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/peel/content/model/OVDWrapper;

.field final synthetic b:Landroid/os/Bundle;

.field final synthetic c:Lcom/peel/ui/b/ap;


# direct methods
.method constructor <init>(Lcom/peel/ui/b/ap;Lcom/peel/content/model/OVDWrapper;Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 505
    iput-object p1, p0, Lcom/peel/ui/b/aq;->c:Lcom/peel/ui/b/ap;

    iput-object p2, p0, Lcom/peel/ui/b/aq;->a:Lcom/peel/content/model/OVDWrapper;

    iput-object p3, p0, Lcom/peel/ui/b/aq;->b:Landroid/os/Bundle;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 508
    iget-object v0, p0, Lcom/peel/ui/b/aq;->a:Lcom/peel/content/model/OVDWrapper;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/peel/ui/b/aq;->a:Lcom/peel/content/model/OVDWrapper;

    invoke-virtual {v0}, Lcom/peel/content/model/OVDWrapper;->b()Ljava/util/Map;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/peel/ui/b/aq;->a:Lcom/peel/content/model/OVDWrapper;

    .line 509
    invoke-virtual {v0}, Lcom/peel/content/model/OVDWrapper;->b()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 510
    iget-object v0, p0, Lcom/peel/ui/b/aq;->a:Lcom/peel/content/model/OVDWrapper;

    invoke-virtual {v0}, Lcom/peel/content/model/OVDWrapper;->b()Ljava/util/Map;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/ui/b/aq;->b:Landroid/os/Bundle;

    const-string/jumbo v2, "selectedSeason"

    .line 511
    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 510
    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/content/model/Season;

    .line 512
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 513
    const-string/jumbo v2, "showId"

    iget-object v3, p0, Lcom/peel/ui/b/aq;->c:Lcom/peel/ui/b/ap;

    iget-object v3, v3, Lcom/peel/ui/b/ap;->b:Lcom/peel/content/listing/LiveListing;

    invoke-virtual {v3}, Lcom/peel/content/listing/LiveListing;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 514
    const-string/jumbo v2, "season"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 515
    iget-object v0, p0, Lcom/peel/ui/b/aq;->c:Lcom/peel/ui/b/ap;

    iget-object v0, v0, Lcom/peel/ui/b/ap;->c:Lcom/peel/ui/b/ag;

    invoke-static {v0}, Lcom/peel/ui/b/ag;->b(Lcom/peel/ui/b/ag;)Lcom/peel/ui/b/q;

    move-result-object v0

    new-instance v2, Lcom/peel/ui/b/bz;

    iget-object v3, p0, Lcom/peel/ui/b/aq;->b:Landroid/os/Bundle;

    const-string/jumbo v4, "selectedSeason"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {}, Lcom/peel/ui/b/ag;->T()I

    move-result v4

    invoke-direct {v2, v1, v3, v4}, Lcom/peel/ui/b/bz;-><init>(Landroid/os/Bundle;Ljava/lang/String;I)V

    invoke-virtual {v0, v2}, Lcom/peel/ui/b/q;->a(Lcom/peel/ui/b/af;)V

    .line 516
    iget-object v0, p0, Lcom/peel/ui/b/aq;->c:Lcom/peel/ui/b/ap;

    iget-object v0, v0, Lcom/peel/ui/b/ap;->c:Lcom/peel/ui/b/ag;

    invoke-static {v0}, Lcom/peel/ui/b/ag;->b(Lcom/peel/ui/b/ag;)Lcom/peel/ui/b/q;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/ui/b/q;->notifyDataSetChanged()V

    .line 518
    :cond_0
    return-void
.end method
