.class Lcom/peel/ui/b/bb;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lcom/peel/content/listing/Listing;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/peel/ui/b/ba;


# direct methods
.method constructor <init>(Lcom/peel/ui/b/ba;)V
    .locals 0

    .prologue
    .line 326
    iput-object p1, p0, Lcom/peel/ui/b/bb;->a:Lcom/peel/ui/b/ba;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/peel/content/listing/Listing;Lcom/peel/content/listing/Listing;)I
    .locals 4

    .prologue
    .line 329
    move-object v0, p1

    check-cast v0, Lcom/peel/content/listing/LiveListing;

    invoke-virtual {v0}, Lcom/peel/content/listing/LiveListing;->o()J

    move-result-wide v2

    move-object v0, p2

    check-cast v0, Lcom/peel/content/listing/LiveListing;

    invoke-virtual {v0}, Lcom/peel/content/listing/LiveListing;->o()J

    move-result-wide v0

    cmp-long v0, v2, v0

    if-gez v0, :cond_0

    .line 330
    const/4 v0, -0x1

    .line 335
    :goto_0
    return v0

    .line 331
    :cond_0
    check-cast p1, Lcom/peel/content/listing/LiveListing;

    invoke-virtual {p1}, Lcom/peel/content/listing/LiveListing;->o()J

    move-result-wide v0

    check-cast p2, Lcom/peel/content/listing/LiveListing;

    .line 332
    invoke-virtual {p2}, Lcom/peel/content/listing/LiveListing;->o()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-lez v0, :cond_1

    .line 333
    const/4 v0, 0x1

    goto :goto_0

    .line 335
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 326
    check-cast p1, Lcom/peel/content/listing/Listing;

    check-cast p2, Lcom/peel/content/listing/Listing;

    invoke-virtual {p0, p1, p2}, Lcom/peel/ui/b/bb;->a(Lcom/peel/content/listing/Listing;Lcom/peel/content/listing/Listing;)I

    move-result v0

    return v0
.end method
