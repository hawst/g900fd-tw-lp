.class Lcom/peel/ui/b/a/k;
.super Lcom/peel/util/t;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/peel/util/t",
        "<",
        "Ljava/util/List",
        "<",
        "Lcom/peel/content/listing/Listing;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/peel/content/listing/LiveListing;

.field final synthetic b:Lcom/peel/util/t;

.field final synthetic c:Lcom/peel/ui/b/a/a;


# direct methods
.method constructor <init>(Lcom/peel/ui/b/a/a;ILcom/peel/content/listing/LiveListing;Lcom/peel/util/t;)V
    .locals 0

    .prologue
    .line 923
    iput-object p1, p0, Lcom/peel/ui/b/a/k;->c:Lcom/peel/ui/b/a/a;

    iput-object p3, p0, Lcom/peel/ui/b/a/k;->a:Lcom/peel/content/listing/LiveListing;

    iput-object p4, p0, Lcom/peel/ui/b/a/k;->b:Lcom/peel/util/t;

    invoke-direct {p0, p2}, Lcom/peel/util/t;-><init>(I)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 926
    iget-boolean v0, p0, Lcom/peel/ui/b/a/k;->i:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/peel/ui/b/a/k;->j:Ljava/lang/Object;

    if-eqz v0, :cond_3

    .line 927
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 928
    iget-object v0, p0, Lcom/peel/ui/b/a/k;->j:Ljava/lang/Object;

    check-cast v0, Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/content/listing/Listing;

    .line 929
    invoke-virtual {v0}, Lcom/peel/content/listing/Listing;->f()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/peel/ui/b/a/k;->a:Lcom/peel/content/listing/LiveListing;

    invoke-virtual {v4}, Lcom/peel/content/listing/LiveListing;->f()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 933
    check-cast v0, Lcom/peel/content/listing/LiveListing;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 935
    :cond_1
    invoke-static {}, Lcom/peel/content/a;->a()Lcom/peel/data/ContentRoom;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/data/ContentRoom;->a()Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/peel/ui/b/a/k;->c:Lcom/peel/ui/b/a/a;

    iget-object v2, v2, Lcom/peel/ui/b/a/a;->k:Lcom/peel/content/library/LiveLibrary;

    invoke-virtual {v2}, Lcom/peel/content/library/LiveLibrary;->e()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v0, v2}, Lcom/peel/util/bx;->a(Ljava/util/List;Ljava/lang/String;Ljava/lang/String;)V

    .line 937
    iget-object v0, p0, Lcom/peel/ui/b/a/k;->b:Lcom/peel/util/t;

    if-eqz v0, :cond_2

    .line 938
    iget-object v0, p0, Lcom/peel/ui/b/a/k;->b:Lcom/peel/util/t;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1, v5}, Lcom/peel/util/t;->a(ZLjava/lang/Object;Ljava/lang/String;)V

    .line 943
    :cond_2
    :goto_1
    return-void

    .line 942
    :cond_3
    iget-object v0, p0, Lcom/peel/ui/b/a/k;->b:Lcom/peel/util/t;

    const/4 v1, 0x0

    invoke-virtual {v0, v1, v5, v5}, Lcom/peel/util/t;->a(ZLjava/lang/Object;Ljava/lang/String;)V

    goto :goto_1
.end method
