.class Lcom/peel/ui/b/be;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/peel/content/listing/LiveListing;

.field final synthetic b:Lcom/peel/ui/b/av;


# direct methods
.method constructor <init>(Lcom/peel/ui/b/av;Lcom/peel/content/listing/LiveListing;)V
    .locals 0

    .prologue
    .line 497
    iput-object p1, p0, Lcom/peel/ui/b/be;->b:Lcom/peel/ui/b/av;

    iput-object p2, p0, Lcom/peel/ui/b/be;->a:Lcom/peel/content/listing/LiveListing;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 500
    iget-object v0, p0, Lcom/peel/ui/b/be;->a:Lcom/peel/content/listing/LiveListing;

    invoke-virtual {v0}, Lcom/peel/content/listing/LiveListing;->k()Ljava/lang/String;

    move-result-object v1

    .line 501
    if-eqz v1, :cond_1

    .line 502
    const/4 v0, 0x0

    .line 503
    const-string/jumbo v2, "movie"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 504
    iget-object v0, p0, Lcom/peel/ui/b/be;->b:Lcom/peel/ui/b/av;

    sget v1, Lcom/peel/ui/ft;->movie:I

    invoke-virtual {v0, v1}, Lcom/peel/ui/b/av;->a(I)Ljava/lang/String;

    move-result-object v0

    .line 510
    :cond_0
    :goto_0
    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/peel/ui/b/be;->b:Lcom/peel/ui/b/av;

    iget-object v2, p0, Lcom/peel/ui/b/be;->b:Lcom/peel/ui/b/av;

    iget-object v2, v2, Lcom/peel/ui/b/av;->b:Lcom/peel/d/i;

    invoke-static {v1, v2, v0}, Lcom/peel/ui/b/av;->a(Lcom/peel/ui/b/av;Lcom/peel/d/i;Ljava/lang/String;)V

    .line 513
    :cond_1
    iget-object v0, p0, Lcom/peel/ui/b/be;->b:Lcom/peel/ui/b/av;

    invoke-static {v0}, Lcom/peel/ui/b/av;->g(Lcom/peel/ui/b/av;)Lcom/peel/ui/b/q;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 514
    iget-object v0, p0, Lcom/peel/ui/b/be;->b:Lcom/peel/ui/b/av;

    invoke-static {v0}, Lcom/peel/ui/b/av;->g(Lcom/peel/ui/b/av;)Lcom/peel/ui/b/q;

    move-result-object v0

    new-instance v1, Lcom/peel/ui/b/cd;

    iget-object v2, p0, Lcom/peel/ui/b/be;->b:Lcom/peel/ui/b/av;

    invoke-static {v2}, Lcom/peel/ui/b/av;->f(Lcom/peel/ui/b/av;)Lcom/peel/ui/b/a/a;

    move-result-object v2

    iget-object v3, p0, Lcom/peel/ui/b/be;->b:Lcom/peel/ui/b/av;

    invoke-virtual {v3}, Lcom/peel/ui/b/av;->m()Landroid/support/v4/app/ae;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/peel/ui/b/cd;-><init>(Lcom/peel/ui/b/a/a;Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Lcom/peel/ui/b/q;->a(Lcom/peel/ui/b/af;)V

    .line 515
    iget-object v0, p0, Lcom/peel/ui/b/be;->b:Lcom/peel/ui/b/av;

    invoke-static {v0}, Lcom/peel/ui/b/av;->g(Lcom/peel/ui/b/av;)Lcom/peel/ui/b/q;

    move-result-object v0

    new-instance v1, Lcom/peel/ui/b/o;

    iget-object v2, p0, Lcom/peel/ui/b/be;->b:Lcom/peel/ui/b/av;

    invoke-static {v2}, Lcom/peel/ui/b/av;->f(Lcom/peel/ui/b/av;)Lcom/peel/ui/b/a/a;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/peel/ui/b/o;-><init>(Lcom/peel/ui/b/a/a;)V

    invoke-virtual {v0, v1}, Lcom/peel/ui/b/q;->a(Lcom/peel/ui/b/af;)V

    .line 516
    iget-object v0, p0, Lcom/peel/ui/b/be;->b:Lcom/peel/ui/b/av;

    invoke-static {v0}, Lcom/peel/ui/b/av;->g(Lcom/peel/ui/b/av;)Lcom/peel/ui/b/q;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/ui/b/q;->notifyDataSetChanged()V

    .line 518
    :cond_2
    return-void

    .line 505
    :cond_3
    const-string/jumbo v2, "program"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 506
    iget-object v0, p0, Lcom/peel/ui/b/be;->b:Lcom/peel/ui/b/av;

    sget v1, Lcom/peel/ui/ft;->tvshow:I

    invoke-virtual {v0, v1}, Lcom/peel/ui/b/av;->a(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 507
    :cond_4
    const-string/jumbo v2, "sports"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 508
    iget-object v0, p0, Lcom/peel/ui/b/be;->b:Lcom/peel/ui/b/av;

    sget v1, Lcom/peel/ui/ft;->sports:I

    invoke-virtual {v0, v1}, Lcom/peel/ui/b/av;->a(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
