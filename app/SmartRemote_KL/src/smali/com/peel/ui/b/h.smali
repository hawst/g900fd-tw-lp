.class Lcom/peel/ui/b/h;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/peel/ui/b/i;

.field final synthetic b:Ljava/util/ArrayList;

.field final synthetic c:Lcom/peel/ui/b/c;


# direct methods
.method constructor <init>(Lcom/peel/ui/b/c;Lcom/peel/ui/b/i;Ljava/util/ArrayList;)V
    .locals 0

    .prologue
    .line 209
    iput-object p1, p0, Lcom/peel/ui/b/h;->c:Lcom/peel/ui/b/c;

    iput-object p2, p0, Lcom/peel/ui/b/h;->a:Lcom/peel/ui/b/i;

    iput-object p3, p0, Lcom/peel/ui/b/h;->b:Ljava/util/ArrayList;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 212
    iget-object v0, p0, Lcom/peel/ui/b/h;->a:Lcom/peel/ui/b/i;

    iget-object v0, v0, Lcom/peel/ui/b/i;->d:Lcom/peel/ui/AutoHeightGridView;

    invoke-virtual {v0}, Lcom/peel/ui/AutoHeightGridView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    if-nez v0, :cond_3

    .line 213
    iget-object v0, p0, Lcom/peel/ui/b/h;->a:Lcom/peel/ui/b/i;

    iget-object v0, v0, Lcom/peel/ui/b/i;->d:Lcom/peel/ui/AutoHeightGridView;

    new-instance v3, Lcom/peel/ui/b/l;

    iget-object v4, p0, Lcom/peel/ui/b/h;->c:Lcom/peel/ui/b/c;

    invoke-static {v4}, Lcom/peel/ui/b/c;->c(Lcom/peel/ui/b/c;)Landroid/content/Context;

    move-result-object v4

    iget-object v5, p0, Lcom/peel/ui/b/h;->b:Ljava/util/ArrayList;

    invoke-direct {v3, v4, v5}, Lcom/peel/ui/b/l;-><init>(Landroid/content/Context;Ljava/util/ArrayList;)V

    invoke-virtual {v0, v3}, Lcom/peel/ui/AutoHeightGridView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 218
    :goto_0
    iget-object v0, p0, Lcom/peel/ui/b/h;->a:Lcom/peel/ui/b/i;

    iget-object v0, v0, Lcom/peel/ui/b/i;->d:Lcom/peel/ui/AutoHeightGridView;

    new-instance v3, Lcom/peel/ui/b/k;

    iget-object v4, p0, Lcom/peel/ui/b/h;->c:Lcom/peel/ui/b/c;

    iget-object v5, p0, Lcom/peel/ui/b/h;->b:Ljava/util/ArrayList;

    invoke-direct {v3, v4, v5}, Lcom/peel/ui/b/k;-><init>(Lcom/peel/ui/b/c;Ljava/util/ArrayList;)V

    invoke-virtual {v0, v3}, Lcom/peel/ui/AutoHeightGridView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 219
    iget-object v0, p0, Lcom/peel/ui/b/h;->a:Lcom/peel/ui/b/i;

    iget-object v3, v0, Lcom/peel/ui/b/i;->b:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/peel/ui/b/h;->b:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/peel/ui/b/h;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_4

    :cond_0
    move v0, v2

    :goto_1
    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 221
    iget-object v0, p0, Lcom/peel/ui/b/h;->a:Lcom/peel/ui/b/i;

    iget-object v0, v0, Lcom/peel/ui/b/i;->d:Lcom/peel/ui/AutoHeightGridView;

    iget-object v3, p0, Lcom/peel/ui/b/h;->b:Ljava/util/ArrayList;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/peel/ui/b/h;->b:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_2

    :cond_1
    move v1, v2

    :cond_2
    invoke-virtual {v0, v1}, Lcom/peel/ui/AutoHeightGridView;->setVisibility(I)V

    .line 223
    return-void

    .line 215
    :cond_3
    iget-object v0, p0, Lcom/peel/ui/b/h;->a:Lcom/peel/ui/b/i;

    iget-object v0, v0, Lcom/peel/ui/b/i;->d:Lcom/peel/ui/AutoHeightGridView;

    invoke-virtual {v0}, Lcom/peel/ui/AutoHeightGridView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lcom/peel/ui/b/l;

    iget-object v3, p0, Lcom/peel/ui/b/h;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Lcom/peel/ui/b/l;->a(Ljava/util/ArrayList;)V

    goto :goto_0

    :cond_4
    move v0, v1

    .line 219
    goto :goto_1
.end method
