.class Lcom/peel/ui/b/a/aa;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/peel/ui/b/a/z;


# direct methods
.method constructor <init>(Lcom/peel/ui/b/a/z;)V
    .locals 0

    .prologue
    .line 793
    iput-object p1, p0, Lcom/peel/ui/b/a/aa;->a:Lcom/peel/ui/b/a/z;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 796
    iget-object v0, p0, Lcom/peel/ui/b/a/aa;->a:Lcom/peel/ui/b/a/z;

    iget-object v0, v0, Lcom/peel/ui/b/a/z;->c:Lcom/peel/ui/b/a/y;

    iget-object v0, v0, Lcom/peel/ui/b/a/y;->d:Lcom/peel/ui/b/a/a;

    new-instance v1, Lcom/peel/widget/ag;

    iget-object v2, p0, Lcom/peel/ui/b/a/aa;->a:Lcom/peel/ui/b/a/z;

    iget-object v2, v2, Lcom/peel/ui/b/a/z;->c:Lcom/peel/ui/b/a/y;

    iget-object v2, v2, Lcom/peel/ui/b/a/y;->d:Lcom/peel/ui/b/a/a;

    iget-object v2, v2, Lcom/peel/ui/b/a/a;->c:Landroid/support/v4/app/ae;

    invoke-direct {v1, v2}, Lcom/peel/widget/ag;-><init>(Landroid/content/Context;)V

    sget v2, Lcom/peel/ui/ft;->error:I

    .line 797
    invoke-virtual {v1, v2}, Lcom/peel/widget/ag;->a(I)Lcom/peel/widget/ag;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/peel/ui/b/a/aa;->a:Lcom/peel/ui/b/a/z;

    iget-object v3, v3, Lcom/peel/ui/b/a/z;->c:Lcom/peel/ui/b/a/y;

    iget-object v3, v3, Lcom/peel/ui/b/a/y;->d:Lcom/peel/ui/b/a/a;

    iget-object v3, v3, Lcom/peel/ui/b/a/a;->k:Lcom/peel/content/library/LiveLibrary;

    .line 798
    invoke-virtual {v3}, Lcom/peel/content/library/LiveLibrary;->e()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/peel/ui/b/a/aa;->a:Lcom/peel/ui/b/a/z;

    iget-object v3, v3, Lcom/peel/ui/b/a/z;->c:Lcom/peel/ui/b/a/y;

    iget-object v3, v3, Lcom/peel/ui/b/a/y;->d:Lcom/peel/ui/b/a/a;

    iget-object v3, v3, Lcom/peel/ui/b/a/a;->c:Landroid/support/v4/app/ae;

    invoke-virtual {v3}, Landroid/support/v4/app/ae;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/peel/ui/ft;->login_failed:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/peel/widget/ag;->b(Ljava/lang/CharSequence;)Lcom/peel/widget/ag;

    move-result-object v1

    sget v2, Lcom/peel/ui/ft;->okay:I

    const/4 v3, 0x0

    .line 799
    invoke-virtual {v1, v2, v3}, Lcom/peel/widget/ag;->a(ILandroid/content/DialogInterface$OnClickListener;)Lcom/peel/widget/ag;

    move-result-object v1

    iput-object v1, v0, Lcom/peel/ui/b/a/a;->f:Lcom/peel/widget/ag;

    .line 800
    iget-object v0, p0, Lcom/peel/ui/b/a/aa;->a:Lcom/peel/ui/b/a/z;

    iget-object v0, v0, Lcom/peel/ui/b/a/z;->c:Lcom/peel/ui/b/a/y;

    iget-object v0, v0, Lcom/peel/ui/b/a/y;->d:Lcom/peel/ui/b/a/a;

    iget-object v0, v0, Lcom/peel/ui/b/a/a;->f:Lcom/peel/widget/ag;

    iget-object v1, p0, Lcom/peel/ui/b/a/aa;->a:Lcom/peel/ui/b/a/z;

    iget-object v1, v1, Lcom/peel/ui/b/a/z;->c:Lcom/peel/ui/b/a/y;

    iget-object v1, v1, Lcom/peel/ui/b/a/y;->d:Lcom/peel/ui/b/a/a;

    iget-object v1, v1, Lcom/peel/ui/b/a/a;->f:Lcom/peel/widget/ag;

    invoke-virtual {v0, v1}, Lcom/peel/widget/ag;->a(Lcom/peel/widget/ag;)Lcom/peel/widget/ag;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/widget/ag;->d()Lcom/peel/widget/ag;

    .line 801
    return-void
.end method
