.class public Lcom/peel/ui/b/bq;
.super Lcom/peel/d/u;


# static fields
.field private static final g:Ljava/lang/String;


# instance fields
.field private aj:Landroid/widget/ExpandableListView;

.field private ak:Landroid/view/LayoutInflater;

.field private al:Lcom/peel/content/model/Season;

.field private am:Lcom/peel/content/model/OVDWrapper;

.field private an:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/peel/content/model/Season;",
            ">;"
        }
    .end annotation
.end field

.field private ao:I

.field private e:Landroid/widget/Spinner;

.field private f:Landroid/widget/ViewFlipper;

.field private h:Ljava/lang/String;

.field private i:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 37
    const-class v0, Lcom/peel/ui/b/bq;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/peel/ui/b/bq;->g:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Lcom/peel/d/u;-><init>()V

    return-void
.end method

.method private S()V
    .locals 3

    .prologue
    .line 125
    iget-object v0, p0, Lcom/peel/ui/b/bq;->an:Ljava/util/Map;

    iget-object v1, p0, Lcom/peel/ui/b/bq;->h:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/content/model/Season;

    iput-object v0, p0, Lcom/peel/ui/b/bq;->al:Lcom/peel/content/model/Season;

    .line 127
    iget-object v0, p0, Lcom/peel/ui/b/bq;->al:Lcom/peel/content/model/Season;

    invoke-virtual {v0}, Lcom/peel/content/model/Season;->b()Ljava/util/List;

    move-result-object v0

    .line 129
    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_1

    .line 130
    :cond_0
    sget-object v0, Lcom/peel/ui/b/bq;->g:Ljava/lang/String;

    const-string/jumbo v1, "get episodes from season"

    new-instance v2, Lcom/peel/ui/b/bu;

    invoke-direct {v2, p0}, Lcom/peel/ui/b/bu;-><init>(Lcom/peel/ui/b/bq;)V

    invoke-static {v0, v1, v2}, Lcom/peel/util/i;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    .line 147
    :goto_0
    return-void

    .line 145
    :cond_1
    invoke-direct {p0}, Lcom/peel/ui/b/bq;->T()V

    goto :goto_0
.end method

.method private T()V
    .locals 3

    .prologue
    .line 150
    sget-object v0, Lcom/peel/ui/b/bq;->g:Ljava/lang/String;

    const-string/jumbo v1, "render episodes"

    new-instance v2, Lcom/peel/ui/b/bw;

    invoke-direct {v2, p0}, Lcom/peel/ui/b/bw;-><init>(Lcom/peel/ui/b/bq;)V

    invoke-static {v0, v1, v2}, Lcom/peel/util/i;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/lang/Runnable;

    .line 187
    return-void
.end method

.method static synthetic a(Lcom/peel/ui/b/bq;)Landroid/view/LayoutInflater;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/peel/ui/b/bq;->ak:Landroid/view/LayoutInflater;

    return-object v0
.end method

.method static synthetic a(Lcom/peel/ui/b/bq;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 34
    iput-object p1, p0, Lcom/peel/ui/b/bq;->h:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic b(Lcom/peel/ui/b/bq;)V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Lcom/peel/ui/b/bq;->S()V

    return-void
.end method

.method static synthetic c(Lcom/peel/ui/b/bq;)Lcom/peel/content/model/OVDWrapper;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/peel/ui/b/bq;->am:Lcom/peel/content/model/OVDWrapper;

    return-object v0
.end method

.method static synthetic c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 34
    sget-object v0, Lcom/peel/ui/b/bq;->g:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic d(Lcom/peel/ui/b/bq;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/peel/ui/b/bq;->h:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic e(Lcom/peel/ui/b/bq;)V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Lcom/peel/ui/b/bq;->T()V

    return-void
.end method

.method static synthetic f(Lcom/peel/ui/b/bq;)Lcom/peel/content/model/Season;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/peel/ui/b/bq;->al:Lcom/peel/content/model/Season;

    return-object v0
.end method

.method static synthetic g(Lcom/peel/ui/b/bq;)Landroid/widget/ExpandableListView;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/peel/ui/b/bq;->aj:Landroid/widget/ExpandableListView;

    return-object v0
.end method

.method static synthetic h(Lcom/peel/ui/b/bq;)I
    .locals 1

    .prologue
    .line 34
    iget v0, p0, Lcom/peel/ui/b/bq;->ao:I

    return v0
.end method


# virtual methods
.method public Z()V
    .locals 6

    .prologue
    .line 191
    iget-object v0, p0, Lcom/peel/ui/b/bq;->d:Lcom/peel/d/a;

    if-nez v0, :cond_0

    .line 192
    new-instance v0, Lcom/peel/d/a;

    sget-object v1, Lcom/peel/d/d;->b:Lcom/peel/d/d;

    sget-object v2, Lcom/peel/d/b;->a:Lcom/peel/d/b;

    sget-object v3, Lcom/peel/d/c;->b:Lcom/peel/d/c;

    sget v4, Lcom/peel/ui/ft;->ondemand:I

    invoke-virtual {p0, v4}, Lcom/peel/ui/b/bq;->a(I)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, Lcom/peel/d/a;-><init>(Lcom/peel/d/d;Lcom/peel/d/b;Lcom/peel/d/c;Ljava/lang/String;Ljava/util/List;)V

    iput-object v0, p0, Lcom/peel/ui/b/bq;->d:Lcom/peel/d/a;

    .line 195
    :cond_0
    iget-object v0, p0, Lcom/peel/ui/b/bq;->b:Lcom/peel/d/i;

    iget-object v1, p0, Lcom/peel/ui/b/bq;->d:Lcom/peel/d/a;

    invoke-virtual {v0, v1}, Lcom/peel/d/i;->a(Lcom/peel/d/a;)V

    .line 196
    return-void
.end method

.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    .line 48
    sget v0, Lcom/peel/ui/fq;->show_details_ondemand:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 49
    sget v0, Lcom/peel/ui/fp;->season_spinner:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lcom/peel/ui/b/bq;->e:Landroid/widget/Spinner;

    .line 50
    sget v0, Lcom/peel/ui/fp;->episodes_list:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ExpandableListView;

    iput-object v0, p0, Lcom/peel/ui/b/bq;->aj:Landroid/widget/ExpandableListView;

    .line 51
    sget v0, Lcom/peel/ui/fp;->flipper:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ViewFlipper;

    iput-object v0, p0, Lcom/peel/ui/b/bq;->f:Landroid/widget/ViewFlipper;

    .line 52
    iput-object p1, p0, Lcom/peel/ui/b/bq;->ak:Landroid/view/LayoutInflater;

    .line 54
    return-object v1
.end method

.method public d(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    .line 59
    invoke-super {p0, p1}, Lcom/peel/d/u;->d(Landroid/os/Bundle;)V

    .line 61
    iget-object v0, p0, Lcom/peel/ui/b/bq;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "showId"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/ui/b/bq;->i:Ljava/lang/String;

    .line 62
    iget-object v0, p0, Lcom/peel/ui/b/bq;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "selectedSeason"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/ui/b/bq;->h:Ljava/lang/String;

    .line 63
    iget-object v0, p0, Lcom/peel/ui/b/bq;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "contextId"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/peel/ui/b/bq;->ao:I

    .line 65
    sget-object v0, Lcom/peel/content/a;->a:Lcom/peel/util/bo;

    iget-object v1, p0, Lcom/peel/ui/b/bq;->i:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/peel/util/bo;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/content/model/OVDWrapper;

    iput-object v0, p0, Lcom/peel/ui/b/bq;->am:Lcom/peel/content/model/OVDWrapper;

    .line 67
    iget-object v0, p0, Lcom/peel/ui/b/bq;->am:Lcom/peel/content/model/OVDWrapper;

    if-nez v0, :cond_0

    .line 122
    :goto_0
    return-void

    .line 68
    :cond_0
    iget-object v0, p0, Lcom/peel/ui/b/bq;->aj:Landroid/widget/ExpandableListView;

    new-instance v1, Lcom/peel/ui/b/c;

    invoke-virtual {p0}, Lcom/peel/ui/b/bq;->m()Landroid/support/v4/app/ae;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/peel/ui/b/c;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Landroid/widget/ExpandableListView;->setAdapter(Landroid/widget/ExpandableListAdapter;)V

    .line 70
    iget-object v0, p0, Lcom/peel/ui/b/bq;->am:Lcom/peel/content/model/OVDWrapper;

    invoke-virtual {v0}, Lcom/peel/content/model/OVDWrapper;->b()Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/ui/b/bq;->an:Ljava/util/Map;

    .line 72
    iget-object v0, p0, Lcom/peel/ui/b/bq;->an:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    const/4 v1, 0x1

    if-le v0, v1, :cond_1

    .line 73
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/peel/ui/b/bq;->an:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 75
    new-instance v1, Lcom/peel/ui/b/br;

    invoke-direct {v1, p0}, Lcom/peel/ui/b/br;-><init>(Lcom/peel/ui/b/bq;)V

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 88
    iget-object v1, p0, Lcom/peel/ui/b/bq;->f:Landroid/widget/ViewFlipper;

    iget-object v2, p0, Lcom/peel/ui/b/bq;->f:Landroid/widget/ViewFlipper;

    iget-object v3, p0, Lcom/peel/ui/b/bq;->e:Landroid/widget/Spinner;

    invoke-virtual {v2, v3}, Landroid/widget/ViewFlipper;->indexOfChild(Landroid/view/View;)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/ViewFlipper;->setDisplayedChild(I)V

    .line 90
    iget-object v1, p0, Lcom/peel/ui/b/bq;->e:Landroid/widget/Spinner;

    new-instance v2, Lcom/peel/ui/b/bs;

    invoke-virtual {p0}, Lcom/peel/ui/b/bq;->m()Landroid/support/v4/app/ae;

    move-result-object v3

    sget v4, Lcom/peel/ui/fq;->show_details_ondemand_spinneritem:I

    invoke-direct {v2, p0, v3, v4, v0}, Lcom/peel/ui/b/bs;-><init>(Lcom/peel/ui/b/bq;Landroid/content/Context;ILjava/util/List;)V

    invoke-virtual {v1, v2}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 107
    iget-object v0, p0, Lcom/peel/ui/b/bq;->e:Landroid/widget/Spinner;

    new-instance v1, Lcom/peel/ui/b/bt;

    invoke-direct {v1, p0}, Lcom/peel/ui/b/bt;-><init>(Lcom/peel/ui/b/bq;)V

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    goto :goto_0

    .line 117
    :cond_1
    invoke-direct {p0}, Lcom/peel/ui/b/bq;->S()V

    .line 118
    iget-object v0, p0, Lcom/peel/ui/b/bq;->f:Landroid/widget/ViewFlipper;

    sget v1, Lcom/peel/ui/fp;->season_text:I

    invoke-virtual {v0, v1}, Landroid/widget/ViewFlipper;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 119
    iget-object v1, p0, Lcom/peel/ui/b/bq;->f:Landroid/widget/ViewFlipper;

    iget-object v2, p0, Lcom/peel/ui/b/bq;->f:Landroid/widget/ViewFlipper;

    invoke-virtual {v2, v0}, Landroid/widget/ViewFlipper;->indexOfChild(Landroid/view/View;)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/ViewFlipper;->setDisplayedChild(I)V

    .line 120
    iget-object v1, p0, Lcom/peel/ui/b/bq;->al:Lcom/peel/content/model/Season;

    invoke-virtual {v1}, Lcom/peel/content/model/Season;->a()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "Unknown"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {p0}, Lcom/peel/ui/b/bq;->n()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/peel/ui/ft;->unknown_season:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    :goto_1
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    :cond_2
    iget-object v1, p0, Lcom/peel/ui/b/bq;->al:Lcom/peel/content/model/Season;

    invoke-virtual {v1}, Lcom/peel/content/model/Season;->a()Ljava/lang/String;

    move-result-object v1

    goto :goto_1
.end method
