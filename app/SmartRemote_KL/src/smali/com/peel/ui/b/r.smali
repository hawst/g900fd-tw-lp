.class public Lcom/peel/ui/b/r;
.super Lcom/peel/ui/b/af;


# instance fields
.field private final c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/peel/ui/model/TwittData;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Landroid/support/v4/app/ae;

.field private final e:[Ljava/lang/String;

.field private final f:I

.field private final g:Ljava/lang/String;

.field private final h:Ljava/lang/String;

.field private final i:Lcom/peel/d/v;

.field private final j:Lcom/peel/ui/b/a/a;


# direct methods
.method public constructor <init>(Ljava/util/List;Landroid/support/v4/app/ae;[Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Lcom/peel/ui/b/a/a;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/peel/ui/model/TwittData;",
            ">;",
            "Landroid/support/v4/app/ae;",
            "[",
            "Ljava/lang/String;",
            "I",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/peel/ui/b/a/a;",
            ")V"
        }
    .end annotation

    .prologue
    .line 53
    invoke-direct {p0}, Lcom/peel/ui/b/af;-><init>()V

    .line 54
    iput-object p2, p0, Lcom/peel/ui/b/r;->d:Landroid/support/v4/app/ae;

    .line 55
    iput-object p1, p0, Lcom/peel/ui/b/r;->c:Ljava/util/List;

    .line 56
    iput-object p3, p0, Lcom/peel/ui/b/r;->e:[Ljava/lang/String;

    .line 57
    iput p4, p0, Lcom/peel/ui/b/r;->f:I

    .line 58
    iput-object p5, p0, Lcom/peel/ui/b/r;->g:Ljava/lang/String;

    .line 59
    iput-object p6, p0, Lcom/peel/ui/b/r;->h:Ljava/lang/String;

    .line 60
    invoke-static {p2}, Lcom/peel/d/v;->a(Landroid/content/Context;)Lcom/peel/d/v;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/ui/b/r;->i:Lcom/peel/d/v;

    .line 61
    iput-object p7, p0, Lcom/peel/ui/b/r;->j:Lcom/peel/ui/b/a/a;

    .line 62
    return-void
.end method

.method static synthetic a(Lcom/peel/ui/b/r;)Lcom/peel/d/v;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/peel/ui/b/r;->i:Lcom/peel/d/v;

    return-object v0
.end method

.method private b()V
    .locals 3

    .prologue
    .line 320
    new-instance v0, Lcom/peel/widget/ag;

    iget-object v1, p0, Lcom/peel/ui/b/r;->d:Landroid/support/v4/app/ae;

    invoke-direct {v0, v1}, Lcom/peel/widget/ag;-><init>(Landroid/content/Context;)V

    sget v1, Lcom/peel/ui/ft;->login_twitter:I

    invoke-virtual {v0, v1}, Lcom/peel/widget/ag;->a(I)Lcom/peel/widget/ag;

    move-result-object v0

    sget v1, Lcom/peel/ui/ft;->twitter_login_desc:I

    invoke-virtual {v0, v1}, Lcom/peel/widget/ag;->b(I)Lcom/peel/widget/ag;

    move-result-object v0

    sget v1, Lcom/peel/ui/ft;->login:I

    new-instance v2, Lcom/peel/ui/b/ad;

    invoke-direct {v2, p0}, Lcom/peel/ui/b/ad;-><init>(Lcom/peel/ui/b/r;)V

    invoke-virtual {v0, v1, v2}, Lcom/peel/widget/ag;->a(ILandroid/content/DialogInterface$OnClickListener;)Lcom/peel/widget/ag;

    move-result-object v0

    sget v1, Lcom/peel/ui/ft;->cancel:I

    new-instance v2, Lcom/peel/ui/b/ac;

    invoke-direct {v2, p0}, Lcom/peel/ui/b/ac;-><init>(Lcom/peel/ui/b/r;)V

    .line 330
    invoke-virtual {v0, v1, v2}, Lcom/peel/widget/ag;->c(ILandroid/content/DialogInterface$OnClickListener;)Lcom/peel/widget/ag;

    move-result-object v0

    .line 335
    invoke-virtual {v0}, Lcom/peel/widget/ag;->d()Lcom/peel/widget/ag;

    .line 336
    return-void
.end method

.method static synthetic b(Lcom/peel/ui/b/r;)V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0}, Lcom/peel/ui/b/r;->b()V

    return-void
.end method

.method static synthetic c(Lcom/peel/ui/b/r;)I
    .locals 1

    .prologue
    .line 42
    iget v0, p0, Lcom/peel/ui/b/r;->f:I

    return v0
.end method

.method static synthetic d(Lcom/peel/ui/b/r;)[Ljava/lang/String;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/peel/ui/b/r;->e:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic e(Lcom/peel/ui/b/r;)Landroid/support/v4/app/ae;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/peel/ui/b/r;->d:Landroid/support/v4/app/ae;

    return-object v0
.end method

.method static synthetic f(Lcom/peel/ui/b/r;)Ljava/util/List;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/peel/ui/b/r;->c:Ljava/util/List;

    return-object v0
.end method

.method static synthetic g(Lcom/peel/ui/b/r;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/peel/ui/b/r;->g:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic h(Lcom/peel/ui/b/r;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/peel/ui/b/r;->h:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 66
    const/4 v0, 0x3

    return v0
.end method

.method public a(Landroid/view/LayoutInflater;Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 24

    .prologue
    .line 71
    if-nez p2, :cond_5

    new-instance v4, Lcom/peel/ui/b/ae;

    move-object/from16 v0, p0

    invoke-direct {v4, v0}, Lcom/peel/ui/b/ae;-><init>(Lcom/peel/ui/b/r;)V

    move-object/from16 v16, v4

    .line 73
    :goto_0
    if-nez p2, :cond_14

    .line 74
    sget v4, Lcom/peel/ui/fq;->show_detail_tweet_layout:I

    const/4 v5, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, p3

    invoke-virtual {v0, v4, v1, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v5

    .line 75
    sget v4, Lcom/peel/ui/fp;->tweet_list:I

    invoke-virtual {v5, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/LinearLayout;

    move-object/from16 v0, v16

    iput-object v4, v0, Lcom/peel/ui/b/ae;->a:Landroid/widget/LinearLayout;

    .line 76
    sget v4, Lcom/peel/ui/fp;->more_bottom:I

    invoke-virtual {v5, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    move-object/from16 v0, v16

    iput-object v4, v0, Lcom/peel/ui/b/ae;->c:Landroid/view/View;

    .line 77
    sget v4, Lcom/peel/ui/fp;->refresh_btn:I

    invoke-virtual {v5, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageView;

    move-object/from16 v0, v16

    iput-object v4, v0, Lcom/peel/ui/b/ae;->b:Landroid/widget/ImageView;

    .line 79
    move-object/from16 v0, v16

    invoke-virtual {v5, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 82
    :goto_1
    move-object/from16 v0, v16

    iget-object v4, v0, Lcom/peel/ui/b/ae;->a:Landroid/widget/LinearLayout;

    invoke-virtual {v4}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 83
    move-object/from16 v0, v16

    iget-object v4, v0, Lcom/peel/ui/b/ae;->b:Landroid/widget/ImageView;

    const/4 v6, 0x1

    invoke-virtual {v4, v6}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 84
    move-object/from16 v0, v16

    iget-object v4, v0, Lcom/peel/ui/b/ae;->b:Landroid/widget/ImageView;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/peel/ui/b/r;->g:Ljava/lang/String;

    invoke-virtual {v4, v6}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    .line 86
    move-object/from16 v0, v16

    iget-object v4, v0, Lcom/peel/ui/b/ae;->b:Landroid/widget/ImageView;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/peel/ui/b/r;->j:Lcom/peel/ui/b/a/a;

    invoke-virtual {v4, v6}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 88
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/peel/ui/b/r;->c:Ljava/util/List;

    if-eqz v4, :cond_12

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/peel/ui/b/r;->c:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-lez v4, :cond_12

    .line 89
    const/4 v4, 0x0

    move v15, v4

    :goto_2
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/peel/ui/b/r;->c:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-ge v15, v4, :cond_10

    const/4 v4, 0x3

    if-ge v15, v4, :cond_10

    .line 90
    sget v6, Lcom/peel/ui/fq;->tweet_list_item:I

    move-object v4, v5

    check-cast v4, Landroid/view/ViewGroup;

    const/4 v7, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v6, v4, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v17

    .line 91
    sget v4, Lcom/peel/ui/fp;->contents:I

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 92
    sget v6, Lcom/peel/ui/fp;->userid:I

    move-object/from16 v0, v17

    invoke-virtual {v0, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    .line 93
    sget v7, Lcom/peel/ui/fp;->username:I

    move-object/from16 v0, v17

    invoke-virtual {v0, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    .line 94
    sget v8, Lcom/peel/ui/fp;->media_image:I

    move-object/from16 v0, v17

    invoke-virtual {v0, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/ImageView;

    .line 95
    sget v9, Lcom/peel/ui/fp;->user_image:I

    move-object/from16 v0, v17

    invoke-virtual {v0, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/ImageView;

    .line 96
    sget v10, Lcom/peel/ui/fp;->timestamp:I

    move-object/from16 v0, v17

    invoke-virtual {v0, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/TextView;

    .line 97
    sget v11, Lcom/peel/ui/fp;->retweet_btn:I

    move-object/from16 v0, v17

    invoke-virtual {v0, v11}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/TextView;

    .line 98
    sget v12, Lcom/peel/ui/fp;->favorite_btn:I

    move-object/from16 v0, v17

    invoke-virtual {v0, v12}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v12

    check-cast v12, Landroid/widget/TextView;

    .line 99
    sget v13, Lcom/peel/ui/fp;->reply_btn:I

    move-object/from16 v0, v17

    invoke-virtual {v0, v13}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v13

    check-cast v13, Landroid/widget/TextView;

    .line 101
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/peel/ui/b/r;->c:Ljava/util/List;

    invoke-interface {v14, v15}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/peel/ui/model/TwittData;

    .line 106
    invoke-virtual {v14}, Lcom/peel/ui/model/TwittData;->b()Ljava/lang/String;

    move-result-object v18

    if-eqz v18, :cond_1

    .line 107
    new-instance v18, Ljava/lang/StringBuilder;

    invoke-virtual {v14}, Lcom/peel/ui/model/TwittData;->b()Ljava/lang/String;

    move-result-object v19

    invoke-direct/range {v18 .. v19}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 109
    invoke-virtual {v14}, Lcom/peel/ui/model/TwittData;->b()Ljava/lang/String;

    move-result-object v19

    const/16 v20, 0x0

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->charAt(I)C

    move-result v19

    const/16 v20, 0x40

    move/from16 v0, v19

    move/from16 v1, v20

    if-eq v0, v1, :cond_0

    .line 110
    const/16 v19, 0x0

    const-string/jumbo v20, "@"

    invoke-virtual/range {v18 .. v20}, Ljava/lang/StringBuilder;->insert(ILjava/lang/String;)Ljava/lang/StringBuilder;

    .line 112
    :cond_0
    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v6, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 115
    :cond_1
    invoke-virtual {v14}, Lcom/peel/ui/model/TwittData;->e()Ljava/lang/String;

    move-result-object v18

    if-eqz v18, :cond_2

    .line 116
    invoke-virtual {v14}, Lcom/peel/ui/model/TwittData;->e()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v7, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 119
    :cond_2
    invoke-virtual {v14}, Lcom/peel/ui/model/TwittData;->d()Ljava/lang/String;

    move-result-object v7

    if-eqz v7, :cond_3

    .line 120
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/peel/ui/b/r;->d:Landroid/support/v4/app/ae;

    invoke-static {v7}, Lcom/peel/util/b/e;->a(Landroid/content/Context;)Lcom/squareup/picasso/Picasso;

    move-result-object v7

    invoke-virtual {v14}, Lcom/peel/ui/model/TwittData;->d()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v7, v0}, Lcom/squareup/picasso/Picasso;->load(Ljava/lang/String;)Lcom/squareup/picasso/RequestCreator;

    move-result-object v7

    invoke-virtual {v7, v9}, Lcom/squareup/picasso/RequestCreator;->into(Landroid/widget/ImageView;)V

    .line 123
    :cond_3
    new-instance v9, Landroid/text/SpannableStringBuilder;

    invoke-virtual {v14}, Lcom/peel/ui/model/TwittData;->a()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v9, v7}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 124
    invoke-virtual {v14}, Lcom/peel/ui/model/TwittData;->h()[Lcom/peel/ui/model/TwittData$TwittEntries;

    move-result-object v18

    .line 125
    if-eqz v18, :cond_6

    move-object/from16 v0, v18

    array-length v7, v0

    if-lez v7, :cond_6

    .line 126
    move-object/from16 v0, v18

    array-length v0, v0

    move/from16 v19, v0

    const/4 v7, 0x0

    :goto_3
    move/from16 v0, v19

    if-ge v7, v0, :cond_6

    aget-object v20, v18, v7

    .line 127
    invoke-virtual {v14}, Lcom/peel/ui/model/TwittData;->a()Ljava/lang/String;

    .line 128
    invoke-virtual {v9}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v21

    invoke-virtual/range {v20 .. v20}, Lcom/peel/ui/model/TwittData$TwittEntries;->a()I

    move-result v22

    add-int/lit8 v22, v22, 0x1

    move/from16 v0, v21

    move/from16 v1, v22

    if-le v0, v1, :cond_4

    invoke-virtual {v9}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v21

    invoke-virtual/range {v20 .. v20}, Lcom/peel/ui/model/TwittData$TwittEntries;->b()I

    move-result v22

    move/from16 v0, v21

    move/from16 v1, v22

    if-lt v0, v1, :cond_4

    .line 129
    new-instance v21, Landroid/text/style/StyleSpan;

    const/16 v22, 0x1

    invoke-direct/range {v21 .. v22}, Landroid/text/style/StyleSpan;-><init>(I)V

    invoke-virtual/range {v20 .. v20}, Lcom/peel/ui/model/TwittData$TwittEntries;->a()I

    move-result v22

    invoke-virtual/range {v20 .. v20}, Lcom/peel/ui/model/TwittData$TwittEntries;->b()I

    move-result v20

    const/16 v23, 0x12

    move-object/from16 v0, v21

    move/from16 v1, v22

    move/from16 v2, v20

    move/from16 v3, v23

    invoke-virtual {v9, v0, v1, v2, v3}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 126
    :cond_4
    add-int/lit8 v7, v7, 0x1

    goto :goto_3

    .line 71
    :cond_5
    invoke-virtual/range {p2 .. p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/peel/ui/b/ae;

    move-object/from16 v16, v4

    goto/16 :goto_0

    .line 133
    :cond_6
    invoke-virtual {v14}, Lcom/peel/ui/model/TwittData;->i()[Lcom/peel/ui/model/TwittData$TwittEntries;

    move-result-object v18

    .line 134
    if-eqz v18, :cond_7

    move-object/from16 v0, v18

    array-length v7, v0

    if-lez v7, :cond_7

    .line 135
    move-object/from16 v0, v18

    array-length v0, v0

    move/from16 v19, v0

    const/4 v7, 0x0

    :goto_4
    move/from16 v0, v19

    if-ge v7, v0, :cond_7

    aget-object v20, v18, v7

    .line 136
    new-instance v21, Lcom/peel/util/URLSpanNodeUnderline;

    new-instance v22, Landroid/text/style/URLSpan;

    invoke-virtual/range {v20 .. v20}, Lcom/peel/ui/model/TwittData$TwittEntries;->c()Ljava/lang/String;

    move-result-object v23

    invoke-direct/range {v22 .. v23}, Landroid/text/style/URLSpan;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v22 .. v22}, Landroid/text/style/URLSpan;->getURL()Ljava/lang/String;

    move-result-object v22

    invoke-direct/range {v21 .. v22}, Lcom/peel/util/URLSpanNodeUnderline;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v20 .. v20}, Lcom/peel/ui/model/TwittData$TwittEntries;->a()I

    move-result v22

    invoke-virtual/range {v20 .. v20}, Lcom/peel/ui/model/TwittData$TwittEntries;->b()I

    move-result v20

    const/16 v23, 0x12

    move-object/from16 v0, v21

    move/from16 v1, v22

    move/from16 v2, v20

    move/from16 v3, v23

    invoke-virtual {v9, v0, v1, v2, v3}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 135
    add-int/lit8 v7, v7, 0x1

    goto :goto_4

    .line 140
    :cond_7
    invoke-virtual {v14}, Lcom/peel/ui/model/TwittData;->j()[Lcom/peel/ui/model/TwittData$TwittEntries;

    move-result-object v7

    .line 141
    if-eqz v7, :cond_a

    array-length v0, v7

    move/from16 v18, v0

    if-lez v18, :cond_a

    .line 142
    const/16 v18, 0x0

    move/from16 v0, v18

    invoke-virtual {v8, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 143
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/peel/ui/b/r;->d:Landroid/support/v4/app/ae;

    move-object/from16 v18, v0

    invoke-static/range {v18 .. v18}, Lcom/peel/util/b/e;->a(Landroid/content/Context;)Lcom/squareup/picasso/Picasso;

    move-result-object v18

    const/16 v19, 0x0

    aget-object v7, v7, v19

    invoke-virtual {v7}, Lcom/peel/ui/model/TwittData$TwittEntries;->c()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, v18

    invoke-virtual {v0, v7}, Lcom/squareup/picasso/Picasso;->load(Ljava/lang/String;)Lcom/squareup/picasso/RequestCreator;

    move-result-object v7

    invoke-virtual {v7, v8}, Lcom/squareup/picasso/RequestCreator;->into(Landroid/widget/ImageView;)V

    .line 148
    :goto_5
    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v7

    invoke-virtual {v4, v7}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 149
    invoke-virtual {v4, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 151
    invoke-virtual {v14}, Lcom/peel/ui/model/TwittData;->g()Z

    move-result v4

    if-eqz v4, :cond_b

    .line 152
    sget v4, Lcom/peel/ui/fo;->favorite_icon_press:I

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual {v12, v4, v7, v8, v9}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    .line 157
    :goto_6
    invoke-virtual {v14}, Lcom/peel/ui/model/TwittData;->f()Z

    move-result v4

    if-eqz v4, :cond_c

    .line 158
    sget v4, Lcom/peel/ui/fo;->retweet_icon_press:I

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual {v11, v4, v7, v8, v9}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    .line 163
    :goto_7
    new-instance v4, Lcom/peel/ui/b/s;

    move-object/from16 v0, p0

    invoke-direct {v4, v0, v14, v11}, Lcom/peel/ui/b/s;-><init>(Lcom/peel/ui/b/r;Lcom/peel/ui/model/TwittData;Landroid/widget/TextView;)V

    invoke-virtual {v11, v4}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 196
    new-instance v4, Lcom/peel/ui/b/v;

    move-object/from16 v0, p0

    invoke-direct {v4, v0, v14, v12}, Lcom/peel/ui/b/v;-><init>(Lcom/peel/ui/b/r;Lcom/peel/ui/model/TwittData;Landroid/widget/TextView;)V

    invoke-virtual {v12, v4}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 226
    new-instance v4, Lcom/peel/ui/b/y;

    move-object/from16 v0, p0

    invoke-direct {v4, v0, v6}, Lcom/peel/ui/b/y;-><init>(Lcom/peel/ui/b/r;Landroid/widget/TextView;)V

    invoke-virtual {v13, v4}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 242
    invoke-virtual {v14}, Lcom/peel/ui/model/TwittData;->k()I

    move-result v4

    .line 245
    const/16 v6, 0x3c

    if-ge v4, v6, :cond_d

    .line 246
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v6, "s"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 255
    :goto_8
    invoke-virtual {v10, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 257
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/peel/ui/b/r;->c:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    if-eq v15, v4, :cond_8

    const/4 v4, 0x2

    if-ne v15, v4, :cond_9

    .line 258
    :cond_8
    sget v4, Lcom/peel/ui/fp;->seperator:I

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    const/16 v6, 0x8

    invoke-virtual {v4, v6}, Landroid/view/View;->setVisibility(I)V

    .line 260
    :cond_9
    move-object/from16 v0, v16

    iget-object v4, v0, Lcom/peel/ui/b/ae;->a:Landroid/widget/LinearLayout;

    move-object/from16 v0, v17

    invoke-virtual {v4, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 89
    add-int/lit8 v4, v15, 0x1

    move v15, v4

    goto/16 :goto_2

    .line 145
    :cond_a
    const/16 v7, 0x8

    invoke-virtual {v8, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_5

    .line 154
    :cond_b
    sget v4, Lcom/peel/ui/fo;->favorite_icon_normal:I

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual {v12, v4, v7, v8, v9}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    goto/16 :goto_6

    .line 160
    :cond_c
    sget v4, Lcom/peel/ui/fo;->retweet_icon_normal:I

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual {v11, v4, v7, v8, v9}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    goto :goto_7

    .line 247
    :cond_d
    const/16 v6, 0x3c

    if-lt v4, v6, :cond_e

    const/16 v6, 0xe10

    if-ge v4, v6, :cond_e

    .line 248
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    div-int/lit8 v4, v4, 0x3c

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v6, "m"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    goto :goto_8

    .line 249
    :cond_e
    const/16 v6, 0xe10

    if-lt v4, v6, :cond_f

    const v6, 0x15180

    if-ge v4, v6, :cond_f

    .line 250
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    div-int/lit16 v4, v4, 0xe10

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v6, "h"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_8

    .line 252
    :cond_f
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const v7, 0x15180

    div-int/2addr v4, v7

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v6, "d"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_8

    .line 262
    :cond_10
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/peel/ui/b/r;->c:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-lez v4, :cond_11

    .line 263
    move-object/from16 v0, v16

    iget-object v4, v0, Lcom/peel/ui/b/ae;->c:Landroid/view/View;

    const/4 v6, 0x0

    invoke-virtual {v4, v6}, Landroid/view/View;->setVisibility(I)V

    .line 264
    move-object/from16 v0, v16

    iget-object v4, v0, Lcom/peel/ui/b/ae;->c:Landroid/view/View;

    new-instance v6, Lcom/peel/ui/b/z;

    move-object/from16 v0, p0

    invoke-direct {v6, v0}, Lcom/peel/ui/b/z;-><init>(Lcom/peel/ui/b/r;)V

    invoke-virtual {v4, v6}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 316
    :cond_11
    :goto_9
    return-object v5

    .line 279
    :cond_12
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/peel/ui/b/r;->i:Lcom/peel/d/v;

    invoke-virtual {v4}, Lcom/peel/d/v;->b()Lcom/peel/d/af;

    move-result-object v4

    if-nez v4, :cond_13

    .line 280
    move-object/from16 v0, v16

    iget-object v4, v0, Lcom/peel/ui/b/ae;->b:Landroid/widget/ImageView;

    const/16 v6, 0x8

    invoke-virtual {v4, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 282
    sget v6, Lcom/peel/ui/fq;->show_details_twitter_login:I

    move-object v4, v5

    check-cast v4, Landroid/view/ViewGroup;

    const/4 v7, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v6, v4, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v4

    .line 283
    sget v6, Lcom/peel/ui/fp;->login_btn:I

    invoke-virtual {v4, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    new-instance v7, Lcom/peel/ui/b/aa;

    move-object/from16 v0, p0

    invoke-direct {v7, v0}, Lcom/peel/ui/b/aa;-><init>(Lcom/peel/ui/b/r;)V

    invoke-virtual {v6, v7}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 296
    move-object/from16 v0, v16

    iget-object v6, v0, Lcom/peel/ui/b/ae;->a:Landroid/widget/LinearLayout;

    invoke-virtual {v6, v4}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    goto :goto_9

    .line 298
    :cond_13
    sget v6, Lcom/peel/ui/fq;->tweet_stream_header_with_text:I

    move-object v4, v5

    check-cast v4, Landroid/view/ViewGroup;

    const/4 v7, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v6, v4, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v4

    .line 299
    new-instance v6, Lcom/peel/ui/b/ab;

    move-object/from16 v0, p0

    invoke-direct {v6, v0}, Lcom/peel/ui/b/ab;-><init>(Lcom/peel/ui/b/r;)V

    invoke-virtual {v4, v6}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 312
    move-object/from16 v0, v16

    iget-object v6, v0, Lcom/peel/ui/b/ae;->a:Landroid/widget/LinearLayout;

    invoke-virtual {v6, v4}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    goto :goto_9

    :cond_14
    move-object/from16 v5, p2

    goto/16 :goto_1
.end method
