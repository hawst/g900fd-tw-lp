.class Lcom/peel/ui/b/az;
.super Lcom/peel/util/t;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/peel/util/t",
        "<",
        "Ljava/util/List",
        "<",
        "Lcom/peel/content/listing/Listing;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/peel/ui/b/av;


# direct methods
.method constructor <init>(Lcom/peel/ui/b/av;I)V
    .locals 0

    .prologue
    .line 291
    iput-object p1, p0, Lcom/peel/ui/b/az;->a:Lcom/peel/ui/b/av;

    invoke-direct {p0, p2}, Lcom/peel/util/t;-><init>(I)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 294
    iget-boolean v0, p0, Lcom/peel/ui/b/az;->i:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/peel/ui/b/az;->j:Ljava/lang/Object;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/peel/ui/b/az;->j:Ljava/lang/Object;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/peel/ui/b/az;->j:Ljava/lang/Object;

    check-cast v0, Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_1

    .line 295
    :cond_0
    invoke-static {}, Lcom/peel/ui/b/av;->c()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "\nresult: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/peel/ui/b/az;->j:Ljava/lang/Object;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "\nmsg = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/peel/ui/b/az;->k:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 310
    :goto_0
    return-void

    .line 299
    :cond_1
    iget-object v0, p0, Lcom/peel/ui/b/az;->a:Lcom/peel/ui/b/av;

    invoke-static {v0}, Lcom/peel/ui/b/av;->f(Lcom/peel/ui/b/av;)Lcom/peel/ui/b/a/a;

    move-result-object v1

    iget-object v0, p0, Lcom/peel/ui/b/az;->j:Ljava/lang/Object;

    check-cast v0, Ljava/util/List;

    invoke-virtual {v1, v0}, Lcom/peel/ui/b/a/a;->a(Ljava/util/List;)V

    .line 301
    iget-object v1, p0, Lcom/peel/ui/b/az;->a:Lcom/peel/ui/b/av;

    iget-object v0, p0, Lcom/peel/ui/b/az;->j:Ljava/lang/Object;

    check-cast v0, Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/content/listing/LiveListing;

    invoke-static {v1, v0}, Lcom/peel/ui/b/av;->a(Lcom/peel/ui/b/av;Lcom/peel/content/listing/LiveListing;)Lcom/peel/content/listing/LiveListing;

    .line 302
    iget-object v0, p0, Lcom/peel/ui/b/az;->a:Lcom/peel/ui/b/av;

    invoke-static {v0}, Lcom/peel/ui/b/av;->p(Lcom/peel/ui/b/av;)V

    .line 309
    iget-object v1, p0, Lcom/peel/ui/b/az;->a:Lcom/peel/ui/b/av;

    iget-object v0, p0, Lcom/peel/ui/b/az;->j:Ljava/lang/Object;

    check-cast v0, Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/content/listing/LiveListing;

    invoke-virtual {v0}, Lcom/peel/content/listing/LiveListing;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/peel/ui/b/av;->a(Lcom/peel/ui/b/av;Ljava/lang/String;)V

    goto :goto_0
.end method
