.class Lcom/peel/ui/b/a/o;
.super Landroid/os/AsyncTask;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Lcom/peel/social/HashTag;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic b:Lcom/peel/social/c;

.field final synthetic c:Lcom/peel/ui/b/a/a;


# direct methods
.method constructor <init>(Lcom/peel/ui/b/a/a;Ljava/lang/String;Lcom/peel/social/c;)V
    .locals 0

    .prologue
    .line 271
    iput-object p1, p0, Lcom/peel/ui/b/a/o;->c:Lcom/peel/ui/b/a/a;

    iput-object p2, p0, Lcom/peel/ui/b/a/o;->a:Ljava/lang/String;

    iput-object p3, p0, Lcom/peel/ui/b/a/o;->b:Lcom/peel/social/c;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected varargs a([Ljava/lang/Void;)Lcom/peel/social/HashTag;
    .locals 4

    .prologue
    .line 274
    const/4 v0, 0x0

    .line 277
    :try_start_0
    iget-object v1, p0, Lcom/peel/ui/b/a/o;->a:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 278
    iget-object v1, p0, Lcom/peel/ui/b/a/o;->b:Lcom/peel/social/c;

    iget-object v2, p0, Lcom/peel/ui/b/a/o;->a:Ljava/lang/String;

    const/16 v3, 0x78

    invoke-interface {v1, v2, v3}, Lcom/peel/social/c;->a(Ljava/lang/String;I)Lcom/peel/social/HashTag;
    :try_end_0
    .catch Lcom/peel/social/b; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 283
    :cond_0
    :goto_0
    return-object v0

    .line 280
    :catch_0
    move-exception v1

    .line 281
    invoke-static {}, Lcom/peel/ui/b/a/a;->h()Ljava/lang/String;

    move-result-object v2

    invoke-static {}, Lcom/peel/ui/b/a/a;->h()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method protected a(Lcom/peel/social/HashTag;)V
    .locals 5

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 288
    if-eqz p1, :cond_4

    iget-object v0, p1, Lcom/peel/social/HashTag;->hashtags:[Ljava/lang/String;

    if-eqz v0, :cond_4

    iget-object v0, p1, Lcom/peel/social/HashTag;->hashtags:[Ljava/lang/String;

    array-length v0, v0

    if-lez v0, :cond_4

    .line 289
    iget-object v0, p0, Lcom/peel/ui/b/a/o;->c:Lcom/peel/ui/b/a/a;

    iget-object v3, p1, Lcom/peel/social/HashTag;->hashtags:[Ljava/lang/String;

    invoke-static {v0, v3}, Lcom/peel/ui/b/a/a;->a(Lcom/peel/ui/b/a/a;[Ljava/lang/String;)[Ljava/lang/String;

    .line 290
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    move v0, v1

    .line 292
    :goto_0
    iget-object v4, p1, Lcom/peel/social/HashTag;->hashtags:[Ljava/lang/String;

    array-length v4, v4

    if-ge v0, v4, :cond_1

    const/16 v4, 0x14

    if-ge v0, v4, :cond_1

    .line 293
    iget-object v4, p1, Lcom/peel/social/HashTag;->hashtags:[Ljava/lang/String;

    aget-object v4, v4, v0

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 295
    iget-object v4, p1, Lcom/peel/social/HashTag;->hashtags:[Ljava/lang/String;

    array-length v4, v4

    add-int/lit8 v4, v4, -0x1

    if-ge v0, v4, :cond_0

    const/16 v4, 0x13

    if-ge v0, v4, :cond_0

    .line 296
    const-string/jumbo v4, " OR "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 292
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 301
    :cond_1
    iget-object v0, p0, Lcom/peel/ui/b/a/o;->c:Lcom/peel/ui/b/a/a;

    invoke-static {v0}, Lcom/peel/ui/b/a/a;->a(Lcom/peel/ui/b/a/a;)Ljava/util/ArrayList;

    move-result-object v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/peel/ui/b/a/o;->c:Lcom/peel/ui/b/a/a;

    invoke-static {v0}, Lcom/peel/ui/b/a/a;->a(Lcom/peel/ui/b/a/a;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_5

    .line 302
    iget-object v0, p0, Lcom/peel/ui/b/a/o;->c:Lcom/peel/ui/b/a/a;

    invoke-static {v0}, Lcom/peel/ui/b/a/a;->a(Lcom/peel/ui/b/a/a;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/ui/model/TwittData;

    invoke-virtual {v0}, Lcom/peel/ui/model/TwittData;->c()Ljava/lang/String;

    move-result-object v0

    .line 305
    :goto_1
    iget-object v1, p0, Lcom/peel/ui/b/a/o;->c:Lcom/peel/ui/b/a/a;

    iget-object v1, v1, Lcom/peel/ui/b/a/a;->c:Landroid/support/v4/app/ae;

    invoke-static {v1}, Lcom/peel/d/v;->a(Landroid/content/Context;)Lcom/peel/d/v;

    move-result-object v2

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    if-nez v0, :cond_3

    const/16 v1, 0x28

    :goto_2
    new-instance v4, Lcom/peel/ui/b/a/p;

    invoke-direct {v4, p0}, Lcom/peel/ui/b/a/p;-><init>(Lcom/peel/ui/b/a/o;)V

    invoke-virtual {v2, v3, v0, v1, v4}, Lcom/peel/d/v;->a(Ljava/lang/String;Ljava/lang/String;ILcom/peel/util/t;)V

    .line 345
    :cond_2
    :goto_3
    return-void

    .line 305
    :cond_3
    const/16 v1, 0xa

    goto :goto_2

    .line 332
    :cond_4
    const-class v0, Lcom/peel/ui/b/a/a;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v3, "final"

    new-instance v4, Lcom/peel/ui/b/a/r;

    invoke-direct {v4, p0}, Lcom/peel/ui/b/a/r;-><init>(Lcom/peel/ui/b/a/o;)V

    invoke-static {v0, v3, v4}, Lcom/peel/util/i;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/lang/Runnable;

    .line 341
    iget-object v0, p0, Lcom/peel/ui/b/a/o;->c:Lcom/peel/ui/b/a/a;

    invoke-static {v0}, Lcom/peel/ui/b/a/a;->c(Lcom/peel/ui/b/a/a;)Lcom/peel/util/t;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 342
    iget-object v0, p0, Lcom/peel/ui/b/a/o;->c:Lcom/peel/ui/b/a/a;

    invoke-static {v0}, Lcom/peel/ui/b/a/a;->c(Lcom/peel/ui/b/a/a;)Lcom/peel/util/t;

    move-result-object v0

    iget-object v3, p0, Lcom/peel/ui/b/a/o;->c:Lcom/peel/ui/b/a/a;

    invoke-static {v3}, Lcom/peel/ui/b/a/a;->a(Lcom/peel/ui/b/a/a;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v0, v1, v3, v2}, Lcom/peel/util/t;->a(ZLjava/lang/Object;Ljava/lang/String;)V

    goto :goto_3

    :cond_5
    move-object v0, v2

    goto :goto_1
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 271
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/peel/ui/b/a/o;->a([Ljava/lang/Void;)Lcom/peel/social/HashTag;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 271
    check-cast p1, Lcom/peel/social/HashTag;

    invoke-virtual {p0, p1}, Lcom/peel/ui/b/a/o;->a(Lcom/peel/social/HashTag;)V

    return-void
.end method
