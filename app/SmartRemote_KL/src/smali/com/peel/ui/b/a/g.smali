.class Lcom/peel/ui/b/a/g;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/peel/ui/b/a/f;


# direct methods
.method constructor <init>(Lcom/peel/ui/b/a/f;)V
    .locals 0

    .prologue
    .line 827
    iput-object p1, p0, Lcom/peel/ui/b/a/g;->a:Lcom/peel/ui/b/a/f;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 13

    .prologue
    const/4 v12, 0x0

    .line 830
    iget-object v0, p0, Lcom/peel/ui/b/a/g;->a:Lcom/peel/ui/b/a/f;

    iget-object v0, v0, Lcom/peel/ui/b/a/f;->f:Lcom/peel/ui/b/a/a;

    iget-object v0, v0, Lcom/peel/ui/b/a/a;->k:Lcom/peel/content/library/LiveLibrary;

    invoke-virtual {v0}, Lcom/peel/content/library/LiveLibrary;->e()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/peel/content/a;->f:Lcom/peel/content/user/User;

    invoke-virtual {v1}, Lcom/peel/content/user/User;->x()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/peel/ui/b/a/g;->a:Lcom/peel/ui/b/a/f;

    iget-object v2, v2, Lcom/peel/ui/b/a/f;->a:Lcom/peel/content/listing/LiveListing;

    .line 831
    invoke-virtual {v2}, Lcom/peel/content/listing/LiveListing;->d()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    iget-object v3, p0, Lcom/peel/ui/b/a/g;->a:Lcom/peel/ui/b/a/f;

    iget-object v3, v3, Lcom/peel/ui/b/a/f;->b:Lcom/peel/content/listing/Listing;

    .line 832
    invoke-static {v3}, Lcom/peel/util/bx;->b(Lcom/peel/content/listing/Listing;)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/peel/ui/b/a/g;->a:Lcom/peel/ui/b/a/f;

    iget-object v4, v4, Lcom/peel/ui/b/a/f;->c:Ljava/lang/String;

    iget-object v5, p0, Lcom/peel/ui/b/a/g;->a:Lcom/peel/ui/b/a/f;

    iget-object v5, v5, Lcom/peel/ui/b/a/f;->d:Ljava/lang/String;

    iget-object v6, p0, Lcom/peel/ui/b/a/g;->a:Lcom/peel/ui/b/a/f;

    iget-object v6, v6, Lcom/peel/ui/b/a/f;->e:Ljava/lang/String;

    const-string/jumbo v7, "Multiple"

    iget-object v8, p0, Lcom/peel/ui/b/a/g;->a:Lcom/peel/ui/b/a/f;

    iget-object v8, v8, Lcom/peel/ui/b/a/f;->b:Lcom/peel/content/listing/Listing;

    .line 834
    invoke-virtual {v8}, Lcom/peel/content/listing/Listing;->k()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/peel/util/al;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    new-instance v9, Lcom/peel/ui/b/a/h;

    invoke-direct {v9, p0}, Lcom/peel/ui/b/a/h;-><init>(Lcom/peel/ui/b/a/g;)V

    .line 830
    invoke-static/range {v0 .. v9}, Lcom/peel/util/al;->a(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/peel/util/t;)V

    .line 841
    invoke-static {}, Lcom/peel/util/a/f;->a()Lcom/peel/util/a/f;

    move-result-object v0

    sget-object v1, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v1}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v1

    if-nez v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    const/16 v2, 0x43c

    iget-object v3, p0, Lcom/peel/ui/b/a/g;->a:Lcom/peel/ui/b/a/f;

    iget-object v3, v3, Lcom/peel/ui/b/a/f;->f:Lcom/peel/ui/b/a/a;

    iget v3, v3, Lcom/peel/ui/b/a/a;->h:I

    iget-object v4, p0, Lcom/peel/ui/b/a/g;->a:Lcom/peel/ui/b/a/f;

    iget-object v4, v4, Lcom/peel/ui/b/a/f;->a:Lcom/peel/content/listing/LiveListing;

    .line 842
    invoke-virtual {v4}, Lcom/peel/content/listing/LiveListing;->d()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/peel/ui/b/a/g;->a:Lcom/peel/ui/b/a/f;

    iget-object v5, v5, Lcom/peel/ui/b/a/f;->b:Lcom/peel/content/listing/Listing;

    invoke-static {v5}, Lcom/peel/util/bx;->b(Lcom/peel/content/listing/Listing;)Ljava/lang/String;

    move-result-object v6

    iget-object v5, p0, Lcom/peel/ui/b/a/g;->a:Lcom/peel/ui/b/a/f;

    iget-object v5, v5, Lcom/peel/ui/b/a/f;->a:Lcom/peel/content/listing/LiveListing;

    .line 843
    invoke-virtual {v5}, Lcom/peel/content/listing/LiveListing;->c()Ljava/lang/String;

    move-result-object v8

    const-string/jumbo v10, "future series"

    move v5, v12

    move v7, v12

    move v9, v12

    move v11, v12

    .line 841
    invoke-virtual/range {v0 .. v11}, Lcom/peel/util/a/f;->a(IIILjava/lang/String;ILjava/lang/String;ILjava/lang/String;ILjava/lang/String;I)V

    .line 844
    return-void

    .line 841
    :cond_0
    sget-object v1, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v1}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/data/at;->f()I

    move-result v1

    goto :goto_0
.end method
