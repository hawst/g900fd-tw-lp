.class public Lcom/peel/ui/b/da;
.super Lcom/peel/ui/b/af;


# instance fields
.field private final c:Landroid/support/v4/app/ae;

.field private d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/peel/content/listing/LiveListing;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/support/v4/app/ae;Ljava/util/List;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/app/ae;",
            "Ljava/util/List",
            "<",
            "Lcom/peel/content/listing/LiveListing;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .line 38
    invoke-direct {p0}, Lcom/peel/ui/b/af;-><init>()V

    .line 39
    iput-object p1, p0, Lcom/peel/ui/b/da;->c:Landroid/support/v4/app/ae;

    .line 40
    iput-object p2, p0, Lcom/peel/ui/b/da;->d:Ljava/util/List;

    .line 41
    iput p3, p0, Lcom/peel/ui/b/da;->b:I

    .line 42
    return-void
.end method

.method static synthetic a(Lcom/peel/ui/b/da;)Ljava/util/List;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/peel/ui/b/da;->d:Ljava/util/List;

    return-object v0
.end method

.method static synthetic b(Lcom/peel/ui/b/da;)Landroid/support/v4/app/ae;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/peel/ui/b/da;->c:Landroid/support/v4/app/ae;

    return-object v0
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 46
    const/4 v0, 0x4

    return v0
.end method

.method public a(Landroid/view/LayoutInflater;Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 16

    .prologue
    .line 51
    if-nez p2, :cond_6

    new-instance v2, Lcom/peel/ui/b/dd;

    const/4 v3, 0x0

    invoke-direct {v2, v3}, Lcom/peel/ui/b/dd;-><init>(Lcom/peel/ui/b/db;)V

    move-object v8, v2

    .line 53
    :goto_0
    if-nez p2, :cond_d

    .line 54
    sget v2, Lcom/peel/ui/fq;->show_details_overview_upcoming:I

    const/4 v3, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, p3

    invoke-virtual {v0, v2, v1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v4

    .line 56
    sget v2, Lcom/peel/ui/fp;->episodes_list:I

    invoke-virtual {v4, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    iput-object v2, v8, Lcom/peel/ui/b/dd;->a:Landroid/widget/LinearLayout;

    .line 57
    sget v2, Lcom/peel/ui/fp;->more_bottom:I

    invoke-virtual {v4, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iput-object v2, v8, Lcom/peel/ui/b/dd;->b:Landroid/view/View;

    .line 58
    invoke-virtual {v4, v8}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 61
    :goto_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/peel/ui/b/da;->d:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    const/4 v3, 0x3

    if-le v2, v3, :cond_0

    .line 62
    iget-object v2, v8, Lcom/peel/ui/b/dd;->b:Landroid/view/View;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    .line 63
    iget-object v2, v8, Lcom/peel/ui/b/dd;->b:Landroid/view/View;

    new-instance v3, Lcom/peel/ui/b/db;

    move-object/from16 v0, p0

    invoke-direct {v3, v0}, Lcom/peel/ui/b/db;-><init>(Lcom/peel/ui/b/da;)V

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 75
    :cond_0
    iget-object v2, v8, Lcom/peel/ui/b/dd;->a:Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 77
    const/4 v2, 0x0

    move v6, v2

    :goto_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/peel/ui/b/da;->d:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v6, v2, :cond_c

    const/4 v2, 0x3

    if-ge v6, v2, :cond_c

    .line 78
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/peel/ui/b/da;->d:Ljava/util/List;

    invoke-interface {v2, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/peel/content/listing/LiveListing;

    .line 80
    sget v5, Lcom/peel/ui/fq;->show_details_upcoming_listitem:I

    move-object v3, v4

    check-cast v3, Landroid/view/ViewGroup;

    const/4 v7, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v3, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v9

    .line 81
    sget v3, Lcom/peel/ui/fp;->reminder_icon:I

    invoke-virtual {v9, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    .line 83
    invoke-static {v2}, Lcom/peel/util/bx;->e(Lcom/peel/content/listing/Listing;)Z

    move-result v5

    if-eqz v5, :cond_7

    .line 84
    const/4 v5, 0x0

    invoke-virtual {v3, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 85
    sget v5, Lcom/peel/ui/fo;->reminder_icon_selected:I

    invoke-virtual {v3, v5}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 96
    :goto_3
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v5

    .line 97
    invoke-virtual {v2}, Lcom/peel/content/listing/LiveListing;->o()J

    move-result-wide v10

    invoke-virtual {v5, v10, v11}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 99
    sget v3, Lcom/peel/ui/fp;->day:I

    invoke-virtual {v9, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 100
    const/4 v7, 0x5

    invoke-virtual {v5, v7}, Ljava/util/Calendar;->get(I)I

    move-result v7

    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 101
    new-instance v7, Ljava/text/DateFormatSymbols;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v3

    invoke-direct {v7, v3}, Ljava/text/DateFormatSymbols;-><init>(Ljava/util/Locale;)V

    .line 103
    sget v3, Lcom/peel/ui/fp;->month:I

    invoke-virtual {v9, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 104
    invoke-virtual {v7}, Ljava/text/DateFormatSymbols;->getShortMonths()[Ljava/lang/String;

    move-result-object v7

    const/4 v10, 0x2

    invoke-virtual {v5, v10}, Ljava/util/Calendar;->get(I)I

    move-result v5

    aget-object v5, v7, v5

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 106
    sget v3, Lcom/peel/ui/fp;->title:I

    invoke-virtual {v9, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 107
    sget v5, Lcom/peel/ui/fp;->season_title:I

    invoke-virtual {v9, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    .line 108
    invoke-virtual {v2}, Lcom/peel/content/listing/LiveListing;->v()Ljava/lang/String;

    move-result-object v7

    .line 110
    if-eqz v5, :cond_4

    .line 111
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    .line 112
    invoke-virtual {v2}, Lcom/peel/content/listing/LiveListing;->w()Ljava/lang/String;

    move-result-object v11

    .line 113
    if-eqz v11, :cond_1

    const-string/jumbo v12, "0"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v12

    if-nez v12, :cond_1

    .line 114
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/peel/ui/b/da;->c:Landroid/support/v4/app/ae;

    sget v13, Lcom/peel/ui/ft;->episode_number:I

    const/4 v14, 0x1

    new-array v14, v14, [Ljava/lang/Object;

    const/4 v15, 0x0

    aput-object v11, v14, v15

    invoke-virtual {v12, v13, v14}, Landroid/support/v4/app/ae;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 117
    :cond_1
    invoke-virtual {v2}, Lcom/peel/content/listing/LiveListing;->A()Ljava/lang/String;

    move-result-object v11

    .line 118
    if-eqz v11, :cond_3

    const-string/jumbo v12, "0"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v12

    if-nez v12, :cond_3

    .line 119
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->length()I

    move-result v12

    if-lez v12, :cond_2

    const-string/jumbo v12, ", "

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 120
    :cond_2
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/peel/ui/b/da;->c:Landroid/support/v4/app/ae;

    sget v13, Lcom/peel/ui/ft;->season_number:I

    const/4 v14, 0x1

    new-array v14, v14, [Ljava/lang/Object;

    const/4 v15, 0x0

    aput-object v11, v14, v15

    invoke-virtual {v12, v13, v14}, Landroid/support/v4/app/ae;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 123
    :cond_3
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->length()I

    move-result v11

    if-lez v11, :cond_9

    .line 124
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v5, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 130
    :cond_4
    :goto_4
    if-eqz v7, :cond_a

    move-object v5, v7

    :goto_5
    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 132
    sget v3, Lcom/peel/ui/fp;->channel:I

    invoke-virtual {v9, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 134
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    .line 136
    invoke-virtual {v2}, Lcom/peel/content/listing/LiveListing;->c()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v10, " "

    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v2}, Lcom/peel/content/listing/LiveListing;->d()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 137
    const-string/jumbo v5, " - "

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 138
    sget-object v5, Lcom/peel/util/x;->c:Ljava/lang/ThreadLocal;

    invoke-virtual {v5}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/text/SimpleDateFormat;

    invoke-virtual {v2}, Lcom/peel/content/listing/LiveListing;->o()J

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    invoke-virtual {v5, v10}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2}, Lcom/peel/content/listing/LiveListing;->j()J

    move-result-wide v10

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/peel/ui/b/da;->c:Landroid/support/v4/app/ae;

    invoke-static {v12}, Landroid/text/format/DateFormat;->is24HourFormat(Landroid/content/Context;)Z

    move-result v12

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/peel/ui/b/da;->c:Landroid/support/v4/app/ae;

    sget v14, Lcom/peel/ui/ft;->time_pattern:I

    invoke-virtual {v13, v14}, Landroid/support/v4/app/ae;->getString(I)Ljava/lang/String;

    move-result-object v13

    invoke-static {v5, v10, v11, v12, v13}, Lcom/peel/util/x;->a(Ljava/lang/String;JZLjava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 140
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->length()I

    move-result v5

    if-lez v5, :cond_b

    .line 141
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 146
    :goto_6
    invoke-virtual {v9, v2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 147
    iget-object v3, v8, Lcom/peel/ui/b/dd;->a:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v9}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 149
    invoke-virtual {v2}, Lcom/peel/content/listing/LiveListing;->n()Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v5, "live"

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 150
    new-instance v3, Lcom/peel/ui/b/dc;

    move-object/from16 v0, p0

    invoke-direct {v3, v0, v2}, Lcom/peel/ui/b/dc;-><init>(Lcom/peel/ui/b/da;Lcom/peel/content/listing/LiveListing;)V

    invoke-virtual {v9, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 77
    :cond_5
    add-int/lit8 v2, v6, 0x1

    move v6, v2

    goto/16 :goto_2

    .line 51
    :cond_6
    invoke-virtual/range {p2 .. p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/peel/ui/b/dd;

    move-object v8, v2

    goto/16 :goto_0

    .line 87
    :cond_7
    invoke-virtual {v2}, Lcom/peel/content/listing/LiveListing;->o()J

    move-result-wide v10

    .line 88
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v12

    const-wide/32 v14, 0x1b7740

    add-long/2addr v12, v14

    cmp-long v5, v10, v12

    if-gtz v5, :cond_8

    .line 89
    const/4 v5, 0x4

    invoke-virtual {v3, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_3

    .line 91
    :cond_8
    const/4 v5, 0x0

    invoke-virtual {v3, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 92
    sget v5, Lcom/peel/ui/fo;->reminder_icon:I

    invoke-virtual {v3, v5}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_3

    .line 126
    :cond_9
    const/16 v10, 0x8

    invoke-virtual {v5, v10}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_4

    .line 130
    :cond_a
    invoke-virtual {v2}, Lcom/peel/content/listing/LiveListing;->l()Ljava/lang/String;

    move-result-object v5

    goto/16 :goto_5

    .line 143
    :cond_b
    const/16 v5, 0x8

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_6

    .line 161
    :cond_c
    return-object v4

    :cond_d
    move-object/from16 v4, p2

    goto/16 :goto_1
.end method
