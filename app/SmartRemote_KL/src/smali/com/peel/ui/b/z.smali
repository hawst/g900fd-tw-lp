.class Lcom/peel/ui/b/z;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/peel/ui/b/r;


# direct methods
.method constructor <init>(Lcom/peel/ui/b/r;)V
    .locals 0

    .prologue
    .line 264
    iput-object p1, p0, Lcom/peel/ui/b/z;->a:Lcom/peel/ui/b/r;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 267
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 268
    const-string/jumbo v0, "keyword"

    iget-object v2, p0, Lcom/peel/ui/b/z;->a:Lcom/peel/ui/b/r;

    invoke-static {v2}, Lcom/peel/ui/b/r;->d(Lcom/peel/ui/b/r;)[Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putStringArray(Ljava/lang/String;[Ljava/lang/String;)V

    .line 269
    const-string/jumbo v0, "context_id"

    iget-object v2, p0, Lcom/peel/ui/b/z;->a:Lcom/peel/ui/b/r;

    invoke-static {v2}, Lcom/peel/ui/b/r;->c(Lcom/peel/ui/b/r;)I

    move-result v2

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 270
    const-string/jumbo v2, "twittList"

    iget-object v0, p0, Lcom/peel/ui/b/z;->a:Lcom/peel/ui/b/r;

    invoke-static {v0}, Lcom/peel/ui/b/r;->f(Lcom/peel/ui/b/r;)Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 271
    const-string/jumbo v0, "showid"

    iget-object v2, p0, Lcom/peel/ui/b/z;->a:Lcom/peel/ui/b/r;

    invoke-static {v2}, Lcom/peel/ui/b/r;->g(Lcom/peel/ui/b/r;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 272
    const-string/jumbo v0, "tmsid"

    iget-object v2, p0, Lcom/peel/ui/b/z;->a:Lcom/peel/ui/b/r;

    invoke-static {v2}, Lcom/peel/ui/b/r;->h(Lcom/peel/ui/b/r;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 273
    iget-object v0, p0, Lcom/peel/ui/b/z;->a:Lcom/peel/ui/b/r;

    .line 274
    invoke-static {v0}, Lcom/peel/ui/b/r;->e(Lcom/peel/ui/b/r;)Landroid/support/v4/app/ae;

    move-result-object v0

    const-class v2, Lcom/peel/ui/b/cp;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    .line 273
    invoke-static {v0, v2, v1}, Lcom/peel/d/e;->c(Landroid/support/v4/app/ae;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 275
    return-void
.end method
