.class public Lcom/peel/ui/b/bz;
.super Lcom/peel/ui/b/af;


# instance fields
.field private c:Landroid/os/Bundle;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:I


# direct methods
.method public constructor <init>(Landroid/os/Bundle;Ljava/lang/String;I)V
    .locals 1

    .prologue
    .line 24
    invoke-direct {p0}, Lcom/peel/ui/b/af;-><init>()V

    .line 25
    iput-object p1, p0, Lcom/peel/ui/b/bz;->c:Landroid/os/Bundle;

    .line 26
    iput-object p2, p0, Lcom/peel/ui/b/bz;->e:Ljava/lang/String;

    .line 27
    const-string/jumbo v0, "showId"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/ui/b/bz;->d:Ljava/lang/String;

    .line 28
    iput p3, p0, Lcom/peel/ui/b/bz;->f:I

    .line 29
    return-void
.end method

.method static synthetic a(Lcom/peel/ui/b/bz;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 18
    iget-object v0, p0, Lcom/peel/ui/b/bz;->d:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic b(Lcom/peel/ui/b/bz;)Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 18
    iget-object v0, p0, Lcom/peel/ui/b/bz;->c:Landroid/os/Bundle;

    return-object v0
.end method

.method static synthetic c(Lcom/peel/ui/b/bz;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 18
    iget-object v0, p0, Lcom/peel/ui/b/bz;->e:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic d(Lcom/peel/ui/b/bz;)I
    .locals 1

    .prologue
    .line 18
    iget v0, p0, Lcom/peel/ui/b/bz;->f:I

    return v0
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 33
    const/4 v0, 0x5

    return v0
.end method

.method public a(Landroid/view/LayoutInflater;Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 11

    .prologue
    const/4 v10, 0x3

    const/4 v4, 0x0

    .line 38
    if-nez p2, :cond_4

    new-instance v0, Lcom/peel/ui/b/cc;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/peel/ui/b/cc;-><init>(Lcom/peel/ui/b/bz;Lcom/peel/ui/b/ca;)V

    move-object v6, v0

    .line 40
    :goto_0
    if-nez p2, :cond_0

    .line 41
    sget v0, Lcom/peel/ui/fq;->show_details_overview_upcoming:I

    invoke-virtual {p1, v0, p3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 42
    sget v0, Lcom/peel/ui/fp;->episodes_list:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, v6, Lcom/peel/ui/b/cc;->b:Landroid/widget/LinearLayout;

    .line 43
    sget v0, Lcom/peel/ui/fp;->more_bottom:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, v6, Lcom/peel/ui/b/cc;->c:Landroid/view/View;

    .line 44
    sget v0, Lcom/peel/ui/fp;->header:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v6, Lcom/peel/ui/b/cc;->a:Landroid/widget/TextView;

    .line 46
    invoke-virtual {p2, v6}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 49
    :cond_0
    iget-object v0, v6, Lcom/peel/ui/b/cc;->a:Landroid/widget/TextView;

    sget v1, Lcom/peel/ui/ft;->ondemand:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 50
    iget-object v0, v6, Lcom/peel/ui/b/cc;->b:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 52
    iget-object v0, p0, Lcom/peel/ui/b/bz;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "season"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/peel/content/model/Season;

    .line 53
    invoke-virtual {v0}, Lcom/peel/content/model/Season;->b()Ljava/util/List;

    move-result-object v7

    .line 54
    if-eqz v7, :cond_6

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_6

    .line 55
    invoke-static {v7}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 57
    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v0

    if-gt v0, v10, :cond_1

    iget-object v0, p0, Lcom/peel/ui/b/bz;->c:Landroid/os/Bundle;

    invoke-virtual {v0}, Landroid/os/Bundle;->size()I

    move-result v0

    const/4 v1, 0x1

    if-le v0, v1, :cond_2

    .line 58
    :cond_1
    iget-object v0, v6, Lcom/peel/ui/b/cc;->c:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 59
    iget-object v0, v6, Lcom/peel/ui/b/cc;->c:Landroid/view/View;

    new-instance v1, Lcom/peel/ui/b/ca;

    invoke-direct {v1, p0}, Lcom/peel/ui/b/ca;-><init>(Lcom/peel/ui/b/bz;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_2
    move v3, v4

    .line 71
    :goto_1
    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v0

    if-ge v3, v0, :cond_6

    if-ge v3, v10, :cond_6

    .line 72
    sget v0, Lcom/peel/ui/fq;->show_details_ondemand_listitem:I

    iget-object v1, v6, Lcom/peel/ui/b/cc;->b:Landroid/widget/LinearLayout;

    invoke-virtual {p1, v0, v1, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v8

    .line 73
    invoke-interface {v7, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/content/model/Episode;

    .line 74
    sget v1, Lcom/peel/ui/fp;->episode_number:I

    invoke-virtual {v8, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 75
    sget v2, Lcom/peel/ui/fp;->episode_title:I

    invoke-virtual {v8, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 76
    invoke-virtual {v0}, Lcom/peel/content/model/Episode;->b()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_3

    invoke-virtual {v0}, Lcom/peel/content/model/Episode;->b()Ljava/lang/String;

    move-result-object v5

    const-string/jumbo v9, "Unknown"

    .line 77
    invoke-virtual {v5, v9}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_5

    :cond_3
    const-string/jumbo v5, ""

    .line 76
    :goto_2
    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 78
    invoke-virtual {v0}, Lcom/peel/content/model/Episode;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 81
    iget-object v0, v6, Lcom/peel/ui/b/cc;->b:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v8}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 84
    new-instance v0, Lcom/peel/ui/b/cb;

    invoke-direct {v0, p0, v3}, Lcom/peel/ui/b/cb;-><init>(Lcom/peel/ui/b/bz;I)V

    invoke-virtual {v8, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 71
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    .line 38
    :cond_4
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/ui/b/cc;

    move-object v6, v0

    goto/16 :goto_0

    .line 77
    :cond_5
    invoke-virtual {v0}, Lcom/peel/content/model/Episode;->b()Ljava/lang/String;

    move-result-object v5

    goto :goto_2

    .line 99
    :cond_6
    return-object p2
.end method
