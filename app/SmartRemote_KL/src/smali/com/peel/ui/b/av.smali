.class public Lcom/peel/ui/b/av;
.super Lcom/peel/d/u;


# static fields
.field private static aC:Lcom/peel/d/a;

.field private static final f:Ljava/lang/String;


# instance fields
.field private aA:Landroid/content/res/Resources;

.field private aB:Z

.field private aD:Lcom/peel/widget/AdVideoView;

.field private aE:Landroid/content/BroadcastReceiver;

.field private aj:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/peel/content/listing/Listing;",
            ">;"
        }
    .end annotation
.end field

.field private ak:Lcom/peel/content/listing/LiveListing;

.field private al:Lcom/peel/ui/ScrollPositionListView;

.field private am:Landroid/widget/ImageView;

.field private an:Landroid/view/View;

.field private ao:Landroid/widget/TextView;

.field private ap:Landroid/widget/TextView;

.field private aq:Landroid/widget/TextView;

.field private ar:Lcom/peel/ui/b/q;

.field private as:I

.field private at:I

.field private au:J

.field private av:Lcom/peel/ui/b/a/a;

.field private aw:Ljava/lang/String;

.field private ax:Ljava/lang/String;

.field private ay:Ljava/lang/String;

.field private az:J

.field e:Lcom/peel/util/t;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/peel/util/t",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/peel/ui/model/TwittData;",
            ">;>;"
        }
    .end annotation
.end field

.field private g:Landroid/view/View;

.field private h:I

.field private i:Lcom/peel/content/library/Library;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 55
    const-class v0, Lcom/peel/ui/b/av;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/peel/ui/b/av;->f:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 54
    invoke-direct {p0}, Lcom/peel/d/u;-><init>()V

    .line 58
    iput v0, p0, Lcom/peel/ui/b/av;->h:I

    .line 73
    iput-boolean v0, p0, Lcom/peel/ui/b/av;->aB:Z

    .line 425
    new-instance v0, Lcom/peel/ui/b/bd;

    invoke-direct {v0, p0}, Lcom/peel/ui/b/bd;-><init>(Lcom/peel/ui/b/av;)V

    iput-object v0, p0, Lcom/peel/ui/b/av;->aE:Landroid/content/BroadcastReceiver;

    .line 524
    new-instance v0, Lcom/peel/ui/b/bf;

    invoke-direct {v0, p0}, Lcom/peel/ui/b/bf;-><init>(Lcom/peel/ui/b/av;)V

    iput-object v0, p0, Lcom/peel/ui/b/av;->e:Lcom/peel/util/t;

    return-void
.end method

.method private S()V
    .locals 3

    .prologue
    .line 159
    iget-object v0, p0, Lcom/peel/ui/b/av;->ak:Lcom/peel/content/listing/LiveListing;

    if-eqz v0, :cond_0

    .line 160
    sget-object v0, Lcom/peel/ui/b/av;->f:Ljava/lang/String;

    const-string/jumbo v1, "refresh adapter"

    new-instance v2, Lcom/peel/ui/b/aw;

    invoke-direct {v2, p0}, Lcom/peel/ui/b/aw;-><init>(Lcom/peel/ui/b/av;)V

    invoke-static {v0, v1, v2}, Lcom/peel/util/i;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/lang/Runnable;

    .line 254
    :cond_0
    return-void
.end method

.method private T()V
    .locals 4

    .prologue
    .line 290
    iget-object v0, p0, Lcom/peel/ui/b/av;->aw:Ljava/lang/String;

    sget-object v1, Lcom/peel/content/a;->e:Ljava/lang/String;

    new-instance v2, Lcom/peel/ui/b/az;

    const/4 v3, 0x1

    invoke-direct {v2, p0, v3}, Lcom/peel/ui/b/az;-><init>(Lcom/peel/ui/b/av;I)V

    invoke-static {v0, v1, v2}, Lcom/peel/content/a/j;->e(Ljava/lang/String;Ljava/lang/String;Lcom/peel/util/t;)V

    .line 312
    return-void
.end method

.method private U()V
    .locals 10

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 439
    iget-object v0, p0, Lcom/peel/ui/b/av;->av:Lcom/peel/ui/b/a/a;

    invoke-virtual {v0}, Lcom/peel/ui/b/a/a;->b()Lcom/peel/content/listing/LiveListing;

    move-result-object v3

    .line 442
    invoke-virtual {v3}, Lcom/peel/content/listing/LiveListing;->o()J

    move-result-wide v4

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    const-wide/32 v8, 0x1b7740

    add-long/2addr v6, v8

    cmp-long v0, v4, v6

    if-lez v0, :cond_2

    move v0, v1

    .line 446
    :goto_0
    invoke-static {v3}, Lcom/peel/util/bx;->e(Lcom/peel/content/listing/Listing;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 447
    iget-object v0, p0, Lcom/peel/ui/b/av;->ap:Landroid/widget/TextView;

    sget v1, Lcom/peel/ui/fo;->undo_reminder_stateful:I

    invoke-virtual {v0, v2, v1, v2, v2}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    .line 448
    iget-object v0, p0, Lcom/peel/ui/b/av;->ap:Landroid/widget/TextView;

    sget v1, Lcom/peel/ui/ft;->undo_reminder:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 462
    :goto_1
    return-void

    .line 450
    :cond_0
    if-eqz v0, :cond_1

    .line 452
    iget-object v0, p0, Lcom/peel/ui/b/av;->ap:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 453
    iget-object v0, p0, Lcom/peel/ui/b/av;->ap:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getCompoundDrawables()[Landroid/graphics/drawable/Drawable;

    move-result-object v0

    aget-object v0, v0, v1

    const/16 v1, 0xff

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 454
    iget-object v0, p0, Lcom/peel/ui/b/av;->ap:Landroid/widget/TextView;

    sget v1, Lcom/peel/ui/ft;->title_set_reminder:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 455
    iget-object v0, p0, Lcom/peel/ui/b/av;->ap:Landroid/widget/TextView;

    sget v1, Lcom/peel/ui/fo;->remind_me_btn_stateful:I

    invoke-virtual {v0, v2, v1, v2, v2}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    goto :goto_1

    .line 458
    :cond_1
    iget-object v0, p0, Lcom/peel/ui/b/av;->ap:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 459
    iget-object v0, p0, Lcom/peel/ui/b/av;->ap:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getCompoundDrawables()[Landroid/graphics/drawable/Drawable;

    move-result-object v0

    aget-object v0, v0, v1

    const/16 v1, 0x40

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    goto :goto_1

    :cond_2
    move v0, v2

    goto :goto_0
.end method

.method private V()V
    .locals 11

    .prologue
    const/16 v10, 0xff

    const/16 v9, 0x40

    const/4 v8, 0x0

    const/4 v7, 0x1

    .line 465
    iget-object v0, p0, Lcom/peel/ui/b/av;->av:Lcom/peel/ui/b/a/a;

    invoke-virtual {v0}, Lcom/peel/ui/b/a/a;->b()Lcom/peel/content/listing/LiveListing;

    move-result-object v0

    .line 466
    invoke-virtual {v0}, Lcom/peel/content/listing/LiveListing;->o()J

    move-result-wide v2

    .line 467
    invoke-virtual {v0}, Lcom/peel/content/listing/LiveListing;->j()J

    move-result-wide v4

    .line 468
    add-long/2addr v4, v4

    .line 470
    if-nez v0, :cond_0

    .line 522
    :goto_0
    return-void

    .line 471
    :cond_0
    sget-object v1, Lcom/peel/ui/b/av;->f:Ljava/lang/String;

    const-string/jumbo v6, "\n\n ***** in renderFullViews() : \n\n"

    invoke-static {v1, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 474
    iget-object v1, p0, Lcom/peel/ui/b/av;->av:Lcom/peel/ui/b/a/a;

    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/peel/ui/b/a/a;->a(JJ)I

    move-result v1

    .line 475
    if-nez v1, :cond_1

    .line 477
    iget-object v1, p0, Lcom/peel/ui/b/av;->ao:Landroid/widget/TextView;

    invoke-virtual {v1, v8}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 478
    iget-object v1, p0, Lcom/peel/ui/b/av;->ao:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getCompoundDrawables()[Landroid/graphics/drawable/Drawable;

    move-result-object v1

    aget-object v1, v1, v7

    invoke-virtual {v1, v9}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 485
    :goto_1
    invoke-direct {p0}, Lcom/peel/ui/b/av;->U()V

    .line 487
    iget-object v1, p0, Lcom/peel/ui/b/av;->av:Lcom/peel/ui/b/a/a;

    invoke-virtual {v1, v2, v3}, Lcom/peel/ui/b/a/a;->a(J)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 489
    iget-object v1, p0, Lcom/peel/ui/b/av;->aq:Landroid/widget/TextView;

    invoke-virtual {v1, v7}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 490
    iget-object v1, p0, Lcom/peel/ui/b/av;->aq:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getCompoundDrawables()[Landroid/graphics/drawable/Drawable;

    move-result-object v1

    aget-object v1, v1, v7

    invoke-virtual {v1, v10}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 497
    :goto_2
    sget-object v1, Lcom/peel/ui/b/av;->f:Ljava/lang/String;

    const-string/jumbo v2, "refresh full views"

    new-instance v3, Lcom/peel/ui/b/be;

    invoke-direct {v3, p0, v0}, Lcom/peel/ui/b/be;-><init>(Lcom/peel/ui/b/av;Lcom/peel/content/listing/LiveListing;)V

    invoke-static {v1, v2, v3}, Lcom/peel/util/i;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/lang/Runnable;

    .line 521
    invoke-direct {p0}, Lcom/peel/ui/b/av;->ae()V

    goto :goto_0

    .line 481
    :cond_1
    iget-object v1, p0, Lcom/peel/ui/b/av;->ao:Landroid/widget/TextView;

    invoke-virtual {v1, v7}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 482
    iget-object v1, p0, Lcom/peel/ui/b/av;->ao:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getCompoundDrawables()[Landroid/graphics/drawable/Drawable;

    move-result-object v1

    aget-object v1, v1, v7

    invoke-virtual {v1, v10}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    goto :goto_1

    .line 493
    :cond_2
    iget-object v1, p0, Lcom/peel/ui/b/av;->aq:Landroid/widget/TextView;

    invoke-virtual {v1, v8}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 494
    iget-object v1, p0, Lcom/peel/ui/b/av;->aq:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getCompoundDrawables()[Landroid/graphics/drawable/Drawable;

    move-result-object v1

    aget-object v1, v1, v7

    invoke-virtual {v1, v9}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    goto :goto_2
.end method

.method static synthetic a(Lcom/peel/ui/b/av;J)J
    .locals 1

    .prologue
    .line 54
    iput-wide p1, p0, Lcom/peel/ui/b/av;->au:J

    return-wide p1
.end method

.method static synthetic a(Lcom/peel/ui/b/av;)Lcom/peel/content/listing/LiveListing;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/peel/ui/b/av;->ak:Lcom/peel/content/listing/LiveListing;

    return-object v0
.end method

.method static synthetic a(Lcom/peel/ui/b/av;Lcom/peel/content/listing/LiveListing;)Lcom/peel/content/listing/LiveListing;
    .locals 0

    .prologue
    .line 54
    iput-object p1, p0, Lcom/peel/ui/b/av;->ak:Lcom/peel/content/listing/LiveListing;

    return-object p1
.end method

.method static synthetic a(Lcom/peel/ui/b/av;Ljava/util/List;)Ljava/util/List;
    .locals 0

    .prologue
    .line 54
    iput-object p1, p0, Lcom/peel/ui/b/av;->aj:Ljava/util/List;

    return-object p1
.end method

.method private a(Lcom/peel/d/i;Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 646
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 647
    sget v0, Lcom/peel/ui/fp;->menu_remote:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 648
    sget v0, Lcom/peel/ui/fp;->menu_share:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 649
    new-instance v0, Lcom/peel/d/a;

    sget-object v1, Lcom/peel/d/d;->b:Lcom/peel/d/d;

    sget-object v2, Lcom/peel/d/b;->a:Lcom/peel/d/b;

    sget-object v3, Lcom/peel/d/c;->b:Lcom/peel/d/c;

    move-object v4, p2

    invoke-direct/range {v0 .. v5}, Lcom/peel/d/a;-><init>(Lcom/peel/d/d;Lcom/peel/d/b;Lcom/peel/d/c;Ljava/lang/String;Ljava/util/List;)V

    sput-object v0, Lcom/peel/ui/b/av;->aC:Lcom/peel/d/a;

    .line 650
    sget-object v0, Lcom/peel/ui/b/av;->aC:Lcom/peel/d/a;

    invoke-virtual {p1, v0}, Lcom/peel/d/i;->a(Lcom/peel/d/a;)V

    .line 651
    return-void
.end method

.method static synthetic a(Lcom/peel/ui/b/av;Lcom/peel/d/i;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 54
    invoke-direct {p0, p1, p2}, Lcom/peel/ui/b/av;->a(Lcom/peel/d/i;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic a(Lcom/peel/ui/b/av;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 54
    invoke-direct {p0, p1}, Lcom/peel/ui/b/av;->a(Ljava/lang/String;)V

    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 317
    sget-object v0, Lcom/peel/ui/b/av;->f:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, " ******** getSchedules: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 318
    const-string/jumbo v0, "live"

    invoke-static {v0}, Lcom/peel/content/a;->c(Ljava/lang/String;)Lcom/peel/content/library/Library;

    move-result-object v0

    .line 319
    invoke-virtual {v0}, Lcom/peel/content/library/Library;->e()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/peel/content/a;->f:Lcom/peel/content/user/User;

    invoke-virtual {v1}, Lcom/peel/content/user/User;->x()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    .line 320
    invoke-virtual {v2}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v2

    invoke-virtual {v2}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v2

    invoke-virtual {v2}, Lcom/peel/data/at;->f()I

    move-result v2

    new-instance v3, Lcom/peel/ui/b/ba;

    invoke-direct {v3, p0}, Lcom/peel/ui/b/ba;-><init>(Lcom/peel/ui/b/av;)V

    .line 318
    invoke-static {p1, v0, v1, v2, v3}, Lcom/peel/content/a/j;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILcom/peel/util/t;)V

    .line 402
    return-void
.end method

.method private ac()V
    .locals 2

    .prologue
    .line 546
    sget-object v0, Lcom/peel/content/a;->e:Ljava/lang/String;

    const-string/jumbo v1, "US"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/peel/content/a;->e:Ljava/lang/String;

    const-string/jumbo v1, "ca"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 547
    :cond_0
    iget-object v0, p0, Lcom/peel/ui/b/av;->av:Lcom/peel/ui/b/a/a;

    new-instance v1, Lcom/peel/ui/b/bh;

    invoke-direct {v1, p0}, Lcom/peel/ui/b/bh;-><init>(Lcom/peel/ui/b/av;)V

    invoke-virtual {v0, v1}, Lcom/peel/ui/b/a/a;->d(Lcom/peel/util/t;)V

    .line 566
    :cond_1
    return-void
.end method

.method private ad()V
    .locals 4

    .prologue
    .line 569
    sget-object v0, Lcom/peel/content/a;->e:Ljava/lang/String;

    const-string/jumbo v1, "US"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 570
    new-instance v0, Lcom/peel/ui/b/bj;

    invoke-direct {v0, p0}, Lcom/peel/ui/b/bj;-><init>(Lcom/peel/ui/b/av;)V

    .line 596
    sget-object v1, Lcom/peel/ui/b/av;->f:Ljava/lang/String;

    const-string/jumbo v2, "\n\n ***** putting in single array content listing as group... nice... @@"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 598
    iget-object v1, p0, Lcom/peel/ui/b/av;->ak:Lcom/peel/content/listing/LiveListing;

    if-nez v1, :cond_1

    .line 616
    :cond_0
    :goto_0
    return-void

    .line 600
    :cond_1
    sget-object v1, Lcom/peel/ui/b/av;->f:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "get graph node: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/peel/ui/b/av;->ak:Lcom/peel/content/listing/LiveListing;

    invoke-virtual {v3}, Lcom/peel/content/listing/LiveListing;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/peel/ui/b/bl;

    invoke-direct {v3, p0, v0}, Lcom/peel/ui/b/bl;-><init>(Lcom/peel/ui/b/av;Lcom/peel/util/t;)V

    invoke-static {v1, v2, v3}, Lcom/peel/util/i;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    goto :goto_0
.end method

.method private ae()V
    .locals 3

    .prologue
    .line 619
    iget-object v0, p0, Lcom/peel/ui/b/av;->aj:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/peel/ui/b/av;->aj:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x2

    if-ge v0, v1, :cond_1

    .line 636
    :cond_0
    :goto_0
    return-void

    .line 622
    :cond_1
    iget-object v0, p0, Lcom/peel/ui/b/av;->aj:Ljava/util/List;

    .line 623
    invoke-static {}, Lcom/peel/content/a;->a()Lcom/peel/data/ContentRoom;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/data/ContentRoom;->a()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/peel/ui/b/av;->i:Lcom/peel/content/library/Library;

    invoke-virtual {v2}, Lcom/peel/content/library/Library;->e()Ljava/lang/String;

    move-result-object v2

    .line 622
    invoke-static {v0, v1, v2}, Lcom/peel/util/bx;->a(Ljava/util/List;Ljava/lang/String;Ljava/lang/String;)V

    .line 625
    iget-object v0, p0, Lcom/peel/ui/b/av;->aj:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 626
    sget-object v0, Lcom/peel/ui/b/av;->f:Ljava/lang/String;

    const-string/jumbo v1, "rendering upcoming list"

    new-instance v2, Lcom/peel/ui/b/ay;

    invoke-direct {v2, p0}, Lcom/peel/ui/b/ay;-><init>(Lcom/peel/ui/b/av;)V

    invoke-static {v0, v1, v2}, Lcom/peel/util/i;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/lang/Runnable;

    goto :goto_0
.end method

.method static synthetic b(Lcom/peel/ui/b/av;)Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/peel/ui/b/av;->am:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic c(Lcom/peel/ui/b/av;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/peel/ui/b/av;->ao:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 54
    sget-object v0, Lcom/peel/ui/b/av;->f:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic d(Lcom/peel/ui/b/av;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/peel/ui/b/av;->aq:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic e(Lcom/peel/ui/b/av;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/peel/ui/b/av;->ap:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic f(Lcom/peel/ui/b/av;)Lcom/peel/ui/b/a/a;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/peel/ui/b/av;->av:Lcom/peel/ui/b/a/a;

    return-object v0
.end method

.method static synthetic g(Lcom/peel/ui/b/av;)Lcom/peel/ui/b/q;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/peel/ui/b/av;->ar:Lcom/peel/ui/b/q;

    return-object v0
.end method

.method static synthetic h(Lcom/peel/ui/b/av;)I
    .locals 1

    .prologue
    .line 54
    iget v0, p0, Lcom/peel/ui/b/av;->h:I

    return v0
.end method

.method static synthetic i(Lcom/peel/ui/b/av;)V
    .locals 0

    .prologue
    .line 54
    invoke-direct {p0}, Lcom/peel/ui/b/av;->ad()V

    return-void
.end method

.method static synthetic j(Lcom/peel/ui/b/av;)V
    .locals 0

    .prologue
    .line 54
    invoke-direct {p0}, Lcom/peel/ui/b/av;->ac()V

    return-void
.end method

.method static synthetic k(Lcom/peel/ui/b/av;)Lcom/peel/ui/ScrollPositionListView;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/peel/ui/b/av;->al:Lcom/peel/ui/ScrollPositionListView;

    return-object v0
.end method

.method static synthetic l(Lcom/peel/ui/b/av;)J
    .locals 2

    .prologue
    .line 54
    iget-wide v0, p0, Lcom/peel/ui/b/av;->au:J

    return-wide v0
.end method

.method static synthetic m(Lcom/peel/ui/b/av;)Landroid/view/View;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/peel/ui/b/av;->an:Landroid/view/View;

    return-object v0
.end method

.method static synthetic n(Lcom/peel/ui/b/av;)I
    .locals 1

    .prologue
    .line 54
    iget v0, p0, Lcom/peel/ui/b/av;->as:I

    return v0
.end method

.method static synthetic o(Lcom/peel/ui/b/av;)I
    .locals 1

    .prologue
    .line 54
    iget v0, p0, Lcom/peel/ui/b/av;->at:I

    return v0
.end method

.method static synthetic p(Lcom/peel/ui/b/av;)V
    .locals 0

    .prologue
    .line 54
    invoke-direct {p0}, Lcom/peel/ui/b/av;->S()V

    return-void
.end method

.method static synthetic q(Lcom/peel/ui/b/av;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/peel/ui/b/av;->ax:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic r(Lcom/peel/ui/b/av;)J
    .locals 2

    .prologue
    .line 54
    iget-wide v0, p0, Lcom/peel/ui/b/av;->az:J

    return-wide v0
.end method

.method static synthetic s(Lcom/peel/ui/b/av;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/peel/ui/b/av;->aw:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic t(Lcom/peel/ui/b/av;)V
    .locals 0

    .prologue
    .line 54
    invoke-direct {p0}, Lcom/peel/ui/b/av;->V()V

    return-void
.end method

.method static synthetic u(Lcom/peel/ui/b/av;)V
    .locals 0

    .prologue
    .line 54
    invoke-direct {p0}, Lcom/peel/ui/b/av;->U()V

    return-void
.end method

.method static synthetic v(Lcom/peel/ui/b/av;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/peel/ui/b/av;->ay:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic w(Lcom/peel/ui/b/av;)Ljava/util/List;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/peel/ui/b/av;->aj:Ljava/util/List;

    return-object v0
.end method


# virtual methods
.method public Z()V
    .locals 2

    .prologue
    .line 684
    sget-object v0, Lcom/peel/ui/b/av;->aC:Lcom/peel/d/a;

    if-eqz v0, :cond_0

    .line 685
    iget-object v0, p0, Lcom/peel/ui/b/av;->b:Lcom/peel/d/i;

    sget-object v1, Lcom/peel/ui/b/av;->aC:Lcom/peel/d/a;

    invoke-virtual {v0, v1}, Lcom/peel/d/i;->a(Lcom/peel/d/a;)V

    .line 687
    :cond_0
    return-void
.end method

.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    .line 99
    sget v0, Lcom/peel/ui/fq;->card_view_listview:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/ui/b/av;->g:Landroid/view/View;

    .line 100
    iget-object v0, p0, Lcom/peel/ui/b/av;->g:Landroid/view/View;

    sget v1, Lcom/peel/ui/fp;->shadow_btn_area:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/ui/b/av;->an:Landroid/view/View;

    .line 101
    iget-object v0, p0, Lcom/peel/ui/b/av;->an:Landroid/view/View;

    sget v1, Lcom/peel/ui/fp;->watch_on_tv:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/peel/ui/b/av;->ao:Landroid/widget/TextView;

    .line 102
    iget-object v0, p0, Lcom/peel/ui/b/av;->an:Landroid/view/View;

    sget v1, Lcom/peel/ui/fp;->set_reminder:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/peel/ui/b/av;->ap:Landroid/widget/TextView;

    .line 103
    iget-object v0, p0, Lcom/peel/ui/b/av;->an:Landroid/view/View;

    sget v1, Lcom/peel/ui/fp;->record_btn:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/peel/ui/b/av;->aq:Landroid/widget/TextView;

    .line 104
    iget-object v0, p0, Lcom/peel/ui/b/av;->g:Landroid/view/View;

    sget v1, Lcom/peel/ui/fp;->detail_list:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/peel/ui/ScrollPositionListView;

    iput-object v0, p0, Lcom/peel/ui/b/av;->al:Lcom/peel/ui/ScrollPositionListView;

    .line 105
    iget-object v0, p0, Lcom/peel/ui/b/av;->g:Landroid/view/View;

    sget v1, Lcom/peel/ui/fp;->epg_image:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/peel/ui/b/av;->am:Landroid/widget/ImageView;

    .line 106
    iget-object v0, p0, Lcom/peel/ui/b/av;->g:Landroid/view/View;

    sget v1, Lcom/peel/ui/fp;->ad_video_view:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/peel/widget/AdVideoView;

    iput-object v0, p0, Lcom/peel/ui/b/av;->aD:Lcom/peel/widget/AdVideoView;

    .line 108
    iget-object v0, p0, Lcom/peel/ui/b/av;->g:Landroid/view/View;

    return-object v0
.end method

.method public a(IILandroid/content/Intent;)V
    .locals 2

    .prologue
    .line 640
    const/16 v0, 0x65

    if-ne p1, v0, :cond_0

    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    if-eqz p3, :cond_0

    const-string/jumbo v0, "loggedin"

    const/4 v1, 0x0

    invoke-virtual {p3, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 641
    iget-object v0, p0, Lcom/peel/ui/b/av;->av:Lcom/peel/ui/b/a/a;

    iget-object v1, p0, Lcom/peel/ui/b/av;->ak:Lcom/peel/content/listing/LiveListing;

    invoke-virtual {v1}, Lcom/peel/content/listing/LiveListing;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/peel/ui/b/a/a;->a(Ljava/lang/String;)V

    .line 643
    :cond_0
    return-void
.end method

.method public a(Landroid/view/MenuItem;)Z
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 655
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v2

    .line 656
    sget v3, Lcom/peel/ui/fp;->menu_share:I

    if-ne v2, v3, :cond_1

    .line 657
    iget-object v2, p0, Lcom/peel/ui/b/av;->ak:Lcom/peel/content/listing/LiveListing;

    if-eqz v2, :cond_0

    .line 658
    new-instance v2, Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/peel/ui/b/av;->aA:Landroid/content/res/Resources;

    sget v4, Lcom/peel/ui/ft;->i_am_watching:I

    new-array v5, v0, [Ljava/lang/Object;

    iget-object v6, p0, Lcom/peel/ui/b/av;->ak:Lcom/peel/content/listing/LiveListing;

    .line 659
    invoke-virtual {v6}, Lcom/peel/content/listing/LiveListing;->l()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v1

    .line 658
    invoke-virtual {v3, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 660
    const-string/jumbo v1, " #"

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v3, p0, Lcom/peel/ui/b/av;->ak:Lcom/peel/content/listing/LiveListing;

    invoke-virtual {v3}, Lcom/peel/content/listing/LiveListing;->l()Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v4, "(\\W)+"

    const-string/jumbo v5, ""

    invoke-virtual {v3, v4, v5}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 661
    new-instance v1, Landroid/content/Intent;

    const-string/jumbo v3, "android.intent.action.SEND"

    invoke-direct {v1, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 662
    const-string/jumbo v3, "text/plain"

    invoke-virtual {v1, v3}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 663
    const-string/jumbo v3, "android.intent.extra.TEXT"

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v3, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 664
    iget-object v2, p0, Lcom/peel/ui/b/av;->aA:Landroid/content/res/Resources;

    sget v3, Lcom/peel/ui/ft;->card_recommend:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/content/Intent;->createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/peel/ui/b/av;->a(Landroid/content/Intent;)V

    .line 673
    :goto_0
    return v0

    .line 666
    :cond_0
    new-instance v1, Landroid/content/Intent;

    const-string/jumbo v2, "android.intent.action.SEND"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 667
    const-string/jumbo v2, "text/plain"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 668
    const-string/jumbo v2, "android.intent.extra.TEXT"

    const-string/jumbo v3, ""

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 669
    iget-object v2, p0, Lcom/peel/ui/b/av;->aA:Landroid/content/res/Resources;

    sget v3, Lcom/peel/ui/ft;->card_recommend:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/content/Intent;->createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/peel/ui/b/av;->a(Landroid/content/Intent;)V

    goto :goto_0

    :cond_1
    move v0, v1

    .line 673
    goto :goto_0
.end method

.method public a_(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 79
    invoke-super {p0, p1}, Lcom/peel/d/u;->a_(Landroid/os/Bundle;)V

    .line 81
    iget-object v0, p0, Lcom/peel/ui/b/av;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "id"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/ui/b/av;->aw:Ljava/lang/String;

    .line 83
    iget-object v0, p0, Lcom/peel/ui/b/av;->aw:Ljava/lang/String;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/peel/ui/b/av;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "show_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/peel/ui/b/av;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "show_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 84
    iget-object v0, p0, Lcom/peel/ui/b/av;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "show_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/ui/b/av;->aw:Ljava/lang/String;

    .line 87
    :cond_0
    iget-object v0, p0, Lcom/peel/ui/b/av;->aw:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 88
    sget-object v0, Lcom/peel/ui/b/av;->f:Ljava/lang/String;

    const-string/jumbo v1, "No id passed in!"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 89
    sget-object v0, Lcom/peel/ui/b/av;->f:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/peel/ui/b/av;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/peel/d/e;->a(Ljava/lang/String;Landroid/support/v4/app/ae;)V

    .line 92
    :cond_1
    iget-object v0, p0, Lcom/peel/ui/b/av;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "movetotop"

    invoke-virtual {v0, v1, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/peel/ui/b/av;->aB:Z

    .line 93
    invoke-virtual {p0}, Lcom/peel/ui/b/av;->n()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/ui/b/av;->aA:Landroid/content/res/Resources;

    .line 94
    iget-object v0, p0, Lcom/peel/ui/b/av;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "context_id"

    invoke-virtual {v0, v1, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/peel/ui/b/av;->h:I

    .line 95
    return-void
.end method

.method public b()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 691
    iget-object v1, p0, Lcom/peel/ui/b/av;->aD:Lcom/peel/widget/AdVideoView;

    invoke-virtual {v1}, Lcom/peel/widget/AdVideoView;->d()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 692
    iget-object v1, p0, Lcom/peel/ui/b/av;->aD:Lcom/peel/widget/AdVideoView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/peel/widget/AdVideoView;->setFullscreen(Z)V

    .line 704
    :goto_0
    return v0

    .line 695
    :cond_0
    iget-boolean v1, p0, Lcom/peel/ui/b/av;->aB:Z

    if-nez v1, :cond_1

    invoke-virtual {p0}, Lcom/peel/ui/b/av;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    invoke-static {v1}, Lcom/peel/d/e;->b(Landroid/support/v4/app/ae;)I

    move-result v1

    if-nez v1, :cond_2

    .line 696
    :cond_1
    new-instance v1, Landroid/content/Intent;

    const-string/jumbo v2, "android.intent.action.VIEW"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 697
    invoke-virtual {p0}, Lcom/peel/ui/b/av;->m()Landroid/support/v4/app/ae;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/ae;->getPackageName()Ljava/lang/String;

    move-result-object v2

    const-class v3, Lcom/peel/main/Home;

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 698
    const/high16 v2, 0x30000000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 699
    const-string/jumbo v2, "peel://home"

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 700
    invoke-virtual {p0, v1}, Lcom/peel/ui/b/av;->a(Landroid/content/Intent;)V

    goto :goto_0

    .line 704
    :cond_2
    invoke-super {p0}, Lcom/peel/d/u;->b()Z

    move-result v0

    goto :goto_0
.end method

.method public c(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    .line 258
    invoke-super {p0, p1}, Lcom/peel/d/u;->c(Landroid/os/Bundle;)V

    .line 260
    sget-object v0, Lcom/peel/content/a;->h:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/peel/ui/b/av;->b:Lcom/peel/d/i;

    if-nez v0, :cond_1

    .line 272
    :cond_0
    :goto_0
    return-void

    .line 263
    :cond_1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/peel/ui/b/av;->aj:Ljava/util/List;

    .line 265
    const-string/jumbo v0, "channel_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/ui/b/av;->ax:Ljava/lang/String;

    .line 266
    const-string/jumbo v0, "starttime"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/peel/ui/b/av;->az:J

    .line 269
    invoke-direct {p0}, Lcom/peel/ui/b/av;->T()V

    .line 271
    iget-object v0, p0, Lcom/peel/ui/b/av;->aD:Lcom/peel/widget/AdVideoView;

    const-string/jumbo v1, "video"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    const-string/jumbo v2, "banner"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v2

    const-string/jumbo v3, "tracking_url"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v3

    const-string/jumbo v4, "campaign_message"

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/peel/widget/AdVideoView;->a(Landroid/os/Bundle;Landroid/os/Bundle;Landroid/os/Bundle;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public d(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 119
    invoke-super {p0, p1}, Lcom/peel/d/u;->d(Landroid/os/Bundle;)V

    .line 121
    new-instance v0, Lcom/peel/ui/b/a/a;

    invoke-virtual {p0}, Lcom/peel/ui/b/av;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    iget-object v2, p0, Lcom/peel/ui/b/av;->g:Landroid/view/View;

    invoke-direct {v0, v1, v2}, Lcom/peel/ui/b/a/a;-><init>(Landroid/support/v4/app/ae;Landroid/view/View;)V

    iput-object v0, p0, Lcom/peel/ui/b/av;->av:Lcom/peel/ui/b/a/a;

    .line 123
    iget-object v0, p0, Lcom/peel/ui/b/av;->ao:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/peel/ui/b/av;->av:Lcom/peel/ui/b/a/a;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 124
    iget-object v0, p0, Lcom/peel/ui/b/av;->aq:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/peel/ui/b/av;->av:Lcom/peel/ui/b/a/a;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 125
    iget-object v0, p0, Lcom/peel/ui/b/av;->ap:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/peel/ui/b/av;->av:Lcom/peel/ui/b/a/a;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 127
    const-string/jumbo v0, "live"

    invoke-static {v0}, Lcom/peel/content/a;->c(Ljava/lang/String;)Lcom/peel/content/library/Library;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/ui/b/av;->i:Lcom/peel/content/library/Library;

    .line 128
    new-instance v0, Lcom/peel/ui/b/q;

    invoke-virtual {p0}, Lcom/peel/ui/b/av;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/peel/ui/b/q;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/peel/ui/b/av;->ar:Lcom/peel/ui/b/q;

    .line 129
    new-instance v0, Lcom/a/a/b/a/a;

    iget-object v1, p0, Lcom/peel/ui/b/av;->ar:Lcom/peel/ui/b/q;

    invoke-direct {v0, v1}, Lcom/a/a/b/a/a;-><init>(Landroid/widget/BaseAdapter;)V

    .line 130
    const-wide/16 v2, 0x12c

    invoke-virtual {v0, v2, v3}, Lcom/a/a/b/a/a;->a(J)V

    .line 131
    iget-object v1, p0, Lcom/peel/ui/b/av;->al:Lcom/peel/ui/ScrollPositionListView;

    invoke-virtual {v0, v1}, Lcom/a/a/b/a/a;->a(Landroid/widget/AbsListView;)V

    .line 133
    iget-object v1, p0, Lcom/peel/ui/b/av;->al:Lcom/peel/ui/ScrollPositionListView;

    invoke-virtual {v1, v0}, Lcom/peel/ui/ScrollPositionListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 136
    if-eqz p1, :cond_0

    .line 137
    iget-object v0, p0, Lcom/peel/ui/b/av;->c:Landroid/os/Bundle;

    invoke-virtual {v0, p1}, Landroid/os/Bundle;->putAll(Landroid/os/Bundle;)V

    .line 140
    :cond_0
    iget-object v0, p0, Lcom/peel/ui/b/av;->aA:Landroid/content/res/Resources;

    sget v1, Lcom/peel/ui/fn;->detail_top_scroll_low_limit:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    iget-object v1, p0, Lcom/peel/ui/b/av;->aA:Landroid/content/res/Resources;

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    invoke-static {v4, v0, v1}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/peel/ui/b/av;->as:I

    .line 141
    iget-object v0, p0, Lcom/peel/ui/b/av;->aA:Landroid/content/res/Resources;

    sget v1, Lcom/peel/ui/fn;->detail_top_scroll_max_limit:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    iget-object v1, p0, Lcom/peel/ui/b/av;->aA:Landroid/content/res/Resources;

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    invoke-static {v4, v0, v1}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/peel/ui/b/av;->at:I

    .line 143
    iget-object v0, p0, Lcom/peel/ui/b/av;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "action"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/ui/b/av;->ay:Ljava/lang/String;

    .line 145
    iget-object v0, p0, Lcom/peel/ui/b/av;->av:Lcom/peel/ui/b/a/a;

    if-eqz v0, :cond_1

    .line 146
    iget-object v0, p0, Lcom/peel/ui/b/av;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "tracking_url"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 147
    iget-object v0, p0, Lcom/peel/ui/b/av;->av:Lcom/peel/ui/b/a/a;

    iget-object v1, p0, Lcom/peel/ui/b/av;->c:Landroid/os/Bundle;

    const-string/jumbo v2, "tracking_url"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    const-string/jumbo v2, "on_wot"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/peel/ui/b/a/a;->b(Ljava/lang/String;)V

    .line 152
    :cond_1
    :goto_0
    return-void

    .line 149
    :cond_2
    iget-object v0, p0, Lcom/peel/ui/b/av;->av:Lcom/peel/ui/b/a/a;

    iget-object v1, p0, Lcom/peel/ui/b/av;->c:Landroid/os/Bundle;

    const-string/jumbo v2, "tracking_url_on_wot"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/peel/ui/b/a/a;->b(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public e(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 678
    invoke-virtual {p1, p1}, Landroid/os/Bundle;->putAll(Landroid/os/Bundle;)V

    .line 679
    invoke-super {p0, p1}, Lcom/peel/d/u;->e(Landroid/os/Bundle;)V

    .line 680
    return-void
.end method

.method public f()V
    .locals 1

    .prologue
    .line 113
    invoke-super {p0}, Lcom/peel/d/u;->f()V

    .line 114
    iget-object v0, p0, Lcom/peel/ui/b/av;->av:Lcom/peel/ui/b/a/a;

    invoke-virtual {v0}, Lcom/peel/ui/b/a/a;->f()V

    .line 115
    return-void
.end method

.method public g()V
    .locals 1

    .prologue
    .line 710
    iget-object v0, p0, Lcom/peel/ui/b/av;->aD:Lcom/peel/widget/AdVideoView;

    invoke-virtual {v0}, Lcom/peel/widget/AdVideoView;->c()V

    .line 711
    invoke-super {p0}, Lcom/peel/d/u;->g()V

    .line 712
    return-void
.end method

.method public w()V
    .locals 4

    .prologue
    .line 414
    iget-object v0, p0, Lcom/peel/ui/b/av;->aD:Lcom/peel/widget/AdVideoView;

    invoke-virtual {v0}, Lcom/peel/widget/AdVideoView;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 415
    iget-object v0, p0, Lcom/peel/ui/b/av;->aD:Lcom/peel/widget/AdVideoView;

    invoke-virtual {v0}, Lcom/peel/widget/AdVideoView;->g()V

    .line 417
    :cond_0
    invoke-virtual {p0}, Lcom/peel/ui/b/av;->Z()V

    .line 418
    invoke-virtual {p0}, Lcom/peel/ui/b/av;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    invoke-static {v0}, Landroid/support/v4/a/q;->a(Landroid/content/Context;)Landroid/support/v4/a/q;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/ui/b/av;->aE:Landroid/content/BroadcastReceiver;

    new-instance v2, Landroid/content/IntentFilter;

    const-string/jumbo v3, "reminder_updated"

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/a/q;->a(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)V

    .line 419
    sget-object v0, Lcom/peel/content/a;->h:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 420
    iget-object v0, p0, Lcom/peel/ui/b/av;->c:Landroid/os/Bundle;

    invoke-virtual {p0, v0}, Lcom/peel/ui/b/av;->c(Landroid/os/Bundle;)V

    .line 422
    :cond_1
    invoke-super {p0}, Lcom/peel/d/u;->w()V

    .line 423
    return-void
.end method

.method public x()V
    .locals 2

    .prologue
    .line 406
    invoke-virtual {p0}, Lcom/peel/ui/b/av;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    invoke-static {v0}, Landroid/support/v4/a/q;->a(Landroid/content/Context;)Landroid/support/v4/a/q;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/ui/b/av;->aE:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/support/v4/a/q;->a(Landroid/content/BroadcastReceiver;)V

    .line 407
    iget-object v0, p0, Lcom/peel/ui/b/av;->aD:Lcom/peel/widget/AdVideoView;

    invoke-virtual {v0}, Lcom/peel/widget/AdVideoView;->b()V

    .line 408
    invoke-super {p0}, Lcom/peel/d/u;->x()V

    .line 409
    return-void
.end method
