.class Lcom/peel/ui/b/ak;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/widget/AbsListView$OnScrollListener;


# instance fields
.field final synthetic a:Lcom/peel/ui/b/ag;


# direct methods
.method constructor <init>(Lcom/peel/ui/b/ag;)V
    .locals 0

    .prologue
    .line 410
    iput-object p1, p0, Lcom/peel/ui/b/ak;->a:Lcom/peel/ui/b/ag;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onScroll(Landroid/widget/AbsListView;III)V
    .locals 6

    .prologue
    .line 418
    if-eqz p1, :cond_1

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/widget/AbsListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_1

    if-nez p2, :cond_1

    .line 419
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-object v2, p0, Lcom/peel/ui/b/ak;->a:Lcom/peel/ui/b/ag;

    invoke-static {v2}, Lcom/peel/ui/b/ag;->c(Lcom/peel/ui/b/ag;)J

    move-result-wide v2

    sub-long/2addr v0, v2

    .line 420
    iget-object v2, p0, Lcom/peel/ui/b/ak;->a:Lcom/peel/ui/b/ag;

    invoke-static {v2}, Lcom/peel/ui/b/ag;->d(Lcom/peel/ui/b/ag;)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getVisibility()I

    move-result v2

    const/16 v3, 0x8

    if-ne v2, v3, :cond_2

    iget-object v2, p0, Lcom/peel/ui/b/ak;->a:Lcom/peel/ui/b/ag;

    invoke-static {v2}, Lcom/peel/ui/b/ag;->e(Lcom/peel/ui/b/ag;)Lcom/peel/ui/ScrollPositionListView;

    move-result-object v2

    invoke-virtual {v2}, Lcom/peel/ui/ScrollPositionListView;->getHeight()I

    move-result v2

    const/16 v3, 0x3e8

    if-le v2, v3, :cond_2

    const/4 v2, 0x1

    invoke-virtual {p1, v2}, Landroid/widget/AbsListView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getTop()I

    move-result v2

    iget-object v3, p0, Lcom/peel/ui/b/ak;->a:Lcom/peel/ui/b/ag;

    invoke-static {v3}, Lcom/peel/ui/b/ag;->d(Lcom/peel/ui/b/ag;)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->getHeight()I

    move-result v3

    iget-object v4, p0, Lcom/peel/ui/b/ak;->a:Lcom/peel/ui/b/ag;

    invoke-static {v4}, Lcom/peel/ui/b/ag;->f(Lcom/peel/ui/b/ag;)I

    move-result v4

    add-int/2addr v3, v4

    if-ge v2, v3, :cond_2

    iget-object v2, p0, Lcom/peel/ui/b/ak;->a:Lcom/peel/ui/b/ag;

    invoke-static {v2}, Lcom/peel/ui/b/ag;->c(Lcom/peel/ui/b/ag;)J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    const-wide/16 v2, 0x82

    cmp-long v2, v0, v2

    if-lez v2, :cond_2

    .line 421
    :cond_0
    iget-object v0, p0, Lcom/peel/ui/b/ak;->a:Lcom/peel/ui/b/ag;

    invoke-static {v0}, Lcom/peel/ui/b/ag;->e(Lcom/peel/ui/b/ag;)Lcom/peel/ui/ScrollPositionListView;

    move-result-object v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/peel/ui/ScrollPositionListView;->smoothScrollToPositionFromTop(II)V

    .line 422
    iget-object v0, p0, Lcom/peel/ui/b/ak;->a:Lcom/peel/ui/b/ag;

    invoke-static {v0}, Lcom/peel/ui/b/ag;->e(Lcom/peel/ui/b/ag;)Lcom/peel/ui/ScrollPositionListView;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/peel/ui/ScrollPositionListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 423
    iget-object v0, p0, Lcom/peel/ui/b/ak;->a:Lcom/peel/ui/b/ag;

    invoke-static {v0}, Lcom/peel/ui/b/ag;->d(Lcom/peel/ui/b/ag;)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 424
    iget-object v0, p0, Lcom/peel/ui/b/ak;->a:Lcom/peel/ui/b/ag;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-static {v0, v2, v3}, Lcom/peel/ui/b/ag;->a(Lcom/peel/ui/b/ag;J)J

    .line 431
    :cond_1
    :goto_0
    return-void

    .line 425
    :cond_2
    iget-object v2, p0, Lcom/peel/ui/b/ak;->a:Lcom/peel/ui/b/ag;

    invoke-static {v2}, Lcom/peel/ui/b/ag;->d(Lcom/peel/ui/b/ag;)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getVisibility()I

    move-result v2

    if-nez v2, :cond_1

    const/4 v2, 0x1

    invoke-virtual {p1, v2}, Landroid/widget/AbsListView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getTop()I

    move-result v2

    iget-object v3, p0, Lcom/peel/ui/b/ak;->a:Lcom/peel/ui/b/ag;

    invoke-static {v3}, Lcom/peel/ui/b/ag;->d(Lcom/peel/ui/b/ag;)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->getHeight()I

    move-result v3

    iget-object v4, p0, Lcom/peel/ui/b/ak;->a:Lcom/peel/ui/b/ag;

    invoke-static {v4}, Lcom/peel/ui/b/ag;->g(Lcom/peel/ui/b/ag;)I

    move-result v4

    add-int/2addr v3, v4

    if-le v2, v3, :cond_1

    const-wide/16 v2, 0x82

    cmp-long v0, v0, v2

    if-gtz v0, :cond_3

    iget-object v0, p0, Lcom/peel/ui/b/ak;->a:Lcom/peel/ui/b/ag;

    invoke-static {v0}, Lcom/peel/ui/b/ag;->c(Lcom/peel/ui/b/ag;)J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    .line 426
    :cond_3
    iget-object v0, p0, Lcom/peel/ui/b/ak;->a:Lcom/peel/ui/b/ag;

    invoke-static {v0}, Lcom/peel/ui/b/ag;->e(Lcom/peel/ui/b/ag;)Lcom/peel/ui/ScrollPositionListView;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/peel/ui/ScrollPositionListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 427
    iget-object v0, p0, Lcom/peel/ui/b/ak;->a:Lcom/peel/ui/b/ag;

    invoke-static {v0}, Lcom/peel/ui/b/ag;->d(Lcom/peel/ui/b/ag;)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 428
    iget-object v0, p0, Lcom/peel/ui/b/ak;->a:Lcom/peel/ui/b/ag;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-static {v0, v2, v3}, Lcom/peel/ui/b/ag;->a(Lcom/peel/ui/b/ag;J)J

    goto :goto_0
.end method

.method public onScrollStateChanged(Landroid/widget/AbsListView;I)V
    .locals 0

    .prologue
    .line 414
    return-void
.end method
