.class Lcom/peel/ui/b/bw;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/peel/ui/b/bq;


# direct methods
.method constructor <init>(Lcom/peel/ui/b/bq;)V
    .locals 0

    .prologue
    .line 150
    iput-object p1, p0, Lcom/peel/ui/b/bw;->a:Lcom/peel/ui/b/bq;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 153
    iget-object v0, p0, Lcom/peel/ui/b/bw;->a:Lcom/peel/ui/b/bq;

    invoke-static {v0}, Lcom/peel/ui/b/bq;->f(Lcom/peel/ui/b/bq;)Lcom/peel/content/model/Season;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/content/model/Season;->b()Ljava/util/List;

    move-result-object v2

    .line 155
    new-instance v0, Lcom/peel/ui/b/bx;

    invoke-direct {v0, p0}, Lcom/peel/ui/b/bx;-><init>(Lcom/peel/ui/b/bw;)V

    invoke-static {v2, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 162
    invoke-static {}, Lcom/peel/ui/b/bq;->c()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "episode list : "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 164
    iget-object v0, p0, Lcom/peel/ui/b/bw;->a:Lcom/peel/ui/b/bq;

    invoke-static {v0}, Lcom/peel/ui/b/bq;->g(Lcom/peel/ui/b/bq;)Landroid/widget/ExpandableListView;

    move-result-object v0

    .line 165
    invoke-virtual {v0}, Landroid/widget/ExpandableListView;->getExpandableListAdapter()Landroid/widget/ExpandableListAdapter;

    move-result-object v0

    check-cast v0, Lcom/peel/ui/b/c;

    .line 166
    if-eqz v0, :cond_2

    .line 167
    invoke-virtual {v0}, Lcom/peel/ui/b/c;->getGroupCount()I

    move-result v3

    .line 169
    const/4 v1, 0x0

    :goto_0
    if-ge v1, v3, :cond_1

    .line 170
    iget-object v4, p0, Lcom/peel/ui/b/bw;->a:Lcom/peel/ui/b/bq;

    invoke-static {v4}, Lcom/peel/ui/b/bq;->g(Lcom/peel/ui/b/bq;)Landroid/widget/ExpandableListView;

    move-result-object v4

    invoke-virtual {v4, v1}, Landroid/widget/ExpandableListView;->isGroupExpanded(I)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 171
    iget-object v4, p0, Lcom/peel/ui/b/bw;->a:Lcom/peel/ui/b/bq;

    invoke-static {v4}, Lcom/peel/ui/b/bq;->g(Lcom/peel/ui/b/bq;)Landroid/widget/ExpandableListView;

    move-result-object v4

    invoke-virtual {v4, v1}, Landroid/widget/ExpandableListView;->collapseGroup(I)Z

    .line 169
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 174
    :cond_1
    iget-object v1, p0, Lcom/peel/ui/b/bw;->a:Lcom/peel/ui/b/bq;

    invoke-static {v1}, Lcom/peel/ui/b/bq;->d(Lcom/peel/ui/b/bq;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, v2}, Lcom/peel/ui/b/c;->a(Ljava/lang/String;Ljava/util/List;)V

    .line 175
    invoke-virtual {v0}, Lcom/peel/ui/b/c;->notifyDataSetChanged()V

    .line 177
    iget-object v0, p0, Lcom/peel/ui/b/bw;->a:Lcom/peel/ui/b/bq;

    invoke-static {v0}, Lcom/peel/ui/b/bq;->g(Lcom/peel/ui/b/bq;)Landroid/widget/ExpandableListView;

    move-result-object v0

    new-instance v1, Lcom/peel/ui/b/by;

    invoke-direct {v1, p0, v2}, Lcom/peel/ui/b/by;-><init>(Lcom/peel/ui/b/bw;Ljava/util/List;)V

    invoke-virtual {v0, v1}, Landroid/widget/ExpandableListView;->setOnGroupExpandListener(Landroid/widget/ExpandableListView$OnGroupExpandListener;)V

    .line 185
    :cond_2
    return-void
.end method
