.class Lcom/peel/ui/r;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/peel/ui/u;

.field final synthetic b:I

.field final synthetic c:Lcom/peel/ui/q;


# direct methods
.method constructor <init>(Lcom/peel/ui/q;Lcom/peel/ui/u;I)V
    .locals 0

    .prologue
    .line 485
    iput-object p1, p0, Lcom/peel/ui/r;->c:Lcom/peel/ui/q;

    iput-object p2, p0, Lcom/peel/ui/r;->a:Lcom/peel/ui/u;

    iput p3, p0, Lcom/peel/ui/r;->b:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 489
    iget-object v0, p0, Lcom/peel/ui/r;->a:Lcom/peel/ui/u;

    iget-object v0, v0, Lcom/peel/ui/u;->a:Landroid/widget/ImageView;

    sget v1, Lcom/peel/ui/fp;->tag_image:I

    .line 490
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 489
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iget v1, p0, Lcom/peel/ui/r;->b:I

    sub-int/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    const/4 v1, 0x7

    if-ge v0, v1, :cond_0

    .line 491
    iget-object v0, p0, Lcom/peel/ui/r;->c:Lcom/peel/ui/q;

    iget-object v0, v0, Lcom/peel/ui/q;->a:Lcom/peel/ui/f;

    invoke-virtual {v0}, Lcom/peel/ui/f;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    invoke-static {v0}, Lcom/peel/util/b/e;->a(Landroid/content/Context;)Lcom/squareup/picasso/Picasso;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/ui/r;->a:Lcom/peel/ui/u;

    iget-object v1, v1, Lcom/peel/ui/u;->b:Ljava/lang/String;

    .line 492
    invoke-virtual {v0, v1}, Lcom/squareup/picasso/Picasso;->load(Ljava/lang/String;)Lcom/squareup/picasso/RequestCreator;

    move-result-object v0

    sget v1, Lcom/peel/ui/fo;->btn_noitem_list:I

    invoke-virtual {v0, v1}, Lcom/squareup/picasso/RequestCreator;->placeholder(I)Lcom/squareup/picasso/RequestCreator;

    move-result-object v0

    .line 493
    invoke-virtual {v0}, Lcom/squareup/picasso/RequestCreator;->noFade()Lcom/squareup/picasso/RequestCreator;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/ui/r;->a:Lcom/peel/ui/u;

    iget-object v1, v1, Lcom/peel/ui/u;->a:Landroid/widget/ImageView;

    new-instance v2, Lcom/peel/ui/s;

    invoke-direct {v2, p0}, Lcom/peel/ui/s;-><init>(Lcom/peel/ui/r;)V

    invoke-virtual {v0, v1, v2}, Lcom/squareup/picasso/RequestCreator;->into(Landroid/widget/ImageView;Lcom/squareup/picasso/Callback;)V

    .line 506
    :cond_0
    return-void
.end method
