.class Lcom/peel/ui/jc;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/peel/ui/gt;


# direct methods
.method constructor <init>(Lcom/peel/ui/gt;)V
    .locals 0

    .prologue
    .line 3381
    iput-object p1, p0, Lcom/peel/ui/jc;->a:Lcom/peel/ui/gt;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 9

    .prologue
    .line 3384
    iget-object v0, p0, Lcom/peel/ui/jc;->a:Lcom/peel/ui/gt;

    invoke-static {v0}, Lcom/peel/ui/gt;->Y(Lcom/peel/ui/gt;)Lcom/peel/widget/ag;

    move-result-object v0

    if-nez v0, :cond_0

    .line 3386
    iget-object v0, p0, Lcom/peel/ui/jc;->a:Lcom/peel/ui/gt;

    invoke-static {v0}, Lcom/peel/ui/gt;->w(Lcom/peel/ui/gt;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lcom/peel/ui/fq;->control_change_room:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 3387
    sget v0, Lcom/peel/ui/fp;->activities_lv:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    .line 3388
    new-instance v2, Lcom/peel/ui/jd;

    iget-object v3, p0, Lcom/peel/ui/jc;->a:Lcom/peel/ui/gt;

    invoke-virtual {v3}, Lcom/peel/ui/gt;->m()Landroid/support/v4/app/ae;

    move-result-object v3

    sget v4, Lcom/peel/ui/fq;->settings_adddevice_activity_item:I

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    iget-object v7, p0, Lcom/peel/ui/jc;->a:Lcom/peel/ui/gt;

    sget v8, Lcom/peel/ui/ft;->DeviceType5:I

    invoke-virtual {v7, v8}, Lcom/peel/ui/gt;->a(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-direct {v2, p0, v3, v4, v5}, Lcom/peel/ui/jd;-><init>(Lcom/peel/ui/jc;Landroid/content/Context;I[Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 3398
    iget-object v2, p0, Lcom/peel/ui/jc;->a:Lcom/peel/ui/gt;

    new-instance v3, Lcom/peel/widget/ag;

    iget-object v4, p0, Lcom/peel/ui/jc;->a:Lcom/peel/ui/gt;

    invoke-virtual {v4}, Lcom/peel/ui/gt;->m()Landroid/support/v4/app/ae;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/peel/widget/ag;-><init>(Landroid/content/Context;)V

    invoke-static {v2, v3}, Lcom/peel/ui/gt;->c(Lcom/peel/ui/gt;Lcom/peel/widget/ag;)Lcom/peel/widget/ag;

    .line 3399
    new-instance v2, Lcom/peel/ui/je;

    invoke-direct {v2, p0}, Lcom/peel/ui/je;-><init>(Lcom/peel/ui/jc;)V

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 3418
    iget-object v0, p0, Lcom/peel/ui/jc;->a:Lcom/peel/ui/gt;

    invoke-static {v0}, Lcom/peel/ui/gt;->Y(Lcom/peel/ui/gt;)Lcom/peel/widget/ag;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/peel/widget/ag;->a(Landroid/view/View;)Lcom/peel/widget/ag;

    .line 3419
    iget-object v0, p0, Lcom/peel/ui/jc;->a:Lcom/peel/ui/gt;

    invoke-static {v0}, Lcom/peel/ui/gt;->Y(Lcom/peel/ui/gt;)Lcom/peel/widget/ag;

    move-result-object v0

    sget v1, Lcom/peel/ui/ft;->button_volume_controller:I

    invoke-virtual {v0, v1}, Lcom/peel/widget/ag;->a(I)Lcom/peel/widget/ag;

    .line 3420
    iget-object v0, p0, Lcom/peel/ui/jc;->a:Lcom/peel/ui/gt;

    invoke-static {v0}, Lcom/peel/ui/gt;->Y(Lcom/peel/ui/gt;)Lcom/peel/widget/ag;

    move-result-object v0

    sget v1, Lcom/peel/ui/ft;->which_volume_control:I

    invoke-virtual {v0, v1}, Lcom/peel/widget/ag;->b(I)Lcom/peel/widget/ag;

    .line 3423
    :cond_0
    iget-object v0, p0, Lcom/peel/ui/jc;->a:Lcom/peel/ui/gt;

    invoke-static {v0}, Lcom/peel/ui/gt;->Y(Lcom/peel/ui/gt;)Lcom/peel/widget/ag;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/widget/ag;->isShowing()Z

    move-result v0

    if-nez v0, :cond_1

    .line 3424
    iget-object v0, p0, Lcom/peel/ui/jc;->a:Lcom/peel/ui/gt;

    invoke-static {v0}, Lcom/peel/ui/gt;->Y(Lcom/peel/ui/gt;)Lcom/peel/widget/ag;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/ui/jc;->a:Lcom/peel/ui/gt;

    invoke-static {v1}, Lcom/peel/ui/gt;->Y(Lcom/peel/ui/gt;)Lcom/peel/widget/ag;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/peel/widget/ag;->a(Lcom/peel/widget/ag;)Lcom/peel/widget/ag;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/widget/ag;->show()V

    .line 3426
    :cond_1
    return-void
.end method
