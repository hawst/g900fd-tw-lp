.class public Lcom/peel/ui/ar;
.super Lcom/peel/widget/ag;


# static fields
.field private static final a:Ljava/lang/String;

.field private static final b:[I


# instance fields
.field private c:Landroid/content/Context;

.field private d:Ljava/lang/String;

.field private e:Lcom/peel/d/i;

.field private f:I

.field private g:Landroid/widget/LinearLayout;

.field private k:Lcom/peel/ui/BackKeyEditText;

.field private l:Landroid/widget/TextView;

.field private m:Landroid/widget/TextView;

.field private n:Landroid/widget/EditText;

.field private o:I

.field private p:Z

.field private q:Z

.field private r:Landroid/text/InputFilter;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 42
    const-class v0, Lcom/peel/ui/ar;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/peel/ui/ar;->a:Ljava/lang/String;

    .line 44
    const/4 v0, 0x7

    new-array v0, v0, [I

    const/4 v1, 0x0

    sget v2, Lcom/peel/ui/ft;->sunday:I

    aput v2, v0, v1

    const/4 v1, 0x1

    sget v2, Lcom/peel/ui/ft;->monday:I

    aput v2, v0, v1

    const/4 v1, 0x2

    sget v2, Lcom/peel/ui/ft;->tuesday:I

    aput v2, v0, v1

    const/4 v1, 0x3

    sget v2, Lcom/peel/ui/ft;->wednesday:I

    aput v2, v0, v1

    const/4 v1, 0x4

    sget v2, Lcom/peel/ui/ft;->thursday:I

    aput v2, v0, v1

    const/4 v1, 0x5

    sget v2, Lcom/peel/ui/ft;->friday:I

    aput v2, v0, v1

    const/4 v1, 0x6

    sget v2, Lcom/peel/ui/ft;->saturday:I

    aput v2, v0, v1

    sput-object v0, Lcom/peel/ui/ar;->b:[I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/peel/d/i;Ljava/util/Date;)V
    .locals 12

    .prologue
    const/4 v7, 0x0

    const/16 v2, 0x8

    const/4 v1, 0x1

    const/4 v6, 0x0

    .line 394
    invoke-direct {p0, p1}, Lcom/peel/widget/ag;-><init>(Landroid/content/Context;)V

    .line 60
    iput-boolean v1, p0, Lcom/peel/ui/ar;->p:Z

    .line 61
    iput-boolean v6, p0, Lcom/peel/ui/ar;->q:Z

    .line 64
    new-instance v0, Lcom/peel/ui/as;

    invoke-direct {v0, p0}, Lcom/peel/ui/as;-><init>(Lcom/peel/ui/ar;)V

    iput-object v0, p0, Lcom/peel/ui/ar;->r:Landroid/text/InputFilter;

    .line 396
    iput-object p1, p0, Lcom/peel/ui/ar;->c:Landroid/content/Context;

    .line 397
    iput-object p2, p0, Lcom/peel/ui/ar;->e:Lcom/peel/d/i;

    .line 398
    iput-boolean v6, p0, Lcom/peel/ui/ar;->p:Z

    .line 400
    sget-object v0, Lcom/peel/util/x;->c:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/text/SimpleDateFormat;

    invoke-virtual {v0, p3}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/ui/ar;->d:Ljava/lang/String;

    .line 401
    invoke-static {p1, v1}, Lcom/peel/ui/ar;->a(Landroid/content/Context;Z)[Ljava/lang/String;

    move-result-object v3

    .line 402
    iget-object v0, p0, Lcom/peel/ui/ar;->d:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/peel/ui/ar;->a(Ljava/lang/String;Z)I

    move-result v0

    iput v0, p0, Lcom/peel/ui/ar;->f:I

    .line 404
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lcom/peel/ui/fq;->ss_datetime_picker:I

    invoke-virtual {v0, v1, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/peel/ui/ar;->a(Landroid/view/View;)Lcom/peel/widget/ag;

    .line 405
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/peel/ui/ft;->customize_time:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/peel/ui/ar;->a(Ljava/lang/CharSequence;)Lcom/peel/widget/ag;

    .line 406
    invoke-virtual {p0, p0}, Lcom/peel/ui/ar;->a(Lcom/peel/widget/ag;)Lcom/peel/widget/ag;

    .line 408
    sget v0, Lcom/peel/ui/fp;->time_linearlayout:I

    invoke-virtual {p0, v0}, Lcom/peel/ui/ar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/peel/ui/ar;->g:Landroid/widget/LinearLayout;

    .line 409
    iget-object v0, p0, Lcom/peel/ui/ar;->g:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 410
    sget v0, Lcom/peel/ui/fp;->dividerView:I

    invoke-virtual {p0, v0}, Lcom/peel/ui/ar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 412
    sget v0, Lcom/peel/ui/fp;->date_textview:I

    invoke-virtual {p0, v0}, Lcom/peel/ui/ar;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 413
    iget v0, p0, Lcom/peel/ui/ar;->f:I

    aget-object v0, v3, v0

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 433
    invoke-direct {p0}, Lcom/peel/ui/ar;->i()V

    .line 435
    sget v0, Lcom/peel/ui/fp;->left_date_button:I

    invoke-virtual {p0, v0}, Lcom/peel/ui/ar;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/Button;

    .line 436
    sget v0, Lcom/peel/ui/fp;->right_date_button:I

    invoke-virtual {p0, v0}, Lcom/peel/ui/ar;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/Button;

    .line 437
    iget v0, p0, Lcom/peel/ui/ar;->f:I

    if-nez v0, :cond_0

    .line 438
    invoke-virtual {v4, v6}, Landroid/widget/Button;->setEnabled(Z)V

    .line 440
    :cond_0
    iget v0, p0, Lcom/peel/ui/ar;->f:I

    const/4 v1, 0x5

    if-ne v0, v1, :cond_1

    .line 441
    invoke-virtual {v5, v6}, Landroid/widget/Button;->setEnabled(Z)V

    .line 443
    :cond_1
    sget v0, Lcom/peel/ui/ft;->set:I

    new-instance v1, Lcom/peel/ui/at;

    invoke-direct {v1, p0, p1}, Lcom/peel/ui/at;-><init>(Lcom/peel/ui/ar;Landroid/content/Context;)V

    invoke-virtual {p0, v0, v1}, Lcom/peel/ui/ar;->a(ILandroid/content/DialogInterface$OnClickListener;)Lcom/peel/widget/ag;

    .line 470
    sget v0, Lcom/peel/ui/ft;->cancel:I

    invoke-virtual {p0, v0, v7}, Lcom/peel/ui/ar;->c(ILandroid/content/DialogInterface$OnClickListener;)Lcom/peel/widget/ag;

    .line 472
    new-instance v0, Lcom/peel/ui/au;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/peel/ui/au;-><init>(Lcom/peel/ui/ar;Landroid/widget/TextView;[Ljava/lang/String;Landroid/widget/Button;Landroid/widget/Button;)V

    invoke-virtual {v4, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 489
    new-instance v6, Lcom/peel/ui/av;

    move-object v7, p0

    move-object v8, v2

    move-object v9, v3

    move-object v10, v5

    move-object v11, v4

    invoke-direct/range {v6 .. v11}, Lcom/peel/ui/av;-><init>(Lcom/peel/ui/ar;Landroid/widget/TextView;[Ljava/lang/String;Landroid/widget/Button;Landroid/widget/Button;)V

    invoke-virtual {v5, v6}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 505
    return-void
.end method

.method public static a(Ljava/lang/String;Z)I
    .locals 8

    .prologue
    const/4 v7, 0x6

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 517
    if-eqz p1, :cond_1

    move v0, v1

    .line 518
    :goto_0
    new-instance v3, Ljava/util/GregorianCalendar;

    invoke-direct {v3}, Ljava/util/GregorianCalendar;-><init>()V

    .line 520
    if-eqz p0, :cond_0

    .line 521
    invoke-virtual {v3, v7}, Ljava/util/GregorianCalendar;->get(I)I

    move-result v4

    .line 523
    :try_start_0
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string/jumbo v5, "yyyy-MM-dd HH:mm:ss"

    sget-object v6, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v0, v5, v6}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 524
    invoke-virtual {v0, p0}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/util/GregorianCalendar;->setTime(Ljava/util/Date;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-object v0, v3

    .line 529
    :goto_1
    invoke-virtual {v0, v7}, Ljava/util/GregorianCalendar;->get(I)I

    move-result v0

    sub-int/2addr v0, v4

    if-eqz p1, :cond_2

    :goto_2
    add-int/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    .line 532
    :cond_0
    rem-int/lit8 v0, v0, 0x6

    return v0

    :cond_1
    move v0, v2

    .line 517
    goto :goto_0

    .line 525
    :catch_0
    move-exception v0

    .line 526
    sget-object v3, Lcom/peel/ui/ar;->a:Ljava/lang/String;

    sget-object v5, Lcom/peel/ui/ar;->a:Ljava/lang/String;

    invoke-static {v3, v5, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 527
    new-instance v0, Ljava/util/GregorianCalendar;

    invoke-direct {v0}, Ljava/util/GregorianCalendar;-><init>()V

    goto :goto_1

    :cond_2
    move v1, v2

    .line 529
    goto :goto_2
.end method

.method static synthetic a(Lcom/peel/ui/ar;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 41
    iput-object p1, p0, Lcom/peel/ui/ar;->d:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic a(Lcom/peel/ui/ar;Lcom/peel/ui/aw;)V
    .locals 0

    .prologue
    .line 41
    invoke-direct {p0, p1}, Lcom/peel/ui/ar;->a(Lcom/peel/ui/aw;)V

    return-void
.end method

.method private a(Lcom/peel/ui/aw;)V
    .locals 3

    .prologue
    .line 508
    sget-object v0, Lcom/peel/ui/aw;->a:Lcom/peel/ui/aw;

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/peel/ui/ar;->n:Landroid/widget/EditText;

    if-eqz v0, :cond_0

    .line 509
    iget-object v0, p0, Lcom/peel/ui/ar;->c:Landroid/content/Context;

    const-string/jumbo v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 510
    iget-object v1, p0, Lcom/peel/ui/ar;->n:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 512
    :cond_0
    iget-object v0, p0, Lcom/peel/ui/ar;->g:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->requestFocus()Z

    .line 513
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/peel/ui/ar;->n:Landroid/widget/EditText;

    .line 514
    return-void
.end method

.method static synthetic a(Lcom/peel/ui/ar;)Z
    .locals 1

    .prologue
    .line 41
    iget-boolean v0, p0, Lcom/peel/ui/ar;->q:Z

    return v0
.end method

.method public static a(Landroid/content/Context;Z)[Ljava/lang/String;
    .locals 13

    .prologue
    const/4 v12, 0x6

    const/4 v11, 0x7

    const/4 v2, 0x2

    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 537
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 538
    if-eqz p1, :cond_0

    .line 539
    new-array v0, v12, [Ljava/lang/String;

    .line 540
    sget v1, Lcom/peel/ui/ft;->today:I

    invoke-virtual {v3, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v9

    .line 541
    sget v1, Lcom/peel/ui/ft;->tomorrow:I

    invoke-virtual {v3, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v10

    .line 542
    new-instance v1, Ljava/util/GregorianCalendar;

    invoke-direct {v1}, Ljava/util/GregorianCalendar;-><init>()V

    .line 543
    invoke-virtual {v1, v11}, Ljava/util/GregorianCalendar;->get(I)I

    move-result v4

    move v1, v2

    .line 544
    :goto_0
    if-ge v1, v12, :cond_1

    .line 545
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    sget-object v6, Lcom/peel/ui/ar;->b:[I

    add-int v7, v1, v4

    add-int/lit8 v7, v7, -0x1

    rem-int/lit8 v7, v7, 0x7

    aget v6, v6, v7

    new-array v7, v2, [Ljava/lang/Object;

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v8

    invoke-virtual {v8}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v7, v9

    aput-object v3, v7, v10

    invoke-virtual {v5, v6, v7}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v0, v1

    .line 544
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 549
    :cond_0
    new-array v0, v11, [Ljava/lang/String;

    .line 550
    sget v1, Lcom/peel/ui/ft;->yesterday:I

    invoke-virtual {v3, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v9

    .line 551
    sget v1, Lcom/peel/ui/ft;->today:I

    invoke-virtual {v3, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v10

    .line 552
    sget v1, Lcom/peel/ui/ft;->tomorrow:I

    invoke-virtual {v3, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v2

    .line 553
    new-instance v1, Ljava/util/GregorianCalendar;

    invoke-direct {v1}, Ljava/util/GregorianCalendar;-><init>()V

    .line 554
    invoke-virtual {v1, v11}, Ljava/util/GregorianCalendar;->get(I)I

    move-result v4

    .line 555
    const/4 v1, 0x3

    :goto_1
    if-ge v1, v11, :cond_1

    .line 556
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    sget-object v6, Lcom/peel/ui/ar;->b:[I

    add-int v7, v1, v4

    add-int/lit8 v7, v7, -0x2

    rem-int/lit8 v7, v7, 0x7

    aget v6, v6, v7

    new-array v7, v2, [Ljava/lang/Object;

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v8

    invoke-virtual {v8}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v7, v9

    aput-object v3, v7, v10

    invoke-virtual {v5, v6, v7}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v0, v1

    .line 555
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 558
    :cond_1
    return-object v0
.end method

.method static synthetic b(Lcom/peel/ui/ar;)I
    .locals 1

    .prologue
    .line 41
    iget v0, p0, Lcom/peel/ui/ar;->f:I

    return v0
.end method

.method static synthetic c(Lcom/peel/ui/ar;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/peel/ui/ar;->d:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic d(Lcom/peel/ui/ar;)I
    .locals 2

    .prologue
    .line 41
    iget v0, p0, Lcom/peel/ui/ar;->f:I

    add-int/lit8 v1, v0, -0x1

    iput v1, p0, Lcom/peel/ui/ar;->f:I

    return v0
.end method

.method static synthetic e(Lcom/peel/ui/ar;)I
    .locals 2

    .prologue
    .line 41
    iget v0, p0, Lcom/peel/ui/ar;->f:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/peel/ui/ar;->f:I

    return v0
.end method

.method private i()V
    .locals 9

    .prologue
    const/16 v4, 0xb

    const/4 v8, 0x1

    const/4 v7, 0x0

    const/16 v6, 0xc

    .line 563
    new-instance v0, Ljava/util/GregorianCalendar;

    invoke-direct {v0}, Ljava/util/GregorianCalendar;-><init>()V

    .line 564
    new-instance v1, Ljava/text/SimpleDateFormat;

    const-string/jumbo v2, "yyyy-MM-dd HH:mm:ss"

    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v1, v2, v3}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 566
    :try_start_0
    iget-object v2, p0, Lcom/peel/ui/ar;->d:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/GregorianCalendar;->setTime(Ljava/util/Date;)V
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    .line 574
    :goto_0
    iget-boolean v1, p0, Lcom/peel/ui/ar;->q:Z

    if-eqz v1, :cond_1

    .line 575
    invoke-virtual {v0, v4}, Ljava/util/GregorianCalendar;->get(I)I

    move-result v1

    iput v1, p0, Lcom/peel/ui/ar;->o:I

    .line 576
    iget-object v1, p0, Lcom/peel/ui/ar;->k:Lcom/peel/ui/BackKeyEditText;

    if-eqz v1, :cond_0

    .line 577
    iget-object v1, p0, Lcom/peel/ui/ar;->k:Lcom/peel/ui/BackKeyEditText;

    const-string/jumbo v2, "%02d"

    new-array v3, v8, [Ljava/lang/Object;

    iget v4, p0, Lcom/peel/ui/ar;->o:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v7

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/peel/ui/BackKeyEditText;->setText(Ljava/lang/CharSequence;)V

    .line 578
    iget-object v1, p0, Lcom/peel/ui/ar;->l:Landroid/widget/TextView;

    const-string/jumbo v2, "%02d"

    new-array v3, v8, [Ljava/lang/Object;

    invoke-virtual {v0, v6}, Ljava/util/GregorianCalendar;->get(I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v3, v7

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 599
    :cond_0
    :goto_1
    return-void

    .line 567
    :catch_0
    move-exception v0

    .line 568
    sget-object v1, Lcom/peel/ui/ar;->a:Ljava/lang/String;

    sget-object v2, Lcom/peel/ui/ar;->a:Ljava/lang/String;

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 569
    new-instance v0, Ljava/util/GregorianCalendar;

    invoke-direct {v0}, Ljava/util/GregorianCalendar;-><init>()V

    goto :goto_0

    .line 581
    :cond_1
    invoke-virtual {v0, v4}, Ljava/util/GregorianCalendar;->get(I)I

    move-result v1

    .line 582
    if-le v1, v6, :cond_2

    .line 583
    add-int/lit8 v2, v1, -0xc

    iput v2, p0, Lcom/peel/ui/ar;->o:I

    .line 589
    :goto_2
    iget-object v2, p0, Lcom/peel/ui/ar;->k:Lcom/peel/ui/BackKeyEditText;

    if-eqz v2, :cond_0

    .line 590
    iget-object v2, p0, Lcom/peel/ui/ar;->k:Lcom/peel/ui/BackKeyEditText;

    const-string/jumbo v3, "%02d"

    new-array v4, v8, [Ljava/lang/Object;

    iget v5, p0, Lcom/peel/ui/ar;->o:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v7

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/peel/ui/BackKeyEditText;->setText(Ljava/lang/CharSequence;)V

    .line 591
    iget-object v2, p0, Lcom/peel/ui/ar;->l:Landroid/widget/TextView;

    const-string/jumbo v3, "%02d"

    new-array v4, v8, [Ljava/lang/Object;

    invoke-virtual {v0, v6}, Ljava/util/GregorianCalendar;->get(I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v4, v7

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 592
    if-lt v1, v6, :cond_4

    .line 593
    iget-object v0, p0, Lcom/peel/ui/ar;->m:Landroid/widget/TextView;

    const-string/jumbo v1, "PM"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 584
    :cond_2
    if-nez v1, :cond_3

    .line 585
    iput v6, p0, Lcom/peel/ui/ar;->o:I

    goto :goto_2

    .line 587
    :cond_3
    iput v1, p0, Lcom/peel/ui/ar;->o:I

    goto :goto_2

    .line 595
    :cond_4
    iget-object v0, p0, Lcom/peel/ui/ar;->m:Landroid/widget/TextView;

    const-string/jumbo v1, "AM"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method


# virtual methods
.method public a()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v1, 0x1

    .line 602
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 603
    sget-object v0, Lcom/peel/ui/ar;->a:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "update() time to: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/peel/ui/ar;->d:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 604
    invoke-static {}, Lcom/peel/util/a/f;->a()Lcom/peel/util/a/f;

    move-result-object v3

    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v0}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    const/16 v4, 0x400

    const/16 v5, 0x7db

    iget-object v6, p0, Lcom/peel/ui/ar;->d:Ljava/lang/String;

    invoke-virtual {v3, v0, v4, v5, v6}, Lcom/peel/util/a/f;->a(IIILjava/lang/String;)V

    .line 605
    iget-boolean v0, p0, Lcom/peel/ui/ar;->p:Z

    if-eqz v0, :cond_1

    .line 606
    iget-object v0, p0, Lcom/peel/ui/ar;->e:Lcom/peel/d/i;

    const-string/jumbo v3, "time"

    iget-object v4, p0, Lcom/peel/ui/ar;->d:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Lcom/peel/d/i;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 613
    :goto_1
    const-string/jumbo v0, "refresh_fragments"

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/String;

    const-class v4, Lcom/peel/ui/ci;

    invoke-virtual {v4}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v7

    const-class v4, Lcom/peel/ui/cg;

    invoke-virtual {v4}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v1

    const/4 v4, 0x2

    const-class v5, Lcom/peel/ui/ch;

    invoke-virtual {v5}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v2, v0, v3}, Landroid/os/Bundle;->putStringArray(Ljava/lang/String;[Ljava/lang/String;)V

    .line 614
    const-string/jumbo v0, "selective"

    invoke-virtual {v2, v0, v7}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 615
    const-string/jumbo v0, "refresh"

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 616
    iget-object v0, p0, Lcom/peel/ui/ar;->e:Lcom/peel/d/i;

    invoke-virtual {v0}, Lcom/peel/d/i;->c()Landroid/support/v4/app/ae;

    move-result-object v0

    .line 617
    invoke-static {v0}, Lcom/peel/d/e;->a(Landroid/support/v4/app/ae;)Lcom/peel/d/u;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v2}, Lcom/peel/d/e;->d(Landroid/support/v4/app/ae;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 618
    return-void

    .line 604
    :cond_0
    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v0}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/data/at;->f()I

    move-result v0

    goto :goto_0

    .line 608
    :cond_1
    :try_start_0
    const-string/jumbo v3, "date_ms"

    sget-object v0, Lcom/peel/util/x;->b:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/text/SimpleDateFormat;

    iget-object v4, p0, Lcom/peel/ui/ar;->d:Ljava/lang/String;

    invoke-virtual {v0, v4}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 609
    :catch_0
    move-exception v0

    .line 610
    sget-object v3, Lcom/peel/ui/ar;->a:Ljava/lang/String;

    sget-object v4, Lcom/peel/ui/ar;->a:Ljava/lang/String;

    invoke-static {v3, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method
