.class public final Lcom/peel/ui/fu;
.super Ljava/lang/Object;


# static fields
.field public static final ActionBarProgressBar_MyStyle:I = 0x7f0e0000

.field public static final ActionButtonStyle:I = 0x7f0e0001

.field public static final Animation_Popup:I = 0x7f0e0003

.field public static final AutoCompleteTextView:I = 0x7f0e0004

.field public static final Base_TextAppearance_AppCompat:I = 0x7f0e0005

.field public static final Base_TextAppearance_AppCompat_Body1:I = 0x7f0e0006

.field public static final Base_TextAppearance_AppCompat_Body2:I = 0x7f0e0007

.field public static final Base_TextAppearance_AppCompat_Button:I = 0x7f0e0008

.field public static final Base_TextAppearance_AppCompat_Caption:I = 0x7f0e0009

.field public static final Base_TextAppearance_AppCompat_Display1:I = 0x7f0e000a

.field public static final Base_TextAppearance_AppCompat_Display2:I = 0x7f0e000b

.field public static final Base_TextAppearance_AppCompat_Display3:I = 0x7f0e000c

.field public static final Base_TextAppearance_AppCompat_Display4:I = 0x7f0e000d

.field public static final Base_TextAppearance_AppCompat_Headline:I = 0x7f0e000e

.field public static final Base_TextAppearance_AppCompat_Inverse:I = 0x7f0e000f

.field public static final Base_TextAppearance_AppCompat_Large:I = 0x7f0e0010

.field public static final Base_TextAppearance_AppCompat_Large_Inverse:I = 0x7f0e0011

.field public static final Base_TextAppearance_AppCompat_Light_Widget_PopupMenu_Large:I = 0x7f0e0012

.field public static final Base_TextAppearance_AppCompat_Light_Widget_PopupMenu_Small:I = 0x7f0e0013

.field public static final Base_TextAppearance_AppCompat_Medium:I = 0x7f0e0014

.field public static final Base_TextAppearance_AppCompat_Medium_Inverse:I = 0x7f0e0015

.field public static final Base_TextAppearance_AppCompat_Menu:I = 0x7f0e0016

.field public static final Base_TextAppearance_AppCompat_SearchResult:I = 0x7f0e0017

.field public static final Base_TextAppearance_AppCompat_SearchResult_Subtitle:I = 0x7f0e0018

.field public static final Base_TextAppearance_AppCompat_SearchResult_Title:I = 0x7f0e0019

.field public static final Base_TextAppearance_AppCompat_Small:I = 0x7f0e001a

.field public static final Base_TextAppearance_AppCompat_Small_Inverse:I = 0x7f0e001b

.field public static final Base_TextAppearance_AppCompat_Subhead:I = 0x7f0e001c

.field public static final Base_TextAppearance_AppCompat_Subhead_Inverse:I = 0x7f0e001d

.field public static final Base_TextAppearance_AppCompat_Title:I = 0x7f0e001e

.field public static final Base_TextAppearance_AppCompat_Title_Inverse:I = 0x7f0e001f

.field public static final Base_TextAppearance_AppCompat_Widget_ActionBar_Menu:I = 0x7f0e0020

.field public static final Base_TextAppearance_AppCompat_Widget_ActionBar_Subtitle:I = 0x7f0e0021

.field public static final Base_TextAppearance_AppCompat_Widget_ActionBar_Subtitle_Inverse:I = 0x7f0e0022

.field public static final Base_TextAppearance_AppCompat_Widget_ActionBar_Title:I = 0x7f0e0023

.field public static final Base_TextAppearance_AppCompat_Widget_ActionBar_Title_Inverse:I = 0x7f0e0024

.field public static final Base_TextAppearance_AppCompat_Widget_ActionMode_Subtitle:I = 0x7f0e0025

.field public static final Base_TextAppearance_AppCompat_Widget_ActionMode_Title:I = 0x7f0e0026

.field public static final Base_TextAppearance_AppCompat_Widget_DropDownItem:I = 0x7f0e0027

.field public static final Base_TextAppearance_AppCompat_Widget_PopupMenu_Large:I = 0x7f0e0028

.field public static final Base_TextAppearance_AppCompat_Widget_PopupMenu_Small:I = 0x7f0e0029

.field public static final Base_TextAppearance_AppCompat_Widget_Switch:I = 0x7f0e002a

.field public static final Base_TextAppearance_Widget_AppCompat_ExpandedMenu_Item:I = 0x7f0e002b

.field public static final Base_TextAppearance_Widget_AppCompat_Toolbar_Subtitle:I = 0x7f0e002c

.field public static final Base_TextAppearance_Widget_AppCompat_Toolbar_Title:I = 0x7f0e002d

.field public static final Base_ThemeOverlay_AppCompat:I = 0x7f0e0038

.field public static final Base_ThemeOverlay_AppCompat_ActionBar:I = 0x7f0e0039

.field public static final Base_ThemeOverlay_AppCompat_Dark:I = 0x7f0e003a

.field public static final Base_ThemeOverlay_AppCompat_Dark_ActionBar:I = 0x7f0e003b

.field public static final Base_ThemeOverlay_AppCompat_Light:I = 0x7f0e003c

.field public static final Base_Theme_AppCompat:I = 0x7f0e002e

.field public static final Base_Theme_AppCompat_CompactMenu:I = 0x7f0e002f

.field public static final Base_Theme_AppCompat_Dialog:I = 0x7f0e0030

.field public static final Base_Theme_AppCompat_DialogWhenLarge:I = 0x7f0e0032

.field public static final Base_Theme_AppCompat_Dialog_FixedSize:I = 0x7f0e0031

.field public static final Base_Theme_AppCompat_Light:I = 0x7f0e0033

.field public static final Base_Theme_AppCompat_Light_DarkActionBar:I = 0x7f0e0034

.field public static final Base_Theme_AppCompat_Light_Dialog:I = 0x7f0e0035

.field public static final Base_Theme_AppCompat_Light_DialogWhenLarge:I = 0x7f0e0037

.field public static final Base_Theme_AppCompat_Light_Dialog_FixedSize:I = 0x7f0e0036

.field public static final Base_V11_Theme_AppCompat:I = 0x7f0e0232

.field public static final Base_V11_Theme_AppCompat_Dialog:I = 0x7f0e0233

.field public static final Base_V11_Theme_AppCompat_Light:I = 0x7f0e0234

.field public static final Base_V11_Theme_AppCompat_Light_Dialog:I = 0x7f0e0235

.field public static final Base_V14_Theme_AppCompat:I = 0x7f0e0236

.field public static final Base_V14_Theme_AppCompat_Dialog:I = 0x7f0e0237

.field public static final Base_V14_Theme_AppCompat_Light:I = 0x7f0e0238

.field public static final Base_V14_Theme_AppCompat_Light_Dialog:I = 0x7f0e0239

.field public static final Base_V21_Theme_AppCompat:I = 0x7f0e023a

.field public static final Base_V21_Theme_AppCompat_Dialog:I = 0x7f0e023b

.field public static final Base_V21_Theme_AppCompat_Light:I = 0x7f0e023c

.field public static final Base_V21_Theme_AppCompat_Light_Dialog:I = 0x7f0e023d

.field public static final Base_V7_Theme_AppCompat:I = 0x7f0e003d

.field public static final Base_V7_Theme_AppCompat_Dialog:I = 0x7f0e003e

.field public static final Base_V7_Theme_AppCompat_Light:I = 0x7f0e003f

.field public static final Base_Widget_AppCompat_ActionBar:I = 0x7f0e0040

.field public static final Base_Widget_AppCompat_ActionBar_Solid:I = 0x7f0e0041

.field public static final Base_Widget_AppCompat_ActionBar_TabBar:I = 0x7f0e0042

.field public static final Base_Widget_AppCompat_ActionBar_TabText:I = 0x7f0e0043

.field public static final Base_Widget_AppCompat_ActionBar_TabView:I = 0x7f0e0044

.field public static final Base_Widget_AppCompat_ActionButton:I = 0x7f0e0045

.field public static final Base_Widget_AppCompat_ActionButton_CloseMode:I = 0x7f0e0046

.field public static final Base_Widget_AppCompat_ActionButton_Overflow:I = 0x7f0e0047

.field public static final Base_Widget_AppCompat_ActionMode:I = 0x7f0e0048

.field public static final Base_Widget_AppCompat_ActivityChooserView:I = 0x7f0e0049

.field public static final Base_Widget_AppCompat_AutoCompleteTextView:I = 0x7f0e004a

.field public static final Base_Widget_AppCompat_CompoundButton_Switch:I = 0x7f0e004b

.field public static final Base_Widget_AppCompat_DrawerArrowToggle:I = 0x7f0e004c

.field public static final Base_Widget_AppCompat_DropDownItem_Spinner:I = 0x7f0e004d

.field public static final Base_Widget_AppCompat_EditText:I = 0x7f0e004e

.field public static final Base_Widget_AppCompat_Light_ActionBar:I = 0x7f0e004f

.field public static final Base_Widget_AppCompat_Light_ActionBar_Solid:I = 0x7f0e0050

.field public static final Base_Widget_AppCompat_Light_ActionBar_TabBar:I = 0x7f0e0051

.field public static final Base_Widget_AppCompat_Light_ActionBar_TabText:I = 0x7f0e0052

.field public static final Base_Widget_AppCompat_Light_ActionBar_TabText_Inverse:I = 0x7f0e0053

.field public static final Base_Widget_AppCompat_Light_ActionBar_TabView:I = 0x7f0e0054

.field public static final Base_Widget_AppCompat_Light_ActivityChooserView:I = 0x7f0e0055

.field public static final Base_Widget_AppCompat_Light_AutoCompleteTextView:I = 0x7f0e0056

.field public static final Base_Widget_AppCompat_Light_PopupMenu:I = 0x7f0e0057

.field public static final Base_Widget_AppCompat_Light_PopupMenu_Overflow:I = 0x7f0e0058

.field public static final Base_Widget_AppCompat_ListPopupWindow:I = 0x7f0e0059

.field public static final Base_Widget_AppCompat_ListView_DropDown:I = 0x7f0e005a

.field public static final Base_Widget_AppCompat_ListView_Menu:I = 0x7f0e005b

.field public static final Base_Widget_AppCompat_PopupMenu:I = 0x7f0e005c

.field public static final Base_Widget_AppCompat_PopupMenu_Overflow:I = 0x7f0e005d

.field public static final Base_Widget_AppCompat_PopupWindow:I = 0x7f0e005e

.field public static final Base_Widget_AppCompat_ProgressBar:I = 0x7f0e005f

.field public static final Base_Widget_AppCompat_ProgressBar_Horizontal:I = 0x7f0e0060

.field public static final Base_Widget_AppCompat_SearchView:I = 0x7f0e0061

.field public static final Base_Widget_AppCompat_Spinner:I = 0x7f0e0062

.field public static final Base_Widget_AppCompat_Spinner_DropDown_ActionBar:I = 0x7f0e0063

.field public static final Base_Widget_AppCompat_Toolbar:I = 0x7f0e0064

.field public static final Base_Widget_AppCompat_Toolbar_Button_Navigation:I = 0x7f0e0065

.field public static final Body:I = 0x7f0e0066

.field public static final Body_Dark:I = 0x7f0e0067

.field public static final ButtonFont:I = 0x7f0e0068

.field public static final ButtonLabel:I = 0x7f0e0069

.field public static final ButtonStyle:I = 0x7f0e006a

.field public static final ButtonStyleBlue:I = 0x7f0e006b

.field public static final ButtonStyleGray:I = 0x7f0e006c

.field public static final ButtonStyle_27505e:I = 0x7f0e006d

.field public static final ButtonStyle_search:I = 0x7f0e006e

.field public static final Controller_Font:I = 0x7f0e006f

.field public static final Copy:I = 0x7f0e0070

.field public static final Date:I = 0x7f0e0072

.field public static final DatebarDate:I = 0x7f0e0073

.field public static final DatebarTime:I = 0x7f0e0074

.field public static final DialogTheme:I = 0x7f0e0075

.field public static final H1:I = 0x7f0e0076

.field public static final H1_Dark:I = 0x7f0e0077

.field public static final H1_Edit:I = 0x7f0e0078

.field public static final H3:I = 0x7f0e0079

.field public static final H3_Dark:I = 0x7f0e007a

.field public static final ListBody:I = 0x7f0e007b

.field public static final ListLabel:I = 0x7f0e007c

.field public static final ListViewStyle:I = 0x7f0e007d

.field public static final Logo_Font:I = 0x7f0e007e

.field public static final Main_black:I = 0x7f0e007f

.field public static final Month:I = 0x7f0e0080

.field public static final NavMenuFont:I = 0x7f0e0081

.field public static final NavMenuItem:I = 0x7f0e0082

.field public static final NavMenuItemHelp:I = 0x7f0e0083

.field public static final NavMenuItemSmall:I = 0x7f0e0084

.field public static final PeelDialogButtonStyle:I = 0x7f0e0085

.field public static final PeelTheme_ActionBarStyle:I = 0x7f0e0087

.field public static final PeelTheme_ActionBar_TitleTextStyle:I = 0x7f0e0086

.field public static final PeelTheme_NoTitleBar:I = 0x7f0e0088

.field public static final PeelTheme_NoTitleBar_NoActionBar:I = 0x7f0e0089

.field public static final PeelTheme_Popup:I = 0x7f0e008a

.field public static final PeelTheme_Widget_HorizontalScrollView:I = 0x7f0e008b

.field public static final Platform_AppCompat:I = 0x7f0e008c

.field public static final Platform_AppCompat_Dialog:I = 0x7f0e008d

.field public static final Platform_AppCompat_Light:I = 0x7f0e008e

.field public static final Platform_AppCompat_Light_Dialog:I = 0x7f0e008f

.field public static final Roboto_light_italic_c3c3c3:I = 0x7f0e0090

.field public static final Roboto_light_italic_d0d0d0:I = 0x7f0e0091

.field public static final RoomOverview_divider:I = 0x7f0e0092

.field public static final RoomOverview_header_divider:I = 0x7f0e0093

.field public static final RtlOverlay_Widget_AppCompat_ActionBar_TitleItem:I = 0x7f0e0094

.field public static final RtlOverlay_Widget_AppCompat_ActionButton_CloseMode:I = 0x7f0e0095

.field public static final RtlOverlay_Widget_AppCompat_ActionButton_Overflow:I = 0x7f0e0096

.field public static final RtlOverlay_Widget_AppCompat_PopupMenuItem:I = 0x7f0e0097

.field public static final RtlOverlay_Widget_AppCompat_PopupMenuItem_InternalGroup:I = 0x7f0e0098

.field public static final RtlOverlay_Widget_AppCompat_PopupMenuItem_Text:I = 0x7f0e0099

.field public static final RtlOverlay_Widget_AppCompat_SearchView_MagIcon:I = 0x7f0e009f

.field public static final RtlOverlay_Widget_AppCompat_Search_DropDown:I = 0x7f0e009a

.field public static final RtlOverlay_Widget_AppCompat_Search_DropDown_Icon1:I = 0x7f0e009b

.field public static final RtlOverlay_Widget_AppCompat_Search_DropDown_Icon2:I = 0x7f0e009c

.field public static final RtlOverlay_Widget_AppCompat_Search_DropDown_Query:I = 0x7f0e009d

.field public static final RtlOverlay_Widget_AppCompat_Search_DropDown_Text:I = 0x7f0e009e

.field public static final SpinnerTextAppearanceSpinnerItem:I = 0x7f0e00a0

.field public static final SpinnerTheme:I = 0x7f0e00a1

.field public static final SpinnerThemeItem:I = 0x7f0e00a2

.field public static final Sub_Copy_Black:I = 0x7f0e00a3

.field public static final Sub_Copy_Lime:I = 0x7f0e00a4

.field public static final Sub_Copy_White:I = 0x7f0e00a5

.field public static final TextAppearance_AppCompat:I = 0x7f0e00a6

.field public static final TextAppearance_AppCompat_Body1:I = 0x7f0e00a7

.field public static final TextAppearance_AppCompat_Body2:I = 0x7f0e00a8

.field public static final TextAppearance_AppCompat_Button:I = 0x7f0e00a9

.field public static final TextAppearance_AppCompat_Caption:I = 0x7f0e00aa

.field public static final TextAppearance_AppCompat_Display1:I = 0x7f0e00ab

.field public static final TextAppearance_AppCompat_Display2:I = 0x7f0e00ac

.field public static final TextAppearance_AppCompat_Display3:I = 0x7f0e00ad

.field public static final TextAppearance_AppCompat_Display4:I = 0x7f0e00ae

.field public static final TextAppearance_AppCompat_Headline:I = 0x7f0e00af

.field public static final TextAppearance_AppCompat_Inverse:I = 0x7f0e00b0

.field public static final TextAppearance_AppCompat_Large:I = 0x7f0e00b1

.field public static final TextAppearance_AppCompat_Large_Inverse:I = 0x7f0e00b2

.field public static final TextAppearance_AppCompat_Light_SearchResult_Subtitle:I = 0x7f0e00b3

.field public static final TextAppearance_AppCompat_Light_SearchResult_Title:I = 0x7f0e00b4

.field public static final TextAppearance_AppCompat_Light_Widget_PopupMenu_Large:I = 0x7f0e00b5

.field public static final TextAppearance_AppCompat_Light_Widget_PopupMenu_Small:I = 0x7f0e00b6

.field public static final TextAppearance_AppCompat_Medium:I = 0x7f0e00b7

.field public static final TextAppearance_AppCompat_Medium_Inverse:I = 0x7f0e00b8

.field public static final TextAppearance_AppCompat_Menu:I = 0x7f0e00b9

.field public static final TextAppearance_AppCompat_SearchResult_Subtitle:I = 0x7f0e00ba

.field public static final TextAppearance_AppCompat_SearchResult_Title:I = 0x7f0e00bb

.field public static final TextAppearance_AppCompat_Small:I = 0x7f0e00bc

.field public static final TextAppearance_AppCompat_Small_Inverse:I = 0x7f0e00bd

.field public static final TextAppearance_AppCompat_Subhead:I = 0x7f0e00be

.field public static final TextAppearance_AppCompat_Subhead_Inverse:I = 0x7f0e00bf

.field public static final TextAppearance_AppCompat_Title:I = 0x7f0e00c0

.field public static final TextAppearance_AppCompat_Title_Inverse:I = 0x7f0e00c1

.field public static final TextAppearance_AppCompat_Widget_ActionBar_Menu:I = 0x7f0e00c2

.field public static final TextAppearance_AppCompat_Widget_ActionBar_Subtitle:I = 0x7f0e00c3

.field public static final TextAppearance_AppCompat_Widget_ActionBar_Subtitle_Inverse:I = 0x7f0e00c4

.field public static final TextAppearance_AppCompat_Widget_ActionBar_Title:I = 0x7f0e00c5

.field public static final TextAppearance_AppCompat_Widget_ActionBar_Title_Inverse:I = 0x7f0e00c6

.field public static final TextAppearance_AppCompat_Widget_ActionMode_Subtitle:I = 0x7f0e00c7

.field public static final TextAppearance_AppCompat_Widget_ActionMode_Subtitle_Inverse:I = 0x7f0e00c8

.field public static final TextAppearance_AppCompat_Widget_ActionMode_Title:I = 0x7f0e00c9

.field public static final TextAppearance_AppCompat_Widget_ActionMode_Title_Inverse:I = 0x7f0e00ca

.field public static final TextAppearance_AppCompat_Widget_DropDownItem:I = 0x7f0e00cb

.field public static final TextAppearance_AppCompat_Widget_PopupMenu_Large:I = 0x7f0e00cc

.field public static final TextAppearance_AppCompat_Widget_PopupMenu_Small:I = 0x7f0e00cd

.field public static final TextAppearance_AppCompat_Widget_Switch:I = 0x7f0e00ce

.field public static final TextAppearance_TabPageIndicator:I = 0x7f0e00cf

.field public static final TextAppearance_Widget_AppCompat_ExpandedMenu_Item:I = 0x7f0e00d0

.field public static final TextAppearance_Widget_AppCompat_Toolbar_Subtitle:I = 0x7f0e00d1

.field public static final TextAppearance_Widget_AppCompat_Toolbar_Title:I = 0x7f0e00d2

.field public static final ThemeOverlay_AppCompat:I = 0x7f0e00de

.field public static final ThemeOverlay_AppCompat_ActionBar:I = 0x7f0e00df

.field public static final ThemeOverlay_AppCompat_Dark:I = 0x7f0e00e0

.field public static final ThemeOverlay_AppCompat_Dark_ActionBar:I = 0x7f0e00e1

.field public static final ThemeOverlay_AppCompat_Light:I = 0x7f0e00e2

.field public static final Theme_AppCompat:I = 0x7f0e00d3

.field public static final Theme_AppCompat_CompactMenu:I = 0x7f0e00d4

.field public static final Theme_AppCompat_Dialog:I = 0x7f0e00d5

.field public static final Theme_AppCompat_DialogWhenLarge:I = 0x7f0e00d6

.field public static final Theme_AppCompat_Light:I = 0x7f0e00d7

.field public static final Theme_AppCompat_Light_DarkActionBar:I = 0x7f0e00d8

.field public static final Theme_AppCompat_Light_Dialog:I = 0x7f0e00d9

.field public static final Theme_AppCompat_Light_DialogWhenLarge:I = 0x7f0e00da

.field public static final Theme_AppCompat_Light_NoActionBar:I = 0x7f0e00db

.field public static final Theme_AppCompat_NoActionBar:I = 0x7f0e00dc

.field public static final Theme_PageIndicatorDefaults:I = 0x7f0e00dd

.field public static final TileBarBase_tile_bar:I = 0x7f0e00e3

.field public static final TileBarHeader:I = 0x7f0e00e4

.field public static final TileBarHeaderPersonalize:I = 0x7f0e00e5

.field public static final TileBarHeader_tile_bar:I = 0x7f0e00e6

.field public static final TileCaptionStyle:I = 0x7f0e00e7

.field public static final TitleLabel:I = 0x7f0e00e8

.field public static final TwoWayGridView_tile_bar:I = 0x7f0e00e9

.field public static final Widget:I = 0x7f0e00ea

.field public static final Widget_AppCompat_ActionBar:I = 0x7f0e00eb

.field public static final Widget_AppCompat_ActionBar_Solid:I = 0x7f0e00ec

.field public static final Widget_AppCompat_ActionBar_TabBar:I = 0x7f0e00ed

.field public static final Widget_AppCompat_ActionBar_TabText:I = 0x7f0e00ee

.field public static final Widget_AppCompat_ActionBar_TabView:I = 0x7f0e00ef

.field public static final Widget_AppCompat_ActionButton:I = 0x7f0e00f0

.field public static final Widget_AppCompat_ActionButton_CloseMode:I = 0x7f0e00f1

.field public static final Widget_AppCompat_ActionButton_Overflow:I = 0x7f0e00f2

.field public static final Widget_AppCompat_ActionMode:I = 0x7f0e00f3

.field public static final Widget_AppCompat_ActivityChooserView:I = 0x7f0e00f4

.field public static final Widget_AppCompat_AutoCompleteTextView:I = 0x7f0e00f5

.field public static final Widget_AppCompat_CompoundButton_Switch:I = 0x7f0e00f6

.field public static final Widget_AppCompat_DrawerArrowToggle:I = 0x7f0e00f7

.field public static final Widget_AppCompat_DropDownItem_Spinner:I = 0x7f0e00f8

.field public static final Widget_AppCompat_EditText:I = 0x7f0e00f9

.field public static final Widget_AppCompat_Light_ActionBar:I = 0x7f0e00fa

.field public static final Widget_AppCompat_Light_ActionBar_Solid:I = 0x7f0e00fb

.field public static final Widget_AppCompat_Light_ActionBar_Solid_Inverse:I = 0x7f0e00fc

.field public static final Widget_AppCompat_Light_ActionBar_TabBar:I = 0x7f0e00fd

.field public static final Widget_AppCompat_Light_ActionBar_TabBar_Inverse:I = 0x7f0e00fe

.field public static final Widget_AppCompat_Light_ActionBar_TabText:I = 0x7f0e00ff

.field public static final Widget_AppCompat_Light_ActionBar_TabText_Inverse:I = 0x7f0e0100

.field public static final Widget_AppCompat_Light_ActionBar_TabView:I = 0x7f0e0101

.field public static final Widget_AppCompat_Light_ActionBar_TabView_Inverse:I = 0x7f0e0102

.field public static final Widget_AppCompat_Light_ActionButton:I = 0x7f0e0103

.field public static final Widget_AppCompat_Light_ActionButton_CloseMode:I = 0x7f0e0104

.field public static final Widget_AppCompat_Light_ActionButton_Overflow:I = 0x7f0e0105

.field public static final Widget_AppCompat_Light_ActionMode_Inverse:I = 0x7f0e0106

.field public static final Widget_AppCompat_Light_ActivityChooserView:I = 0x7f0e0107

.field public static final Widget_AppCompat_Light_AutoCompleteTextView:I = 0x7f0e0108

.field public static final Widget_AppCompat_Light_DropDownItem_Spinner:I = 0x7f0e0109

.field public static final Widget_AppCompat_Light_ListPopupWindow:I = 0x7f0e010a

.field public static final Widget_AppCompat_Light_ListView_DropDown:I = 0x7f0e010b

.field public static final Widget_AppCompat_Light_PopupMenu:I = 0x7f0e010c

.field public static final Widget_AppCompat_Light_PopupMenu_Overflow:I = 0x7f0e010d

.field public static final Widget_AppCompat_Light_SearchView:I = 0x7f0e010e

.field public static final Widget_AppCompat_Light_Spinner_DropDown_ActionBar:I = 0x7f0e010f

.field public static final Widget_AppCompat_ListPopupWindow:I = 0x7f0e0110

.field public static final Widget_AppCompat_ListView_DropDown:I = 0x7f0e0111

.field public static final Widget_AppCompat_ListView_Menu:I = 0x7f0e0112

.field public static final Widget_AppCompat_PopupMenu:I = 0x7f0e0113

.field public static final Widget_AppCompat_PopupMenu_Overflow:I = 0x7f0e0114

.field public static final Widget_AppCompat_PopupWindow:I = 0x7f0e0115

.field public static final Widget_AppCompat_ProgressBar:I = 0x7f0e0116

.field public static final Widget_AppCompat_ProgressBar_Horizontal:I = 0x7f0e0117

.field public static final Widget_AppCompat_SearchView:I = 0x7f0e0118

.field public static final Widget_AppCompat_Spinner:I = 0x7f0e0119

.field public static final Widget_AppCompat_Spinner_DropDown:I = 0x7f0e011a

.field public static final Widget_AppCompat_Spinner_DropDown_ActionBar:I = 0x7f0e011b

.field public static final Widget_AppCompat_Toolbar:I = 0x7f0e011c

.field public static final Widget_AppCompat_Toolbar_Button_Navigation:I = 0x7f0e011d

.field public static final Widget_IconPageIndicator:I = 0x7f0e011e

.field public static final Widget_PeelTabPageIndicator:I = 0x7f0e011f

.field public static final Widget_TabPageIndicator:I = 0x7f0e0120

.field public static final about_appversion:I = 0x7f0e0121

.field public static final actionbar_menu_overflow:I = 0x7f0e0122

.field public static final actionbar_next:I = 0x7f0e0123

.field public static final card_view_bottom_bar_img:I = 0x7f0e0124

.field public static final card_view_datetime_txt_480:I = 0x7f0e0125

.field public static final card_view_datetime_txt_540:I = 0x7f0e0126

.field public static final card_view_datetime_txt_720:I = 0x7f0e0127

.field public static final card_view_title_txt_720:I = 0x7f0e0128

.field public static final card_view_txt_480:I = 0x7f0e0129

.field public static final card_view_txt_540:I = 0x7f0e012a

.field public static final card_view_txt_720:I = 0x7f0e012b

.field public static final channel_3_char:I = 0x7f0e012c

.field public static final channel_4_char:I = 0x7f0e012d

.field public static final channel_5_char:I = 0x7f0e012e

.field public static final com_facebook_loginview_default_style:I = 0x7f0e012f

.field public static final com_facebook_loginview_silver_style:I = 0x7f0e0130

.field public static final configure_activity_text:I = 0x7f0e0131

.field public static final configure_activity_text_sub:I = 0x7f0e0132

.field public static final configure_activity_view_divider:I = 0x7f0e0133

.field public static final control_power_text:I = 0x7f0e0134

.field public static final controlpad_new_iv_close:I = 0x7f0e0135

.field public static final controlpad_new_message_popup:I = 0x7f0e0136

.field public static final controlpad_new_message_popup_tv:I = 0x7f0e0137

.field public static final customDialog:I = 0x7f0e0138

.field public static final device_add_row_icon:I = 0x7f0e0139

.field public static final device_add_row_text:I = 0x7f0e013a

.field public static final device_list_divider:I = 0x7f0e013b

.field public static final device_power_row_text:I = 0x7f0e013c

.field public static final device_remote_activity_text1:I = 0x7f0e013d

.field public static final device_setup_search:I = 0x7f0e013e

.field public static final device_setup_search_icon:I = 0x7f0e013f

.field public static final device_setup_search_layout:I = 0x7f0e0140

.field public static final device_type_selection_listview:I = 0x7f0e0141

.field public static final dial_number_btn:I = 0x7f0e0142

.field public static final dial_number_btn_jp:I = 0x7f0e0143

.field public static final dial_number_btn_jp_modes4:I = 0x7f0e0144

.field public static final dlv_list_divider:I = 0x7f0e0145

.field public static final dlv_list_text_item_text:I = 0x7f0e0146

.field public static final dlv_view_divider:I = 0x7f0e0147

.field public static final editViewStyle:I = 0x7f0e0148

.field public static final edit_channel_list_instructions_text:I = 0x7f0e0149

.field public static final edit_channel_list_instructions_text_common:I = 0x7f0e014a

.field public static final edit_channel_list_view:I = 0x7f0e014b

.field public static final edit_channel_option_row_bg:I = 0x7f0e014c

.field public static final edit_channel_option_row_text:I = 0x7f0e014d

.field public static final edit_channel_option_row_text_sub:I = 0x7f0e014e

.field public static final fav_channel_search:I = 0x7f0e014f

.field public static final fav_channel_search_search_icon:I = 0x7f0e0150

.field public static final fav_channel_search_search_setup:I = 0x7f0e0151

.field public static final favoites_channel_program_search_row_text:I = 0x7f0e0152

.field public static final favoites_channel_program_search_row_title:I = 0x7f0e0153

.field public static final favorites_add_channel__bg:I = 0x7f0e0154

.field public static final favorites_add_channel_text:I = 0x7f0e0155

.field public static final favorites_channels_row_bg:I = 0x7f0e0156

.field public static final favorites_edit_channel_listview_bg:I = 0x7f0e0157

.field public static final favorites_empty_channel_text:I = 0x7f0e0158

.field public static final favorites_l1_list_item_ls6:I = 0x7f0e0159

.field public static final favorites_list_divider:I = 0x7f0e015a

.field public static final favorites_pager_bg:I = 0x7f0e015b

.field public static final favorites_show_list_divider:I = 0x7f0e015c

.field public static final favorites_shows_TileBarHeader:I = 0x7f0e015d

.field public static final favorites_shows_empty_list_text:I = 0x7f0e015e

.field public static final favorites_shows_frame_layout_bg:I = 0x7f0e015f

.field public static final h1_settings:I = 0x7f0e0160

.field public static final h1_windowdialog:I = 0x7f0e0161

.field public static final h2_NavMenuItem:I = 0x7f0e0162

.field public static final h2_settings_header:I = 0x7f0e0163

.field public static final h2_settings_header_room:I = 0x7f0e0164

.field public static final h2_visualprogram:I = 0x7f0e0165

.field public static final h3_sendfeedback:I = 0x7f0e0166

.field public static final hb_visualprogram:I = 0x7f0e0167

.field public static final header_h1_text:I = 0x7f0e0168

.field public static final header_image_divider_bar:I = 0x7f0e0169

.field public static final hs2_h2_list_title:I = 0x7f0e016a

.field public static final ir_learning_list:I = 0x7f0e016b

.field public static final ir_learning_row_btn:I = 0x7f0e016c

.field public static final ir_learning_row_learn_btn:I = 0x7f0e016d

.field public static final ir_learning_row_test_btn:I = 0x7f0e016e

.field public static final ir_learning_row_text:I = 0x7f0e016f

.field public static final l1_list_item:I = 0x7f0e0171

.field public static final l1_list_item_ls6:I = 0x7f0e0172

.field public static final l1_list_item_sub_ls6:I = 0x7f0e0173

.field public static final l1_list_item_sub_ls6_black:I = 0x7f0e0174

.field public static final l1_list_sub_item:I = 0x7f0e0175

.field public static final l1_list_text_item:I = 0x7f0e0176

.field public static final l1_sendfeedback:I = 0x7f0e0177

.field public static final l1_settings:I = 0x7f0e0178

.field public static final l3_l1_list_item:I = 0x7f0e0179

.field public static final l3_l2_list_item:I = 0x7f0e017a

.field public static final l3_visualprogram:I = 0x7f0e017b

.field public static final l6_l1_list_item:I = 0x7f0e017c

.field public static final l6_settings_brandListRow:I = 0x7f0e017d

.field public static final list_divider_0_4:I = 0x7f0e017e

.field public static final list_divider_0_5:I = 0x7f0e017f

.field public static final list_header_txt_480:I = 0x7f0e0180

.field public static final list_header_txt_540:I = 0x7f0e0181

.field public static final list_header_txt_720:I = 0x7f0e0182

.field public static final listitem_header_style:I = 0x7f0e0183

.field public static final mc_NavMenuItem:I = 0x7f0e0184

.field public static final menu_item_font:I = 0x7f0e0185

.field public static final notifications_10dp_shadow:I = 0x7f0e0186

.field public static final notifications_winset_18sp:I = 0x7f0e0187

.field public static final notifications_winset_18sp_shadow:I = 0x7f0e0188

.field public static final p2_visualprogram:I = 0x7f0e0189

.field public static final p_Copy:I = 0x7f0e018a

.field public static final p_NavMenuItem:I = 0x7f0e018b

.field public static final p_sendfeedback:I = 0x7f0e018c

.field public static final p_settings:I = 0x7f0e018d

.field public static final p_settings_white:I = 0x7f0e018e

.field public static final p_windowdialog:I = 0x7f0e018f

.field public static final plus_sign_image:I = 0x7f0e0190

.field public static final plus_sign_image_for_black_bg:I = 0x7f0e0191

.field public static final popup_button:I = 0x7f0e0192

.field public static final popup_device_row_text:I = 0x7f0e0193

.field public static final popup_dialog_alertTitle:I = 0x7f0e0194

.field public static final popup_dialog_bg:I = 0x7f0e0195

.field public static final popup_dialog_divider_image:I = 0x7f0e0196

.field public static final popup_dialog_list_divider:I = 0x7f0e0197

.field public static final popup_dialog_list_item_image:I = 0x7f0e0198

.field public static final popup_dialog_title_ll:I = 0x7f0e0199

.field public static final popup_edit:I = 0x7f0e019a

.field public static final popup_list_divider:I = 0x7f0e019b

.field public static final region_spinner_list_item:I = 0x7f0e019c

.field public static final reminder_button:I = 0x7f0e019d

.field public static final reminder_checkbox_text:I = 0x7f0e019e

.field public static final reminder_copy_dialog:I = 0x7f0e019f

.field public static final reminder_header_text:I = 0x7f0e01a0

.field public static final reminder_item:I = 0x7f0e01a1

.field public static final reminder_list_divider_view:I = 0x7f0e01a2

.field public static final reminder_show_title_text:I = 0x7f0e01a3

.field public static final remote_color_btn_linear:I = 0x7f0e01a4

.field public static final remote_device_textview:I = 0x7f0e01a5

.field public static final remote_dial_linear:I = 0x7f0e01a6

.field public static final remote_mid_space:I = 0x7f0e01a7

.field public static final remote_more_btn:I = 0x7f0e01a8

.field public static final remote_more_linear:I = 0x7f0e01a9

.field public static final remote_more_samll_btn:I = 0x7f0e01aa

.field public static final remote_panel:I = 0x7f0e01ab

.field public static final remote_sub_color_btn:I = 0x7f0e01ac

.field public static final remote_sub_color_stb_btn:I = 0x7f0e01ad

.field public static final remote_up_inc:I = 0x7f0e01ae

.field public static final res_lang_row_text:I = 0x7f0e01af

.field public static final resume_episode_title_txt_480:I = 0x7f0e01b0

.field public static final resume_episode_title_txt_540:I = 0x7f0e01b1

.field public static final resume_episode_title_txt_720:I = 0x7f0e01b2

.field public static final resume_show_title_txt_480:I = 0x7f0e01b3

.field public static final resume_show_title_txt_540:I = 0x7f0e01b4

.field public static final resume_show_title_txt_720:I = 0x7f0e01b5

.field public static final resume_txt_480:I = 0x7f0e01b6

.field public static final resume_txt_540:I = 0x7f0e01b7

.field public static final resume_txt_720:I = 0x7f0e01b8

.field public static final roboto_8e8e8e:I = 0x7f0e01b9

.field public static final roboto_Light_b4b4b4_font12:I = 0x7f0e01ba

.field public static final roboto_Light_bold_white_font17:I = 0x7f0e01bb

.field public static final roboto_Light_c8c8c8:I = 0x7f0e01bc

.field public static final roboto_Light_country_font18:I = 0x7f0e01bd

.field public static final roboto_Light_dcdcdc:I = 0x7f0e01be

.field public static final roboto_Light_white_font12:I = 0x7f0e01bf

.field public static final roboto_Medium_white_font13:I = 0x7f0e01c0

.field public static final roboto_Regular_7d7d7d_font10:I = 0x7f0e01c1

.field public static final roboto_Regular_7d7d7d_font11:I = 0x7f0e01c2

.field public static final roboto_Regular_Light_aaaaaa:I = 0x7f0e01c3

.field public static final roboto_Regular_Regular_aaaaaa:I = 0x7f0e01c4

.field public static final roboto_Regular_a8a8a8:I = 0x7f0e01c5

.field public static final roboto_Regular_bold_262626_font16:I = 0x7f0e01c6

.field public static final roboto_Regular_bold_a6a6a6_font11:I = 0x7f0e01c7

.field public static final roboto_Regular_bold_aaaaaa:I = 0x7f0e01c8

.field public static final roboto_Regular_bold_c61b00:I = 0x7f0e01c9

.field public static final roboto_Regular_white:I = 0x7f0e01ca

.field public static final roboto_Regular_white_font18:I = 0x7f0e01cb

.field public static final roboto_Regular_white_italic:I = 0x7f0e01cc

.field public static final roboto_bold_252525_italic:I = 0x7f0e01cd

.field public static final roboto_italic_black:I = 0x7f0e01ce

.field public static final roboto_light_ffffff:I = 0x7f0e01cf

.field public static final roboto_light_italic:I = 0x7f0e01d0

.field public static final roboto_light_italic_04b7ea:I = 0x7f0e01d1

.field public static final roboto_light_italic_ffffff:I = 0x7f0e01d2

.field public static final roboto_light_white:I = 0x7f0e01d3

.field public static final roboto_light_white_italic:I = 0x7f0e01d4

.field public static final roboto_regula_5e5e5e:I = 0x7f0e01d5

.field public static final roboto_regular_04b7ea:I = 0x7f0e01d6

.field public static final roboto_regular_252525:I = 0x7f0e01d7

.field public static final roboto_regular_252525_header_italic:I = 0x7f0e01d8

.field public static final roboto_regular_b4b4b4_italic:I = 0x7f0e01d9

.field public static final roboto_regular_bold_white_font20:I = 0x7f0e01da

.field public static final roboto_regular_e4e4e4:I = 0x7f0e01db

.field public static final roboto_regular_white_font22:I = 0x7f0e01dc

.field public static final room_add_row_text:I = 0x7f0e01dd

.field public static final search_base_ssr:I = 0x7f0e01de

.field public static final search_icon_default:I = 0x7f0e01df

.field public static final search_item_desc:I = 0x7f0e01e0

.field public static final search_item_title:I = 0x7f0e01e1

.field public static final search_layout_default:I = 0x7f0e01e2

.field public static final search_list:I = 0x7f0e01e3

.field public static final search_row:I = 0x7f0e01e4

.field public static final search_text_listTilte:I = 0x7f0e01e5

.field public static final setting_row_text:I = 0x7f0e01e6

.field public static final setting_single_selection_txt:I = 0x7f0e01e7

.field public static final settings_add_room_edit_text:I = 0x7f0e01e8

.field public static final settings_add_room_view_divider:I = 0x7f0e01e9

.field public static final settings_add_room_view_text_and_image:I = 0x7f0e01ea

.field public static final settings_age_gender_text:I = 0x7f0e01eb

.field public static final settings_brand_row_text:I = 0x7f0e01ec

.field public static final settings_dlv_title_text:I = 0x7f0e01ed

.field public static final settings_header_row_divider:I = 0x7f0e01ee

.field public static final settings_liked_programs_list_divider:I = 0x7f0e01ef

.field public static final settings_liked_programs_text:I = 0x7f0e01f0

.field public static final settings_list_divider:I = 0x7f0e01f1

.field public static final settings_login_text:I = 0x7f0e01f2

.field public static final settings_room_overview_text:I = 0x7f0e01f3

.field public static final settings_room_overview_text_rm:I = 0x7f0e01f4

.field public static final settings_single_selection_row:I = 0x7f0e01f5

.field public static final settings_view_divider:I = 0x7f0e01f6

.field public static final setup_country_list:I = 0x7f0e01f7

.field public static final setup_country_search_icon:I = 0x7f0e01f8

.field public static final setup_country_search_layout:I = 0x7f0e01f9

.field public static final setup_main_country_list:I = 0x7f0e01fa

.field public static final setup_main_search_icon:I = 0x7f0e01fb

.field public static final setup_main_selection_customspinner:I = 0x7f0e01fc

.field public static final setup_main_selection_search_layout:I = 0x7f0e01fd

.field public static final setup_main_selection_title_setup_region:I = 0x7f0e01fe

.field public static final setup_main_selection_zipcode_txt:I = 0x7f0e01ff

.field public static final setup_splash:I = 0x7f0e0200

.field public static final setup_test_bottom_btn_bg:I = 0x7f0e0201

.field public static final setup_test_btn:I = 0x7f0e0202

.field public static final setup_test_tv_turn_on_msg:I = 0x7f0e0203

.field public static final setup_zip_help_txt:I = 0x7f0e0204

.field public static final setup_zip_list_divider:I = 0x7f0e0205

.field public static final setup_zip_zipcode_txt:I = 0x7f0e0206

.field public static final show_details_card__view_bg:I = 0x7f0e0207

.field public static final show_details_items_divider:I = 0x7f0e0208

.field public static final show_details_more_like_this_bg:I = 0x7f0e0209

.field public static final show_details_more_like_this_item_text:I = 0x7f0e020a

.field public static final show_details_on_demand_season_spinner:I = 0x7f0e020b

.field public static final show_details_overview_TileBarHeader:I = 0x7f0e020c

.field public static final show_details_overview_more_text:I = 0x7f0e020d

.field public static final show_details_overview_section_synopsis_bg:I = 0x7f0e020e

.field public static final show_details_watchon_divider:I = 0x7f0e020f

.field public static final show_details_watchon_divider_list_divider:I = 0x7f0e0210

.field public static final show_details_watchon_divider_vertical:I = 0x7f0e0211

.field public static final show_details_watchon_item_text:I = 0x7f0e0212

.field public static final show_details_watchon_item_text_body:I = 0x7f0e0213

.field public static final slideout_help_text:I = 0x7f0e0214

.field public static final slideout_menu_search:I = 0x7f0e0215

.field public static final slideout_menu_selected_bar:I = 0x7f0e0216

.field public static final spinnerStyle:I = 0x7f0e0217

.field public static final ss_tiles_with_pager:I = 0x7f0e0218

.field public static final ss_tiles_with_pager_pager:I = 0x7f0e0219

.field public static final st_visualprogram:I = 0x7f0e021a

.field public static final sub_remote_btn_text:I = 0x7f0e021b

.field public static final sub_remote_btn_text_extra:I = 0x7f0e021c

.field public static final synopsis_Body:I = 0x7f0e021d

.field public static final synopsis_Body_Dark:I = 0x7f0e021e

.field public static final synopsis_H1:I = 0x7f0e021f

.field public static final synopsis_H1_Dark:I = 0x7f0e0220

.field public static final synopsis_H1_dialog:I = 0x7f0e0221

.field public static final text_small:I = 0x7f0e0222

.field public static final tile_view_click_state_more_btn:I = 0x7f0e0223

.field public static final time_header_row_divider:I = 0x7f0e0224

.field public static final tooltip_bubble_text:I = 0x7f0e0225

.field public static final tv_remotecontrol_button_caption:I = 0x7f0e0226

.field public static final tv_remotecontrol_button_caption_13:I = 0x7f0e0227

.field public static final tv_remotecontrol_button_input_15:I = 0x7f0e0228

.field public static final twitter_btn_text:I = 0x7f0e0229

.field public static final twitter_h1:I = 0x7f0e022a

.field public static final twitter_h1_username:I = 0x7f0e022b

.field public static final twitter_input_h1:I = 0x7f0e022c

.field public static final twitter_tags:I = 0x7f0e022d

.field public static final twitter_tags_label:I = 0x7f0e022e

.field public static final view_divider_0_4:I = 0x7f0e022f

.field public static final view_divider_0_5:I = 0x7f0e0230

.field public static final widget_device_title:I = 0x7f0e0231
