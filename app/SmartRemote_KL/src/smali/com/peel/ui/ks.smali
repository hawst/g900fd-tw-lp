.class Lcom/peel/ui/ks;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/jess/ui/z;


# instance fields
.field final synthetic a:Lcom/peel/ui/kr;


# direct methods
.method constructor <init>(Lcom/peel/ui/kr;)V
    .locals 0

    .prologue
    .line 89
    iput-object p1, p0, Lcom/peel/ui/ks;->a:Lcom/peel/ui/kr;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/jess/ui/v;Landroid/view/View;IJ)V
    .locals 26
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/jess/ui/v",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 92
    sget-object v4, Lcom/peel/content/a;->h:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v4}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v4

    if-eqz v4, :cond_0

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/peel/ui/ks;->a:Lcom/peel/ui/kr;

    iget-object v4, v4, Lcom/peel/ui/kr;->b:Lcom/peel/d/i;

    if-nez v4, :cond_1

    .line 322
    :cond_0
    :goto_0
    return-void

    .line 93
    :cond_1
    new-instance v4, Landroid/content/Intent;

    const-string/jumbo v5, "dismiss_toast"

    invoke-direct {v4, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 94
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/peel/ui/ks;->a:Lcom/peel/ui/kr;

    invoke-virtual {v5}, Lcom/peel/ui/kr;->m()Landroid/support/v4/app/ae;

    move-result-object v5

    invoke-virtual {v5}, Landroid/support/v4/app/ae;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5}, Landroid/support/v4/a/q;->a(Landroid/content/Context;)Landroid/support/v4/a/q;

    move-result-object v5

    invoke-virtual {v5, v4}, Landroid/support/v4/a/q;->a(Landroid/content/Intent;)Z

    .line 96
    new-instance v20, Landroid/os/Bundle;

    invoke-direct/range {v20 .. v20}, Landroid/os/Bundle;-><init>()V

    .line 97
    invoke-virtual/range {p1 .. p1}, Lcom/jess/ui/v;->getAdapter()Landroid/widget/Adapter;

    move-result-object v4

    check-cast v4, Lcom/peel/ui/ka;

    move/from16 v0, p3

    invoke-virtual {v4, v0}, Lcom/peel/ui/ka;->getItem(I)Ljava/lang/Object;

    move-result-object v4

    move-object v14, v4

    check-cast v14, [Lcom/peel/content/listing/Listing;

    move-object/from16 v15, p2

    .line 99
    check-cast v15, Landroid/widget/FrameLayout;

    .line 100
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/peel/ui/ks;->a:Lcom/peel/ui/kr;

    invoke-static {v4}, Lcom/peel/ui/kr;->a(Lcom/peel/ui/kr;)Landroid/view/View;

    move-result-object v4

    if-eqz v4, :cond_2

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/peel/ui/ks;->a:Lcom/peel/ui/kr;

    invoke-static {v4}, Lcom/peel/ui/kr;->a(Lcom/peel/ui/kr;)Landroid/view/View;

    move-result-object v4

    const-string/jumbo v5, "overlay"

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewWithTag(Ljava/lang/Object;)Landroid/view/View;

    move-result-object v4

    if-eqz v4, :cond_2

    .line 101
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/peel/ui/ks;->a:Lcom/peel/ui/kr;

    invoke-static {v4}, Lcom/peel/ui/kr;->a(Lcom/peel/ui/kr;)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/FrameLayout;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/peel/ui/ks;->a:Lcom/peel/ui/kr;

    invoke-static {v5}, Lcom/peel/ui/kr;->a(Lcom/peel/ui/kr;)Landroid/view/View;

    move-result-object v5

    const-string/jumbo v6, "overlay"

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewWithTag(Ljava/lang/Object;)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/FrameLayout;->removeView(Landroid/view/View;)V

    .line 103
    :cond_2
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/peel/ui/ks;->a:Lcom/peel/ui/kr;

    move-object/from16 v0, p2

    invoke-static {v4, v0}, Lcom/peel/ui/kr;->a(Lcom/peel/ui/kr;Landroid/view/View;)Landroid/view/View;

    .line 104
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/peel/ui/ks;->a:Lcom/peel/ui/kr;

    invoke-virtual {v4}, Lcom/peel/ui/kr;->m()Landroid/support/v4/app/ae;

    move-result-object v4

    invoke-virtual {v4}, Landroid/support/v4/app/ae;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    const-string/jumbo v5, "tuneinoptions"

    invoke-static {v4, v5}, Lcom/peel/util/dq;->c(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    .line 106
    const-string/jumbo v4, "overlay"

    invoke-virtual {v15, v4}, Landroid/widget/FrameLayout;->findViewWithTag(Ljava/lang/Object;)Landroid/view/View;

    move-result-object v4

    if-nez v4, :cond_0

    .line 109
    if-eqz v14, :cond_0

    .line 111
    const/4 v4, 0x0

    aget-object v16, v14, v4

    .line 113
    move-object/from16 v0, v16

    instance-of v4, v0, Lcom/peel/content/listing/LiveListing;

    if-eqz v4, :cond_a

    move-object/from16 v4, v16

    check-cast v4, Lcom/peel/content/listing/LiveListing;

    invoke-virtual {v4}, Lcom/peel/content/listing/LiveListing;->o()J

    move-result-wide v4

    move-wide/from16 v18, v4

    .line 114
    :goto_1
    invoke-virtual/range {v16 .. v16}, Lcom/peel/content/listing/Listing;->j()J

    move-result-wide v4

    .line 115
    add-long v22, v18, v4

    .line 117
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v24

    .line 118
    if-eqz v14, :cond_3

    const-string/jumbo v4, "selected"

    const/4 v5, 0x0

    aget-object v5, v14, v5

    invoke-virtual {v5}, Lcom/peel/content/listing/Listing;->f()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v20

    invoke-virtual {v0, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 119
    :cond_3
    invoke-virtual/range {p1 .. p1}, Lcom/jess/ui/v;->getTag()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/os/Bundle;

    move-object/from16 v0, v20

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->putAll(Landroid/os/Bundle;)V

    .line 122
    const-string/jumbo v4, "comparator"

    const-class v5, Lcom/peel/content/listing/c;

    invoke-virtual {v5}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v20

    invoke-virtual {v0, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 125
    const-string/jumbo v4, "start"

    move-object/from16 v0, v20

    move-wide/from16 v1, v18

    invoke-virtual {v0, v4, v1, v2}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 126
    const-string/jumbo v4, "end"

    move-object/from16 v0, v20

    move-wide/from16 v1, v22

    invoke-virtual {v0, v4, v1, v2}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 129
    const/16 v17, -0x1

    .line 131
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/peel/ui/ks;->a:Lcom/peel/ui/kr;

    iget-object v4, v4, Lcom/peel/ui/kr;->c:Landroid/os/Bundle;

    if-eqz v4, :cond_c

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/peel/ui/ks;->a:Lcom/peel/ui/kr;

    iget-object v4, v4, Lcom/peel/ui/kr;->c:Landroid/os/Bundle;

    const-string/jumbo v5, "context_id"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_c

    .line 132
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/peel/ui/ks;->a:Lcom/peel/ui/kr;

    iget-object v4, v4, Lcom/peel/ui/kr;->c:Landroid/os/Bundle;

    const-string/jumbo v5, "context_id"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v7

    .line 133
    const-string/jumbo v4, "context_id"

    move-object/from16 v0, v20

    invoke-virtual {v0, v4, v7}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 137
    invoke-static {}, Lcom/peel/util/a/f;->a()Lcom/peel/util/a/f;

    move-result-object v4

    sget-object v5, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v5}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v5

    if-nez v5, :cond_b

    const/4 v5, 0x1

    :goto_2
    const/16 v6, 0x4b1

    const-string/jumbo v8, "category"

    .line 139
    move-object/from16 v0, v20

    invoke-virtual {v0, v8}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-static/range {v16 .. v16}, Lcom/peel/util/bx;->b(Lcom/peel/content/listing/Listing;)Ljava/lang/String;

    move-result-object v10

    const/4 v11, 0x0

    invoke-virtual/range {v16 .. v16}, Lcom/peel/content/listing/Listing;->h()Ljava/lang/String;

    move-result-object v12

    const/4 v13, 0x0

    move/from16 v9, p3

    .line 137
    invoke-virtual/range {v4 .. v13}, Lcom/peel/util/a/f;->a(IIILjava/lang/String;ILjava/lang/String;ILjava/lang/String;I)V

    .line 150
    :goto_3
    cmp-long v4, v24, v18

    if-lez v4, :cond_e

    cmp-long v4, v24, v22

    if-gez v4, :cond_e

    .line 151
    sget v4, Lcom/peel/ui/fq;->tunein_overlay:I

    move v5, v4

    .line 159
    :goto_4
    const/4 v4, -0x1

    if-le v5, v4, :cond_0

    .line 161
    sget-object v4, Lcom/peel/util/bx;->d:Ljava/util/Map;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v4, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    move-object v13, v4

    check-cast v13, Landroid/view/View;

    .line 162
    const/4 v11, 0x0

    .line 163
    const/4 v12, 0x0

    .line 165
    sget-object v4, Lcom/peel/content/a;->e:Ljava/lang/String;

    const-string/jumbo v6, "CN"

    invoke-virtual {v4, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_10

    .line 166
    const/4 v4, 0x0

    aget-object v4, v14, v4

    invoke-static {v4}, Lcom/peel/util/bx;->d(Lcom/peel/content/listing/Listing;)Ljava/lang/String;

    move-result-object v12

    .line 168
    if-eqz v12, :cond_4

    const/4 v11, 0x1

    .line 178
    :cond_4
    :goto_5
    sget-object v4, Lcom/peel/util/a;->e:[I

    if-eqz v4, :cond_5

    sget-object v4, Lcom/peel/util/a;->e:[I

    const/4 v6, 0x0

    aget v4, v4, v6

    const/4 v6, -0x1

    if-eq v4, v6, :cond_5

    sget-object v4, Lcom/peel/util/a;->e:[I

    const/4 v6, 0x1

    aget v4, v4, v6

    const/4 v6, -0x1

    if-eq v4, v6, :cond_5

    .line 179
    invoke-virtual {v15}, Landroid/widget/FrameLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    .line 180
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/peel/ui/ks;->a:Lcom/peel/ui/kr;

    invoke-virtual {v6}, Lcom/peel/ui/kr;->n()Landroid/content/res/Resources;

    move-result-object v6

    sget v7, Lcom/peel/ui/fn;->tile_width:I

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v6

    float-to-int v6, v6

    sget-object v7, Lcom/peel/util/a;->e:[I

    const/4 v8, 0x0

    aget v7, v7, v8

    add-int/2addr v6, v7

    iput v6, v4, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 181
    invoke-virtual {v13, v4}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 184
    :cond_5
    sget v4, Lcom/peel/ui/fq;->tunein_overlay:I

    if-ne v5, v4, :cond_11

    const/4 v6, 0x1

    .line 186
    :goto_6
    if-nez v6, :cond_12

    if-eqz v11, :cond_12

    .line 187
    sget v4, Lcom/peel/ui/fp;->btn:I

    invoke-virtual {v13, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageView;

    .line 189
    if-eqz v4, :cond_6

    .line 190
    sget v5, Lcom/peel/ui/fo;->editreminder_stateful:I

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 193
    :cond_6
    sget v4, Lcom/peel/ui/fp;->remind_me_txt:I

    invoke-virtual {v13, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    sget v5, Lcom/peel/ui/ft;->undo_reminder:I

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 207
    :cond_7
    :goto_7
    sget v4, Lcom/peel/ui/fp;->btn:I

    invoke-virtual {v13, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v17

    new-instance v4, Lcom/peel/ui/kt;

    move-object/from16 v5, p0

    move-object/from16 v7, v16

    move/from16 v8, p3

    move-object/from16 v9, v20

    move-object v10, v14

    invoke-direct/range {v4 .. v12}, Lcom/peel/ui/kt;-><init>(Lcom/peel/ui/ks;ZLcom/peel/content/listing/Listing;ILandroid/os/Bundle;[Lcom/peel/content/listing/Listing;ZLjava/lang/String;)V

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 271
    sget v4, Lcom/peel/ui/fp;->more_btn:I

    invoke-virtual {v13, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    new-instance v5, Lcom/peel/ui/kv;

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    move/from16 v2, p3

    move-object/from16 v3, v16

    invoke-direct {v5, v0, v1, v2, v3}, Lcom/peel/ui/kv;-><init>(Lcom/peel/ui/ks;Landroid/os/Bundle;ILcom/peel/content/listing/Listing;)V

    invoke-virtual {v4, v5}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 300
    cmp-long v4, v24, v18

    if-lez v4, :cond_8

    cmp-long v4, v24, v22

    if-gez v4, :cond_8

    invoke-static {}, Lcom/peel/util/bx;->a()Z

    move-result v4

    if-eqz v4, :cond_8

    .line 301
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/peel/ui/ks;->a:Lcom/peel/ui/kr;

    iget-object v4, v4, Lcom/peel/ui/kr;->b:Lcom/peel/d/i;

    const-string/jumbo v5, "popuptoast"

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Lcom/peel/d/i;->a(Ljava/lang/String;I)I

    move-result v4

    if-nez v4, :cond_8

    .line 302
    new-instance v4, Landroid/content/Intent;

    const-string/jumbo v5, "show_toast"

    invoke-direct {v4, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 303
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/peel/ui/ks;->a:Lcom/peel/ui/kr;

    invoke-virtual {v5}, Lcom/peel/ui/kr;->m()Landroid/support/v4/app/ae;

    move-result-object v5

    invoke-virtual {v5}, Landroid/support/v4/app/ae;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5}, Landroid/support/v4/a/q;->a(Landroid/content/Context;)Landroid/support/v4/a/q;

    move-result-object v5

    invoke-virtual {v5, v4}, Landroid/support/v4/a/q;->a(Landroid/content/Intent;)Z

    .line 306
    :cond_8
    invoke-virtual {v13}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v4

    if-eqz v4, :cond_9

    .line 307
    invoke-virtual {v13}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v4

    .line 308
    check-cast v4, Landroid/widget/FrameLayout;

    invoke-virtual {v4, v13}, Landroid/widget/FrameLayout;->removeView(Landroid/view/View;)V

    .line 310
    :cond_9
    invoke-virtual {v15, v13}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    goto/16 :goto_0

    .line 113
    :cond_a
    const-wide/16 v4, 0x0

    move-wide/from16 v18, v4

    goto/16 :goto_1

    .line 137
    :cond_b
    sget-object v5, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v5}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v5

    invoke-virtual {v5}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v5

    invoke-virtual {v5}, Lcom/peel/data/at;->f()I

    move-result v5

    goto/16 :goto_2

    .line 145
    :cond_c
    invoke-static {}, Lcom/peel/util/a/f;->a()Lcom/peel/util/a/f;

    move-result-object v4

    sget-object v5, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v5}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v5

    if-nez v5, :cond_d

    const/4 v5, 0x1

    :goto_8
    const/16 v6, 0x4b1

    const/16 v7, 0x7d0

    const-string/jumbo v8, "category"

    .line 147
    move-object/from16 v0, v20

    invoke-virtual {v0, v8}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-static/range {v16 .. v16}, Lcom/peel/util/bx;->b(Lcom/peel/content/listing/Listing;)Ljava/lang/String;

    move-result-object v10

    const/4 v11, 0x0

    invoke-virtual/range {v16 .. v16}, Lcom/peel/content/listing/Listing;->h()Ljava/lang/String;

    move-result-object v12

    const/4 v13, 0x0

    move/from16 v9, p3

    .line 145
    invoke-virtual/range {v4 .. v13}, Lcom/peel/util/a/f;->a(IIILjava/lang/String;ILjava/lang/String;ILjava/lang/String;I)V

    goto/16 :goto_3

    :cond_d
    sget-object v5, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v5}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v5

    invoke-virtual {v5}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v5

    invoke-virtual {v5}, Lcom/peel/data/at;->f()I

    move-result v5

    goto :goto_8

    .line 152
    :cond_e
    const-wide/32 v4, 0x1b7740

    add-long v4, v4, v24

    cmp-long v4, v18, v4

    if-lez v4, :cond_f

    array-length v4, v14

    const/4 v5, 0x1

    if-lt v4, v5, :cond_f

    .line 153
    sget v4, Lcom/peel/ui/fq;->setreminder_overlay:I

    move v5, v4

    goto/16 :goto_4

    .line 155
    :cond_f
    const-string/jumbo v4, "refresh_fragment"

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/peel/ui/ks;->a:Lcom/peel/ui/kr;

    iget-object v5, v5, Lcom/peel/ui/kr;->c:Landroid/os/Bundle;

    const-string/jumbo v6, "fragment"

    invoke-virtual {v5, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v20

    invoke-virtual {v0, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 156
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/peel/ui/ks;->a:Lcom/peel/ui/kr;

    invoke-virtual {v4}, Lcom/peel/ui/kr;->m()Landroid/support/v4/app/ae;

    move-result-object v4

    const-class v5, Lcom/peel/ui/b/ag;

    invoke-virtual {v5}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v20

    invoke-static {v4, v5, v0}, Lcom/peel/d/e;->c(Landroid/support/v4/app/ae;Ljava/lang/String;Landroid/os/Bundle;)V

    move/from16 v5, v17

    goto/16 :goto_4

    .line 170
    :cond_10
    const/4 v4, 0x0

    aget-object v4, v14, v4

    invoke-static {v4}, Lcom/peel/util/bx;->e(Lcom/peel/content/listing/Listing;)Z

    move-result v11

    goto/16 :goto_5

    .line 184
    :cond_11
    const/4 v6, 0x0

    goto/16 :goto_6

    .line 194
    :cond_12
    if-nez v6, :cond_7

    .line 195
    sget v4, Lcom/peel/ui/fp;->btn:I

    invoke-virtual {v13, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageView;

    .line 197
    if-eqz v4, :cond_13

    .line 198
    sget v5, Lcom/peel/ui/fo;->setreminder_stateful:I

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 201
    :cond_13
    sget v4, Lcom/peel/ui/fp;->remind_me_txt:I

    invoke-virtual {v13, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    sget v5, Lcom/peel/ui/ft;->label_remind_me_all_caps:I

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    goto/16 :goto_7
.end method
