.class Lcom/peel/ui/er;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:I

.field final synthetic b:Lcom/peel/ui/eq;


# direct methods
.method constructor <init>(Lcom/peel/ui/eq;I)V
    .locals 0

    .prologue
    .line 274
    iput-object p1, p0, Lcom/peel/ui/er;->b:Lcom/peel/ui/eq;

    iput p2, p0, Lcom/peel/ui/er;->a:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    .line 277
    iget-object v0, p0, Lcom/peel/ui/er;->b:Lcom/peel/ui/eq;

    invoke-static {v0}, Lcom/peel/ui/eq;->a(Lcom/peel/ui/eq;)[Lcom/peel/data/Channel;

    move-result-object v0

    iget v1, p0, Lcom/peel/ui/er;->a:I

    aget-object v0, v0, v1

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/peel/data/Channel;->b(Z)V

    .line 278
    iget-object v0, p0, Lcom/peel/ui/er;->b:Lcom/peel/ui/eq;

    iget-object v0, v0, Lcom/peel/ui/eq;->a:Lcom/peel/ui/ek;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/peel/ui/ek;->a(Lcom/peel/ui/ek;Z)Z

    .line 279
    iget-object v0, p0, Lcom/peel/ui/er;->b:Lcom/peel/ui/eq;

    iget-object v0, v0, Lcom/peel/ui/eq;->a:Lcom/peel/ui/ek;

    invoke-static {v0}, Lcom/peel/ui/ek;->c(Lcom/peel/ui/ek;)Ljava/util/ArrayList;

    move-result-object v0

    iget v1, p0, Lcom/peel/ui/er;->a:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 280
    iget-object v0, p0, Lcom/peel/ui/er;->b:Lcom/peel/ui/eq;

    iget-object v0, v0, Lcom/peel/ui/eq;->a:Lcom/peel/ui/ek;

    iget-object v1, p0, Lcom/peel/ui/er;->b:Lcom/peel/ui/eq;

    invoke-static {v1}, Lcom/peel/ui/eq;->a(Lcom/peel/ui/eq;)[Lcom/peel/data/Channel;

    move-result-object v1

    iget v2, p0, Lcom/peel/ui/er;->a:I

    aget-object v1, v1, v2

    invoke-virtual {v1}, Lcom/peel/data/Channel;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/peel/ui/ek;->a(Lcom/peel/ui/ek;Ljava/lang/String;)V

    .line 281
    iget-object v0, p0, Lcom/peel/ui/er;->b:Lcom/peel/ui/eq;

    iget-object v0, v0, Lcom/peel/ui/eq;->a:Lcom/peel/ui/ek;

    invoke-virtual {v0}, Lcom/peel/ui/ek;->f()V

    .line 282
    iget-object v0, p0, Lcom/peel/ui/er;->b:Lcom/peel/ui/eq;

    invoke-virtual {v0}, Lcom/peel/ui/eq;->notifyDataSetChanged()V

    .line 283
    iget-object v0, p0, Lcom/peel/ui/er;->b:Lcom/peel/ui/eq;

    iget-object v0, v0, Lcom/peel/ui/eq;->a:Lcom/peel/ui/ek;

    .line 285
    invoke-static {v0}, Lcom/peel/ui/ek;->e(Lcom/peel/ui/ek;)Ljava/util/ArrayList;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/ui/er;->b:Lcom/peel/ui/eq;

    iget-object v1, v1, Lcom/peel/ui/eq;->a:Lcom/peel/ui/ek;

    .line 286
    invoke-static {v1}, Lcom/peel/ui/ek;->d(Lcom/peel/ui/ek;)I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    sget v1, Lcom/peel/ui/fp;->epg_list:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    sget v1, Lcom/peel/ui/fp;->epg_list:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/peel/widget/ObservableListView;

    .line 287
    invoke-virtual {v0}, Lcom/peel/widget/ObservableListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lcom/peel/ui/eq;

    .line 290
    invoke-static {}, Lcom/peel/ui/ek;->g()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "wait 5 seconds for guide"

    new-instance v3, Lcom/peel/ui/es;

    invoke-direct {v3, p0, v0}, Lcom/peel/ui/es;-><init>(Lcom/peel/ui/er;Lcom/peel/ui/eq;)V

    const-wide/16 v4, 0x1388

    invoke-static {v1, v2, v3, v4, v5}, Lcom/peel/util/i;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;J)Ljava/lang/Runnable;

    .line 318
    return-void
.end method
