.class Lcom/peel/ui/q;
.super Landroid/widget/BaseAdapter;


# instance fields
.field final synthetic a:Lcom/peel/ui/f;

.field private b:[Lcom/peel/data/Channel;

.field private c:I


# direct methods
.method private constructor <init>(Lcom/peel/ui/f;)V
    .locals 1

    .prologue
    .line 427
    iput-object p1, p0, Lcom/peel/ui/q;->a:Lcom/peel/ui/f;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 429
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/peel/data/Channel;

    iput-object v0, p0, Lcom/peel/ui/q;->b:[Lcom/peel/data/Channel;

    return-void
.end method

.method synthetic constructor <init>(Lcom/peel/ui/f;Lcom/peel/ui/g;)V
    .locals 0

    .prologue
    .line 427
    invoke-direct {p0, p1}, Lcom/peel/ui/q;-><init>(Lcom/peel/ui/f;)V

    return-void
.end method

.method static synthetic a(Lcom/peel/ui/q;)I
    .locals 1

    .prologue
    .line 427
    iget v0, p0, Lcom/peel/ui/q;->c:I

    return v0
.end method

.method static synthetic a(Lcom/peel/ui/q;I)V
    .locals 0

    .prologue
    .line 427
    invoke-direct {p0, p1}, Lcom/peel/ui/q;->b(I)V

    return-void
.end method

.method private b(I)V
    .locals 0

    .prologue
    .line 438
    iput p1, p0, Lcom/peel/ui/q;->c:I

    .line 439
    return-void
.end method


# virtual methods
.method public a(I)Lcom/peel/data/Channel;
    .locals 1

    .prologue
    .line 448
    iget-object v0, p0, Lcom/peel/ui/q;->b:[Lcom/peel/data/Channel;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/peel/ui/q;->b:[Lcom/peel/data/Channel;

    aget-object v0, v0, p1

    goto :goto_0
.end method

.method public a([Lcom/peel/data/Channel;)V
    .locals 0

    .prologue
    .line 433
    iput-object p1, p0, Lcom/peel/ui/q;->b:[Lcom/peel/data/Channel;

    .line 434
    invoke-virtual {p0}, Lcom/peel/ui/q;->notifyDataSetChanged()V

    .line 435
    return-void
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 443
    iget-object v0, p0, Lcom/peel/ui/q;->b:[Lcom/peel/data/Channel;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/peel/ui/q;->b:[Lcom/peel/data/Channel;

    array-length v0, v0

    goto :goto_0
.end method

.method public synthetic getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 427
    invoke-virtual {p0, p1}, Lcom/peel/ui/q;->a(I)Lcom/peel/data/Channel;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2

    .prologue
    .line 453
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 8

    .prologue
    .line 458
    invoke-virtual {p0, p1}, Lcom/peel/ui/q;->a(I)Lcom/peel/data/Channel;

    move-result-object v2

    .line 459
    if-eqz p2, :cond_3

    .line 460
    :goto_0
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    .line 461
    new-instance v1, Lcom/peel/ui/u;

    invoke-direct {v1}, Lcom/peel/ui/u;-><init>()V

    .line 462
    sget v0, Lcom/peel/ui/fp;->channel_image:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, v1, Lcom/peel/ui/u;->a:Landroid/widget/ImageView;

    .line 463
    sget v0, Lcom/peel/ui/fp;->channel_name:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lcom/peel/ui/u;->c:Landroid/widget/TextView;

    .line 464
    invoke-virtual {p2, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 466
    :cond_0
    if-eqz v2, :cond_2

    .line 467
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/ui/u;

    .line 468
    iget-object v1, v0, Lcom/peel/ui/u;->c:Landroid/widget/TextView;

    invoke-virtual {v2}, Lcom/peel/data/Channel;->m()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 469
    invoke-virtual {v2}, Lcom/peel/data/Channel;->f()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/peel/ui/u;->b:Ljava/lang/String;

    .line 471
    invoke-virtual {v2}, Lcom/peel/data/Channel;->f()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 473
    iget-object v1, v0, Lcom/peel/ui/u;->a:Landroid/widget/ImageView;

    sget v3, Lcom/peel/ui/fp;->tag_image:I

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Landroid/widget/ImageView;->setTag(ILjava/lang/Object;)V

    .line 474
    iget-object v1, v0, Lcom/peel/ui/u;->a:Landroid/widget/ImageView;

    sget v3, Lcom/peel/ui/fo;->btn_noitem_list:I

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 477
    const/16 v1, 0xc8

    .line 479
    iget-object v3, p0, Lcom/peel/ui/q;->a:Lcom/peel/ui/f;

    invoke-static {v3}, Lcom/peel/ui/f;->l(Lcom/peel/ui/f;)Ljava/util/ArrayList;

    move-result-object v3

    iget-object v4, v0, Lcom/peel/ui/u;->b:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 480
    const/16 v1, 0x64

    .line 484
    :cond_1
    const-class v3, Lcom/peel/ui/ey;

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v4, "load image"

    new-instance v5, Lcom/peel/ui/r;

    invoke-direct {v5, p0, v0, p1}, Lcom/peel/ui/r;-><init>(Lcom/peel/ui/q;Lcom/peel/ui/u;I)V

    int-to-long v6, v1

    invoke-static {v3, v4, v5, v6, v7}, Lcom/peel/util/i;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;J)Ljava/lang/Runnable;

    .line 512
    :goto_1
    iget-object v0, v0, Lcom/peel/ui/u;->a:Landroid/widget/ImageView;

    invoke-virtual {v2}, Lcom/peel/data/Channel;->f()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    .line 514
    new-instance v0, Lcom/peel/ui/t;

    invoke-direct {v0, p0, v2}, Lcom/peel/ui/t;-><init>(Lcom/peel/ui/q;Lcom/peel/data/Channel;)V

    invoke-virtual {p2, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 562
    :cond_2
    return-object p2

    .line 459
    :cond_3
    iget-object v0, p0, Lcom/peel/ui/q;->a:Lcom/peel/ui/f;

    invoke-static {v0}, Lcom/peel/ui/f;->k(Lcom/peel/ui/f;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lcom/peel/ui/fq;->epg_sub_channel_item:I

    iget-object v3, p0, Lcom/peel/ui/q;->a:Lcom/peel/ui/f;

    invoke-static {v3}, Lcom/peel/ui/f;->e(Lcom/peel/ui/f;)Lcom/peel/widget/ObservableListView;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v0, v1, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    goto/16 :goto_0

    .line 509
    :cond_4
    iget-object v1, v0, Lcom/peel/ui/u;->a:Landroid/widget/ImageView;

    sget v3, Lcom/peel/ui/fo;->btn_noitem_list:I

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_1
.end method
