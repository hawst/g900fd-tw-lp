.class public Lcom/peel/ui/jw;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/view/View$OnTouchListener;


# instance fields
.field private final a:Ljava/lang/Runnable;

.field private final b:Ljava/lang/Runnable;

.field private final c:Ljava/lang/Runnable;

.field private d:J

.field private e:Landroid/os/Handler;

.field private f:I

.field private g:I

.field private h:Landroid/widget/ImageView;


# direct methods
.method public constructor <init>(Landroid/widget/ImageView;I)V
    .locals 2

    .prologue
    .line 3513
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3479
    new-instance v0, Lcom/peel/ui/jx;

    invoke-direct {v0, p0}, Lcom/peel/ui/jx;-><init>(Lcom/peel/ui/jw;)V

    iput-object v0, p0, Lcom/peel/ui/jw;->a:Ljava/lang/Runnable;

    .line 3497
    new-instance v0, Lcom/peel/ui/jy;

    invoke-direct {v0, p0}, Lcom/peel/ui/jy;-><init>(Lcom/peel/ui/jw;)V

    iput-object v0, p0, Lcom/peel/ui/jw;->b:Ljava/lang/Runnable;

    .line 3502
    new-instance v0, Lcom/peel/ui/jz;

    invoke-direct {v0, p0}, Lcom/peel/ui/jz;-><init>(Lcom/peel/ui/jw;)V

    iput-object v0, p0, Lcom/peel/ui/jw;->c:Ljava/lang/Runnable;

    .line 3507
    const-wide/16 v0, 0x96

    iput-wide v0, p0, Lcom/peel/ui/jw;->d:J

    .line 3508
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/peel/ui/jw;->e:Landroid/os/Handler;

    .line 3509
    const/4 v0, 0x1

    iput v0, p0, Lcom/peel/ui/jw;->f:I

    .line 3514
    iput-object p1, p0, Lcom/peel/ui/jw;->h:Landroid/widget/ImageView;

    .line 3515
    iput p2, p0, Lcom/peel/ui/jw;->g:I

    .line 3516
    return-void
.end method

.method static synthetic a(Lcom/peel/ui/jw;)I
    .locals 1

    .prologue
    .line 3475
    iget v0, p0, Lcom/peel/ui/jw;->f:I

    return v0
.end method

.method static synthetic a(Lcom/peel/ui/jw;I)I
    .locals 0

    .prologue
    .line 3475
    iput p1, p0, Lcom/peel/ui/jw;->f:I

    return p1
.end method

.method static synthetic a(Lcom/peel/ui/jw;J)J
    .locals 1

    .prologue
    .line 3475
    iput-wide p1, p0, Lcom/peel/ui/jw;->d:J

    return-wide p1
.end method

.method static synthetic b(Lcom/peel/ui/jw;)Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 3475
    iget-object v0, p0, Lcom/peel/ui/jw;->a:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic c(Lcom/peel/ui/jw;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 3475
    iget-object v0, p0, Lcom/peel/ui/jw;->e:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic d(Lcom/peel/ui/jw;)Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 3475
    iget-object v0, p0, Lcom/peel/ui/jw;->h:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic e(Lcom/peel/ui/jw;)J
    .locals 2

    .prologue
    .line 3475
    iget-wide v0, p0, Lcom/peel/ui/jw;->d:J

    return-wide v0
.end method

.method static synthetic f(Lcom/peel/ui/jw;)Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 3475
    iget-object v0, p0, Lcom/peel/ui/jw;->b:Ljava/lang/Runnable;

    return-object v0
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 3519
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_3

    .line 3520
    const/4 v0, 0x0

    iput v0, p0, Lcom/peel/ui/jw;->f:I

    .line 3521
    const-wide/16 v0, 0x96

    iput-wide v0, p0, Lcom/peel/ui/jw;->d:J

    .line 3523
    iget-object v0, p0, Lcom/peel/ui/jw;->e:Landroid/os/Handler;

    iget-object v1, p0, Lcom/peel/ui/jw;->a:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 3525
    iget-object v0, p0, Lcom/peel/ui/jw;->h:Landroid/widget/ImageView;

    iget v1, p0, Lcom/peel/ui/jw;->g:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 3526
    invoke-static {}, Lcom/peel/ui/gt;->S()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "\n\n ########## onTouch view id: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3527
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 3528
    invoke-static {}, Lcom/peel/ui/gt;->S()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "\n\n ########## setting rocker tag view.getTag(): "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3529
    iget-object v0, p0, Lcom/peel/ui/jw;->h:Landroid/widget/ImageView;

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    .line 3533
    :goto_0
    iget-object v0, p0, Lcom/peel/ui/jw;->h:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->isSoundEffectsEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 3534
    iget-object v0, p0, Lcom/peel/ui/jw;->h:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setSoundEffectsEnabled(Z)V

    .line 3536
    :cond_0
    iget-object v0, p0, Lcom/peel/ui/jw;->e:Landroid/os/Handler;

    iget-object v1, p0, Lcom/peel/ui/jw;->a:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 3542
    :cond_1
    :goto_1
    return v4

    .line 3531
    :cond_2
    invoke-static {}, Lcom/peel/ui/gt;->S()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "\n\n ########## No tag view.getTag() to set"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 3537
    :cond_3
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    if-eq v4, v0, :cond_4

    const/4 v0, 0x3

    .line 3538
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    if-ne v0, v1, :cond_1

    .line 3539
    :cond_4
    iget-object v0, p0, Lcom/peel/ui/jw;->e:Landroid/os/Handler;

    iget-object v1, p0, Lcom/peel/ui/jw;->c:Ljava/lang/Runnable;

    const-wide/16 v2, 0xa

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_1
.end method
