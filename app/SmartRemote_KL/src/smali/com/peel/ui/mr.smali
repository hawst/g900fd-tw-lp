.class Lcom/peel/ui/mr;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field final synthetic a:Lcom/peel/ui/mo;


# direct methods
.method constructor <init>(Lcom/peel/ui/mo;)V
    .locals 0

    .prologue
    .line 122
    iput-object p1, p0, Lcom/peel/ui/mr;->a:Lcom/peel/ui/mo;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 0

    .prologue
    .line 137
    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 126
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 2

    .prologue
    .line 130
    iget-object v0, p0, Lcom/peel/ui/mr;->a:Lcom/peel/ui/mo;

    iget-object v1, p0, Lcom/peel/ui/mr;->a:Lcom/peel/ui/mo;

    invoke-static {v1}, Lcom/peel/ui/mo;->c(Lcom/peel/ui/mo;)Landroid/widget/EditText;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->length()I

    move-result v1

    rsub-int v1, v1, 0x8c

    invoke-static {v0, v1}, Lcom/peel/ui/mo;->a(Lcom/peel/ui/mo;I)I

    .line 131
    iget-object v0, p0, Lcom/peel/ui/mr;->a:Lcom/peel/ui/mo;

    invoke-virtual {v0}, Lcom/peel/ui/mo;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/ae;->invalidateOptionsMenu()V

    .line 132
    return-void
.end method
