.class Lcom/peel/ui/jj;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field final synthetic a:Lcom/peel/data/ContentRoom;

.field final synthetic b:Lcom/peel/ui/gt;


# direct methods
.method constructor <init>(Lcom/peel/ui/gt;Lcom/peel/data/ContentRoom;)V
    .locals 0

    .prologue
    .line 3672
    iput-object p1, p0, Lcom/peel/ui/jj;->b:Lcom/peel/ui/gt;

    iput-object p2, p0, Lcom/peel/ui/jj;->a:Lcom/peel/data/ContentRoom;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 3675
    iget-object v0, p0, Lcom/peel/ui/jj;->b:Lcom/peel/ui/gt;

    invoke-static {v0}, Lcom/peel/ui/gt;->ac(Lcom/peel/ui/gt;)Lcom/peel/widget/ag;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/widget/ag;->dismiss()V

    .line 3676
    invoke-virtual {p1}, Landroid/widget/AdapterView;->getAdapter()Landroid/widget/Adapter;

    move-result-object v0

    invoke-interface {v0}, Landroid/widget/Adapter;->getCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-ne v0, p3, :cond_1

    .line 3677
    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    iget-object v1, p0, Lcom/peel/ui/jj;->a:Lcom/peel/data/ContentRoom;

    invoke-virtual {v1}, Lcom/peel/data/ContentRoom;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/peel/control/am;->a(Ljava/lang/String;)Lcom/peel/control/RoomControl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/control/RoomControl;->c()[Lcom/peel/control/a;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 3678
    iget-object v0, p0, Lcom/peel/ui/jj;->b:Lcom/peel/ui/gt;

    invoke-static {v0}, Lcom/peel/ui/gt;->Z(Lcom/peel/ui/gt;)Lcom/peel/ui/ai;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/ui/ai;->a()V

    .line 3698
    :goto_0
    return-void

    .line 3680
    :cond_0
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 3681
    const-string/jumbo v1, "passback_clazz"

    iget-object v2, p0, Lcom/peel/ui/jj;->b:Lcom/peel/ui/gt;

    invoke-virtual {v2}, Lcom/peel/ui/gt;->m()Landroid/support/v4/app/ae;

    move-result-object v2

    invoke-static {v2}, Lcom/peel/d/e;->a(Landroid/support/v4/app/ae;)Lcom/peel/d/u;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 3682
    const-string/jumbo v1, "passback_bundle"

    iget-object v2, p0, Lcom/peel/ui/jj;->b:Lcom/peel/ui/gt;

    iget-object v2, v2, Lcom/peel/ui/gt;->c:Landroid/os/Bundle;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 3683
    const-string/jumbo v1, "room"

    iget-object v2, p0, Lcom/peel/ui/jj;->a:Lcom/peel/data/ContentRoom;

    invoke-virtual {v2}, Lcom/peel/data/ContentRoom;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 3684
    iget-object v1, p0, Lcom/peel/ui/jj;->b:Lcom/peel/ui/gt;

    invoke-virtual {v1}, Lcom/peel/ui/gt;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    const-class v2, Lcom/peel/i/dr;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lcom/peel/d/e;->c(Landroid/support/v4/app/ae;Ljava/lang/String;Landroid/os/Bundle;)V

    goto :goto_0

    .line 3687
    :cond_1
    invoke-static {}, Lcom/peel/ui/gt;->S()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "started activity"

    new-instance v2, Lcom/peel/ui/jk;

    invoke-direct {v2, p0, p1, p3}, Lcom/peel/ui/jk;-><init>(Lcom/peel/ui/jj;Landroid/widget/AdapterView;I)V

    invoke-static {v0, v1, v2}, Lcom/peel/util/i;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/lang/Runnable;

    goto :goto_0
.end method
