.class Lcom/peel/ui/il;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/peel/ui/gt;


# direct methods
.method constructor <init>(Lcom/peel/ui/gt;)V
    .locals 0

    .prologue
    .line 2570
    iput-object p1, p0, Lcom/peel/ui/il;->a:Lcom/peel/ui/gt;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 9

    .prologue
    const/4 v8, 0x0

    const/4 v5, 0x0

    const/4 v7, 0x1

    .line 2573
    invoke-static {}, Lcom/peel/ui/gt;->T()Landroid/app/Dialog;

    move-result-object v0

    if-nez v0, :cond_b

    .line 2574
    iget-object v0, p0, Lcom/peel/ui/il;->a:Lcom/peel/ui/gt;

    invoke-static {v0}, Lcom/peel/ui/gt;->D(Lcom/peel/ui/gt;)Landroid/widget/LinearLayout;

    move-result-object v0

    if-nez v0, :cond_a

    .line 2576
    iget-object v0, p0, Lcom/peel/ui/il;->a:Lcom/peel/ui/gt;

    invoke-static {v0}, Lcom/peel/ui/gt;->a(Lcom/peel/ui/gt;)Lcom/peel/control/a;

    move-result-object v0

    invoke-virtual {v0, v7}, Lcom/peel/control/a;->a(I)Lcom/peel/control/h;

    move-result-object v4

    .line 2577
    if-eqz v4, :cond_a

    .line 2579
    iget-object v0, p0, Lcom/peel/ui/il;->a:Lcom/peel/ui/gt;

    invoke-static {v0}, Lcom/peel/ui/gt;->E(Lcom/peel/ui/gt;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string/jumbo v1, "country"

    const-string/jumbo v2, ""

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "Japan"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/peel/ui/il;->a:Lcom/peel/ui/gt;

    invoke-static {v0}, Lcom/peel/ui/gt;->E(Lcom/peel/ui/gt;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string/jumbo v1, "country"

    const-string/jumbo v2, ""

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "jp"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    move v1, v7

    .line 2580
    :goto_0
    invoke-virtual {v4}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v0

    const-string/jumbo v2, "---"

    invoke-virtual {v0, v2}, Lcom/peel/data/g;->a(Ljava/lang/String;)Z

    move-result v2

    .line 2582
    if-eqz v1, :cond_4

    .line 2583
    if-eqz v2, :cond_3

    .line 2584
    iget-object v3, p0, Lcom/peel/ui/il;->a:Lcom/peel/ui/gt;

    iget-object v0, p0, Lcom/peel/ui/il;->a:Lcom/peel/ui/gt;

    invoke-static {v0}, Lcom/peel/ui/gt;->w(Lcom/peel/ui/gt;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v6, Lcom/peel/ui/fq;->controlpad_numberpad_12keys_modes4:I

    invoke-virtual {v0, v6, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    invoke-static {v3, v0}, Lcom/peel/ui/gt;->a(Lcom/peel/ui/gt;Landroid/widget/LinearLayout;)Landroid/widget/LinearLayout;

    .line 2585
    iget-object v0, p0, Lcom/peel/ui/il;->a:Lcom/peel/ui/gt;

    invoke-static {v0}, Lcom/peel/ui/gt;->D(Lcom/peel/ui/gt;)Landroid/widget/LinearLayout;

    move-result-object v0

    sget v3, Lcom/peel/ui/fo;->fc_device_popup_keypad_jp_4bu:I

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    .line 2597
    :goto_1
    sget-object v0, Lcom/peel/c/a;->b:Lcom/peel/c/e;

    invoke-static {v0}, Lcom/peel/c/a;->a(Lcom/peel/c/e;)Ljava/lang/Object;

    move-result-object v0

    sget-object v3, Lcom/peel/c/b;->c:Lcom/peel/c/b;

    if-ne v0, v3, :cond_6

    move v0, v7

    .line 2598
    :goto_2
    const-string/jumbo v3, "."

    invoke-virtual {v4, v3}, Lcom/peel/control/h;->a(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_1

    const-string/jumbo v3, "Dot_DASh"

    invoke-virtual {v4, v3}, Lcom/peel/control/h;->a(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_7

    :cond_1
    move v3, v7

    .line 2599
    :goto_3
    const-string/jumbo v6, "Enter"

    invoke-virtual {v4, v6}, Lcom/peel/control/h;->a(Ljava/lang/String;)Z

    move-result v4

    .line 2601
    if-eqz v0, :cond_8

    .line 2602
    iget-object v0, p0, Lcom/peel/ui/il;->a:Lcom/peel/ui/gt;

    invoke-virtual {v0}, Lcom/peel/ui/gt;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    iget-object v6, p0, Lcom/peel/ui/il;->a:Lcom/peel/ui/gt;

    .line 2603
    invoke-static {v6}, Lcom/peel/ui/gt;->F(Lcom/peel/ui/gt;)Landroid/view/View$OnClickListener;

    move-result-object v6

    .line 2602
    invoke-static/range {v0 .. v6}, Lcom/peel/util/dw;->b(Landroid/content/Context;ZZZZILandroid/view/View$OnClickListener;)Ljava/util/List;

    move-result-object v0

    .line 2609
    :goto_4
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_5
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iget-object v2, p0, Lcom/peel/ui/il;->a:Lcom/peel/ui/gt;

    invoke-static {v2}, Lcom/peel/ui/gt;->D(Lcom/peel/ui/gt;)Landroid/widget/LinearLayout;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    goto :goto_5

    :cond_2
    move v1, v5

    .line 2579
    goto :goto_0

    .line 2587
    :cond_3
    iget-object v3, p0, Lcom/peel/ui/il;->a:Lcom/peel/ui/gt;

    iget-object v0, p0, Lcom/peel/ui/il;->a:Lcom/peel/ui/gt;

    invoke-static {v0}, Lcom/peel/ui/gt;->w(Lcom/peel/ui/gt;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v6, Lcom/peel/ui/fq;->controlpad_numberpad_12keys_modes3:I

    invoke-virtual {v0, v6, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    invoke-static {v3, v0}, Lcom/peel/ui/gt;->a(Lcom/peel/ui/gt;Landroid/widget/LinearLayout;)Landroid/widget/LinearLayout;

    .line 2588
    iget-object v0, p0, Lcom/peel/ui/il;->a:Lcom/peel/ui/gt;

    invoke-static {v0}, Lcom/peel/ui/gt;->D(Lcom/peel/ui/gt;)Landroid/widget/LinearLayout;

    move-result-object v0

    sget v3, Lcom/peel/ui/fo;->fc_device_popup_keypad_jp_3bu:I

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    goto :goto_1

    .line 2591
    :cond_4
    iget-object v0, p0, Lcom/peel/ui/il;->a:Lcom/peel/ui/gt;

    invoke-static {v0}, Lcom/peel/ui/gt;->C(Lcom/peel/ui/gt;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 2592
    iget-object v3, p0, Lcom/peel/ui/il;->a:Lcom/peel/ui/gt;

    iget-object v0, p0, Lcom/peel/ui/il;->a:Lcom/peel/ui/gt;

    invoke-static {v0}, Lcom/peel/ui/gt;->w(Lcom/peel/ui/gt;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v6, Lcom/peel/ui/fq;->controlpad_numberpad:I

    invoke-virtual {v0, v6, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    invoke-static {v3, v0}, Lcom/peel/ui/gt;->a(Lcom/peel/ui/gt;Landroid/widget/LinearLayout;)Landroid/widget/LinearLayout;

    goto/16 :goto_1

    .line 2594
    :cond_5
    iget-object v3, p0, Lcom/peel/ui/il;->a:Lcom/peel/ui/gt;

    iget-object v0, p0, Lcom/peel/ui/il;->a:Lcom/peel/ui/gt;

    invoke-static {v0}, Lcom/peel/ui/gt;->w(Lcom/peel/ui/gt;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v6, Lcom/peel/ui/fq;->controlpad_numberpad_controlonly:I

    invoke-virtual {v0, v6, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    invoke-static {v3, v0}, Lcom/peel/ui/gt;->a(Lcom/peel/ui/gt;Landroid/widget/LinearLayout;)Landroid/widget/LinearLayout;

    goto/16 :goto_1

    :cond_6
    move v0, v5

    .line 2597
    goto/16 :goto_2

    :cond_7
    move v3, v5

    .line 2598
    goto/16 :goto_3

    .line 2605
    :cond_8
    iget-object v0, p0, Lcom/peel/ui/il;->a:Lcom/peel/ui/gt;

    invoke-virtual {v0}, Lcom/peel/ui/gt;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    iget-object v6, p0, Lcom/peel/ui/il;->a:Lcom/peel/ui/gt;

    .line 2606
    invoke-static {v6}, Lcom/peel/ui/gt;->F(Lcom/peel/ui/gt;)Landroid/view/View$OnClickListener;

    move-result-object v6

    .line 2605
    invoke-static/range {v0 .. v6}, Lcom/peel/util/dw;->a(Landroid/content/Context;ZZZZILandroid/view/View$OnClickListener;)Ljava/util/List;

    move-result-object v0

    goto/16 :goto_4

    .line 2611
    :cond_9
    iget-object v0, p0, Lcom/peel/ui/il;->a:Lcom/peel/ui/gt;

    invoke-static {v0}, Lcom/peel/ui/gt;->C(Lcom/peel/ui/gt;)Z

    move-result v0

    if-nez v0, :cond_a

    .line 2612
    iget-object v0, p0, Lcom/peel/ui/il;->a:Lcom/peel/ui/gt;

    invoke-static {v0}, Lcom/peel/ui/gt;->D(Lcom/peel/ui/gt;)Landroid/widget/LinearLayout;

    move-result-object v0

    sget v1, Lcom/peel/ui/fp;->favorite_channels_textview:I

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 2613
    iget-object v1, p0, Lcom/peel/ui/il;->a:Lcom/peel/ui/gt;

    invoke-static {v1}, Lcom/peel/ui/gt;->D(Lcom/peel/ui/gt;)Landroid/widget/LinearLayout;

    move-result-object v1

    sget v2, Lcom/peel/ui/fp;->add_favorite_channel_btn:I

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 2614
    if-eqz v0, :cond_a

    if-eqz v1, :cond_a

    .line 2615
    new-instance v2, Lcom/peel/ui/im;

    invoke-direct {v2, p0, v0, v1}, Lcom/peel/ui/im;-><init>(Lcom/peel/ui/il;Landroid/view/View;Landroid/view/View;)V

    .line 2625
    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2626
    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2632
    :cond_a
    new-instance v0, Landroid/app/Dialog;

    iget-object v1, p0, Lcom/peel/ui/il;->a:Lcom/peel/ui/gt;

    invoke-virtual {v1}, Lcom/peel/ui/gt;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/Dialog;-><init>(Landroid/content/Context;)V

    invoke-static {v0}, Lcom/peel/ui/gt;->a(Landroid/app/Dialog;)Landroid/app/Dialog;

    .line 2633
    invoke-static {}, Lcom/peel/ui/gt;->T()Landroid/app/Dialog;

    move-result-object v0

    invoke-virtual {v0, v7}, Landroid/app/Dialog;->requestWindowFeature(I)Z

    .line 2634
    invoke-static {}, Lcom/peel/ui/gt;->T()Landroid/app/Dialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    new-instance v1, Landroid/graphics/drawable/BitmapDrawable;

    iget-object v2, p0, Lcom/peel/ui/il;->a:Lcom/peel/ui/gt;

    invoke-virtual {v2}, Lcom/peel/ui/gt;->n()Landroid/content/res/Resources;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;)V

    invoke-virtual {v0, v1}, Landroid/view/Window;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2635
    invoke-static {}, Lcom/peel/ui/gt;->T()Landroid/app/Dialog;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/ui/il;->a:Lcom/peel/ui/gt;

    invoke-static {v1}, Lcom/peel/ui/gt;->D(Lcom/peel/ui/gt;)Landroid/widget/LinearLayout;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setContentView(Landroid/view/View;)V

    .line 2636
    invoke-static {}, Lcom/peel/ui/gt;->T()Landroid/app/Dialog;

    move-result-object v0

    invoke-virtual {v0, v7}, Landroid/app/Dialog;->setCanceledOnTouchOutside(Z)V

    .line 2637
    invoke-static {}, Lcom/peel/ui/gt;->T()Landroid/app/Dialog;

    move-result-object v0

    new-instance v1, Lcom/peel/ui/in;

    invoke-direct {v1, p0}, Lcom/peel/ui/in;-><init>(Lcom/peel/ui/il;)V

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 2646
    :cond_b
    iget-object v0, p0, Lcom/peel/ui/il;->a:Lcom/peel/ui/gt;

    invoke-static {v0}, Lcom/peel/ui/gt;->C(Lcom/peel/ui/gt;)Z

    move-result v0

    if-nez v0, :cond_c

    .line 2647
    iget-object v0, p0, Lcom/peel/ui/il;->a:Lcom/peel/ui/gt;

    iget-object v1, p0, Lcom/peel/ui/il;->a:Lcom/peel/ui/gt;

    invoke-static {v1}, Lcom/peel/ui/gt;->D(Lcom/peel/ui/gt;)Landroid/widget/LinearLayout;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/peel/ui/gt;->c(Lcom/peel/ui/gt;Landroid/view/View;)V

    .line 2649
    :cond_c
    invoke-static {}, Lcom/peel/ui/gt;->T()Landroid/app/Dialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    .line 2650
    return-void
.end method
