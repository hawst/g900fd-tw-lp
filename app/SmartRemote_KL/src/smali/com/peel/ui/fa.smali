.class Lcom/peel/ui/fa;
.super Lcom/peel/util/t;


# instance fields
.field final synthetic a:Lcom/peel/ui/ez;


# direct methods
.method constructor <init>(Lcom/peel/ui/ez;I)V
    .locals 0

    .prologue
    .line 227
    iput-object p1, p0, Lcom/peel/ui/fa;->a:Lcom/peel/ui/ez;

    invoke-direct {p0, p2}, Lcom/peel/util/t;-><init>(I)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 229
    iget-object v0, p0, Lcom/peel/ui/fa;->j:Ljava/lang/Object;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/peel/ui/fa;->i:Z

    if-nez v0, :cond_1

    .line 230
    :cond_0
    invoke-static {}, Lcom/peel/ui/ey;->g()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "handle error case"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 259
    :goto_0
    return-void

    .line 235
    :cond_1
    iget-object v0, p0, Lcom/peel/ui/fa;->j:Ljava/lang/Object;

    check-cast v0, Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/content/listing/Listing;

    .line 236
    if-nez v0, :cond_2

    .line 237
    invoke-static {}, Lcom/peel/ui/ey;->g()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "null listing"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 240
    :cond_2
    check-cast v0, Lcom/peel/content/listing/LiveListing;

    .line 241
    invoke-virtual {v0}, Lcom/peel/content/listing/LiveListing;->q()Ljava/lang/String;

    move-result-object v3

    .line 242
    iget-object v1, p0, Lcom/peel/ui/fa;->a:Lcom/peel/ui/ez;

    iget-object v1, v1, Lcom/peel/ui/ez;->a:Landroid/os/Bundle;

    invoke-virtual {v1, v3}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    .line 243
    if-nez v1, :cond_3

    .line 244
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 245
    iget-object v4, p0, Lcom/peel/ui/fa;->a:Lcom/peel/ui/ez;

    iget-object v4, v4, Lcom/peel/ui/ez;->a:Landroid/os/Bundle;

    invoke-virtual {v4, v3, v1}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 247
    :cond_3
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 251
    :cond_4
    iget-object v0, p0, Lcom/peel/ui/fa;->a:Lcom/peel/ui/ez;

    iget-object v0, v0, Lcom/peel/ui/ez;->a:Landroid/os/Bundle;

    const-string/jumbo v1, "has_listings"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 254
    const-class v0, Lcom/peel/ui/ey;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "refresh listview"

    new-instance v2, Lcom/peel/ui/fb;

    invoke-direct {v2, p0}, Lcom/peel/ui/fb;-><init>(Lcom/peel/ui/fa;)V

    invoke-static {v0, v1, v2}, Lcom/peel/util/i;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/lang/Runnable;

    goto :goto_0
.end method
