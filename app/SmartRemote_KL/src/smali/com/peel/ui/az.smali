.class public Lcom/peel/ui/az;
.super Landroid/widget/ArrayAdapter;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lcom/peel/data/Channel;",
        ">;"
    }
.end annotation


# static fields
.field private static final f:Ljava/lang/String;


# instance fields
.field a:Lcom/peel/ui/bh;

.field b:Landroid/view/View;

.field c:Landroid/widget/ListView;

.field d:Z

.field e:Landroid/os/Handler;

.field private g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/peel/data/Channel;",
            ">;"
        }
    .end annotation
.end field

.field private h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private i:Landroid/view/LayoutInflater;

.field private j:I

.field private k:Ljava/lang/String;

.field private l:Lcom/peel/data/ContentRoom;

.field private m:Landroid/widget/ImageView;

.field private final n:Landroid/support/v4/app/ae;

.field private o:Ljava/lang/Runnable;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 38
    const-class v0, Lcom/peel/ui/az;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/peel/ui/az;->f:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/support/v4/app/ae;ILjava/util/List;Ljava/util/List;Ljava/lang/String;Lcom/peel/data/ContentRoom;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/app/ae;",
            "I",
            "Ljava/util/List",
            "<",
            "Lcom/peel/data/Channel;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Lcom/peel/data/ContentRoom;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 50
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 43
    iput v0, p0, Lcom/peel/ui/az;->j:I

    .line 220
    iput-boolean v0, p0, Lcom/peel/ui/az;->d:Z

    .line 242
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/peel/ui/az;->e:Landroid/os/Handler;

    .line 243
    new-instance v0, Lcom/peel/ui/bf;

    invoke-direct {v0, p0}, Lcom/peel/ui/bf;-><init>(Lcom/peel/ui/az;)V

    iput-object v0, p0, Lcom/peel/ui/az;->o:Ljava/lang/Runnable;

    .line 51
    iput-object p1, p0, Lcom/peel/ui/az;->n:Landroid/support/v4/app/ae;

    .line 52
    iput-object p3, p0, Lcom/peel/ui/az;->g:Ljava/util/List;

    .line 53
    iget-object v0, p0, Lcom/peel/ui/az;->g:Ljava/util/List;

    new-instance v1, Lcom/peel/ui/ba;

    invoke-direct {v1, p0}, Lcom/peel/ui/ba;-><init>(Lcom/peel/ui/az;)V

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 62
    iput-object p5, p0, Lcom/peel/ui/az;->k:Ljava/lang/String;

    .line 63
    iput-object p6, p0, Lcom/peel/ui/az;->l:Lcom/peel/data/ContentRoom;

    .line 64
    iput-object p4, p0, Lcom/peel/ui/az;->h:Ljava/util/List;

    .line 65
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/ui/az;->i:Landroid/view/LayoutInflater;

    .line 66
    return-void
.end method

.method static synthetic a(Lcom/peel/ui/az;)Landroid/support/v4/app/ae;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/peel/ui/az;->n:Landroid/support/v4/app/ae;

    return-object v0
.end method

.method private a(Landroid/view/View;)V
    .locals 4

    .prologue
    .line 226
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/peel/ui/az;->d:Z

    .line 228
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    iput-object v0, p0, Lcom/peel/ui/az;->b:Landroid/view/View;

    .line 229
    iget-object v0, p0, Lcom/peel/ui/az;->b:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    invoke-interface {v0}, Landroid/view/ViewParent;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/peel/ui/az;->c:Landroid/widget/ListView;

    .line 232
    iget-object v0, p0, Lcom/peel/ui/az;->c:Landroid/widget/ListView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setEnabled(Z)V

    .line 234
    iget-object v0, p0, Lcom/peel/ui/az;->e:Landroid/os/Handler;

    iget-object v1, p0, Lcom/peel/ui/az;->o:Ljava/lang/Runnable;

    const-wide/16 v2, 0xbb8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 235
    return-void
.end method

.method private a(Lcom/peel/data/Channel;)V
    .locals 4

    .prologue
    .line 253
    const-string/jumbo v0, "live"

    invoke-static {v0}, Lcom/peel/content/a;->c(Ljava/lang/String;)Lcom/peel/content/library/Library;

    move-result-object v0

    .line 254
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 255
    const-string/jumbo v2, "channel"

    invoke-virtual {p1}, Lcom/peel/data/Channel;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 256
    const-string/jumbo v2, "callsign"

    invoke-virtual {p1}, Lcom/peel/data/Channel;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 257
    const-string/jumbo v2, "library"

    invoke-virtual {v0}, Lcom/peel/content/library/Library;->e()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 258
    const-string/jumbo v0, "room"

    invoke-static {}, Lcom/peel/content/a;->a()Lcom/peel/data/ContentRoom;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 259
    const-string/jumbo v0, "path"

    const-string/jumbo v2, "channel/lastchannel"

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 261
    sget-object v0, Lcom/peel/content/a;->f:Lcom/peel/content/user/User;

    new-instance v2, Lcom/peel/ui/bg;

    invoke-direct {v2, p0, p1}, Lcom/peel/ui/bg;-><init>(Lcom/peel/ui/az;Lcom/peel/data/Channel;)V

    invoke-virtual {v0, v1, v2}, Lcom/peel/content/user/User;->a(Landroid/os/Bundle;Lcom/peel/util/t;)V

    .line 267
    return-void
.end method

.method static synthetic a(Lcom/peel/ui/az;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0, p1}, Lcom/peel/ui/az;->a(Landroid/view/View;)V

    return-void
.end method

.method static synthetic a(Lcom/peel/ui/az;Lcom/peel/data/Channel;)V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0, p1}, Lcom/peel/ui/az;->a(Lcom/peel/data/Channel;)V

    return-void
.end method

.method static synthetic b(Lcom/peel/ui/az;)Ljava/util/List;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/peel/ui/az;->g:Ljava/util/List;

    return-object v0
.end method

.method private b()V
    .locals 2

    .prologue
    .line 238
    iget-object v0, p0, Lcom/peel/ui/az;->c:Landroid/widget/ListView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setEnabled(Z)V

    .line 240
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/peel/ui/az;->d:Z

    .line 241
    return-void
.end method

.method static synthetic c(Lcom/peel/ui/az;)Lcom/peel/data/ContentRoom;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/peel/ui/az;->l:Lcom/peel/data/ContentRoom;

    return-object v0
.end method

.method static synthetic d(Lcom/peel/ui/az;)Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/peel/ui/az;->m:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic e(Lcom/peel/ui/az;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/peel/ui/az;->k:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic f(Lcom/peel/ui/az;)V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0}, Lcom/peel/ui/az;->b()V

    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 68
    iget v0, p0, Lcom/peel/ui/az;->j:I

    return v0
.end method

.method public a(I)V
    .locals 0

    .prologue
    .line 71
    iput p1, p0, Lcom/peel/ui/az;->j:I

    .line 72
    return-void
.end method

.method public a(Lcom/peel/ui/bh;)V
    .locals 0

    .prologue
    .line 75
    iput-object p1, p0, Lcom/peel/ui/az;->a:Lcom/peel/ui/bh;

    .line 76
    return-void
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 7

    .prologue
    const/16 v6, 0x8

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 81
    if-nez p2, :cond_0

    iget-object v0, p0, Lcom/peel/ui/az;->i:Landroid/view/LayoutInflater;

    sget v1, Lcom/peel/ui/fq;->fav_channel_row:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 82
    :cond_0
    sget v0, Lcom/peel/ui/fp;->channel_image:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/peel/ui/az;->m:Landroid/widget/ImageView;

    .line 83
    iget-object v0, p0, Lcom/peel/ui/az;->m:Landroid/widget/ImageView;

    new-instance v1, Lcom/peel/ui/bb;

    invoke-direct {v1, p0, p1}, Lcom/peel/ui/bb;-><init>(Lcom/peel/ui/az;I)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 112
    iget-object v0, p0, Lcom/peel/ui/az;->g:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/data/Channel;

    invoke-virtual {v0}, Lcom/peel/data/Channel;->f()Ljava/lang/String;

    move-result-object v2

    .line 114
    iget-object v1, p0, Lcom/peel/ui/az;->h:Ljava/util/List;

    iget-object v0, p0, Lcom/peel/ui/az;->g:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/data/Channel;

    invoke-virtual {v0}, Lcom/peel/data/Channel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 115
    iget-object v0, p0, Lcom/peel/ui/az;->m:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 116
    invoke-virtual {p2, v4}, Landroid/view/View;->setEnabled(Z)V

    .line 117
    sget v0, Lcom/peel/ui/fp;->channel:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    sget v1, Lcom/peel/ui/ft;->favorites_not_available:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 118
    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_2

    .line 119
    iget-object v0, p0, Lcom/peel/ui/az;->n:Landroid/support/v4/app/ae;

    invoke-static {v0}, Lcom/peel/util/b/e;->a(Landroid/content/Context;)Lcom/squareup/picasso/Picasso;

    move-result-object v0

    .line 120
    invoke-virtual {v0, v2}, Lcom/squareup/picasso/Picasso;->load(Ljava/lang/String;)Lcom/squareup/picasso/RequestCreator;

    move-result-object v0

    sget v1, Lcom/peel/ui/fo;->btn_noitem_list:I

    .line 121
    invoke-virtual {v0, v1}, Lcom/squareup/picasso/RequestCreator;->placeholder(I)Lcom/squareup/picasso/RequestCreator;

    move-result-object v1

    sget v0, Lcom/peel/ui/fp;->channel_image:I

    .line 122
    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Lcom/squareup/picasso/RequestCreator;->into(Landroid/widget/ImageView;)V

    .line 174
    :goto_0
    iget v0, p0, Lcom/peel/ui/az;->j:I

    if-nez v0, :cond_5

    .line 175
    sget v0, Lcom/peel/ui/fp;->indicator:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    .line 176
    iget-object v1, p0, Lcom/peel/ui/az;->h:Ljava/util/List;

    iget-object v0, p0, Lcom/peel/ui/az;->g:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/data/Channel;

    invoke-virtual {v0}, Lcom/peel/data/Channel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 177
    sget v0, Lcom/peel/ui/fp;->more_channel_icon:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    .line 215
    :cond_1
    :goto_1
    return-object p2

    .line 124
    :cond_2
    iget-object v0, p0, Lcom/peel/ui/az;->m:Landroid/widget/ImageView;

    sget v1, Lcom/peel/ui/fo;->btn_noitem_list:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    .line 127
    :cond_3
    iget-object v0, p0, Lcom/peel/ui/az;->m:Landroid/widget/ImageView;

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 128
    invoke-virtual {p2, v5}, Landroid/view/View;->setEnabled(Z)V

    .line 129
    sget v0, Lcom/peel/ui/fp;->channel:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v1, p0, Lcom/peel/ui/az;->g:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/peel/data/Channel;

    invoke-virtual {v1}, Lcom/peel/data/Channel;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 130
    sget-object v1, Lcom/peel/ui/az;->f:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "get schedule for channel: "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v0, p0, Lcom/peel/ui/az;->g:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/data/Channel;

    invoke-virtual {v0}, Lcom/peel/data/Channel;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v3, Lcom/peel/ui/bc;

    invoke-direct {v3, p0, p1, p2, v2}, Lcom/peel/ui/bc;-><init>(Lcom/peel/ui/az;ILandroid/view/View;Ljava/lang/String;)V

    invoke-static {v1, v0, v3}, Lcom/peel/util/i;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    goto :goto_0

    .line 179
    :cond_4
    sget v0, Lcom/peel/ui/fp;->more_channel_icon:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1

    .line 183
    :cond_5
    iget v0, p0, Lcom/peel/ui/az;->j:I

    if-ne v0, v5, :cond_1

    .line 185
    sget v0, Lcom/peel/ui/fp;->indicator:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 186
    sget v0, Lcom/peel/ui/fp;->more_channel_icon:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    .line 187
    sget v0, Lcom/peel/ui/fp;->indicator:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    sget v1, Lcom/peel/ui/fo;->fav_channel_delete_stateful:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 188
    sget v0, Lcom/peel/ui/fp;->indicator:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/peel/ui/be;

    invoke-direct {v1, p0, p1}, Lcom/peel/ui/be;-><init>(Lcom/peel/ui/az;I)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_1
.end method
