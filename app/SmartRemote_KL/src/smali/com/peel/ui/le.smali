.class Lcom/peel/ui/le;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/peel/ui/kr;


# direct methods
.method constructor <init>(Lcom/peel/ui/kr;)V
    .locals 0

    .prologue
    .line 763
    iput-object p1, p0, Lcom/peel/ui/le;->a:Lcom/peel/ui/kr;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 766
    iget-object v0, p0, Lcom/peel/ui/le;->a:Lcom/peel/ui/kr;

    invoke-static {v0}, Lcom/peel/ui/kr;->d(Lcom/peel/ui/kr;)Landroid/animation/ObjectAnimator;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/peel/ui/le;->a:Lcom/peel/ui/kr;

    invoke-static {v0}, Lcom/peel/ui/kr;->d(Lcom/peel/ui/kr;)Landroid/animation/ObjectAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 767
    iget-object v0, p0, Lcom/peel/ui/le;->a:Lcom/peel/ui/kr;

    invoke-static {v0}, Lcom/peel/ui/kr;->d(Lcom/peel/ui/kr;)Landroid/animation/ObjectAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->cancel()V

    .line 770
    :cond_0
    iget-object v0, p0, Lcom/peel/ui/le;->a:Lcom/peel/ui/kr;

    invoke-static {v0}, Lcom/peel/ui/kr;->b(Lcom/peel/ui/kr;)Landroid/view/View;

    move-result-object v0

    sget v1, Lcom/peel/ui/fp;->no_content_panel:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 771
    iget-object v0, p0, Lcom/peel/ui/le;->a:Lcom/peel/ui/kr;

    invoke-static {v0}, Lcom/peel/ui/kr;->b(Lcom/peel/ui/kr;)Landroid/view/View;

    move-result-object v0

    sget v1, Lcom/peel/ui/fp;->no_content_panel:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 777
    :goto_0
    iget-object v0, p0, Lcom/peel/ui/le;->a:Lcom/peel/ui/kr;

    invoke-static {v0}, Lcom/peel/ui/kr;->b(Lcom/peel/ui/kr;)Landroid/view/View;

    move-result-object v0

    sget v1, Lcom/peel/ui/fp;->refresh:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 778
    new-instance v1, Lcom/peel/ui/lf;

    invoke-direct {v1, p0, v0}, Lcom/peel/ui/lf;-><init>(Lcom/peel/ui/le;Landroid/view/View;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 804
    return-void

    .line 773
    :cond_1
    iget-object v0, p0, Lcom/peel/ui/le;->a:Lcom/peel/ui/kr;

    invoke-static {v0}, Lcom/peel/ui/kr;->e(Lcom/peel/ui/kr;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lcom/peel/ui/fq;->no_content:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 774
    iget-object v0, p0, Lcom/peel/ui/le;->a:Lcom/peel/ui/kr;

    invoke-static {v0}, Lcom/peel/ui/kr;->b(Lcom/peel/ui/kr;)Landroid/view/View;

    move-result-object v0

    sget v2, Lcom/peel/ui/fp;->genre_container:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    goto :goto_0
.end method
