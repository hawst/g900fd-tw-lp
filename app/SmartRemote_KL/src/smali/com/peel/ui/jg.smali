.class Lcom/peel/ui/jg;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field final synthetic a:Lcom/peel/ui/gt;


# direct methods
.method constructor <init>(Lcom/peel/ui/gt;)V
    .locals 0

    .prologue
    .line 3582
    iput-object p1, p0, Lcom/peel/ui/jg;->a:Lcom/peel/ui/gt;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 3585
    packed-switch p3, :pswitch_data_0

    .line 3622
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/peel/ui/jg;->a:Lcom/peel/ui/gt;

    invoke-static {v0}, Lcom/peel/ui/gt;->A(Lcom/peel/ui/gt;)Landroid/widget/PopupWindow;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->dismiss()V

    .line 3623
    return-void

    .line 3591
    :pswitch_0
    iget-object v0, p0, Lcom/peel/ui/jg;->a:Lcom/peel/ui/gt;

    invoke-static {v0}, Lcom/peel/ui/gt;->Z(Lcom/peel/ui/gt;)Lcom/peel/ui/ai;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 3592
    iget-object v0, p0, Lcom/peel/ui/jg;->a:Lcom/peel/ui/gt;

    invoke-static {v0}, Lcom/peel/ui/gt;->Z(Lcom/peel/ui/gt;)Lcom/peel/ui/ai;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/ui/ai;->c()V

    goto :goto_0

    .line 3601
    :pswitch_1
    iget-object v0, p0, Lcom/peel/ui/jg;->a:Lcom/peel/ui/gt;

    invoke-static {v0}, Lcom/peel/ui/gt;->aa(Lcom/peel/ui/gt;)[Ljava/lang/String;

    move-result-object v0

    aget-object v0, v0, p3

    iget-object v1, p0, Lcom/peel/ui/jg;->a:Lcom/peel/ui/gt;

    invoke-virtual {v1}, Lcom/peel/ui/gt;->n()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/peel/ui/ft;->label_settings:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 3602
    iget-object v0, p0, Lcom/peel/ui/jg;->a:Lcom/peel/ui/gt;

    invoke-virtual {v0}, Lcom/peel/ui/gt;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    const-class v1, Lcom/peel/h/a/gr;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v4}, Lcom/peel/d/e;->c(Landroid/support/v4/app/ae;Ljava/lang/String;Landroid/os/Bundle;)V

    goto :goto_0

    .line 3604
    :cond_1
    iget-object v0, p0, Lcom/peel/ui/jg;->a:Lcom/peel/ui/gt;

    invoke-virtual {v0}, Lcom/peel/ui/gt;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    const-string/jumbo v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/ae;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 3605
    sget v1, Lcom/peel/ui/fq;->about:I

    invoke-virtual {v0, v1, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 3607
    sget v0, Lcom/peel/ui/fp;->about_app_version:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v2, p0, Lcom/peel/ui/jg;->a:Lcom/peel/ui/gt;

    invoke-virtual {v2}, Lcom/peel/ui/gt;->m()Landroid/support/v4/app/ae;

    move-result-object v2

    invoke-static {v2}, Lcom/peel/util/bx;->g(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 3608
    iget-object v0, p0, Lcom/peel/ui/jg;->a:Lcom/peel/ui/gt;

    new-instance v2, Lcom/peel/widget/ag;

    iget-object v3, p0, Lcom/peel/ui/jg;->a:Lcom/peel/ui/gt;

    invoke-virtual {v3}, Lcom/peel/ui/gt;->m()Landroid/support/v4/app/ae;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/peel/widget/ag;-><init>(Landroid/content/Context;)V

    invoke-virtual {v2}, Lcom/peel/widget/ag;->b()Lcom/peel/widget/ag;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/peel/widget/ag;->a(Landroid/view/View;)Lcom/peel/widget/ag;

    move-result-object v1

    sget v2, Lcom/peel/ui/ft;->ok:I

    invoke-virtual {v1, v2, v4}, Lcom/peel/widget/ag;->a(ILandroid/content/DialogInterface$OnClickListener;)Lcom/peel/widget/ag;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/peel/ui/gt;->d(Lcom/peel/ui/gt;Lcom/peel/widget/ag;)Lcom/peel/widget/ag;

    .line 3609
    iget-object v0, p0, Lcom/peel/ui/jg;->a:Lcom/peel/ui/gt;

    invoke-static {v0}, Lcom/peel/ui/gt;->ab(Lcom/peel/ui/gt;)Lcom/peel/widget/ag;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/widget/ag;->show()V

    goto/16 :goto_0

    .line 3614
    :pswitch_2
    iget-object v0, p0, Lcom/peel/ui/jg;->a:Lcom/peel/ui/gt;

    invoke-virtual {v0}, Lcom/peel/ui/gt;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    const-string/jumbo v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/ae;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 3615
    sget v1, Lcom/peel/ui/fq;->about:I

    invoke-virtual {v0, v1, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 3617
    sget v0, Lcom/peel/ui/fp;->about_app_version:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v2, p0, Lcom/peel/ui/jg;->a:Lcom/peel/ui/gt;

    invoke-virtual {v2}, Lcom/peel/ui/gt;->m()Landroid/support/v4/app/ae;

    move-result-object v2

    invoke-static {v2}, Lcom/peel/util/bx;->g(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 3618
    new-instance v0, Lcom/peel/widget/ag;

    iget-object v2, p0, Lcom/peel/ui/jg;->a:Lcom/peel/ui/gt;

    invoke-virtual {v2}, Lcom/peel/ui/gt;->m()Landroid/support/v4/app/ae;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/peel/widget/ag;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0}, Lcom/peel/widget/ag;->b()Lcom/peel/widget/ag;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/peel/widget/ag;->a(Landroid/view/View;)Lcom/peel/widget/ag;

    move-result-object v0

    sget v1, Lcom/peel/ui/ft;->ok:I

    invoke-virtual {v0, v1, v4}, Lcom/peel/widget/ag;->a(ILandroid/content/DialogInterface$OnClickListener;)Lcom/peel/widget/ag;

    move-result-object v0

    .line 3619
    invoke-virtual {v0}, Lcom/peel/widget/ag;->show()V

    goto/16 :goto_0

    .line 3585
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
