.class public Lcom/peel/ui/ai;
.super Landroid/widget/BaseAdapter;


# instance fields
.field public a:Lcom/peel/widget/ag;

.field public b:Lcom/peel/widget/ag;

.field c:Landroid/content/DialogInterface$OnDismissListener;

.field private final d:Landroid/view/LayoutInflater;

.field private final e:Landroid/support/v4/app/ae;

.field private f:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Lcom/peel/ui/ao;",
            ">;"
        }
    .end annotation
.end field

.field private final g:Ljava/util/HashMap;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "UseSparseArrays"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final h:Lcom/peel/d/i;

.field private i:I

.field private j:I

.field private k:Z

.field private l:Z

.field private m:Lcom/peel/control/RoomControl;

.field private n:Lcom/peel/control/a;

.field private o:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/support/v4/app/ae;Lcom/peel/d/i;ZZLjava/lang/String;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 56
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 45
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/peel/ui/ai;->f:Landroid/util/SparseArray;

    .line 46
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/peel/ui/ai;->g:Ljava/util/HashMap;

    .line 49
    iput v1, p0, Lcom/peel/ui/ai;->i:I

    iput v1, p0, Lcom/peel/ui/ai;->j:I

    .line 50
    iput-boolean v1, p0, Lcom/peel/ui/ai;->k:Z

    iput-boolean v1, p0, Lcom/peel/ui/ai;->l:Z

    .line 54
    iput-object v2, p0, Lcom/peel/ui/ai;->a:Lcom/peel/widget/ag;

    iput-object v2, p0, Lcom/peel/ui/ai;->b:Lcom/peel/widget/ag;

    .line 444
    new-instance v0, Lcom/peel/ui/al;

    invoke-direct {v0, p0}, Lcom/peel/ui/al;-><init>(Lcom/peel/ui/ai;)V

    iput-object v0, p0, Lcom/peel/ui/ai;->c:Landroid/content/DialogInterface$OnDismissListener;

    .line 57
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/ui/ai;->d:Landroid/view/LayoutInflater;

    .line 58
    iput-object p1, p0, Lcom/peel/ui/ai;->e:Landroid/support/v4/app/ae;

    .line 59
    iput-object p2, p0, Lcom/peel/ui/ai;->h:Lcom/peel/d/i;

    .line 60
    iput-boolean p3, p0, Lcom/peel/ui/ai;->k:Z

    .line 61
    iput-boolean p4, p0, Lcom/peel/ui/ai;->l:Z

    .line 62
    iput-object p5, p0, Lcom/peel/ui/ai;->o:Ljava/lang/String;

    .line 64
    if-eqz p4, :cond_0

    .line 65
    invoke-direct {p0}, Lcom/peel/ui/ai;->f()V

    .line 69
    :goto_0
    return-void

    .line 67
    :cond_0
    invoke-direct {p0}, Lcom/peel/ui/ai;->g()V

    goto :goto_0
.end method

.method static synthetic a(Lcom/peel/ui/ai;I)I
    .locals 0

    .prologue
    .line 37
    iput p1, p0, Lcom/peel/ui/ai;->i:I

    return p1
.end method

.method static synthetic a(Lcom/peel/ui/ai;)Landroid/support/v4/app/ae;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/peel/ui/ai;->e:Landroid/support/v4/app/ae;

    return-object v0
.end method

.method static synthetic b(Lcom/peel/ui/ai;)Lcom/peel/d/i;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/peel/ui/ai;->h:Lcom/peel/d/i;

    return-object v0
.end method

.method private f()V
    .locals 9

    .prologue
    .line 72
    iget-object v0, p0, Lcom/peel/ui/ai;->g:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 73
    iget-object v0, p0, Lcom/peel/ui/ai;->f:Landroid/util/SparseArray;

    if-eqz v0, :cond_0

    .line 74
    iget-object v0, p0, Lcom/peel/ui/ai;->f:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->clear()V

    .line 76
    :cond_0
    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v0}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/ui/ai;->m:Lcom/peel/control/RoomControl;

    .line 77
    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v0}, Lcom/peel/control/am;->c()[Lcom/peel/control/RoomControl;

    move-result-object v7

    .line 78
    iget-object v0, p0, Lcom/peel/ui/ai;->f:Landroid/util/SparseArray;

    if-nez v0, :cond_1

    .line 79
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/peel/ui/ai;->f:Landroid/util/SparseArray;

    .line 82
    :cond_1
    if-eqz v7, :cond_3

    array-length v0, v7

    if-lez v0, :cond_3

    .line 83
    const/4 v0, 0x0

    move v6, v0

    :goto_0
    array-length v0, v7

    if-ge v6, v0, :cond_3

    .line 84
    iget-object v8, p0, Lcom/peel/ui/ai;->f:Landroid/util/SparseArray;

    new-instance v0, Lcom/peel/ui/ao;

    aget-object v1, v7, v6

    invoke-virtual {v1}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/data/at;->b()Ljava/lang/String;

    move-result-object v2

    aget-object v1, v7, v6

    invoke-virtual {v1}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/data/at;->a()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, -0x1

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/peel/ui/ao;-><init>(Lcom/peel/ui/ai;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-virtual {v8, v6, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 85
    aget-object v0, v7, v6

    invoke-virtual {v0}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/data/at;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/ui/ai;->m:Lcom/peel/control/RoomControl;

    invoke-virtual {v1}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/data/at;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 86
    iput v6, p0, Lcom/peel/ui/ai;->j:I

    .line 83
    :cond_2
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto :goto_0

    .line 91
    :cond_3
    iget-object v0, p0, Lcom/peel/ui/ai;->g:Ljava/util/HashMap;

    iget-object v1, p0, Lcom/peel/ui/ai;->f:Landroid/util/SparseArray;

    invoke-virtual {v1}, Landroid/util/SparseArray;->size()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iget-object v2, p0, Lcom/peel/ui/ai;->e:Landroid/support/v4/app/ae;

    sget v3, Lcom/peel/ui/ft;->label_add_room:I

    invoke-virtual {v2, v3}, Landroid/support/v4/app/ae;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 92
    return-void
.end method

.method private g()V
    .locals 15

    .prologue
    const/4 v14, 0x1

    const/4 v0, 0x0

    .line 99
    iget-object v1, p0, Lcom/peel/ui/ai;->g:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->clear()V

    .line 101
    iget-object v1, p0, Lcom/peel/ui/ai;->f:Landroid/util/SparseArray;

    if-eqz v1, :cond_0

    .line 102
    iget-object v1, p0, Lcom/peel/ui/ai;->f:Landroid/util/SparseArray;

    invoke-virtual {v1}, Landroid/util/SparseArray;->clear()V

    .line 104
    :cond_0
    sget-object v1, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v1}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v1

    iput-object v1, p0, Lcom/peel/ui/ai;->m:Lcom/peel/control/RoomControl;

    .line 105
    iget-object v1, p0, Lcom/peel/ui/ai;->m:Lcom/peel/control/RoomControl;

    if-nez v1, :cond_2

    .line 135
    :cond_1
    :goto_0
    return-void

    .line 108
    :cond_2
    iget-object v1, p0, Lcom/peel/ui/ai;->m:Lcom/peel/control/RoomControl;

    invoke-virtual {v1}, Lcom/peel/control/RoomControl;->d()Lcom/peel/control/a;

    move-result-object v1

    iput-object v1, p0, Lcom/peel/ui/ai;->n:Lcom/peel/control/a;

    .line 109
    iget-object v1, p0, Lcom/peel/ui/ai;->n:Lcom/peel/control/a;

    if-eqz v1, :cond_1

    .line 112
    iget-object v1, p0, Lcom/peel/ui/ai;->f:Landroid/util/SparseArray;

    if-nez v1, :cond_3

    .line 113
    new-instance v1, Landroid/util/SparseArray;

    invoke-direct {v1}, Landroid/util/SparseArray;-><init>()V

    iput-object v1, p0, Lcom/peel/ui/ai;->f:Landroid/util/SparseArray;

    .line 118
    :cond_3
    iget-object v1, p0, Lcom/peel/ui/ai;->m:Lcom/peel/control/RoomControl;

    invoke-virtual {v1}, Lcom/peel/control/RoomControl;->c()[Lcom/peel/control/a;

    move-result-object v9

    .line 120
    if-eqz v9, :cond_6

    .line 121
    array-length v10, v9

    move v8, v0

    move v6, v0

    :goto_1
    if-ge v8, v10, :cond_6

    aget-object v1, v9, v8

    .line 122
    invoke-virtual {v1, v14}, Lcom/peel/control/a;->a(I)Lcom/peel/control/h;

    move-result-object v5

    .line 123
    if-eqz v5, :cond_7

    iget-boolean v0, p0, Lcom/peel/ui/ai;->k:Z

    if-nez v0, :cond_4

    const/4 v0, 0x5

    invoke-virtual {v5}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v2

    invoke-virtual {v2}, Lcom/peel/data/g;->d()I

    move-result v2

    if-ne v0, v2, :cond_4

    move v0, v6

    .line 121
    :goto_2
    add-int/lit8 v1, v8, 0x1

    move v8, v1

    move v6, v0

    goto :goto_1

    .line 125
    :cond_4
    invoke-virtual {v1}, Lcom/peel/control/a;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/peel/ui/ai;->n:Lcom/peel/control/a;

    invoke-virtual {v2}, Lcom/peel/control/a;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 126
    iput v6, p0, Lcom/peel/ui/ai;->i:I

    .line 128
    :cond_5
    iget-object v11, p0, Lcom/peel/ui/ai;->f:Landroid/util/SparseArray;

    add-int/lit8 v7, v6, 0x1

    new-instance v0, Lcom/peel/ui/ao;

    invoke-virtual {v1}, Lcom/peel/control/a;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1}, Lcom/peel/control/a;->a()Ljava/lang/String;

    move-result-object v3

    iget-boolean v4, p0, Lcom/peel/ui/ai;->k:Z

    invoke-virtual {v1, v14}, Lcom/peel/control/a;->a(I)Lcom/peel/control/h;

    move-result-object v12

    iget-object v13, p0, Lcom/peel/ui/ai;->e:Landroid/support/v4/app/ae;

    invoke-static {v4, v1, v12, v13}, Lcom/peel/util/bx;->a(ZLcom/peel/control/a;Lcom/peel/control/h;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v5}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/data/g;->d()I

    move-result v5

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/peel/ui/ao;-><init>(Lcom/peel/ui/ai;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-virtual {v11, v6, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    move v0, v7

    goto :goto_2

    .line 132
    :cond_6
    iget-object v0, p0, Lcom/peel/ui/ai;->f:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    .line 134
    iget-object v1, p0, Lcom/peel/ui/ai;->g:Ljava/util/HashMap;

    add-int/lit8 v0, v0, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iget-object v2, p0, Lcom/peel/ui/ai;->e:Landroid/support/v4/app/ae;

    sget v3, Lcom/peel/ui/ft;->label_change_room:I

    invoke-virtual {v2, v3}, Landroid/support/v4/app/ae;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    :cond_7
    move v0, v6

    goto :goto_2
.end method

.method private h()V
    .locals 3

    .prologue
    .line 518
    iget-object v0, p0, Lcom/peel/ui/ai;->e:Landroid/support/v4/app/ae;

    check-cast v0, Landroid/support/v7/app/ActionBarActivity;

    .line 519
    invoke-virtual {v0}, Landroid/support/v7/app/ActionBarActivity;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v0

    .line 520
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/support/v7/app/ActionBar;->setNavigationMode(I)V

    .line 522
    iget v1, p0, Lcom/peel/ui/ai;->i:I

    const/4 v2, -0x1

    if-le v1, v2, :cond_0

    .line 523
    iget v1, p0, Lcom/peel/ui/ai;->i:I

    invoke-virtual {v0, v1}, Landroid/support/v7/app/ActionBar;->setSelectedNavigationItem(I)V

    .line 525
    :cond_0
    return-void
.end method


# virtual methods
.method a()V
    .locals 10

    .prologue
    const/4 v9, 0x2

    const/4 v8, 0x1

    const/16 v7, 0xa

    const/4 v1, 0x0

    .line 239
    new-instance v2, Landroid/util/SparseArray;

    invoke-direct {v2, v7}, Landroid/util/SparseArray;-><init>(I)V

    .line 243
    iget-object v0, p0, Lcom/peel/ui/ai;->e:Landroid/support/v4/app/ae;

    sget v3, Lcom/peel/ui/ft;->DeviceType1:I

    invoke-virtual {v0, v3}, Landroid/support/v4/app/ae;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v8, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 244
    iget-object v0, p0, Lcom/peel/ui/ai;->e:Landroid/support/v4/app/ae;

    sget v3, Lcom/peel/ui/ft;->DeviceType2:I

    invoke-virtual {v0, v3}, Landroid/support/v4/app/ae;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v9, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 245
    iget-object v0, p0, Lcom/peel/ui/ai;->e:Landroid/support/v4/app/ae;

    sget v3, Lcom/peel/ui/ft;->DeviceType10:I

    invoke-virtual {v0, v3}, Landroid/support/v4/app/ae;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v7, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 247
    const/4 v0, 0x3

    iget-object v3, p0, Lcom/peel/ui/ai;->e:Landroid/support/v4/app/ae;

    sget v4, Lcom/peel/ui/ft;->DeviceType3:I

    invoke-virtual {v3, v4}, Landroid/support/v4/app/ae;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 248
    const/4 v0, 0x4

    iget-object v3, p0, Lcom/peel/ui/ai;->e:Landroid/support/v4/app/ae;

    sget v4, Lcom/peel/ui/ft;->DeviceType4:I

    invoke-virtual {v3, v4}, Landroid/support/v4/app/ae;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 249
    const/4 v0, 0x5

    iget-object v3, p0, Lcom/peel/ui/ai;->e:Landroid/support/v4/app/ae;

    sget v4, Lcom/peel/ui/ft;->DeviceType5:I

    invoke-virtual {v3, v4}, Landroid/support/v4/app/ae;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 250
    const/4 v0, 0x6

    iget-object v3, p0, Lcom/peel/ui/ai;->e:Landroid/support/v4/app/ae;

    sget v4, Lcom/peel/ui/ft;->DeviceType6:I

    invoke-virtual {v3, v4}, Landroid/support/v4/app/ae;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 251
    const/16 v0, 0x12

    iget-object v3, p0, Lcom/peel/ui/ai;->e:Landroid/support/v4/app/ae;

    sget v4, Lcom/peel/ui/ft;->DeviceType18:I

    invoke-virtual {v3, v4}, Landroid/support/v4/app/ae;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 254
    new-instance v3, Landroid/util/SparseArray;

    invoke-direct {v3}, Landroid/util/SparseArray;-><init>()V

    move v0, v1

    .line 255
    :goto_0
    invoke-virtual {v2}, Landroid/util/SparseArray;->size()I

    move-result v4

    if-ge v0, v4, :cond_0

    .line 256
    invoke-virtual {v2, v0}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v4

    .line 257
    invoke-virtual {v2, v4}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 255
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 265
    :cond_0
    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v0}, Lcom/peel/control/am;->e()[Lcom/peel/control/h;

    move-result-object v2

    .line 266
    if-eqz v2, :cond_1

    array-length v0, v2

    if-lez v0, :cond_1

    .line 267
    array-length v4, v2

    move v0, v1

    :goto_1
    if-ge v0, v4, :cond_1

    aget-object v5, v2, v0

    .line 268
    invoke-virtual {v5}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v6

    invoke-virtual {v6}, Lcom/peel/data/g;->d()I

    move-result v6

    sparse-switch v6, :sswitch_data_0

    .line 283
    invoke-virtual {v5}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v5

    invoke-virtual {v5}, Lcom/peel/data/g;->d()I

    move-result v5

    invoke-virtual {v3, v5}, Landroid/util/SparseArray;->remove(I)V

    .line 267
    :goto_2
    :sswitch_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 274
    :sswitch_1
    invoke-virtual {v3, v9}, Landroid/util/SparseArray;->remove(I)V

    .line 275
    const/16 v5, 0x14

    invoke-virtual {v3, v5}, Landroid/util/SparseArray;->remove(I)V

    goto :goto_2

    .line 279
    :sswitch_2
    invoke-virtual {v3, v8}, Landroid/util/SparseArray;->remove(I)V

    .line 280
    invoke-virtual {v3, v7}, Landroid/util/SparseArray;->remove(I)V

    goto :goto_2

    .line 288
    :cond_1
    invoke-static {}, Lcom/peel/content/a;->a()Lcom/peel/data/ContentRoom;

    move-result-object v4

    .line 290
    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v4}, Lcom/peel/data/ContentRoom;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/peel/control/am;->a(Ljava/lang/String;)Lcom/peel/control/RoomControl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/control/RoomControl;->c()[Lcom/peel/control/a;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 291
    new-instance v5, Landroid/os/Bundle;

    invoke-direct {v5}, Landroid/os/Bundle;-><init>()V

    .line 294
    new-instance v6, Ljava/util/ArrayList;

    invoke-virtual {v3}, Landroid/util/SparseArray;->size()I

    move-result v0

    invoke-direct {v6, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 295
    invoke-virtual {v3}, Landroid/util/SparseArray;->size()I

    move-result v0

    new-array v7, v0, [Ljava/lang/String;

    move v2, v1

    .line 296
    :goto_3
    invoke-virtual {v3}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-ge v2, v0, :cond_2

    .line 297
    invoke-virtual {v3, v2}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 298
    invoke-virtual {v3, v2}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    aput-object v0, v7, v2

    .line 296
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_3

    .line 301
    :cond_2
    const-string/jumbo v0, "valid_device_types"

    invoke-virtual {v5, v0, v6}, Landroid/os/Bundle;->putIntegerArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 302
    new-instance v0, Lcom/peel/widget/ag;

    iget-object v2, p0, Lcom/peel/ui/ai;->e:Landroid/support/v4/app/ae;

    invoke-direct {v0, v2}, Lcom/peel/widget/ag;-><init>(Landroid/content/Context;)V

    iget-object v2, p0, Lcom/peel/ui/ai;->e:Landroid/support/v4/app/ae;

    invoke-virtual {v2}, Landroid/support/v4/app/ae;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/peel/ui/ft;->label_device_type:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, ":"

    const-string/jumbo v8, " "

    invoke-virtual {v2, v3, v8}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/peel/widget/ag;->a(Ljava/lang/CharSequence;)Lcom/peel/widget/ag;

    move-result-object v0

    sget v2, Lcom/peel/ui/ft;->cancel:I

    new-instance v3, Lcom/peel/ui/aj;

    invoke-direct {v3, p0}, Lcom/peel/ui/aj;-><init>(Lcom/peel/ui/ai;)V

    invoke-virtual {v0, v2, v3}, Lcom/peel/widget/ag;->c(ILandroid/content/DialogInterface$OnClickListener;)Lcom/peel/widget/ag;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/ui/ai;->b:Lcom/peel/widget/ag;

    .line 309
    iget-object v0, p0, Lcom/peel/ui/ai;->b:Lcom/peel/widget/ag;

    iget-object v2, p0, Lcom/peel/ui/ai;->c:Landroid/content/DialogInterface$OnDismissListener;

    invoke-virtual {v0, v2}, Lcom/peel/widget/ag;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 310
    iget-object v0, p0, Lcom/peel/ui/ai;->e:Landroid/support/v4/app/ae;

    const-string/jumbo v2, "layout_inflater"

    invoke-virtual {v0, v2}, Landroid/support/v4/app/ae;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 311
    if-nez v0, :cond_4

    .line 334
    :cond_3
    :goto_4
    return-void

    .line 313
    :cond_4
    sget v2, Lcom/peel/ui/fq;->device_list_view:I

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    .line 314
    new-instance v1, Lcom/peel/ui/ax;

    iget-object v2, p0, Lcom/peel/ui/ai;->e:Landroid/support/v4/app/ae;

    sget v3, Lcom/peel/ui/fq;->device_row:I

    invoke-direct {v1, v2, v3, v7, v6}, Lcom/peel/ui/ax;-><init>(Landroid/content/Context;I[Ljava/lang/String;Ljava/util/ArrayList;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 315
    new-instance v1, Lcom/peel/ui/ak;

    invoke-direct {v1, p0, v5, v6, v4}, Lcom/peel/ui/ak;-><init>(Lcom/peel/ui/ai;Landroid/os/Bundle;Ljava/util/ArrayList;Lcom/peel/data/ContentRoom;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 331
    iget-object v1, p0, Lcom/peel/ui/ai;->b:Lcom/peel/widget/ag;

    invoke-virtual {v1, v0}, Lcom/peel/widget/ag;->a(Landroid/view/View;)Lcom/peel/widget/ag;

    .line 332
    iget-object v0, p0, Lcom/peel/ui/ai;->b:Lcom/peel/widget/ag;

    invoke-virtual {v0}, Lcom/peel/widget/ag;->show()V

    goto :goto_4

    .line 268
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_2
        0x2 -> :sswitch_1
        0x6 -> :sswitch_0
        0xa -> :sswitch_2
        0x14 -> :sswitch_1
    .end sparse-switch
.end method

.method b()V
    .locals 0

    .prologue
    .line 440
    invoke-virtual {p0}, Lcom/peel/ui/ai;->notifyDataSetChanged()V

    .line 441
    invoke-direct {p0}, Lcom/peel/ui/ai;->h()V

    .line 442
    return-void
.end method

.method c()V
    .locals 8

    .prologue
    const/4 v6, 0x0

    const/4 v7, 0x1

    .line 452
    iget-object v0, p0, Lcom/peel/ui/ai;->e:Landroid/support/v4/app/ae;

    const-string/jumbo v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/ae;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 454
    sget v1, Lcom/peel/ui/fq;->control_change_room:I

    invoke-virtual {v0, v1, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 455
    sget v1, Lcom/peel/ui/fp;->activities_lv:I

    invoke-virtual {v2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ListView;

    .line 457
    sget-object v3, Lcom/peel/content/a;->f:Lcom/peel/content/user/User;

    invoke-virtual {v3}, Lcom/peel/content/user/User;->k()Z

    move-result v3

    if-nez v3, :cond_1

    .line 499
    :cond_0
    :goto_0
    return-void

    .line 458
    :cond_1
    sget-object v3, Lcom/peel/content/a;->f:Lcom/peel/content/user/User;

    invoke-virtual {v3}, Lcom/peel/content/user/User;->l()[Lcom/peel/data/ContentRoom;

    move-result-object v3

    .line 460
    sget-object v4, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v4}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v4

    .line 461
    if-eqz v4, :cond_0

    .line 462
    sget v5, Lcom/peel/ui/fq;->room_add_row:I

    invoke-virtual {v0, v5, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/ListView;->addFooterView(Landroid/view/View;)V

    .line 463
    new-instance v0, Lcom/peel/ui/gf;

    iget-object v5, p0, Lcom/peel/ui/ai;->e:Landroid/support/v4/app/ae;

    sget v6, Lcom/peel/ui/fq;->settings_single_selection_row_wospace:I

    invoke-direct {v0, v5, v6, v3}, Lcom/peel/ui/gf;-><init>(Landroid/content/Context;I[Lcom/peel/data/ContentRoom;)V

    invoke-virtual {v1, v0}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 464
    const/4 v0, 0x0

    :goto_1
    array-length v5, v3

    if-ge v0, v5, :cond_2

    .line 465
    invoke-virtual {v4}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v5

    invoke-virtual {v5}, Lcom/peel/data/at;->b()Ljava/lang/String;

    move-result-object v5

    aget-object v6, v3, v0

    invoke-virtual {v6}, Lcom/peel/data/ContentRoom;->a()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 466
    invoke-virtual {v1, v0, v7}, Landroid/widget/ListView;->setItemChecked(IZ)V

    .line 471
    :cond_2
    new-instance v0, Lcom/peel/widget/ag;

    iget-object v4, p0, Lcom/peel/ui/ai;->e:Landroid/support/v4/app/ae;

    invoke-direct {v0, v4}, Lcom/peel/widget/ag;-><init>(Landroid/content/Context;)V

    sget v4, Lcom/peel/ui/ft;->label_select_room:I

    invoke-virtual {v0, v4}, Lcom/peel/widget/ag;->a(I)Lcom/peel/widget/ag;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/peel/widget/ag;->a(Landroid/view/View;)Lcom/peel/widget/ag;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/ui/ai;->a:Lcom/peel/widget/ag;

    .line 472
    iget-object v0, p0, Lcom/peel/ui/ai;->a:Lcom/peel/widget/ag;

    sget v2, Lcom/peel/ui/ft;->cancel:I

    new-instance v4, Lcom/peel/ui/am;

    invoke-direct {v4, p0}, Lcom/peel/ui/am;-><init>(Lcom/peel/ui/ai;)V

    invoke-virtual {v0, v2, v4}, Lcom/peel/widget/ag;->c(ILandroid/content/DialogInterface$OnClickListener;)Lcom/peel/widget/ag;

    .line 478
    iget-object v0, p0, Lcom/peel/ui/ai;->a:Lcom/peel/widget/ag;

    iget-object v2, p0, Lcom/peel/ui/ai;->c:Landroid/content/DialogInterface$OnDismissListener;

    invoke-virtual {v0, v2}, Lcom/peel/widget/ag;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 480
    new-instance v0, Lcom/peel/ui/an;

    invoke-direct {v0, p0, v3}, Lcom/peel/ui/an;-><init>(Lcom/peel/ui/ai;[Lcom/peel/data/ContentRoom;)V

    invoke-virtual {v1, v0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 497
    iget-object v0, p0, Lcom/peel/ui/ai;->a:Lcom/peel/widget/ag;

    invoke-virtual {v0, v7}, Lcom/peel/widget/ag;->setCanceledOnTouchOutside(Z)V

    .line 498
    iget-object v0, p0, Lcom/peel/ui/ai;->a:Lcom/peel/widget/ag;

    iget-object v1, p0, Lcom/peel/ui/ai;->a:Lcom/peel/widget/ag;

    invoke-virtual {v0, v1}, Lcom/peel/widget/ag;->a(Lcom/peel/widget/ag;)Lcom/peel/widget/ag;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/widget/ag;->show()V

    goto :goto_0

    .line 464
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method public d()V
    .locals 1

    .prologue
    .line 502
    iget-object v0, p0, Lcom/peel/ui/ai;->a:Lcom/peel/widget/ag;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/peel/ui/ai;->a:Lcom/peel/widget/ag;

    invoke-virtual {v0}, Lcom/peel/widget/ag;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 503
    iget-object v0, p0, Lcom/peel/ui/ai;->a:Lcom/peel/widget/ag;

    invoke-virtual {v0}, Lcom/peel/widget/ag;->dismiss()V

    .line 505
    :cond_0
    iget-object v0, p0, Lcom/peel/ui/ai;->b:Lcom/peel/widget/ag;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/peel/ui/ai;->b:Lcom/peel/widget/ag;

    invoke-virtual {v0}, Lcom/peel/widget/ag;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 506
    iget-object v0, p0, Lcom/peel/ui/ai;->b:Lcom/peel/widget/ag;

    invoke-virtual {v0}, Lcom/peel/widget/ag;->dismiss()V

    .line 508
    :cond_1
    return-void
.end method

.method public e()V
    .locals 3

    .prologue
    .line 511
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 512
    const-string/jumbo v1, "add_room"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 513
    const-string/jumbo v1, "back_visibility"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 514
    iget-object v1, p0, Lcom/peel/ui/ai;->e:Landroid/support/v4/app/ae;

    const-class v2, Lcom/peel/i/fq;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lcom/peel/d/e;->c(Landroid/support/v4/app/ae;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 515
    return-void
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 139
    iget-object v0, p0, Lcom/peel/ui/ai;->f:Landroid/util/SparseArray;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-boolean v0, p0, Lcom/peel/ui/ai;->l:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/peel/ui/ai;->f:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/peel/ui/ai;->f:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    add-int/lit8 v0, v0, 0x2

    goto :goto_0
.end method

.method public getDropDownView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 7

    .prologue
    const/4 v5, 0x1

    const/4 v6, 0x0

    const/4 v4, 0x0

    .line 177
    invoke-virtual {p0, p1}, Lcom/peel/ui/ai;->getItemViewType(I)I

    move-result v1

    .line 178
    iget-object v0, p0, Lcom/peel/ui/ai;->f:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/ui/ao;

    .line 179
    new-instance v2, Lcom/peel/ui/ap;

    invoke-direct {v2, p0, v4}, Lcom/peel/ui/ap;-><init>(Lcom/peel/ui/ai;Lcom/peel/ui/aj;)V

    .line 181
    packed-switch v1, :pswitch_data_0

    .line 234
    :cond_0
    :goto_0
    invoke-virtual {p2, v2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 235
    return-object p2

    .line 183
    :pswitch_0
    if-eqz v0, :cond_0

    .line 184
    iget-object v1, p0, Lcom/peel/ui/ai;->d:Landroid/view/LayoutInflater;

    sget v3, Lcom/peel/ui/fq;->control_list_contents:I

    invoke-virtual {v1, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 185
    sget v1, Lcom/peel/ui/fp;->content_text:I

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, v2, Lcom/peel/ui/ap;->a:Landroid/widget/TextView;

    .line 186
    sget v1, Lcom/peel/ui/fp;->plus_image:I

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, v2, Lcom/peel/ui/ap;->c:Landroid/widget/ImageView;

    .line 188
    iget-boolean v1, p0, Lcom/peel/ui/ai;->l:Z

    if-eqz v1, :cond_2

    .line 189
    invoke-virtual {v0}, Lcom/peel/ui/ao;->a()Ljava/lang/String;

    move-result-object v1

    iget-object v3, p0, Lcom/peel/ui/ai;->m:Lcom/peel/control/RoomControl;

    invoke-virtual {v3}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v3

    invoke-virtual {v3}, Lcom/peel/data/at;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 190
    iget-object v1, v2, Lcom/peel/ui/ap;->c:Landroid/widget/ImageView;

    sget v3, Lcom/peel/ui/fo;->winset_select_activity_small:I

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 192
    :cond_1
    iget-object v1, v2, Lcom/peel/ui/ap;->a:Landroid/widget/TextView;

    invoke-virtual {v0}, Lcom/peel/ui/ao;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 194
    :cond_2
    invoke-virtual {v0}, Lcom/peel/ui/ao;->c()I

    move-result v1

    packed-switch v1, :pswitch_data_1

    .line 199
    iget-object v1, v2, Lcom/peel/ui/ap;->a:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/peel/ui/ai;->e:Landroid/support/v4/app/ae;

    sget v4, Lcom/peel/ui/ft;->watch_fmt:I

    invoke-virtual {v3, v4}, Landroid/support/v4/app/ae;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-array v4, v5, [Ljava/lang/Object;

    invoke-virtual {v0}, Lcom/peel/ui/ao;->d()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 203
    :goto_1
    invoke-virtual {v0}, Lcom/peel/ui/ao;->a()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/ui/ai;->n:Lcom/peel/control/a;

    invoke-virtual {v1}, Lcom/peel/control/a;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 204
    iget-object v0, v2, Lcom/peel/ui/ap;->c:Landroid/widget/ImageView;

    sget v1, Lcom/peel/ui/fo;->winset_select_activity_small:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_0

    .line 196
    :pswitch_1
    iget-object v1, v2, Lcom/peel/ui/ap;->a:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/peel/ui/ai;->e:Landroid/support/v4/app/ae;

    sget v4, Lcom/peel/ui/ft;->DeviceType18:I

    invoke-virtual {v3, v4}, Landroid/support/v4/app/ae;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 210
    :pswitch_2
    iget-object v0, p0, Lcom/peel/ui/ai;->d:Landroid/view/LayoutInflater;

    sget v1, Lcom/peel/ui/fq;->control_list_contents:I

    invoke-virtual {v0, v1, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 211
    sget v0, Lcom/peel/ui/fo;->spinner_bottom_bg_stateful:I

    invoke-virtual {p2, v0}, Landroid/view/View;->setBackgroundResource(I)V

    .line 212
    sget v0, Lcom/peel/ui/fp;->content_text:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v2, Lcom/peel/ui/ap;->a:Landroid/widget/TextView;

    .line 213
    sget v0, Lcom/peel/ui/fp;->plus_image:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, v2, Lcom/peel/ui/ap;->c:Landroid/widget/ImageView;

    .line 214
    iget-object v1, v2, Lcom/peel/ui/ap;->a:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/peel/ui/ai;->g:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 215
    iget-object v0, v2, Lcom/peel/ui/ap;->a:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/peel/ui/ai;->e:Landroid/support/v4/app/ae;

    sget v3, Lcom/peel/ui/fu;->roboto_regular_252525_header_italic:I

    invoke-virtual {v0, v1, v3}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 216
    iget-object v0, v2, Lcom/peel/ui/ap;->a:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 218
    iget-object v1, p0, Lcom/peel/ui/ai;->e:Landroid/support/v4/app/ae;

    sget v3, Lcom/peel/ui/ft;->label_change_room:I

    invoke-virtual {v1, v3}, Landroid/support/v4/app/ae;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 219
    iget-object v0, v2, Lcom/peel/ui/ap;->c:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_0

    .line 221
    :cond_3
    iget-object v0, v2, Lcom/peel/ui/ap;->c:Landroid/widget/ImageView;

    sget v1, Lcom/peel/ui/fo;->change_room_plus_icon:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_0

    .line 226
    :pswitch_3
    iget-object v0, p0, Lcom/peel/ui/ai;->d:Landroid/view/LayoutInflater;

    sget v1, Lcom/peel/ui/fq;->content_list_two_rows:I

    invoke-virtual {v0, v1, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 227
    sget v0, Lcom/peel/ui/fp;->content_text:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v2, Lcom/peel/ui/ap;->a:Landroid/widget/TextView;

    .line 228
    sget v0, Lcom/peel/ui/fp;->content_sub_text:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v2, Lcom/peel/ui/ap;->b:Landroid/widget/TextView;

    .line 230
    iget-object v0, v2, Lcom/peel/ui/ap;->a:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/peel/ui/ai;->e:Landroid/support/v4/app/ae;

    sget v3, Lcom/peel/ui/ft;->add_device:I

    invoke-virtual {v1, v3}, Landroid/support/v4/app/ae;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-array v3, v5, [Ljava/lang/Object;

    const-string/jumbo v4, ""

    aput-object v4, v3, v6

    invoke-static {v1, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 231
    iget-object v0, v2, Lcom/peel/ui/ap;->b:Landroid/widget/TextView;

    sget v1, Lcom/peel/ui/ft;->device_add_category:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto/16 :goto_0

    .line 181
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_2
        :pswitch_3
    .end packed-switch

    .line 194
    :pswitch_data_1
    .packed-switch 0x12
        :pswitch_1
    .end packed-switch
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 155
    invoke-virtual {p0, p1}, Lcom/peel/ui/ai;->getItemViewType(I)I

    move-result v0

    .line 157
    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 158
    iget-object v0, p0, Lcom/peel/ui/ai;->g:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 165
    :goto_0
    return-object v0

    .line 159
    :cond_0
    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 160
    iget-object v0, p0, Lcom/peel/ui/ai;->f:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    .line 161
    :cond_1
    const/4 v1, 0x3

    if-ne v0, v1, :cond_2

    .line 162
    iget-object v0, p0, Lcom/peel/ui/ai;->e:Landroid/support/v4/app/ae;

    sget v1, Lcom/peel/ui/ft;->add_device:I

    invoke-virtual {v0, v1}, Landroid/support/v4/app/ae;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 165
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getItemId(I)J
    .locals 2

    .prologue
    .line 171
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public getItemViewType(I)I
    .locals 2

    .prologue
    .line 376
    iget-object v0, p0, Lcom/peel/ui/ai;->g:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 377
    const/4 v0, 0x2

    .line 381
    :goto_0
    return v0

    .line 378
    :cond_0
    iget-object v0, p0, Lcom/peel/ui/ai;->f:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 379
    const/4 v0, 0x1

    goto :goto_0

    .line 381
    :cond_1
    const/4 v0, 0x3

    goto :goto_0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 9

    .prologue
    const/4 v3, 0x0

    const/4 v5, 0x1

    .line 338
    .line 340
    if-nez p2, :cond_0

    .line 341
    new-instance v1, Lcom/peel/ui/ap;

    invoke-direct {v1, p0, v3}, Lcom/peel/ui/ap;-><init>(Lcom/peel/ui/ai;Lcom/peel/ui/aj;)V

    .line 342
    iget-object v0, p0, Lcom/peel/ui/ai;->d:Landroid/view/LayoutInflater;

    sget v2, Lcom/peel/ui/fq;->control_spinner_item:I

    invoke-virtual {v0, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 343
    const v0, 0x1020014

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lcom/peel/ui/ap;->a:Landroid/widget/TextView;

    .line 344
    const v0, 0x1020015

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lcom/peel/ui/ap;->b:Landroid/widget/TextView;

    .line 345
    invoke-virtual {p2, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    move-object v0, v1

    .line 350
    :goto_0
    iget-boolean v1, p0, Lcom/peel/ui/ai;->l:Z

    if-eqz v1, :cond_1

    .line 351
    iget-object v1, v0, Lcom/peel/ui/ap;->b:Landroid/widget/TextView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 352
    iget-object v0, v0, Lcom/peel/ui/ap;->a:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/peel/ui/ai;->o:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/peel/ui/ai;->m:Lcom/peel/control/RoomControl;

    invoke-virtual {v2}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v2

    invoke-virtual {v2}, Lcom/peel/data/at;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 371
    :goto_1
    return-object p2

    .line 347
    :cond_0
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/ui/ap;

    goto :goto_0

    .line 355
    :cond_1
    iget-object v1, p0, Lcom/peel/ui/ai;->n:Lcom/peel/control/a;

    invoke-virtual {v1, v5}, Lcom/peel/control/a;->a(I)Lcom/peel/control/h;

    move-result-object v1

    .line 357
    if-eqz v1, :cond_2

    .line 358
    invoke-virtual {v1}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v2

    invoke-virtual {v2}, Lcom/peel/data/g;->d()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 363
    iget-object v2, v0, Lcom/peel/ui/ap;->a:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/peel/ui/ai;->e:Landroid/support/v4/app/ae;

    sget v4, Lcom/peel/ui/ft;->watching:I

    invoke-virtual {v3, v4}, Landroid/support/v4/app/ae;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-array v4, v5, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-boolean v6, p0, Lcom/peel/ui/ai;->k:Z

    iget-object v7, p0, Lcom/peel/ui/ai;->n:Lcom/peel/control/a;

    iget-object v8, p0, Lcom/peel/ui/ai;->e:Landroid/support/v4/app/ae;

    invoke-static {v6, v7, v1, v8}, Lcom/peel/util/bx;->a(ZLcom/peel/control/a;Lcom/peel/control/h;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 369
    :cond_2
    :goto_2
    iget-object v0, v0, Lcom/peel/ui/ap;->b:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/peel/ui/ai;->m:Lcom/peel/control/RoomControl;

    invoke-virtual {v1}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/data/at;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 360
    :pswitch_0
    iget-object v1, v0, Lcom/peel/ui/ap;->a:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/peel/ui/ai;->e:Landroid/support/v4/app/ae;

    sget v3, Lcom/peel/ui/ft;->DeviceType18:I

    invoke-virtual {v2, v3}, Landroid/support/v4/app/ae;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2

    .line 358
    :pswitch_data_0
    .packed-switch 0x12
        :pswitch_0
    .end packed-switch
.end method

.method public getViewTypeCount()I
    .locals 1

    .prologue
    .line 388
    const/4 v0, 0x4

    return v0
.end method

.method public isEnabled(I)Z
    .locals 1

    .prologue
    .line 393
    invoke-virtual {p0, p1}, Lcom/peel/ui/ai;->getItemViewType(I)I

    move-result v0

    if-nez v0, :cond_0

    .line 394
    const/4 v0, 0x0

    .line 397
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public notifyDataSetChanged()V
    .locals 1

    .prologue
    .line 144
    iget-boolean v0, p0, Lcom/peel/ui/ai;->l:Z

    if-eqz v0, :cond_0

    .line 145
    invoke-direct {p0}, Lcom/peel/ui/ai;->f()V

    .line 150
    :goto_0
    invoke-super {p0}, Landroid/widget/BaseAdapter;->notifyDataSetChanged()V

    .line 151
    return-void

    .line 147
    :cond_0
    invoke-direct {p0}, Lcom/peel/ui/ai;->g()V

    goto :goto_0
.end method
