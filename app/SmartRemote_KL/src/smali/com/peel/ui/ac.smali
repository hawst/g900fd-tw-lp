.class Lcom/peel/ui/ac;
.super Landroid/widget/Filter;


# instance fields
.field final synthetic a:Lcom/peel/ui/aa;


# direct methods
.method constructor <init>(Lcom/peel/ui/aa;)V
    .locals 0

    .prologue
    .line 187
    iput-object p1, p0, Lcom/peel/ui/ac;->a:Lcom/peel/ui/aa;

    invoke-direct {p0}, Landroid/widget/Filter;-><init>()V

    return-void
.end method


# virtual methods
.method protected performFiltering(Ljava/lang/CharSequence;)Landroid/widget/Filter$FilterResults;
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v2, 0x0

    .line 190
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 192
    iget-object v0, p0, Lcom/peel/ui/ac;->a:Lcom/peel/ui/aa;

    invoke-static {v0}, Lcom/peel/ui/aa;->a(Lcom/peel/ui/aa;)Ljava/util/Set;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 193
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v4

    .line 194
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_4

    move v1, v2

    .line 195
    :goto_0
    iget-object v0, p0, Lcom/peel/ui/ac;->a:Lcom/peel/ui/aa;

    invoke-static {v0}, Lcom/peel/ui/aa;->b(Lcom/peel/ui/aa;)I

    move-result v0

    if-ge v1, v0, :cond_9

    .line 196
    iget-object v0, p0, Lcom/peel/ui/ac;->a:Lcom/peel/ui/aa;

    invoke-static {v0, v2}, Lcom/peel/ui/aa;->a(Lcom/peel/ui/aa;Z)Z

    .line 197
    iget-object v0, p0, Lcom/peel/ui/ac;->a:Lcom/peel/ui/aa;

    invoke-static {v0, v1}, Lcom/peel/ui/aa;->a(Lcom/peel/ui/aa;I)Lcom/peel/data/Channel;

    move-result-object v5

    .line 198
    invoke-virtual {v5}, Lcom/peel/data/Channel;->d()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 199
    invoke-virtual {v5}, Lcom/peel/data/Channel;->m()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 200
    invoke-virtual {v5}, Lcom/peel/data/Channel;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 201
    :cond_0
    invoke-virtual {v5}, Lcom/peel/data/Channel;->k()Z

    move-result v0

    if-nez v0, :cond_3

    .line 202
    iget-object v0, p0, Lcom/peel/ui/ac;->a:Lcom/peel/ui/aa;

    invoke-static {v0}, Lcom/peel/ui/aa;->a(Lcom/peel/ui/aa;)Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    .line 203
    :cond_1
    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 204
    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/data/Channel;

    .line 205
    invoke-virtual {v5}, Lcom/peel/data/Channel;->d()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0}, Lcom/peel/data/Channel;->d()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 206
    iget-object v0, p0, Lcom/peel/ui/ac;->a:Lcom/peel/ui/aa;

    invoke-static {v0, v8}, Lcom/peel/ui/aa;->a(Lcom/peel/ui/aa;Z)Z

    goto :goto_1

    .line 209
    :cond_2
    iget-object v0, p0, Lcom/peel/ui/ac;->a:Lcom/peel/ui/aa;

    invoke-static {v0}, Lcom/peel/ui/aa;->c(Lcom/peel/ui/aa;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 210
    invoke-interface {v3, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 195
    :cond_3
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_4
    move v1, v2

    .line 216
    :goto_2
    iget-object v0, p0, Lcom/peel/ui/ac;->a:Lcom/peel/ui/aa;

    invoke-static {v0}, Lcom/peel/ui/aa;->b(Lcom/peel/ui/aa;)I

    move-result v0

    if-ge v1, v0, :cond_9

    .line 217
    iget-object v0, p0, Lcom/peel/ui/ac;->a:Lcom/peel/ui/aa;

    invoke-static {v0, v2}, Lcom/peel/ui/aa;->a(Lcom/peel/ui/aa;Z)Z

    .line 218
    iget-object v0, p0, Lcom/peel/ui/ac;->a:Lcom/peel/ui/aa;

    invoke-static {v0, v1}, Lcom/peel/ui/aa;->a(Lcom/peel/ui/aa;I)Lcom/peel/data/Channel;

    move-result-object v5

    .line 219
    invoke-virtual {v5}, Lcom/peel/data/Channel;->d()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_5

    invoke-virtual {v5}, Lcom/peel/data/Channel;->m()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_5

    invoke-virtual {v5}, Lcom/peel/data/Channel;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 220
    :cond_5
    invoke-virtual {v5}, Lcom/peel/data/Channel;->k()Z

    move-result v0

    if-nez v0, :cond_8

    .line 221
    iget-object v0, p0, Lcom/peel/ui/ac;->a:Lcom/peel/ui/aa;

    invoke-static {v0}, Lcom/peel/ui/aa;->a(Lcom/peel/ui/aa;)Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    .line 222
    :cond_6
    :goto_3
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 223
    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/data/Channel;

    .line 224
    invoke-virtual {v5}, Lcom/peel/data/Channel;->d()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0}, Lcom/peel/data/Channel;->d()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 225
    iget-object v0, p0, Lcom/peel/ui/ac;->a:Lcom/peel/ui/aa;

    invoke-static {v0, v8}, Lcom/peel/ui/aa;->a(Lcom/peel/ui/aa;Z)Z

    goto :goto_3

    .line 228
    :cond_7
    iget-object v0, p0, Lcom/peel/ui/ac;->a:Lcom/peel/ui/aa;

    invoke-static {v0}, Lcom/peel/ui/aa;->c(Lcom/peel/ui/aa;)Z

    move-result v0

    if-nez v0, :cond_8

    .line 229
    invoke-interface {v3, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 216
    :cond_8
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 236
    :cond_9
    new-instance v0, Landroid/widget/Filter$FilterResults;

    invoke-direct {v0}, Landroid/widget/Filter$FilterResults;-><init>()V

    .line 237
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v1

    iput v1, v0, Landroid/widget/Filter$FilterResults;->count:I

    .line 238
    iput-object v3, v0, Landroid/widget/Filter$FilterResults;->values:Ljava/lang/Object;

    .line 240
    return-object v0
.end method

.method protected publishResults(Ljava/lang/CharSequence;Landroid/widget/Filter$FilterResults;)V
    .locals 3

    .prologue
    .line 245
    iget-object v1, p0, Lcom/peel/ui/ac;->a:Lcom/peel/ui/aa;

    iget-object v0, p2, Landroid/widget/Filter$FilterResults;->values:Ljava/lang/Object;

    check-cast v0, Ljava/util/List;

    invoke-static {v1, v0}, Lcom/peel/ui/aa;->a(Lcom/peel/ui/aa;Ljava/util/List;)Ljava/util/List;

    .line 246
    iget-object v0, p0, Lcom/peel/ui/ac;->a:Lcom/peel/ui/aa;

    iget-object v1, p0, Lcom/peel/ui/ac;->a:Lcom/peel/ui/aa;

    invoke-static {v1}, Lcom/peel/ui/aa;->d(Lcom/peel/ui/aa;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    new-array v1, v1, [Z

    invoke-static {v0, v1}, Lcom/peel/ui/aa;->a(Lcom/peel/ui/aa;[Z)[Z

    .line 248
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/peel/ui/ac;->a:Lcom/peel/ui/aa;

    invoke-static {v1}, Lcom/peel/ui/aa;->a(Lcom/peel/ui/aa;)Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 249
    iget-object v1, p0, Lcom/peel/ui/ac;->a:Lcom/peel/ui/aa;

    invoke-static {v1}, Lcom/peel/ui/aa;->e(Lcom/peel/ui/aa;)[Z

    move-result-object v1

    const/4 v2, 0x1

    aput-boolean v2, v1, v0

    .line 248
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 251
    :cond_0
    iget-object v0, p0, Lcom/peel/ui/ac;->a:Lcom/peel/ui/aa;

    invoke-virtual {v0}, Lcom/peel/ui/aa;->notifyDataSetChanged()V

    .line 252
    return-void
.end method
