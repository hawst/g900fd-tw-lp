.class public Lcom/peel/ui/lq;
.super Lcom/peel/d/u;


# static fields
.field private static aA:Z


# instance fields
.field private aB:Landroid/widget/RelativeLayout;

.field private final aC:Landroid/support/v4/view/cs;

.field private aD:Landroid/content/BroadcastReceiver;

.field private aE:Ljava/lang/Runnable;

.field private aj:Z

.field private ak:Landroid/widget/TextView;

.field private al:Landroid/view/LayoutInflater;

.field private am:Landroid/view/View;

.field private an:Landroid/content/SharedPreferences;

.field private ao:Landroid/widget/PopupWindow;

.field private ap:Ljava/util/Timer;

.field private aq:Ljava/util/TimerTask;

.field private ar:Z

.field private as:Landroid/widget/RelativeLayout;

.field private at:Lcom/peel/ui/a/a;

.field private au:Lcom/peel/ui/a/a;

.field private av:Lcom/peel/ui/a/p;

.field private aw:Ljava/lang/String;

.field private ax:I

.field private ay:Z

.field private az:Landroid/os/Handler;

.field private e:I

.field private f:Lcom/viewpagerindicator/TabPageIndicator;

.field private g:Lcom/peel/widget/CustomViewPager;

.field private h:Lcom/peel/util/dz;

.field private i:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 76
    invoke-direct {p0}, Lcom/peel/d/u;-><init>()V

    .line 78
    iput v0, p0, Lcom/peel/ui/lq;->e:I

    .line 82
    iput-boolean v0, p0, Lcom/peel/ui/lq;->i:Z

    .line 83
    iput-boolean v0, p0, Lcom/peel/ui/lq;->aj:Z

    .line 96
    const-string/jumbo v0, "topPicks"

    iput-object v0, p0, Lcom/peel/ui/lq;->aw:Ljava/lang/String;

    .line 97
    const/16 v0, 0x7d0

    iput v0, p0, Lcom/peel/ui/lq;->ax:I

    .line 105
    new-instance v0, Lcom/peel/ui/lr;

    invoke-direct {v0, p0}, Lcom/peel/ui/lr;-><init>(Lcom/peel/ui/lq;)V

    iput-object v0, p0, Lcom/peel/ui/lq;->aC:Landroid/support/v4/view/cs;

    .line 432
    new-instance v0, Lcom/peel/ui/mg;

    invoke-direct {v0, p0}, Lcom/peel/ui/mg;-><init>(Lcom/peel/ui/lq;)V

    iput-object v0, p0, Lcom/peel/ui/lq;->aD:Landroid/content/BroadcastReceiver;

    .line 956
    new-instance v0, Lcom/peel/ui/lx;

    invoke-direct {v0, p0}, Lcom/peel/ui/lx;-><init>(Lcom/peel/ui/lq;)V

    iput-object v0, p0, Lcom/peel/ui/lq;->aE:Ljava/lang/Runnable;

    return-void
.end method

.method private S()V
    .locals 5

    .prologue
    .line 127
    iget-object v0, p0, Lcom/peel/ui/lq;->g:Lcom/peel/widget/CustomViewPager;

    invoke-virtual {v0}, Lcom/peel/widget/CustomViewPager;->getAdapter()Landroid/support/v4/view/av;

    move-result-object v0

    check-cast v0, Lcom/peel/ui/ds;

    iget v1, p0, Lcom/peel/ui/lq;->e:I

    .line 128
    invoke-virtual {v0, v1}, Lcom/peel/ui/ds;->d(I)Lcom/peel/d/u;

    move-result-object v0

    .line 129
    instance-of v1, v0, Lcom/peel/ui/kr;

    if-eqz v1, :cond_1

    .line 130
    const-string/jumbo v1, "topPicks"

    iput-object v1, p0, Lcom/peel/ui/lq;->aw:Ljava/lang/String;

    .line 131
    const/16 v1, 0x7d0

    iput v1, p0, Lcom/peel/ui/lq;->ax:I

    .line 150
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/peel/ui/lq;->at:Lcom/peel/ui/a/a;

    iget-object v2, p0, Lcom/peel/ui/lq;->aw:Ljava/lang/String;

    iget v3, p0, Lcom/peel/ui/lq;->ax:I

    new-instance v4, Lcom/peel/ui/ly;

    invoke-direct {v4, p0, v0}, Lcom/peel/ui/ly;-><init>(Lcom/peel/ui/lq;Lcom/peel/d/u;)V

    invoke-virtual {v1, v2, v3, v4}, Lcom/peel/ui/a/a;->a(Ljava/lang/String;ILcom/peel/widget/r;)V

    .line 178
    return-void

    .line 132
    :cond_1
    instance-of v1, v0, Lcom/peel/ui/bw;

    if-eqz v1, :cond_0

    .line 133
    const-string/jumbo v1, "tvshows"

    iput-object v1, p0, Lcom/peel/ui/lq;->aw:Ljava/lang/String;

    .line 134
    iget v1, p0, Lcom/peel/ui/lq;->e:I

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 144
    :pswitch_0
    const-string/jumbo v1, "tvshows"

    iput-object v1, p0, Lcom/peel/ui/lq;->aw:Ljava/lang/String;

    .line 145
    const/16 v1, 0x7d1

    iput v1, p0, Lcom/peel/ui/lq;->ax:I

    goto :goto_0

    .line 136
    :pswitch_1
    const-string/jumbo v1, "sports"

    iput-object v1, p0, Lcom/peel/ui/lq;->aw:Ljava/lang/String;

    .line 137
    const/16 v1, 0x7d3

    iput v1, p0, Lcom/peel/ui/lq;->ax:I

    goto :goto_0

    .line 140
    :pswitch_2
    const-string/jumbo v1, "movies"

    iput-object v1, p0, Lcom/peel/ui/lq;->aw:Ljava/lang/String;

    .line 141
    const/16 v1, 0x7d2

    iput v1, p0, Lcom/peel/ui/lq;->ax:I

    goto :goto_0

    .line 134
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method private T()V
    .locals 8

    .prologue
    .line 565
    iget-object v0, p0, Lcom/peel/ui/lq;->b:Lcom/peel/d/i;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/peel/ui/lq;->b:Lcom/peel/d/i;

    const-string/jumbo v1, "time"

    invoke-virtual {v0, v1}, Lcom/peel/d/i;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    .line 568
    :goto_0
    if-nez v1, :cond_2

    .line 622
    :cond_0
    :goto_1
    return-void

    .line 565
    :cond_1
    const/4 v0, 0x0

    move-object v1, v0

    goto :goto_0

    .line 569
    :cond_2
    iget-object v0, p0, Lcom/peel/ui/lq;->ak:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 572
    :try_start_0
    sget-object v0, Lcom/peel/util/x;->e:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/text/SimpleDateFormat;

    invoke-virtual {v0, v1}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    .line 573
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v4

    .line 574
    invoke-virtual {v4, v2, v3}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 576
    new-instance v0, Ljava/util/GregorianCalendar;

    invoke-direct {v0}, Ljava/util/GregorianCalendar;-><init>()V

    .line 577
    invoke-virtual {v0, v2, v3}, Ljava/util/GregorianCalendar;->setTimeInMillis(J)V

    .line 579
    invoke-static {v1}, Lcom/peel/util/x;->a(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 580
    invoke-virtual {p0}, Lcom/peel/ui/lq;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    invoke-static {v0}, Landroid/text/format/DateFormat;->is24HourFormat(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 581
    sget-object v0, Lcom/peel/util/x;->g:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/text/SimpleDateFormat;

    invoke-virtual {v4}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    .line 582
    iget-object v2, p0, Lcom/peel/ui/lq;->ak:Landroid/widget/TextView;

    sget v3, Lcom/peel/ui/ft;->day_time_label:I

    invoke-virtual {p0, v3}, Lcom/peel/ui/lq;->a(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    const/4 v6, 0x0

    aget-object v1, v1, v6

    aput-object v1, v4, v5

    const/4 v1, 0x1

    aput-object v0, v4, v1

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 588
    :goto_2
    iget-boolean v0, p0, Lcom/peel/ui/lq;->aj:Z

    if-nez v0, :cond_3

    .line 589
    iget-object v0, p0, Lcom/peel/ui/lq;->ak:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 591
    :cond_3
    sget-object v0, Lcom/peel/c/a;->b:Lcom/peel/c/e;

    invoke-static {v0}, Lcom/peel/c/a;->a(Lcom/peel/c/e;)Ljava/lang/Object;

    move-result-object v0

    sget-object v1, Lcom/peel/c/b;->c:Lcom/peel/c/b;

    if-ne v0, v1, :cond_6

    .line 592
    iget-object v0, p0, Lcom/peel/ui/lq;->aB:Landroid/widget/RelativeLayout;

    new-instance v1, Lcom/peel/ui/mm;

    invoke-direct {v1, p0}, Lcom/peel/ui/mm;-><init>(Lcom/peel/ui/lq;)V

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 619
    :catch_0
    move-exception v0

    .line 620
    iget-object v1, p0, Lcom/peel/ui/lq;->a:Ljava/lang/String;

    iget-object v2, p0, Lcom/peel/ui/lq;->a:Ljava/lang/String;

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_1

    .line 585
    :cond_4
    :try_start_1
    iget-object v2, p0, Lcom/peel/ui/lq;->ak:Landroid/widget/TextView;

    sget v0, Lcom/peel/ui/ft;->day_time_label:I

    invoke-virtual {p0, v0}, Lcom/peel/ui/lq;->a(I)Ljava/lang/String;

    move-result-object v3

    const/4 v0, 0x2

    new-array v4, v0, [Ljava/lang/Object;

    const/4 v0, 0x0

    const/4 v5, 0x0

    aget-object v5, v1, v5

    aput-object v5, v4, v0

    const/4 v5, 0x1

    const/4 v0, 0x1

    aget-object v0, v1, v0

    const-string/jumbo v6, "AM"

    invoke-virtual {v0, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_5

    const/4 v0, 0x1

    aget-object v0, v1, v0

    const-string/jumbo v1, "AM"

    invoke-virtual {p0}, Lcom/peel/ui/lq;->n()Landroid/content/res/Resources;

    move-result-object v6

    sget v7, Lcom/peel/ui/ft;->time_am:I

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v1, v6}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_3
    aput-object v0, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2

    :cond_5
    const/4 v0, 0x1

    aget-object v0, v1, v0

    const-string/jumbo v1, "PM"

    invoke-virtual {p0}, Lcom/peel/ui/lq;->n()Landroid/content/res/Resources;

    move-result-object v6

    sget v7, Lcom/peel/ui/ft;->time_pm:I

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v1, v6}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_3

    .line 604
    :cond_6
    iget-object v0, p0, Lcom/peel/ui/lq;->ak:Landroid/widget/TextView;

    new-instance v1, Lcom/peel/ui/ls;

    invoke-direct {v1, p0}, Lcom/peel/ui/ls;-><init>(Lcom/peel/ui/lq;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V
    :try_end_1
    .catch Ljava/text/ParseException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_1
.end method

.method static synthetic a(Lcom/peel/ui/lq;I)I
    .locals 0

    .prologue
    .line 76
    iput p1, p0, Lcom/peel/ui/lq;->e:I

    return p1
.end method

.method static synthetic a(Lcom/peel/ui/lq;)Landroid/widget/PopupWindow;
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lcom/peel/ui/lq;->ao:Landroid/widget/PopupWindow;

    return-object v0
.end method

.method static synthetic a(Lcom/peel/ui/lq;Landroid/widget/PopupWindow;)Landroid/widget/PopupWindow;
    .locals 0

    .prologue
    .line 76
    iput-object p1, p0, Lcom/peel/ui/lq;->ao:Landroid/widget/PopupWindow;

    return-object p1
.end method

.method private a(Z)V
    .locals 3

    .prologue
    .line 742
    invoke-virtual {p0}, Lcom/peel/ui/lq;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/ae;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lcom/peel/ui/fq;->trigger_action_edit_channels:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 744
    if-eqz p1, :cond_0

    .line 745
    sget v0, Lcom/peel/ui/fp;->trigger_title:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    sget v2, Lcom/peel/ui/ft;->pending_remote:I

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    .line 746
    sget v0, Lcom/peel/ui/fp;->trigger_desc:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    sget v2, Lcom/peel/ui/ft;->pending_remote_desc:I

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    .line 749
    :cond_0
    iget-object v0, p0, Lcom/peel/ui/lq;->as:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 750
    iget-object v0, p0, Lcom/peel/ui/lq;->as:Landroid/widget/RelativeLayout;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 752
    sget v0, Lcom/peel/ui/fp;->iv_cancel_icon:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 753
    new-instance v2, Lcom/peel/ui/lt;

    invoke-direct {v2, p0, p1}, Lcom/peel/ui/lt;-><init>(Lcom/peel/ui/lq;Z)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 798
    new-instance v1, Lcom/peel/ui/lu;

    invoke-direct {v1, p0}, Lcom/peel/ui/lu;-><init>(Lcom/peel/ui/lq;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 829
    return-void
.end method

.method static synthetic a(Lcom/peel/ui/lq;Z)Z
    .locals 0

    .prologue
    .line 76
    iput-boolean p1, p0, Lcom/peel/ui/lq;->i:Z

    return p1
.end method

.method static synthetic b(Lcom/peel/ui/lq;)Lcom/peel/widget/CustomViewPager;
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lcom/peel/ui/lq;->g:Lcom/peel/widget/CustomViewPager;

    return-object v0
.end method

.method static synthetic b(Lcom/peel/ui/lq;Z)Z
    .locals 0

    .prologue
    .line 76
    iput-boolean p1, p0, Lcom/peel/ui/lq;->ar:Z

    return p1
.end method

.method static synthetic c(Lcom/peel/ui/lq;)V
    .locals 0

    .prologue
    .line 76
    invoke-direct {p0}, Lcom/peel/ui/lq;->S()V

    return-void
.end method

.method static synthetic c(Lcom/peel/ui/lq;Z)Z
    .locals 0

    .prologue
    .line 76
    iput-boolean p1, p0, Lcom/peel/ui/lq;->ay:Z

    return p1
.end method

.method static synthetic d(Lcom/peel/ui/lq;)I
    .locals 1

    .prologue
    .line 76
    iget v0, p0, Lcom/peel/ui/lq;->e:I

    return v0
.end method

.method static synthetic e(Lcom/peel/ui/lq;)Lcom/peel/ui/a/a;
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lcom/peel/ui/lq;->at:Lcom/peel/ui/a/a;

    return-object v0
.end method

.method static synthetic f(Lcom/peel/ui/lq;)Lcom/peel/ui/a/p;
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lcom/peel/ui/lq;->av:Lcom/peel/ui/a/p;

    return-object v0
.end method

.method static synthetic g(Lcom/peel/ui/lq;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lcom/peel/ui/lq;->aw:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic h(Lcom/peel/ui/lq;)I
    .locals 1

    .prologue
    .line 76
    iget v0, p0, Lcom/peel/ui/lq;->ax:I

    return v0
.end method

.method static synthetic i(Lcom/peel/ui/lq;)Z
    .locals 1

    .prologue
    .line 76
    iget-boolean v0, p0, Lcom/peel/ui/lq;->i:Z

    return v0
.end method

.method static synthetic j(Lcom/peel/ui/lq;)Z
    .locals 1

    .prologue
    .line 76
    iget-boolean v0, p0, Lcom/peel/ui/lq;->aj:Z

    return v0
.end method

.method static synthetic k(Lcom/peel/ui/lq;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lcom/peel/ui/lq;->a:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic l(Lcom/peel/ui/lq;)Landroid/view/LayoutInflater;
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lcom/peel/ui/lq;->al:Landroid/view/LayoutInflater;

    return-object v0
.end method

.method static synthetic m(Lcom/peel/ui/lq;)Landroid/view/View;
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lcom/peel/ui/lq;->am:Landroid/view/View;

    return-object v0
.end method

.method static synthetic n(Lcom/peel/ui/lq;)Landroid/content/SharedPreferences;
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lcom/peel/ui/lq;->an:Landroid/content/SharedPreferences;

    return-object v0
.end method

.method static synthetic o(Lcom/peel/ui/lq;)V
    .locals 0

    .prologue
    .line 76
    invoke-direct {p0}, Lcom/peel/ui/lq;->T()V

    return-void
.end method

.method static synthetic p(Lcom/peel/ui/lq;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lcom/peel/ui/lq;->ak:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic q(Lcom/peel/ui/lq;)Z
    .locals 1

    .prologue
    .line 76
    iget-boolean v0, p0, Lcom/peel/ui/lq;->ar:Z

    return v0
.end method

.method static synthetic r(Lcom/peel/ui/lq;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lcom/peel/ui/lq;->a:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic s(Lcom/peel/ui/lq;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lcom/peel/ui/lq;->a:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic t(Lcom/peel/ui/lq;)Landroid/widget/RelativeLayout;
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lcom/peel/ui/lq;->as:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method static synthetic u(Lcom/peel/ui/lq;)Z
    .locals 1

    .prologue
    .line 76
    iget-boolean v0, p0, Lcom/peel/ui/lq;->ay:Z

    return v0
.end method

.method static synthetic v(Lcom/peel/ui/lq;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lcom/peel/ui/lq;->az:Landroid/os/Handler;

    return-object v0
.end method


# virtual methods
.method public W()Z
    .locals 1

    .prologue
    .line 322
    const/4 v0, 0x1

    return v0
.end method

.method public X()Z
    .locals 1

    .prologue
    .line 181
    iget v0, p0, Lcom/peel/ui/lq;->e:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public Z()V
    .locals 6

    .prologue
    .line 925
    iget-object v0, p0, Lcom/peel/ui/lq;->d:Lcom/peel/d/a;

    if-nez v0, :cond_4

    .line 926
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 928
    iget-boolean v0, p0, Lcom/peel/ui/lq;->aj:Z

    if-nez v0, :cond_0

    sget-object v0, Lcom/peel/c/a;->b:Lcom/peel/c/e;

    invoke-static {v0}, Lcom/peel/c/a;->a(Lcom/peel/c/e;)Ljava/lang/Object;

    move-result-object v0

    sget-object v1, Lcom/peel/c/b;->c:Lcom/peel/c/b;

    if-ne v0, v1, :cond_1

    :cond_0
    sget-object v0, Lcom/peel/c/a;->b:Lcom/peel/c/e;

    .line 929
    invoke-static {v0}, Lcom/peel/c/a;->a(Lcom/peel/c/e;)Ljava/lang/Object;

    move-result-object v0

    sget-object v1, Lcom/peel/c/b;->b:Lcom/peel/c/b;

    if-ne v0, v1, :cond_2

    .line 930
    :cond_1
    sget v0, Lcom/peel/ui/fp;->menu_time:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 933
    :cond_2
    sget-object v0, Lcom/peel/c/a;->b:Lcom/peel/c/e;

    invoke-static {v0}, Lcom/peel/c/a;->a(Lcom/peel/c/e;)Ljava/lang/Object;

    move-result-object v0

    sget-object v1, Lcom/peel/c/b;->b:Lcom/peel/c/b;

    if-ne v0, v1, :cond_3

    .line 934
    sget v0, Lcom/peel/ui/fp;->overflow_menu_btn:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 937
    :cond_3
    sget-object v0, Lcom/peel/c/a;->b:Lcom/peel/c/e;

    invoke-static {v0}, Lcom/peel/c/a;->a(Lcom/peel/c/e;)Ljava/lang/Object;

    move-result-object v0

    sget-object v1, Lcom/peel/c/b;->a:Lcom/peel/c/b;

    if-ne v0, v1, :cond_5

    .line 938
    sget v0, Lcom/peel/ui/fp;->menu_remote:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 939
    sget v0, Lcom/peel/ui/fp;->menu_change_room:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 940
    sget v0, Lcom/peel/ui/fp;->menu_channels:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 941
    sget v0, Lcom/peel/ui/fp;->menu_settings:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 942
    sget v0, Lcom/peel/ui/fp;->menu_about:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 950
    :goto_0
    new-instance v0, Lcom/peel/d/a;

    sget-object v1, Lcom/peel/d/d;->b:Lcom/peel/d/d;

    sget-object v2, Lcom/peel/d/b;->a:Lcom/peel/d/b;

    sget-object v3, Lcom/peel/d/c;->b:Lcom/peel/d/c;

    sget v4, Lcom/peel/ui/ft;->browse:I

    invoke-virtual {p0, v4}, Lcom/peel/ui/lq;->a(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct/range {v0 .. v5}, Lcom/peel/d/a;-><init>(Lcom/peel/d/d;Lcom/peel/d/b;Lcom/peel/d/c;Ljava/lang/String;Ljava/util/List;)V

    iput-object v0, p0, Lcom/peel/ui/lq;->d:Lcom/peel/d/a;

    .line 953
    :cond_4
    iget-object v0, p0, Lcom/peel/ui/lq;->b:Lcom/peel/d/i;

    iget-object v1, p0, Lcom/peel/ui/lq;->d:Lcom/peel/d/a;

    invoke-virtual {v0, v1}, Lcom/peel/d/i;->a(Lcom/peel/d/a;)V

    .line 954
    return-void

    .line 943
    :cond_5
    sget-object v0, Lcom/peel/c/a;->b:Lcom/peel/c/e;

    invoke-static {v0}, Lcom/peel/c/a;->a(Lcom/peel/c/e;)Ljava/lang/Object;

    move-result-object v0

    sget-object v1, Lcom/peel/c/b;->c:Lcom/peel/c/b;

    if-ne v0, v1, :cond_6

    .line 944
    sget v0, Lcom/peel/ui/fp;->menu_change_room:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 945
    sget v0, Lcom/peel/ui/fp;->menu_remote:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 947
    :cond_6
    sget v0, Lcom/peel/ui/fp;->menu_remote:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 6

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 230
    sget v0, Lcom/peel/ui/fq;->ss_tiles_with_pager:I

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/ui/lq;->am:Landroid/view/View;

    .line 231
    iget-object v0, p0, Lcom/peel/ui/lq;->am:Landroid/view/View;

    sget v2, Lcom/peel/ui/fp;->pager:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/peel/widget/CustomViewPager;

    iput-object v0, p0, Lcom/peel/ui/lq;->g:Lcom/peel/widget/CustomViewPager;

    .line 232
    iget-object v0, p0, Lcom/peel/ui/lq;->am:Landroid/view/View;

    sget v2, Lcom/peel/ui/fp;->indicator:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/viewpagerindicator/TabPageIndicator;

    iput-object v0, p0, Lcom/peel/ui/lq;->f:Lcom/viewpagerindicator/TabPageIndicator;

    .line 233
    iget-object v0, p0, Lcom/peel/ui/lq;->am:Landroid/view/View;

    sget v2, Lcom/peel/ui/fp;->triggers_placeholder:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/peel/ui/lq;->as:Landroid/widget/RelativeLayout;

    .line 234
    new-instance v0, Lcom/peel/ui/a/a;

    invoke-virtual {p0}, Lcom/peel/ui/lq;->m()Landroid/support/v4/app/ae;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/peel/ui/a/a;-><init>(Landroid/app/Activity;)V

    iput-object v0, p0, Lcom/peel/ui/lq;->at:Lcom/peel/ui/a/a;

    .line 235
    new-instance v0, Lcom/peel/ui/a/a;

    invoke-virtual {p0}, Lcom/peel/ui/lq;->m()Landroid/support/v4/app/ae;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/peel/ui/a/a;-><init>(Landroid/app/Activity;)V

    iput-object v0, p0, Lcom/peel/ui/lq;->au:Lcom/peel/ui/a/a;

    .line 236
    iget-object v0, p0, Lcom/peel/ui/lq;->g:Lcom/peel/widget/CustomViewPager;

    invoke-virtual {v0, v1}, Lcom/peel/widget/CustomViewPager;->setPagingEnabled(Z)V

    .line 237
    iput-object p1, p0, Lcom/peel/ui/lq;->al:Landroid/view/LayoutInflater;

    .line 238
    invoke-virtual {p0}, Lcom/peel/ui/lq;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/ui/lq;->an:Landroid/content/SharedPreferences;

    .line 240
    sget-object v0, Lcom/peel/c/a;->b:Lcom/peel/c/e;

    invoke-static {v0}, Lcom/peel/c/a;->a(Lcom/peel/c/e;)Ljava/lang/Object;

    move-result-object v0

    sget-object v2, Lcom/peel/c/b;->c:Lcom/peel/c/b;

    if-ne v0, v2, :cond_2

    .line 241
    iput-boolean v1, p0, Lcom/peel/ui/lq;->aj:Z

    .line 242
    iput-boolean v1, p0, Lcom/peel/ui/lq;->i:Z

    .line 251
    :goto_0
    if-eqz p3, :cond_0

    .line 252
    iget-object v0, p0, Lcom/peel/ui/lq;->c:Landroid/os/Bundle;

    invoke-virtual {v0, p3}, Landroid/os/Bundle;->putAll(Landroid/os/Bundle;)V

    .line 255
    :cond_0
    iget-object v0, p0, Lcom/peel/ui/lq;->c:Landroid/os/Bundle;

    const-string/jumbo v2, "refresh"

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 256
    iget-object v0, p0, Lcom/peel/ui/lq;->g:Lcom/peel/widget/CustomViewPager;

    new-instance v2, Lcom/peel/ui/ds;

    invoke-virtual {p0}, Lcom/peel/ui/lq;->p()Landroid/support/v4/app/aj;

    move-result-object v3

    invoke-virtual {p0}, Lcom/peel/ui/lq;->m()Landroid/support/v4/app/ae;

    move-result-object v4

    const/4 v5, 0x0

    invoke-direct {v2, v3, v4, v5}, Lcom/peel/ui/ds;-><init>(Landroid/support/v4/app/aj;Landroid/content/Context;Ljava/util/List;)V

    invoke-virtual {v0, v2}, Lcom/peel/widget/CustomViewPager;->setAdapter(Landroid/support/v4/view/av;)V

    .line 258
    iget-object v0, p0, Lcom/peel/ui/lq;->f:Lcom/viewpagerindicator/TabPageIndicator;

    iget-object v2, p0, Lcom/peel/ui/lq;->g:Lcom/peel/widget/CustomViewPager;

    invoke-virtual {v0, v2}, Lcom/viewpagerindicator/TabPageIndicator;->setViewPager(Landroid/support/v4/view/ViewPager;)V

    .line 259
    iget-object v0, p0, Lcom/peel/ui/lq;->f:Lcom/viewpagerindicator/TabPageIndicator;

    iget-object v2, p0, Lcom/peel/ui/lq;->aC:Landroid/support/v4/view/cs;

    invoke-virtual {v0, v2}, Lcom/viewpagerindicator/TabPageIndicator;->setOnPageChangeListener(Landroid/support/v4/view/cs;)V

    .line 261
    iget-object v0, p0, Lcom/peel/ui/lq;->b:Lcom/peel/d/i;

    if-nez v0, :cond_4

    move v0, v1

    :goto_1
    iput v0, p0, Lcom/peel/ui/lq;->e:I

    .line 264
    invoke-static {}, Lcom/peel/widget/ag;->g()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 266
    invoke-static {}, Lcom/peel/widget/ag;->h()V

    .line 268
    :cond_1
    iget-object v0, p0, Lcom/peel/ui/lq;->am:Landroid/view/View;

    return-object v0

    .line 243
    :cond_2
    invoke-virtual {p0}, Lcom/peel/ui/lq;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string/jumbo v2, "yosemite_enabled"

    invoke-interface {v0, v2, v1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 244
    iput-boolean v3, p0, Lcom/peel/ui/lq;->aj:Z

    .line 245
    iput-boolean v3, p0, Lcom/peel/ui/lq;->i:Z

    goto :goto_0

    .line 247
    :cond_3
    iget-object v0, p0, Lcom/peel/ui/lq;->an:Landroid/content/SharedPreferences;

    const-string/jumbo v2, "yosemite_available"

    invoke-interface {v0, v2, v1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/peel/ui/lq;->aj:Z

    goto :goto_0

    .line 261
    :cond_4
    iget-object v0, p0, Lcom/peel/ui/lq;->b:Lcom/peel/d/i;

    const-string/jumbo v2, "selected_genre0"

    invoke-virtual {v0, v2, v1}, Lcom/peel/d/i;->a(Ljava/lang/String;I)I

    move-result v0

    goto :goto_1
.end method

.method public a(IILandroid/content/Intent;)V
    .locals 2

    .prologue
    .line 914
    invoke-super {p0, p1, p2, p3}, Lcom/peel/d/u;->a(IILandroid/content/Intent;)V

    .line 915
    invoke-virtual {p0}, Lcom/peel/ui/lq;->p()Landroid/support/v4/app/aj;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/aj;->d()Ljava/util/List;

    move-result-object v0

    .line 916
    if-eqz v0, :cond_0

    .line 917
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/Fragment;

    .line 918
    invoke-virtual {v0, p1, p2, p3}, Landroid/support/v4/app/Fragment;->a(IILandroid/content/Intent;)V

    goto :goto_0

    .line 921
    :cond_0
    return-void
.end method

.method public a(Landroid/view/Menu;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 327
    invoke-virtual {p0}, Lcom/peel/ui/lq;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    check-cast v0, Landroid/support/v7/app/ActionBarActivity;

    invoke-virtual {v0}, Landroid/support/v7/app/ActionBarActivity;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v1

    .line 328
    sget-object v0, Lcom/peel/c/a;->b:Lcom/peel/c/e;

    invoke-static {v0}, Lcom/peel/c/a;->a(Lcom/peel/c/e;)Ljava/lang/Object;

    move-result-object v0

    sget-object v2, Lcom/peel/c/b;->c:Lcom/peel/c/b;

    if-ne v0, v2, :cond_1

    .line 329
    sget v0, Lcom/peel/ui/fq;->custom_action_layout:I

    invoke-virtual {v1, v0}, Landroid/support/v7/app/ActionBar;->setCustomView(I)V

    .line 343
    :goto_0
    invoke-virtual {v1, v4}, Landroid/support/v7/app/ActionBar;->setDisplayShowCustomEnabled(Z)V

    .line 344
    invoke-virtual {v1, v4}, Landroid/support/v7/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 345
    invoke-virtual {v1, v4}, Landroid/support/v7/app/ActionBar;->setHomeButtonEnabled(Z)V

    .line 346
    invoke-virtual {v1, v3}, Landroid/support/v7/app/ActionBar;->setNavigationMode(I)V

    .line 347
    invoke-virtual {v1, v3}, Landroid/support/v7/app/ActionBar;->setDisplayShowTitleEnabled(Z)V

    .line 348
    invoke-virtual {p0}, Lcom/peel/ui/lq;->n()Landroid/content/res/Resources;

    move-result-object v0

    sget v2, Lcom/peel/ui/fo;->action_bar_background:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/support/v7/app/ActionBar;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 350
    sget-object v0, Lcom/peel/c/a;->b:Lcom/peel/c/e;

    invoke-static {v0}, Lcom/peel/c/a;->a(Lcom/peel/c/e;)Ljava/lang/Object;

    move-result-object v0

    sget-object v2, Lcom/peel/c/b;->a:Lcom/peel/c/b;

    if-ne v0, v2, :cond_3

    .line 351
    invoke-virtual {v1, v4}, Landroid/support/v7/app/ActionBar;->setDisplayShowHomeEnabled(Z)V

    .line 360
    :goto_1
    invoke-virtual {v1}, Landroid/support/v7/app/ActionBar;->show()V

    .line 362
    invoke-virtual {v1}, Landroid/support/v7/app/ActionBar;->getCustomView()Landroid/view/View;

    move-result-object v0

    sget v2, Lcom/peel/ui/fp;->time_label:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/peel/ui/lq;->ak:Landroid/widget/TextView;

    .line 364
    iget-boolean v0, p0, Lcom/peel/ui/lq;->aj:Z

    if-eqz v0, :cond_0

    .line 365
    invoke-virtual {v1}, Landroid/support/v7/app/ActionBar;->getCustomView()Landroid/view/View;

    move-result-object v0

    sget v2, Lcom/peel/ui/fp;->prism_txt:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    new-instance v2, Lcom/peel/ui/mc;

    invoke-direct {v2, p0}, Lcom/peel/ui/mc;-><init>(Lcom/peel/ui/lq;)V

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 414
    invoke-virtual {v1}, Landroid/support/v7/app/ActionBar;->getCustomView()Landroid/view/View;

    move-result-object v0

    sget v1, Lcom/peel/ui/fp;->on_air_txt:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    new-instance v1, Lcom/peel/ui/mf;

    invoke-direct {v1, p0}, Lcom/peel/ui/mf;-><init>(Lcom/peel/ui/lq;)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 429
    :cond_0
    invoke-direct {p0}, Lcom/peel/ui/lq;->T()V

    .line 430
    return-void

    .line 330
    :cond_1
    iget-boolean v0, p0, Lcom/peel/ui/lq;->aj:Z

    if-eqz v0, :cond_2

    .line 331
    sget v0, Lcom/peel/ui/fq;->ss_wataction:I

    invoke-virtual {v1, v0}, Landroid/support/v7/app/ActionBar;->setCustomView(I)V

    goto :goto_0

    .line 333
    :cond_2
    sget v0, Lcom/peel/ui/fq;->custom_layout:I

    invoke-virtual {v1, v0}, Landroid/support/v7/app/ActionBar;->setCustomView(I)V

    goto :goto_0

    .line 352
    :cond_3
    sget-object v0, Lcom/peel/c/a;->b:Lcom/peel/c/e;

    invoke-static {v0}, Lcom/peel/c/a;->a(Lcom/peel/c/e;)Ljava/lang/Object;

    move-result-object v0

    sget-object v2, Lcom/peel/c/b;->c:Lcom/peel/c/b;

    if-ne v0, v2, :cond_4

    .line 353
    invoke-virtual {v1, v3}, Landroid/support/v7/app/ActionBar;->setDisplayShowHomeEnabled(Z)V

    .line 354
    invoke-virtual {v1}, Landroid/support/v7/app/ActionBar;->getCustomView()Landroid/view/View;

    move-result-object v0

    sget v2, Lcom/peel/ui/fp;->time_label_layout:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/peel/ui/lq;->aB:Landroid/widget/RelativeLayout;

    goto :goto_1

    .line 357
    :cond_4
    invoke-virtual {v1, v3}, Landroid/support/v7/app/ActionBar;->setDisplayShowHomeEnabled(Z)V

    goto :goto_1
.end method

.method public a_(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 185
    invoke-super {p0, p1}, Lcom/peel/d/u;->a_(Landroid/os/Bundle;)V

    .line 186
    new-instance v0, Lcom/peel/util/dz;

    iget-object v1, p0, Lcom/peel/ui/lq;->b:Lcom/peel/d/i;

    const-string/jumbo v2, "live"

    invoke-direct {v0, v1, v2}, Lcom/peel/util/dz;-><init>(Lcom/peel/d/i;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/peel/ui/lq;->h:Lcom/peel/util/dz;

    .line 187
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/peel/ui/lq;->az:Landroid/os/Handler;

    .line 189
    new-instance v0, Lcom/peel/ui/lz;

    invoke-direct {v0, p0}, Lcom/peel/ui/lz;-><init>(Lcom/peel/ui/lq;)V

    iput-object v0, p0, Lcom/peel/ui/lq;->av:Lcom/peel/ui/a/p;

    .line 226
    return-void
.end method

.method public b()Z
    .locals 3

    .prologue
    const v2, 0x800003

    .line 635
    invoke-virtual {p0}, Lcom/peel/ui/lq;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    sget v1, Lcom/peel/ui/fp;->drawer_layout:I

    invoke-virtual {v0, v1}, Landroid/support/v4/app/ae;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/widget/DrawerLayout;

    invoke-virtual {v0, v2}, Landroid/support/v4/widget/DrawerLayout;->g(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 636
    invoke-virtual {p0}, Lcom/peel/ui/lq;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    sget v1, Lcom/peel/ui/fp;->drawer_layout:I

    invoke-virtual {v0, v1}, Landroid/support/v4/app/ae;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/widget/DrawerLayout;

    invoke-virtual {v0, v2}, Landroid/support/v4/widget/DrawerLayout;->f(I)V

    .line 637
    const/4 v0, 0x1

    .line 639
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c()Lcom/peel/util/dz;
    .locals 1

    .prologue
    .line 909
    iget-object v0, p0, Lcom/peel/ui/lq;->h:Lcom/peel/util/dz;

    return-object v0
.end method

.method public c(Landroid/os/Bundle;)V
    .locals 12

    .prologue
    const/4 v11, 0x1

    const/4 v2, 0x0

    .line 652
    invoke-super {p0, p1}, Lcom/peel/d/u;->c(Landroid/os/Bundle;)V

    .line 654
    sget-object v0, Lcom/peel/content/a;->h:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/peel/ui/lq;->b:Lcom/peel/d/i;

    if-nez v0, :cond_1

    .line 738
    :cond_0
    :goto_0
    return-void

    .line 661
    :cond_1
    sget-boolean v0, Lcom/peel/ui/kj;->g:Z

    if-eqz v0, :cond_7

    .line 662
    const-string/jumbo v0, "refresh_fragments"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 663
    const-string/jumbo v0, "selective"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v5

    .line 665
    const-string/jumbo v0, "refresh_fragments"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->remove(Ljava/lang/String;)V

    .line 666
    const-string/jumbo v0, "refresh"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->remove(Ljava/lang/String;)V

    .line 667
    const-string/jumbo v0, "selective"

    invoke-virtual {p1, v0, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 668
    sput-boolean v2, Lcom/peel/ui/kj;->g:Z

    .line 670
    iget-boolean v0, p0, Lcom/peel/ui/lq;->aj:Z

    if-eqz v0, :cond_2

    .line 671
    invoke-direct {p0}, Lcom/peel/ui/lq;->T()V

    .line 673
    :cond_2
    iget-object v0, p0, Lcom/peel/ui/lq;->g:Lcom/peel/widget/CustomViewPager;

    invoke-virtual {v0}, Lcom/peel/widget/CustomViewPager;->getAdapter()Landroid/support/v4/view/av;

    move-result-object v0

    check-cast v0, Lcom/peel/ui/ds;

    move v1, v2

    .line 674
    :goto_1
    invoke-virtual {v0}, Lcom/peel/ui/ds;->b()I

    move-result v3

    if-ge v1, v3, :cond_8

    .line 675
    invoke-virtual {v0, v1}, Lcom/peel/ui/ds;->d(I)Lcom/peel/d/u;

    move-result-object v6

    .line 676
    if-nez v6, :cond_4

    .line 674
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 678
    :cond_4
    array-length v7, v4

    move v3, v2

    :goto_2
    if-ge v3, v7, :cond_3

    aget-object v8, v4, v3

    .line 679
    invoke-virtual {v6}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9, v8}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_5

    .line 678
    :goto_3
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 680
    :cond_5
    iget-object v8, v6, Lcom/peel/d/u;->c:Landroid/os/Bundle;

    const-string/jumbo v9, "selective"

    invoke-virtual {v8, v9, v5}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 681
    iget-object v8, v6, Lcom/peel/d/u;->c:Landroid/os/Bundle;

    const-string/jumbo v9, "refresh"

    invoke-virtual {v8, v9, v11}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 682
    const-string/jumbo v8, "forcetime"

    invoke-virtual {p1, v8}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    if-eqz v8, :cond_6

    .line 683
    iget-object v8, p0, Lcom/peel/ui/lq;->a:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v10, "forcetime mofo: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string/jumbo v10, "forcetime"

    invoke-virtual {p1, v10}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 684
    iget-object v8, v6, Lcom/peel/d/u;->c:Landroid/os/Bundle;

    const-string/jumbo v9, "forcetime"

    const-string/jumbo v10, "forcetime"

    invoke-virtual {p1, v10}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v9, v10}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 686
    :cond_6
    iget-object v8, v6, Lcom/peel/d/u;->c:Landroid/os/Bundle;

    invoke-virtual {v6, v8}, Lcom/peel/d/u;->c(Landroid/os/Bundle;)V

    goto :goto_3

    .line 689
    :cond_7
    const-string/jumbo v0, "refresh"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-ne v11, v0, :cond_8

    .line 691
    const-string/jumbo v0, "refresh"

    invoke-virtual {p1, v0, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 693
    iget-object v0, p0, Lcom/peel/ui/lq;->g:Lcom/peel/widget/CustomViewPager;

    invoke-virtual {v0}, Lcom/peel/widget/CustomViewPager;->getAdapter()Landroid/support/v4/view/av;

    move-result-object v0

    check-cast v0, Lcom/peel/ui/ds;

    .line 695
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 697
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 698
    const-string/jumbo v4, "title"

    sget v5, Lcom/peel/ui/ft;->toppicks:I

    invoke-virtual {p0, v5}, Lcom/peel/ui/lq;->a(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 699
    const-string/jumbo v4, "fragment"

    const-class v5, Lcom/peel/ui/kr;

    invoke-virtual {v5}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 700
    const-string/jumbo v4, "context_id"

    const/16 v5, 0x7d0

    invoke-virtual {v3, v4, v5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 701
    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 703
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 704
    const-string/jumbo v4, "title"

    sget v5, Lcom/peel/ui/ft;->tvshows:I

    invoke-virtual {p0, v5}, Lcom/peel/ui/lq;->a(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 705
    const-string/jumbo v4, "fragment"

    const-class v5, Lcom/peel/ui/ci;

    invoke-virtual {v5}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 706
    const-string/jumbo v4, "scheme"

    const-string/jumbo v5, "live"

    invoke-virtual {v3, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 707
    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 709
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 710
    const-string/jumbo v4, "title"

    sget v5, Lcom/peel/ui/ft;->movies:I

    invoke-virtual {p0, v5}, Lcom/peel/ui/lq;->a(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 711
    const-string/jumbo v4, "fragment"

    const-class v5, Lcom/peel/ui/cg;

    invoke-virtual {v5}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 712
    const-string/jumbo v4, "scheme"

    const-string/jumbo v5, "live"

    invoke-virtual {v3, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 713
    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 715
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 716
    const-string/jumbo v4, "title"

    sget v5, Lcom/peel/ui/ft;->sports:I

    invoke-virtual {p0, v5}, Lcom/peel/ui/lq;->a(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 717
    const-string/jumbo v4, "fragment"

    const-class v5, Lcom/peel/ui/ch;

    invoke-virtual {v5}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 718
    const-string/jumbo v4, "scheme"

    const-string/jumbo v5, "live"

    invoke-virtual {v3, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 719
    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 721
    invoke-virtual {v0, v1}, Lcom/peel/ui/ds;->a(Ljava/util/List;)V

    .line 723
    iget-object v0, p0, Lcom/peel/ui/lq;->f:Lcom/viewpagerindicator/TabPageIndicator;

    invoke-virtual {v0}, Lcom/viewpagerindicator/TabPageIndicator;->a()V

    .line 726
    :cond_8
    iget-object v0, p0, Lcom/peel/ui/lq;->f:Lcom/viewpagerindicator/TabPageIndicator;

    iget v1, p0, Lcom/peel/ui/lq;->e:I

    invoke-virtual {v0, v1}, Lcom/viewpagerindicator/TabPageIndicator;->setCurrentItem(I)V

    .line 728
    iget-object v0, p0, Lcom/peel/ui/lq;->b:Lcom/peel/d/i;

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/peel/ui/lq;->b:Lcom/peel/d/i;

    const-string/jumbo v1, "time"

    invoke-virtual {v0, v1}, Lcom/peel/d/i;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_9

    .line 729
    iget-object v0, p0, Lcom/peel/ui/lq;->b:Lcom/peel/d/i;

    const-string/jumbo v1, "time"

    invoke-static {}, Lcom/peel/util/x;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v3}, Lcom/peel/d/i;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 732
    :cond_9
    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v0}, Lcom/peel/control/am;->e()[Lcom/peel/control/h;

    move-result-object v0

    .line 733
    if-nez v0, :cond_a

    sget-object v0, Lcom/peel/util/a;->j:Ljava/lang/String;

    if-eqz v0, :cond_a

    sget-object v0, Lcom/peel/util/a;->j:Ljava/lang/String;

    sget-object v1, Lcom/peel/util/a;->c:[Ljava/lang/String;

    const/4 v3, 0x2

    aget-object v1, v1, v3

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 734
    invoke-direct {p0, v11}, Lcom/peel/ui/lq;->a(Z)V

    goto/16 :goto_0

    .line 735
    :cond_a
    invoke-virtual {p0}, Lcom/peel/ui/lq;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    const-string/jumbo v1, "edit_channel_popup_trigger"

    invoke-static {v0, v1}, Lcom/peel/util/dq;->b(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 736
    invoke-direct {p0, v2}, Lcom/peel/ui/lq;->a(Z)V

    goto/16 :goto_0
.end method

.method public d(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 273
    invoke-super {p0, p1}, Lcom/peel/d/u;->d(Landroid/os/Bundle;)V

    .line 275
    invoke-virtual {p0}, Lcom/peel/ui/lq;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/ae;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/peel/social/w;->f(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/peel/util/dq;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/peel/ui/lq;->aA:Z

    if-nez v0, :cond_0

    .line 276
    const/4 v0, 0x1

    sput-boolean v0, Lcom/peel/ui/lq;->aA:Z

    .line 277
    invoke-virtual {p0}, Lcom/peel/ui/lq;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    const/16 v1, 0xbf4

    const/16 v2, 0x7d0

    new-instance v3, Lcom/peel/ui/mb;

    invoke-direct {v3, p0}, Lcom/peel/ui/mb;-><init>(Lcom/peel/ui/lq;)V

    invoke-static {v0, v1, v2, v4, v3}, Lcom/peel/social/e;->a(Landroid/app/Activity;IIZLcom/peel/util/t;)V

    .line 305
    :cond_0
    iget-object v0, p0, Lcom/peel/ui/lq;->b:Lcom/peel/d/i;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/peel/ui/lq;->b:Lcom/peel/d/i;

    const-string/jumbo v1, "slideout_selected_id"

    sget v2, Lcom/peel/ui/fp;->menu_settings:I

    invoke-virtual {v0, v1, v2}, Lcom/peel/d/i;->a(Ljava/lang/String;I)I

    move-result v0

    sget v1, Lcom/peel/ui/fp;->menu_browse:I

    if-eq v0, v1, :cond_1

    .line 307
    invoke-virtual {p0}, Lcom/peel/ui/lq;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/ae;->getSupportFragmentManager()Landroid/support/v4/app/aj;

    move-result-object v0

    sget v1, Lcom/peel/ui/fp;->drawer:I

    invoke-virtual {v0, v1}, Landroid/support/v4/app/aj;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 308
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 309
    const-string/jumbo v0, "drawer_open"

    invoke-virtual {v1, v0, v4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 310
    iget-object v0, p0, Lcom/peel/ui/lq;->b:Lcom/peel/d/i;

    const-string/jumbo v2, "slideout_selected_id"

    invoke-virtual {v0, v2}, Lcom/peel/d/i;->d(Ljava/lang/String;)V

    .line 311
    invoke-virtual {p0}, Lcom/peel/ui/lq;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/ae;->getSupportFragmentManager()Landroid/support/v4/app/aj;

    move-result-object v0

    sget v2, Lcom/peel/ui/fp;->drawer:I

    invoke-virtual {v0, v2}, Landroid/support/v4/app/aj;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/peel/d/u;

    invoke-virtual {v0, v1}, Lcom/peel/d/u;->c(Landroid/os/Bundle;)V

    .line 315
    :cond_1
    sget-object v0, Lcom/peel/content/a;->h:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 316
    iget-object v0, p0, Lcom/peel/ui/lq;->c:Landroid/os/Bundle;

    invoke-virtual {p0, v0}, Lcom/peel/ui/lq;->c(Landroid/os/Bundle;)V

    .line 318
    :cond_2
    return-void
.end method

.method public e(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 905
    iget-object v0, p0, Lcom/peel/ui/lq;->c:Landroid/os/Bundle;

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->putAll(Landroid/os/Bundle;)V

    .line 906
    invoke-super {p0, p1}, Lcom/peel/d/u;->e(Landroid/os/Bundle;)V

    .line 907
    return-void
.end method

.method public f()V
    .locals 0

    .prologue
    .line 647
    invoke-super {p0}, Lcom/peel/d/u;->f()V

    .line 648
    return-void
.end method

.method public g()V
    .locals 3

    .prologue
    .line 899
    iget-object v0, p0, Lcom/peel/ui/lq;->b:Lcom/peel/d/i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/peel/ui/lq;->b:Lcom/peel/d/i;

    const-string/jumbo v1, "selected_genre0"

    iget v2, p0, Lcom/peel/ui/lq;->e:I

    invoke-virtual {v0, v1, v2}, Lcom/peel/d/i;->b(Ljava/lang/String;I)V

    .line 900
    :cond_0
    invoke-super {p0}, Lcom/peel/d/u;->g()V

    .line 901
    return-void
.end method

.method public h(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 626
    invoke-super {p0, p1}, Lcom/peel/d/u;->h(Landroid/os/Bundle;)V

    .line 627
    iget-object v1, p0, Lcom/peel/ui/lq;->b:Lcom/peel/d/i;

    if-nez v1, :cond_0

    :goto_0
    iput v0, p0, Lcom/peel/ui/lq;->e:I

    .line 628
    invoke-virtual {p0}, Lcom/peel/ui/lq;->Z()V

    .line 629
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/peel/ui/lq;->ay:Z

    .line 630
    iget-object v0, p0, Lcom/peel/ui/lq;->az:Landroid/os/Handler;

    iget-object v1, p0, Lcom/peel/ui/lq;->aE:Ljava/lang/Runnable;

    const-wide/16 v2, 0x190

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 631
    return-void

    .line 627
    :cond_0
    iget-object v1, p0, Lcom/peel/ui/lq;->b:Lcom/peel/d/i;

    const-string/jumbo v2, "selected_genre0"

    invoke-virtual {v1, v2, v0}, Lcom/peel/d/i;->a(Ljava/lang/String;I)I

    move-result v0

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 8

    .prologue
    const-wide/16 v6, 0x32

    const/4 v1, 0x0

    const/4 v2, 0x1

    const/4 v0, 0x0

    .line 833
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v3

    .line 834
    sget v4, Lcom/peel/ui/fp;->prism_txt:I

    if-ne v3, v4, :cond_7

    .line 835
    iget-boolean v1, p0, Lcom/peel/ui/lq;->i:Z

    if-eqz v1, :cond_2

    .line 836
    const-class v0, Lcom/peel/ui/lq;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "launch yosemite"

    new-instance v3, Lcom/peel/ui/lv;

    invoke-direct {v3, p0}, Lcom/peel/ui/lv;-><init>(Lcom/peel/ui/lq;)V

    invoke-static {v0, v1, v3, v6, v7}, Lcom/peel/util/i;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;J)Ljava/lang/Runnable;

    .line 874
    :cond_0
    :goto_0
    :try_start_0
    invoke-static {}, Lcom/peel/util/a/f;->a()Lcom/peel/util/a/f;

    move-result-object v0

    sget-object v1, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v1}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v1

    if-nez v1, :cond_5

    move v1, v2

    :goto_1
    const/16 v2, 0x1f4a

    const/16 v3, 0x7db

    invoke-virtual {p0}, Lcom/peel/ui/lq;->m()Landroid/support/v4/app/ae;

    move-result-object v4

    invoke-static {v4}, Lcom/peel/util/bx;->d(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_6

    const-string/jumbo v4, "XX"

    :goto_2
    const/4 v5, 0x0

    sget-object v6, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const/4 v7, 0x0

    invoke-virtual/range {v0 .. v7}, Lcom/peel/util/a/f;->a(IIILjava/lang/String;ILjava/lang/String;I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    .line 895
    :cond_1
    :goto_3
    return-void

    .line 844
    :cond_2
    iget-boolean v1, p0, Lcom/peel/ui/lq;->aj:Z

    if-eqz v1, :cond_0

    .line 846
    :try_start_1
    invoke-virtual {p0}, Lcom/peel/ui/lq;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    const-string/jumbo v3, "com.sec.yosemite.phone"

    invoke-static {v1, v3}, Lcom/peel/util/bx;->c(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/peel/ui/lq;->i:Z

    .line 848
    iget-boolean v1, p0, Lcom/peel/ui/lq;->i:Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    if-nez v1, :cond_3

    .line 850
    :try_start_2
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "android.intent.action.VIEW"

    const-string/jumbo v3, "market://details?id=com.sec.yosemite.phone"

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-direct {v0, v1, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 851
    const/high16 v1, 0x40080000    # 2.125f

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 852
    invoke-virtual {p0, v0}, Lcom/peel/ui/lq;->a(Landroid/content/Intent;)V
    :try_end_2
    .catch Landroid/content/ActivityNotFoundException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_0

    .line 853
    :catch_0
    move-exception v0

    .line 854
    :try_start_3
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "android.intent.action.VIEW"

    const-string/jumbo v3, "http://play.google.com/store/apps/details?id=com.sec.yosemite.phone"

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-direct {v0, v1, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 855
    const/high16 v1, 0x40080000    # 2.125f

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 856
    invoke-virtual {p0, v0}, Lcom/peel/ui/lq;->a(Landroid/content/Intent;)V

    goto :goto_0

    .line 871
    :catch_1
    move-exception v0

    goto :goto_0

    .line 859
    :cond_3
    invoke-virtual {p0}, Lcom/peel/ui/lq;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    invoke-static {v1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string/jumbo v3, "yosemite_enabled"

    iget-boolean v4, p0, Lcom/peel/ui/lq;->i:Z

    invoke-interface {v1, v3, v4}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 860
    iget-object v1, p0, Lcom/peel/ui/lq;->b:Lcom/peel/d/i;

    const-string/jumbo v3, "yosemite_enabled"

    iget-boolean v4, p0, Lcom/peel/ui/lq;->i:Z

    if-eqz v4, :cond_4

    move v0, v2

    :cond_4
    invoke-virtual {v1, v3, v0}, Lcom/peel/d/i;->b(Ljava/lang/String;I)V

    .line 862
    const-class v0, Lcom/peel/ui/lq;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "launch yosemite"

    new-instance v3, Lcom/peel/ui/lw;

    invoke-direct {v3, p0}, Lcom/peel/ui/lw;-><init>(Lcom/peel/ui/lq;)V

    const-wide/16 v4, 0x32

    invoke-static {v0, v1, v3, v4, v5}, Lcom/peel/util/i;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;J)Ljava/lang/Runnable;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    goto/16 :goto_0

    .line 874
    :cond_5
    :try_start_4
    sget-object v1, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v1}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/data/at;->f()I

    move-result v1

    goto/16 :goto_1

    :cond_6
    invoke-virtual {p0}, Lcom/peel/ui/lq;->m()Landroid/support/v4/app/ae;

    move-result-object v4

    invoke-static {v4}, Lcom/peel/util/bx;->d(Landroid/content/Context;)Ljava/lang/String;
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_2

    move-result-object v4

    goto/16 :goto_2

    .line 879
    :cond_7
    sget v0, Lcom/peel/ui/fp;->on_air_txt:I

    if-ne v3, v0, :cond_9

    .line 880
    invoke-virtual {p0}, Lcom/peel/ui/lq;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    invoke-static {v0}, Lcom/peel/d/e;->a(Landroid/support/v4/app/ae;)Lcom/peel/d/u;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    const-class v1, Lcom/peel/ui/kj;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 881
    iget-object v0, p0, Lcom/peel/ui/lq;->a:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/peel/ui/lq;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/peel/d/e;->a(Ljava/lang/String;Landroid/support/v4/app/ae;)V

    goto/16 :goto_3

    .line 883
    :cond_8
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 884
    const-string/jumbo v1, "yosemiteCountry"

    iget-boolean v2, p0, Lcom/peel/ui/lq;->aj:Z

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 885
    const-string/jumbo v1, "yosemiteInstalled"

    iget-boolean v2, p0, Lcom/peel/ui/lq;->i:Z

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 886
    invoke-virtual {p0}, Lcom/peel/ui/lq;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    const-class v2, Lcom/peel/ui/kj;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lcom/peel/d/e;->c(Landroid/support/v4/app/ae;Ljava/lang/String;Landroid/os/Bundle;)V

    goto/16 :goto_3

    .line 890
    :cond_9
    sget-object v0, Lcom/peel/content/a;->h:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/peel/ui/lq;->b:Lcom/peel/d/i;

    if-eqz v0, :cond_1

    .line 891
    iget-object v0, p0, Lcom/peel/ui/lq;->g:Lcom/peel/widget/CustomViewPager;

    if-nez v0, :cond_a

    move-object v0, v1

    :goto_4
    check-cast v0, Lcom/peel/ui/ds;

    check-cast v0, Lcom/peel/ui/ds;

    .line 892
    if-nez v0, :cond_b

    .line 893
    :goto_5
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/peel/d/u;->v()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v1, p1}, Lcom/peel/d/u;->onClick(Landroid/view/View;)V

    goto/16 :goto_3

    .line 891
    :cond_a
    iget-object v0, p0, Lcom/peel/ui/lq;->g:Lcom/peel/widget/CustomViewPager;

    invoke-virtual {v0}, Lcom/peel/widget/CustomViewPager;->getAdapter()Landroid/support/v4/view/av;

    move-result-object v0

    goto :goto_4

    .line 892
    :cond_b
    iget-object v1, p0, Lcom/peel/ui/lq;->g:Lcom/peel/widget/CustomViewPager;

    invoke-virtual {v1}, Lcom/peel/widget/CustomViewPager;->getCurrentItem()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/peel/ui/ds;->d(I)Lcom/peel/d/u;

    move-result-object v1

    goto :goto_5

    .line 876
    :catch_2
    move-exception v0

    goto/16 :goto_3
.end method

.method public w()V
    .locals 6

    .prologue
    .line 483
    invoke-super {p0}, Lcom/peel/d/u;->w()V

    .line 487
    invoke-virtual {p0}, Lcom/peel/ui/lq;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    invoke-static {v0}, Landroid/support/v4/a/q;->a(Landroid/content/Context;)Landroid/support/v4/a/q;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/ui/lq;->aD:Landroid/content/BroadcastReceiver;

    new-instance v2, Landroid/content/IntentFilter;

    const-string/jumbo v3, "dismiss_toast"

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/a/q;->a(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)V

    .line 488
    invoke-virtual {p0}, Lcom/peel/ui/lq;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    invoke-static {v0}, Landroid/support/v4/a/q;->a(Landroid/content/Context;)Landroid/support/v4/a/q;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/ui/lq;->aD:Landroid/content/BroadcastReceiver;

    new-instance v2, Landroid/content/IntentFilter;

    const-string/jumbo v3, "show_toast"

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/a/q;->a(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)V

    .line 490
    invoke-static {}, Lcom/peel/util/dq;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 491
    iget-object v0, p0, Lcom/peel/ui/lq;->au:Lcom/peel/ui/a/a;

    invoke-virtual {p0}, Lcom/peel/ui/lq;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    const-string/jumbo v2, "topPicks"

    const/16 v3, 0x7d0

    invoke-virtual {v0, v1, v2, v3}, Lcom/peel/ui/a/a;->a(Landroid/app/Activity;Ljava/lang/String;I)V

    .line 494
    :cond_0
    iget-object v0, p0, Lcom/peel/ui/lq;->ap:Ljava/util/Timer;

    if-eqz v0, :cond_1

    .line 495
    iget-object v0, p0, Lcom/peel/ui/lq;->ap:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 497
    :cond_1
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, Lcom/peel/ui/lq;->ap:Ljava/util/Timer;

    .line 498
    new-instance v0, Lcom/peel/ui/mj;

    invoke-direct {v0, p0}, Lcom/peel/ui/mj;-><init>(Lcom/peel/ui/lq;)V

    iput-object v0, p0, Lcom/peel/ui/lq;->aq:Ljava/util/TimerTask;

    .line 545
    iget-object v0, p0, Lcom/peel/ui/lq;->ap:Ljava/util/Timer;

    iget-object v1, p0, Lcom/peel/ui/lq;->aq:Ljava/util/TimerTask;

    const-wide/16 v2, 0x3e8

    const-wide/16 v4, 0x9c4

    invoke-virtual/range {v0 .. v5}, Ljava/util/Timer;->scheduleAtFixedRate(Ljava/util/TimerTask;JJ)V

    .line 546
    return-void
.end method

.method public x()V
    .locals 2

    .prologue
    .line 550
    invoke-super {p0}, Lcom/peel/d/u;->x()V

    .line 552
    iget-object v0, p0, Lcom/peel/ui/lq;->ao:Landroid/widget/PopupWindow;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/peel/ui/lq;->ao:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 553
    iget-object v0, p0, Lcom/peel/ui/lq;->ao:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->dismiss()V

    .line 556
    :cond_0
    invoke-virtual {p0}, Lcom/peel/ui/lq;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    invoke-static {v0}, Landroid/support/v4/a/q;->a(Landroid/content/Context;)Landroid/support/v4/a/q;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/ui/lq;->aD:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/support/v4/a/q;->a(Landroid/content/BroadcastReceiver;)V

    .line 557
    iget-object v0, p0, Lcom/peel/ui/lq;->ap:Ljava/util/Timer;

    if-eqz v0, :cond_1

    .line 558
    iget-object v0, p0, Lcom/peel/ui/lq;->ap:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 560
    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/peel/ui/lq;->ar:Z

    .line 562
    return-void
.end method
