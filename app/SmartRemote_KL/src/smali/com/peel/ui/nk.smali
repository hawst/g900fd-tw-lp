.class Lcom/peel/ui/nk;
.super Landroid/webkit/WebViewClient;


# instance fields
.field final synthetic a:Lcom/peel/ui/nj;


# direct methods
.method constructor <init>(Lcom/peel/ui/nj;)V
    .locals 0

    .prologue
    .line 64
    iput-object p1, p0, Lcom/peel/ui/nk;->a:Lcom/peel/ui/nj;

    invoke-direct {p0}, Landroid/webkit/WebViewClient;-><init>()V

    return-void
.end method


# virtual methods
.method public onPageFinished(Landroid/webkit/WebView;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 84
    iget-object v0, p0, Lcom/peel/ui/nk;->a:Lcom/peel/ui/nj;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/peel/ui/nj;->a(Lcom/peel/ui/nj;Z)Z

    .line 85
    iget-object v0, p0, Lcom/peel/ui/nk;->a:Lcom/peel/ui/nj;

    invoke-static {v0}, Lcom/peel/ui/nj;->a(Lcom/peel/ui/nj;)Landroid/os/Bundle;

    move-result-object v0

    const-string/jumbo v1, "video"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 86
    iget-object v0, p0, Lcom/peel/ui/nk;->a:Lcom/peel/ui/nj;

    iget-object v1, p0, Lcom/peel/ui/nk;->a:Lcom/peel/ui/nj;

    invoke-static {v1}, Lcom/peel/ui/nj;->b(Lcom/peel/ui/nj;)Landroid/webkit/WebView;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/peel/ui/nj;->a(Lcom/peel/ui/nj;Landroid/webkit/WebView;)V

    .line 88
    :cond_0
    return-void
.end method

.method public shouldOverrideUrlLoading(Landroid/webkit/WebView;Ljava/lang/String;)Z
    .locals 3

    .prologue
    .line 66
    invoke-static {}, Lcom/peel/ui/nj;->b()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "loading url "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 67
    const-string/jumbo v0, "peel"

    invoke-virtual {p2, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 68
    const-string/jumbo v0, "programs"

    invoke-virtual {p2, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string/jumbo v0, "tunein"

    invoke-virtual {p2, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string/jumbo v0, "remote"

    invoke-virtual {p2, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string/jumbo v0, "home"

    invoke-virtual {p2, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 69
    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 70
    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 71
    const/high16 v1, 0x24000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 72
    iget-object v1, p0, Lcom/peel/ui/nk;->a:Lcom/peel/ui/nj;

    invoke-virtual {v1, v0}, Lcom/peel/ui/nj;->a(Landroid/content/Intent;)V

    .line 77
    :goto_0
    const/4 v0, 0x1

    .line 79
    :goto_1
    return v0

    .line 75
    :cond_1
    iget-object v0, p0, Lcom/peel/ui/nk;->a:Lcom/peel/ui/nj;

    invoke-virtual {v0}, Lcom/peel/ui/nj;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/ae;->finish()V

    goto :goto_0

    .line 79
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method
