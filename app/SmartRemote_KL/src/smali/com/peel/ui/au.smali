.class Lcom/peel/ui/au;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Landroid/widget/TextView;

.field final synthetic b:[Ljava/lang/String;

.field final synthetic c:Landroid/widget/Button;

.field final synthetic d:Landroid/widget/Button;

.field final synthetic e:Lcom/peel/ui/ar;


# direct methods
.method constructor <init>(Lcom/peel/ui/ar;Landroid/widget/TextView;[Ljava/lang/String;Landroid/widget/Button;Landroid/widget/Button;)V
    .locals 0

    .prologue
    .line 472
    iput-object p1, p0, Lcom/peel/ui/au;->e:Lcom/peel/ui/ar;

    iput-object p2, p0, Lcom/peel/ui/au;->a:Landroid/widget/TextView;

    iput-object p3, p0, Lcom/peel/ui/au;->b:[Ljava/lang/String;

    iput-object p4, p0, Lcom/peel/ui/au;->c:Landroid/widget/Button;

    iput-object p5, p0, Lcom/peel/ui/au;->d:Landroid/widget/Button;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 475
    iget-object v0, p0, Lcom/peel/ui/au;->e:Lcom/peel/ui/ar;

    sget-object v1, Lcom/peel/ui/aw;->a:Lcom/peel/ui/aw;

    invoke-static {v0, v1}, Lcom/peel/ui/ar;->a(Lcom/peel/ui/ar;Lcom/peel/ui/aw;)V

    .line 476
    iget-object v0, p0, Lcom/peel/ui/au;->e:Lcom/peel/ui/ar;

    invoke-static {v0}, Lcom/peel/ui/ar;->b(Lcom/peel/ui/ar;)I

    move-result v0

    if-lez v0, :cond_0

    .line 477
    iget-object v0, p0, Lcom/peel/ui/au;->e:Lcom/peel/ui/ar;

    invoke-static {v0}, Lcom/peel/ui/ar;->d(Lcom/peel/ui/ar;)I

    .line 478
    iget-object v0, p0, Lcom/peel/ui/au;->a:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/peel/ui/au;->b:[Ljava/lang/String;

    iget-object v2, p0, Lcom/peel/ui/au;->e:Lcom/peel/ui/ar;

    invoke-static {v2}, Lcom/peel/ui/ar;->b(Lcom/peel/ui/ar;)I

    move-result v2

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 479
    iget-object v0, p0, Lcom/peel/ui/au;->e:Lcom/peel/ui/ar;

    invoke-static {v0}, Lcom/peel/ui/ar;->b(Lcom/peel/ui/ar;)I

    move-result v0

    if-nez v0, :cond_1

    .line 480
    iget-object v0, p0, Lcom/peel/ui/au;->c:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 484
    :goto_0
    iget-object v0, p0, Lcom/peel/ui/au;->d:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setEnabled(Z)V

    .line 486
    :cond_0
    return-void

    .line 482
    :cond_1
    iget-object v0, p0, Lcom/peel/ui/au;->c:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_0
.end method
