.class public Lcom/peel/ui/v;
.super Lcom/peel/d/u;


# instance fields
.field private aj:Landroid/widget/TextView;

.field private e:Landroid/view/View;

.field private f:Landroid/widget/ListView;

.field private g:Landroid/widget/AutoCompleteTextView;

.field private h:Landroid/widget/ImageView;

.field private i:Landroid/widget/ImageView;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 45
    invoke-direct {p0}, Lcom/peel/d/u;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/peel/ui/v;)Landroid/widget/ListView;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/peel/ui/v;->f:Landroid/widget/ListView;

    return-object v0
.end method

.method static synthetic b(Lcom/peel/ui/v;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/peel/ui/v;->a:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic c(Lcom/peel/ui/v;)Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/peel/ui/v;->h:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic d(Lcom/peel/ui/v;)Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/peel/ui/v;->i:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic e(Lcom/peel/ui/v;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/peel/ui/v;->a:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public W()Z
    .locals 1

    .prologue
    .line 232
    const/4 v0, 0x0

    return v0
.end method

.method public Z()V
    .locals 6

    .prologue
    .line 331
    iget-object v0, p0, Lcom/peel/ui/v;->d:Lcom/peel/d/a;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/peel/ui/v;->f:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lcom/peel/ui/aa;

    invoke-virtual {v0}, Lcom/peel/ui/aa;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 332
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 333
    sget v0, Lcom/peel/ui/fp;->menu_done:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 334
    new-instance v0, Lcom/peel/d/a;

    sget-object v1, Lcom/peel/d/d;->b:Lcom/peel/d/d;

    sget-object v2, Lcom/peel/d/b;->a:Lcom/peel/d/b;

    sget-object v3, Lcom/peel/d/c;->b:Lcom/peel/d/c;

    sget v4, Lcom/peel/ui/ft;->edit_fav_channels:I

    .line 337
    invoke-virtual {p0, v4}, Lcom/peel/ui/v;->a(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct/range {v0 .. v5}, Lcom/peel/d/a;-><init>(Lcom/peel/d/d;Lcom/peel/d/b;Lcom/peel/d/c;Ljava/lang/String;Ljava/util/List;)V

    iput-object v0, p0, Lcom/peel/ui/v;->d:Lcom/peel/d/a;

    .line 344
    :goto_0
    iget-object v0, p0, Lcom/peel/ui/v;->b:Lcom/peel/d/i;

    iget-object v1, p0, Lcom/peel/ui/v;->d:Lcom/peel/d/a;

    invoke-virtual {v0, v1}, Lcom/peel/d/i;->a(Lcom/peel/d/a;)V

    .line 345
    return-void

    .line 339
    :cond_0
    new-instance v0, Lcom/peel/d/a;

    sget-object v1, Lcom/peel/d/d;->b:Lcom/peel/d/d;

    sget-object v2, Lcom/peel/d/b;->a:Lcom/peel/d/b;

    sget-object v3, Lcom/peel/d/c;->b:Lcom/peel/d/c;

    sget v4, Lcom/peel/ui/ft;->edit_fav_channels:I

    .line 342
    invoke-virtual {p0, v4}, Lcom/peel/ui/v;->a(I)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, Lcom/peel/d/a;-><init>(Lcom/peel/d/d;Lcom/peel/d/b;Lcom/peel/d/c;Ljava/lang/String;Ljava/util/List;)V

    iput-object v0, p0, Lcom/peel/ui/v;->d:Lcom/peel/d/a;

    goto :goto_0
.end method

.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4

    .prologue
    .line 84
    sget v0, Lcom/peel/ui/fq;->fav_channel_search:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/ui/v;->e:Landroid/view/View;

    .line 85
    iget-object v0, p0, Lcom/peel/ui/v;->e:Landroid/view/View;

    sget v1, Lcom/peel/ui/fp;->list:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/peel/ui/v;->f:Landroid/widget/ListView;

    .line 86
    iget-object v0, p0, Lcom/peel/ui/v;->e:Landroid/view/View;

    sget v1, Lcom/peel/ui/fp;->keyword:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/AutoCompleteTextView;

    iput-object v0, p0, Lcom/peel/ui/v;->g:Landroid/widget/AutoCompleteTextView;

    .line 87
    iget-object v0, p0, Lcom/peel/ui/v;->e:Landroid/view/View;

    sget v1, Lcom/peel/ui/fp;->clear_btn:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/peel/ui/v;->h:Landroid/widget/ImageView;

    .line 88
    iget-object v0, p0, Lcom/peel/ui/v;->h:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 89
    iget-object v0, p0, Lcom/peel/ui/v;->e:Landroid/view/View;

    sget v1, Lcom/peel/ui/fp;->search_icon:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/peel/ui/v;->i:Landroid/widget/ImageView;

    .line 90
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v0

    .line 91
    const-string/jumbo v1, "iw"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string/jumbo v1, "ar"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 92
    :cond_0
    iget-object v0, p0, Lcom/peel/ui/v;->g:Landroid/widget/AutoCompleteTextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/peel/ui/v;->n()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/peel/ui/ft;->hint_channel_search:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/AutoCompleteTextView;->setHint(Ljava/lang/CharSequence;)V

    .line 99
    :goto_0
    iget-object v0, p0, Lcom/peel/ui/v;->e:Landroid/view/View;

    return-object v0

    .line 94
    :cond_1
    iget-object v0, p0, Lcom/peel/ui/v;->g:Landroid/widget/AutoCompleteTextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "       "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/peel/ui/v;->n()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/peel/ui/ft;->hint_channel_search:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/AutoCompleteTextView;->setHint(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public a(Landroid/view/Menu;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 211
    invoke-virtual {p0}, Lcom/peel/ui/v;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    check-cast v0, Landroid/support/v7/app/ActionBarActivity;

    .line 212
    if-nez v0, :cond_0

    .line 213
    iget-object v0, p0, Lcom/peel/ui/v;->a:Ljava/lang/String;

    const-string/jumbo v1, "unable to set actionbar, activity is null"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 228
    :goto_0
    return-void

    .line 217
    :cond_0
    invoke-virtual {v0}, Landroid/support/v7/app/ActionBarActivity;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v0

    .line 219
    invoke-virtual {v0, v1}, Landroid/support/v7/app/ActionBar;->setDisplayShowCustomEnabled(Z)V

    .line 220
    invoke-virtual {v0, v1}, Landroid/support/v7/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 221
    invoke-virtual {v0, v2}, Landroid/support/v7/app/ActionBar;->setDisplayShowHomeEnabled(Z)V

    .line 222
    invoke-virtual {v0, v1}, Landroid/support/v7/app/ActionBar;->setHomeButtonEnabled(Z)V

    .line 223
    invoke-virtual {v0, v2}, Landroid/support/v7/app/ActionBar;->setNavigationMode(I)V

    .line 224
    invoke-virtual {v0, v1}, Landroid/support/v7/app/ActionBar;->setDisplayShowTitleEnabled(Z)V

    .line 225
    invoke-virtual {p0}, Lcom/peel/ui/v;->n()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/peel/ui/ft;->edit_fav_channels:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v7/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    .line 226
    invoke-virtual {p0}, Lcom/peel/ui/v;->n()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/peel/ui/fo;->action_bar_background:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v7/app/ActionBar;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 227
    invoke-virtual {v0}, Landroid/support/v7/app/ActionBar;->show()V

    goto :goto_0
.end method

.method public a(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 141
    invoke-interface {p1}, Landroid/view/Menu;->clear()V

    .line 143
    sget v0, Lcom/peel/ui/fr;->menu:I

    invoke-virtual {p2, v0, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    move v1, v2

    .line 161
    :goto_0
    invoke-interface {p1}, Landroid/view/Menu;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 162
    invoke-interface {p1, v1}, Landroid/view/Menu;->getItem(I)Landroid/view/MenuItem;

    move-result-object v3

    .line 163
    invoke-interface {v3}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    sget v4, Lcom/peel/ui/fp;->menu_done:I

    if-ne v0, v4, :cond_1

    .line 164
    invoke-interface {v3}, Landroid/view/MenuItem;->getActionView()Landroid/view/View;

    move-result-object v0

    sget v4, Lcom/peel/ui/fp;->done_btn:I

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/peel/ui/v;->aj:Landroid/widget/TextView;

    .line 165
    iget-object v0, p0, Lcom/peel/ui/v;->aj:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 166
    iget-object v0, p0, Lcom/peel/ui/v;->aj:Landroid/widget/TextView;

    new-instance v4, Lcom/peel/ui/w;

    invoke-direct {v4, p0}, Lcom/peel/ui/w;-><init>(Lcom/peel/ui/v;)V

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 178
    :cond_0
    const/4 v0, 0x1

    invoke-interface {v3, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 161
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 180
    :cond_1
    invoke-interface {v3, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_1

    .line 207
    :cond_2
    return-void
.end method

.method public a(Landroid/view/MenuItem;)Z
    .locals 2

    .prologue
    .line 237
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    .line 238
    sget v1, Lcom/peel/ui/fp;->menu_done:I

    if-ne v0, v1, :cond_1

    .line 239
    sget-object v0, Lcom/peel/content/a;->h:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/peel/ui/v;->b:Lcom/peel/d/i;

    if-eqz v0, :cond_1

    .line 240
    iget-object v0, p0, Lcom/peel/ui/v;->f:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 241
    iget-object v0, p0, Lcom/peel/ui/v;->f:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lcom/peel/ui/aa;

    invoke-virtual {v0}, Lcom/peel/ui/aa;->b()V

    .line 242
    :cond_0
    iget-object v0, p0, Lcom/peel/ui/v;->a:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/peel/ui/v;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/peel/d/e;->a(Ljava/lang/String;Landroid/support/v4/app/ae;)V

    .line 245
    :cond_1
    invoke-super {p0, p1}, Lcom/peel/d/u;->a(Landroid/view/MenuItem;)Z

    move-result v0

    return v0
.end method

.method public aa()Z
    .locals 1

    .prologue
    .line 79
    const/4 v0, 0x0

    return v0
.end method

.method public c(Landroid/os/Bundle;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 349
    invoke-super {p0, p1}, Lcom/peel/d/u;->c(Landroid/os/Bundle;)V

    .line 351
    sget-object v0, Lcom/peel/content/a;->h:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-nez v0, :cond_1

    .line 387
    :cond_0
    :goto_0
    return-void

    .line 354
    :cond_1
    iget-object v0, p0, Lcom/peel/ui/v;->f:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    if-nez v0, :cond_0

    .line 355
    const-string/jumbo v0, "live"

    invoke-static {v0}, Lcom/peel/content/a;->c(Ljava/lang/String;)Lcom/peel/content/library/Library;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/peel/content/library/LiveLibrary;

    .line 356
    invoke-virtual {v4}, Lcom/peel/content/library/LiveLibrary;->i()[Lcom/peel/data/Channel;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v3

    .line 358
    sget-object v0, Lcom/peel/content/a;->f:Lcom/peel/content/user/User;

    invoke-virtual {v0}, Lcom/peel/content/user/User;->h()Landroid/os/Bundle;

    move-result-object v0

    .line 360
    if-eqz v0, :cond_5

    .line 361
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 362
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 364
    const-string/jumbo v5, "favchannels"

    invoke-virtual {v0, v5}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v5

    if-eqz v5, :cond_2

    .line 365
    const-string/jumbo v5, "favchannels"

    invoke-virtual {v0, v5}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 367
    :cond_2
    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 369
    :try_start_0
    const-string/jumbo v5, "#"

    invoke-virtual {v0, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    const/4 v5, 0x1

    aget-object v0, v0, v5

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 370
    :catch_0
    move-exception v0

    goto :goto_1

    .line 374
    :cond_3
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_4
    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/data/Channel;

    .line 375
    invoke-virtual {v0}, Lcom/peel/data/Channel;->c()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 376
    invoke-virtual {v0, v6}, Lcom/peel/data/Channel;->a(Z)V

    goto :goto_2

    .line 382
    :cond_5
    iget-object v6, p0, Lcom/peel/ui/v;->f:Landroid/widget/ListView;

    new-instance v0, Lcom/peel/ui/aa;

    invoke-virtual {p0}, Lcom/peel/ui/v;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    sget v2, Lcom/peel/ui/fq;->channel_search_row:I

    .line 383
    invoke-virtual {v4}, Lcom/peel/content/library/LiveLibrary;->e()Ljava/lang/String;

    move-result-object v4

    .line 384
    invoke-static {}, Lcom/peel/content/a;->a()Lcom/peel/data/ContentRoom;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, Lcom/peel/ui/aa;-><init>(Landroid/content/Context;ILjava/util/List;Ljava/lang/String;Lcom/peel/data/ContentRoom;)V

    .line 382
    invoke-virtual {v6, v0}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    goto/16 :goto_0
.end method

.method public d(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 250
    invoke-super {p0, p1}, Lcom/peel/d/u;->d(Landroid/os/Bundle;)V

    .line 251
    iget-object v0, p0, Lcom/peel/ui/v;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "category"

    sget v2, Lcom/peel/ui/ft;->editchannels:I

    invoke-virtual {p0, v2}, Lcom/peel/ui/v;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 254
    iget-object v0, p0, Lcom/peel/ui/v;->f:Landroid/widget/ListView;

    new-instance v1, Lcom/peel/ui/x;

    invoke-direct {v1, p0}, Lcom/peel/ui/x;-><init>(Lcom/peel/ui/v;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 266
    iget-object v0, p0, Lcom/peel/ui/v;->g:Landroid/widget/AutoCompleteTextView;

    new-instance v1, Lcom/peel/ui/y;

    invoke-direct {v1, p0}, Lcom/peel/ui/y;-><init>(Lcom/peel/ui/v;)V

    invoke-virtual {v0, v1}, Landroid/widget/AutoCompleteTextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 300
    iget-object v0, p0, Lcom/peel/ui/v;->g:Landroid/widget/AutoCompleteTextView;

    new-instance v1, Lcom/peel/ui/z;

    invoke-direct {v1, p0}, Lcom/peel/ui/z;-><init>(Lcom/peel/ui/v;)V

    invoke-virtual {v0, v1}, Landroid/widget/AutoCompleteTextView;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 314
    sget-object v0, Lcom/peel/content/a;->h:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 315
    iget-object v0, p0, Lcom/peel/ui/v;->c:Landroid/os/Bundle;

    invoke-virtual {p0, v0}, Lcom/peel/ui/v;->c(Landroid/os/Bundle;)V

    .line 316
    :cond_0
    return-void
.end method

.method public h(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 320
    invoke-super {p0, p1}, Lcom/peel/d/u;->h(Landroid/os/Bundle;)V

    .line 321
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 322
    sget v1, Lcom/peel/ui/fp;->menu_done:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 325
    invoke-virtual {p0}, Lcom/peel/ui/v;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/ae;->invalidateOptionsMenu()V

    .line 326
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 115
    sget-object v0, Lcom/peel/content/a;->h:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/peel/ui/v;->b:Lcom/peel/d/i;

    if-nez v0, :cond_1

    .line 137
    :cond_0
    :goto_0
    return-void

    .line 117
    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    .line 118
    sget v1, Lcom/peel/ui/fp;->done_btn:I

    if-ne v0, v1, :cond_3

    .line 119
    iget-object v0, p0, Lcom/peel/ui/v;->f:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 120
    iget-object v0, p0, Lcom/peel/ui/v;->f:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lcom/peel/ui/aa;

    invoke-virtual {v0}, Lcom/peel/ui/aa;->b()V

    .line 122
    :cond_2
    invoke-virtual {p0}, Lcom/peel/ui/v;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    const-string/jumbo v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/ae;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 123
    iget-object v1, p0, Lcom/peel/ui/v;->g:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v1}, Landroid/widget/AutoCompleteTextView;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 124
    iget-object v0, p0, Lcom/peel/ui/v;->a:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/peel/ui/v;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/peel/d/e;->a(Ljava/lang/String;Landroid/support/v4/app/ae;)V

    goto :goto_0

    .line 125
    :cond_3
    sget v1, Lcom/peel/ui/fp;->action_btn:I

    if-ne v0, v1, :cond_4

    .line 126
    invoke-virtual {p0}, Lcom/peel/ui/v;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    const-string/jumbo v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/ae;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 127
    iget-object v1, p0, Lcom/peel/ui/v;->g:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v1}, Landroid/widget/AutoCompleteTextView;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 128
    iget-object v0, p0, Lcom/peel/ui/v;->a:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/peel/ui/v;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/peel/d/e;->a(Ljava/lang/String;Landroid/support/v4/app/ae;)V

    goto :goto_0

    .line 129
    :cond_4
    sget v1, Lcom/peel/ui/fp;->clear_btn:I

    if-ne v0, v1, :cond_0

    .line 130
    iget-object v0, p0, Lcom/peel/ui/v;->g:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v0}, Landroid/widget/AutoCompleteTextView;->getEditableText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->clear()V

    .line 131
    invoke-virtual {p0}, Lcom/peel/ui/v;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    const-string/jumbo v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/ae;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 132
    iget-object v1, p0, Lcom/peel/ui/v;->g:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->showSoftInput(Landroid/view/View;I)Z

    .line 133
    iget-object v0, p0, Lcom/peel/ui/v;->i:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getVisibility()I

    move-result v0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    .line 134
    iget-object v0, p0, Lcom/peel/ui/v;->i:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_0
.end method

.method public w()V
    .locals 6

    .prologue
    .line 103
    invoke-super {p0}, Lcom/peel/d/u;->w()V

    .line 104
    iget-object v0, p0, Lcom/peel/ui/v;->g:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v0}, Landroid/widget/AutoCompleteTextView;->requestFocus()Z

    .line 105
    invoke-virtual {p0}, Lcom/peel/ui/v;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    const-class v1, Lcom/peel/ui/v;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/peel/ui/v;->g:Landroid/widget/AutoCompleteTextView;

    const-wide/16 v4, 0x64

    invoke-static {v0, v1, v2, v4, v5}, Lcom/peel/util/bx;->a(Landroid/content/Context;Ljava/lang/String;Landroid/widget/EditText;J)V

    .line 106
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/peel/ui/v;->d:Lcom/peel/d/a;

    .line 107
    invoke-virtual {p0}, Lcom/peel/ui/v;->Z()V

    .line 110
    return-void
.end method
