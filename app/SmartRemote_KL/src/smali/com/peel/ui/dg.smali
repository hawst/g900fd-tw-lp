.class public Lcom/peel/ui/dg;
.super Lcom/peel/widget/ag;


# static fields
.field private static final k:Ljava/lang/String;


# instance fields
.field public a:Landroid/content/Context;

.field public b:Landroid/widget/EditText;

.field public c:Landroid/widget/EditText;

.field public d:Landroid/widget/EditText;

.field public e:Landroid/widget/TextView;

.field public f:Landroid/widget/TextView;

.field public g:Landroid/widget/TextView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 29
    const-class v0, Lcom/peel/ui/dg;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/peel/ui/dg;->k:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;IILjava/util/List;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/peel/ui/dr;Landroid/widget/EditText;)V
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "II",
            "Ljava/util/List",
            "<",
            "Lcom/peel/f/a;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/peel/f/a;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/peel/ui/dr;",
            "Landroid/widget/EditText;",
            ")V"
        }
    .end annotation

    .prologue
    .line 43
    invoke-direct {p0, p1}, Lcom/peel/widget/ag;-><init>(Landroid/content/Context;)V

    .line 44
    iput-object p1, p0, Lcom/peel/ui/dg;->a:Landroid/content/Context;

    .line 46
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    sget v3, Lcom/peel/ui/fq;->report_missing_brand:I

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v12

    .line 47
    sget v2, Lcom/peel/ui/fp;->email:I

    invoke-virtual {v12, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/EditText;

    iput-object v2, p0, Lcom/peel/ui/dg;->b:Landroid/widget/EditText;

    .line 48
    sget v2, Lcom/peel/ui/fp;->brand:I

    invoke-virtual {v12, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/EditText;

    iput-object v2, p0, Lcom/peel/ui/dg;->c:Landroid/widget/EditText;

    .line 49
    sget v2, Lcom/peel/ui/fp;->model:I

    invoke-virtual {v12, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/EditText;

    iput-object v2, p0, Lcom/peel/ui/dg;->d:Landroid/widget/EditText;

    .line 50
    sget v2, Lcom/peel/ui/fp;->email_tv:I

    invoke-virtual {v12, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/peel/ui/dg;->e:Landroid/widget/TextView;

    .line 51
    sget v2, Lcom/peel/ui/fp;->brand_tv:I

    invoke-virtual {v12, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/peel/ui/dg;->f:Landroid/widget/TextView;

    .line 52
    sget v2, Lcom/peel/ui/fp;->model_tv:I

    invoke-virtual {v12, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/peel/ui/dg;->g:Landroid/widget/TextView;

    .line 53
    iget-object v2, p0, Lcom/peel/ui/dg;->e:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/peel/ui/dg;->e:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-interface {v3}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v4, ":"

    const-string/jumbo v5, " "

    invoke-virtual {v3, v4, v5}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 54
    iget-object v2, p0, Lcom/peel/ui/dg;->f:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/peel/ui/dg;->f:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-interface {v3}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v4, ":"

    const-string/jumbo v5, " "

    invoke-virtual {v3, v4, v5}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 55
    iget-object v2, p0, Lcom/peel/ui/dg;->g:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/peel/ui/dg;->g:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-interface {v3}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v4, ":"

    const-string/jumbo v5, " "

    invoke-virtual {v3, v4, v5}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 57
    if-eqz p7, :cond_0

    .line 58
    iget-object v2, p0, Lcom/peel/ui/dg;->b:Landroid/widget/EditText;

    move-object/from16 v0, p7

    invoke-virtual {v2, v0}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 60
    :cond_0
    if-eqz p8, :cond_1

    iget-object v2, p0, Lcom/peel/ui/dg;->c:Landroid/widget/EditText;

    move-object/from16 v0, p8

    invoke-virtual {v2, v0}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 61
    :cond_1
    if-eqz p10, :cond_2

    const-string/jumbo v2, "email"

    move-object/from16 v0, p10

    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 62
    iget-object v2, p0, Lcom/peel/ui/dg;->b:Landroid/widget/EditText;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->setCursorVisible(Z)V

    .line 63
    iget-object v2, p0, Lcom/peel/ui/dg;->b:Landroid/widget/EditText;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->setFocusable(Z)V

    .line 64
    iget-object v2, p0, Lcom/peel/ui/dg;->b:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->requestFocus()Z

    .line 65
    iget-object v2, p0, Lcom/peel/ui/dg;->b:Landroid/widget/EditText;

    iget-object v3, p0, Lcom/peel/ui/dg;->b:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-interface {v3}, Landroid/text/Editable;->length()I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->setSelection(I)V

    .line 66
    const-class v2, Lcom/peel/ui/dg;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/peel/ui/dg;->b:Landroid/widget/EditText;

    const-wide/16 v4, 0xfa

    invoke-static {p1, v2, v3, v4, v5}, Lcom/peel/util/bx;->a(Landroid/content/Context;Ljava/lang/String;Landroid/widget/EditText;J)V

    .line 68
    :cond_2
    if-eqz p10, :cond_3

    const-string/jumbo v2, "brand"

    move-object/from16 v0, p10

    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 69
    iget-object v2, p0, Lcom/peel/ui/dg;->c:Landroid/widget/EditText;

    const-string/jumbo v3, ""

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 70
    iget-object v2, p0, Lcom/peel/ui/dg;->c:Landroid/widget/EditText;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->setCursorVisible(Z)V

    .line 71
    iget-object v2, p0, Lcom/peel/ui/dg;->c:Landroid/widget/EditText;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->setFocusable(Z)V

    .line 72
    iget-object v2, p0, Lcom/peel/ui/dg;->c:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->requestFocus()Z

    .line 74
    const-class v2, Lcom/peel/ui/dg;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/peel/ui/dg;->c:Landroid/widget/EditText;

    const-wide/16 v4, 0xfa

    invoke-static {p1, v2, v3, v4, v5}, Lcom/peel/util/bx;->a(Landroid/content/Context;Ljava/lang/String;Landroid/widget/EditText;J)V

    .line 76
    :cond_3
    if-eqz p10, :cond_4

    const-string/jumbo v2, "model"

    move-object/from16 v0, p10

    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 77
    iget-object v2, p0, Lcom/peel/ui/dg;->d:Landroid/widget/EditText;

    const-string/jumbo v3, ""

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 78
    iget-object v2, p0, Lcom/peel/ui/dg;->d:Landroid/widget/EditText;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->setCursorVisible(Z)V

    .line 79
    iget-object v2, p0, Lcom/peel/ui/dg;->d:Landroid/widget/EditText;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->setFocusable(Z)V

    .line 80
    iget-object v2, p0, Lcom/peel/ui/dg;->d:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->requestFocus()Z

    .line 83
    const-class v2, Lcom/peel/ui/dg;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/peel/ui/dg;->c:Landroid/widget/EditText;

    const-wide/16 v4, 0xfa

    invoke-static {p1, v2, v3, v4, v5}, Lcom/peel/util/bx;->a(Landroid/content/Context;Ljava/lang/String;Landroid/widget/EditText;J)V

    .line 85
    :cond_4
    sget v2, Lcom/peel/ui/ft;->report_missing_brand:I

    invoke-virtual {p0, v2}, Lcom/peel/ui/dg;->a(I)Lcom/peel/widget/ag;

    .line 86
    sget v2, Lcom/peel/ui/ft;->report_missing_brand_desc:I

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p2, v3, v4

    invoke-virtual {p1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/peel/ui/dg;->b(Ljava/lang/CharSequence;)Lcom/peel/widget/ag;

    .line 87
    sget v13, Lcom/peel/ui/ft;->send:I

    new-instance v2, Lcom/peel/ui/dh;

    move-object v3, p0

    move-object v4, p1

    move-object/from16 v5, p2

    move/from16 v6, p3

    move/from16 v7, p4

    move-object/from16 v8, p5

    move-object/from16 v9, p6

    move-object/from16 v10, p11

    move-object/from16 v11, p12

    invoke-direct/range {v2 .. v11}, Lcom/peel/ui/dh;-><init>(Lcom/peel/ui/dg;Landroid/content/Context;Ljava/lang/String;IILjava/util/List;Ljava/util/List;Lcom/peel/ui/dr;Landroid/widget/EditText;)V

    invoke-virtual {p0, v13, v2}, Lcom/peel/ui/dg;->a(ILandroid/content/DialogInterface$OnClickListener;)Lcom/peel/widget/ag;

    .line 261
    sget v2, Lcom/peel/ui/ft;->cancel:I

    new-instance v3, Lcom/peel/ui/dp;

    invoke-direct {v3, p0, p1}, Lcom/peel/ui/dp;-><init>(Lcom/peel/ui/dg;Landroid/content/Context;)V

    invoke-virtual {p0, v2, v3}, Lcom/peel/ui/dg;->c(ILandroid/content/DialogInterface$OnClickListener;)Lcom/peel/widget/ag;

    .line 268
    invoke-virtual {p0, v12}, Lcom/peel/ui/dg;->a(Landroid/view/View;)Lcom/peel/widget/ag;

    .line 269
    invoke-virtual {p0}, Lcom/peel/ui/dg;->d()Lcom/peel/widget/ag;

    .line 270
    return-void
.end method

.method static synthetic a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 28
    sget-object v0, Lcom/peel/ui/dg;->k:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method a(Landroid/content/Context;Landroid/widget/EditText;)V
    .locals 3

    .prologue
    .line 273
    const-string/jumbo v0, "input_method"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 274
    invoke-virtual {p2}, Landroid/widget/EditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 275
    return-void
.end method

.method a(Lcom/peel/widget/ag;Landroid/widget/EditText;)V
    .locals 1

    .prologue
    .line 279
    new-instance v0, Lcom/peel/ui/dq;

    invoke-direct {v0, p0, p2}, Lcom/peel/ui/dq;-><init>(Lcom/peel/ui/dg;Landroid/widget/EditText;)V

    invoke-virtual {p1, v0}, Lcom/peel/widget/ag;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 287
    return-void
.end method
