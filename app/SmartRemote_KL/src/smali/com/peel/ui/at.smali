.class Lcom/peel/ui/at;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic a:Landroid/content/Context;

.field final synthetic b:Lcom/peel/ui/ar;


# direct methods
.method constructor <init>(Lcom/peel/ui/ar;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 443
    iput-object p1, p0, Lcom/peel/ui/at;->b:Lcom/peel/ui/ar;

    iput-object p2, p0, Lcom/peel/ui/at;->a:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 446
    new-instance v0, Ljava/util/GregorianCalendar;

    invoke-direct {v0}, Ljava/util/GregorianCalendar;-><init>()V

    .line 448
    const/4 v1, 0x6

    iget-object v2, p0, Lcom/peel/ui/at;->b:Lcom/peel/ui/ar;

    invoke-static {v2}, Lcom/peel/ui/ar;->b(Lcom/peel/ui/ar;)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Ljava/util/GregorianCalendar;->add(II)V

    .line 452
    iget-object v1, p0, Lcom/peel/ui/at;->b:Lcom/peel/ui/ar;

    invoke-static {v1}, Lcom/peel/ui/ar;->b(Lcom/peel/ui/ar;)I

    move-result v1

    if-lez v1, :cond_0

    const/16 v1, 0xb

    invoke-virtual {v0, v1, v5}, Ljava/util/GregorianCalendar;->set(II)V

    .line 453
    :cond_0
    const/16 v1, 0xc

    invoke-virtual {v0, v1, v5}, Ljava/util/GregorianCalendar;->set(II)V

    .line 454
    const/16 v1, 0xd

    invoke-virtual {v0, v1, v5}, Ljava/util/GregorianCalendar;->set(II)V

    .line 456
    new-instance v1, Ljava/text/SimpleDateFormat;

    const-string/jumbo v2, "yyyy-MM-dd HH:mm:ss"

    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v1, v2, v3}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 457
    iget-object v2, p0, Lcom/peel/ui/at;->b:Lcom/peel/ui/ar;

    invoke-virtual {v0}, Ljava/util/GregorianCalendar;->getTime()Ljava/util/Date;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/peel/ui/ar;->a(Lcom/peel/ui/ar;Ljava/lang/String;)Ljava/lang/String;

    .line 459
    iget-object v0, p0, Lcom/peel/ui/at;->b:Lcom/peel/ui/ar;

    invoke-static {v0}, Lcom/peel/ui/ar;->c(Lcom/peel/ui/ar;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/ui/at;->a:Landroid/content/Context;

    invoke-static {v1}, Landroid/text/format/DateFormat;->is24HourFormat(Landroid/content/Context;)Z

    move-result v1

    iget-object v2, p0, Lcom/peel/ui/at;->a:Landroid/content/Context;

    iget-object v3, p0, Lcom/peel/ui/at;->a:Landroid/content/Context;

    sget v4, Lcom/peel/ui/ft;->time_pattern:I

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, Lcom/peel/util/bx;->a(Ljava/lang/String;ZLandroid/content/Context;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 460
    const/4 v1, 0x1

    aget-object v1, v0, v1

    if-eqz v1, :cond_1

    .line 461
    iget-object v0, p0, Lcom/peel/ui/at;->b:Lcom/peel/ui/ar;

    invoke-virtual {v0}, Lcom/peel/ui/ar;->a()V

    .line 467
    :goto_0
    return-void

    .line 464
    :cond_1
    new-instance v1, Lcom/peel/widget/ag;

    iget-object v2, p0, Lcom/peel/ui/at;->a:Landroid/content/Context;

    invoke-direct {v1, v2}, Lcom/peel/widget/ag;-><init>(Landroid/content/Context;)V

    sget v2, Lcom/peel/ui/ft;->noschedulesavailable:I

    .line 465
    invoke-virtual {v1, v2}, Lcom/peel/widget/ag;->a(I)Lcom/peel/widget/ag;

    move-result-object v1

    aget-object v0, v0, v5

    invoke-virtual {v1, v0}, Lcom/peel/widget/ag;->b(Ljava/lang/CharSequence;)Lcom/peel/widget/ag;

    move-result-object v0

    sget v1, Lcom/peel/ui/ft;->okay:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/peel/widget/ag;->a(ILandroid/content/DialogInterface$OnClickListener;)Lcom/peel/widget/ag;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/widget/ag;->show()V

    goto :goto_0
.end method
