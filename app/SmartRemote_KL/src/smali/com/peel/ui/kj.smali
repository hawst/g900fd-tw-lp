.class public Lcom/peel/ui/kj;
.super Lcom/peel/d/u;


# static fields
.field private static final am:[I

.field public static e:I

.field public static f:I

.field public static g:Z


# instance fields
.field private aj:J

.field private ak:J

.field private al:Landroid/widget/ListView;

.field private an:[I

.field private ao:[Ljava/lang/String;

.field private ap:I

.field private aq:J

.field private ar:Landroid/view/LayoutInflater;

.field private as:Landroid/widget/TextView;

.field private at:Landroid/widget/LinearLayout;

.field private au:Lcom/peel/ui/kp;

.field private h:Ljava/lang/String;

.field private i:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, -0x1

    .line 47
    const/4 v0, 0x7

    new-array v0, v0, [I

    sget v1, Lcom/peel/ui/ft;->sunday:I

    aput v1, v0, v4

    const/4 v1, 0x1

    sget v2, Lcom/peel/ui/ft;->monday:I

    aput v2, v0, v1

    const/4 v1, 0x2

    sget v2, Lcom/peel/ui/ft;->tuesday:I

    aput v2, v0, v1

    const/4 v1, 0x3

    sget v2, Lcom/peel/ui/ft;->wednesday:I

    aput v2, v0, v1

    const/4 v1, 0x4

    sget v2, Lcom/peel/ui/ft;->thursday:I

    aput v2, v0, v1

    const/4 v1, 0x5

    sget v2, Lcom/peel/ui/ft;->friday:I

    aput v2, v0, v1

    const/4 v1, 0x6

    sget v2, Lcom/peel/ui/ft;->saturday:I

    aput v2, v0, v1

    sput-object v0, Lcom/peel/ui/kj;->am:[I

    .line 55
    sput v3, Lcom/peel/ui/kj;->e:I

    sput v3, Lcom/peel/ui/kj;->f:I

    .line 56
    sput-boolean v4, Lcom/peel/ui/kj;->g:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 43
    invoke-direct {p0}, Lcom/peel/d/u;-><init>()V

    .line 50
    const/4 v0, 0x0

    iput v0, p0, Lcom/peel/ui/kj;->ap:I

    .line 51
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/peel/ui/kj;->aq:J

    .line 57
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/peel/ui/kj;->au:Lcom/peel/ui/kp;

    .line 329
    return-void
.end method

.method private S()V
    .locals 9

    .prologue
    const/4 v0, 0x2

    const/4 v8, 0x0

    const/4 v7, 0x1

    const/4 v6, 0x7

    .line 309
    new-array v1, v6, [Ljava/lang/String;

    iput-object v1, p0, Lcom/peel/ui/kj;->ao:[Ljava/lang/String;

    .line 310
    iget-object v1, p0, Lcom/peel/ui/kj;->ao:[Ljava/lang/String;

    sget v2, Lcom/peel/ui/ft;->yesterday:I

    invoke-virtual {p0, v2}, Lcom/peel/ui/kj;->a(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v8

    .line 311
    iget-object v1, p0, Lcom/peel/ui/kj;->ao:[Ljava/lang/String;

    sget v2, Lcom/peel/ui/ft;->today:I

    invoke-virtual {p0, v2}, Lcom/peel/ui/kj;->a(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v7

    .line 312
    iget-object v1, p0, Lcom/peel/ui/kj;->ao:[Ljava/lang/String;

    sget v2, Lcom/peel/ui/ft;->tomorrow:I

    invoke-virtual {p0, v2}, Lcom/peel/ui/kj;->a(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    .line 313
    new-instance v1, Ljava/util/GregorianCalendar;

    invoke-direct {v1}, Ljava/util/GregorianCalendar;-><init>()V

    .line 314
    invoke-virtual {v1, v6}, Ljava/util/GregorianCalendar;->get(I)I

    move-result v2

    .line 316
    const/4 v1, 0x3

    :goto_0
    if-ge v1, v6, :cond_0

    .line 317
    iget-object v3, p0, Lcom/peel/ui/kj;->ao:[Ljava/lang/String;

    sget-object v4, Lcom/peel/ui/kj;->am:[I

    add-int v5, v1, v2

    add-int/lit8 v5, v5, -0x2

    rem-int/lit8 v5, v5, 0x7

    aget v4, v4, v5

    invoke-virtual {p0, v4}, Lcom/peel/ui/kj;->a(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v1

    .line 316
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 320
    :cond_0
    new-array v1, v6, [I

    iput-object v1, p0, Lcom/peel/ui/kj;->an:[I

    .line 321
    iget-object v1, p0, Lcom/peel/ui/kj;->an:[I

    aput v7, v1, v8

    .line 322
    iget-object v1, p0, Lcom/peel/ui/kj;->an:[I

    iget v2, p0, Lcom/peel/ui/kj;->ap:I

    add-int/lit8 v2, v2, 0x1

    aput v2, v1, v7

    .line 324
    :goto_1
    if-ge v0, v6, :cond_1

    .line 325
    iget-object v1, p0, Lcom/peel/ui/kj;->an:[I

    iget-object v2, p0, Lcom/peel/ui/kj;->an:[I

    add-int/lit8 v3, v0, -0x1

    aget v2, v2, v3

    add-int/lit8 v2, v2, 0x31

    aput v2, v1, v0

    .line 324
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 327
    :cond_1
    return-void
.end method

.method static synthetic a(Lcom/peel/ui/kj;)Landroid/widget/LinearLayout;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/peel/ui/kj;->at:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic a(Lcom/peel/ui/kj;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 43
    iput-object p1, p0, Lcom/peel/ui/kj;->h:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic b(Lcom/peel/ui/kj;)[I
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/peel/ui/kj;->an:[I

    return-object v0
.end method

.method private c()I
    .locals 4

    .prologue
    .line 292
    iget-wide v0, p0, Lcom/peel/ui/kj;->ak:J

    iget-wide v2, p0, Lcom/peel/ui/kj;->aj:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 293
    const/16 v0, 0x32

    .line 304
    :goto_0
    return v0

    .line 295
    :cond_0
    iget-wide v0, p0, Lcom/peel/ui/kj;->ak:J

    iget-wide v2, p0, Lcom/peel/ui/kj;->aj:J

    sub-long/2addr v0, v2

    const-wide/16 v2, 0x3e8

    div-long/2addr v0, v2

    const-wide/16 v2, 0x3c

    div-long/2addr v0, v2

    const-wide/16 v2, 0x1e

    div-long/2addr v0, v2

    long-to-int v0, v0

    .line 296
    if-gez v0, :cond_2

    .line 297
    add-int/lit8 v1, v0, 0x32

    iget v2, p0, Lcom/peel/ui/kj;->ap:I

    if-le v1, v2, :cond_1

    .line 298
    add-int/lit8 v0, v0, 0x32

    goto :goto_0

    .line 300
    :cond_1
    add-int/lit8 v0, v0, 0x31

    goto :goto_0

    .line 304
    :cond_2
    add-int/lit8 v1, v0, 0x32

    add-int/lit8 v0, v0, 0x32

    iget v2, p0, Lcom/peel/ui/kj;->ap:I

    add-int/lit8 v2, v2, 0x1

    sub-int/2addr v0, v2

    div-int/lit8 v0, v0, 0x30

    add-int/2addr v0, v1

    goto :goto_0
.end method

.method static synthetic c(Lcom/peel/ui/kj;)[Ljava/lang/String;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/peel/ui/kj;->ao:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic d(Lcom/peel/ui/kj;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/peel/ui/kj;->as:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic e(Lcom/peel/ui/kj;)Landroid/widget/ListView;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/peel/ui/kj;->al:Landroid/widget/ListView;

    return-object v0
.end method

.method static synthetic f(Lcom/peel/ui/kj;)Lcom/peel/ui/kp;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/peel/ui/kj;->au:Lcom/peel/ui/kp;

    return-object v0
.end method

.method static synthetic g(Lcom/peel/ui/kj;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/peel/ui/kj;->h:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic h(Lcom/peel/ui/kj;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/peel/ui/kj;->a:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic i(Lcom/peel/ui/kj;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/peel/ui/kj;->a:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic j(Lcom/peel/ui/kj;)I
    .locals 1

    .prologue
    .line 43
    iget v0, p0, Lcom/peel/ui/kj;->ap:I

    return v0
.end method

.method static synthetic k(Lcom/peel/ui/kj;)J
    .locals 2

    .prologue
    .line 43
    iget-wide v0, p0, Lcom/peel/ui/kj;->aq:J

    return-wide v0
.end method

.method static synthetic l(Lcom/peel/ui/kj;)Landroid/view/LayoutInflater;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/peel/ui/kj;->ar:Landroid/view/LayoutInflater;

    return-object v0
.end method

.method static synthetic m(Lcom/peel/ui/kj;)J
    .locals 2

    .prologue
    .line 43
    iget-wide v0, p0, Lcom/peel/ui/kj;->aj:J

    return-wide v0
.end method


# virtual methods
.method public W()Z
    .locals 1

    .prologue
    .line 180
    const/4 v0, 0x1

    return v0
.end method

.method public Z()V
    .locals 6

    .prologue
    .line 446
    iget-object v0, p0, Lcom/peel/ui/kj;->d:Lcom/peel/d/a;

    if-nez v0, :cond_2

    .line 447
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 448
    sget v0, Lcom/peel/ui/fp;->menu_remote:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 450
    sget-object v0, Lcom/peel/c/a;->b:Lcom/peel/c/e;

    invoke-static {v0}, Lcom/peel/c/a;->a(Lcom/peel/c/e;)Ljava/lang/Object;

    move-result-object v0

    sget-object v1, Lcom/peel/c/b;->b:Lcom/peel/c/b;

    if-ne v0, v1, :cond_0

    .line 451
    sget v0, Lcom/peel/ui/fp;->overflow_menu_btn:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 454
    :cond_0
    sget-object v0, Lcom/peel/c/a;->b:Lcom/peel/c/e;

    invoke-static {v0}, Lcom/peel/c/a;->a(Lcom/peel/c/e;)Ljava/lang/Object;

    move-result-object v0

    sget-object v1, Lcom/peel/c/b;->a:Lcom/peel/c/b;

    if-ne v0, v1, :cond_1

    .line 455
    sget v0, Lcom/peel/ui/fp;->menu_change_room:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 456
    sget v0, Lcom/peel/ui/fp;->menu_channels:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 457
    sget v0, Lcom/peel/ui/fp;->menu_settings:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 458
    sget v0, Lcom/peel/ui/fp;->menu_about:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 460
    :cond_1
    new-instance v0, Lcom/peel/d/a;

    sget-object v1, Lcom/peel/d/d;->b:Lcom/peel/d/d;

    sget-object v2, Lcom/peel/d/b;->a:Lcom/peel/d/b;

    sget-object v3, Lcom/peel/d/c;->b:Lcom/peel/d/c;

    sget v4, Lcom/peel/ui/ft;->customize_time:I

    invoke-virtual {p0, v4}, Lcom/peel/ui/kj;->a(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct/range {v0 .. v5}, Lcom/peel/d/a;-><init>(Lcom/peel/d/d;Lcom/peel/d/b;Lcom/peel/d/c;Ljava/lang/String;Ljava/util/List;)V

    iput-object v0, p0, Lcom/peel/ui/kj;->d:Lcom/peel/d/a;

    .line 461
    iget-object v0, p0, Lcom/peel/ui/kj;->b:Lcom/peel/d/i;

    iget-object v1, p0, Lcom/peel/ui/kj;->d:Lcom/peel/d/a;

    invoke-virtual {v0, v1}, Lcom/peel/d/i;->a(Lcom/peel/d/a;)V

    .line 465
    :goto_0
    return-void

    .line 463
    :cond_2
    iget-object v0, p0, Lcom/peel/ui/kj;->b:Lcom/peel/d/i;

    iget-object v1, p0, Lcom/peel/ui/kj;->d:Lcom/peel/d/a;

    invoke-virtual {v0, v1}, Lcom/peel/d/i;->a(Lcom/peel/d/a;)V

    goto :goto_0
.end method

.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3

    .prologue
    .line 65
    sget v0, Lcom/peel/ui/fq;->time_list:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 66
    sget v0, Lcom/peel/ui/fp;->time_list:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/peel/ui/kj;->al:Landroid/widget/ListView;

    .line 67
    sget v0, Lcom/peel/ui/fp;->header:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/peel/ui/kj;->at:Landroid/widget/LinearLayout;

    .line 68
    iget-object v0, p0, Lcom/peel/ui/kj;->at:Landroid/widget/LinearLayout;

    sget v2, Lcom/peel/ui/fp;->time_row:I

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/peel/ui/kj;->as:Landroid/widget/TextView;

    .line 71
    iput-object p1, p0, Lcom/peel/ui/kj;->ar:Landroid/view/LayoutInflater;

    .line 72
    return-object v1
.end method

.method public a(Landroid/view/Menu;)V
    .locals 8

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 185
    invoke-virtual {p0}, Lcom/peel/ui/kj;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    check-cast v0, Landroid/support/v7/app/ActionBarActivity;

    invoke-virtual {v0}, Landroid/support/v7/app/ActionBarActivity;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v0

    .line 186
    sget-object v1, Lcom/peel/c/a;->b:Lcom/peel/c/e;

    invoke-static {v1}, Lcom/peel/c/a;->a(Lcom/peel/c/e;)Ljava/lang/Object;

    move-result-object v1

    sget-object v2, Lcom/peel/c/b;->c:Lcom/peel/c/b;

    if-ne v1, v2, :cond_1

    .line 187
    sget v1, Lcom/peel/ui/fq;->custom_action_layout:I

    invoke-virtual {v0, v1}, Landroid/support/v7/app/ActionBar;->setCustomView(I)V

    .line 194
    :goto_0
    invoke-virtual {v0, v3}, Landroid/support/v7/app/ActionBar;->setDisplayShowCustomEnabled(Z)V

    .line 195
    invoke-virtual {v0, v3}, Landroid/support/v7/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 196
    sget-object v1, Lcom/peel/c/a;->b:Lcom/peel/c/e;

    invoke-static {v1}, Lcom/peel/c/a;->a(Lcom/peel/c/e;)Ljava/lang/Object;

    move-result-object v1

    sget-object v2, Lcom/peel/c/b;->a:Lcom/peel/c/b;

    if-ne v1, v2, :cond_3

    .line 197
    invoke-virtual {v0, v3}, Landroid/support/v7/app/ActionBar;->setDisplayShowHomeEnabled(Z)V

    .line 200
    :cond_0
    :goto_1
    invoke-virtual {v0, v3}, Landroid/support/v7/app/ActionBar;->setHomeButtonEnabled(Z)V

    .line 201
    invoke-virtual {v0, v4}, Landroid/support/v7/app/ActionBar;->setNavigationMode(I)V

    .line 202
    invoke-virtual {v0, v4}, Landroid/support/v7/app/ActionBar;->setDisplayShowTitleEnabled(Z)V

    .line 203
    invoke-virtual {p0}, Lcom/peel/ui/kj;->n()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/peel/ui/fo;->action_bar_bg:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v7/app/ActionBar;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 204
    invoke-virtual {v0}, Landroid/support/v7/app/ActionBar;->show()V

    .line 205
    invoke-static {}, Lcom/peel/util/x;->b()Ljava/lang/String;

    move-result-object v2

    .line 206
    invoke-virtual {v0}, Landroid/support/v7/app/ActionBar;->getCustomView()Landroid/view/View;

    move-result-object v0

    sget v1, Lcom/peel/ui/fp;->time_label:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 207
    iget-object v1, p0, Lcom/peel/ui/kj;->b:Lcom/peel/d/i;

    const-string/jumbo v3, "time"

    invoke-virtual {v1, v3}, Lcom/peel/d/i;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 210
    :try_start_0
    sget-object v1, Lcom/peel/util/x;->e:Ljava/lang/ThreadLocal;

    invoke-virtual {v1}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/text/SimpleDateFormat;

    invoke-virtual {v1, v3}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    .line 211
    sget-object v1, Lcom/peel/util/x;->e:Ljava/lang/ThreadLocal;

    invoke-virtual {v1}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/text/SimpleDateFormat;

    invoke-virtual {v1, v2}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Date;->getTime()J

    .line 212
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v2

    .line 213
    invoke-virtual {v2, v4, v5}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 216
    new-instance v1, Ljava/util/GregorianCalendar;

    invoke-direct {v1}, Ljava/util/GregorianCalendar;-><init>()V

    .line 217
    invoke-virtual {v1, v4, v5}, Ljava/util/GregorianCalendar;->setTimeInMillis(J)V

    .line 218
    invoke-static {v3}, Lcom/peel/util/x;->a(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 219
    invoke-virtual {p0}, Lcom/peel/ui/kj;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    invoke-static {v1}, Landroid/text/format/DateFormat;->is24HourFormat(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 220
    sget-object v1, Lcom/peel/util/x;->g:Ljava/lang/ThreadLocal;

    invoke-virtual {v1}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/text/SimpleDateFormat;

    invoke-virtual {v2}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    .line 221
    sget v2, Lcom/peel/ui/ft;->day_time_label:I

    invoke-virtual {p0, v2}, Lcom/peel/ui/kj;->a(I)Ljava/lang/String;

    move-result-object v2

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    const/4 v6, 0x0

    aget-object v3, v3, v6

    aput-object v3, v4, v5

    const/4 v3, 0x1

    aput-object v1, v4, v3

    invoke-static {v2, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 229
    :goto_2
    new-instance v1, Lcom/peel/ui/km;

    invoke-direct {v1, p0}, Lcom/peel/ui/km;-><init>(Lcom/peel/ui/kj;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    .line 238
    :goto_3
    return-void

    .line 188
    :cond_1
    iget-object v1, p0, Lcom/peel/ui/kj;->c:Landroid/os/Bundle;

    const-string/jumbo v2, "yosemite_country"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 189
    sget v1, Lcom/peel/ui/fq;->ss_wataction:I

    invoke-virtual {v0, v1}, Landroid/support/v7/app/ActionBar;->setCustomView(I)V

    goto/16 :goto_0

    .line 192
    :cond_2
    sget v1, Lcom/peel/ui/fq;->custom_layout:I

    invoke-virtual {v0, v1}, Landroid/support/v7/app/ActionBar;->setCustomView(I)V

    goto/16 :goto_0

    .line 198
    :cond_3
    sget-object v1, Lcom/peel/c/a;->b:Lcom/peel/c/e;

    invoke-static {v1}, Lcom/peel/c/a;->a(Lcom/peel/c/e;)Ljava/lang/Object;

    move-result-object v1

    sget-object v2, Lcom/peel/c/b;->b:Lcom/peel/c/b;

    if-ne v1, v2, :cond_0

    .line 199
    invoke-virtual {v0, v4}, Landroid/support/v7/app/ActionBar;->setDisplayShowHomeEnabled(Z)V

    goto/16 :goto_1

    .line 227
    :cond_4
    :try_start_1
    sget v1, Lcom/peel/ui/ft;->day_time_label:I

    invoke-virtual {p0, v1}, Lcom/peel/ui/kj;->a(I)Ljava/lang/String;

    move-result-object v2

    const/4 v1, 0x2

    new-array v4, v1, [Ljava/lang/Object;

    const/4 v1, 0x0

    const/4 v5, 0x0

    aget-object v5, v3, v5

    aput-object v5, v4, v1

    const/4 v5, 0x1

    const/4 v1, 0x1

    aget-object v1, v3, v1

    const-string/jumbo v6, "AM"

    invoke-virtual {v1, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_5

    const/4 v1, 0x1

    aget-object v1, v3, v1

    const-string/jumbo v3, "AM"

    invoke-virtual {p0}, Lcom/peel/ui/kj;->n()Landroid/content/res/Resources;

    move-result-object v6

    sget v7, Lcom/peel/ui/ft;->time_am:I

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v3, v6}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    :goto_4
    aput-object v1, v4, v5

    invoke-static {v2, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V
    :try_end_1
    .catch Ljava/text/ParseException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2

    .line 235
    :catch_0
    move-exception v0

    .line 236
    invoke-virtual {v0}, Ljava/text/ParseException;->printStackTrace()V

    goto :goto_3

    .line 227
    :cond_5
    const/4 v1, 0x1

    :try_start_2
    aget-object v1, v3, v1

    const-string/jumbo v3, "PM"

    invoke-virtual {p0}, Lcom/peel/ui/kj;->n()Landroid/content/res/Resources;

    move-result-object v6

    sget v7, Lcom/peel/ui/ft;->time_pm:I

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v3, v6}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_2
    .catch Ljava/text/ParseException; {:try_start_2 .. :try_end_2} :catch_0

    move-result-object v1

    goto :goto_4
.end method

.method public a(Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 77
    invoke-super {p0, p1}, Lcom/peel/d/u;->a(Landroid/view/MenuItem;)Z

    move-result v0

    return v0
.end method

.method public ab()Z
    .locals 1

    .prologue
    .line 469
    const/4 v0, 0x0

    return v0
.end method

.method public d(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 82
    invoke-super {p0, p1}, Lcom/peel/d/u;->d(Landroid/os/Bundle;)V

    .line 84
    invoke-static {}, Lcom/peel/util/x;->b()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/ui/kj;->h:Ljava/lang/String;

    .line 85
    iget-object v0, p0, Lcom/peel/ui/kj;->b:Lcom/peel/d/i;

    const-string/jumbo v1, "time"

    invoke-virtual {v0, v1}, Lcom/peel/d/i;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/ui/kj;->i:Ljava/lang/String;

    .line 88
    :try_start_0
    sget-object v0, Lcom/peel/util/x;->e:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/text/SimpleDateFormat;

    iget-object v1, p0, Lcom/peel/ui/kj;->h:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/peel/ui/kj;->aj:J

    .line 89
    sget-object v0, Lcom/peel/util/x;->e:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/text/SimpleDateFormat;

    iget-object v1, p0, Lcom/peel/ui/kj;->i:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/peel/ui/kj;->ak:J

    .line 91
    iget-wide v0, p0, Lcom/peel/ui/kj;->aj:J

    iget-wide v2, p0, Lcom/peel/ui/kj;->ak:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 92
    const/4 v0, -0x1

    sput v0, Lcom/peel/ui/kj;->f:I

    .line 95
    :cond_0
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 96
    iget-wide v2, p0, Lcom/peel/ui/kj;->aj:J

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 97
    const/16 v1, 0xb

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->set(II)V

    .line 98
    const/16 v1, 0xc

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->set(II)V

    .line 99
    const/16 v1, 0xd

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->set(II)V

    .line 100
    const/16 v1, 0xe

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->set(II)V

    .line 103
    iget-wide v2, p0, Lcom/peel/ui/kj;->aj:J

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v0

    sub-long v0, v2, v0

    long-to-int v0, v0

    div-int/lit16 v0, v0, 0x3e8

    div-int/lit8 v0, v0, 0x3c

    div-int/lit8 v0, v0, 0x1e

    rsub-int/lit8 v0, v0, 0x30

    .line 105
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v1

    .line 106
    iget-wide v2, p0, Lcom/peel/ui/kj;->aj:J

    invoke-virtual {v1, v2, v3}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 107
    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/peel/ui/kj;->ap:I

    .line 108
    iget-wide v0, p0, Lcom/peel/ui/kj;->aj:J

    const-wide/32 v2, 0x541d340

    sub-long/2addr v0, v2

    iput-wide v0, p0, Lcom/peel/ui/kj;->aq:J

    .line 109
    invoke-direct {p0}, Lcom/peel/ui/kj;->S()V

    .line 110
    invoke-direct {p0}, Lcom/peel/ui/kj;->c()I

    .line 111
    new-instance v0, Lcom/peel/ui/kp;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/peel/ui/kp;-><init>(Lcom/peel/ui/kj;Lcom/peel/ui/kk;)V

    iput-object v0, p0, Lcom/peel/ui/kj;->au:Lcom/peel/ui/kp;

    .line 112
    iget-object v0, p0, Lcom/peel/ui/kj;->al:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/peel/ui/kj;->au:Lcom/peel/ui/kp;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 114
    iget-object v0, p0, Lcom/peel/ui/kj;->al:Landroid/widget/ListView;

    invoke-direct {p0}, Lcom/peel/ui/kj;->c()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setSelection(I)V

    .line 115
    iget-object v0, p0, Lcom/peel/ui/kj;->i:Ljava/lang/String;

    invoke-static {v0}, Lcom/peel/util/x;->a(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 116
    iget-object v1, p0, Lcom/peel/ui/kj;->as:Landroid/widget/TextView;

    const/4 v2, 0x0

    aget-object v0, v0, v2

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 118
    iget-object v0, p0, Lcom/peel/ui/kj;->al:Landroid/widget/ListView;

    new-instance v1, Lcom/peel/ui/kk;

    invoke-direct {v1, p0}, Lcom/peel/ui/kk;-><init>(Lcom/peel/ui/kj;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 143
    iget-object v0, p0, Lcom/peel/ui/kj;->al:Landroid/widget/ListView;

    new-instance v1, Lcom/peel/ui/kl;

    invoke-direct {v1, p0}, Lcom/peel/ui/kl;-><init>(Lcom/peel/ui/kj;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    .line 176
    :goto_0
    return-void

    .line 173
    :catch_0
    move-exception v0

    .line 174
    invoke-virtual {v0}, Ljava/text/ParseException;->printStackTrace()V

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 8

    .prologue
    const-wide/16 v6, 0x32

    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 242
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    .line 243
    sget v3, Lcom/peel/ui/fp;->prism_txt:I

    if-ne v2, v3, :cond_1

    .line 244
    iget-object v2, p0, Lcom/peel/ui/kj;->c:Landroid/os/Bundle;

    const-string/jumbo v3, "yosemite_installed"

    invoke-virtual {v2, v3, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    .line 245
    iget-object v3, p0, Lcom/peel/ui/kj;->c:Landroid/os/Bundle;

    const-string/jumbo v4, "yosemite_country"

    invoke-virtual {v3, v4, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    .line 246
    if-eqz v2, :cond_2

    .line 247
    const-class v0, Lcom/peel/ui/lq;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v2, "launch yosemite"

    new-instance v3, Lcom/peel/ui/kn;

    invoke-direct {v3, p0}, Lcom/peel/ui/kn;-><init>(Lcom/peel/ui/kj;)V

    invoke-static {v0, v2, v3, v6, v7}, Lcom/peel/util/i;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;J)Ljava/lang/Runnable;

    .line 285
    :cond_0
    :goto_0
    :try_start_0
    invoke-static {}, Lcom/peel/util/a/f;->a()Lcom/peel/util/a/f;

    move-result-object v0

    sget-object v2, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v2}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v2

    if-nez v2, :cond_5

    :goto_1
    const/16 v2, 0x1f4a

    const/16 v3, 0x7db

    invoke-virtual {p0}, Lcom/peel/ui/kj;->m()Landroid/support/v4/app/ae;

    move-result-object v4

    invoke-static {v4}, Lcom/peel/util/bx;->d(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_6

    const-string/jumbo v4, "XX"

    :goto_2
    const/4 v5, 0x0

    sget-object v6, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const/4 v7, 0x0

    invoke-virtual/range {v0 .. v7}, Lcom/peel/util/a/f;->a(IIILjava/lang/String;ILjava/lang/String;I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    .line 289
    :cond_1
    :goto_3
    return-void

    .line 255
    :cond_2
    if-eqz v3, :cond_0

    .line 257
    :try_start_1
    invoke-virtual {p0}, Lcom/peel/ui/kj;->m()Landroid/support/v4/app/ae;

    move-result-object v2

    const-string/jumbo v3, "com.sec.yosemite.phone"

    invoke-static {v2, v3}, Lcom/peel/util/bx;->c(Landroid/content/Context;Ljava/lang/String;)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-result v2

    .line 259
    if-nez v2, :cond_3

    .line 261
    :try_start_2
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v2, "android.intent.action.VIEW"

    const-string/jumbo v3, "market://details?id=com.sec.yosemite.phone"

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-direct {v0, v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 262
    const/high16 v2, 0x40080000    # 2.125f

    invoke-virtual {v0, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 263
    invoke-virtual {p0, v0}, Lcom/peel/ui/kj;->a(Landroid/content/Intent;)V
    :try_end_2
    .catch Landroid/content/ActivityNotFoundException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_0

    .line 264
    :catch_0
    move-exception v0

    .line 265
    :try_start_3
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v2, "android.intent.action.VIEW"

    const-string/jumbo v3, "http://play.google.com/store/apps/details?id=com.sec.yosemite.phone"

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-direct {v0, v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 266
    const/high16 v2, 0x40080000    # 2.125f

    invoke-virtual {v0, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 267
    invoke-virtual {p0, v0}, Lcom/peel/ui/kj;->a(Landroid/content/Intent;)V

    goto :goto_0

    .line 282
    :catch_1
    move-exception v0

    goto :goto_0

    .line 270
    :cond_3
    invoke-virtual {p0}, Lcom/peel/ui/kj;->m()Landroid/support/v4/app/ae;

    move-result-object v3

    invoke-static {v3}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v3

    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    const-string/jumbo v4, "yosemite_enabled"

    invoke-interface {v3, v4, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    invoke-interface {v3}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 271
    iget-object v3, p0, Lcom/peel/ui/kj;->b:Lcom/peel/d/i;

    const-string/jumbo v4, "yosemite_enabled"

    if-eqz v2, :cond_4

    move v0, v1

    :cond_4
    invoke-virtual {v3, v4, v0}, Lcom/peel/d/i;->b(Ljava/lang/String;I)V

    .line 273
    const-class v0, Lcom/peel/ui/lq;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v2, "launch yosemite"

    new-instance v3, Lcom/peel/ui/ko;

    invoke-direct {v3, p0}, Lcom/peel/ui/ko;-><init>(Lcom/peel/ui/kj;)V

    const-wide/16 v4, 0x32

    invoke-static {v0, v2, v3, v4, v5}, Lcom/peel/util/i;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;J)Ljava/lang/Runnable;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    goto/16 :goto_0

    .line 285
    :cond_5
    :try_start_4
    sget-object v1, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v1}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/data/at;->f()I

    move-result v1

    goto/16 :goto_1

    :cond_6
    invoke-virtual {p0}, Lcom/peel/ui/kj;->m()Landroid/support/v4/app/ae;

    move-result-object v4

    invoke-static {v4}, Lcom/peel/util/bx;->d(Landroid/content/Context;)Ljava/lang/String;
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_2

    move-result-object v4

    goto/16 :goto_2

    .line 286
    :catch_2
    move-exception v0

    goto/16 :goto_3
.end method
