.class public Lcom/peel/ui/gm;
.super Lcom/peel/d/u;


# static fields
.field private static final e:Ljava/lang/String;


# instance fields
.field private aj:Lcom/peel/content/library/LiveLibrary;

.field private ak:Landroid/app/ProgressDialog;

.field private al:Ljava/lang/String;

.field private am:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/peel/content/listing/Listing;",
            ">;"
        }
    .end annotation
.end field

.field private an:Ljava/util/concurrent/Future;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/Future",
            "<*>;"
        }
    .end annotation
.end field

.field private f:Landroid/view/View;

.field private g:Landroid/widget/TextView;

.field private h:Landroid/widget/TextView;

.field private i:Landroid/widget/ListView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 41
    const-class v0, Lcom/peel/ui/gm;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/peel/ui/gm;->e:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 40
    invoke-direct {p0}, Lcom/peel/d/u;-><init>()V

    .line 118
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/peel/ui/gm;->an:Ljava/util/concurrent/Future;

    return-void
.end method

.method private S()V
    .locals 1

    .prologue
    .line 242
    iget-object v0, p0, Lcom/peel/ui/gm;->ak:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/peel/ui/gm;->ak:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-nez v0, :cond_1

    .line 244
    :cond_0
    :goto_0
    return-void

    .line 243
    :cond_1
    iget-object v0, p0, Lcom/peel/ui/gm;->ak:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    goto :goto_0
.end method

.method static synthetic a(Lcom/peel/ui/gm;)Landroid/view/View;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/peel/ui/gm;->f:Landroid/view/View;

    return-object v0
.end method

.method static synthetic a(Lcom/peel/ui/gm;Ljava/util/List;)Ljava/util/List;
    .locals 0

    .prologue
    .line 40
    iput-object p1, p0, Lcom/peel/ui/gm;->am:Ljava/util/List;

    return-object p1
.end method

.method static synthetic a(Lcom/peel/ui/gm;Ljava/util/concurrent/Future;)Ljava/util/concurrent/Future;
    .locals 0

    .prologue
    .line 40
    iput-object p1, p0, Lcom/peel/ui/gm;->an:Ljava/util/concurrent/Future;

    return-object p1
.end method

.method private a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 222
    iget-object v0, p0, Lcom/peel/ui/gm;->ak:Landroid/app/ProgressDialog;

    if-nez v0, :cond_2

    .line 223
    invoke-virtual {p0}, Lcom/peel/ui/gm;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    .line 224
    if-nez v0, :cond_1

    .line 239
    :cond_0
    :goto_0
    return-void

    .line 225
    :cond_1
    new-instance v1, Landroid/app/ProgressDialog;

    sget v2, Lcom/peel/ui/fu;->DialogTheme:I

    invoke-direct {v1, v0, v2}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;I)V

    iput-object v1, p0, Lcom/peel/ui/gm;->ak:Landroid/app/ProgressDialog;

    .line 226
    iget-object v0, p0, Lcom/peel/ui/gm;->ak:Landroid/app/ProgressDialog;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setCanceledOnTouchOutside(Z)V

    .line 237
    :cond_2
    iget-object v0, p0, Lcom/peel/ui/gm;->ak:Landroid/app/ProgressDialog;

    invoke-virtual {v0, p1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 238
    iget-object v0, p0, Lcom/peel/ui/gm;->ak:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/peel/ui/gm;->ak:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    goto :goto_0
.end method

.method private a(Ljava/lang/String;Z)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 120
    invoke-virtual {p0}, Lcom/peel/ui/gm;->Z()V

    .line 121
    sget-object v0, Lcom/peel/content/a;->h:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-nez v0, :cond_1

    .line 206
    :cond_0
    :goto_0
    return-void

    .line 122
    :cond_1
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-eqz v0, :cond_0

    .line 124
    invoke-virtual {p0}, Lcom/peel/ui/gm;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    const-string/jumbo v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/ae;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 125
    iget-object v1, p0, Lcom/peel/ui/gm;->f:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 127
    sget v0, Lcom/peel/ui/ft;->searching_for:I

    new-array v1, v3, [Ljava/lang/Object;

    aput-object p1, v1, v2

    invoke-virtual {p0, v0, v1}, Lcom/peel/ui/gm;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/peel/ui/gm;->a(Ljava/lang/String;)V

    .line 129
    iget-object v0, p0, Lcom/peel/ui/gm;->an:Ljava/util/concurrent/Future;

    if-eqz v0, :cond_2

    .line 130
    iget-object v0, p0, Lcom/peel/ui/gm;->an:Ljava/util/concurrent/Future;

    invoke-interface {v0, v3}, Ljava/util/concurrent/Future;->cancel(Z)Z

    .line 133
    :cond_2
    sget-object v0, Lcom/peel/ui/gm;->e:Ljava/lang/String;

    const-string/jumbo v1, "performSearch"

    new-instance v2, Lcom/peel/ui/go;

    invoke-direct {v2, p0, p1}, Lcom/peel/ui/go;-><init>(Lcom/peel/ui/gm;Ljava/lang/String;)V

    invoke-static {v0, v1, v2}, Lcom/peel/util/i;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/ui/gm;->an:Ljava/util/concurrent/Future;

    goto :goto_0
.end method

.method static synthetic b(Lcom/peel/ui/gm;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/peel/ui/gm;->al:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 40
    sget-object v0, Lcom/peel/ui/gm;->e:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic c(Lcom/peel/ui/gm;)V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0}, Lcom/peel/ui/gm;->S()V

    return-void
.end method

.method static synthetic d(Lcom/peel/ui/gm;)Lcom/peel/content/library/LiveLibrary;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/peel/ui/gm;->aj:Lcom/peel/content/library/LiveLibrary;

    return-object v0
.end method

.method static synthetic e(Lcom/peel/ui/gm;)Ljava/util/List;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/peel/ui/gm;->am:Ljava/util/List;

    return-object v0
.end method

.method static synthetic f(Lcom/peel/ui/gm;)Landroid/widget/ListView;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/peel/ui/gm;->i:Landroid/widget/ListView;

    return-object v0
.end method

.method static synthetic g(Lcom/peel/ui/gm;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/peel/ui/gm;->g:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic h(Lcom/peel/ui/gm;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/peel/ui/gm;->h:Landroid/widget/TextView;

    return-object v0
.end method


# virtual methods
.method public Z()V
    .locals 6

    .prologue
    .line 255
    iget-object v0, p0, Lcom/peel/ui/gm;->d:Lcom/peel/d/a;

    if-nez v0, :cond_0

    .line 256
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 257
    sget v0, Lcom/peel/ui/fp;->menu_search:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 258
    iget-object v0, p0, Lcom/peel/ui/gm;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "keyword"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/peel/ui/gm;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "keyword"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-static {v0}, Lcom/peel/d/k;->a(Ljava/lang/String;)V

    .line 259
    new-instance v0, Lcom/peel/d/a;

    sget-object v1, Lcom/peel/d/d;->b:Lcom/peel/d/d;

    sget-object v2, Lcom/peel/d/b;->a:Lcom/peel/d/b;

    sget-object v3, Lcom/peel/d/c;->b:Lcom/peel/d/c;

    const/4 v4, 0x0

    invoke-direct/range {v0 .. v5}, Lcom/peel/d/a;-><init>(Lcom/peel/d/d;Lcom/peel/d/b;Lcom/peel/d/c;Ljava/lang/String;Ljava/util/List;)V

    iput-object v0, p0, Lcom/peel/ui/gm;->d:Lcom/peel/d/a;

    .line 261
    :cond_0
    iget-object v0, p0, Lcom/peel/ui/gm;->b:Lcom/peel/d/i;

    iget-object v1, p0, Lcom/peel/ui/gm;->d:Lcom/peel/d/a;

    invoke-virtual {v0, v1}, Lcom/peel/d/i;->a(Lcom/peel/d/a;)V

    .line 262
    return-void

    .line 258
    :cond_1
    const-string/jumbo v0, ""

    goto :goto_0
.end method

.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    .line 53
    sget v0, Lcom/peel/ui/fq;->search:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/ui/gm;->f:Landroid/view/View;

    .line 54
    iget-object v0, p0, Lcom/peel/ui/gm;->f:Landroid/view/View;

    sget v1, Lcom/peel/ui/fp;->list:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/peel/ui/gm;->i:Landroid/widget/ListView;

    .line 55
    iget-object v0, p0, Lcom/peel/ui/gm;->f:Landroid/view/View;

    sget v1, Lcom/peel/ui/fp;->empty:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/peel/ui/gm;->g:Landroid/widget/TextView;

    .line 56
    iget-object v0, p0, Lcom/peel/ui/gm;->f:Landroid/view/View;

    sget v1, Lcom/peel/ui/fp;->listTitle:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/peel/ui/gm;->h:Landroid/widget/TextView;

    .line 57
    iget-object v0, p0, Lcom/peel/ui/gm;->f:Landroid/view/View;

    return-object v0
.end method

.method public a_(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 62
    invoke-super {p0, p1}, Lcom/peel/d/u;->a_(Landroid/os/Bundle;)V

    .line 63
    const-string/jumbo v0, "live"

    invoke-static {v0}, Lcom/peel/content/a;->c(Ljava/lang/String;)Lcom/peel/content/library/Library;

    move-result-object v0

    check-cast v0, Lcom/peel/content/library/LiveLibrary;

    iput-object v0, p0, Lcom/peel/ui/gm;->aj:Lcom/peel/content/library/LiveLibrary;

    .line 64
    return-void
.end method

.method public c(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 110
    invoke-super {p0, p1}, Lcom/peel/d/u;->c(Landroid/os/Bundle;)V

    .line 111
    sget-object v0, Lcom/peel/content/a;->h:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/peel/ui/gm;->v()Landroid/view/View;

    move-result-object v0

    if-nez v0, :cond_1

    .line 116
    :cond_0
    :goto_0
    return-void

    .line 113
    :cond_1
    const-string/jumbo v0, "keyword"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 114
    const-string/jumbo v0, "keyword"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/peel/ui/gm;->a(Ljava/lang/String;Z)V

    goto :goto_0
.end method

.method public e(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 210
    iget-object v0, p0, Lcom/peel/ui/gm;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "keyword"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "keyword"

    iget-object v1, p0, Lcom/peel/ui/gm;->c:Landroid/os/Bundle;

    const-string/jumbo v2, "keyword"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 211
    :cond_0
    invoke-super {p0, p1}, Lcom/peel/d/u;->e(Landroid/os/Bundle;)V

    .line 212
    return-void
.end method

.method public g()V
    .locals 1

    .prologue
    .line 216
    invoke-super {p0}, Lcom/peel/d/u;->g()V

    .line 217
    invoke-direct {p0}, Lcom/peel/ui/gm;->S()V

    .line 218
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/peel/ui/gm;->ak:Landroid/app/ProgressDialog;

    .line 219
    return-void
.end method

.method public h(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 68
    invoke-super {p0, p1}, Lcom/peel/d/u;->h(Landroid/os/Bundle;)V

    .line 69
    invoke-virtual {p0}, Lcom/peel/ui/gm;->Z()V

    .line 71
    iget-object v0, p0, Lcom/peel/ui/gm;->i:Landroid/widget/ListView;

    new-instance v1, Lcom/peel/ui/gn;

    invoke-direct {v1, p0}, Lcom/peel/ui/gn;-><init>(Lcom/peel/ui/gm;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 96
    if-eqz p1, :cond_0

    const-string/jumbo v0, "keyword"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 97
    iget-object v0, p0, Lcom/peel/ui/gm;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "keyword"

    const-string/jumbo v2, "keyword"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 100
    :cond_0
    invoke-virtual {p0}, Lcom/peel/ui/gm;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string/jumbo v1, "country_ISO"

    const-string/jumbo v2, "US"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/ui/gm;->al:Ljava/lang/String;

    .line 102
    invoke-static {}, Lcom/peel/util/a/f;->a()Lcom/peel/util/a/f;

    move-result-object v1

    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v0}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v0

    if-nez v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    const/16 v2, 0x3f7

    const/16 v3, 0x7d4

    invoke-virtual {v1, v0, v2, v3}, Lcom/peel/util/a/f;->a(III)V

    .line 105
    sget-object v0, Lcom/peel/content/a;->h:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/peel/ui/gm;->c:Landroid/os/Bundle;

    invoke-virtual {p0, v0}, Lcom/peel/ui/gm;->c(Landroid/os/Bundle;)V

    .line 106
    :cond_1
    return-void

    .line 102
    :cond_2
    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v0}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/data/at;->f()I

    move-result v0

    goto :goto_0
.end method

.method public x()V
    .locals 3

    .prologue
    .line 248
    invoke-super {p0}, Lcom/peel/d/u;->x()V

    .line 249
    iget-object v0, p0, Lcom/peel/ui/gm;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "grid_state"

    iget-object v2, p0, Lcom/peel/ui/gm;->i:Landroid/widget/ListView;

    invoke-virtual {v2}, Landroid/widget/ListView;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 250
    return-void
.end method
