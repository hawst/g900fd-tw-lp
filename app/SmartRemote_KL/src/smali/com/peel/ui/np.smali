.class Lcom/peel/ui/np;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/peel/content/listing/Listing;

.field final synthetic b:[Lcom/peel/content/listing/Listing;

.field final synthetic c:Ljava/lang/String;

.field final synthetic d:I

.field final synthetic e:Lcom/peel/ui/nm;


# direct methods
.method constructor <init>(Lcom/peel/ui/nm;Lcom/peel/content/listing/Listing;[Lcom/peel/content/listing/Listing;Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 207
    iput-object p1, p0, Lcom/peel/ui/np;->e:Lcom/peel/ui/nm;

    iput-object p2, p0, Lcom/peel/ui/np;->a:Lcom/peel/content/listing/Listing;

    iput-object p3, p0, Lcom/peel/ui/np;->b:[Lcom/peel/content/listing/Listing;

    iput-object p4, p0, Lcom/peel/ui/np;->c:Ljava/lang/String;

    iput p5, p0, Lcom/peel/ui/np;->d:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 11

    .prologue
    const/16 v3, 0x7dd

    const/4 v10, 0x1

    const/4 v7, 0x0

    .line 211
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 213
    const-string/jumbo v1, "libraryIds"

    new-array v2, v10, [Ljava/lang/String;

    iget-object v4, p0, Lcom/peel/ui/np;->e:Lcom/peel/ui/nm;

    iget-object v4, v4, Lcom/peel/ui/nm;->a:Lcom/peel/ui/nl;

    invoke-static {v4}, Lcom/peel/ui/nl;->a(Lcom/peel/ui/nl;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v7

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putStringArray(Ljava/lang/String;[Ljava/lang/String;)V

    .line 214
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "listings/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/peel/ui/np;->e:Lcom/peel/ui/nm;

    iget-object v2, v2, Lcom/peel/ui/nm;->a:Lcom/peel/ui/nl;

    invoke-static {v2}, Lcom/peel/ui/nl;->a(Lcom/peel/ui/nl;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/peel/ui/nq;

    invoke-direct {v2, p0}, Lcom/peel/ui/nq;-><init>(Lcom/peel/ui/np;)V

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 215
    const-string/jumbo v1, "selected"

    iget-object v2, p0, Lcom/peel/ui/np;->b:[Lcom/peel/content/listing/Listing;

    aget-object v2, v2, v7

    invoke-virtual {v2}, Lcom/peel/content/listing/Listing;->f()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 216
    const-string/jumbo v1, "context_id"

    invoke-virtual {v0, v1, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 217
    const-string/jumbo v1, "category"

    iget-object v2, p0, Lcom/peel/ui/np;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 218
    iget-object v1, p0, Lcom/peel/ui/np;->e:Lcom/peel/ui/nm;

    iget-object v1, v1, Lcom/peel/ui/nm;->a:Lcom/peel/ui/nl;

    invoke-virtual {v1}, Lcom/peel/ui/nl;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    const-class v2, Lcom/peel/ui/b/ag;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lcom/peel/d/e;->b(Landroid/support/v4/app/ae;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 223
    invoke-static {}, Lcom/peel/util/a/f;->a()Lcom/peel/util/a/f;

    move-result-object v0

    sget-object v1, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v1}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v1

    if-nez v1, :cond_0

    move v1, v10

    :goto_0
    const/16 v2, 0x4b1

    const-string/jumbo v4, "favorites"

    iget v5, p0, Lcom/peel/ui/np;->d:I

    iget-object v6, p0, Lcom/peel/ui/np;->a:Lcom/peel/content/listing/Listing;

    .line 225
    invoke-static {v6}, Lcom/peel/util/bx;->b(Lcom/peel/content/listing/Listing;)Ljava/lang/String;

    move-result-object v6

    iget-object v8, p0, Lcom/peel/ui/np;->a:Lcom/peel/content/listing/Listing;

    invoke-virtual {v8}, Lcom/peel/content/listing/Listing;->h()Ljava/lang/String;

    move-result-object v8

    move v9, v7

    .line 223
    invoke-virtual/range {v0 .. v9}, Lcom/peel/util/a/f;->a(IIILjava/lang/String;ILjava/lang/String;ILjava/lang/String;I)V

    .line 227
    invoke-static {}, Lcom/peel/util/a/f;->a()Lcom/peel/util/a/f;

    move-result-object v0

    sget-object v1, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v1}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v1

    if-nez v1, :cond_1

    move v1, v10

    :goto_1
    const/16 v2, 0x4b3

    const-string/jumbo v4, "favorites"

    iget v5, p0, Lcom/peel/ui/np;->d:I

    iget-object v6, p0, Lcom/peel/ui/np;->a:Lcom/peel/content/listing/Listing;

    .line 229
    invoke-static {v6}, Lcom/peel/util/bx;->b(Lcom/peel/content/listing/Listing;)Ljava/lang/String;

    move-result-object v6

    iget-object v8, p0, Lcom/peel/ui/np;->a:Lcom/peel/content/listing/Listing;

    invoke-virtual {v8}, Lcom/peel/content/listing/Listing;->h()Ljava/lang/String;

    move-result-object v8

    move v9, v7

    .line 227
    invoke-virtual/range {v0 .. v9}, Lcom/peel/util/a/f;->a(IIILjava/lang/String;ILjava/lang/String;ILjava/lang/String;I)V

    .line 231
    return-void

    .line 223
    :cond_0
    sget-object v1, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v1}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/data/at;->f()I

    move-result v1

    goto :goto_0

    .line 227
    :cond_1
    sget-object v1, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v1}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/data/at;->f()I

    move-result v1

    goto :goto_1
.end method
