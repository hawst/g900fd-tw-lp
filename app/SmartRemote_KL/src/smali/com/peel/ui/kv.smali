.class Lcom/peel/ui/kv;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Landroid/os/Bundle;

.field final synthetic b:I

.field final synthetic c:Lcom/peel/content/listing/Listing;

.field final synthetic d:Lcom/peel/ui/ks;


# direct methods
.method constructor <init>(Lcom/peel/ui/ks;Landroid/os/Bundle;ILcom/peel/content/listing/Listing;)V
    .locals 0

    .prologue
    .line 271
    iput-object p1, p0, Lcom/peel/ui/kv;->d:Lcom/peel/ui/ks;

    iput-object p2, p0, Lcom/peel/ui/kv;->a:Landroid/os/Bundle;

    iput p3, p0, Lcom/peel/ui/kv;->b:I

    iput-object p4, p0, Lcom/peel/ui/kv;->c:Lcom/peel/content/listing/Listing;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 10

    .prologue
    const/16 v2, 0x4b3

    const/4 v4, 0x1

    const/4 v7, 0x0

    .line 274
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "dismiss_toast"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 275
    iget-object v1, p0, Lcom/peel/ui/kv;->d:Lcom/peel/ui/ks;

    iget-object v1, v1, Lcom/peel/ui/ks;->a:Lcom/peel/ui/kr;

    invoke-virtual {v1}, Lcom/peel/ui/kr;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/ae;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/support/v4/a/q;->a(Landroid/content/Context;)Landroid/support/v4/a/q;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/support/v4/a/q;->a(Landroid/content/Intent;)Z

    .line 277
    iget-object v0, p0, Lcom/peel/ui/kv;->d:Lcom/peel/ui/ks;

    iget-object v0, v0, Lcom/peel/ui/ks;->a:Lcom/peel/ui/kr;

    invoke-static {v0}, Lcom/peel/ui/kr;->a(Lcom/peel/ui/kr;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iget-object v1, p0, Lcom/peel/ui/kv;->d:Lcom/peel/ui/ks;

    iget-object v1, v1, Lcom/peel/ui/ks;->a:Lcom/peel/ui/kr;

    invoke-static {v1}, Lcom/peel/ui/kr;->a(Lcom/peel/ui/kr;)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/FrameLayout;

    invoke-virtual {v1}, Landroid/widget/FrameLayout;->getChildCount()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->removeViewAt(I)V

    .line 278
    iget-object v0, p0, Lcom/peel/ui/kv;->d:Lcom/peel/ui/ks;

    iget-object v0, v0, Lcom/peel/ui/ks;->a:Lcom/peel/ui/kr;

    iget-object v0, v0, Lcom/peel/ui/kr;->c:Landroid/os/Bundle;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/peel/ui/kv;->d:Lcom/peel/ui/ks;

    iget-object v0, v0, Lcom/peel/ui/ks;->a:Lcom/peel/ui/kr;

    iget-object v0, v0, Lcom/peel/ui/kr;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "context_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 279
    iget-object v0, p0, Lcom/peel/ui/kv;->d:Lcom/peel/ui/ks;

    iget-object v0, v0, Lcom/peel/ui/ks;->a:Lcom/peel/ui/kr;

    iget-object v0, v0, Lcom/peel/ui/kr;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "context_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v3

    .line 280
    iget-object v0, p0, Lcom/peel/ui/kv;->a:Landroid/os/Bundle;

    const-string/jumbo v1, "context_id"

    invoke-virtual {v0, v1, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 284
    invoke-static {}, Lcom/peel/util/a/f;->a()Lcom/peel/util/a/f;

    move-result-object v0

    sget-object v1, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v1}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v1

    if-nez v1, :cond_0

    move v1, v4

    :goto_0
    iget-object v4, p0, Lcom/peel/ui/kv;->a:Landroid/os/Bundle;

    const-string/jumbo v5, "category"

    .line 286
    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iget v5, p0, Lcom/peel/ui/kv;->b:I

    iget-object v6, p0, Lcom/peel/ui/kv;->c:Lcom/peel/content/listing/Listing;

    invoke-static {v6}, Lcom/peel/util/bx;->b(Lcom/peel/content/listing/Listing;)Ljava/lang/String;

    move-result-object v6

    iget-object v8, p0, Lcom/peel/ui/kv;->c:Lcom/peel/content/listing/Listing;

    invoke-virtual {v8}, Lcom/peel/content/listing/Listing;->h()Ljava/lang/String;

    move-result-object v8

    move v9, v7

    .line 284
    invoke-virtual/range {v0 .. v9}, Lcom/peel/util/a/f;->a(IIILjava/lang/String;ILjava/lang/String;ILjava/lang/String;I)V

    .line 296
    :goto_1
    iget-object v0, p0, Lcom/peel/ui/kv;->d:Lcom/peel/ui/ks;

    iget-object v0, v0, Lcom/peel/ui/ks;->a:Lcom/peel/ui/kr;

    invoke-virtual {v0}, Lcom/peel/ui/kr;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    const-class v1, Lcom/peel/ui/b/ag;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/peel/ui/kv;->a:Landroid/os/Bundle;

    invoke-static {v0, v1, v2}, Lcom/peel/d/e;->c(Landroid/support/v4/app/ae;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 297
    return-void

    .line 284
    :cond_0
    sget-object v1, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v1}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/data/at;->f()I

    move-result v1

    goto :goto_0

    .line 292
    :cond_1
    invoke-static {}, Lcom/peel/util/a/f;->a()Lcom/peel/util/a/f;

    move-result-object v0

    sget-object v1, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v1}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v1

    if-nez v1, :cond_2

    move v1, v4

    :goto_2
    const/16 v3, 0x7d0

    iget-object v4, p0, Lcom/peel/ui/kv;->a:Landroid/os/Bundle;

    const-string/jumbo v5, "category"

    .line 294
    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iget v5, p0, Lcom/peel/ui/kv;->b:I

    iget-object v6, p0, Lcom/peel/ui/kv;->c:Lcom/peel/content/listing/Listing;

    invoke-static {v6}, Lcom/peel/util/bx;->b(Lcom/peel/content/listing/Listing;)Ljava/lang/String;

    move-result-object v6

    iget-object v8, p0, Lcom/peel/ui/kv;->c:Lcom/peel/content/listing/Listing;

    invoke-virtual {v8}, Lcom/peel/content/listing/Listing;->h()Ljava/lang/String;

    move-result-object v8

    move v9, v7

    .line 292
    invoke-virtual/range {v0 .. v9}, Lcom/peel/util/a/f;->a(IIILjava/lang/String;ILjava/lang/String;ILjava/lang/String;I)V

    goto :goto_1

    :cond_2
    sget-object v1, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v1}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/data/at;->f()I

    move-result v1

    goto :goto_2
.end method
