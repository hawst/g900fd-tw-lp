.class Lcom/peel/ui/kb;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/squareup/picasso/Callback;


# instance fields
.field final synthetic a:Landroid/widget/ImageView;

.field final synthetic b:Landroid/view/View;

.field final synthetic c:Ljava/lang/String;

.field final synthetic d:Lcom/peel/ui/ka;


# direct methods
.method constructor <init>(Lcom/peel/ui/ka;Landroid/widget/ImageView;Landroid/view/View;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 143
    iput-object p1, p0, Lcom/peel/ui/kb;->d:Lcom/peel/ui/ka;

    iput-object p2, p0, Lcom/peel/ui/kb;->a:Landroid/widget/ImageView;

    iput-object p3, p0, Lcom/peel/ui/kb;->b:Landroid/view/View;

    iput-object p4, p0, Lcom/peel/ui/kb;->c:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onError()V
    .locals 2

    .prologue
    .line 151
    sget-boolean v0, Lcom/peel/util/a;->f:Z

    if-nez v0, :cond_0

    .line 152
    iget-object v0, p0, Lcom/peel/ui/kb;->b:Landroid/view/View;

    sget v1, Lcom/peel/ui/fp;->caption_altn:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 153
    iget-object v0, p0, Lcom/peel/ui/kb;->b:Landroid/view/View;

    sget v1, Lcom/peel/ui/fp;->caption_altn:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v1, p0, Lcom/peel/ui/kb;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 155
    :cond_0
    iget-object v0, p0, Lcom/peel/ui/kb;->d:Lcom/peel/ui/ka;

    invoke-static {v0}, Lcom/peel/ui/ka;->a(Lcom/peel/ui/ka;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/peel/util/b/e;->a(Landroid/content/Context;)Lcom/squareup/picasso/Picasso;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/ui/kb;->a:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Lcom/squareup/picasso/Picasso;->cancelRequest(Landroid/widget/ImageView;)V

    .line 156
    return-void
.end method

.method public onSuccess()V
    .locals 2

    .prologue
    .line 146
    iget-object v0, p0, Lcom/peel/ui/kb;->d:Lcom/peel/ui/ka;

    invoke-static {v0}, Lcom/peel/ui/ka;->a(Lcom/peel/ui/ka;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/peel/util/b/e;->a(Landroid/content/Context;)Lcom/squareup/picasso/Picasso;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/ui/kb;->a:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Lcom/squareup/picasso/Picasso;->cancelRequest(Landroid/widget/ImageView;)V

    .line 147
    return-void
.end method
