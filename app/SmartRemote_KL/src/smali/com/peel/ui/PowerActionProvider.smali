.class public Lcom/peel/ui/PowerActionProvider;
.super Landroid/support/v4/view/n;


# instance fields
.field private final context:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0, p1}, Landroid/support/v4/view/n;-><init>(Landroid/content/Context;)V

    .line 29
    iput-object p1, p0, Lcom/peel/ui/PowerActionProvider;->context:Landroid/content/Context;

    .line 30
    return-void
.end method


# virtual methods
.method public onCreateActionView()Landroid/view/View;
    .locals 1

    .prologue
    .line 75
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreateActionView(Landroid/view/MenuItem;)Landroid/view/View;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 35
    iget-object v0, p0, Lcom/peel/ui/PowerActionProvider;->context:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 36
    sget v2, Lcom/peel/ui/fq;->power_action_layout:I

    invoke-virtual {v0, v2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 38
    sget-object v2, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v2}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v2

    if-nez v2, :cond_0

    move-object v0, v1

    .line 69
    :goto_0
    return-object v0

    .line 39
    :cond_0
    sget-object v2, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v2}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v2

    invoke-virtual {v2}, Lcom/peel/control/RoomControl;->d()Lcom/peel/control/a;

    move-result-object v2

    .line 40
    if-nez v2, :cond_1

    move-object v0, v1

    goto :goto_0

    .line 41
    :cond_1
    invoke-static {v2}, Lcom/peel/util/bx;->a(Lcom/peel/control/a;)Ljava/util/ArrayList;

    move-result-object v2

    .line 42
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    .line 44
    if-nez v2, :cond_2

    .line 45
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 52
    :goto_1
    new-instance v1, Lcom/peel/ui/dt;

    invoke-direct {v1, p0, v2, v0}, Lcom/peel/ui/dt;-><init>(Lcom/peel/ui/PowerActionProvider;ILandroid/widget/ImageView;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    .line 46
    :cond_2
    const/4 v1, 0x1

    if-ne v2, v1, :cond_3

    .line 47
    sget v1, Lcom/peel/ui/fo;->action_power_stateful:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_1

    .line 49
    :cond_3
    sget v1, Lcom/peel/ui/fo;->action_bar_power_1more_btn:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_1
.end method
