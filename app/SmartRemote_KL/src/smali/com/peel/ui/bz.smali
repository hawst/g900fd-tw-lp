.class Lcom/peel/ui/bz;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Z

.field final synthetic b:Lcom/peel/content/listing/LiveListing;

.field final synthetic c:I

.field final synthetic d:Ljava/lang/String;

.field final synthetic e:[Lcom/peel/content/listing/Listing;

.field final synthetic f:Z

.field final synthetic g:Ljava/lang/String;

.field final synthetic h:Lcom/peel/ui/by;


# direct methods
.method constructor <init>(Lcom/peel/ui/by;ZLcom/peel/content/listing/LiveListing;ILjava/lang/String;[Lcom/peel/content/listing/Listing;ZLjava/lang/String;)V
    .locals 0

    .prologue
    .line 504
    iput-object p1, p0, Lcom/peel/ui/bz;->h:Lcom/peel/ui/by;

    iput-boolean p2, p0, Lcom/peel/ui/bz;->a:Z

    iput-object p3, p0, Lcom/peel/ui/bz;->b:Lcom/peel/content/listing/LiveListing;

    iput p4, p0, Lcom/peel/ui/bz;->c:I

    iput-object p5, p0, Lcom/peel/ui/bz;->d:Ljava/lang/String;

    iput-object p6, p0, Lcom/peel/ui/bz;->e:[Lcom/peel/content/listing/Listing;

    iput-boolean p7, p0, Lcom/peel/ui/bz;->f:Z

    iput-object p8, p0, Lcom/peel/ui/bz;->g:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 10

    .prologue
    const/4 v4, 0x0

    const/4 v2, 0x1

    const/4 v7, 0x0

    .line 507
    iget-object v0, p0, Lcom/peel/ui/bz;->h:Lcom/peel/ui/by;

    iget-object v0, v0, Lcom/peel/ui/by;->d:Lcom/peel/ui/bw;

    invoke-static {v0}, Lcom/peel/ui/bw;->c(Lcom/peel/ui/bw;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iget-object v1, p0, Lcom/peel/ui/bz;->h:Lcom/peel/ui/by;

    iget-object v1, v1, Lcom/peel/ui/by;->d:Lcom/peel/ui/bw;

    invoke-static {v1}, Lcom/peel/ui/bw;->c(Lcom/peel/ui/bw;)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/FrameLayout;

    invoke-virtual {v1}, Landroid/widget/FrameLayout;->getChildCount()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->removeViewAt(I)V

    .line 510
    iget-boolean v0, p0, Lcom/peel/ui/bz;->a:Z

    if-eqz v0, :cond_1

    .line 511
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "dismiss_toast"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 512
    iget-object v1, p0, Lcom/peel/ui/bz;->h:Lcom/peel/ui/by;

    iget-object v1, v1, Lcom/peel/ui/by;->d:Lcom/peel/ui/bw;

    invoke-virtual {v1}, Lcom/peel/ui/bw;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/ae;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/support/v4/a/q;->a(Landroid/content/Context;)Landroid/support/v4/a/q;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/support/v4/a/q;->a(Landroid/content/Intent;)Z

    .line 514
    invoke-static {}, Lcom/peel/util/bx;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 515
    iget-object v0, p0, Lcom/peel/ui/bz;->h:Lcom/peel/ui/by;

    iget-object v0, v0, Lcom/peel/ui/by;->d:Lcom/peel/ui/bw;

    invoke-virtual {v0}, Lcom/peel/ui/bw;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/ui/bz;->b:Lcom/peel/content/listing/LiveListing;

    invoke-virtual {v1}, Lcom/peel/content/listing/LiveListing;->d()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/peel/util/bx;->e(Landroid/content/Context;Ljava/lang/String;)V

    .line 517
    iget-object v0, p0, Lcom/peel/ui/bz;->h:Lcom/peel/ui/by;

    iget-object v0, v0, Lcom/peel/ui/by;->d:Lcom/peel/ui/bw;

    invoke-virtual {v0}, Lcom/peel/ui/bw;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/ui/bz;->h:Lcom/peel/ui/by;

    iget-object v1, v1, Lcom/peel/ui/by;->d:Lcom/peel/ui/bw;

    .line 518
    invoke-static {v1}, Lcom/peel/ui/bw;->e(Lcom/peel/ui/bw;)Landroid/view/View;

    move-result-object v1

    iget-object v2, p0, Lcom/peel/ui/bz;->b:Lcom/peel/content/listing/LiveListing;

    iget v3, p0, Lcom/peel/ui/bz;->c:I

    iget-object v4, p0, Lcom/peel/ui/bz;->d:Ljava/lang/String;

    iget-object v5, p0, Lcom/peel/ui/bz;->h:Lcom/peel/ui/by;

    iget-object v5, v5, Lcom/peel/ui/by;->d:Lcom/peel/ui/bw;

    iget v5, v5, Lcom/peel/ui/bw;->f:I

    .line 517
    invoke-static/range {v0 .. v5}, Lcom/peel/ui/a/v;->a(Landroid/content/Context;Landroid/view/View;Lcom/peel/content/listing/Listing;ILjava/lang/String;I)V

    .line 521
    iget-object v0, p0, Lcom/peel/ui/bz;->e:[Lcom/peel/content/listing/Listing;

    aget-object v0, v0, v7

    check-cast v0, Lcom/peel/content/listing/LiveListing;

    invoke-static {v0}, Lcom/peel/util/bx;->a(Lcom/peel/content/listing/LiveListing;)V

    .line 571
    :goto_0
    return-void

    .line 524
    :cond_0
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 525
    const-string/jumbo v1, "control_only_mode"

    invoke-virtual {v0, v1, v7}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 526
    const-string/jumbo v1, "passback_clazz"

    iget-object v2, p0, Lcom/peel/ui/bz;->h:Lcom/peel/ui/by;

    iget-object v2, v2, Lcom/peel/ui/by;->d:Lcom/peel/ui/bw;

    invoke-virtual {v2}, Lcom/peel/ui/bw;->m()Landroid/support/v4/app/ae;

    move-result-object v2

    invoke-static {v2}, Lcom/peel/d/e;->a(Landroid/support/v4/app/ae;)Lcom/peel/d/u;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 527
    const-string/jumbo v1, "passback_bundle"

    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 528
    iget-object v1, p0, Lcom/peel/ui/bz;->h:Lcom/peel/ui/by;

    iget-object v1, v1, Lcom/peel/ui/by;->d:Lcom/peel/ui/bw;

    invoke-virtual {v1}, Lcom/peel/ui/bw;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    const-class v2, Lcom/peel/i/dr;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lcom/peel/d/e;->c(Landroid/support/v4/app/ae;Ljava/lang/String;Landroid/os/Bundle;)V

    goto :goto_0

    .line 533
    :cond_1
    iget-boolean v0, p0, Lcom/peel/ui/bz;->f:Z

    if-eqz v0, :cond_3

    .line 534
    sget-object v0, Lcom/peel/content/a;->e:Ljava/lang/String;

    const-string/jumbo v1, "CN"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 535
    iget-object v0, p0, Lcom/peel/ui/bz;->h:Lcom/peel/ui/by;

    iget-object v0, v0, Lcom/peel/ui/by;->d:Lcom/peel/ui/bw;

    .line 536
    invoke-virtual {v0}, Lcom/peel/ui/bw;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/ui/bz;->e:[Lcom/peel/content/listing/Listing;

    aget-object v1, v1, v7

    iget-object v3, p0, Lcom/peel/ui/bz;->g:Ljava/lang/String;

    invoke-static {v0, v1, v3}, Lcom/peel/util/bx;->a(Landroid/content/Context;Lcom/peel/content/listing/Listing;Ljava/lang/String;)V

    .line 538
    iget-object v0, p0, Lcom/peel/ui/bz;->h:Lcom/peel/ui/by;

    iget-object v0, v0, Lcom/peel/ui/by;->d:Lcom/peel/ui/bw;

    iget-object v0, v0, Lcom/peel/ui/bw;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "selective"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 539
    iget-object v0, p0, Lcom/peel/ui/bz;->h:Lcom/peel/ui/by;

    iget-object v0, v0, Lcom/peel/ui/by;->d:Lcom/peel/ui/bw;

    iget-object v1, p0, Lcom/peel/ui/bz;->h:Lcom/peel/ui/by;

    iget-object v1, v1, Lcom/peel/ui/by;->d:Lcom/peel/ui/bw;

    iget-object v1, v1, Lcom/peel/ui/bw;->c:Landroid/os/Bundle;

    invoke-virtual {v0, v1}, Lcom/peel/ui/bw;->c(Landroid/os/Bundle;)V

    goto :goto_0

    .line 541
    :cond_2
    iget-object v0, p0, Lcom/peel/ui/bz;->e:[Lcom/peel/content/listing/Listing;

    aget-object v0, v0, v7

    iget-object v1, p0, Lcom/peel/ui/bz;->h:Lcom/peel/ui/by;

    iget-object v1, v1, Lcom/peel/ui/by;->d:Lcom/peel/ui/bw;

    .line 542
    invoke-virtual {v1}, Lcom/peel/ui/bw;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    new-instance v2, Lcom/peel/ui/ca;

    invoke-direct {v2, p0}, Lcom/peel/ui/ca;-><init>(Lcom/peel/ui/bz;)V

    .line 541
    invoke-static {v0, v1, v2}, Lcom/peel/util/bx;->a(Lcom/peel/content/listing/Listing;Landroid/content/Context;Lcom/peel/util/t;)V

    goto/16 :goto_0

    .line 563
    :cond_3
    iget-object v0, p0, Lcom/peel/ui/bz;->e:[Lcom/peel/content/listing/Listing;

    aget-object v0, v0, v7

    iget-object v1, p0, Lcom/peel/ui/bz;->h:Lcom/peel/ui/by;

    iget-object v1, v1, Lcom/peel/ui/by;->d:Lcom/peel/ui/bw;

    .line 564
    invoke-virtual {v1}, Lcom/peel/ui/bw;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    iget-object v3, p0, Lcom/peel/ui/bz;->h:Lcom/peel/ui/by;

    iget-object v3, v3, Lcom/peel/ui/by;->d:Lcom/peel/ui/bw;

    iget v3, v3, Lcom/peel/ui/bw;->f:I

    .line 563
    invoke-static {v0, v1, v3, v4, v4}, Lcom/peel/util/bx;->a(Lcom/peel/content/listing/Listing;Landroid/support/v4/app/ae;ILjava/lang/String;Ljava/lang/String;)V

    .line 566
    invoke-static {}, Lcom/peel/util/a/f;->a()Lcom/peel/util/a/f;

    move-result-object v0

    sget-object v1, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v1}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v1

    if-nez v1, :cond_4

    move v1, v2

    :goto_1
    const/16 v2, 0x4cf

    iget-object v3, p0, Lcom/peel/ui/bz;->h:Lcom/peel/ui/by;

    iget-object v3, v3, Lcom/peel/ui/by;->d:Lcom/peel/ui/bw;

    iget v3, v3, Lcom/peel/ui/bw;->f:I

    iget-object v4, p0, Lcom/peel/ui/bz;->d:Ljava/lang/String;

    iget v5, p0, Lcom/peel/ui/bz;->c:I

    iget-object v6, p0, Lcom/peel/ui/bz;->b:Lcom/peel/content/listing/LiveListing;

    .line 568
    invoke-static {v6}, Lcom/peel/util/bx;->b(Lcom/peel/content/listing/Listing;)Ljava/lang/String;

    move-result-object v6

    iget-object v8, p0, Lcom/peel/ui/bz;->b:Lcom/peel/content/listing/LiveListing;

    invoke-virtual {v8}, Lcom/peel/content/listing/LiveListing;->h()Ljava/lang/String;

    move-result-object v8

    move v9, v7

    .line 566
    invoke-virtual/range {v0 .. v9}, Lcom/peel/util/a/f;->a(IIILjava/lang/String;ILjava/lang/String;ILjava/lang/String;I)V

    goto/16 :goto_0

    :cond_4
    sget-object v1, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v1}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/data/at;->f()I

    move-result v1

    goto :goto_1
.end method
