.class public final Lcom/peel/ui/fq;
.super Ljava/lang/Object;


# static fields
.field public static final abc_action_bar_title_item:I = 0x7f030000

.field public static final abc_action_bar_up_container:I = 0x7f030001

.field public static final abc_action_bar_view_list_nav_layout:I = 0x7f030002

.field public static final abc_action_menu_item_layout:I = 0x7f030003

.field public static final abc_action_menu_layout:I = 0x7f030004

.field public static final abc_action_mode_bar:I = 0x7f030005

.field public static final abc_action_mode_close_item_material:I = 0x7f030006

.field public static final abc_activity_chooser_view:I = 0x7f030007

.field public static final abc_activity_chooser_view_include:I = 0x7f030008

.field public static final abc_activity_chooser_view_list_item:I = 0x7f030009

.field public static final abc_expanded_menu_layout:I = 0x7f03000a

.field public static final abc_list_menu_item_checkbox:I = 0x7f03000b

.field public static final abc_list_menu_item_icon:I = 0x7f03000c

.field public static final abc_list_menu_item_layout:I = 0x7f03000d

.field public static final abc_list_menu_item_radio:I = 0x7f03000e

.field public static final abc_popup_menu_item_layout:I = 0x7f03000f

.field public static final abc_screen_content_include:I = 0x7f030010

.field public static final abc_screen_simple:I = 0x7f030011

.field public static final abc_screen_simple_overlay_action_mode:I = 0x7f030012

.field public static final abc_screen_toolbar:I = 0x7f030013

.field public static final abc_search_dropdown_item_icons_2line:I = 0x7f030014

.field public static final abc_search_view:I = 0x7f030015

.field public static final abc_simple_dropdown_hint:I = 0x7f030016

.field public static final about:I = 0x7f030017

.field public static final actionbar_done_layout:I = 0x7f030018

.field public static final actionbar_next_layout:I = 0x7f030019

.field public static final advideoview:I = 0x7f03001a

.field public static final always_widget_base:I = 0x7f03001b

.field public static final appirater:I = 0x7f03001c

.field public static final auto_list_item:I = 0x7f03001d

.field public static final autolocklist:I = 0x7f03001e

.field public static final before_setup_step1:I = 0x7f03001f

.field public static final before_setup_step2:I = 0x7f030020

.field public static final brand_row:I = 0x7f030021

.field public static final card_view_listview:I = 0x7f030022

.field public static final channel_guide_sub_fragment:I = 0x7f030023

.field public static final channel_search_row:I = 0x7f030024

.field public static final channel_setting_row:I = 0x7f030025

.field public static final com_facebook_friendpickerfragment:I = 0x7f030026

.field public static final com_facebook_login_activity_layout:I = 0x7f030027

.field public static final com_facebook_picker_activity_circle_row:I = 0x7f030028

.field public static final com_facebook_picker_checkbox:I = 0x7f030029

.field public static final com_facebook_picker_image:I = 0x7f03002a

.field public static final com_facebook_picker_list_row:I = 0x7f03002b

.field public static final com_facebook_picker_list_section_header:I = 0x7f03002c

.field public static final com_facebook_picker_search_box:I = 0x7f03002d

.field public static final com_facebook_picker_title_bar:I = 0x7f03002e

.field public static final com_facebook_picker_title_bar_stub:I = 0x7f03002f

.field public static final com_facebook_placepickerfragment:I = 0x7f030030

.field public static final com_facebook_placepickerfragment_list_row:I = 0x7f030031

.field public static final com_facebook_search_bar_layout:I = 0x7f030032

.field public static final com_facebook_tooltip_bubble:I = 0x7f030033

.field public static final com_facebook_usersettingsfragment:I = 0x7f030034

.field public static final common_listview_layout:I = 0x7f030035

.field public static final config_activity_view:I = 0x7f030036

.field public static final content_list_two_rows:I = 0x7f030037

.field public static final control_change_room:I = 0x7f030038

.field public static final control_custom_contents:I = 0x7f030039

.field public static final control_list_contents:I = 0x7f03003a

.field public static final control_spinner_item:I = 0x7f03003b

.field public static final controlpad_activity_layout_container:I = 0x7f03003c

.field public static final controlpad_channel:I = 0x7f03003d

.field public static final controlpad_device_layout_container:I = 0x7f03003e

.field public static final controlpad_new:I = 0x7f03003f

.field public static final controlpad_numberpad:I = 0x7f030040

.field public static final controlpad_numberpad_12keys_modes3:I = 0x7f030041

.field public static final controlpad_numberpad_12keys_modes4:I = 0x7f030042

.field public static final controlpad_numberpad_controlonly:I = 0x7f030043

.field public static final controlpad_spinner:I = 0x7f030044

.field public static final controlpad_spinner_item:I = 0x7f030045

.field public static final controlpad_spinner_layout:I = 0x7f030046

.field public static final controlpad_volume_layout:I = 0x7f030047

.field public static final country_row:I = 0x7f030048

.field public static final custom_action_layout:I = 0x7f030049

.field public static final custom_layout:I = 0x7f03004a

.field public static final custom_toast:I = 0x7f03004b

.field public static final delay_settings_view:I = 0x7f03004c

.field public static final delete_device_button:I = 0x7f03004d

.field public static final device_add_row:I = 0x7f03004e

.field public static final device_import_options:I = 0x7f03004f

.field public static final device_ir_row:I = 0x7f030050

.field public static final device_item_view:I = 0x7f030051

.field public static final device_list_view:I = 0x7f030052

.field public static final device_power_row:I = 0x7f030053

.field public static final device_remote_activity:I = 0x7f030054

.field public static final device_row:I = 0x7f030055

.field public static final device_setup:I = 0x7f030056

.field public static final device_setup_test:I = 0x7f030057

.field public static final device_tab_double:I = 0x7f030058

.field public static final device_tab_single:I = 0x7f030059

.field public static final device_tab_triple:I = 0x7f03005a

.field public static final device_type_row:I = 0x7f03005b

.field public static final device_type_selection_fragment:I = 0x7f03005c

.field public static final dfp_advertisement:I = 0x7f03005d

.field public static final dfp_full_page:I = 0x7f03005e

.field public static final disambiguation:I = 0x7f03005f

.field public static final dtv_detail_view:I = 0x7f030060

.field public static final dtv_device_list_view:I = 0x7f030061

.field public static final dtv_device_row:I = 0x7f030062

.field public static final dtv_loading_progress_view:I = 0x7f030063

.field public static final dtv_login_form:I = 0x7f030064

.field public static final dynamic_list_view_row:I = 0x7f030065

.field public static final edit_channel_option_row:I = 0x7f030066

.field public static final empty_view:I = 0x7f030067

.field public static final epg:I = 0x7f030068

.field public static final epg_channel_item:I = 0x7f030069

.field public static final epg_list_item:I = 0x7f03006a

.field public static final epg_page:I = 0x7f03006b

.field public static final epg_sub_channel_item:I = 0x7f03006c

.field public static final epg_sub_list_item:I = 0x7f03006d

.field public static final epg_sub_page:I = 0x7f03006e

.field public static final episode_list_item:I = 0x7f03006f

.field public static final fav_ch_scroller:I = 0x7f030070

.field public static final fav_channel_list:I = 0x7f030071

.field public static final fav_channel_row:I = 0x7f030072

.field public static final fav_channel_search:I = 0x7f030073

.field public static final fav_cut_progam_list:I = 0x7f030074

.field public static final fav_cut_row:I = 0x7f030075

.field public static final favorites_pager:I = 0x7f030076

.field public static final favorites_shows_layout:I = 0x7f030077

.field public static final feedback:I = 0x7f030078

.field public static final gender_age_option_layout:I = 0x7f030079

.field public static final generic_ad:I = 0x7f03007a

.field public static final homescreen_placeholder1:I = 0x7f03007b

.field public static final homescreen_placeholder2:I = 0x7f03007c

.field public static final homescreen_placeholder3:I = 0x7f03007d

.field public static final inc_horizontal_scrollview:I = 0x7f03007e

.field public static final inc_missing_ir_report:I = 0x7f03007f

.field public static final init_roku_screen:I = 0x7f030080

.field public static final input_list_layout:I = 0x7f030081

.field public static final input_row:I = 0x7f030082

.field public static final ir_learning:I = 0x7f030083

.field public static final ir_learning_cmd_list:I = 0x7f030084

.field public static final ir_learning_row:I = 0x7f030085

.field public static final issue_type_spinner_item:I = 0x7f030086

.field public static final l17_list_item:I = 0x7f030087

.field public static final load_error:I = 0x7f030088

.field public static final loading_splash:I = 0x7f030089

.field public static final lockscreen_device_select:I = 0x7f03008a

.field public static final lockscreen_item_placeholder1:I = 0x7f03008b

.field public static final lockscreen_item_placeholder2:I = 0x7f03008c

.field public static final lockscreen_item_placeholder3:I = 0x7f03008d

.field public static final lockscreen_item_placeholder4:I = 0x7f03008e

.field public static final lockscreen_placeholder1:I = 0x7f03008f

.field public static final lockscreen_placeholder2:I = 0x7f030090

.field public static final lockscreen_placeholder3:I = 0x7f030091

.field public static final login_view:I = 0x7f030092

.field public static final main:I = 0x7f030093

.field public static final main_webview:I = 0x7f030094

.field public static final menu_row:I = 0x7f030095

.field public static final more_bottom_layout:I = 0x7f030096

.field public static final more_like_this_item:I = 0x7f030097

.field public static final more_like_this_layout:I = 0x7f030098

.field public static final netflix_setup:I = 0x7f030099

.field public static final network_dialog:I = 0x7f03009a

.field public static final new_code_view:I = 0x7f03009b

.field public static final no_content:I = 0x7f03009c

.field public static final noti_item_placeholder1:I = 0x7f03009d

.field public static final noti_item_placeholder2:I = 0x7f03009e

.field public static final noti_item_placeholder3:I = 0x7f03009f

.field public static final noti_item_placeholder4:I = 0x7f0300a0

.field public static final notification_collapsedview:I = 0x7f0300a1

.field public static final notification_collapsedview_setup:I = 0x7f0300a2

.field public static final notification_placeholder1:I = 0x7f0300a3

.field public static final notification_placeholder1_sdk21:I = 0x7f0300a4

.field public static final notification_placeholder2:I = 0x7f0300a5

.field public static final notification_placeholder2_sdk21:I = 0x7f0300a6

.field public static final notification_placeholder3:I = 0x7f0300a7

.field public static final notification_placeholder3_sdk21:I = 0x7f0300a8

.field public static final notification_playing:I = 0x7f0300a9

.field public static final notification_playing_collapsed:I = 0x7f0300aa

.field public static final notification_playing_new:I = 0x7f0300ab

.field public static final notifications_control:I = 0x7f0300ac

.field public static final notifications_control_header:I = 0x7f0300ad

.field public static final notifications_control_header_new:I = 0x7f0300ae

.field public static final notifications_control_new:I = 0x7f0300af

.field public static final notifications_device_selector:I = 0x7f0300b0

.field public static final notifications_setup:I = 0x7f0300b1

.field public static final notifications_setup_header:I = 0x7f0300b2

.field public static final notifications_setup_new:I = 0x7f0300b3

.field public static final onboarding:I = 0x7f0300b4

.field public static final ondemandvideo_layout:I = 0x7f0300b5

.field public static final overflow_menu:I = 0x7f0300b6

.field public static final overview_btn_layout:I = 0x7f0300b7

.field public static final panel_powered_peel_noti:I = 0x7f0300b8

.field public static final personalization:I = 0x7f0300b9

.field public static final popup_dialog:I = 0x7f0300ba

.field public static final popup_tunein_check:I = 0x7f0300bb

.field public static final power_action_layout:I = 0x7f0300bc

.field public static final powered_by:I = 0x7f0300bd

.field public static final preset_key_view:I = 0x7f0300be

.field public static final privacy_xml:I = 0x7f0300bf

.field public static final program_search_row:I = 0x7f0300c0

.field public static final provider_row:I = 0x7f0300c1

.field public static final region_spinner_list_item:I = 0x7f0300c2

.field public static final reminder_syndicate_layout:I = 0x7f0300c3

.field public static final remote_pin_controlpad_new:I = 0x7f0300c4

.field public static final rename_room:I = 0x7f0300c5

.field public static final rename_room_new:I = 0x7f0300c6

.field public static final report_missing_brand:I = 0x7f0300c7

.field public static final report_missing_service:I = 0x7f0300c8

.field public static final report_missing_service_provider_footer:I = 0x7f0300c9

.field public static final res_lang_row:I = 0x7f0300ca

.field public static final roku_list_item:I = 0x7f0300cb

.field public static final room_add_row:I = 0x7f0300cc

.field public static final room_overview:I = 0x7f0300cd

.field public static final room_overview_layout:I = 0x7f0300ce

.field public static final roomoverview_device_row_layout:I = 0x7f0300cf

.field public static final roomoverview_room_overview_layout:I = 0x7f0300d0

.field public static final roomoverview_settings_edit_room_row:I = 0x7f0300d1

.field public static final roomoverview_settings_header_row:I = 0x7f0300d2

.field public static final roomoverview_settings_provider_row:I = 0x7f0300d3

.field public static final schedules_for_channel_view:I = 0x7f0300d4

.field public static final search:I = 0x7f0300d5

.field public static final search_row:I = 0x7f0300d6

.field public static final searched_item:I = 0x7f0300d7

.field public static final setreminder_overlay:I = 0x7f0300d8

.field public static final settings_add_foreign_language_channel:I = 0x7f0300d9

.field public static final settings_add_room_view:I = 0x7f0300da

.field public static final settings_adddevice_activities:I = 0x7f0300db

.field public static final settings_adddevice_activities_power:I = 0x7f0300dc

.field public static final settings_adddevice_activity_item:I = 0x7f0300dd

.field public static final settings_age_gender:I = 0x7f0300de

.field public static final settings_button:I = 0x7f0300df

.field public static final settings_channels:I = 0x7f0300e0

.field public static final settings_channels_edit_channels:I = 0x7f0300e1

.field public static final settings_channels_text:I = 0x7f0300e2

.field public static final settings_device_row:I = 0x7f0300e3

.field public static final settings_genres:I = 0x7f0300e4

.field public static final settings_header_row:I = 0x7f0300e5

.field public static final settings_header_row_main:I = 0x7f0300e6

.field public static final settings_login:I = 0x7f0300e7

.field public static final settings_main_view:I = 0x7f0300e8

.field public static final settings_preset_key_row:I = 0x7f0300e9

.field public static final settings_preset_keys:I = 0x7f0300ea

.field public static final settings_row:I = 0x7f0300eb

.field public static final settings_single_selection_list_row:I = 0x7f0300ec

.field public static final settings_single_selection_row_wospace:I = 0x7f0300ed

.field public static final setup_country:I = 0x7f0300ee

.field public static final setup_dialog:I = 0x7f0300ef

.field public static final setup_main_selection:I = 0x7f0300f0

.field public static final setup_name:I = 0x7f0300f1

.field public static final setup_region:I = 0x7f0300f2

.field public static final setup_splash:I = 0x7f0300f3

.field public static final setup_zip:I = 0x7f0300f4

.field public static final show_detail_tweet_layout:I = 0x7f0300f5

.field public static final show_details_ondemand:I = 0x7f0300f6

.field public static final show_details_ondemand_listitem:I = 0x7f0300f7

.field public static final show_details_ondemand_spinneritem:I = 0x7f0300f8

.field public static final show_details_overview_actions:I = 0x7f0300f9

.field public static final show_details_overview_synopsis:I = 0x7f0300fa

.field public static final show_details_overview_upcoming:I = 0x7f0300fb

.field public static final show_details_twitter_login:I = 0x7f0300fc

.field public static final show_details_twitter_overview:I = 0x7f0300fd

.field public static final show_details_upcoming_listitem:I = 0x7f0300fe

.field public static final show_details_watchon_item:I = 0x7f0300ff

.field public static final show_details_watchons:I = 0x7f030100

.field public static final show_synopsis_layout:I = 0x7f030101

.field public static final simplified_device_setup:I = 0x7f030102

.field public static final simplified_device_setup_test_stb:I = 0x7f030103

.field public static final simplified_device_setup_test_tv:I = 0x7f030104

.field public static final single_linear_layout:I = 0x7f030105

.field public static final slideout_menu_layout:I = 0x7f030106

.field public static final spinner_item:I = 0x7f030107

.field public static final ss_datetime_picker:I = 0x7f030108

.field public static final ss_datetime_picker_menu:I = 0x7f030109

.field public static final ss_tiles_with_pager:I = 0x7f03010a

.field public static final ss_wataction:I = 0x7f03010b

.field public static final sticky_card_header_view:I = 0x7f03010c

.field public static final support_simple_spinner_dropdown_item:I = 0x7f03010d

.field public static final switch_tv_source_view:I = 0x7f03010e

.field public static final test_other_btn_pagers_view:I = 0x7f03010f

.field public static final test_pw_btn_pagers_view:I = 0x7f030110

.field public static final test_tune_btn_pagers_view:I = 0x7f030111

.field public static final three_button_layout:I = 0x7f030112

.field public static final three_button_row:I = 0x7f030113

.field public static final tile_bar:I = 0x7f030114

.field public static final tile_bar_personalize:I = 0x7f030115

.field public static final tile_view_new:I = 0x7f030116

.field public static final tile_view_personal:I = 0x7f030117

.field public static final time_header_row:I = 0x7f030118

.field public static final time_list:I = 0x7f030119

.field public static final time_slot_item:I = 0x7f03011a

.field public static final tops_layout:I = 0x7f03011b

.field public static final trigger_action_edit_channels:I = 0x7f03011c

.field public static final tunein_overlay:I = 0x7f03011d

.field public static final tutorial:I = 0x7f03011e

.field public static final tweet_list_item:I = 0x7f03011f

.field public static final tweet_refresh_layout:I = 0x7f030120

.field public static final tweet_stream_header:I = 0x7f030121

.field public static final tweet_stream_header_with_text:I = 0x7f030122

.field public static final twitter_compose_layout:I = 0x7f030123

.field public static final twitter_trending_layout:I = 0x7f030124

.field public static final two_vertical_textviews:I = 0x7f030125

.field public static final webview_simple_layout:I = 0x7f030126

.field public static final weektile_bar:I = 0x7f030127

.field public static final widget_device_select:I = 0x7f030128

.field public static final widget_fav_channels:I = 0x7f030129

.field public static final widget_layout_home_setup:I = 0x7f03012a

.field public static final widget_layout_setup:I = 0x7f03012b

.field public static final widget_remote_handle:I = 0x7f03012c
