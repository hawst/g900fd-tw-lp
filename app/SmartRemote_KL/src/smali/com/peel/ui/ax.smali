.class public Lcom/peel/ui/ax;
.super Landroid/widget/ArrayAdapter;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field private a:Landroid/view/LayoutInflater;

.field private b:[Ljava/lang/String;

.field private c:Landroid/content/Context;

.field private d:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;I[Ljava/lang/String;Ljava/util/ArrayList;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I[",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 27
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    .line 28
    iput-object p3, p0, Lcom/peel/ui/ax;->b:[Ljava/lang/String;

    .line 29
    iput-object p4, p0, Lcom/peel/ui/ax;->d:Ljava/util/ArrayList;

    .line 30
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/ui/ax;->a:Landroid/view/LayoutInflater;

    .line 31
    iput-object p1, p0, Lcom/peel/ui/ax;->c:Landroid/content/Context;

    .line 32
    return-void
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 36
    if-eqz p2, :cond_0

    move-object v1, p2

    :goto_0
    move-object v0, v1

    .line 37
    check-cast v0, Landroid/widget/TextView;

    iget-object v2, p0, Lcom/peel/ui/ax;->b:[Ljava/lang/String;

    aget-object v2, v2, p1

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 43
    sget v2, Lcom/peel/ui/fo;->popup_icon_tivo_normal:I

    .line 45
    iget-object v0, p0, Lcom/peel/ui/ax;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    move v0, v2

    .line 81
    :goto_1
    iget-object v2, p0, Lcom/peel/ui/ax;->c:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 82
    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v3

    invoke-virtual {v2, v5, v5, v0, v3}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    move-object v0, v1

    .line 84
    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v2, v4, v4, v4}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 85
    return-object v1

    .line 36
    :cond_0
    iget-object v0, p0, Lcom/peel/ui/ax;->a:Landroid/view/LayoutInflater;

    sget v1, Lcom/peel/ui/fq;->device_row:I

    invoke-virtual {v0, v1, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    goto :goto_0

    .line 49
    :pswitch_1
    sget v0, Lcom/peel/ui/fo;->popup_icon_settop_normal:I

    goto :goto_1

    .line 54
    :pswitch_2
    sget v0, Lcom/peel/ui/fo;->popup_icon_dvd_normal:I

    goto :goto_1

    .line 59
    :pswitch_3
    sget v0, Lcom/peel/ui/fo;->popup_icon_bluray_normal:I

    goto :goto_1

    .line 63
    :pswitch_4
    sget v0, Lcom/peel/ui/fo;->popup_icon_ac_normal:I

    goto :goto_1

    .line 67
    :pswitch_5
    sget v0, Lcom/peel/ui/fo;->popup_icon_av_receiver_normal:I

    goto :goto_1

    .line 71
    :pswitch_6
    sget v0, Lcom/peel/ui/fo;->popup_icon_appletv_roku_normal:I

    goto :goto_1

    .line 75
    :pswitch_7
    sget v0, Lcom/peel/ui/fo;->popup_icon_projector_normal:I

    goto :goto_1

    .line 45
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_5
        :pswitch_6
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_7
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_4
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
