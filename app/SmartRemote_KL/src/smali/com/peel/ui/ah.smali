.class public Lcom/peel/ui/ah;
.super Landroid/widget/ArrayAdapter;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lcom/peel/control/a;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private b:Landroid/view/LayoutInflater;

.field private c:Landroid/content/Context;

.field private d:Lcom/peel/control/a;

.field private e:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 29
    const-class v0, Lcom/peel/ui/ah;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/peel/ui/ah;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;ILjava/util/List;Lcom/peel/control/a;Z)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Ljava/util/List",
            "<",
            "Lcom/peel/control/a;",
            ">;",
            "Lcom/peel/control/a;",
            "Z)V"
        }
    .end annotation

    .prologue
    .line 37
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 38
    iput-object p1, p0, Lcom/peel/ui/ah;->c:Landroid/content/Context;

    .line 39
    iput-object p4, p0, Lcom/peel/ui/ah;->d:Lcom/peel/control/a;

    .line 40
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/ui/ah;->b:Landroid/view/LayoutInflater;

    .line 41
    iput-boolean p5, p0, Lcom/peel/ui/ah;->e:Z

    .line 42
    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 2

    .prologue
    .line 44
    invoke-super {p0}, Landroid/widget/ArrayAdapter;->getCount()I

    move-result v1

    iget-boolean v0, p0, Lcom/peel/ui/ah;->e:Z

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    add-int/2addr v0, v1

    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public getItemViewType(I)I
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 46
    invoke-super {p0}, Landroid/widget/ArrayAdapter;->getCount()I

    move-result v1

    if-ne p1, v1, :cond_0

    iget-boolean v1, p0, Lcom/peel/ui/ah;->e:Z

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 11

    .prologue
    const/4 v2, 0x0

    const/4 v10, 0x0

    const/4 v9, 0x1

    .line 51
    invoke-virtual {p0, p1}, Lcom/peel/ui/ah;->getItemViewType(I)I

    move-result v0

    if-nez v0, :cond_b

    .line 52
    if-eqz p2, :cond_2

    .line 53
    :goto_0
    const v0, 0x1020014

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckedTextView;

    .line 54
    invoke-virtual {p0, p1}, Lcom/peel/ui/ah;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/peel/control/a;

    .line 57
    iget-object v2, p0, Lcom/peel/ui/ah;->d:Lcom/peel/control/a;

    if-eqz v2, :cond_3

    invoke-virtual {v1}, Lcom/peel/control/a;->b()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/peel/ui/ah;->d:Lcom/peel/control/a;

    invoke-virtual {v3}, Lcom/peel/control/a;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 58
    invoke-virtual {v0, v9}, Landroid/widget/CheckedTextView;->setChecked(Z)V

    .line 63
    :goto_1
    invoke-virtual {v1}, Lcom/peel/control/a;->d()[Ljava/lang/String;

    move-result-object v3

    .line 66
    invoke-virtual {v1, v9}, Lcom/peel/control/a;->a(I)Lcom/peel/control/h;

    move-result-object v4

    .line 67
    sget v2, Lcom/peel/ui/fp;->icon:I

    invoke-virtual {p2, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    .line 68
    if-eqz v4, :cond_5

    .line 69
    invoke-virtual {v4}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v5

    invoke-virtual {v5}, Lcom/peel/data/g;->f()Ljava/lang/String;

    move-result-object v5

    const-string/jumbo v6, "TiVo"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 70
    sget v5, Lcom/peel/ui/fo;->popup_icon_tivo_normal:I

    invoke-virtual {v2, v5}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 111
    :goto_2
    invoke-virtual {v1}, Lcom/peel/control/a;->a()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v5, "Player"

    const-string/jumbo v6, ""

    invoke-virtual {v2, v5, v6}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 112
    if-eqz v3, :cond_9

    array-length v5, v3

    if-lez v5, :cond_9

    .line 113
    invoke-static {v3}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v3

    .line 114
    const-string/jumbo v5, "nflx"

    invoke-interface {v3, v5}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 115
    iget-object v3, p0, Lcom/peel/ui/ah;->c:Landroid/content/Context;

    sget v5, Lcom/peel/ui/ft;->watch_fmt:I

    new-array v6, v9, [Ljava/lang/Object;

    iget-object v7, p0, Lcom/peel/ui/ah;->c:Landroid/content/Context;

    sget v8, Lcom/peel/ui/ft;->netflix_label:I

    invoke-virtual {v7, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v10

    invoke-virtual {v3, v5, v6}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/CheckedTextView;->setText(Ljava/lang/CharSequence;)V

    .line 128
    :goto_3
    invoke-virtual {v4}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v3

    invoke-virtual {v3}, Lcom/peel/data/g;->d()I

    move-result v3

    const/4 v5, 0x6

    if-ne v3, v5, :cond_a

    .line 129
    iget-object v2, p0, Lcom/peel/ui/ah;->c:Landroid/content/Context;

    sget v3, Lcom/peel/ui/ft;->watch_fmt:I

    new-array v5, v9, [Ljava/lang/Object;

    invoke-virtual {v4}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v4

    invoke-virtual {v4}, Lcom/peel/data/g;->f()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v5, v10

    invoke-virtual {v2, v3, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/CheckedTextView;->setText(Ljava/lang/CharSequence;)V

    .line 134
    :cond_0
    :goto_4
    invoke-virtual {p2, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 138
    :cond_1
    :goto_5
    return-object p2

    .line 52
    :cond_2
    iget-object v0, p0, Lcom/peel/ui/ah;->b:Landroid/view/LayoutInflater;

    sget v1, Lcom/peel/ui/fq;->device_remote_activity:I

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    goto/16 :goto_0

    .line 60
    :cond_3
    invoke-virtual {v0, v10}, Landroid/widget/CheckedTextView;->setChecked(Z)V

    goto/16 :goto_1

    .line 72
    :cond_4
    invoke-virtual {v4}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v5

    invoke-virtual {v5}, Lcom/peel/data/g;->d()I

    move-result v5

    packed-switch v5, :pswitch_data_0

    .line 99
    :pswitch_0
    sget v5, Lcom/peel/ui/fo;->popup_icon_tivo_normal:I

    invoke-virtual {v2, v5}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_2

    .line 76
    :pswitch_1
    sget v5, Lcom/peel/ui/fo;->popup_icon_settop_normal:I

    invoke-virtual {v2, v5}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_2

    .line 80
    :pswitch_2
    sget v5, Lcom/peel/ui/fo;->popup_icon_dvd_normal:I

    invoke-virtual {v2, v5}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_2

    .line 84
    :pswitch_3
    sget v5, Lcom/peel/ui/fo;->popup_icon_bluray_normal:I

    invoke-virtual {v2, v5}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_2

    .line 87
    :pswitch_4
    sget v5, Lcom/peel/ui/fo;->popup_icon_ac_normal:I

    invoke-virtual {v2, v5}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_2

    .line 90
    :pswitch_5
    sget v5, Lcom/peel/ui/fo;->popup_icon_av_receiver_normal:I

    invoke-virtual {v2, v5}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_2

    .line 93
    :pswitch_6
    sget v5, Lcom/peel/ui/fo;->popup_icon_appletv_roku_normal:I

    invoke-virtual {v2, v5}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_2

    .line 96
    :pswitch_7
    sget v5, Lcom/peel/ui/fo;->popup_icon_projector_normal:I

    invoke-virtual {v2, v5}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_2

    .line 103
    :cond_5
    sget-object v2, Lcom/peel/ui/ah;->a:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "bad... no control device for activity: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v1}, Lcom/peel/control/a;->a()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2

    .line 116
    :cond_6
    const-string/jumbo v5, "live"

    invoke-interface {v3, v5}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_7

    const-string/jumbo v5, "dtv"

    invoke-interface {v3, v5}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_8

    .line 119
    :cond_7
    iget-object v3, p0, Lcom/peel/ui/ah;->c:Landroid/content/Context;

    sget v5, Lcom/peel/ui/ft;->watch_fmt:I

    new-array v6, v9, [Ljava/lang/Object;

    invoke-virtual {v1}, Lcom/peel/control/a;->a()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v10

    invoke-virtual {v3, v5, v6}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/CheckedTextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_3

    .line 121
    :cond_8
    iget-object v3, p0, Lcom/peel/ui/ah;->c:Landroid/content/Context;

    sget v5, Lcom/peel/ui/ft;->watch_fmt:I

    new-array v6, v9, [Ljava/lang/Object;

    aput-object v2, v6, v10

    invoke-virtual {v3, v5, v6}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/CheckedTextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_3

    .line 124
    :cond_9
    iget-object v3, p0, Lcom/peel/ui/ah;->c:Landroid/content/Context;

    sget v5, Lcom/peel/ui/ft;->watch_fmt:I

    new-array v6, v9, [Ljava/lang/Object;

    aput-object v2, v6, v10

    invoke-virtual {v3, v5, v6}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/CheckedTextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_3

    .line 130
    :cond_a
    invoke-virtual {v4}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v3

    invoke-virtual {v3}, Lcom/peel/data/g;->d()I

    move-result v3

    const/16 v4, 0x12

    if-ne v3, v4, :cond_0

    .line 131
    invoke-virtual {v0, v2}, Landroid/widget/CheckedTextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_4

    .line 136
    :cond_b
    if-nez p2, :cond_1

    iget-object v0, p0, Lcom/peel/ui/ah;->b:Landroid/view/LayoutInflater;

    sget v1, Lcom/peel/ui/fq;->device_add_row:I

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    goto/16 :goto_5

    .line 72
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_5
        :pswitch_6
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_7
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_4
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public getViewTypeCount()I
    .locals 1

    .prologue
    .line 47
    iget-boolean v0, p0, Lcom/peel/ui/ah;->e:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x2

    goto :goto_0
.end method
