.class public Lcom/peel/ui/BackKeyEditText;
.super Landroid/widget/EditText;


# instance fields
.field private a:Lcom/peel/ui/b;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0, p1}, Landroid/widget/EditText;-><init>(Landroid/content/Context;)V

    .line 19
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0, p1, p2}, Landroid/widget/EditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 23
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/EditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 27
    return-void
.end method


# virtual methods
.method public onKeyPreIme(ILandroid/view/KeyEvent;)Z
    .locals 2

    .prologue
    .line 31
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    invoke-virtual {p2}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    .line 32
    iget-object v0, p0, Lcom/peel/ui/BackKeyEditText;->a:Lcom/peel/ui/b;

    if-eqz v0, :cond_0

    .line 33
    iget-object v0, p0, Lcom/peel/ui/BackKeyEditText;->a:Lcom/peel/ui/b;

    invoke-interface {v0, p0}, Lcom/peel/ui/b;->a(Lcom/peel/ui/BackKeyEditText;)V

    .line 36
    :cond_0
    invoke-super {p0, p2}, Landroid/widget/EditText;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method public setOnEditTextImeBackListener(Lcom/peel/ui/b;)V
    .locals 0

    .prologue
    .line 40
    iput-object p1, p0, Lcom/peel/ui/BackKeyEditText;->a:Lcom/peel/ui/b;

    .line 41
    return-void
.end method
