.class Lcom/peel/ui/bd;
.super Lcom/peel/util/t;


# instance fields
.field final synthetic a:Lcom/peel/ui/bc;


# direct methods
.method constructor <init>(Lcom/peel/ui/bc;I)V
    .locals 0

    .prologue
    .line 143
    iput-object p1, p0, Lcom/peel/ui/bd;->a:Lcom/peel/ui/bc;

    invoke-direct {p0, p2}, Lcom/peel/util/t;-><init>(I)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    const/4 v3, 0x0

    .line 146
    iget-boolean v0, p0, Lcom/peel/ui/bd;->i:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/peel/ui/bd;->j:Ljava/lang/Object;

    if-eqz v0, :cond_0

    .line 147
    iget-object v0, p0, Lcom/peel/ui/bd;->j:Ljava/lang/Object;

    check-cast v0, Ljava/util/List;

    .line 148
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_1

    .line 149
    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/peel/content/listing/LiveListing;

    .line 150
    iget-object v0, p0, Lcom/peel/ui/bd;->a:Lcom/peel/ui/bc;

    iget-object v0, v0, Lcom/peel/ui/bc;->b:Landroid/view/View;

    sget v1, Lcom/peel/ui/fp;->channel:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 151
    iget-object v0, p0, Lcom/peel/ui/bd;->a:Lcom/peel/ui/bc;

    iget-object v0, v0, Lcom/peel/ui/bc;->b:Landroid/view/View;

    sget v1, Lcom/peel/ui/fp;->channel:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v2}, Lcom/peel/content/listing/LiveListing;->l()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 152
    iget-object v0, p0, Lcom/peel/ui/bd;->a:Lcom/peel/ui/bc;

    iget-object v0, v0, Lcom/peel/ui/bc;->b:Landroid/view/View;

    sget v1, Lcom/peel/ui/fp;->title:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Landroid/widget/TextView;

    .line 153
    invoke-virtual {v2}, Lcom/peel/content/listing/LiveListing;->o()J

    move-result-wide v0

    invoke-virtual {v2}, Lcom/peel/content/listing/LiveListing;->j()J

    move-result-wide v2

    iget-object v4, p0, Lcom/peel/ui/bd;->a:Lcom/peel/ui/bc;

    iget-object v4, v4, Lcom/peel/ui/bc;->d:Lcom/peel/ui/az;

    invoke-static {v4}, Lcom/peel/ui/az;->a(Lcom/peel/ui/az;)Landroid/support/v4/app/ae;

    move-result-object v4

    invoke-static {v4}, Landroid/text/format/DateFormat;->is24HourFormat(Landroid/content/Context;)Z

    move-result v4

    iget-object v5, p0, Lcom/peel/ui/bd;->a:Lcom/peel/ui/bc;

    iget-object v5, v5, Lcom/peel/ui/bc;->d:Lcom/peel/ui/az;

    invoke-static {v5}, Lcom/peel/ui/az;->a(Lcom/peel/ui/az;)Landroid/support/v4/app/ae;

    move-result-object v5

    sget v7, Lcom/peel/ui/ft;->time_pattern:I

    invoke-virtual {v5, v7}, Landroid/support/v4/app/ae;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static/range {v0 .. v5}, Lcom/peel/util/x;->a(JJZLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 152
    invoke-virtual {v6, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 159
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/peel/ui/bd;->a:Lcom/peel/ui/bc;

    iget-object v0, v0, Lcom/peel/ui/bc;->c:Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/peel/ui/bd;->a:Lcom/peel/ui/bc;

    iget-object v0, v0, Lcom/peel/ui/bc;->c:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_2

    .line 160
    iget-object v0, p0, Lcom/peel/ui/bd;->a:Lcom/peel/ui/bc;

    iget-object v0, v0, Lcom/peel/ui/bc;->d:Lcom/peel/ui/az;

    invoke-static {v0}, Lcom/peel/ui/az;->a(Lcom/peel/ui/az;)Landroid/support/v4/app/ae;

    move-result-object v0

    invoke-static {v0}, Lcom/peel/util/b/e;->a(Landroid/content/Context;)Lcom/squareup/picasso/Picasso;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/ui/bd;->a:Lcom/peel/ui/bc;

    iget-object v1, v1, Lcom/peel/ui/bc;->c:Ljava/lang/String;

    .line 161
    invoke-virtual {v0, v1}, Lcom/squareup/picasso/Picasso;->load(Ljava/lang/String;)Lcom/squareup/picasso/RequestCreator;

    move-result-object v0

    sget v1, Lcom/peel/ui/fo;->btn_noitem_list:I

    .line 162
    invoke-virtual {v0, v1}, Lcom/squareup/picasso/RequestCreator;->placeholder(I)Lcom/squareup/picasso/RequestCreator;

    move-result-object v1

    iget-object v0, p0, Lcom/peel/ui/bd;->a:Lcom/peel/ui/bc;

    iget-object v0, v0, Lcom/peel/ui/bc;->b:Landroid/view/View;

    sget v2, Lcom/peel/ui/fp;->channel_image:I

    .line 163
    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Lcom/squareup/picasso/RequestCreator;->into(Landroid/widget/ImageView;)V

    .line 167
    :goto_1
    return-void

    .line 155
    :cond_1
    iget-object v0, p0, Lcom/peel/ui/bd;->a:Lcom/peel/ui/bc;

    iget-object v0, v0, Lcom/peel/ui/bc;->b:Landroid/view/View;

    sget v1, Lcom/peel/ui/fp;->channel:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v1, p0, Lcom/peel/ui/bd;->a:Lcom/peel/ui/bc;

    iget-object v1, v1, Lcom/peel/ui/bc;->d:Lcom/peel/ui/az;

    invoke-static {v1}, Lcom/peel/ui/az;->a(Lcom/peel/ui/az;)Landroid/support/v4/app/ae;

    move-result-object v1

    sget v2, Lcom/peel/ui/ft;->no_shows_airing:I

    invoke-virtual {v1, v2}, Landroid/support/v4/app/ae;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 156
    iget-object v0, p0, Lcom/peel/ui/bd;->a:Lcom/peel/ui/bc;

    iget-object v0, v0, Lcom/peel/ui/bc;->b:Landroid/view/View;

    sget v1, Lcom/peel/ui/fp;->title:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 165
    :cond_2
    iget-object v0, p0, Lcom/peel/ui/bd;->a:Lcom/peel/ui/bc;

    iget-object v0, v0, Lcom/peel/ui/bc;->d:Lcom/peel/ui/az;

    invoke-static {v0}, Lcom/peel/ui/az;->d(Lcom/peel/ui/az;)Landroid/widget/ImageView;

    move-result-object v0

    sget v1, Lcom/peel/ui/fo;->btn_noitem_list:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_1
.end method
