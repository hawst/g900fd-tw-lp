.class Lcom/peel/ui/kp;
.super Landroid/widget/BaseAdapter;


# instance fields
.field final synthetic a:Lcom/peel/ui/kj;


# direct methods
.method private constructor <init>(Lcom/peel/ui/kj;)V
    .locals 0

    .prologue
    .line 329
    iput-object p1, p0, Lcom/peel/ui/kp;->a:Lcom/peel/ui/kj;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 438
    return-void
.end method

.method synthetic constructor <init>(Lcom/peel/ui/kj;Lcom/peel/ui/kk;)V
    .locals 0

    .prologue
    .line 329
    invoke-direct {p0, p1}, Lcom/peel/ui/kp;-><init>(Lcom/peel/ui/kj;)V

    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 332
    iget-object v0, p0, Lcom/peel/ui/kp;->a:Lcom/peel/ui/kj;

    invoke-static {v0}, Lcom/peel/ui/kj;->j(Lcom/peel/ui/kj;)I

    move-result v0

    add-int/lit16 v0, v0, 0x120

    add-int/lit8 v0, v0, 0x6

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 337
    if-nez p1, :cond_0

    .line 338
    iget-object v0, p0, Lcom/peel/ui/kp;->a:Lcom/peel/ui/kj;

    invoke-static {v0}, Lcom/peel/ui/kj;->c(Lcom/peel/ui/kj;)[Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    aget-object v0, v0, v1

    .line 346
    :goto_0
    return-object v0

    .line 339
    :cond_0
    iget-object v0, p0, Lcom/peel/ui/kp;->a:Lcom/peel/ui/kj;

    invoke-static {v0}, Lcom/peel/ui/kj;->j(Lcom/peel/ui/kj;)I

    move-result v0

    if-ne p1, v0, :cond_1

    .line 340
    iget-object v0, p0, Lcom/peel/ui/kp;->a:Lcom/peel/ui/kj;

    invoke-static {v0}, Lcom/peel/ui/kj;->c(Lcom/peel/ui/kj;)[Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    aget-object v0, v0, v1

    goto :goto_0

    .line 341
    :cond_1
    iget-object v0, p0, Lcom/peel/ui/kp;->a:Lcom/peel/ui/kj;

    invoke-static {v0}, Lcom/peel/ui/kj;->j(Lcom/peel/ui/kj;)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    if-le p1, v0, :cond_2

    iget-object v0, p0, Lcom/peel/ui/kp;->a:Lcom/peel/ui/kj;

    invoke-static {v0}, Lcom/peel/ui/kj;->j(Lcom/peel/ui/kj;)I

    move-result v0

    sub-int v0, p1, v0

    div-int/lit8 v1, p1, 0x30

    sub-int/2addr v0, v1

    rem-int/lit8 v0, v0, 0x30

    if-nez v0, :cond_2

    .line 342
    iget-object v0, p0, Lcom/peel/ui/kp;->a:Lcom/peel/ui/kj;

    invoke-static {v0}, Lcom/peel/ui/kj;->c(Lcom/peel/ui/kj;)[Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/ui/kp;->a:Lcom/peel/ui/kj;

    invoke-static {v1}, Lcom/peel/ui/kj;->j(Lcom/peel/ui/kj;)I

    move-result v1

    sub-int v1, p1, v1

    div-int/lit8 v2, p1, 0x30

    sub-int/2addr v1, v2

    div-int/lit8 v1, v1, 0x30

    add-int/lit8 v1, v1, 0x1

    aget-object v0, v0, v1

    goto :goto_0

    .line 343
    :cond_2
    iget-object v0, p0, Lcom/peel/ui/kp;->a:Lcom/peel/ui/kj;

    invoke-static {v0}, Lcom/peel/ui/kj;->j(Lcom/peel/ui/kj;)I

    move-result v0

    if-ge p1, v0, :cond_3

    .line 344
    iget-object v0, p0, Lcom/peel/ui/kp;->a:Lcom/peel/ui/kj;

    invoke-static {v0}, Lcom/peel/ui/kj;->k(Lcom/peel/ui/kj;)J

    move-result-wide v0

    mul-int/lit8 v2, p1, 0x1e

    mul-int/lit8 v2, v2, 0x3c

    mul-int/lit16 v2, v2, 0x3e8

    int-to-long v2, v2

    add-long/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    goto :goto_0

    .line 346
    :cond_3
    iget-object v0, p0, Lcom/peel/ui/kp;->a:Lcom/peel/ui/kj;

    invoke-static {v0}, Lcom/peel/ui/kj;->k(Lcom/peel/ui/kj;)J

    move-result-wide v0

    iget-object v2, p0, Lcom/peel/ui/kp;->a:Lcom/peel/ui/kj;

    invoke-static {v2}, Lcom/peel/ui/kj;->j(Lcom/peel/ui/kj;)I

    move-result v2

    sub-int v2, p1, v2

    div-int/lit8 v3, p1, 0x30

    sub-int/2addr v2, v3

    div-int/lit8 v2, v2, 0x30

    sub-int v2, p1, v2

    add-int/lit8 v2, v2, -0x1

    mul-int/lit8 v2, v2, 0x1e

    mul-int/lit8 v2, v2, 0x3c

    mul-int/lit16 v2, v2, 0x3e8

    int-to-long v2, v2

    add-long/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    goto/16 :goto_0
.end method

.method public getItemId(I)J
    .locals 2

    .prologue
    .line 372
    int-to-long v0, p1

    return-wide v0
.end method

.method public getItemViewType(I)I
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 357
    if-nez p1, :cond_1

    .line 366
    :cond_0
    :goto_0
    return v0

    .line 359
    :cond_1
    iget-object v2, p0, Lcom/peel/ui/kp;->a:Lcom/peel/ui/kj;

    invoke-static {v2}, Lcom/peel/ui/kj;->j(Lcom/peel/ui/kj;)I

    move-result v2

    if-eq p1, v2, :cond_0

    .line 361
    iget-object v2, p0, Lcom/peel/ui/kp;->a:Lcom/peel/ui/kj;

    invoke-static {v2}, Lcom/peel/ui/kj;->j(Lcom/peel/ui/kj;)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    if-le p1, v2, :cond_2

    iget-object v2, p0, Lcom/peel/ui/kp;->a:Lcom/peel/ui/kj;

    invoke-static {v2}, Lcom/peel/ui/kj;->j(Lcom/peel/ui/kj;)I

    move-result v2

    sub-int v2, p1, v2

    div-int/lit8 v3, p1, 0x30

    sub-int/2addr v2, v3

    rem-int/lit8 v2, v2, 0x30

    if-eqz v2, :cond_0

    .line 363
    :cond_2
    iget-object v0, p0, Lcom/peel/ui/kp;->a:Lcom/peel/ui/kj;

    invoke-static {v0}, Lcom/peel/ui/kj;->j(Lcom/peel/ui/kj;)I

    move-result v0

    if-ge p1, v0, :cond_3

    move v0, v1

    .line 364
    goto :goto_0

    :cond_3
    move v0, v1

    .line 366
    goto :goto_0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 9

    .prologue
    const/4 v3, 0x0

    const/4 v8, 0x0

    .line 386
    .line 387
    invoke-virtual {p0, p1}, Lcom/peel/ui/kp;->getItemViewType(I)I

    move-result v0

    .line 388
    new-instance v2, Lcom/peel/ui/kq;

    invoke-direct {v2, p0}, Lcom/peel/ui/kq;-><init>(Lcom/peel/ui/kp;)V

    .line 389
    packed-switch v0, :pswitch_data_0

    .line 435
    :goto_0
    return-object p2

    .line 392
    :pswitch_0
    iget-object v0, p0, Lcom/peel/ui/kp;->a:Lcom/peel/ui/kj;

    invoke-static {v0}, Lcom/peel/ui/kj;->l(Lcom/peel/ui/kj;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lcom/peel/ui/fq;->settings_header_row:I

    invoke-virtual {v0, v1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 393
    sget v0, Lcom/peel/ui/fp;->text:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v2, Lcom/peel/ui/kq;->a:Landroid/widget/TextView;

    .line 394
    invoke-virtual {p0, p1}, Lcom/peel/ui/kp;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 395
    iget-object v1, v2, Lcom/peel/ui/kq;->a:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 398
    :pswitch_1
    iget-object v0, p0, Lcom/peel/ui/kp;->a:Lcom/peel/ui/kj;

    invoke-static {v0}, Lcom/peel/ui/kj;->l(Lcom/peel/ui/kj;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lcom/peel/ui/fq;->time_slot_item:I

    invoke-virtual {v0, v1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 399
    sget v0, Lcom/peel/ui/fp;->time_row:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v2, Lcom/peel/ui/kq;->a:Landroid/widget/TextView;

    .line 401
    sget v0, Lcom/peel/ui/fp;->bar:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, v2, Lcom/peel/ui/kq;->b:Landroid/widget/ImageView;

    .line 403
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v1

    .line 404
    invoke-virtual {p0, p1}, Lcom/peel/ui/kp;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual {v1, v4, v5}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 405
    iget-object v0, p0, Lcom/peel/ui/kp;->a:Lcom/peel/ui/kj;

    invoke-virtual {v0}, Lcom/peel/ui/kj;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    invoke-static {v0}, Landroid/text/format/DateFormat;->is24HourFormat(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 406
    sget-object v0, Lcom/peel/util/x;->g:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/text/SimpleDateFormat;

    invoke-virtual {v1}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    .line 410
    :goto_1
    iget-object v0, v2, Lcom/peel/ui/kq;->a:Landroid/widget/TextView;

    invoke-virtual {p0, p1}, Lcom/peel/ui/kp;->getItem(I)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setTag(Ljava/lang/Object;)V

    .line 411
    invoke-virtual {p0, p1}, Lcom/peel/ui/kp;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 413
    sget v0, Lcom/peel/ui/fo;->settings_list_color:I

    invoke-virtual {p2, v0}, Landroid/view/View;->setBackgroundResource(I)V

    .line 415
    iget-object v0, v2, Lcom/peel/ui/kq;->a:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getTag()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/peel/ui/kp;->a:Lcom/peel/ui/kj;

    invoke-static {v0}, Lcom/peel/ui/kj;->m(Lcom/peel/ui/kj;)J

    move-result-wide v4

    iget-object v0, v2, Lcom/peel/ui/kq;->a:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    cmp-long v0, v4, v6

    if-nez v0, :cond_2

    .line 416
    iget-object v0, v2, Lcom/peel/ui/kq;->a:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/peel/ui/kp;->a:Lcom/peel/ui/kj;

    sget v4, Lcom/peel/ui/ft;->now_airling_time:I

    invoke-virtual {v3, v4}, Lcom/peel/ui/kj;->a(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    aput-object v1, v4, v8

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 418
    sget v0, Lcom/peel/ui/kj;->f:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 419
    sput p1, Lcom/peel/ui/kj;->f:I

    .line 424
    :cond_0
    :goto_2
    sget v0, Lcom/peel/ui/kj;->f:I

    if-ne v0, p1, :cond_3

    .line 425
    iget-object v0, v2, Lcom/peel/ui/kq;->b:Landroid/widget/ImageView;

    invoke-virtual {v0, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_0

    .line 408
    :cond_1
    sget-object v0, Lcom/peel/util/x;->f:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/text/SimpleDateFormat;

    invoke-virtual {v1}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    goto :goto_1

    .line 422
    :cond_2
    iget-object v0, v2, Lcom/peel/ui/kq;->a:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2

    .line 428
    :cond_3
    iget-object v0, v2, Lcom/peel/ui/kq;->b:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_0

    .line 389
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public getViewTypeCount()I
    .locals 1

    .prologue
    .line 351
    const/4 v0, 0x2

    return v0
.end method

.method public isEnabled(I)Z
    .locals 2

    .prologue
    .line 377
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/peel/ui/kp;->a:Lcom/peel/ui/kj;

    invoke-static {v0}, Lcom/peel/ui/kj;->j(Lcom/peel/ui/kj;)I

    move-result v0

    if-eq p1, v0, :cond_0

    iget-object v0, p0, Lcom/peel/ui/kp;->a:Lcom/peel/ui/kj;

    invoke-static {v0}, Lcom/peel/ui/kj;->j(Lcom/peel/ui/kj;)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    if-le p1, v0, :cond_1

    iget-object v0, p0, Lcom/peel/ui/kp;->a:Lcom/peel/ui/kj;

    invoke-static {v0}, Lcom/peel/ui/kj;->j(Lcom/peel/ui/kj;)I

    move-result v0

    sub-int v0, p1, v0

    div-int/lit8 v1, p1, 0x30

    sub-int/2addr v0, v1

    rem-int/lit8 v0, v0, 0x30

    if-nez v0, :cond_1

    .line 378
    :cond_0
    const/4 v0, 0x0

    .line 381
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method
