.class Lcom/peel/ui/eg;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/squareup/picasso/Callback;


# instance fields
.field final synthetic a:Lcom/peel/ui/ej;

.field final synthetic b:Lcom/peel/data/Channel;

.field final synthetic c:Lcom/peel/ui/ef;


# direct methods
.method constructor <init>(Lcom/peel/ui/ef;Lcom/peel/ui/ej;Lcom/peel/data/Channel;)V
    .locals 0

    .prologue
    .line 340
    iput-object p1, p0, Lcom/peel/ui/eg;->c:Lcom/peel/ui/ef;

    iput-object p2, p0, Lcom/peel/ui/eg;->a:Lcom/peel/ui/ej;

    iput-object p3, p0, Lcom/peel/ui/eg;->b:Lcom/peel/data/Channel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onError()V
    .locals 2

    .prologue
    .line 351
    iget-object v0, p0, Lcom/peel/ui/eg;->a:Lcom/peel/ui/ej;

    iget-object v0, v0, Lcom/peel/ui/ej;->a:Landroid/widget/ImageView;

    sget v1, Lcom/peel/ui/fo;->btn_noitem_list:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 352
    iget-object v0, p0, Lcom/peel/ui/eg;->c:Lcom/peel/ui/ef;

    iget-object v0, v0, Lcom/peel/ui/ef;->a:Lcom/peel/ui/du;

    invoke-virtual {v0}, Lcom/peel/ui/du;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    invoke-static {v0}, Lcom/peel/util/b/e;->a(Landroid/content/Context;)Lcom/squareup/picasso/Picasso;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/ui/eg;->a:Lcom/peel/ui/ej;

    iget-object v1, v1, Lcom/peel/ui/ej;->a:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Lcom/squareup/picasso/Picasso;->cancelRequest(Landroid/widget/ImageView;)V

    .line 353
    return-void
.end method

.method public onSuccess()V
    .locals 2

    .prologue
    .line 343
    iget-object v0, p0, Lcom/peel/ui/eg;->a:Lcom/peel/ui/ej;

    iget-object v0, v0, Lcom/peel/ui/ej;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/peel/ui/eg;->b:Lcom/peel/data/Channel;

    invoke-virtual {v0}, Lcom/peel/data/Channel;->f()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/peel/ui/eg;->a:Lcom/peel/ui/ej;

    iget-object v0, v0, Lcom/peel/ui/ej;->b:Ljava/lang/String;

    iget-object v1, p0, Lcom/peel/ui/eg;->b:Lcom/peel/data/Channel;

    invoke-virtual {v1}, Lcom/peel/data/Channel;->f()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 344
    :cond_0
    iget-object v0, p0, Lcom/peel/ui/eg;->a:Lcom/peel/ui/ej;

    iget-object v0, v0, Lcom/peel/ui/ej;->a:Landroid/widget/ImageView;

    sget v1, Lcom/peel/ui/fo;->btn_noitem_list:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 345
    iget-object v0, p0, Lcom/peel/ui/eg;->c:Lcom/peel/ui/ef;

    iget-object v0, v0, Lcom/peel/ui/ef;->a:Lcom/peel/ui/du;

    invoke-virtual {v0}, Lcom/peel/ui/du;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    invoke-static {v0}, Lcom/peel/util/b/e;->a(Landroid/content/Context;)Lcom/squareup/picasso/Picasso;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/ui/eg;->a:Lcom/peel/ui/ej;

    iget-object v1, v1, Lcom/peel/ui/ej;->a:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Lcom/squareup/picasso/Picasso;->cancelRequest(Landroid/widget/ImageView;)V

    .line 347
    :cond_1
    return-void
.end method
