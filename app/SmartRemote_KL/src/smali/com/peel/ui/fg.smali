.class Lcom/peel/ui/fg;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/peel/content/listing/LiveListing;

.field final synthetic b:Lcom/peel/data/Channel;

.field final synthetic c:I

.field final synthetic d:Lcom/peel/ui/fd;


# direct methods
.method constructor <init>(Lcom/peel/ui/fd;Lcom/peel/content/listing/LiveListing;Lcom/peel/data/Channel;I)V
    .locals 0

    .prologue
    .line 448
    iput-object p1, p0, Lcom/peel/ui/fg;->d:Lcom/peel/ui/fd;

    iput-object p2, p0, Lcom/peel/ui/fg;->a:Lcom/peel/content/listing/LiveListing;

    iput-object p3, p0, Lcom/peel/ui/fg;->b:Lcom/peel/data/Channel;

    iput p4, p0, Lcom/peel/ui/fg;->c:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x7e7

    const/4 v2, 0x1

    .line 451
    iget-object v0, p0, Lcom/peel/ui/fg;->d:Lcom/peel/ui/fd;

    iget-object v0, v0, Lcom/peel/ui/fd;->a:Lcom/peel/ui/ey;

    invoke-static {v0}, Lcom/peel/ui/ey;->h(Lcom/peel/ui/ey;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 452
    iget-object v0, p0, Lcom/peel/ui/fg;->d:Lcom/peel/ui/fd;

    iget-object v0, v0, Lcom/peel/ui/fd;->a:Lcom/peel/ui/ey;

    invoke-static {v0}, Lcom/peel/ui/ey;->e(Lcom/peel/ui/ey;)Landroid/content/Context;

    move-result-object v0

    const-string/jumbo v1, "remote_channel_guide_trigger"

    invoke-static {v0, v1, v2}, Lcom/peel/util/dq;->a(Landroid/content/Context;Ljava/lang/String;Z)V

    .line 453
    invoke-static {}, Lcom/peel/d/i;->a()Lcom/peel/d/i;

    move-result-object v0

    .line 454
    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/peel/ui/fg;->d:Lcom/peel/ui/fd;

    iget-object v0, v0, Lcom/peel/ui/fd;->a:Lcom/peel/ui/ey;

    invoke-static {v0}, Lcom/peel/ui/ey;->c(Lcom/peel/ui/ey;)Lcom/peel/content/library/LiveLibrary;

    move-result-object v0

    if-nez v0, :cond_1

    .line 470
    :cond_0
    :goto_0
    return-void

    .line 455
    :cond_1
    iget-object v0, p0, Lcom/peel/ui/fg;->d:Lcom/peel/ui/fd;

    iget-object v0, v0, Lcom/peel/ui/fd;->a:Lcom/peel/ui/ey;

    invoke-static {v0}, Lcom/peel/ui/ey;->i(Lcom/peel/ui/ey;)I

    move-result v0

    if-nez v0, :cond_2

    .line 456
    iget-object v0, p0, Lcom/peel/ui/fg;->a:Lcom/peel/content/listing/LiveListing;

    const-string/jumbo v1, "Channel Guide"

    invoke-static {v0, v3, v1}, Lcom/peel/util/bx;->a(Lcom/peel/content/listing/Listing;ILjava/lang/String;)V

    .line 462
    :goto_1
    iget-object v0, p0, Lcom/peel/ui/fg;->d:Lcom/peel/ui/fd;

    iget-object v0, v0, Lcom/peel/ui/fd;->a:Lcom/peel/ui/ey;

    invoke-static {v0}, Lcom/peel/ui/ey;->e(Lcom/peel/ui/ey;)Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/ui/fg;->b:Lcom/peel/data/Channel;

    invoke-virtual {v1}, Lcom/peel/data/Channel;->m()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/peel/util/bx;->e(Landroid/content/Context;Ljava/lang/String;)V

    .line 463
    iget-object v0, p0, Lcom/peel/ui/fg;->b:Lcom/peel/data/Channel;

    invoke-static {v0}, Lcom/peel/util/bx;->a(Lcom/peel/data/Channel;)V

    .line 464
    iget-object v0, p0, Lcom/peel/ui/fg;->d:Lcom/peel/ui/fd;

    iget-object v0, v0, Lcom/peel/ui/fd;->a:Lcom/peel/ui/ey;

    invoke-static {v0}, Lcom/peel/ui/ey;->j(Lcom/peel/ui/ey;)Lcom/peel/ui/a/q;

    move-result-object v0

    iget v1, p0, Lcom/peel/ui/fg;->c:I

    invoke-virtual {v0, v1}, Lcom/peel/ui/a/q;->a(I)V

    .line 465
    iget-object v0, p0, Lcom/peel/ui/fg;->d:Lcom/peel/ui/fd;

    iget-object v0, v0, Lcom/peel/ui/fd;->a:Lcom/peel/ui/ey;

    invoke-static {v0, p1}, Lcom/peel/ui/ey;->a(Lcom/peel/ui/ey;Landroid/view/View;)Landroid/view/View;

    .line 466
    iget-object v0, p0, Lcom/peel/ui/fg;->d:Lcom/peel/ui/fd;

    iget-object v0, v0, Lcom/peel/ui/fd;->a:Lcom/peel/ui/ey;

    invoke-static {v0, v2}, Lcom/peel/ui/ey;->a(Lcom/peel/ui/ey;Z)Z

    .line 467
    iget-object v0, p0, Lcom/peel/ui/fg;->d:Lcom/peel/ui/fd;

    iget-object v0, v0, Lcom/peel/ui/fd;->a:Lcom/peel/ui/ey;

    invoke-static {v0}, Lcom/peel/ui/ey;->k(Lcom/peel/ui/ey;)Landroid/view/View;

    move-result-object v0

    sget v1, Lcom/peel/ui/fo;->ch_bg_press:I

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    .line 468
    iget-object v0, p0, Lcom/peel/ui/fg;->d:Lcom/peel/ui/fd;

    iget-object v0, v0, Lcom/peel/ui/fd;->a:Lcom/peel/ui/ey;

    iget-object v0, v0, Lcom/peel/ui/ey;->a:Landroid/os/Handler;

    iget-object v1, p0, Lcom/peel/ui/fg;->d:Lcom/peel/ui/fd;

    iget-object v1, v1, Lcom/peel/ui/fd;->a:Lcom/peel/ui/ey;

    invoke-static {v1}, Lcom/peel/ui/ey;->l(Lcom/peel/ui/ey;)Ljava/lang/Runnable;

    move-result-object v1

    const-wide/16 v2, 0xbb8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 459
    :cond_2
    iget-object v0, p0, Lcom/peel/ui/fg;->a:Lcom/peel/content/listing/LiveListing;

    const-string/jumbo v1, "Recently Watched"

    invoke-static {v0, v3, v1}, Lcom/peel/util/bx;->a(Lcom/peel/content/listing/Listing;ILjava/lang/String;)V

    goto :goto_1
.end method
