.class Lcom/peel/ui/dh;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic a:Landroid/content/Context;

.field final synthetic b:Ljava/lang/String;

.field final synthetic c:I

.field final synthetic d:I

.field final synthetic e:Ljava/util/List;

.field final synthetic f:Ljava/util/List;

.field final synthetic g:Lcom/peel/ui/dr;

.field final synthetic h:Landroid/widget/EditText;

.field final synthetic i:Lcom/peel/ui/dg;


# direct methods
.method constructor <init>(Lcom/peel/ui/dg;Landroid/content/Context;Ljava/lang/String;IILjava/util/List;Ljava/util/List;Lcom/peel/ui/dr;Landroid/widget/EditText;)V
    .locals 0

    .prologue
    .line 87
    iput-object p1, p0, Lcom/peel/ui/dh;->i:Lcom/peel/ui/dg;

    iput-object p2, p0, Lcom/peel/ui/dh;->a:Landroid/content/Context;

    iput-object p3, p0, Lcom/peel/ui/dh;->b:Ljava/lang/String;

    iput p4, p0, Lcom/peel/ui/dh;->c:I

    iput p5, p0, Lcom/peel/ui/dh;->d:I

    iput-object p6, p0, Lcom/peel/ui/dh;->e:Ljava/util/List;

    iput-object p7, p0, Lcom/peel/ui/dh;->f:Ljava/util/List;

    iput-object p8, p0, Lcom/peel/ui/dh;->g:Lcom/peel/ui/dr;

    iput-object p9, p0, Lcom/peel/ui/dh;->h:Landroid/widget/EditText;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 10

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x0

    const/4 v1, 0x1

    .line 90
    iget-object v0, p0, Lcom/peel/ui/dh;->i:Lcom/peel/ui/dg;

    iget-object v0, v0, Lcom/peel/ui/dg;->b:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v6

    .line 91
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {v6}, Lcom/peel/util/bx;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 92
    :cond_0
    iget-object v0, p0, Lcom/peel/ui/dh;->i:Lcom/peel/ui/dg;

    iget-object v1, p0, Lcom/peel/ui/dh;->a:Landroid/content/Context;

    iget-object v2, p0, Lcom/peel/ui/dh;->i:Lcom/peel/ui/dg;

    iget-object v2, v2, Lcom/peel/ui/dg;->b:Landroid/widget/EditText;

    invoke-virtual {v0, v1, v2}, Lcom/peel/ui/dg;->a(Landroid/content/Context;Landroid/widget/EditText;)V

    .line 93
    iget-object v0, p0, Lcom/peel/ui/dh;->i:Lcom/peel/ui/dg;

    invoke-virtual {v0}, Lcom/peel/ui/dg;->dismiss()V

    .line 94
    new-instance v0, Lcom/peel/widget/ag;

    iget-object v1, p0, Lcom/peel/ui/dh;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/peel/widget/ag;-><init>(Landroid/content/Context;)V

    sget v1, Lcom/peel/ui/ft;->attention_required:I

    .line 95
    invoke-virtual {v0, v1}, Lcom/peel/widget/ag;->a(I)Lcom/peel/widget/ag;

    move-result-object v0

    sget v1, Lcom/peel/ui/ft;->enter_valid_email:I

    .line 96
    invoke-virtual {v0, v1}, Lcom/peel/widget/ag;->b(I)Lcom/peel/widget/ag;

    move-result-object v0

    sget v1, Lcom/peel/ui/ft;->okay:I

    new-instance v2, Lcom/peel/ui/di;

    invoke-direct {v2, p0}, Lcom/peel/ui/di;-><init>(Lcom/peel/ui/dh;)V

    .line 97
    invoke-virtual {v0, v1, v2}, Lcom/peel/widget/ag;->a(ILandroid/content/DialogInterface$OnClickListener;)Lcom/peel/widget/ag;

    move-result-object v0

    .line 108
    invoke-virtual {v0}, Lcom/peel/widget/ag;->d()Lcom/peel/widget/ag;

    move-result-object v0

    .line 109
    iget-object v1, p0, Lcom/peel/ui/dh;->i:Lcom/peel/ui/dg;

    iget-object v2, p0, Lcom/peel/ui/dh;->h:Landroid/widget/EditText;

    invoke-virtual {v1, v0, v2}, Lcom/peel/ui/dg;->a(Lcom/peel/widget/ag;Landroid/widget/EditText;)V

    .line 259
    :goto_0
    return-void

    .line 113
    :cond_1
    iget-object v0, p0, Lcom/peel/ui/dh;->i:Lcom/peel/ui/dg;

    iget-object v0, v0, Lcom/peel/ui/dg;->c:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v7

    .line 114
    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_2

    .line 115
    iget-object v0, p0, Lcom/peel/ui/dh;->i:Lcom/peel/ui/dg;

    iget-object v1, p0, Lcom/peel/ui/dh;->a:Landroid/content/Context;

    iget-object v2, p0, Lcom/peel/ui/dh;->i:Lcom/peel/ui/dg;

    iget-object v2, v2, Lcom/peel/ui/dg;->c:Landroid/widget/EditText;

    invoke-virtual {v0, v1, v2}, Lcom/peel/ui/dg;->a(Landroid/content/Context;Landroid/widget/EditText;)V

    .line 116
    iget-object v0, p0, Lcom/peel/ui/dh;->i:Lcom/peel/ui/dg;

    invoke-virtual {v0}, Lcom/peel/ui/dg;->dismiss()V

    .line 117
    new-instance v0, Lcom/peel/widget/ag;

    iget-object v1, p0, Lcom/peel/ui/dh;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/peel/widget/ag;-><init>(Landroid/content/Context;)V

    sget v1, Lcom/peel/ui/ft;->attention_required:I

    .line 118
    invoke-virtual {v0, v1}, Lcom/peel/widget/ag;->a(I)Lcom/peel/widget/ag;

    move-result-object v0

    sget v1, Lcom/peel/ui/ft;->please_enter_brand:I

    .line 119
    invoke-virtual {v0, v1}, Lcom/peel/widget/ag;->b(I)Lcom/peel/widget/ag;

    move-result-object v0

    sget v1, Lcom/peel/ui/ft;->okay:I

    new-instance v2, Lcom/peel/ui/dj;

    invoke-direct {v2, p0}, Lcom/peel/ui/dj;-><init>(Lcom/peel/ui/dh;)V

    .line 120
    invoke-virtual {v0, v1, v2}, Lcom/peel/widget/ag;->a(ILandroid/content/DialogInterface$OnClickListener;)Lcom/peel/widget/ag;

    move-result-object v0

    .line 130
    invoke-virtual {v0}, Lcom/peel/widget/ag;->d()Lcom/peel/widget/ag;

    move-result-object v0

    .line 131
    iget-object v1, p0, Lcom/peel/ui/dh;->i:Lcom/peel/ui/dg;

    iget-object v2, p0, Lcom/peel/ui/dh;->h:Landroid/widget/EditText;

    invoke-virtual {v1, v0, v2}, Lcom/peel/ui/dg;->a(Lcom/peel/widget/ag;Landroid/widget/EditText;)V

    goto :goto_0

    .line 134
    :cond_2
    iget-object v0, p0, Lcom/peel/ui/dh;->i:Lcom/peel/ui/dg;

    iget-object v0, v0, Lcom/peel/ui/dg;->d:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 135
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_3

    .line 136
    iget-object v0, p0, Lcom/peel/ui/dh;->i:Lcom/peel/ui/dg;

    iget-object v1, p0, Lcom/peel/ui/dh;->a:Landroid/content/Context;

    iget-object v2, p0, Lcom/peel/ui/dh;->i:Lcom/peel/ui/dg;

    iget-object v2, v2, Lcom/peel/ui/dg;->c:Landroid/widget/EditText;

    invoke-virtual {v0, v1, v2}, Lcom/peel/ui/dg;->a(Landroid/content/Context;Landroid/widget/EditText;)V

    .line 137
    iget-object v0, p0, Lcom/peel/ui/dh;->i:Lcom/peel/ui/dg;

    invoke-virtual {v0}, Lcom/peel/ui/dg;->dismiss()V

    .line 138
    new-instance v0, Lcom/peel/widget/ag;

    iget-object v1, p0, Lcom/peel/ui/dh;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/peel/widget/ag;-><init>(Landroid/content/Context;)V

    sget v1, Lcom/peel/ui/ft;->attention_required:I

    .line 139
    invoke-virtual {v0, v1}, Lcom/peel/widget/ag;->a(I)Lcom/peel/widget/ag;

    move-result-object v0

    sget v1, Lcom/peel/ui/ft;->please_enter_model:I

    .line 140
    invoke-virtual {v0, v1}, Lcom/peel/widget/ag;->b(I)Lcom/peel/widget/ag;

    move-result-object v0

    sget v1, Lcom/peel/ui/ft;->okay:I

    new-instance v2, Lcom/peel/ui/dk;

    invoke-direct {v2, p0}, Lcom/peel/ui/dk;-><init>(Lcom/peel/ui/dh;)V

    .line 141
    invoke-virtual {v0, v1, v2}, Lcom/peel/widget/ag;->a(ILandroid/content/DialogInterface$OnClickListener;)Lcom/peel/widget/ag;

    move-result-object v0

    .line 151
    invoke-virtual {v0}, Lcom/peel/widget/ag;->d()Lcom/peel/widget/ag;

    move-result-object v0

    .line 152
    iget-object v1, p0, Lcom/peel/ui/dh;->i:Lcom/peel/ui/dg;

    iget-object v2, p0, Lcom/peel/ui/dh;->h:Landroid/widget/EditText;

    invoke-virtual {v1, v0, v2}, Lcom/peel/ui/dg;->a(Lcom/peel/widget/ag;Landroid/widget/EditText;)V

    goto/16 :goto_0

    .line 161
    :cond_3
    iget-object v0, p0, Lcom/peel/ui/dh;->e:Ljava/util/List;

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/peel/ui/dh;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_8

    .line 162
    iget-object v0, p0, Lcom/peel/ui/dh;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_4
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/f/a;

    .line 163
    invoke-virtual {v0}, Lcom/peel/f/a;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_4

    move v2, v1

    move-object v3, v0

    .line 170
    :goto_1
    if-nez v2, :cond_7

    .line 171
    iget-object v0, p0, Lcom/peel/ui/dh;->f:Ljava/util/List;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/peel/ui/dh;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_7

    .line 172
    iget-object v0, p0, Lcom/peel/ui/dh;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_5
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/f/a;

    .line 173
    invoke-virtual {v0}, Lcom/peel/f/a;->b()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_5

    move-object v2, v0

    move v0, v1

    .line 181
    :goto_2
    if-eqz v0, :cond_6

    .line 184
    new-instance v0, Lcom/peel/widget/ag;

    iget-object v3, p0, Lcom/peel/ui/dh;->a:Landroid/content/Context;

    invoke-direct {v0, v3}, Lcom/peel/widget/ag;-><init>(Landroid/content/Context;)V

    sget v3, Lcom/peel/ui/ft;->attention_required:I

    .line 185
    invoke-virtual {v0, v3}, Lcom/peel/widget/ag;->a(I)Lcom/peel/widget/ag;

    move-result-object v0

    iget-object v3, p0, Lcom/peel/ui/dh;->a:Landroid/content/Context;

    sget v8, Lcom/peel/ui/ft;->brand_supported:I

    const/4 v9, 0x2

    new-array v9, v9, [Ljava/lang/Object;

    aput-object v7, v9, v4

    iget-object v4, p0, Lcom/peel/ui/dh;->b:Ljava/lang/String;

    aput-object v4, v9, v1

    .line 186
    invoke-virtual {v3, v8, v9}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/peel/widget/ag;->b(Ljava/lang/CharSequence;)Lcom/peel/widget/ag;

    move-result-object v0

    sget v1, Lcom/peel/ui/ft;->set_up:I

    new-instance v3, Lcom/peel/ui/dl;

    invoke-direct {v3, p0, v6, v2}, Lcom/peel/ui/dl;-><init>(Lcom/peel/ui/dh;Ljava/lang/String;Lcom/peel/f/a;)V

    .line 187
    invoke-virtual {v0, v1, v3}, Lcom/peel/widget/ag;->a(ILandroid/content/DialogInterface$OnClickListener;)Lcom/peel/widget/ag;

    move-result-object v0

    sget v1, Lcom/peel/ui/ft;->cancel:I

    .line 198
    invoke-virtual {v0, v1, v5}, Lcom/peel/widget/ag;->c(ILandroid/content/DialogInterface$OnClickListener;)Lcom/peel/widget/ag;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/widget/ag;->d()Lcom/peel/widget/ag;

    goto/16 :goto_0

    .line 201
    :cond_6
    iget-object v0, p0, Lcom/peel/ui/dh;->i:Lcom/peel/ui/dg;

    iget-object v1, p0, Lcom/peel/ui/dh;->a:Landroid/content/Context;

    iget-object v2, p0, Lcom/peel/ui/dh;->i:Lcom/peel/ui/dg;

    iget-object v2, v2, Lcom/peel/ui/dg;->c:Landroid/widget/EditText;

    invoke-virtual {v0, v1, v2}, Lcom/peel/ui/dg;->a(Landroid/content/Context;Landroid/widget/EditText;)V

    .line 203
    const-class v0, Lcom/peel/util/bx;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "report missing brand"

    new-instance v2, Lcom/peel/ui/dm;

    invoke-direct {v2, p0, v7, v6}, Lcom/peel/ui/dm;-><init>(Lcom/peel/ui/dh;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0, v1, v2}, Lcom/peel/util/i;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/lang/Runnable;

    .line 243
    iget-object v0, p0, Lcom/peel/ui/dh;->a:Landroid/content/Context;

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 244
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v1, "user_email"

    invoke-interface {v0, v1, v6}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 245
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "confirmation"

    new-instance v2, Lcom/peel/ui/dn;

    invoke-direct {v2, p0}, Lcom/peel/ui/dn;-><init>(Lcom/peel/ui/dh;)V

    invoke-static {v0, v1, v2}, Lcom/peel/util/i;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/lang/Runnable;

    goto/16 :goto_0

    :cond_7
    move v0, v2

    move-object v2, v3

    goto/16 :goto_2

    :cond_8
    move v2, v4

    move-object v3, v5

    goto/16 :goto_1
.end method
