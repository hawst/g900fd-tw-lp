.class public Lcom/peel/ui/gg;
.super Lcom/peel/d/u;


# static fields
.field private static final e:Ljava/lang/String;


# instance fields
.field private aj:Landroid/widget/TextView;

.field private ak:Lcom/peel/h/a/fa;

.field private al:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/peel/content/listing/Listing;",
            ">;>;"
        }
    .end annotation
.end field

.field private am:Landroid/os/Bundle;

.field private f:Landroid/view/LayoutInflater;

.field private g:Ljava/lang/String;

.field private h:Ljava/lang/String;

.field private i:Landroid/widget/ListView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 41
    const-class v0, Lcom/peel/ui/gg;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/peel/ui/gg;->e:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0}, Lcom/peel/d/u;-><init>()V

    return-void
.end method

.method static synthetic S()Ljava/lang/String;
    .locals 1

    .prologue
    .line 40
    sget-object v0, Lcom/peel/ui/gg;->e:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic a(Lcom/peel/ui/gg;)Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/peel/ui/gg;->am:Landroid/os/Bundle;

    return-object v0
.end method

.method static synthetic a(Lcom/peel/ui/gg;Lcom/peel/h/a/fa;)Lcom/peel/h/a/fa;
    .locals 0

    .prologue
    .line 40
    iput-object p1, p0, Lcom/peel/ui/gg;->ak:Lcom/peel/h/a/fa;

    return-object p1
.end method

.method static synthetic a(Lcom/peel/ui/gg;Ljava/util/Map;)Ljava/util/Map;
    .locals 0

    .prologue
    .line 40
    iput-object p1, p0, Lcom/peel/ui/gg;->al:Ljava/util/Map;

    return-object p1
.end method

.method static synthetic b(Lcom/peel/ui/gg;)Landroid/widget/ListView;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/peel/ui/gg;->i:Landroid/widget/ListView;

    return-object v0
.end method

.method static synthetic c(Lcom/peel/ui/gg;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/peel/ui/gg;->aj:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic d(Lcom/peel/ui/gg;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/peel/ui/gg;->h:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic e(Lcom/peel/ui/gg;)Ljava/util/Map;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/peel/ui/gg;->al:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic f(Lcom/peel/ui/gg;)Landroid/view/LayoutInflater;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/peel/ui/gg;->f:Landroid/view/LayoutInflater;

    return-object v0
.end method

.method static synthetic g(Lcom/peel/ui/gg;)Lcom/peel/h/a/fa;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/peel/ui/gg;->ak:Lcom/peel/h/a/fa;

    return-object v0
.end method


# virtual methods
.method public Z()V
    .locals 6

    .prologue
    .line 208
    iget-object v0, p0, Lcom/peel/ui/gg;->d:Lcom/peel/d/a;

    if-nez v0, :cond_0

    .line 209
    new-instance v0, Lcom/peel/d/a;

    sget-object v1, Lcom/peel/d/d;->b:Lcom/peel/d/d;

    sget-object v2, Lcom/peel/d/b;->a:Lcom/peel/d/b;

    sget-object v3, Lcom/peel/d/c;->b:Lcom/peel/d/c;

    iget-object v4, p0, Lcom/peel/ui/gg;->h:Ljava/lang/String;

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, Lcom/peel/d/a;-><init>(Lcom/peel/d/d;Lcom/peel/d/b;Lcom/peel/d/c;Ljava/lang/String;Ljava/util/List;)V

    iput-object v0, p0, Lcom/peel/ui/gg;->d:Lcom/peel/d/a;

    .line 214
    :cond_0
    iget-object v0, p0, Lcom/peel/ui/gg;->b:Lcom/peel/d/i;

    iget-object v1, p0, Lcom/peel/ui/gg;->d:Lcom/peel/d/a;

    invoke-virtual {v0, v1}, Lcom/peel/d/i;->a(Lcom/peel/d/a;)V

    .line 215
    return-void
.end method

.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    .line 58
    iput-object p1, p0, Lcom/peel/ui/gg;->f:Landroid/view/LayoutInflater;

    .line 60
    sget v0, Lcom/peel/ui/fq;->schedules_for_channel_view:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 61
    sget v0, Lcom/peel/ui/fp;->list:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/peel/ui/gg;->i:Landroid/widget/ListView;

    .line 62
    sget v0, Lcom/peel/ui/fp;->empty_list_view:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/peel/ui/gg;->aj:Landroid/widget/TextView;

    .line 64
    return-object v1
.end method

.method public a_(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 53
    invoke-super {p0, p1}, Lcom/peel/d/u;->a_(Landroid/os/Bundle;)V

    .line 54
    return-void
.end method

.method public c()V
    .locals 3

    .prologue
    .line 87
    sget-object v0, Lcom/peel/ui/gg;->e:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/peel/ui/gg;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lcom/peel/util/bx;->a(Ljava/lang/String;Landroid/app/Activity;Z)V

    .line 89
    sget-object v0, Lcom/peel/ui/gg;->e:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "grab schedules for channel "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/peel/ui/gg;->g:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/peel/ui/gh;

    invoke-direct {v2, p0}, Lcom/peel/ui/gh;-><init>(Lcom/peel/ui/gg;)V

    invoke-static {v0, v1, v2}, Lcom/peel/util/i;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    .line 162
    iget-object v0, p0, Lcom/peel/ui/gg;->i:Landroid/widget/ListView;

    new-instance v1, Lcom/peel/ui/gj;

    invoke-direct {v1, p0}, Lcom/peel/ui/gj;-><init>(Lcom/peel/ui/gg;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 195
    iget-object v0, p0, Lcom/peel/ui/gg;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "category"

    iget-object v2, p0, Lcom/peel/ui/gg;->h:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 196
    return-void
.end method

.method public h(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 69
    invoke-super {p0, p1}, Lcom/peel/d/u;->h(Landroid/os/Bundle;)V

    .line 71
    iget-object v0, p0, Lcom/peel/ui/gg;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "callsign"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/ui/gg;->g:Ljava/lang/String;

    .line 72
    iget-object v0, p0, Lcom/peel/ui/gg;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "name"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/ui/gg;->h:Ljava/lang/String;

    .line 74
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, Lcom/peel/ui/gg;->am:Landroid/os/Bundle;

    .line 75
    iget-object v0, p0, Lcom/peel/ui/gg;->am:Landroid/os/Bundle;

    const-string/jumbo v1, "path"

    const-string/jumbo v2, "listing/channel"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 76
    iget-object v0, p0, Lcom/peel/ui/gg;->am:Landroid/os/Bundle;

    const-string/jumbo v1, "user"

    sget-object v2, Lcom/peel/content/a;->f:Lcom/peel/content/user/User;

    invoke-virtual {v2}, Lcom/peel/content/user/User;->x()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 77
    iget-object v0, p0, Lcom/peel/ui/gg;->am:Landroid/os/Bundle;

    const-string/jumbo v1, "library"

    const-string/jumbo v2, "live"

    invoke-static {v2}, Lcom/peel/content/a;->c(Ljava/lang/String;)Lcom/peel/content/library/Library;

    move-result-object v2

    invoke-virtual {v2}, Lcom/peel/content/library/Library;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 78
    iget-object v0, p0, Lcom/peel/ui/gg;->am:Landroid/os/Bundle;

    const-string/jumbo v1, "start"

    const-wide/16 v2, 0x1

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 79
    iget-object v0, p0, Lcom/peel/ui/gg;->am:Landroid/os/Bundle;

    const-string/jumbo v1, "window"

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 80
    iget-object v0, p0, Lcom/peel/ui/gg;->am:Landroid/os/Bundle;

    const-string/jumbo v1, "cacheWindow"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 81
    iget-object v0, p0, Lcom/peel/ui/gg;->am:Landroid/os/Bundle;

    const-string/jumbo v1, "limit"

    const/16 v2, 0x14

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 82
    iget-object v0, p0, Lcom/peel/ui/gg;->am:Landroid/os/Bundle;

    const-string/jumbo v1, "room"

    invoke-static {}, Lcom/peel/content/a;->a()Lcom/peel/data/ContentRoom;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 83
    iget-object v0, p0, Lcom/peel/ui/gg;->am:Landroid/os/Bundle;

    const-string/jumbo v1, "callsign"

    iget-object v2, p0, Lcom/peel/ui/gg;->g:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 84
    return-void
.end method

.method public w()V
    .locals 0

    .prologue
    .line 200
    invoke-super {p0}, Lcom/peel/d/u;->w()V

    .line 202
    invoke-virtual {p0}, Lcom/peel/ui/gg;->c()V

    .line 203
    return-void
.end method
