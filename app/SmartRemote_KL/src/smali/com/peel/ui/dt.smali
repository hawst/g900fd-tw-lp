.class Lcom/peel/ui/dt;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:I

.field final synthetic b:Landroid/widget/ImageView;

.field final synthetic c:Lcom/peel/ui/PowerActionProvider;


# direct methods
.method constructor <init>(Lcom/peel/ui/PowerActionProvider;ILandroid/widget/ImageView;)V
    .locals 0

    .prologue
    .line 52
    iput-object p1, p0, Lcom/peel/ui/dt;->c:Lcom/peel/ui/PowerActionProvider;

    iput p2, p0, Lcom/peel/ui/dt;->a:I

    iput-object p3, p0, Lcom/peel/ui/dt;->b:Landroid/widget/ImageView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 55
    iget v0, p0, Lcom/peel/ui/dt;->a:I

    if-gt v0, v1, :cond_1

    .line 56
    iget-object v0, p0, Lcom/peel/ui/dt;->b:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setClickable(Z)V

    .line 57
    iget-object v0, p0, Lcom/peel/ui/dt;->b:Landroid/widget/ImageView;

    const-string/jumbo v1, "Power_0"

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    .line 62
    :goto_0
    invoke-static {}, Lcom/peel/d/i;->a()Lcom/peel/d/i;

    move-result-object v0

    .line 63
    if-eqz v0, :cond_0

    .line 64
    invoke-virtual {v0}, Lcom/peel/d/i;->c()Landroid/support/v4/app/ae;

    move-result-object v0

    invoke-static {v0}, Lcom/peel/d/e;->a(Landroid/support/v4/app/ae;)Lcom/peel/d/u;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/ui/dt;->b:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Lcom/peel/d/u;->onClick(Landroid/view/View;)V

    .line 66
    :cond_0
    return-void

    .line 59
    :cond_1
    iget-object v0, p0, Lcom/peel/ui/dt;->b:Landroid/widget/ImageView;

    const-string/jumbo v1, "expand_power"

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    goto :goto_0
.end method
