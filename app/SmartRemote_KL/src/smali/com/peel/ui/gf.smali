.class public Lcom/peel/ui/gf;
.super Landroid/widget/ArrayAdapter;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lcom/peel/data/ContentRoom;",
        ">;"
    }
.end annotation


# instance fields
.field private a:Landroid/view/LayoutInflater;


# direct methods
.method public constructor <init>(Landroid/content/Context;I[Lcom/peel/data/ContentRoom;)V
    .locals 1

    .prologue
    .line 16
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    .line 17
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/ui/gf;->a:Landroid/view/LayoutInflater;

    .line 18
    return-void
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 22
    if-eqz p2, :cond_0

    .line 23
    :goto_0
    const v0, 0x1020014

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckedTextView;

    .line 24
    invoke-virtual {p0, p1}, Lcom/peel/ui/gf;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/peel/data/ContentRoom;

    invoke-virtual {v1}, Lcom/peel/data/ContentRoom;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/CheckedTextView;->setText(Ljava/lang/CharSequence;)V

    .line 25
    return-object p2

    .line 22
    :cond_0
    iget-object v0, p0, Lcom/peel/ui/gf;->a:Landroid/view/LayoutInflater;

    sget v1, Lcom/peel/ui/fq;->settings_single_selection_row_wospace:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    goto :goto_0
.end method
