.class public Lcom/peel/ui/bh;
.super Lcom/peel/d/u;


# static fields
.field public static e:Z

.field public static f:Z


# instance fields
.field private aj:Landroid/widget/Button;

.field private ak:Landroid/widget/LinearLayout;

.field private al:Lcom/peel/data/ContentRoom;

.field private am:Lcom/peel/content/library/LiveLibrary;

.field private an:Lcom/peel/ui/az;

.field private ao:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/peel/data/Channel;",
            ">;"
        }
    .end annotation
.end field

.field private ap:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private aq:Landroid/widget/TextView;

.field private ar:Ljava/util/Timer;

.field private as:Ljava/util/TimerTask;

.field private g:Landroid/widget/ListView;

.field private h:Landroid/view/View;

.field private i:Landroid/widget/Button;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 61
    sput-boolean v0, Lcom/peel/ui/bh;->e:Z

    .line 62
    sput-boolean v0, Lcom/peel/ui/bh;->f:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 49
    invoke-direct {p0}, Lcom/peel/d/u;-><init>()V

    .line 58
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/peel/ui/bh;->ao:Ljava/util/List;

    return-void
.end method

.method private S()V
    .locals 13

    .prologue
    const/4 v2, 0x0

    .line 440
    sget-object v0, Lcom/peel/content/a;->f:Lcom/peel/content/user/User;

    invoke-virtual {v0}, Lcom/peel/content/user/User;->l()[Lcom/peel/data/ContentRoom;

    move-result-object v1

    .line 442
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 443
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 444
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 446
    sget-object v0, Lcom/peel/content/a;->f:Lcom/peel/content/user/User;

    invoke-virtual {v0}, Lcom/peel/content/user/User;->h()Landroid/os/Bundle;

    move-result-object v6

    .line 447
    if-eqz v6, :cond_0

    .line 449
    const-string/jumbo v0, "favchannels"

    invoke-virtual {v6, v0}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 533
    :cond_0
    :goto_0
    return-void

    .line 453
    :cond_1
    invoke-virtual {v6}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_2
    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 454
    const-string/jumbo v8, "/"

    invoke-virtual {v0, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_2

    invoke-virtual {v6, v0}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v8

    if-eqz v8, :cond_2

    .line 456
    invoke-virtual {v6, v0}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v8

    .line 457
    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v8

    if-lez v8, :cond_2

    .line 458
    const-string/jumbo v8, "/"

    invoke-virtual {v0, v8}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v8

    aget-object v8, v8, v2

    invoke-virtual {v4, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 459
    invoke-virtual {v6, v0}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v8

    invoke-virtual {v5, v8}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 460
    iget-object v8, p0, Lcom/peel/ui/bh;->a:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v10, "adding room id: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string/jumbo v10, "/"

    invoke-virtual {v0, v10}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    aget-object v0, v0, v2

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v8, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 464
    :cond_3
    array-length v6, v1

    move v0, v2

    :goto_2
    if-ge v0, v6, :cond_5

    aget-object v7, v1, v0

    .line 465
    invoke-virtual {v7}, Lcom/peel/data/ContentRoom;->a()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v4, v8}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_4

    .line 466
    iget-object v8, p0, Lcom/peel/ui/bh;->a:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v10, "adding libraries: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v7}, Lcom/peel/data/ContentRoom;->g()[Ljava/lang/String;

    move-result-object v10

    array-length v10, v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 467
    invoke-virtual {v7}, Lcom/peel/data/ContentRoom;->g()[Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v7

    invoke-virtual {v3, v7}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 464
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 471
    :cond_5
    sget-object v0, Lcom/peel/content/a;->f:Lcom/peel/content/user/User;

    invoke-virtual {v0}, Lcom/peel/content/user/User;->s()V

    .line 475
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v4, v0, [Ljava/lang/String;

    move v1, v2

    .line 476
    :goto_3
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_6

    .line 477
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    aput-object v0, v4, v1

    .line 476
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    .line 480
    :cond_6
    invoke-static {v4}, Lcom/peel/data/m;->a([Ljava/lang/String;)[Lcom/peel/data/k;

    move-result-object v3

    .line 482
    if-eqz v3, :cond_7

    array-length v0, v3

    if-nez v0, :cond_8

    .line 484
    :cond_7
    iget-object v0, p0, Lcom/peel/ui/bh;->a:Ljava/lang/String;

    const-string/jumbo v1, "......NO libs from PeelData.getLibraries for libids: "

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 488
    :cond_8
    array-length v0, v3

    new-array v4, v0, [Lcom/peel/content/library/Library;

    move v1, v2

    .line 489
    :goto_4
    array-length v0, v4

    if-ge v1, v0, :cond_0

    .line 490
    aget-object v0, v3, v1

    invoke-virtual {v0}, Lcom/peel/data/k;->b()Ljava/lang/String;

    move-result-object v0

    aget-object v6, v3, v1

    invoke-virtual {v6}, Lcom/peel/data/k;->a()Ljava/lang/String;

    move-result-object v6

    aget-object v7, v3, v1

    invoke-virtual {v7}, Lcom/peel/data/k;->d()Landroid/os/Bundle;

    move-result-object v7

    invoke-static {v0, v6, v7}, Lcom/peel/content/library/Library;->a(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Lcom/peel/content/library/Library;

    move-result-object v0

    aput-object v0, v4, v1

    .line 492
    iget-object v0, p0, Lcom/peel/ui/bh;->a:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "Scheme Type: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    aget-object v7, v3, v1

    invoke-virtual {v7}, Lcom/peel/data/k;->b()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v0, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 495
    aget-object v0, v3, v1

    invoke-virtual {v0}, Lcom/peel/data/k;->b()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v6, "live"

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 496
    iget-object v0, p0, Lcom/peel/ui/bh;->a:Ljava/lang/String;

    const-string/jumbo v6, "Test Loading Scheme Live"

    invoke-static {v0, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 497
    aget-object v0, v4, v1

    check-cast v0, Lcom/peel/content/library/LiveLibrary;

    .line 498
    invoke-virtual {v0}, Lcom/peel/content/library/LiveLibrary;->i()[Lcom/peel/data/Channel;

    move-result-object v6

    .line 499
    if-eqz v6, :cond_b

    array-length v0, v6

    if-lez v0, :cond_b

    .line 501
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 503
    array-length v8, v6

    move v0, v2

    :goto_5
    if-ge v0, v8, :cond_a

    aget-object v9, v6, v0

    .line 504
    invoke-virtual {v9}, Lcom/peel/data/Channel;->a()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v5, v10}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_9

    .line 505
    iget-object v10, p0, Lcom/peel/ui/bh;->a:Ljava/lang/String;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v12, "Favorites Matching Channel Lineup: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    .line 507
    invoke-virtual {v9}, Lcom/peel/data/Channel;->c()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    .line 505
    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 508
    invoke-static {v9}, Lcom/peel/util/bx;->b(Lcom/peel/data/Channel;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 503
    :cond_9
    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    .line 511
    :cond_a
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 512
    const-string/jumbo v6, "path"

    const-string/jumbo v8, "channel/setfavs"

    invoke-virtual {v0, v6, v8}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 513
    const-string/jumbo v6, "channels"

    invoke-virtual {v0, v6, v7}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 515
    sget-object v6, Lcom/peel/content/a;->f:Lcom/peel/content/user/User;

    new-instance v7, Lcom/peel/ui/bp;

    invoke-direct {v7, p0}, Lcom/peel/ui/bp;-><init>(Lcom/peel/ui/bh;)V

    invoke-virtual {v6, v0, v7}, Lcom/peel/content/user/User;->a(Landroid/os/Bundle;Lcom/peel/util/t;)V

    .line 489
    :cond_b
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto/16 :goto_4
.end method

.method static synthetic a(Lcom/peel/ui/bh;)Lcom/peel/ui/az;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/peel/ui/bh;->an:Lcom/peel/ui/az;

    return-object v0
.end method

.method static synthetic a(Lcom/peel/ui/bh;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 49
    invoke-direct {p0, p1}, Lcom/peel/ui/bh;->a(Ljava/lang/String;)V

    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 9

    .prologue
    const/4 v3, 0x1

    const/4 v6, 0x0

    .line 160
    invoke-virtual {p0}, Lcom/peel/ui/bh;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    invoke-static {v0}, Landroid/text/format/DateFormat;->is24HourFormat(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 163
    :try_start_0
    sget-object v0, Lcom/peel/util/x;->c:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/text/SimpleDateFormat;

    invoke-virtual {v0, p1}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v1

    .line 164
    iget-object v2, p0, Lcom/peel/ui/bh;->aq:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/peel/ui/bh;->n()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/peel/ui/ft;->now_airling_time:I

    const/4 v0, 0x1

    new-array v5, v0, [Ljava/lang/Object;

    const/4 v6, 0x0

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1}, Ljava/util/Date;->getHours()I

    move-result v0

    const/16 v8, 0xa

    if-ge v0, v8, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "0"

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v1}, Ljava/util/Date;->getHours()I

    move-result v8

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v7, ":"

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v1}, Ljava/util/Date;->getMinutes()I

    move-result v0

    const/16 v1, 0x1e

    if-ge v0, v1, :cond_1

    const-string/jumbo v0, "00"

    :goto_1
    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v5, v6

    invoke-virtual {v3, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 171
    :goto_2
    return-void

    .line 164
    :cond_0
    invoke-virtual {v1}, Ljava/util/Date;->getHours()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    :cond_1
    const-string/jumbo v0, "30"
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 165
    :catch_0
    move-exception v0

    .line 166
    iget-object v1, p0, Lcom/peel/ui/bh;->a:Ljava/lang/String;

    iget-object v2, p0, Lcom/peel/ui/bh;->a:Ljava/lang/String;

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_2

    .line 169
    :cond_2
    iget-object v0, p0, Lcom/peel/ui/bh;->aq:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/peel/ui/bh;->n()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/peel/ui/ft;->now_airling_time:I

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/peel/ui/bh;->m()Landroid/support/v4/app/ae;

    move-result-object v4

    sget v5, Lcom/peel/ui/ft;->time_pattern:I

    invoke-virtual {v4, v5}, Landroid/support/v4/app/ae;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {p1, v4}, Lcom/peel/util/x;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2
.end method

.method static synthetic b(Lcom/peel/ui/bh;)Ljava/util/List;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/peel/ui/bh;->ao:Ljava/util/List;

    return-object v0
.end method

.method static synthetic c(Lcom/peel/ui/bh;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/peel/ui/bh;->a:Ljava/lang/String;

    return-object v0
.end method

.method private c()V
    .locals 17

    .prologue
    .line 251
    invoke-virtual/range {p0 .. p0}, Lcom/peel/ui/bh;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    if-nez v1, :cond_0

    .line 343
    :goto_0
    return-void

    .line 254
    :cond_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/peel/ui/bh;->ao:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 255
    sget-object v1, Lcom/peel/content/a;->f:Lcom/peel/content/user/User;

    invoke-virtual {v1}, Lcom/peel/content/user/User;->h()Landroid/os/Bundle;

    move-result-object v1

    .line 256
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 257
    new-instance v13, Ljava/util/ArrayList;

    invoke-direct {v13}, Ljava/util/ArrayList;-><init>()V

    .line 259
    if-eqz v1, :cond_8

    .line 264
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/peel/ui/bh;->ap:Ljava/util/ArrayList;

    .line 267
    const-string/jumbo v3, "favchannels"

    invoke-virtual {v1, v3}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 268
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/peel/ui/bh;->ap:Ljava/util/ArrayList;

    const-string/jumbo v4, "favchannels"

    invoke-virtual {v1, v4}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 274
    :cond_1
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/peel/ui/bh;->ap:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 275
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 278
    :cond_2
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/peel/ui/bh;->a:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, " **** favChannelIds: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/peel/ui/bh;->ap:Ljava/util/ArrayList;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 280
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/peel/ui/bh;->a:Ljava/lang/String;

    const-string/jumbo v3, "do the libaries tes thing"

    new-instance v4, Lcom/peel/ui/bm;

    move-object/from16 v0, p0

    invoke-direct {v4, v0}, Lcom/peel/ui/bm;-><init>(Lcom/peel/ui/bh;)V

    invoke-static {v1, v3, v4}, Lcom/peel/util/i;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/lang/Runnable;

    .line 288
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/peel/ui/bh;->ap:Ljava/util/ArrayList;

    if-eqz v1, :cond_6

    .line 289
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/peel/ui/bh;->ap:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 290
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/peel/ui/bh;->a:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, " **** cid: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v4, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 292
    :cond_3
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/peel/ui/bh;->am:Lcom/peel/content/library/LiveLibrary;

    invoke-virtual {v1}, Lcom/peel/content/library/LiveLibrary;->i()[Lcom/peel/data/Channel;

    move-result-object v3

    .line 293
    array-length v4, v3

    const/4 v1, 0x0

    :goto_3
    if-ge v1, v4, :cond_5

    aget-object v5, v3, v1

    .line 294
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/peel/ui/bh;->ap:Ljava/util/ArrayList;

    invoke-virtual {v5}, Lcom/peel/data/Channel;->c()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/peel/util/bx;->a(Ljava/util/ArrayList;Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 295
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/peel/ui/bh;->ao:Ljava/util/List;

    invoke-interface {v6, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 297
    invoke-virtual {v5}, Lcom/peel/data/Channel;->c()Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v5}, Lcom/peel/util/bx;->b(Ljava/util/ArrayList;Ljava/lang/String;)I

    move-result v5

    .line 298
    const/4 v6, -0x1

    if-eq v5, v6, :cond_4

    .line 299
    invoke-virtual {v2, v5}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 293
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 304
    :cond_5
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_7

    .line 305
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v14

    :goto_4
    invoke-interface {v14}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_7

    invoke-interface {v14}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 306
    const-string/jumbo v2, "#"

    invoke-virtual {v1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v15

    .line 307
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/peel/ui/bh;->ao:Ljava/util/List;

    move-object/from16 v16, v0

    new-instance v1, Lcom/peel/data/Channel;

    const/4 v2, 0x0

    aget-object v2, v15, v2

    const/4 v3, 0x0

    const/4 v4, 0x1

    aget-object v4, v15, v4

    const/4 v5, 0x5

    aget-object v5, v15, v5

    const/4 v6, 0x4

    aget-object v6, v15, v6

    const/4 v7, 0x2

    aget-object v7, v15, v7

    const/4 v8, 0x3

    aget-object v8, v15, v8

    .line 310
    invoke-static {v8}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v8

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    invoke-direct/range {v1 .. v12}, Lcom/peel/data/Channel;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 307
    move-object/from16 v0, v16

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 313
    const/4 v1, 0x0

    aget-object v1, v15, v1

    invoke-virtual {v13, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 318
    :cond_6
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/peel/ui/bh;->a:Ljava/lang/String;

    const-string/jumbo v2, " **** favChannelIds is NULL"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 323
    :cond_7
    :goto_5
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/peel/ui/bh;->ao:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-nez v1, :cond_9

    .line 324
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/peel/ui/bh;->h:Landroid/view/View;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 325
    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v2, -0x1

    const/4 v3, -0x1

    invoke-direct {v1, v2, v3}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 326
    const/16 v2, 0x32

    iput v2, v1, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    .line 327
    const/16 v2, 0x32

    iput v2, v1, Landroid/widget/LinearLayout$LayoutParams;->rightMargin:I

    .line 329
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/peel/ui/bh;->i:Landroid/widget/Button;

    invoke-virtual {v2, v1}, Landroid/widget/Button;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 331
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/peel/ui/bh;->aj:Landroid/widget/Button;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 332
    const/4 v1, 0x0

    sput-boolean v1, Lcom/peel/ui/bh;->f:Z

    .line 341
    :goto_6
    const/4 v1, 0x0

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/peel/ui/bh;->d:Lcom/peel/d/a;

    .line 342
    invoke-virtual/range {p0 .. p0}, Lcom/peel/ui/bh;->Z()V

    goto/16 :goto_0

    .line 321
    :cond_8
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/peel/ui/bh;->a:Ljava/lang/String;

    const-string/jumbo v2, " **** PeelContent.legacy.getFavChannels() == NULL"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_5

    .line 334
    :cond_9
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/peel/ui/bh;->h:Landroid/view/View;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 335
    new-instance v1, Lcom/peel/ui/az;

    invoke-virtual/range {p0 .. p0}, Lcom/peel/ui/bh;->m()Landroid/support/v4/app/ae;

    move-result-object v2

    sget v3, Lcom/peel/ui/fp;->channel:I

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/peel/ui/bh;->ao:Ljava/util/List;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/peel/ui/bh;->am:Lcom/peel/content/library/LiveLibrary;

    .line 336
    invoke-virtual {v5}, Lcom/peel/content/library/LiveLibrary;->e()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/peel/ui/bh;->al:Lcom/peel/data/ContentRoom;

    move-object v5, v13

    invoke-direct/range {v1 .. v7}, Lcom/peel/ui/az;-><init>(Landroid/support/v4/app/ae;ILjava/util/List;Ljava/util/List;Ljava/lang/String;Lcom/peel/data/ContentRoom;)V

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/peel/ui/bh;->an:Lcom/peel/ui/az;

    .line 337
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/peel/ui/bh;->an:Lcom/peel/ui/az;

    move-object/from16 v0, p0

    invoke-virtual {v1, v0}, Lcom/peel/ui/az;->a(Lcom/peel/ui/bh;)V

    .line 338
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/peel/ui/bh;->aj:Landroid/widget/Button;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/peel/ui/bh;->ao:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-nez v1, :cond_a

    const/16 v1, 0x8

    :goto_7
    invoke-virtual {v2, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 339
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/peel/ui/bh;->g:Landroid/widget/ListView;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/peel/ui/bh;->an:Lcom/peel/ui/az;

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    goto :goto_6

    .line 338
    :cond_a
    const/4 v1, 0x0

    goto :goto_7
.end method

.method static synthetic d(Lcom/peel/ui/bh;)Landroid/widget/Button;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/peel/ui/bh;->i:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic e(Lcom/peel/ui/bh;)Landroid/widget/Button;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/peel/ui/bh;->aj:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic f(Lcom/peel/ui/bh;)V
    .locals 0

    .prologue
    .line 49
    invoke-direct {p0}, Lcom/peel/ui/bh;->c()V

    return-void
.end method

.method static synthetic g(Lcom/peel/ui/bh;)V
    .locals 0

    .prologue
    .line 49
    invoke-direct {p0}, Lcom/peel/ui/bh;->S()V

    return-void
.end method

.method static synthetic h(Lcom/peel/ui/bh;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/peel/ui/bh;->a:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public Z()V
    .locals 6

    .prologue
    const/16 v3, 0x8

    const/4 v2, 0x0

    .line 407
    iget-object v0, p0, Lcom/peel/ui/bh;->d:Lcom/peel/d/a;

    if-nez v0, :cond_2

    .line 408
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 409
    sget v0, Lcom/peel/ui/fp;->menu_remote:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 412
    sget-object v0, Lcom/peel/c/a;->b:Lcom/peel/c/e;

    invoke-static {v0}, Lcom/peel/c/a;->a(Lcom/peel/c/e;)Ljava/lang/Object;

    move-result-object v0

    sget-object v1, Lcom/peel/c/b;->b:Lcom/peel/c/b;

    if-ne v0, v1, :cond_0

    sget v0, Lcom/peel/ui/fp;->menu_done:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v5, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 413
    sget v0, Lcom/peel/ui/fp;->overflow_menu_btn:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 416
    :cond_0
    iget-object v0, p0, Lcom/peel/ui/bh;->ao:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_3

    .line 417
    sget v0, Lcom/peel/ui/fp;->menu_edit:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 418
    iget-object v0, p0, Lcom/peel/ui/bh;->ak:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 419
    iget-object v0, p0, Lcom/peel/ui/bh;->aq:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 420
    const/4 v0, 0x1

    sput-boolean v0, Lcom/peel/ui/bh;->e:Z

    .line 427
    :goto_0
    sget-object v0, Lcom/peel/c/a;->b:Lcom/peel/c/e;

    invoke-static {v0}, Lcom/peel/c/a;->a(Lcom/peel/c/e;)Ljava/lang/Object;

    move-result-object v0

    sget-object v1, Lcom/peel/c/b;->a:Lcom/peel/c/b;

    if-ne v0, v1, :cond_1

    .line 428
    sget v0, Lcom/peel/ui/fp;->menu_change_room:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 429
    sget v0, Lcom/peel/ui/fp;->menu_channels:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 430
    sget v0, Lcom/peel/ui/fp;->menu_settings:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 431
    sget v0, Lcom/peel/ui/fp;->menu_about:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 433
    :cond_1
    new-instance v0, Lcom/peel/d/a;

    sget-object v1, Lcom/peel/d/d;->b:Lcom/peel/d/d;

    sget-object v2, Lcom/peel/d/b;->a:Lcom/peel/d/b;

    sget-object v3, Lcom/peel/d/c;->b:Lcom/peel/d/c;

    sget v4, Lcom/peel/ui/ft;->favorites:I

    .line 434
    invoke-virtual {p0, v4}, Lcom/peel/ui/bh;->a(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct/range {v0 .. v5}, Lcom/peel/d/a;-><init>(Lcom/peel/d/d;Lcom/peel/d/b;Lcom/peel/d/c;Ljava/lang/String;Ljava/util/List;)V

    iput-object v0, p0, Lcom/peel/ui/bh;->d:Lcom/peel/d/a;

    .line 436
    :cond_2
    iget-object v0, p0, Lcom/peel/ui/bh;->b:Lcom/peel/d/i;

    iget-object v1, p0, Lcom/peel/ui/bh;->d:Lcom/peel/d/a;

    invoke-virtual {v0, v1}, Lcom/peel/d/i;->a(Lcom/peel/d/a;)V

    .line 437
    return-void

    .line 422
    :cond_3
    iget-object v0, p0, Lcom/peel/ui/bh;->aq:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 423
    iget-object v0, p0, Lcom/peel/ui/bh;->ak:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 424
    sput-boolean v2, Lcom/peel/ui/bh;->e:Z

    goto :goto_0
.end method

.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    .line 68
    iget-object v0, p0, Lcom/peel/ui/bh;->a:Ljava/lang/String;

    const-string/jumbo v1, "\n\n ******** onCreateView"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 69
    sget v0, Lcom/peel/ui/fq;->fav_channel_list:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 71
    sget v0, Lcom/peel/ui/fp;->fav_ch_list:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/peel/ui/bh;->g:Landroid/widget/ListView;

    .line 72
    sget v0, Lcom/peel/ui/fp;->add_channel_btn:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/peel/ui/bh;->i:Landroid/widget/Button;

    .line 73
    sget v0, Lcom/peel/ui/fp;->add_channel_row:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/peel/ui/bh;->ak:Landroid/widget/LinearLayout;

    .line 74
    sget v0, Lcom/peel/ui/fp;->removeChannelBtn:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/peel/ui/bh;->aj:Landroid/widget/Button;

    .line 75
    sget v0, Lcom/peel/ui/fp;->empty_list_view:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/ui/bh;->h:Landroid/view/View;

    .line 76
    sget v0, Lcom/peel/ui/fp;->favChannelTitle:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/peel/ui/bh;->aq:Landroid/widget/TextView;

    .line 77
    return-object v1
.end method

.method public a(Landroid/view/MenuItem;)Z
    .locals 3

    .prologue
    const/16 v2, 0x8

    .line 175
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    .line 176
    sget v1, Lcom/peel/ui/fp;->menu_edit:I

    if-ne v0, v1, :cond_0

    .line 177
    iget-object v0, p0, Lcom/peel/ui/bh;->am:Lcom/peel/content/library/LiveLibrary;

    invoke-virtual {v0}, Lcom/peel/content/library/LiveLibrary;->i()[Lcom/peel/data/Channel;

    move-result-object v0

    array-length v0, v0

    .line 178
    iget-object v1, p0, Lcom/peel/ui/bh;->ao:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ne v1, v0, :cond_1

    .line 179
    iget-object v0, p0, Lcom/peel/ui/bh;->ak:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 183
    :goto_0
    iget-object v0, p0, Lcom/peel/ui/bh;->aq:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 184
    iget-object v0, p0, Lcom/peel/ui/bh;->an:Lcom/peel/ui/az;

    if-eqz v0, :cond_0

    .line 185
    const-class v0, Lcom/peel/ui/bh;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "remove channels"

    new-instance v2, Lcom/peel/ui/bk;

    invoke-direct {v2, p0}, Lcom/peel/ui/bk;-><init>(Lcom/peel/ui/bh;)V

    invoke-static {v0, v1, v2}, Lcom/peel/util/i;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/lang/Runnable;

    .line 206
    :cond_0
    invoke-super {p0, p1}, Lcom/peel/d/u;->a(Landroid/view/MenuItem;)Z

    move-result v0

    return v0

    .line 181
    :cond_1
    iget-object v0, p0, Lcom/peel/ui/bh;->ak:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_0
.end method

.method public b()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 347
    iget-object v2, p0, Lcom/peel/ui/bh;->a:Ljava/lang/String;

    const-string/jumbo v3, "\n\n ******** back"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 348
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/peel/ui/bh;->d:Lcom/peel/d/a;

    .line 349
    invoke-virtual {p0}, Lcom/peel/ui/bh;->Z()V

    .line 350
    sget-object v2, Lcom/peel/content/a;->h:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/peel/ui/bh;->b:Lcom/peel/d/i;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/peel/ui/bh;->an:Lcom/peel/ui/az;

    if-eqz v2, :cond_0

    .line 351
    iget-object v2, p0, Lcom/peel/ui/bh;->an:Lcom/peel/ui/az;

    invoke-virtual {v2}, Lcom/peel/ui/az;->a()I

    move-result v2

    if-ne v0, v2, :cond_0

    .line 352
    iget-object v2, p0, Lcom/peel/ui/bh;->an:Lcom/peel/ui/az;

    invoke-virtual {v2, v1}, Lcom/peel/ui/az;->a(I)V

    .line 353
    iget-object v2, p0, Lcom/peel/ui/bh;->i:Landroid/widget/Button;

    invoke-virtual {v2, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 354
    iget-object v2, p0, Lcom/peel/ui/bh;->aj:Landroid/widget/Button;

    invoke-virtual {v2, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 355
    sput-boolean v1, Lcom/peel/ui/bh;->f:Z

    .line 356
    invoke-direct {p0}, Lcom/peel/ui/bh;->c()V

    .line 362
    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public c(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 121
    invoke-super {p0, p1}, Lcom/peel/d/u;->c(Landroid/os/Bundle;)V

    .line 123
    iget-object v0, p0, Lcom/peel/ui/bh;->a:Ljava/lang/String;

    const-string/jumbo v1, "\n\n ******** update"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 125
    sget-object v0, Lcom/peel/content/a;->h:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-nez v0, :cond_0

    .line 142
    :goto_0
    return-void

    .line 128
    :cond_0
    invoke-static {}, Lcom/peel/content/a;->a()Lcom/peel/data/ContentRoom;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/ui/bh;->al:Lcom/peel/data/ContentRoom;

    .line 129
    const-string/jumbo v0, "live"

    invoke-static {v0}, Lcom/peel/content/a;->c(Ljava/lang/String;)Lcom/peel/content/library/Library;

    move-result-object v0

    check-cast v0, Lcom/peel/content/library/LiveLibrary;

    iput-object v0, p0, Lcom/peel/ui/bh;->am:Lcom/peel/content/library/LiveLibrary;

    .line 130
    const-class v0, Lcom/peel/ui/bh;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "get favorite channels"

    new-instance v2, Lcom/peel/ui/bj;

    invoke-direct {v2, p0}, Lcom/peel/ui/bj;-><init>(Lcom/peel/ui/bh;)V

    invoke-static {v0, v1, v2}, Lcom/peel/util/i;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/lang/Runnable;

    goto :goto_0
.end method

.method public d(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 82
    invoke-super {p0, p1}, Lcom/peel/d/u;->d(Landroid/os/Bundle;)V

    .line 84
    iget-object v0, p0, Lcom/peel/ui/bh;->a:Ljava/lang/String;

    const-string/jumbo v1, "\n\n ******** onActivityCreated"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 86
    if-eqz p1, :cond_0

    const-string/jumbo v0, "bundle"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 87
    const-string/jumbo v0, "bundle"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    invoke-super {p0, v0}, Lcom/peel/d/u;->c(Landroid/os/Bundle;)V

    .line 90
    :cond_0
    iget-object v0, p0, Lcom/peel/ui/bh;->g:Landroid/widget/ListView;

    new-instance v1, Lcom/peel/ui/bi;

    invoke-direct {v1, p0}, Lcom/peel/ui/bi;-><init>(Lcom/peel/ui/bh;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 115
    sget-object v0, Lcom/peel/content/a;->h:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 116
    iget-object v0, p0, Lcom/peel/ui/bh;->c:Landroid/os/Bundle;

    invoke-virtual {p0, v0}, Lcom/peel/ui/bh;->c(Landroid/os/Bundle;)V

    .line 117
    :cond_1
    return-void
.end method

.method public h(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 146
    invoke-super {p0, p1}, Lcom/peel/d/u;->h(Landroid/os/Bundle;)V

    .line 147
    iget-object v0, p0, Lcom/peel/ui/bh;->b:Lcom/peel/d/i;

    const-string/jumbo v1, "time"

    invoke-virtual {v0, v1}, Lcom/peel/d/i;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 148
    if-nez v0, :cond_0

    .line 149
    invoke-static {}, Lcom/peel/util/x;->b()Ljava/lang/String;

    move-result-object v0

    .line 150
    iget-object v1, p0, Lcom/peel/ui/bh;->b:Lcom/peel/d/i;

    const-string/jumbo v2, "time"

    invoke-virtual {v1, v2, v0}, Lcom/peel/d/i;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 153
    :cond_0
    invoke-direct {p0, v0}, Lcom/peel/ui/bh;->a(Ljava/lang/String;)V

    .line 154
    iget-object v0, p0, Lcom/peel/ui/bh;->a:Ljava/lang/String;

    const-string/jumbo v1, "\n\n ******** onViewStateRestored"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 156
    invoke-virtual {p0}, Lcom/peel/ui/bh;->Z()V

    .line 157
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    .line 211
    iget-object v0, p0, Lcom/peel/ui/bh;->b:Lcom/peel/d/i;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/peel/content/a;->h:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-nez v0, :cond_1

    .line 248
    :cond_0
    :goto_0
    return-void

    .line 214
    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    .line 215
    sget v1, Lcom/peel/ui/fp;->add_channel_row:I

    if-eq v0, v1, :cond_2

    sget v1, Lcom/peel/ui/fp;->add_channel_btn:I

    if-ne v0, v1, :cond_4

    .line 216
    :cond_2
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 217
    const-string/jumbo v1, "favChannelIds"

    iget-object v2, p0, Lcom/peel/ui/bh;->ap:Ljava/util/ArrayList;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 218
    invoke-virtual {p0}, Lcom/peel/ui/bh;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    const-class v2, Lcom/peel/ui/v;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lcom/peel/d/e;->b(Landroid/support/v4/app/ae;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 220
    invoke-static {}, Lcom/peel/util/a/f;->a()Lcom/peel/util/a/f;

    move-result-object v1

    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v0}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v0

    if-nez v0, :cond_3

    const/4 v0, 0x1

    :goto_1
    const/16 v2, 0x417

    const/16 v3, 0x7dd

    invoke-virtual {v1, v0, v2, v3}, Lcom/peel/util/a/f;->a(III)V

    goto :goto_0

    :cond_3
    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v0}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/data/at;->f()I

    move-result v0

    goto :goto_1

    .line 222
    :cond_4
    sget v1, Lcom/peel/ui/fp;->removeChannelBtn:I

    if-ne v0, v1, :cond_0

    .line 223
    iget-object v0, p0, Lcom/peel/ui/bh;->an:Lcom/peel/ui/az;

    if-eqz v0, :cond_0

    .line 224
    const-class v0, Lcom/peel/ui/bh;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "remove channels"

    new-instance v2, Lcom/peel/ui/bl;

    invoke-direct {v2, p0}, Lcom/peel/ui/bl;-><init>(Lcom/peel/ui/bh;)V

    invoke-static {v0, v1, v2}, Lcom/peel/util/i;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/lang/Runnable;

    goto :goto_0
.end method

.method public w()V
    .locals 6

    .prologue
    .line 374
    invoke-super {p0}, Lcom/peel/d/u;->w()V

    .line 375
    iget-object v0, p0, Lcom/peel/ui/bh;->a:Ljava/lang/String;

    const-string/jumbo v1, "\n\n ******** onResume"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 376
    iget-object v0, p0, Lcom/peel/ui/bh;->ar:Ljava/util/Timer;

    if-eqz v0, :cond_0

    .line 377
    iget-object v0, p0, Lcom/peel/ui/bh;->ar:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 379
    :cond_0
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, Lcom/peel/ui/bh;->ar:Ljava/util/Timer;

    .line 380
    new-instance v0, Lcom/peel/ui/bn;

    invoke-direct {v0, p0}, Lcom/peel/ui/bn;-><init>(Lcom/peel/ui/bh;)V

    iput-object v0, p0, Lcom/peel/ui/bh;->as:Ljava/util/TimerTask;

    .line 401
    iget-object v0, p0, Lcom/peel/ui/bh;->ar:Ljava/util/Timer;

    iget-object v1, p0, Lcom/peel/ui/bh;->as:Ljava/util/TimerTask;

    const-wide/16 v2, 0x3e8

    const-wide/16 v4, 0x1388

    invoke-virtual/range {v0 .. v5}, Ljava/util/Timer;->scheduleAtFixedRate(Ljava/util/TimerTask;JJ)V

    .line 402
    return-void
.end method

.method public x()V
    .locals 2

    .prologue
    .line 367
    invoke-super {p0}, Lcom/peel/d/u;->x()V

    .line 368
    iget-object v0, p0, Lcom/peel/ui/bh;->a:Ljava/lang/String;

    const-string/jumbo v1, "\n\n ******** onPause"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 369
    iget-object v0, p0, Lcom/peel/ui/bh;->ar:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 370
    return-void
.end method
