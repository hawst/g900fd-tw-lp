.class Lcom/peel/ui/jb;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field final synthetic a:Lcom/peel/ui/iz;


# direct methods
.method constructor <init>(Lcom/peel/ui/iz;)V
    .locals 0

    .prologue
    .line 3249
    iput-object p1, p0, Lcom/peel/ui/jb;->a:Lcom/peel/ui/iz;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    const/4 v8, 0x2

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 3255
    iget-object v0, p0, Lcom/peel/ui/jb;->a:Lcom/peel/ui/iz;

    iget-object v0, v0, Lcom/peel/ui/iz;->a:Lcom/peel/ui/gt;

    invoke-static {v0}, Lcom/peel/ui/gt;->U(Lcom/peel/ui/gt;)Lcom/peel/widget/ag;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/widget/ag;->dismiss()V

    .line 3257
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/control/h;

    .line 3258
    invoke-static {}, Lcom/peel/ui/gt;->S()Ljava/lang/String;

    move-result-object v1

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "\n\nvalid_device: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 3260
    invoke-virtual {v0}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v5

    .line 3261
    invoke-virtual {v5}, Lcom/peel/data/g;->f()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, " -- "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 3264
    invoke-virtual {v0}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v5

    .line 3265
    invoke-virtual {v5}, Lcom/peel/data/g;->d()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 3258
    invoke-static {v1, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3267
    iget-object v1, p0, Lcom/peel/ui/jb;->a:Lcom/peel/ui/iz;

    iget-object v1, v1, Lcom/peel/ui/iz;->a:Lcom/peel/ui/gt;

    invoke-static {v1}, Lcom/peel/ui/gt;->a(Lcom/peel/ui/gt;)Lcom/peel/control/a;

    move-result-object v1

    .line 3268
    invoke-virtual {v1}, Lcom/peel/control/a;->e()[Lcom/peel/control/h;

    move-result-object v4

    .line 3267
    array-length v5, v4

    move v1, v3

    :goto_0
    if-ge v1, v5, :cond_7

    aget-object v6, v4, v1

    .line 3269
    invoke-virtual {v6}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v6

    .line 3270
    invoke-virtual {v6}, Lcom/peel/data/g;->b()Ljava/lang/String;

    move-result-object v6

    .line 3272
    invoke-virtual {v0}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v7

    .line 3273
    invoke-virtual {v7}, Lcom/peel/data/g;->b()Ljava/lang/String;

    move-result-object v7

    .line 3271
    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    move v1, v2

    .line 3279
    :goto_1
    iget-object v4, p0, Lcom/peel/ui/jb;->a:Lcom/peel/ui/iz;

    iget-object v4, v4, Lcom/peel/ui/iz;->a:Lcom/peel/ui/gt;

    invoke-static {v4}, Lcom/peel/ui/gt;->a(Lcom/peel/ui/gt;)Lcom/peel/control/a;

    move-result-object v4

    .line 3280
    invoke-virtual {v4, v0}, Lcom/peel/control/a;->b(Lcom/peel/control/h;)[Ljava/lang/Integer;

    move-result-object v4

    .line 3282
    if-eqz v4, :cond_0

    array-length v5, v4

    if-nez v5, :cond_3

    .line 3284
    :cond_0
    new-array v2, v2, [Ljava/lang/Integer;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    .line 3299
    :goto_2
    if-eqz v1, :cond_6

    .line 3300
    invoke-static {}, Lcom/peel/ui/gt;->S()Ljava/lang/String;

    move-result-object v1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "\n\nhas device: update "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/peel/ui/jb;->a:Lcom/peel/ui/iz;

    iget-object v4, v4, Lcom/peel/ui/iz;->a:Lcom/peel/ui/gt;

    .line 3301
    invoke-static {v4}, Lcom/peel/ui/gt;->a(Lcom/peel/ui/gt;)Lcom/peel/control/a;

    move-result-object v4

    .line 3302
    invoke-virtual {v4}, Lcom/peel/control/a;->c()Lcom/peel/data/d;

    move-result-object v4

    .line 3303
    invoke-virtual {v4}, Lcom/peel/data/d;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, " with audio: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 3306
    invoke-virtual {v0}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v4

    .line 3307
    invoke-virtual {v4}, Lcom/peel/data/g;->f()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, " -- "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 3310
    invoke-virtual {v0}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v4

    .line 3311
    invoke-virtual {v4}, Lcom/peel/data/g;->d()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 3300
    invoke-static {v1, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3312
    iget-object v1, p0, Lcom/peel/ui/jb;->a:Lcom/peel/ui/iz;

    iget-object v1, v1, Lcom/peel/ui/iz;->a:Lcom/peel/ui/gt;

    invoke-static {v1}, Lcom/peel/ui/gt;->a(Lcom/peel/ui/gt;)Lcom/peel/control/a;

    move-result-object v1

    iget-object v3, p0, Lcom/peel/ui/jb;->a:Lcom/peel/ui/iz;

    iget-object v3, v3, Lcom/peel/ui/iz;->a:Lcom/peel/ui/gt;

    .line 3314
    invoke-static {v3}, Lcom/peel/ui/gt;->a(Lcom/peel/ui/gt;)Lcom/peel/control/a;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/peel/control/a;->a(Lcom/peel/control/h;)Ljava/lang/String;

    move-result-object v3

    .line 3312
    invoke-virtual {v1, v0, v3, v2}, Lcom/peel/control/a;->b(Lcom/peel/control/h;Ljava/lang/String;[Ljava/lang/Integer;)V

    .line 3334
    :goto_3
    iget-object v0, p0, Lcom/peel/ui/jb;->a:Lcom/peel/ui/iz;

    iget-object v0, v0, Lcom/peel/ui/iz;->a:Lcom/peel/ui/gt;

    iget-object v1, p0, Lcom/peel/ui/jb;->a:Lcom/peel/ui/iz;

    iget-object v1, v1, Lcom/peel/ui/iz;->a:Lcom/peel/ui/gt;

    invoke-static {v1}, Lcom/peel/ui/gt;->a(Lcom/peel/ui/gt;)Lcom/peel/control/a;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/peel/ui/gt;->b(Lcom/peel/ui/gt;Lcom/peel/control/a;)V

    .line 3336
    iget-object v0, p0, Lcom/peel/ui/jb;->a:Lcom/peel/ui/iz;

    iget-object v0, v0, Lcom/peel/ui/iz;->a:Lcom/peel/ui/gt;

    invoke-virtual {v0}, Lcom/peel/ui/gt;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    sget-object v1, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v1}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/peel/util/bx;->a(Landroid/content/Context;Lcom/peel/control/RoomControl;)V

    .line 3337
    iget-object v0, p0, Lcom/peel/ui/jb;->a:Lcom/peel/ui/iz;

    iget-object v0, v0, Lcom/peel/ui/iz;->a:Lcom/peel/ui/gt;

    invoke-static {v0}, Lcom/peel/ui/gt;->W(Lcom/peel/ui/gt;)V

    .line 3338
    sget-object v0, Lcom/peel/c/a;->b:Lcom/peel/c/e;

    invoke-static {v0}, Lcom/peel/c/a;->a(Lcom/peel/c/e;)Ljava/lang/Object;

    move-result-object v0

    sget-object v1, Lcom/peel/c/b;->c:Lcom/peel/c/b;

    if-ne v0, v1, :cond_1

    .line 3339
    iget-object v0, p0, Lcom/peel/ui/jb;->a:Lcom/peel/ui/iz;

    iget-object v0, v0, Lcom/peel/ui/iz;->a:Lcom/peel/ui/gt;

    invoke-static {v0}, Lcom/peel/ui/gt;->X(Lcom/peel/ui/gt;)V

    .line 3341
    :cond_1
    iget-object v0, p0, Lcom/peel/ui/jb;->a:Lcom/peel/ui/iz;

    iget-object v0, v0, Lcom/peel/ui/iz;->a:Lcom/peel/ui/gt;

    invoke-virtual {v0}, Lcom/peel/ui/gt;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/ae;->invalidateOptionsMenu()V

    .line 3342
    return-void

    .line 3267
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_0

    .line 3285
    :cond_3
    array-length v5, v4

    if-ne v5, v2, :cond_5

    .line 3286
    aget-object v5, v4, v3

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    if-nez v5, :cond_4

    move-object v2, v4

    .line 3288
    goto/16 :goto_2

    .line 3290
    :cond_4
    new-array v4, v8, [Ljava/lang/Integer;

    .line 3291
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v3

    .line 3292
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v4, v2

    move-object v2, v4

    goto/16 :goto_2

    .line 3295
    :cond_5
    new-array v4, v8, [Ljava/lang/Integer;

    .line 3296
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v3

    .line 3297
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v4, v2

    move-object v2, v4

    goto/16 :goto_2

    .line 3317
    :cond_6
    invoke-static {}, Lcom/peel/ui/gt;->S()Ljava/lang/String;

    move-result-object v1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "\n\nno device yet: add "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/peel/ui/jb;->a:Lcom/peel/ui/iz;

    iget-object v4, v4, Lcom/peel/ui/iz;->a:Lcom/peel/ui/gt;

    .line 3318
    invoke-static {v4}, Lcom/peel/ui/gt;->a(Lcom/peel/ui/gt;)Lcom/peel/control/a;

    move-result-object v4

    .line 3319
    invoke-virtual {v4}, Lcom/peel/control/a;->c()Lcom/peel/data/d;

    move-result-object v4

    .line 3320
    invoke-virtual {v4}, Lcom/peel/data/d;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, " with audio: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 3323
    invoke-virtual {v0}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v4

    .line 3324
    invoke-virtual {v4}, Lcom/peel/data/g;->f()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, " -- "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 3327
    invoke-virtual {v0}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v4

    .line 3328
    invoke-virtual {v4}, Lcom/peel/data/g;->d()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 3317
    invoke-static {v1, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3329
    iget-object v1, p0, Lcom/peel/ui/jb;->a:Lcom/peel/ui/iz;

    iget-object v1, v1, Lcom/peel/ui/iz;->a:Lcom/peel/ui/gt;

    invoke-static {v1}, Lcom/peel/ui/gt;->a(Lcom/peel/ui/gt;)Lcom/peel/control/a;

    move-result-object v1

    const/4 v3, 0x0

    invoke-virtual {v1, v0, v3, v2}, Lcom/peel/control/a;->a(Lcom/peel/control/h;Ljava/lang/String;[Ljava/lang/Integer;)Z

    goto/16 :goto_3

    :cond_7
    move v1, v3

    goto/16 :goto_1
.end method
