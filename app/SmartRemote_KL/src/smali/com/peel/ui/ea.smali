.class Lcom/peel/ui/ea;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/peel/ui/a/t;


# instance fields
.field final synthetic a:Lcom/peel/ui/du;


# direct methods
.method constructor <init>(Lcom/peel/ui/du;)V
    .locals 0

    .prologue
    .line 190
    iput-object p1, p0, Lcom/peel/ui/ea;->a:Lcom/peel/ui/du;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(I)V
    .locals 0

    .prologue
    .line 194
    return-void
.end method

.method public a(Z)V
    .locals 4

    .prologue
    .line 197
    iget-object v0, p0, Lcom/peel/ui/ea;->a:Lcom/peel/ui/du;

    invoke-static {v0}, Lcom/peel/ui/du;->d(Lcom/peel/ui/du;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Ignore Sync: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/peel/ui/ea;->a:Lcom/peel/ui/du;

    invoke-static {v2}, Lcom/peel/ui/du;->e(Lcom/peel/ui/du;)Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 198
    sget-object v0, Lcom/peel/b/a;->h:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/peel/ui/ea;->a:Lcom/peel/ui/du;

    invoke-static {v0}, Lcom/peel/ui/du;->e(Lcom/peel/ui/du;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 199
    sget-object v1, Lcom/peel/b/a;->h:Ljava/lang/String;

    iget-object v0, p0, Lcom/peel/ui/ea;->a:Lcom/peel/ui/du;

    .line 200
    invoke-static {v0}, Lcom/peel/ui/du;->b(Lcom/peel/ui/du;)Landroid/support/v4/view/ViewPager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->getAdapter()Landroid/support/v4/view/av;

    move-result-object v0

    check-cast v0, Lcom/peel/ui/ek;

    invoke-virtual {v0}, Lcom/peel/ui/ek;->d()[Lcom/peel/data/Channel;

    move-result-object v0

    .line 199
    invoke-static {v1, v0}, Lcom/peel/util/bx;->a(Ljava/lang/String;[Lcom/peel/data/Channel;)I

    move-result v0

    .line 201
    iget-object v1, p0, Lcom/peel/ui/ea;->a:Lcom/peel/ui/du;

    invoke-static {v1}, Lcom/peel/ui/du;->f(Lcom/peel/ui/du;)Lcom/peel/ui/a/q;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/peel/ui/a/q;->b(I)V

    .line 202
    iget-object v1, p0, Lcom/peel/ui/ea;->a:Lcom/peel/ui/du;

    invoke-static {v1}, Lcom/peel/ui/du;->g(Lcom/peel/ui/du;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "force selection 3 from iIndex: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 204
    :cond_0
    return-void
.end method

.method public b(I)V
    .locals 0

    .prologue
    .line 209
    return-void
.end method
