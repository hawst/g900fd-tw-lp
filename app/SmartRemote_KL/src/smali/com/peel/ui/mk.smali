.class Lcom/peel/ui/mk;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/peel/ui/mj;


# direct methods
.method constructor <init>(Lcom/peel/ui/mj;)V
    .locals 0

    .prologue
    .line 507
    iput-object p1, p0, Lcom/peel/ui/mk;->a:Lcom/peel/ui/mj;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 510
    iget-object v0, p0, Lcom/peel/ui/mk;->a:Lcom/peel/ui/mj;

    iget-object v0, v0, Lcom/peel/ui/mj;->a:Lcom/peel/ui/lq;

    iget-object v0, v0, Lcom/peel/ui/lq;->b:Lcom/peel/d/i;

    const-string/jumbo v1, "top_cache_time"

    iget-object v2, p0, Lcom/peel/ui/mk;->a:Lcom/peel/ui/mj;

    iget-object v2, v2, Lcom/peel/ui/mj;->a:Lcom/peel/ui/lq;

    iget-object v2, v2, Lcom/peel/ui/lq;->b:Lcom/peel/d/i;

    const-string/jumbo v3, "newTime"

    invoke-virtual {v2, v3}, Lcom/peel/d/i;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/peel/d/i;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 511
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 512
    const-string/jumbo v1, "refresh_fragments"

    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/String;

    const-class v3, Lcom/peel/ui/kr;

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v5

    const-class v3, Lcom/peel/ui/ci;

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v6

    const/4 v3, 0x2

    const-class v4, Lcom/peel/ui/cg;

    invoke-virtual {v4}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x3

    const-class v4, Lcom/peel/ui/ch;

    invoke-virtual {v4}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putStringArray(Ljava/lang/String;[Ljava/lang/String;)V

    .line 513
    const-string/jumbo v1, "selective"

    invoke-virtual {v0, v1, v5}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 514
    const-string/jumbo v1, "refresh"

    invoke-virtual {v0, v1, v6}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 515
    const-string/jumbo v1, "forcetime"

    iget-object v2, p0, Lcom/peel/ui/mk;->a:Lcom/peel/ui/mj;

    iget-object v2, v2, Lcom/peel/ui/mj;->a:Lcom/peel/ui/lq;

    iget-object v2, v2, Lcom/peel/ui/lq;->b:Lcom/peel/d/i;

    const-string/jumbo v3, "time"

    invoke-virtual {v2, v3}, Lcom/peel/d/i;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 516
    iget-object v1, p0, Lcom/peel/ui/mk;->a:Lcom/peel/ui/mj;

    iget-object v1, v1, Lcom/peel/ui/mj;->a:Lcom/peel/ui/lq;

    invoke-virtual {v1, v0}, Lcom/peel/ui/lq;->c(Landroid/os/Bundle;)V

    .line 517
    iget-object v0, p0, Lcom/peel/ui/mk;->a:Lcom/peel/ui/mj;

    iget-object v0, v0, Lcom/peel/ui/mj;->a:Lcom/peel/ui/lq;

    invoke-static {v0}, Lcom/peel/ui/lq;->o(Lcom/peel/ui/lq;)V

    .line 518
    iget-object v0, p0, Lcom/peel/ui/mk;->a:Lcom/peel/ui/mj;

    iget-object v0, v0, Lcom/peel/ui/mj;->a:Lcom/peel/ui/lq;

    invoke-static {v0}, Lcom/peel/ui/lq;->p(Lcom/peel/ui/lq;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/TextView;->invalidate()V

    .line 520
    return-void
.end method
