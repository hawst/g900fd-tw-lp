.class Lcom/peel/ui/de;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Ljava/lang/CharSequence;

.field final synthetic b:Lcom/peel/ui/dd;


# direct methods
.method constructor <init>(Lcom/peel/ui/dd;Ljava/lang/CharSequence;)V
    .locals 0

    .prologue
    .line 329
    iput-object p1, p0, Lcom/peel/ui/de;->b:Lcom/peel/ui/dd;

    iput-object p2, p0, Lcom/peel/ui/de;->a:Ljava/lang/CharSequence;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 333
    :try_start_0
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 334
    const-string/jumbo v1, "keyword"

    iget-object v2, p0, Lcom/peel/ui/de;->a:Ljava/lang/CharSequence;

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 335
    const-string/jumbo v1, "country"

    iget-object v2, p0, Lcom/peel/ui/de;->b:Lcom/peel/ui/dd;

    iget-object v2, v2, Lcom/peel/ui/dd;->a:Lcom/peel/ui/cj;

    invoke-virtual {v2}, Lcom/peel/ui/cj;->m()Landroid/support/v4/app/ae;

    move-result-object v2

    invoke-static {v2}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v2

    const-string/jumbo v3, "country_ISO"

    const-string/jumbo v4, "US"

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 337
    new-instance v1, Lcom/peel/ui/df;

    const/4 v2, 0x1

    invoke-direct {v1, p0, v2}, Lcom/peel/ui/df;-><init>(Lcom/peel/ui/de;I)V

    invoke-static {v0, v1}, Lcom/peel/content/a/j;->d(Landroid/os/Bundle;Lcom/peel/util/t;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 357
    iget-object v0, p0, Lcom/peel/ui/de;->b:Lcom/peel/ui/dd;

    invoke-static {v0, v5}, Lcom/peel/ui/dd;->a(Lcom/peel/ui/dd;Ljava/util/concurrent/Future;)Ljava/util/concurrent/Future;

    .line 359
    :goto_0
    return-void

    .line 354
    :catch_0
    move-exception v0

    .line 355
    :try_start_1
    invoke-static {}, Lcom/peel/ui/cj;->S()Ljava/lang/String;

    move-result-object v1

    invoke-static {}, Lcom/peel/ui/cj;->S()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 357
    iget-object v0, p0, Lcom/peel/ui/de;->b:Lcom/peel/ui/dd;

    invoke-static {v0, v5}, Lcom/peel/ui/dd;->a(Lcom/peel/ui/dd;Ljava/util/concurrent/Future;)Ljava/util/concurrent/Future;

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/peel/ui/de;->b:Lcom/peel/ui/dd;

    invoke-static {v1, v5}, Lcom/peel/ui/dd;->a(Lcom/peel/ui/dd;Ljava/util/concurrent/Future;)Ljava/util/concurrent/Future;

    throw v0
.end method
