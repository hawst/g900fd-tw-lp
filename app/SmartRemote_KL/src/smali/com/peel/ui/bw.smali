.class public abstract Lcom/peel/ui/bw;
.super Lcom/peel/d/u;

# interfaces
.implements Lcom/peel/widget/af;


# static fields
.field private static final g:Ljava/lang/String;


# instance fields
.field private aj:Ljava/lang/String;

.field private ak:Lcom/peel/util/dz;

.field private al:Lcom/jess/ui/TwoWayGridView;

.field private am:Landroid/view/View;

.field private an:Lcom/peel/widget/ObservableScrollView;

.field private ao:Landroid/view/ViewGroup;

.field private ap:Landroid/animation/ObjectAnimator;

.field private aq:Ljava/lang/String;

.field private ar:Z

.field private as:Lcom/peel/ui/a/p;

.field private at:Landroid/view/View;

.field private au:Landroid/content/BroadcastReceiver;

.field private final av:Lcom/peel/util/s;

.field protected e:I

.field protected f:I

.field private h:Landroid/view/LayoutInflater;

.field private i:Landroid/widget/LinearLayout;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 65
    const-class v0, Lcom/peel/ui/bw;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/peel/ui/bw;->g:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 64
    invoke-direct {p0}, Lcom/peel/d/u;-><init>()V

    .line 68
    const/4 v0, -0x1

    iput v0, p0, Lcom/peel/ui/bw;->e:I

    const/16 v0, 0x7d1

    iput v0, p0, Lcom/peel/ui/bw;->f:I

    .line 78
    const-class v0, Lcom/peel/ui/b/ag;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/ui/bw;->aq:Ljava/lang/String;

    .line 619
    new-instance v0, Lcom/peel/ui/cc;

    invoke-direct {v0, p0}, Lcom/peel/ui/cc;-><init>(Lcom/peel/ui/bw;)V

    iput-object v0, p0, Lcom/peel/ui/bw;->au:Landroid/content/BroadcastReceiver;

    .line 700
    new-instance v0, Lcom/peel/ui/ce;

    invoke-direct {v0, p0}, Lcom/peel/ui/ce;-><init>(Lcom/peel/ui/bw;)V

    iput-object v0, p0, Lcom/peel/ui/bw;->av:Lcom/peel/util/s;

    return-void
.end method

.method private S()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 235
    iget-object v0, p0, Lcom/peel/ui/bw;->at:Landroid/view/View;

    sget v1, Lcom/peel/ui/fp;->no_content_panel:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 236
    iget-object v0, p0, Lcom/peel/ui/bw;->at:Landroid/view/View;

    sget v1, Lcom/peel/ui/fp;->no_content_panel:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 243
    :goto_0
    iget-object v0, p0, Lcom/peel/ui/bw;->ap:Landroid/animation/ObjectAnimator;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/peel/ui/bw;->ap:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 244
    iget-object v0, p0, Lcom/peel/ui/bw;->ap:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->cancel()V

    .line 247
    :cond_0
    iget-object v0, p0, Lcom/peel/ui/bw;->at:Landroid/view/View;

    sget v1, Lcom/peel/ui/fp;->refresh:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 248
    new-instance v1, Lcom/peel/ui/bx;

    invoke-direct {v1, p0, v0}, Lcom/peel/ui/bx;-><init>(Lcom/peel/ui/bw;Landroid/view/View;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 279
    return-void

    .line 238
    :cond_1
    iget-object v0, p0, Lcom/peel/ui/bw;->h:Landroid/view/LayoutInflater;

    sget v1, Lcom/peel/ui/fq;->no_content:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 239
    iget-object v0, p0, Lcom/peel/ui/bw;->at:Landroid/view/View;

    sget v2, Lcom/peel/ui/fp;->genre_container:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 240
    iget-object v0, p0, Lcom/peel/ui/bw;->at:Landroid/view/View;

    sget v2, Lcom/peel/ui/fp;->genre_container:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    goto :goto_0
.end method

.method private T()Lcom/peel/util/s;
    .locals 1

    .prologue
    .line 698
    iget-object v0, p0, Lcom/peel/ui/bw;->av:Lcom/peel/util/s;

    return-object v0
.end method

.method static synthetic a(Lcom/peel/ui/bw;)Landroid/animation/ObjectAnimator;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/peel/ui/bw;->ap:Landroid/animation/ObjectAnimator;

    return-object v0
.end method

.method static synthetic a(Lcom/peel/ui/bw;Landroid/animation/ObjectAnimator;)Landroid/animation/ObjectAnimator;
    .locals 0

    .prologue
    .line 64
    iput-object p1, p0, Lcom/peel/ui/bw;->ap:Landroid/animation/ObjectAnimator;

    return-object p1
.end method

.method static synthetic a(Lcom/peel/ui/bw;Landroid/view/View;)Landroid/view/View;
    .locals 0

    .prologue
    .line 64
    iput-object p1, p0, Lcom/peel/ui/bw;->am:Landroid/view/View;

    return-object p1
.end method

.method private a(Ljava/util/List;Ljava/util/Map;Lcom/peel/ui/kg;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<[",
            "Lcom/peel/content/listing/Listing;",
            ">;>;",
            "Lcom/peel/ui/kg;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    const/4 v7, -0x1

    .line 643
    iget-object v0, p0, Lcom/peel/ui/bw;->i:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 644
    iget-object v0, p0, Lcom/peel/ui/bw;->i:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 647
    :cond_0
    iget-object v0, p0, Lcom/peel/ui/bw;->at:Landroid/view/View;

    sget v1, Lcom/peel/ui/fp;->no_content_panel:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 648
    iget-object v0, p0, Lcom/peel/ui/bw;->at:Landroid/view/View;

    sget v1, Lcom/peel/ui/fp;->no_content_panel:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 651
    :cond_1
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_2
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 652
    invoke-interface {p2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    .line 654
    if-eqz v1, :cond_2

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_2

    .line 655
    iget-object v2, p0, Lcom/peel/ui/bw;->h:Landroid/view/LayoutInflater;

    sget v3, Lcom/peel/ui/fq;->tile_bar:I

    iget-object v5, p0, Lcom/peel/ui/bw;->i:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v3, v5, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    .line 656
    sget v3, Lcom/peel/ui/fp;->title:I

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 657
    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 659
    sget v3, Lcom/peel/ui/fp;->tiles_grid:I

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/jess/ui/TwoWayGridView;

    iput-object v3, p0, Lcom/peel/ui/bw;->al:Lcom/jess/ui/TwoWayGridView;

    .line 660
    iget-object v3, p0, Lcom/peel/ui/bw;->al:Lcom/jess/ui/TwoWayGridView;

    new-instance v5, Lcom/peel/ui/ka;

    invoke-virtual {p0}, Lcom/peel/ui/bw;->m()Landroid/support/v4/app/ae;

    move-result-object v6

    invoke-direct {v5, v6, v7, v1}, Lcom/peel/ui/ka;-><init>(Landroid/content/Context;ILjava/util/List;)V

    invoke-virtual {v3, v5}, Lcom/jess/ui/TwoWayGridView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 661
    iget-object v1, p0, Lcom/peel/ui/bw;->al:Lcom/jess/ui/TwoWayGridView;

    new-instance v3, Lcom/peel/ui/cd;

    invoke-direct {v3, p0, p3, v0}, Lcom/peel/ui/cd;-><init>(Lcom/peel/ui/bw;Lcom/peel/ui/kg;Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Lcom/jess/ui/TwoWayGridView;->setOnItemClickListener(Lcom/jess/ui/z;)V

    .line 670
    sget-object v0, Lcom/peel/util/a;->e:[I

    if-eqz v0, :cond_3

    sget-object v0, Lcom/peel/util/a;->e:[I

    aget v0, v0, v8

    if-eq v0, v7, :cond_3

    sget-object v0, Lcom/peel/util/a;->e:[I

    aget v0, v0, v9

    if-eq v0, v7, :cond_3

    .line 671
    iget-object v0, p0, Lcom/peel/ui/bw;->al:Lcom/jess/ui/TwoWayGridView;

    invoke-virtual {v0}, Lcom/jess/ui/TwoWayGridView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 672
    invoke-virtual {p0}, Lcom/peel/ui/bw;->n()Landroid/content/res/Resources;

    move-result-object v1

    sget v3, Lcom/peel/ui/fn;->tile_height_actual:I

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sget-object v3, Lcom/peel/util/a;->e:[I

    aget v3, v3, v9

    add-int/2addr v1, v3

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 673
    iget-object v1, p0, Lcom/peel/ui/bw;->al:Lcom/jess/ui/TwoWayGridView;

    invoke-virtual {v1, v0}, Lcom/jess/ui/TwoWayGridView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 675
    :cond_3
    iget-object v0, p0, Lcom/peel/ui/bw;->i:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    goto/16 :goto_0

    .line 679
    :cond_4
    iget-object v0, p0, Lcom/peel/ui/bw;->i:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v0

    if-nez v0, :cond_5

    .line 680
    invoke-direct {p0}, Lcom/peel/ui/bw;->S()V

    .line 682
    :cond_5
    return-void
.end method

.method private a(Ljava/util/Map;Ljava/util/concurrent/atomic/AtomicInteger;)V
    .locals 22
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Landroid/os/Bundle;",
            ">;",
            "Ljava/util/concurrent/atomic/AtomicInteger;",
            ")V"
        }
    .end annotation

    .prologue
    .line 283
    monitor-enter p1

    .line 284
    :try_start_0
    invoke-interface/range {p1 .. p1}, Ljava/util/Map;->isEmpty()Z

    move-result v4

    .line 285
    monitor-exit p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 287
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/peel/ui/bw;->ap:Landroid/animation/ObjectAnimator;

    if-eqz v5, :cond_0

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/peel/ui/bw;->ap:Landroid/animation/ObjectAnimator;

    invoke-virtual {v5}, Landroid/animation/ObjectAnimator;->isRunning()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 288
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/peel/ui/bw;->ap:Landroid/animation/ObjectAnimator;

    invoke-virtual {v5}, Landroid/animation/ObjectAnimator;->cancel()V

    .line 291
    :cond_0
    move-object/from16 v0, p0

    iget v5, v0, Lcom/peel/ui/bw;->e:I

    packed-switch v5, :pswitch_data_0

    .line 299
    const/16 v5, 0x7d1

    move-object/from16 v0, p0

    iput v5, v0, Lcom/peel/ui/bw;->f:I

    .line 302
    :goto_0
    if-nez v4, :cond_f

    .line 303
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/peel/ui/bw;->an:Lcom/peel/widget/ObservableScrollView;

    new-instance v5, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v6, -0x1

    const/4 v7, -0x1

    invoke-direct {v5, v6, v7}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v4, v5}, Lcom/peel/widget/ObservableScrollView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 304
    new-instance v13, Ljava/util/LinkedHashMap;

    invoke-direct {v13}, Ljava/util/LinkedHashMap;-><init>()V

    .line 307
    const/4 v4, 0x3

    move-object/from16 v0, p0

    iget v5, v0, Lcom/peel/ui/bw;->e:I

    if-ne v4, v5, :cond_2

    sget-object v4, Lcom/peel/content/a;->f:Lcom/peel/content/user/User;

    .line 308
    invoke-virtual {v4}, Lcom/peel/content/user/User;->n()Ljava/util/List;

    move-result-object v4

    .line 309
    :goto_1
    const/4 v5, 0x0

    .line 311
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    move v6, v5

    :cond_1
    :goto_2
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_5

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/peel/data/Genre;

    .line 312
    invoke-virtual {v4}, Lcom/peel/data/Genre;->c()Z

    move-result v5

    if-nez v5, :cond_1

    .line 314
    invoke-virtual {v4}, Lcom/peel/data/Genre;->b()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-interface {v0, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/os/Bundle;

    .line 315
    if-eqz v5, :cond_1

    .line 317
    add-int/lit8 v6, v6, 0x1

    .line 319
    new-instance v8, Landroid/os/Bundle;

    invoke-direct {v8}, Landroid/os/Bundle;-><init>()V

    .line 320
    invoke-virtual {v4}, Lcom/peel/data/Genre;->b()Ljava/lang/String;

    move-result-object v9

    .line 321
    const-string/jumbo v4, "[^A-Za-z0-9]"

    const-string/jumbo v10, ""

    .line 322
    invoke-virtual {v9, v4, v10}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/peel/ui/bw;->aj:Ljava/lang/String;

    invoke-virtual/range {p0 .. p0}, Lcom/peel/ui/bw;->m()Landroid/support/v4/app/ae;

    move-result-object v11

    invoke-virtual {v11}, Landroid/support/v4/app/ae;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    .line 321
    invoke-static {v4, v10, v11}, Lcom/peel/util/bx;->a(Ljava/lang/String;Ljava/lang/String;Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v13, v4, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 324
    const-string/jumbo v4, "context_id"

    move-object/from16 v0, p0

    iget v10, v0, Lcom/peel/ui/bw;->e:I

    move-object/from16 v0, p0

    invoke-direct {v0, v10}, Lcom/peel/ui/bw;->b(I)I

    move-result v10

    invoke-virtual {v8, v4, v10}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 325
    const-string/jumbo v4, "empty"

    sget v10, Lcom/peel/ui/fq;->empty_view:I

    invoke-virtual {v8, v4, v10}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 326
    const-string/jumbo v4, "libraryIdList"

    invoke-virtual {v5, v4}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v10

    .line 327
    const-string/jumbo v11, "libraryIds"

    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v4

    new-array v4, v4, [Ljava/lang/String;

    invoke-virtual {v10, v4}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [Ljava/lang/String;

    invoke-virtual {v8, v11, v4}, Landroid/os/Bundle;->putStringArray(Ljava/lang/String;[Ljava/lang/String;)V

    .line 328
    invoke-virtual {v10}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :goto_3
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 329
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v12, "listings/"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v5, v11}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v11

    .line 330
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v14, "listings/"

    invoke-virtual {v12, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v8, v4, v11}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    goto :goto_3

    .line 285
    :catchall_0
    move-exception v4

    :try_start_1
    monitor-exit p1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v4

    .line 293
    :pswitch_0
    const/16 v5, 0x7d2

    move-object/from16 v0, p0

    iput v5, v0, Lcom/peel/ui/bw;->f:I

    goto/16 :goto_0

    .line 296
    :pswitch_1
    const/16 v5, 0x7d3

    move-object/from16 v0, p0

    iput v5, v0, Lcom/peel/ui/bw;->f:I

    goto/16 :goto_0

    .line 308
    :cond_2
    sget-object v4, Lcom/peel/content/a;->f:Lcom/peel/content/user/User;

    invoke-virtual {v4}, Lcom/peel/content/user/User;->m()Ljava/util/List;

    move-result-object v4

    goto/16 :goto_1

    .line 333
    :cond_3
    const-string/jumbo v4, "genre"

    invoke-virtual {v8, v4, v9}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 336
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/peel/ui/bw;->c:Landroid/os/Bundle;

    const-string/jumbo v5, "genre"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v9, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 338
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/peel/ui/bw;->c:Landroid/os/Bundle;

    const-string/jumbo v5, "genre"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->remove(Ljava/lang/String;)V

    .line 339
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/peel/ui/bw;->b:Lcom/peel/d/i;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v10, "selected_genre"

    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget v10, v0, Lcom/peel/ui/bw;->e:I

    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5, v6}, Lcom/peel/d/i;->b(Ljava/lang/String;I)V

    .line 342
    :cond_4
    const-string/jumbo v4, "title"

    const-string/jumbo v5, "[^A-Za-z0-9]"

    const-string/jumbo v10, ""

    .line 343
    invoke-virtual {v9, v5, v10}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/peel/ui/bw;->aj:Ljava/lang/String;

    invoke-virtual/range {p0 .. p0}, Lcom/peel/ui/bw;->m()Landroid/support/v4/app/ae;

    move-result-object v10

    invoke-virtual {v10}, Landroid/support/v4/app/ae;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    .line 342
    invoke-static {v5, v9, v10}, Lcom/peel/util/bx;->a(Ljava/lang/String;Ljava/lang/String;Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v8, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 346
    :cond_5
    if-nez v6, :cond_6

    .line 348
    invoke-direct/range {p0 .. p0}, Lcom/peel/ui/bw;->S()V

    .line 616
    :goto_4
    const-class v4, Lcom/peel/ui/bw;

    invoke-virtual {v4}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual/range {p0 .. p0}, Lcom/peel/ui/bw;->m()Landroid/support/v4/app/ae;

    move-result-object v5

    const/4 v6, 0x0

    invoke-static {v4, v5, v6}, Lcom/peel/util/bx;->a(Ljava/lang/String;Landroid/app/Activity;Z)V

    .line 617
    return-void

    .line 350
    :cond_6
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/peel/ui/bw;->at:Landroid/view/View;

    sget v5, Lcom/peel/ui/fp;->no_content_panel:I

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    if-eqz v4, :cond_7

    .line 351
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/peel/ui/bw;->at:Landroid/view/View;

    sget v5, Lcom/peel/ui/fp;->no_content_panel:I

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    const/16 v5, 0x8

    invoke-virtual {v4, v5}, Landroid/view/View;->setVisibility(I)V

    .line 356
    :cond_7
    :try_start_2
    const-string/jumbo v4, "live"

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/peel/ui/bw;->c:Landroid/os/Bundle;

    const-string/jumbo v6, "scheme"

    invoke-virtual {v5, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_a

    .line 357
    sget-object v4, Lcom/peel/util/x;->c:Ljava/lang/ThreadLocal;

    invoke-virtual {v4}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/text/SimpleDateFormat;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/peel/ui/bw;->b:Lcom/peel/d/i;

    const-string/jumbo v6, "time"

    invoke-virtual {v5, v6}, Lcom/peel/d/i;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/Date;->getTime()J

    move-result-wide v6

    .line 358
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/peel/ui/bw;->b:Lcom/peel/d/i;

    const-string/jumbo v5, "time"

    invoke-virtual {v4, v5}, Lcom/peel/d/i;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {}, Lcom/peel/util/x;->b()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_9

    .line 360
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/Calendar;->getTimeInMillis()J
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    move-result-wide v4

    .line 364
    :goto_5
    const-wide/32 v8, 0x1b7740

    add-long/2addr v6, v8

    move-wide/from16 v20, v6

    move-wide v6, v4

    move-wide/from16 v4, v20

    :goto_6
    move-wide v8, v4

    .line 373
    :goto_7
    new-instance v18, Ljava/util/ArrayList;

    invoke-direct/range {v18 .. v18}, Ljava/util/ArrayList;-><init>()V

    .line 374
    new-instance v19, Ljava/util/HashMap;

    invoke-direct/range {v19 .. v19}, Ljava/util/HashMap;-><init>()V

    .line 375
    invoke-interface {v13}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v12

    :cond_8
    :goto_8
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_d

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 376
    invoke-interface {v13, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/os/Bundle;

    .line 377
    const-string/jumbo v5, "libraryIds"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v14

    .line 379
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 380
    array-length v15, v14

    const/4 v10, 0x0

    move v11, v10

    :goto_9
    if-ge v11, v15, :cond_c

    aget-object v10, v14, v11

    .line 381
    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v17, "listings/"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v4, v10}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v10

    .line 382
    invoke-virtual {v10}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v16

    :goto_a
    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_b

    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Landroid/os/Parcelable;

    .line 383
    check-cast v10, Lcom/peel/content/listing/Listing;

    invoke-interface {v5, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_a

    :cond_9
    move-wide v4, v6

    .line 362
    goto :goto_5

    .line 366
    :cond_a
    const-wide/16 v4, -0x1

    move-wide v6, v4

    goto :goto_6

    .line 368
    :catch_0
    move-exception v4

    .line 369
    sget-object v5, Lcom/peel/ui/bw;->g:Ljava/lang/String;

    sget-object v6, Lcom/peel/ui/bw;->g:Ljava/lang/String;

    invoke-static {v5, v6, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 370
    const-wide/16 v8, -0x1

    move-wide v6, v8

    goto :goto_7

    .line 380
    :cond_b
    add-int/lit8 v10, v11, 0x1

    move v11, v10

    goto :goto_9

    .line 387
    :cond_c
    invoke-interface {v5}, Ljava/util/List;->isEmpty()Z

    move-result v10

    if-nez v10, :cond_8

    .line 389
    const-string/jumbo v10, "title"

    invoke-virtual {v4, v10}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    move-object/from16 v0, v18

    invoke-interface {v0, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 390
    const-string/jumbo v10, "title"

    invoke-virtual {v4, v10}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-class v10, Lcom/peel/content/listing/c;

    invoke-virtual {v10}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v10

    invoke-static {}, Lcom/peel/content/a;->a()Lcom/peel/data/ContentRoom;

    move-result-object v11

    invoke-static/range {v5 .. v11}, Lcom/peel/util/bx;->a(Ljava/util/List;JJLjava/lang/String;Lcom/peel/data/ContentRoom;)Ljava/util/List;

    move-result-object v5

    move-object/from16 v0, v19

    invoke-interface {v0, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_8

    .line 393
    :cond_d
    invoke-interface/range {v18 .. v18}, Ljava/util/List;->size()I

    move-result v4

    if-lez v4, :cond_e

    .line 395
    new-instance v11, Lcom/peel/ui/by;

    move-object/from16 v12, p0

    move-wide v14, v6

    move-wide/from16 v16, v8

    invoke-direct/range {v11 .. v17}, Lcom/peel/ui/by;-><init>(Lcom/peel/ui/bw;Ljava/util/Map;JJ)V

    .line 607
    move-object/from16 v0, p0

    move-object/from16 v1, v18

    move-object/from16 v2, v19

    invoke-direct {v0, v1, v2, v11}, Lcom/peel/ui/bw;->a(Ljava/util/List;Ljava/util/Map;Lcom/peel/ui/kg;)V

    goto/16 :goto_4

    .line 609
    :cond_e
    invoke-direct/range {p0 .. p0}, Lcom/peel/ui/bw;->S()V

    goto/16 :goto_4

    .line 613
    :cond_f
    invoke-direct/range {p0 .. p0}, Lcom/peel/ui/bw;->S()V

    goto/16 :goto_4

    .line 291
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private b(I)I
    .locals 1

    .prologue
    .line 83
    packed-switch p1, :pswitch_data_0

    .line 91
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 85
    :pswitch_0
    const/16 v0, 0x7d2

    goto :goto_0

    .line 87
    :pswitch_1
    const/16 v0, 0x7d3

    goto :goto_0

    .line 89
    :pswitch_2
    const/16 v0, 0x7d1

    goto :goto_0

    .line 83
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method static synthetic b(Lcom/peel/ui/bw;)Lcom/peel/util/dz;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/peel/ui/bw;->ak:Lcom/peel/util/dz;

    return-object v0
.end method

.method static synthetic c(Lcom/peel/ui/bw;)Landroid/view/View;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/peel/ui/bw;->am:Landroid/view/View;

    return-object v0
.end method

.method static synthetic c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 64
    sget-object v0, Lcom/peel/ui/bw;->g:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic d(Lcom/peel/ui/bw;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/peel/ui/bw;->aq:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic e(Lcom/peel/ui/bw;)Landroid/view/View;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/peel/ui/bw;->at:Landroid/view/View;

    return-object v0
.end method


# virtual methods
.method public X()Z
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 96
    iget-object v2, p0, Lcom/peel/ui/bw;->b:Lcom/peel/d/i;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/peel/ui/bw;->b:Lcom/peel/d/i;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "selected_genre"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/peel/ui/bw;->e:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3, v1}, Lcom/peel/d/i;->a(Ljava/lang/String;I)I

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 102
    iput-object p1, p0, Lcom/peel/ui/bw;->h:Landroid/view/LayoutInflater;

    .line 103
    sget v0, Lcom/peel/ui/fq;->tops_layout:I

    invoke-virtual {p1, v0, p2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/ui/bw;->at:Landroid/view/View;

    .line 104
    iget-object v0, p0, Lcom/peel/ui/bw;->at:Landroid/view/View;

    sget v1, Lcom/peel/ui/fp;->genre_container:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/peel/ui/bw;->i:Landroid/widget/LinearLayout;

    .line 105
    iget-object v0, p0, Lcom/peel/ui/bw;->at:Landroid/view/View;

    sget v1, Lcom/peel/ui/fp;->tops_scroll:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/peel/widget/ObservableScrollView;

    iput-object v0, p0, Lcom/peel/ui/bw;->an:Lcom/peel/widget/ObservableScrollView;

    .line 106
    iget-object v0, p0, Lcom/peel/ui/bw;->at:Landroid/view/View;

    sget v1, Lcom/peel/ui/fp;->ads_placeholder:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/peel/ui/bw;->ao:Landroid/view/ViewGroup;

    .line 108
    sget-object v0, Lcom/peel/util/bx;->d:Ljava/util/Map;

    sget v1, Lcom/peel/ui/fq;->tunein_overlay:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 109
    sget-object v0, Lcom/peel/util/bx;->d:Ljava/util/Map;

    sget v1, Lcom/peel/ui/fq;->tunein_overlay:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget v2, Lcom/peel/ui/fq;->tunein_overlay:I

    invoke-virtual {p1, v2, v4, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 110
    :cond_0
    sget-object v0, Lcom/peel/util/bx;->d:Ljava/util/Map;

    sget v1, Lcom/peel/ui/fq;->setreminder_overlay:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 111
    sget-object v0, Lcom/peel/util/bx;->d:Ljava/util/Map;

    sget v1, Lcom/peel/ui/fq;->setreminder_overlay:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget v2, Lcom/peel/ui/fq;->setreminder_overlay:I

    invoke-virtual {p1, v2, v4, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 113
    :cond_1
    iget-object v0, p0, Lcom/peel/ui/bw;->at:Landroid/view/View;

    return-object v0
.end method

.method public a(Landroid/view/View;Lcom/peel/ui/a/p;)V
    .locals 1

    .prologue
    .line 738
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/peel/ui/bw;->ao:Landroid/view/ViewGroup;

    if-eqz v0, :cond_0

    .line 739
    iget-object v0, p0, Lcom/peel/ui/bw;->ao:Landroid/view/ViewGroup;

    invoke-virtual {v0, p1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 741
    :cond_0
    iput-object p2, p0, Lcom/peel/ui/bw;->as:Lcom/peel/ui/a/p;

    .line 742
    return-void
.end method

.method public a(Lcom/peel/widget/ObservableScrollView;IIII)V
    .locals 3

    .prologue
    .line 718
    iget-object v0, p0, Lcom/peel/ui/bw;->am:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/peel/ui/bw;->am:Landroid/view/View;

    const-string/jumbo v1, "overlay"

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewWithTag(Ljava/lang/Object;)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 719
    iget-object v0, p0, Lcom/peel/ui/bw;->am:Landroid/view/View;

    check-cast v0, Landroid/widget/FrameLayout;

    iget-object v1, p0, Lcom/peel/ui/bw;->am:Landroid/view/View;

    const-string/jumbo v2, "overlay"

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewWithTag(Ljava/lang/Object;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->removeView(Landroid/view/View;)V

    .line 722
    :cond_0
    int-to-float v0, p3

    const/high16 v1, 0x43480000    # 200.0f

    invoke-virtual {p0}, Lcom/peel/ui/bw;->m()Landroid/support/v4/app/ae;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/ae;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v2, v2, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v1, v2

    cmpl-float v0, v0, v1

    if-lez v0, :cond_1

    .line 723
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/peel/ui/bw;->ar:Z

    .line 726
    :cond_1
    if-nez p3, :cond_3

    .line 727
    iget-boolean v0, p0, Lcom/peel/ui/bw;->ar:Z

    if-eqz v0, :cond_3

    .line 728
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/peel/ui/bw;->ar:Z

    .line 729
    iget-object v0, p0, Lcom/peel/ui/bw;->as:Lcom/peel/ui/a/p;

    if-eqz v0, :cond_2

    .line 730
    iget-object v0, p0, Lcom/peel/ui/bw;->as:Lcom/peel/ui/a/p;

    invoke-interface {v0}, Lcom/peel/ui/a/p;->a()V

    .line 732
    :cond_2
    sget-object v0, Lcom/peel/ui/bw;->g:Ljava/lang/String;

    const-string/jumbo v1, "Should refresh banner ad"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 735
    :cond_3
    return-void
.end method

.method public c(Landroid/os/Bundle;)V
    .locals 10

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 156
    sget-object v0, Lcom/peel/content/a;->h:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/peel/ui/bw;->b:Lcom/peel/d/i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/peel/ui/bw;->ak:Lcom/peel/util/dz;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/peel/util/ed;->b:Lcom/peel/util/ed;

    iget-object v3, p0, Lcom/peel/ui/bw;->ak:Lcom/peel/util/dz;

    iget-object v3, v3, Lcom/peel/util/dz;->a:Lcom/peel/util/ed;

    if-ne v0, v3, :cond_1

    .line 232
    :cond_0
    :goto_0
    return-void

    .line 159
    :cond_1
    const-string/jumbo v0, "selective"

    invoke-virtual {p1, v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 161
    iget-object v0, p0, Lcom/peel/ui/bw;->at:Landroid/view/View;

    check-cast v0, Landroid/view/ViewGroup;

    invoke-static {v0}, Lcom/peel/ui/a/v;->a(Landroid/view/ViewGroup;)V

    .line 162
    iget-object v0, p0, Lcom/peel/ui/bw;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "selective"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    goto :goto_0

    .line 165
    :cond_2
    invoke-super {p0, p1}, Lcom/peel/d/u;->c(Landroid/os/Bundle;)V

    .line 169
    iget-object v0, p0, Lcom/peel/ui/bw;->c:Landroid/os/Bundle;

    const-string/jumbo v3, "scheme"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 172
    iget-object v0, p0, Lcom/peel/ui/bw;->b:Lcom/peel/d/i;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/peel/ui/bw;->b:Lcom/peel/d/i;

    const-string/jumbo v4, "time"

    invoke-virtual {v0, v4}, Lcom/peel/d/i;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_3

    const-string/jumbo v0, "live"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 173
    iget-object v0, p0, Lcom/peel/ui/bw;->b:Lcom/peel/d/i;

    const-string/jumbo v4, "time"

    invoke-static {}, Lcom/peel/util/x;->b()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v4, v5}, Lcom/peel/d/i;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 177
    :cond_3
    iget-object v0, p0, Lcom/peel/ui/bw;->c:Landroid/os/Bundle;

    const-string/jumbo v4, "refresh"

    invoke-virtual {v0, v4, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 178
    iget-object v0, p0, Lcom/peel/ui/bw;->c:Landroid/os/Bundle;

    const-string/jumbo v4, "refresh"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->remove(Ljava/lang/String;)V

    .line 180
    sget-object v0, Lcom/peel/content/a;->h:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 181
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v4, "://"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 183
    invoke-static {}, Lcom/peel/content/a;->a()Lcom/peel/data/ContentRoom;

    move-result-object v5

    .line 186
    iget-object v0, p0, Lcom/peel/ui/bw;->b:Lcom/peel/d/i;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, "cache_time"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, Lcom/peel/d/i;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/peel/ui/bw;->b:Lcom/peel/d/i;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, "cache_time"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, Lcom/peel/d/i;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v6, p0, Lcom/peel/ui/bw;->b:Lcom/peel/d/i;

    const-string/jumbo v7, "time"

    invoke-virtual {v6, v7}, Lcom/peel/d/i;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_4

    move v0, v1

    .line 188
    :goto_1
    if-eqz v0, :cond_5

    const-string/jumbo v0, "live"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    move v0, v1

    .line 190
    :goto_2
    iget-object v3, p0, Lcom/peel/ui/bw;->b:Lcom/peel/d/i;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, "genres_listingsCount"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Lcom/peel/d/i;->a(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_7

    if-nez v0, :cond_7

    invoke-virtual {v5}, Lcom/peel/data/ContentRoom;->a()Ljava/lang/String;

    move-result-object v0

    iget-object v3, p0, Lcom/peel/ui/bw;->b:Lcom/peel/d/i;

    const-string/jumbo v6, "current_room_id"

    invoke-virtual {v3, v6}, Lcom/peel/d/i;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 193
    iget v0, p0, Lcom/peel/ui/bw;->e:I

    packed-switch v0, :pswitch_data_0

    .line 204
    const-string/jumbo v3, "shows_"

    .line 205
    const-string/jumbo v0, "shows_genres"

    .line 209
    :goto_3
    iget-object v6, p0, Lcom/peel/ui/bw;->b:Lcom/peel/d/i;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Lcom/peel/d/i;->e(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    .line 210
    if-eqz v0, :cond_7

    .line 211
    new-instance v6, Ljava/util/HashMap;

    invoke-direct {v6}, Ljava/util/HashMap;-><init>()V

    .line 212
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_4
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 213
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    const-string/jumbo v9, ""

    invoke-virtual {v0, v8, v9}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v8

    iget-object v9, p0, Lcom/peel/ui/bw;->b:Lcom/peel/d/i;

    invoke-virtual {v9, v0}, Lcom/peel/d/i;->c(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    invoke-interface {v6, v8, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_4

    :cond_4
    move v0, v2

    .line 186
    goto/16 :goto_1

    :cond_5
    move v0, v2

    .line 188
    goto/16 :goto_2

    .line 195
    :pswitch_0
    const-string/jumbo v3, "sports_"

    .line 196
    const-string/jumbo v0, "sports_genres"

    goto :goto_3

    .line 199
    :pswitch_1
    const-string/jumbo v3, "movies_"

    .line 200
    const-string/jumbo v0, "movies_genres"

    goto :goto_3

    .line 215
    :cond_6
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    iget-object v3, p0, Lcom/peel/ui/bw;->b:Lcom/peel/d/i;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v7, "genres_listingsCount"

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4, v2}, Lcom/peel/d/i;->a(Ljava/lang/String;I)I

    move-result v2

    invoke-direct {v0, v2}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    invoke-direct {p0, v6, v0}, Lcom/peel/ui/bw;->a(Ljava/util/Map;Ljava/util/concurrent/atomic/AtomicInteger;)V

    move v2, v1

    .line 220
    :cond_7
    if-nez v2, :cond_0

    .line 221
    iget-object v0, p0, Lcom/peel/ui/bw;->b:Lcom/peel/d/i;

    const-string/jumbo v2, "current_room_id"

    invoke-virtual {v5}, Lcom/peel/data/ContentRoom;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/peel/d/i;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 222
    sget-object v0, Lcom/peel/util/ed;->b:Lcom/peel/util/ed;

    iget-object v2, p0, Lcom/peel/ui/bw;->ak:Lcom/peel/util/dz;

    iget-object v2, v2, Lcom/peel/util/dz;->a:Lcom/peel/util/ed;

    if-eq v0, v2, :cond_0

    .line 223
    sget-object v0, Lcom/peel/util/ed;->d:Lcom/peel/util/ed;

    iget-object v2, p0, Lcom/peel/ui/bw;->ak:Lcom/peel/util/dz;

    iget-object v2, v2, Lcom/peel/util/dz;->a:Lcom/peel/util/ed;

    if-ne v0, v2, :cond_8

    .line 224
    invoke-direct {p0}, Lcom/peel/ui/bw;->S()V

    goto/16 :goto_0

    .line 226
    :cond_8
    const-class v0, Lcom/peel/ui/bw;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/peel/ui/bw;->m()Landroid/support/v4/app/ae;

    move-result-object v2

    invoke-static {v0, v2, v1}, Lcom/peel/util/bx;->a(Ljava/lang/String;Landroid/app/Activity;Z)V

    .line 227
    iget-object v0, p0, Lcom/peel/ui/bw;->ak:Lcom/peel/util/dz;

    invoke-virtual {v0}, Lcom/peel/util/dz;->a()V

    goto/16 :goto_0

    .line 193
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public d(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 131
    invoke-super {p0, p1}, Lcom/peel/d/u;->d(Landroid/os/Bundle;)V

    .line 133
    invoke-virtual {p0}, Lcom/peel/ui/bw;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/ae;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/ui/bw;->aj:Ljava/lang/String;

    .line 136
    if-eqz p1, :cond_0

    .line 137
    invoke-super {p0, p1}, Lcom/peel/d/u;->c(Landroid/os/Bundle;)V

    .line 140
    :cond_0
    invoke-virtual {p0}, Lcom/peel/ui/bw;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    invoke-static {v0}, Lcom/peel/d/e;->a(Landroid/support/v4/app/ae;)Lcom/peel/d/u;

    move-result-object v0

    .line 141
    if-eqz v0, :cond_1

    instance-of v0, v0, Lcom/peel/ui/lq;

    if-nez v0, :cond_2

    .line 153
    :cond_1
    :goto_0
    return-void

    .line 142
    :cond_2
    invoke-virtual {p0}, Lcom/peel/ui/bw;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    invoke-static {v0}, Lcom/peel/d/e;->a(Landroid/support/v4/app/ae;)Lcom/peel/d/u;

    move-result-object v0

    check-cast v0, Lcom/peel/ui/lq;

    invoke-virtual {v0}, Lcom/peel/ui/lq;->c()Lcom/peel/util/dz;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/ui/bw;->ak:Lcom/peel/util/dz;

    .line 143
    iget-object v0, p0, Lcom/peel/ui/bw;->ak:Lcom/peel/util/dz;

    if-eqz v0, :cond_3

    .line 144
    iget-object v0, p0, Lcom/peel/ui/bw;->ak:Lcom/peel/util/dz;

    invoke-direct {p0}, Lcom/peel/ui/bw;->T()Lcom/peel/util/s;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/peel/util/dz;->a(Lcom/peel/util/s;)V

    .line 147
    :cond_3
    sget-object v0, Lcom/peel/content/a;->h:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 149
    iget-object v0, p0, Lcom/peel/ui/bw;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "refresh"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 150
    iget-object v0, p0, Lcom/peel/ui/bw;->c:Landroid/os/Bundle;

    invoke-virtual {p0, v0}, Lcom/peel/ui/bw;->c(Landroid/os/Bundle;)V

    .line 152
    :cond_4
    iget-object v0, p0, Lcom/peel/ui/bw;->an:Lcom/peel/widget/ObservableScrollView;

    invoke-virtual {v0, p0}, Lcom/peel/widget/ObservableScrollView;->setOnScrollListener(Lcom/peel/widget/af;)V

    goto :goto_0
.end method

.method public e()V
    .locals 0

    .prologue
    .line 125
    invoke-super {p0}, Lcom/peel/d/u;->e()V

    .line 128
    return-void
.end method

.method public e(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 694
    iget-object v0, p0, Lcom/peel/ui/bw;->c:Landroid/os/Bundle;

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->putAll(Landroid/os/Bundle;)V

    .line 695
    invoke-super {p0, p1}, Lcom/peel/d/u;->e(Landroid/os/Bundle;)V

    .line 696
    return-void
.end method

.method public f()V
    .locals 0

    .prologue
    .line 118
    invoke-super {p0}, Lcom/peel/d/u;->f()V

    .line 121
    return-void
.end method

.method public g()V
    .locals 2

    .prologue
    .line 685
    iget-object v0, p0, Lcom/peel/ui/bw;->ak:Lcom/peel/util/dz;

    if-eqz v0, :cond_0

    .line 686
    iget-object v0, p0, Lcom/peel/ui/bw;->ak:Lcom/peel/util/dz;

    invoke-direct {p0}, Lcom/peel/ui/bw;->T()Lcom/peel/util/s;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/peel/util/dz;->b(Lcom/peel/util/s;)V

    .line 687
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/peel/ui/bw;->ak:Lcom/peel/util/dz;

    .line 690
    :cond_0
    invoke-super {p0}, Lcom/peel/d/u;->g()V

    .line 691
    return-void
.end method

.method public w()V
    .locals 4

    .prologue
    .line 638
    invoke-super {p0}, Lcom/peel/d/u;->w()V

    .line 639
    invoke-virtual {p0}, Lcom/peel/ui/bw;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    invoke-static {v0}, Landroid/support/v4/a/q;->a(Landroid/content/Context;)Landroid/support/v4/a/q;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/ui/bw;->au:Landroid/content/BroadcastReceiver;

    new-instance v2, Landroid/content/IntentFilter;

    const-string/jumbo v3, "reminder_updated"

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/a/q;->a(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)V

    .line 640
    return-void
.end method

.method public x()V
    .locals 2

    .prologue
    .line 632
    invoke-super {p0}, Lcom/peel/d/u;->x()V

    .line 633
    invoke-virtual {p0}, Lcom/peel/ui/bw;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    invoke-static {v0}, Landroid/support/v4/a/q;->a(Landroid/content/Context;)Landroid/support/v4/a/q;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/ui/bw;->au:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/support/v4/a/q;->a(Landroid/content/BroadcastReceiver;)V

    .line 634
    return-void
.end method
