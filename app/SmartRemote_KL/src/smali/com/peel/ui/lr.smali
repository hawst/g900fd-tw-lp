.class Lcom/peel/ui/lr;
.super Landroid/support/v4/view/cw;


# instance fields
.field final synthetic a:Lcom/peel/ui/lq;


# direct methods
.method constructor <init>(Lcom/peel/ui/lq;)V
    .locals 0

    .prologue
    .line 105
    iput-object p1, p0, Lcom/peel/ui/lr;->a:Lcom/peel/ui/lq;

    invoke-direct {p0}, Landroid/support/v4/view/cw;-><init>()V

    return-void
.end method


# virtual methods
.method public a(I)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    .line 108
    iget-object v0, p0, Lcom/peel/ui/lr;->a:Lcom/peel/ui/lq;

    invoke-static {v0, p1}, Lcom/peel/ui/lq;->a(Lcom/peel/ui/lq;I)I

    .line 110
    iget-object v0, p0, Lcom/peel/ui/lr;->a:Lcom/peel/ui/lq;

    invoke-static {v0}, Lcom/peel/ui/lq;->a(Lcom/peel/ui/lq;)Landroid/widget/PopupWindow;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/peel/ui/lr;->a:Lcom/peel/ui/lq;

    invoke-static {v0}, Lcom/peel/ui/lq;->a(Lcom/peel/ui/lq;)Landroid/widget/PopupWindow;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 111
    iget-object v0, p0, Lcom/peel/ui/lr;->a:Lcom/peel/ui/lq;

    invoke-static {v0}, Lcom/peel/ui/lq;->a(Lcom/peel/ui/lq;)Landroid/widget/PopupWindow;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->dismiss()V

    .line 113
    :cond_0
    iget-object v0, p0, Lcom/peel/ui/lr;->a:Lcom/peel/ui/lq;

    invoke-static {v0}, Lcom/peel/ui/lq;->b(Lcom/peel/ui/lq;)Lcom/peel/widget/CustomViewPager;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 114
    iget-object v0, p0, Lcom/peel/ui/lr;->a:Lcom/peel/ui/lq;

    invoke-static {v0}, Lcom/peel/ui/lq;->b(Lcom/peel/ui/lq;)Lcom/peel/widget/CustomViewPager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/widget/CustomViewPager;->getAdapter()Landroid/support/v4/view/av;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/support/v4/view/av;->b(I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    .line 116
    iget-object v0, p0, Lcom/peel/ui/lr;->a:Lcom/peel/ui/lq;

    invoke-static {v0}, Lcom/peel/ui/lq;->c(Lcom/peel/ui/lq;)V

    .line 118
    if-eqz v2, :cond_1

    .line 119
    invoke-static {}, Lcom/peel/util/a/f;->a()Lcom/peel/util/a/f;

    move-result-object v3

    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v0}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v0

    if-nez v0, :cond_3

    move v0, v1

    :goto_0
    const/16 v4, 0x3f8

    const/16 v5, 0x7d0

    invoke-virtual {v3, v0, v4, v5, v2}, Lcom/peel/util/a/f;->a(IIILjava/lang/String;)V

    .line 122
    :cond_1
    iget-object v0, p0, Lcom/peel/ui/lr;->a:Lcom/peel/ui/lq;

    iget-object v0, v0, Lcom/peel/ui/lq;->b:Lcom/peel/d/i;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/peel/ui/lr;->a:Lcom/peel/ui/lq;

    iget-object v0, v0, Lcom/peel/ui/lq;->b:Lcom/peel/d/i;

    iget-object v2, p0, Lcom/peel/ui/lr;->a:Lcom/peel/ui/lq;

    invoke-static {v2}, Lcom/peel/ui/lq;->d(Lcom/peel/ui/lq;)I

    move-result v2

    if-eqz v2, :cond_4

    :goto_1
    invoke-virtual {v0, v1}, Lcom/peel/d/i;->a(Z)V

    .line 123
    :cond_2
    return-void

    .line 119
    :cond_3
    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v0}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/data/at;->f()I

    move-result v0

    goto :goto_0

    .line 122
    :cond_4
    const/4 v1, 0x0

    goto :goto_1
.end method
