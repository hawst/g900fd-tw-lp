.class Lcom/peel/ui/mh;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/peel/ui/mg;


# direct methods
.method constructor <init>(Lcom/peel/ui/mg;)V
    .locals 0

    .prologue
    .line 455
    iput-object p1, p0, Lcom/peel/ui/mh;->a:Lcom/peel/ui/mg;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    .line 458
    iget-object v0, p0, Lcom/peel/ui/mh;->a:Lcom/peel/ui/mg;

    iget-object v0, v0, Lcom/peel/ui/mg;->a:Lcom/peel/ui/lq;

    iget-object v0, v0, Lcom/peel/ui/lq;->b:Lcom/peel/d/i;

    const-string/jumbo v1, "popuptoast"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/peel/d/i;->b(Ljava/lang/String;I)V

    .line 459
    iget-object v0, p0, Lcom/peel/ui/mh;->a:Lcom/peel/ui/mg;

    iget-object v0, v0, Lcom/peel/ui/mg;->a:Lcom/peel/ui/lq;

    invoke-static {v0}, Lcom/peel/ui/lq;->n(Lcom/peel/ui/lq;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v1, "close_toast_count"

    iget-object v2, p0, Lcom/peel/ui/mh;->a:Lcom/peel/ui/mg;

    iget-object v2, v2, Lcom/peel/ui/mg;->a:Lcom/peel/ui/lq;

    invoke-static {v2}, Lcom/peel/ui/lq;->n(Lcom/peel/ui/lq;)Landroid/content/SharedPreferences;

    move-result-object v2

    const-string/jumbo v3, "close_toast_count"

    const/4 v4, 0x0

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 461
    iget-object v0, p0, Lcom/peel/ui/mh;->a:Lcom/peel/ui/mg;

    iget-object v0, v0, Lcom/peel/ui/mg;->a:Lcom/peel/ui/lq;

    invoke-static {v0}, Lcom/peel/ui/lq;->a(Lcom/peel/ui/lq;)Landroid/widget/PopupWindow;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/peel/ui/mh;->a:Lcom/peel/ui/mg;

    iget-object v0, v0, Lcom/peel/ui/mg;->a:Lcom/peel/ui/lq;

    invoke-static {v0}, Lcom/peel/ui/lq;->a(Lcom/peel/ui/lq;)Landroid/widget/PopupWindow;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 462
    iget-object v0, p0, Lcom/peel/ui/mh;->a:Lcom/peel/ui/mg;

    iget-object v0, v0, Lcom/peel/ui/mg;->a:Lcom/peel/ui/lq;

    invoke-static {v0}, Lcom/peel/ui/lq;->a(Lcom/peel/ui/lq;)Landroid/widget/PopupWindow;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->dismiss()V

    .line 464
    :cond_0
    return-void
.end method
