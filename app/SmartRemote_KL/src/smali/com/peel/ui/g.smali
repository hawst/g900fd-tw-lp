.class Lcom/peel/ui/g;
.super Landroid/support/v4/view/cw;


# instance fields
.field final synthetic a:Lcom/peel/ui/f;


# direct methods
.method constructor <init>(Lcom/peel/ui/f;)V
    .locals 0

    .prologue
    .line 71
    iput-object p1, p0, Lcom/peel/ui/g;->a:Lcom/peel/ui/f;

    invoke-direct {p0}, Landroid/support/v4/view/cw;-><init>()V

    return-void
.end method


# virtual methods
.method public a(I)V
    .locals 2

    .prologue
    .line 73
    iget-object v0, p0, Lcom/peel/ui/g;->a:Lcom/peel/ui/f;

    invoke-static {v0, p1}, Lcom/peel/ui/f;->a(Lcom/peel/ui/f;I)I

    .line 74
    iget-object v0, p0, Lcom/peel/ui/g;->a:Lcom/peel/ui/f;

    invoke-static {v0}, Lcom/peel/ui/f;->b(Lcom/peel/ui/f;)Lcom/peel/ui/ey;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/ui/g;->a:Lcom/peel/ui/f;

    invoke-static {v1}, Lcom/peel/ui/f;->a(Lcom/peel/ui/f;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/peel/ui/ey;->a(I)V

    .line 76
    return-void
.end method

.method public b(I)V
    .locals 5

    .prologue
    .line 79
    const/4 v0, 0x1

    if-ne v0, p1, :cond_0

    iget-object v0, p0, Lcom/peel/ui/g;->a:Lcom/peel/ui/f;

    invoke-static {v0}, Lcom/peel/ui/f;->c(Lcom/peel/ui/f;)Landroid/support/v4/view/ViewPager;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 80
    iget-object v0, p0, Lcom/peel/ui/g;->a:Lcom/peel/ui/f;

    invoke-static {v0}, Lcom/peel/ui/f;->b(Lcom/peel/ui/f;)Lcom/peel/ui/ey;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 81
    iget-object v0, p0, Lcom/peel/ui/g;->a:Lcom/peel/ui/f;

    invoke-static {v0}, Lcom/peel/ui/f;->b(Lcom/peel/ui/f;)Lcom/peel/ui/ey;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/ui/ey;->e()Ljava/util/ArrayList;

    move-result-object v1

    .line 82
    iget-object v0, p0, Lcom/peel/ui/g;->a:Lcom/peel/ui/f;

    invoke-static {v0}, Lcom/peel/ui/f;->a(Lcom/peel/ui/f;)I

    move-result v0

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 83
    iget-object v0, p0, Lcom/peel/ui/g;->a:Lcom/peel/ui/f;

    invoke-static {v0}, Lcom/peel/ui/f;->a(Lcom/peel/ui/f;)I

    move-result v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 84
    if-nez v0, :cond_1

    .line 101
    :cond_0
    :goto_0
    return-void

    .line 86
    :cond_1
    sget v2, Lcom/peel/ui/fp;->epg_list:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    .line 87
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/widget/ListView;->getChildCount()I

    move-result v2

    if-eqz v2, :cond_0

    .line 90
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getTop()I

    move-result v2

    .line 91
    invoke-virtual {v0}, Landroid/widget/ListView;->getFirstVisiblePosition()I

    move-result v3

    .line 92
    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 93
    if-eqz v0, :cond_2

    .line 94
    sget v4, Lcom/peel/ui/fp;->epg_list:I

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    invoke-virtual {v0, v3, v2}, Landroid/widget/ListView;->setSelectionFromTop(II)V

    goto :goto_1

    .line 96
    :cond_3
    iget-object v0, p0, Lcom/peel/ui/g;->a:Lcom/peel/ui/f;

    iget-object v0, v0, Lcom/peel/ui/f;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "top"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 97
    iget-object v0, p0, Lcom/peel/ui/g;->a:Lcom/peel/ui/f;

    iget-object v0, v0, Lcom/peel/ui/f;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "first_visible"

    invoke-virtual {v0, v1, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    goto :goto_0
.end method
