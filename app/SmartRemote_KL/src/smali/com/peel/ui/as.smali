.class Lcom/peel/ui/as;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/text/InputFilter;


# instance fields
.field final synthetic a:Lcom/peel/ui/ar;


# direct methods
.method constructor <init>(Lcom/peel/ui/ar;)V
    .locals 0

    .prologue
    .line 64
    iput-object p1, p0, Lcom/peel/ui/as;->a:Lcom/peel/ui/ar;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public filter(Ljava/lang/CharSequence;IILandroid/text/Spanned;II)Ljava/lang/CharSequence;
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 69
    :try_start_0
    invoke-virtual {p4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2, p5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 70
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    .line 71
    invoke-virtual {p4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, p6}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    .line 72
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 73
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 74
    iget-object v2, p0, Lcom/peel/ui/as;->a:Lcom/peel/ui/ar;

    invoke-static {v2}, Lcom/peel/ui/ar;->a(Lcom/peel/ui/ar;)Z
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    if-eqz v2, :cond_1

    .line 75
    if-ltz v1, :cond_2

    const/16 v2, 0x17

    if-gt v1, v2, :cond_2

    .line 85
    :cond_0
    :goto_0
    return-object v0

    .line 79
    :cond_1
    const/4 v2, 0x1

    if-lt v1, v2, :cond_2

    const/16 v2, 0xc

    if-le v1, v2, :cond_0

    .line 85
    :cond_2
    :goto_1
    const-string/jumbo v0, ""

    goto :goto_0

    .line 83
    :catch_0
    move-exception v0

    goto :goto_1
.end method
