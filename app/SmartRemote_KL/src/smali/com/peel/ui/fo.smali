.class public final Lcom/peel/ui/fo;
.super Ljava/lang/Object;


# static fields
.field public static final ab_dvd_left_btn:I = 0x7f020000

.field public static final ab_dvd_left_btn_press:I = 0x7f020001

.field public static final ab_dvd_right_btn:I = 0x7f020002

.field public static final ab_dvd_right_btn_press:I = 0x7f020003

.field public static final ab_indeterminate_progress_bar:I = 0x7f020004

.field public static final ab_remote_back_btn:I = 0x7f020005

.field public static final ab_remote_back_btn_press:I = 0x7f020006

.field public static final ab_remote_ch_minus_btn_normal:I = 0x7f020007

.field public static final ab_remote_ch_minus_btn_press:I = 0x7f020008

.field public static final ab_remote_ch_plus_btn_normal:I = 0x7f020009

.field public static final ab_remote_ch_plus_btn_press:I = 0x7f02000a

.field public static final ab_remote_fwd_btn:I = 0x7f02000b

.field public static final ab_remote_fwd_btn_press:I = 0x7f02000c

.field public static final ab_remote_keypad_btn_normal:I = 0x7f02000d

.field public static final ab_remote_keypad_btn_press:I = 0x7f02000e

.field public static final ab_remote_mute_btn_normal:I = 0x7f02000f

.field public static final ab_remote_mute_btn_press:I = 0x7f020010

.field public static final ab_remote_playpause_btn:I = 0x7f020011

.field public static final ab_remote_playpause_btn_press:I = 0x7f020012

.field public static final ab_remote_vol_minus_btn_normal:I = 0x7f020013

.field public static final ab_remote_vol_minus_btn_press:I = 0x7f020014

.field public static final ab_remote_vol_plus_btn_normal:I = 0x7f020015

.field public static final ab_remote_vol_plus_btn_press:I = 0x7f020016

.field public static final ab_roku_back_btn:I = 0x7f020017

.field public static final ab_roku_back_btn_press:I = 0x7f020018

.field public static final ab_roku_home_btn:I = 0x7f020019

.field public static final ab_roku_home_btn_press:I = 0x7f02001a

.field public static final ab_roku_reflash_btn:I = 0x7f02001b

.field public static final ab_roku_reflash_btn_press:I = 0x7f02001c

.field public static final ab_roku_star_btn:I = 0x7f02001d

.field public static final ab_roku_star_btn_press:I = 0x7f02001e

.field public static final ab_sub_remote_btn_normal:I = 0x7f02001f

.field public static final ab_sub_remote_btn_press:I = 0x7f020020

.field public static final abc_ab_share_pack_holo_dark:I = 0x7f020021

.field public static final abc_ab_share_pack_holo_light:I = 0x7f020022

.field public static final abc_btn_check_material:I = 0x7f020023

.field public static final abc_btn_check_to_on_mtrl_000:I = 0x7f020024

.field public static final abc_btn_check_to_on_mtrl_015:I = 0x7f020025

.field public static final abc_btn_radio_material:I = 0x7f020026

.field public static final abc_btn_radio_to_on_mtrl_000:I = 0x7f020027

.field public static final abc_btn_radio_to_on_mtrl_015:I = 0x7f020028

.field public static final abc_btn_switch_to_on_mtrl_00001:I = 0x7f020029

.field public static final abc_btn_switch_to_on_mtrl_00012:I = 0x7f02002a

.field public static final abc_cab_background_internal_bg:I = 0x7f02002b

.field public static final abc_cab_background_top_material:I = 0x7f02002c

.field public static final abc_cab_background_top_mtrl_alpha:I = 0x7f02002d

.field public static final abc_edit_text_material:I = 0x7f02002e

.field public static final abc_ic_ab_back_mtrl_am_alpha:I = 0x7f02002f

.field public static final abc_ic_clear_mtrl_alpha:I = 0x7f020030

.field public static final abc_ic_commit_search_api_mtrl_alpha:I = 0x7f020031

.field public static final abc_ic_go_search_api_mtrl_alpha:I = 0x7f020032

.field public static final abc_ic_menu_copy_mtrl_am_alpha:I = 0x7f020033

.field public static final abc_ic_menu_cut_mtrl_alpha:I = 0x7f020034

.field public static final abc_ic_menu_moreoverflow_mtrl_alpha:I = 0x7f020035

.field public static final abc_ic_menu_paste_mtrl_am_alpha:I = 0x7f020036

.field public static final abc_ic_menu_selectall_mtrl_alpha:I = 0x7f020037

.field public static final abc_ic_menu_share_mtrl_alpha:I = 0x7f020038

.field public static final abc_ic_search:I = 0x7f020039

.field public static final abc_ic_search_api_mtrl_alpha:I = 0x7f02003a

.field public static final abc_ic_voice_search_api_mtrl_alpha:I = 0x7f02003b

.field public static final abc_item_background_holo_dark:I = 0x7f02003c

.field public static final abc_item_background_holo_light:I = 0x7f02003d

.field public static final abc_list_divider_mtrl_alpha:I = 0x7f02003e

.field public static final abc_list_focused_holo:I = 0x7f02003f

.field public static final abc_list_longpressed_holo:I = 0x7f020040

.field public static final abc_list_pressed_holo_dark:I = 0x7f020041

.field public static final abc_list_pressed_holo_light:I = 0x7f020042

.field public static final abc_list_selector_background_transition_holo_dark:I = 0x7f020043

.field public static final abc_list_selector_background_transition_holo_light:I = 0x7f020044

.field public static final abc_list_selector_disabled_holo_dark:I = 0x7f020045

.field public static final abc_list_selector_disabled_holo_light:I = 0x7f020046

.field public static final abc_list_selector_holo_dark:I = 0x7f020047

.field public static final abc_list_selector_holo_light:I = 0x7f020048

.field public static final abc_menu_hardkey_panel_mtrl_mult:I = 0x7f020049

.field public static final abc_popup_background_mtrl_mult:I = 0x7f02004a

.field public static final abc_spinner_mtrl_am_alpha:I = 0x7f02004b

.field public static final abc_switch_thumb_material:I = 0x7f02004c

.field public static final abc_switch_track_mtrl_alpha:I = 0x7f02004d

.field public static final abc_tab_indicator_material:I = 0x7f02004e

.field public static final abc_tab_indicator_mtrl_alpha:I = 0x7f02004f

.field public static final abc_textfield_activated_mtrl_alpha:I = 0x7f020050

.field public static final abc_textfield_default_mtrl_alpha:I = 0x7f020051

.field public static final abc_textfield_search_activated_mtrl_alpha:I = 0x7f020052

.field public static final abc_textfield_search_default_mtrl_alpha:I = 0x7f020053

.field public static final abc_textfield_search_material:I = 0x7f020054

.field public static final ac_down_btn_normal:I = 0x7f020055

.field public static final ac_down_btn_press:I = 0x7f020056

.field public static final ac_minus_btn_normal:I = 0x7f020057

.field public static final ac_minus_btn_press:I = 0x7f020058

.field public static final ac_mode_btn_normal:I = 0x7f020059

.field public static final ac_mode_btn_press:I = 0x7f02005a

.field public static final ac_mode_icon:I = 0x7f02005b

.field public static final ac_plus_btn_normal:I = 0x7f02005c

.field public static final ac_plus_btn_press:I = 0x7f02005d

.field public static final ac_remote_down_btn:I = 0x7f02005e

.field public static final ac_remote_down_btn_press:I = 0x7f02005f

.field public static final ac_remote_up_btn:I = 0x7f020060

.field public static final ac_remote_up_btn_press:I = 0x7f020061

.field public static final ac_up_btn_normal:I = 0x7f020062

.field public static final ac_up_btn_press:I = 0x7f020063

.field public static final action_bar_background:I = 0x7f020064

.field public static final action_bar_bg:I = 0x7f020065

.field public static final action_bar_epg_btn_normal:I = 0x7f020066

.field public static final action_bar_epg_btn_pressed:I = 0x7f020067

.field public static final action_bar_menu_icon_stateful:I = 0x7f020068

.field public static final action_bar_more_btn_normal:I = 0x7f020069

.field public static final action_bar_more_btn_press:I = 0x7f02006a

.field public static final action_bar_power_1more_btn:I = 0x7f02006b

.field public static final action_bar_power_btn:I = 0x7f02006c

.field public static final action_bar_power_btn_press:I = 0x7f02006d

.field public static final action_bar_setting_btn_normal:I = 0x7f02006e

.field public static final action_bar_setting_btn_press:I = 0x7f02006f

.field public static final action_check_btn_normal:I = 0x7f020070

.field public static final action_check_btn_press:I = 0x7f020071

.field public static final action_check_btn_stateful:I = 0x7f020072

.field public static final action_power_stateful:I = 0x7f020073

.field public static final action_remote_btn_normal:I = 0x7f020074

.field public static final action_remote_btn_press:I = 0x7f020075

.field public static final action_room_change_btn_stateful:I = 0x7f020076

.field public static final action_search:I = 0x7f020736

.field public static final action_send_btn_normal:I = 0x7f020077

.field public static final action_send_btn_press:I = 0x7f020078

.field public static final action_send_btn_stateful:I = 0x7f020079

.field public static final action_setting_btn_stateful:I = 0x7f02007a

.field public static final action_time_btn_normal:I = 0x7f02007b

.field public static final action_time_btn_stateful:I = 0x7f02007c

.field public static final action_trash_btn_normal:I = 0x7f02007d

.field public static final action_trash_btn_press:I = 0x7f02007e

.field public static final action_trash_stateful:I = 0x7f02007f

.field public static final actionbar_left:I = 0x7f020080

.field public static final actionbar_right:I = 0x7f020081

.field public static final actionbar_tab_bg_left_select:I = 0x7f020082

.field public static final actionbar_tab_bg_right:I = 0x7f020083

.field public static final actionbar_tab_bg_right_press:I = 0x7f020084

.field public static final actionbar_tab_bg_right_select:I = 0x7f020085

.field public static final actionbar_tab_bg_right_stateful:I = 0x7f020086

.field public static final ad_close_normal:I = 0x7f020087

.field public static final ad_close_pressed:I = 0x7f020088

.field public static final ad_video_close_normal:I = 0x7f020089

.field public static final ad_video_close_pressed:I = 0x7f02008a

.field public static final ad_video_mute_normal:I = 0x7f02008b

.field public static final ad_video_mute_pressed:I = 0x7f02008c

.field public static final ad_video_vol_normal:I = 0x7f02008d

.field public static final ad_video_vol_pressed:I = 0x7f02008e

.field public static final add_icon_normal:I = 0x7f02008f

.field public static final add_icon_press:I = 0x7f020090

.field public static final add_icon_states:I = 0x7f020091

.field public static final add_roku_device_01:I = 0x7f020092

.field public static final add_roku_device_02:I = 0x7f020093

.field public static final add_roku_device_03:I = 0x7f020094

.field public static final add_roku_tryagain_icon:I = 0x7f020095

.field public static final age_gender_stateful:I = 0x7f020096

.field public static final always_widget_bg:I = 0x7f020097

.field public static final always_widget_bg_gradient:I = 0x7f020098

.field public static final always_widget_handle_normal:I = 0x7f020099

.field public static final always_widget_handle_select:I = 0x7f02009a

.field public static final app_background:I = 0x7f020737

.field public static final app_icon:I = 0x7f02009b

.field public static final av_btn_stateful:I = 0x7f02009c

.field public static final before_bg_01:I = 0x7f02009d

.field public static final bgtoggle:I = 0x7f02009e

.field public static final bigbang:I = 0x7f02009f

.field public static final blue_btn_stateful:I = 0x7f0200a0

.field public static final browse_color:I = 0x7f020738

.field public static final browse_selector:I = 0x7f0200a1

.field public static final btn1_a_stateful:I = 0x7f0200a2

.field public static final btn1_b_stateful:I = 0x7f0200a3

.field public static final btn1_c_stateful:I = 0x7f0200a4

.field public static final btn1_d_stateful:I = 0x7f0200a5

.field public static final btn2_a_stateful:I = 0x7f0200a6

.field public static final btn2_b_stateful:I = 0x7f0200a7

.field public static final btn2_c_stateful:I = 0x7f0200a8

.field public static final btn2_d_stateful:I = 0x7f0200a9

.field public static final btn3_white_stateful:I = 0x7f0200aa

.field public static final btn4_a_stateful:I = 0x7f0200ab

.field public static final btn4_b_stateful:I = 0x7f0200ac

.field public static final btn4_c_stateful:I = 0x7f0200ad

.field public static final btn4_d_stateful:I = 0x7f0200ae

.field public static final btn6_active:I = 0x7f0200af

.field public static final btn6_active_press:I = 0x7f0200b0

.field public static final btn6_audio:I = 0x7f0200b1

.field public static final btn6_audio_press:I = 0x7f0200b2

.field public static final btn6_av_mode:I = 0x7f0200b3

.field public static final btn6_av_mode_press:I = 0x7f0200b4

.field public static final btn6_av_stateful:I = 0x7f0200b5

.field public static final btn6_back:I = 0x7f0200b6

.field public static final btn6_back_press:I = 0x7f0200b7

.field public static final btn6_back_stateful:I = 0x7f0200b8

.field public static final btn6_bg:I = 0x7f0200b9

.field public static final btn6_bg_press:I = 0x7f0200ba

.field public static final btn6_dvr:I = 0x7f0200bb

.field public static final btn6_dvr_press:I = 0x7f0200bc

.field public static final btn6_dvr_stateful:I = 0x7f0200bd

.field public static final btn6_esc:I = 0x7f0200be

.field public static final btn6_esc_press:I = 0x7f0200bf

.field public static final btn6_esc_stateful:I = 0x7f0200c0

.field public static final btn6_exit:I = 0x7f0200c1

.field public static final btn6_exit_press:I = 0x7f0200c2

.field public static final btn6_exit_stateful:I = 0x7f0200c3

.field public static final btn6_fav:I = 0x7f0200c4

.field public static final btn6_fav_press:I = 0x7f0200c5

.field public static final btn6_fav_stateful:I = 0x7f0200c6

.field public static final btn6_flash_back:I = 0x7f0200c7

.field public static final btn6_flash_back_press:I = 0x7f0200c8

.field public static final btn6_flash_back_stateful:I = 0x7f0200c9

.field public static final btn6_guide:I = 0x7f0200ca

.field public static final btn6_guide_press:I = 0x7f0200cb

.field public static final btn6_guide_stateful:I = 0x7f0200cc

.field public static final btn6_home:I = 0x7f0200cd

.field public static final btn6_home_press:I = 0x7f0200ce

.field public static final btn6_home_stateful:I = 0x7f0200cf

.field public static final btn6_info:I = 0x7f0200d0

.field public static final btn6_info_press:I = 0x7f0200d1

.field public static final btn6_info_stateful:I = 0x7f0200d2

.field public static final btn6_last:I = 0x7f0200d3

.field public static final btn6_last_press:I = 0x7f0200d4

.field public static final btn6_last_stateful:I = 0x7f0200d5

.field public static final btn6_list:I = 0x7f0200d6

.field public static final btn6_list_press:I = 0x7f0200d7

.field public static final btn6_list_stateful:I = 0x7f0200d8

.field public static final btn6_livetv:I = 0x7f0200d9

.field public static final btn6_livetv_press:I = 0x7f0200da

.field public static final btn6_livetv_stateful:I = 0x7f0200db

.field public static final btn6_menu:I = 0x7f0200dc

.field public static final btn6_menu_press:I = 0x7f0200dd

.field public static final btn6_menu_stateful:I = 0x7f0200de

.field public static final btn6_ondemand:I = 0x7f0200df

.field public static final btn6_ondemand_press:I = 0x7f0200e0

.field public static final btn6_ondemand_stateful:I = 0x7f0200e1

.field public static final btn6_option:I = 0x7f0200e2

.field public static final btn6_option_press:I = 0x7f0200e3

.field public static final btn6_option_stateful:I = 0x7f0200e4

.field public static final btn6_popup_menu:I = 0x7f0200e5

.field public static final btn6_popup_menu_press:I = 0x7f0200e6

.field public static final btn6_popup_menu_stateful:I = 0x7f0200e7

.field public static final btn6_recall:I = 0x7f0200e8

.field public static final btn6_recall_press:I = 0x7f0200e9

.field public static final btn6_recall_stateful:I = 0x7f0200ea

.field public static final btn6_return:I = 0x7f0200eb

.field public static final btn6_return_press:I = 0x7f0200ec

.field public static final btn6_return_stateful:I = 0x7f0200ed

.field public static final btn6_roku_star:I = 0x7f0200ee

.field public static final btn6_roku_star_press:I = 0x7f0200ef

.field public static final btn6_roku_star_stateful:I = 0x7f0200f0

.field public static final btn6_samsung_link:I = 0x7f0200f1

.field public static final btn6_samsung_link_press:I = 0x7f0200f2

.field public static final btn6_samsung_link_stateful:I = 0x7f0200f3

.field public static final btn6_skip_back:I = 0x7f0200f4

.field public static final btn6_skip_back_press:I = 0x7f0200f5

.field public static final btn6_skip_back_stateful:I = 0x7f0200f6

.field public static final btn6_skip_forward:I = 0x7f0200f7

.field public static final btn6_skip_forward_press:I = 0x7f0200f8

.field public static final btn6_skip_forward_stateful:I = 0x7f0200f9

.field public static final btn6_slow:I = 0x7f0200fa

.field public static final btn6_slow_press:I = 0x7f0200fb

.field public static final btn6_slow_stateful:I = 0x7f0200fc

.field public static final btn6_source:I = 0x7f0200fd

.field public static final btn6_source_press:I = 0x7f0200fe

.field public static final btn6_source_stateful:I = 0x7f0200ff

.field public static final btn6_star:I = 0x7f020100

.field public static final btn6_star_press:I = 0x7f020101

.field public static final btn6_star_stateful:I = 0x7f020102

.field public static final btn6_sync_menu:I = 0x7f020103

.field public static final btn6_sync_menu_press:I = 0x7f020104

.field public static final btn6_sync_menu_stateful:I = 0x7f020105

.field public static final btn6_thumbs_down:I = 0x7f020106

.field public static final btn6_thumbs_down_press:I = 0x7f020107

.field public static final btn6_thumbs_down_stateful:I = 0x7f020108

.field public static final btn6_thumbs_up:I = 0x7f020109

.field public static final btn6_thumbs_up_press:I = 0x7f02010a

.field public static final btn6_thumbs_up_stateful:I = 0x7f02010b

.field public static final btn6_tivo:I = 0x7f02010c

.field public static final btn6_tivo_press:I = 0x7f02010d

.field public static final btn6_tivo_stateful:I = 0x7f02010e

.field public static final btn_01_box_a:I = 0x7f02010f

.field public static final btn_01_box_a_press:I = 0x7f020110

.field public static final btn_01_box_b:I = 0x7f020111

.field public static final btn_01_box_b_press:I = 0x7f020112

.field public static final btn_01_box_c:I = 0x7f020113

.field public static final btn_01_box_c_press:I = 0x7f020114

.field public static final btn_01_box_d:I = 0x7f020115

.field public static final btn_01_box_d_press:I = 0x7f020116

.field public static final btn_02_box_a:I = 0x7f020117

.field public static final btn_02_box_a_press:I = 0x7f020118

.field public static final btn_02_box_b:I = 0x7f020119

.field public static final btn_02_box_b_press:I = 0x7f02011a

.field public static final btn_02_box_c:I = 0x7f02011b

.field public static final btn_02_box_c_press:I = 0x7f02011c

.field public static final btn_02_box_d:I = 0x7f02011d

.field public static final btn_02_box_d_press:I = 0x7f02011e

.field public static final btn_03_box_white:I = 0x7f02011f

.field public static final btn_03_box_white_press:I = 0x7f020120

.field public static final btn_04_box_a:I = 0x7f020121

.field public static final btn_04_box_a_press:I = 0x7f020122

.field public static final btn_04_box_b:I = 0x7f020123

.field public static final btn_04_box_b_press:I = 0x7f020124

.field public static final btn_04_box_c:I = 0x7f020125

.field public static final btn_04_box_c_press:I = 0x7f020126

.field public static final btn_04_box_d:I = 0x7f020127

.field public static final btn_04_box_d_press:I = 0x7f020128

.field public static final btn_a_ff:I = 0x7f020129

.field public static final btn_a_ff_press:I = 0x7f02012a

.field public static final btn_a_ff_stateful:I = 0x7f02012b

.field public static final btn_a_rewind:I = 0x7f02012c

.field public static final btn_a_rewind_press:I = 0x7f02012d

.field public static final btn_a_rewind_stateful:I = 0x7f02012e

.field public static final btn_ad_close:I = 0x7f02012f

.field public static final btn_ad_video_close_stateful:I = 0x7f020130

.field public static final btn_ad_video_mute_stateful:I = 0x7f020131

.field public static final btn_ad_video_vol_stateful:I = 0x7f020132

.field public static final btn_b_ff:I = 0x7f020133

.field public static final btn_b_ff_press:I = 0x7f020134

.field public static final btn_b_next:I = 0x7f020135

.field public static final btn_b_next_press:I = 0x7f020136

.field public static final btn_b_play_pause:I = 0x7f020137

.field public static final btn_b_play_pause_press:I = 0x7f020138

.field public static final btn_b_playpause_states:I = 0x7f020139

.field public static final btn_b_prev:I = 0x7f02013a

.field public static final btn_b_prev_press:I = 0x7f02013b

.field public static final btn_b_rewind:I = 0x7f02013c

.field public static final btn_b_rewind_press:I = 0x7f02013d

.field public static final btn_back:I = 0x7f02013e

.field public static final btn_back_press:I = 0x7f02013f

.field public static final btn_back_stateful:I = 0x7f020140

.field public static final btn_bottom:I = 0x7f020141

.field public static final btn_bottom_press:I = 0x7f020142

.field public static final btn_box_a:I = 0x7f020143

.field public static final btn_box_a_press:I = 0x7f020144

.field public static final btn_box_b:I = 0x7f020145

.field public static final btn_box_b_press:I = 0x7f020146

.field public static final btn_box_c:I = 0x7f020147

.field public static final btn_box_c_press:I = 0x7f020148

.field public static final btn_box_d:I = 0x7f020149

.field public static final btn_box_d_press:I = 0x7f02014a

.field public static final btn_checkbox_off_states:I = 0x7f02014b

.field public static final btn_checkbox_on_states:I = 0x7f02014c

.field public static final btn_checkbox_stateful:I = 0x7f02014d

.field public static final btn_color_27505e:I = 0x7f02014e

.field public static final btn_direction:I = 0x7f02014f

.field public static final btn_direction_press:I = 0x7f020150

.field public static final btn_direction_stateful:I = 0x7f020151

.field public static final btn_fan:I = 0x7f020152

.field public static final btn_fan_down:I = 0x7f020153

.field public static final btn_fan_down_press:I = 0x7f020154

.field public static final btn_fan_up:I = 0x7f020155

.field public static final btn_fan_up_press:I = 0x7f020156

.field public static final btn_fanspeed_down:I = 0x7f020157

.field public static final btn_fanspeed_down_press:I = 0x7f020158

.field public static final btn_fanspeed_up:I = 0x7f020159

.field public static final btn_fanspeed_up_press:I = 0x7f02015a

.field public static final btn_handle_flipper:I = 0x7f02015b

.field public static final btn_home:I = 0x7f02015c

.field public static final btn_home_press:I = 0x7f02015d

.field public static final btn_home_stateful:I = 0x7f02015e

.field public static final btn_keypad:I = 0x7f02015f

.field public static final btn_keypad_press:I = 0x7f020160

.field public static final btn_keypad_stateful:I = 0x7f020161

.field public static final btn_left:I = 0x7f020162

.field public static final btn_left_press:I = 0x7f020163

.field public static final btn_m_ff:I = 0x7f020164

.field public static final btn_m_ff_press:I = 0x7f020165

.field public static final btn_m_ff_stateful:I = 0x7f020166

.field public static final btn_m_next:I = 0x7f020167

.field public static final btn_m_next_press:I = 0x7f020168

.field public static final btn_m_next_stateful:I = 0x7f020169

.field public static final btn_m_play_pause:I = 0x7f02016a

.field public static final btn_m_play_pause_press:I = 0x7f02016b

.field public static final btn_m_play_pause_stateful:I = 0x7f02016c

.field public static final btn_m_prev:I = 0x7f02016d

.field public static final btn_m_prev_press:I = 0x7f02016e

.field public static final btn_m_prev_stateful:I = 0x7f02016f

.field public static final btn_m_rewind:I = 0x7f020170

.field public static final btn_m_rewind_press:I = 0x7f020171

.field public static final btn_m_rewind_stateful:I = 0x7f020172

.field public static final btn_m_stop:I = 0x7f020173

.field public static final btn_m_stop_press:I = 0x7f020174

.field public static final btn_m_stop_stateful:I = 0x7f020175

.field public static final btn_mode_small:I = 0x7f020176

.field public static final btn_mode_small_press:I = 0x7f020177

.field public static final btn_mode_stateful:I = 0x7f020178

.field public static final btn_mute:I = 0x7f020179

.field public static final btn_mute_press:I = 0x7f02017a

.field public static final btn_mute_select:I = 0x7f02017b

.field public static final btn_noitem_list:I = 0x7f02017c

.field public static final btn_ok:I = 0x7f02017d

.field public static final btn_ok_bg:I = 0x7f02017e

.field public static final btn_ok_press:I = 0x7f02017f

.field public static final btn_onoff:I = 0x7f020180

.field public static final btn_onoff_press:I = 0x7f020181

.field public static final btn_onoff_states:I = 0x7f020182

.field public static final btn_play_pause:I = 0x7f020183

.field public static final btn_play_pause_press:I = 0x7f020184

.field public static final btn_playpause_stateful:I = 0x7f020185

.field public static final btn_regular_disabled:I = 0x7f020186

.field public static final btn_regular_normal:I = 0x7f020187

.field public static final btn_regular_pressed:I = 0x7f020188

.field public static final btn_right:I = 0x7f020189

.field public static final btn_right_press:I = 0x7f02018a

.field public static final btn_s_play_pause_press:I = 0x7f02018b

.field public static final btn_s_play_pausel:I = 0x7f02018c

.field public static final btn_source_small:I = 0x7f02018d

.field public static final btn_source_small_press:I = 0x7f02018e

.field public static final btn_sso_facebook:I = 0x7f02018f

.field public static final btn_sso_gplus:I = 0x7f020190

.field public static final btn_sso_samsung:I = 0x7f020191

.field public static final btn_stateful:I = 0x7f020192

.field public static final btn_stateful_27505e:I = 0x7f020193

.field public static final btn_stateful_blue:I = 0x7f020194

.field public static final btn_stateful_gray:I = 0x7f020195

.field public static final btn_temp:I = 0x7f020196

.field public static final btn_temp_full_minus_press:I = 0x7f020197

.field public static final btn_temp_full_plus_press:I = 0x7f020198

.field public static final btn_temp_minus:I = 0x7f020199

.field public static final btn_temp_minus_press:I = 0x7f02019a

.field public static final btn_temp_minus_stateful:I = 0x7f02019b

.field public static final btn_temp_plus:I = 0x7f02019c

.field public static final btn_temp_plus_press:I = 0x7f02019d

.field public static final btn_temp_plus_stateful:I = 0x7f02019e

.field public static final btn_top:I = 0x7f02019f

.field public static final btn_top_press:I = 0x7f0201a0

.field public static final btn_tv:I = 0x7f0201a1

.field public static final btn_tv_press:I = 0x7f0201a2

.field public static final btn_white_checkbox_states:I = 0x7f0201a3

.field public static final button_back_stateful:I = 0x7f0201a4

.field public static final button_box_a_red_stateful:I = 0x7f0201a5

.field public static final button_box_b_green_stateful:I = 0x7f0201a6

.field public static final button_box_c_yellow_stateful:I = 0x7f0201a7

.field public static final button_box_d_blue_stateful:I = 0x7f0201a8

.field public static final button_down_stateful:I = 0x7f0201a9

.field public static final button_info_stateful:I = 0x7f0201aa

.field public static final button_keypad_2_ff_stateful:I = 0x7f0201ab

.field public static final button_keypad_2_next_stateful:I = 0x7f0201ac

.field public static final button_keypad_2_pause_stateful:I = 0x7f0201ad

.field public static final button_keypad_2_play_stateful:I = 0x7f0201ae

.field public static final button_keypad_2_prev_stateful:I = 0x7f0201af

.field public static final button_keypad_2_rewind_stateful:I = 0x7f0201b0

.field public static final button_keypad_2_stop_stateful:I = 0x7f0201b1

.field public static final button_keypad_4_next_stateful:I = 0x7f0201b2

.field public static final button_keypad_4_prev_stateful:I = 0x7f0201b3

.field public static final button_keypad_4_rec_stateful:I = 0x7f0201b4

.field public static final button_keypad_4_stop_stateful:I = 0x7f0201b5

.field public static final button_keypad_ff_stateful:I = 0x7f0201b6

.field public static final button_keypad_next_stateful:I = 0x7f0201b7

.field public static final button_keypad_pause_stateful:I = 0x7f0201b8

.field public static final button_keypad_play_stateful:I = 0x7f0201b9

.field public static final button_keypad_prev_stateful:I = 0x7f0201ba

.field public static final button_keypad_rec_stateful:I = 0x7f0201bb

.field public static final button_keypad_rewind_stateful:I = 0x7f0201bc

.field public static final button_keypad_stop_stateful:I = 0x7f0201bd

.field public static final button_left_stateful:I = 0x7f0201be

.field public static final button_menu_stateful:I = 0x7f0201bf

.field public static final button_ok_stateful:I = 0x7f0201c0

.field public static final button_right_stateful:I = 0x7f0201c1

.field public static final button_up_stateful:I = 0x7f0201c2

.field public static final camera_icon_normal:I = 0x7f0201c3

.field public static final camera_icon_press:I = 0x7f0201c4

.field public static final camera_icon_stateful:I = 0x7f0201c5

.field public static final card_view_menu_dropdown_icon:I = 0x7f0201c6

.field public static final carrot_icon_normal:I = 0x7f0201c7

.field public static final carrot_icon_stateful:I = 0x7f0201c8

.field public static final ch_001:I = 0x7f0201c9

.field public static final ch_015:I = 0x7f0201ca

.field public static final ch_029:I = 0x7f0201cb

.field public static final ch_bg_normal:I = 0x7f0201cc

.field public static final ch_bg_press:I = 0x7f0201cd

.field public static final ch_common_btn_normal:I = 0x7f0201ce

.field public static final ch_common_btn_press:I = 0x7f0201cf

.field public static final ch_down_btn_normal:I = 0x7f0201d0

.field public static final ch_down_btn_normal_bg:I = 0x7f0201d1

.field public static final ch_down_btn_press:I = 0x7f0201d2

.field public static final ch_down_btn_press_bg:I = 0x7f0201d3

.field public static final ch_guide_coachmark_bg:I = 0x7f0201d4

.field public static final ch_guide_handle_normal:I = 0x7f0201d5

.field public static final ch_guide_handle_press:I = 0x7f0201d6

.field public static final ch_guide_time_divider:I = 0x7f0201d7

.field public static final ch_left_btn_normal:I = 0x7f0201d8

.field public static final ch_left_btn_press:I = 0x7f0201d9

.field public static final ch_plus_btn_normal:I = 0x7f0201da

.field public static final ch_plus_btn_press:I = 0x7f0201db

.field public static final ch_right_btn_normal:I = 0x7f0201dc

.field public static final ch_right_btn_press:I = 0x7f0201dd

.field public static final ch_up_btn_normal:I = 0x7f0201de

.field public static final ch_up_btn_normal_bg:I = 0x7f0201df

.field public static final ch_up_btn_press:I = 0x7f0201e0

.field public static final ch_up_btn_press_bg:I = 0x7f0201e1

.field public static final change_remote:I = 0x7f0201e2

.field public static final change_remote_bg:I = 0x7f0201e3

.field public static final change_remote_bg_stateful:I = 0x7f0201e4

.field public static final change_remote_press:I = 0x7f0201e5

.field public static final change_room_bottom_list_bg:I = 0x7f0201e6

.field public static final change_room_plus_icon:I = 0x7f0201e7

.field public static final channel_arrow:I = 0x7f0201e8

.field public static final channel_dn:I = 0x7f0201e9

.field public static final channel_up:I = 0x7f0201ea

.field public static final com_facebook_button_blue:I = 0x7f0201eb

.field public static final com_facebook_button_blue_focused:I = 0x7f0201ec

.field public static final com_facebook_button_blue_normal:I = 0x7f0201ed

.field public static final com_facebook_button_blue_pressed:I = 0x7f0201ee

.field public static final com_facebook_button_check:I = 0x7f0201ef

.field public static final com_facebook_button_check_off:I = 0x7f0201f0

.field public static final com_facebook_button_check_on:I = 0x7f0201f1

.field public static final com_facebook_button_grey_focused:I = 0x7f0201f2

.field public static final com_facebook_button_grey_normal:I = 0x7f0201f3

.field public static final com_facebook_button_grey_pressed:I = 0x7f0201f4

.field public static final com_facebook_button_like:I = 0x7f0201f5

.field public static final com_facebook_button_like_background:I = 0x7f0201f6

.field public static final com_facebook_button_like_background_selected:I = 0x7f0201f7

.field public static final com_facebook_button_like_icon:I = 0x7f0201f8

.field public static final com_facebook_button_like_icon_selected:I = 0x7f0201f9

.field public static final com_facebook_button_like_pressed:I = 0x7f0201fa

.field public static final com_facebook_button_like_selected:I = 0x7f0201fb

.field public static final com_facebook_close:I = 0x7f0201fc

.field public static final com_facebook_inverse_icon:I = 0x7f0201fd

.field public static final com_facebook_list_divider:I = 0x7f0201fe

.field public static final com_facebook_list_section_header_background:I = 0x7f0201ff

.field public static final com_facebook_loginbutton_silver:I = 0x7f020200

.field public static final com_facebook_logo:I = 0x7f020201

.field public static final com_facebook_picker_default_separator_color:I = 0x7f020739

.field public static final com_facebook_picker_item_background:I = 0x7f020202

.field public static final com_facebook_picker_list_focused:I = 0x7f020203

.field public static final com_facebook_picker_list_longpressed:I = 0x7f020204

.field public static final com_facebook_picker_list_pressed:I = 0x7f020205

.field public static final com_facebook_picker_list_selector:I = 0x7f020206

.field public static final com_facebook_picker_list_selector_background_transition:I = 0x7f020207

.field public static final com_facebook_picker_list_selector_disabled:I = 0x7f020208

.field public static final com_facebook_picker_magnifier:I = 0x7f020209

.field public static final com_facebook_picker_top_button:I = 0x7f02020a

.field public static final com_facebook_place_default_icon:I = 0x7f02020b

.field public static final com_facebook_profile_default_icon:I = 0x7f02020c

.field public static final com_facebook_profile_picture_blank_portrait:I = 0x7f02020d

.field public static final com_facebook_profile_picture_blank_square:I = 0x7f02020e

.field public static final com_facebook_tooltip_black_background:I = 0x7f02020f

.field public static final com_facebook_tooltip_black_bottomnub:I = 0x7f020210

.field public static final com_facebook_tooltip_black_topnub:I = 0x7f020211

.field public static final com_facebook_tooltip_black_xout:I = 0x7f020212

.field public static final com_facebook_tooltip_blue_background:I = 0x7f020213

.field public static final com_facebook_tooltip_blue_bottomnub:I = 0x7f020214

.field public static final com_facebook_tooltip_blue_topnub:I = 0x7f020215

.field public static final com_facebook_tooltip_blue_xout:I = 0x7f020216

.field public static final com_facebook_top_background:I = 0x7f020217

.field public static final com_facebook_top_button:I = 0x7f020218

.field public static final com_facebook_usersettingsfragment_background_gradient:I = 0x7f020219

.field public static final combo_backing:I = 0x7f02021a

.field public static final common_btn_normal:I = 0x7f02021b

.field public static final common_btn_press:I = 0x7f02021c

.field public static final common_btn_stateful:I = 0x7f02021d

.field public static final common_separation_line_black:I = 0x7f02021e

.field public static final common_separation_line_dark_gray:I = 0x7f02021f

.field public static final common_separation_line_light_gray:I = 0x7f020220

.field public static final common_signin_btn_icon_dark:I = 0x7f020221

.field public static final common_signin_btn_icon_disabled_dark:I = 0x7f020222

.field public static final common_signin_btn_icon_disabled_focus_dark:I = 0x7f020223

.field public static final common_signin_btn_icon_disabled_focus_light:I = 0x7f020224

.field public static final common_signin_btn_icon_disabled_light:I = 0x7f020225

.field public static final common_signin_btn_icon_focus_dark:I = 0x7f020226

.field public static final common_signin_btn_icon_focus_light:I = 0x7f020227

.field public static final common_signin_btn_icon_light:I = 0x7f020228

.field public static final common_signin_btn_icon_normal_dark:I = 0x7f020229

.field public static final common_signin_btn_icon_normal_light:I = 0x7f02022a

.field public static final common_signin_btn_icon_pressed_dark:I = 0x7f02022b

.field public static final common_signin_btn_icon_pressed_light:I = 0x7f02022c

.field public static final common_signin_btn_text_dark:I = 0x7f02022d

.field public static final common_signin_btn_text_disabled_dark:I = 0x7f02022e

.field public static final common_signin_btn_text_disabled_focus_dark:I = 0x7f02022f

.field public static final common_signin_btn_text_disabled_focus_light:I = 0x7f020230

.field public static final common_signin_btn_text_disabled_light:I = 0x7f020231

.field public static final common_signin_btn_text_focus_dark:I = 0x7f020232

.field public static final common_signin_btn_text_focus_light:I = 0x7f020233

.field public static final common_signin_btn_text_light:I = 0x7f020234

.field public static final common_signin_btn_text_normal_dark:I = 0x7f020235

.field public static final common_signin_btn_text_normal_light:I = 0x7f020236

.field public static final common_signin_btn_text_pressed_dark:I = 0x7f020237

.field public static final common_signin_btn_text_pressed_light:I = 0x7f020238

.field public static final continue_btn_normal:I = 0x7f020239

.field public static final continue_btn_press:I = 0x7f02023a

.field public static final continue_stateful:I = 0x7f02023b

.field public static final control_down_btn:I = 0x7f02023c

.field public static final control_down_btn_press:I = 0x7f02023d

.field public static final control_down_btn_selector:I = 0x7f02023e

.field public static final control_left_btn:I = 0x7f02023f

.field public static final control_left_btn_press:I = 0x7f020240

.field public static final control_left_btn_selector:I = 0x7f020241

.field public static final control_ok_btn:I = 0x7f020242

.field public static final control_ok_btn_grey:I = 0x7f020243

.field public static final control_ok_btn_press:I = 0x7f020244

.field public static final control_ok_btn_press_grey:I = 0x7f020245

.field public static final control_ok_btn_selector:I = 0x7f020246

.field public static final control_right_btn:I = 0x7f020247

.field public static final control_right_btn_press:I = 0x7f020248

.field public static final control_right_btn_selector:I = 0x7f020249

.field public static final control_top_btn:I = 0x7f02024a

.field public static final control_top_btn_press:I = 0x7f02024b

.field public static final control_top_btn_selector:I = 0x7f02024c

.field public static final controller_bg:I = 0x7f02024d

.field public static final controlpad_bg:I = 0x7f02073a

.field public static final controlpad_tutorial_bottom:I = 0x7f02024e

.field public static final controlpad_tutorial_middle:I = 0x7f02024f

.field public static final controlpad_tutorial_top:I = 0x7f020250

.field public static final csi_miami:I = 0x7f020251

.field public static final custom_close_btn:I = 0x7f020252

.field public static final custom_close_btn_press:I = 0x7f020253

.field public static final custom_done_sub_btn:I = 0x7f020254

.field public static final custom_done_sub_btn_press:I = 0x7f020255

.field public static final custom_edit_icon:I = 0x7f020256

.field public static final custom_edit_sub_btn:I = 0x7f020257

.field public static final custom_edit_sub_btn_press:I = 0x7f020258

.field public static final custom_plus_icon:I = 0x7f020259

.field public static final custom_plus_icon_press:I = 0x7f02025a

.field public static final custom_remove_icon:I = 0x7f02025b

.field public static final custom_remove_icon_press:I = 0x7f02025c

.field public static final customprogress:I = 0x7f02025d

.field public static final dark_list_selector:I = 0x7f02025e

.field public static final dark_list_selector_color:I = 0x7f02073b

.field public static final dashboard_volume_bg:I = 0x7f02025f

.field public static final dashboard_volume_icon:I = 0x7f020260

.field public static final dashboard_volume_minus:I = 0x7f020261

.field public static final dashboard_volume_minus_activity:I = 0x7f020262

.field public static final dashboard_volume_minus_stateful:I = 0x7f020263

.field public static final dashboard_volume_plus:I = 0x7f020264

.field public static final dashboard_volume_plus_activity:I = 0x7f020265

.field public static final dashboard_volume_plus_stateful:I = 0x7f020266

.field public static final detail_box_bg:I = 0x7f020267

.field public static final detail_scroll_up_shadow:I = 0x7f020268

.field public static final dial_00_btn:I = 0x7f020269

.field public static final dial_00_btn_press:I = 0x7f02026a

.field public static final dial_00_btn_selector:I = 0x7f02026b

.field public static final dial_01_btn:I = 0x7f02026c

.field public static final dial_01_btn_press:I = 0x7f02026d

.field public static final dial_01_btn_selector:I = 0x7f02026e

.field public static final dial_02_btn:I = 0x7f02026f

.field public static final dial_02_btn_press:I = 0x7f020270

.field public static final dial_02_btn_selector:I = 0x7f020271

.field public static final dial_03_btn:I = 0x7f020272

.field public static final dial_03_btn_press:I = 0x7f020273

.field public static final dial_03_btn_selector:I = 0x7f020274

.field public static final dial_04_btn:I = 0x7f020275

.field public static final dial_04_btn_press:I = 0x7f020276

.field public static final dial_04_btn_selector:I = 0x7f020277

.field public static final dial_05_btn:I = 0x7f020278

.field public static final dial_05_btn_press:I = 0x7f020279

.field public static final dial_05_btn_selector:I = 0x7f02027a

.field public static final dial_06_btn:I = 0x7f02027b

.field public static final dial_06_btn_press:I = 0x7f02027c

.field public static final dial_06_btn_selector:I = 0x7f02027d

.field public static final dial_07_btn:I = 0x7f02027e

.field public static final dial_07_btn_press:I = 0x7f02027f

.field public static final dial_07_btn_selector:I = 0x7f020280

.field public static final dial_08_btn:I = 0x7f020281

.field public static final dial_08_btn_press:I = 0x7f020282

.field public static final dial_08_btn_selector:I = 0x7f020283

.field public static final dial_09_btn:I = 0x7f020284

.field public static final dial_09_btn_press:I = 0x7f020285

.field public static final dial_09_btn_selector:I = 0x7f020286

.field public static final dial_10_btn:I = 0x7f020287

.field public static final dial_10_btn_press:I = 0x7f020288

.field public static final dial_10_btn_selector:I = 0x7f020289

.field public static final dial_11_btn:I = 0x7f02028a

.field public static final dial_11_btn_press:I = 0x7f02028b

.field public static final dial_11_btn_selector:I = 0x7f02028c

.field public static final dial_12_btn:I = 0x7f02028d

.field public static final dial_12_btn_press:I = 0x7f02028e

.field public static final dial_12_btn_selector:I = 0x7f02028f

.field public static final dial_bs_btn:I = 0x7f020290

.field public static final dial_bs_btn_press:I = 0x7f020291

.field public static final dial_bs_btn_selector:I = 0x7f020292

.field public static final dial_cs_btn:I = 0x7f020293

.field public static final dial_cs_btn_press:I = 0x7f020294

.field public static final dial_cs_btn_selector:I = 0x7f020295

.field public static final dial_enter_btn:I = 0x7f020296

.field public static final dial_enter_btn_press:I = 0x7f020297

.field public static final dial_enter_btn_selector:I = 0x7f020298

.field public static final dial_lld_btn:I = 0x7f020299

.field public static final dial_lld_btn_press:I = 0x7f02029a

.field public static final dial_lld_btn_selector:I = 0x7f02029b

.field public static final dial_minus_btn:I = 0x7f02029c

.field public static final dial_minus_btn_press:I = 0x7f02029d

.field public static final dial_minus_btn_selector:I = 0x7f02029e

.field public static final dial_pad_bg:I = 0x7f02029f

.field public static final dial_up_btn_normal:I = 0x7f0202a0

.field public static final dial_up_btn_press:I = 0x7f0202a1

.field public static final dislike_btn_normal:I = 0x7f0202a2

.field public static final dislike_btn_select:I = 0x7f0202a3

.field public static final divider:I = 0x7f0202a4

.field public static final divider_favorite_channel:I = 0x7f0202a5

.field public static final drawer_menu_before_login_bg:I = 0x7f0202a6

.field public static final drawer_menu_login_bg:I = 0x7f0202a7

.field public static final drawer_menu_profile_bg:I = 0x7f0202a8

.field public static final drawer_menu_shadow_bottom:I = 0x7f0202a9

.field public static final drawer_menu_shadow_top:I = 0x7f0202aa

.field public static final dvd_left_btn:I = 0x7f0202ab

.field public static final dvd_left_btn_press:I = 0x7f0202ac

.field public static final dvd_right_btn:I = 0x7f0202ad

.field public static final dvd_right_btn_press:I = 0x7f0202ae

.field public static final editreminder_normal:I = 0x7f0202af

.field public static final editreminder_press:I = 0x7f0202b0

.field public static final editreminder_stateful:I = 0x7f0202b1

.field public static final epg_image_cover:I = 0x7f0202b2

.field public static final epg_sub_list_item_bg:I = 0x7f0202b3

.field public static final fav_channel_delete_stateful:I = 0x7f0202b4

.field public static final fav_lock_next_states:I = 0x7f0202b5

.field public static final fav_lock_prev_states:I = 0x7f0202b6

.field public static final favorite_channel_ic_next:I = 0x7f0202b7

.field public static final favorite_channel_ic_next_focused:I = 0x7f0202b8

.field public static final favorite_channel_ic_next_press:I = 0x7f0202b9

.field public static final favorite_channel_ic_next_seleted:I = 0x7f0202ba

.field public static final favorite_channel_ic_pre:I = 0x7f0202bb

.field public static final favorite_channel_ic_pre_focused:I = 0x7f0202bc

.field public static final favorite_channel_ic_pre_press:I = 0x7f0202bd

.field public static final favorite_channel_ic_pre_seleted:I = 0x7f0202be

.field public static final favorite_channel_icon:I = 0x7f0202bf

.field public static final favorite_channel_icon_press:I = 0x7f0202c0

.field public static final favorite_channel_icon_stateful:I = 0x7f0202c1

.field public static final favorite_icon_normal:I = 0x7f0202c2

.field public static final favorite_icon_press:I = 0x7f0202c3

.field public static final favorites_channel_empty_icon:I = 0x7f0202c4

.field public static final favorites_show_empty_icon:I = 0x7f0202c5

.field public static final fc_device_popup_keypad:I = 0x7f0202c6

.field public static final fc_device_popup_keypad_controlonly:I = 0x7f0202c7

.field public static final fc_device_popup_keypad_jp_3bu:I = 0x7f0202c8

.field public static final fc_device_popup_keypad_jp_4bu:I = 0x7f0202c9

.field public static final female_btn_01:I = 0x7f0202ca

.field public static final female_btn_02:I = 0x7f0202cb

.field public static final female_btn_03:I = 0x7f0202cc

.field public static final female_btn_04:I = 0x7f0202cd

.field public static final female_btn_05:I = 0x7f0202ce

.field public static final female_btn_06:I = 0x7f0202cf

.field public static final gallery_icon_normal:I = 0x7f0203b0

.field public static final gallery_icon_press:I = 0x7f0203b1

.field public static final gallery_icon_stateful:I = 0x7f0203b2

.field public static final generic_power_btn_normal:I = 0x7f0203b3

.field public static final generic_power_btn_press:I = 0x7f0203b4

.field public static final genre_placeholder:I = 0x7f0203b5

.field public static final genre_placeholder_02:I = 0x7f0203b6

.field public static final genre_placeholder_big:I = 0x7f0203b7

.field public static final genre_placeholder_big_psr_l:I = 0x7f0203b8

.field public static final gray_btn_normal:I = 0x7f0203b9

.field public static final gray_btn_stateful:I = 0x7f0203ba

.field public static final green_btn_stateful:I = 0x7f0203bb

.field public static final handler_close_mr:I = 0x7f0203bc

.field public static final handler_down:I = 0x7f0203bd

.field public static final handler_down_press:I = 0x7f0203be

.field public static final handler_more_mr:I = 0x7f0203bf

.field public static final handler_up:I = 0x7f0203c0

.field public static final handler_up_press:I = 0x7f0203c1

.field public static final help_btn_normal:I = 0x7f0203c2

.field public static final help_btn_press:I = 0x7f0203c3

.field public static final home_widget_bg:I = 0x7f0203c4

.field public static final homescreen_left_arrow_states:I = 0x7f0203c5

.field public static final homescreen_right_arrow_states:I = 0x7f0203c6

.field public static final ic_drawer:I = 0x7f0203c7

.field public static final ic_plusone_medium_off_client:I = 0x7f0203c8

.field public static final ic_plusone_small_off_client:I = 0x7f0203c9

.field public static final ic_plusone_standard_off_client:I = 0x7f0203ca

.field public static final ic_plusone_tall_off_client:I = 0x7f0203cb

.field public static final icon:I = 0x7f0203cc

.field public static final infographic:I = 0x7f0203cd

.field public static final initial_tv_power_onoff_stateful:I = 0x7f0203ce

.field public static final input_text_bg:I = 0x7f0203cf

.field public static final ir_learn_btn_state:I = 0x7f0203d0

.field public static final ir_learning_test_bg:I = 0x7f0203d1

.field public static final ir_test_btn_state:I = 0x7f0203d2

.field public static final ivory_btn_normal:I = 0x7f0203d3

.field public static final ivory_btn_press:I = 0x7f0203d4

.field public static final ivory_btn_stateful:I = 0x7f0203d5

.field public static final key3_next_normal:I = 0x7f0203d6

.field public static final key3_next_press:I = 0x7f0203d7

.field public static final key3_prev_normal:I = 0x7f0203d8

.field public static final key3_prev_press:I = 0x7f0203d9

.field public static final key3_stop_normal:I = 0x7f0203da

.field public static final key3_stop_press:I = 0x7f0203db

.field public static final key4_ff_normal:I = 0x7f0203dc

.field public static final key4_ff_press:I = 0x7f0203dd

.field public static final key4_next_normal:I = 0x7f0203de

.field public static final key4_next_press:I = 0x7f0203df

.field public static final key4_pause_normal:I = 0x7f0203e0

.field public static final key4_pause_press:I = 0x7f0203e1

.field public static final key4_play_normal:I = 0x7f0203e2

.field public static final key4_play_press:I = 0x7f0203e3

.field public static final key4_prev_normal:I = 0x7f0203e4

.field public static final key4_prev_press:I = 0x7f0203e5

.field public static final key4_rec_normal:I = 0x7f0203e6

.field public static final key4_rec_press:I = 0x7f0203e7

.field public static final key4_rewind_normal:I = 0x7f0203e8

.field public static final key4_rewind_press:I = 0x7f0203e9

.field public static final key4_stop_normal:I = 0x7f0203ea

.field public static final key4_stop_press:I = 0x7f0203eb

.field public static final key_playpause:I = 0x7f0203ec

.field public static final key_playpause_press:I = 0x7f0203ed

.field public static final key_playpause_stateful:I = 0x7f0203ee

.field public static final keypad_0:I = 0x7f0203ef

.field public static final keypad_0_press:I = 0x7f0203f0

.field public static final keypad_1:I = 0x7f0203f1

.field public static final keypad_10:I = 0x7f0203f2

.field public static final keypad_10_press:I = 0x7f0203f3

.field public static final keypad_11:I = 0x7f0203f4

.field public static final keypad_11_press:I = 0x7f0203f5

.field public static final keypad_12:I = 0x7f0203f6

.field public static final keypad_12_press:I = 0x7f0203f7

.field public static final keypad_1_press:I = 0x7f0203f8

.field public static final keypad_2:I = 0x7f0203f9

.field public static final keypad_2_press:I = 0x7f0203fa

.field public static final keypad_3:I = 0x7f0203fb

.field public static final keypad_3_press:I = 0x7f0203fc

.field public static final keypad_4:I = 0x7f0203fd

.field public static final keypad_4_press:I = 0x7f0203fe

.field public static final keypad_5:I = 0x7f0203ff

.field public static final keypad_5_press:I = 0x7f020400

.field public static final keypad_6:I = 0x7f020401

.field public static final keypad_6_press:I = 0x7f020402

.field public static final keypad_7:I = 0x7f020403

.field public static final keypad_7_press:I = 0x7f020404

.field public static final keypad_8:I = 0x7f020405

.field public static final keypad_8_press:I = 0x7f020406

.field public static final keypad_9:I = 0x7f020407

.field public static final keypad_9_press:I = 0x7f020408

.field public static final keypad_bg_numberpad:I = 0x7f020409

.field public static final keypad_bg_player:I = 0x7f02040a

.field public static final keypad_blank:I = 0x7f02040b

.field public static final keypad_blank_reverse:I = 0x7f02040c

.field public static final keypad_dash:I = 0x7f02040d

.field public static final keypad_dash_press:I = 0x7f02040e

.field public static final keypad_enter:I = 0x7f02040f

.field public static final keypad_enter_press:I = 0x7f020410

.field public static final keypad_ff:I = 0x7f020411

.field public static final keypad_ff_press:I = 0x7f020412

.field public static final keypad_modes3_blank_center:I = 0x7f020413

.field public static final keypad_modes3_blank_left:I = 0x7f020414

.field public static final keypad_modes3_blank_right:I = 0x7f020415

.field public static final keypad_modes3_btn_center:I = 0x7f020416

.field public static final keypad_modes3_btn_left:I = 0x7f020417

.field public static final keypad_modes3_btn_right:I = 0x7f020418

.field public static final keypad_modes3_press_center:I = 0x7f020419

.field public static final keypad_modes3_press_left:I = 0x7f02041a

.field public static final keypad_modes3_press_right:I = 0x7f02041b

.field public static final keypad_modes4_blank_center:I = 0x7f02041c

.field public static final keypad_modes4_blank_left:I = 0x7f02041d

.field public static final keypad_modes4_blank_right:I = 0x7f02041e

.field public static final keypad_modes4_btn_center:I = 0x7f02041f

.field public static final keypad_modes4_btn_left:I = 0x7f020420

.field public static final keypad_modes4_btn_right:I = 0x7f020421

.field public static final keypad_modes4_press_center:I = 0x7f020422

.field public static final keypad_modes4_press_left:I = 0x7f020423

.field public static final keypad_modes4_press_right:I = 0x7f020424

.field public static final keypad_pause:I = 0x7f020425

.field public static final keypad_pause_press:I = 0x7f020426

.field public static final keypad_play:I = 0x7f020427

.field public static final keypad_play_press:I = 0x7f020428

.field public static final keypad_rec:I = 0x7f020429

.field public static final keypad_rec_press:I = 0x7f02042a

.field public static final keypad_rec_selected:I = 0x7f02042b

.field public static final l_quick_panel_setup_image:I = 0x7f02042c

.field public static final l_widget_ac_down_normal:I = 0x7f02042d

.field public static final l_widget_ac_down_press:I = 0x7f02042e

.field public static final l_widget_ac_up_normal:I = 0x7f02042f

.field public static final l_widget_ac_up_press:I = 0x7f020430

.field public static final l_widget_cover_icon:I = 0x7f020431

.field public static final l_widget_drop_down_normal:I = 0x7f020432

.field public static final l_widget_drop_down_press:I = 0x7f020433

.field public static final l_widget_mute_normal:I = 0x7f020434

.field public static final l_widget_mute_press:I = 0x7f020435

.field public static final l_widget_place_holder:I = 0x7f020436

.field public static final l_widget_place_holder_psr:I = 0x7f020437

.field public static final l_widget_play_pause_normal:I = 0x7f020438

.field public static final l_widget_play_pause_press:I = 0x7f020439

.field public static final l_widget_wot_btn_kitkat_psr_normal:I = 0x7f02043a

.field public static final l_widget_wot_btn_kitkat_psr_press:I = 0x7f02043b

.field public static final l_widget_wot_btn_normal:I = 0x7f02043c

.field public static final l_widget_wot_btn_press:I = 0x7f02043d

.field public static final launch_app_btn_normal:I = 0x7f02043e

.field public static final launch_app_btn_press:I = 0x7f02043f

.field public static final layer_list_ab_indeterminate_progress_bar:I = 0x7f020440

.field public static final left_arrow_btn_normal:I = 0x7f020441

.field public static final left_arrow_btn_press:I = 0x7f020442

.field public static final like_btn_normal:I = 0x7f020443

.field public static final like_btn_select:I = 0x7f020444

.field public static final list_check_icon:I = 0x7f020445

.field public static final list_color:I = 0x7f02073c

.field public static final list_menu_selector:I = 0x7f020446

.field public static final listmodule_divider:I = 0x7f020447

.field public static final lock_btn_normal:I = 0x7f020448

.field public static final lock_ch_down_btn_normal:I = 0x7f020449

.field public static final lock_ch_down_btn_press:I = 0x7f02044a

.field public static final lock_ch_down_icon_normal:I = 0x7f02044b

.field public static final lock_ch_down_icon_press:I = 0x7f02044c

.field public static final lock_ch_down_normal:I = 0x7f02044d

.field public static final lock_ch_down_press:I = 0x7f02044e

.field public static final lock_ch_up_icon_normal:I = 0x7f02044f

.field public static final lock_ch_up_icon_press:I = 0x7f020450

.field public static final lock_ch_up_normal:I = 0x7f020451

.field public static final lock_ch_up_press:I = 0x7f020452

.field public static final lock_custom_btn_normal:I = 0x7f020453

.field public static final lock_custom_btn_press:I = 0x7f020454

.field public static final lock_expand_btn_normal:I = 0x7f020455

.field public static final lock_expand_btn_press:I = 0x7f020456

.field public static final lock_ff_icon_normal:I = 0x7f020457

.field public static final lock_ff_icon_press:I = 0x7f020458

.field public static final lock_launch_normal:I = 0x7f020459

.field public static final lock_launch_press:I = 0x7f02045a

.field public static final lock_mute_icon_normal:I = 0x7f02045b

.field public static final lock_mute_icon_press:I = 0x7f02045c

.field public static final lock_non_select:I = 0x7f02045d

.field public static final lock_play_pause_icon_normal:I = 0x7f02045e

.field public static final lock_play_pause_icon_press:I = 0x7f02045f

.field public static final lock_qw_icon:I = 0x7f020460

.field public static final lock_qw_normal:I = 0x7f020461

.field public static final lock_qw_press:I = 0x7f020462

.field public static final lock_rew_icon_normal:I = 0x7f020467

.field public static final lock_rew_icon_press:I = 0x7f020468

.field public static final lock_select:I = 0x7f020469

.field public static final lock_vol_down_normal:I = 0x7f02046a

.field public static final lock_vol_down_press:I = 0x7f02046b

.field public static final lock_vol_up_normal:I = 0x7f02046c

.field public static final lock_vol_up_press:I = 0x7f02046d

.field public static final lockscreen_app_launch_states:I = 0x7f02046e

.field public static final lockscreen_btn_ch_dn_states:I = 0x7f02046f

.field public static final lockscreen_btn_ch_up_states:I = 0x7f020470

.field public static final lockscreen_ch_dn_states:I = 0x7f020471

.field public static final lockscreen_ch_up_states:I = 0x7f020472

.field public static final lockscreen_custom_button_states:I = 0x7f020473

.field public static final lockscreen_ff_states:I = 0x7f020474

.field public static final lockscreen_launch_button_states:I = 0x7f020475

.field public static final lockscreen_mute:I = 0x7f020476

.field public static final lockscreen_mute_states:I = 0x7f020477

.field public static final lockscreen_playpause_states:I = 0x7f020478

.field public static final lockscreen_power_button_stateful:I = 0x7f020479

.field public static final lockscreen_recent_channel_bg:I = 0x7f02047a

.field public static final lockscreen_rewind_states:I = 0x7f02047b

.field public static final lockscreen_vol_dn_states:I = 0x7f02047c

.field public static final lockscreen_vol_up_states:I = 0x7f02047d

.field public static final login_btn_fb_normal:I = 0x7f02047e

.field public static final login_btn_fb_press:I = 0x7f02047f

.field public static final login_btn_google_normal:I = 0x7f020480

.field public static final login_btn_google_press:I = 0x7f020481

.field public static final login_btn_samsung_normal:I = 0x7f020482

.field public static final login_btn_samsung_press:I = 0x7f020483

.field public static final male_btn_01:I = 0x7f020484

.field public static final male_btn_02:I = 0x7f020485

.field public static final male_btn_03:I = 0x7f020486

.field public static final male_btn_04:I = 0x7f020487

.field public static final male_btn_05:I = 0x7f020488

.field public static final male_btn_06:I = 0x7f020489

.field public static final menu_activity_bar:I = 0x7f02048a

.field public static final menu_activity_bar_hidden:I = 0x7f02048b

.field public static final menu_item_normal:I = 0x7f02073d

.field public static final menu_item_pressed:I = 0x7f02073e

.field public static final mini_btn_channel_down:I = 0x7f02048c

.field public static final mini_btn_channel_down_press:I = 0x7f02048d

.field public static final mini_btn_channel_up:I = 0x7f02048e

.field public static final mini_btn_channel_up_press:I = 0x7f02048f

.field public static final mini_btn_volume_minus:I = 0x7f020490

.field public static final mini_btn_volume_minus_press:I = 0x7f020491

.field public static final mini_btn_volume_plus:I = 0x7f020492

.field public static final mini_btn_volume_plus_press:I = 0x7f020493

.field public static final missing_channel:I = 0x7f020494

.field public static final missing_channel_pressed:I = 0x7f020495

.field public static final missing_channel_stateful:I = 0x7f020496

.field public static final missing_movie:I = 0x7f020497

.field public static final missing_tv:I = 0x7f020498

.field public static final more_back_btn:I = 0x7f020499

.field public static final more_back_btn_press:I = 0x7f02049a

.field public static final more_back_btn_selector:I = 0x7f02049b

.field public static final more_bottom_tap_1_type_press:I = 0x7f02049c

.field public static final more_bottom_tap_2_type_press_left:I = 0x7f02049d

.field public static final more_bottom_tap_2_type_press_right:I = 0x7f02049e

.field public static final more_bottom_tap_bg:I = 0x7f02049f

.field public static final more_bottom_tap_line:I = 0x7f0204a0

.field public static final more_bottom_tap_press_bg_center:I = 0x7f0204a1

.field public static final more_bottom_tap_press_bg_center_selector:I = 0x7f0204a2

.field public static final more_bottom_tap_press_bg_left:I = 0x7f0204a3

.field public static final more_bottom_tap_press_bg_left_selector:I = 0x7f0204a4

.field public static final more_bottom_tap_press_bg_right:I = 0x7f0204a5

.field public static final more_bottom_tap_press_bg_right_selector:I = 0x7f0204a6

.field public static final more_fwd_btn:I = 0x7f0204a7

.field public static final more_fwd_btn_press:I = 0x7f0204a8

.field public static final more_fwd_btn_selector:I = 0x7f0204a9

.field public static final more_icon_normal:I = 0x7f0204aa

.field public static final more_pause_btn:I = 0x7f0204ab

.field public static final more_pause_btn_press:I = 0x7f0204ac

.field public static final more_pause_btn_selector:I = 0x7f0204ad

.field public static final more_play_btn:I = 0x7f0204ae

.field public static final more_play_btn_press:I = 0x7f0204af

.field public static final more_play_btn_selector:I = 0x7f0204b0

.field public static final more_remote_btn:I = 0x7f0204b1

.field public static final more_remote_btn_press:I = 0x7f0204b2

.field public static final more_remote_btn_stateful:I = 0x7f0204b3

.field public static final more_remote_down_btn:I = 0x7f0204b4

.field public static final more_remote_down_btn_press:I = 0x7f0204b5

.field public static final more_remote_down_btn_selector:I = 0x7f0204b6

.field public static final more_small_back_btn:I = 0x7f0204b7

.field public static final more_small_back_btn_press:I = 0x7f0204b8

.field public static final more_small_back_btn_selector:I = 0x7f0204b9

.field public static final more_small_fwd_btn:I = 0x7f0204ba

.field public static final more_small_fwd_btn_press:I = 0x7f0204bb

.field public static final more_small_fwd_btn_selector:I = 0x7f0204bc

.field public static final more_small_left_btn:I = 0x7f0204bd

.field public static final more_small_left_btn_press:I = 0x7f0204be

.field public static final more_small_left_btn_selector:I = 0x7f0204bf

.field public static final more_small_pause_btn:I = 0x7f0204c0

.field public static final more_small_pause_btn_press:I = 0x7f0204c1

.field public static final more_small_pause_btn_selector:I = 0x7f0204c2

.field public static final more_small_play_btn:I = 0x7f0204c3

.field public static final more_small_play_btn_press:I = 0x7f0204c4

.field public static final more_small_play_btn_selector:I = 0x7f0204c5

.field public static final more_small_record_btn:I = 0x7f0204c6

.field public static final more_small_record_btn_press:I = 0x7f0204c7

.field public static final more_small_record_btn_selector:I = 0x7f0204c8

.field public static final more_small_right_btn:I = 0x7f0204c9

.field public static final more_small_right_btn_press:I = 0x7f0204ca

.field public static final more_small_right_btn_selector:I = 0x7f0204cb

.field public static final more_small_stop_btn:I = 0x7f0204cc

.field public static final more_small_stop_btn_press:I = 0x7f0204cd

.field public static final more_small_stop_btn_selector:I = 0x7f0204ce

.field public static final more_stop_btn:I = 0x7f0204cf

.field public static final more_stop_btn_press:I = 0x7f0204d0

.field public static final more_stop_btn_selector:I = 0x7f0204d1

.field public static final my_locaiton_visual_01:I = 0x7f0204d2

.field public static final my_locaiton_visual_02:I = 0x7f0204d3

.field public static final myroom_channel_empty_set:I = 0x7f0204d4

.field public static final navbar_time_next:I = 0x7f0204d5

.field public static final navbar_time_prev:I = 0x7f0204d6

.field public static final ncis:I = 0x7f0204d7

.field public static final netflix_bottom_btn_nomal:I = 0x7f0204d8

.field public static final netflix_bottom_btn_press:I = 0x7f0204d9

.field public static final netflix_left_btn_nomal:I = 0x7f0204da

.field public static final netflix_left_btn_press:I = 0x7f0204db

.field public static final netflix_ok_btn_nomal:I = 0x7f0204dc

.field public static final netflix_ok_btn_press:I = 0x7f0204dd

.field public static final netflix_right_btn_nomal:I = 0x7f0204de

.field public static final netflix_right_btn_press:I = 0x7f0204df

.field public static final netflix_top_btn_nomal:I = 0x7f0204e0

.field public static final netflix_top_btn_press:I = 0x7f0204e1

.field public static final next_btn_default:I = 0x7f0204e2

.field public static final next_btn_press:I = 0x7f0204e3

.field public static final no_text_smartremote:I = 0x7f0204e4

.field public static final noti_btn_mute:I = 0x7f0204e5

.field public static final noti_btn_mute_press:I = 0x7f0204e6

.field public static final noti_btn_mute_select:I = 0x7f0204e7

.field public static final noti_btn_onoff:I = 0x7f0204e8

.field public static final noti_btn_onoff_press:I = 0x7f0204e9

.field public static final noti_close_icon:I = 0x7f0204ea

.field public static final noti_close_icon_new:I = 0x7f0204eb

.field public static final noti_main_icon:I = 0x7f0204ec

.field public static final noti_non_select:I = 0x7f0204ed

.field public static final noti_prev_icon:I = 0x7f0204ee

.field public static final noti_prev_icon_new:I = 0x7f0204ef

.field public static final noti_select:I = 0x7f0204f0

.field public static final noti_trailer_icon:I = 0x7f0204f1

.field public static final noti_twitter_icon:I = 0x7f0204f2

.field public static final noti_twitter_icon_new:I = 0x7f0204f3

.field public static final noti_watchon_icon:I = 0x7f0204f4

.field public static final noti_watchon_icon_new:I = 0x7f0204f5

.field public static final noti_watchon_icon_psr_kitkat:I = 0x7f0204f6

.field public static final noti_watchon_small_icon:I = 0x7f0204f7

.field public static final numberpicker_down_states:I = 0x7f0204f8

.field public static final numberpicker_left_states:I = 0x7f0204f9

.field public static final numberpicker_right_states:I = 0x7f0204fa

.field public static final numberpicker_up_states:I = 0x7f0204fb

.field public static final ondemand_amazon_logo:I = 0x7f0204fc

.field public static final ondemand_amazon_logo_hd:I = 0x7f0204fd

.field public static final ondemand_hulu_logo:I = 0x7f0204fe

.field public static final ondemand_hulu_logo_hd:I = 0x7f0204ff

.field public static final ondemand_icon:I = 0x7f020500

.field public static final ondemand_netflix_logo:I = 0x7f020501

.field public static final ondemand_netflix_logo_hd:I = 0x7f020502

.field public static final ondemand_youtube_logo:I = 0x7f020503

.field public static final option_ic_about:I = 0x7f020504

.field public static final option_ic_editchannels:I = 0x7f020505

.field public static final option_ic_help:I = 0x7f020506

.field public static final option_ic_room:I = 0x7f020507

.field public static final peel__tab_indicator:I = 0x7f020508

.field public static final peel__tab_selected_holo:I = 0x7f020509

.field public static final peel__tab_unselected_holo:I = 0x7f02050a

.field public static final peel_app_visual:I = 0x7f02050b

.field public static final peel_icon_large:I = 0x7f02050c

.field public static final peel_icon_small:I = 0x7f02050d

.field public static final peel_transparent:I = 0x7f02073f

.field public static final peel_widget_icon:I = 0x7f02050e

.field public static final peronalize_normal:I = 0x7f02050f

.field public static final peronalize_select:I = 0x7f020510

.field public static final personalize_normal:I = 0x7f020511

.field public static final personalize_select:I = 0x7f020512

.field public static final phone_ani_01:I = 0x7f020513

.field public static final phone_ani_02:I = 0x7f020514

.field public static final phone_ani_03:I = 0x7f020515

.field public static final phone_ani_04:I = 0x7f020516

.field public static final phone_ani_05:I = 0x7f020517

.field public static final phone_ani_06:I = 0x7f020518

.field public static final phone_ani_07:I = 0x7f020519

.field public static final phone_ani_08:I = 0x7f02051a

.field public static final phone_ani_09:I = 0x7f02051b

.field public static final popup_button_bg:I = 0x7f02051c

.field public static final popup_button_text:I = 0x7f02051d

.field public static final popup_icon_ac:I = 0x7f02051e

.field public static final popup_icon_ac_normal:I = 0x7f02051f

.field public static final popup_icon_ac_press:I = 0x7f020520

.field public static final popup_icon_appletv_roku:I = 0x7f020521

.field public static final popup_icon_appletv_roku_normal:I = 0x7f020522

.field public static final popup_icon_appletv_roku_press:I = 0x7f020523

.field public static final popup_icon_av_receiver:I = 0x7f020524

.field public static final popup_icon_av_receiver_normal:I = 0x7f020525

.field public static final popup_icon_av_receiver_press:I = 0x7f020526

.field public static final popup_icon_bluray:I = 0x7f020527

.field public static final popup_icon_bluray_normal:I = 0x7f020528

.field public static final popup_icon_bluray_press:I = 0x7f020529

.field public static final popup_icon_dvd:I = 0x7f02052a

.field public static final popup_icon_dvd_normal:I = 0x7f02052b

.field public static final popup_icon_dvd_press:I = 0x7f02052c

.field public static final popup_icon_projector:I = 0x7f02052d

.field public static final popup_icon_projector_normal:I = 0x7f02052e

.field public static final popup_icon_projector_press:I = 0x7f02052f

.field public static final popup_icon_settop:I = 0x7f020530

.field public static final popup_icon_settop_normal:I = 0x7f020531

.field public static final popup_icon_settop_press:I = 0x7f020532

.field public static final popup_icon_tivo:I = 0x7f020533

.field public static final popup_icon_tivo_normal:I = 0x7f020534

.field public static final popup_icon_tivo_press:I = 0x7f020535

.field public static final popup_on:I = 0x7f020536

.field public static final power_btn_home_normal:I = 0x7f020537

.field public static final power_btn_home_press:I = 0x7f020538

.field public static final power_projector_btn:I = 0x7f020539

.field public static final power_projector_btn_press:I = 0x7f02053a

.field public static final preview:I = 0x7f02053b

.field public static final profile_check_icon:I = 0x7f02053c

.field public static final program_channel:I = 0x7f02053d

.field public static final progress_bar:I = 0x7f02053e

.field public static final ptile_reminder_btn_normal:I = 0x7f02053f

.field public static final ptile_reminder_btn_press:I = 0x7f020540

.field public static final ptile_tunein_btn_normal:I = 0x7f020541

.field public static final ptile_tunein_btn_press:I = 0x7f020542

.field public static final quick_ch_dn_states:I = 0x7f020543

.field public static final quick_ch_up_states:I = 0x7f020544

.field public static final quick_panel_clearbtn_normal:I = 0x7f020545

.field public static final quick_panel_clearbtn_press:I = 0x7f020546

.field public static final quick_panel_setup_image:I = 0x7f020547

.field public static final quick_vol_dn_states:I = 0x7f020548

.field public static final quick_vol_up_states:I = 0x7f020549

.field public static final record_btn_normal:I = 0x7f02054a

.field public static final record_btn_press:I = 0x7f02054b

.field public static final record_btn_stateful:I = 0x7f02054c

.field public static final record_icon_normal:I = 0x7f02054d

.field public static final record_icon_press:I = 0x7f02054e

.field public static final region_list_stateful:I = 0x7f020550

.field public static final remind_me_btn_normal:I = 0x7f020551

.field public static final remind_me_btn_press:I = 0x7f020552

.field public static final remind_me_btn_stateful:I = 0x7f020553

.field public static final reminder_icon:I = 0x7f020554

.field public static final reminder_icon_selected:I = 0x7f020555

.field public static final remote_back_btn:I = 0x7f020556

.field public static final remote_back_btn_press:I = 0x7f020557

.field public static final remote_back_btn_stateful:I = 0x7f020558

.field public static final remote_ch_guide_bg_content:I = 0x7f020559

.field public static final remote_ch_guide_bg_handle:I = 0x7f02055a

.field public static final remote_ch_minus_btn_normal:I = 0x7f02055b

.field public static final remote_ch_minus_btn_press:I = 0x7f02055c

.field public static final remote_ch_plus_btn_normal:I = 0x7f02055d

.field public static final remote_ch_plus_btn_press:I = 0x7f02055e

.field public static final remote_fwd_btn:I = 0x7f02055f

.field public static final remote_fwd_btn_press:I = 0x7f020560

.field public static final remote_fwd_btn_stateful:I = 0x7f020561

.field public static final remote_icon_back:I = 0x7f020562

.field public static final remote_icon_enter:I = 0x7f020563

.field public static final remote_icon_forward:I = 0x7f020564

.field public static final remote_icon_home:I = 0x7f020565

.field public static final remote_icon_keypad:I = 0x7f020566

.field public static final remote_icon_minus:I = 0x7f020567

.field public static final remote_icon_mute:I = 0x7f020568

.field public static final remote_icon_next:I = 0x7f020569

.field public static final remote_icon_pause:I = 0x7f02056a

.field public static final remote_icon_play:I = 0x7f02056b

.field public static final remote_icon_play_pause:I = 0x7f02056c

.field public static final remote_icon_pre:I = 0x7f02056d

.field public static final remote_icon_pw:I = 0x7f02056e

.field public static final remote_icon_record:I = 0x7f02056f

.field public static final remote_icon_rewind:I = 0x7f020570

.field public static final remote_icon_roku_option:I = 0x7f020571

.field public static final remote_icon_roku_replay:I = 0x7f020572

.field public static final remote_icon_samsung_smart:I = 0x7f020573

.field public static final remote_icon_skip_back:I = 0x7f020574

.field public static final remote_icon_skip_ff:I = 0x7f020575

.field public static final remote_icon_stop:I = 0x7f020576

.field public static final remote_icon_tivo_dislike:I = 0x7f020577

.field public static final remote_icon_tivo_like:I = 0x7f020578

.field public static final remote_icon_tivo_logo:I = 0x7f020579

.field public static final remote_indicator:I = 0x7f02057a

.field public static final remote_indicator_activity:I = 0x7f02057b

.field public static final remote_indicator_home:I = 0x7f02057c

.field public static final remote_indicator_home_activity:I = 0x7f02057d

.field public static final remote_keypad_btn_normal:I = 0x7f02057e

.field public static final remote_keypad_btn_press:I = 0x7f02057f

.field public static final remote_middle_bg:I = 0x7f020580

.field public static final remote_more_btn_close_normal:I = 0x7f020581

.field public static final remote_more_btn_close_press:I = 0x7f020582

.field public static final remote_more_btn_normal:I = 0x7f020583

.field public static final remote_more_btn_press:I = 0x7f020584

.field public static final remote_mute_btn_normal:I = 0x7f020585

.field public static final remote_mute_btn_press:I = 0x7f020586

.field public static final remote_play_pause_btn_stateful:I = 0x7f020587

.field public static final remote_playpause_btn:I = 0x7f020588

.field public static final remote_playpause_btn_press:I = 0x7f020589

.field public static final remote_rec_bg:I = 0x7f02058a

.field public static final remote_rewind_btn_stateful:I = 0x7f02058b

.field public static final remote_setup_remote_01:I = 0x7f02058c

.field public static final remote_setup_remote_02:I = 0x7f02058d

.field public static final remote_setup_remote_03:I = 0x7f02058e

.field public static final remote_setup_remote_04:I = 0x7f02058f

.field public static final remote_spiner_icon:I = 0x7f020590

.field public static final remote_vol_minus_btn_normal:I = 0x7f020591

.field public static final remote_vol_minus_btn_press:I = 0x7f020592

.field public static final remote_vol_plus_btn_normal:I = 0x7f020593

.field public static final remote_vol_plus_btn_press:I = 0x7f020594

.field public static final reply_icon_normal:I = 0x7f020595

.field public static final reply_icon_press:I = 0x7f020596

.field public static final retry_icon:I = 0x7f020597

.field public static final retweet_icon_normal:I = 0x7f020598

.field public static final retweet_icon_press:I = 0x7f020599

.field public static final right_arrow_btn_normal:I = 0x7f02059a

.field public static final right_arrow_btn_press:I = 0x7f02059b

.field public static final roku_amazon_btn:I = 0x7f02059c

.field public static final roku_amazon_btn_press:I = 0x7f02059d

.field public static final roku_amazon_btn_selector:I = 0x7f02059e

.field public static final roku_back_btn:I = 0x7f02059f

.field public static final roku_back_btn_press:I = 0x7f0205a0

.field public static final roku_back_btn_selector:I = 0x7f0205a1

.field public static final roku_crackle_btn:I = 0x7f0205a2

.field public static final roku_crackle_btn_press:I = 0x7f0205a3

.field public static final roku_crackle_btn_selector:I = 0x7f0205a4

.field public static final roku_home_btn:I = 0x7f0205a5

.field public static final roku_home_btn_press:I = 0x7f0205a6

.field public static final roku_home_btn_selector:I = 0x7f0205a7

.field public static final roku_hulu_btn:I = 0x7f0205a8

.field public static final roku_hulu_btn_press:I = 0x7f0205a9

.field public static final roku_hulu_btn_selector:I = 0x7f0205aa

.field public static final roku_left_btn:I = 0x7f0205ab

.field public static final roku_left_btn_press:I = 0x7f0205ac

.field public static final roku_left_btn_selector:I = 0x7f0205ad

.field public static final roku_netflix_btn:I = 0x7f0205ae

.field public static final roku_netflix_btn_press:I = 0x7f0205af

.field public static final roku_netflix_btn_selector:I = 0x7f0205b0

.field public static final roku_pandora_btn:I = 0x7f0205b1

.field public static final roku_pandora_btn_press:I = 0x7f0205b2

.field public static final roku_pandora_btn_selector:I = 0x7f0205b3

.field public static final roku_play_btn:I = 0x7f0205b4

.field public static final roku_play_btn_press:I = 0x7f0205b5

.field public static final roku_play_btn_selector:I = 0x7f0205b6

.field public static final roku_reflash_btn:I = 0x7f0205b7

.field public static final roku_reflash_btn_press:I = 0x7f0205b8

.field public static final roku_reflash_btn_selector:I = 0x7f0205b9

.field public static final roku_right_btn:I = 0x7f0205ba

.field public static final roku_right_btn_press:I = 0x7f0205bb

.field public static final roku_right_btn_selector:I = 0x7f0205bc

.field public static final roku_star_btn:I = 0x7f0205bd

.field public static final roku_star_btn_press:I = 0x7f0205be

.field public static final roku_star_btn_selector:I = 0x7f0205bf

.field public static final roku_vudu_btn:I = 0x7f0205c0

.field public static final roku_vudu_btn_press:I = 0x7f0205c1

.field public static final roku_vudu_btn_selector:I = 0x7f0205c2

.field public static final s5_carrot_icon_normal:I = 0x7f0205c3

.field public static final s5_carrot_icon_press:I = 0x7f0205c4

.field public static final s5_optimize_tunein_handle:I = 0x7f0205c5

.field public static final s5_optimize_tunein_handle_bg:I = 0x7f0205c6

.field public static final s5_optimize_tunein_handle_press:I = 0x7f0205c7

.field public static final s5_plus_icon:I = 0x7f0205c8

.field public static final s5_rooms_icon_normal:I = 0x7f0205c9

.field public static final s5_rooms_icon_press:I = 0x7f0205ca

.field public static final samsung_btn_big_normal:I = 0x7f0205cb

.field public static final samsung_btn_big_press:I = 0x7f0205cc

.field public static final samsung_btn_power_on:I = 0x7f0205cd

.field public static final samsung_btn_power_on_stateful:I = 0x7f0205ce

.field public static final samsung_btn_stateful:I = 0x7f0205cf

.field public static final samsung_num_button_00_stateful:I = 0x7f0205d0

.field public static final samsung_num_button_01_stateful:I = 0x7f0205d1

.field public static final samsung_num_button_02_stateful:I = 0x7f0205d2

.field public static final samsung_num_button_03_stateful:I = 0x7f0205d3

.field public static final samsung_num_button_04_stateful:I = 0x7f0205d4

.field public static final samsung_num_button_05_stateful:I = 0x7f0205d5

.field public static final samsung_num_button_06_stateful:I = 0x7f0205d6

.field public static final samsung_num_button_07_stateful:I = 0x7f0205d7

.field public static final samsung_num_button_08_stateful:I = 0x7f0205d8

.field public static final samsung_num_button_09_stateful:I = 0x7f0205d9

.field public static final samsung_num_button_10_stateful:I = 0x7f0205da

.field public static final samsung_num_button_11_stateful:I = 0x7f0205db

.field public static final samsung_num_button_12_stateful:I = 0x7f0205dc

.field public static final samsung_num_button_dash_stateful:I = 0x7f0205dd

.field public static final samsung_num_button_enter_stateful:I = 0x7f0205de

.field public static final search_close_icon:I = 0x7f0205df

.field public static final search_close_icon_press:I = 0x7f0205e0

.field public static final search_cursor_color:I = 0x7f020740

.field public static final search_cusor:I = 0x7f0205e1

.field public static final search_hint_color:I = 0x7f0205e2

.field public static final setreminder_badge:I = 0x7f0205e3

.field public static final setreminder_icon_normal:I = 0x7f0205e4

.field public static final setreminder_icon_press:I = 0x7f0205e5

.field public static final setreminder_normal:I = 0x7f0205e6

.field public static final setreminder_press:I = 0x7f0205e7

.field public static final setreminder_stateful:I = 0x7f0205e8

.field public static final setting_check:I = 0x7f0205e9

.field public static final setting_check_box_bg:I = 0x7f0205ea

.field public static final setting_edit_icon_btn:I = 0x7f0205eb

.field public static final setting_edit_icon_btn_presss:I = 0x7f0205ec

.field public static final setting_genres_swipe_icon:I = 0x7f0205ed

.field public static final setting_genres_swipe_icon_press:I = 0x7f0205ee

.field public static final settings_edit_button_states:I = 0x7f0205ef

.field public static final settings_list_color:I = 0x7f0205f0

.field public static final setup_search_btn_icon:I = 0x7f0205f1

.field public static final setup_stb_btn_normal:I = 0x7f0205f2

.field public static final setup_stb_btn_press:I = 0x7f0205f3

.field public static final setup_stb_btn_states:I = 0x7f0205f4

.field public static final setup_test_stb_ch_up_btn_states:I = 0x7f0205f5

.field public static final setup_tv_btn_normal:I = 0x7f0205f6

.field public static final setup_tv_btn_press:I = 0x7f0205f7

.field public static final setup_tv_btn_states:I = 0x7f0205f8

.field public static final setup_tv_signal_arrow_anim:I = 0x7f0205f9

.field public static final share_icon_normal:I = 0x7f0205fa

.field public static final share_icon_stateful:I = 0x7f0205fb

.field public static final sidebar_genre_firstrun:I = 0x7f0205fc

.field public static final sidebar_genre_firstrun_press:I = 0x7f0205fd

.field public static final sidebar_sport_baseball:I = 0x7f0205fe

.field public static final sidebar_sport_baseball_press:I = 0x7f0205ff

.field public static final sidebar_sport_baseball_stateful:I = 0x7f020600

.field public static final sidebar_sport_basketball:I = 0x7f020601

.field public static final sidebar_sport_basketball_press:I = 0x7f020602

.field public static final sidebar_sport_basketball_stateful:I = 0x7f020603

.field public static final sidebar_sport_football:I = 0x7f020604

.field public static final sidebar_sport_football_press:I = 0x7f020605

.field public static final sidebar_sport_football_stateful:I = 0x7f020606

.field public static final sidebar_sport_hockey:I = 0x7f020607

.field public static final sidebar_sport_hockey_press:I = 0x7f020608

.field public static final sidebar_sport_hockey_stateful:I = 0x7f020609

.field public static final sidebar_sport_newstalk:I = 0x7f02060a

.field public static final sidebar_sport_newstalk_press:I = 0x7f02060b

.field public static final sidebar_sport_newstalk_stateful:I = 0x7f02060c

.field public static final sidebar_sport_other:I = 0x7f02060d

.field public static final sidebar_sport_other_press:I = 0x7f02060e

.field public static final sidebar_sport_other_stateful:I = 0x7f02060f

.field public static final sidebar_sport_soccer:I = 0x7f020610

.field public static final sidebar_sport_soccer_press:I = 0x7f020611

.field public static final sidebar_sport_soccer_stateful:I = 0x7f020612

.field public static final spinner_bg_stateful:I = 0x7f020613

.field public static final spinner_bottom_bg_stateful:I = 0x7f020614

.field public static final splashpage1:I = 0x7f020615

.field public static final sport_baseball_movie:I = 0x7f020616

.field public static final sport_baseball_movie_big:I = 0x7f020617

.field public static final sport_basketball_movie:I = 0x7f020618

.field public static final sport_basketball_movie_big:I = 0x7f020619

.field public static final sport_football_movie:I = 0x7f02061a

.field public static final sport_football_movie_big:I = 0x7f02061b

.field public static final sport_hockey_movie:I = 0x7f02061c

.field public static final sport_hockey_movie_big:I = 0x7f02061d

.field public static final sport_newstalk_movie:I = 0x7f02061e

.field public static final sport_newstalk_movie_big:I = 0x7f02061f

.field public static final sport_other_movie:I = 0x7f020620

.field public static final sport_other_movie_big:I = 0x7f020621

.field public static final sport_soccer_movie:I = 0x7f020622

.field public static final sport_soccer_movie_big:I = 0x7f020623

.field public static final stat_notify_smart_remote:I = 0x7f020624

.field public static final stb_3tab_bg_left:I = 0x7f020625

.field public static final stb_3tab_bg_middle:I = 0x7f020626

.field public static final stb_3tab_bg_right:I = 0x7f020627

.field public static final stb_3tab_left:I = 0x7f020628

.field public static final stb_3tab_middle:I = 0x7f020629

.field public static final stb_3tab_press_left:I = 0x7f02062a

.field public static final stb_3tab_press_middle:I = 0x7f02062b

.field public static final stb_3tab_press_right:I = 0x7f02062c

.field public static final stb_3tab_right:I = 0x7f02062d

.field public static final stb_3tab_select_left:I = 0x7f02062e

.field public static final stb_3tab_select_middle:I = 0x7f02062f

.field public static final stb_3tab_select_right:I = 0x7f020630

.field public static final stb_list_highlight_bg:I = 0x7f020631

.field public static final stb_tab_bg_left:I = 0x7f020632

.field public static final stb_tab_bg_right:I = 0x7f020633

.field public static final stb_tab_left:I = 0x7f020634

.field public static final stb_tab_no_add_bg_01:I = 0x7f020635

.field public static final stb_tab_no_add_press_01:I = 0x7f020636

.field public static final stb_tab_press_left:I = 0x7f020637

.field public static final stb_tab_press_right:I = 0x7f020638

.field public static final stb_tab_right:I = 0x7f020639

.field public static final stb_tab_select_left:I = 0x7f02063a

.field public static final stb_tab_select_right:I = 0x7f02063b

.field public static final stb_tab_single:I = 0x7f02063c

.field public static final step_number_activity:I = 0x7f02063d

.field public static final step_number_black_circle:I = 0x7f02063e

.field public static final step_number_black_line:I = 0x7f02063f

.field public static final step_number_blue_circle:I = 0x7f020640

.field public static final step_number_blue_line:I = 0x7f020641

.field public static final sub_blue_btn:I = 0x7f020642

.field public static final sub_blue_btn_press:I = 0x7f020643

.field public static final sub_blue_btn_selector:I = 0x7f020644

.field public static final sub_green_btn:I = 0x7f020645

.field public static final sub_green_btn_press:I = 0x7f020646

.field public static final sub_green_btn_selector:I = 0x7f020647

.field public static final sub_play_btn:I = 0x7f020648

.field public static final sub_play_btn_press:I = 0x7f020649

.field public static final sub_play_btn_selector:I = 0x7f02064a

.field public static final sub_power_btn:I = 0x7f02064b

.field public static final sub_power_btn_press:I = 0x7f02064c

.field public static final sub_red_btn:I = 0x7f02064d

.field public static final sub_red_btn_press:I = 0x7f02064e

.field public static final sub_red_btn_selector:I = 0x7f02064f

.field public static final sub_remote_btn_normal:I = 0x7f020650

.field public static final sub_remote_btn_plus_icon:I = 0x7f020651

.field public static final sub_remote_btn_press:I = 0x7f020652

.field public static final sub_remote_btn_stateful:I = 0x7f020653

.field public static final sub_yellow_btn:I = 0x7f020654

.field public static final sub_yellow_btn_press:I = 0x7f020655

.field public static final sub_yellow_btn_selector:I = 0x7f020656

.field public static final swipe_indicatior_active:I = 0x7f020657

.field public static final swipe_indicatior_disable:I = 0x7f020658

.field public static final tab_ic_like:I = 0x7f020659

.field public static final tab_ic_like_press:I = 0x7f02065a

.field public static final tabbar_bg_color:I = 0x7f020741

.field public static final tabbar_divider_color:I = 0x7f020742

.field public static final teaser_icon_normal:I = 0x7f02065b

.field public static final teaser_icon_press:I = 0x7f02065c

.field public static final test_ac_drawing:I = 0x7f02065d

.field public static final test_arrow_left_normal:I = 0x7f02065e

.field public static final test_arrow_left_press:I = 0x7f02065f

.field public static final test_arrow_left_stateful:I = 0x7f020660

.field public static final test_arrow_right_normal:I = 0x7f020661

.field public static final test_arrow_right_press:I = 0x7f020662

.field public static final test_arrow_right_stateful:I = 0x7f020663

.field public static final test_av_drawing:I = 0x7f020664

.field public static final test_bluray_drawing:I = 0x7f020665

.field public static final test_dvd_drawing:I = 0x7f020666

.field public static final test_other_btn_normal:I = 0x7f020667

.field public static final test_other_btn_press:I = 0x7f020668

.field public static final test_projector_drawing:I = 0x7f020669

.field public static final test_pw_btn_normal:I = 0x7f02066a

.field public static final test_pw_btn_press:I = 0x7f02066b

.field public static final test_stb_drawing:I = 0x7f02066c

.field public static final test_tv_drawing:I = 0x7f02066d

.field public static final test_tv_stb:I = 0x7f02066e

.field public static final text_underline:I = 0x7f02066f

.field public static final textcolor_selector:I = 0x7f020670

.field public static final textfield_slide_menu:I = 0x7f020671

.field public static final tile_bottom_bg_stateful:I = 0x7f020672

.field public static final tile_channel_unit_bg:I = 0x7f020673

.field public static final tile_channel_unit_bg_focus:I = 0x7f020674

.field public static final tile_channel_unit_bg_press:I = 0x7f020675

.field public static final title_back:I = 0x7f020676

.field public static final toast:I = 0x7f020677

.field public static final toast_close:I = 0x7f020678

.field public static final toastclose_normal:I = 0x7f020679

.field public static final toastclose_press:I = 0x7f02067a

.field public static final toggle_off:I = 0x7f02067b

.field public static final toggle_on:I = 0x7f02067c

.field public static final track_pad_bottom_btn:I = 0x7f02067d

.field public static final track_pad_bottom_btn_press:I = 0x7f02067e

.field public static final track_pad_left_btn:I = 0x7f02067f

.field public static final track_pad_left_btn_press:I = 0x7f020680

.field public static final track_pad_right_btn:I = 0x7f020681

.field public static final track_pad_right_btn_press:I = 0x7f020682

.field public static final track_pad_top_btn:I = 0x7f020683

.field public static final track_pad_top_btn_press:I = 0x7f020684

.field public static final tune_in_btn_stateful:I = 0x7f020685

.field public static final tunein_btn_normal:I = 0x7f020686

.field public static final tunein_btn_press:I = 0x7f020687

.field public static final tunein_normal:I = 0x7f020688

.field public static final tunein_press:I = 0x7f020689

.field public static final tunein_stateful:I = 0x7f02068a

.field public static final tutorial_swipe_hold_icon:I = 0x7f02068b

.field public static final tv_big_hand:I = 0x7f02068c

.field public static final tv_small_hand:I = 0x7f02068d

.field public static final tw_ab_bottom_transparent_dark_holo:I = 0x7f02068e

.field public static final tw_action_bar_icon_next:I = 0x7f02068f

.field public static final tw_action_bar_icon_next_disable:I = 0x7f020690

.field public static final tw_action_bar_icon_prev:I = 0x7f020691

.field public static final tw_action_bar_icon_prev_disable:I = 0x7f020692

.field public static final tw_action_bar_sub_tab_bg_holo_dark:I = 0x7f020693

.field public static final tw_action_bar_tab_selected_bg_holo_dark:I = 0x7f020694

.field public static final tw_action_item_background_dark:I = 0x7f020695

.field public static final tw_action_item_background_focused_holo_dark:I = 0x7f020696

.field public static final tw_action_item_background_pressed_holo_dark:I = 0x7f020697

.field public static final tw_action_item_background_selected_holo_dark:I = 0x7f020698

.field public static final tw_background_dark:I = 0x7f020699

.field public static final tw_btn_check_off_disabled_holo_dark:I = 0x7f02069a

.field public static final tw_btn_check_off_holo_dark:I = 0x7f02069b

.field public static final tw_btn_check_on_disabled_holo_dark:I = 0x7f02069c

.field public static final tw_btn_check_on_holo_dark:I = 0x7f02069d

.field public static final tw_btn_next_default_holo_dark:I = 0x7f02069e

.field public static final tw_btn_next_pressed_holo_dark:I = 0x7f02069f

.field public static final tw_btn_radio_off_disabled_focused_holo_dark:I = 0x7f0206a0

.field public static final tw_btn_radio_off_disabled_holo_dark:I = 0x7f0206a1

.field public static final tw_btn_radio_off_focused_holo_dark:I = 0x7f0206a2

.field public static final tw_btn_radio_off_holo_dark:I = 0x7f0206a3

.field public static final tw_btn_radio_off_pressed_holo_dark:I = 0x7f0206a4

.field public static final tw_btn_radio_on_disabled_focused_holo_dark:I = 0x7f0206a5

.field public static final tw_btn_radio_on_disabled_holo_dark:I = 0x7f0206a6

.field public static final tw_btn_radio_on_focused_holo_dark:I = 0x7f0206a7

.field public static final tw_btn_radio_on_holo_dark:I = 0x7f0206a8

.field public static final tw_btn_radio_on_pressed_holo_dark:I = 0x7f0206a9

.field public static final tw_buttonbarbutton_selector_default_holo_dark:I = 0x7f0206aa

.field public static final tw_buttonbarbutton_selector_disabled_focused_holo_dark:I = 0x7f0206ab

.field public static final tw_buttonbarbutton_selector_disabled_holo_dark:I = 0x7f0206ac

.field public static final tw_buttonbarbutton_selector_focused_holo_dark:I = 0x7f0206ad

.field public static final tw_buttonbarbutton_selector_pressed_holo_dark:I = 0x7f0206ae

.field public static final tw_buttonbarbutton_selector_selected_holo_dark:I = 0x7f0206af

.field public static final tw_divider_ab_holo_dark:I = 0x7f0206b0

.field public static final tw_divider_popup_vertical_holo_dark:I = 0x7f0206b1

.field public static final tw_ic_cancel:I = 0x7f0206b2

.field public static final tw_ic_search:I = 0x7f0206b3

.field public static final tw_list_divider_holo_dark:I = 0x7f0206b4

.field public static final tw_list_focused_holo_dark:I = 0x7f0206b5

.field public static final tw_list_icon_minus_disabled_holo_dark:I = 0x7f0206b6

.field public static final tw_list_icon_minus_focused_holo_dark:I = 0x7f0206b7

.field public static final tw_list_icon_minus_holo_dark:I = 0x7f0206b8

.field public static final tw_list_icon_minus_pressed_holo_dark:I = 0x7f0206b9

.field public static final tw_list_icon_reorder:I = 0x7f0206ba

.field public static final tw_list_pressed_holo_dark:I = 0x7f0206bb

.field public static final tw_list_selected_holo_dark:I = 0x7f0206bc

.field public static final tw_menu_ab_dropdown_panel_holo_dark:I = 0x7f0206bd

.field public static final tw_menu_popup_panel_holo_dark:I = 0x7f0206be

.field public static final tw_numberpicker_down_disabled_focused_holo_dark:I = 0x7f0206bf

.field public static final tw_numberpicker_down_disabled_holo_dark:I = 0x7f0206c0

.field public static final tw_numberpicker_down_focused_holo_dark:I = 0x7f0206c1

.field public static final tw_numberpicker_down_normal_holo_dark:I = 0x7f0206c2

.field public static final tw_numberpicker_down_pressed_holo_dark:I = 0x7f0206c3

.field public static final tw_numberpicker_down_selected_holo_dark:I = 0x7f0206c4

.field public static final tw_numberpicker_left_disabled:I = 0x7f0206c5

.field public static final tw_numberpicker_left_disabled_focused:I = 0x7f0206c6

.field public static final tw_numberpicker_left_focused:I = 0x7f0206c7

.field public static final tw_numberpicker_left_normal:I = 0x7f0206c8

.field public static final tw_numberpicker_left_pressed:I = 0x7f0206c9

.field public static final tw_numberpicker_right_disabled:I = 0x7f0206ca

.field public static final tw_numberpicker_right_disabled_focused:I = 0x7f0206cb

.field public static final tw_numberpicker_right_focused:I = 0x7f0206cc

.field public static final tw_numberpicker_right_normal:I = 0x7f0206cd

.field public static final tw_numberpicker_right_pressed:I = 0x7f0206ce

.field public static final tw_numberpicker_up_disabled_focused_holo_dark:I = 0x7f0206cf

.field public static final tw_numberpicker_up_disabled_holo_dark:I = 0x7f0206d0

.field public static final tw_numberpicker_up_focused_holo_dark:I = 0x7f0206d1

.field public static final tw_numberpicker_up_normal_holo_dark:I = 0x7f0206d2

.field public static final tw_numberpicker_up_pressed_holo_dark:I = 0x7f0206d3

.field public static final tw_radio:I = 0x7f0206d4

.field public static final tw_spinner_default_holo_dark:I = 0x7f0206d5

.field public static final tw_spinner_default_holo_dark_am:I = 0x7f0206d6

.field public static final tw_spinner_disabled_dark:I = 0x7f0206d7

.field public static final tw_spinner_focused_dark:I = 0x7f0206d8

.field public static final tw_spinner_list_focused_holo_dark:I = 0x7f0206d9

.field public static final tw_spinner_list_pressed_holo_dark:I = 0x7f0206da

.field public static final tw_spinner_pressed_dark:I = 0x7f0206db

.field public static final tw_spinner_pressed_holo_dark_am:I = 0x7f0206dc

.field public static final tw_spinner_pressed_states:I = 0x7f0206dd

.field public static final tw_textfield_activated_holo_dark:I = 0x7f0206de

.field public static final tw_textfield_disabled_focused_holo_dark:I = 0x7f0206df

.field public static final tw_textfield_disabled_holo_dark:I = 0x7f0206e0

.field public static final tw_textfield_focused_holo_dark:I = 0x7f0206e1

.field public static final tw_textfield_search_default_holo_dark:I = 0x7f0206e2

.field public static final tw_textfield_search_selected_holo_dark:I = 0x7f0206e3

.field public static final twitt_reply_stateful:I = 0x7f0206e4

.field public static final twitter_blue_btn:I = 0x7f0206e5

.field public static final twitter_cancel_normal:I = 0x7f0206e6

.field public static final twitter_cancel_press:I = 0x7f0206e7

.field public static final twitter_icon:I = 0x7f0206e8

.field public static final twitter_image_cancel_stateful:I = 0x7f0206e9

.field public static final twitter_refresh_icon:I = 0x7f0206ea

.field public static final undo_remind_btn_normal:I = 0x7f0206eb

.field public static final undo_remind_btn_press:I = 0x7f0206ec

.field public static final undo_reminder_stateful:I = 0x7f0206ed

.field public static final v1_step_01_bg:I = 0x7f0206ee

.field public static final v2_step_01_bg:I = 0x7f0206ef

.field public static final v2_step_02_bg:I = 0x7f0206f0

.field public static final v2_step_03_bg:I = 0x7f0206f1

.field public static final v3_step_01_bg:I = 0x7f0206f2

.field public static final video_expand:I = 0x7f0206f3

.field public static final video_pause_btn:I = 0x7f0206f4

.field public static final video_play_btn:I = 0x7f0206f5

.field public static final video_vol:I = 0x7f0206f6

.field public static final video_vol_mute:I = 0x7f0206f7

.field public static final vol_001:I = 0x7f0206f8

.field public static final vol_015:I = 0x7f0206f9

.field public static final vol_029:I = 0x7f0206fa

.field public static final volume_dn:I = 0x7f0206fb

.field public static final volume_up:I = 0x7f0206fc

.field public static final vpi__tab_indicator:I = 0x7f0206fd

.field public static final vpi__tab_selected_focused_holo:I = 0x7f0206fe

.field public static final vpi__tab_selected_holo:I = 0x7f0206ff

.field public static final vpi__tab_selected_pressed_holo:I = 0x7f020700

.field public static final vpi__tab_unselected_focused_holo:I = 0x7f020701

.field public static final vpi__tab_unselected_holo:I = 0x7f020702

.field public static final vpi__tab_unselected_pressed_holo:I = 0x7f020703

.field public static final watchon_small_btn:I = 0x7f020704

.field public static final watchon_small_btn_press:I = 0x7f020705

.field public static final white_cancel_icon:I = 0x7f020706

.field public static final white_carrot:I = 0x7f020707

.field public static final widget_btn_mute:I = 0x7f020708

.field public static final widget_btn_mute_press:I = 0x7f020709

.field public static final widget_btn_mute_select:I = 0x7f02070a

.field public static final widget_btn_normal:I = 0x7f02070b

.field public static final widget_btn_press:I = 0x7f02070c

.field public static final widget_btn_source:I = 0x7f02070d

.field public static final widget_btn_source_press:I = 0x7f02070e

.field public static final widget_btn_tv:I = 0x7f02070f

.field public static final widget_btn_tv_press:I = 0x7f020710

.field public static final widget_ch_down_icon:I = 0x7f020711

.field public static final widget_ch_down_normal:I = 0x7f020712

.field public static final widget_ch_down_press:I = 0x7f020713

.field public static final widget_ch_up_icon:I = 0x7f020714

.field public static final widget_ch_up_normal:I = 0x7f020715

.field public static final widget_ch_up_press:I = 0x7f020716

.field public static final widget_device_select_btn_focus:I = 0x7f020717

.field public static final widget_device_select_btn_press:I = 0x7f020718

.field public static final widget_device_select_btn_select:I = 0x7f020719

.field public static final widget_drop_down_stateful:I = 0x7f02071a

.field public static final widget_empty_states_bg:I = 0x7f02071b

.field public static final widget_expand_icon:I = 0x7f02071c

.field public static final widget_ff_icon:I = 0x7f02071d

.field public static final widget_left_arrow_normal:I = 0x7f02071e

.field public static final widget_left_arrow_press:I = 0x7f02071f

.field public static final widget_mute_icon:I = 0x7f020720

.field public static final widget_mute_stateful:I = 0x7f020721

.field public static final widget_play_pause_icon:I = 0x7f020722

.field public static final widget_play_pause_stateful:I = 0x7f020723

.field public static final widget_pw_icon:I = 0x7f020724

.field public static final widget_rewind_icon:I = 0x7f020725

.field public static final widget_right_arrow_normal:I = 0x7f020726

.field public static final widget_right_arrow_press:I = 0x7f020727

.field public static final widget_temp_down_stateful:I = 0x7f020728

.field public static final widget_temp_up_stateful:I = 0x7f020729

.field public static final widget_thumbnail:I = 0x7f02072a

.field public static final widget_vol_down_normal:I = 0x7f02072b

.field public static final widget_vol_down_press:I = 0x7f02072c

.field public static final widget_vol_up_normal:I = 0x7f02072d

.field public static final widget_vol_up_press:I = 0x7f02072e

.field public static final widget_wot_kitkat_psr_stateful:I = 0x7f02072f

.field public static final widget_wot_stateful:I = 0x7f020730

.field public static final wifi_logo:I = 0x7f020731

.field public static final wifi_logo_noconneted:I = 0x7f020732

.field public static final winset_select_activity_small:I = 0x7f020733

.field public static final winset_select_bg_small:I = 0x7f020734

.field public static final yosemite_icon_remocontroll:I = 0x7f020735
