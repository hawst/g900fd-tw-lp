.class Lcom/peel/ui/jl;
.super Landroid/widget/ArrayAdapter;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lcom/peel/control/h;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/peel/ui/gt;


# direct methods
.method constructor <init>(Lcom/peel/ui/gt;Landroid/content/Context;ILjava/util/List;)V
    .locals 0

    .prologue
    .line 3733
    iput-object p1, p0, Lcom/peel/ui/jl;->a:Lcom/peel/ui/gt;

    invoke-direct {p0, p2, p3, p4}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    return-void
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4

    .prologue
    .line 3737
    if-eqz p2, :cond_1

    .line 3739
    :goto_0
    sget v0, Lcom/peel/ui/fp;->text:I

    .line 3740
    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 3741
    invoke-super {p0}, Landroid/widget/ArrayAdapter;->getCount()I

    move-result v1

    if-ge p1, v1, :cond_0

    .line 3742
    invoke-virtual {p0, p1}, Lcom/peel/ui/jl;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/peel/control/h;

    .line 3743
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v3

    .line 3744
    invoke-virtual {v3}, Lcom/peel/data/g;->f()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/peel/ui/jl;->a:Lcom/peel/ui/gt;

    .line 3746
    invoke-virtual {v3}, Lcom/peel/ui/gt;->m()Landroid/support/v4/app/ae;

    move-result-object v3

    .line 3747
    invoke-virtual {v1}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/data/g;->d()I

    move-result v1

    .line 3746
    invoke-static {v3, v1}, Lcom/peel/util/bx;->a(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 3743
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 3749
    :cond_0
    return-object p2

    .line 3737
    :cond_1
    iget-object v0, p0, Lcom/peel/ui/jl;->a:Lcom/peel/ui/gt;

    .line 3738
    invoke-static {v0}, Lcom/peel/ui/gt;->w(Lcom/peel/ui/gt;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lcom/peel/ui/fq;->device_power_row:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    goto :goto_0
.end method
