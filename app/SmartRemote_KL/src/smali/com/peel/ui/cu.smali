.class Lcom/peel/ui/cu;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/peel/ui/cj;


# direct methods
.method constructor <init>(Lcom/peel/ui/cj;)V
    .locals 0

    .prologue
    .line 159
    iput-object p1, p0, Lcom/peel/ui/cu;->a:Lcom/peel/ui/cj;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 162
    iget-object v0, p0, Lcom/peel/ui/cu;->a:Lcom/peel/ui/cj;

    invoke-virtual {v0}, Lcom/peel/ui/cj;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    const-string/jumbo v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/ae;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 163
    sget v1, Lcom/peel/ui/fq;->about:I

    invoke-virtual {v0, v1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 164
    sget v0, Lcom/peel/ui/fp;->about_app_version:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v2, p0, Lcom/peel/ui/cu;->a:Lcom/peel/ui/cj;

    invoke-virtual {v2}, Lcom/peel/ui/cj;->m()Landroid/support/v4/app/ae;

    move-result-object v2

    invoke-static {v2}, Lcom/peel/util/bx;->g(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 165
    new-instance v0, Lcom/peel/ui/cv;

    invoke-direct {v0, p0}, Lcom/peel/ui/cv;-><init>(Lcom/peel/ui/cu;)V

    invoke-virtual {v1, v0}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 244
    new-instance v0, Lcom/peel/widget/ag;

    iget-object v2, p0, Lcom/peel/ui/cu;->a:Lcom/peel/ui/cj;

    invoke-virtual {v2}, Lcom/peel/ui/cj;->m()Landroid/support/v4/app/ae;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/peel/widget/ag;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0}, Lcom/peel/widget/ag;->b()Lcom/peel/widget/ag;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/peel/widget/ag;->a(Landroid/view/View;)Lcom/peel/widget/ag;

    move-result-object v0

    sget v1, Lcom/peel/ui/ft;->ok:I

    invoke-virtual {v0, v1, v3}, Lcom/peel/widget/ag;->a(ILandroid/content/DialogInterface$OnClickListener;)Lcom/peel/widget/ag;

    move-result-object v0

    .line 245
    invoke-virtual {v0}, Lcom/peel/widget/ag;->show()V

    .line 246
    return-void
.end method
