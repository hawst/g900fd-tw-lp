.class Lcom/peel/ui/hf;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/peel/widget/al;


# instance fields
.field final synthetic a:Lcom/peel/ui/gt;


# direct methods
.method constructor <init>(Lcom/peel/ui/gt;)V
    .locals 0

    .prologue
    .line 648
    iput-object p1, p0, Lcom/peel/ui/hf;->a:Lcom/peel/ui/gt;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 9

    .prologue
    const/high16 v4, 0x3f000000    # 0.5f

    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v8, 0x0

    .line 651
    new-instance v7, Landroid/view/animation/AlphaAnimation;

    const/high16 v0, 0x3f800000    # 1.0f

    invoke-direct {v7, v0, v2}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 652
    const-wide/16 v0, 0x2bc

    invoke-virtual {v7, v0, v1}, Landroid/view/animation/AlphaAnimation;->setDuration(J)V

    .line 653
    invoke-virtual {v7, v8}, Landroid/view/animation/AlphaAnimation;->setFillAfter(Z)V

    .line 655
    new-instance v0, Landroid/view/animation/RotateAnimation;

    const/high16 v1, 0x43340000    # 180.0f

    move v5, v3

    move v6, v4

    invoke-direct/range {v0 .. v6}, Landroid/view/animation/RotateAnimation;-><init>(FFIFIF)V

    .line 656
    const-wide/16 v4, 0x1f4

    invoke-virtual {v0, v4, v5}, Landroid/view/animation/RotateAnimation;->setDuration(J)V

    .line 657
    invoke-virtual {v0, v3}, Landroid/view/animation/RotateAnimation;->setFillAfter(Z)V

    .line 658
    iget-object v1, p0, Lcom/peel/ui/hf;->a:Lcom/peel/ui/gt;

    invoke-static {v1}, Lcom/peel/ui/gt;->r(Lcom/peel/ui/gt;)Landroid/widget/ImageView;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 660
    new-instance v0, Lcom/peel/ui/hg;

    invoke-direct {v0, p0}, Lcom/peel/ui/hg;-><init>(Lcom/peel/ui/hf;)V

    invoke-virtual {v7, v0}, Landroid/view/animation/AlphaAnimation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 682
    iget-object v0, p0, Lcom/peel/ui/hf;->a:Lcom/peel/ui/gt;

    invoke-static {v0}, Lcom/peel/ui/gt;->s(Lcom/peel/ui/gt;)Landroid/widget/FrameLayout;

    move-result-object v0

    invoke-virtual {v0, v7}, Landroid/widget/FrameLayout;->startAnimation(Landroid/view/animation/Animation;)V

    .line 684
    iget-object v0, p0, Lcom/peel/ui/hf;->a:Lcom/peel/ui/gt;

    invoke-static {v0, v8, v8}, Lcom/peel/ui/gt;->a(Lcom/peel/ui/gt;ZZ)V

    .line 685
    return-void
.end method
