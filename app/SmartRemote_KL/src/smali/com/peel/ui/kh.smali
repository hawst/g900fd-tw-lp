.class public Lcom/peel/ui/kh;
.super Landroid/widget/ArrayAdapter;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<[",
        "Lcom/peel/content/listing/Listing;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Landroid/view/LayoutInflater;


# direct methods
.method public constructor <init>(Landroid/content/Context;I[[Lcom/peel/content/listing/Listing;)V
    .locals 1

    .prologue
    .line 31
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    .line 32
    iput-object p1, p0, Lcom/peel/ui/kh;->a:Landroid/content/Context;

    .line 33
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/ui/kh;->b:Landroid/view/LayoutInflater;

    .line 34
    return-void
.end method


# virtual methods
.method public final getItemId(I)J
    .locals 2

    .prologue
    .line 39
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 54
    invoke-virtual {p0, p1}, Lcom/peel/ui/kh;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/peel/content/listing/Listing;

    .line 55
    invoke-virtual {p0, p1}, Lcom/peel/ui/kh;->getItemViewType(I)I

    move-result v4

    .line 56
    if-nez p2, :cond_0

    iget-object v1, p0, Lcom/peel/ui/kh;->b:Landroid/view/LayoutInflater;

    sget v2, Lcom/peel/ui/fq;->searched_item:I

    invoke-virtual {v1, v2, p3, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 58
    :cond_0
    sget v1, Lcom/peel/ui/fp;->title:I

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 62
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v2

    if-nez v2, :cond_3

    .line 63
    new-instance v3, Lcom/peel/ui/ki;

    invoke-direct {v3}, Lcom/peel/ui/ki;-><init>()V

    .line 64
    sget v2, Lcom/peel/ui/fp;->caption:I

    invoke-virtual {p2, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, v3, Lcom/peel/ui/ki;->a:Landroid/widget/ImageView;

    .line 66
    invoke-virtual {p2, v3}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 71
    :goto_0
    aget-object v5, v0, v8

    const/4 v2, 0x1

    if-ne v2, v4, :cond_4

    const/16 v2, 0x2b

    :goto_1
    invoke-static {v5, v2}, Lcom/peel/util/bx;->a(Lcom/peel/content/listing/Listing;I)I

    move-result v4

    .line 73
    iget-object v2, v3, Lcom/peel/ui/ki;->a:Landroid/widget/ImageView;

    invoke-virtual {v2, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 74
    aget-object v2, v0, v8

    check-cast v2, Lcom/peel/content/listing/LiveListing;

    invoke-virtual {v2}, Lcom/peel/content/listing/LiveListing;->y()Ljava/lang/String;

    move-result-object v2

    .line 76
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 77
    const/4 v5, 0x3

    const/4 v6, 0x4

    const/16 v7, 0x10e

    aget-object v2, v0, v8

    check-cast v2, Lcom/peel/content/listing/LiveListing;

    invoke-virtual {v2}, Lcom/peel/content/listing/LiveListing;->B()Ljava/util/Map;

    move-result-object v2

    invoke-static {v5, v6, v7, v2}, Lcom/peel/util/bx;->a(IIILjava/util/Map;)Ljava/lang/String;

    move-result-object v2

    .line 80
    :cond_1
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 81
    iget-object v5, p0, Lcom/peel/ui/kh;->a:Landroid/content/Context;

    invoke-static {v5}, Lcom/peel/util/b/e;->a(Landroid/content/Context;)Lcom/squareup/picasso/Picasso;

    move-result-object v5

    invoke-virtual {v5, v2}, Lcom/squareup/picasso/Picasso;->load(Ljava/lang/String;)Lcom/squareup/picasso/RequestCreator;

    move-result-object v2

    invoke-virtual {v2, v4}, Lcom/squareup/picasso/RequestCreator;->placeholder(I)Lcom/squareup/picasso/RequestCreator;

    move-result-object v2

    iget-object v3, v3, Lcom/peel/ui/ki;->a:Landroid/widget/ImageView;

    invoke-virtual {v2, v3}, Lcom/squareup/picasso/RequestCreator;->into(Landroid/widget/ImageView;)V

    .line 84
    :cond_2
    aget-object v0, v0, v8

    invoke-static {v1, v0}, Lcom/peel/util/bx;->a(Landroid/widget/TextView;Lcom/peel/content/listing/Listing;)V

    .line 86
    return-object p2

    .line 68
    :cond_3
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/peel/ui/ki;

    move-object v3, v2

    goto :goto_0

    .line 71
    :cond_4
    const/16 v2, 0xa9

    goto :goto_1
.end method

.method public getViewTypeCount()I
    .locals 1

    .prologue
    .line 43
    const/4 v0, 0x4

    return v0
.end method

.method public hasStableIds()Z
    .locals 1

    .prologue
    .line 37
    const/4 v0, 0x1

    return v0
.end method

.method public isEnabled(I)Z
    .locals 1

    .prologue
    .line 48
    invoke-super {p0}, Landroid/widget/ArrayAdapter;->getCount()I

    move-result v0

    if-ge p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public notifyDataSetChanged()V
    .locals 0

    .prologue
    .line 36
    invoke-super {p0}, Landroid/widget/ArrayAdapter;->notifyDataSetChanged()V

    return-void
.end method
