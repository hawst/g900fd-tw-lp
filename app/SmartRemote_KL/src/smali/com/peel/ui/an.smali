.class Lcom/peel/ui/an;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field final synthetic a:[Lcom/peel/data/ContentRoom;

.field final synthetic b:Lcom/peel/ui/ai;


# direct methods
.method constructor <init>(Lcom/peel/ui/ai;[Lcom/peel/data/ContentRoom;)V
    .locals 0

    .prologue
    .line 480
    iput-object p1, p0, Lcom/peel/ui/an;->b:Lcom/peel/ui/ai;

    iput-object p2, p0, Lcom/peel/ui/an;->a:[Lcom/peel/data/ContentRoom;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 483
    sget-object v0, Lcom/peel/content/a;->h:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/peel/ui/an;->b:Lcom/peel/ui/ai;

    invoke-static {v0}, Lcom/peel/ui/ai;->b(Lcom/peel/ui/ai;)Lcom/peel/d/i;

    move-result-object v0

    if-nez v0, :cond_1

    .line 494
    :cond_0
    :goto_0
    return-void

    .line 485
    :cond_1
    iget-object v0, p0, Lcom/peel/ui/an;->b:Lcom/peel/ui/ai;

    iget-object v0, v0, Lcom/peel/ui/ai;->a:Lcom/peel/widget/ag;

    if-eqz v0, :cond_2

    .line 486
    iget-object v0, p0, Lcom/peel/ui/an;->b:Lcom/peel/ui/ai;

    iget-object v0, v0, Lcom/peel/ui/ai;->a:Lcom/peel/widget/ag;

    invoke-virtual {v0}, Lcom/peel/widget/ag;->dismiss()V

    .line 488
    :cond_2
    invoke-virtual {p1}, Landroid/widget/AdapterView;->getAdapter()Landroid/widget/Adapter;

    move-result-object v0

    invoke-interface {v0}, Landroid/widget/Adapter;->getCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-ne v0, p3, :cond_3

    .line 489
    iget-object v0, p0, Lcom/peel/ui/an;->b:Lcom/peel/ui/ai;

    invoke-virtual {v0}, Lcom/peel/ui/ai;->e()V

    goto :goto_0

    .line 491
    :cond_3
    iget-object v0, p0, Lcom/peel/ui/an;->b:Lcom/peel/ui/ai;

    const/4 v1, -0x1

    invoke-static {v0, v1}, Lcom/peel/ui/ai;->a(Lcom/peel/ui/ai;I)I

    .line 492
    iget-object v0, p0, Lcom/peel/ui/an;->a:[Lcom/peel/data/ContentRoom;

    aget-object v0, v0, p3

    invoke-virtual {v0}, Lcom/peel/data/ContentRoom;->a()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-static {v0, v1, v2, v3}, Lcom/peel/content/a;->a(Ljava/lang/String;ZZLcom/peel/util/t;)V

    goto :goto_0
.end method
