.class Lcom/peel/ui/ja;
.super Landroid/widget/ArrayAdapter;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lcom/peel/control/h;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/peel/ui/iz;


# direct methods
.method constructor <init>(Lcom/peel/ui/iz;Landroid/content/Context;ILjava/util/List;)V
    .locals 0

    .prologue
    .line 3218
    iput-object p1, p0, Lcom/peel/ui/ja;->a:Lcom/peel/ui/iz;

    invoke-direct {p0, p2, p3, p4}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    return-void
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 5

    .prologue
    .line 3222
    if-eqz p2, :cond_0

    .line 3227
    :goto_0
    sget v0, Lcom/peel/ui/fp;->settings_activity_tv:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 3229
    iget-object v1, p0, Lcom/peel/ui/ja;->a:Lcom/peel/ui/iz;

    iget-object v1, v1, Lcom/peel/ui/iz;->a:Lcom/peel/ui/gt;

    invoke-static {v1}, Lcom/peel/ui/gt;->V(Lcom/peel/ui/gt;)Ljava/util/ArrayList;

    move-result-object v1

    .line 3230
    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/peel/control/h;

    .line 3231
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 3232
    invoke-virtual {v1}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v3

    invoke-virtual {v3}, Lcom/peel/data/g;->f()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/peel/ui/ja;->a:Lcom/peel/ui/iz;

    iget-object v3, v3, Lcom/peel/ui/iz;->a:Lcom/peel/ui/gt;

    .line 3236
    invoke-virtual {v3}, Lcom/peel/ui/gt;->m()Landroid/support/v4/app/ae;

    move-result-object v3

    .line 3238
    invoke-virtual {v1}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v4

    .line 3239
    invoke-virtual {v4}, Lcom/peel/data/g;->d()I

    move-result v4

    .line 3235
    invoke-static {v3, v4}, Lcom/peel/util/bx;->a(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 3231
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 3241
    invoke-virtual {p2, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 3243
    return-object p2

    .line 3222
    :cond_0
    iget-object v0, p0, Lcom/peel/ui/ja;->a:Lcom/peel/ui/iz;

    iget-object v0, v0, Lcom/peel/ui/iz;->a:Lcom/peel/ui/gt;

    .line 3223
    invoke-static {v0}, Lcom/peel/ui/gt;->w(Lcom/peel/ui/gt;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lcom/peel/ui/fq;->settings_adddevice_activity_item:I

    const/4 v2, 0x0

    .line 3224
    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    goto :goto_0
.end method
