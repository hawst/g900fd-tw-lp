.class Lcom/peel/ui/gu;
.super Lcom/peel/util/s;


# instance fields
.field final synthetic a:Lcom/peel/ui/gt;


# direct methods
.method constructor <init>(Lcom/peel/ui/gt;)V
    .locals 0

    .prologue
    .line 183
    iput-object p1, p0, Lcom/peel/ui/gu;->a:Lcom/peel/ui/gt;

    invoke-direct {p0}, Lcom/peel/util/s;-><init>()V

    return-void
.end method


# virtual methods
.method public final varargs a(ILjava/lang/Object;[Ljava/lang/Object;)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 186
    sparse-switch p1, :sswitch_data_0

    .line 212
    :goto_0
    return-void

    .line 188
    :sswitch_0
    iget-object v0, p0, Lcom/peel/ui/gu;->a:Lcom/peel/ui/gt;

    invoke-static {v0}, Lcom/peel/ui/gt;->a(Lcom/peel/ui/gt;)Lcom/peel/control/a;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/peel/ui/gu;->a:Lcom/peel/ui/gt;

    invoke-static {v0}, Lcom/peel/ui/gt;->a(Lcom/peel/ui/gt;)Lcom/peel/control/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/control/a;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/ui/gu;->a:Lcom/peel/ui/gt;

    invoke-static {v1}, Lcom/peel/ui/gt;->b(Lcom/peel/ui/gt;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 189
    iget-object v0, p0, Lcom/peel/ui/gu;->a:Lcom/peel/ui/gt;

    invoke-static {v0}, Lcom/peel/ui/gt;->a(Lcom/peel/ui/gt;)Lcom/peel/control/a;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/ui/gu;->a:Lcom/peel/ui/gt;

    invoke-static {v1}, Lcom/peel/ui/gt;->c(Lcom/peel/ui/gt;)Lcom/peel/util/s;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/peel/control/a;->b(Lcom/peel/util/s;)V

    .line 192
    :cond_0
    iget-object v0, p0, Lcom/peel/ui/gu;->a:Lcom/peel/ui/gt;

    iget-object v0, v0, Lcom/peel/ui/gt;->c:Landroid/os/Bundle;

    .line 193
    const-string/jumbo v1, "refresh"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 194
    const-string/jumbo v1, "activityId"

    check-cast p2, Lcom/peel/control/a;

    invoke-virtual {p2}, Lcom/peel/control/a;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 196
    invoke-static {}, Lcom/peel/ui/gt;->S()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "updating ControlPad"

    new-instance v3, Lcom/peel/ui/gv;

    invoke-direct {v3, p0, v0}, Lcom/peel/ui/gv;-><init>(Lcom/peel/ui/gu;Landroid/os/Bundle;)V

    invoke-static {v1, v2, v3}, Lcom/peel/util/i;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/lang/Runnable;

    goto :goto_0

    .line 204
    :sswitch_1
    iget-object v0, p0, Lcom/peel/ui/gu;->a:Lcom/peel/ui/gt;

    invoke-static {v0}, Lcom/peel/ui/gt;->a(Lcom/peel/ui/gt;)Lcom/peel/control/a;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/peel/ui/gu;->a:Lcom/peel/ui/gt;

    .line 205
    invoke-static {v0}, Lcom/peel/ui/gt;->a(Lcom/peel/ui/gt;)Lcom/peel/control/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/control/a;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/ui/gu;->a:Lcom/peel/ui/gt;

    invoke-static {v1}, Lcom/peel/ui/gt;->b(Lcom/peel/ui/gt;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 206
    iget-object v0, p0, Lcom/peel/ui/gu;->a:Lcom/peel/ui/gt;

    invoke-static {v0}, Lcom/peel/ui/gt;->a(Lcom/peel/ui/gt;)Lcom/peel/control/a;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/ui/gu;->a:Lcom/peel/ui/gt;

    invoke-static {v1}, Lcom/peel/ui/gt;->c(Lcom/peel/ui/gt;)Lcom/peel/util/s;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/peel/control/a;->b(Lcom/peel/util/s;)V

    .line 208
    :cond_1
    iget-object v0, p0, Lcom/peel/ui/gu;->a:Lcom/peel/ui/gt;

    invoke-static {v0, v2}, Lcom/peel/ui/gt;->a(Lcom/peel/ui/gt;Lcom/peel/control/a;)Lcom/peel/control/a;

    .line 209
    iget-object v0, p0, Lcom/peel/ui/gu;->a:Lcom/peel/ui/gt;

    invoke-static {v0, v2}, Lcom/peel/ui/gt;->a(Lcom/peel/ui/gt;Lcom/peel/control/h;)Lcom/peel/control/h;

    goto/16 :goto_0

    .line 186
    :sswitch_data_0
    .sparse-switch
        0xa -> :sswitch_1
        0x14 -> :sswitch_0
    .end sparse-switch
.end method
