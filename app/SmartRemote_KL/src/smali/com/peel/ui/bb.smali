.class Lcom/peel/ui/bb;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:I

.field final synthetic b:Lcom/peel/ui/az;


# direct methods
.method constructor <init>(Lcom/peel/ui/az;I)V
    .locals 0

    .prologue
    .line 83
    iput-object p1, p0, Lcom/peel/ui/bb;->b:Lcom/peel/ui/az;

    iput p2, p0, Lcom/peel/ui/bb;->a:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 10

    .prologue
    const/16 v3, 0x7dd

    const/4 v9, 0x1

    const/4 v5, 0x0

    .line 86
    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    sget-object v1, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v1}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/peel/control/am;->c(Lcom/peel/control/RoomControl;)[Lcom/peel/control/h;

    move-result-object v0

    .line 87
    if-eqz v0, :cond_0

    array-length v0, v0

    if-nez v0, :cond_2

    .line 88
    :cond_0
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 89
    const-string/jumbo v1, "passback_clazz"

    iget-object v2, p0, Lcom/peel/ui/bb;->b:Lcom/peel/ui/az;

    invoke-static {v2}, Lcom/peel/ui/az;->a(Lcom/peel/ui/az;)Landroid/support/v4/app/ae;

    move-result-object v2

    invoke-static {v2}, Lcom/peel/d/e;->a(Landroid/support/v4/app/ae;)Lcom/peel/d/u;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 90
    const-string/jumbo v1, "passback_bundle"

    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 91
    iget-object v1, p0, Lcom/peel/ui/bb;->b:Lcom/peel/ui/az;

    invoke-static {v1}, Lcom/peel/ui/az;->a(Lcom/peel/ui/az;)Landroid/support/v4/app/ae;

    move-result-object v1

    const-class v2, Lcom/peel/i/dr;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lcom/peel/d/e;->c(Landroid/support/v4/app/ae;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 109
    :cond_1
    :goto_0
    return-void

    .line 94
    :cond_2
    iget-object v0, p0, Lcom/peel/ui/bb;->b:Lcom/peel/ui/az;

    iget-boolean v0, v0, Lcom/peel/ui/az;->d:Z

    if-nez v0, :cond_1

    .line 95
    iget-object v0, p0, Lcom/peel/ui/bb;->b:Lcom/peel/ui/az;

    invoke-static {v0, p1}, Lcom/peel/ui/az;->a(Lcom/peel/ui/az;Landroid/view/View;)V

    .line 96
    iget-object v0, p0, Lcom/peel/ui/bb;->b:Lcom/peel/ui/az;

    invoke-static {v0}, Lcom/peel/ui/az;->b(Lcom/peel/ui/az;)Ljava/util/List;

    move-result-object v0

    iget v1, p0, Lcom/peel/ui/bb;->a:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lcom/peel/data/Channel;

    .line 97
    iget-object v0, p0, Lcom/peel/ui/bb;->b:Lcom/peel/ui/az;

    invoke-static {v0}, Lcom/peel/ui/az;->a(Lcom/peel/ui/az;)Landroid/support/v4/app/ae;

    move-result-object v0

    invoke-virtual {v8}, Lcom/peel/data/Channel;->m()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/peel/util/bx;->e(Landroid/content/Context;Ljava/lang/String;)V

    .line 98
    iget-object v0, p0, Lcom/peel/ui/bb;->b:Lcom/peel/ui/az;

    invoke-static {v0, v8}, Lcom/peel/ui/az;->a(Lcom/peel/ui/az;Lcom/peel/data/Channel;)V

    .line 99
    invoke-static {}, Lcom/peel/util/a/f;->a()Lcom/peel/util/a/f;

    move-result-object v0

    sget-object v1, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    .line 100
    invoke-virtual {v1}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v1

    if-nez v1, :cond_3

    move v1, v9

    :goto_1
    const/16 v2, 0x419

    .line 102
    invoke-virtual {v8}, Lcom/peel/data/Channel;->m()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v8}, Lcom/peel/data/Channel;->b()Ljava/lang/String;

    move-result-object v6

    move v7, v5

    .line 99
    invoke-virtual/range {v0 .. v7}, Lcom/peel/util/a/f;->a(IIILjava/lang/String;ILjava/lang/String;I)V

    .line 104
    invoke-static {}, Lcom/peel/util/a/f;->a()Lcom/peel/util/a/f;

    move-result-object v0

    sget-object v1, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v1}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v1

    if-nez v1, :cond_4

    move v1, v9

    :goto_2
    const/16 v2, 0x3f3

    .line 106
    invoke-virtual {v8}, Lcom/peel/data/Channel;->e()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v8}, Lcom/peel/data/Channel;->b()Ljava/lang/String;

    move-result-object v6

    move v7, v5

    .line 104
    invoke-virtual/range {v0 .. v7}, Lcom/peel/util/a/f;->a(IIILjava/lang/String;ILjava/lang/String;I)V

    goto :goto_0

    .line 100
    :cond_3
    sget-object v1, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v1}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/data/at;->f()I

    move-result v1

    goto :goto_1

    .line 104
    :cond_4
    sget-object v1, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v1}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/data/at;->f()I

    move-result v1

    goto :goto_2
.end method
