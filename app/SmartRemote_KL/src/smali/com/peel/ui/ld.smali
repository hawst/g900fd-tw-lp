.class Lcom/peel/ui/ld;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/content/DialogInterface$OnDismissListener;


# instance fields
.field final synthetic a:Lcom/peel/ui/kr;


# direct methods
.method constructor <init>(Lcom/peel/ui/kr;)V
    .locals 0

    .prologue
    .line 463
    iput-object p1, p0, Lcom/peel/ui/ld;->a:Lcom/peel/ui/kr;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDismiss(Landroid/content/DialogInterface;)V
    .locals 2

    .prologue
    .line 466
    iget-object v0, p0, Lcom/peel/ui/ld;->a:Lcom/peel/ui/kr;

    invoke-virtual {v0}, Lcom/peel/ui/kr;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    const-string/jumbo v1, "edit_channel_popup_trigger"

    invoke-static {v0, v1}, Lcom/peel/util/dq;->b(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 467
    iget-object v0, p0, Lcom/peel/ui/ld;->a:Lcom/peel/ui/kr;

    invoke-static {v0}, Lcom/peel/ui/kr;->b(Lcom/peel/ui/kr;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getRootView()Landroid/view/View;

    move-result-object v0

    sget v1, Lcom/peel/ui/fp;->triggers_placeholder:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 468
    if-eqz v0, :cond_0

    .line 469
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 472
    :cond_0
    return-void
.end method
