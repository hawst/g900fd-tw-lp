.class public Lcom/peel/ui/du;
.super Lcom/peel/d/u;


# instance fields
.field private aA:Lcom/peel/ui/a/a;

.field private aB:I

.field private aC:Z

.field private final aD:Landroid/support/v4/view/cs;

.field private aE:Ljava/lang/Runnable;

.field private aj:Landroid/view/LayoutInflater;

.field private ak:Landroid/view/View;

.field private al:Landroid/view/View;

.field private am:Landroid/view/View;

.field private an:Landroid/support/v4/view/ViewPager;

.field private ao:Landroid/widget/TextView;

.field private ap:Landroid/widget/TextView;

.field private aq:Lcom/peel/widget/ObservableListView;

.field private ar:Lcom/peel/ui/a/q;

.field private as:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/os/Bundle;",
            ">;"
        }
    .end annotation
.end field

.field private at:Ljava/util/Date;

.field private au:I

.field private av:Ljava/util/Timer;

.field private aw:Ljava/util/TimerTask;

.field private ax:I

.field private ay:Landroid/widget/RelativeLayout;

.field private az:Z

.field e:Lcom/peel/ui/ar;

.field f:Landroid/view/View;

.field g:Landroid/widget/ListView;

.field h:Z

.field i:Landroid/os/Handler;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 58
    invoke-direct {p0}, Lcom/peel/d/u;-><init>()V

    .line 66
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/peel/ui/du;->as:Ljava/util/ArrayList;

    .line 68
    iput v1, p0, Lcom/peel/ui/du;->au:I

    .line 69
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/peel/ui/du;->e:Lcom/peel/ui/ar;

    .line 76
    const/4 v0, -0x1

    iput v0, p0, Lcom/peel/ui/du;->aB:I

    .line 84
    new-instance v0, Lcom/peel/ui/dv;

    invoke-direct {v0, p0}, Lcom/peel/ui/dv;-><init>(Lcom/peel/ui/du;)V

    iput-object v0, p0, Lcom/peel/ui/du;->aD:Landroid/support/v4/view/cs;

    .line 526
    iput-boolean v1, p0, Lcom/peel/ui/du;->h:Z

    .line 596
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/peel/ui/du;->i:Landroid/os/Handler;

    .line 597
    new-instance v0, Lcom/peel/ui/dx;

    invoke-direct {v0, p0}, Lcom/peel/ui/dx;-><init>(Lcom/peel/ui/du;)V

    iput-object v0, p0, Lcom/peel/ui/du;->aE:Ljava/lang/Runnable;

    return-void
.end method

.method static synthetic a(Lcom/peel/ui/du;)I
    .locals 1

    .prologue
    .line 58
    iget v0, p0, Lcom/peel/ui/du;->au:I

    return v0
.end method

.method static synthetic a(Lcom/peel/ui/du;I)I
    .locals 0

    .prologue
    .line 58
    iput p1, p0, Lcom/peel/ui/du;->au:I

    return p1
.end method

.method private a(Landroid/view/View;)V
    .locals 4

    .prologue
    .line 533
    iput-object p1, p0, Lcom/peel/ui/du;->f:Landroid/view/View;

    .line 534
    iget-object v0, p0, Lcom/peel/ui/du;->f:Landroid/view/View;

    sget v1, Lcom/peel/ui/fm;->common_signin_btn_dark_text_disabled:I

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    .line 535
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/peel/ui/du;->h:Z

    .line 536
    iget-object v0, p0, Lcom/peel/ui/du;->f:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/peel/ui/du;->g:Landroid/widget/ListView;

    .line 537
    iget-object v0, p0, Lcom/peel/ui/du;->g:Landroid/widget/ListView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setEnabled(Z)V

    .line 539
    iget-object v0, p0, Lcom/peel/ui/du;->i:Landroid/os/Handler;

    iget-object v1, p0, Lcom/peel/ui/du;->aE:Ljava/lang/Runnable;

    const-wide/16 v2, 0xbb8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 540
    return-void
.end method

.method static synthetic a(Lcom/peel/ui/du;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 58
    invoke-direct {p0, p1}, Lcom/peel/ui/du;->a(Landroid/view/View;)V

    return-void
.end method

.method static synthetic a(Lcom/peel/ui/du;Z)Z
    .locals 0

    .prologue
    .line 58
    iput-boolean p1, p0, Lcom/peel/ui/du;->aC:Z

    return p1
.end method

.method static synthetic b(Lcom/peel/ui/du;I)I
    .locals 0

    .prologue
    .line 58
    iput p1, p0, Lcom/peel/ui/du;->aB:I

    return p1
.end method

.method static synthetic b(Lcom/peel/ui/du;)Landroid/support/v4/view/ViewPager;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/peel/ui/du;->an:Landroid/support/v4/view/ViewPager;

    return-object v0
.end method

.method static synthetic c(Lcom/peel/ui/du;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/peel/ui/du;->a:Ljava/lang/String;

    return-object v0
.end method

.method private c()V
    .locals 2

    .prologue
    .line 590
    iget-object v0, p0, Lcom/peel/ui/du;->f:Landroid/view/View;

    sget v1, Lcom/peel/ui/fm;->common_signin_btn_dark_text_default:I

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    .line 591
    iget-object v0, p0, Lcom/peel/ui/du;->g:Landroid/widget/ListView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setEnabled(Z)V

    .line 592
    const/4 v0, -0x1

    iput v0, p0, Lcom/peel/ui/du;->aB:I

    .line 593
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/peel/ui/du;->h:Z

    .line 594
    return-void
.end method

.method static synthetic d(Lcom/peel/ui/du;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/peel/ui/du;->a:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic e(Lcom/peel/ui/du;)Z
    .locals 1

    .prologue
    .line 58
    iget-boolean v0, p0, Lcom/peel/ui/du;->aC:Z

    return v0
.end method

.method static synthetic f(Lcom/peel/ui/du;)Lcom/peel/ui/a/q;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/peel/ui/du;->ar:Lcom/peel/ui/a/q;

    return-object v0
.end method

.method static synthetic g(Lcom/peel/ui/du;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/peel/ui/du;->a:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic h(Lcom/peel/ui/du;)I
    .locals 1

    .prologue
    .line 58
    iget v0, p0, Lcom/peel/ui/du;->ax:I

    return v0
.end method

.method static synthetic i(Lcom/peel/ui/du;)Landroid/support/v4/view/cs;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/peel/ui/du;->aD:Landroid/support/v4/view/cs;

    return-object v0
.end method

.method static synthetic j(Lcom/peel/ui/du;)Lcom/peel/widget/ObservableListView;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/peel/ui/du;->aq:Lcom/peel/widget/ObservableListView;

    return-object v0
.end method

.method static synthetic k(Lcom/peel/ui/du;)Landroid/view/LayoutInflater;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/peel/ui/du;->aj:Landroid/view/LayoutInflater;

    return-object v0
.end method

.method static synthetic l(Lcom/peel/ui/du;)I
    .locals 1

    .prologue
    .line 58
    iget v0, p0, Lcom/peel/ui/du;->aB:I

    return v0
.end method

.method static synthetic m(Lcom/peel/ui/du;)Landroid/widget/RelativeLayout;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/peel/ui/du;->ay:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method static synthetic n(Lcom/peel/ui/du;)V
    .locals 0

    .prologue
    .line 58
    invoke-direct {p0}, Lcom/peel/ui/du;->c()V

    return-void
.end method


# virtual methods
.method public X()Z
    .locals 1

    .prologue
    .line 81
    iget v0, p0, Lcom/peel/ui/du;->au:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public Z()V
    .locals 7

    .prologue
    .line 551
    iget-object v0, p0, Lcom/peel/ui/du;->a:Ljava/lang/String;

    const-string/jumbo v1, "updateABConfigOnBack"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 552
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/peel/ui/du;->aC:Z

    .line 553
    iget-object v0, p0, Lcom/peel/ui/du;->d:Lcom/peel/d/a;

    if-nez v0, :cond_2

    .line 554
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 555
    sget v0, Lcom/peel/ui/fp;->menu_remote:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 557
    sget-object v0, Lcom/peel/c/a;->b:Lcom/peel/c/e;

    invoke-static {v0}, Lcom/peel/c/a;->a(Lcom/peel/c/e;)Ljava/lang/Object;

    move-result-object v0

    sget-object v1, Lcom/peel/c/b;->a:Lcom/peel/c/b;

    if-ne v0, v1, :cond_1

    .line 558
    sget v0, Lcom/peel/ui/fp;->menu_change_room:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 559
    sget v0, Lcom/peel/ui/fp;->menu_channels:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 560
    sget v0, Lcom/peel/ui/fp;->menu_settings:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 561
    sget v0, Lcom/peel/ui/fp;->menu_about:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 566
    :goto_0
    iget-object v6, p0, Lcom/peel/ui/du;->b:Lcom/peel/d/i;

    new-instance v0, Lcom/peel/d/a;

    sget-object v1, Lcom/peel/d/d;->b:Lcom/peel/d/d;

    sget-object v2, Lcom/peel/d/b;->a:Lcom/peel/d/b;

    sget-object v3, Lcom/peel/d/c;->b:Lcom/peel/d/c;

    sget v4, Lcom/peel/ui/ft;->guide:I

    invoke-virtual {p0, v4}, Lcom/peel/ui/du;->a(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct/range {v0 .. v5}, Lcom/peel/d/a;-><init>(Lcom/peel/d/d;Lcom/peel/d/b;Lcom/peel/d/c;Ljava/lang/String;Ljava/util/List;)V

    invoke-virtual {v6, v0}, Lcom/peel/d/i;->a(Lcom/peel/d/a;)V

    .line 572
    :goto_1
    sget-object v0, Lcom/peel/b/a;->h:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 573
    sget-object v1, Lcom/peel/b/a;->h:Ljava/lang/String;

    iget-object v0, p0, Lcom/peel/ui/du;->an:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->getAdapter()Landroid/support/v4/view/av;

    move-result-object v0

    check-cast v0, Lcom/peel/ui/ek;

    invoke-virtual {v0}, Lcom/peel/ui/ek;->d()[Lcom/peel/data/Channel;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/peel/util/bx;->a(Ljava/lang/String;[Lcom/peel/data/Channel;)I

    move-result v0

    .line 574
    iget-object v1, p0, Lcom/peel/ui/du;->ar:Lcom/peel/ui/a/q;

    invoke-virtual {v1, v0}, Lcom/peel/ui/a/q;->b(I)V

    .line 575
    iget-object v1, p0, Lcom/peel/ui/du;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "force selection 2 from iIndex: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 578
    :cond_0
    return-void

    .line 563
    :cond_1
    sget v0, Lcom/peel/ui/fp;->overflow_menu_btn:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 569
    :cond_2
    iget-object v0, p0, Lcom/peel/ui/du;->b:Lcom/peel/d/i;

    iget-object v1, p0, Lcom/peel/ui/du;->d:Lcom/peel/d/a;

    invoke-virtual {v0, v1}, Lcom/peel/d/i;->a(Lcom/peel/d/a;)V

    goto :goto_1
.end method

.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 122
    iput-boolean v2, p0, Lcom/peel/ui/du;->aC:Z

    .line 123
    iput-object p1, p0, Lcom/peel/ui/du;->aj:Landroid/view/LayoutInflater;

    .line 125
    sget v0, Lcom/peel/ui/fq;->epg:I

    invoke-virtual {p1, v0, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/ui/du;->ak:Landroid/view/View;

    .line 126
    iget-object v0, p0, Lcom/peel/ui/du;->ak:Landroid/view/View;

    sget v1, Lcom/peel/ui/fp;->channels_list:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/peel/widget/ObservableListView;

    iput-object v0, p0, Lcom/peel/ui/du;->aq:Lcom/peel/widget/ObservableListView;

    .line 127
    iget-object v0, p0, Lcom/peel/ui/du;->ak:Landroid/view/View;

    sget v1, Lcom/peel/ui/fp;->epg_pager:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ViewPager;

    iput-object v0, p0, Lcom/peel/ui/du;->an:Landroid/support/v4/view/ViewPager;

    .line 128
    iget-object v0, p0, Lcom/peel/ui/du;->an:Landroid/support/v4/view/ViewPager;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setOffscreenPageLimit(I)V

    .line 129
    iget-object v0, p0, Lcom/peel/ui/du;->ak:Landroid/view/View;

    sget v1, Lcom/peel/ui/fp;->date_label:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/peel/ui/du;->ao:Landroid/widget/TextView;

    .line 130
    iget-object v0, p0, Lcom/peel/ui/du;->ak:Landroid/view/View;

    sget v1, Lcom/peel/ui/fp;->date_label_small:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/peel/ui/du;->ap:Landroid/widget/TextView;

    .line 131
    iget-object v0, p0, Lcom/peel/ui/du;->ak:Landroid/view/View;

    sget v1, Lcom/peel/ui/fp;->prev_time_btn:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/ui/du;->al:Landroid/view/View;

    .line 132
    iget-object v0, p0, Lcom/peel/ui/du;->ak:Landroid/view/View;

    sget v1, Lcom/peel/ui/fp;->next_time_btn:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/ui/du;->am:Landroid/view/View;

    .line 133
    iget-object v0, p0, Lcom/peel/ui/du;->ak:Landroid/view/View;

    sget v1, Lcom/peel/ui/fp;->triggers_placeholder:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/peel/ui/du;->ay:Landroid/widget/RelativeLayout;

    .line 134
    new-instance v0, Lcom/peel/ui/a/a;

    invoke-virtual {p0}, Lcom/peel/ui/du;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/peel/ui/a/a;-><init>(Landroid/app/Activity;)V

    iput-object v0, p0, Lcom/peel/ui/du;->aA:Lcom/peel/ui/a/a;

    .line 137
    invoke-virtual {p0}, Lcom/peel/ui/du;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    const-string/jumbo v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/ae;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 138
    invoke-virtual {p0}, Lcom/peel/ui/du;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/ae;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 140
    iget-object v0, p0, Lcom/peel/ui/du;->aq:Lcom/peel/widget/ObservableListView;

    new-instance v1, Lcom/peel/ui/dy;

    invoke-direct {v1, p0}, Lcom/peel/ui/dy;-><init>(Lcom/peel/ui/du;)V

    invoke-virtual {v0, v1}, Lcom/peel/widget/ObservableListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 153
    iget-object v0, p0, Lcom/peel/ui/du;->ak:Landroid/view/View;

    return-object v0
.end method

.method public a()Ljava/lang/String;
    .locals 2

    .prologue
    .line 544
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/peel/ui/du;->aC:Z

    .line 545
    iget-object v0, p0, Lcom/peel/ui/du;->a:Ljava/lang/String;

    const-string/jumbo v1, "backTo"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 546
    invoke-super {p0}, Lcom/peel/d/u;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public b()Z
    .locals 3

    .prologue
    const v2, 0x800003

    .line 489
    iget-object v0, p0, Lcom/peel/ui/du;->a:Ljava/lang/String;

    const-string/jumbo v1, "back"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 490
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/peel/ui/du;->aC:Z

    .line 491
    invoke-virtual {p0}, Lcom/peel/ui/du;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    sget v1, Lcom/peel/ui/fp;->drawer_layout:I

    invoke-virtual {v0, v1}, Landroid/support/v4/app/ae;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/widget/DrawerLayout;

    invoke-virtual {v0, v2}, Landroid/support/v4/widget/DrawerLayout;->g(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 492
    invoke-virtual {p0}, Lcom/peel/ui/du;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    sget v1, Lcom/peel/ui/fp;->drawer_layout:I

    invoke-virtual {v0, v1}, Landroid/support/v4/app/ae;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/widget/DrawerLayout;

    invoke-virtual {v0, v2}, Landroid/support/v4/widget/DrawerLayout;->f(I)V

    .line 493
    const/4 v0, 0x1

    .line 495
    :goto_0
    return v0

    :cond_0
    invoke-super {p0}, Lcom/peel/d/u;->b()Z

    move-result v0

    goto :goto_0
.end method

.method public c(Landroid/os/Bundle;)V
    .locals 9

    .prologue
    const/16 v6, 0x1e

    const/16 v8, 0xc

    const/4 v1, 0x1

    const/4 v7, 0x6

    const/4 v2, 0x0

    .line 268
    invoke-super {p0, p1}, Lcom/peel/d/u;->c(Landroid/os/Bundle;)V

    .line 269
    sget-object v0, Lcom/peel/content/a;->h:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/peel/ui/du;->b:Lcom/peel/d/i;

    if-nez v0, :cond_1

    .line 442
    :cond_0
    :goto_0
    return-void

    .line 272
    :cond_1
    invoke-virtual {p0}, Lcom/peel/ui/du;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/peel/ui/du;->b:Lcom/peel/d/i;

    if-eqz v0, :cond_2

    .line 275
    iget-object v0, p0, Lcom/peel/ui/du;->aA:Lcom/peel/ui/a/a;

    invoke-virtual {p0}, Lcom/peel/ui/du;->m()Landroid/support/v4/app/ae;

    move-result-object v3

    const-string/jumbo v4, "guide"

    const/16 v5, 0x7de

    invoke-virtual {v0, v3, v4, v5}, Lcom/peel/ui/a/a;->a(Landroid/app/Activity;Ljava/lang/String;I)V

    .line 280
    :cond_2
    iget-object v0, p0, Lcom/peel/ui/du;->c:Landroid/os/Bundle;

    const-string/jumbo v3, "refresh"

    invoke-virtual {v0, v3, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 281
    new-instance v0, Ljava/util/GregorianCalendar;

    invoke-direct {v0}, Ljava/util/GregorianCalendar;-><init>()V

    .line 282
    invoke-virtual {v0, v8}, Ljava/util/GregorianCalendar;->get(I)I

    move-result v0

    .line 283
    if-ge v0, v6, :cond_7

    .line 284
    iput v6, p0, Lcom/peel/ui/du;->ax:I

    .line 289
    :goto_1
    iget-object v0, p0, Lcom/peel/ui/du;->c:Landroid/os/Bundle;

    const-string/jumbo v3, "refresh"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->remove(Ljava/lang/String;)V

    .line 291
    const-class v0, Lcom/peel/ui/du;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v3, "initial selection"

    new-instance v4, Lcom/peel/ui/ee;

    invoke-direct {v4, p0}, Lcom/peel/ui/ee;-><init>(Lcom/peel/ui/du;)V

    invoke-static {v0, v3, v4}, Lcom/peel/util/i;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/lang/Runnable;

    .line 297
    iget-object v0, p0, Lcom/peel/ui/du;->c:Landroid/os/Bundle;

    const-string/jumbo v3, "date_ms"

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v4

    invoke-virtual {v0, v3, v4, v5}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v4

    .line 298
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v3

    .line 299
    invoke-virtual {v3, v7}, Ljava/util/Calendar;->get(I)I

    move-result v6

    .line 300
    invoke-virtual {v3, v4, v5}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 301
    invoke-virtual {v3, v8}, Ljava/util/Calendar;->clear(I)V

    .line 302
    const/16 v0, 0xd

    invoke-virtual {v3, v0}, Ljava/util/Calendar;->clear(I)V

    .line 303
    const/16 v0, 0xe

    invoke-virtual {v3, v0}, Ljava/util/Calendar;->clear(I)V

    .line 305
    iget-object v4, p0, Lcom/peel/ui/du;->al:Landroid/view/View;

    invoke-virtual {v3, v7}, Ljava/util/Calendar;->get(I)I

    move-result v0

    if-eq v0, v6, :cond_8

    move v0, v1

    :goto_2
    invoke-virtual {v4, v0}, Landroid/view/View;->setEnabled(Z)V

    .line 306
    iget-object v0, p0, Lcom/peel/ui/du;->am:Landroid/view/View;

    invoke-virtual {v3, v7}, Ljava/util/Calendar;->get(I)I

    move-result v4

    sub-int/2addr v4, v6

    const/4 v5, 0x5

    if-ge v4, v5, :cond_9

    :goto_3
    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    .line 308
    iget-object v0, p0, Lcom/peel/ui/du;->at:Ljava/util/Date;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/peel/ui/du;->at:Ljava/util/Date;

    invoke-virtual {v3}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/Date;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 309
    :cond_3
    iget-object v0, p0, Lcom/peel/ui/du;->as:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 312
    :cond_4
    invoke-virtual {v3}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/ui/du;->at:Ljava/util/Date;

    .line 313
    iget-object v0, p0, Lcom/peel/ui/du;->ao:Landroid/widget/TextView;

    invoke-static {v3}, Lcom/peel/util/x;->a(Ljava/util/Calendar;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 314
    iget-object v0, p0, Lcom/peel/ui/du;->ap:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/peel/ui/du;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    invoke-static {v1}, Landroid/text/format/DateFormat;->getMediumDateFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v1

    invoke-virtual {v3}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 316
    iget-object v0, p0, Lcom/peel/ui/du;->an:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->getAdapter()Landroid/support/v4/view/av;

    move-result-object v0

    check-cast v0, Lcom/peel/ui/ek;

    .line 317
    const-string/jumbo v1, "live"

    invoke-static {v1}, Lcom/peel/content/a;->c(Ljava/lang/String;)Lcom/peel/content/library/Library;

    move-result-object v1

    check-cast v1, Lcom/peel/content/library/LiveLibrary;

    invoke-virtual {v0, v1}, Lcom/peel/ui/ek;->a(Lcom/peel/content/library/LiveLibrary;)V

    .line 318
    iget-object v1, p0, Lcom/peel/ui/du;->at:Ljava/util/Date;

    iget-object v3, p0, Lcom/peel/ui/du;->as:Ljava/util/ArrayList;

    invoke-virtual {v0, v1, v3}, Lcom/peel/ui/ek;->a(Ljava/util/Date;Ljava/util/ArrayList;)V

    .line 319
    iget-object v1, p0, Lcom/peel/ui/du;->an:Landroid/support/v4/view/ViewPager;

    iget v3, p0, Lcom/peel/ui/du;->au:I

    invoke-virtual {v1, v3}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    .line 321
    invoke-virtual {v0}, Lcom/peel/ui/ek;->d()[Lcom/peel/data/Channel;

    move-result-object v1

    if-eqz v1, :cond_a

    .line 322
    iget-object v1, p0, Lcom/peel/ui/du;->aq:Lcom/peel/widget/ObservableListView;

    new-instance v3, Lcom/peel/ui/ef;

    invoke-virtual {p0}, Lcom/peel/ui/du;->m()Landroid/support/v4/app/ae;

    move-result-object v4

    const/4 v5, -0x1

    invoke-virtual {v0}, Lcom/peel/ui/ek;->d()[Lcom/peel/data/Channel;

    move-result-object v0

    invoke-direct {v3, p0, v4, v5, v0}, Lcom/peel/ui/ef;-><init>(Lcom/peel/ui/du;Landroid/content/Context;I[Lcom/peel/data/Channel;)V

    invoke-virtual {v1, v3}, Lcom/peel/widget/ObservableListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 391
    invoke-virtual {p0}, Lcom/peel/ui/du;->v()Landroid/view/View;

    move-result-object v0

    sget v1, Lcom/peel/ui/fp;->empty:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 396
    :cond_5
    :goto_4
    iget-object v0, p0, Lcom/peel/ui/du;->ar:Lcom/peel/ui/a/q;

    iget-object v1, p0, Lcom/peel/ui/du;->aq:Lcom/peel/widget/ObservableListView;

    invoke-virtual {v0, v1}, Lcom/peel/ui/a/q;->a(Lcom/peel/widget/ObservableListView;)V

    .line 397
    iget-boolean v0, p0, Lcom/peel/ui/du;->az:Z

    if-eqz v0, :cond_6

    .line 398
    iput-boolean v2, p0, Lcom/peel/ui/du;->az:Z

    .line 399
    iget-object v0, p0, Lcom/peel/ui/du;->ar:Lcom/peel/ui/a/q;

    iget-object v1, p0, Lcom/peel/ui/du;->c:Landroid/os/Bundle;

    const-string/jumbo v3, "first_visible"

    invoke-virtual {v1, v3, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    iget-object v3, p0, Lcom/peel/ui/du;->c:Landroid/os/Bundle;

    const-string/jumbo v4, "top"

    invoke-virtual {v3, v4, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v3

    invoke-virtual {v0, v1, v3}, Lcom/peel/ui/a/q;->a(II)V

    .line 400
    iget-object v0, p0, Lcom/peel/ui/du;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "force selection 1 from visible: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v3, p0, Lcom/peel/ui/du;->c:Landroid/os/Bundle;

    const-string/jumbo v4, "first_visible"

    invoke-virtual {v3, v4, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v3, " top: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v3, p0, Lcom/peel/ui/du;->c:Landroid/os/Bundle;

    const-string/jumbo v4, "top"

    invoke-virtual {v3, v4, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 404
    :cond_6
    invoke-virtual {p0}, Lcom/peel/ui/du;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    const-string/jumbo v1, "edit_channel_popup_trigger"

    invoke-static {v0, v1}, Lcom/peel/util/dq;->b(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 405
    invoke-virtual {p0}, Lcom/peel/ui/du;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/ae;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lcom/peel/ui/fq;->trigger_action_edit_channels:I

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 406
    iget-object v0, p0, Lcom/peel/ui/du;->ay:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 407
    iget-object v0, p0, Lcom/peel/ui/du;->ay:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 408
    sget v0, Lcom/peel/ui/fp;->iv_cancel_icon:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 410
    new-instance v2, Lcom/peel/ui/ei;

    invoke-direct {v2, p0}, Lcom/peel/ui/ei;-><init>(Lcom/peel/ui/du;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 429
    new-instance v1, Lcom/peel/ui/dw;

    invoke-direct {v1, p0}, Lcom/peel/ui/dw;-><init>(Lcom/peel/ui/du;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_0

    .line 286
    :cond_7
    iput v2, p0, Lcom/peel/ui/du;->ax:I

    goto/16 :goto_1

    :cond_8
    move v0, v2

    .line 305
    goto/16 :goto_2

    :cond_9
    move v1, v2

    .line 306
    goto/16 :goto_3

    .line 393
    :cond_a
    invoke-virtual {p0}, Lcom/peel/ui/du;->v()Landroid/view/View;

    move-result-object v0

    sget v1, Lcom/peel/ui/fp;->empty:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_4
.end method

.method public e(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 499
    new-instance v0, Landroid/os/Bundle;

    iget-object v1, p0, Lcom/peel/ui/du;->c:Landroid/os/Bundle;

    invoke-direct {v0, v1}, Landroid/os/Bundle;-><init>(Landroid/os/Bundle;)V

    .line 500
    iget-object v1, p0, Lcom/peel/ui/du;->at:Ljava/util/Date;

    if-eqz v1, :cond_0

    const-string/jumbo v1, "date_ms"

    iget-object v2, p0, Lcom/peel/ui/du;->at:Ljava/util/Date;

    invoke-virtual {v2}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 501
    :cond_0
    iget-object v1, p0, Lcom/peel/ui/du;->as:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_1

    const-string/jumbo v1, "payloads"

    iget-object v2, p0, Lcom/peel/ui/du;->as:Ljava/util/ArrayList;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 502
    :cond_1
    const-string/jumbo v1, "selected"

    iget v2, p0, Lcom/peel/ui/du;->au:I

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 503
    const-string/jumbo v1, "bundle"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 504
    invoke-super {p0, p1}, Lcom/peel/d/u;->e(Landroid/os/Bundle;)V

    .line 505
    return-void
.end method

.method public f()V
    .locals 2

    .prologue
    .line 581
    iget-object v0, p0, Lcom/peel/ui/du;->a:Ljava/lang/String;

    const-string/jumbo v1, "onStop"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 582
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/peel/ui/du;->aC:Z

    .line 583
    invoke-super {p0}, Lcom/peel/d/u;->f()V

    .line 584
    iget-object v0, p0, Lcom/peel/ui/du;->e:Lcom/peel/ui/ar;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/peel/ui/du;->e:Lcom/peel/ui/ar;

    invoke-virtual {v0}, Lcom/peel/ui/ar;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 585
    iget-object v0, p0, Lcom/peel/ui/du;->e:Lcom/peel/ui/ar;

    invoke-virtual {v0}, Lcom/peel/ui/ar;->dismiss()V

    .line 587
    :cond_0
    return-void
.end method

.method public h(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    .line 157
    iget-object v0, p0, Lcom/peel/ui/du;->a:Ljava/lang/String;

    const-string/jumbo v1, "viewStateRestorted"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 158
    invoke-super {p0, p1}, Lcom/peel/d/u;->h(Landroid/os/Bundle;)V

    .line 159
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/peel/ui/du;->aC:Z

    .line 160
    if-eqz p1, :cond_0

    const-string/jumbo v0, "bundle"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 161
    iget-object v0, p0, Lcom/peel/ui/du;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "bundle"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->putAll(Landroid/os/Bundle;)V

    .line 163
    iget-object v0, p0, Lcom/peel/ui/du;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "date_ms"

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    .line 164
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v2

    .line 165
    invoke-virtual {v2, v0, v1}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 166
    const/16 v0, 0xc

    invoke-virtual {v2, v0}, Ljava/util/Calendar;->clear(I)V

    .line 167
    const/16 v0, 0xd

    invoke-virtual {v2, v0}, Ljava/util/Calendar;->clear(I)V

    .line 168
    const/16 v0, 0xe

    invoke-virtual {v2, v0}, Ljava/util/Calendar;->clear(I)V

    .line 169
    invoke-virtual {v2}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/ui/du;->at:Ljava/util/Date;

    .line 172
    :cond_0
    iget-object v0, p0, Lcom/peel/ui/du;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "payloads"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 173
    iget-object v0, p0, Lcom/peel/ui/du;->as:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 174
    iget-object v0, p0, Lcom/peel/ui/du;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "payloads"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Parcelable;

    .line 175
    iget-object v2, p0, Lcom/peel/ui/du;->as:Ljava/util/ArrayList;

    check-cast v0, Landroid/os/Bundle;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 179
    :cond_1
    new-instance v0, Lcom/peel/ui/a/q;

    iget-object v1, p0, Lcom/peel/ui/du;->c:Landroid/os/Bundle;

    invoke-direct {v0, v1}, Lcom/peel/ui/a/q;-><init>(Landroid/os/Bundle;)V

    iput-object v0, p0, Lcom/peel/ui/du;->ar:Lcom/peel/ui/a/q;

    .line 180
    iget-object v0, p0, Lcom/peel/ui/du;->ar:Lcom/peel/ui/a/q;

    new-instance v1, Lcom/peel/ui/dz;

    invoke-direct {v1, p0}, Lcom/peel/ui/dz;-><init>(Lcom/peel/ui/du;)V

    invoke-virtual {v0, v1}, Lcom/peel/ui/a/q;->a(Lcom/peel/ui/a/u;)V

    .line 190
    iget-object v0, p0, Lcom/peel/ui/du;->ar:Lcom/peel/ui/a/q;

    new-instance v1, Lcom/peel/ui/ea;

    invoke-direct {v1, p0}, Lcom/peel/ui/ea;-><init>(Lcom/peel/ui/du;)V

    invoke-virtual {v0, v1}, Lcom/peel/ui/a/q;->a(Lcom/peel/ui/a/t;)V

    .line 212
    iget-object v0, p0, Lcom/peel/ui/du;->an:Landroid/support/v4/view/ViewPager;

    new-instance v1, Lcom/peel/ui/ek;

    iget-object v2, p0, Lcom/peel/ui/du;->aj:Landroid/view/LayoutInflater;

    invoke-virtual {p0}, Lcom/peel/ui/du;->m()Landroid/support/v4/app/ae;

    move-result-object v3

    invoke-static {v3}, Landroid/text/format/DateFormat;->is24HourFormat(Landroid/content/Context;)Z

    move-result v3

    iget-object v4, p0, Lcom/peel/ui/du;->ar:Lcom/peel/ui/a/q;

    invoke-direct {v1, v2, v3, v4}, Lcom/peel/ui/ek;-><init>(Landroid/view/LayoutInflater;ZLcom/peel/ui/a/q;)V

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setAdapter(Landroid/support/v4/view/av;)V

    .line 213
    iget-object v0, p0, Lcom/peel/ui/du;->an:Landroid/support/v4/view/ViewPager;

    iget-object v1, p0, Lcom/peel/ui/du;->aD:Landroid/support/v4/view/cs;

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setOnPageChangeListener(Landroid/support/v4/view/cs;)V

    .line 215
    iget-object v0, p0, Lcom/peel/ui/du;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "selected"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/peel/ui/du;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "selected"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/peel/ui/du;->au:I

    .line 217
    :cond_2
    sget-object v0, Lcom/peel/content/a;->h:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 219
    iget-object v0, p0, Lcom/peel/ui/du;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "refresh"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 220
    iget-object v0, p0, Lcom/peel/ui/du;->c:Landroid/os/Bundle;

    invoke-virtual {p0, v0}, Lcom/peel/ui/du;->c(Landroid/os/Bundle;)V

    .line 223
    :cond_3
    const-class v0, Lcom/peel/ui/du;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "insights"

    new-instance v2, Lcom/peel/ui/eb;

    invoke-direct {v2, p0}, Lcom/peel/ui/eb;-><init>(Lcom/peel/ui/du;)V

    invoke-static {v0, v1, v2}, Lcom/peel/util/i;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/lang/Runnable;

    .line 230
    sget-object v0, Lcom/peel/b/a;->h:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 231
    sget-object v1, Lcom/peel/b/a;->h:Ljava/lang/String;

    iget-object v0, p0, Lcom/peel/ui/du;->an:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->getAdapter()Landroid/support/v4/view/av;

    move-result-object v0

    check-cast v0, Lcom/peel/ui/ek;

    invoke-virtual {v0}, Lcom/peel/ui/ek;->d()[Lcom/peel/data/Channel;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/peel/util/bx;->a(Ljava/lang/String;[Lcom/peel/data/Channel;)I

    move-result v0

    .line 232
    iget-object v1, p0, Lcom/peel/ui/du;->ar:Lcom/peel/ui/a/q;

    invoke-virtual {v1, v0}, Lcom/peel/ui/a/q;->b(I)V

    .line 233
    iget-object v1, p0, Lcom/peel/ui/du;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "force selection 2 from iIndex: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 236
    :cond_4
    invoke-virtual {p0}, Lcom/peel/ui/du;->Z()V

    .line 237
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 10

    .prologue
    const/16 v9, 0xc

    const/16 v8, 0xb

    const/4 v7, 0x1

    const/4 v6, 0x0

    const/4 v5, 0x6

    .line 445
    iget-object v0, p0, Lcom/peel/ui/du;->b:Lcom/peel/d/i;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/peel/content/a;->h:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-nez v0, :cond_1

    .line 485
    :cond_0
    :goto_0
    return-void

    .line 447
    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    .line 448
    sget v1, Lcom/peel/ui/fp;->date_container:I

    if-ne v0, v1, :cond_2

    .line 449
    new-instance v0, Lcom/peel/ui/ar;

    invoke-virtual {p0}, Lcom/peel/ui/du;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    iget-object v2, p0, Lcom/peel/ui/du;->b:Lcom/peel/d/i;

    iget-object v3, p0, Lcom/peel/ui/du;->at:Ljava/util/Date;

    invoke-direct {v0, v1, v2, v3}, Lcom/peel/ui/ar;-><init>(Landroid/content/Context;Lcom/peel/d/i;Ljava/util/Date;)V

    iput-object v0, p0, Lcom/peel/ui/du;->e:Lcom/peel/ui/ar;

    .line 450
    iget-object v0, p0, Lcom/peel/ui/du;->e:Lcom/peel/ui/ar;

    invoke-virtual {v0}, Lcom/peel/ui/ar;->isShowing()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/peel/ui/du;->e:Lcom/peel/ui/ar;

    invoke-virtual {v0}, Lcom/peel/ui/ar;->show()V

    goto :goto_0

    .line 451
    :cond_2
    sget v1, Lcom/peel/ui/fp;->prev_time_btn:I

    if-eq v0, v1, :cond_3

    sget v1, Lcom/peel/ui/fp;->next_time_btn:I

    if-ne v0, v1, :cond_0

    .line 452
    :cond_3
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v1

    .line 453
    invoke-virtual {v1, v5}, Ljava/util/Calendar;->get(I)I

    move-result v2

    .line 454
    invoke-virtual {v1, v8}, Ljava/util/Calendar;->get(I)I

    move-result v3

    .line 455
    iget-object v4, p0, Lcom/peel/ui/du;->at:Ljava/util/Date;

    invoke-virtual {v1, v4}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 457
    sget v4, Lcom/peel/ui/fp;->next_time_btn:I

    if-ne v0, v4, :cond_5

    .line 458
    invoke-virtual {v1, v5}, Ljava/util/Calendar;->get(I)I

    move-result v0

    sub-int/2addr v0, v2

    const/4 v2, 0x5

    if-ge v0, v2, :cond_0

    .line 461
    invoke-virtual {v1, v5, v7}, Ljava/util/Calendar;->add(II)V

    .line 462
    invoke-virtual {v1, v8, v6}, Ljava/util/Calendar;->set(II)V

    .line 463
    invoke-virtual {v1, v9, v6}, Ljava/util/Calendar;->set(II)V

    .line 476
    :cond_4
    :goto_1
    iput v6, p0, Lcom/peel/ui/du;->au:I

    .line 477
    iput-boolean v7, p0, Lcom/peel/ui/du;->az:Z

    .line 478
    iget-object v0, p0, Lcom/peel/ui/du;->as:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 479
    invoke-virtual {v1}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/ui/du;->at:Ljava/util/Date;

    .line 480
    iget-object v0, p0, Lcom/peel/ui/du;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "date_ms"

    iget-object v2, p0, Lcom/peel/ui/du;->at:Ljava/util/Date;

    invoke-virtual {v2}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 481
    iget-object v0, p0, Lcom/peel/ui/du;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "refresh"

    invoke-virtual {v0, v1, v7}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 482
    iget-object v0, p0, Lcom/peel/ui/du;->c:Landroid/os/Bundle;

    invoke-virtual {p0, v0}, Lcom/peel/ui/du;->c(Landroid/os/Bundle;)V

    goto :goto_0

    .line 465
    :cond_5
    invoke-virtual {v1, v5}, Ljava/util/Calendar;->get(I)I

    move-result v0

    if-le v0, v2, :cond_0

    .line 468
    const/4 v0, -0x1

    invoke-virtual {v1, v5, v0}, Ljava/util/Calendar;->add(II)V

    .line 469
    invoke-virtual {v1, v5}, Ljava/util/Calendar;->get(I)I

    move-result v0

    if-ne v0, v2, :cond_4

    .line 470
    invoke-virtual {v1, v8, v3}, Ljava/util/Calendar;->set(II)V

    .line 471
    invoke-virtual {v1, v9, v6}, Ljava/util/Calendar;->set(II)V

    goto :goto_1
.end method

.method public w()V
    .locals 6

    .prologue
    .line 241
    iget-object v0, p0, Lcom/peel/ui/du;->a:Ljava/lang/String;

    const-string/jumbo v1, "onResume"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 242
    invoke-super {p0}, Lcom/peel/d/u;->w()V

    .line 243
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, Lcom/peel/ui/du;->av:Ljava/util/Timer;

    .line 244
    new-instance v0, Lcom/peel/ui/ec;

    invoke-direct {v0, p0}, Lcom/peel/ui/ec;-><init>(Lcom/peel/ui/du;)V

    iput-object v0, p0, Lcom/peel/ui/du;->aw:Ljava/util/TimerTask;

    .line 264
    iget-object v0, p0, Lcom/peel/ui/du;->av:Ljava/util/Timer;

    iget-object v1, p0, Lcom/peel/ui/du;->aw:Ljava/util/TimerTask;

    const-wide/16 v2, 0x3e8

    const-wide/16 v4, 0x1388

    invoke-virtual/range {v0 .. v5}, Ljava/util/Timer;->scheduleAtFixedRate(Ljava/util/TimerTask;JJ)V

    .line 265
    return-void
.end method

.method public x()V
    .locals 2

    .prologue
    .line 509
    iget-object v0, p0, Lcom/peel/ui/du;->a:Ljava/lang/String;

    const-string/jumbo v1, "onPause"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 510
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/peel/ui/du;->aC:Z

    .line 511
    invoke-super {p0}, Lcom/peel/d/u;->x()V

    .line 512
    iget-object v0, p0, Lcom/peel/ui/du;->av:Ljava/util/Timer;

    if-eqz v0, :cond_0

    .line 513
    iget-object v0, p0, Lcom/peel/ui/du;->av:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 515
    :cond_0
    return-void
.end method
