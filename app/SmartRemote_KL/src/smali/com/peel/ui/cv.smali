.class Lcom/peel/ui/cv;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/view/View$OnLongClickListener;


# instance fields
.field final synthetic a:Lcom/peel/ui/cu;


# direct methods
.method constructor <init>(Lcom/peel/ui/cu;)V
    .locals 0

    .prologue
    .line 165
    iput-object p1, p0, Lcom/peel/ui/cv;->a:Lcom/peel/ui/cu;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onLongClick(Landroid/view/View;)Z
    .locals 3

    .prologue
    .line 168
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/peel/ui/cv;->a:Lcom/peel/ui/cu;

    iget-object v1, v1, Lcom/peel/ui/cu;->a:Lcom/peel/ui/cj;

    invoke-virtual {v1}, Lcom/peel/ui/cj;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 169
    const-string/jumbo v1, "Upload debug info?"

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const-string/jumbo v1, "Upload"

    new-instance v2, Lcom/peel/ui/cw;

    invoke-direct {v2, p0}, Lcom/peel/ui/cw;-><init>(Lcom/peel/ui/cv;)V

    .line 170
    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const-string/jumbo v1, "Cancel"

    const/4 v2, 0x0

    .line 238
    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 239
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 241
    const/4 v0, 0x1

    return v0
.end method
