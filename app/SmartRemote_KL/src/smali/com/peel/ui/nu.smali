.class Lcom/peel/ui/nu;
.super Lcom/peel/util/t;


# instance fields
.field final synthetic a:Lcom/peel/ui/nt;


# direct methods
.method constructor <init>(Lcom/peel/ui/nt;I)V
    .locals 0

    .prologue
    .line 428
    iput-object p1, p0, Lcom/peel/ui/nu;->a:Lcom/peel/ui/nt;

    invoke-direct {p0, p2}, Lcom/peel/util/t;-><init>(I)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 12

    .prologue
    .line 431
    sget-object v0, Lcom/peel/content/a;->h:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/peel/ui/nu;->a:Lcom/peel/ui/nt;

    iget-object v0, v0, Lcom/peel/ui/nt;->c:Lcom/peel/ui/nl;

    iget-object v0, v0, Lcom/peel/ui/nl;->b:Lcom/peel/d/i;

    if-nez v0, :cond_1

    .line 484
    :cond_0
    :goto_0
    return-void

    .line 434
    :cond_1
    iget-boolean v0, p0, Lcom/peel/ui/nu;->i:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/peel/ui/nu;->j:Ljava/lang/Object;

    if-nez v0, :cond_3

    .line 435
    :cond_2
    invoke-static {}, Lcom/peel/ui/nl;->c()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/ui/nu;->a:Lcom/peel/ui/nt;

    iget-object v1, v1, Lcom/peel/ui/nt;->c:Lcom/peel/ui/nl;

    invoke-virtual {v1}, Lcom/peel/ui/nl;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/peel/util/bx;->a(Ljava/lang/String;Landroid/app/Activity;Z)V

    goto :goto_0

    .line 439
    :cond_3
    iget-object v0, p0, Lcom/peel/ui/nu;->a:Lcom/peel/ui/nt;

    iget-object v1, v0, Lcom/peel/ui/nt;->b:Landroid/os/Bundle;

    const-string/jumbo v2, "weekly"

    iget-object v0, p0, Lcom/peel/ui/nu;->j:Ljava/lang/Object;

    check-cast v0, Ljava/util/ArrayList;

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 441
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 442
    new-instance v9, Ljava/util/LinkedHashMap;

    invoke-direct {v9}, Ljava/util/LinkedHashMap;-><init>()V

    .line 443
    new-instance v10, Ljava/util/TreeMap;

    invoke-direct {v10}, Ljava/util/TreeMap;-><init>()V

    .line 445
    iget-object v0, p0, Lcom/peel/ui/nu;->j:Ljava/lang/Object;

    check-cast v0, Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_4
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/peel/content/listing/Listing;

    .line 446
    if-eqz v1, :cond_4

    instance-of v0, v1, Lcom/peel/content/listing/LiveListing;

    if-eqz v0, :cond_4

    move-object v0, v1

    .line 447
    check-cast v0, Lcom/peel/content/listing/LiveListing;

    .line 448
    invoke-virtual {v0}, Lcom/peel/content/listing/LiveListing;->o()J

    move-result-wide v4

    .line 449
    const-wide/16 v6, 0x0

    cmp-long v0, v6, v4

    if-eqz v0, :cond_4

    .line 451
    new-instance v6, Ljava/util/GregorianCalendar;

    invoke-direct {v6}, Ljava/util/GregorianCalendar;-><init>()V

    .line 452
    invoke-virtual {v6, v4, v5}, Ljava/util/GregorianCalendar;->setTimeInMillis(J)V

    .line 453
    invoke-static {v6}, Lcom/peel/util/x;->a(Ljava/util/Calendar;)Ljava/lang/String;

    move-result-object v0

    .line 455
    sget-object v2, Lcom/peel/util/x;->n:[Ljava/lang/String;

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    .line 456
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v2, " ("

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/peel/ui/nu;->a:Lcom/peel/ui/nt;

    iget-object v2, v2, Lcom/peel/ui/nt;->c:Lcom/peel/ui/nl;

    invoke-virtual {v2}, Lcom/peel/ui/nl;->m()Landroid/support/v4/app/ae;

    move-result-object v2

    invoke-static {v2}, Landroid/text/format/DateFormat;->getMediumDateFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v2

    invoke-virtual {v6}, Ljava/util/GregorianCalendar;->getTime()Ljava/util/Date;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v2, ")"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    move-object v2, v0

    .line 459
    :goto_2
    invoke-interface {v9, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 460
    if-nez v0, :cond_5

    .line 461
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 462
    invoke-interface {v9, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 463
    invoke-virtual {v6}, Ljava/util/GregorianCalendar;->getTimeInMillis()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-interface {v10, v4, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 465
    :cond_5
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 468
    :cond_6
    invoke-interface {v10}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :cond_7
    :goto_3
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 469
    invoke-interface {v10, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 470
    invoke-interface {v9, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    .line 471
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    const-wide/16 v4, -0x1

    const-class v6, Lcom/peel/content/listing/c;

    invoke-virtual {v6}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-static {}, Lcom/peel/content/a;->a()Lcom/peel/data/ContentRoom;

    move-result-object v7

    invoke-static/range {v1 .. v7}, Lcom/peel/util/bx;->a(Ljava/util/List;JJLjava/lang/String;Lcom/peel/data/ContentRoom;)Ljava/util/List;

    move-result-object v1

    .line 472
    if-eqz v1, :cond_7

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_7

    .line 473
    invoke-static {v0, v1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 477
    :cond_8
    invoke-static {}, Lcom/peel/ui/nl;->c()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "set adapter"

    new-instance v2, Lcom/peel/ui/nv;

    invoke-direct {v2, p0, v8}, Lcom/peel/ui/nv;-><init>(Lcom/peel/ui/nu;Ljava/util/List;)V

    invoke-static {v0, v1, v2}, Lcom/peel/util/i;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/lang/Runnable;

    goto/16 :goto_0

    :cond_9
    move-object v2, v0

    goto :goto_2
.end method
