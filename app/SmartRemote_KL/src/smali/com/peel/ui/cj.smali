.class public Lcom/peel/ui/cj;
.super Lcom/peel/d/u;


# static fields
.field private static final h:Ljava/lang/String;


# instance fields
.field private aj:I

.field private ak:Landroid/widget/AutoCompleteTextView;

.field private al:Lcom/peel/d/a;

.field private am:Lcom/peel/widget/CircleImageView;

.field private an:Landroid/widget/TextView;

.field private ao:Landroid/view/View;

.field private ap:Landroid/view/View;

.field private aq:Landroid/widget/ImageView;

.field private ar:Z

.field private as:Z

.field private at:Landroid/widget/ImageView;

.field private au:Landroid/view/LayoutInflater;

.field private av:Lcom/peel/widget/CustomScrollview;

.field private aw:Landroid/content/BroadcastReceiver;

.field public e:Ljava/lang/String;

.field public f:Landroid/os/Bundle;

.field private final g:Landroid/os/Bundle;

.field private i:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 73
    const-class v0, Lcom/peel/ui/cj;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/peel/ui/cj;->h:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 71
    invoke-direct {p0}, Lcom/peel/d/u;-><init>()V

    .line 72
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, Lcom/peel/ui/cj;->g:Landroid/os/Bundle;

    .line 74
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/peel/ui/cj;->i:Landroid/util/SparseArray;

    .line 75
    sget v0, Lcom/peel/ui/fp;->menu_browse:I

    iput v0, p0, Lcom/peel/ui/cj;->aj:I

    .line 88
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/peel/ui/cj;->ar:Z

    .line 89
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/peel/ui/cj;->as:Z

    .line 641
    new-instance v0, Lcom/peel/ui/cq;

    invoke-direct {v0, p0}, Lcom/peel/ui/cq;-><init>(Lcom/peel/ui/cj;)V

    iput-object v0, p0, Lcom/peel/ui/cj;->aw:Landroid/content/BroadcastReceiver;

    return-void
.end method

.method static synthetic S()Ljava/lang/String;
    .locals 1

    .prologue
    .line 71
    sget-object v0, Lcom/peel/ui/cj;->h:Ljava/lang/String;

    return-object v0
.end method

.method private T()V
    .locals 2

    .prologue
    .line 619
    invoke-virtual {p0}, Lcom/peel/ui/cj;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    new-instance v1, Lcom/peel/ui/co;

    invoke-direct {v1, p0}, Lcom/peel/ui/co;-><init>(Lcom/peel/ui/cj;)V

    invoke-static {v0, v1}, Lcom/peel/social/w;->a(Landroid/content/Context;Lcom/peel/util/t;)V

    .line 639
    return-void
.end method

.method static synthetic a(Lcom/peel/ui/cj;)Landroid/widget/AutoCompleteTextView;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/peel/ui/cj;->ak:Landroid/widget/AutoCompleteTextView;

    return-object v0
.end method

.method static synthetic a(Lcom/peel/ui/cj;Ljava/lang/String;Z)V
    .locals 0

    .prologue
    .line 71
    invoke-direct {p0, p1, p2}, Lcom/peel/ui/cj;->a(Ljava/lang/String;Z)V

    return-void
.end method

.method private a(Ljava/lang/String;Z)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 683
    iput-boolean v3, p0, Lcom/peel/ui/cj;->ar:Z

    .line 685
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 686
    const-string/jumbo v1, "type"

    const/4 v2, 0x5

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 688
    const-string/jumbo v1, "keyword"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 689
    const-string/jumbo v1, "useExactSearch"

    invoke-virtual {v0, v1, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 690
    const-string/jumbo v1, "addToBackStack"

    invoke-virtual {v0, v1, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 692
    invoke-virtual {p0}, Lcom/peel/ui/cj;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    const-class v2, Lcom/peel/ui/gm;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lcom/peel/d/e;->b(Landroid/support/v4/app/ae;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 693
    return-void
.end method

.method static synthetic a(Lcom/peel/ui/cj;Z)Z
    .locals 0

    .prologue
    .line 71
    iput-boolean p1, p0, Lcom/peel/ui/cj;->as:Z

    return p1
.end method

.method static synthetic b(Lcom/peel/ui/cj;)Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/peel/ui/cj;->at:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic b(Lcom/peel/ui/cj;Z)Z
    .locals 0

    .prologue
    .line 71
    iput-boolean p1, p0, Lcom/peel/ui/cj;->ar:Z

    return p1
.end method

.method static synthetic c(Lcom/peel/ui/cj;)Z
    .locals 1

    .prologue
    .line 71
    iget-boolean v0, p0, Lcom/peel/ui/cj;->as:Z

    return v0
.end method

.method static synthetic d(Lcom/peel/ui/cj;)Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/peel/ui/cj;->aq:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic e(Lcom/peel/ui/cj;)Z
    .locals 1

    .prologue
    .line 71
    iget-boolean v0, p0, Lcom/peel/ui/cj;->ar:Z

    return v0
.end method

.method static synthetic f(Lcom/peel/ui/cj;)V
    .locals 0

    .prologue
    .line 71
    invoke-direct {p0}, Lcom/peel/ui/cj;->T()V

    return-void
.end method


# virtual methods
.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 6

    .prologue
    const/16 v5, 0x8

    const/4 v4, 0x0

    .line 96
    sget v0, Lcom/peel/ui/fq;->slideout_menu_layout:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 98
    iput-object p1, p0, Lcom/peel/ui/cj;->au:Landroid/view/LayoutInflater;

    .line 100
    sget v0, Lcom/peel/ui/fp;->help:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    .line 101
    invoke-virtual {v0, v4}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 102
    sget v0, Lcom/peel/ui/fp;->scrollView:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/peel/widget/CustomScrollview;

    iput-object v0, p0, Lcom/peel/ui/cj;->av:Lcom/peel/widget/CustomScrollview;

    .line 103
    iget-object v0, p0, Lcom/peel/ui/cj;->av:Lcom/peel/widget/CustomScrollview;

    new-instance v2, Lcom/peel/ui/ck;

    invoke-direct {v2, p0, v1}, Lcom/peel/ui/ck;-><init>(Lcom/peel/ui/cj;Landroid/view/View;)V

    invoke-virtual {v0, v2}, Lcom/peel/widget/CustomScrollview;->setOnBottomReachedListener(Lcom/peel/widget/j;)V

    .line 110
    iget-object v0, p0, Lcom/peel/ui/cj;->av:Lcom/peel/widget/CustomScrollview;

    new-instance v2, Lcom/peel/ui/cr;

    invoke-direct {v2, p0, v1}, Lcom/peel/ui/cr;-><init>(Lcom/peel/ui/cj;Landroid/view/View;)V

    invoke-virtual {v0, v2}, Lcom/peel/widget/CustomScrollview;->setOnTopReachedListener(Lcom/peel/widget/l;)V

    .line 117
    iget-object v0, p0, Lcom/peel/ui/cj;->av:Lcom/peel/widget/CustomScrollview;

    new-instance v2, Lcom/peel/ui/cs;

    invoke-direct {v2, p0, v1}, Lcom/peel/ui/cs;-><init>(Lcom/peel/ui/cj;Landroid/view/View;)V

    invoke-virtual {v0, v2}, Lcom/peel/widget/CustomScrollview;->setOnMiddleReachedListener(Lcom/peel/widget/k;)V

    .line 125
    sget v0, Lcom/peel/ui/fp;->tv_name:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/peel/ui/cj;->an:Landroid/widget/TextView;

    .line 126
    sget v0, Lcom/peel/ui/fp;->iv_profile_pic:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/peel/widget/CircleImageView;

    iput-object v0, p0, Lcom/peel/ui/cj;->am:Lcom/peel/widget/CircleImageView;

    .line 127
    sget v0, Lcom/peel/ui/fp;->profile_parent:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/ui/cj;->ao:Landroid/view/View;

    .line 128
    sget v0, Lcom/peel/ui/fp;->profile_parent_logout:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/ui/cj;->ap:Landroid/view/View;

    .line 129
    iget-object v0, p0, Lcom/peel/ui/cj;->ao:Landroid/view/View;

    new-instance v2, Lcom/peel/ui/ct;

    invoke-direct {v2, p0}, Lcom/peel/ui/ct;-><init>(Lcom/peel/ui/cj;)V

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 135
    iget-object v0, p0, Lcom/peel/ui/cj;->i:Landroid/util/SparseArray;

    sget v2, Lcom/peel/ui/fp;->menu_browse:I

    sget v3, Lcom/peel/ui/fp;->menu_browse:I

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 136
    sget v0, Lcom/peel/ui/fp;->search_icon:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/peel/ui/cj;->at:Landroid/widget/ImageView;

    .line 137
    iget-object v0, p0, Lcom/peel/ui/cj;->i:Landroid/util/SparseArray;

    sget v2, Lcom/peel/ui/fp;->menu_favorites:I

    sget v3, Lcom/peel/ui/fp;->menu_favorites:I

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 138
    sget-object v0, Lcom/peel/c/a;->b:Lcom/peel/c/e;

    invoke-static {v0}, Lcom/peel/c/a;->a(Lcom/peel/c/e;)Ljava/lang/Object;

    move-result-object v0

    sget-object v2, Lcom/peel/c/b;->c:Lcom/peel/c/b;

    if-ne v0, v2, :cond_2

    .line 139
    sget v0, Lcom/peel/ui/fp;->menu_program_guide:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    .line 140
    iget-object v0, p0, Lcom/peel/ui/cj;->at:Landroid/widget/ImageView;

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 144
    :goto_0
    iget-object v0, p0, Lcom/peel/ui/cj;->i:Landroid/util/SparseArray;

    sget v2, Lcom/peel/ui/fp;->menu_settings:I

    sget v3, Lcom/peel/ui/fp;->menu_settings:I

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 145
    iget-object v0, p0, Lcom/peel/ui/cj;->i:Landroid/util/SparseArray;

    sget v2, Lcom/peel/ui/fp;->menu_browse_forum:I

    sget v3, Lcom/peel/ui/fp;->menu_browse_forum:I

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 146
    iget-object v0, p0, Lcom/peel/ui/cj;->i:Landroid/util/SparseArray;

    sget v2, Lcom/peel/ui/fp;->menu_send_feedback:I

    sget v3, Lcom/peel/ui/fp;->menu_send_feedback:I

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 147
    iget-object v0, p0, Lcom/peel/ui/cj;->i:Landroid/util/SparseArray;

    sget v2, Lcom/peel/ui/fp;->menu_troubleshoot_channel:I

    sget v3, Lcom/peel/ui/fp;->menu_troubleshoot_channel:I

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 149
    sget-object v0, Lcom/peel/c/a;->b:Lcom/peel/c/e;

    invoke-static {v0}, Lcom/peel/c/a;->a(Lcom/peel/c/e;)Ljava/lang/Object;

    move-result-object v0

    sget-object v2, Lcom/peel/c/b;->a:Lcom/peel/c/b;

    if-eq v0, v2, :cond_0

    sget-object v0, Lcom/peel/c/a;->b:Lcom/peel/c/e;

    .line 150
    invoke-static {v0}, Lcom/peel/c/a;->a(Lcom/peel/c/e;)Ljava/lang/Object;

    move-result-object v0

    sget-object v2, Lcom/peel/c/b;->c:Lcom/peel/c/b;

    if-ne v0, v2, :cond_3

    .line 151
    :cond_0
    sget v0, Lcom/peel/ui/fp;->menu_settings:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 152
    iget-object v0, p0, Lcom/peel/ui/cj;->i:Landroid/util/SparseArray;

    sget v2, Lcom/peel/ui/fp;->menu_settings:I

    sget v3, Lcom/peel/ui/fp;->menu_settings:I

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 157
    :goto_1
    sget v0, Lcom/peel/ui/fp;->peel_txt:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 159
    new-instance v2, Lcom/peel/ui/cu;

    invoke-direct {v2, p0}, Lcom/peel/ui/cu;-><init>(Lcom/peel/ui/cj;)V

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 249
    sget v0, Lcom/peel/ui/fp;->cancel_btn:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/peel/ui/cj;->aq:Landroid/widget/ImageView;

    .line 250
    iget-object v0, p0, Lcom/peel/ui/cj;->aq:Landroid/widget/ImageView;

    new-instance v2, Lcom/peel/ui/da;

    invoke-direct {v2, p0}, Lcom/peel/ui/da;-><init>(Lcom/peel/ui/cj;)V

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 268
    sget v0, Lcom/peel/ui/fp;->searchword:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/AutoCompleteTextView;

    iput-object v0, p0, Lcom/peel/ui/cj;->ak:Landroid/widget/AutoCompleteTextView;

    .line 269
    iget-object v0, p0, Lcom/peel/ui/cj;->ak:Landroid/widget/AutoCompleteTextView;

    const/16 v2, 0x91

    invoke-virtual {v0, v2}, Landroid/widget/AutoCompleteTextView;->setInputType(I)V

    .line 270
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v0

    .line 271
    const-string/jumbo v2, "iw"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    const-string/jumbo v2, "ar"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 272
    :cond_1
    iget-object v0, p0, Lcom/peel/ui/cj;->ak:Landroid/widget/AutoCompleteTextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget v3, Lcom/peel/ui/ft;->hint_search_box:I

    invoke-virtual {p0, v3}, Lcom/peel/ui/cj;->a(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/AutoCompleteTextView;->setHint(Ljava/lang/CharSequence;)V

    .line 277
    :goto_2
    iget-object v0, p0, Lcom/peel/ui/cj;->ak:Landroid/widget/AutoCompleteTextView;

    new-instance v2, Lcom/peel/ui/gk;

    invoke-virtual {p0}, Lcom/peel/ui/cj;->m()Landroid/support/v4/app/ae;

    move-result-object v3

    sget v4, Lcom/peel/ui/fq;->search_row:I

    invoke-direct {v2, v3, v4}, Lcom/peel/ui/gk;-><init>(Landroid/content/Context;I)V

    invoke-virtual {v0, v2}, Landroid/widget/AutoCompleteTextView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 279
    iget-object v0, p0, Lcom/peel/ui/cj;->ak:Landroid/widget/AutoCompleteTextView;

    new-instance v2, Lcom/peel/ui/db;

    invoke-direct {v2, p0}, Lcom/peel/ui/db;-><init>(Lcom/peel/ui/cj;)V

    invoke-virtual {v0, v2}, Landroid/widget/AutoCompleteTextView;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 293
    iget-object v0, p0, Lcom/peel/ui/cj;->ak:Landroid/widget/AutoCompleteTextView;

    new-instance v2, Lcom/peel/ui/dc;

    invoke-direct {v2, p0}, Lcom/peel/ui/dc;-><init>(Lcom/peel/ui/cj;)V

    invoke-virtual {v0, v2}, Landroid/widget/AutoCompleteTextView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 301
    iget-object v0, p0, Lcom/peel/ui/cj;->ak:Landroid/widget/AutoCompleteTextView;

    new-instance v2, Lcom/peel/ui/dd;

    invoke-direct {v2, p0}, Lcom/peel/ui/dd;-><init>(Lcom/peel/ui/cj;)V

    invoke-virtual {v0, v2}, Landroid/widget/AutoCompleteTextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 378
    return-object v1

    .line 142
    :cond_2
    iget-object v0, p0, Lcom/peel/ui/cj;->i:Landroid/util/SparseArray;

    sget v2, Lcom/peel/ui/fp;->menu_program_guide:I

    sget v3, Lcom/peel/ui/fp;->menu_program_guide:I

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    goto/16 :goto_0

    .line 154
    :cond_3
    sget v0, Lcom/peel/ui/fp;->menu_settings:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_1

    .line 274
    :cond_4
    iget-object v0, p0, Lcom/peel/ui/cj;->ak:Landroid/widget/AutoCompleteTextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "       "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget v3, Lcom/peel/ui/ft;->hint_search_box:I

    invoke-virtual {p0, v3}, Lcom/peel/ui/cj;->a(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/AutoCompleteTextView;->setHint(Ljava/lang/CharSequence;)V

    goto :goto_2
.end method

.method public a(Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 609
    invoke-super {p0, p1}, Lcom/peel/d/u;->a(Landroid/view/MenuItem;)Z

    move-result v0

    return v0
.end method

.method protected b(I)V
    .locals 4

    .prologue
    .line 588
    iget-object v0, p0, Lcom/peel/ui/cj;->i:Landroid/util/SparseArray;

    iget v1, p0, Lcom/peel/ui/cj;->aj:I

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 589
    iget-object v1, p0, Lcom/peel/ui/cj;->i:Landroid/util/SparseArray;

    invoke-virtual {v1, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    .line 591
    if-eqz v0, :cond_0

    .line 592
    sget v2, Lcom/peel/ui/fp;->selected_bar:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 593
    invoke-virtual {p0}, Lcom/peel/ui/cj;->m()Landroid/support/v4/app/ae;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/ae;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/peel/ui/fo;->menu_activity_bar_hidden:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 597
    :cond_0
    if-eqz v1, :cond_1

    .line 598
    sget v0, Lcom/peel/ui/fp;->selected_bar:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 599
    invoke-virtual {p0}, Lcom/peel/ui/cj;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/ae;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/peel/ui/fo;->menu_activity_bar:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 601
    iput p1, p0, Lcom/peel/ui/cj;->aj:I

    .line 602
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/peel/ui/cj;->al:Lcom/peel/d/a;

    .line 603
    iget-object v0, p0, Lcom/peel/ui/cj;->b:Lcom/peel/d/i;

    const-string/jumbo v1, "slideout_selected_id"

    iget v2, p0, Lcom/peel/ui/cj;->aj:I

    invoke-virtual {v0, v1, v2}, Lcom/peel/d/i;->b(Ljava/lang/String;I)V

    .line 605
    :cond_1
    return-void
.end method

.method protected c()V
    .locals 7

    .prologue
    .line 483
    iget-object v0, p0, Lcom/peel/ui/cj;->i:Landroid/util/SparseArray;

    iget v1, p0, Lcom/peel/ui/cj;->aj:I

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 484
    if-nez v0, :cond_0

    .line 488
    :goto_0
    return-void

    .line 485
    :cond_0
    iget-object v6, p0, Lcom/peel/ui/cj;->b:Lcom/peel/d/i;

    new-instance v0, Lcom/peel/d/a;

    sget-object v1, Lcom/peel/d/d;->b:Lcom/peel/d/d;

    sget-object v2, Lcom/peel/d/b;->a:Lcom/peel/d/b;

    sget-object v3, Lcom/peel/d/c;->b:Lcom/peel/d/c;

    sget v4, Lcom/peel/ui/ft;->app_name:I

    .line 487
    invoke-virtual {p0, v4}, Lcom/peel/ui/cj;->a(I)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, Lcom/peel/d/a;-><init>(Lcom/peel/d/d;Lcom/peel/d/b;Lcom/peel/d/c;Ljava/lang/String;Ljava/util/List;)V

    .line 485
    invoke-virtual {v6, v0}, Lcom/peel/d/i;->a(Lcom/peel/d/a;)V

    goto :goto_0
.end method

.method public c(Landroid/os/Bundle;)V
    .locals 7

    .prologue
    const/16 v1, 0x8

    const/16 v6, -0x64

    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 393
    invoke-super {p0, p1}, Lcom/peel/d/u;->c(Landroid/os/Bundle;)V

    .line 394
    iget-object v0, p0, Lcom/peel/ui/cj;->b:Lcom/peel/d/i;

    if-nez v0, :cond_0

    .line 480
    :goto_0
    return-void

    .line 397
    :cond_0
    sget-object v0, Lcom/peel/c/a;->e:Lcom/peel/c/e;

    invoke-static {v0}, Lcom/peel/c/a;->a(Lcom/peel/c/e;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 398
    invoke-virtual {p0}, Lcom/peel/ui/cj;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    invoke-static {v0}, Lcom/peel/social/w;->f(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 399
    iget-object v0, p0, Lcom/peel/ui/cj;->ap:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 400
    iget-object v0, p0, Lcom/peel/ui/cj;->ao:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 402
    invoke-virtual {p0}, Lcom/peel/ui/cj;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    const-string/jumbo v1, "social_accounts_setup"

    invoke-virtual {v0, v1, v4}, Landroid/support/v4/app/ae;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string/jumbo v1, "scalos_pic_url"

    invoke-interface {v0, v1, v5}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 403
    invoke-virtual {p0}, Lcom/peel/ui/cj;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    const-string/jumbo v2, "social_accounts_setup"

    invoke-virtual {v0, v2, v4}, Landroid/support/v4/app/ae;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string/jumbo v2, "scalos_user_name"

    invoke-interface {v0, v2, v5}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 405
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    const-string/jumbo v2, "null"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 406
    :cond_1
    invoke-virtual {p0}, Lcom/peel/ui/cj;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    const-string/jumbo v2, "social_accounts_setup"

    invoke-virtual {v0, v2, v4}, Landroid/support/v4/app/ae;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string/jumbo v2, "scalos_email"

    .line 407
    invoke-interface {v0, v2, v5}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 410
    :cond_2
    iget-object v2, p0, Lcom/peel/ui/cj;->am:Lcom/peel/widget/CircleImageView;

    sget v3, Lcom/peel/ui/fo;->drawer_menu_profile_bg:I

    invoke-virtual {v2, v3}, Lcom/peel/widget/CircleImageView;->setImageResource(I)V

    .line 412
    if-eqz v1, :cond_3

    .line 413
    invoke-virtual {p0}, Lcom/peel/ui/cj;->m()Landroid/support/v4/app/ae;

    move-result-object v2

    invoke-static {v2}, Lcom/peel/util/b/e;->a(Landroid/content/Context;)Lcom/squareup/picasso/Picasso;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/squareup/picasso/Picasso;->load(Ljava/lang/String;)Lcom/squareup/picasso/RequestCreator;

    move-result-object v1

    sget v2, Lcom/peel/ui/fo;->drawer_menu_profile_bg:I

    invoke-virtual {v1, v2}, Lcom/squareup/picasso/RequestCreator;->placeholder(I)Lcom/squareup/picasso/RequestCreator;

    move-result-object v1

    iget-object v2, p0, Lcom/peel/ui/cj;->am:Lcom/peel/widget/CircleImageView;

    invoke-virtual {v1, v2}, Lcom/squareup/picasso/RequestCreator;->into(Landroid/widget/ImageView;)V

    .line 416
    :cond_3
    if-eqz v0, :cond_4

    const-string/jumbo v1, "null"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 417
    iget-object v1, p0, Lcom/peel/ui/cj;->an:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 461
    :cond_4
    :goto_1
    const-string/jumbo v0, "drawer_open"

    invoke-virtual {p1, v0, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_9

    .line 462
    iget-object v0, p0, Lcom/peel/ui/cj;->al:Lcom/peel/d/a;

    if-eqz v0, :cond_5

    .line 463
    iget-object v0, p0, Lcom/peel/ui/cj;->b:Lcom/peel/d/i;

    iget-object v1, p0, Lcom/peel/ui/cj;->al:Lcom/peel/d/a;

    invoke-virtual {v0, v1}, Lcom/peel/d/i;->a(Lcom/peel/d/a;)V

    .line 466
    :cond_5
    iget-object v0, p0, Lcom/peel/ui/cj;->e:Ljava/lang/String;

    if-eqz v0, :cond_6

    .line 467
    invoke-virtual {p0}, Lcom/peel/ui/cj;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/ui/cj;->e:Ljava/lang/String;

    iget-object v2, p0, Lcom/peel/ui/cj;->f:Landroid/os/Bundle;

    const/4 v3, 0x1

    invoke-static {v0, v1, v2, v3}, Lcom/peel/d/e;->a(Landroid/support/v4/app/ae;Ljava/lang/String;Landroid/os/Bundle;Z)V

    .line 468
    iput-object v5, p0, Lcom/peel/ui/cj;->e:Ljava/lang/String;

    .line 471
    :cond_6
    iget-object v0, p0, Lcom/peel/ui/cj;->b:Lcom/peel/d/i;

    const-string/jumbo v1, "slideout_selected_id"

    sget v2, Lcom/peel/ui/fp;->menu_browse:I

    invoke-virtual {v0, v1, v2}, Lcom/peel/d/i;->a(Ljava/lang/String;I)I

    move-result v0

    if-eq v0, v6, :cond_8

    .line 472
    iget-object v0, p0, Lcom/peel/ui/cj;->b:Lcom/peel/d/i;

    const-string/jumbo v1, "slideout_selected_id"

    sget v2, Lcom/peel/ui/fp;->menu_browse:I

    invoke-virtual {v0, v1, v2}, Lcom/peel/d/i;->a(Ljava/lang/String;I)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/peel/ui/cj;->b(I)V

    goto/16 :goto_0

    .line 420
    :cond_7
    iget-object v0, p0, Lcom/peel/ui/cj;->ao:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 421
    iget-object v0, p0, Lcom/peel/ui/cj;->ap:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 422
    iget-object v0, p0, Lcom/peel/ui/cj;->ap:Landroid/view/View;

    new-instance v1, Lcom/peel/ui/cl;

    invoke-direct {v1, p0}, Lcom/peel/ui/cl;-><init>(Lcom/peel/ui/cj;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_1

    .line 474
    :cond_8
    iput v6, p0, Lcom/peel/ui/cj;->aj:I

    goto/16 :goto_0

    .line 477
    :cond_9
    iget-object v0, p0, Lcom/peel/ui/cj;->b:Lcom/peel/d/i;

    invoke-virtual {v0}, Lcom/peel/d/i;->i()Lcom/peel/d/a;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/ui/cj;->al:Lcom/peel/d/a;

    .line 478
    invoke-virtual {p0}, Lcom/peel/ui/cj;->c()V

    goto/16 :goto_0
.end method

.method public e(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 614
    iget-object v0, p0, Lcom/peel/ui/cj;->c:Landroid/os/Bundle;

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->putAll(Landroid/os/Bundle;)V

    .line 615
    invoke-super {p0, p1}, Lcom/peel/d/u;->e(Landroid/os/Bundle;)V

    .line 616
    return-void
.end method

.method public f()V
    .locals 0

    .prologue
    .line 669
    invoke-super {p0}, Lcom/peel/d/u;->f()V

    .line 670
    return-void
.end method

.method public h(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 384
    invoke-super {p0, p1}, Lcom/peel/d/u;->h(Landroid/os/Bundle;)V

    .line 385
    if-eqz p1, :cond_0

    .line 386
    iget-object v0, p0, Lcom/peel/ui/cj;->c:Landroid/os/Bundle;

    invoke-virtual {v0, p1}, Landroid/os/Bundle;->putAll(Landroid/os/Bundle;)V

    .line 388
    :cond_0
    iget-object v0, p0, Lcom/peel/ui/cj;->c:Landroid/os/Bundle;

    invoke-virtual {p0, v0}, Lcom/peel/ui/cj;->c(Landroid/os/Bundle;)V

    .line 389
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 492
    sget-object v2, Lcom/peel/content/a;->h:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/peel/ui/cj;->b:Lcom/peel/d/i;

    if-nez v2, :cond_1

    .line 585
    :cond_0
    :goto_0
    return-void

    .line 496
    :cond_1
    :try_start_0
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    iput-object v2, p0, Lcom/peel/ui/cj;->f:Landroid/os/Bundle;

    .line 497
    iget-object v2, p0, Lcom/peel/ui/cj;->f:Landroid/os/Bundle;

    const-string/jumbo v3, "menuShowing"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 498
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    .line 501
    iget v3, p0, Lcom/peel/ui/cj;->aj:I

    if-eq v2, v3, :cond_3

    .line 502
    sget v3, Lcom/peel/ui/fp;->menu_browse:I

    if-ne v2, v3, :cond_4

    .line 503
    const-class v0, Lcom/peel/ui/lq;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/ui/cj;->e:Ljava/lang/String;

    .line 504
    iget-object v0, p0, Lcom/peel/ui/cj;->f:Landroid/os/Bundle;

    const-string/jumbo v1, "type"

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 505
    iget-object v0, p0, Lcom/peel/ui/cj;->b:Lcom/peel/d/i;

    if-eqz v0, :cond_2

    .line 506
    iget-object v0, p0, Lcom/peel/ui/cj;->b:Lcom/peel/d/i;

    const-string/jumbo v1, "category"

    sget v3, Lcom/peel/ui/ft;->toppicks:I

    invoke-virtual {v0, v1, v3}, Lcom/peel/d/i;->b(Ljava/lang/String;I)V

    .line 578
    :cond_2
    :goto_1
    invoke-virtual {p0, v2}, Lcom/peel/ui/cj;->b(I)V

    .line 581
    :cond_3
    iget-object v0, p0, Lcom/peel/ui/cj;->b:Lcom/peel/d/i;

    invoke-virtual {v0}, Lcom/peel/d/i;->g()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 582
    :catch_0
    move-exception v0

    .line 583
    sget-object v1, Lcom/peel/ui/cj;->h:Ljava/lang/String;

    sget-object v2, Lcom/peel/ui/cj;->h:Ljava/lang/String;

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 508
    :cond_4
    :try_start_1
    sget v3, Lcom/peel/ui/fp;->menu_favorites:I

    if-ne v2, v3, :cond_7

    .line 509
    const-class v1, Lcom/peel/ui/bq;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/peel/ui/cj;->e:Ljava/lang/String;

    .line 510
    iget-object v1, p0, Lcom/peel/ui/cj;->b:Lcom/peel/d/i;

    if-eqz v1, :cond_5

    .line 511
    iget-object v1, p0, Lcom/peel/ui/cj;->b:Lcom/peel/d/i;

    const-string/jumbo v3, "category"

    sget v4, Lcom/peel/ui/ft;->favorites:I

    invoke-virtual {v1, v3, v4}, Lcom/peel/d/i;->b(Ljava/lang/String;I)V

    .line 512
    :cond_5
    invoke-static {}, Lcom/peel/util/a/f;->a()Lcom/peel/util/a/f;

    move-result-object v1

    sget-object v3, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v3}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v3

    if-nez v3, :cond_6

    :goto_2
    const/16 v3, 0x7d7

    const/16 v4, 0x7dd

    invoke-virtual {v1, v0, v3, v4}, Lcom/peel/util/a/f;->a(III)V

    goto :goto_1

    :cond_6
    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v0}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/data/at;->f()I

    move-result v0

    goto :goto_2

    .line 515
    :cond_7
    sget v3, Lcom/peel/ui/fp;->menu_settings:I

    if-ne v2, v3, :cond_8

    .line 516
    const-class v0, Lcom/peel/h/a/gr;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/ui/cj;->e:Ljava/lang/String;

    goto :goto_1

    .line 518
    :cond_8
    sget v3, Lcom/peel/ui/fp;->menu_program_guide:I

    if-ne v2, v3, :cond_9

    .line 519
    const-class v0, Lcom/peel/ui/du;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/ui/cj;->e:Ljava/lang/String;

    goto :goto_1

    .line 521
    :cond_9
    sget v3, Lcom/peel/ui/fp;->menu_send_feedback:I

    if-ne v2, v3, :cond_a

    .line 523
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/peel/ui/cj;->al:Lcom/peel/d/a;

    .line 524
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/peel/ui/cj;->e:Ljava/lang/String;

    .line 525
    iget-object v0, p0, Lcom/peel/ui/cj;->f:Landroid/os/Bundle;

    const-string/jumbo v1, "category"

    invoke-virtual {p0}, Lcom/peel/ui/cj;->n()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/peel/ui/ft;->sendcomment:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 526
    invoke-virtual {p0}, Lcom/peel/ui/cj;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    const-class v1, Lcom/peel/h/a/du;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/peel/ui/cj;->f:Landroid/os/Bundle;

    invoke-static {v0, v1, v2}, Lcom/peel/d/e;->c(Landroid/support/v4/app/ae;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 527
    iget-object v0, p0, Lcom/peel/ui/cj;->b:Lcom/peel/d/i;

    invoke-virtual {v0}, Lcom/peel/d/i;->g()V

    goto/16 :goto_0

    .line 529
    :cond_a
    sget v3, Lcom/peel/ui/fp;->menu_troubleshoot_channel:I

    if-ne v2, v3, :cond_d

    .line 530
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/peel/ui/cj;->al:Lcom/peel/d/a;

    .line 531
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/peel/ui/cj;->e:Ljava/lang/String;

    .line 535
    sget-object v2, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v2}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v2

    invoke-virtual {v2}, Lcom/peel/control/RoomControl;->g()[Ljava/lang/String;

    move-result-object v3

    .line 536
    if-eqz v3, :cond_11

    .line 537
    array-length v4, v3

    move v2, v1

    :goto_3
    if-ge v2, v4, :cond_11

    aget-object v5, v3, v2

    .line 538
    const-string/jumbo v6, "live"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_b

    .line 545
    :goto_4
    if-nez v0, :cond_c

    .line 547
    invoke-virtual {p0}, Lcom/peel/ui/cj;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    const-class v1, Lcom/peel/i/dr;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    invoke-static {v0, v1, v2}, Lcom/peel/d/e;->b(Landroid/support/v4/app/ae;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 554
    :goto_5
    iget-object v0, p0, Lcom/peel/ui/cj;->b:Lcom/peel/d/i;

    invoke-virtual {v0}, Lcom/peel/d/i;->g()V

    goto/16 :goto_0

    .line 537
    :cond_b
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 549
    :cond_c
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 550
    const-string/jumbo v1, "context_id"

    const/16 v2, 0x7d8

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 551
    invoke-virtual {p0}, Lcom/peel/ui/cj;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    const-class v2, Lcom/peel/h/a/cp;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lcom/peel/d/e;->c(Landroid/support/v4/app/ae;Ljava/lang/String;Landroid/os/Bundle;)V

    goto :goto_5

    .line 556
    :cond_d
    sget v0, Lcom/peel/ui/fp;->menu_browse_forum:I

    if-ne v2, v0, :cond_10

    .line 557
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/peel/ui/cj;->al:Lcom/peel/d/a;

    .line 558
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/peel/ui/cj;->e:Ljava/lang/String;

    .line 560
    sget-object v0, Lcom/peel/c/a;->b:Lcom/peel/c/e;

    invoke-static {v0}, Lcom/peel/c/a;->a(Lcom/peel/c/e;)Ljava/lang/Object;

    move-result-object v0

    sget-object v1, Lcom/peel/c/b;->a:Lcom/peel/c/b;

    if-ne v0, v1, :cond_e

    .line 561
    iget-object v0, p0, Lcom/peel/ui/cj;->f:Landroid/os/Bundle;

    const-string/jumbo v1, "url"

    const-string/jumbo v3, "http://help.peel.com/categories/20150906-Samsung-WatchON"

    invoke-virtual {v0, v1, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 567
    :goto_6
    iget-object v0, p0, Lcom/peel/ui/cj;->f:Landroid/os/Bundle;

    const-string/jumbo v1, "title"

    invoke-virtual {p0}, Lcom/peel/ui/cj;->n()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/peel/ui/ft;->browseonlinesupport:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 568
    invoke-virtual {p0}, Lcom/peel/ui/cj;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    const-class v1, Lcom/peel/h/a/fe;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    iget-object v3, p0, Lcom/peel/ui/cj;->f:Landroid/os/Bundle;

    invoke-static {v0, v1, v3}, Lcom/peel/d/e;->c(Landroid/support/v4/app/ae;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 569
    iget-object v0, p0, Lcom/peel/ui/cj;->b:Lcom/peel/d/i;

    invoke-virtual {v0}, Lcom/peel/d/i;->g()V

    .line 571
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/peel/ui/cj;->e:Ljava/lang/String;

    goto/16 :goto_1

    .line 562
    :cond_e
    sget-object v0, Lcom/peel/c/a;->b:Lcom/peel/c/e;

    invoke-static {v0}, Lcom/peel/c/a;->a(Lcom/peel/c/e;)Ljava/lang/Object;

    move-result-object v0

    sget-object v1, Lcom/peel/c/b;->b:Lcom/peel/c/b;

    if-ne v0, v1, :cond_f

    .line 563
    iget-object v0, p0, Lcom/peel/ui/cj;->f:Landroid/os/Bundle;

    const-string/jumbo v1, "url"

    const-string/jumbo v3, "http://help.peel.com/categories/20151486-Smart-remote-for-Samsung-Galaxy-S5"

    invoke-virtual {v0, v1, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_6

    .line 565
    :cond_f
    iget-object v0, p0, Lcom/peel/ui/cj;->f:Landroid/os/Bundle;

    const-string/jumbo v1, "url"

    const-string/jumbo v3, "http://help.peel.com/categories/9272-Peel-Smart-remote"

    invoke-virtual {v0, v1, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_6

    .line 574
    :cond_10
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/peel/ui/cj;->e:Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_1

    :cond_11
    move v0, v1

    goto/16 :goto_4
.end method

.method public w()V
    .locals 4

    .prologue
    .line 657
    invoke-super {p0}, Lcom/peel/d/u;->w()V

    .line 658
    invoke-virtual {p0}, Lcom/peel/ui/cj;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    invoke-static {v0}, Landroid/support/v4/a/q;->a(Landroid/content/Context;)Landroid/support/v4/a/q;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/ui/cj;->aw:Landroid/content/BroadcastReceiver;

    new-instance v2, Landroid/content/IntentFilter;

    const-string/jumbo v3, "samsung_account"

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/a/q;->a(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)V

    .line 659
    return-void
.end method

.method public x()V
    .locals 2

    .prologue
    .line 663
    invoke-super {p0}, Lcom/peel/d/u;->x()V

    .line 664
    invoke-virtual {p0}, Lcom/peel/ui/cj;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    invoke-static {v0}, Landroid/support/v4/a/q;->a(Landroid/content/Context;)Landroid/support/v4/a/q;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/ui/cj;->aw:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/support/v4/a/q;->a(Landroid/content/BroadcastReceiver;)V

    .line 665
    return-void
.end method

.method public y()V
    .locals 1

    .prologue
    .line 674
    invoke-super {p0}, Lcom/peel/d/u;->y()V

    .line 675
    sget-object v0, Lcom/peel/social/e;->e:Lcom/peel/widget/ag;

    if-eqz v0, :cond_0

    .line 676
    sget-object v0, Lcom/peel/social/e;->e:Lcom/peel/widget/ag;

    invoke-virtual {v0}, Lcom/peel/widget/ag;->dismiss()V

    .line 679
    :cond_0
    return-void
.end method
