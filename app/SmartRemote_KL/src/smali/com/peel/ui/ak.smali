.class Lcom/peel/ui/ak;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field final synthetic a:Landroid/os/Bundle;

.field final synthetic b:Ljava/util/ArrayList;

.field final synthetic c:Lcom/peel/data/ContentRoom;

.field final synthetic d:Lcom/peel/ui/ai;


# direct methods
.method constructor <init>(Lcom/peel/ui/ai;Landroid/os/Bundle;Ljava/util/ArrayList;Lcom/peel/data/ContentRoom;)V
    .locals 0

    .prologue
    .line 315
    iput-object p1, p0, Lcom/peel/ui/ak;->d:Lcom/peel/ui/ai;

    iput-object p2, p0, Lcom/peel/ui/ak;->a:Landroid/os/Bundle;

    iput-object p3, p0, Lcom/peel/ui/ak;->b:Ljava/util/ArrayList;

    iput-object p4, p0, Lcom/peel/ui/ak;->c:Lcom/peel/data/ContentRoom;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 318
    iget-object v0, p0, Lcom/peel/ui/ak;->d:Lcom/peel/ui/ai;

    iget-object v0, v0, Lcom/peel/ui/ai;->b:Lcom/peel/widget/ag;

    invoke-virtual {v0}, Lcom/peel/widget/ag;->dismiss()V

    .line 319
    iget-object v1, p0, Lcom/peel/ui/ak;->a:Landroid/os/Bundle;

    const-string/jumbo v2, "device_type"

    iget-object v0, p0, Lcom/peel/ui/ak;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 320
    iget-object v0, p0, Lcom/peel/ui/ak;->a:Landroid/os/Bundle;

    const-string/jumbo v1, "room"

    iget-object v2, p0, Lcom/peel/ui/ak;->c:Lcom/peel/data/ContentRoom;

    invoke-virtual {v2}, Lcom/peel/data/ContentRoom;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 321
    iget-object v0, p0, Lcom/peel/ui/ak;->a:Landroid/os/Bundle;

    const-string/jumbo v1, "back_to_clazz"

    iget-object v2, p0, Lcom/peel/ui/ak;->d:Lcom/peel/ui/ai;

    invoke-static {v2}, Lcom/peel/ui/ai;->a(Lcom/peel/ui/ai;)Landroid/support/v4/app/ae;

    move-result-object v2

    invoke-static {v2}, Lcom/peel/d/e;->a(Landroid/support/v4/app/ae;)Lcom/peel/d/u;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 322
    iget-object v0, p0, Lcom/peel/ui/ak;->d:Lcom/peel/ui/ai;

    invoke-static {v0}, Lcom/peel/ui/ai;->a(Lcom/peel/ui/ai;)Landroid/support/v4/app/ae;

    move-result-object v0

    const-class v1, Lcom/peel/h/a/a;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/peel/ui/ak;->a:Landroid/os/Bundle;

    invoke-static {v0, v1, v2}, Lcom/peel/d/e;->c(Landroid/support/v4/app/ae;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 323
    return-void
.end method
