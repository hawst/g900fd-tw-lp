.class Lcom/peel/ui/ew;
.super Landroid/text/style/ClickableSpan;


# instance fields
.field final synthetic a:I

.field final synthetic b:Ljava/util/List;

.field final synthetic c:I

.field final synthetic d:Lcom/peel/ui/eq;


# direct methods
.method constructor <init>(Lcom/peel/ui/eq;ILjava/util/List;I)V
    .locals 0

    .prologue
    .line 399
    iput-object p1, p0, Lcom/peel/ui/ew;->d:Lcom/peel/ui/eq;

    iput p2, p0, Lcom/peel/ui/ew;->a:I

    iput-object p3, p0, Lcom/peel/ui/ew;->b:Ljava/util/List;

    iput p4, p0, Lcom/peel/ui/ew;->c:I

    invoke-direct {p0}, Landroid/text/style/ClickableSpan;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 13

    .prologue
    const/16 v3, 0x7de

    const/4 v10, 0x1

    const/4 v7, 0x0

    .line 402
    invoke-static {}, Lcom/peel/d/i;->a()Lcom/peel/d/i;

    move-result-object v11

    .line 403
    if-eqz v11, :cond_0

    iget-object v0, p0, Lcom/peel/ui/ew;->d:Lcom/peel/ui/eq;

    iget-object v0, v0, Lcom/peel/ui/eq;->a:Lcom/peel/ui/ek;

    invoke-static {v0}, Lcom/peel/ui/ek;->f(Lcom/peel/ui/ek;)Lcom/peel/content/library/Library;

    move-result-object v0

    if-nez v0, :cond_1

    .line 423
    :cond_0
    :goto_0
    return-void

    .line 407
    :cond_1
    new-instance v12, Landroid/os/Bundle;

    invoke-direct {v12}, Landroid/os/Bundle;-><init>()V

    .line 408
    const-string/jumbo v0, "libraryIds"

    new-array v1, v10, [Ljava/lang/String;

    iget-object v2, p0, Lcom/peel/ui/ew;->d:Lcom/peel/ui/eq;

    iget-object v2, v2, Lcom/peel/ui/eq;->a:Lcom/peel/ui/ek;

    invoke-static {v2}, Lcom/peel/ui/ek;->f(Lcom/peel/ui/ek;)Lcom/peel/content/library/Library;

    move-result-object v2

    invoke-virtual {v2}, Lcom/peel/content/library/Library;->e()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v7

    invoke-virtual {v12, v0, v1}, Landroid/os/Bundle;->putStringArray(Ljava/lang/String;[Ljava/lang/String;)V

    .line 410
    invoke-static {}, Lcom/peel/util/a/f;->a()Lcom/peel/util/a/f;

    move-result-object v0

    sget-object v1, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    .line 411
    invoke-virtual {v1}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v1

    if-nez v1, :cond_2

    move v1, v10

    :goto_1
    const/16 v2, 0x4b3

    const-string/jumbo v4, "Channel Guide"

    iget v5, p0, Lcom/peel/ui/ew;->a:I

    iget-object v6, p0, Lcom/peel/ui/ew;->b:Ljava/util/List;

    iget v8, p0, Lcom/peel/ui/ew;->c:I

    .line 414
    invoke-interface {v6, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/peel/content/listing/Listing;

    invoke-static {v6}, Lcom/peel/util/bx;->b(Lcom/peel/content/listing/Listing;)Ljava/lang/String;

    move-result-object v6

    iget-object v8, p0, Lcom/peel/ui/ew;->b:Ljava/util/List;

    iget v9, p0, Lcom/peel/ui/ew;->c:I

    .line 415
    invoke-interface {v8, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/peel/content/listing/Listing;

    invoke-virtual {v8}, Lcom/peel/content/listing/Listing;->h()Ljava/lang/String;

    move-result-object v8

    move v9, v7

    .line 410
    invoke-virtual/range {v0 .. v9}, Lcom/peel/util/a/f;->a(IIILjava/lang/String;ILjava/lang/String;ILjava/lang/String;I)V

    .line 417
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "listings/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/ui/ew;->d:Lcom/peel/ui/eq;

    iget-object v1, v1, Lcom/peel/ui/eq;->a:Lcom/peel/ui/ek;

    invoke-static {v1}, Lcom/peel/ui/ek;->f(Lcom/peel/ui/ek;)Lcom/peel/content/library/Library;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/content/library/Library;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v0, p0, Lcom/peel/ui/ew;->b:Ljava/util/List;

    check-cast v0, Ljava/util/ArrayList;

    invoke-virtual {v12, v1, v0}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 418
    const-string/jumbo v0, "needsgrouping"

    invoke-virtual {v12, v0, v7}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 419
    const-string/jumbo v0, "fromschedules"

    invoke-virtual {v12, v0, v10}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 420
    const-string/jumbo v0, "category"

    iget-object v1, p0, Lcom/peel/ui/ew;->d:Lcom/peel/ui/eq;

    invoke-static {v1}, Lcom/peel/ui/eq;->a(Lcom/peel/ui/eq;)[Lcom/peel/data/Channel;

    move-result-object v1

    iget v2, p0, Lcom/peel/ui/ew;->a:I

    aget-object v1, v1, v2

    invoke-virtual {v1}, Lcom/peel/data/Channel;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v12, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 421
    const-string/jumbo v0, "context_id"

    invoke-virtual {v12, v0, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 422
    invoke-virtual {v11}, Lcom/peel/d/i;->c()Landroid/support/v4/app/ae;

    move-result-object v0

    const-class v1, Lcom/peel/ui/b/ag;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v12}, Lcom/peel/d/e;->c(Landroid/support/v4/app/ae;Ljava/lang/String;Landroid/os/Bundle;)V

    goto/16 :goto_0

    .line 411
    :cond_2
    sget-object v1, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v1}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/data/at;->f()I

    move-result v1

    goto/16 :goto_1
.end method

.method public updateDrawState(Landroid/text/TextPaint;)V
    .locals 1

    .prologue
    .line 426
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/text/TextPaint;->setUnderlineText(Z)V

    .line 427
    return-void
.end method
