.class Lcom/peel/ui/by;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/peel/ui/kg;


# instance fields
.field final synthetic a:Ljava/util/Map;

.field final synthetic b:J

.field final synthetic c:J

.field final synthetic d:Lcom/peel/ui/bw;


# direct methods
.method constructor <init>(Lcom/peel/ui/bw;Ljava/util/Map;JJ)V
    .locals 1

    .prologue
    .line 395
    iput-object p1, p0, Lcom/peel/ui/by;->d:Lcom/peel/ui/bw;

    iput-object p2, p0, Lcom/peel/ui/by;->a:Ljava/util/Map;

    iput-wide p3, p0, Lcom/peel/ui/by;->b:J

    iput-wide p5, p0, Lcom/peel/ui/by;->c:J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/jess/ui/v;Landroid/view/View;IJLjava/lang/String;)V
    .locals 23
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/jess/ui/v",
            "<*>;",
            "Landroid/view/View;",
            "IJ",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 397
    sget-object v2, Lcom/peel/content/a;->h:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v2

    if-eqz v2, :cond_0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/peel/ui/by;->d:Lcom/peel/ui/bw;

    iget-object v2, v2, Lcom/peel/ui/bw;->b:Lcom/peel/d/i;

    if-nez v2, :cond_1

    .line 603
    :cond_0
    :goto_0
    return-void

    .line 399
    :cond_1
    invoke-virtual/range {p1 .. p1}, Lcom/jess/ui/v;->getAdapter()Landroid/widget/Adapter;

    move-result-object v2

    check-cast v2, Lcom/peel/ui/ka;

    move/from16 v0, p3

    invoke-virtual {v2, v0}, Lcom/peel/ui/ka;->getItem(I)Ljava/lang/Object;

    move-result-object v2

    move-object v12, v2

    check-cast v12, [Lcom/peel/content/listing/Listing;

    move-object/from16 v13, p2

    .line 401
    check-cast v13, Landroid/widget/FrameLayout;

    .line 402
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/peel/ui/by;->d:Lcom/peel/ui/bw;

    invoke-static {v2}, Lcom/peel/ui/bw;->c(Lcom/peel/ui/bw;)Landroid/view/View;

    move-result-object v2

    if-eqz v2, :cond_2

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/peel/ui/by;->d:Lcom/peel/ui/bw;

    invoke-static {v2}, Lcom/peel/ui/bw;->c(Lcom/peel/ui/bw;)Landroid/view/View;

    move-result-object v2

    const-string/jumbo v3, "overlay"

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewWithTag(Ljava/lang/Object;)Landroid/view/View;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 403
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/peel/ui/by;->d:Lcom/peel/ui/bw;

    invoke-static {v2}, Lcom/peel/ui/bw;->c(Lcom/peel/ui/bw;)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/FrameLayout;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/peel/ui/by;->d:Lcom/peel/ui/bw;

    invoke-static {v3}, Lcom/peel/ui/bw;->c(Lcom/peel/ui/bw;)Landroid/view/View;

    move-result-object v3

    const-string/jumbo v4, "overlay"

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewWithTag(Ljava/lang/Object;)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/FrameLayout;->removeView(Landroid/view/View;)V

    .line 405
    :cond_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/peel/ui/by;->d:Lcom/peel/ui/bw;

    move-object/from16 v0, p2

    invoke-static {v2, v0}, Lcom/peel/ui/bw;->a(Lcom/peel/ui/bw;Landroid/view/View;)Landroid/view/View;

    .line 406
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/peel/ui/by;->d:Lcom/peel/ui/bw;

    invoke-virtual {v2}, Lcom/peel/ui/bw;->m()Landroid/support/v4/app/ae;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/ae;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const-string/jumbo v3, "tuneinoptions"

    invoke-static {v2, v3}, Lcom/peel/util/dq;->c(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    .line 408
    const-string/jumbo v2, "overlay"

    invoke-virtual {v13, v2}, Landroid/widget/FrameLayout;->findViewWithTag(Ljava/lang/Object;)Landroid/view/View;

    move-result-object v2

    if-nez v2, :cond_0

    .line 411
    if-eqz v12, :cond_0

    .line 413
    const/4 v2, 0x0

    aget-object v2, v12, v2

    move-object v14, v2

    check-cast v14, Lcom/peel/content/listing/LiveListing;

    .line 415
    invoke-virtual {v14}, Lcom/peel/content/listing/LiveListing;->o()J

    move-result-wide v16

    .line 416
    invoke-virtual {v14}, Lcom/peel/content/listing/LiveListing;->j()J

    move-result-wide v2

    .line 417
    add-long v18, v16, v2

    .line 418
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v20

    .line 419
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/peel/ui/by;->a:Ljava/util/Map;

    move-object/from16 v0, p6

    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/os/Bundle;

    .line 420
    new-instance v15, Landroid/os/Bundle;

    invoke-direct {v15}, Landroid/os/Bundle;-><init>()V

    .line 421
    invoke-virtual {v15, v2}, Landroid/os/Bundle;->putAll(Landroid/os/Bundle;)V

    .line 422
    const-string/jumbo v2, "selected"

    const/4 v3, 0x0

    aget-object v3, v12, v3

    invoke-virtual {v3}, Lcom/peel/content/listing/Listing;->f()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v15, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 423
    const-string/jumbo v2, "comparator"

    const-class v3, Lcom/peel/content/listing/c;

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v15, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 424
    const-string/jumbo v2, "start"

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/peel/ui/by;->b:J

    invoke-virtual {v15, v2, v4, v5}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 425
    const-string/jumbo v2, "end"

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/peel/ui/by;->c:J

    invoke-virtual {v15, v2, v4, v5}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 426
    const-string/jumbo v2, "category"

    move-object/from16 v0, p6

    invoke-virtual {v15, v2, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 428
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/peel/ui/by;->a:Ljava/util/Map;

    move-object/from16 v0, p6

    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/os/Bundle;

    const-string/jumbo v3, "context_id"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 429
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/peel/ui/by;->d:Lcom/peel/ui/bw;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/peel/ui/by;->a:Ljava/util/Map;

    move-object/from16 v0, p6

    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/os/Bundle;

    const-string/jumbo v4, "context_id"

    invoke-virtual {v2, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    iput v2, v3, Lcom/peel/ui/bw;->f:I

    .line 431
    :cond_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/peel/ui/by;->d:Lcom/peel/ui/bw;

    iget v2, v2, Lcom/peel/ui/bw;->f:I

    const/4 v3, -0x1

    if-le v2, v3, :cond_4

    .line 432
    invoke-static {}, Lcom/peel/util/a/f;->a()Lcom/peel/util/a/f;

    move-result-object v2

    sget-object v3, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v3}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v3

    if-nez v3, :cond_b

    const/4 v3, 0x1

    :goto_1
    const/16 v4, 0x4b1

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/peel/ui/by;->d:Lcom/peel/ui/bw;

    iget v5, v5, Lcom/peel/ui/bw;->f:I

    .line 436
    invoke-static {v12}, Lcom/peel/util/bx;->a([Lcom/peel/content/listing/Listing;)Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x0

    const/4 v6, 0x0

    aget-object v6, v12, v6

    .line 438
    invoke-virtual {v6}, Lcom/peel/content/listing/Listing;->h()Ljava/lang/String;

    move-result-object v10

    const/4 v11, 0x0

    move-object/from16 v6, p6

    move/from16 v7, p3

    .line 432
    invoke-virtual/range {v2 .. v11}, Lcom/peel/util/a/f;->a(IIILjava/lang/String;ILjava/lang/String;ILjava/lang/String;I)V

    .line 443
    :cond_4
    const/4 v2, -0x1

    .line 445
    cmp-long v3, v20, v16

    if-lez v3, :cond_c

    cmp-long v3, v20, v18

    if-gez v3, :cond_c

    .line 446
    sget v2, Lcom/peel/ui/fq;->tunein_overlay:I

    move v3, v2

    .line 455
    :goto_2
    const/4 v2, -0x1

    if-le v3, v2, :cond_0

    .line 457
    sget-object v2, Lcom/peel/util/bx;->d:Ljava/util/Map;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v2, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    move-object v11, v2

    check-cast v11, Landroid/view/View;

    .line 463
    sget-object v2, Lcom/peel/util/a;->e:[I

    if-eqz v2, :cond_5

    sget-object v2, Lcom/peel/util/a;->e:[I

    const/4 v4, 0x0

    aget v2, v2, v4

    const/4 v4, -0x1

    if-eq v2, v4, :cond_5

    sget-object v2, Lcom/peel/util/a;->e:[I

    const/4 v4, 0x1

    aget v2, v2, v4

    const/4 v4, -0x1

    if-eq v2, v4, :cond_5

    .line 464
    invoke-virtual {v13}, Landroid/widget/FrameLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    .line 465
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/peel/ui/by;->d:Lcom/peel/ui/bw;

    invoke-virtual {v4}, Lcom/peel/ui/bw;->n()Landroid/content/res/Resources;

    move-result-object v4

    sget v5, Lcom/peel/ui/fn;->tile_width:I

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v4

    float-to-int v4, v4

    sget-object v5, Lcom/peel/util/a;->e:[I

    const/4 v6, 0x0

    aget v5, v5, v6

    add-int/2addr v4, v5

    iput v4, v2, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 466
    invoke-virtual {v11, v2}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 469
    :cond_5
    sget v2, Lcom/peel/ui/fq;->tunein_overlay:I

    if-ne v3, v2, :cond_e

    const/4 v4, 0x1

    .line 471
    :goto_3
    const/4 v10, 0x0

    .line 472
    const/4 v9, 0x0

    .line 474
    sget-object v2, Lcom/peel/content/a;->e:Ljava/lang/String;

    const-string/jumbo v3, "CN"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_f

    .line 475
    const/4 v2, 0x0

    aget-object v2, v12, v2

    invoke-static {v2}, Lcom/peel/util/bx;->d(Lcom/peel/content/listing/Listing;)Ljava/lang/String;

    move-result-object v10

    .line 477
    if-eqz v10, :cond_6

    const/4 v9, 0x1

    .line 482
    :cond_6
    :goto_4
    if-nez v4, :cond_10

    if-eqz v9, :cond_10

    .line 483
    sget v2, Lcom/peel/ui/fp;->btn:I

    invoke-virtual {v11, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    .line 485
    if-eqz v2, :cond_7

    .line 486
    sget v3, Lcom/peel/ui/fo;->editreminder_stateful:I

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 489
    :cond_7
    sget v2, Lcom/peel/ui/fp;->remind_me_txt:I

    invoke-virtual {v11, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    sget v3, Lcom/peel/ui/ft;->undo_reminder:I

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    .line 504
    :cond_8
    :goto_5
    sget v2, Lcom/peel/ui/fp;->btn:I

    invoke-virtual {v11, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v22

    new-instance v2, Lcom/peel/ui/bz;

    move-object/from16 v3, p0

    move-object v5, v14

    move/from16 v6, p3

    move-object/from16 v7, p6

    move-object v8, v12

    invoke-direct/range {v2 .. v10}, Lcom/peel/ui/bz;-><init>(Lcom/peel/ui/by;ZLcom/peel/content/listing/LiveListing;ILjava/lang/String;[Lcom/peel/content/listing/Listing;ZLjava/lang/String;)V

    move-object/from16 v0, v22

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 574
    sget v2, Lcom/peel/ui/fp;->more_btn:I

    invoke-virtual {v11, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    new-instance v2, Lcom/peel/ui/cb;

    move-object/from16 v3, p0

    move-object/from16 v4, p6

    move/from16 v5, p3

    move-object v6, v14

    move-object v7, v15

    invoke-direct/range {v2 .. v7}, Lcom/peel/ui/cb;-><init>(Lcom/peel/ui/by;Ljava/lang/String;ILcom/peel/content/listing/LiveListing;Landroid/os/Bundle;)V

    invoke-virtual {v8, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 589
    cmp-long v2, v20, v16

    if-lez v2, :cond_9

    cmp-long v2, v20, v18

    if-gez v2, :cond_9

    invoke-static {}, Lcom/peel/util/bx;->a()Z

    move-result v2

    if-eqz v2, :cond_9

    .line 590
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/peel/ui/by;->d:Lcom/peel/ui/bw;

    iget-object v2, v2, Lcom/peel/ui/bw;->b:Lcom/peel/d/i;

    const-string/jumbo v3, "popuptoast"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Lcom/peel/d/i;->a(Ljava/lang/String;I)I

    move-result v2

    if-nez v2, :cond_9

    .line 591
    new-instance v2, Landroid/content/Intent;

    const-string/jumbo v3, "show_toast"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 592
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/peel/ui/by;->d:Lcom/peel/ui/bw;

    invoke-virtual {v3}, Lcom/peel/ui/bw;->m()Landroid/support/v4/app/ae;

    move-result-object v3

    invoke-virtual {v3}, Landroid/support/v4/app/ae;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Landroid/support/v4/a/q;->a(Landroid/content/Context;)Landroid/support/v4/a/q;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/support/v4/a/q;->a(Landroid/content/Intent;)Z

    .line 595
    :cond_9
    invoke-virtual {v11}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    if-eqz v2, :cond_a

    .line 596
    invoke-virtual {v11}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    .line 597
    check-cast v2, Landroid/widget/FrameLayout;

    invoke-virtual {v2, v11}, Landroid/widget/FrameLayout;->removeView(Landroid/view/View;)V

    .line 599
    :cond_a
    invoke-virtual {v13, v11}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    goto/16 :goto_0

    .line 432
    :cond_b
    sget-object v3, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v3}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v3

    invoke-virtual {v3}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v3

    invoke-virtual {v3}, Lcom/peel/data/at;->f()I

    move-result v3

    goto/16 :goto_1

    .line 447
    :cond_c
    const-wide/32 v4, 0x1b7740

    add-long v4, v4, v20

    cmp-long v3, v16, v4

    if-lez v3, :cond_d

    array-length v3, v12

    const/4 v4, 0x1

    if-lt v3, v4, :cond_d

    .line 448
    sget v2, Lcom/peel/ui/fq;->setreminder_overlay:I

    move v3, v2

    goto/16 :goto_2

    .line 451
    :cond_d
    const-string/jumbo v3, "set_target"

    const/4 v4, 0x1

    invoke-virtual {v15, v3, v4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 452
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/peel/ui/by;->d:Lcom/peel/ui/bw;

    invoke-virtual {v3}, Lcom/peel/ui/bw;->m()Landroid/support/v4/app/ae;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/peel/ui/by;->d:Lcom/peel/ui/bw;

    invoke-static {v4}, Lcom/peel/ui/bw;->d(Lcom/peel/ui/bw;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v15}, Lcom/peel/d/e;->c(Landroid/support/v4/app/ae;Ljava/lang/String;Landroid/os/Bundle;)V

    move v3, v2

    goto/16 :goto_2

    .line 469
    :cond_e
    const/4 v4, 0x0

    goto/16 :goto_3

    .line 479
    :cond_f
    const/4 v2, 0x0

    aget-object v2, v12, v2

    invoke-static {v2}, Lcom/peel/util/bx;->e(Lcom/peel/content/listing/Listing;)Z

    move-result v9

    goto/16 :goto_4

    .line 490
    :cond_10
    if-nez v4, :cond_8

    .line 491
    sget v2, Lcom/peel/ui/fp;->btn:I

    invoke-virtual {v11, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    .line 493
    if-eqz v2, :cond_11

    .line 494
    sget v3, Lcom/peel/ui/fo;->setreminder_stateful:I

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 497
    :cond_11
    sget v2, Lcom/peel/ui/fp;->remind_me_txt:I

    invoke-virtual {v11, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    sget v3, Lcom/peel/ui/ft;->label_remind_me_all_caps:I

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    goto/16 :goto_5
.end method
