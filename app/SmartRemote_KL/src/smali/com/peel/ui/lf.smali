.class Lcom/peel/ui/lf;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Landroid/view/View;

.field final synthetic b:Lcom/peel/ui/le;


# direct methods
.method constructor <init>(Lcom/peel/ui/le;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 778
    iput-object p1, p0, Lcom/peel/ui/lf;->b:Lcom/peel/ui/le;

    iput-object p2, p0, Lcom/peel/ui/lf;->a:Landroid/view/View;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    .line 781
    iget-object v0, p0, Lcom/peel/ui/lf;->a:Landroid/view/View;

    sget v1, Lcom/peel/ui/fp;->refresh_icon:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 783
    iget-object v1, p0, Lcom/peel/ui/lf;->b:Lcom/peel/ui/le;

    iget-object v1, v1, Lcom/peel/ui/le;->a:Lcom/peel/ui/kr;

    invoke-static {v1}, Lcom/peel/ui/kr;->d(Lcom/peel/ui/kr;)Landroid/animation/ObjectAnimator;

    move-result-object v1

    if-nez v1, :cond_0

    .line 784
    iget-object v1, p0, Lcom/peel/ui/lf;->b:Lcom/peel/ui/le;

    iget-object v1, v1, Lcom/peel/ui/le;->a:Lcom/peel/ui/kr;

    const-string/jumbo v2, "rotation"

    const/4 v3, 0x2

    new-array v3, v3, [F

    fill-array-data v3, :array_0

    invoke-static {v0, v2, v3}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/peel/ui/kr;->a(Lcom/peel/ui/kr;Landroid/animation/ObjectAnimator;)Landroid/animation/ObjectAnimator;

    .line 785
    iget-object v0, p0, Lcom/peel/ui/lf;->b:Lcom/peel/ui/le;

    iget-object v0, v0, Lcom/peel/ui/le;->a:Lcom/peel/ui/kr;

    invoke-static {v0}, Lcom/peel/ui/kr;->d(Lcom/peel/ui/kr;)Landroid/animation/ObjectAnimator;

    move-result-object v0

    const-wide/16 v2, 0x2bc

    invoke-virtual {v0, v2, v3}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 786
    iget-object v0, p0, Lcom/peel/ui/lf;->b:Lcom/peel/ui/le;

    iget-object v0, v0, Lcom/peel/ui/le;->a:Lcom/peel/ui/kr;

    invoke-static {v0}, Lcom/peel/ui/kr;->d(Lcom/peel/ui/kr;)Landroid/animation/ObjectAnimator;

    move-result-object v0

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->setRepeatCount(I)V

    .line 787
    iget-object v0, p0, Lcom/peel/ui/lf;->b:Lcom/peel/ui/le;

    iget-object v0, v0, Lcom/peel/ui/le;->a:Lcom/peel/ui/kr;

    invoke-static {v0}, Lcom/peel/ui/kr;->d(Lcom/peel/ui/kr;)Landroid/animation/ObjectAnimator;

    move-result-object v0

    new-instance v1, Landroid/view/animation/LinearInterpolator;

    invoke-direct {v1}, Landroid/view/animation/LinearInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 790
    :cond_0
    iget-object v0, p0, Lcom/peel/ui/lf;->b:Lcom/peel/ui/le;

    iget-object v0, v0, Lcom/peel/ui/le;->a:Lcom/peel/ui/kr;

    invoke-static {v0}, Lcom/peel/ui/kr;->d(Lcom/peel/ui/kr;)Landroid/animation/ObjectAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    .line 793
    :try_start_0
    sget-object v0, Lcom/peel/util/x;->c:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/text/SimpleDateFormat;

    iget-object v1, p0, Lcom/peel/ui/lf;->b:Lcom/peel/ui/le;

    iget-object v1, v1, Lcom/peel/ui/le;->a:Lcom/peel/ui/kr;

    iget-object v1, v1, Lcom/peel/ui/kr;->c:Landroid/os/Bundle;

    const-string/jumbo v2, "time"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v0

    .line 795
    const-string/jumbo v2, "live"

    invoke-static {v2}, Lcom/peel/content/a;->c(Ljava/lang/String;)Lcom/peel/content/library/Library;

    move-result-object v2

    .line 796
    const/4 v3, 0x0

    invoke-virtual {v2, v3, v0, v1}, Lcom/peel/content/library/Library;->a(IJ)V
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    .line 801
    :goto_0
    iget-object v0, p0, Lcom/peel/ui/lf;->b:Lcom/peel/ui/le;

    iget-object v0, v0, Lcom/peel/ui/le;->a:Lcom/peel/ui/kr;

    invoke-static {v0}, Lcom/peel/ui/kr;->f(Lcom/peel/ui/kr;)V

    .line 802
    return-void

    .line 797
    :catch_0
    move-exception v0

    .line 798
    invoke-virtual {v0}, Ljava/text/ParseException;->printStackTrace()V

    goto :goto_0

    .line 784
    nop

    :array_0
    .array-data 4
        0x0
        0x43340000    # 180.0f
    .end array-data
.end method
