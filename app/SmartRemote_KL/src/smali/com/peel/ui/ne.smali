.class Lcom/peel/ui/ne;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/peel/ui/nd;


# direct methods
.method constructor <init>(Lcom/peel/ui/nd;)V
    .locals 0

    .prologue
    .line 234
    iput-object p1, p0, Lcom/peel/ui/ne;->a:Lcom/peel/ui/nd;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    const/4 v5, 0x0

    .line 237
    iget-object v0, p0, Lcom/peel/ui/ne;->a:Lcom/peel/ui/nd;

    iget-object v0, v0, Lcom/peel/ui/nd;->a:Lcom/peel/ui/nc;

    iget-object v0, v0, Lcom/peel/ui/nc;->a:Lcom/peel/ui/model/TwittData;

    invoke-virtual {v0}, Lcom/peel/ui/model/TwittData;->f()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 238
    iget-object v0, p0, Lcom/peel/ui/ne;->a:Lcom/peel/ui/nd;

    iget-object v0, v0, Lcom/peel/ui/nd;->a:Lcom/peel/ui/nc;

    iget-object v0, v0, Lcom/peel/ui/nc;->b:Lcom/peel/ui/nh;

    iget-object v0, v0, Lcom/peel/ui/nh;->f:Landroid/widget/TextView;

    sget v1, Lcom/peel/ui/fo;->retweet_icon_press:I

    invoke-virtual {v0, v1, v5, v5, v5}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    .line 239
    invoke-static {}, Lcom/peel/util/a/f;->a()Lcom/peel/util/a/f;

    move-result-object v0

    sget-object v1, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v1}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v1

    if-nez v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    const/16 v2, 0x4c0

    iget-object v3, p0, Lcom/peel/ui/ne;->a:Lcom/peel/ui/nd;

    iget-object v3, v3, Lcom/peel/ui/nd;->a:Lcom/peel/ui/nc;

    iget-object v3, v3, Lcom/peel/ui/nc;->c:Lcom/peel/ui/mx;

    .line 240
    invoke-static {v3}, Lcom/peel/ui/mx;->b(Lcom/peel/ui/mx;)I

    move-result v3

    iget-object v4, p0, Lcom/peel/ui/ne;->a:Lcom/peel/ui/nd;

    iget-object v4, v4, Lcom/peel/ui/nd;->a:Lcom/peel/ui/nc;

    iget-object v4, v4, Lcom/peel/ui/nc;->c:Lcom/peel/ui/mx;

    invoke-static {v4}, Lcom/peel/ui/mx;->d(Lcom/peel/ui/mx;)Ljava/lang/String;

    move-result-object v4

    iget-object v6, p0, Lcom/peel/ui/ne;->a:Lcom/peel/ui/nd;

    iget-object v6, v6, Lcom/peel/ui/nd;->a:Lcom/peel/ui/nc;

    iget-object v6, v6, Lcom/peel/ui/nc;->c:Lcom/peel/ui/mx;

    invoke-static {v6}, Lcom/peel/ui/mx;->c(Lcom/peel/ui/mx;)Ljava/lang/String;

    move-result-object v6

    move v7, v5

    .line 239
    invoke-virtual/range {v0 .. v7}, Lcom/peel/util/a/f;->a(IIILjava/lang/String;ILjava/lang/String;I)V

    .line 244
    :goto_1
    return-void

    .line 239
    :cond_0
    sget-object v1, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v1}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/data/at;->f()I

    move-result v1

    goto :goto_0

    .line 242
    :cond_1
    iget-object v0, p0, Lcom/peel/ui/ne;->a:Lcom/peel/ui/nd;

    iget-object v0, v0, Lcom/peel/ui/nd;->a:Lcom/peel/ui/nc;

    iget-object v0, v0, Lcom/peel/ui/nc;->b:Lcom/peel/ui/nh;

    iget-object v0, v0, Lcom/peel/ui/nh;->f:Landroid/widget/TextView;

    sget v1, Lcom/peel/ui/fo;->retweet_icon_normal:I

    invoke-virtual {v0, v1, v5, v5, v5}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    goto :goto_1
.end method
