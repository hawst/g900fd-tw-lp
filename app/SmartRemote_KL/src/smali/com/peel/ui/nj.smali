.class public Lcom/peel/ui/nj;
.super Landroid/support/v4/app/Fragment;


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private b:Landroid/webkit/WebView;

.field private c:Landroid/os/Bundle;

.field private d:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22
    const-class v0, Lcom/peel/ui/nj;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/peel/ui/nj;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 21
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 25
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, Lcom/peel/ui/nj;->c:Landroid/os/Bundle;

    .line 26
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/peel/ui/nj;->d:Z

    return-void
.end method

.method static synthetic a(Lcom/peel/ui/nj;)Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/peel/ui/nj;->c:Landroid/os/Bundle;

    return-object v0
.end method

.method private a(Landroid/webkit/WebSettings;)V
    .locals 1
    .annotation build Landroid/annotation/TargetApi;
        value = 0x11
    .end annotation

    .prologue
    .line 99
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/webkit/WebSettings;->setMediaPlaybackRequiresUserGesture(Z)V

    .line 101
    return-void
.end method

.method private a(Landroid/webkit/WebView;)V
    .locals 1

    .prologue
    .line 104
    iget-boolean v0, p0, Lcom/peel/ui/nj;->d:Z

    if-eqz v0, :cond_0

    .line 105
    const-string/jumbo v0, "javascript:(function() { document.getElementById(\"html5_video\").play(); })()"

    invoke-virtual {p1, v0}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 107
    :cond_0
    return-void
.end method

.method static synthetic a(Lcom/peel/ui/nj;Landroid/webkit/WebView;)V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0, p1}, Lcom/peel/ui/nj;->a(Landroid/webkit/WebView;)V

    return-void
.end method

.method static synthetic a(Lcom/peel/ui/nj;Z)Z
    .locals 0

    .prologue
    .line 21
    iput-boolean p1, p0, Lcom/peel/ui/nj;->d:Z

    return p1
.end method

.method static synthetic b(Lcom/peel/ui/nj;)Landroid/webkit/WebView;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/peel/ui/nj;->b:Landroid/webkit/WebView;

    return-object v0
.end method

.method static synthetic b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 21
    sget-object v0, Lcom/peel/ui/nj;->a:Ljava/lang/String;

    return-object v0
.end method

.method private b(Landroid/webkit/WebView;)V
    .locals 1

    .prologue
    .line 110
    iget-boolean v0, p0, Lcom/peel/ui/nj;->d:Z

    if-eqz v0, :cond_0

    .line 111
    const-string/jumbo v0, "javascript:(function() { document.getElementById(\"html5_video\").pause(); })()"

    invoke-virtual {p1, v0}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 113
    :cond_0
    return-void
.end method


# virtual methods
.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    .line 34
    iget-object v0, p0, Lcom/peel/ui/nj;->b:Landroid/webkit/WebView;

    if-eqz v0, :cond_0

    .line 35
    iget-object v0, p0, Lcom/peel/ui/nj;->b:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->destroy()V

    .line 37
    :cond_0
    sget v0, Lcom/peel/ui/fq;->webview_simple_layout:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/webkit/WebView;

    iput-object v0, p0, Lcom/peel/ui/nj;->b:Landroid/webkit/WebView;

    .line 38
    iget-object v0, p0, Lcom/peel/ui/nj;->b:Landroid/webkit/WebView;

    return-object v0
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 145
    iget-object v0, p0, Lcom/peel/ui/nj;->b:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->canGoBack()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 146
    iget-object v0, p0, Lcom/peel/ui/nj;->b:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->goBack()V

    .line 147
    const/4 v0, 0x1

    .line 150
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a_(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 29
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->a_(Landroid/os/Bundle;)V

    .line 30
    invoke-virtual {p0}, Lcom/peel/ui/nj;->j()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/peel/ui/nj;->c:Landroid/os/Bundle;

    invoke-virtual {p0}, Lcom/peel/ui/nj;->j()Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->putAll(Landroid/os/Bundle;)V

    .line 31
    :cond_0
    return-void
.end method

.method public e(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 154
    const-string/jumbo v0, "bundle"

    new-instance v1, Landroid/os/Bundle;

    iget-object v2, p0, Lcom/peel/ui/nj;->c:Landroid/os/Bundle;

    invoke-direct {v1, v2}, Landroid/os/Bundle;-><init>(Landroid/os/Bundle;)V

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 155
    iget-object v0, p0, Lcom/peel/ui/nj;->b:Landroid/webkit/WebView;

    invoke-virtual {v0, p1}, Landroid/webkit/WebView;->saveState(Landroid/os/Bundle;)Landroid/webkit/WebBackForwardList;

    .line 156
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->e(Landroid/os/Bundle;)V

    .line 157
    return-void
.end method

.method public h(Landroid/os/Bundle;)V
    .locals 4
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "SetJavaScriptEnabled"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    const/4 v1, 0x1

    .line 43
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->h(Landroid/os/Bundle;)V

    .line 45
    iget-object v0, p0, Lcom/peel/ui/nj;->b:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    .line 46
    invoke-virtual {v0, v1}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V

    .line 47
    invoke-virtual {v0, v1}, Landroid/webkit/WebSettings;->setJavaScriptCanOpenWindowsAutomatically(Z)V

    .line 49
    invoke-virtual {v0, v1}, Landroid/webkit/WebSettings;->setDomStorageEnabled(Z)V

    .line 50
    invoke-virtual {v0, v1}, Landroid/webkit/WebSettings;->setDatabaseEnabled(Z)V

    .line 51
    invoke-direct {p0, v0}, Lcom/peel/ui/nj;->a(Landroid/webkit/WebSettings;)V

    .line 53
    if-eqz p1, :cond_0

    const-string/jumbo v1, "bundle"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 54
    iget-object v0, p0, Lcom/peel/ui/nj;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "bundle"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->putAll(Landroid/os/Bundle;)V

    .line 55
    iget-object v0, p0, Lcom/peel/ui/nj;->b:Landroid/webkit/WebView;

    invoke-virtual {v0, p1}, Landroid/webkit/WebView;->restoreState(Landroid/os/Bundle;)Landroid/webkit/WebBackForwardList;

    .line 95
    :goto_0
    return-void

    .line 57
    :cond_0
    iget-object v1, p0, Lcom/peel/ui/nj;->c:Landroid/os/Bundle;

    const-string/jumbo v2, "video"

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 58
    iget-object v0, p0, Lcom/peel/ui/nj;->b:Landroid/webkit/WebView;

    const/4 v1, 0x2

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/webkit/WebView;->setLayerType(ILandroid/graphics/Paint;)V

    .line 64
    :goto_1
    iget-object v0, p0, Lcom/peel/ui/nj;->b:Landroid/webkit/WebView;

    new-instance v1, Lcom/peel/ui/nk;

    invoke-direct {v1, p0}, Lcom/peel/ui/nk;-><init>(Lcom/peel/ui/nj;)V

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 91
    iput-boolean v3, p0, Lcom/peel/ui/nj;->d:Z

    .line 92
    iget-object v0, p0, Lcom/peel/ui/nj;->b:Landroid/webkit/WebView;

    new-instance v1, Landroid/webkit/WebChromeClient;

    invoke-direct {v1}, Landroid/webkit/WebChromeClient;-><init>()V

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->setWebChromeClient(Landroid/webkit/WebChromeClient;)V

    .line 93
    iget-object v0, p0, Lcom/peel/ui/nj;->b:Landroid/webkit/WebView;

    iget-object v1, p0, Lcom/peel/ui/nj;->c:Landroid/os/Bundle;

    const-string/jumbo v2, "url"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    goto :goto_0

    .line 60
    :cond_1
    sget-object v1, Landroid/webkit/WebSettings$RenderPriority;->HIGH:Landroid/webkit/WebSettings$RenderPriority;

    invoke-virtual {v0, v1}, Landroid/webkit/WebSettings;->setRenderPriority(Landroid/webkit/WebSettings$RenderPriority;)V

    .line 61
    iget-object v0, p0, Lcom/peel/ui/nj;->b:Landroid/webkit/WebView;

    invoke-virtual {v0, v3}, Landroid/webkit/WebView;->setBackgroundColor(I)V

    goto :goto_1
.end method

.method public w()V
    .locals 3

    .prologue
    .line 124
    iget-object v0, p0, Lcom/peel/ui/nj;->b:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->onResume()V

    .line 125
    iget-object v0, p0, Lcom/peel/ui/nj;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "video"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 126
    iget-object v0, p0, Lcom/peel/ui/nj;->b:Landroid/webkit/WebView;

    invoke-direct {p0, v0}, Lcom/peel/ui/nj;->a(Landroid/webkit/WebView;)V

    .line 132
    :cond_0
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->w()V

    .line 133
    return-void
.end method

.method public x()V
    .locals 3

    .prologue
    .line 116
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->x()V

    .line 117
    iget-object v0, p0, Lcom/peel/ui/nj;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "video"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 118
    iget-object v0, p0, Lcom/peel/ui/nj;->b:Landroid/webkit/WebView;

    invoke-direct {p0, v0}, Lcom/peel/ui/nj;->b(Landroid/webkit/WebView;)V

    .line 120
    :cond_0
    iget-object v0, p0, Lcom/peel/ui/nj;->b:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->onPause()V

    .line 121
    return-void
.end method

.method public y()V
    .locals 1

    .prologue
    .line 136
    iget-object v0, p0, Lcom/peel/ui/nj;->b:Landroid/webkit/WebView;

    if-eqz v0, :cond_0

    .line 137
    iget-object v0, p0, Lcom/peel/ui/nj;->b:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->destroy()V

    .line 138
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/peel/ui/nj;->b:Landroid/webkit/WebView;

    .line 141
    :cond_0
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->y()V

    .line 142
    return-void
.end method
