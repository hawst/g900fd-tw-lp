.class Lcom/peel/ui/ht;
.super Landroid/widget/ArrayAdapter;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/peel/widget/HorizontalListView;

.field final synthetic b:Lcom/peel/ui/gt;


# direct methods
.method constructor <init>(Lcom/peel/ui/gt;Landroid/content/Context;ILjava/util/List;Lcom/peel/widget/HorizontalListView;)V
    .locals 0

    .prologue
    .line 1056
    iput-object p1, p0, Lcom/peel/ui/ht;->b:Lcom/peel/ui/gt;

    iput-object p5, p0, Lcom/peel/ui/ht;->a:Lcom/peel/widget/HorizontalListView;

    invoke-direct {p0, p2, p3, p4}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    return-void
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 7

    .prologue
    const/4 v6, 0x4

    .line 1059
    iget-object v0, p0, Lcom/peel/ui/ht;->b:Lcom/peel/ui/gt;

    invoke-static {v0}, Lcom/peel/ui/gt;->w(Lcom/peel/ui/gt;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lcom/peel/ui/fq;->controlpad_channel:I

    iget-object v2, p0, Lcom/peel/ui/ht;->a:Lcom/peel/widget/HorizontalListView;

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v4

    .line 1060
    sget v0, Lcom/peel/ui/fp;->channel_image:I

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 1061
    sget v1, Lcom/peel/ui/fp;->channel_callsign:I

    invoke-virtual {v4, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 1062
    sget v2, Lcom/peel/ui/fp;->channel_text:I

    invoke-virtual {v4, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 1063
    iget-object v3, p0, Lcom/peel/ui/ht;->b:Lcom/peel/ui/gt;

    invoke-static {v3}, Lcom/peel/ui/gt;->x(Lcom/peel/ui/gt;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/peel/data/Channel;

    .line 1064
    invoke-virtual {v3}, Lcom/peel/data/Channel;->b()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1065
    invoke-virtual {v3}, Lcom/peel/data/Channel;->m()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1067
    invoke-virtual {v3}, Lcom/peel/data/Channel;->f()Ljava/lang/String;

    move-result-object v3

    .line 1068
    if-eqz v3, :cond_0

    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    if-lez v5, :cond_0

    .line 1070
    iget-object v5, p0, Lcom/peel/ui/ht;->b:Lcom/peel/ui/gt;

    invoke-virtual {v5}, Lcom/peel/ui/gt;->m()Landroid/support/v4/app/ae;

    move-result-object v5

    invoke-static {v5}, Lcom/peel/util/b/e;->a(Landroid/content/Context;)Lcom/squareup/picasso/Picasso;

    move-result-object v5

    invoke-virtual {v5, v3}, Lcom/squareup/picasso/Picasso;->load(Ljava/lang/String;)Lcom/squareup/picasso/RequestCreator;

    move-result-object v3

    sget v5, Lcom/peel/ui/fo;->missing_channel:I

    .line 1071
    invoke-virtual {v3, v5}, Lcom/squareup/picasso/RequestCreator;->placeholder(I)Lcom/squareup/picasso/RequestCreator;

    move-result-object v3

    .line 1072
    invoke-virtual {v3, v0}, Lcom/squareup/picasso/RequestCreator;->into(Landroid/widget/ImageView;)V

    .line 1074
    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1075
    invoke-virtual {v2, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1081
    :goto_0
    iget-object v0, p0, Lcom/peel/ui/ht;->a:Lcom/peel/widget/HorizontalListView;

    new-instance v1, Lcom/peel/ui/hu;

    invoke-direct {v1, p0}, Lcom/peel/ui/hu;-><init>(Lcom/peel/ui/ht;)V

    invoke-virtual {v0, v1}, Lcom/peel/widget/HorizontalListView;->setOnPeelItemPressedListener(Lcom/peel/widget/ad;)V

    .line 1092
    iget-object v0, p0, Lcom/peel/ui/ht;->a:Lcom/peel/widget/HorizontalListView;

    new-instance v1, Lcom/peel/ui/hv;

    invoke-direct {v1, p0}, Lcom/peel/ui/hv;-><init>(Lcom/peel/ui/ht;)V

    invoke-virtual {v0, v1}, Lcom/peel/widget/HorizontalListView;->setOnPeelItemCanceledListener(Lcom/peel/widget/ab;)V

    .line 1103
    iget-object v0, p0, Lcom/peel/ui/ht;->a:Lcom/peel/widget/HorizontalListView;

    new-instance v1, Lcom/peel/ui/hw;

    invoke-direct {v1, p0}, Lcom/peel/ui/hw;-><init>(Lcom/peel/ui/ht;)V

    invoke-virtual {v0, v1}, Lcom/peel/widget/HorizontalListView;->setOnPeelItemConfirmedListener(Lcom/peel/widget/ac;)V

    .line 1155
    return-object v4

    .line 1077
    :cond_0
    sget v1, Lcom/peel/ui/fo;->missing_channel:I

    .line 1078
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0
.end method
