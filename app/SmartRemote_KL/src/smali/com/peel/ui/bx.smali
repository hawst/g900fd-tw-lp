.class Lcom/peel/ui/bx;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Landroid/view/View;

.field final synthetic b:Lcom/peel/ui/bw;


# direct methods
.method constructor <init>(Lcom/peel/ui/bw;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 248
    iput-object p1, p0, Lcom/peel/ui/bx;->b:Lcom/peel/ui/bw;

    iput-object p2, p0, Lcom/peel/ui/bx;->a:Landroid/view/View;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    .line 251
    iget-object v0, p0, Lcom/peel/ui/bx;->a:Landroid/view/View;

    sget v1, Lcom/peel/ui/fp;->refresh_icon:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 253
    iget-object v1, p0, Lcom/peel/ui/bx;->b:Lcom/peel/ui/bw;

    invoke-static {v1}, Lcom/peel/ui/bw;->a(Lcom/peel/ui/bw;)Landroid/animation/ObjectAnimator;

    move-result-object v1

    if-nez v1, :cond_0

    .line 254
    iget-object v1, p0, Lcom/peel/ui/bx;->b:Lcom/peel/ui/bw;

    const-string/jumbo v2, "rotation"

    const/4 v3, 0x2

    new-array v3, v3, [F

    fill-array-data v3, :array_0

    invoke-static {v0, v2, v3}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/peel/ui/bw;->a(Lcom/peel/ui/bw;Landroid/animation/ObjectAnimator;)Landroid/animation/ObjectAnimator;

    .line 255
    iget-object v0, p0, Lcom/peel/ui/bx;->b:Lcom/peel/ui/bw;

    invoke-static {v0}, Lcom/peel/ui/bw;->a(Lcom/peel/ui/bw;)Landroid/animation/ObjectAnimator;

    move-result-object v0

    const-wide/16 v2, 0x2bc

    invoke-virtual {v0, v2, v3}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 256
    iget-object v0, p0, Lcom/peel/ui/bx;->b:Lcom/peel/ui/bw;

    invoke-static {v0}, Lcom/peel/ui/bw;->a(Lcom/peel/ui/bw;)Landroid/animation/ObjectAnimator;

    move-result-object v0

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->setRepeatCount(I)V

    .line 257
    iget-object v0, p0, Lcom/peel/ui/bx;->b:Lcom/peel/ui/bw;

    invoke-static {v0}, Lcom/peel/ui/bw;->a(Lcom/peel/ui/bw;)Landroid/animation/ObjectAnimator;

    move-result-object v0

    new-instance v1, Landroid/view/animation/LinearInterpolator;

    invoke-direct {v1}, Landroid/view/animation/LinearInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 260
    :cond_0
    iget-object v0, p0, Lcom/peel/ui/bx;->b:Lcom/peel/ui/bw;

    invoke-static {v0}, Lcom/peel/ui/bw;->a(Lcom/peel/ui/bw;)Landroid/animation/ObjectAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    .line 262
    iget-object v0, p0, Lcom/peel/ui/bx;->b:Lcom/peel/ui/bw;

    iget-object v0, v0, Lcom/peel/ui/bw;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "scheme"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 263
    const-string/jumbo v1, "live"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 264
    const-string/jumbo v0, "live"

    invoke-static {v0}, Lcom/peel/content/a;->c(Ljava/lang/String;)Lcom/peel/content/library/Library;

    move-result-object v1

    .line 265
    iget-object v0, p0, Lcom/peel/ui/bx;->b:Lcom/peel/ui/bw;

    iget-object v0, v0, Lcom/peel/ui/bw;->b:Lcom/peel/d/i;

    const-string/jumbo v2, "time"

    invoke-virtual {v0, v2}, Lcom/peel/d/i;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 267
    :try_start_0
    sget-object v0, Lcom/peel/util/x;->c:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/text/SimpleDateFormat;

    invoke-virtual {v0, v2}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    .line 268
    const/4 v0, 0x1

    invoke-virtual {v1, v0, v2, v3}, Lcom/peel/content/library/Library;->a(IJ)V
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    .line 274
    :cond_1
    :goto_0
    sget-object v0, Lcom/peel/util/ed;->b:Lcom/peel/util/ed;

    iget-object v1, p0, Lcom/peel/ui/bx;->b:Lcom/peel/ui/bw;

    invoke-static {v1}, Lcom/peel/ui/bw;->b(Lcom/peel/ui/bw;)Lcom/peel/util/dz;

    move-result-object v1

    iget-object v1, v1, Lcom/peel/util/dz;->a:Lcom/peel/util/ed;

    if-eq v0, v1, :cond_2

    .line 275
    iget-object v0, p0, Lcom/peel/ui/bx;->b:Lcom/peel/ui/bw;

    invoke-static {v0}, Lcom/peel/ui/bw;->b(Lcom/peel/ui/bw;)Lcom/peel/util/dz;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/util/dz;->a()V

    .line 277
    :cond_2
    return-void

    .line 269
    :catch_0
    move-exception v0

    .line 270
    invoke-static {}, Lcom/peel/ui/bw;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Ljava/text/ParseException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 254
    nop

    :array_0
    .array-data 4
        0x0
        0x43340000    # 180.0f
    .end array-data
.end method
