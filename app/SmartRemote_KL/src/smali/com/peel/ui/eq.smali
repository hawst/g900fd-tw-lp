.class public Lcom/peel/ui/eq;
.super Landroid/widget/BaseAdapter;


# instance fields
.field final synthetic a:Lcom/peel/ui/ek;

.field private b:[Lcom/peel/data/Channel;

.field private c:Landroid/os/Bundle;


# direct methods
.method public constructor <init>(Lcom/peel/ui/ek;Landroid/os/Bundle;[Lcom/peel/data/Channel;)V
    .locals 0

    .prologue
    .line 238
    iput-object p1, p0, Lcom/peel/ui/eq;->a:Lcom/peel/ui/ek;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 239
    iput-object p2, p0, Lcom/peel/ui/eq;->c:Landroid/os/Bundle;

    .line 240
    iput-object p3, p0, Lcom/peel/ui/eq;->b:[Lcom/peel/data/Channel;

    .line 241
    return-void
.end method

.method static synthetic a(Lcom/peel/ui/eq;)[Lcom/peel/data/Channel;
    .locals 1

    .prologue
    .line 233
    iget-object v0, p0, Lcom/peel/ui/eq;->b:[Lcom/peel/data/Channel;

    return-object v0
.end method

.method static synthetic b(Lcom/peel/ui/eq;)Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 233
    iget-object v0, p0, Lcom/peel/ui/eq;->c:Landroid/os/Bundle;

    return-object v0
.end method


# virtual methods
.method public a(Landroid/os/Bundle;[Lcom/peel/data/Channel;)V
    .locals 0

    .prologue
    .line 244
    iput-object p1, p0, Lcom/peel/ui/eq;->c:Landroid/os/Bundle;

    .line 245
    iput-object p2, p0, Lcom/peel/ui/eq;->b:[Lcom/peel/data/Channel;

    .line 246
    invoke-virtual {p0}, Lcom/peel/ui/eq;->notifyDataSetChanged()V

    .line 247
    return-void
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 250
    iget-object v0, p0, Lcom/peel/ui/eq;->b:[Lcom/peel/data/Channel;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/peel/ui/eq;->b:[Lcom/peel/data/Channel;

    array-length v0, v0

    goto :goto_0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 254
    const/4 v0, 0x0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2

    .prologue
    .line 258
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 19

    .prologue
    .line 262
    .line 263
    if-nez p2, :cond_0

    .line 264
    new-instance v3, Lcom/peel/ui/ex;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/peel/ui/eq;->a:Lcom/peel/ui/ek;

    invoke-direct {v3, v2}, Lcom/peel/ui/ex;-><init>(Lcom/peel/ui/ek;)V

    .line 265
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/peel/ui/eq;->a:Lcom/peel/ui/ek;

    invoke-static {v2}, Lcom/peel/ui/ek;->b(Lcom/peel/ui/ek;)Landroid/view/LayoutInflater;

    move-result-object v2

    sget v4, Lcom/peel/ui/fq;->epg_list_item:I

    const/4 v5, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v2, v4, v0, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 266
    sget v2, Lcom/peel/ui/fp;->tv_epg_text:I

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, v3, Lcom/peel/ui/ex;->a:Landroid/widget/TextView;

    .line 267
    sget v2, Lcom/peel/ui/fp;->tv_add_text:I

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, v3, Lcom/peel/ui/ex;->b:Landroid/widget/TextView;

    .line 268
    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    move-object v4, v3

    .line 272
    :goto_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/peel/ui/eq;->b:[Lcom/peel/data/Channel;

    aget-object v2, v2, p1

    invoke-virtual {v2}, Lcom/peel/data/Channel;->l()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 274
    new-instance v2, Lcom/peel/ui/er;

    move-object/from16 v0, p0

    move/from16 v1, p1

    invoke-direct {v2, v0, v1}, Lcom/peel/ui/er;-><init>(Lcom/peel/ui/eq;I)V

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 320
    iget-object v2, v4, Lcom/peel/ui/ex;->a:Landroid/widget/TextView;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 321
    iget-object v2, v4, Lcom/peel/ui/ex;->b:Landroid/widget/TextView;

    sget v3, Lcom/peel/ui/ft;->add_channel_list_item:I

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    .line 322
    iget-object v2, v4, Lcom/peel/ui/ex;->b:Landroid/widget/TextView;

    sget v3, Lcom/peel/ui/fo;->s5_plus_icon:I

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual {v2, v3, v5, v6, v7}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    .line 323
    iget-object v2, v4, Lcom/peel/ui/ex;->b:Landroid/widget/TextView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 324
    const-string/jumbo v2, "#042936"

    invoke-static {v2}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/view/View;->setBackgroundColor(I)V

    .line 442
    :goto_1
    return-object p2

    .line 270
    :cond_0
    invoke-virtual/range {p2 .. p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/peel/ui/ex;

    move-object v4, v2

    goto :goto_0

    .line 327
    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/peel/ui/eq;->a:Lcom/peel/ui/ek;

    invoke-static {v2}, Lcom/peel/ui/ek;->c(Lcom/peel/ui/ek;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-static/range {p1 .. p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/peel/ui/eq;->c:Landroid/os/Bundle;

    if-eqz v2, :cond_2

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/peel/ui/eq;->c:Landroid/os/Bundle;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/peel/ui/eq;->b:[Lcom/peel/data/Channel;

    aget-object v3, v3, p1

    .line 328
    invoke-virtual {v3}, Lcom/peel/data/Channel;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 329
    :cond_2
    iget-object v2, v4, Lcom/peel/ui/ex;->b:Landroid/widget/TextView;

    sget v3, Lcom/peel/ui/ft;->loading:I

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    .line 330
    iget-object v2, v4, Lcom/peel/ui/ex;->b:Landroid/widget/TextView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 331
    iget-object v2, v4, Lcom/peel/ui/ex;->b:Landroid/widget/TextView;

    const/4 v3, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual {v2, v3, v5, v6, v7}, Landroid/widget/TextView;->setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 332
    iget-object v2, v4, Lcom/peel/ui/ex;->a:Landroid/widget/TextView;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 337
    :goto_2
    iget-object v2, v4, Lcom/peel/ui/ex;->a:Landroid/widget/TextView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 338
    const-string/jumbo v2, "#0a485d"

    invoke-static {v2}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/view/View;->setBackgroundColor(I)V

    .line 346
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/peel/ui/eq;->c:Landroid/os/Bundle;

    if-eqz v2, :cond_b

    .line 347
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/peel/ui/eq;->c:Landroid/os/Bundle;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/peel/ui/eq;->b:[Lcom/peel/data/Channel;

    aget-object v3, v3, p1

    invoke-virtual {v3}, Lcom/peel/data/Channel;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_a

    .line 348
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/peel/ui/eq;->c:Landroid/os/Bundle;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/peel/ui/eq;->b:[Lcom/peel/data/Channel;

    aget-object v3, v3, p1

    invoke-virtual {v3}, Lcom/peel/data/Channel;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v7

    .line 349
    invoke-static {v7}, Lcom/peel/content/listing/LiveListing;->a(Ljava/util/List;)V

    .line 351
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/peel/ui/eq;->c:Landroid/os/Bundle;

    const-string/jumbo v3, "start"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v8

    .line 352
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v6

    .line 353
    new-instance v10, Landroid/text/SpannableStringBuilder;

    invoke-direct {v10}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 354
    const-string/jumbo v5, ""

    .line 356
    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    .line 357
    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v12

    :goto_3
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/os/Parcelable;

    move-object v3, v2

    .line 359
    check-cast v3, Lcom/peel/content/listing/LiveListing;

    invoke-virtual {v3}, Lcom/peel/content/listing/LiveListing;->o()J

    move-result-wide v14

    .line 360
    invoke-virtual {v6, v14, v15}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 363
    cmp-long v3, v8, v14

    if-gtz v3, :cond_4

    .line 364
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v13, "<font color=#35c6df>"

    invoke-virtual {v3, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v13, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string/jumbo v14, "%02d"

    const/4 v15, 0x1

    new-array v15, v15, [Ljava/lang/Object;

    const/16 v16, 0x0

    const/16 v17, 0xc

    move/from16 v0, v17

    invoke-virtual {v6, v0}, Ljava/util/Calendar;->get(I)I

    move-result v17

    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v17

    aput-object v17, v15, v16

    invoke-static {v13, v14, v15}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v3, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v13, "</font> "

    invoke-virtual {v3, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    check-cast v2, Lcom/peel/content/listing/Listing;

    invoke-virtual {v2}, Lcom/peel/content/listing/Listing;->l()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "<br />"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 373
    :goto_4
    invoke-virtual {v5, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_c

    .line 374
    invoke-static {v2}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v3

    invoke-virtual {v10, v3}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 375
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 377
    :goto_5
    invoke-virtual {v10}, Landroid/text/SpannableStringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v11, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move-object v5, v2

    .line 378
    goto/16 :goto_3

    .line 334
    :cond_3
    iget-object v2, v4, Lcom/peel/ui/ex;->b:Landroid/widget/TextView;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 335
    iget-object v2, v4, Lcom/peel/ui/ex;->a:Landroid/widget/TextView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_2

    .line 366
    :cond_4
    sub-long v14, v8, v14

    long-to-float v3, v14

    const v13, 0x4a5bba00    # 3600000.0f

    div-float/2addr v3, v13

    const/high16 v13, 0x3f800000    # 1.0f

    cmpl-float v3, v3, v13

    if-lez v3, :cond_5

    .line 367
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v13, "<font color=#35c6df>&#60;&#60;</font> "

    invoke-virtual {v3, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    check-cast v2, Lcom/peel/content/listing/Listing;

    invoke-virtual {v2}, Lcom/peel/content/listing/Listing;->l()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "<br />"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_4

    .line 369
    :cond_5
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v13, "<font color=#35c6df>&#60;</font> "

    invoke-virtual {v3, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    check-cast v2, Lcom/peel/content/listing/Listing;

    invoke-virtual {v2}, Lcom/peel/content/listing/Listing;->l()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "<br />"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_4

    .line 380
    :cond_6
    new-instance v8, Landroid/text/SpannableString;

    invoke-direct {v8, v10}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 382
    const/4 v3, 0x0

    .line 384
    const/4 v2, 0x0

    move v5, v3

    move v3, v2

    :goto_6
    invoke-virtual {v11}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v3, v2, :cond_9

    .line 385
    if-nez v3, :cond_7

    .line 386
    const/4 v6, 0x0

    .line 387
    invoke-virtual {v11, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 388
    add-int/2addr v5, v2

    .line 399
    :goto_7
    new-instance v9, Lcom/peel/ui/ew;

    move-object/from16 v0, p0

    move/from16 v1, p1

    invoke-direct {v9, v0, v1, v7, v3}, Lcom/peel/ui/ew;-><init>(Lcom/peel/ui/eq;ILjava/util/List;I)V

    const/4 v12, 0x0

    invoke-virtual {v8, v9, v6, v2, v12}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 384
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_6

    .line 389
    :cond_7
    invoke-virtual {v11}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    if-ne v3, v2, :cond_8

    .line 391
    invoke-virtual {v10}, Landroid/text/SpannableStringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    move v6, v5

    goto :goto_7

    .line 394
    :cond_8
    add-int/lit8 v2, v3, -0x1

    invoke-virtual {v11, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v6

    .line 395
    invoke-virtual {v11, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    move/from16 v18, v5

    move v5, v6

    move/from16 v6, v18

    goto :goto_7

    .line 432
    :cond_9
    iget-object v2, v4, Lcom/peel/ui/ex;->a:Landroid/widget/TextView;

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 433
    iget-object v2, v4, Lcom/peel/ui/ex;->a:Landroid/widget/TextView;

    sget-object v3, Landroid/widget/TextView$BufferType;->SPANNABLE:Landroid/widget/TextView$BufferType;

    invoke-virtual {v2, v8, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;Landroid/widget/TextView$BufferType;)V

    goto/16 :goto_1

    .line 436
    :cond_a
    iget-object v2, v4, Lcom/peel/ui/ex;->a:Landroid/widget/TextView;

    const-string/jumbo v3, ""

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 439
    :cond_b
    iget-object v2, v4, Lcom/peel/ui/ex;->a:Landroid/widget/TextView;

    const-string/jumbo v3, ""

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    :cond_c
    move-object v2, v5

    goto/16 :goto_5
.end method
