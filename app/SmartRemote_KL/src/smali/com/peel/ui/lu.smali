.class Lcom/peel/ui/lu;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/peel/ui/lq;


# direct methods
.method constructor <init>(Lcom/peel/ui/lq;)V
    .locals 0

    .prologue
    .line 798
    iput-object p1, p0, Lcom/peel/ui/lu;->a:Lcom/peel/ui/lq;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    .line 801
    const/4 v1, 0x0

    .line 802
    iget-object v0, p0, Lcom/peel/ui/lu;->a:Lcom/peel/ui/lq;

    invoke-static {v0}, Lcom/peel/ui/lq;->b(Lcom/peel/ui/lq;)Lcom/peel/widget/CustomViewPager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/widget/CustomViewPager;->getAdapter()Landroid/support/v4/view/av;

    move-result-object v0

    check-cast v0, Lcom/peel/ui/ds;

    iget-object v2, p0, Lcom/peel/ui/lu;->a:Lcom/peel/ui/lq;

    invoke-static {v2}, Lcom/peel/ui/lq;->d(Lcom/peel/ui/lq;)I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/peel/ui/ds;->d(I)Lcom/peel/d/u;

    move-result-object v0

    .line 804
    instance-of v2, v0, Lcom/peel/ui/kr;

    if-eqz v2, :cond_0

    .line 805
    const/16 v0, 0x7d0

    .line 820
    :goto_0
    invoke-static {}, Lcom/peel/util/a/f;->a()Lcom/peel/util/a/f;

    move-result-object v2

    sget-object v1, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v1}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v1

    if-nez v1, :cond_2

    const/4 v1, 0x1

    :goto_1
    const/16 v3, 0x51f

    invoke-virtual {v2, v1, v3, v0}, Lcom/peel/util/a/f;->a(III)V

    .line 824
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 825
    iget-object v0, p0, Lcom/peel/ui/lu;->a:Lcom/peel/ui/lq;

    invoke-static {v0}, Lcom/peel/ui/lq;->t(Lcom/peel/ui/lq;)Landroid/widget/RelativeLayout;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/widget/RelativeLayout;->removeView(Landroid/view/View;)V

    .line 826
    iget-object v0, p0, Lcom/peel/ui/lu;->a:Lcom/peel/ui/lq;

    invoke-static {v0}, Lcom/peel/ui/lq;->t(Lcom/peel/ui/lq;)Landroid/widget/RelativeLayout;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 827
    return-void

    .line 806
    :cond_0
    instance-of v0, v0, Lcom/peel/ui/bw;

    if-eqz v0, :cond_1

    .line 807
    const-string/jumbo v0, "tvshows"

    .line 808
    iget-object v0, p0, Lcom/peel/ui/lu;->a:Lcom/peel/ui/lq;

    invoke-static {v0}, Lcom/peel/ui/lq;->d(Lcom/peel/ui/lq;)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :cond_1
    move v0, v1

    goto :goto_0

    .line 810
    :pswitch_0
    const/16 v0, 0x7d3

    .line 811
    goto :goto_0

    .line 813
    :pswitch_1
    const/16 v0, 0x7d2

    .line 814
    goto :goto_0

    .line 816
    :pswitch_2
    const/16 v0, 0x7d1

    goto :goto_0

    .line 820
    :cond_2
    sget-object v1, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v1}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/data/at;->f()I

    move-result v1

    goto :goto_1

    .line 808
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
