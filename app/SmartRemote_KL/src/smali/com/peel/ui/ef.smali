.class Lcom/peel/ui/ef;
.super Landroid/widget/ArrayAdapter;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lcom/peel/data/Channel;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/peel/ui/du;


# direct methods
.method constructor <init>(Lcom/peel/ui/du;Landroid/content/Context;I[Lcom/peel/data/Channel;)V
    .locals 0

    .prologue
    .line 322
    iput-object p1, p0, Lcom/peel/ui/ef;->a:Lcom/peel/ui/du;

    invoke-direct {p0, p2, p3, p4}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 5

    .prologue
    .line 324
    invoke-virtual {p0, p1}, Lcom/peel/ui/ef;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/data/Channel;

    .line 325
    if-eqz p2, :cond_1

    .line 327
    :goto_0
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_0

    .line 328
    new-instance v2, Lcom/peel/ui/ej;

    invoke-direct {v2}, Lcom/peel/ui/ej;-><init>()V

    .line 329
    sget v1, Lcom/peel/ui/fp;->channel_image:I

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, v2, Lcom/peel/ui/ej;->a:Landroid/widget/ImageView;

    .line 330
    sget v1, Lcom/peel/ui/fp;->channel_name:I

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, v2, Lcom/peel/ui/ej;->c:Landroid/widget/TextView;

    .line 331
    invoke-virtual {p2, v2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 333
    :cond_0
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/peel/ui/ej;

    .line 334
    iget-object v2, v1, Lcom/peel/ui/ej;->c:Landroid/widget/TextView;

    invoke-virtual {v0}, Lcom/peel/data/Channel;->m()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 335
    invoke-virtual {v0}, Lcom/peel/data/Channel;->f()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/peel/ui/ej;->b:Ljava/lang/String;

    .line 337
    invoke-virtual {v0}, Lcom/peel/data/Channel;->f()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 338
    iget-object v2, v1, Lcom/peel/ui/ej;->a:Landroid/widget/ImageView;

    sget v3, Lcom/peel/ui/fo;->btn_noitem_list:I

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 340
    iget-object v2, p0, Lcom/peel/ui/ef;->a:Lcom/peel/ui/du;

    invoke-virtual {v2}, Lcom/peel/ui/du;->m()Landroid/support/v4/app/ae;

    move-result-object v2

    invoke-static {v2}, Lcom/peel/util/b/e;->a(Landroid/content/Context;)Lcom/squareup/picasso/Picasso;

    move-result-object v2

    iget-object v3, v1, Lcom/peel/ui/ej;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/squareup/picasso/Picasso;->load(Ljava/lang/String;)Lcom/squareup/picasso/RequestCreator;

    move-result-object v2

    iget-object v3, v1, Lcom/peel/ui/ej;->a:Landroid/widget/ImageView;

    new-instance v4, Lcom/peel/ui/eg;

    invoke-direct {v4, p0, v1, v0}, Lcom/peel/ui/eg;-><init>(Lcom/peel/ui/ef;Lcom/peel/ui/ej;Lcom/peel/data/Channel;)V

    invoke-virtual {v2, v3, v4}, Lcom/squareup/picasso/RequestCreator;->into(Landroid/widget/ImageView;Lcom/squareup/picasso/Callback;)V

    .line 359
    :goto_1
    iget-object v1, v1, Lcom/peel/ui/ej;->a:Landroid/widget/ImageView;

    invoke-virtual {v0}, Lcom/peel/data/Channel;->f()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    .line 361
    new-instance v1, Lcom/peel/ui/eh;

    invoke-direct {v1, p0, p1, v0}, Lcom/peel/ui/eh;-><init>(Lcom/peel/ui/ef;ILcom/peel/data/Channel;)V

    invoke-virtual {p2, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 381
    iget-object v0, p0, Lcom/peel/ui/ef;->a:Lcom/peel/ui/du;

    invoke-static {v0}, Lcom/peel/ui/du;->l(Lcom/peel/ui/du;)I

    move-result v0

    if-ne p1, v0, :cond_3

    iget-object v0, p0, Lcom/peel/ui/ef;->a:Lcom/peel/ui/du;

    iget-boolean v0, v0, Lcom/peel/ui/du;->h:Z

    if-eqz v0, :cond_3

    .line 382
    sget v0, Lcom/peel/ui/fm;->common_signin_btn_dark_text_disabled:I

    invoke-virtual {p2, v0}, Landroid/view/View;->setBackgroundResource(I)V

    .line 387
    :goto_2
    return-object p2

    .line 325
    :cond_1
    iget-object v1, p0, Lcom/peel/ui/ef;->a:Lcom/peel/ui/du;

    invoke-static {v1}, Lcom/peel/ui/du;->k(Lcom/peel/ui/du;)Landroid/view/LayoutInflater;

    move-result-object v1

    sget v2, Lcom/peel/ui/fq;->epg_channel_item:I

    iget-object v3, p0, Lcom/peel/ui/ef;->a:Lcom/peel/ui/du;

    invoke-static {v3}, Lcom/peel/ui/du;->j(Lcom/peel/ui/du;)Lcom/peel/widget/ObservableListView;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    goto/16 :goto_0

    .line 356
    :cond_2
    iget-object v2, v1, Lcom/peel/ui/ej;->a:Landroid/widget/ImageView;

    sget v3, Lcom/peel/ui/fo;->btn_noitem_list:I

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_1

    .line 384
    :cond_3
    sget v0, Lcom/peel/ui/fm;->common_signin_btn_dark_text_default:I

    invoke-virtual {p2, v0}, Landroid/view/View;->setBackgroundResource(I)V

    goto :goto_2
.end method
