.class public Lcom/peel/ui/ey;
.super Landroid/support/v4/view/av;


# static fields
.field private static final b:Ljava/lang/String;


# instance fields
.field a:Landroid/os/Handler;

.field private c:Landroid/view/LayoutInflater;

.field private d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/os/Bundle;",
            ">;"
        }
    .end annotation
.end field

.field private e:[Lcom/peel/data/Channel;

.field private f:Lcom/peel/content/library/LiveLibrary;

.field private g:Lcom/peel/data/ContentRoom;

.field private h:Z

.field private i:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private j:Lcom/peel/ui/a/q;

.field private k:I

.field private l:Landroid/content/Context;

.field private m:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/peel/data/Channel;",
            ">;"
        }
    .end annotation
.end field

.field private n:Z

.field private o:Landroid/view/View;

.field private p:I

.field private q:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private r:Ljava/lang/Runnable;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 50
    const-class v0, Lcom/peel/ui/ey;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/peel/ui/ey;->b:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/view/LayoutInflater;ZLcom/peel/ui/a/q;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 70
    invoke-direct {p0}, Landroid/support/v4/view/av;-><init>()V

    .line 57
    iput-boolean v1, p0, Lcom/peel/ui/ey;->h:Z

    .line 58
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/peel/ui/ey;->i:Ljava/util/ArrayList;

    .line 60
    iput v1, p0, Lcom/peel/ui/ey;->k:I

    .line 550
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/peel/ui/ey;->a:Landroid/os/Handler;

    .line 551
    new-instance v0, Lcom/peel/ui/fc;

    invoke-direct {v0, p0}, Lcom/peel/ui/fc;-><init>(Lcom/peel/ui/ey;)V

    iput-object v0, p0, Lcom/peel/ui/ey;->r:Ljava/lang/Runnable;

    .line 71
    iput-object p2, p0, Lcom/peel/ui/ey;->c:Landroid/view/LayoutInflater;

    .line 72
    iput-boolean p3, p0, Lcom/peel/ui/ey;->h:Z

    .line 73
    iput-object p4, p0, Lcom/peel/ui/ey;->j:Lcom/peel/ui/a/q;

    .line 74
    iput-object p1, p0, Lcom/peel/ui/ey;->l:Landroid/content/Context;

    .line 75
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/peel/ui/ey;->q:Ljava/util/ArrayList;

    .line 76
    return-void
.end method

.method static synthetic a(Lcom/peel/ui/ey;Landroid/view/View;)Landroid/view/View;
    .locals 0

    .prologue
    .line 49
    iput-object p1, p0, Lcom/peel/ui/ey;->o:Landroid/view/View;

    return-object p1
.end method

.method static synthetic a(Lcom/peel/ui/ey;)Lcom/peel/data/ContentRoom;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/peel/ui/ey;->g:Lcom/peel/data/ContentRoom;

    return-object v0
.end method

.method static synthetic a(Lcom/peel/ui/ey;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 49
    invoke-direct {p0, p1}, Lcom/peel/ui/ey;->a(Ljava/lang/String;)V

    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 532
    iget-object v0, p0, Lcom/peel/ui/ey;->q:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/16 v1, 0x64

    if-ne v0, v1, :cond_0

    .line 533
    iget-object v0, p0, Lcom/peel/ui/ey;->q:Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 535
    :cond_0
    iget-object v0, p0, Lcom/peel/ui/ey;->q:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 536
    iget-object v0, p0, Lcom/peel/ui/ey;->q:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 538
    :cond_1
    iget-object v0, p0, Lcom/peel/ui/ey;->q:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 540
    return-void
.end method

.method static synthetic a(Lcom/peel/ui/ey;Z)Z
    .locals 0

    .prologue
    .line 49
    iput-boolean p1, p0, Lcom/peel/ui/ey;->n:Z

    return p1
.end method

.method static synthetic b(Lcom/peel/ui/ey;)[Lcom/peel/data/Channel;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/peel/ui/ey;->e:[Lcom/peel/data/Channel;

    return-object v0
.end method

.method static synthetic c(Lcom/peel/ui/ey;)Lcom/peel/content/library/LiveLibrary;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/peel/ui/ey;->f:Lcom/peel/content/library/LiveLibrary;

    return-object v0
.end method

.method static synthetic d(Lcom/peel/ui/ey;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/peel/ui/ey;->m:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic e(Lcom/peel/ui/ey;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/peel/ui/ey;->l:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic f(Lcom/peel/ui/ey;)Landroid/view/LayoutInflater;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/peel/ui/ey;->c:Landroid/view/LayoutInflater;

    return-object v0
.end method

.method static synthetic g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 49
    sget-object v0, Lcom/peel/ui/ey;->b:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic g(Lcom/peel/ui/ey;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/peel/ui/ey;->q:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic h(Lcom/peel/ui/ey;)Z
    .locals 1

    .prologue
    .line 49
    iget-boolean v0, p0, Lcom/peel/ui/ey;->n:Z

    return v0
.end method

.method static synthetic i(Lcom/peel/ui/ey;)I
    .locals 1

    .prologue
    .line 49
    iget v0, p0, Lcom/peel/ui/ey;->p:I

    return v0
.end method

.method static synthetic j(Lcom/peel/ui/ey;)Lcom/peel/ui/a/q;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/peel/ui/ey;->j:Lcom/peel/ui/a/q;

    return-object v0
.end method

.method static synthetic k(Lcom/peel/ui/ey;)Landroid/view/View;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/peel/ui/ey;->o:Landroid/view/View;

    return-object v0
.end method

.method static synthetic l(Lcom/peel/ui/ey;)Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/peel/ui/ey;->r:Ljava/lang/Runnable;

    return-object v0
.end method


# virtual methods
.method public a(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 158
    const/4 v0, -0x2

    return v0
.end method

.method public a(Landroid/view/ViewGroup;I)Ljava/lang/Object;
    .locals 11

    .prologue
    const/4 v5, 0x0

    const/16 v10, 0xc

    const/4 v9, 0x0

    .line 174
    iget-object v0, p0, Lcom/peel/ui/ey;->i:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-le v0, p2, :cond_1

    .line 175
    iget-object v0, p0, Lcom/peel/ui/ey;->i:Ljava/util/ArrayList;

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 176
    if-eqz v0, :cond_1

    .line 177
    iget v1, p0, Lcom/peel/ui/ey;->k:I

    if-ne p2, v1, :cond_0

    .line 178
    iget-object v2, p0, Lcom/peel/ui/ey;->j:Lcom/peel/ui/a/q;

    sget v1, Lcom/peel/ui/fp;->epg_list:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/peel/widget/ObservableListView;

    invoke-virtual {v2, v1}, Lcom/peel/ui/a/q;->a(Lcom/peel/widget/ObservableListView;)V

    .line 292
    :cond_0
    :goto_0
    return-object v0

    .line 183
    :cond_1
    sget-object v0, Lcom/peel/ui/ey;->b:Ljava/lang/String;

    const-string/jumbo v1, "instantiate item"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 186
    :goto_1
    iget-object v0, p0, Lcom/peel/ui/ey;->i:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-gt v0, p2, :cond_2

    .line 187
    iget-object v0, p0, Lcom/peel/ui/ey;->i:Ljava/util/ArrayList;

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 189
    :cond_2
    iget-object v0, p0, Lcom/peel/ui/ey;->c:Landroid/view/LayoutInflater;

    sget v1, Lcom/peel/ui/fq;->epg_sub_page:I

    invoke-virtual {v0, v1, p1, v9}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    .line 190
    sget v1, Lcom/peel/ui/fp;->epg_list:I

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/peel/widget/ObservableListView;

    .line 191
    iget v2, p0, Lcom/peel/ui/ey;->k:I

    if-ne p2, v2, :cond_3

    iget-object v2, p0, Lcom/peel/ui/ey;->j:Lcom/peel/ui/a/q;

    invoke-virtual {v2, v1}, Lcom/peel/ui/a/q;->a(Lcom/peel/widget/ObservableListView;)V

    .line 193
    :cond_3
    iget-object v2, p0, Lcom/peel/ui/ey;->d:Ljava/util/List;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/peel/ui/ey;->d:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge p2, v2, :cond_4

    .line 194
    iget-object v2, p0, Lcom/peel/ui/ey;->d:Ljava/util/List;

    invoke-interface {v2, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/os/Bundle;

    .line 195
    const-string/jumbo v3, "start"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v6

    .line 197
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v8

    .line 198
    invoke-virtual {v8, v6, v7}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 200
    iget-boolean v3, p0, Lcom/peel/ui/ey;->h:Z

    if-eqz v3, :cond_5

    .line 201
    sget v3, Lcom/peel/ui/fp;->timeslot_1:I

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    sget-object v4, Lcom/peel/util/x;->g:Ljava/lang/ThreadLocal;

    invoke-virtual {v4}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/text/SimpleDateFormat;

    invoke-virtual {v8}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v4

    const-string/jumbo v6, "^0+(?!$)"

    const-string/jumbo v7, ""

    invoke-virtual {v4, v6, v7}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 203
    const-string/jumbo v3, "window"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v3

    mul-int/lit8 v3, v3, 0x1e

    invoke-virtual {v8, v10, v3}, Ljava/util/Calendar;->add(II)V

    .line 204
    sget v3, Lcom/peel/ui/fp;->timeslot_2:I

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    sget-object v4, Lcom/peel/util/x;->g:Ljava/lang/ThreadLocal;

    invoke-virtual {v4}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/text/SimpleDateFormat;

    invoke-virtual {v8}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v4

    const-string/jumbo v6, "^0+(?!$)"

    const-string/jumbo v7, ""

    invoke-virtual {v4, v6, v7}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 214
    :goto_2
    new-instance v4, Lcom/peel/ui/fd;

    iget-object v3, p0, Lcom/peel/ui/ey;->d:Ljava/util/List;

    if-nez v3, :cond_6

    move-object v3, v5

    :goto_3
    iget-object v5, p0, Lcom/peel/ui/ey;->e:[Lcom/peel/data/Channel;

    invoke-direct {v4, p0, v3, v5}, Lcom/peel/ui/fd;-><init>(Lcom/peel/ui/ey;Landroid/os/Bundle;[Lcom/peel/data/Channel;)V

    invoke-virtual {v1, v4}, Lcom/peel/widget/ObservableListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 216
    const-string/jumbo v3, "has_listings"

    invoke-virtual {v2, v3, v9}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    if-nez v3, :cond_4

    .line 217
    const-class v3, Lcom/peel/ui/ey;

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v4, "epg library.get()"

    new-instance v5, Lcom/peel/ui/ez;

    invoke-direct {v5, p0, v2, v1}, Lcom/peel/ui/ez;-><init>(Lcom/peel/ui/ey;Landroid/os/Bundle;Lcom/peel/widget/ObservableListView;)V

    invoke-static {v3, v4, v5}, Lcom/peel/util/i;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setTag(Ljava/lang/Object;)V

    .line 290
    :cond_4
    iget-object v1, p0, Lcom/peel/ui/ey;->i:Ljava/util/ArrayList;

    invoke-virtual {v1, p2, v0}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 291
    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    goto/16 :goto_0

    .line 207
    :cond_5
    sget v3, Lcom/peel/ui/fp;->timeslot_1:I

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    sget-object v4, Lcom/peel/util/x;->f:Ljava/lang/ThreadLocal;

    invoke-virtual {v4}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/text/SimpleDateFormat;

    invoke-virtual {v8}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v4

    const-string/jumbo v6, "^0+(?!$)"

    const-string/jumbo v7, ""

    invoke-virtual {v4, v6, v7}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 209
    const-string/jumbo v3, "window"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v3

    mul-int/lit8 v3, v3, 0x1e

    invoke-virtual {v8, v10, v3}, Ljava/util/Calendar;->add(II)V

    .line 210
    sget v3, Lcom/peel/ui/fp;->timeslot_2:I

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    sget-object v4, Lcom/peel/util/x;->f:Ljava/lang/ThreadLocal;

    invoke-virtual {v4}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/text/SimpleDateFormat;

    invoke-virtual {v8}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v4

    const-string/jumbo v6, "^0+(?!$)"

    const-string/jumbo v7, ""

    invoke-virtual {v4, v6, v7}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    .line 214
    :cond_6
    iget-object v3, p0, Lcom/peel/ui/ey;->d:Ljava/util/List;

    invoke-interface {v3, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/os/Bundle;

    goto/16 :goto_3
.end method

.method public a(I)V
    .locals 3

    .prologue
    .line 101
    iget-object v0, p0, Lcom/peel/ui/ey;->i:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt p1, v0, :cond_1

    .line 114
    :cond_0
    :goto_0
    return-void

    .line 103
    :cond_1
    iget-object v0, p0, Lcom/peel/ui/ey;->i:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 104
    if-eqz v0, :cond_0

    .line 106
    iget-object v2, p0, Lcom/peel/ui/ey;->j:Lcom/peel/ui/a/q;

    sget v1, Lcom/peel/ui/fp;->epg_list:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/peel/widget/ObservableListView;

    invoke-virtual {v2, v1}, Lcom/peel/ui/a/q;->b(Lcom/peel/widget/ObservableListView;)V

    .line 108
    iput p1, p0, Lcom/peel/ui/ey;->k:I

    .line 110
    iget-object v1, p0, Lcom/peel/ui/ey;->i:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    .line 111
    if-eqz v1, :cond_0

    .line 113
    iget-object v1, p0, Lcom/peel/ui/ey;->j:Lcom/peel/ui/a/q;

    sget v2, Lcom/peel/ui/fp;->epg_list:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/peel/widget/ObservableListView;

    invoke-virtual {v1, v0}, Lcom/peel/ui/a/q;->a(Lcom/peel/widget/ObservableListView;)V

    goto :goto_0
.end method

.method public a(Landroid/view/ViewGroup;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 166
    move-object v0, p3

    check-cast v0, Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Future;

    check-cast v0, Ljava/util/concurrent/Future;

    .line 167
    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/concurrent/Future;->isDone()Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Ljava/util/concurrent/Future;->cancel(Z)Z

    .line 168
    :cond_0
    iget-object v0, p0, Lcom/peel/ui/ey;->i:Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-virtual {v0, p2, v1}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    move-object v0, p3

    .line 169
    check-cast v0, Landroid/view/View;

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 170
    iget-object v1, p0, Lcom/peel/ui/ey;->j:Lcom/peel/ui/a/q;

    check-cast p3, Landroid/view/View;

    sget v0, Lcom/peel/ui/fp;->epg_list:I

    invoke-virtual {p3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/peel/widget/ObservableListView;

    invoke-virtual {v1, v0}, Lcom/peel/ui/a/q;->b(Lcom/peel/widget/ObservableListView;)V

    .line 171
    return-void
.end method

.method public a(Lcom/peel/content/library/LiveLibrary;)V
    .locals 7

    .prologue
    .line 79
    if-nez p1, :cond_1

    .line 94
    :cond_0
    :goto_0
    return-void

    .line 81
    :cond_1
    iput-object p1, p0, Lcom/peel/ui/ey;->f:Lcom/peel/content/library/LiveLibrary;

    .line 83
    invoke-virtual {p1}, Lcom/peel/content/library/LiveLibrary;->i()[Lcom/peel/data/Channel;

    move-result-object v2

    .line 84
    if-eqz v2, :cond_0

    .line 86
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 87
    array-length v4, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v4, :cond_5

    aget-object v5, v2, v1

    .line 88
    invoke-virtual {v5}, Lcom/peel/data/Channel;->l()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 87
    :cond_2
    :goto_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 89
    :cond_3
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_4

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/data/Channel;

    invoke-virtual {v0}, Lcom/peel/data/Channel;->e()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5}, Lcom/peel/data/Channel;->e()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 91
    :cond_4
    invoke-interface {v3, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 93
    :cond_5
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x1

    if-ge v0, v1, :cond_6

    const/4 v0, 0x0

    :goto_3
    iput-object v0, p0, Lcom/peel/ui/ey;->e:[Lcom/peel/data/Channel;

    goto :goto_0

    :cond_6
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Lcom/peel/data/Channel;

    invoke-interface {v3, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/peel/data/Channel;

    goto :goto_3
.end method

.method public a(Ljava/util/ArrayList;I)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .line 506
    iput p2, p0, Lcom/peel/ui/ey;->p:I

    .line 507
    if-nez p1, :cond_0

    .line 508
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/peel/ui/ey;->m:Ljava/util/ArrayList;

    .line 529
    :goto_0
    return-void

    .line 511
    :cond_0
    iget-object v0, p0, Lcom/peel/ui/ey;->e:[Lcom/peel/data/Channel;

    if-eqz v0, :cond_4

    .line 512
    new-instance v1, Ljava/util/HashMap;

    iget-object v0, p0, Lcom/peel/ui/ey;->e:[Lcom/peel/data/Channel;

    array-length v0, v0

    invoke-direct {v1, v0}, Ljava/util/HashMap;-><init>(I)V

    .line 513
    iget-object v2, p0, Lcom/peel/ui/ey;->e:[Lcom/peel/data/Channel;

    array-length v3, v2

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v3, :cond_1

    aget-object v4, v2, v0

    .line 514
    invoke-virtual {v4}, Lcom/peel/data/Channel;->a()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v1, v5, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 513
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 517
    :cond_1
    new-instance v2, Ljava/util/LinkedHashSet;

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    invoke-direct {v2, v0}, Ljava/util/LinkedHashSet;-><init>(I)V

    .line 518
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 519
    invoke-interface {v1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 520
    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 524
    :cond_3
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/peel/ui/ey;->m:Ljava/util/ArrayList;

    .line 525
    iget-object v0, p0, Lcom/peel/ui/ey;->m:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 528
    :cond_4
    invoke-virtual {p0}, Lcom/peel/ui/ey;->c()V

    goto :goto_0
.end method

.method public a(Ljava/util/Date;Ljava/util/ArrayList;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Date;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/os/Bundle;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 117
    iput-object p2, p0, Lcom/peel/ui/ey;->d:Ljava/util/List;

    .line 119
    invoke-static {}, Lcom/peel/content/a;->a()Lcom/peel/data/ContentRoom;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/ui/ey;->g:Lcom/peel/data/ContentRoom;

    .line 121
    iget-object v0, p0, Lcom/peel/ui/ey;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 122
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v2

    .line 123
    invoke-virtual {v2, p1}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 124
    const/4 v0, 0x5

    const/4 v1, 0x1

    invoke-virtual {v2, v0, v1}, Ljava/util/Calendar;->add(II)V

    .line 125
    const/16 v0, 0xb

    const/4 v1, -0x1

    invoke-virtual {v2, v0, v1}, Ljava/util/Calendar;->set(II)V

    .line 127
    invoke-virtual {p1}, Ljava/util/Date;->getTime()J

    move-result-wide v0

    .line 128
    :goto_0
    invoke-virtual {v2}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v4

    cmp-long v3, v0, v4

    if-gtz v3, :cond_0

    .line 129
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 130
    const-string/jumbo v4, "start"

    invoke-virtual {v3, v4, v0, v1}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 131
    const-string/jumbo v4, "window"

    const/4 v5, 0x2

    invoke-virtual {v3, v4, v5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 132
    iget-object v4, p0, Lcom/peel/ui/ey;->d:Ljava/util/List;

    invoke-interface {v4, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 133
    const-wide/32 v4, 0x36ee80

    add-long/2addr v0, v4

    .line 134
    goto :goto_0

    .line 137
    :cond_0
    invoke-virtual {p0}, Lcom/peel/ui/ey;->c()V

    .line 138
    return-void
.end method

.method public a(Landroid/view/View;Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 154
    invoke-virtual {p1, p2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public b()I
    .locals 1

    .prologue
    .line 146
    iget-object v0, p0, Lcom/peel/ui/ey;->m:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 147
    iget-object v0, p0, Lcom/peel/ui/ey;->m:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 149
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/peel/ui/ey;->d:Ljava/util/List;

    if-nez v0, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/peel/ui/ey;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    goto :goto_0
.end method

.method public d()[Lcom/peel/data/Channel;
    .locals 1

    .prologue
    .line 97
    iget-object v0, p0, Lcom/peel/ui/ey;->e:[Lcom/peel/data/Channel;

    return-object v0
.end method

.method public e()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation

    .prologue
    .line 162
    iget-object v0, p0, Lcom/peel/ui/ey;->i:Ljava/util/ArrayList;

    return-object v0
.end method

.method public f()I
    .locals 1

    .prologue
    .line 560
    iget v0, p0, Lcom/peel/ui/ey;->k:I

    return v0
.end method
