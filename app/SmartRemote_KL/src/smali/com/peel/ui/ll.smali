.class Lcom/peel/ui/ll;
.super Landroid/support/v4/view/av;


# instance fields
.field final synthetic a:Lcom/peel/ui/kr;

.field private b:Landroid/view/LayoutInflater;

.field private c:Landroid/support/v4/view/ViewPager;

.field private d:Lcom/peel/d/i;

.field private e:Landroid/app/AlertDialog;

.field private f:I


# direct methods
.method public constructor <init>(Lcom/peel/ui/kr;Landroid/content/Context;Landroid/support/v4/view/ViewPager;Lcom/peel/d/i;Landroid/app/AlertDialog;I)V
    .locals 2

    .prologue
    .line 485
    iput-object p1, p0, Lcom/peel/ui/ll;->a:Lcom/peel/ui/kr;

    invoke-direct {p0}, Landroid/support/v4/view/av;-><init>()V

    .line 486
    invoke-static {p2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/ui/ll;->b:Landroid/view/LayoutInflater;

    .line 487
    iput-object p3, p0, Lcom/peel/ui/ll;->c:Landroid/support/v4/view/ViewPager;

    .line 488
    iput-object p4, p0, Lcom/peel/ui/ll;->d:Lcom/peel/d/i;

    .line 489
    iput-object p5, p0, Lcom/peel/ui/ll;->e:Landroid/app/AlertDialog;

    .line 490
    iput p6, p0, Lcom/peel/ui/ll;->f:I

    .line 491
    iget-object v0, p0, Lcom/peel/ui/ll;->e:Landroid/app/AlertDialog;

    new-instance v1, Lcom/peel/ui/lm;

    invoke-direct {v1, p0, p1}, Lcom/peel/ui/lm;-><init>(Lcom/peel/ui/ll;Lcom/peel/ui/kr;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V

    .line 499
    return-void
.end method

.method static synthetic a(Lcom/peel/ui/ll;)Landroid/support/v4/view/ViewPager;
    .locals 1

    .prologue
    .line 477
    iget-object v0, p0, Lcom/peel/ui/ll;->c:Landroid/support/v4/view/ViewPager;

    return-object v0
.end method

.method static synthetic b(Lcom/peel/ui/ll;)Landroid/app/AlertDialog;
    .locals 1

    .prologue
    .line 477
    iget-object v0, p0, Lcom/peel/ui/ll;->e:Landroid/app/AlertDialog;

    return-object v0
.end method


# virtual methods
.method public a()Landroid/os/Parcelable;
    .locals 1

    .prologue
    .line 695
    const/4 v0, 0x0

    return-object v0
.end method

.method public a(Landroid/view/ViewGroup;I)Ljava/lang/Object;
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 519
    iget-object v0, p0, Lcom/peel/ui/ll;->b:Landroid/view/LayoutInflater;

    sget v1, Lcom/peel/ui/fq;->tutorial:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v4

    .line 520
    sget v0, Lcom/peel/ui/fp;->next_btn:I

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 521
    sget v1, Lcom/peel/ui/fp;->tutorial_title:I

    invoke-virtual {v4, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 522
    sget v2, Lcom/peel/ui/fp;->tutorial_msg:I

    invoke-virtual {v4, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 524
    new-instance v3, Lcom/peel/ui/ln;

    invoke-direct {v3, p0, p2}, Lcom/peel/ui/ln;-><init>(Lcom/peel/ui/ll;I)V

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 535
    sget v3, Lcom/peel/ui/fo;->continue_stateful:I

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setBackgroundResource(I)V

    .line 536
    packed-switch p2, :pswitch_data_0

    .line 641
    :goto_0
    const/4 v0, 0x0

    invoke-virtual {p1, v4, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;I)V

    .line 642
    return-object v4

    .line 538
    :pswitch_0
    invoke-static {}, Lcom/peel/ui/kr;->S()I

    move-result v3

    if-eq v3, v6, :cond_0

    invoke-static {}, Lcom/peel/ui/kr;->S()I

    move-result v3

    const/4 v5, 0x3

    if-ne v3, v5, :cond_4

    .line 539
    :cond_0
    invoke-static {}, Lcom/peel/ui/kr;->S()I

    move-result v3

    if-ne v3, v6, :cond_1

    iget-object v3, p0, Lcom/peel/ui/ll;->a:Lcom/peel/ui/kr;

    sget v5, Lcom/peel/ui/ft;->ok:I

    invoke-virtual {v3, v5}, Lcom/peel/ui/kr;->a(I)Ljava/lang/String;

    move-result-object v3

    :goto_1
    invoke-virtual {v0, v3}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 540
    invoke-static {}, Lcom/peel/ui/kr;->S()I

    move-result v3

    if-ne v3, v6, :cond_2

    sget v3, Lcom/peel/ui/fo;->v1_step_01_bg:I

    :goto_2
    invoke-virtual {v4, v3}, Landroid/view/View;->setBackgroundResource(I)V

    .line 541
    iget-object v3, p0, Lcom/peel/ui/ll;->a:Lcom/peel/ui/kr;

    sget v5, Lcom/peel/ui/ft;->tutorial_title_1:I

    invoke-virtual {v3, v5}, Lcom/peel/ui/kr;->a(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 542
    iget-object v3, p0, Lcom/peel/ui/ll;->a:Lcom/peel/ui/kr;

    invoke-static {}, Lcom/peel/ui/kr;->S()I

    move-result v1

    if-ne v1, v6, :cond_3

    sget v1, Lcom/peel/ui/ft;->tutorial_msg_1:I

    :goto_3
    invoke-virtual {v3, v1}, Lcom/peel/ui/kr;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 543
    new-instance v1, Lcom/peel/ui/lo;

    invoke-direct {v1, p0, p2}, Lcom/peel/ui/lo;-><init>(Lcom/peel/ui/ll;I)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    .line 539
    :cond_1
    iget-object v3, p0, Lcom/peel/ui/ll;->a:Lcom/peel/ui/kr;

    sget v5, Lcom/peel/ui/ft;->tutorial_btn:I

    invoke-virtual {v3, v5}, Lcom/peel/ui/kr;->a(I)Ljava/lang/String;

    move-result-object v3

    goto :goto_1

    .line 540
    :cond_2
    sget v3, Lcom/peel/ui/fo;->v3_step_01_bg:I

    goto :goto_2

    .line 542
    :cond_3
    sget v1, Lcom/peel/ui/ft;->tutorial_msg_3:I

    goto :goto_3

    .line 582
    :cond_4
    iget-object v3, p0, Lcom/peel/ui/ll;->a:Lcom/peel/ui/kr;

    sget v5, Lcom/peel/ui/ft;->next:I

    invoke-virtual {v3, v5}, Lcom/peel/ui/kr;->a(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 583
    sget v0, Lcom/peel/ui/fo;->v2_step_01_bg:I

    invoke-virtual {v4, v0}, Landroid/view/View;->setBackgroundResource(I)V

    .line 584
    iget-object v0, p0, Lcom/peel/ui/ll;->a:Lcom/peel/ui/kr;

    sget v3, Lcom/peel/ui/ft;->tutorial_title_2_step1:I

    invoke-virtual {v0, v3}, Lcom/peel/ui/kr;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 585
    iget-object v0, p0, Lcom/peel/ui/ll;->a:Lcom/peel/ui/kr;

    sget v1, Lcom/peel/ui/ft;->tutorial_msg_2_step1:I

    invoke-virtual {v0, v1}, Lcom/peel/ui/kr;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 589
    :pswitch_1
    iget-object v3, p0, Lcom/peel/ui/ll;->a:Lcom/peel/ui/kr;

    sget v5, Lcom/peel/ui/ft;->next:I

    invoke-virtual {v3, v5}, Lcom/peel/ui/kr;->a(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 590
    sget v0, Lcom/peel/ui/fo;->v2_step_02_bg:I

    invoke-virtual {v4, v0}, Landroid/view/View;->setBackgroundResource(I)V

    .line 591
    iget-object v0, p0, Lcom/peel/ui/ll;->a:Lcom/peel/ui/kr;

    sget v3, Lcom/peel/ui/ft;->tutorial_title_2_step2:I

    invoke-virtual {v0, v3}, Lcom/peel/ui/kr;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 592
    iget-object v0, p0, Lcom/peel/ui/ll;->a:Lcom/peel/ui/kr;

    sget v1, Lcom/peel/ui/ft;->tutorial_msg_2_step2:I

    invoke-virtual {v0, v1}, Lcom/peel/ui/kr;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 595
    :pswitch_2
    iget-object v3, p0, Lcom/peel/ui/ll;->a:Lcom/peel/ui/kr;

    sget v5, Lcom/peel/ui/ft;->tutorial_btn:I

    invoke-virtual {v3, v5}, Lcom/peel/ui/kr;->a(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 596
    sget v3, Lcom/peel/ui/fo;->v2_step_03_bg:I

    invoke-virtual {v4, v3}, Landroid/view/View;->setBackgroundResource(I)V

    .line 597
    iget-object v3, p0, Lcom/peel/ui/ll;->a:Lcom/peel/ui/kr;

    sget v5, Lcom/peel/ui/ft;->tutorial_title_1:I

    invoke-virtual {v3, v5}, Lcom/peel/ui/kr;->a(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 598
    iget-object v1, p0, Lcom/peel/ui/ll;->a:Lcom/peel/ui/kr;

    sget v3, Lcom/peel/ui/ft;->tutorial_msg_3:I

    invoke-virtual {v1, v3}, Lcom/peel/ui/kr;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 599
    new-instance v1, Lcom/peel/ui/lp;

    invoke-direct {v1, p0, p2}, Lcom/peel/ui/lp;-><init>(Lcom/peel/ui/ll;I)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_0

    .line 536
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public a(Landroid/os/Parcelable;Ljava/lang/ClassLoader;)V
    .locals 0

    .prologue
    .line 691
    return-void
.end method

.method public a(Landroid/view/ViewGroup;)V
    .locals 0

    .prologue
    .line 700
    return-void
.end method

.method public a(Landroid/view/ViewGroup;ILjava/lang/Object;)V
    .locals 0

    .prologue
    .line 657
    check-cast p3, Landroid/widget/RelativeLayout;

    invoke-virtual {p1, p3}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 658
    return-void
.end method

.method public a(Landroid/view/View;Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 672
    if-ne p1, p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()I
    .locals 1

    .prologue
    .line 503
    iget v0, p0, Lcom/peel/ui/ll;->f:I

    return v0
.end method

.method public b(Landroid/view/ViewGroup;)V
    .locals 0

    .prologue
    .line 686
    return-void
.end method
