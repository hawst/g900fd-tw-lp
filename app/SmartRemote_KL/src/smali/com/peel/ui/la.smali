.class Lcom/peel/ui/la;
.super Lcom/peel/util/t;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/peel/util/t",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/peel/ui/kr;


# direct methods
.method constructor <init>(Lcom/peel/ui/kr;)V
    .locals 0

    .prologue
    .line 394
    iput-object p1, p0, Lcom/peel/ui/la;->a:Lcom/peel/ui/kr;

    invoke-direct {p0}, Lcom/peel/util/t;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(ZLjava/lang/Object;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 394
    check-cast p2, Ljava/lang/String;

    invoke-virtual {p0, p1, p2, p3}, Lcom/peel/ui/la;->a(ZLjava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public a(ZLjava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 397
    if-eqz p1, :cond_1

    .line 399
    invoke-static {}, Lcom/peel/ui/kr;->c()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Show remote setup tutorial A/B testing result: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 400
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 401
    sput-object p2, Lcom/peel/util/a;->n:Ljava/lang/String;

    .line 403
    :cond_0
    invoke-static {}, Lcom/peel/ui/kr;->c()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "Show tutorial dialog"

    new-instance v2, Lcom/peel/ui/lb;

    invoke-direct {v2, p0}, Lcom/peel/ui/lb;-><init>(Lcom/peel/ui/la;)V

    invoke-static {v0, v1, v2}, Lcom/peel/util/i;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/lang/Runnable;

    .line 410
    :cond_1
    return-void
.end method
