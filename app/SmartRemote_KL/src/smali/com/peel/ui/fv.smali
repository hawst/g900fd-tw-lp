.class public final Lcom/peel/ui/fv;
.super Ljava/lang/Object;


# static fields
.field public static final ActionBar:[I

.field public static final ActionBarLayout:[I

.field public static final ActionBarLayout_android_layout_gravity:I = 0x0

.field public static final ActionBar_background:I = 0xa

.field public static final ActionBar_backgroundSplit:I = 0xc

.field public static final ActionBar_backgroundStacked:I = 0xb

.field public static final ActionBar_contentInsetEnd:I = 0x15

.field public static final ActionBar_contentInsetLeft:I = 0x16

.field public static final ActionBar_contentInsetRight:I = 0x17

.field public static final ActionBar_contentInsetStart:I = 0x14

.field public static final ActionBar_customNavigationLayout:I = 0xd

.field public static final ActionBar_displayOptions:I = 0x3

.field public static final ActionBar_divider:I = 0x9

.field public static final ActionBar_elevation:I = 0x18

.field public static final ActionBar_height:I = 0x0

.field public static final ActionBar_hideOnContentScroll:I = 0x13

.field public static final ActionBar_homeAsUpIndicator:I = 0x1a

.field public static final ActionBar_homeLayout:I = 0xe

.field public static final ActionBar_icon:I = 0x7

.field public static final ActionBar_indeterminateProgressStyle:I = 0x10

.field public static final ActionBar_itemPadding:I = 0x12

.field public static final ActionBar_logo:I = 0x8

.field public static final ActionBar_navigationMode:I = 0x2

.field public static final ActionBar_popupTheme:I = 0x19

.field public static final ActionBar_progressBarPadding:I = 0x11

.field public static final ActionBar_progressBarStyle:I = 0xf

.field public static final ActionBar_subtitle:I = 0x4

.field public static final ActionBar_subtitleTextStyle:I = 0x6

.field public static final ActionBar_title:I = 0x1

.field public static final ActionBar_titleTextStyle:I = 0x5

.field public static final ActionMenuItemView:[I

.field public static final ActionMenuItemView_android_minWidth:I = 0x0

.field public static final ActionMenuView:[I

.field public static final ActionMode:[I

.field public static final ActionMode_background:I = 0x3

.field public static final ActionMode_backgroundSplit:I = 0x4

.field public static final ActionMode_closeItemLayout:I = 0x5

.field public static final ActionMode_height:I = 0x0

.field public static final ActionMode_subtitleTextStyle:I = 0x2

.field public static final ActionMode_titleTextStyle:I = 0x1

.field public static final ActivityChooserView:[I

.field public static final ActivityChooserView_expandActivityOverflowButtonDrawable:I = 0x1

.field public static final ActivityChooserView_initialActivityCount:I = 0x0

.field public static final AdsAttrs:[I

.field public static final AdsAttrs_adSize:I = 0x0

.field public static final AdsAttrs_adSizes:I = 0x1

.field public static final AdsAttrs_adUnitId:I = 0x2

.field public static final CircleImageView:[I

.field public static final CircleImageView_border_color:I = 0x1

.field public static final CircleImageView_border_width:I = 0x0

.field public static final CirclePageIndicator:[I

.field public static final CirclePageIndicator_android_background:I = 0x1

.field public static final CirclePageIndicator_android_orientation:I = 0x0

.field public static final CirclePageIndicator_centered:I = 0x2

.field public static final CirclePageIndicator_fillColor:I = 0x4

.field public static final CirclePageIndicator_pageColor:I = 0x5

.field public static final CirclePageIndicator_radius:I = 0x6

.field public static final CirclePageIndicator_snap:I = 0x7

.field public static final CirclePageIndicator_strokeColor:I = 0x8

.field public static final CirclePageIndicator_strokeWidth:I = 0x3

.field public static final CompatTextView:[I

.field public static final CompatTextView_textAllCaps:I = 0x0

.field public static final DrawerArrowToggle:[I

.field public static final DrawerArrowToggle_barSize:I = 0x6

.field public static final DrawerArrowToggle_color:I = 0x0

.field public static final DrawerArrowToggle_drawableSize:I = 0x2

.field public static final DrawerArrowToggle_gapBetweenBars:I = 0x3

.field public static final DrawerArrowToggle_middleBarArrowSize:I = 0x5

.field public static final DrawerArrowToggle_spinBars:I = 0x1

.field public static final DrawerArrowToggle_thickness:I = 0x7

.field public static final DrawerArrowToggle_topBottomBarArrowSize:I = 0x4

.field public static final LinePageIndicator:[I

.field public static final LinePageIndicator_android_background:I = 0x0

.field public static final LinePageIndicator_centered:I = 0x1

.field public static final LinePageIndicator_gapWidth:I = 0x6

.field public static final LinePageIndicator_lineWidth:I = 0x5

.field public static final LinePageIndicator_selectedColor:I = 0x2

.field public static final LinePageIndicator_strokeWidth:I = 0x3

.field public static final LinePageIndicator_unselectedColor:I = 0x4

.field public static final LinearLayoutCompat:[I

.field public static final LinearLayoutCompat_Layout:[I

.field public static final LinearLayoutCompat_Layout_android_layout_gravity:I = 0x0

.field public static final LinearLayoutCompat_Layout_android_layout_height:I = 0x2

.field public static final LinearLayoutCompat_Layout_android_layout_weight:I = 0x3

.field public static final LinearLayoutCompat_Layout_android_layout_width:I = 0x1

.field public static final LinearLayoutCompat_android_baselineAligned:I = 0x2

.field public static final LinearLayoutCompat_android_baselineAlignedChildIndex:I = 0x3

.field public static final LinearLayoutCompat_android_gravity:I = 0x0

.field public static final LinearLayoutCompat_android_orientation:I = 0x1

.field public static final LinearLayoutCompat_android_weightSum:I = 0x4

.field public static final LinearLayoutCompat_divider:I = 0x5

.field public static final LinearLayoutCompat_dividerPadding:I = 0x8

.field public static final LinearLayoutCompat_measureWithLargestChild:I = 0x6

.field public static final LinearLayoutCompat_showDividers:I = 0x7

.field public static final ListPopupWindow:[I

.field public static final ListPopupWindow_android_dropDownHorizontalOffset:I = 0x0

.field public static final ListPopupWindow_android_dropDownVerticalOffset:I = 0x1

.field public static final MapAttrs:[I

.field public static final MapAttrs_cameraBearing:I = 0x1

.field public static final MapAttrs_cameraTargetLat:I = 0x2

.field public static final MapAttrs_cameraTargetLng:I = 0x3

.field public static final MapAttrs_cameraTilt:I = 0x4

.field public static final MapAttrs_cameraZoom:I = 0x5

.field public static final MapAttrs_mapType:I = 0x0

.field public static final MapAttrs_uiCompass:I = 0x6

.field public static final MapAttrs_uiRotateGestures:I = 0x7

.field public static final MapAttrs_uiScrollGestures:I = 0x8

.field public static final MapAttrs_uiTiltGestures:I = 0x9

.field public static final MapAttrs_uiZoomControls:I = 0xa

.field public static final MapAttrs_uiZoomGestures:I = 0xb

.field public static final MapAttrs_useViewLifecycle:I = 0xc

.field public static final MapAttrs_zOrderOnTop:I = 0xd

.field public static final MenuGroup:[I

.field public static final MenuGroup_android_checkableBehavior:I = 0x5

.field public static final MenuGroup_android_enabled:I = 0x0

.field public static final MenuGroup_android_id:I = 0x1

.field public static final MenuGroup_android_menuCategory:I = 0x3

.field public static final MenuGroup_android_orderInCategory:I = 0x4

.field public static final MenuGroup_android_visible:I = 0x2

.field public static final MenuItem:[I

.field public static final MenuItem_actionLayout:I = 0xe

.field public static final MenuItem_actionProviderClass:I = 0x10

.field public static final MenuItem_actionViewClass:I = 0xf

.field public static final MenuItem_android_alphabeticShortcut:I = 0x9

.field public static final MenuItem_android_checkable:I = 0xb

.field public static final MenuItem_android_checked:I = 0x3

.field public static final MenuItem_android_enabled:I = 0x1

.field public static final MenuItem_android_icon:I = 0x0

.field public static final MenuItem_android_id:I = 0x2

.field public static final MenuItem_android_menuCategory:I = 0x5

.field public static final MenuItem_android_numericShortcut:I = 0xa

.field public static final MenuItem_android_onClick:I = 0xc

.field public static final MenuItem_android_orderInCategory:I = 0x6

.field public static final MenuItem_android_title:I = 0x7

.field public static final MenuItem_android_titleCondensed:I = 0x8

.field public static final MenuItem_android_visible:I = 0x4

.field public static final MenuItem_showAsAction:I = 0xd

.field public static final MenuView:[I

.field public static final MenuView_android_headerBackground:I = 0x4

.field public static final MenuView_android_horizontalDivider:I = 0x2

.field public static final MenuView_android_itemBackground:I = 0x5

.field public static final MenuView_android_itemIconDisabledAlpha:I = 0x6

.field public static final MenuView_android_itemTextAppearance:I = 0x1

.field public static final MenuView_android_verticalDivider:I = 0x3

.field public static final MenuView_android_windowAnimationStyle:I = 0x0

.field public static final MenuView_preserveIconSpacing:I = 0x7

.field public static final PeelSlider:[I

.field public static final PeelSlider_duration:I = 0x3

.field public static final PeelSlider_host:I = 0x1

.field public static final PeelSlider_menu:I = 0x0

.field public static final PeelSlider_offset:I = 0x2

.field public static final PeelSlider_touchWidth:I = 0x4

.field public static final PopupWindow:[I

.field public static final PopupWindowBackgroundState:[I

.field public static final PopupWindowBackgroundState_state_above_anchor:I = 0x0

.field public static final PopupWindow_android_popupBackground:I = 0x0

.field public static final PopupWindow_overlapAnchor:I = 0x1

.field public static final SearchView:[I

.field public static final SearchView_android_focusable:I = 0x0

.field public static final SearchView_android_imeOptions:I = 0x3

.field public static final SearchView_android_inputType:I = 0x2

.field public static final SearchView_android_maxWidth:I = 0x1

.field public static final SearchView_closeIcon:I = 0x7

.field public static final SearchView_commitIcon:I = 0xb

.field public static final SearchView_goIcon:I = 0x8

.field public static final SearchView_iconifiedByDefault:I = 0x5

.field public static final SearchView_layout:I = 0x4

.field public static final SearchView_queryBackground:I = 0xd

.field public static final SearchView_queryHint:I = 0x6

.field public static final SearchView_searchIcon:I = 0x9

.field public static final SearchView_submitBackground:I = 0xe

.field public static final SearchView_suggestionRowLayout:I = 0xc

.field public static final SearchView_voiceIcon:I = 0xa

.field public static final SlidingDrawer:[I

.field public static final SlidingDrawer_allowSingleTap:I = 0x5

.field public static final SlidingDrawer_animateOnClick:I = 0x6

.field public static final SlidingDrawer_bottomOffset:I = 0x3

.field public static final SlidingDrawer_content:I = 0x1

.field public static final SlidingDrawer_handle:I = 0x0

.field public static final SlidingDrawer_orientation:I = 0x2

.field public static final SlidingDrawer_topDistance:I = 0x7

.field public static final SlidingDrawer_topOffset:I = 0x4

.field public static final Spinner:[I

.field public static final Spinner_android_background:I = 0x1

.field public static final Spinner_android_dropDownHorizontalOffset:I = 0x5

.field public static final Spinner_android_dropDownSelector:I = 0x2

.field public static final Spinner_android_dropDownVerticalOffset:I = 0x6

.field public static final Spinner_android_dropDownWidth:I = 0x4

.field public static final Spinner_android_gravity:I = 0x0

.field public static final Spinner_android_popupBackground:I = 0x3

.field public static final Spinner_disableChildrenWhenDisabled:I = 0xa

.field public static final Spinner_popupPromptView:I = 0x9

.field public static final Spinner_prompt:I = 0x7

.field public static final Spinner_spinnerMode:I = 0x8

.field public static final StepsView:[I

.field public static final StepsView_bg_line_height_dp:I = 0x2

.field public static final StepsView_current_step:I = 0x1

.field public static final StepsView_fg_line_height_dp:I = 0x3

.field public static final StepsView_total_steps:I = 0x0

.field public static final SwitchCompat:[I

.field public static final SwitchCompat_android_textOff:I = 0x1

.field public static final SwitchCompat_android_textOn:I = 0x0

.field public static final SwitchCompat_android_thumb:I = 0x2

.field public static final SwitchCompat_showText:I = 0x9

.field public static final SwitchCompat_splitTrack:I = 0x8

.field public static final SwitchCompat_switchMinWidth:I = 0x6

.field public static final SwitchCompat_switchPadding:I = 0x7

.field public static final SwitchCompat_switchTextAppearance:I = 0x5

.field public static final SwitchCompat_thumbTextPadding:I = 0x4

.field public static final SwitchCompat_track:I = 0x3

.field public static final Theme:[I

.field public static final Theme_actionBarDivider:I = 0x13

.field public static final Theme_actionBarItemBackground:I = 0x14

.field public static final Theme_actionBarPopupTheme:I = 0xd

.field public static final Theme_actionBarSize:I = 0x12

.field public static final Theme_actionBarSplitStyle:I = 0xf

.field public static final Theme_actionBarStyle:I = 0xe

.field public static final Theme_actionBarTabBarStyle:I = 0x9

.field public static final Theme_actionBarTabStyle:I = 0x8

.field public static final Theme_actionBarTabTextStyle:I = 0xa

.field public static final Theme_actionBarTheme:I = 0x10

.field public static final Theme_actionBarWidgetTheme:I = 0x11

.field public static final Theme_actionButtonStyle:I = 0x2b

.field public static final Theme_actionDropDownStyle:I = 0x26

.field public static final Theme_actionMenuTextAppearance:I = 0x15

.field public static final Theme_actionMenuTextColor:I = 0x16

.field public static final Theme_actionModeBackground:I = 0x19

.field public static final Theme_actionModeCloseButtonStyle:I = 0x18

.field public static final Theme_actionModeCloseDrawable:I = 0x1b

.field public static final Theme_actionModeCopyDrawable:I = 0x1d

.field public static final Theme_actionModeCutDrawable:I = 0x1c

.field public static final Theme_actionModeFindDrawable:I = 0x21

.field public static final Theme_actionModePasteDrawable:I = 0x1e

.field public static final Theme_actionModePopupWindowStyle:I = 0x23

.field public static final Theme_actionModeSelectAllDrawable:I = 0x1f

.field public static final Theme_actionModeShareDrawable:I = 0x20

.field public static final Theme_actionModeSplitBackground:I = 0x1a

.field public static final Theme_actionModeStyle:I = 0x17

.field public static final Theme_actionModeWebSearchDrawable:I = 0x22

.field public static final Theme_actionOverflowButtonStyle:I = 0xb

.field public static final Theme_actionOverflowMenuStyle:I = 0xc

.field public static final Theme_activityChooserViewStyle:I = 0x32

.field public static final Theme_android_windowIsFloating:I = 0x0

.field public static final Theme_buttonBarButtonStyle:I = 0x2d

.field public static final Theme_buttonBarStyle:I = 0x2c

.field public static final Theme_colorAccent:I = 0x4d

.field public static final Theme_colorButtonNormal:I = 0x51

.field public static final Theme_colorControlActivated:I = 0x4f

.field public static final Theme_colorControlHighlight:I = 0x50

.field public static final Theme_colorControlNormal:I = 0x4e

.field public static final Theme_colorPrimary:I = 0x4b

.field public static final Theme_colorPrimaryDark:I = 0x4c

.field public static final Theme_colorSwitchThumbNormal:I = 0x52

.field public static final Theme_dividerHorizontal:I = 0x31

.field public static final Theme_dividerVertical:I = 0x30

.field public static final Theme_dropDownListViewStyle:I = 0x43

.field public static final Theme_dropdownListPreferredItemHeight:I = 0x27

.field public static final Theme_editTextBackground:I = 0x38

.field public static final Theme_editTextColor:I = 0x37

.field public static final Theme_homeAsUpIndicator:I = 0x2a

.field public static final Theme_listChoiceBackgroundIndicator:I = 0x4a

.field public static final Theme_listPopupWindowStyle:I = 0x44

.field public static final Theme_listPreferredItemHeight:I = 0x3e

.field public static final Theme_listPreferredItemHeightLarge:I = 0x40

.field public static final Theme_listPreferredItemHeightSmall:I = 0x3f

.field public static final Theme_listPreferredItemPaddingLeft:I = 0x41

.field public static final Theme_listPreferredItemPaddingRight:I = 0x42

.field public static final Theme_panelBackground:I = 0x47

.field public static final Theme_panelMenuListTheme:I = 0x49

.field public static final Theme_panelMenuListWidth:I = 0x48

.field public static final Theme_popupMenuStyle:I = 0x35

.field public static final Theme_popupWindowStyle:I = 0x36

.field public static final Theme_searchViewStyle:I = 0x3d

.field public static final Theme_selectableItemBackground:I = 0x2e

.field public static final Theme_selectableItemBackgroundBorderless:I = 0x2f

.field public static final Theme_spinnerDropDownItemStyle:I = 0x29

.field public static final Theme_spinnerStyle:I = 0x28

.field public static final Theme_switchStyle:I = 0x39

.field public static final Theme_textAppearanceLargePopupMenu:I = 0x24

.field public static final Theme_textAppearanceListItem:I = 0x45

.field public static final Theme_textAppearanceListItemSmall:I = 0x46

.field public static final Theme_textAppearanceSearchResultSubtitle:I = 0x3b

.field public static final Theme_textAppearanceSearchResultTitle:I = 0x3a

.field public static final Theme_textAppearanceSmallPopupMenu:I = 0x25

.field public static final Theme_textColorSearchUrl:I = 0x3c

.field public static final Theme_toolbarNavigationButtonStyle:I = 0x34

.field public static final Theme_toolbarStyle:I = 0x33

.field public static final Theme_windowActionBar:I = 0x1

.field public static final Theme_windowActionBarOverlay:I = 0x2

.field public static final Theme_windowActionModeOverlay:I = 0x3

.field public static final Theme_windowFixedHeightMajor:I = 0x7

.field public static final Theme_windowFixedHeightMinor:I = 0x5

.field public static final Theme_windowFixedWidthMajor:I = 0x4

.field public static final Theme_windowFixedWidthMinor:I = 0x6

.field public static final TitlePageIndicator:[I

.field public static final TitlePageIndicator_android_background:I = 0x2

.field public static final TitlePageIndicator_android_textColor:I = 0x1

.field public static final TitlePageIndicator_android_textSize:I = 0x0

.field public static final TitlePageIndicator_clipPadding:I = 0x4

.field public static final TitlePageIndicator_footerColor:I = 0x5

.field public static final TitlePageIndicator_footerIndicatorHeight:I = 0x8

.field public static final TitlePageIndicator_footerIndicatorStyle:I = 0x7

.field public static final TitlePageIndicator_footerIndicatorUnderlinePadding:I = 0x9

.field public static final TitlePageIndicator_footerLineHeight:I = 0x6

.field public static final TitlePageIndicator_footerPadding:I = 0xa

.field public static final TitlePageIndicator_linePosition:I = 0xb

.field public static final TitlePageIndicator_selectedBold:I = 0xc

.field public static final TitlePageIndicator_selectedColor:I = 0x3

.field public static final TitlePageIndicator_titlePadding:I = 0xd

.field public static final TitlePageIndicator_topPadding:I = 0xe

.field public static final Toolbar:[I

.field public static final Toolbar_android_gravity:I = 0x0

.field public static final Toolbar_android_minHeight:I = 0x1

.field public static final Toolbar_collapseContentDescription:I = 0x13

.field public static final Toolbar_collapseIcon:I = 0x12

.field public static final Toolbar_contentInsetEnd:I = 0x5

.field public static final Toolbar_contentInsetLeft:I = 0x6

.field public static final Toolbar_contentInsetRight:I = 0x7

.field public static final Toolbar_contentInsetStart:I = 0x4

.field public static final Toolbar_maxButtonHeight:I = 0x10

.field public static final Toolbar_navigationContentDescription:I = 0x15

.field public static final Toolbar_navigationIcon:I = 0x14

.field public static final Toolbar_popupTheme:I = 0x8

.field public static final Toolbar_subtitle:I = 0x3

.field public static final Toolbar_subtitleTextAppearance:I = 0xa

.field public static final Toolbar_theme:I = 0x11

.field public static final Toolbar_title:I = 0x2

.field public static final Toolbar_titleMarginBottom:I = 0xf

.field public static final Toolbar_titleMarginEnd:I = 0xd

.field public static final Toolbar_titleMarginStart:I = 0xc

.field public static final Toolbar_titleMarginTop:I = 0xe

.field public static final Toolbar_titleMargins:I = 0xb

.field public static final Toolbar_titleTextAppearance:I = 0x9

.field public static final TouchListView:[I

.field public static final TouchListView_dragndrop_background:I = 0x3

.field public static final TouchListView_expanded_height:I = 0x1

.field public static final TouchListView_grabber:I = 0x2

.field public static final TouchListView_normal_height:I = 0x0

.field public static final TouchListView_remove_mode:I = 0x4

.field public static final TwoWayAbsListView:[I

.field public static final TwoWayAbsListView_cacheColorHint:I = 0x5

.field public static final TwoWayAbsListView_drawSelectorOnTop:I = 0x1

.field public static final TwoWayAbsListView_listSelector:I = 0x0

.field public static final TwoWayAbsListView_scrollDirectionLandscape:I = 0x8

.field public static final TwoWayAbsListView_scrollDirectionPortrait:I = 0x7

.field public static final TwoWayAbsListView_scrollingCache:I = 0x3

.field public static final TwoWayAbsListView_smoothScrollbar:I = 0x6

.field public static final TwoWayAbsListView_stackFromBottom:I = 0x2

.field public static final TwoWayAbsListView_transcriptMode:I = 0x4

.field public static final TwoWayGridView:[I

.field public static final TwoWayGridView_columnWidth:I = 0x4

.field public static final TwoWayGridView_gravity:I = 0x0

.field public static final TwoWayGridView_horizontalSpacing:I = 0x1

.field public static final TwoWayGridView_numColumns:I = 0x6

.field public static final TwoWayGridView_numRows:I = 0x7

.field public static final TwoWayGridView_rowHeight:I = 0x5

.field public static final TwoWayGridView_stretchMode:I = 0x3

.field public static final TwoWayGridView_verticalSpacing:I = 0x2

.field public static final UnderlinePageIndicator:[I

.field public static final UnderlinePageIndicator_android_background:I = 0x0

.field public static final UnderlinePageIndicator_fadeDelay:I = 0x3

.field public static final UnderlinePageIndicator_fadeLength:I = 0x4

.field public static final UnderlinePageIndicator_fades:I = 0x2

.field public static final UnderlinePageIndicator_selectedColor:I = 0x1

.field public static final View:[I

.field public static final ViewPagerIndicator:[I

.field public static final ViewPagerIndicator_vpiCirclePageIndicatorStyle:I = 0x0

.field public static final ViewPagerIndicator_vpiIconPageIndicatorStyle:I = 0x1

.field public static final ViewPagerIndicator_vpiLinePageIndicatorStyle:I = 0x2

.field public static final ViewPagerIndicator_vpiTabPageIndicatorStyle:I = 0x4

.field public static final ViewPagerIndicator_vpiTitlePageIndicatorStyle:I = 0x3

.field public static final ViewPagerIndicator_vpiUnderlinePageIndicatorStyle:I = 0x5

.field public static final ViewStubCompat:[I

.field public static final ViewStubCompat_android_id:I = 0x0

.field public static final ViewStubCompat_android_inflatedId:I = 0x2

.field public static final ViewStubCompat_android_layout:I = 0x1

.field public static final View_android_focusable:I = 0x0

.field public static final View_paddingEnd:I = 0x2

.field public static final View_paddingStart:I = 0x1

.field public static final com_facebook_friend_picker_fragment:[I

.field public static final com_facebook_friend_picker_fragment_multi_select:I = 0x0

.field public static final com_facebook_like_view:[I

.field public static final com_facebook_like_view_auxiliary_view_position:I = 0x3

.field public static final com_facebook_like_view_foreground_color:I = 0x0

.field public static final com_facebook_like_view_horizontal_alignment:I = 0x4

.field public static final com_facebook_like_view_object_id:I = 0x1

.field public static final com_facebook_like_view_style:I = 0x2

.field public static final com_facebook_login_view:[I

.field public static final com_facebook_login_view_confirm_logout:I = 0x0

.field public static final com_facebook_login_view_fetch_user_info:I = 0x1

.field public static final com_facebook_login_view_login_text:I = 0x2

.field public static final com_facebook_login_view_logout_text:I = 0x3

.field public static final com_facebook_picker_fragment:[I

.field public static final com_facebook_picker_fragment_done_button_background:I = 0x6

.field public static final com_facebook_picker_fragment_done_button_text:I = 0x4

.field public static final com_facebook_picker_fragment_extra_fields:I = 0x1

.field public static final com_facebook_picker_fragment_show_pictures:I = 0x0

.field public static final com_facebook_picker_fragment_show_title_bar:I = 0x2

.field public static final com_facebook_picker_fragment_title_bar_background:I = 0x5

.field public static final com_facebook_picker_fragment_title_text:I = 0x3

.field public static final com_facebook_place_picker_fragment:[I

.field public static final com_facebook_place_picker_fragment_radius_in_meters:I = 0x0

.field public static final com_facebook_place_picker_fragment_results_limit:I = 0x1

.field public static final com_facebook_place_picker_fragment_search_text:I = 0x2

.field public static final com_facebook_place_picker_fragment_show_search_box:I = 0x3

.field public static final com_facebook_profile_picture_view:[I

.field public static final com_facebook_profile_picture_view_is_cropped:I = 0x1

.field public static final com_facebook_profile_picture_view_preset_size:I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x5

    const/4 v5, 0x4

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 5301
    const/16 v0, 0x1b

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/peel/ui/fv;->ActionBar:[I

    .line 5302
    new-array v0, v3, [I

    const v1, 0x10100b3

    aput v1, v0, v2

    sput-object v0, Lcom/peel/ui/fv;->ActionBarLayout:[I

    .line 5331
    new-array v0, v3, [I

    const v1, 0x101013f

    aput v1, v0, v2

    sput-object v0, Lcom/peel/ui/fv;->ActionMenuItemView:[I

    .line 5333
    new-array v0, v2, [I

    sput-object v0, Lcom/peel/ui/fv;->ActionMenuView:[I

    .line 5334
    const/4 v0, 0x6

    new-array v0, v0, [I

    fill-array-data v0, :array_1

    sput-object v0, Lcom/peel/ui/fv;->ActionMode:[I

    .line 5341
    new-array v0, v4, [I

    fill-array-data v0, :array_2

    sput-object v0, Lcom/peel/ui/fv;->ActivityChooserView:[I

    .line 5344
    const/4 v0, 0x3

    new-array v0, v0, [I

    fill-array-data v0, :array_3

    sput-object v0, Lcom/peel/ui/fv;->AdsAttrs:[I

    .line 5348
    new-array v0, v4, [I

    fill-array-data v0, :array_4

    sput-object v0, Lcom/peel/ui/fv;->CircleImageView:[I

    .line 5351
    const/16 v0, 0x9

    new-array v0, v0, [I

    fill-array-data v0, :array_5

    sput-object v0, Lcom/peel/ui/fv;->CirclePageIndicator:[I

    .line 5361
    new-array v0, v3, [I

    const v1, 0x7f01002f

    aput v1, v0, v2

    sput-object v0, Lcom/peel/ui/fv;->CompatTextView:[I

    .line 5363
    const/16 v0, 0x8

    new-array v0, v0, [I

    fill-array-data v0, :array_6

    sput-object v0, Lcom/peel/ui/fv;->DrawerArrowToggle:[I

    .line 5372
    const/4 v0, 0x7

    new-array v0, v0, [I

    fill-array-data v0, :array_7

    sput-object v0, Lcom/peel/ui/fv;->LinePageIndicator:[I

    .line 5380
    const/16 v0, 0x9

    new-array v0, v0, [I

    fill-array-data v0, :array_8

    sput-object v0, Lcom/peel/ui/fv;->LinearLayoutCompat:[I

    .line 5381
    new-array v0, v5, [I

    fill-array-data v0, :array_9

    sput-object v0, Lcom/peel/ui/fv;->LinearLayoutCompat_Layout:[I

    .line 5395
    new-array v0, v4, [I

    fill-array-data v0, :array_a

    sput-object v0, Lcom/peel/ui/fv;->ListPopupWindow:[I

    .line 5398
    const/16 v0, 0xe

    new-array v0, v0, [I

    fill-array-data v0, :array_b

    sput-object v0, Lcom/peel/ui/fv;->MapAttrs:[I

    .line 5413
    const/4 v0, 0x6

    new-array v0, v0, [I

    fill-array-data v0, :array_c

    sput-object v0, Lcom/peel/ui/fv;->MenuGroup:[I

    .line 5420
    const/16 v0, 0x11

    new-array v0, v0, [I

    fill-array-data v0, :array_d

    sput-object v0, Lcom/peel/ui/fv;->MenuItem:[I

    .line 5438
    const/16 v0, 0x8

    new-array v0, v0, [I

    fill-array-data v0, :array_e

    sput-object v0, Lcom/peel/ui/fv;->MenuView:[I

    .line 5447
    new-array v0, v6, [I

    fill-array-data v0, :array_f

    sput-object v0, Lcom/peel/ui/fv;->PeelSlider:[I

    .line 5453
    new-array v0, v4, [I

    fill-array-data v0, :array_10

    sput-object v0, Lcom/peel/ui/fv;->PopupWindow:[I

    .line 5454
    new-array v0, v3, [I

    const v1, 0x7f010056

    aput v1, v0, v2

    sput-object v0, Lcom/peel/ui/fv;->PopupWindowBackgroundState:[I

    .line 5458
    const/16 v0, 0xf

    new-array v0, v0, [I

    fill-array-data v0, :array_11

    sput-object v0, Lcom/peel/ui/fv;->SearchView:[I

    .line 5474
    const/16 v0, 0x8

    new-array v0, v0, [I

    fill-array-data v0, :array_12

    sput-object v0, Lcom/peel/ui/fv;->SlidingDrawer:[I

    .line 5483
    const/16 v0, 0xb

    new-array v0, v0, [I

    fill-array-data v0, :array_13

    sput-object v0, Lcom/peel/ui/fv;->Spinner:[I

    .line 5495
    new-array v0, v5, [I

    fill-array-data v0, :array_14

    sput-object v0, Lcom/peel/ui/fv;->StepsView:[I

    .line 5500
    const/16 v0, 0xa

    new-array v0, v0, [I

    fill-array-data v0, :array_15

    sput-object v0, Lcom/peel/ui/fv;->SwitchCompat:[I

    .line 5511
    const/16 v0, 0x53

    new-array v0, v0, [I

    fill-array-data v0, :array_16

    sput-object v0, Lcom/peel/ui/fv;->Theme:[I

    .line 5595
    const/16 v0, 0xf

    new-array v0, v0, [I

    fill-array-data v0, :array_17

    sput-object v0, Lcom/peel/ui/fv;->TitlePageIndicator:[I

    .line 5611
    const/16 v0, 0x16

    new-array v0, v0, [I

    fill-array-data v0, :array_18

    sput-object v0, Lcom/peel/ui/fv;->Toolbar:[I

    .line 5634
    new-array v0, v6, [I

    fill-array-data v0, :array_19

    sput-object v0, Lcom/peel/ui/fv;->TouchListView:[I

    .line 5640
    const/16 v0, 0x9

    new-array v0, v0, [I

    fill-array-data v0, :array_1a

    sput-object v0, Lcom/peel/ui/fv;->TwoWayAbsListView:[I

    .line 5650
    const/16 v0, 0x8

    new-array v0, v0, [I

    fill-array-data v0, :array_1b

    sput-object v0, Lcom/peel/ui/fv;->TwoWayGridView:[I

    .line 5659
    new-array v0, v6, [I

    fill-array-data v0, :array_1c

    sput-object v0, Lcom/peel/ui/fv;->UnderlinePageIndicator:[I

    .line 5665
    const/4 v0, 0x3

    new-array v0, v0, [I

    fill-array-data v0, :array_1d

    sput-object v0, Lcom/peel/ui/fv;->View:[I

    .line 5666
    const/4 v0, 0x6

    new-array v0, v0, [I

    fill-array-data v0, :array_1e

    sput-object v0, Lcom/peel/ui/fv;->ViewPagerIndicator:[I

    .line 5673
    const/4 v0, 0x3

    new-array v0, v0, [I

    fill-array-data v0, :array_1f

    sput-object v0, Lcom/peel/ui/fv;->ViewStubCompat:[I

    .line 5680
    new-array v0, v3, [I

    const v1, 0x7f010103

    aput v1, v0, v2

    sput-object v0, Lcom/peel/ui/fv;->com_facebook_friend_picker_fragment:[I

    .line 5682
    new-array v0, v6, [I

    fill-array-data v0, :array_20

    sput-object v0, Lcom/peel/ui/fv;->com_facebook_like_view:[I

    .line 5688
    new-array v0, v5, [I

    fill-array-data v0, :array_21

    sput-object v0, Lcom/peel/ui/fv;->com_facebook_login_view:[I

    .line 5693
    const/4 v0, 0x7

    new-array v0, v0, [I

    fill-array-data v0, :array_22

    sput-object v0, Lcom/peel/ui/fv;->com_facebook_picker_fragment:[I

    .line 5701
    new-array v0, v5, [I

    fill-array-data v0, :array_23

    sput-object v0, Lcom/peel/ui/fv;->com_facebook_place_picker_fragment:[I

    .line 5706
    new-array v0, v4, [I

    fill-array-data v0, :array_24

    sput-object v0, Lcom/peel/ui/fv;->com_facebook_profile_picture_view:[I

    return-void

    .line 5301
    :array_0
    .array-data 4
        0x7f010004
        0x7f010008
        0x7f01000a
        0x7f01000b
        0x7f01000c
        0x7f01000d
        0x7f01000e
        0x7f01000f
        0x7f010010
        0x7f010011
        0x7f010012
        0x7f010013
        0x7f010014
        0x7f010015
        0x7f010016
        0x7f010017
        0x7f010018
        0x7f010019
        0x7f01001a
        0x7f01001b
        0x7f01001c
        0x7f01001d
        0x7f01001e
        0x7f01001f
        0x7f010020
        0x7f010021
        0x7f0100a2
    .end array-data

    .line 5334
    :array_1
    .array-data 4
        0x7f010004
        0x7f01000d
        0x7f01000e
        0x7f010012
        0x7f010014
        0x7f010022
    .end array-data

    .line 5341
    :array_2
    .array-data 4
        0x7f010023
        0x7f010024
    .end array-data

    .line 5344
    :array_3
    .array-data 4
        0x7f010025
        0x7f010026
        0x7f010027
    .end array-data

    .line 5348
    :array_4
    .array-data 4
        0x7f010028
        0x7f010029
    .end array-data

    .line 5351
    :array_5
    .array-data 4
        0x10100c4
        0x10100d4
        0x7f010000
        0x7f010007
        0x7f01002a
        0x7f01002b
        0x7f01002c
        0x7f01002d
        0x7f01002e
    .end array-data

    .line 5363
    :array_6
    .array-data 4
        0x7f010030
        0x7f010031
        0x7f010032
        0x7f010033
        0x7f010034
        0x7f010035
        0x7f010036
        0x7f010037
    .end array-data

    .line 5372
    :array_7
    .array-data 4
        0x10100d4
        0x7f010000
        0x7f010006
        0x7f010007
        0x7f010009
        0x7f010038
        0x7f010039
    .end array-data

    .line 5380
    :array_8
    .array-data 4
        0x10100af
        0x10100c4
        0x1010126
        0x1010127
        0x1010128
        0x7f010011
        0x7f01003a
        0x7f01003b
        0x7f01003c
    .end array-data

    .line 5381
    :array_9
    .array-data 4
        0x10100b3
        0x10100f4
        0x10100f5
        0x1010181
    .end array-data

    .line 5395
    :array_a
    .array-data 4
        0x10102ac
        0x10102ad
    .end array-data

    .line 5398
    :array_b
    .array-data 4
        0x7f01003d
        0x7f01003e
        0x7f01003f
        0x7f010040
        0x7f010041
        0x7f010042
        0x7f010043
        0x7f010044
        0x7f010045
        0x7f010046
        0x7f010047
        0x7f010048
        0x7f010049
        0x7f01004a
    .end array-data

    .line 5413
    :array_c
    .array-data 4
        0x101000e
        0x10100d0
        0x1010194
        0x10101de
        0x10101df
        0x10101e0
    .end array-data

    .line 5420
    :array_d
    .array-data 4
        0x1010002
        0x101000e
        0x10100d0
        0x1010106
        0x1010194
        0x10101de
        0x10101df
        0x10101e1
        0x10101e2
        0x10101e3
        0x10101e4
        0x10101e5
        0x101026f
        0x7f01004b
        0x7f01004c
        0x7f01004d
        0x7f01004e
    .end array-data

    .line 5438
    :array_e
    .array-data 4
        0x10100ae
        0x101012c
        0x101012d
        0x101012e
        0x101012f
        0x1010130
        0x1010131
        0x7f01004f
    .end array-data

    .line 5447
    :array_f
    .array-data 4
        0x7f010050
        0x7f010051
        0x7f010052
        0x7f010053
        0x7f010054
    .end array-data

    .line 5453
    :array_10
    .array-data 4
        0x1010176
        0x7f010055
    .end array-data

    .line 5458
    :array_11
    .array-data 4
        0x10100da
        0x101011f
        0x1010220
        0x1010264
        0x7f010057
        0x7f010058
        0x7f010059
        0x7f01005a
        0x7f01005b
        0x7f01005c
        0x7f01005d
        0x7f01005e
        0x7f01005f
        0x7f010060
        0x7f010061
    .end array-data

    .line 5474
    :array_12
    .array-data 4
        0x7f010062
        0x7f010063
        0x7f010064
        0x7f010065
        0x7f010066
        0x7f010067
        0x7f010068
        0x7f010069
    .end array-data

    .line 5483
    :array_13
    .array-data 4
        0x10100af
        0x10100d4
        0x1010175
        0x1010176
        0x1010262
        0x10102ac
        0x10102ad
        0x7f01006a
        0x7f01006b
        0x7f01006c
        0x7f01006d
    .end array-data

    .line 5495
    :array_14
    .array-data 4
        0x7f01006e
        0x7f01006f
        0x7f010070
        0x7f010071
    .end array-data

    .line 5500
    :array_15
    .array-data 4
        0x1010124
        0x1010125
        0x1010142
        0x7f010072
        0x7f010073
        0x7f010074
        0x7f010075
        0x7f010076
        0x7f010077
        0x7f010078
    .end array-data

    .line 5511
    :array_16
    .array-data 4
        0x1010057
        0x7f010079
        0x7f01007a
        0x7f01007b
        0x7f01007c
        0x7f01007d
        0x7f01007e
        0x7f01007f
        0x7f010080
        0x7f010081
        0x7f010082
        0x7f010083
        0x7f010084
        0x7f010085
        0x7f010086
        0x7f010087
        0x7f010088
        0x7f010089
        0x7f01008a
        0x7f01008b
        0x7f01008c
        0x7f01008d
        0x7f01008e
        0x7f01008f
        0x7f010090
        0x7f010091
        0x7f010092
        0x7f010093
        0x7f010094
        0x7f010095
        0x7f010096
        0x7f010097
        0x7f010098
        0x7f010099
        0x7f01009a
        0x7f01009b
        0x7f01009c
        0x7f01009d
        0x7f01009e
        0x7f01009f
        0x7f0100a0
        0x7f0100a1
        0x7f0100a2
        0x7f0100a3
        0x7f0100a4
        0x7f0100a5
        0x7f0100a6
        0x7f0100a7
        0x7f0100a8
        0x7f0100a9
        0x7f0100aa
        0x7f0100ab
        0x7f0100ac
        0x7f0100ad
        0x7f0100ae
        0x7f0100af
        0x7f0100b0
        0x7f0100b1
        0x7f0100b2
        0x7f0100b3
        0x7f0100b4
        0x7f0100b5
        0x7f0100b6
        0x7f0100b7
        0x7f0100b8
        0x7f0100b9
        0x7f0100ba
        0x7f0100bb
        0x7f0100bc
        0x7f0100bd
        0x7f0100be
        0x7f0100bf
        0x7f0100c0
        0x7f0100c1
        0x7f0100c2
        0x7f0100c3
        0x7f0100c4
        0x7f0100c5
        0x7f0100c6
        0x7f0100c7
        0x7f0100c8
        0x7f0100c9
        0x7f0100ca
    .end array-data

    .line 5595
    :array_17
    .array-data 4
        0x1010095
        0x1010098
        0x10100d4
        0x7f010006
        0x7f0100cb
        0x7f0100cc
        0x7f0100cd
        0x7f0100ce
        0x7f0100cf
        0x7f0100d0
        0x7f0100d1
        0x7f0100d2
        0x7f0100d3
        0x7f0100d4
        0x7f0100d5
    .end array-data

    .line 5611
    :array_18
    .array-data 4
        0x10100af
        0x1010140
        0x7f010008
        0x7f01000c
        0x7f01001c
        0x7f01001d
        0x7f01001e
        0x7f01001f
        0x7f010021
        0x7f0100d6
        0x7f0100d7
        0x7f0100d8
        0x7f0100d9
        0x7f0100da
        0x7f0100db
        0x7f0100dc
        0x7f0100dd
        0x7f0100de
        0x7f0100df
        0x7f0100e0
        0x7f0100e1
        0x7f0100e2
    .end array-data

    .line 5634
    :array_19
    .array-data 4
        0x7f0100e3
        0x7f0100e4
        0x7f0100e5
        0x7f0100e6
        0x7f0100e7
    .end array-data

    .line 5640
    :array_1a
    .array-data 4
        0x7f0100e8
        0x7f0100e9
        0x7f0100ea
        0x7f0100eb
        0x7f0100ec
        0x7f0100ed
        0x7f0100ee
        0x7f0100ef
        0x7f0100f0
    .end array-data

    .line 5650
    :array_1b
    .array-data 4
        0x7f010002
        0x7f0100f1
        0x7f0100f2
        0x7f0100f3
        0x7f0100f4
        0x7f0100f5
        0x7f0100f6
        0x7f0100f7
    .end array-data

    .line 5659
    :array_1c
    .array-data 4
        0x10100d4
        0x7f010006
        0x7f0100f8
        0x7f0100f9
        0x7f0100fa
    .end array-data

    .line 5665
    :array_1d
    .array-data 4
        0x10100da
        0x7f0100fb
        0x7f0100fc
    .end array-data

    .line 5666
    :array_1e
    .array-data 4
        0x7f0100fd
        0x7f0100fe
        0x7f0100ff
        0x7f010100
        0x7f010101
        0x7f010102
    .end array-data

    .line 5673
    :array_1f
    .array-data 4
        0x10100d0
        0x10100f2
        0x10100f3
    .end array-data

    .line 5682
    :array_20
    .array-data 4
        0x7f010104
        0x7f010105
        0x7f010106
        0x7f010107
        0x7f010108
    .end array-data

    .line 5688
    :array_21
    .array-data 4
        0x7f010109
        0x7f01010a
        0x7f01010b
        0x7f01010c
    .end array-data

    .line 5693
    :array_22
    .array-data 4
        0x7f01010d
        0x7f01010e
        0x7f01010f
        0x7f010110
        0x7f010111
        0x7f010112
        0x7f010113
    .end array-data

    .line 5701
    :array_23
    .array-data 4
        0x7f010114
        0x7f010115
        0x7f010116
        0x7f010117
    .end array-data

    .line 5706
    :array_24
    .array-data 4
        0x7f010118
        0x7f010119
    .end array-data
.end method
