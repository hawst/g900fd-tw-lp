.class public Lcom/peel/ui/kc;
.super Landroid/widget/BaseAdapter;


# instance fields
.field private final a:Landroid/util/LongSparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/LongSparseArray",
            "<",
            "Landroid/os/Bundle;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<[",
            "Lcom/peel/content/listing/Listing;",
            ">;>;"
        }
    .end annotation
.end field

.field private final c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Lcom/peel/ui/kg;

.field private final e:Landroid/view/LayoutInflater;

.field private final f:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/List;Ljava/util/Map;Lcom/peel/ui/kg;Landroid/util/LongSparseArray;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<[",
            "Lcom/peel/content/listing/Listing;",
            ">;>;",
            "Lcom/peel/ui/kg;",
            "Landroid/util/LongSparseArray",
            "<",
            "Landroid/os/Bundle;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 35
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 36
    iput-object p1, p0, Lcom/peel/ui/kc;->f:Landroid/content/Context;

    .line 37
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/ui/kc;->e:Landroid/view/LayoutInflater;

    .line 38
    iput-object p2, p0, Lcom/peel/ui/kc;->c:Ljava/util/List;

    .line 39
    iput-object p3, p0, Lcom/peel/ui/kc;->b:Ljava/util/Map;

    .line 40
    iput-object p4, p0, Lcom/peel/ui/kc;->d:Lcom/peel/ui/kg;

    .line 41
    if-nez p5, :cond_0

    .line 42
    new-instance v0, Landroid/util/LongSparseArray;

    invoke-direct {v0}, Landroid/util/LongSparseArray;-><init>()V

    iput-object v0, p0, Lcom/peel/ui/kc;->a:Landroid/util/LongSparseArray;

    .line 46
    :goto_0
    return-void

    .line 44
    :cond_0
    iput-object p5, p0, Lcom/peel/ui/kc;->a:Landroid/util/LongSparseArray;

    goto :goto_0
.end method

.method static synthetic a(Lcom/peel/ui/kc;)Lcom/peel/ui/kg;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lcom/peel/ui/kc;->d:Lcom/peel/ui/kg;

    return-object v0
.end method

.method static synthetic b(Lcom/peel/ui/kc;)Ljava/util/List;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lcom/peel/ui/kc;->c:Ljava/util/List;

    return-object v0
.end method

.method static synthetic c(Lcom/peel/ui/kc;)Landroid/util/LongSparseArray;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lcom/peel/ui/kc;->a:Landroid/util/LongSparseArray;

    return-object v0
.end method


# virtual methods
.method public a()Landroid/util/LongSparseArray;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/util/LongSparseArray",
            "<",
            "Landroid/os/Bundle;",
            ">;"
        }
    .end annotation

    .prologue
    .line 49
    iget-object v0, p0, Lcom/peel/ui/kc;->a:Landroid/util/LongSparseArray;

    return-object v0
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/peel/ui/kc;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 57
    iget-object v0, p0, Lcom/peel/ui/kc;->b:Ljava/util/Map;

    iget-object v1, p0, Lcom/peel/ui/kc;->c:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2

    .prologue
    .line 65
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 8

    .prologue
    .line 69
    if-eqz p2, :cond_1

    move-object v0, p2

    :goto_0
    check-cast v0, Landroid/widget/LinearLayout;

    check-cast v0, Landroid/widget/LinearLayout;

    .line 72
    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getTag()Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_2

    .line 73
    new-instance v3, Lcom/peel/ui/kf;

    sget v1, Lcom/peel/ui/fp;->title:I

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    sget v2, Lcom/peel/ui/fp;->tiles_grid:I

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/jess/ui/TwoWayGridView;

    invoke-direct {v3, v1, v2, p1}, Lcom/peel/ui/kf;-><init>(Landroid/widget/TextView;Lcom/jess/ui/TwoWayGridView;I)V

    .line 74
    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setTag(Ljava/lang/Object;)V

    move-object v2, v3

    .line 79
    :goto_1
    iput p1, v2, Lcom/peel/ui/kf;->c:I

    .line 80
    iget-object v3, v2, Lcom/peel/ui/kf;->a:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/peel/ui/kc;->c:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v3, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 83
    iget-object v3, v2, Lcom/peel/ui/kf;->b:Lcom/jess/ui/TwoWayGridView;

    new-instance v4, Lcom/peel/ui/ka;

    iget-object v5, p0, Lcom/peel/ui/kc;->f:Landroid/content/Context;

    const/4 v6, -0x1

    iget-object v1, p0, Lcom/peel/ui/kc;->b:Ljava/util/Map;

    iget-object v7, p0, Lcom/peel/ui/kc;->c:Ljava/util/List;

    invoke-interface {v7, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    invoke-interface {v1, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    invoke-direct {v4, v5, v6, v1}, Lcom/peel/ui/ka;-><init>(Landroid/content/Context;ILjava/util/List;)V

    invoke-virtual {v3, v4}, Lcom/jess/ui/TwoWayGridView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 84
    iget-object v1, v2, Lcom/peel/ui/kf;->b:Lcom/jess/ui/TwoWayGridView;

    new-instance v3, Lcom/peel/ui/kd;

    invoke-direct {v3, p0, p1}, Lcom/peel/ui/kd;-><init>(Lcom/peel/ui/kc;I)V

    invoke-virtual {v1, v3}, Lcom/jess/ui/TwoWayGridView;->setOnItemClickListener(Lcom/jess/ui/z;)V

    .line 91
    iget-object v1, v2, Lcom/peel/ui/kf;->b:Lcom/jess/ui/TwoWayGridView;

    new-instance v3, Lcom/peel/ui/ke;

    invoke-direct {v3, p0, p1}, Lcom/peel/ui/ke;-><init>(Lcom/peel/ui/kc;I)V

    invoke-virtual {v1, v3}, Lcom/jess/ui/TwoWayGridView;->setOnScrollListener(Lcom/jess/ui/i;)V

    .line 108
    iget-object v1, p0, Lcom/peel/ui/kc;->a:Landroid/util/LongSparseArray;

    invoke-virtual {p0, p1}, Lcom/peel/ui/kc;->getItemId(I)J

    move-result-wide v4

    invoke-virtual {v1, v4, v5}, Landroid/util/LongSparseArray;->get(J)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/Bundle;

    .line 109
    if-eqz v1, :cond_0

    .line 110
    iget-object v2, v2, Lcom/peel/ui/kf;->b:Lcom/jess/ui/TwoWayGridView;

    const-string/jumbo v3, "firstVisible"

    invoke-virtual {v1, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v2, v1}, Lcom/jess/ui/TwoWayGridView;->setSelection(I)V

    .line 115
    :cond_0
    return-object v0

    .line 69
    :cond_1
    iget-object v0, p0, Lcom/peel/ui/kc;->e:Landroid/view/LayoutInflater;

    sget v1, Lcom/peel/ui/fq;->weektile_bar:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    goto/16 :goto_0

    .line 76
    :cond_2
    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/peel/ui/kf;

    move-object v2, v1

    goto :goto_1
.end method
