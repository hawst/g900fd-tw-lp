.class public final Lcom/peel/ui/fp;
.super Ljava/lang/Object;


# static fields
.field public static final about_app_version:I = 0x7f0a0066

.field public static final action_bar:I = 0x7f0a0057

.field public static final action_bar_activity_content:I = 0x7f0a0000

.field public static final action_bar_container:I = 0x7f0a0056

.field public static final action_bar_root:I = 0x7f0a0052

.field public static final action_bar_spinner:I = 0x7f0a0001

.field public static final action_bar_subtitle:I = 0x7f0a0045

.field public static final action_bar_title:I = 0x7f0a0044

.field public static final action_btn:I = 0x7f0a00b4

.field public static final action_context_bar:I = 0x7f0a0058

.field public static final action_menu_divider:I = 0x7f0a0002

.field public static final action_menu_presenter:I = 0x7f0a0003

.field public static final action_mode_bar:I = 0x7f0a0054

.field public static final action_mode_bar_stub:I = 0x7f0a0053

.field public static final action_mode_close_button:I = 0x7f0a0046

.field public static final action_power_icon:I = 0x7f0a0217

.field public static final activities_lv:I = 0x7f0a00b9

.field public static final activity_btn:I = 0x7f0a00cd

.field public static final activity_chooser_view_content:I = 0x7f0a0047

.field public static final activity_content:I = 0x7f0a00c7

.field public static final activity_text:I = 0x7f0a01d2

.field public static final adViewDisplay:I = 0x7f0a017d

.field public static final ad_banner_view:I = 0x7f0a0070

.field public static final ad_container:I = 0x7f0a011b

.field public static final ad_placeholder:I = 0x7f0a00d6

.field public static final ad_video_view:I = 0x7f0a008d

.field public static final add_btn:I = 0x7f0a019d

.field public static final add_channel_airing_row:I = 0x7f0a015b

.field public static final add_channel_btn:I = 0x7f0a0159

.field public static final add_channel_row:I = 0x7f0a015c

.field public static final add_device:I = 0x7f0a0239

.field public static final add_devices:I = 0x7f0a00df

.field public static final add_favorite_channel_btn:I = 0x7f0a0156

.field public static final add_foreign_langauge_channel:I = 0x7f0a0241

.field public static final add_roku_ip:I = 0x7f0a019c

.field public static final ads_placeholder:I = 0x7f0a0302

.field public static final adview_container:I = 0x7f0a006a

.field public static final after_switch_textview:I = 0x7f0a02e0

.field public static final age_text:I = 0x7f0a017b

.field public static final age_title:I = 0x7f0a017a

.field public static final alertTitle:I = 0x7f0a0209

.field public static final alt_name:I = 0x7f0a021c

.field public static final always:I = 0x7f0a0028

.field public static final alwaysScroll:I = 0x7f0a0036

.field public static final ampm_container:I = 0x7f0a02d1

.field public static final ampm_down_button:I = 0x7f0a02d4

.field public static final ampm_separator:I = 0x7f0a02d0

.field public static final ampm_textview:I = 0x7f0a02d3

.field public static final ampm_up_button:I = 0x7f0a02d2

.field public static final app_name_text:I = 0x7f0a01d1

.field public static final app_name_text_no_action:I = 0x7f0a0200

.field public static final app_title:I = 0x7f0a026d

.field public static final arrow:I = 0x7f0a0139

.field public static final auth_webview_id:I = 0x7f0a01c5

.field public static final auto_fit:I = 0x7f0a003d

.field public static final auto_update_btn:I = 0x7f0a0125

.field public static final availablenow_linearlayout:I = 0x7f0a02d7

.field public static final badge_txt:I = 0x7f0a02fb

.field public static final bar:I = 0x7f0a0300

.field public static final before_bg_img:I = 0x7f0a0086

.field public static final before_setup_viewpager:I = 0x7f0a028f

.field public static final beginning:I = 0x7f0a0022

.field public static final bg:I = 0x7f0a0307

.field public static final bottom:I = 0x7f0a000c

.field public static final bottom_control_bar:I = 0x7f0a0158

.field public static final box_count:I = 0x7f0a003e

.field public static final brand:I = 0x7f0a022c

.field public static final brand_list:I = 0x7f0a010b

.field public static final brand_tv:I = 0x7f0a022b

.field public static final btn:I = 0x7f0a0107

.field public static final btn1:I = 0x7f0a01b1

.field public static final btn10:I = 0x7f0a0182

.field public static final btn11:I = 0x7f0a0183

.field public static final btn12:I = 0x7f0a0184

.field public static final btn13:I = 0x7f0a0185

.field public static final btn14:I = 0x7f0a0186

.field public static final btn15:I = 0x7f0a0187

.field public static final btn16:I = 0x7f0a01d7

.field public static final btn17:I = 0x7f0a018d

.field public static final btn2:I = 0x7f0a01b2

.field public static final btn20:I = 0x7f0a01d4

.field public static final btn21:I = 0x7f0a01d3

.field public static final btn3:I = 0x7f0a01b3

.field public static final btn4:I = 0x7f0a0188

.field public static final btn5:I = 0x7f0a0189

.field public static final btn6:I = 0x7f0a018a

.field public static final btn7:I = 0x7f0a018b

.field public static final btn8:I = 0x7f0a01b7

.field public static final btn9:I = 0x7f0a01b8

.field public static final btn_1:I = 0x7f0a02f1

.field public static final btn_2:I = 0x7f0a02f2

.field public static final btn_3:I = 0x7f0a02f3

.field public static final btn_close:I = 0x7f0a011c

.field public static final btn_custom:I = 0x7f0a01ac

.field public static final btn_no:I = 0x7f0a0216

.field public static final btn_peel_tv:I = 0x7f0a018e

.field public static final btn_peel_tv1:I = 0x7f0a0324

.field public static final btn_txt_id:I = 0x7f0a0004

.field public static final btn_yes:I = 0x7f0a0215

.field public static final button:I = 0x7f0a003f

.field public static final button1:I = 0x7f0a020e

.field public static final button2:I = 0x7f0a0212

.field public static final button3:I = 0x7f0a0210

.field public static final buttonPanel:I = 0x7f0a020d

.field public static final button_1_layout:I = 0x7f0a02ef

.field public static final button_area:I = 0x7f0a0104

.field public static final button_num_text:I = 0x7f0a02e2

.field public static final button_num_text_small:I = 0x7f0a02e4

.field public static final buttons_panel:I = 0x7f0a0291

.field public static final callsign:I = 0x7f0a028b

.field public static final camera_icon:I = 0x7f0a0286

.field public static final campaign_message_text:I = 0x7f0a006f

.field public static final cancel:I = 0x7f0a007d

.field public static final cancel_btn:I = 0x7f0a0192

.field public static final caption:I = 0x7f0a023e

.field public static final caption_altn:I = 0x7f0a02f8

.field public static final center:I = 0x7f0a000d

.field public static final center_horizontal:I = 0x7f0a000e

.field public static final center_vertical:I = 0x7f0a000f

.field public static final channel:I = 0x7f0a0095

.field public static final channel_1:I = 0x7f0a0326

.field public static final channel_2:I = 0x7f0a0329

.field public static final channel_3:I = 0x7f0a032a

.field public static final channel_4:I = 0x7f0a032b

.field public static final channel_btn:I = 0x7f0a0121

.field public static final channel_callsign:I = 0x7f0a00c1

.field public static final channel_container:I = 0x7f0a0325

.field public static final channel_guide_content:I = 0x7f0a008e

.field public static final channel_guide_drawer:I = 0x7f0a00d2

.field public static final channel_guide_handle:I = 0x7f0a00d3

.field public static final channel_image:I = 0x7f0a00bf

.field public static final channel_list:I = 0x7f0a024b

.field public static final channel_name:I = 0x7f0a0094

.field public static final channel_num_text:I = 0x7f0a02ea

.field public static final channel_num_text_small:I = 0x7f0a02ee

.field public static final channel_row:I = 0x7f0a00c0

.field public static final channel_sep:I = 0x7f0a028c

.field public static final channel_text:I = 0x7f0a00c2

.field public static final channel_text_small:I = 0x7f0a02ed

.field public static final channelnumber:I = 0x7f0a0096

.field public static final channels_list:I = 0x7f0a0090

.field public static final check_row:I = 0x7f0a0255

.field public static final checkbox:I = 0x7f0a004f

.field public static final checked:I = 0x7f0a0135

.field public static final checked_icon:I = 0x7f0a0089

.field public static final checked_image:I = 0x7f0a017c

.field public static final checkmark:I = 0x7f0a012e

.field public static final circle_location:I = 0x7f0a0082

.field public static final clear_btn:I = 0x7f0a0164

.field public static final clip_horizontal:I = 0x7f0a0010

.field public static final clip_vertical:I = 0x7f0a0011

.field public static final close_rel:I = 0x7f0a00ed

.field public static final cmd:I = 0x7f0a01a3

.field public static final collapsable_part:I = 0x7f0a01eb

.field public static final collapseActionView:I = 0x7f0a0029

.field public static final collapsed_view:I = 0x7f0a01ff

.field public static final columnWidth:I = 0x7f0a003a

.field public static final com_facebook_body_frame:I = 0x7f0a00ab

.field public static final com_facebook_button_xout:I = 0x7f0a00ad

.field public static final com_facebook_login_activity_progress_bar:I = 0x7f0a009b

.field public static final com_facebook_picker_activity_circle:I = 0x7f0a009a

.field public static final com_facebook_picker_checkbox:I = 0x7f0a009d

.field public static final com_facebook_picker_checkbox_stub:I = 0x7f0a00a1

.field public static final com_facebook_picker_divider:I = 0x7f0a00a5

.field public static final com_facebook_picker_done_button:I = 0x7f0a00a4

.field public static final com_facebook_picker_image:I = 0x7f0a009e

.field public static final com_facebook_picker_list_section_header:I = 0x7f0a00a2

.field public static final com_facebook_picker_list_view:I = 0x7f0a0099

.field public static final com_facebook_picker_profile_pic_stub:I = 0x7f0a009f

.field public static final com_facebook_picker_row_activity_circle:I = 0x7f0a009c

.field public static final com_facebook_picker_search_text:I = 0x7f0a00aa

.field public static final com_facebook_picker_title:I = 0x7f0a00a0

.field public static final com_facebook_picker_title_bar:I = 0x7f0a00a7

.field public static final com_facebook_picker_title_bar_stub:I = 0x7f0a00a6

.field public static final com_facebook_picker_top_bar:I = 0x7f0a00a3

.field public static final com_facebook_search_bar_view:I = 0x7f0a00a9

.field public static final com_facebook_tooltip_bubble_view_bottom_pointer:I = 0x7f0a00af

.field public static final com_facebook_tooltip_bubble_view_text_body:I = 0x7f0a00ae

.field public static final com_facebook_tooltip_bubble_view_top_pointer:I = 0x7f0a00ac

.field public static final com_facebook_usersettingsfragment_login_button:I = 0x7f0a00b2

.field public static final com_facebook_usersettingsfragment_logo_image:I = 0x7f0a00b0

.field public static final com_facebook_usersettingsfragment_profile_name:I = 0x7f0a00b1

.field public static final command_holder1:I = 0x7f0a0180

.field public static final command_holder2:I = 0x7f0a0181

.field public static final compose_tweet:I = 0x7f0a0283

.field public static final container:I = 0x7f0a0207

.field public static final content:I = 0x7f0a00c9

.field public static final contentPanel:I = 0x7f0a020a

.field public static final content_new:I = 0x7f0a00c5

.field public static final content_sub_text:I = 0x7f0a00b6

.field public static final content_text:I = 0x7f0a00b5

.field public static final contents:I = 0x7f0a030c

.field public static final contents_container:I = 0x7f0a00bb

.field public static final continue_btn:I = 0x7f0a025c

.field public static final controllerScreen:I = 0x7f0a01ec

.field public static final controlpad_back_btn:I = 0x7f0a00be

.field public static final controlpad_device_container:I = 0x7f0a00c4

.field public static final controlpad_room_state:I = 0x7f0a00bd

.field public static final controlpad_scrollview:I = 0x7f0a00c3

.field public static final country_btn:I = 0x7f0a025e

.field public static final country_list:I = 0x7f0a025a

.field public static final country_list_btn:I = 0x7f0a025f

.field public static final country_list_filter:I = 0x7f0a0258

.field public static final coverView:I = 0x7f0a00f0

.field public static final current_episode_only:I = 0x7f0a0221

.field public static final customPanel:I = 0x7f0a020c

.field public static final customize_linearlayout:I = 0x7f0a02da

.field public static final customize_time_textview:I = 0x7f0a02db

.field public static final dashboard_minus:I = 0x7f0a00e3

.field public static final dashboard_plus:I = 0x7f0a00e4

.field public static final date_container:I = 0x7f0a013f

.field public static final date_label:I = 0x7f0a0140

.field public static final date_label_small:I = 0x7f0a0141

.field public static final date_textview:I = 0x7f0a02c4

.field public static final day:I = 0x7f0a0287

.field public static final decor_content_parent:I = 0x7f0a0055

.field public static final default_activity_button:I = 0x7f0a004a

.field public static final delete_button:I = 0x7f0a00fe

.field public static final delete_image:I = 0x7f0a031a

.field public static final delete_room:I = 0x7f0a023a

.field public static final desc:I = 0x7f0a0166

.field public static final desc_layout:I = 0x7f0a0165

.field public static final detail_list:I = 0x7f0a008b

.field public static final device:I = 0x7f0a01a0

.field public static final device_0:I = 0x7f0a01f4

.field public static final device_1:I = 0x7f0a01f5

.field public static final device_2:I = 0x7f0a01f6

.field public static final device_3:I = 0x7f0a01f7

.field public static final device_4:I = 0x7f0a01f8

.field public static final device_5:I = 0x7f0a01f9

.field public static final device_6:I = 0x7f0a01fa

.field public static final device_button:I = 0x7f0a01ed

.field public static final device_list:I = 0x7f0a0129

.field public static final device_name:I = 0x7f0a01ee

.field public static final device_select:I = 0x7f0a0321

.field public static final device_tab_1:I = 0x7f0a0115

.field public static final device_tab_2:I = 0x7f0a0116

.field public static final device_tab_3:I = 0x7f0a0117

.field public static final device_visual:I = 0x7f0a0114

.field public static final devices_drawer:I = 0x7f0a00c8

.field public static final devices_flipper:I = 0x7f0a00cf

.field public static final devices_selector:I = 0x7f0a01f3

.field public static final devices_tabs:I = 0x7f0a00ce

.field public static final dialog:I = 0x7f0a002d

.field public static final diff_list:I = 0x7f0a0120

.field public static final diff_question:I = 0x7f0a011f

.field public static final disableHome:I = 0x7f0a001b

.field public static final disabled:I = 0x7f0a0037

.field public static final disambiguation_desc:I = 0x7f0a011e

.field public static final disambiguation_panel:I = 0x7f0a011d

.field public static final discover_magic:I = 0x7f0a026e

.field public static final dislike_btn:I = 0x7f0a027d

.field public static final divider:I = 0x7f0a00de

.field public static final divider1:I = 0x7f0a01de

.field public static final divider2:I = 0x7f0a01e0

.field public static final divider3:I = 0x7f0a01e2

.field public static final dividerRow:I = 0x7f0a015e

.field public static final dividerView:I = 0x7f0a02c6

.field public static final divider_country_list_down:I = 0x7f0a0260

.field public static final divider_country_list_top:I = 0x7f0a025d

.field public static final divider_view:I = 0x7f0a013a

.field public static final do_not_show_btn:I = 0x7f0a01c7

.field public static final done:I = 0x7f0a012b

.field public static final done_btn:I = 0x7f0a0068

.field public static final drag_icon:I = 0x7f0a0136

.field public static final drawer:I = 0x7f0a01bd

.field public static final drawer_layout:I = 0x7f0a01bc

.field public static final dropdown:I = 0x7f0a002e

.field public static final editText:I = 0x7f0a0178

.field public static final edit_query:I = 0x7f0a0059

.field public static final edittext:I = 0x7f0a01c8

.field public static final email:I = 0x7f0a022a

.field public static final email_tv:I = 0x7f0a016c

.field public static final emoji:I = 0x7f0a01a9

.field public static final empty:I = 0x7f0a0092

.field public static final empty_bg:I = 0x7f0a01c0

.field public static final empty_layout:I = 0x7f0a01fe

.field public static final empty_list_msg:I = 0x7f0a0168

.field public static final empty_list_view:I = 0x7f0a0160

.field public static final end:I = 0x7f0a0023

.field public static final epg_image:I = 0x7f0a008a

.field public static final epg_list:I = 0x7f0a014a

.field public static final epg_pager:I = 0x7f0a0091

.field public static final episode_name:I = 0x7f0a021d

.field public static final episode_number:I = 0x7f0a0279

.field public static final episode_season:I = 0x7f0a027f

.field public static final episode_title:I = 0x7f0a027a

.field public static final episodes_list:I = 0x7f0a0278

.field public static final exit_btn_container:I = 0x7f0a02f0

.field public static final expand_activities_button:I = 0x7f0a0048

.field public static final expanded_menu:I = 0x7f0a004e

.field public static final expanded_toggle:I = 0x7f0a01cf

.field public static final fake_edit:I = 0x7f0a0284

.field public static final fan_label:I = 0x7f0a018c

.field public static final favChannelTitle:I = 0x7f0a015d

.field public static final fav_ch_list:I = 0x7f0a015f

.field public static final fav_ch_listview:I = 0x7f0a0155

.field public static final fav_ch_scroller:I = 0x7f0a00db

.field public static final fav_divider:I = 0x7f0a0328

.field public static final fav_divider1:I = 0x7f0a032d

.field public static final favorite_btn:I = 0x7f0a0311

.field public static final favorite_channels_textview:I = 0x7f0a0157

.field public static final feedback_confirm_screen:I = 0x7f0a0176

.field public static final feedback_form_screen:I = 0x7f0a016b

.field public static final feedback_form_scroll:I = 0x7f0a016a

.field public static final feedback_label:I = 0x7f0a0170

.field public static final feedback_model:I = 0x7f0a0172

.field public static final fill:I = 0x7f0a0012

.field public static final fill_horizontal:I = 0x7f0a0013

.field public static final fill_vertical:I = 0x7f0a0014

.field public static final fling:I = 0x7f0a0032

.field public static final flipper:I = 0x7f0a010a

.field public static final footerView:I = 0x7f0a00ba

.field public static final found_roku_label:I = 0x7f0a0199

.field public static final gallery_icon:I = 0x7f0a0285

.field public static final gender_age_selection_female_grid:I = 0x7f0a0248

.field public static final gender_age_selection_male_grid:I = 0x7f0a0246

.field public static final gender_image:I = 0x7f0a0179

.field public static final genre:I = 0x7f0a021e

.field public static final genre_container:I = 0x7f0a0303

.field public static final genre_list:I = 0x7f0a024d

.field public static final genres:I = 0x7f0a027e

.field public static final group_indicator:I = 0x7f0a027b

.field public static final handle:I = 0x7f0a00ca

.field public static final hash_label:I = 0x7f0a031d

.field public static final hash_tags:I = 0x7f0a031e

.field public static final header:I = 0x7f0a01c3

.field public static final headerView:I = 0x7f0a00b8

.field public static final header_label:I = 0x7f0a0320

.field public static final header_sep:I = 0x7f0a008f

.field public static final help:I = 0x7f0a02c0

.field public static final help_txt:I = 0x7f0a0273

.field public static final here:I = 0x7f0a0249

.field public static final hide_button:I = 0x7f0a01ef

.field public static final hide_button_text:I = 0x7f0a01f0

.field public static final home:I = 0x7f0a0005

.field public static final homeAsUp:I = 0x7f0a001c

.field public static final horizontal:I = 0x7f0a0038

.field public static final hours_down_button:I = 0x7f0a02cb

.field public static final hours_edittext:I = 0x7f0a02ca

.field public static final hours_up_button:I = 0x7f0a02c9

.field public static final hybrid:I = 0x7f0a0025

.field public static final icon:I = 0x7f0a004c

.field public static final icon_peel_tv:I = 0x7f0a01ad

.field public static final icon_txt:I = 0x7f0a0334

.field public static final id1:I = 0x7f0a0123

.field public static final id2:I = 0x7f0a0124

.field public static final id3:I = 0x7f0a0126

.field public static final ifRoom:I = 0x7f0a002a

.field public static final image:I = 0x7f0a0049

.field public static final imageView:I = 0x7f0a02b2

.field public static final img:I = 0x7f0a025b

.field public static final img1:I = 0x7f0a0331

.field public static final img2:I = 0x7f0a0332

.field public static final img3:I = 0x7f0a0333

.field public static final img_controlpad:I = 0x7f0a00c6

.field public static final img_device_controlpad:I = 0x7f0a00cb

.field public static final img_facebook:I = 0x7f0a01ba

.field public static final img_google:I = 0x7f0a01bb

.field public static final img_personalize_checked:I = 0x7f0a02fd

.field public static final img_personalize_normal:I = 0x7f0a02fc

.field public static final img_samsung:I = 0x7f0a01b9

.field public static final img_shadow_bottom:I = 0x7f0a02c1

.field public static final import_continue:I = 0x7f0a0106

.field public static final import_header:I = 0x7f0a0101

.field public static final import_list:I = 0x7f0a0103

.field public static final indicator:I = 0x7f0a0097

.field public static final info:I = 0x7f0a028d

.field public static final info_rel:I = 0x7f0a00e9

.field public static final inline:I = 0x7f0a0041

.field public static final input_list:I = 0x7f0a019f

.field public static final ip:I = 0x7f0a012c

.field public static final ip_address:I = 0x7f0a0235

.field public static final ip_device_list_panel:I = 0x7f0a0128

.field public static final item_container:I = 0x7f0a018f

.field public static final iv_cancel_icon:I = 0x7f0a0305

.field public static final iv_close:I = 0x7f0a00d9

.field public static final iv_handle_flipper:I = 0x7f0a00d5

.field public static final iv_profile_no_pic:I = 0x7f0a024f

.field public static final iv_profile_pic:I = 0x7f0a02ae

.field public static final iv_show_image:I = 0x7f0a014c

.field public static final key_list:I = 0x7f0a0254

.field public static final key_name:I = 0x7f0a0252

.field public static final keyword:I = 0x7f0a0163

.field public static final label_lockscreen:I = 0x7f0a0083

.field public static final lable:I = 0x7f0a00e7

.field public static final large:I = 0x7f0a0042

.field public static final layout_buttons:I = 0x7f0a0237

.field public static final layout_device_select:I = 0x7f0a017e

.field public static final layout_device_setup_test:I = 0x7f0a0113

.field public static final layout_prev_tweet:I = 0x7f0a01df

.field public static final layout_spinner:I = 0x7f0a00e0

.field public static final layout_test_btn:I = 0x7f0a00f8

.field public static final layout_test_msg:I = 0x7f0a00f6

.field public static final layout_widget_remote_inner:I = 0x7f0a0079

.field public static final learn_btn:I = 0x7f0a01a4

.field public static final learn_new_code_btn:I = 0x7f0a01a2

.field public static final left:I = 0x7f0a0015

.field public static final left_date_button:I = 0x7f0a02c3

.field public static final left_layout:I = 0x7f0a013d

.field public static final left_spacer:I = 0x7f0a02c8

.field public static final like_btn:I = 0x7f0a027c

.field public static final line1:I = 0x7f0a020f

.field public static final line2:I = 0x7f0a0211

.field public static final list:I = 0x7f0a00b3

.field public static final listMode:I = 0x7f0a0018

.field public static final listTitle:I = 0x7f0a023d

.field public static final list_item:I = 0x7f0a004b

.field public static final ll:I = 0x7f0a0238

.field public static final ll_parent:I = 0x7f0a02b3

.field public static final ll_parent2:I = 0x7f0a02bc

.field public static final loc_list:I = 0x7f0a0084

.field public static final location:I = 0x7f0a012d

.field public static final location_check:I = 0x7f0a0081

.field public static final lock_command_holder:I = 0x7f0a017f

.field public static final lock_common_container:I = 0x7f0a01b6

.field public static final lock_power_container:I = 0x7f0a01b0

.field public static final lock_text_hint:I = 0x7f0a0229

.field public static final lockscreen_widget:I = 0x7f0a0224

.field public static final login_btn:I = 0x7f0a0281

.field public static final logo:I = 0x7f0a0065

.field public static final logo_text:I = 0x7f0a0219

.field public static final lv_drawer_option:I = 0x7f0a00da

.field public static final main_item:I = 0x7f0a01a5

.field public static final margin_view:I = 0x7f0a030e

.field public static final media_image:I = 0x7f0a030d

.field public static final menu_about:I = 0x7f0a0347

.field public static final menu_browse:I = 0x7f0a02b4

.field public static final menu_browse_forum:I = 0x7f0a02be

.field public static final menu_change_room:I = 0x7f0a0345

.field public static final menu_channels:I = 0x7f0a0346

.field public static final menu_check:I = 0x7f0a033d

.field public static final menu_delete:I = 0x7f0a033e

.field public static final menu_done:I = 0x7f0a0340

.field public static final menu_edit:I = 0x7f0a033b

.field public static final menu_favorites:I = 0x7f0a02b7

.field public static final menu_help:I = 0x7f0a02ba

.field public static final menu_like_set:I = 0x7f0a0339

.field public static final menu_like_unset:I = 0x7f0a033a

.field public static final menu_next:I = 0x7f0a033f

.field public static final menu_program_guide:I = 0x7f0a02b8

.field public static final menu_remote:I = 0x7f0a033c

.field public static final menu_retry:I = 0x7f0a0344

.field public static final menu_room_change:I = 0x7f0a0338

.field public static final menu_search:I = 0x7f0a0336

.field public static final menu_send:I = 0x7f0a0342

.field public static final menu_send_feedback:I = 0x7f0a02bd

.field public static final menu_settings:I = 0x7f0a02b9

.field public static final menu_share:I = 0x7f0a0343

.field public static final menu_text:I = 0x7f0a0341

.field public static final menu_time:I = 0x7f0a0337

.field public static final menu_troubleshoot_channel:I = 0x7f0a02bf

.field public static final menu_tweet_refresh:I = 0x7f0a0348

.field public static final message:I = 0x7f0a007a

.field public static final messageDivider:I = 0x7f0a02df

.field public static final message_popup:I = 0x7f0a00d7

.field public static final middle:I = 0x7f0a0024

.field public static final minutes_down_button:I = 0x7f0a02cf

.field public static final minutes_layout:I = 0x7f0a02cc

.field public static final minutes_textview:I = 0x7f0a02ce

.field public static final minutes_up_button:I = 0x7f0a02cd

.field public static final missing_device_brand_btn:I = 0x7f0a0112

.field public static final missing_other_stb_brand_btn:I = 0x7f0a02aa

.field public static final missing_tv_cancel_btn:I = 0x7f0a0233

.field public static final missing_tv_msg_desc:I = 0x7f0a0232

.field public static final missing_tv_msg_email:I = 0x7f0a022f

.field public static final missing_tv_msg_service_provider_name:I = 0x7f0a0231

.field public static final missing_tv_msg_subject:I = 0x7f0a0230

.field public static final missing_tv_projector_brand_btn:I = 0x7f0a0298

.field public static final missing_tv_send_btn:I = 0x7f0a0234

.field public static final model:I = 0x7f0a022e

.field public static final model_number:I = 0x7f0a0173

.field public static final model_subject:I = 0x7f0a0174

.field public static final model_tv:I = 0x7f0a022d

.field public static final month:I = 0x7f0a0288

.field public static final more_bottom:I = 0x7f0a01bf

.field public static final more_btn:I = 0x7f0a0240

.field public static final more_channel_icon:I = 0x7f0a0161

.field public static final more_like_this_list:I = 0x7f0a01c4

.field public static final more_txt:I = 0x7f0a0152

.field public static final msg:I = 0x7f0a0093

.field public static final msg_desc:I = 0x7f0a0175

.field public static final msg_email:I = 0x7f0a016d

.field public static final msg_model_num:I = 0x7f0a0191

.field public static final msg_subject:I = 0x7f0a016f

.field public static final my_phone:I = 0x7f0a026c

.field public static final name:I = 0x7f0a0088

.field public static final never:I = 0x7f0a002b

.field public static final new_episode_only:I = 0x7f0a0222

.field public static final new_episode_rerun:I = 0x7f0a0223

.field public static final next_btn:I = 0x7f0a0069

.field public static final next_channel_btn:I = 0x7f0a02e7

.field public static final next_time_btn:I = 0x7f0a0144

.field public static final next_view:I = 0x7f0a0067

.field public static final no_btn:I = 0x7f0a00fb

.field public static final no_content_panel:I = 0x7f0a01c9

.field public static final no_net:I = 0x7f0a0085

.field public static final no_thanks:I = 0x7f0a012a

.field public static final nocontent_text:I = 0x7f0a01ca

.field public static final node_list:I = 0x7f0a0167

.field public static final none:I = 0x7f0a001d

.field public static final normal:I = 0x7f0a0019

.field public static final not_found_roku_label:I = 0x7f0a019b

.field public static final noti_btn_close:I = 0x7f0a01dd

.field public static final noti_btn_collapsed_watchon:I = 0x7f0a01e9

.field public static final noti_btn_prev:I = 0x7f0a01e1

.field public static final noti_btn_twitter:I = 0x7f0a01e3

.field public static final noti_btn_watchon:I = 0x7f0a01e4

.field public static final noti_main:I = 0x7f0a01ce

.field public static final noti_playing_widget_bg_img:I = 0x7f0a01e6

.field public static final noti_show_channel:I = 0x7f0a01da

.field public static final noti_show_image:I = 0x7f0a01d9

.field public static final noti_show_image_collapsed:I = 0x7f0a01e7

.field public static final noti_show_status:I = 0x7f0a01dc

.field public static final noti_show_title:I = 0x7f0a01db

.field public static final noti_show_title_collapsed:I = 0x7f0a01ea

.field public static final notification_panel:I = 0x7f0a0226

.field public static final on_air_txt:I = 0x7f0a02dc

.field public static final ondemand_video_host_icon:I = 0x7f0a0202

.field public static final online_support_btn:I = 0x7f0a0196

.field public static final option_container:I = 0x7f0a0220

.field public static final other_brand_btn:I = 0x7f0a010c

.field public static final other_container:I = 0x7f0a0295

.field public static final other_list:I = 0x7f0a0111

.field public static final other_settop_list_filter:I = 0x7f0a029e

.field public static final other_settop_stb_brand_list:I = 0x7f0a02a9

.field public static final other_settopbox_list_filter:I = 0x7f0a02a6

.field public static final other_tv_brand_btn:I = 0x7f0a0292

.field public static final other_tv_list_filter:I = 0x7f0a0296

.field public static final overflow_menu_btn:I = 0x7f0a00bc

.field public static final pad_layout:I = 0x7f0a00dc

.field public static final pager:I = 0x7f0a0169

.field public static final parentPanel:I = 0x7f0a0208

.field public static final password:I = 0x7f0a0134

.field public static final peel_logo_layout:I = 0x7f0a01d5

.field public static final peel_trademark:I = 0x7f0a01ae

.field public static final peel_txt:I = 0x7f0a02c2

.field public static final picker_subtitle:I = 0x7f0a00a8

.field public static final pin_donotshow_layout:I = 0x7f0a0228

.field public static final pin_lock_text:I = 0x7f0a0225

.field public static final pin_noti_text:I = 0x7f0a0227

.field public static final playback_ctrl_view:I = 0x7f0a0071

.field public static final playing_widget_collapsed_layout:I = 0x7f0a01e5

.field public static final playing_widget_layout:I = 0x7f0a01d8

.field public static final plus_image:I = 0x7f0a00b7

.field public static final popup_element:I = 0x7f0a00dd

.field public static final popup_linear:I = 0x7f0a01c6

.field public static final popup_txt:I = 0x7f0a00d8

.field public static final post_btn:I = 0x7f0a0314

.field public static final post_pic:I = 0x7f0a0319

.field public static final post_pic_container:I = 0x7f0a0318

.field public static final powerbypeel:I = 0x7f0a01f2

.field public static final powered_rel:I = 0x7f0a0218

.field public static final prev_time_btn:I = 0x7f0a013e

.field public static final primetime_linearlayout:I = 0x7f0a02d8

.field public static final primetime_time_textview:I = 0x7f0a02d9

.field public static final prism_txt:I = 0x7f0a02dd

.field public static final profile_parent:I = 0x7f0a02ad

.field public static final profile_parent_logout:I = 0x7f0a024e

.field public static final profile_search_parent:I = 0x7f0a02b0

.field public static final progress_bar:I = 0x7f0a0132

.field public static final progress_circular:I = 0x7f0a0006

.field public static final progress_desc:I = 0x7f0a0130

.field public static final progress_horizontal:I = 0x7f0a0007

.field public static final progress_msg:I = 0x7f0a0131

.field public static final progress_panel:I = 0x7f0a012f

.field public static final projector_btn:I = 0x7f0a0293

.field public static final providerName:I = 0x7f0a00e1

.field public static final radio:I = 0x7f0a0051

.field public static final rate:I = 0x7f0a007b

.field public static final rateLater:I = 0x7f0a007c

.field public static final record_btn:I = 0x7f0a0205

.field public static final refresh:I = 0x7f0a01cb

.field public static final refresh_btn:I = 0x7f0a0313

.field public static final refresh_icon:I = 0x7f0a01cd

.field public static final refresh_txt:I = 0x7f0a01cc

.field public static final regions:I = 0x7f0a0267

.field public static final related_tags_container:I = 0x7f0a031c

.field public static final relativeLayout1:I = 0x7f0a00e8

.field public static final remind_me_txt:I = 0x7f0a023f

.field public static final reminder_badge:I = 0x7f0a02f9

.field public static final reminder_icon:I = 0x7f0a028a

.field public static final removeChannelBtn:I = 0x7f0a015a

.field public static final rename_icon:I = 0x7f0a023b

.field public static final reply_btn:I = 0x7f0a030f

.field public static final report:I = 0x7f0a01aa

.field public static final report_ir_confirm_msg:I = 0x7f0a0195

.field public static final report_missing_ir_confirm_screen:I = 0x7f0a0194

.field public static final report_missing_ir_form_screen:I = 0x7f0a0190

.field public static final reset:I = 0x7f0a01ab

.field public static final retweet_btn:I = 0x7f0a0310

.field public static final right:I = 0x7f0a0016

.field public static final right_date_button:I = 0x7f0a02c5

.field public static final right_layout:I = 0x7f0a0143

.field public static final right_spacer:I = 0x7f0a02d5

.field public static final rl_extra_content:I = 0x7f0a011a

.field public static final rl_image_parent:I = 0x7f0a014b

.field public static final roku_list:I = 0x7f0a019a

.field public static final roku_progress:I = 0x7f0a0198

.field public static final roku_setup_label:I = 0x7f0a0197

.field public static final room_input_container:I = 0x7f0a026a

.field public static final room_name:I = 0x7f0a00cc

.field public static final room_next:I = 0x7f0a026b

.field public static final roomname:I = 0x7f0a00e2

.field public static final satellite:I = 0x7f0a0026

.field public static final screen_name:I = 0x7f0a0316

.field public static final scrollView:I = 0x7f0a020b

.field public static final scroller:I = 0x7f0a0236

.field public static final search_badge:I = 0x7f0a005b

.field public static final search_bar:I = 0x7f0a005a

.field public static final search_btn:I = 0x7f0a0263

.field public static final search_button:I = 0x7f0a005c

.field public static final search_by_region_layout:I = 0x7f0a0265

.field public static final search_by_zipcode_layout:I = 0x7f0a0261

.field public static final search_cancel_btn:I = 0x7f0a0259

.field public static final search_cancel_btn_other:I = 0x7f0a0110

.field public static final search_close_btn:I = 0x7f0a0061

.field public static final search_edit_frame:I = 0x7f0a005d

.field public static final search_go_btn:I = 0x7f0a0063

.field public static final search_icon:I = 0x7f0a010f

.field public static final search_icon_othersetup:I = 0x7f0a029f

.field public static final search_icon_othersetupbox:I = 0x7f0a02a7

.field public static final search_icon_othertv:I = 0x7f0a0297

.field public static final search_layout:I = 0x7f0a0257

.field public static final search_layout_other:I = 0x7f0a010d

.field public static final search_mag_icon:I = 0x7f0a005e

.field public static final search_other_list_filter:I = 0x7f0a010e

.field public static final search_plate:I = 0x7f0a005f

.field public static final search_row_layout:I = 0x7f0a0272

.field public static final search_set_cancel_btn:I = 0x7f0a02a0

.field public static final search_settop_cancel_btn:I = 0x7f0a02a8

.field public static final search_setup_layout:I = 0x7f0a02a5

.field public static final search_setup_layout_settop:I = 0x7f0a0162

.field public static final search_src_text:I = 0x7f0a0060

.field public static final search_voice_btn:I = 0x7f0a0064

.field public static final searchword:I = 0x7f0a02b1

.field public static final season_spinner:I = 0x7f0a0277

.field public static final season_text:I = 0x7f0a0276

.field public static final season_title:I = 0x7f0a0289

.field public static final selected:I = 0x7f0a01a8

.field public static final selected_bar:I = 0x7f0a02b5

.field public static final send_btn:I = 0x7f0a0193

.field public static final separating_sign:I = 0x7f0a0253

.field public static final seperator:I = 0x7f0a023c

.field public static final set_reminder:I = 0x7f0a0204

.field public static final settings_activity_tv:I = 0x7f0a0243

.field public static final settings_button:I = 0x7f0a024a

.field public static final settings_list:I = 0x7f0a0251

.field public static final setup_btn:I = 0x7f0a0271

.field public static final setup_button:I = 0x7f0a01f1

.field public static final setup_header_layout:I = 0x7f0a01fb

.field public static final setup_image:I = 0x7f0a01fc

.field public static final setup_ir_btn:I = 0x7f0a019e

.field public static final setup_text:I = 0x7f0a01fd

.field public static final shadow_btn_area:I = 0x7f0a008c

.field public static final shortcut:I = 0x7f0a0050

.field public static final showCustom:I = 0x7f0a001e

.field public static final showHome:I = 0x7f0a001f

.field public static final showTitle:I = 0x7f0a0020

.field public static final show_image:I = 0x7f0a01c1

.field public static final show_list_container:I = 0x7f0a031f

.field public static final show_title:I = 0x7f0a01c2

.field public static final shows_with_one_tap:I = 0x7f0a026f

.field public static final showtime:I = 0x7f0a021f

.field public static final single_list:I = 0x7f0a0118

.field public static final skip_btn:I = 0x7f0a0122

.field public static final skip_import:I = 0x7f0a0105

.field public static final slide:I = 0x7f0a0033

.field public static final slideLeft:I = 0x7f0a0034

.field public static final slideRight:I = 0x7f0a0035

.field public static final small:I = 0x7f0a0043

.field public static final source_btn:I = 0x7f0a02de

.field public static final spacingWidth:I = 0x7f0a003b

.field public static final spacingWidthUniform:I = 0x7f0a003c

.field public static final spinner_issue_type:I = 0x7f0a0171

.field public static final split_action_bar:I = 0x7f0a0008

.field public static final ss_epg:I = 0x7f0a013c

.field public static final standard:I = 0x7f0a0040

.field public static final stb_brand_list:I = 0x7f0a02a3

.field public static final stb_highlight_img:I = 0x7f0a02a2

.field public static final stb_signal_container:I = 0x7f0a02ac

.field public static final sub_item:I = 0x7f0a01a6

.field public static final subject:I = 0x7f0a016e

.field public static final submit_area:I = 0x7f0a0062

.field public static final subregions:I = 0x7f0a0268

.field public static final support_btn:I = 0x7f0a0177

.field public static final switch_box_btn:I = 0x7f0a0127

.field public static final synopsis:I = 0x7f0a0151

.field public static final tabMode:I = 0x7f0a001a

.field public static final tag_image:I = 0x7f0a0009

.field public static final taphere:I = 0x7f0a0270

.field public static final temperature_txt_id:I = 0x7f0a000a

.field public static final terrain:I = 0x7f0a0027

.field public static final test_bg:I = 0x7f0a00f9

.field public static final test_btn:I = 0x7f0a01a1

.field public static final test_btn_viewpager:I = 0x7f0a00f3

.field public static final test_other_btn_large_view:I = 0x7f0a02e1

.field public static final test_other_btn_small_view:I = 0x7f0a02e3

.field public static final test_pager_left_btn:I = 0x7f0a00f4

.field public static final test_pager_right_btn:I = 0x7f0a00f5

.field public static final test_pw_btn_large_view:I = 0x7f0a02e5

.field public static final test_pw_btn_small_view:I = 0x7f0a02e6

.field public static final test_question_msg:I = 0x7f0a00fa

.field public static final test_status_msg:I = 0x7f0a00fd

.field public static final test_tune_btn_large_view:I = 0x7f0a02e8

.field public static final test_tune_btn_small_view:I = 0x7f0a02eb

.field public static final testing_stb_list_title_textview:I = 0x7f0a02a4

.field public static final testing_turn_on_msg:I = 0x7f0a00f2

.field public static final text:I = 0x7f0a00ff

.field public static final text1:I = 0x7f0a007f

.field public static final text2:I = 0x7f0a0080

.field public static final textView:I = 0x7f0a0206

.field public static final textView2:I = 0x7f0a00ec

.field public static final text_add_room:I = 0x7f0a0242

.field public static final text_overlay:I = 0x7f0a024c

.field public static final text_view:I = 0x7f0a02b6

.field public static final text_view_help:I = 0x7f0a02bb

.field public static final textview1:I = 0x7f0a01b4

.field public static final textview2:I = 0x7f0a01b5

.field public static final third_item:I = 0x7f0a01a7

.field public static final tile_title_layout:I = 0x7f0a02fa

.field public static final tiles_grid:I = 0x7f0a02f7

.field public static final time:I = 0x7f0a021b

.field public static final time_duration_text:I = 0x7f0a0074

.field public static final time_elapse_text:I = 0x7f0a0073

.field public static final time_label:I = 0x7f0a00e6

.field public static final time_label_layout:I = 0x7f0a00e5

.field public static final time_linearlayout:I = 0x7f0a02c7

.field public static final time_list:I = 0x7f0a02ff

.field public static final time_row:I = 0x7f0a02fe

.field public static final timeslot_1:I = 0x7f0a0148

.field public static final timeslot_2:I = 0x7f0a0149

.field public static final timeslot_container:I = 0x7f0a0147

.field public static final timestamp:I = 0x7f0a0312

.field public static final title:I = 0x7f0a004d

.field public static final titleDivider:I = 0x7f0a0102

.field public static final title_bg_img:I = 0x7f0a0244

.field public static final title_female:I = 0x7f0a0247

.field public static final title_male:I = 0x7f0a0245

.field public static final title_setup_region:I = 0x7f0a0266

.field public static final title_setup_zipcode:I = 0x7f0a0262

.field public static final toast_close_img:I = 0x7f0a00ee

.field public static final toast_rel:I = 0x7f0a00ea

.field public static final toast_title:I = 0x7f0a00eb

.field public static final toggleButton1:I = 0x7f0a0256

.field public static final tool_container:I = 0x7f0a031b

.field public static final top:I = 0x7f0a0017

.field public static final topPanel:I = 0x7f0a0100

.field public static final tops_scroll:I = 0x7f0a0301

.field public static final triangle:I = 0x7f0a0030

.field public static final trigger_desc:I = 0x7f0a0306

.field public static final trigger_title:I = 0x7f0a0304

.field public static final triggers_placeholder:I = 0x7f0a013b

.field public static final tune_channel_btn:I = 0x7f0a02e9

.field public static final tune_channel_btn_small:I = 0x7f0a02ec

.field public static final tunein_check_msg:I = 0x7f0a0214

.field public static final turn_on_msg:I = 0x7f0a00f7

.field public static final tutorial_msg:I = 0x7f0a0308

.field public static final tutorial_title:I = 0x7f0a0309

.field public static final tutorials:I = 0x7f0a0201

.field public static final tv_add_text:I = 0x7f0a0146

.field public static final tv_brand_container:I = 0x7f0a0290

.field public static final tv_channel_guide_text:I = 0x7f0a00d4

.field public static final tv_desc:I = 0x7f0a0250

.field public static final tv_epg_text:I = 0x7f0a0145

.field public static final tv_heading:I = 0x7f0a0137

.field public static final tv_input_container:I = 0x7f0a0108

.field public static final tv_input_dropdown:I = 0x7f0a0109

.field public static final tv_list:I = 0x7f0a0294

.field public static final tv_name:I = 0x7f0a02af

.field public static final tv_new:I = 0x7f0a014e

.field public static final tv_next_show:I = 0x7f0a0150

.field public static final tv_only_btn:I = 0x7f0a029c

.field public static final tv_only_textview:I = 0x7f0a029d

.field public static final tv_point_anim:I = 0x7f0a0087

.field public static final tv_service_list:I = 0x7f0a0269

.field public static final tv_show_name:I = 0x7f0a014d

.field public static final tv_show_time:I = 0x7f0a014f

.field public static final tv_signal_container:I = 0x7f0a00ef

.field public static final tv_small_hand_img:I = 0x7f0a02a1

.field public static final tv_subheading:I = 0x7f0a0138

.field public static final tv_tuner_intro:I = 0x7f0a0299

.field public static final tv_visual:I = 0x7f0a00f1

.field public static final tv_with_dvr_btn:I = 0x7f0a029a

.field public static final tv_with_dvr_textview:I = 0x7f0a029b

.field public static final tweet_list:I = 0x7f0a0275

.field public static final tweets_listview:I = 0x7f0a0282

.field public static final twitt_text:I = 0x7f0a0317

.field public static final twitter_login_text:I = 0x7f0a0280

.field public static final txt1:I = 0x7f0a02f4

.field public static final txt2:I = 0x7f0a02f5

.field public static final txt3:I = 0x7f0a02f6

.field public static final type:I = 0x7f0a0098

.field public static final underline:I = 0x7f0a0031

.field public static final up:I = 0x7f0a000b

.field public static final useLogo:I = 0x7f0a0021

.field public static final user_image:I = 0x7f0a030a

.field public static final user_pic:I = 0x7f0a0315

.field public static final userid:I = 0x7f0a030b

.field public static final username:I = 0x7f0a0133

.field public static final vertical:I = 0x7f0a0039

.field public static final vhandle_close:I = 0x7f0a00d0

.field public static final video_btn_min_scr:I = 0x7f0a0076

.field public static final video_btn_mute:I = 0x7f0a0075

.field public static final video_btn_playpause:I = 0x7f0a0077

.field public static final video_container_view:I = 0x7f0a006b

.field public static final video_original_btn_close:I = 0x7f0a006d

.field public static final video_original_btn_mute:I = 0x7f0a006e

.field public static final video_progressbar:I = 0x7f0a0072

.field public static final video_view:I = 0x7f0a006c

.field public static final view_block:I = 0x7f0a00d1

.field public static final view_right:I = 0x7f0a0142

.field public static final view_temp:I = 0x7f0a0078

.field public static final vod_selection_grid:I = 0x7f0a0154

.field public static final vod_selection_title:I = 0x7f0a0153

.field public static final watch_img:I = 0x7f0a01e8

.field public static final watch_on_tv:I = 0x7f0a0203

.field public static final watchons:I = 0x7f0a028e

.field public static final webView:I = 0x7f0a021a

.field public static final webcontent:I = 0x7f0a01be

.field public static final webview:I = 0x7f0a0119

.field public static final widget_channel_next:I = 0x7f0a032c

.field public static final widget_channel_prev:I = 0x7f0a0327

.field public static final widget_device_name:I = 0x7f0a01af

.field public static final widget_device_select_next:I = 0x7f0a0322

.field public static final widget_device_select_prev:I = 0x7f0a0323

.field public static final widget_home_setup_label:I = 0x7f0a032f

.field public static final widget_home_setup_layout:I = 0x7f0a032e

.field public static final widget_home_setup_tap:I = 0x7f0a0330

.field public static final widget_icon:I = 0x7f0a01d0

.field public static final widget_remote_handle_icon:I = 0x7f0a0335

.field public static final wifi_logo:I = 0x7f0a007e

.field public static final withText:I = 0x7f0a002c

.field public static final wrap_content:I = 0x7f0a002f

.field public static final yes_btn:I = 0x7f0a00fc

.field public static final zip_separator:I = 0x7f0a0274

.field public static final zipcode_txt:I = 0x7f0a0264
