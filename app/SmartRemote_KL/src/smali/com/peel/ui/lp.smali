.class Lcom/peel/ui/lp;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:I

.field final synthetic b:Lcom/peel/ui/ll;


# direct methods
.method constructor <init>(Lcom/peel/ui/ll;I)V
    .locals 0

    .prologue
    .line 599
    iput-object p1, p0, Lcom/peel/ui/lp;->b:Lcom/peel/ui/ll;

    iput p2, p0, Lcom/peel/ui/lp;->a:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 602
    iget-object v0, p0, Lcom/peel/ui/lp;->b:Lcom/peel/ui/ll;

    invoke-static {v0}, Lcom/peel/ui/ll;->b(Lcom/peel/ui/ll;)Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 604
    :try_start_0
    invoke-static {}, Lcom/peel/util/a/f;->a()Lcom/peel/util/a/f;

    move-result-object v0

    sget-object v1, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    .line 606
    invoke-virtual {v1}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v1

    if-nez v1, :cond_0

    move v1, v6

    .line 610
    :goto_0
    const/16 v2, 0xc1f

    const/16 v3, 0x7d0

    const-string/jumbo v4, ""

    iget v5, p0, Lcom/peel/ui/lp;->a:I

    add-int/lit8 v5, v5, 0x1

    .line 604
    invoke-virtual/range {v0 .. v5}, Lcom/peel/util/a/f;->a(IIILjava/lang/String;I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 618
    :goto_1
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 619
    const-string/jumbo v1, "from_tutorial"

    invoke-virtual {v0, v1, v6}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 620
    iget-object v1, p0, Lcom/peel/ui/lp;->b:Lcom/peel/ui/ll;

    iget-object v1, v1, Lcom/peel/ui/ll;->a:Lcom/peel/ui/kr;

    invoke-virtual {v1}, Lcom/peel/ui/kr;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    const-class v2, Lcom/peel/i/gm;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lcom/peel/d/e;->c(Landroid/support/v4/app/ae;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 623
    :try_start_1
    invoke-static {}, Lcom/peel/util/a/f;->a()Lcom/peel/util/a/f;

    move-result-object v2

    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    .line 624
    invoke-virtual {v0}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v0

    if-nez v0, :cond_1

    move v1, v6

    .line 625
    :goto_2
    const/16 v3, 0x151c

    const/16 v4, 0x7d5

    iget-object v0, p0, Lcom/peel/ui/lp;->b:Lcom/peel/ui/ll;

    iget-object v0, v0, Lcom/peel/ui/ll;->a:Lcom/peel/ui/kr;

    .line 627
    invoke-virtual {v0}, Lcom/peel/ui/kr;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    invoke-static {v0}, Lcom/peel/util/bx;->d(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/peel/ui/lp;->b:Lcom/peel/ui/ll;

    iget-object v0, v0, Lcom/peel/ui/ll;->a:Lcom/peel/ui/kr;

    .line 628
    invoke-virtual {v0}, Lcom/peel/ui/kr;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    invoke-static {v0}, Lcom/peel/util/bx;->d(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v5, "china"

    invoke-virtual {v0, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string/jumbo v0, ""

    .line 623
    :goto_3
    invoke-virtual {v2, v1, v3, v4, v0}, Lcom/peel/util/a/f;->a(IIILjava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 636
    :goto_4
    return-void

    .line 606
    :cond_0
    :try_start_2
    sget-object v1, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    .line 608
    invoke-virtual {v1}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v1

    .line 609
    invoke-virtual {v1}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v1

    .line 610
    invoke-virtual {v1}, Lcom/peel/data/at;->f()I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    move-result v1

    goto :goto_0

    .line 615
    :catch_0
    move-exception v0

    .line 616
    invoke-static {}, Lcom/peel/ui/kr;->c()Ljava/lang/String;

    move-result-object v1

    invoke-static {}, Lcom/peel/ui/kr;->c()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1

    .line 624
    :cond_1
    :try_start_3
    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v0}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v0

    .line 625
    invoke-virtual {v0}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/data/at;->f()I

    move-result v0

    move v1, v0

    goto :goto_2

    .line 628
    :cond_2
    iget-object v0, p0, Lcom/peel/ui/lp;->b:Lcom/peel/ui/ll;

    iget-object v0, v0, Lcom/peel/ui/ll;->a:Lcom/peel/ui/kr;

    .line 631
    invoke-virtual {v0}, Lcom/peel/ui/kr;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    .line 630
    invoke-static {v0}, Lcom/peel/util/bx;->b(Landroid/content/Context;)Ljava/lang/String;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    move-result-object v0

    goto :goto_3

    .line 633
    :catch_1
    move-exception v0

    .line 634
    invoke-static {}, Lcom/peel/ui/kr;->c()Ljava/lang/String;

    move-result-object v1

    invoke-static {}, Lcom/peel/ui/kr;->c()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_4
.end method
