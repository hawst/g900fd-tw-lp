.class Lcom/peel/ui/mw;
.super Landroid/text/style/ClickableSpan;


# instance fields
.field final synthetic a:Lcom/peel/ui/mo;

.field private b:Ljava/lang/String;

.field private c:I


# direct methods
.method private constructor <init>(Lcom/peel/ui/mo;Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 184
    iput-object p1, p0, Lcom/peel/ui/mw;->a:Lcom/peel/ui/mo;

    invoke-direct {p0}, Landroid/text/style/ClickableSpan;-><init>()V

    .line 185
    iput-object p2, p0, Lcom/peel/ui/mw;->b:Ljava/lang/String;

    .line 186
    iput p3, p0, Lcom/peel/ui/mw;->c:I

    .line 187
    return-void
.end method

.method synthetic constructor <init>(Lcom/peel/ui/mo;Ljava/lang/String;ILcom/peel/ui/mp;)V
    .locals 0

    .prologue
    .line 180
    invoke-direct {p0, p1, p2, p3}, Lcom/peel/ui/mw;-><init>(Lcom/peel/ui/mo;Ljava/lang/String;I)V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 11

    .prologue
    const/4 v5, 0x0

    .line 195
    iget-object v0, p0, Lcom/peel/ui/mw;->a:Lcom/peel/ui/mo;

    invoke-static {v0}, Lcom/peel/ui/mo;->c(Lcom/peel/ui/mo;)Landroid/widget/EditText;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 196
    new-instance v10, Ljava/lang/StringBuffer;

    iget-object v0, p0, Lcom/peel/ui/mw;->a:Lcom/peel/ui/mo;

    invoke-static {v0}, Lcom/peel/ui/mo;->c(Lcom/peel/ui/mo;)Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-direct {v10, v0}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/CharSequence;)V

    .line 198
    invoke-virtual {v10}, Ljava/lang/StringBuffer;->length()I

    move-result v0

    iget-object v1, p0, Lcom/peel/ui/mw;->b:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/2addr v0, v1

    const/16 v1, 0x8c

    if-le v0, v1, :cond_0

    .line 210
    :goto_0
    return-void

    .line 200
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/ui/mw;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v10, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 202
    invoke-static {}, Lcom/peel/util/a/f;->a()Lcom/peel/util/a/f;

    move-result-object v0

    sget-object v1, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v1}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v1

    if-nez v1, :cond_2

    const/4 v1, 0x1

    :goto_1
    const/16 v2, 0x4c5

    iget-object v3, p0, Lcom/peel/ui/mw;->a:Lcom/peel/ui/mo;

    .line 203
    invoke-static {v3}, Lcom/peel/ui/mo;->d(Lcom/peel/ui/mo;)I

    move-result v3

    iget-object v4, p0, Lcom/peel/ui/mw;->b:Ljava/lang/String;

    iget-object v6, p0, Lcom/peel/ui/mw;->a:Lcom/peel/ui/mo;

    invoke-static {v6}, Lcom/peel/ui/mo;->e(Lcom/peel/ui/mo;)Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lcom/peel/ui/mw;->a:Lcom/peel/ui/mo;

    invoke-static {v7}, Lcom/peel/ui/mo;->a(Lcom/peel/ui/mo;)Ljava/lang/String;

    move-result-object v8

    move v7, v5

    move v9, v5

    .line 202
    invoke-virtual/range {v0 .. v9}, Lcom/peel/util/a/f;->a(IIILjava/lang/String;ILjava/lang/String;ILjava/lang/String;I)V

    .line 205
    iget-object v0, p0, Lcom/peel/ui/mw;->a:Lcom/peel/ui/mo;

    invoke-static {v0}, Lcom/peel/ui/mo;->c(Lcom/peel/ui/mo;)Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v10}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 208
    :cond_1
    iget-object v0, p0, Lcom/peel/ui/mw;->a:Lcom/peel/ui/mo;

    invoke-static {v0}, Lcom/peel/ui/mo;->f(Lcom/peel/ui/mo;)Ljava/util/List;

    move-result-object v0

    iget v1, p0, Lcom/peel/ui/mw;->c:I

    invoke-interface {v0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 209
    iget-object v0, p0, Lcom/peel/ui/mw;->a:Lcom/peel/ui/mo;

    invoke-static {v0}, Lcom/peel/ui/mo;->h(Lcom/peel/ui/mo;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/ui/mw;->a:Lcom/peel/ui/mo;

    invoke-static {v1}, Lcom/peel/ui/mo;->g(Lcom/peel/ui/mo;)Landroid/text/SpannableStringBuilder;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 202
    :cond_2
    sget-object v1, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v1}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/data/at;->f()I

    move-result v1

    goto :goto_1
.end method
