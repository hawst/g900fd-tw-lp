.class Lcom/peel/ui/lt;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Z

.field final synthetic b:Lcom/peel/ui/lq;


# direct methods
.method constructor <init>(Lcom/peel/ui/lq;Z)V
    .locals 0

    .prologue
    .line 753
    iput-object p1, p0, Lcom/peel/ui/lt;->b:Lcom/peel/ui/lq;

    iput-boolean p2, p0, Lcom/peel/ui/lt;->a:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v2, 0x1

    .line 756
    const/4 v1, 0x0

    .line 757
    iget-object v0, p0, Lcom/peel/ui/lt;->b:Lcom/peel/ui/lq;

    invoke-static {v0}, Lcom/peel/ui/lq;->b(Lcom/peel/ui/lq;)Lcom/peel/widget/CustomViewPager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/widget/CustomViewPager;->getAdapter()Landroid/support/v4/view/av;

    move-result-object v0

    check-cast v0, Lcom/peel/ui/ds;

    iget-object v3, p0, Lcom/peel/ui/lt;->b:Lcom/peel/ui/lq;

    invoke-static {v3}, Lcom/peel/ui/lq;->d(Lcom/peel/ui/lq;)I

    move-result v3

    invoke-virtual {v0, v3}, Lcom/peel/ui/ds;->d(I)Lcom/peel/d/u;

    move-result-object v0

    .line 759
    instance-of v3, v0, Lcom/peel/ui/kr;

    if-eqz v3, :cond_1

    .line 760
    const/16 v0, 0x7d0

    .line 775
    :goto_0
    invoke-static {}, Lcom/peel/util/a/f;->a()Lcom/peel/util/a/f;

    move-result-object v3

    sget-object v1, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v1}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v1

    if-nez v1, :cond_3

    move v1, v2

    :goto_1
    const/16 v4, 0x51e

    invoke-virtual {v3, v1, v4, v0}, Lcom/peel/util/a/f;->a(III)V

    .line 778
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 779
    iget-boolean v1, p0, Lcom/peel/ui/lt;->a:Z

    if-eqz v1, :cond_4

    .line 780
    const-string/jumbo v1, "from_tutorial"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 781
    iget-object v1, p0, Lcom/peel/ui/lt;->b:Lcom/peel/ui/lq;

    invoke-virtual {v1}, Lcom/peel/ui/lq;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    const-class v2, Lcom/peel/i/gm;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lcom/peel/d/e;->c(Landroid/support/v4/app/ae;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 782
    iget-object v0, p0, Lcom/peel/ui/lt;->b:Lcom/peel/ui/lq;

    invoke-static {v0}, Lcom/peel/ui/lq;->t(Lcom/peel/ui/lq;)Landroid/widget/RelativeLayout;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/widget/RelativeLayout;->removeView(Landroid/view/View;)V

    .line 783
    iget-object v0, p0, Lcom/peel/ui/lt;->b:Lcom/peel/ui/lq;

    invoke-static {v0}, Lcom/peel/ui/lq;->t(Lcom/peel/ui/lq;)Landroid/widget/RelativeLayout;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 795
    :cond_0
    :goto_2
    return-void

    .line 761
    :cond_1
    instance-of v0, v0, Lcom/peel/ui/bw;

    if-eqz v0, :cond_2

    .line 762
    const-string/jumbo v0, "tvshows"

    .line 763
    iget-object v0, p0, Lcom/peel/ui/lt;->b:Lcom/peel/ui/lq;

    invoke-static {v0}, Lcom/peel/ui/lq;->d(Lcom/peel/ui/lq;)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :cond_2
    move v0, v1

    goto :goto_0

    .line 765
    :pswitch_0
    const/16 v0, 0x7d3

    .line 766
    goto :goto_0

    .line 768
    :pswitch_1
    const/16 v0, 0x7d2

    .line 769
    goto :goto_0

    .line 771
    :pswitch_2
    const/16 v0, 0x7d1

    goto :goto_0

    .line 775
    :cond_3
    sget-object v1, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v1}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/data/at;->f()I

    move-result v1

    goto :goto_1

    .line 785
    :cond_4
    invoke-static {}, Lcom/peel/content/a;->a()Lcom/peel/data/ContentRoom;

    move-result-object v1

    .line 786
    const-string/jumbo v2, "live"

    invoke-static {v2}, Lcom/peel/content/a;->c(Ljava/lang/String;)Lcom/peel/content/library/Library;

    move-result-object v2

    .line 788
    if-eqz v1, :cond_0

    if-eqz v2, :cond_0

    .line 790
    const-string/jumbo v3, "room"

    invoke-virtual {v0, v3, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 791
    const-string/jumbo v1, "library"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 792
    iget-object v1, p0, Lcom/peel/ui/lt;->b:Lcom/peel/ui/lq;

    invoke-virtual {v1}, Lcom/peel/ui/lq;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    const-class v2, Lcom/peel/h/a/bb;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lcom/peel/d/e;->b(Landroid/support/v4/app/ae;Ljava/lang/String;Landroid/os/Bundle;)V

    goto :goto_2

    .line 763
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
