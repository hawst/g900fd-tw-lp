.class public Lcom/peel/ui/aa;
.super Landroid/widget/ArrayAdapter;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lcom/peel/data/Channel;",
        ">;"
    }
.end annotation


# static fields
.field private static final b:Ljava/lang/String;


# instance fields
.field final a:Landroid/content/Context;

.field private c:Landroid/view/LayoutInflater;

.field private d:Ljava/lang/String;

.field private e:Lcom/peel/data/ContentRoom;

.field private f:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/peel/data/Channel;",
            ">;"
        }
    .end annotation
.end field

.field private g:[Z

.field private h:Z

.field private i:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/peel/data/Channel;",
            ">;"
        }
    .end annotation
.end field

.field private j:Landroid/widget/Filter;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 33
    const-class v0, Lcom/peel/ui/aa;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/peel/ui/aa;->b:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;ILjava/util/List;Ljava/lang/String;Lcom/peel/data/ContentRoom;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Ljava/util/List",
            "<",
            "Lcom/peel/data/Channel;",
            ">;",
            "Ljava/lang/String;",
            "Lcom/peel/data/ContentRoom;",
            ")V"
        }
    .end annotation

    .prologue
    .line 53
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 38
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/peel/ui/aa;->f:Ljava/util/Set;

    .line 41
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/peel/ui/aa;->h:Z

    .line 42
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/peel/ui/aa;->i:Ljava/util/List;

    .line 54
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/ui/aa;->c:Landroid/view/LayoutInflater;

    .line 55
    iput-object p4, p0, Lcom/peel/ui/aa;->d:Ljava/lang/String;

    .line 56
    iput-object p5, p0, Lcom/peel/ui/aa;->e:Lcom/peel/data/ContentRoom;

    .line 58
    iput-object p1, p0, Lcom/peel/ui/aa;->a:Landroid/content/Context;

    .line 59
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 61
    invoke-interface {p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/data/Channel;

    .line 62
    invoke-virtual {v0}, Lcom/peel/data/Channel;->k()Z

    move-result v3

    if-nez v3, :cond_1

    invoke-virtual {v0}, Lcom/peel/data/Channel;->c()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 64
    iget-object v3, p0, Lcom/peel/ui/aa;->i:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 65
    :cond_1
    invoke-virtual {v0}, Lcom/peel/data/Channel;->k()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 66
    invoke-virtual {v0}, Lcom/peel/data/Channel;->c()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 70
    :cond_2
    iget-object v0, p0, Lcom/peel/ui/aa;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Z

    iput-object v0, p0, Lcom/peel/ui/aa;->g:[Z

    .line 71
    return-void
.end method

.method static synthetic a(Lcom/peel/ui/aa;I)Lcom/peel/data/Channel;
    .locals 1

    .prologue
    .line 32
    invoke-direct {p0, p1}, Lcom/peel/ui/aa;->b(I)Lcom/peel/data/Channel;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/peel/ui/aa;Ljava/util/List;)Ljava/util/List;
    .locals 0

    .prologue
    .line 32
    iput-object p1, p0, Lcom/peel/ui/aa;->i:Ljava/util/List;

    return-object p1
.end method

.method static synthetic a(Lcom/peel/ui/aa;)Ljava/util/Set;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/peel/ui/aa;->f:Ljava/util/Set;

    return-object v0
.end method

.method static synthetic a(Lcom/peel/ui/aa;Z)Z
    .locals 0

    .prologue
    .line 32
    iput-boolean p1, p0, Lcom/peel/ui/aa;->h:Z

    return p1
.end method

.method static synthetic a(Lcom/peel/ui/aa;[Z)[Z
    .locals 0

    .prologue
    .line 32
    iput-object p1, p0, Lcom/peel/ui/aa;->g:[Z

    return-object p1
.end method

.method static synthetic b(Lcom/peel/ui/aa;)I
    .locals 1

    .prologue
    .line 32
    invoke-direct {p0}, Lcom/peel/ui/aa;->d()I

    move-result v0

    return v0
.end method

.method private b(I)Lcom/peel/data/Channel;
    .locals 1

    .prologue
    .line 46
    invoke-super {p0, p1}, Landroid/widget/ArrayAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/data/Channel;

    return-object v0
.end method

.method static synthetic c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 32
    sget-object v0, Lcom/peel/ui/aa;->b:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic c(Lcom/peel/ui/aa;)Z
    .locals 1

    .prologue
    .line 32
    iget-boolean v0, p0, Lcom/peel/ui/aa;->h:Z

    return v0
.end method

.method private d()I
    .locals 1

    .prologue
    .line 45
    invoke-super {p0}, Landroid/widget/ArrayAdapter;->getCount()I

    move-result v0

    return v0
.end method

.method static synthetic d(Lcom/peel/ui/aa;)Ljava/util/List;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/peel/ui/aa;->i:Ljava/util/List;

    return-object v0
.end method

.method static synthetic e(Lcom/peel/ui/aa;)[Z
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/peel/ui/aa;->g:[Z

    return-object v0
.end method


# virtual methods
.method public a(I)Lcom/peel/data/Channel;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/peel/ui/aa;->i:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/data/Channel;

    return-object v0
.end method

.method public a(ILandroid/view/View;)V
    .locals 2

    .prologue
    .line 74
    iget-object v0, p0, Lcom/peel/ui/aa;->i:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/data/Channel;

    .line 75
    iget-object v1, p0, Lcom/peel/ui/aa;->g:[Z

    aget-boolean v1, v1, p1

    if-eqz v1, :cond_0

    .line 76
    iget-object v1, p0, Lcom/peel/ui/aa;->f:Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 78
    iget-object v0, p0, Lcom/peel/ui/aa;->g:[Z

    const/4 v1, 0x0

    aput-boolean v1, v0, p1

    .line 80
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/ui/ad;

    iget-object v0, v0, Lcom/peel/ui/ad;->b:Landroid/widget/ImageView;

    sget v1, Lcom/peel/ui/fo;->btn_checkbox_off_states:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 90
    :goto_0
    return-void

    .line 83
    :cond_0
    iget-object v1, p0, Lcom/peel/ui/aa;->f:Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 85
    iget-object v0, p0, Lcom/peel/ui/aa;->g:[Z

    const/4 v1, 0x1

    aput-boolean v1, v0, p1

    .line 87
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/ui/ad;

    iget-object v0, v0, Lcom/peel/ui/ad;->b:Landroid/widget/ImageView;

    sget v1, Lcom/peel/ui/fo;->btn_checkbox_on_states:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0
.end method

.method public a()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 93
    iget-object v1, p0, Lcom/peel/ui/aa;->f:Ljava/util/Set;

    if-eqz v1, :cond_0

    .line 94
    iget-object v1, p0, Lcom/peel/ui/aa;->f:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 95
    const/4 v0, 0x1

    .line 99
    :cond_0
    return v0
.end method

.method public b()V
    .locals 7

    .prologue
    const/16 v6, 0x7d4

    .line 104
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 105
    iget-object v0, p0, Lcom/peel/ui/aa;->f:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/data/Channel;

    .line 106
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 107
    const-string/jumbo v2, "channel"

    invoke-virtual {v0}, Lcom/peel/data/Channel;->a()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v2, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 108
    const-string/jumbo v2, "library"

    iget-object v5, p0, Lcom/peel/ui/aa;->d:Ljava/lang/String;

    invoke-virtual {v1, v2, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 109
    const-string/jumbo v2, "room"

    iget-object v5, p0, Lcom/peel/ui/aa;->e:Lcom/peel/data/ContentRoom;

    invoke-virtual {v1, v2, v5}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 110
    const-string/jumbo v2, "path"

    const-string/jumbo v5, "channel/fav"

    invoke-virtual {v1, v2, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 111
    const-string/jumbo v2, "context_id"

    invoke-virtual {v1, v2, v6}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 112
    const-string/jumbo v2, "prgsvcid"

    invoke-virtual {v0}, Lcom/peel/data/Channel;->c()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v2, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 113
    const-string/jumbo v2, "logo"

    invoke-virtual {v0}, Lcom/peel/data/Channel;->f()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v2, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 114
    const-string/jumbo v2, "type"

    invoke-virtual {v0}, Lcom/peel/data/Channel;->i()I

    move-result v5

    invoke-virtual {v1, v2, v5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 115
    const-string/jumbo v2, "channelNumber"

    invoke-virtual {v0}, Lcom/peel/data/Channel;->e()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v2, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 116
    const-string/jumbo v2, "name"

    invoke-virtual {v0}, Lcom/peel/data/Channel;->d()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v2, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 117
    sget-object v2, Lcom/peel/content/a;->f:Lcom/peel/content/user/User;

    const/4 v5, 0x0

    invoke-virtual {v2, v1, v5}, Lcom/peel/content/user/User;->a(Landroid/os/Bundle;Lcom/peel/util/t;)V

    .line 118
    const/4 v1, 0x0

    move v2, v1

    :goto_1
    iget-object v1, p0, Lcom/peel/ui/aa;->g:[Z

    array-length v1, v1

    if-ge v2, v1, :cond_1

    .line 119
    iget-object v1, p0, Lcom/peel/ui/aa;->i:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/peel/data/Channel;

    invoke-virtual {v1}, Lcom/peel/data/Channel;->k()Z

    move-result v1

    iget-object v5, p0, Lcom/peel/ui/aa;->g:[Z

    aget-boolean v5, v5, v2

    xor-int/2addr v1, v5

    if-eqz v1, :cond_0

    .line 120
    iget-object v1, p0, Lcom/peel/ui/aa;->i:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/peel/data/Channel;

    iget-object v5, p0, Lcom/peel/ui/aa;->g:[Z

    aget-boolean v5, v5, v2

    invoke-virtual {v1, v5}, Lcom/peel/data/Channel;->a(Z)V

    .line 118
    :cond_0
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_1

    .line 123
    :cond_1
    invoke-virtual {v0}, Lcom/peel/data/Channel;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_0

    .line 125
    :cond_2
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    if-lez v0, :cond_3

    .line 126
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->deleteCharAt(I)Ljava/lang/StringBuilder;

    .line 128
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 129
    const-string/jumbo v1, "library"

    iget-object v2, p0, Lcom/peel/ui/aa;->d:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 130
    sget-object v1, Lcom/peel/ui/aa;->b:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "Sending prgsvcids: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 131
    const-string/jumbo v1, "prgsvcids"

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 132
    const-string/jumbo v1, "user"

    sget-object v2, Lcom/peel/content/a;->f:Lcom/peel/content/user/User;

    invoke-virtual {v2}, Lcom/peel/content/user/User;->x()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 133
    const-string/jumbo v1, "context_id"

    invoke-virtual {v0, v1, v6}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 134
    const-string/jumbo v1, "path"

    const-string/jumbo v2, "channel/fav"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 135
    const-string/jumbo v1, "room"

    iget-object v2, p0, Lcom/peel/ui/aa;->e:Lcom/peel/data/ContentRoom;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 136
    new-instance v1, Lcom/peel/ui/ab;

    invoke-direct {v1, p0}, Lcom/peel/ui/ab;-><init>(Lcom/peel/ui/aa;)V

    invoke-static {v0, v1}, Lcom/peel/content/a/j;->b(Landroid/os/Bundle;Lcom/peel/util/t;)V

    .line 143
    :cond_3
    return-void
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/peel/ui/aa;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getFilter()Landroid/widget/Filter;
    .locals 1

    .prologue
    .line 186
    iget-object v0, p0, Lcom/peel/ui/aa;->j:Landroid/widget/Filter;

    if-nez v0, :cond_0

    .line 187
    new-instance v0, Lcom/peel/ui/ac;

    invoke-direct {v0, p0}, Lcom/peel/ui/ac;-><init>(Lcom/peel/ui/aa;)V

    iput-object v0, p0, Lcom/peel/ui/aa;->j:Landroid/widget/Filter;

    .line 256
    :cond_0
    iget-object v0, p0, Lcom/peel/ui/aa;->j:Landroid/widget/Filter;

    return-object v0
.end method

.method public synthetic getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 32
    invoke-virtual {p0, p1}, Lcom/peel/ui/aa;->a(I)Lcom/peel/data/Channel;

    move-result-object v0

    return-object v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 8

    .prologue
    const/4 v2, 0x0

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 147
    if-nez p2, :cond_0

    iget-object v0, p0, Lcom/peel/ui/aa;->c:Landroid/view/LayoutInflater;

    sget v1, Lcom/peel/ui/fq;->channel_search_row:I

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 149
    :cond_0
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_2

    .line 150
    new-instance v1, Lcom/peel/ui/ad;

    invoke-direct {v1, p0}, Lcom/peel/ui/ad;-><init>(Lcom/peel/ui/aa;)V

    .line 151
    sget v0, Lcom/peel/ui/fp;->icon:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, v1, Lcom/peel/ui/ad;->a:Landroid/widget/ImageView;

    .line 152
    sget v0, Lcom/peel/ui/fp;->indicator:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, v1, Lcom/peel/ui/ad;->b:Landroid/widget/ImageView;

    .line 153
    sget v0, Lcom/peel/ui/fp;->name:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lcom/peel/ui/ad;->c:Landroid/widget/TextView;

    .line 154
    sget v0, Lcom/peel/ui/fp;->channel:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lcom/peel/ui/ad;->d:Landroid/widget/TextView;

    .line 155
    sget v0, Lcom/peel/ui/fp;->channelnumber:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lcom/peel/ui/ad;->e:Landroid/widget/TextView;

    .line 156
    invoke-virtual {p2, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    move-object v0, v1

    .line 161
    :goto_0
    iput-boolean v7, v0, Lcom/peel/ui/ad;->f:Z

    .line 163
    invoke-virtual {p0, p1}, Lcom/peel/ui/aa;->a(I)Lcom/peel/data/Channel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/data/Channel;->f()Ljava/lang/String;

    move-result-object v1

    .line 164
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_1

    move-object v1, v2

    .line 168
    :cond_1
    iget-object v2, p0, Lcom/peel/ui/aa;->a:Landroid/content/Context;

    invoke-static {v2}, Lcom/peel/util/b/e;->a(Landroid/content/Context;)Lcom/squareup/picasso/Picasso;

    move-result-object v2

    .line 169
    invoke-virtual {v2, v1}, Lcom/squareup/picasso/Picasso;->load(Ljava/lang/String;)Lcom/squareup/picasso/RequestCreator;

    move-result-object v1

    sget v2, Lcom/peel/ui/fo;->btn_noitem_list:I

    .line 170
    invoke-virtual {v1, v2}, Lcom/squareup/picasso/RequestCreator;->placeholder(I)Lcom/squareup/picasso/RequestCreator;

    move-result-object v1

    iget-object v2, v0, Lcom/peel/ui/ad;->a:Landroid/widget/ImageView;

    .line 171
    invoke-virtual {v1, v2}, Lcom/squareup/picasso/RequestCreator;->into(Landroid/widget/ImageView;)V

    .line 173
    invoke-virtual {p0, p1}, Lcom/peel/ui/aa;->a(I)Lcom/peel/data/Channel;

    move-result-object v1

    .line 174
    iget-object v2, v0, Lcom/peel/ui/ad;->c:Landroid/widget/TextView;

    invoke-virtual {p0, p1}, Lcom/peel/ui/aa;->a(I)Lcom/peel/data/Channel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/peel/data/Channel;->d()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 175
    iget-object v2, v0, Lcom/peel/ui/ad;->d:Landroid/widget/TextView;

    const-string/jumbo v3, "%s\u2014%s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    invoke-virtual {v1}, Lcom/peel/data/Channel;->b()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-virtual {v1}, Lcom/peel/data/Channel;->m()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v7

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 176
    iget-object v2, v0, Lcom/peel/ui/ad;->e:Landroid/widget/TextView;

    invoke-virtual {v1}, Lcom/peel/data/Channel;->m()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 177
    iget-object v2, v0, Lcom/peel/ui/ad;->b:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/peel/ui/aa;->g:[Z

    aget-boolean v1, v1, p1

    if-eqz v1, :cond_3

    sget v1, Lcom/peel/ui/fo;->btn_checkbox_on_states:I

    :goto_1
    invoke-virtual {v2, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 179
    iget-object v0, v0, Lcom/peel/ui/ad;->b:Landroid/widget/ImageView;

    invoke-virtual {v0, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 181
    return-object p2

    .line 158
    :cond_2
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/ui/ad;

    goto :goto_0

    .line 177
    :cond_3
    sget v1, Lcom/peel/ui/fo;->btn_checkbox_off_states:I

    goto :goto_1
.end method
