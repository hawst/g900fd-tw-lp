.class Lcom/peel/ui/dv;
.super Landroid/support/v4/view/cw;


# instance fields
.field final synthetic a:Lcom/peel/ui/du;


# direct methods
.method constructor <init>(Lcom/peel/ui/du;)V
    .locals 0

    .prologue
    .line 84
    iput-object p1, p0, Lcom/peel/ui/dv;->a:Lcom/peel/ui/du;

    invoke-direct {p0}, Landroid/support/v4/view/cw;-><init>()V

    return-void
.end method


# virtual methods
.method public a(I)V
    .locals 2

    .prologue
    .line 86
    iget-object v0, p0, Lcom/peel/ui/dv;->a:Lcom/peel/ui/du;

    invoke-static {v0, p1}, Lcom/peel/ui/du;->a(Lcom/peel/ui/du;I)I

    .line 87
    iget-object v0, p0, Lcom/peel/ui/dv;->a:Lcom/peel/ui/du;

    invoke-static {v0}, Lcom/peel/ui/du;->b(Lcom/peel/ui/du;)Landroid/support/v4/view/ViewPager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->getAdapter()Landroid/support/v4/view/av;

    move-result-object v0

    check-cast v0, Lcom/peel/ui/ek;

    iget-object v1, p0, Lcom/peel/ui/dv;->a:Lcom/peel/ui/du;

    invoke-static {v1}, Lcom/peel/ui/du;->a(Lcom/peel/ui/du;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/peel/ui/ek;->a(I)V

    .line 88
    iget-object v0, p0, Lcom/peel/ui/dv;->a:Lcom/peel/ui/du;

    iget-object v0, v0, Lcom/peel/ui/du;->b:Lcom/peel/d/i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/peel/ui/dv;->a:Lcom/peel/ui/du;

    iget-object v1, v0, Lcom/peel/ui/du;->b:Lcom/peel/d/i;

    iget-object v0, p0, Lcom/peel/ui/dv;->a:Lcom/peel/ui/du;

    invoke-static {v0}, Lcom/peel/ui/du;->a(Lcom/peel/ui/du;)I

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Lcom/peel/d/i;->a(Z)V

    .line 90
    :cond_0
    return-void

    .line 88
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(I)V
    .locals 6

    .prologue
    const/4 v2, 0x1

    .line 93
    if-ne v2, p1, :cond_0

    iget-object v0, p0, Lcom/peel/ui/dv;->a:Lcom/peel/ui/du;

    invoke-static {v0}, Lcom/peel/ui/du;->b(Lcom/peel/ui/du;)Landroid/support/v4/view/ViewPager;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 94
    iget-object v0, p0, Lcom/peel/ui/dv;->a:Lcom/peel/ui/du;

    invoke-static {v0}, Lcom/peel/ui/du;->b(Lcom/peel/ui/du;)Landroid/support/v4/view/ViewPager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->getAdapter()Landroid/support/v4/view/av;

    move-result-object v0

    check-cast v0, Lcom/peel/ui/ek;

    .line 95
    iget-object v1, p0, Lcom/peel/ui/dv;->a:Lcom/peel/ui/du;

    invoke-static {v1, v2}, Lcom/peel/ui/du;->a(Lcom/peel/ui/du;Z)Z

    .line 96
    if-eqz v0, :cond_0

    .line 97
    invoke-virtual {v0}, Lcom/peel/ui/ek;->e()Ljava/util/ArrayList;

    move-result-object v1

    .line 98
    iget-object v0, p0, Lcom/peel/ui/dv;->a:Lcom/peel/ui/du;

    invoke-static {v0}, Lcom/peel/ui/du;->a(Lcom/peel/ui/du;)I

    move-result v0

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 99
    iget-object v0, p0, Lcom/peel/ui/dv;->a:Lcom/peel/ui/du;

    invoke-static {v0}, Lcom/peel/ui/du;->a(Lcom/peel/ui/du;)I

    move-result v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 100
    if-nez v0, :cond_1

    .line 118
    :cond_0
    :goto_0
    return-void

    .line 102
    :cond_1
    sget v2, Lcom/peel/ui/fp;->epg_list:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    .line 103
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/widget/ListView;->getChildCount()I

    move-result v2

    if-eqz v2, :cond_0

    .line 106
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getTop()I

    move-result v2

    .line 107
    invoke-virtual {v0}, Landroid/widget/ListView;->getFirstVisiblePosition()I

    move-result v3

    .line 108
    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 109
    if-eqz v0, :cond_2

    .line 110
    sget v4, Lcom/peel/ui/fp;->epg_list:I

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    invoke-virtual {v0, v3, v2}, Landroid/widget/ListView;->setSelectionFromTop(II)V

    .line 111
    iget-object v0, p0, Lcom/peel/ui/dv;->a:Lcom/peel/ui/du;

    invoke-static {v0}, Lcom/peel/ui/du;->c(Lcom/peel/ui/du;)Ljava/lang/String;

    move-result-object v0

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "force selection 0 from visible: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, " top: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 113
    :cond_3
    iget-object v0, p0, Lcom/peel/ui/dv;->a:Lcom/peel/ui/du;

    iget-object v0, v0, Lcom/peel/ui/du;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "top"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 114
    iget-object v0, p0, Lcom/peel/ui/dv;->a:Lcom/peel/ui/du;

    iget-object v0, v0, Lcom/peel/ui/du;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "first_visible"

    invoke-virtual {v0, v1, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    goto :goto_0
.end method
