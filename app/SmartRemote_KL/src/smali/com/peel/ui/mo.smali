.class public Lcom/peel/ui/mo;
.super Lcom/peel/d/u;


# static fields
.field private static final aB:[Ljava/lang/String;


# instance fields
.field private aA:Ljava/lang/String;

.field private aj:Landroid/widget/ImageView;

.field private ak:Landroid/widget/ImageView;

.field private al:Landroid/widget/ImageView;

.field private am:Landroid/widget/ImageView;

.field private an:Landroid/widget/TextView;

.field private ao:Landroid/widget/TextView;

.field private ap:Landroid/widget/TextView;

.field private aq:Landroid/widget/EditText;

.field private ar:Landroid/view/View;

.field private as:Landroid/graphics/Bitmap;

.field private at:Ljava/io/File;

.field private au:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private av:I

.field private aw:Landroid/view/Menu;

.field private ax:I

.field private ay:Ljava/lang/String;

.field private az:Ljava/lang/String;

.field private e:Lcom/peel/d/v;

.field private f:Lcom/peel/d/af;

.field private g:[Ljava/lang/String;

.field private h:Ljava/lang/String;

.field private i:Landroid/net/Uri;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 82
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string/jumbo v2, "viggle"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string/jumbo v2, "tvtag"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string/jumbo v2, "getglue"

    aput-object v2, v0, v1

    sput-object v0, Lcom/peel/ui/mo;->aB:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 58
    invoke-direct {p0}, Lcom/peel/d/u;-><init>()V

    .line 77
    const/16 v0, 0x8c

    iput v0, p0, Lcom/peel/ui/mo;->av:I

    .line 180
    return-void
.end method

.method private S()I
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v3, 0x0

    .line 321
    invoke-virtual {p0}, Lcom/peel/ui/mo;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/ae;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/ui/mo;->i:Landroid/net/Uri;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const-string/jumbo v4, "orientation"

    aput-object v4, v2, v6

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 322
    if-eqz v0, :cond_0

    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 323
    invoke-interface {v0, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    .line 325
    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method static synthetic a(Lcom/peel/ui/mo;I)I
    .locals 0

    .prologue
    .line 58
    iput p1, p0, Lcom/peel/ui/mo;->av:I

    return p1
.end method

.method private a(II)Landroid/graphics/Bitmap;
    .locals 10

    .prologue
    const/16 v9, 0x10e

    const/16 v8, 0x5a

    const/4 v1, 0x0

    const/4 v3, 0x0

    const/4 v6, 0x1

    .line 330
    new-instance v5, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v5}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 331
    iput-boolean v6, v5, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 335
    :try_start_0
    invoke-virtual {p0}, Lcom/peel/ui/mo;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/ae;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v2, p0, Lcom/peel/ui/mo;->i:Landroid/net/Uri;

    invoke-virtual {v0, v2}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 340
    :goto_0
    if-nez v2, :cond_1

    move-object v0, v3

    .line 382
    :cond_0
    :goto_1
    return-object v0

    .line 336
    :catch_0
    move-exception v0

    .line 337
    invoke-virtual {v0}, Ljava/io/FileNotFoundException;->printStackTrace()V

    move-object v2, v3

    goto :goto_0

    .line 342
    :cond_1
    invoke-static {v2, v3, v5}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 344
    :try_start_1
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    .line 349
    :goto_2
    invoke-direct {p0}, Lcom/peel/ui/mo;->S()I

    move-result v7

    .line 350
    if-eq v7, v8, :cond_2

    if-ne v7, v9, :cond_5

    :cond_2
    iget v0, v5, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    move v4, v0

    .line 351
    :goto_3
    if-eq v7, v8, :cond_3

    if-ne v7, v9, :cond_6

    :cond_3
    iget v0, v5, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    .line 354
    :goto_4
    if-gtz p1, :cond_4

    if-lez p2, :cond_7

    .line 355
    :cond_4
    div-int/2addr v4, p1

    div-int/2addr v0, p2

    invoke-static {v4, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 358
    :goto_5
    new-instance v4, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v4}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 359
    iput-boolean v1, v4, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 360
    iput v0, v4, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 361
    iput-boolean v6, v4, Landroid/graphics/BitmapFactory$Options;->inPurgeable:Z

    .line 364
    :try_start_2
    invoke-virtual {p0}, Lcom/peel/ui/mo;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/ae;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v5, p0, Lcom/peel/ui/mo;->i:Landroid/net/Uri;

    invoke-virtual {v0, v5}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_2

    move-result-object v0

    move-object v2, v0

    .line 369
    :goto_6
    invoke-static {v2, v3, v4}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 371
    :try_start_3
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_3

    .line 376
    :goto_7
    if-lez v7, :cond_0

    .line 377
    new-instance v5, Landroid/graphics/Matrix;

    invoke-direct {v5}, Landroid/graphics/Matrix;-><init>()V

    .line 378
    int-to-float v2, v7

    invoke-virtual {v5, v2}, Landroid/graphics/Matrix;->postRotate(F)Z

    .line 379
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    move v2, v1

    invoke-static/range {v0 .. v6}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_1

    .line 345
    :catch_1
    move-exception v0

    .line 346
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2

    .line 350
    :cond_5
    iget v0, v5, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    move v4, v0

    goto :goto_3

    .line 351
    :cond_6
    iget v0, v5, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    goto :goto_4

    .line 365
    :catch_2
    move-exception v0

    .line 366
    invoke-virtual {v0}, Ljava/io/FileNotFoundException;->printStackTrace()V

    goto :goto_6

    .line 372
    :catch_3
    move-exception v2

    .line 373
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_7

    :cond_7
    move v0, v6

    goto :goto_5
.end method

.method static synthetic a(Lcom/peel/ui/mo;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/peel/ui/mo;->ay:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic a(Lcom/peel/ui/mo;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 58
    iput-object p1, p0, Lcom/peel/ui/mo;->aA:Ljava/lang/String;

    return-object p1
.end method

.method private a(Landroid/text/SpannableStringBuilder;Landroid/text/style/ClickableSpan;II)V
    .locals 1

    .prologue
    .line 177
    const/16 v0, 0x12

    invoke-virtual {p1, p2, p3, p4, v0}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 178
    return-void
.end method

.method static synthetic b(Lcom/peel/ui/mo;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/peel/ui/mo;->a:Ljava/lang/String;

    return-object v0
.end method

.method private c()Landroid/text/SpannableStringBuilder;
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 214
    new-instance v3, Landroid/text/SpannableStringBuilder;

    invoke-direct {v3}, Landroid/text/SpannableStringBuilder;-><init>()V

    move v1, v0

    move v2, v0

    .line 217
    :goto_0
    iget-object v0, p0, Lcom/peel/ui/mo;->au:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    const/16 v0, 0xa

    if-ge v1, v0, :cond_0

    .line 218
    iget-object v0, p0, Lcom/peel/ui/mo;->au:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 219
    new-instance v4, Lcom/peel/ui/mw;

    const/4 v5, 0x0

    invoke-direct {v4, p0, v0, v1, v5}, Lcom/peel/ui/mw;-><init>(Lcom/peel/ui/mo;Ljava/lang/String;ILcom/peel/ui/mp;)V

    .line 220
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 221
    invoke-virtual {v3}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    invoke-direct {p0, v3, v4, v2, v5}, Lcom/peel/ui/mo;->a(Landroid/text/SpannableStringBuilder;Landroid/text/style/ClickableSpan;II)V

    .line 222
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    add-int/2addr v0, v2

    add-int/lit8 v2, v0, 0x1

    .line 217
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 225
    :cond_0
    return-object v3
.end method

.method static synthetic c(Lcom/peel/ui/mo;)Landroid/widget/EditText;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/peel/ui/mo;->aq:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic d(Lcom/peel/ui/mo;)I
    .locals 1

    .prologue
    .line 58
    iget v0, p0, Lcom/peel/ui/mo;->ax:I

    return v0
.end method

.method static synthetic e(Lcom/peel/ui/mo;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/peel/ui/mo;->az:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic f(Lcom/peel/ui/mo;)Ljava/util/List;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/peel/ui/mo;->au:Ljava/util/List;

    return-object v0
.end method

.method static synthetic g(Lcom/peel/ui/mo;)Landroid/text/SpannableStringBuilder;
    .locals 1

    .prologue
    .line 58
    invoke-direct {p0}, Lcom/peel/ui/mo;->c()Landroid/text/SpannableStringBuilder;

    move-result-object v0

    return-object v0
.end method

.method static synthetic h(Lcom/peel/ui/mo;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/peel/ui/mo;->ap:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic i(Lcom/peel/ui/mo;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/peel/ui/mo;->a:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic j(Lcom/peel/ui/mo;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/peel/ui/mo;->a:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public Z()V
    .locals 7

    .prologue
    .line 466
    iget-object v0, p0, Lcom/peel/ui/mo;->d:Lcom/peel/d/a;

    if-nez v0, :cond_0

    .line 467
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 468
    sget v0, Lcom/peel/ui/fp;->menu_send:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 469
    sget v0, Lcom/peel/ui/fp;->menu_text:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 470
    new-instance v0, Lcom/peel/d/a;

    sget-object v1, Lcom/peel/d/d;->b:Lcom/peel/d/d;

    sget-object v2, Lcom/peel/d/b;->a:Lcom/peel/d/b;

    sget-object v3, Lcom/peel/d/c;->b:Lcom/peel/d/c;

    iget-object v4, p0, Lcom/peel/ui/mo;->c:Landroid/os/Bundle;

    const-string/jumbo v6, "title"

    invoke-virtual {v4, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-direct/range {v0 .. v5}, Lcom/peel/d/a;-><init>(Lcom/peel/d/d;Lcom/peel/d/b;Lcom/peel/d/c;Ljava/lang/String;Ljava/util/List;)V

    iput-object v0, p0, Lcom/peel/ui/mo;->d:Lcom/peel/d/a;

    .line 472
    :cond_0
    iget-object v0, p0, Lcom/peel/ui/mo;->b:Lcom/peel/d/i;

    iget-object v1, p0, Lcom/peel/ui/mo;->d:Lcom/peel/d/a;

    invoke-virtual {v0, v1}, Lcom/peel/d/i;->a(Lcom/peel/d/a;)V

    .line 473
    return-void
.end method

.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    .line 230
    sget v0, Lcom/peel/ui/fq;->twitter_compose_layout:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 231
    sget v0, Lcom/peel/ui/fp;->user_pic:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/peel/ui/mo;->aj:Landroid/widget/ImageView;

    .line 232
    sget v0, Lcom/peel/ui/fp;->username:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/peel/ui/mo;->an:Landroid/widget/TextView;

    .line 233
    sget v0, Lcom/peel/ui/fp;->screen_name:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/peel/ui/mo;->ao:Landroid/widget/TextView;

    .line 234
    sget v0, Lcom/peel/ui/fp;->post_pic:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/peel/ui/mo;->ak:Landroid/widget/ImageView;

    .line 235
    sget v0, Lcom/peel/ui/fp;->gallery_icon:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/peel/ui/mo;->al:Landroid/widget/ImageView;

    .line 236
    sget v0, Lcom/peel/ui/fp;->camera_icon:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/peel/ui/mo;->am:Landroid/widget/ImageView;

    .line 238
    sget v0, Lcom/peel/ui/fp;->post_pic_container:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/ui/mo;->ar:Landroid/view/View;

    .line 239
    sget v0, Lcom/peel/ui/fp;->twitt_text:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/peel/ui/mo;->aq:Landroid/widget/EditText;

    .line 240
    sget v0, Lcom/peel/ui/fp;->hash_tags:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/peel/ui/mo;->ap:Landroid/widget/TextView;

    .line 242
    invoke-virtual {p0}, Lcom/peel/ui/mo;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    invoke-static {v0}, Lcom/peel/util/g;->a(Landroid/app/Activity;)V

    .line 243
    return-object v1
.end method

.method public a(IILandroid/content/Intent;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 296
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    if-eqz p3, :cond_0

    .line 297
    invoke-virtual {p3}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/ui/mo;->i:Landroid/net/Uri;

    .line 299
    iget-object v0, p0, Lcom/peel/ui/mo;->i:Landroid/net/Uri;

    if-eqz v0, :cond_0

    .line 300
    invoke-virtual {p0}, Lcom/peel/ui/mo;->n()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/peel/ui/fn;->twitt_image_width:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    invoke-virtual {p0}, Lcom/peel/ui/mo;->n()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    invoke-static {v3, v0, v1}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    float-to-int v0, v0

    .line 301
    invoke-virtual {p0}, Lcom/peel/ui/mo;->n()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/peel/ui/fn;->twitt_image_height:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    invoke-virtual {p0}, Lcom/peel/ui/mo;->n()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    invoke-static {v3, v1, v2}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v1

    float-to-int v1, v1

    .line 302
    invoke-direct {p0, v0, v1}, Lcom/peel/ui/mo;->a(II)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/ui/mo;->as:Landroid/graphics/Bitmap;

    .line 304
    iget-object v0, p0, Lcom/peel/ui/mo;->as:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    .line 305
    iget-object v0, p0, Lcom/peel/ui/mo;->ar:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 306
    iget-object v0, p0, Lcom/peel/ui/mo;->ak:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/peel/ui/mo;->as:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 308
    if-nez p1, :cond_1

    .line 309
    iget-object v0, p0, Lcom/peel/ui/mo;->al:Landroid/widget/ImageView;

    sget v1, Lcom/peel/ui/fo;->gallery_icon_press:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 310
    iget-object v0, p0, Lcom/peel/ui/mo;->am:Landroid/widget/ImageView;

    sget v1, Lcom/peel/ui/fo;->camera_icon_normal:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 318
    :cond_0
    :goto_0
    return-void

    .line 311
    :cond_1
    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    .line 312
    iget-object v0, p0, Lcom/peel/ui/mo;->am:Landroid/widget/ImageView;

    sget v1, Lcom/peel/ui/fo;->camera_icon_press:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 313
    iget-object v0, p0, Lcom/peel/ui/mo;->al:Landroid/widget/ImageView;

    sget v1, Lcom/peel/ui/fo;->gallery_icon_normal:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0
.end method

.method public a(Landroid/view/Menu;)V
    .locals 2

    .prologue
    .line 490
    sget v0, Lcom/peel/ui/fp;->menu_text:I

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0}, Landroid/view/MenuItem;->getActionView()Landroid/view/View;

    move-result-object v0

    .line 491
    iput-object p1, p0, Lcom/peel/ui/mo;->aw:Landroid/view/Menu;

    .line 492
    if-eqz v0, :cond_0

    .line 493
    check-cast v0, Landroid/widget/TextView;

    iget v1, p0, Lcom/peel/ui/mo;->av:I

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 495
    :cond_0
    return-void
.end method

.method public a(Landroid/view/MenuItem;)Z
    .locals 12

    .prologue
    const/4 v11, 0x1

    const/4 v2, 0x0

    .line 387
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    .line 388
    sget v1, Lcom/peel/ui/fp;->menu_send:I

    if-ne v0, v1, :cond_2

    .line 389
    invoke-interface {p1, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 390
    iget-object v0, p0, Lcom/peel/ui/mo;->aw:Landroid/view/Menu;

    if-eqz v0, :cond_0

    .line 391
    iget-object v0, p0, Lcom/peel/ui/mo;->aw:Landroid/view/Menu;

    sget v1, Lcom/peel/ui/fp;->menu_text:I

    invoke-interface {v0, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 392
    :cond_0
    const-class v0, Lcom/peel/ui/mo;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/peel/ui/mo;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    invoke-static {v0, v1, v11}, Lcom/peel/util/bx;->a(Ljava/lang/String;Landroid/app/Activity;Z)V

    .line 393
    iget-object v0, p0, Lcom/peel/ui/mo;->aq:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    .line 396
    iget-object v1, p0, Lcom/peel/ui/mo;->aA:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 397
    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v1

    iget-object v3, p0, Lcom/peel/ui/mo;->aA:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/2addr v1, v3

    add-int/lit8 v1, v1, 0x1

    const/16 v3, 0x8c

    if-gt v1, v3, :cond_1

    .line 398
    const-string/jumbo v1, " "

    invoke-interface {v0, v1}, Landroid/text/Editable;->append(Ljava/lang/CharSequence;)Landroid/text/Editable;

    move-result-object v1

    iget-object v3, p0, Lcom/peel/ui/mo;->aA:Ljava/lang/String;

    invoke-interface {v1, v3}, Landroid/text/Editable;->append(Ljava/lang/CharSequence;)Landroid/text/Editable;

    .line 402
    :cond_1
    if-nez v0, :cond_3

    iget-object v1, p0, Lcom/peel/ui/mo;->i:Landroid/net/Uri;

    if-nez v1, :cond_3

    .line 461
    :cond_2
    :goto_0
    return v11

    .line 403
    :cond_3
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    .line 405
    if-eqz v0, :cond_4

    .line 406
    const-string/jumbo v1, "status"

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 413
    :cond_4
    iget-object v0, p0, Lcom/peel/ui/mo;->i:Landroid/net/Uri;

    if-nez v0, :cond_5

    .line 414
    iget-object v0, p0, Lcom/peel/ui/mo;->e:Lcom/peel/d/v;

    const/4 v1, 0x2

    const/4 v3, 0x0

    new-instance v5, Lcom/peel/ui/ms;

    invoke-direct {v5, p0}, Lcom/peel/ui/ms;-><init>(Lcom/peel/ui/mo;)V

    invoke-virtual/range {v0 .. v5}, Lcom/peel/d/v;->a(IZLjava/lang/String;Ljava/util/HashMap;Lcom/peel/util/t;)V

    .line 456
    :goto_1
    invoke-static {}, Lcom/peel/util/a/f;->a()Lcom/peel/util/a/f;

    move-result-object v3

    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v0}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v0

    if-nez v0, :cond_6

    move v4, v11

    :goto_2
    const/16 v5, 0x4bf

    iget v6, p0, Lcom/peel/ui/mo;->ax:I

    iget-object v7, p0, Lcom/peel/ui/mo;->az:Ljava/lang/String;

    iget-object v9, p0, Lcom/peel/ui/mo;->ay:Ljava/lang/String;

    move v8, v2

    move v10, v2

    invoke-virtual/range {v3 .. v10}, Lcom/peel/util/a/f;->a(IIILjava/lang/String;ILjava/lang/String;I)V

    goto :goto_0

    .line 427
    :cond_5
    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v0

    .line 428
    new-instance v1, Ljava/io/File;

    const-string/jumbo v3, "temp.jpg"

    invoke-direct {v1, v0, v3}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v1, p0, Lcom/peel/ui/mo;->at:Ljava/io/File;

    .line 430
    :try_start_0
    new-instance v0, Ljava/io/FileOutputStream;

    iget-object v1, p0, Lcom/peel/ui/mo;->at:Ljava/io/File;

    invoke-direct {v0, v1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 431
    iget-object v1, p0, Lcom/peel/ui/mo;->as:Landroid/graphics/Bitmap;

    sget-object v3, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v5, 0x5a

    invoke-virtual {v1, v3, v5, v0}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 432
    invoke-virtual {v0}, Ljava/io/FileOutputStream;->flush()V

    .line 433
    invoke-virtual {v0}, Ljava/io/FileOutputStream;->close()V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 439
    :goto_3
    iget-object v0, p0, Lcom/peel/ui/mo;->as:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getByteCount()I

    move-result v0

    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 440
    iget-object v1, p0, Lcom/peel/ui/mo;->as:Landroid/graphics/Bitmap;

    invoke-virtual {v1, v0}, Landroid/graphics/Bitmap;->copyPixelsToBuffer(Ljava/nio/Buffer;)V

    .line 442
    iget-object v0, p0, Lcom/peel/ui/mo;->e:Lcom/peel/d/v;

    iget-object v1, p0, Lcom/peel/ui/mo;->at:Ljava/io/File;

    new-instance v3, Lcom/peel/ui/mu;

    invoke-direct {v3, p0}, Lcom/peel/ui/mu;-><init>(Lcom/peel/ui/mo;)V

    invoke-virtual {v0, v1, v4, v3}, Lcom/peel/d/v;->a(Ljava/io/File;Ljava/util/Map;Lcom/peel/util/t;)V

    goto :goto_1

    .line 434
    :catch_0
    move-exception v0

    .line 435
    invoke-virtual {v0}, Ljava/io/FileNotFoundException;->printStackTrace()V

    goto :goto_3

    .line 436
    :catch_1
    move-exception v0

    .line 437
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_3

    .line 456
    :cond_6
    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v0}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/data/at;->f()I

    move-result v4

    goto :goto_2
.end method

.method public d(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    const/4 v4, 0x0

    const/4 v1, 0x0

    .line 86
    invoke-super {p0, p1}, Lcom/peel/d/u;->d(Landroid/os/Bundle;)V

    .line 88
    iget-object v0, p0, Lcom/peel/ui/mo;->c:Landroid/os/Bundle;

    const-string/jumbo v2, "context_id"

    iget v3, p0, Lcom/peel/ui/mo;->ax:I

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/peel/ui/mo;->ax:I

    .line 89
    iget-object v0, p0, Lcom/peel/ui/mo;->c:Landroid/os/Bundle;

    const-string/jumbo v2, "showid"

    invoke-virtual {v0, v2, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/ui/mo;->ay:Ljava/lang/String;

    .line 90
    iget-object v0, p0, Lcom/peel/ui/mo;->c:Landroid/os/Bundle;

    const-string/jumbo v2, "tmsid"

    invoke-virtual {v0, v2, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/ui/mo;->az:Ljava/lang/String;

    .line 92
    iget-object v0, p0, Lcom/peel/ui/mo;->ay:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 93
    iget-object v0, p0, Lcom/peel/ui/mo;->a:Ljava/lang/String;

    const-string/jumbo v2, "get short url"

    new-instance v3, Lcom/peel/ui/mp;

    invoke-direct {v3, p0}, Lcom/peel/ui/mp;-><init>(Lcom/peel/ui/mo;)V

    invoke-static {v0, v2, v3}, Lcom/peel/util/i;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    .line 109
    :cond_0
    invoke-virtual {p0}, Lcom/peel/ui/mo;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    invoke-static {v0}, Lcom/peel/d/v;->a(Landroid/content/Context;)Lcom/peel/d/v;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/ui/mo;->e:Lcom/peel/d/v;

    .line 110
    iget-object v0, p0, Lcom/peel/ui/mo;->e:Lcom/peel/d/v;

    invoke-virtual {v0}, Lcom/peel/d/v;->b()Lcom/peel/d/af;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/ui/mo;->f:Lcom/peel/d/af;

    .line 112
    iget-object v0, p0, Lcom/peel/ui/mo;->f:Lcom/peel/d/af;

    invoke-virtual {v0}, Lcom/peel/d/af;->g()Ljava/lang/String;

    move-result-object v3

    .line 114
    iget-object v0, p0, Lcom/peel/ui/mo;->ao:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/peel/ui/mo;->f:Lcom/peel/d/af;

    invoke-virtual {v2}, Lcom/peel/d/af;->f()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 116
    iget-object v0, p0, Lcom/peel/ui/mo;->f:Lcom/peel/d/af;

    invoke-virtual {v0}, Lcom/peel/d/af;->e()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 117
    iget-object v0, p0, Lcom/peel/ui/mo;->an:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "@"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v4, p0, Lcom/peel/ui/mo;->f:Lcom/peel/d/af;

    invoke-virtual {v4}, Lcom/peel/d/af;->e()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 119
    :cond_1
    iget-object v0, p0, Lcom/peel/ui/mo;->c:Landroid/os/Bundle;

    const-string/jumbo v2, "keyword"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/ui/mo;->g:[Ljava/lang/String;

    .line 121
    iget-object v0, p0, Lcom/peel/ui/mo;->c:Landroid/os/Bundle;

    const-string/jumbo v2, "pre_text"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/ui/mo;->h:Ljava/lang/String;

    .line 122
    iget-object v0, p0, Lcom/peel/ui/mo;->aq:Landroid/widget/EditText;

    new-instance v2, Lcom/peel/ui/mr;

    invoke-direct {v2, p0}, Lcom/peel/ui/mr;-><init>(Lcom/peel/ui/mo;)V

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 139
    iget-object v0, p0, Lcom/peel/ui/mo;->h:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 140
    iget-object v0, p0, Lcom/peel/ui/mo;->aq:Landroid/widget/EditText;

    iget-object v2, p0, Lcom/peel/ui/mo;->h:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->append(Ljava/lang/CharSequence;)V

    .line 143
    :cond_2
    iget-object v0, p0, Lcom/peel/ui/mo;->g:[Ljava/lang/String;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/peel/ui/mo;->g:[Ljava/lang/String;

    array-length v0, v0

    if-lez v0, :cond_6

    .line 144
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/peel/ui/mo;->au:Ljava/util/List;

    move v0, v1

    .line 145
    :goto_0
    iget-object v2, p0, Lcom/peel/ui/mo;->g:[Ljava/lang/String;

    array-length v2, v2

    if-ge v0, v2, :cond_5

    move v2, v1

    .line 147
    :goto_1
    sget-object v4, Lcom/peel/ui/mo;->aB:[Ljava/lang/String;

    array-length v4, v4

    if-ge v2, v4, :cond_7

    .line 148
    iget-object v4, p0, Lcom/peel/ui/mo;->g:[Ljava/lang/String;

    aget-object v4, v4, v0

    sget-object v5, Lcom/peel/ui/mo;->aB:[Ljava/lang/String;

    aget-object v5, v5, v2

    invoke-virtual {v4, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 149
    const/4 v2, 0x1

    .line 153
    :goto_2
    if-nez v2, :cond_3

    .line 154
    iget-object v2, p0, Lcom/peel/ui/mo;->au:Ljava/util/List;

    iget-object v4, p0, Lcom/peel/ui/mo;->g:[Ljava/lang/String;

    aget-object v4, v4, v0

    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 145
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 147
    :cond_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 157
    :cond_5
    iget-object v0, p0, Lcom/peel/ui/mo;->ap:Landroid/widget/TextView;

    invoke-direct {p0}, Lcom/peel/ui/mo;->c()Landroid/text/SpannableStringBuilder;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 158
    iget-object v0, p0, Lcom/peel/ui/mo;->ap:Landroid/widget/TextView;

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 161
    :cond_6
    invoke-virtual {p0}, Lcom/peel/ui/mo;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    invoke-static {v0}, Lcom/peel/util/b/e;->a(Landroid/content/Context;)Lcom/squareup/picasso/Picasso;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/squareup/picasso/Picasso;->load(Ljava/lang/String;)Lcom/squareup/picasso/RequestCreator;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/ui/mo;->aj:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Lcom/squareup/picasso/RequestCreator;->into(Landroid/widget/ImageView;)V

    .line 174
    return-void

    :cond_7
    move v2, v1

    goto :goto_2
.end method

.method public onClick(Landroid/view/View;)V
    .locals 8

    .prologue
    const/4 v3, 0x0

    const/4 v1, 0x1

    const/4 v5, 0x0

    .line 256
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    .line 257
    sget v2, Lcom/peel/ui/fp;->gallery_icon:I

    if-ne v0, v2, :cond_2

    .line 258
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v2, "android.intent.action.PICK"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 259
    const-string/jumbo v2, "image/*"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 260
    invoke-virtual {p0}, Lcom/peel/ui/mo;->m()Landroid/support/v4/app/ae;

    move-result-object v2

    invoke-virtual {v2, v0, v5}, Landroid/support/v4/app/ae;->startActivityForResult(Landroid/content/Intent;I)V

    .line 262
    invoke-static {}, Lcom/peel/util/a/f;->a()Lcom/peel/util/a/f;

    move-result-object v0

    sget-object v2, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v2}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v2

    if-nez v2, :cond_1

    :goto_0
    const/16 v2, 0x4c2

    iget v3, p0, Lcom/peel/ui/mo;->ax:I

    iget-object v4, p0, Lcom/peel/ui/mo;->az:Ljava/lang/String;

    iget-object v6, p0, Lcom/peel/ui/mo;->ay:Ljava/lang/String;

    move v7, v5

    invoke-virtual/range {v0 .. v7}, Lcom/peel/util/a/f;->a(IIILjava/lang/String;ILjava/lang/String;I)V

    .line 292
    :cond_0
    :goto_1
    return-void

    .line 262
    :cond_1
    sget-object v1, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v1}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/data/at;->f()I

    move-result v1

    goto :goto_0

    .line 264
    :cond_2
    sget v2, Lcom/peel/ui/fp;->camera_icon:I

    if-ne v0, v2, :cond_4

    .line 265
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v2, "android.media.action.IMAGE_CAPTURE"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 266
    invoke-virtual {p0}, Lcom/peel/ui/mo;->m()Landroid/support/v4/app/ae;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, Landroid/support/v4/app/ae;->startActivityForResult(Landroid/content/Intent;I)V

    .line 268
    invoke-static {}, Lcom/peel/util/a/f;->a()Lcom/peel/util/a/f;

    move-result-object v0

    sget-object v2, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v2}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v2

    if-nez v2, :cond_3

    :goto_2
    const/16 v2, 0x4c3

    iget v3, p0, Lcom/peel/ui/mo;->ax:I

    iget-object v4, p0, Lcom/peel/ui/mo;->az:Ljava/lang/String;

    iget-object v6, p0, Lcom/peel/ui/mo;->ay:Ljava/lang/String;

    move v7, v5

    invoke-virtual/range {v0 .. v7}, Lcom/peel/util/a/f;->a(IIILjava/lang/String;ILjava/lang/String;I)V

    goto :goto_1

    :cond_3
    sget-object v1, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v1}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/data/at;->f()I

    move-result v1

    goto :goto_2

    .line 283
    :cond_4
    sget v1, Lcom/peel/ui/fp;->delete_image:I

    if-ne v0, v1, :cond_0

    .line 284
    iget-object v0, p0, Lcom/peel/ui/mo;->ak:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 285
    iget-object v0, p0, Lcom/peel/ui/mo;->ar:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 286
    iput-object v3, p0, Lcom/peel/ui/mo;->i:Landroid/net/Uri;

    .line 287
    iget-object v0, p0, Lcom/peel/ui/mo;->al:Landroid/widget/ImageView;

    sget v1, Lcom/peel/ui/fo;->gallery_icon_normal:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 288
    iget-object v0, p0, Lcom/peel/ui/mo;->am:Landroid/widget/ImageView;

    sget v1, Lcom/peel/ui/fo;->camera_icon_normal:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 290
    iget-object v0, p0, Lcom/peel/ui/mo;->at:Ljava/io/File;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/peel/ui/mo;->at:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->deleteOnExit()V

    goto :goto_1
.end method

.method public w()V
    .locals 6

    .prologue
    const/4 v1, 0x1

    .line 248
    invoke-super {p0}, Lcom/peel/d/u;->w()V

    .line 249
    iget-object v0, p0, Lcom/peel/ui/mo;->aq:Landroid/widget/EditText;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setFocusable(Z)V

    .line 250
    iget-object v0, p0, Lcom/peel/ui/mo;->aq:Landroid/widget/EditText;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setCursorVisible(Z)V

    .line 251
    invoke-virtual {p0}, Lcom/peel/ui/mo;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    const-class v1, Lcom/peel/ui/mo;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/peel/ui/mo;->aq:Landroid/widget/EditText;

    const-wide/16 v4, 0xfa

    invoke-static {v0, v1, v2, v4, v5}, Lcom/peel/util/bx;->a(Landroid/content/Context;Ljava/lang/String;Landroid/widget/EditText;J)V

    .line 252
    return-void
.end method
