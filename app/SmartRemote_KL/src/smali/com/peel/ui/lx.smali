.class Lcom/peel/ui/lx;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/peel/ui/lq;


# direct methods
.method constructor <init>(Lcom/peel/ui/lq;)V
    .locals 0

    .prologue
    .line 956
    iput-object p1, p0, Lcom/peel/ui/lx;->a:Lcom/peel/ui/lq;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 959
    iget-object v0, p0, Lcom/peel/ui/lx;->a:Lcom/peel/ui/lq;

    invoke-static {v0}, Lcom/peel/ui/lq;->b(Lcom/peel/ui/lq;)Lcom/peel/widget/CustomViewPager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/widget/CustomViewPager;->getAdapter()Landroid/support/v4/view/av;

    move-result-object v0

    check-cast v0, Lcom/peel/ui/ds;

    iget-object v1, p0, Lcom/peel/ui/lx;->a:Lcom/peel/ui/lq;

    .line 960
    invoke-static {v1}, Lcom/peel/ui/lq;->d(Lcom/peel/ui/lq;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/peel/ui/ds;->d(I)Lcom/peel/d/u;

    move-result-object v0

    .line 961
    if-nez v0, :cond_0

    iget-object v1, p0, Lcom/peel/ui/lx;->a:Lcom/peel/ui/lq;

    invoke-static {v1}, Lcom/peel/ui/lq;->u(Lcom/peel/ui/lq;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 962
    iget-object v0, p0, Lcom/peel/ui/lx;->a:Lcom/peel/ui/lq;

    invoke-static {v0}, Lcom/peel/ui/lq;->v(Lcom/peel/ui/lq;)Landroid/os/Handler;

    move-result-object v0

    const-wide/16 v2, 0x190

    invoke-virtual {v0, p0, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 970
    :goto_0
    return-void

    .line 963
    :cond_0
    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/peel/ui/lx;->a:Lcom/peel/ui/lq;

    invoke-static {v0}, Lcom/peel/ui/lq;->u(Lcom/peel/ui/lq;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 964
    iget-object v0, p0, Lcom/peel/ui/lx;->a:Lcom/peel/ui/lq;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/peel/ui/lq;->c(Lcom/peel/ui/lq;Z)Z

    .line 965
    iget-object v0, p0, Lcom/peel/ui/lx;->a:Lcom/peel/ui/lq;

    invoke-static {v0}, Lcom/peel/ui/lq;->v(Lcom/peel/ui/lq;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 966
    iget-object v0, p0, Lcom/peel/ui/lx;->a:Lcom/peel/ui/lq;

    invoke-static {v0}, Lcom/peel/ui/lq;->c(Lcom/peel/ui/lq;)V

    goto :goto_0

    .line 968
    :cond_1
    iget-object v0, p0, Lcom/peel/ui/lx;->a:Lcom/peel/ui/lq;

    invoke-static {v0}, Lcom/peel/ui/lq;->v(Lcom/peel/ui/lq;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    goto :goto_0
.end method
