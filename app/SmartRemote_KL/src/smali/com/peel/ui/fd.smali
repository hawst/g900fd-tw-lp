.class public Lcom/peel/ui/fd;
.super Landroid/widget/BaseAdapter;


# instance fields
.field final synthetic a:Lcom/peel/ui/ey;

.field private b:[Lcom/peel/data/Channel;

.field private c:Landroid/os/Bundle;


# direct methods
.method public constructor <init>(Lcom/peel/ui/ey;Landroid/os/Bundle;[Lcom/peel/data/Channel;)V
    .locals 0

    .prologue
    .line 299
    iput-object p1, p0, Lcom/peel/ui/fd;->a:Lcom/peel/ui/ey;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 300
    iput-object p2, p0, Lcom/peel/ui/fd;->c:Landroid/os/Bundle;

    .line 301
    iput-object p3, p0, Lcom/peel/ui/fd;->b:[Lcom/peel/data/Channel;

    .line 302
    return-void
.end method


# virtual methods
.method public a(Landroid/os/Bundle;[Lcom/peel/data/Channel;)V
    .locals 0

    .prologue
    .line 305
    iput-object p1, p0, Lcom/peel/ui/fd;->c:Landroid/os/Bundle;

    .line 306
    iput-object p2, p0, Lcom/peel/ui/fd;->b:[Lcom/peel/data/Channel;

    .line 307
    invoke-virtual {p0}, Lcom/peel/ui/fd;->notifyDataSetChanged()V

    .line 308
    return-void
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 311
    iget-object v0, p0, Lcom/peel/ui/fd;->a:Lcom/peel/ui/ey;

    invoke-static {v0}, Lcom/peel/ui/ey;->d(Lcom/peel/ui/ey;)Ljava/util/ArrayList;

    move-result-object v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/peel/ui/fd;->b:[Lcom/peel/data/Channel;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/peel/ui/fd;->b:[Lcom/peel/data/Channel;

    array-length v0, v0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/peel/ui/fd;->a:Lcom/peel/ui/ey;

    invoke-static {v0}, Lcom/peel/ui/ey;->d(Lcom/peel/ui/ey;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    goto :goto_0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 315
    const/4 v0, 0x0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2

    .prologue
    .line 319
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 16

    .prologue
    .line 324
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/peel/ui/fd;->a:Lcom/peel/ui/ey;

    invoke-static {v2}, Lcom/peel/ui/ey;->e(Lcom/peel/ui/ey;)Landroid/content/Context;

    move-result-object v2

    if-eqz v2, :cond_0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/peel/ui/fd;->a:Lcom/peel/ui/ey;

    invoke-static {v2}, Lcom/peel/ui/ey;->f(Lcom/peel/ui/ey;)Landroid/view/LayoutInflater;

    move-result-object v2

    if-nez v2, :cond_1

    .line 501
    :cond_0
    :goto_0
    return-object p2

    .line 327
    :cond_1
    if-nez p2, :cond_2

    .line 328
    new-instance v4, Lcom/peel/ui/fh;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/peel/ui/fd;->a:Lcom/peel/ui/ey;

    invoke-direct {v4, v2}, Lcom/peel/ui/fh;-><init>(Lcom/peel/ui/ey;)V

    .line 329
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/peel/ui/fd;->a:Lcom/peel/ui/ey;

    invoke-static {v2}, Lcom/peel/ui/ey;->f(Lcom/peel/ui/ey;)Landroid/view/LayoutInflater;

    move-result-object v2

    sget v3, Lcom/peel/ui/fq;->epg_sub_list_item:I

    const/4 v5, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v2, v3, v0, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 330
    sget v2, Lcom/peel/ui/fp;->iv_show_image:I

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, v4, Lcom/peel/ui/fh;->a:Landroid/widget/ImageView;

    .line 331
    sget v2, Lcom/peel/ui/fp;->tv_show_name:I

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, v4, Lcom/peel/ui/fh;->b:Landroid/widget/TextView;

    .line 332
    sget v2, Lcom/peel/ui/fp;->tv_show_time:I

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, v4, Lcom/peel/ui/fh;->c:Landroid/widget/TextView;

    .line 333
    sget v2, Lcom/peel/ui/fp;->tv_next_show:I

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, v4, Lcom/peel/ui/fh;->d:Landroid/widget/TextView;

    .line 334
    sget v2, Lcom/peel/ui/fp;->tv_new:I

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, v4, Lcom/peel/ui/fh;->e:Landroid/widget/TextView;

    .line 335
    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 340
    :goto_1
    const/4 v3, 0x0

    .line 341
    const/4 v9, 0x0

    .line 343
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/peel/ui/fd;->c:Landroid/os/Bundle;

    if-eqz v2, :cond_0

    .line 345
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/peel/ui/fd;->a:Lcom/peel/ui/ey;

    invoke-static {v2}, Lcom/peel/ui/ey;->d(Lcom/peel/ui/ey;)Ljava/util/ArrayList;

    move-result-object v2

    if-eqz v2, :cond_3

    .line 346
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/peel/ui/fd;->a:Lcom/peel/ui/ey;

    invoke-static {v2}, Lcom/peel/ui/ey;->d(Lcom/peel/ui/ey;)Ljava/util/ArrayList;

    move-result-object v2

    move/from16 v0, p1

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/peel/data/Channel;

    move-object v8, v2

    .line 350
    :goto_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/peel/ui/fd;->c:Landroid/os/Bundle;

    invoke-virtual {v8}, Lcom/peel/data/Channel;->a()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 351
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/peel/ui/fd;->c:Landroid/os/Bundle;

    invoke-virtual {v8}, Lcom/peel/data/Channel;->a()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    .line 352
    invoke-static {v2}, Lcom/peel/content/listing/LiveListing;->a(Ljava/util/List;)V

    .line 354
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/peel/ui/fd;->c:Landroid/os/Bundle;

    const-string/jumbo v6, "start"

    invoke-virtual {v5, v6}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    .line 355
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v5

    .line 358
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    move-object v10, v3

    :goto_3
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/os/Parcelable;

    .line 359
    check-cast v2, Lcom/peel/content/listing/LiveListing;

    .line 364
    if-nez v10, :cond_c

    .line 365
    invoke-virtual {v2}, Lcom/peel/content/listing/LiveListing;->o()J

    move-result-wide v12

    invoke-virtual {v2}, Lcom/peel/content/listing/LiveListing;->j()J

    move-result-wide v14

    add-long/2addr v12, v14

    .line 366
    invoke-virtual {v5}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v14

    cmp-long v3, v12, v14

    if-lez v3, :cond_c

    move-object v3, v2

    .line 371
    :goto_4
    if-nez v9, :cond_b

    if-eqz v3, :cond_b

    if-eqz v2, :cond_b

    .line 373
    invoke-virtual {v2}, Lcom/peel/content/listing/LiveListing;->o()J

    move-result-wide v10

    invoke-virtual {v3}, Lcom/peel/content/listing/LiveListing;->o()J

    move-result-wide v12

    .line 374
    invoke-virtual {v3}, Lcom/peel/content/listing/LiveListing;->j()J

    move-result-wide v14

    add-long/2addr v12, v14

    cmp-long v7, v10, v12

    if-ltz v7, :cond_b

    :goto_5
    move-object v9, v2

    move-object v10, v3

    .line 380
    goto :goto_3

    .line 337
    :cond_2
    invoke-virtual/range {p2 .. p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/peel/ui/fh;

    move-object v4, v2

    goto/16 :goto_1

    .line 348
    :cond_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/peel/ui/fd;->b:[Lcom/peel/data/Channel;

    aget-object v2, v2, p1

    move-object v8, v2

    goto :goto_2

    .line 382
    :cond_4
    if-eqz v10, :cond_5

    .line 384
    const/16 v2, 0x2b

    invoke-static {v10, v2}, Lcom/peel/util/bx;->a(Lcom/peel/content/listing/Listing;I)I

    move-result v7

    .line 387
    iget-object v2, v4, Lcom/peel/ui/fh;->b:Landroid/widget/TextView;

    invoke-virtual {v10}, Lcom/peel/content/listing/LiveListing;->l()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 388
    iget-object v3, v4, Lcom/peel/ui/fh;->c:Landroid/widget/TextView;

    sget-object v2, Lcom/peel/util/x;->c:Ljava/lang/ThreadLocal;

    .line 389
    invoke-virtual {v2}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/text/SimpleDateFormat;

    invoke-virtual {v10}, Lcom/peel/content/listing/LiveListing;->o()J

    move-result-wide v12

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 390
    invoke-virtual {v10}, Lcom/peel/content/listing/LiveListing;->j()J

    move-result-wide v12

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/peel/ui/fd;->a:Lcom/peel/ui/ey;

    invoke-static {v5}, Lcom/peel/ui/ey;->e(Lcom/peel/ui/ey;)Landroid/content/Context;

    move-result-object v5

    invoke-static {v5}, Landroid/text/format/DateFormat;->is24HourFormat(Landroid/content/Context;)Z

    move-result v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/peel/ui/fd;->a:Lcom/peel/ui/ey;

    .line 391
    invoke-static {v6}, Lcom/peel/ui/ey;->e(Lcom/peel/ui/ey;)Landroid/content/Context;

    move-result-object v6

    sget v11, Lcom/peel/ui/ft;->time_pattern:I

    invoke-virtual {v6, v11}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 388
    invoke-static {v2, v12, v13, v5, v6}, Lcom/peel/util/x;->a(Ljava/lang/String;JZLjava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 392
    invoke-virtual {v10}, Lcom/peel/content/listing/LiveListing;->y()Ljava/lang/String;

    move-result-object v6

    .line 394
    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_6

    .line 395
    iget-object v2, v4, Lcom/peel/ui/fh;->a:Landroid/widget/ImageView;

    sget v3, Lcom/peel/ui/fp;->tag_image:I

    invoke-static/range {p1 .. p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v2, v3, v5}, Landroid/widget/ImageView;->setTag(ILjava/lang/Object;)V

    .line 396
    iget-object v2, v4, Lcom/peel/ui/fh;->a:Landroid/widget/ImageView;

    invoke-virtual {v2, v7}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 399
    const/16 v2, 0xc8

    .line 401
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/peel/ui/fd;->a:Lcom/peel/ui/ey;

    invoke-static {v3}, Lcom/peel/ui/ey;->g(Lcom/peel/ui/ey;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3, v6}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_a

    .line 402
    const/16 v2, 0x32

    move v11, v2

    .line 405
    :goto_6
    const-class v2, Lcom/peel/ui/ey;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v12

    const-string/jumbo v13, "load image"

    new-instance v2, Lcom/peel/ui/fe;

    move-object/from16 v3, p0

    move/from16 v5, p1

    invoke-direct/range {v2 .. v7}, Lcom/peel/ui/fe;-><init>(Lcom/peel/ui/fd;Lcom/peel/ui/fh;ILjava/lang/String;I)V

    int-to-long v6, v11

    invoke-static {v12, v13, v2, v6, v7}, Lcom/peel/util/i;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;J)Ljava/lang/Runnable;

    .line 439
    :goto_7
    invoke-virtual {v10}, Lcom/peel/content/listing/LiveListing;->u()Ljava/lang/String;

    move-result-object v2

    .line 440
    if-eqz v2, :cond_7

    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_7

    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "new"

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 441
    iget-object v2, v4, Lcom/peel/ui/fh;->e:Landroid/widget/TextView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 448
    :goto_8
    new-instance v2, Lcom/peel/ui/fg;

    move-object/from16 v0, p0

    move/from16 v1, p1

    invoke-direct {v2, v0, v10, v8, v1}, Lcom/peel/ui/fg;-><init>(Lcom/peel/ui/fd;Lcom/peel/content/listing/LiveListing;Lcom/peel/data/Channel;I)V

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 476
    :cond_5
    if-eqz v9, :cond_8

    .line 477
    sget-object v2, Lcom/peel/util/x;->c:Ljava/lang/ThreadLocal;

    .line 478
    invoke-virtual {v2}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/text/SimpleDateFormat;

    invoke-virtual {v9}, Lcom/peel/content/listing/LiveListing;->o()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    const-wide/16 v6, 0x0

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/peel/ui/fd;->a:Lcom/peel/ui/ey;

    .line 479
    invoke-static {v3}, Lcom/peel/ui/ey;->e(Lcom/peel/ui/ey;)Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Landroid/text/format/DateFormat;->is24HourFormat(Landroid/content/Context;)Z

    move-result v3

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/peel/ui/fd;->a:Lcom/peel/ui/ey;

    .line 480
    invoke-static {v5}, Lcom/peel/ui/ey;->e(Lcom/peel/ui/ey;)Landroid/content/Context;

    move-result-object v5

    sget v8, Lcom/peel/ui/ft;->time_pattern:I

    invoke-virtual {v5, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 477
    invoke-static {v2, v6, v7, v3, v5}, Lcom/peel/util/x;->a(Ljava/lang/String;JZLjava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 482
    iget-object v3, v4, Lcom/peel/ui/fh;->d:Landroid/widget/TextView;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v5, ": "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 483
    invoke-virtual {v9}, Lcom/peel/content/listing/LiveListing;->l()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 482
    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 488
    :goto_9
    iget-object v2, v4, Lcom/peel/ui/fh;->a:Landroid/widget/ImageView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 489
    iget-object v2, v4, Lcom/peel/ui/fh;->b:Landroid/widget/TextView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 490
    iget-object v2, v4, Lcom/peel/ui/fh;->c:Landroid/widget/TextView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 491
    iget-object v2, v4, Lcom/peel/ui/fh;->d:Landroid/widget/TextView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 492
    iget-object v2, v4, Lcom/peel/ui/fh;->e:Landroid/widget/TextView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_0

    .line 436
    :cond_6
    iget-object v2, v4, Lcom/peel/ui/fh;->a:Landroid/widget/ImageView;

    invoke-virtual {v2, v7}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_7

    .line 443
    :cond_7
    iget-object v2, v4, Lcom/peel/ui/fh;->e:Landroid/widget/TextView;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_8

    .line 485
    :cond_8
    iget-object v2, v4, Lcom/peel/ui/fh;->d:Landroid/widget/TextView;

    const-string/jumbo v3, ""

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_9

    .line 494
    :cond_9
    iget-object v2, v4, Lcom/peel/ui/fh;->a:Landroid/widget/ImageView;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 495
    iget-object v2, v4, Lcom/peel/ui/fh;->b:Landroid/widget/TextView;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 496
    iget-object v2, v4, Lcom/peel/ui/fh;->c:Landroid/widget/TextView;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 497
    iget-object v2, v4, Lcom/peel/ui/fh;->d:Landroid/widget/TextView;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 498
    iget-object v2, v4, Lcom/peel/ui/fh;->e:Landroid/widget/TextView;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_0

    :cond_a
    move v11, v2

    goto/16 :goto_6

    :cond_b
    move-object v2, v9

    goto/16 :goto_5

    :cond_c
    move-object v3, v10

    goto/16 :goto_4
.end method
