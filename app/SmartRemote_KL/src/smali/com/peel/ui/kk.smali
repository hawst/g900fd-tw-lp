.class Lcom/peel/ui/kk;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/widget/AbsListView$OnScrollListener;


# instance fields
.field final synthetic a:Lcom/peel/ui/kj;


# direct methods
.method constructor <init>(Lcom/peel/ui/kj;)V
    .locals 0

    .prologue
    .line 118
    iput-object p1, p0, Lcom/peel/ui/kk;->a:Lcom/peel/ui/kj;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onScroll(Landroid/widget/AbsListView;III)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 126
    if-nez p2, :cond_1

    .line 127
    iget-object v0, p0, Lcom/peel/ui/kk;->a:Lcom/peel/ui/kj;

    invoke-static {v0}, Lcom/peel/ui/kj;->a(Lcom/peel/ui/kj;)Landroid/widget/LinearLayout;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 141
    :cond_0
    :goto_0
    return-void

    .line 129
    :cond_1
    iget-object v0, p0, Lcom/peel/ui/kk;->a:Lcom/peel/ui/kj;

    invoke-static {v0}, Lcom/peel/ui/kj;->a(Lcom/peel/ui/kj;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    move v0, v1

    .line 131
    :goto_1
    iget-object v2, p0, Lcom/peel/ui/kk;->a:Lcom/peel/ui/kj;

    invoke-static {v2}, Lcom/peel/ui/kj;->b(Lcom/peel/ui/kj;)[I

    move-result-object v2

    array-length v2, v2

    if-ge v0, v2, :cond_0

    .line 132
    iget-object v2, p0, Lcom/peel/ui/kk;->a:Lcom/peel/ui/kj;

    invoke-static {v2}, Lcom/peel/ui/kj;->b(Lcom/peel/ui/kj;)[I

    move-result-object v2

    aget v2, v2, v0

    if-ne p2, v2, :cond_2

    .line 133
    iget-object v1, p0, Lcom/peel/ui/kk;->a:Lcom/peel/ui/kj;

    invoke-static {v1}, Lcom/peel/ui/kj;->d(Lcom/peel/ui/kj;)Landroid/widget/TextView;

    move-result-object v1

    iget-object v2, p0, Lcom/peel/ui/kk;->a:Lcom/peel/ui/kj;

    invoke-static {v2}, Lcom/peel/ui/kj;->c(Lcom/peel/ui/kj;)[Ljava/lang/String;

    move-result-object v2

    aget-object v0, v2, v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 135
    :cond_2
    iget-object v2, p0, Lcom/peel/ui/kk;->a:Lcom/peel/ui/kj;

    invoke-static {v2}, Lcom/peel/ui/kj;->b(Lcom/peel/ui/kj;)[I

    move-result-object v2

    aget v2, v2, v0

    add-int/lit8 v2, v2, -0x2

    if-ne p2, v2, :cond_3

    iget-object v2, p0, Lcom/peel/ui/kk;->a:Lcom/peel/ui/kj;

    invoke-static {v2}, Lcom/peel/ui/kj;->d(Lcom/peel/ui/kj;)Landroid/widget/TextView;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    iget-object v3, p0, Lcom/peel/ui/kk;->a:Lcom/peel/ui/kj;

    invoke-static {v3}, Lcom/peel/ui/kj;->c(Lcom/peel/ui/kj;)[Ljava/lang/String;

    move-result-object v3

    add-int/lit8 v4, v0, -0x1

    aget-object v3, v3, v4

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 136
    iget-object v2, p0, Lcom/peel/ui/kk;->a:Lcom/peel/ui/kj;

    invoke-static {v2}, Lcom/peel/ui/kj;->a(Lcom/peel/ui/kj;)Landroid/widget/LinearLayout;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 137
    iget-object v2, p0, Lcom/peel/ui/kk;->a:Lcom/peel/ui/kj;

    invoke-static {v2}, Lcom/peel/ui/kj;->d(Lcom/peel/ui/kj;)Landroid/widget/TextView;

    move-result-object v2

    iget-object v3, p0, Lcom/peel/ui/kk;->a:Lcom/peel/ui/kj;

    invoke-static {v3}, Lcom/peel/ui/kj;->c(Lcom/peel/ui/kj;)[Ljava/lang/String;

    move-result-object v3

    add-int/lit8 v4, v0, -0x1

    aget-object v3, v3, v4

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 131
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method public onScrollStateChanged(Landroid/widget/AbsListView;I)V
    .locals 0

    .prologue
    .line 122
    return-void
.end method
