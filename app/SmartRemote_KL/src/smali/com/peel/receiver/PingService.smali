.class public Lcom/peel/receiver/PingService;
.super Landroid/app/IntentService;


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private b:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27
    const-class v0, Lcom/peel/receiver/PingService;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/peel/receiver/PingService;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 32
    const-string/jumbo v0, "Ping Service"

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    .line 33
    return-void
.end method

.method static synthetic a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 25
    sget-object v0, Lcom/peel/receiver/PingService;->a:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic a(Lcom/peel/receiver/PingService;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lcom/peel/receiver/PingService;->b:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic a(Lcom/peel/receiver/PingService;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 25
    iput-object p1, p0, Lcom/peel/receiver/PingService;->b:Ljava/lang/String;

    return-object p1
.end method


# virtual methods
.method protected onHandleIntent(Landroid/content/Intent;)V
    .locals 6

    .prologue
    .line 37
    sget-object v0, Lcom/peel/receiver/PingService;->a:Ljava/lang/String;

    const-string/jumbo v1, "onHandleIntent..."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 40
    invoke-virtual {p0}, Lcom/peel/receiver/PingService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/peel/util/au;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 41
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_0

    .line 45
    const-string/jumbo v0, "country_code"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 46
    const-string/jumbo v0, "rom_region"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 48
    new-instance v0, Lcom/peel/receiver/d;

    const/4 v2, 0x2

    move-object v1, p0

    move-object v5, p0

    invoke-direct/range {v0 .. v5}, Lcom/peel/receiver/d;-><init>(Lcom/peel/receiver/PingService;ILjava/lang/String;Ljava/lang/String;Landroid/content/Context;)V

    invoke-static {p0, v4, v0}, Lcom/peel/util/bx;->a(Landroid/content/Context;Ljava/lang/String;Lcom/peel/util/t;)V

    .line 123
    :cond_0
    return-void
.end method
