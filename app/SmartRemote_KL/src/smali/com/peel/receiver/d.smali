.class Lcom/peel/receiver/d;
.super Lcom/peel/util/t;


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic b:Ljava/lang/String;

.field final synthetic c:Landroid/content/Context;

.field final synthetic d:Lcom/peel/receiver/PingService;


# direct methods
.method constructor <init>(Lcom/peel/receiver/PingService;ILjava/lang/String;Ljava/lang/String;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 48
    iput-object p1, p0, Lcom/peel/receiver/d;->d:Lcom/peel/receiver/PingService;

    iput-object p3, p0, Lcom/peel/receiver/d;->a:Ljava/lang/String;

    iput-object p4, p0, Lcom/peel/receiver/d;->b:Ljava/lang/String;

    iput-object p5, p0, Lcom/peel/receiver/d;->c:Landroid/content/Context;

    invoke-direct {p0, p2}, Lcom/peel/util/t;-><init>(I)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 51
    :try_start_0
    iget-boolean v0, p0, Lcom/peel/receiver/d;->i:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/peel/receiver/d;->j:Ljava/lang/Object;

    if-nez v0, :cond_1

    .line 52
    :cond_0
    invoke-static {}, Lcom/peel/receiver/PingService;->a()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "getcountries call failed"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 120
    :goto_0
    return-void

    .line 56
    :cond_1
    iget-object v0, p0, Lcom/peel/receiver/d;->j:Ljava/lang/Object;

    check-cast v0, Ljava/util/ArrayList;

    .line 58
    iget-object v1, p0, Lcom/peel/receiver/d;->d:Lcom/peel/receiver/PingService;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/peel/receiver/PingService;->a(Lcom/peel/receiver/PingService;Ljava/lang/String;)Ljava/lang/String;

    .line 60
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    .line 68
    iget-object v2, p0, Lcom/peel/receiver/d;->a:Ljava/lang/String;

    const-string/jumbo v3, "iso"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 69
    const-string/jumbo v2, "endpoint"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 70
    const-string/jumbo v2, "europe"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 71
    iget-object v0, p0, Lcom/peel/receiver/d;->d:Lcom/peel/receiver/PingService;

    const-string/jumbo v1, "EU"

    invoke-static {v0, v1}, Lcom/peel/receiver/PingService;->a(Lcom/peel/receiver/PingService;Ljava/lang/String;)Ljava/lang/String;

    .line 89
    :cond_3
    :goto_1
    invoke-static {}, Lcom/peel/receiver/PingService;->a()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "\n\n found region_prefix: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/peel/receiver/d;->d:Lcom/peel/receiver/PingService;

    invoke-static {v2}, Lcom/peel/receiver/PingService;->a(Lcom/peel/receiver/PingService;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 90
    iget-object v0, p0, Lcom/peel/receiver/d;->d:Lcom/peel/receiver/PingService;

    invoke-static {v0}, Lcom/peel/receiver/PingService;->a(Lcom/peel/receiver/PingService;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_4

    .line 91
    iget-object v0, p0, Lcom/peel/receiver/d;->d:Lcom/peel/receiver/PingService;

    const-string/jumbo v1, "XX"

    invoke-static {v0, v1}, Lcom/peel/receiver/PingService;->a(Lcom/peel/receiver/PingService;Ljava/lang/String;)Ljava/lang/String;

    .line 94
    :cond_4
    const-class v0, Lcom/peel/receiver/PingService;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, ""

    new-instance v2, Lcom/peel/receiver/e;

    invoke-direct {v2, p0}, Lcom/peel/receiver/e;-><init>(Lcom/peel/receiver/d;)V

    invoke-static {v0, v1, v2}, Lcom/peel/util/i;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/util/concurrent/Future;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 117
    :catch_0
    move-exception v0

    .line 118
    invoke-static {}, Lcom/peel/receiver/PingService;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {}, Lcom/peel/receiver/PingService;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_0

    .line 73
    :cond_5
    :try_start_1
    const-string/jumbo v2, "asia"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 74
    iget-object v0, p0, Lcom/peel/receiver/d;->d:Lcom/peel/receiver/PingService;

    const-string/jumbo v1, "AS"

    invoke-static {v0, v1}, Lcom/peel/receiver/PingService;->a(Lcom/peel/receiver/PingService;Ljava/lang/String;)Ljava/lang/String;

    goto :goto_1

    .line 76
    :cond_6
    const-string/jumbo v2, "latin"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 77
    iget-object v0, p0, Lcom/peel/receiver/d;->d:Lcom/peel/receiver/PingService;

    const-string/jumbo v1, "LA"

    invoke-static {v0, v1}, Lcom/peel/receiver/PingService;->a(Lcom/peel/receiver/PingService;Ljava/lang/String;)Ljava/lang/String;

    goto :goto_1

    .line 79
    :cond_7
    const-string/jumbo v2, "bramex"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 80
    iget-object v0, p0, Lcom/peel/receiver/d;->d:Lcom/peel/receiver/PingService;

    const-string/jumbo v1, "BM"

    invoke-static {v0, v1}, Lcom/peel/receiver/PingService;->a(Lcom/peel/receiver/PingService;Ljava/lang/String;)Ljava/lang/String;

    goto/16 :goto_1

    .line 82
    :cond_8
    const-string/jumbo v2, "usa"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 83
    iget-object v0, p0, Lcom/peel/receiver/d;->d:Lcom/peel/receiver/PingService;

    const-string/jumbo v1, "US"

    invoke-static {v0, v1}, Lcom/peel/receiver/PingService;->a(Lcom/peel/receiver/PingService;Ljava/lang/String;)Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_1
.end method
