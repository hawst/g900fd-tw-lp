.class Lcom/peel/receiver/c;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/squareup/picasso/Target;


# instance fields
.field final synthetic a:Landroid/os/Bundle;

.field final synthetic b:Z

.field final synthetic c:Landroid/support/v4/app/bw;

.field final synthetic d:Lcom/peel/receiver/GcmBroadcastReceiver;


# direct methods
.method constructor <init>(Lcom/peel/receiver/GcmBroadcastReceiver;Landroid/os/Bundle;ZLandroid/support/v4/app/bw;)V
    .locals 0

    .prologue
    .line 390
    iput-object p1, p0, Lcom/peel/receiver/c;->d:Lcom/peel/receiver/GcmBroadcastReceiver;

    iput-object p2, p0, Lcom/peel/receiver/c;->a:Landroid/os/Bundle;

    iput-boolean p3, p0, Lcom/peel/receiver/c;->b:Z

    iput-object p4, p0, Lcom/peel/receiver/c;->c:Landroid/support/v4/app/bw;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onBitmapFailed(Landroid/graphics/drawable/Drawable;)V
    .locals 4

    .prologue
    .line 430
    iget-object v0, p0, Lcom/peel/receiver/c;->c:Landroid/support/v4/app/bw;

    new-instance v1, Landroid/support/v4/app/bv;

    invoke-direct {v1}, Landroid/support/v4/app/bv;-><init>()V

    iget-object v2, p0, Lcom/peel/receiver/c;->a:Landroid/os/Bundle;

    const-string/jumbo v3, "message"

    .line 432
    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/support/v4/app/bv;->a(Ljava/lang/CharSequence;)Landroid/support/v4/app/bv;

    move-result-object v1

    .line 430
    invoke-virtual {v0, v1}, Landroid/support/v4/app/bw;->a(Landroid/support/v4/app/ch;)Landroid/support/v4/app/bw;

    .line 433
    iget-object v0, p0, Lcom/peel/receiver/c;->d:Lcom/peel/receiver/GcmBroadcastReceiver;

    invoke-static {v0}, Lcom/peel/receiver/GcmBroadcastReceiver;->a(Lcom/peel/receiver/GcmBroadcastReceiver;)Landroid/app/NotificationManager;

    move-result-object v0

    const/16 v1, 0x1ca6

    iget-object v2, p0, Lcom/peel/receiver/c;->c:Landroid/support/v4/app/bw;

    invoke-virtual {v2}, Landroid/support/v4/app/bw;->a()Landroid/app/Notification;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 434
    return-void
.end method

.method public onBitmapLoaded(Landroid/graphics/Bitmap;Lcom/squareup/picasso/Picasso$LoadedFrom;)V
    .locals 5

    .prologue
    .line 392
    iget-object v0, p0, Lcom/peel/receiver/c;->a:Landroid/os/Bundle;

    const-string/jumbo v1, "message"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/peel/receiver/c;->a:Landroid/os/Bundle;

    const-string/jumbo v1, "message"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    .line 393
    :goto_0
    const-string/jumbo v0, ""

    .line 394
    iget-object v2, p0, Lcom/peel/receiver/c;->a:Landroid/os/Bundle;

    const-string/jumbo v3, "starttime"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 395
    iget-object v2, p0, Lcom/peel/receiver/c;->a:Landroid/os/Bundle;

    const-string/jumbo v3, "starttime"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/peel/util/x;->c(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v2

    .line 396
    if-eqz v2, :cond_0

    .line 397
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Starting at "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v0, Lcom/peel/util/x;->f:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/text/SimpleDateFormat;

    invoke-virtual {v0, v2}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 401
    :cond_0
    iget-boolean v2, p0, Lcom/peel/receiver/c;->b:Z

    if-eqz v2, :cond_2

    .line 402
    iget-object v2, p0, Lcom/peel/receiver/c;->c:Landroid/support/v4/app/bw;

    .line 403
    invoke-virtual {v2, p1}, Landroid/support/v4/app/bw;->a(Landroid/graphics/Bitmap;)Landroid/support/v4/app/bw;

    move-result-object v2

    iget-object v3, p0, Lcom/peel/receiver/c;->a:Landroid/os/Bundle;

    const-string/jumbo v4, "title"

    .line 406
    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/support/v4/app/bw;->a(Ljava/lang/CharSequence;)Landroid/support/v4/app/bw;

    move-result-object v2

    iget-object v3, p0, Lcom/peel/receiver/c;->a:Landroid/os/Bundle;

    const-string/jumbo v4, "title"

    .line 407
    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/support/v4/app/bw;->c(Ljava/lang/CharSequence;)Landroid/support/v4/app/bw;

    move-result-object v2

    new-instance v3, Landroid/support/v4/app/bx;

    invoke-direct {v3}, Landroid/support/v4/app/bx;-><init>()V

    .line 412
    invoke-virtual {v3, v1}, Landroid/support/v4/app/bx;->c(Ljava/lang/CharSequence;)Landroid/support/v4/app/bx;

    move-result-object v1

    iget-object v3, p0, Lcom/peel/receiver/c;->a:Landroid/os/Bundle;

    const-string/jumbo v4, "title"

    .line 413
    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/support/v4/app/bx;->a(Ljava/lang/CharSequence;)Landroid/support/v4/app/bx;

    move-result-object v1

    .line 414
    invoke-virtual {v1, v0}, Landroid/support/v4/app/bx;->b(Ljava/lang/CharSequence;)Landroid/support/v4/app/bx;

    move-result-object v0

    .line 410
    invoke-virtual {v2, v0}, Landroid/support/v4/app/bw;->a(Landroid/support/v4/app/ch;)Landroid/support/v4/app/bw;

    .line 426
    :goto_1
    iget-object v0, p0, Lcom/peel/receiver/c;->d:Lcom/peel/receiver/GcmBroadcastReceiver;

    invoke-static {v0}, Lcom/peel/receiver/GcmBroadcastReceiver;->a(Lcom/peel/receiver/GcmBroadcastReceiver;)Landroid/app/NotificationManager;

    move-result-object v0

    const/16 v1, 0x1ca6

    iget-object v2, p0, Lcom/peel/receiver/c;->c:Landroid/support/v4/app/bw;

    invoke-virtual {v2}, Landroid/support/v4/app/bw;->a()Landroid/app/Notification;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 427
    return-void

    .line 392
    :cond_1
    const-string/jumbo v0, ""

    move-object v1, v0

    goto/16 :goto_0

    .line 419
    :cond_2
    iget-object v0, p0, Lcom/peel/receiver/c;->c:Landroid/support/v4/app/bw;

    new-instance v2, Landroid/support/v4/app/bu;

    invoke-direct {v2}, Landroid/support/v4/app/bu;-><init>()V

    .line 421
    invoke-virtual {v2, p1}, Landroid/support/v4/app/bu;->a(Landroid/graphics/Bitmap;)Landroid/support/v4/app/bu;

    move-result-object v2

    .line 422
    invoke-virtual {v2, v1}, Landroid/support/v4/app/bu;->a(Ljava/lang/CharSequence;)Landroid/support/v4/app/bu;

    move-result-object v1

    .line 419
    invoke-virtual {v0, v1}, Landroid/support/v4/app/bw;->a(Landroid/support/v4/app/ch;)Landroid/support/v4/app/bw;

    goto :goto_1
.end method

.method public onPrepareLoad(Landroid/graphics/drawable/Drawable;)V
    .locals 0

    .prologue
    .line 438
    return-void
.end method
