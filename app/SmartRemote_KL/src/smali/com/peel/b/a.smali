.class public Lcom/peel/b/a;
.super Ljava/lang/Object;


# static fields
.field public static final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final b:Ljava/lang/String;

.field public static c:Ljava/lang/String;

.field public static d:Ljava/lang/String;

.field public static e:Ljava/lang/String;

.field public static f:Ljava/lang/String;

.field public static g:F

.field public static h:Ljava/lang/String;

.field private static final i:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 21
    const-class v0, Lcom/peel/b/a;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/peel/b/a;->i:Ljava/lang/String;

    .line 23
    invoke-static {}, Lcom/peel/b/a;->a()Ljava/util/Map;

    move-result-object v0

    sput-object v0, Lcom/peel/b/a;->a:Ljava/util/Map;

    .line 24
    sget-object v0, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    sget-object v1, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v0, v1}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "TCT"

    const-string/jumbo v2, "TCL"

    .line 25
    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "SAMSUNG"

    const-string/jumbo v2, "SM"

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "[^a-zA-Z0-9]"

    const-string/jumbo v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/peel/b/a;->b:Ljava/lang/String;

    .line 54
    const-string/jumbo v0, "name|USA|endpoint|usa|iso|US|type|5digitzip"

    sput-object v0, Lcom/peel/b/a;->c:Ljava/lang/String;

    .line 55
    const-string/jumbo v0, "https://scalos.peel-prod.com/"

    sput-object v0, Lcom/peel/b/a;->d:Ljava/lang/String;

    .line 56
    const-string/jumbo v0, "http://staging.zelfy.com/"

    sput-object v0, Lcom/peel/b/a;->e:Ljava/lang/String;

    .line 57
    const-string/jumbo v0, "http://hashtag.peel-prod.com/"

    sput-object v0, Lcom/peel/b/a;->f:Ljava/lang/String;

    .line 60
    sget-object v0, Lcom/peel/b/a;->e:Ljava/lang/String;

    invoke-static {v0}, Lcom/peel/content/a/j;->a(Ljava/lang/String;)V

    .line 61
    sget-object v0, Lcom/peel/b/a;->f:Ljava/lang/String;

    invoke-static {v0}, Lcom/peel/content/a/d;->a(Ljava/lang/String;)V

    .line 62
    sget-object v0, Lcom/peel/b/a;->d:Ljava/lang/String;

    invoke-static {v0}, Lcom/peel/content/a/d;->b(Ljava/lang/String;)V

    .line 63
    const-string/jumbo v0, "http://personalization.peel-prod.com/"

    invoke-static {v0}, Lcom/peel/content/a/j;->b(Ljava/lang/String;)V

    .line 67
    const-string/jumbo v0, "http://graph.peel.com/"

    const-string/jumbo v1, "https://graph.peel.com/"

    invoke-static {v0, v1}, Lcom/peel/content/a/g;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 68
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    return-void
.end method

.method private static a()Ljava/util/Map;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 28
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 29
    sget-object v2, Lcom/peel/b/b;->a:[I

    sget-object v0, Lcom/peel/c/a;->c:Lcom/peel/c/e;

    invoke-static {v0}, Lcom/peel/c/a;->a(Lcom/peel/c/e;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/c/d;

    invoke-virtual {v0}, Lcom/peel/c/d;->ordinal()I

    move-result v0

    aget v0, v2, v0

    packed-switch v0, :pswitch_data_0

    .line 49
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 31
    :pswitch_0
    const-string/jumbo v0, "usa"

    const-string/jumbo v2, "https://epg-us.peel.com/"

    invoke-interface {v1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 32
    const-string/jumbo v0, "asia"

    const-string/jumbo v2, "https://epg-asia.peel.com/"

    invoke-interface {v1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 33
    const-string/jumbo v0, "europe"

    const-string/jumbo v2, "https://epg-eu.peel.com/"

    invoke-interface {v1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 34
    const-string/jumbo v0, "australia"

    const-string/jumbo v2, "https://epg-asia.peel.com/"

    invoke-interface {v1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 35
    const-string/jumbo v0, "bramex"

    const-string/jumbo v2, "https://epg-bramex.peel.com/"

    invoke-interface {v1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 36
    const-string/jumbo v0, "latin"

    const-string/jumbo v2, "https://epg-la.peel.com/"

    invoke-interface {v1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 51
    :goto_0
    invoke-static {v1}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    return-object v0

    .line 39
    :pswitch_1
    const-string/jumbo v0, "usa"

    const-string/jumbo v2, "http://staging.zelfy.com/"

    invoke-interface {v1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 40
    const-string/jumbo v0, "asia"

    const-string/jumbo v2, "http://staging.epg.asia.samsung.peel.com/"

    invoke-interface {v1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 41
    const-string/jumbo v0, "europe"

    const-string/jumbo v2, "http://test.epg.eu.samsung.peel.com/"

    invoke-interface {v1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 42
    const-string/jumbo v0, "australia"

    const-string/jumbo v2, "http://staging.epg.asia.samsung.peel.com/"

    invoke-interface {v1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 43
    const-string/jumbo v0, "bramex"

    const-string/jumbo v2, "http://test.epg.bramex.samsung.peel.com/"

    invoke-interface {v1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 44
    const-string/jumbo v0, "latin"

    const-string/jumbo v2, "http://test.epg.la.samsung.peel.com/"

    invoke-interface {v1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 29
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static a(Landroid/app/Activity;)V
    .locals 2

    .prologue
    .line 73
    new-instance v0, Landroid/util/DisplayMetrics;

    invoke-direct {v0}, Landroid/util/DisplayMetrics;-><init>()V

    .line 74
    invoke-virtual {p0}, Landroid/app/Activity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v1

    invoke-interface {v1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v1

    .line 75
    invoke-virtual {v1, v0}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 76
    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    sput v0, Lcom/peel/b/a;->g:F

    .line 77
    return-void
.end method
