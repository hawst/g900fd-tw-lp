.class public Lcom/peel/e/b;
.super Landroid/database/sqlite/SQLiteOpenHelper;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 16
    const-string/jumbo v0, "mylocation.db"

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-direct {p0, p1, v0, v1, v2}, Landroid/database/sqlite/SQLiteOpenHelper;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V

    .line 17
    return-void
.end method


# virtual methods
.method public onCreate(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 22
    const-string/jumbo v0, "CREATE TABLE IF NOT EXISTS my_location (_id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, type INTEGER, name TEXT , timestamp VARCHAR, wifi_name VARCHAR, wifi_bssid VARCHAR, gps_location VARCHAR, gps_latitude INT, gps_longitude INT, gps_map BLOB, is_enabled INT DEFAULT 0)"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 23
    return-void
.end method

.method public onUpgrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 1

    .prologue
    .line 27
    const-string/jumbo v0, "DROP TABLE IF EXISTS mylocation.db"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 28
    return-void
.end method
