.class Lcom/peel/main/ay;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic a:Landroid/widget/CheckBox;

.field final synthetic b:Landroid/content/SharedPreferences;

.field final synthetic c:Lcom/peel/main/Main;


# direct methods
.method constructor <init>(Lcom/peel/main/Main;Landroid/widget/CheckBox;Landroid/content/SharedPreferences;)V
    .locals 0

    .prologue
    .line 381
    iput-object p1, p0, Lcom/peel/main/ay;->c:Lcom/peel/main/Main;

    iput-object p2, p0, Lcom/peel/main/ay;->a:Landroid/widget/CheckBox;

    iput-object p3, p0, Lcom/peel/main/ay;->b:Landroid/content/SharedPreferences;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3

    .prologue
    .line 384
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 385
    iget-object v0, p0, Lcom/peel/main/ay;->a:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 386
    iget-object v0, p0, Lcom/peel/main/ay;->b:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v1, "wlan_dialog"

    iget-object v2, p0, Lcom/peel/main/ay;->c:Lcom/peel/main/Main;

    invoke-static {v2}, Lcom/peel/main/Main;->c(Lcom/peel/main/Main;)Z

    move-result v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 387
    iget-object v0, p0, Lcom/peel/main/ay;->b:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v1, "wlan_network"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 389
    :cond_0
    iget-object v0, p0, Lcom/peel/main/ay;->c:Lcom/peel/main/Main;

    invoke-static {v0}, Lcom/peel/main/Main;->b(Lcom/peel/main/Main;)V

    .line 391
    return-void
.end method
