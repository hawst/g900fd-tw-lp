.class Lcom/peel/main/k;
.super Lcom/peel/util/t;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/peel/util/t",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:I

.field final synthetic b:Lcom/peel/backup/c;

.field final synthetic c:I

.field final synthetic d:Landroid/content/Intent;

.field final synthetic e:Lcom/peel/main/Home;


# direct methods
.method constructor <init>(Lcom/peel/main/Home;ILcom/peel/backup/c;ILandroid/content/Intent;)V
    .locals 0

    .prologue
    .line 724
    iput-object p1, p0, Lcom/peel/main/k;->e:Lcom/peel/main/Home;

    iput p2, p0, Lcom/peel/main/k;->a:I

    iput-object p3, p0, Lcom/peel/main/k;->b:Lcom/peel/backup/c;

    iput p4, p0, Lcom/peel/main/k;->c:I

    iput-object p5, p0, Lcom/peel/main/k;->d:Landroid/content/Intent;

    invoke-direct {p0}, Lcom/peel/util/t;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    const/4 v1, 0x1

    .line 727
    iget-boolean v0, p0, Lcom/peel/main/k;->i:Z

    if-eqz v0, :cond_2

    .line 728
    invoke-static {}, Lcom/peel/util/a/f;->a()Lcom/peel/util/a/f;

    move-result-object v2

    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v0}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v0

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    const/16 v3, 0xbf5

    const/16 v4, 0x7d5

    sget-object v5, Lcom/peel/social/e;->b:Ljava/lang/String;

    invoke-virtual {v2, v0, v3, v4, v5}, Lcom/peel/util/a/f;->a(IIILjava/lang/String;)V

    .line 729
    iget-object v0, p0, Lcom/peel/main/k;->e:Lcom/peel/main/Home;

    iget-object v2, p0, Lcom/peel/main/k;->k:Ljava/lang/String;

    invoke-static {v0, v2, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 731
    iget v0, p0, Lcom/peel/main/k;->a:I

    sget v1, Lcom/peel/social/w;->a:I

    if-ne v0, v1, :cond_0

    .line 732
    iget-object v0, p0, Lcom/peel/main/k;->b:Lcom/peel/backup/c;

    invoke-virtual {v0}, Lcom/peel/backup/c;->a()V

    .line 735
    :cond_0
    iget-object v0, p0, Lcom/peel/main/k;->e:Lcom/peel/main/Home;

    invoke-static {v0}, Lcom/peel/d/e;->a(Landroid/support/v4/app/ae;)Lcom/peel/d/u;

    move-result-object v0

    iget v1, p0, Lcom/peel/main/k;->a:I

    iget v2, p0, Lcom/peel/main/k;->c:I

    iget-object v3, p0, Lcom/peel/main/k;->d:Landroid/content/Intent;

    .line 736
    invoke-virtual {v0, v1, v2, v3}, Lcom/peel/d/u;->a(IILandroid/content/Intent;)V

    .line 738
    invoke-static {}, Lcom/peel/main/Home;->c()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "update menu"

    new-instance v2, Lcom/peel/main/l;

    invoke-direct {v2, p0}, Lcom/peel/main/l;-><init>(Lcom/peel/main/k;)V

    invoke-static {v0, v1, v2}, Lcom/peel/util/i;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/lang/Runnable;

    .line 747
    :goto_1
    return-void

    .line 728
    :cond_1
    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v0}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/data/at;->f()I

    move-result v0

    goto :goto_0

    .line 745
    :cond_2
    iget-object v0, p0, Lcom/peel/main/k;->e:Lcom/peel/main/Home;

    iget-object v2, p0, Lcom/peel/main/k;->k:Ljava/lang/String;

    invoke-static {v0, v2, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_1
.end method
