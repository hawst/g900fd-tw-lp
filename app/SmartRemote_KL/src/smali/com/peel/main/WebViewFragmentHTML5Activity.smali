.class public Lcom/peel/main/WebViewFragmentHTML5Activity;
.super Landroid/support/v4/app/ae;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Landroid/support/v4/app/ae;-><init>()V

    return-void
.end method


# virtual methods
.method public onBackPressed()V
    .locals 2

    .prologue
    .line 52
    invoke-virtual {p0}, Lcom/peel/main/WebViewFragmentHTML5Activity;->getSupportFragmentManager()Landroid/support/v4/app/aj;

    move-result-object v0

    .line 53
    if-nez v0, :cond_2

    const/4 v0, 0x0

    :goto_0
    check-cast v0, Lcom/peel/ui/nj;

    check-cast v0, Lcom/peel/ui/nj;

    .line 54
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/peel/ui/nj;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 55
    :cond_0
    invoke-virtual {p0}, Lcom/peel/main/WebViewFragmentHTML5Activity;->finish()V

    .line 57
    :cond_1
    return-void

    .line 53
    :cond_2
    sget v1, Lcom/peel/ui/fp;->webcontent:I

    invoke-virtual {v0, v1}, Landroid/support/v4/app/aj;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    const/16 v5, 0x1770

    const/16 v3, 0x400

    const/4 v0, 0x1

    .line 20
    invoke-super {p0, p1}, Landroid/support/v4/app/ae;->onCreate(Landroid/os/Bundle;)V

    .line 22
    invoke-virtual {p0}, Lcom/peel/main/WebViewFragmentHTML5Activity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 23
    if-nez v1, :cond_0

    .line 24
    invoke-virtual {p0}, Lcom/peel/main/WebViewFragmentHTML5Activity;->finish()V

    .line 49
    :goto_0
    return-void

    .line 29
    :cond_0
    invoke-virtual {p0, v0}, Lcom/peel/main/WebViewFragmentHTML5Activity;->requestWindowFeature(I)Z

    .line 32
    invoke-virtual {p0}, Lcom/peel/main/WebViewFragmentHTML5Activity;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-virtual {v2, v3, v3}, Landroid/view/Window;->setFlags(II)V

    .line 33
    invoke-virtual {p0}, Lcom/peel/main/WebViewFragmentHTML5Activity;->getWindow()Landroid/view/Window;

    move-result-object v2

    const/16 v3, 0x80

    invoke-virtual {v2, v3}, Landroid/view/Window;->addFlags(I)V

    .line 35
    sget v2, Lcom/peel/ui/fq;->main_webview:I

    invoke-virtual {p0, v2}, Lcom/peel/main/WebViewFragmentHTML5Activity;->setContentView(I)V

    .line 37
    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 38
    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    const-string/jumbo v3, "context_id"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v2

    if-ne v2, v5, :cond_1

    .line 39
    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    const-string/jumbo v3, "url"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 40
    invoke-static {}, Lcom/peel/util/a/f;->a()Lcom/peel/util/a/f;

    move-result-object v3

    sget-object v4, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v4}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v4

    if-nez v4, :cond_2

    :goto_1
    const/16 v4, 0x2ee4

    invoke-virtual {v3, v0, v4, v5, v2}, Lcom/peel/util/a/f;->a(IIILjava/lang/String;)V

    .line 45
    :cond_1
    invoke-virtual {p0}, Lcom/peel/main/WebViewFragmentHTML5Activity;->getSupportFragmentManager()Landroid/support/v4/app/aj;

    move-result-object v0

    .line 46
    invoke-virtual {v0}, Landroid/support/v4/app/aj;->a()Landroid/support/v4/app/ax;

    move-result-object v0

    .line 47
    sget v2, Lcom/peel/ui/fp;->webcontent:I

    const-class v3, Lcom/peel/ui/nj;

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    invoke-static {p0, v3, v1}, Landroid/support/v4/app/Fragment;->a(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;)Landroid/support/v4/app/Fragment;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Landroid/support/v4/app/ax;->b(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/ax;

    .line 48
    invoke-virtual {v0}, Landroid/support/v4/app/ax;->b()I

    goto :goto_0

    .line 40
    :cond_2
    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v0}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/data/at;->f()I

    move-result v0

    goto :goto_1
.end method
