.class public Lcom/peel/main/ar;
.super Landroid/content/BroadcastReceiver;


# instance fields
.field final synthetic a:Lcom/peel/main/Home;


# direct methods
.method public constructor <init>(Lcom/peel/main/Home;)V
    .locals 0

    .prologue
    .line 1651
    iput-object p1, p0, Lcom/peel/main/ar;->a:Lcom/peel/main/Home;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, -0x1

    const/4 v2, 0x1

    .line 1653
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 1655
    const-string/jumbo v1, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1656
    iget-object v0, p0, Lcom/peel/main/ar;->a:Lcom/peel/main/Home;

    invoke-static {v0}, Lcom/peel/main/Home;->e(Lcom/peel/main/Home;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1657
    const-string/jumbo v0, "noConnectivity"

    invoke-virtual {p2, v0, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    .line 1658
    if-nez v0, :cond_3

    .line 1659
    const-string/jumbo v0, "networkType"

    invoke-virtual {p2, v0, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 1660
    if-le v1, v3, :cond_0

    .line 1661
    const-string/jumbo v0, "connectivity"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 1662
    if-eqz v0, :cond_0

    .line 1663
    invoke-virtual {v0, v1}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v0

    .line 1664
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1665
    invoke-static {}, Lcom/peel/d/i;->a()Lcom/peel/d/i;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/main/ar;->a:Lcom/peel/main/Home;

    invoke-static {v0, v1, v2}, Lcom/peel/util/bx;->a(Lcom/peel/d/i;Landroid/content/Context;Z)V

    .line 1666
    invoke-static {}, Lcom/peel/util/a/f;->a()Lcom/peel/util/a/f;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/util/a/f;->d()V

    .line 1679
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/peel/main/ar;->a:Lcom/peel/main/Home;

    invoke-static {v0, v4}, Lcom/peel/main/Home;->a(Lcom/peel/main/Home;Z)Z

    .line 1681
    :cond_1
    return-void

    .line 1669
    :cond_2
    sput-boolean v2, Lcom/peel/util/b/a;->a:Z

    .line 1670
    iget-object v0, p0, Lcom/peel/main/ar;->a:Lcom/peel/main/Home;

    iget-object v1, p0, Lcom/peel/main/ar;->a:Lcom/peel/main/Home;

    invoke-static {v0, v1}, Lcom/peel/util/bx;->a(Landroid/content/Context;Landroid/support/v4/app/ae;)V

    goto :goto_0

    .line 1675
    :cond_3
    sput-boolean v2, Lcom/peel/util/b/a;->a:Z

    .line 1676
    iget-object v0, p0, Lcom/peel/main/ar;->a:Lcom/peel/main/Home;

    iget-object v1, p0, Lcom/peel/main/ar;->a:Lcom/peel/main/Home;

    invoke-static {v0, v1}, Lcom/peel/util/bx;->a(Landroid/content/Context;Landroid/support/v4/app/ae;)V

    goto :goto_0
.end method
