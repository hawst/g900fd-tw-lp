.class Lcom/peel/main/ah;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/peel/main/Home;


# direct methods
.method constructor <init>(Lcom/peel/main/Home;)V
    .locals 0

    .prologue
    .line 267
    iput-object p1, p0, Lcom/peel/main/ah;->a:Lcom/peel/main/Home;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 270
    iget-object v0, p0, Lcom/peel/main/ah;->a:Lcom/peel/main/Home;

    invoke-static {v0}, Lcom/peel/d/e;->a(Landroid/support/v4/app/ae;)Lcom/peel/d/u;

    move-result-object v3

    .line 272
    if-nez v3, :cond_2

    const-string/jumbo v0, "contentFragment NULL"

    .line 273
    :goto_0
    invoke-static {}, Lcom/peel/main/Home;->c()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 277
    iget-object v0, p0, Lcom/peel/main/ah;->a:Lcom/peel/main/Home;

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string/jumbo v4, "setup_type"

    invoke-interface {v0, v4, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    if-ne v1, v0, :cond_3

    move v0, v1

    .line 278
    :goto_1
    if-eqz v3, :cond_a

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    const-class v5, Lcom/peel/ui/gt;

    invoke-virtual {v5}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_a

    .line 280
    sget-object v4, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v4}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v4

    invoke-virtual {v4}, Lcom/peel/control/RoomControl;->c()[Lcom/peel/control/a;

    move-result-object v4

    if-eqz v4, :cond_0

    sget-object v4, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v4}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v4

    invoke-virtual {v4}, Lcom/peel/control/RoomControl;->c()[Lcom/peel/control/a;

    move-result-object v4

    array-length v4, v4

    if-nez v4, :cond_a

    .line 281
    :cond_0
    if-eqz v0, :cond_7

    .line 283
    sget-object v0, Lcom/peel/content/a;->f:Lcom/peel/content/user/User;

    invoke-virtual {v0}, Lcom/peel/content/user/User;->k()Z

    move-result v0

    if-nez v0, :cond_4

    .line 332
    :cond_1
    :goto_2
    return-void

    .line 272
    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "contentFragment: "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_3
    move v0, v2

    .line 277
    goto :goto_1

    .line 284
    :cond_4
    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v0}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/data/at;->b()Ljava/lang/String;

    move-result-object v0

    .line 285
    sget-object v4, Lcom/peel/content/a;->f:Lcom/peel/content/user/User;

    invoke-virtual {v4}, Lcom/peel/content/user/User;->l()[Lcom/peel/data/ContentRoom;

    move-result-object v4

    array-length v5, v4

    :goto_3
    if-ge v2, v5, :cond_6

    aget-object v6, v4, v2

    .line 286
    invoke-virtual {v6}, Lcom/peel/data/ContentRoom;->a()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 285
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 287
    :cond_5
    invoke-virtual {v6}, Lcom/peel/data/ContentRoom;->a()Ljava/lang/String;

    move-result-object v0

    new-instance v2, Lcom/peel/main/ai;

    invoke-direct {v2, p0, v1}, Lcom/peel/main/ai;-><init>(Lcom/peel/main/ah;I)V

    invoke-static {v0, v1, v1, v2}, Lcom/peel/content/a;->a(Ljava/lang/String;ZZLcom/peel/util/t;)V

    :cond_6
    move v0, v1

    .line 325
    :goto_4
    if-eqz v0, :cond_1

    .line 326
    if-eqz v3, :cond_1

    .line 327
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 328
    const-string/jumbo v2, "refresh"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 329
    iget-object v1, p0, Lcom/peel/main/ah;->a:Lcom/peel/main/Home;

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lcom/peel/d/e;->d(Landroid/support/v4/app/ae;Ljava/lang/String;Landroid/os/Bundle;)V

    goto :goto_2

    .line 299
    :cond_7
    iget-object v0, p0, Lcom/peel/main/ah;->a:Lcom/peel/main/Home;

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string/jumbo v4, "country_ISO"

    const-string/jumbo v5, "US"

    invoke-interface {v0, v4, v5}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 300
    const-string/jumbo v4, "US"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 302
    const-string/jumbo v0, "live"

    invoke-static {v0}, Lcom/peel/content/a;->c(Ljava/lang/String;)Lcom/peel/content/library/Library;

    move-result-object v0

    check-cast v0, Lcom/peel/content/library/LiveLibrary;

    .line 303
    if-eqz v0, :cond_8

    .line 304
    invoke-virtual {v0}, Lcom/peel/content/library/LiveLibrary;->e()Ljava/lang/String;

    move-result-object v0

    new-instance v4, Lcom/peel/main/aj;

    invoke-direct {v4, p0}, Lcom/peel/main/aj;-><init>(Lcom/peel/main/ah;)V

    invoke-static {v0, v4}, Lcom/peel/util/al;->a(Ljava/lang/String;Lcom/peel/util/t;)V

    .line 313
    :cond_8
    sget-boolean v0, Lcom/peel/util/a;->g:Z

    if-eqz v0, :cond_9

    .line 314
    iget-object v0, p0, Lcom/peel/main/ah;->a:Lcom/peel/main/Home;

    invoke-static {v0}, Lcom/peel/main/Home;->b(Lcom/peel/main/Home;)Lcom/peel/d/i;

    move-result-object v0

    iget-object v4, p0, Lcom/peel/main/ah;->a:Lcom/peel/main/Home;

    invoke-virtual {v4}, Lcom/peel/main/Home;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v0, v4}, Lcom/peel/util/bx;->b(Lcom/peel/d/i;Landroid/content/Context;)V

    :goto_5
    move v0, v2

    .line 321
    goto :goto_4

    .line 316
    :cond_9
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 317
    const-string/jumbo v4, "control_only_mode"

    invoke-virtual {v0, v4, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 318
    iget-object v4, p0, Lcom/peel/main/ah;->a:Lcom/peel/main/Home;

    const-class v5, Lcom/peel/ui/lq;

    invoke-virtual {v5}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5, v0}, Lcom/peel/d/e;->a(Landroid/support/v4/app/ae;Ljava/lang/String;Landroid/os/Bundle;)V

    goto :goto_5

    :cond_a
    move v0, v1

    goto :goto_4
.end method
