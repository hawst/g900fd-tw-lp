.class Lcom/peel/main/al;
.super Landroid/support/v4/app/a;


# instance fields
.field final synthetic a:Lcom/peel/main/Home;


# direct methods
.method constructor <init>(Lcom/peel/main/Home;Landroid/app/Activity;Landroid/support/v4/widget/DrawerLayout;III)V
    .locals 6

    .prologue
    .line 470
    iput-object p1, p0, Lcom/peel/main/al;->a:Lcom/peel/main/Home;

    move-object v0, p0

    move-object v1, p2

    move-object v2, p3

    move v3, p4

    move v4, p5

    move v5, p6

    invoke-direct/range {v0 .. v5}, Landroid/support/v4/app/a;-><init>(Landroid/app/Activity;Landroid/support/v4/widget/DrawerLayout;III)V

    return-void
.end method


# virtual methods
.method public onDrawerClosed(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 473
    iget-object v0, p0, Lcom/peel/main/al;->a:Lcom/peel/main/Home;

    invoke-static {v0}, Lcom/peel/d/e;->a(Landroid/support/v4/app/ae;)Lcom/peel/d/u;

    move-result-object v3

    .line 474
    if-eqz v3, :cond_0

    iget-object v0, p0, Lcom/peel/main/al;->a:Lcom/peel/main/Home;

    invoke-static {v0}, Lcom/peel/main/Home;->b(Lcom/peel/main/Home;)Lcom/peel/d/i;

    move-result-object v0

    if-nez v0, :cond_1

    .line 490
    :cond_0
    :goto_0
    return-void

    .line 476
    :cond_1
    invoke-virtual {v3}, Lcom/peel/d/u;->X()Z

    move-result v0

    if-nez v0, :cond_3

    move v0, v1

    .line 477
    :goto_1
    iget-object v4, p0, Lcom/peel/main/al;->a:Lcom/peel/main/Home;

    invoke-static {v4}, Lcom/peel/main/Home;->c(Lcom/peel/main/Home;)Landroid/support/v4/widget/DrawerLayout;

    move-result-object v4

    if-eqz v0, :cond_4

    :goto_2
    invoke-virtual {v4, v1}, Landroid/support/v4/widget/DrawerLayout;->setDrawerLockMode(I)V

    .line 479
    iget-object v0, p0, Lcom/peel/main/al;->a:Lcom/peel/main/Home;

    invoke-virtual {v0}, Lcom/peel/main/Home;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x10

    invoke-virtual {v0, v1}, Landroid/view/Window;->setSoftInputMode(I)V

    .line 481
    iget-object v0, p0, Lcom/peel/main/al;->a:Lcom/peel/main/Home;

    const-string/jumbo v1, "input_method"

    invoke-virtual {v0, v1}, Lcom/peel/main/Home;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 482
    if-eqz v0, :cond_2

    iget-object v1, p0, Lcom/peel/main/al;->a:Lcom/peel/main/Home;

    invoke-static {v1}, Lcom/peel/main/Home;->c(Lcom/peel/main/Home;)Landroid/support/v4/widget/DrawerLayout;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/widget/DrawerLayout;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 484
    :cond_2
    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    const-class v1, Lcom/peel/ui/gm;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    const-class v1, Lcom/peel/i/gm;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 485
    iget-object v0, p0, Lcom/peel/main/al;->a:Lcom/peel/main/Home;

    invoke-virtual {v0}, Lcom/peel/main/Home;->invalidateOptionsMenu()V

    .line 486
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 487
    const-string/jumbo v0, "drawer_open"

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 488
    iget-object v0, p0, Lcom/peel/main/al;->a:Lcom/peel/main/Home;

    invoke-virtual {v0}, Lcom/peel/main/Home;->getSupportFragmentManager()Landroid/support/v4/app/aj;

    move-result-object v0

    sget v2, Lcom/peel/ui/fp;->drawer:I

    invoke-virtual {v0, v2}, Landroid/support/v4/app/aj;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/peel/d/u;

    invoke-virtual {v0, v1}, Lcom/peel/d/u;->c(Landroid/os/Bundle;)V

    goto/16 :goto_0

    :cond_3
    move v0, v2

    .line 476
    goto :goto_1

    :cond_4
    move v1, v2

    .line 477
    goto :goto_2
.end method

.method public onDrawerOpened(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 494
    iget-object v0, p0, Lcom/peel/main/al;->a:Lcom/peel/main/Home;

    invoke-static {v0}, Lcom/peel/main/Home;->c(Lcom/peel/main/Home;)Landroid/support/v4/widget/DrawerLayout;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/DrawerLayout;->setDrawerLockMode(I)V

    .line 496
    iget-object v0, p0, Lcom/peel/main/al;->a:Lcom/peel/main/Home;

    invoke-virtual {v0}, Lcom/peel/main/Home;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x30

    invoke-virtual {v0, v1}, Landroid/view/Window;->setSoftInputMode(I)V

    .line 497
    iget-object v0, p0, Lcom/peel/main/al;->a:Lcom/peel/main/Home;

    invoke-virtual {v0}, Lcom/peel/main/Home;->invalidateOptionsMenu()V

    .line 499
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 500
    const-string/jumbo v0, "drawer_open"

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 501
    iget-object v0, p0, Lcom/peel/main/al;->a:Lcom/peel/main/Home;

    invoke-virtual {v0}, Lcom/peel/main/Home;->getSupportFragmentManager()Landroid/support/v4/app/aj;

    move-result-object v0

    sget v2, Lcom/peel/ui/fp;->drawer:I

    invoke-virtual {v0, v2}, Landroid/support/v4/app/aj;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/peel/d/u;

    invoke-virtual {v0, v1}, Lcom/peel/d/u;->c(Landroid/os/Bundle;)V

    .line 502
    return-void
.end method
