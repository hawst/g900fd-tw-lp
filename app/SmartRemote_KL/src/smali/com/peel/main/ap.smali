.class Lcom/peel/main/ap;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;


# instance fields
.field final synthetic a:Lcom/peel/main/Home;


# direct methods
.method constructor <init>(Lcom/peel/main/Home;)V
    .locals 0

    .prologue
    .line 555
    iput-object p1, p0, Lcom/peel/main/ap;->a:Lcom/peel/main/Home;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onSharedPreferenceChanged(Landroid/content/SharedPreferences;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 558
    const-string/jumbo v0, "is_setup_complete"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 559
    const-string/jumbo v0, "is_setup_complete"

    const/4 v1, 0x0

    invoke-interface {p1, v0, v1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 560
    iget-object v0, p0, Lcom/peel/main/ap;->a:Lcom/peel/main/Home;

    invoke-virtual {v0}, Lcom/peel/main/Home;->b()V

    .line 572
    :cond_0
    :goto_0
    return-void

    .line 562
    :cond_1
    sget-object v0, Lcom/peel/util/a;->k:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 563
    const-string/jumbo v0, "Setup"

    iget-object v1, p0, Lcom/peel/main/ap;->a:Lcom/peel/main/Home;

    .line 564
    invoke-virtual {v1}, Lcom/peel/main/Home;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    new-instance v2, Lcom/peel/main/aq;

    invoke-direct {v2, p0}, Lcom/peel/main/aq;-><init>(Lcom/peel/main/ap;)V

    .line 563
    invoke-static {v0, v1, v2}, Lcom/peel/util/a;->a(Ljava/lang/String;Landroid/content/Context;Lcom/peel/util/t;)V

    goto :goto_0
.end method
