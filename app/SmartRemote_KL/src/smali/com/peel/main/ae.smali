.class Lcom/peel/main/ae;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/peel/main/Home;


# direct methods
.method constructor <init>(Lcom/peel/main/Home;)V
    .locals 0

    .prologue
    .line 202
    iput-object p1, p0, Lcom/peel/main/ae;->a:Lcom/peel/main/Home;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 205
    iget-object v2, p0, Lcom/peel/main/ae;->a:Lcom/peel/main/Home;

    invoke-static {v2}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v2

    const-string/jumbo v3, "is_setup_complete"

    invoke-interface {v2, v3, v1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 206
    invoke-static {}, Lcom/peel/util/a/f;->a()Lcom/peel/util/a/f;

    move-result-object v2

    const-wide/16 v4, 0x7530

    invoke-virtual {v2, v4, v5}, Lcom/peel/util/a/f;->a(J)V

    .line 208
    iget-object v2, p0, Lcom/peel/main/ae;->a:Lcom/peel/main/Home;

    invoke-static {v2}, Lcom/peel/main/Home;->a(Lcom/peel/main/Home;)Landroid/content/SharedPreferences;

    move-result-object v2

    const-string/jumbo v3, "setup_type"

    invoke-interface {v2, v3, v1}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v2

    if-ne v0, v2, :cond_0

    move v1, v0

    .line 209
    :cond_0
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 210
    sget-object v3, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v3}, Lcom/peel/control/am;->e()[Lcom/peel/control/h;

    move-result-object v3

    .line 211
    if-eqz v1, :cond_3

    if-eqz v3, :cond_1

    array-length v3, v3

    if-nez v3, :cond_3

    .line 213
    :cond_1
    const-string/jumbo v1, "room_with_no_devices"

    invoke-virtual {v2, v1, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 214
    iget-object v0, p0, Lcom/peel/main/ae;->a:Lcom/peel/main/Home;

    const-class v1, Lcom/peel/i/ai;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v2}, Lcom/peel/d/e;->a(Landroid/support/v4/app/ae;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 262
    :cond_2
    :goto_0
    return-void

    .line 217
    :cond_3
    const-string/jumbo v0, "control_only_mode"

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 219
    if-nez v1, :cond_4

    .line 221
    iget-object v0, p0, Lcom/peel/main/ae;->a:Lcom/peel/main/Home;

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string/jumbo v3, "country_ISO"

    const-string/jumbo v4, "US"

    invoke-interface {v0, v3, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 222
    const-string/jumbo v3, "US"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 224
    const-string/jumbo v0, "live"

    invoke-static {v0}, Lcom/peel/content/a;->c(Ljava/lang/String;)Lcom/peel/content/library/Library;

    move-result-object v0

    check-cast v0, Lcom/peel/content/library/LiveLibrary;

    .line 225
    if-eqz v0, :cond_4

    .line 226
    invoke-virtual {v0}, Lcom/peel/content/library/LiveLibrary;->e()Ljava/lang/String;

    move-result-object v0

    new-instance v3, Lcom/peel/main/af;

    invoke-direct {v3, p0}, Lcom/peel/main/af;-><init>(Lcom/peel/main/ae;)V

    invoke-static {v0, v3}, Lcom/peel/util/al;->a(Ljava/lang/String;Lcom/peel/util/t;)V

    .line 237
    :cond_4
    if-eqz v1, :cond_5

    .line 238
    iget-object v0, p0, Lcom/peel/main/ae;->a:Lcom/peel/main/Home;

    const-class v3, Lcom/peel/ui/gt;

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3, v2}, Lcom/peel/d/e;->a(Landroid/support/v4/app/ae;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 245
    :goto_1
    if-nez v1, :cond_2

    sget-boolean v0, Lcom/peel/util/a;->h:Z

    if-eqz v0, :cond_2

    .line 246
    const-string/jumbo v0, "tuneinoptions"

    iget-object v1, p0, Lcom/peel/main/ae;->a:Lcom/peel/main/Home;

    .line 247
    invoke-virtual {v1}, Lcom/peel/main/Home;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    new-instance v2, Lcom/peel/main/ag;

    invoke-direct {v2, p0}, Lcom/peel/main/ag;-><init>(Lcom/peel/main/ae;)V

    .line 246
    invoke-static {v0, v1, v2}, Lcom/peel/util/a;->a(Ljava/lang/String;Landroid/content/Context;Lcom/peel/util/t;)V

    goto :goto_0

    .line 239
    :cond_5
    sget-boolean v0, Lcom/peel/util/a;->g:Z

    if-eqz v0, :cond_6

    .line 240
    iget-object v0, p0, Lcom/peel/main/ae;->a:Lcom/peel/main/Home;

    invoke-static {v0}, Lcom/peel/main/Home;->b(Lcom/peel/main/Home;)Lcom/peel/d/i;

    move-result-object v0

    iget-object v2, p0, Lcom/peel/main/ae;->a:Lcom/peel/main/Home;

    invoke-virtual {v2}, Lcom/peel/main/Home;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/peel/util/bx;->b(Lcom/peel/d/i;Landroid/content/Context;)V

    goto :goto_1

    .line 242
    :cond_6
    iget-object v0, p0, Lcom/peel/main/ae;->a:Lcom/peel/main/Home;

    const-class v3, Lcom/peel/ui/lq;

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3, v2}, Lcom/peel/d/e;->a(Landroid/support/v4/app/ae;Ljava/lang/String;Landroid/os/Bundle;)V

    goto :goto_1

    .line 259
    :cond_7
    iget-object v0, p0, Lcom/peel/main/ae;->a:Lcom/peel/main/Home;

    const-class v1, Lcom/peel/i/fv;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/peel/d/e;->a(Landroid/support/v4/app/ae;Ljava/lang/String;Landroid/os/Bundle;)V

    goto/16 :goto_0
.end method
