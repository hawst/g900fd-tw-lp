.class public Lcom/peel/main/GoogleLoginActivity;
.super Landroid/app/Activity;

# interfaces
.implements Lcom/google/android/gms/common/api/GoogleApiClient$ConnectionCallbacks;
.implements Lcom/google/android/gms/common/api/GoogleApiClient$OnConnectionFailedListener;


# static fields
.field public static a:Ljava/lang/String;

.field public static b:Ljava/lang/String;


# instance fields
.field c:I

.field d:Z

.field private e:Lcom/google/android/gms/common/api/GoogleApiClient;

.field private f:Z

.field private g:Lcom/google/android/gms/common/ConnectionResult;

.field private h:Z

.field private i:Z

.field private j:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 51
    const-string/jumbo v0, "logout_access"

    sput-object v0, Lcom/peel/main/GoogleLoginActivity;->a:Ljava/lang/String;

    .line 52
    const-string/jumbo v0, "revoke_access"

    sput-object v0, Lcom/peel/main/GoogleLoginActivity;->b:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 38
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 61
    const/16 v0, 0x5a

    iput v0, p0, Lcom/peel/main/GoogleLoginActivity;->c:I

    return-void
.end method

.method static synthetic a(Lcom/peel/main/GoogleLoginActivity;)Lcom/google/android/gms/common/api/GoogleApiClient;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/peel/main/GoogleLoginActivity;->e:Lcom/google/android/gms/common/api/GoogleApiClient;

    return-object v0
.end method

.method private b()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 112
    iget-object v0, p0, Lcom/peel/main/GoogleLoginActivity;->g:Lcom/google/android/gms/common/ConnectionResult;

    invoke-virtual {v0}, Lcom/google/android/gms/common/ConnectionResult;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 114
    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/peel/main/GoogleLoginActivity;->f:Z

    .line 115
    iget-object v0, p0, Lcom/peel/main/GoogleLoginActivity;->g:Lcom/google/android/gms/common/ConnectionResult;

    const/4 v1, 0x0

    invoke-virtual {v0, p0, v1}, Lcom/google/android/gms/common/ConnectionResult;->a(Landroid/app/Activity;I)V
    :try_end_0
    .catch Landroid/content/IntentSender$SendIntentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 121
    :cond_0
    :goto_0
    return-void

    .line 116
    :catch_0
    move-exception v0

    .line 117
    iput-boolean v2, p0, Lcom/peel/main/GoogleLoginActivity;->f:Z

    .line 118
    iget-object v0, p0, Lcom/peel/main/GoogleLoginActivity;->e:Lcom/google/android/gms/common/api/GoogleApiClient;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/GoogleApiClient;->a()V

    goto :goto_0
.end method

.method private c()V
    .locals 2

    .prologue
    .line 263
    iget-object v0, p0, Lcom/peel/main/GoogleLoginActivity;->e:Lcom/google/android/gms/common/api/GoogleApiClient;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/GoogleApiClient;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 264
    sget-object v0, Lcom/google/android/gms/plus/Plus;->g:Lcom/google/android/gms/plus/Account;

    iget-object v1, p0, Lcom/peel/main/GoogleLoginActivity;->e:Lcom/google/android/gms/common/api/GoogleApiClient;

    invoke-interface {v0, v1}, Lcom/google/android/gms/plus/Account;->b(Lcom/google/android/gms/common/api/GoogleApiClient;)V

    .line 265
    iget-object v0, p0, Lcom/peel/main/GoogleLoginActivity;->e:Lcom/google/android/gms/common/api/GoogleApiClient;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/GoogleApiClient;->b()V

    .line 266
    iget-object v0, p0, Lcom/peel/main/GoogleLoginActivity;->e:Lcom/google/android/gms/common/api/GoogleApiClient;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/GoogleApiClient;->a()V

    .line 268
    :cond_0
    return-void
.end method

.method private d()V
    .locals 2

    .prologue
    .line 274
    iget-object v0, p0, Lcom/peel/main/GoogleLoginActivity;->e:Lcom/google/android/gms/common/api/GoogleApiClient;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/GoogleApiClient;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 275
    sget-object v0, Lcom/google/android/gms/plus/Plus;->g:Lcom/google/android/gms/plus/Account;

    iget-object v1, p0, Lcom/peel/main/GoogleLoginActivity;->e:Lcom/google/android/gms/common/api/GoogleApiClient;

    invoke-interface {v0, v1}, Lcom/google/android/gms/plus/Account;->b(Lcom/google/android/gms/common/api/GoogleApiClient;)V

    .line 276
    sget-object v0, Lcom/google/android/gms/plus/Plus;->g:Lcom/google/android/gms/plus/Account;

    iget-object v1, p0, Lcom/peel/main/GoogleLoginActivity;->e:Lcom/google/android/gms/common/api/GoogleApiClient;

    invoke-interface {v0, v1}, Lcom/google/android/gms/plus/Account;->a(Lcom/google/android/gms/common/api/GoogleApiClient;)Lcom/google/android/gms/common/api/PendingResult;

    move-result-object v0

    new-instance v1, Lcom/peel/main/e;

    invoke-direct {v1, p0}, Lcom/peel/main/e;-><init>(Lcom/peel/main/GoogleLoginActivity;)V

    .line 277
    invoke-interface {v0, v1}, Lcom/google/android/gms/common/api/PendingResult;->a(Lcom/google/android/gms/common/api/ResultCallback;)V

    .line 286
    :cond_0
    return-void
.end method


# virtual methods
.method public a()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 202
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string/jumbo v2, "profile"

    aput-object v2, v0, v1

    const-string/jumbo v1, "https://www.googleapis.com/auth/plus.login"

    aput-object v1, v0, v3

    const/4 v1, 0x2

    const-string/jumbo v2, "https://www.googleapis.com/auth/plus.me"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string/jumbo v2, "https://www.googleapis.com/auth/plus.circles.read"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string/jumbo v2, "https://www.googleapis.com/auth/plus.profiles.read"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string/jumbo v2, "https://www.googleapis.com/auth/userinfo.email"

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    .line 203
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "oauth2:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " "

    invoke-static {v2, v0}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 206
    :try_start_0
    sget-object v1, Lcom/google/android/gms/plus/Plus;->g:Lcom/google/android/gms/plus/Account;

    iget-object v2, p0, Lcom/peel/main/GoogleLoginActivity;->e:Lcom/google/android/gms/common/api/GoogleApiClient;

    invoke-interface {v1, v2}, Lcom/google/android/gms/plus/Account;->c(Lcom/google/android/gms/common/api/GoogleApiClient;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1, v0}, Lcom/google/android/gms/auth/GoogleAuthUtil;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 208
    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 209
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 210
    sget-object v2, Lcom/peel/social/w;->h:Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 211
    sget v0, Lcom/peel/social/w;->b:I

    invoke-virtual {p0, v0, v1}, Lcom/peel/main/GoogleLoginActivity;->setResult(ILandroid/content/Intent;)V

    .line 212
    invoke-virtual {p0}, Lcom/peel/main/GoogleLoginActivity;->finish()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Lcom/google/android/gms/auth/UserRecoverableAuthException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/google/android/gms/auth/GoogleAuthException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 227
    :cond_0
    :goto_0
    return-void

    .line 216
    :catch_0
    move-exception v0

    .line 217
    iget-boolean v1, p0, Lcom/peel/main/GoogleLoginActivity;->d:Z

    if-nez v1, :cond_0

    .line 218
    invoke-virtual {v0}, Lcom/google/android/gms/auth/UserRecoverableAuthException;->a()Landroid/content/Intent;

    move-result-object v0

    iget v1, p0, Lcom/peel/main/GoogleLoginActivity;->c:I

    invoke-virtual {p0, v0, v1}, Lcom/peel/main/GoogleLoginActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 219
    iput-boolean v3, p0, Lcom/peel/main/GoogleLoginActivity;->d:Z

    goto :goto_0

    .line 224
    :catch_1
    move-exception v0

    .line 225
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 222
    :catch_2
    move-exception v0

    goto :goto_0

    .line 214
    :catch_3
    move-exception v0

    goto :goto_0
.end method

.method public a(I)V
    .locals 1

    .prologue
    .line 247
    iget-object v0, p0, Lcom/peel/main/GoogleLoginActivity;->e:Lcom/google/android/gms/common/api/GoogleApiClient;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/GoogleApiClient;->a()V

    .line 248
    return-void
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 171
    iget-boolean v0, p0, Lcom/peel/main/GoogleLoginActivity;->h:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/peel/main/GoogleLoginActivity;->i:Z

    if-nez v0, :cond_0

    .line 172
    new-instance v0, Lcom/peel/main/d;

    invoke-direct {v0, p0}, Lcom/peel/main/d;-><init>(Lcom/peel/main/GoogleLoginActivity;)V

    .line 178
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 180
    :cond_0
    iget-boolean v0, p0, Lcom/peel/main/GoogleLoginActivity;->h:Z

    if-eqz v0, :cond_1

    .line 181
    invoke-direct {p0}, Lcom/peel/main/GoogleLoginActivity;->c()V

    .line 182
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 183
    sget-object v1, Lcom/peel/main/GoogleLoginActivity;->a:Ljava/lang/String;

    const-string/jumbo v2, "true"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 184
    sget v1, Lcom/peel/social/w;->b:I

    invoke-virtual {p0, v1, v0}, Lcom/peel/main/GoogleLoginActivity;->setResult(ILandroid/content/Intent;)V

    .line 185
    invoke-virtual {p0}, Lcom/peel/main/GoogleLoginActivity;->finish()V

    .line 187
    :cond_1
    iget-boolean v0, p0, Lcom/peel/main/GoogleLoginActivity;->i:Z

    if-eqz v0, :cond_2

    .line 188
    invoke-direct {p0}, Lcom/peel/main/GoogleLoginActivity;->d()V

    .line 189
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 190
    sget-object v1, Lcom/peel/main/GoogleLoginActivity;->b:Ljava/lang/String;

    const-string/jumbo v2, "true"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 191
    sget v1, Lcom/peel/social/w;->b:I

    invoke-virtual {p0, v1, v0}, Lcom/peel/main/GoogleLoginActivity;->setResult(ILandroid/content/Intent;)V

    .line 192
    invoke-virtual {p0}, Lcom/peel/main/GoogleLoginActivity;->finish()V

    .line 197
    :cond_2
    return-void
.end method

.method public a(Lcom/google/android/gms/common/ConnectionResult;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 125
    iget-boolean v0, p0, Lcom/peel/main/GoogleLoginActivity;->j:Z

    if-eqz v0, :cond_0

    .line 126
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 127
    sget-object v1, Lcom/peel/main/GoogleLoginActivity;->a:Ljava/lang/String;

    const-string/jumbo v2, "true"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 128
    sget v1, Lcom/peel/social/w;->b:I

    invoke-virtual {p0, v1, v0}, Lcom/peel/main/GoogleLoginActivity;->setResult(ILandroid/content/Intent;)V

    .line 129
    invoke-virtual {p0}, Lcom/peel/main/GoogleLoginActivity;->finish()V

    .line 131
    :cond_0
    invoke-virtual {p1}, Lcom/google/android/gms/common/ConnectionResult;->a()Z

    move-result v0

    if-nez v0, :cond_2

    .line 132
    invoke-virtual {p1}, Lcom/google/android/gms/common/ConnectionResult;->c()I

    move-result v0

    invoke-static {v0, p0, v3}, Lcom/google/android/gms/common/GooglePlayServicesUtil;->a(ILandroid/app/Activity;I)Landroid/app/Dialog;

    move-result-object v0

    .line 133
    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    .line 152
    :cond_1
    :goto_0
    return-void

    .line 137
    :cond_2
    iget-boolean v0, p0, Lcom/peel/main/GoogleLoginActivity;->f:Z

    if-nez v0, :cond_1

    .line 139
    iput-object p1, p0, Lcom/peel/main/GoogleLoginActivity;->g:Lcom/google/android/gms/common/ConnectionResult;

    .line 141
    const-string/jumbo v0, "social_accounts_setup"

    invoke-virtual {p0, v0, v3}, Lcom/peel/main/GoogleLoginActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 142
    const-string/jumbo v1, "google_access_token"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 144
    if-nez v0, :cond_1

    .line 148
    invoke-direct {p0}, Lcom/peel/main/GoogleLoginActivity;->b()V

    goto :goto_0
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 157
    if-nez p1, :cond_0

    .line 158
    iput-boolean v1, p0, Lcom/peel/main/GoogleLoginActivity;->f:Z

    .line 160
    iget-object v0, p0, Lcom/peel/main/GoogleLoginActivity;->e:Lcom/google/android/gms/common/api/GoogleApiClient;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/GoogleApiClient;->d()Z

    move-result v0

    if-nez v0, :cond_0

    .line 161
    iget-object v0, p0, Lcom/peel/main/GoogleLoginActivity;->e:Lcom/google/android/gms/common/api/GoogleApiClient;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/GoogleApiClient;->a()V

    .line 164
    :cond_0
    iget v0, p0, Lcom/peel/main/GoogleLoginActivity;->c:I

    if-ne p1, v0, :cond_1

    .line 165
    iput-boolean v1, p0, Lcom/peel/main/GoogleLoginActivity;->d:Z

    .line 167
    :cond_1
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v0, 0x0

    .line 69
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 70
    invoke-virtual {p0, v4}, Lcom/peel/main/GoogleLoginActivity;->requestWindowFeature(I)Z

    .line 71
    new-instance v1, Landroid/widget/RelativeLayout;

    invoke-direct {v1, p0}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    invoke-virtual {p0, v1}, Lcom/peel/main/GoogleLoginActivity;->setContentView(Landroid/view/View;)V

    .line 73
    invoke-virtual {p0}, Lcom/peel/main/GoogleLoginActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 74
    invoke-virtual {v1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {v1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/peel/main/GoogleLoginActivity;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 75
    iput-boolean v4, p0, Lcom/peel/main/GoogleLoginActivity;->h:Z

    .line 83
    :goto_0
    invoke-virtual {p0}, Lcom/peel/main/GoogleLoginActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    sget-object v2, Lcom/peel/social/w;->i:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/peel/main/GoogleLoginActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    sget-object v2, Lcom/peel/social/w;->i:Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    :cond_0
    iput-boolean v0, p0, Lcom/peel/main/GoogleLoginActivity;->j:Z

    .line 86
    new-instance v0, Lcom/google/android/gms/common/api/GoogleApiClient$Builder;

    invoke-direct {v0, p0}, Lcom/google/android/gms/common/api/GoogleApiClient$Builder;-><init>(Landroid/content/Context;)V

    .line 87
    invoke-virtual {v0, p0}, Lcom/google/android/gms/common/api/GoogleApiClient$Builder;->a(Lcom/google/android/gms/common/api/GoogleApiClient$ConnectionCallbacks;)Lcom/google/android/gms/common/api/GoogleApiClient$Builder;

    move-result-object v0

    .line 88
    invoke-virtual {v0, p0}, Lcom/google/android/gms/common/api/GoogleApiClient$Builder;->a(Lcom/google/android/gms/common/api/GoogleApiClient$OnConnectionFailedListener;)Lcom/google/android/gms/common/api/GoogleApiClient$Builder;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/plus/Plus;->b:Lcom/google/android/gms/common/api/Api;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/common/api/GoogleApiClient$Builder;->a(Lcom/google/android/gms/common/api/Api;Lcom/google/android/gms/common/api/GoogleApiClient$ApiOptions;)Lcom/google/android/gms/common/api/GoogleApiClient$Builder;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/plus/Plus;->c:Lcom/google/android/gms/common/api/Scope;

    .line 89
    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/api/GoogleApiClient$Builder;->a(Lcom/google/android/gms/common/api/Scope;)Lcom/google/android/gms/common/api/GoogleApiClient$Builder;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/plus/Plus;->d:Lcom/google/android/gms/common/api/Scope;

    .line 90
    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/api/GoogleApiClient$Builder;->a(Lcom/google/android/gms/common/api/Scope;)Lcom/google/android/gms/common/api/GoogleApiClient$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/GoogleApiClient$Builder;->b()Lcom/google/android/gms/common/api/GoogleApiClient;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/main/GoogleLoginActivity;->e:Lcom/google/android/gms/common/api/GoogleApiClient;

    .line 91
    return-void

    .line 76
    :cond_1
    invoke-virtual {v1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-virtual {v1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/peel/main/GoogleLoginActivity;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 77
    iput-boolean v4, p0, Lcom/peel/main/GoogleLoginActivity;->i:Z

    goto :goto_0

    .line 79
    :cond_2
    iput-boolean v0, p0, Lcom/peel/main/GoogleLoginActivity;->h:Z

    .line 80
    iput-boolean v0, p0, Lcom/peel/main/GoogleLoginActivity;->i:Z

    goto :goto_0
.end method

.method protected onStart()V
    .locals 1

    .prologue
    .line 95
    invoke-super {p0}, Landroid/app/Activity;->onStart()V

    .line 96
    iget-object v0, p0, Lcom/peel/main/GoogleLoginActivity;->e:Lcom/google/android/gms/common/api/GoogleApiClient;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/GoogleApiClient;->a()V

    .line 98
    return-void
.end method

.method protected onStop()V
    .locals 1

    .prologue
    .line 102
    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    .line 103
    iget-object v0, p0, Lcom/peel/main/GoogleLoginActivity;->e:Lcom/google/android/gms/common/api/GoogleApiClient;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/GoogleApiClient;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 104
    iget-object v0, p0, Lcom/peel/main/GoogleLoginActivity;->e:Lcom/google/android/gms/common/api/GoogleApiClient;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/GoogleApiClient;->b()V

    .line 106
    :cond_0
    return-void
.end method
