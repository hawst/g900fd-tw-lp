.class public Lcom/peel/main/Home;
.super Lcom/peel/util/a/d;


# static fields
.field public static c:[I

.field private static final d:Ljava/lang/String;

.field private static final p:[I

.field private static r:Z


# instance fields
.field public a:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

.field b:Landroid/widget/Toast;

.field private e:Lcom/peel/d/i;

.field private final f:Lcom/peel/main/ar;

.field private g:Landroid/content/SharedPreferences;

.field private h:Landroid/content/SharedPreferences;

.field private i:Lcom/peel/util/au;

.field private j:Ljava/util/Timer;

.field private k:Ljava/util/TimerTask;

.field private l:Landroid/support/v4/app/a;

.field private m:Landroid/support/v4/widget/DrawerLayout;

.field private n:Z

.field private o:Lcom/peel/social/a/f;

.field private q:Lcom/peel/util/s;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 118
    const-class v0, Lcom/peel/main/Home;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/peel/main/Home;->d:Ljava/lang/String;

    .line 349
    const/4 v0, 0x7

    new-array v0, v0, [I

    sget v1, Lcom/peel/ui/ft;->sunday:I

    aput v1, v0, v3

    sget v1, Lcom/peel/ui/ft;->monday:I

    aput v1, v0, v4

    sget v1, Lcom/peel/ui/ft;->tuesday:I

    aput v1, v0, v5

    sget v1, Lcom/peel/ui/ft;->wednesday:I

    aput v1, v0, v6

    sget v1, Lcom/peel/ui/ft;->thursday:I

    aput v1, v0, v7

    const/4 v1, 0x5

    sget v2, Lcom/peel/ui/ft;->friday:I

    aput v2, v0, v1

    const/4 v1, 0x6

    sget v2, Lcom/peel/ui/ft;->saturday:I

    aput v2, v0, v1

    sput-object v0, Lcom/peel/main/Home;->p:[I

    .line 350
    new-array v0, v7, [I

    sget v1, Lcom/peel/ui/ft;->yesterday:I

    aput v1, v0, v3

    sget v1, Lcom/peel/ui/ft;->today:I

    aput v1, v0, v4

    sget v1, Lcom/peel/ui/ft;->tomorrow:I

    aput v1, v0, v5

    sget v1, Lcom/peel/ui/ft;->onrightnow:I

    aput v1, v0, v6

    sput-object v0, Lcom/peel/main/Home;->c:[I

    .line 1490
    sput-boolean v3, Lcom/peel/main/Home;->r:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 117
    invoke-direct {p0}, Lcom/peel/util/a/d;-><init>()V

    .line 121
    new-instance v0, Lcom/peel/main/ar;

    invoke-direct {v0, p0}, Lcom/peel/main/ar;-><init>(Lcom/peel/main/Home;)V

    iput-object v0, p0, Lcom/peel/main/Home;->f:Lcom/peel/main/ar;

    .line 131
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/peel/main/Home;->n:Z

    .line 1283
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/peel/main/Home;->q:Lcom/peel/util/s;

    .line 1651
    return-void
.end method

.method static synthetic a(Lcom/peel/main/Home;)Landroid/content/SharedPreferences;
    .locals 1

    .prologue
    .line 117
    iget-object v0, p0, Lcom/peel/main/Home;->g:Landroid/content/SharedPreferences;

    return-object v0
.end method

.method private a(Lcom/peel/control/RoomControl;Lcom/peel/d/i;)Lcom/peel/control/a;
    .locals 11

    .prologue
    const/4 v0, 0x0

    const/4 v3, 0x0

    .line 1421
    invoke-virtual {p1}, Lcom/peel/control/RoomControl;->d()Lcom/peel/control/a;

    move-result-object v1

    .line 1422
    if-eqz v1, :cond_0

    const/4 v2, 0x2

    invoke-virtual {v1}, Lcom/peel/control/a;->f()I

    move-result v4

    if-ne v2, v4, :cond_0

    .line 1423
    sget-object v0, Lcom/peel/main/Home;->d:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "activity already started"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v1}, Lcom/peel/control/a;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v1

    .line 1456
    :goto_0
    return-object v0

    .line 1428
    :cond_0
    invoke-virtual {p1}, Lcom/peel/control/RoomControl;->c()[Lcom/peel/control/a;

    move-result-object v5

    .line 1429
    if-nez v5, :cond_1

    .line 1430
    sget-object v1, Lcom/peel/main/Home;->d:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "no activities for room "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v3

    invoke-virtual {v3}, Lcom/peel/data/at;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1435
    :cond_1
    iget-object v1, p0, Lcom/peel/main/Home;->g:Landroid/content/SharedPreferences;

    const-string/jumbo v2, "last_activity"

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 1436
    if-eqz v4, :cond_3

    .line 1438
    array-length v6, v5

    move v2, v3

    :goto_1
    if-ge v2, v6, :cond_3

    aget-object v1, v5, v2

    .line 1439
    invoke-virtual {v1}, Lcom/peel/control/a;->b()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_2

    move-object v0, v1

    goto :goto_0

    .line 1438
    :cond_2
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_1

    .line 1444
    :cond_3
    array-length v6, v5

    move v4, v3

    :goto_2
    if-ge v4, v6, :cond_7

    aget-object v1, v5, v4

    .line 1445
    invoke-virtual {v1}, Lcom/peel/control/a;->d()[Ljava/lang/String;

    move-result-object v7

    .line 1446
    if-nez v7, :cond_5

    .line 1444
    :cond_4
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    goto :goto_2

    .line 1447
    :cond_5
    array-length v8, v7

    move v2, v3

    :goto_3
    if-ge v2, v8, :cond_4

    aget-object v9, v7, v2

    .line 1448
    const-string/jumbo v10, "live"

    invoke-virtual {v10, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_6

    move-object v0, v1

    goto :goto_0

    .line 1447
    :cond_6
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 1453
    :cond_7
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "no live activity returning first entry"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    aget-object v2, v5, v3

    if-nez v2, :cond_8

    :goto_4
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1454
    sget-object v1, Lcom/peel/main/Home;->d:Ljava/lang/String;

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1456
    aget-object v0, v5, v3

    goto/16 :goto_0

    .line 1453
    :cond_8
    aget-object v0, v5, v3

    invoke-virtual {v0}, Lcom/peel/control/a;->a()Ljava/lang/String;

    move-result-object v0

    goto :goto_4
.end method

.method static synthetic a(Lcom/peel/main/Home;Lcom/peel/util/s;)Lcom/peel/util/s;
    .locals 0

    .prologue
    .line 117
    iput-object p1, p0, Lcom/peel/main/Home;->q:Lcom/peel/util/s;

    return-object p1
.end method

.method private a(Ljava/lang/String;)Ljava/lang/String;
    .locals 11

    .prologue
    const/4 v7, 0x1

    const/4 v2, 0x0

    const/4 v0, 0x0

    .line 1203
    if-nez p1, :cond_1

    .line 1204
    sget-object v1, Lcom/peel/main/Home;->d:Ljava/lang/String;

    const-string/jumbo v2, "deviceid is null, return"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1238
    :cond_0
    :goto_0
    return-object v0

    .line 1208
    :cond_1
    sget-object v1, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v1}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v1

    if-nez v1, :cond_2

    .line 1209
    sget-object v1, Lcom/peel/main/Home;->d:Ljava/lang/String;

    const-string/jumbo v2, "current room is null, return"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1213
    :cond_2
    sget-object v1, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v1}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/control/RoomControl;->c()[Lcom/peel/control/a;

    move-result-object v4

    .line 1214
    if-eqz v4, :cond_3

    array-length v1, v4

    if-nez v1, :cond_4

    .line 1215
    :cond_3
    sget-object v1, Lcom/peel/main/Home;->d:Ljava/lang/String;

    const-string/jumbo v2, "no activities, return"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1222
    :cond_4
    array-length v3, v4

    move v1, v2

    :goto_1
    if-ge v1, v3, :cond_6

    aget-object v5, v4, v1

    .line 1223
    invoke-virtual {v5, v7}, Lcom/peel/control/a;->a(I)Lcom/peel/control/h;

    move-result-object v6

    if-eqz v6, :cond_5

    invoke-virtual {v5, v7}, Lcom/peel/control/a;->a(I)Lcom/peel/control/h;

    move-result-object v6

    invoke-virtual {v6}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v6

    invoke-virtual {v6}, Lcom/peel/data/g;->b()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 1224
    invoke-virtual {v5}, Lcom/peel/control/a;->b()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1222
    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1228
    :cond_6
    array-length v5, v4

    move v3, v2

    :goto_2
    if-ge v3, v5, :cond_0

    aget-object v6, v4, v3

    .line 1229
    invoke-virtual {v6}, Lcom/peel/control/a;->d()[Ljava/lang/String;

    move-result-object v7

    .line 1230
    if-nez v7, :cond_8

    .line 1228
    :cond_7
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_2

    .line 1232
    :cond_8
    array-length v8, v7

    move v1, v2

    :goto_3
    if-ge v1, v8, :cond_7

    aget-object v9, v7, v1

    .line 1233
    const-string/jumbo v10, "live"

    invoke-virtual {v10, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_9

    .line 1234
    invoke-virtual {v6}, Lcom/peel/control/a;->b()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1232
    :cond_9
    add-int/lit8 v1, v1, 0x1

    goto :goto_3
.end method

.method static synthetic a(Lcom/peel/main/Home;Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 117
    invoke-direct {p0, p1, p2}, Lcom/peel/main/Home;->a(Ljava/lang/String;Landroid/os/Bundle;)V

    return-void
.end method

.method private a(Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 12

    .prologue
    const/16 v3, 0x1770

    const/4 v5, 0x0

    .line 1156
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string/jumbo v1, "is_setup_complete"

    invoke-interface {v0, v1, v5}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1157
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 1158
    const-string/jumbo v1, "id"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1159
    invoke-static {p2}, Lcom/peel/util/bx;->a(Landroid/os/Bundle;)V

    .line 1160
    if-eqz p2, :cond_4

    .line 1161
    const-string/jumbo v1, "starttime"

    const-string/jumbo v2, "starttime"

    invoke-virtual {p2, v2}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v6

    invoke-virtual {v0, v1, v6, v7}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 1162
    const-string/jumbo v1, "show_id"

    const-string/jumbo v2, "show_id"

    invoke-virtual {p2, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1163
    const-string/jumbo v1, "provider"

    const-string/jumbo v2, "provider"

    invoke-virtual {p2, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1164
    const-string/jumbo v1, "channel_name"

    const-string/jumbo v2, "channel_name"

    invoke-virtual {p2, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1165
    const-string/jumbo v1, "channel_number"

    const-string/jumbo v2, "channel_number"

    invoke-virtual {p2, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1166
    const-string/jumbo v1, "channel_id"

    const-string/jumbo v2, "channel_id"

    invoke-virtual {p2, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1167
    const-string/jumbo v1, "action"

    const-string/jumbo v2, "action"

    invoke-virtual {p2, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1168
    const-string/jumbo v1, "context_id"

    invoke-virtual {v0, v1, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1169
    const-string/jumbo v1, "tracking_url_on_wot"

    invoke-virtual {p2, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1170
    const-string/jumbo v1, "tracking_url_on_wot"

    const-string/jumbo v2, "tracking_url_on_wot"

    invoke-virtual {p2, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1172
    :cond_0
    const-string/jumbo v1, "video"

    invoke-virtual {p2, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1173
    const-string/jumbo v1, "video"

    const-string/jumbo v2, "video"

    invoke-virtual {p2, v2}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 1175
    :cond_1
    const-string/jumbo v1, "tracking_url"

    invoke-virtual {p2, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1176
    const-string/jumbo v1, "tracking_url"

    const-string/jumbo v2, "tracking_url"

    invoke-virtual {p2, v2}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 1178
    :cond_2
    const-string/jumbo v1, "campaign_message"

    invoke-virtual {p2, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1179
    const-string/jumbo v1, "campaign_message"

    const-string/jumbo v2, "campaign_message"

    invoke-virtual {p2, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1181
    :cond_3
    const-string/jumbo v1, "banner"

    invoke-virtual {p2, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 1182
    const-string/jumbo v1, "banner"

    const-string/jumbo v2, "banner"

    invoke-virtual {p2, v2}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 1185
    :cond_4
    iget-object v1, p0, Lcom/peel/main/Home;->e:Lcom/peel/d/i;

    const-string/jumbo v2, "slideout_selected_id"

    const/16 v4, -0x64

    invoke-virtual {v1, v2, v4}, Lcom/peel/d/i;->b(Ljava/lang/String;I)V

    .line 1186
    const-class v1, Lcom/peel/ui/b/av;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1, v0}, Lcom/peel/d/e;->a(Landroid/support/v4/app/ae;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 1191
    :goto_0
    if-eqz p2, :cond_5

    const-string/jumbo v0, "context_id"

    invoke-virtual {p2, v0, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    if-ne v0, v3, :cond_5

    .line 1192
    invoke-static {}, Lcom/peel/util/a/f;->a()Lcom/peel/util/a/f;

    move-result-object v0

    sget-object v1, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v1}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v1

    if-nez v1, :cond_7

    const/4 v1, 0x1

    :goto_1
    const/16 v2, 0x2ee4

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "type"

    const-string/jumbo v7, ""

    .line 1194
    invoke-virtual {p2, v6, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v6, "|"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v6, "actions"

    const-string/jumbo v7, ""

    invoke-virtual {p2, v6, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const-string/jumbo v6, "show_id"

    const-string/jumbo v7, ""

    .line 1195
    invoke-virtual {p2, v6, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    const-string/jumbo v7, "episode_id"

    const-string/jumbo v8, ""

    .line 1196
    invoke-virtual {p2, v7, v8}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    const-string/jumbo v7, "show_title"

    const-string/jumbo v9, ""

    .line 1197
    invoke-virtual {p2, v7, v9}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    move v7, v5

    move v9, v5

    move v11, v5

    .line 1192
    invoke-virtual/range {v0 .. v11}, Lcom/peel/util/a/f;->a(IIILjava/lang/String;ILjava/lang/String;ILjava/lang/String;ILjava/lang/String;I)V

    .line 1199
    :cond_5
    return-void

    .line 1188
    :cond_6
    const-class v0, Lcom/peel/i/fv;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Lcom/peel/d/e;->a(Landroid/support/v4/app/ae;Ljava/lang/String;Landroid/os/Bundle;)V

    goto :goto_0

    .line 1192
    :cond_7
    sget-object v1, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v1}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/data/at;->f()I

    move-result v1

    goto :goto_1
.end method

.method private a(Landroid/content/Intent;)Z
    .locals 17

    .prologue
    .line 897
    sget-object v2, Lcom/peel/content/a;->h:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v2

    if-eqz v2, :cond_0

    if-nez p1, :cond_2

    :cond_0
    const/4 v14, 0x0

    .line 1152
    :cond_1
    :goto_0
    return v14

    .line 898
    :cond_2
    const/4 v14, 0x0

    .line 899
    invoke-static/range {p0 .. p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v15

    .line 902
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "\n\n********** intent extras: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual/range {p1 .. p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    if-nez v2, :cond_8

    const-string/jumbo v2, "NULL"

    :goto_1
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 903
    sget-object v3, Lcom/peel/main/Home;->d:Ljava/lang/String;

    invoke-static {v3, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 907
    invoke-virtual/range {p1 .. p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    if-eqz v2, :cond_5

    .line 909
    invoke-virtual/range {p1 .. p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    const-string/jumbo v3, "noti_event_id"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 910
    invoke-virtual/range {p1 .. p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    const-string/jumbo v3, "noti_event_id"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    .line 911
    invoke-virtual/range {p1 .. p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v3

    const-string/jumbo v4, "episode_id"

    const-string/jumbo v5, ""

    invoke-virtual {v3, v4, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 912
    invoke-virtual/range {p1 .. p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v3

    const-string/jumbo v4, "show_id"

    const-string/jumbo v5, ""

    invoke-virtual {v3, v4, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 913
    packed-switch v2, :pswitch_data_0

    .line 927
    :cond_3
    :goto_2
    :pswitch_0
    invoke-virtual/range {p1 .. p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    const-string/jumbo v3, "context_id"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 928
    invoke-virtual/range {p1 .. p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    const-string/jumbo v3, "context_id"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    .line 930
    const/16 v3, 0x1770

    if-ne v2, v3, :cond_4

    .line 931
    invoke-virtual/range {p1 .. p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v4

    .line 932
    if-nez v4, :cond_b

    const/4 v2, 0x0

    move-object v3, v2

    .line 933
    :goto_3
    if-nez v4, :cond_c

    const/4 v2, 0x0

    .line 935
    :goto_4
    const-string/jumbo v4, "tunein"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_4

    if-eqz v2, :cond_d

    const-string/jumbo v3, "tunein"

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_d

    .line 947
    :cond_4
    :goto_5
    invoke-virtual/range {p1 .. p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    const-string/jumbo v3, "from"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 948
    invoke-static {}, Lcom/peel/util/a/f;->a()Lcom/peel/util/a/f;

    move-result-object v2

    const/4 v3, 0x1

    const/16 v4, 0x440

    const/16 v5, 0x1770

    const/4 v7, -0x1

    invoke-virtual/range {v2 .. v7}, Lcom/peel/util/a/f;->a(IIILjava/lang/String;I)V

    .line 953
    :cond_5
    const-string/jumbo v2, "android.intent.action.SEARCH"

    invoke-virtual/range {p1 .. p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_f

    .line 954
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 955
    const-string/jumbo v2, "type"

    const/4 v4, 0x5

    invoke-virtual {v3, v2, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 956
    const-string/jumbo v2, "query"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 957
    if-eqz v2, :cond_6

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v4

    if-nez v4, :cond_7

    :cond_6
    const-string/jumbo v2, "user_query"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 958
    :cond_7
    const-string/jumbo v4, "keyword"

    invoke-virtual {v3, v4, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 959
    const-string/jumbo v2, "useExactSearch"

    const/4 v4, 0x1

    invoke-virtual {v3, v2, v4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 960
    const-string/jumbo v2, "addToBackStack"

    const/4 v4, 0x1

    invoke-virtual {v3, v2, v4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 961
    const/4 v14, 0x1

    .line 962
    const-class v2, Lcom/peel/ui/gm;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-static {v0, v2, v3}, Lcom/peel/d/e;->b(Landroid/support/v4/app/ae;Ljava/lang/String;Landroid/os/Bundle;)V

    goto/16 :goto_0

    .line 902
    :cond_8
    invoke-virtual/range {p1 .. p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    const-string/jumbo v4, "context_id"

    invoke-virtual {v2, v4}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    goto/16 :goto_1

    .line 915
    :pswitch_1
    invoke-static {}, Lcom/peel/util/a/f;->a()Lcom/peel/util/a/f;

    move-result-object v2

    sget-object v3, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v3}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v3

    if-nez v3, :cond_9

    const/4 v3, 0x1

    :goto_6
    const/16 v4, 0x2eea

    const/16 v5, 0x1771

    const/4 v7, 0x0

    const/4 v9, 0x0

    invoke-virtual/range {v2 .. v9}, Lcom/peel/util/a/f;->a(IIILjava/lang/String;ILjava/lang/String;I)V

    goto/16 :goto_2

    :cond_9
    sget-object v3, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v3}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v3

    invoke-virtual {v3}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v3

    invoke-virtual {v3}, Lcom/peel/data/at;->f()I

    move-result v3

    goto :goto_6

    .line 919
    :pswitch_2
    invoke-static {}, Lcom/peel/util/a/f;->a()Lcom/peel/util/a/f;

    move-result-object v2

    sget-object v3, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v3}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v3

    if-nez v3, :cond_a

    const/4 v3, 0x1

    :goto_7
    const/16 v4, 0x2eec

    const/16 v5, 0x1771

    const/4 v7, 0x0

    const/4 v9, 0x0

    invoke-virtual/range {v2 .. v9}, Lcom/peel/util/a/f;->a(IIILjava/lang/String;ILjava/lang/String;I)V

    goto/16 :goto_2

    :cond_a
    sget-object v3, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v3}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v3

    invoke-virtual {v3}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v3

    invoke-virtual {v3}, Lcom/peel/data/at;->f()I

    move-result v3

    goto :goto_7

    .line 932
    :cond_b
    invoke-virtual {v4}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v2

    move-object v3, v2

    goto/16 :goto_3

    .line 933
    :cond_c
    invoke-virtual {v4}, Landroid/net/Uri;->getEncodedPath()Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_4

    .line 938
    :cond_d
    invoke-static {}, Lcom/peel/util/a/f;->a()Lcom/peel/util/a/f;

    move-result-object v2

    sget-object v3, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v3}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v3

    if-nez v3, :cond_e

    const/4 v3, 0x1

    :goto_8
    const/16 v4, 0x2ee4

    const/16 v5, 0x1770

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    .line 940
    invoke-virtual/range {p1 .. p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v7

    const-string/jumbo v8, "type"

    const-string/jumbo v9, ""

    invoke-virtual {v7, v8, v9}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, "|"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual/range {p1 .. p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v7

    const-string/jumbo v8, "actions"

    const-string/jumbo v9, ""

    invoke-virtual {v7, v8, v9}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    .line 941
    invoke-virtual/range {p1 .. p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v8

    const-string/jumbo v9, "show_id"

    const-string/jumbo v10, ""

    invoke-virtual {v8, v9, v10}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x0

    .line 942
    invoke-virtual/range {p1 .. p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v10

    const-string/jumbo v11, "episode_id"

    const-string/jumbo v12, ""

    invoke-virtual {v10, v11, v12}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    const/4 v11, 0x0

    .line 943
    invoke-virtual/range {p1 .. p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v12

    const-string/jumbo v13, "show_title"

    const-string/jumbo v16, ""

    move-object/from16 v0, v16

    invoke-virtual {v12, v13, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    const/4 v13, 0x0

    .line 938
    invoke-virtual/range {v2 .. v13}, Lcom/peel/util/a/f;->a(IIILjava/lang/String;ILjava/lang/String;ILjava/lang/String;ILjava/lang/String;I)V

    goto/16 :goto_5

    :cond_e
    sget-object v3, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v3}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v3

    invoke-virtual {v3}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v3

    invoke-virtual {v3}, Lcom/peel/data/at;->f()I

    move-result v3

    goto :goto_8

    .line 963
    :cond_f
    const-string/jumbo v2, "peel"

    invoke-virtual/range {p1 .. p1}, Landroid/content/Intent;->getScheme()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_10

    const-string/jumbo v2, "http"

    invoke-virtual/range {p1 .. p1}, Landroid/content/Intent;->getScheme()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_10
    const-string/jumbo v2, "is_setup_complete"

    const/4 v3, 0x0

    invoke-interface {v15, v2, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 964
    invoke-virtual/range {p1 .. p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v12

    .line 965
    if-nez v12, :cond_13

    const/4 v2, 0x0

    move-object v11, v2

    .line 966
    :goto_9
    if-nez v12, :cond_14

    const/4 v2, 0x0

    move-object v10, v2

    .line 967
    :goto_a
    if-nez v12, :cond_15

    const/4 v2, 0x0

    move-object v3, v2

    .line 969
    :goto_b
    sget-object v2, Lcom/peel/main/Home;->d:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "\n\n ******* host: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, " -- path: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 971
    const-string/jumbo v2, "programs"

    invoke-virtual {v2, v11}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1c

    .line 972
    if-eqz v10, :cond_2e

    .line 973
    invoke-virtual/range {p1 .. p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    if-eqz v2, :cond_18

    invoke-virtual/range {p1 .. p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    const-string/jumbo v3, "listing"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_18

    .line 974
    invoke-static/range {p0 .. p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v2

    const-string/jumbo v3, "is_setup_complete"

    const/4 v4, 0x0

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_2e

    .line 975
    const/4 v2, 0x1

    invoke-static/range {p0 .. p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v3

    const-string/jumbo v4, "setup_type"

    const/4 v5, 0x0

    invoke-interface {v3, v4, v5}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v3

    if-ne v2, v3, :cond_16

    const/4 v2, 0x1

    .line 976
    :goto_c
    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    .line 977
    const-string/jumbo v3, "control_only_mode"

    invoke-virtual {v4, v3, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 978
    if-eqz v2, :cond_17

    const-class v3, Lcom/peel/ui/gt;

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    :goto_d
    move-object/from16 v0, p0

    invoke-static {v0, v3, v4}, Lcom/peel/d/e;->a(Landroid/support/v4/app/ae;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 980
    if-nez v2, :cond_11

    .line 981
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 982
    const-string/jumbo v3, "listing"

    invoke-virtual/range {p1 .. p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v4

    const-string/jumbo v5, "listing"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 983
    const-string/jumbo v3, "needsgrouping"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 984
    const-string/jumbo v3, "fromschedules"

    const/4 v4, 0x1

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 985
    const-string/jumbo v3, "autotune"

    invoke-virtual/range {p1 .. p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v4

    const-string/jumbo v5, "autotune"

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 986
    const-class v3, Lcom/peel/ui/b/ag;

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    invoke-static {v0, v3, v2}, Lcom/peel/d/e;->c(Landroid/support/v4/app/ae;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 988
    :cond_11
    const/4 v2, 0x1

    move v3, v2

    .line 1027
    :goto_e
    const-string/jumbo v2, "notification"

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/peel/main/Home;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/NotificationManager;

    const/16 v4, 0x1ca6

    invoke-virtual {v2, v4}, Landroid/app/NotificationManager;->cancel(I)V

    move v14, v3

    .line 1139
    :cond_12
    :goto_f
    if-nez v14, :cond_1

    const-string/jumbo v2, "home"

    invoke-virtual {v2, v11}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1142
    :try_start_0
    invoke-virtual/range {p1 .. p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    if-eqz v2, :cond_1

    const/16 v2, 0x1770

    invoke-virtual/range {p1 .. p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v3

    const-string/jumbo v4, "context_id"

    const/4 v5, -0x1

    invoke-virtual {v3, v4, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v3

    if-ne v2, v3, :cond_1

    .line 1143
    invoke-static {}, Lcom/peel/util/a/f;->a()Lcom/peel/util/a/f;

    move-result-object v2

    const/4 v3, 0x1

    const/16 v4, 0x2ee1

    const/4 v5, 0x0

    invoke-virtual/range {p1 .. p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v6

    const-string/jumbo v7, "messageid"

    const-string/jumbo v8, "messageid"

    invoke-virtual {v6, v7, v8}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    invoke-virtual/range {p1 .. p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v8

    const-string/jumbo v9, "url"

    invoke-virtual {v8, v9}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x0

    invoke-virtual/range {v2 .. v9}, Lcom/peel/util/a/f;->a(IIILjava/lang/String;ILjava/lang/String;I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 1146
    :catch_0
    move-exception v2

    .line 1147
    sget-object v3, Lcom/peel/main/Home;->d:Ljava/lang/String;

    sget-object v4, Lcom/peel/main/Home;->d:Ljava/lang/String;

    invoke-static {v3, v4, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_0

    .line 965
    :cond_13
    invoke-virtual {v12}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v2

    move-object v11, v2

    goto/16 :goto_9

    .line 966
    :cond_14
    invoke-virtual {v12}, Landroid/net/Uri;->getEncodedPath()Ljava/lang/String;

    move-result-object v2

    move-object v10, v2

    goto/16 :goto_a

    .line 967
    :cond_15
    invoke-virtual {v12}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v2

    move-object v3, v2

    goto/16 :goto_b

    .line 975
    :cond_16
    const/4 v2, 0x0

    goto/16 :goto_c

    .line 978
    :cond_17
    const-class v3, Lcom/peel/ui/lq;

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_d

    .line 991
    :cond_18
    const-string/jumbo v2, "/"

    invoke-virtual {v10, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 992
    if-eqz v4, :cond_2e

    array-length v2, v4

    if-lez v2, :cond_2e

    .line 993
    invoke-static/range {p0 .. p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v2

    const-string/jumbo v3, "is_setup_complete"

    const/4 v5, 0x0

    invoke-interface {v2, v3, v5}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_2e

    .line 994
    const/4 v2, 0x1

    invoke-static/range {p0 .. p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v3

    const-string/jumbo v5, "setup_type"

    const/4 v6, 0x0

    invoke-interface {v3, v5, v6}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v3

    if-ne v2, v3, :cond_1a

    const/4 v2, 0x1

    .line 995
    :goto_10
    new-instance v5, Landroid/os/Bundle;

    invoke-direct {v5}, Landroid/os/Bundle;-><init>()V

    .line 996
    const-string/jumbo v3, "control_only_mode"

    invoke-virtual {v5, v3, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 997
    if-eqz v2, :cond_1b

    const-class v3, Lcom/peel/ui/gt;

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    :goto_11
    move-object/from16 v0, p0

    invoke-static {v0, v3, v5}, Lcom/peel/d/e;->a(Landroid/support/v4/app/ae;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 999
    if-nez v2, :cond_19

    .line 1000
    const-string/jumbo v2, "live"

    .line 1001
    invoke-static {v2}, Lcom/peel/content/a;->c(Ljava/lang/String;)Lcom/peel/content/library/Library;

    move-result-object v2

    check-cast v2, Lcom/peel/content/library/LiveLibrary;

    .line 1002
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 1003
    const-string/jumbo v5, "libraryIds"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    invoke-virtual {v2}, Lcom/peel/content/library/LiveLibrary;->e()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-virtual {v3, v5, v6}, Landroid/os/Bundle;->putStringArray(Ljava/lang/String;[Ljava/lang/String;)V

    .line 1004
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "listings/"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v2}, Lcom/peel/content/library/LiveLibrary;->e()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "live://"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    .line 1006
    invoke-virtual {v2}, Lcom/peel/content/library/LiveLibrary;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v8, "/"

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    array-length v8, v4

    add-int/lit8 v8, v8, -0x1

    aget-object v4, v4, v8

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v6, v7

    .line 1004
    invoke-virtual {v3, v5, v6}, Landroid/os/Bundle;->putStringArray(Ljava/lang/String;[Ljava/lang/String;)V

    .line 1007
    const-string/jumbo v2, "needsgrouping"

    const/4 v4, 0x0

    invoke-virtual {v3, v2, v4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1008
    const-class v2, Lcom/peel/ui/b/ag;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-static {v0, v2, v3}, Lcom/peel/d/e;->c(Landroid/support/v4/app/ae;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 1011
    :cond_19
    const/4 v2, 0x1

    move v3, v2

    goto/16 :goto_e

    .line 994
    :cond_1a
    const/4 v2, 0x0

    goto/16 :goto_10

    .line 997
    :cond_1b
    const-class v3, Lcom/peel/ui/lq;

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_11

    .line 1028
    :cond_1c
    const-string/jumbo v2, "tunein"

    invoke-virtual {v2, v11}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1e

    if-eqz v10, :cond_1d

    const-string/jumbo v2, "tunein"

    invoke-virtual {v10, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1e

    :cond_1d
    const-string/jumbo v2, "peel.in"

    invoke-virtual {v2, v11}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_23

    .line 1030
    :cond_1e
    invoke-virtual/range {p1 .. p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    if-eqz v2, :cond_1f

    invoke-virtual/range {p1 .. p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    const-string/jumbo v3, "tracking_url_on_click"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1f

    .line 1031
    invoke-virtual/range {p1 .. p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    const-string/jumbo v3, "tracking_url_on_click"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/peel/util/bq;->a(Ljava/lang/String;)V

    .line 1033
    :cond_1f
    invoke-virtual/range {p1 .. p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    if-eqz v2, :cond_20

    invoke-virtual/range {p1 .. p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    const-string/jumbo v3, "tracking_url"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_20

    invoke-virtual/range {p1 .. p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    const-string/jumbo v3, "tracking_url"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v2

    const-string/jumbo v3, "on_notification_click"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_20

    .line 1034
    invoke-virtual/range {p1 .. p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    const-string/jumbo v3, "tracking_url"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v2

    const-string/jumbo v3, "on_notification_click"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/peel/util/bq;->a(Ljava/lang/String;)V

    .line 1038
    :cond_20
    :try_start_1
    invoke-virtual/range {p1 .. p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    if-eqz v2, :cond_21

    invoke-virtual/range {p1 .. p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    const-string/jumbo v3, "from"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_21

    .line 1039
    invoke-virtual/range {p1 .. p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    const-string/jumbo v3, "from"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "Reminder"

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-result v2

    if-eqz v2, :cond_21

    .line 1049
    :goto_12
    const-string/jumbo v2, "/"

    invoke-virtual {v10, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 1050
    array-length v2, v3

    if-lez v2, :cond_12

    sget-object v2, Lcom/peel/content/a;->f:Lcom/peel/content/user/User;

    if-eqz v2, :cond_12

    .line 1051
    const-string/jumbo v2, "live"

    invoke-static {v2}, Lcom/peel/content/a;->c(Ljava/lang/String;)Lcom/peel/content/library/Library;

    move-result-object v2

    check-cast v2, Lcom/peel/content/library/LiveLibrary;

    .line 1052
    if-eqz v2, :cond_12

    .line 1053
    const/4 v14, 0x1

    .line 1055
    const-string/jumbo v2, "peel.in"

    invoke-virtual {v2, v11}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_22

    .line 1056
    invoke-virtual {v12}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/peel/main/q;

    const/4 v4, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v3, v0, v4, v1}, Lcom/peel/main/q;-><init>(Lcom/peel/main/Home;ILandroid/content/Intent;)V

    invoke-static {v2, v3}, Lcom/peel/content/a/bt;->a(Ljava/lang/String;Lcom/peel/util/t;)V

    goto/16 :goto_f

    .line 1042
    :cond_21
    :try_start_2
    invoke-static {}, Lcom/peel/util/a/f;->a()Lcom/peel/util/a/f;

    move-result-object v2

    const/4 v3, 0x1

    const/16 v4, 0x440

    .line 1043
    invoke-virtual/range {p1 .. p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v5

    const-string/jumbo v6, "context_id"

    const/16 v7, 0x1770

    invoke-virtual {v5, v6, v7}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v5

    const-string/jumbo v6, "Tune In Link"

    const/4 v7, 0x0

    .line 1044
    invoke-virtual {v12}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x0

    .line 1042
    invoke-virtual/range {v2 .. v9}, Lcom/peel/util/a/f;->a(IIILjava/lang/String;ILjava/lang/String;I)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_12

    .line 1046
    :catch_1
    move-exception v2

    .line 1047
    sget-object v3, Lcom/peel/main/Home;->d:Ljava/lang/String;

    sget-object v4, Lcom/peel/main/Home;->d:Ljava/lang/String;

    invoke-static {v3, v4, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_12

    .line 1067
    :cond_22
    array-length v2, v3

    add-int/lit8 v2, v2, -0x1

    aget-object v2, v3, v2

    invoke-virtual/range {p1 .. p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v3

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v3}, Lcom/peel/main/Home;->a(Ljava/lang/String;Landroid/os/Bundle;)V

    goto/16 :goto_f

    .line 1071
    :cond_23
    const-string/jumbo v2, "remote"

    invoke-virtual {v2, v11}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_12

    .line 1075
    if-eqz v3, :cond_29

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_29

    .line 1076
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v2

    const/4 v4, 0x2

    if-ne v2, v4, :cond_27

    .line 1078
    invoke-static {}, Lcom/peel/content/a;->a()Lcom/peel/data/ContentRoom;

    move-result-object v2

    invoke-virtual {v2}, Lcom/peel/data/ContentRoom;->a()Ljava/lang/String;

    move-result-object v4

    const/4 v2, 0x0

    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v4, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_26

    .line 1079
    sget-object v2, Lcom/peel/main/Home;->d:Ljava/lang/String;

    const-string/jumbo v4, "\n\n ******* room id matches, no switch needed"

    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1081
    const/4 v2, 0x1

    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/peel/main/Home;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1083
    if-eqz v2, :cond_24

    .line 1084
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 1085
    const-string/jumbo v4, "fromintent"

    const/4 v5, 0x1

    invoke-virtual {v3, v4, v5}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1086
    const-string/jumbo v4, "activityId"

    invoke-virtual {v3, v4, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1087
    const/4 v2, 0x1

    invoke-static/range {p0 .. p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v4

    const-string/jumbo v5, "setup_type"

    const/4 v6, 0x0

    invoke-interface {v4, v5, v6}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v4

    if-ne v2, v4, :cond_25

    const/4 v2, 0x1

    .line 1088
    :goto_13
    const-string/jumbo v4, "control_only_mode"

    invoke-virtual {v3, v4, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1089
    const-class v2, Lcom/peel/ui/gt;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-static {v0, v2, v3}, Lcom/peel/d/e;->a(Landroid/support/v4/app/ae;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 1136
    :cond_24
    :goto_14
    const/4 v14, 0x1

    goto/16 :goto_f

    .line 1087
    :cond_25
    const/4 v2, 0x0

    goto :goto_13

    .line 1092
    :cond_26
    const/4 v2, 0x0

    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    const/4 v3, 0x1

    const/4 v4, 0x1

    const/4 v5, 0x0

    invoke-static {v2, v3, v4, v5}, Lcom/peel/content/a;->a(Ljava/lang/String;ZZLcom/peel/util/t;)V

    goto :goto_14

    .line 1095
    :cond_27
    invoke-static {}, Lcom/peel/content/a;->a()Lcom/peel/data/ContentRoom;

    move-result-object v2

    invoke-virtual {v2}, Lcom/peel/data/ContentRoom;->a()Ljava/lang/String;

    move-result-object v4

    const/4 v2, 0x0

    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v4, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_28

    .line 1096
    const-class v2, Lcom/peel/main/Home;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "show device setup"

    new-instance v4, Lcom/peel/main/r;

    move-object/from16 v0, p0

    invoke-direct {v4, v0}, Lcom/peel/main/r;-><init>(Lcom/peel/main/Home;)V

    invoke-static {v2, v3, v4}, Lcom/peel/util/i;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/lang/Runnable;

    goto :goto_14

    .line 1107
    :cond_28
    const/4 v2, 0x0

    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    const/4 v3, 0x1

    const/4 v4, 0x1

    const/4 v5, 0x0

    invoke-static {v2, v3, v4, v5}, Lcom/peel/content/a;->a(Ljava/lang/String;ZZLcom/peel/util/t;)V

    goto :goto_14

    .line 1111
    :cond_29
    sget-object v2, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v2}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v2

    invoke-virtual {v2}, Lcom/peel/control/RoomControl;->g()[Ljava/lang/String;

    move-result-object v2

    .line 1112
    if-eqz v2, :cond_2b

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    const-string/jumbo v3, "live"

    invoke-interface {v2, v3}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2b

    .line 1113
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 1114
    const-string/jumbo v2, "fromintent"

    const/4 v4, 0x1

    invoke-virtual {v3, v2, v4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1115
    const/4 v2, 0x1

    invoke-static/range {p0 .. p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v4

    const-string/jumbo v5, "setup_type"

    const/4 v6, 0x0

    invoke-interface {v4, v5, v6}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v4

    if-ne v2, v4, :cond_2a

    const/4 v2, 0x1

    .line 1116
    :goto_15
    const-string/jumbo v4, "control_only_mode"

    invoke-virtual {v3, v4, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1117
    const-class v2, Lcom/peel/ui/gt;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-static {v0, v2, v3}, Lcom/peel/d/e;->a(Landroid/support/v4/app/ae;Ljava/lang/String;Landroid/os/Bundle;)V

    goto/16 :goto_14

    .line 1115
    :cond_2a
    const/4 v2, 0x0

    goto :goto_15

    .line 1119
    :cond_2b
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/peel/main/Home;->e:Lcom/peel/d/i;

    iget-boolean v2, v2, Lcom/peel/d/i;->a:Z

    if-nez v2, :cond_24

    .line 1120
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 1121
    const/4 v2, 0x1

    invoke-static/range {p0 .. p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v4

    const-string/jumbo v5, "setup_type"

    const/4 v6, 0x0

    invoke-interface {v4, v5, v6}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v4

    if-ne v2, v4, :cond_2c

    const/4 v2, 0x1

    .line 1122
    :goto_16
    const-string/jumbo v4, "control_only_mode"

    invoke-virtual {v3, v4, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1123
    const-string/jumbo v4, "passback_clazz"

    const-class v5, Lcom/peel/ui/lq;

    invoke-virtual {v5}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1124
    if-eqz v2, :cond_2d

    const-class v2, Lcom/peel/i/ai;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    :goto_17
    move-object/from16 v0, p0

    invoke-static {v0, v2, v3}, Lcom/peel/d/e;->b(Landroid/support/v4/app/ae;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 1125
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/peel/main/Home;->e:Lcom/peel/d/i;

    const/4 v3, 0x1

    iput-boolean v3, v2, Lcom/peel/d/i;->a:Z

    .line 1126
    new-instance v2, Landroid/os/Handler;

    invoke-direct {v2}, Landroid/os/Handler;-><init>()V

    new-instance v3, Lcom/peel/main/s;

    move-object/from16 v0, p0

    invoke-direct {v3, v0}, Lcom/peel/main/s;-><init>(Lcom/peel/main/Home;)V

    const-wide/16 v4, 0x7d0

    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/16 :goto_14

    .line 1121
    :cond_2c
    const/4 v2, 0x0

    goto :goto_16

    .line 1124
    :cond_2d
    const-class v2, Lcom/peel/i/gm;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    goto :goto_17

    :cond_2e
    move v3, v14

    goto/16 :goto_e

    .line 913
    :pswitch_data_0
    .packed-switch 0x2eea
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method static synthetic a(Lcom/peel/main/Home;Z)Z
    .locals 0

    .prologue
    .line 117
    iput-boolean p1, p0, Lcom/peel/main/Home;->n:Z

    return p1
.end method

.method static synthetic b(Lcom/peel/main/Home;)Lcom/peel/d/i;
    .locals 1

    .prologue
    .line 117
    iget-object v0, p0, Lcom/peel/main/Home;->e:Lcom/peel/d/i;

    return-object v0
.end method

.method static synthetic c(Lcom/peel/main/Home;)Landroid/support/v4/widget/DrawerLayout;
    .locals 1

    .prologue
    .line 117
    iget-object v0, p0, Lcom/peel/main/Home;->m:Landroid/support/v4/widget/DrawerLayout;

    return-object v0
.end method

.method static synthetic c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 117
    sget-object v0, Lcom/peel/main/Home;->d:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic d(Lcom/peel/main/Home;)Lcom/peel/util/s;
    .locals 1

    .prologue
    .line 117
    iget-object v0, p0, Lcom/peel/main/Home;->q:Lcom/peel/util/s;

    return-object v0
.end method

.method static synthetic d()Z
    .locals 1

    .prologue
    .line 117
    sget-boolean v0, Lcom/peel/main/Home;->r:Z

    return v0
.end method

.method private e()V
    .locals 6

    .prologue
    .line 1472
    const/4 v1, 0x0

    .line 1473
    :try_start_0
    invoke-virtual {p0}, Lcom/peel/main/Home;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-virtual {p0}, Lcom/peel/main/Home;->getPackageName()Ljava/lang/String;

    move-result-object v2

    const/16 v3, 0x40

    invoke-virtual {v0, v2, v3}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v0

    .line 1474
    iget-object v3, v0, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;

    array-length v4, v3

    const/4 v0, 0x0

    move-object v2, v1

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_0

    aget-object v0, v3, v1

    .line 1475
    const-string/jumbo v2, "X.509"

    invoke-static {v2}, Ljava/security/cert/CertificateFactory;->getInstance(Ljava/lang/String;)Ljava/security/cert/CertificateFactory;

    move-result-object v2

    new-instance v5, Ljava/io/ByteArrayInputStream;

    invoke-virtual {v0}, Landroid/content/pm/Signature;->toByteArray()[B

    move-result-object v0

    invoke-direct {v5, v0}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-virtual {v2, v5}, Ljava/security/cert/CertificateFactory;->generateCertificate(Ljava/io/InputStream;)Ljava/security/cert/Certificate;

    move-result-object v0

    check-cast v0, Ljava/security/cert/X509Certificate;

    .line 1476
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/security/cert/X509Certificate;->checkValidity(Ljava/util/Date;)V

    .line 1477
    invoke-virtual {v0}, Ljava/security/cert/X509Certificate;->getNotAfter()Ljava/util/Date;

    move-result-object v2

    .line 1474
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1480
    :cond_0
    if-eqz v2, :cond_1

    .line 1481
    iget-object v1, p0, Lcom/peel/main/Home;->e:Lcom/peel/d/i;

    const-string/jumbo v3, "expiry"

    sget-object v0, Lcom/peel/util/x;->b:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/text/SimpleDateFormat;

    invoke-virtual {v0, v2}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v3, v0}, Lcom/peel/d/i;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1488
    :cond_1
    :goto_1
    return-void

    .line 1483
    :catch_0
    move-exception v0

    .line 1484
    sget-object v1, Lcom/peel/main/Home;->d:Ljava/lang/String;

    sget-object v2, Lcom/peel/main/Home;->d:Ljava/lang/String;

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1485
    invoke-virtual {v0}, Ljava/lang/Exception;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 1486
    invoke-virtual {p0}, Lcom/peel/main/Home;->finish()V

    goto :goto_1
.end method

.method static synthetic e(Lcom/peel/main/Home;)Z
    .locals 1

    .prologue
    .line 117
    iget-boolean v0, p0, Lcom/peel/main/Home;->n:Z

    return v0
.end method

.method private f()V
    .locals 5

    .prologue
    .line 1492
    sget-boolean v0, Lcom/peel/util/bn;->b:Z

    if-nez v0, :cond_1

    .line 1493
    sget-object v0, Lcom/peel/c/a;->f:Lcom/peel/c/e;

    invoke-static {v0}, Lcom/peel/c/a;->a(Lcom/peel/c/e;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 1496
    :try_start_0
    invoke-virtual {p0}, Lcom/peel/main/Home;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-virtual {p0}, Lcom/peel/main/Home;->getPackageName()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v0

    .line 1497
    const-class v2, Lcom/peel/util/bx;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "\n\n####### shared user id: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, v0, Landroid/content/pm/PackageInfo;->sharedUserId:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "\n\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1498
    const-string/jumbo v2, "android.uid.system"

    iget-object v0, v0, Landroid/content/pm/PackageInfo;->sharedUserId:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    sput-boolean v0, Lcom/peel/main/Home;->r:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1502
    :cond_0
    :goto_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "http://download.peel.com/app/PeelSmartRemote_"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v2, ".apk"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1504
    sget-object v2, Lcom/peel/main/Home;->d:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "\n\n ##### BuildConfig.FLAVOR:  prod id: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1505
    new-instance v2, Lcom/peel/main/w;

    invoke-direct {v2, p0, v0}, Lcom/peel/main/w;-><init>(Lcom/peel/main/Home;Ljava/lang/String;)V

    invoke-static {p0, v1, v2}, Lcom/peel/content/a/j;->a(Landroid/content/Context;ILcom/peel/util/t;)V

    .line 1594
    :cond_1
    return-void

    .line 1499
    :catch_0
    move-exception v0

    .line 1500
    sget-object v2, Lcom/peel/main/Home;->d:Ljava/lang/String;

    sget-object v3, Lcom/peel/main/Home;->d:Ljava/lang/String;

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method private g()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1597
    iget-object v0, p0, Lcom/peel/main/Home;->j:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 1598
    iget-object v0, p0, Lcom/peel/main/Home;->k:Ljava/util/TimerTask;

    invoke-virtual {v0}, Ljava/util/TimerTask;->cancel()Z

    .line 1599
    iput-object v1, p0, Lcom/peel/main/Home;->k:Ljava/util/TimerTask;

    .line 1600
    iput-object v1, p0, Lcom/peel/main/Home;->j:Ljava/util/Timer;

    .line 1601
    return-void
.end method

.method private h()V
    .locals 6

    .prologue
    .line 1604
    iget-object v0, p0, Lcom/peel/main/Home;->j:Ljava/util/Timer;

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/peel/main/Home;->g()V

    .line 1606
    :cond_0
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, Lcom/peel/main/Home;->j:Ljava/util/Timer;

    .line 1607
    new-instance v0, Lcom/peel/main/ab;

    invoke-direct {v0, p0}, Lcom/peel/main/ab;-><init>(Lcom/peel/main/Home;)V

    iput-object v0, p0, Lcom/peel/main/Home;->k:Ljava/util/TimerTask;

    .line 1638
    iget-object v0, p0, Lcom/peel/main/Home;->e:Lcom/peel/d/i;

    const-string/jumbo v1, "time"

    invoke-virtual {v0, v1}, Lcom/peel/d/i;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1639
    if-eqz v0, :cond_1

    .line 1641
    invoke-static {v0}, Lcom/peel/util/x;->b(Ljava/lang/String;)J

    move-result-wide v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-gez v0, :cond_1

    .line 1642
    iget-object v0, p0, Lcom/peel/main/Home;->k:Ljava/util/TimerTask;

    invoke-virtual {v0}, Ljava/util/TimerTask;->run()V

    .line 1646
    :cond_1
    iget-object v0, p0, Lcom/peel/main/Home;->j:Ljava/util/Timer;

    iget-object v1, p0, Lcom/peel/main/Home;->k:Ljava/util/TimerTask;

    invoke-static {}, Lcom/peel/util/x;->d()I

    move-result v2

    int-to-long v2, v2

    const-wide/32 v4, 0x1b7740

    invoke-virtual/range {v0 .. v5}, Ljava/util/Timer;->scheduleAtFixedRate(Ljava/util/TimerTask;JJ)V

    .line 1647
    return-void
.end method


# virtual methods
.method public a()V
    .locals 3

    .prologue
    .line 338
    const-class v0, Lcom/peel/main/Home;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "ContentEvents.LOADED room_changed reload top picks"

    new-instance v2, Lcom/peel/main/ak;

    invoke-direct {v2, p0}, Lcom/peel/main/ak;-><init>(Lcom/peel/main/Home;)V

    invoke-static {v0, v1, v2}, Lcom/peel/util/i;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/lang/Runnable;

    .line 347
    return-void
.end method

.method public a(ZZ)V
    .locals 11

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 135
    invoke-virtual {p0}, Lcom/peel/main/Home;->getIntent()Landroid/content/Intent;

    move-result-object v7

    .line 136
    if-eqz p2, :cond_6

    .line 138
    const-string/jumbo v2, "c583c7c46eef455992a6846c81573f02"

    const-string/jumbo v3, "6f5d1afa224b4b2c93666d1405795725"

    invoke-static {v2, v3}, Lcom/peel/util/b/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 139
    const-string/jumbo v2, "52297d5efe084068ad24d018a1fd861e"

    const-string/jumbo v3, "bc663646f2e3418e97dbab9ec1db26fa"

    invoke-static {v2, v3}, Lcom/peel/util/b/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 140
    invoke-static {p0}, Lcom/peel/util/dq;->c(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 141
    invoke-static {p0}, Lcom/peel/content/a/d;->a(Landroid/content/Context;)V

    .line 144
    :cond_0
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v2

    const-string/jumbo v3, "country_ISO"

    const-string/jumbo v4, "US"

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 145
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v3

    const-string/jumbo v4, "is_setup_complete"

    invoke-interface {v3, v4, v1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    .line 146
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v4

    const-string/jumbo v5, "setup_type"

    invoke-interface {v4, v5, v1}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v4

    if-ne v0, v4, :cond_5

    .line 148
    :goto_0
    sget-boolean v4, Lcom/peel/util/a;->i:Z

    if-eqz v4, :cond_1

    const-string/jumbo v4, "US"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    if-nez v0, :cond_1

    if-eqz v3, :cond_1

    .line 149
    const-string/jumbo v2, "remotechannelguide"

    invoke-virtual {p0}, Lcom/peel/main/Home;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    new-instance v4, Lcom/peel/main/f;

    invoke-direct {v4, p0}, Lcom/peel/main/f;-><init>(Lcom/peel/main/Home;)V

    invoke-static {v2, v3, v4}, Lcom/peel/util/a;->a(Ljava/lang/String;Landroid/content/Context;Lcom/peel/util/t;)V

    .line 157
    :cond_1
    if-nez v0, :cond_2

    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string/jumbo v2, "is_device_setup_complete"

    invoke-interface {v0, v2, v1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_2

    .line 158
    const-string/jumbo v0, "Remote Setup"

    invoke-virtual {p0}, Lcom/peel/main/Home;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    new-instance v3, Lcom/peel/main/t;

    invoke-direct {v3, p0}, Lcom/peel/main/t;-><init>(Lcom/peel/main/Home;)V

    invoke-static {v0, v2, v3}, Lcom/peel/util/a;->a(Ljava/lang/String;Landroid/content/Context;Lcom/peel/util/t;)V

    .line 169
    const-string/jumbo v0, "showhelpalloc"

    invoke-virtual {p0}, Lcom/peel/main/Home;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/peel/util/a;->a(Ljava/lang/String;Landroid/content/Context;)V

    .line 172
    :cond_2
    sget-object v0, Lcom/peel/content/a;->f:Lcom/peel/content/user/User;

    if-eqz v0, :cond_3

    .line 174
    invoke-static {p0}, Lcom/peel/util/au;->b(Landroid/content/Context;)V

    .line 177
    :cond_3
    invoke-static {}, Lcom/peel/content/a;->c()[Lcom/peel/content/library/Library;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 179
    invoke-static {}, Lcom/peel/content/a;->c()[Lcom/peel/content/library/Library;

    move-result-object v8

    array-length v9, v8

    move v6, v1

    :goto_1
    if-ge v6, v9, :cond_6

    aget-object v10, v8, v6

    .line 180
    sget-object v0, Lcom/peel/main/Home;->d:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "\n ##### library: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v10}, Lcom/peel/content/library/Library;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v10}, Lcom/peel/content/library/Library;->f()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 181
    invoke-virtual {v10}, Lcom/peel/content/library/Library;->b()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_4

    .line 182
    sget-object v0, Lcom/peel/main/Home;->d:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "\n ##### library next poll: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v10}, Lcom/peel/content/library/Library;->b()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 183
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    .line 184
    new-instance v1, Lcom/peel/main/ad;

    invoke-direct {v1, p0, v10}, Lcom/peel/main/ad;-><init>(Lcom/peel/main/Home;Lcom/peel/content/library/Library;)V

    .line 191
    invoke-virtual {v10}, Lcom/peel/content/library/Library;->b()J

    move-result-wide v2

    invoke-virtual {v10}, Lcom/peel/content/library/Library;->b()J

    move-result-wide v4

    invoke-virtual/range {v0 .. v5}, Ljava/util/Timer;->scheduleAtFixedRate(Ljava/util/TimerTask;JJ)V

    .line 192
    sget-object v0, Lcom/peel/main/Home;->d:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "\n##### scheduled at interval: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v10}, Lcom/peel/content/library/Library;->b()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 179
    :cond_4
    add-int/lit8 v1, v6, 0x1

    move v6, v1

    goto/16 :goto_1

    :cond_5
    move v0, v1

    .line 146
    goto/16 :goto_0

    .line 198
    :cond_6
    if-eqz p1, :cond_8

    .line 201
    invoke-direct {p0, v7}, Lcom/peel/main/Home;->a(Landroid/content/Intent;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 202
    const-class v0, Lcom/peel/main/Home;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "ContentEvents.LOADED reload"

    new-instance v2, Lcom/peel/main/ae;

    invoke-direct {v2, p0}, Lcom/peel/main/ae;-><init>(Lcom/peel/main/Home;)V

    invoke-static {v0, v1, v2}, Lcom/peel/util/i;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/lang/Runnable;

    .line 335
    :cond_7
    :goto_2
    return-void

    .line 267
    :cond_8
    const-class v0, Lcom/peel/main/Home;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "ContentEvents.LOADED update action and content fragments"

    new-instance v2, Lcom/peel/main/ah;

    invoke-direct {v2, p0}, Lcom/peel/main/ah;-><init>(Lcom/peel/main/Home;)V

    invoke-static {v0, v1, v2}, Lcom/peel/util/i;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/lang/Runnable;

    goto :goto_2
.end method

.method public b()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 638
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v2

    const-string/jumbo v3, "setup_type"

    invoke-interface {v2, v3, v1}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v2

    if-ne v0, v2, :cond_2

    .line 639
    :goto_0
    if-nez v0, :cond_1

    .line 641
    sget-object v0, Lcom/peel/util/a;->e:[I

    if-nez v0, :cond_0

    .line 642
    invoke-virtual {p0}, Lcom/peel/main/Home;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    new-instance v1, Lcom/peel/main/h;

    invoke-direct {v1, p0, v4}, Lcom/peel/main/h;-><init>(Lcom/peel/main/Home;I)V

    invoke-static {v0, v1}, Lcom/peel/util/bx;->a(Landroid/content/Context;Lcom/peel/util/t;)V

    .line 652
    invoke-virtual {p0}, Lcom/peel/main/Home;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    new-instance v1, Lcom/peel/main/i;

    invoke-direct {v1, p0, v4}, Lcom/peel/main/i;-><init>(Lcom/peel/main/Home;I)V

    invoke-static {v0, v1}, Lcom/peel/util/bx;->b(Landroid/content/Context;Lcom/peel/util/t;)V

    .line 662
    :cond_0
    const-string/jumbo v0, "epgSetupComplete"

    invoke-virtual {p0}, Lcom/peel/main/Home;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/peel/util/a;->a(Ljava/lang/String;Landroid/content/Context;)V

    .line 663
    sget-object v0, Lcom/peel/util/a;->j:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 664
    const-string/jumbo v0, "RemoteSetupTrigger"

    .line 665
    invoke-virtual {p0}, Lcom/peel/main/Home;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    new-instance v2, Lcom/peel/main/j;

    invoke-direct {v2, p0}, Lcom/peel/main/j;-><init>(Lcom/peel/main/Home;)V

    .line 664
    invoke-static {v0, v1, v2}, Lcom/peel/util/a;->a(Ljava/lang/String;Landroid/content/Context;Lcom/peel/util/t;)V

    .line 673
    :cond_1
    return-void

    :cond_2
    move v0, v1

    .line 638
    goto :goto_0
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 7

    .prologue
    const/16 v4, 0x1f45

    const/16 v1, 0x1f41

    const/4 v2, 0x1

    .line 715
    invoke-super {p0, p1, p2, p3}, Lcom/peel/util/a/d;->onActivityResult(IILandroid/content/Intent;)V

    .line 716
    iget-object v0, p0, Lcom/peel/main/Home;->o:Lcom/peel/social/a/f;

    invoke-virtual {v0, p0, p1, p2, p3}, Lcom/peel/social/a/f;->a(Landroid/app/Activity;IILandroid/content/Intent;)Z

    .line 717
    new-instance v3, Lcom/peel/backup/c;

    invoke-virtual {p0}, Lcom/peel/main/Home;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {v3, v0}, Lcom/peel/backup/c;-><init>(Landroid/content/Context;)V

    .line 719
    sget v0, Lcom/peel/social/w;->a:I

    if-eq p1, v0, :cond_0

    sget v0, Lcom/peel/social/w;->c:I

    if-ne p1, v0, :cond_2

    .line 721
    :cond_0
    if-eqz p3, :cond_1

    sget-object v0, Lcom/peel/social/w;->e:Ljava/lang/String;

    invoke-virtual {p3, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 722
    sget-object v0, Lcom/peel/social/w;->e:Ljava/lang/String;

    .line 723
    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    new-instance v0, Lcom/peel/main/k;

    move-object v1, p0

    move v2, p1

    move v4, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/peel/main/k;-><init>(Lcom/peel/main/Home;ILcom/peel/backup/c;ILandroid/content/Intent;)V

    .line 722
    invoke-static {p0, v6, v0}, Lcom/peel/social/w;->a(Landroid/content/Context;Ljava/lang/String;Lcom/peel/util/t;)V

    .line 835
    :cond_1
    :goto_0
    return-void

    .line 750
    :cond_2
    sget v0, Lcom/peel/social/w;->b:I

    if-eq p1, v0, :cond_3

    sget v0, Lcom/peel/social/w;->d:I

    if-ne p1, v0, :cond_5

    .line 752
    :cond_3
    if-eqz p3, :cond_4

    sget-object v0, Lcom/peel/social/w;->h:Ljava/lang/String;

    invoke-virtual {p3, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 753
    sget-object v0, Lcom/peel/social/w;->h:Ljava/lang/String;

    .line 754
    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    new-instance v0, Lcom/peel/main/m;

    move-object v1, p0

    move v2, p1

    move v4, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/peel/main/m;-><init>(Lcom/peel/main/Home;ILcom/peel/backup/c;ILandroid/content/Intent;)V

    .line 753
    invoke-static {p0, v6, v0}, Lcom/peel/social/w;->b(Landroid/content/Context;Ljava/lang/String;Lcom/peel/util/t;)V

    goto :goto_0

    .line 779
    :cond_4
    if-nez p3, :cond_1

    .line 780
    sget-boolean v0, Lcom/peel/social/e;->f:Z

    if-ne v0, v2, :cond_1

    .line 781
    const/4 v0, 0x0

    sput-boolean v0, Lcom/peel/social/e;->f:Z

    .line 782
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/peel/main/GoogleLoginActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 783
    sget-object v1, Lcom/peel/social/w;->i:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 784
    invoke-virtual {p0, v0, p1}, Lcom/peel/main/Home;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    .line 787
    :cond_5
    const/16 v0, 0x1f43

    if-eq p1, v0, :cond_6

    if-eq p1, v1, :cond_6

    const/16 v0, 0x1f42

    if-eq p1, v0, :cond_6

    const/16 v0, 0x1f44

    if-eq p1, v0, :cond_6

    if-eq p1, v4, :cond_6

    const/16 v0, 0x1f46

    if-ne p1, v0, :cond_1

    .line 793
    :cond_6
    const/4 v0, -0x1

    if-eq p2, v0, :cond_7

    .line 795
    invoke-static {p0}, Lcom/peel/d/e;->a(Landroid/support/v4/app/ae;)Lcom/peel/d/u;

    move-result-object v0

    .line 796
    invoke-virtual {v0, p1, p2, p3}, Lcom/peel/d/u;->a(IILandroid/content/Intent;)V

    goto :goto_0

    .line 798
    :cond_7
    invoke-static {p1, p2, p3, p0}, Lcom/peel/util/dx;->a(IILandroid/content/Intent;Landroid/app/Activity;)V

    .line 800
    if-eq p1, v1, :cond_8

    if-ne p1, v4, :cond_1

    .line 802
    :cond_8
    new-instance v0, Lcom/peel/main/o;

    move-object v1, p0

    move v2, p1

    move v4, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/peel/main/o;-><init>(Lcom/peel/main/Home;ILcom/peel/backup/c;ILandroid/content/Intent;)V

    invoke-static {p0, v0}, Lcom/peel/social/w;->a(Landroid/content/Context;Lcom/peel/util/t;)V

    goto :goto_0
.end method

.method public onBackPressed()V
    .locals 1

    .prologue
    .line 839
    sget-object v0, Lcom/peel/main/Home;->d:Ljava/lang/String;

    invoke-static {v0, p0}, Lcom/peel/d/e;->a(Ljava/lang/String;Landroid/support/v4/app/ae;)V

    .line 840
    return-void
.end method

.method public onClickContent(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 1461
    invoke-static {p0}, Lcom/peel/d/e;->a(Landroid/support/v4/app/ae;)Lcom/peel/d/u;

    move-result-object v0

    .line 1462
    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Lcom/peel/d/u;->onClick(Landroid/view/View;)V

    .line 1463
    :cond_0
    return-void
.end method

.method public onClickMenu(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 1466
    invoke-virtual {p0}, Lcom/peel/main/Home;->getSupportFragmentManager()Landroid/support/v4/app/aj;

    move-result-object v0

    sget v1, Lcom/peel/ui/fp;->drawer:I

    invoke-virtual {v0, v1}, Landroid/support/v4/app/aj;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/peel/d/u;

    .line 1467
    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Lcom/peel/d/u;->onClick(Landroid/view/View;)V

    .line 1468
    :cond_0
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 1686
    invoke-super {p0, p1}, Lcom/peel/util/a/d;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 1687
    iget v0, p1, Landroid/content/res/Configuration;->orientation:I

    .line 1689
    if-ne v0, v2, :cond_0

    .line 1695
    :goto_0
    return-void

    .line 1690
    :cond_0
    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    .line 1691
    invoke-virtual {p0, v2}, Lcom/peel/main/Home;->setRequestedOrientation(I)V

    goto :goto_0

    .line 1693
    :cond_1
    invoke-virtual {p0, v2}, Lcom/peel/main/Home;->setRequestedOrientation(I)V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 13

    .prologue
    const/4 v12, 0x2

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 392
    invoke-super {p0, p1}, Lcom/peel/util/a/d;->onCreate(Landroid/os/Bundle;)V

    .line 393
    const/16 v0, 0xe

    invoke-virtual {p0, v0}, Lcom/peel/main/Home;->setRequestedOrientation(I)V

    .line 395
    const/4 v0, 0x5

    invoke-virtual {p0, v0}, Lcom/peel/main/Home;->supportRequestWindowFeature(I)Z

    .line 397
    sget-object v0, Lcom/peel/main/Home;->p:[I

    array-length v0, v0

    new-array v1, v0, [Ljava/lang/String;

    move v0, v7

    .line 398
    :goto_0
    sget-object v2, Lcom/peel/main/Home;->p:[I

    array-length v2, v2

    if-ge v0, v2, :cond_0

    .line 399
    sget-object v2, Lcom/peel/main/Home;->p:[I

    aget v2, v2, v0

    invoke-virtual {p0, v2}, Lcom/peel/main/Home;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    .line 398
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 400
    :cond_0
    invoke-static {v1}, Lcom/peel/util/x;->a([Ljava/lang/String;)V

    .line 402
    sget-object v0, Lcom/peel/main/Home;->c:[I

    array-length v0, v0

    new-array v1, v0, [Ljava/lang/String;

    move v0, v7

    .line 403
    :goto_1
    sget-object v2, Lcom/peel/main/Home;->c:[I

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 404
    sget-object v2, Lcom/peel/main/Home;->c:[I

    aget v2, v2, v0

    invoke-virtual {p0, v2}, Lcom/peel/main/Home;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    .line 403
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 405
    :cond_1
    invoke-static {v1}, Lcom/peel/util/x;->b([Ljava/lang/String;)V

    .line 408
    invoke-static {p0}, Lcom/peel/d/i;->a(Lcom/peel/util/a/d;)V

    .line 409
    invoke-static {}, Lcom/peel/d/i;->a()Lcom/peel/d/i;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/main/Home;->e:Lcom/peel/d/i;

    .line 411
    const-string/jumbo v0, "currentVersion"

    invoke-static {p0, v0}, Lcom/peel/util/dq;->a(Landroid/content/Context;Ljava/lang/String;)I

    move-result v9

    .line 412
    invoke-static {p0}, Lcom/peel/util/bx;->j(Landroid/content/Context;)I

    move-result v10

    .line 413
    const-string/jumbo v0, "currentVersion"

    invoke-static {p0, v0, v10}, Lcom/peel/util/dq;->a(Landroid/content/Context;Ljava/lang/String;I)V

    .line 415
    iget-object v0, p0, Lcom/peel/main/Home;->e:Lcom/peel/d/i;

    invoke-static {p0, v0}, Lcom/peel/util/bx;->a(Landroid/content/Context;Lcom/peel/d/i;)V

    .line 417
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/main/Home;->g:Landroid/content/SharedPreferences;

    .line 425
    iget-object v0, p0, Lcom/peel/main/Home;->g:Landroid/content/SharedPreferences;

    const-string/jumbo v1, "app_launch_count"

    invoke-interface {v0, v1, v7}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 426
    const v1, 0x7fffffff

    if-le v1, v0, :cond_2

    iget-object v1, p0, Lcom/peel/main/Home;->g:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string/jumbo v2, "app_launch_count"

    add-int/lit8 v0, v0, 0x1

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 428
    :cond_2
    iget-object v0, p0, Lcom/peel/main/Home;->g:Landroid/content/SharedPreferences;

    const-string/jumbo v1, "config_legacy"

    const-string/jumbo v2, "name|USA|endpoint|usa|iso|US|type|5digitzip"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/peel/b/a;->c:Ljava/lang/String;

    .line 429
    sget-object v0, Lcom/peel/b/a;->c:Ljava/lang/String;

    invoke-static {v0}, Lcom/peel/util/bx;->b(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v11

    .line 430
    sget-object v0, Lcom/peel/b/a;->a:Ljava/util/Map;

    const-string/jumbo v1, "endpoint"

    invoke-virtual {v11, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    sput-object v0, Lcom/peel/b/a;->e:Ljava/lang/String;

    .line 431
    sget-object v0, Lcom/peel/b/a;->e:Ljava/lang/String;

    invoke-static {v0}, Lcom/peel/content/a/j;->a(Ljava/lang/String;)V

    .line 432
    const-string/jumbo v0, "iso"

    invoke-virtual {v11, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/peel/content/a;->e:Ljava/lang/String;

    .line 433
    const-string/jumbo v0, "urloverride"

    invoke-virtual {v11, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/peel/util/b/a;->b:Ljava/lang/String;

    .line 436
    iget-object v1, p0, Lcom/peel/main/Home;->e:Lcom/peel/d/i;

    const-string/jumbo v2, "yosemite_enabled"

    iget-object v0, p0, Lcom/peel/main/Home;->g:Landroid/content/SharedPreferences;

    const-string/jumbo v3, "yosemite_enabled"

    invoke-interface {v0, v3, v7}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_b

    move v0, v8

    :goto_2
    invoke-virtual {v1, v2, v0}, Lcom/peel/d/i;->b(Ljava/lang/String;I)V

    .line 437
    iget-object v0, p0, Lcom/peel/main/Home;->e:Lcom/peel/d/i;

    const-string/jumbo v1, "time"

    invoke-static {}, Lcom/peel/util/x;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/peel/d/i;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 438
    if-eqz p1, :cond_3

    .line 439
    const-string/jumbo v0, "properties"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    .line 440
    if-eqz v0, :cond_3

    iget-object v1, p0, Lcom/peel/main/Home;->e:Lcom/peel/d/i;

    invoke-virtual {v1, v0}, Lcom/peel/d/i;->a(Landroid/os/Bundle;)V

    .line 444
    :cond_3
    sget v0, Lcom/peel/ui/fq;->main:I

    invoke-virtual {p0, v0}, Lcom/peel/main/Home;->setContentView(I)V

    .line 447
    invoke-virtual {p0}, Lcom/peel/main/Home;->getSupportFragmentManager()Landroid/support/v4/app/aj;

    move-result-object v0

    .line 448
    invoke-virtual {v0}, Landroid/support/v4/app/aj;->a()Landroid/support/v4/app/ax;

    move-result-object v1

    sget v2, Lcom/peel/ui/fp;->drawer:I

    sget-object v0, Lcom/peel/c/a;->d:Lcom/peel/c/e;

    .line 449
    invoke-static {v0}, Lcom/peel/c/a;->a(Lcom/peel/c/e;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/ui/ni;

    const-class v3, Lcom/peel/ui/cj;

    .line 450
    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    .line 449
    invoke-virtual {v0, p0, v3}, Lcom/peel/ui/ni;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    const-string/jumbo v3, "menu"

    invoke-virtual {v1, v2, v0, v3}, Landroid/support/v4/app/ax;->b(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/ax;

    move-result-object v0

    .line 450
    invoke-virtual {v0}, Landroid/support/v4/app/ax;->b()I

    .line 453
    invoke-virtual {p0}, Lcom/peel/main/Home;->getSupportFragmentManager()Landroid/support/v4/app/aj;

    move-result-object v0

    .line 454
    invoke-virtual {v0}, Landroid/support/v4/app/aj;->a()Landroid/support/v4/app/ax;

    move-result-object v0

    const-class v1, Lcom/peel/d/k;

    .line 455
    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1}, Landroid/support/v4/app/Fragment;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v1

    const-string/jumbo v2, "action"

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/ax;->a(Landroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/ax;

    move-result-object v0

    .line 456
    invoke-virtual {v0}, Landroid/support/v4/app/ax;->b()I

    .line 461
    sget v0, Lcom/peel/ui/fp;->drawer_layout:I

    invoke-virtual {p0, v0}, Lcom/peel/main/Home;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/widget/DrawerLayout;

    iput-object v0, p0, Lcom/peel/main/Home;->m:Landroid/support/v4/widget/DrawerLayout;

    .line 464
    new-instance v0, Lcom/peel/main/al;

    iget-object v3, p0, Lcom/peel/main/Home;->m:Landroid/support/v4/widget/DrawerLayout;

    sget v4, Lcom/peel/ui/fo;->ic_drawer:I

    sget v5, Lcom/peel/ui/ft;->drawer_open:I

    sget v6, Lcom/peel/ui/ft;->drawer_close:I

    move-object v1, p0

    move-object v2, p0

    invoke-direct/range {v0 .. v6}, Lcom/peel/main/al;-><init>(Lcom/peel/main/Home;Landroid/app/Activity;Landroid/support/v4/widget/DrawerLayout;III)V

    iput-object v0, p0, Lcom/peel/main/Home;->l:Landroid/support/v4/app/a;

    .line 504
    iget-object v0, p0, Lcom/peel/main/Home;->m:Landroid/support/v4/widget/DrawerLayout;

    iget-object v1, p0, Lcom/peel/main/Home;->l:Landroid/support/v4/app/a;

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/DrawerLayout;->setDrawerListener(Landroid/support/v4/widget/p;)V

    .line 505
    iget-object v0, p0, Lcom/peel/main/Home;->e:Lcom/peel/d/i;

    iget-object v1, p0, Lcom/peel/main/Home;->l:Landroid/support/v4/app/a;

    invoke-virtual {v0, v1}, Lcom/peel/d/i;->a(Landroid/support/v4/app/a;)V

    .line 506
    invoke-direct {p0}, Lcom/peel/main/Home;->e()V

    .line 509
    const-class v0, Lcom/peel/main/Home;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "check NTP time"

    new-instance v2, Lcom/peel/main/am;

    invoke-direct {v2, p0}, Lcom/peel/main/am;-><init>(Lcom/peel/main/Home;)V

    invoke-static {v0, v1, v2}, Lcom/peel/util/i;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    .line 530
    invoke-direct {p0}, Lcom/peel/main/Home;->f()V

    .line 532
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 533
    const-string/jumbo v1, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 534
    iput-boolean v8, p0, Lcom/peel/main/Home;->n:Z

    .line 535
    iget-object v1, p0, Lcom/peel/main/Home;->f:Lcom/peel/main/ar;

    invoke-virtual {p0, v1, v0}, Lcom/peel/main/Home;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 537
    invoke-static {p0}, Lcom/peel/b/a;->a(Landroid/app/Activity;)V

    .line 539
    invoke-static {p0}, Lcom/peel/util/bx;->d(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_c

    invoke-static {p0}, Lcom/peel/util/bx;->d(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "Korea"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 549
    :cond_4
    :goto_3
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string/jumbo v1, "close_toast_count"

    invoke-interface {v0, v1, v7}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 550
    if-le v0, v12, :cond_5

    .line 551
    iget-object v0, p0, Lcom/peel/main/Home;->e:Lcom/peel/d/i;

    const-string/jumbo v1, "popuptoast"

    invoke-virtual {v0, v1, v8}, Lcom/peel/d/i;->b(Ljava/lang/String;I)V

    .line 554
    :cond_5
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/main/Home;->h:Landroid/content/SharedPreferences;

    .line 555
    new-instance v0, Lcom/peel/main/ap;

    invoke-direct {v0, p0}, Lcom/peel/main/ap;-><init>(Lcom/peel/main/Home;)V

    iput-object v0, p0, Lcom/peel/main/Home;->a:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    .line 574
    iget-object v0, p0, Lcom/peel/main/Home;->h:Landroid/content/SharedPreferences;

    iget-object v1, p0, Lcom/peel/main/Home;->a:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences;->registerOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 576
    sget-object v0, Lcom/peel/content/a;->h:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_8

    if-nez p1, :cond_8

    .line 577
    iget-object v0, p0, Lcom/peel/main/Home;->h:Landroid/content/SharedPreferences;

    const-string/jumbo v1, "is_setup_complete"

    invoke-interface {v0, v1, v7}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 578
    invoke-virtual {p0}, Lcom/peel/main/Home;->b()V

    .line 588
    :cond_6
    :goto_4
    sget-object v0, Lcom/peel/c/a;->b:Lcom/peel/c/e;

    invoke-static {v0}, Lcom/peel/c/a;->a(Lcom/peel/c/e;)Ljava/lang/Object;

    move-result-object v0

    sget-object v1, Lcom/peel/c/b;->c:Lcom/peel/c/b;

    if-ne v0, v1, :cond_7

    sget-boolean v0, Ltv/peel/widget/WidgetService;->d:Z

    if-nez v0, :cond_7

    .line 590
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string/jumbo v1, "always_remote_widget_enabled"

    invoke-interface {v0, v1, v7}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 591
    new-instance v0, Landroid/content/Intent;

    const-class v1, Ltv/peel/widget/WidgetService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 592
    const-string/jumbo v1, "tv.peel.widget.action.UPDATE_WIDGETS"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 593
    invoke-virtual {p0, v0}, Lcom/peel/main/Home;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 596
    :cond_7
    invoke-virtual {p0, v8, v8}, Lcom/peel/main/Home;->a(ZZ)V

    .line 600
    :cond_8
    sget-boolean v0, Lcom/peel/util/bn;->b:Z

    if-eqz v0, :cond_9

    .line 601
    invoke-static {p0}, Lcom/peel/util/eh;->a(Landroid/content/Context;)Lcom/peel/util/eh;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/peel/util/eh;->a(Landroid/app/Activity;)V

    .line 605
    :cond_9
    new-instance v0, Lcom/peel/util/au;

    invoke-virtual {p0}, Lcom/peel/main/Home;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/peel/util/au;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/peel/main/Home;->i:Lcom/peel/util/au;

    .line 606
    invoke-virtual {p0}, Lcom/peel/main/Home;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/peel/util/au;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 607
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_f

    .line 608
    iget-object v0, p0, Lcom/peel/main/Home;->i:Lcom/peel/util/au;

    invoke-virtual {p0}, Lcom/peel/main/Home;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/peel/util/au;->c(Landroid/content/Context;)V

    .line 617
    :goto_5
    sget-object v0, Lcom/peel/content/a;->e:Ljava/lang/String;

    invoke-static {v0}, Lcom/peel/util/bx;->c(Ljava/lang/String;)V

    .line 619
    sget-object v0, Lcom/peel/content/a;->f:Lcom/peel/content/user/User;

    if-eqz v0, :cond_a

    .line 620
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/peel/util/bx;->a(Lcom/peel/util/t;)V

    .line 623
    :cond_a
    new-instance v0, Lcom/peel/social/a/i;

    invoke-direct {v0}, Lcom/peel/social/a/i;-><init>()V

    const-string/jumbo v1, "118836928154289"

    .line 624
    invoke-virtual {v0, v1}, Lcom/peel/social/a/i;->a(Ljava/lang/String;)Lcom/peel/social/a/i;

    move-result-object v0

    const-string/jumbo v1, "peelsocialtv"

    .line 625
    invoke-virtual {v0, v1}, Lcom/peel/social/a/i;->b(Ljava/lang/String;)Lcom/peel/social/a/i;

    move-result-object v0

    new-array v1, v12, [Lcom/peel/social/a/a;

    sget-object v2, Lcom/peel/social/a/a;->E:Lcom/peel/social/a/a;

    aput-object v2, v1, v7

    sget-object v2, Lcom/peel/social/a/a;->a:Lcom/peel/social/a/a;

    aput-object v2, v1, v8

    .line 626
    invoke-virtual {v0, v1}, Lcom/peel/social/a/i;->a([Lcom/peel/social/a/a;)Lcom/peel/social/a/i;

    move-result-object v0

    sget-object v1, Lcom/facebook/ct;->c:Lcom/facebook/ct;

    .line 627
    invoke-virtual {v0, v1}, Lcom/peel/social/a/i;->a(Lcom/facebook/ct;)Lcom/peel/social/a/i;

    move-result-object v0

    .line 628
    invoke-virtual {v0, v7}, Lcom/peel/social/a/i;->a(Z)Lcom/peel/social/a/i;

    move-result-object v0

    .line 629
    invoke-virtual {v0}, Lcom/peel/social/a/i;->a()Lcom/peel/social/a/g;

    move-result-object v0

    .line 630
    invoke-static {v0}, Lcom/peel/social/a/f;->a(Lcom/peel/social/a/g;)V

    .line 632
    invoke-static {p0}, Lcom/peel/social/a/f;->a(Landroid/app/Activity;)Lcom/peel/social/a/f;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/main/Home;->o:Lcom/peel/social/a/f;

    .line 634
    return-void

    :cond_b
    move v0, v7

    .line 436
    goto/16 :goto_2

    .line 541
    :cond_c
    const-string/jumbo v0, "iso"

    invoke-virtual {v11, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_d

    const-string/jumbo v0, "iso"

    invoke-virtual {v11, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "KR"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 544
    :cond_d
    const-string/jumbo v0, "release"

    const-string/jumbo v1, "debug"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_4

    if-gt v10, v9, :cond_4

    .line 545
    invoke-static {p0}, Lcom/c/a/a;->a(Landroid/content/Context;)V

    goto/16 :goto_3

    .line 579
    :cond_e
    iget-object v0, p0, Lcom/peel/main/Home;->h:Landroid/content/SharedPreferences;

    const-string/jumbo v1, "is_setup_complete"

    invoke-interface {v0, v1, v7}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_6

    sget-object v0, Lcom/peel/util/a;->k:Ljava/lang/String;

    if-nez v0, :cond_6

    .line 580
    const-string/jumbo v0, "Setup"

    .line 581
    invoke-virtual {p0}, Lcom/peel/main/Home;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    new-instance v2, Lcom/peel/main/g;

    invoke-direct {v2, p0}, Lcom/peel/main/g;-><init>(Lcom/peel/main/Home;)V

    .line 580
    invoke-static {v0, v1, v2}, Lcom/peel/util/a;->a(Ljava/lang/String;Landroid/content/Context;Lcom/peel/util/t;)V

    goto/16 :goto_4

    .line 610
    :cond_f
    sget-object v1, Lcom/peel/main/Home;->d:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Device already registered, registration id="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_5
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1261
    const/4 v0, 0x0

    sput-boolean v0, Lcom/peel/util/bx;->h:Z

    .line 1262
    sput-object v1, Lcom/peel/util/bx;->e:Lcom/peel/widget/ag;

    .line 1263
    sput-object v1, Lcom/peel/util/bx;->f:Lcom/peel/widget/ag;

    .line 1265
    invoke-static {}, Lcom/peel/d/i;->b()V

    .line 1266
    iput-object v1, p0, Lcom/peel/main/Home;->e:Lcom/peel/d/i;

    .line 1271
    iget-object v0, p0, Lcom/peel/main/Home;->f:Lcom/peel/main/ar;

    invoke-virtual {p0, v0}, Lcom/peel/main/Home;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 1273
    iget-object v0, p0, Lcom/peel/main/Home;->h:Landroid/content/SharedPreferences;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/peel/main/Home;->a:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    if-eqz v0, :cond_0

    .line 1274
    iget-object v0, p0, Lcom/peel/main/Home;->h:Landroid/content/SharedPreferences;

    iget-object v1, p0, Lcom/peel/main/Home;->a:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences;->unregisterOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 1277
    :cond_0
    invoke-super {p0}, Lcom/peel/util/a/d;->onDestroy()V

    .line 1279
    sget-boolean v0, Lcom/peel/util/bn;->b:Z

    if-eqz v0, :cond_1

    invoke-static {p0}, Lcom/peel/util/eh;->a(Landroid/content/Context;)Lcom/peel/util/eh;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/peel/util/eh;->b(Landroid/app/Activity;)V

    .line 1280
    :cond_1
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 9

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 1287
    sparse-switch p1, :sswitch_data_0

    .line 1394
    :cond_0
    invoke-super {p0, p1, p2}, Lcom/peel/util/a/d;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v1

    :cond_1
    :goto_0
    return v1

    .line 1290
    :sswitch_0
    sget-object v2, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v2}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v2

    .line 1291
    if-nez v2, :cond_2

    invoke-super {p0, p1, p2}, Lcom/peel/util/a/d;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v1

    goto :goto_0

    .line 1294
    :cond_2
    iget-object v3, p0, Lcom/peel/main/Home;->e:Lcom/peel/d/i;

    invoke-direct {p0, v2, v3}, Lcom/peel/main/Home;->a(Lcom/peel/control/RoomControl;Lcom/peel/d/i;)Lcom/peel/control/a;

    move-result-object v3

    .line 1295
    if-nez v3, :cond_3

    invoke-super {p0, p1, p2}, Lcom/peel/util/a/d;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v1

    goto :goto_0

    .line 1297
    :cond_3
    invoke-virtual {v3, v0}, Lcom/peel/control/a;->a(I)Lcom/peel/control/h;

    move-result-object v4

    if-nez v4, :cond_4

    invoke-super {p0, p1, p2}, Lcom/peel/util/a/d;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v1

    goto :goto_0

    .line 1299
    :cond_4
    sget-object v4, Lcom/peel/content/a;->h:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v4}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v4

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/peel/main/Home;->e:Lcom/peel/d/i;

    if-eqz v4, :cond_1

    .line 1300
    iget-object v4, p0, Lcom/peel/main/Home;->b:Landroid/widget/Toast;

    if-eqz v4, :cond_5

    iget-object v4, p0, Lcom/peel/main/Home;->b:Landroid/widget/Toast;

    invoke-virtual {v4}, Landroid/widget/Toast;->cancel()V

    .line 1302
    :cond_5
    new-instance v4, Landroid/widget/Toast;

    invoke-virtual {p0}, Lcom/peel/main/Home;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    invoke-direct {v4, v5}, Landroid/widget/Toast;-><init>(Landroid/content/Context;)V

    iput-object v4, p0, Lcom/peel/main/Home;->b:Landroid/widget/Toast;

    .line 1303
    iget-object v4, p0, Lcom/peel/main/Home;->b:Landroid/widget/Toast;

    const/16 v5, 0x11

    invoke-virtual {v4, v5, v0, v0}, Landroid/widget/Toast;->setGravity(III)V

    .line 1304
    iget-object v4, p0, Lcom/peel/main/Home;->b:Landroid/widget/Toast;

    invoke-virtual {v4, v0}, Landroid/widget/Toast;->setDuration(I)V

    .line 1305
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v4

    sget v5, Lcom/peel/ui/fq;->controlpad_volume_layout:I

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v4

    .line 1306
    sget v5, Lcom/peel/ui/fp;->dashboard_plus:I

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    .line 1307
    sget v6, Lcom/peel/ui/fp;->dashboard_minus:I

    invoke-virtual {v4, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    .line 1309
    if-eqz v6, :cond_6

    if-eqz v5, :cond_6

    .line 1310
    const/16 v7, 0x19

    if-ne p1, v7, :cond_7

    .line 1311
    invoke-virtual {v6, v1}, Landroid/view/View;->setPressed(Z)V

    .line 1317
    :cond_6
    :goto_1
    iget-object v5, p0, Lcom/peel/main/Home;->b:Landroid/widget/Toast;

    invoke-virtual {v5, v4}, Landroid/widget/Toast;->setView(Landroid/view/View;)V

    .line 1318
    iget-object v4, p0, Lcom/peel/main/Home;->b:Landroid/widget/Toast;

    invoke-virtual {v4}, Landroid/widget/Toast;->show()V

    .line 1320
    const/4 v4, 0x2

    invoke-virtual {v3}, Lcom/peel/control/a;->f()I

    move-result v5

    if-eq v4, v5, :cond_8

    iget-object v4, p0, Lcom/peel/main/Home;->q:Lcom/peel/util/s;

    if-nez v4, :cond_8

    .line 1321
    new-instance v4, Lcom/peel/main/u;

    invoke-direct {v4, p0, v3, p1}, Lcom/peel/main/u;-><init>(Lcom/peel/main/Home;Lcom/peel/control/a;I)V

    iput-object v4, p0, Lcom/peel/main/Home;->q:Lcom/peel/util/s;

    .line 1336
    sget-object v4, Lcom/peel/control/am;->a:Lcom/peel/control/aq;

    iget-object v5, p0, Lcom/peel/main/Home;->q:Lcom/peel/util/s;

    invoke-virtual {v4, v5}, Lcom/peel/control/aq;->a(Ljava/lang/Object;)V

    .line 1337
    invoke-virtual {v2, v3, v0}, Lcom/peel/control/RoomControl;->a(Lcom/peel/control/a;I)Z

    goto/16 :goto_0

    .line 1313
    :cond_7
    invoke-virtual {v5, v1}, Landroid/view/View;->setPressed(Z)V

    goto :goto_1

    .line 1340
    :cond_8
    const-class v0, Lcom/peel/main/Home;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v2, "send audio command "

    new-instance v4, Lcom/peel/main/v;

    invoke-direct {v4, p0, v3, p1}, Lcom/peel/main/v;-><init>(Lcom/peel/main/Home;Lcom/peel/control/a;I)V

    invoke-static {v0, v2, v4}, Lcom/peel/util/i;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/lang/Runnable;

    goto/16 :goto_0

    .line 1349
    :sswitch_1
    sget-object v2, Lcom/peel/c/a;->b:Lcom/peel/c/e;

    invoke-static {v2}, Lcom/peel/c/a;->a(Lcom/peel/c/e;)Ljava/lang/Object;

    move-result-object v2

    sget-object v3, Lcom/peel/c/b;->b:Lcom/peel/c/b;

    if-eq v2, v3, :cond_9

    sget-object v2, Lcom/peel/c/a;->b:Lcom/peel/c/e;

    invoke-static {v2}, Lcom/peel/c/a;->a(Lcom/peel/c/e;)Ljava/lang/Object;

    move-result-object v2

    sget-object v3, Lcom/peel/c/b;->c:Lcom/peel/c/b;

    if-ne v2, v3, :cond_a

    :cond_9
    move v1, v0

    .line 1350
    goto/16 :goto_0

    .line 1353
    :cond_a
    sget-object v2, Lcom/peel/c/a;->b:Lcom/peel/c/e;

    invoke-static {v2}, Lcom/peel/c/a;->a(Lcom/peel/c/e;)Ljava/lang/Object;

    move-result-object v2

    sget-object v3, Lcom/peel/c/b;->a:Lcom/peel/c/b;

    if-ne v2, v3, :cond_b

    .line 1354
    invoke-static {p0}, Lcom/peel/d/e;->a(Landroid/support/v4/app/ae;)Lcom/peel/d/u;

    move-result-object v2

    .line 1355
    if-eqz v2, :cond_b

    .line 1356
    invoke-virtual {v2}, Lcom/peel/d/u;->ab()Z

    move-result v3

    if-eqz v3, :cond_b

    invoke-virtual {v2}, Lcom/peel/d/u;->i()Ljava/lang/String;

    move-result-object v2

    const-class v3, Lcom/peel/ui/gt;

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_b

    move v1, v0

    .line 1357
    goto/16 :goto_0

    .line 1364
    :cond_b
    :try_start_0
    invoke-virtual {p0}, Lcom/peel/main/Home;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v2

    sget v3, Lcom/peel/ui/fp;->overflow_menu_btn:I

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 1365
    if-eqz v2, :cond_c

    .line 1366
    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/view/View;->setSoundEffectsEnabled(Z)V

    .line 1367
    invoke-virtual {v2}, Landroid/view/View;->performClick()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1369
    const/4 v0, 0x1

    :try_start_1
    invoke-virtual {v2, v0}, Landroid/view/View;->setSoundEffectsEnabled(Z)V

    .line 1370
    sget-object v0, Lcom/peel/main/Home;->d:Ljava/lang/String;

    const-string/jumbo v2, "\nfound overflow_menu_rel: perform click here"

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_3

    move v0, v1

    .line 1375
    :cond_c
    :goto_2
    if-nez v0, :cond_d

    .line 1377
    :try_start_2
    invoke-virtual {p0}, Lcom/peel/main/Home;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v2

    sget v3, Lcom/peel/ui/fp;->overflow_menu_btn:I

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 1378
    if-eqz v2, :cond_d

    .line 1379
    invoke-virtual {v2}, Landroid/view/View;->performClick()Z
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    .line 1381
    :try_start_3
    sget-object v0, Lcom/peel/main/Home;->d:Ljava/lang/String;

    const-string/jumbo v2, "\nfound overflow_menu_btn: perform click here"

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2

    move v0, v1

    .line 1388
    :cond_d
    :goto_3
    if-eqz v0, :cond_0

    .line 1389
    sget-object v0, Lcom/peel/main/Home;->d:Ljava/lang/String;

    const-string/jumbo v2, "\nclicked, we return true here"

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 1372
    :catch_0
    move-exception v2

    move-object v8, v2

    move v2, v0

    move-object v0, v8

    .line 1373
    :goto_4
    sget-object v3, Lcom/peel/main/Home;->d:Ljava/lang/String;

    sget-object v4, Lcom/peel/main/Home;->d:Ljava/lang/String;

    invoke-static {v3, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move v0, v2

    goto :goto_2

    .line 1383
    :catch_1
    move-exception v2

    move-object v8, v2

    move v2, v0

    move-object v0, v8

    .line 1384
    :goto_5
    sget-object v3, Lcom/peel/main/Home;->d:Ljava/lang/String;

    sget-object v4, Lcom/peel/main/Home;->d:Ljava/lang/String;

    invoke-static {v3, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move v0, v2

    goto :goto_3

    .line 1383
    :catch_2
    move-exception v0

    move v2, v1

    goto :goto_5

    .line 1372
    :catch_3
    move-exception v0

    move v2, v1

    goto :goto_4

    .line 1287
    :sswitch_data_0
    .sparse-switch
        0x18 -> :sswitch_0
        0x19 -> :sswitch_0
        0x52 -> :sswitch_1
    .end sparse-switch
.end method

.method public onKeyLongPress(ILandroid/view/KeyEvent;)Z
    .locals 2

    .prologue
    .line 1400
    const/16 v0, 0x52

    if-ne p1, v0, :cond_0

    sget-object v0, Lcom/peel/c/a;->b:Lcom/peel/c/e;

    invoke-static {v0}, Lcom/peel/c/a;->a(Lcom/peel/c/e;)Ljava/lang/Object;

    move-result-object v0

    sget-object v1, Lcom/peel/c/b;->a:Lcom/peel/c/b;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    .line 1402
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 1

    .prologue
    .line 1407
    const/16 v0, 0x19

    if-eq v0, p1, :cond_0

    const/16 v0, 0x18

    if-ne v0, p1, :cond_2

    .line 1408
    :cond_0
    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v0}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v0

    .line 1409
    if-eqz v0, :cond_1

    .line 1410
    invoke-virtual {v0}, Lcom/peel/control/RoomControl;->b()Lcom/peel/control/o;

    move-result-object v0

    .line 1411
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/peel/control/o;->c()V

    .line 1413
    :cond_1
    const/4 v0, 0x1

    .line 1416
    :goto_0
    return v0

    :cond_2
    invoke-super {p0, p1, p2}, Lcom/peel/util/a/d;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    .line 876
    invoke-super {p0, p1}, Lcom/peel/util/a/d;->onNewIntent(Landroid/content/Intent;)V

    .line 877
    invoke-virtual {p0, p1}, Lcom/peel/main/Home;->setIntent(Landroid/content/Intent;)V

    .line 879
    invoke-virtual {p1}, Landroid/content/Intent;->getCategories()Ljava/util/Set;

    move-result-object v0

    .line 880
    if-eqz v0, :cond_0

    const-string/jumbo v2, "android.intent.category.LAUNCHER"

    invoke-interface {v0, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 885
    :cond_0
    sget-object v0, Lcom/peel/content/a;->h:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 886
    const/4 v0, 0x0

    invoke-virtual {p0, v1, v0}, Lcom/peel/main/Home;->a(ZZ)V

    .line 894
    :cond_1
    :goto_0
    return-void

    .line 888
    :cond_2
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string/jumbo v2, "context_id"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 889
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string/jumbo v2, "context_id"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v3

    .line 890
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string/jumbo v2, "from"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 891
    invoke-static {}, Lcom/peel/util/a/f;->a()Lcom/peel/util/a/f;

    move-result-object v0

    const/16 v2, 0x440

    const/4 v5, -0x1

    invoke-virtual/range {v0 .. v5}, Lcom/peel/util/a/f;->a(IIILjava/lang/String;I)V

    goto :goto_0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 688
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 700
    :cond_0
    invoke-super {p0, p1}, Lcom/peel/util/a/d;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    :goto_0
    return v0

    .line 690
    :pswitch_0
    iget-object v1, p0, Lcom/peel/main/Home;->m:Landroid/support/v4/widget/DrawerLayout;

    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Landroid/support/v4/widget/DrawerLayout;->a(I)I

    move-result v1

    .line 691
    invoke-static {p0}, Lcom/peel/d/e;->b(Landroid/support/v4/app/ae;)I

    move-result v2

    if-lez v2, :cond_1

    if-eqz v1, :cond_1

    .line 693
    sget-object v1, Lcom/peel/main/Home;->d:Ljava/lang/String;

    invoke-static {v1, p0}, Lcom/peel/d/e;->a(Ljava/lang/String;Landroid/support/v4/app/ae;)V

    goto :goto_0

    .line 695
    :cond_1
    iget-object v1, p0, Lcom/peel/main/Home;->l:Landroid/support/v4/app/a;

    invoke-virtual {v1, p1}, Landroid/support/v4/app/a;->a(Landroid/view/MenuItem;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0

    .line 688
    nop

    :pswitch_data_0
    .packed-switch 0x102002c
        :pswitch_0
    .end packed-switch
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 844
    invoke-super {p0}, Lcom/peel/util/a/d;->onPause()V

    .line 845
    sget-object v0, Lcom/peel/c/a;->b:Lcom/peel/c/e;

    invoke-static {v0}, Lcom/peel/c/a;->a(Lcom/peel/c/e;)Ljava/lang/Object;

    move-result-object v0

    sget-object v1, Lcom/peel/c/b;->c:Lcom/peel/c/b;

    if-ne v0, v1, :cond_0

    sget-boolean v0, Ltv/peel/widget/WidgetService;->a:Z

    if-nez v0, :cond_0

    .line 846
    const/4 v0, 0x0

    sput-boolean v0, Ltv/peel/widget/WidgetService;->e:Z

    .line 847
    new-instance v0, Landroid/content/Intent;

    const-class v1, Ltv/peel/widget/WidgetService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 848
    const-string/jumbo v1, "tv.peel.widget.action.UPDATE_WIDGETS"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 849
    invoke-virtual {p0, v0}, Lcom/peel/main/Home;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 851
    :cond_0
    invoke-direct {p0}, Lcom/peel/main/Home;->g()V

    .line 852
    return-void
.end method

.method protected onPostCreate(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 681
    invoke-super {p0, p1}, Lcom/peel/util/a/d;->onPostCreate(Landroid/os/Bundle;)V

    .line 683
    iget-object v0, p0, Lcom/peel/main/Home;->l:Landroid/support/v4/app/a;

    invoke-virtual {v0}, Landroid/support/v4/app/a;->a()V

    .line 684
    return-void
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 705
    invoke-super {p0}, Lcom/peel/util/a/d;->onResume()V

    .line 706
    const/4 v0, 0x1

    sput-boolean v0, Lcom/peel/util/bx;->h:Z

    .line 707
    invoke-direct {p0}, Lcom/peel/main/Home;->h()V

    .line 709
    sget-boolean v0, Lcom/peel/util/bn;->b:Z

    if-eqz v0, :cond_0

    invoke-static {p0}, Lcom/peel/util/eh;->a(Landroid/content/Context;)Lcom/peel/util/eh;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/peel/util/eh;->c(Landroid/app/Activity;)V

    .line 710
    :cond_0
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 1243
    const-string/jumbo v0, "properties"

    iget-object v1, p0, Lcom/peel/main/Home;->e:Lcom/peel/d/i;

    invoke-virtual {v1}, Lcom/peel/d/i;->e()Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 1245
    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v0}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v0

    .line 1246
    if-eqz v0, :cond_0

    .line 1247
    invoke-virtual {v0}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/data/at;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/peel/util/bx;->b(Landroid/content/Context;Ljava/lang/String;)V

    .line 1249
    invoke-virtual {v0}, Lcom/peel/control/RoomControl;->d()Lcom/peel/control/a;

    move-result-object v0

    .line 1250
    if-eqz v0, :cond_0

    .line 1251
    iget-object v1, p0, Lcom/peel/main/Home;->g:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string/jumbo v2, "last_activity"

    invoke-virtual {v0}, Lcom/peel/control/a;->b()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 1256
    :cond_0
    invoke-super {p0, p1}, Lcom/peel/util/a/d;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 1257
    return-void
.end method

.method public onSearchRequested()Z
    .locals 1

    .prologue
    .line 1699
    const/4 v0, 0x0

    return v0
.end method

.method protected onStart()V
    .locals 12

    .prologue
    .line 354
    invoke-super {p0}, Lcom/peel/util/a/d;->onStart()V

    .line 356
    invoke-virtual {p0}, Lcom/peel/main/Home;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Ltv/peel/app/c;

    .line 357
    if-eqz v0, :cond_0

    new-instance v1, Ljava/lang/ref/WeakReference;

    invoke-direct {v1, p0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v1, v0, Ltv/peel/app/c;->a:Ljava/lang/ref/WeakReference;

    .line 360
    :cond_0
    :try_start_0
    invoke-virtual {p0}, Lcom/peel/main/Home;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-virtual {p0}, Lcom/peel/main/Home;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v8

    .line 362
    const-string/jumbo v0, "US"

    .line 363
    sget-object v1, Lcom/peel/b/a;->e:Ljava/lang/String;

    sget-object v2, Lcom/peel/b/a;->a:Ljava/util/Map;

    const-string/jumbo v3, "asia"

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string/jumbo v0, "AS"

    move-object v10, v0

    .line 369
    :goto_0
    invoke-static {}, Lcom/peel/util/a/f;->a()Lcom/peel/util/a/f;

    move-result-object v0

    const/4 v1, 0x1

    const/16 v2, 0x3e8

    const/16 v3, 0x7db

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "Handset: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-object v5, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "-"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-object v5, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, -0x1

    sget-object v6, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    iget v7, v8, Landroid/content/pm/PackageInfo;->versionCode:I

    iget-object v8, v8, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    const/4 v9, -0x1

    sget-object v11, Lcom/peel/b/a;->b:Ljava/lang/String;

    .line 371
    invoke-static {p0, v10, v11}, Lcom/peel/util/eg;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    const/4 v11, -0x1

    .line 369
    invoke-virtual/range {v0 .. v11}, Lcom/peel/util/a/f;->a(IIILjava/lang/String;ILjava/lang/String;ILjava/lang/String;ILjava/lang/String;I)V

    .line 373
    invoke-static {}, Lcom/peel/util/a/f;->a()Lcom/peel/util/a/f;

    move-result-object v0

    const/4 v1, 0x1

    const/16 v2, 0x802

    const/16 v3, 0x7db

    invoke-static {p0}, Lcom/peel/util/eg;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/peel/util/a/f;->a(IIILjava/lang/String;)V

    .line 375
    invoke-static {}, Lcom/peel/util/a/f;->a()Lcom/peel/util/a/f;

    move-result-object v0

    const/4 v1, 0x1

    const/16 v2, 0x804

    const/16 v3, 0x7db

    invoke-virtual {p0}, Lcom/peel/main/Home;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/peel/util/a/f;->a(IIILjava/lang/String;)V

    .line 377
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string/jumbo v1, "yyyyMMdd"

    invoke-direct {v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 378
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 379
    invoke-virtual {p0}, Lcom/peel/main/Home;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    invoke-virtual {p0}, Lcom/peel/main/Home;->getPackageName()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v1

    iget-wide v2, v1, Landroid/content/pm/PackageInfo;->firstInstallTime:J

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 387
    :goto_1
    return-void

    .line 364
    :cond_1
    sget-object v1, Lcom/peel/b/a;->e:Ljava/lang/String;

    sget-object v2, Lcom/peel/b/a;->a:Ljava/util/Map;

    const-string/jumbo v3, "europe"

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    const-string/jumbo v0, "EU"

    move-object v10, v0

    goto/16 :goto_0

    .line 365
    :cond_2
    sget-object v1, Lcom/peel/b/a;->e:Ljava/lang/String;

    sget-object v2, Lcom/peel/b/a;->a:Ljava/util/Map;

    const-string/jumbo v3, "australia"

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    const-string/jumbo v0, "AU"

    move-object v10, v0

    goto/16 :goto_0

    .line 366
    :cond_3
    sget-object v1, Lcom/peel/b/a;->e:Ljava/lang/String;

    sget-object v2, Lcom/peel/b/a;->a:Ljava/util/Map;

    const-string/jumbo v3, "latin"

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    const-string/jumbo v0, "LA"

    move-object v10, v0

    goto/16 :goto_0

    .line 367
    :cond_4
    sget-object v1, Lcom/peel/b/a;->e:Ljava/lang/String;

    sget-object v2, Lcom/peel/b/a;->a:Ljava/util/Map;

    const-string/jumbo v3, "bramex"

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    const-string/jumbo v0, "BM"
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-object v10, v0

    goto/16 :goto_0

    .line 383
    :catch_0
    move-exception v0

    .line 384
    sget-object v1, Lcom/peel/main/Home;->d:Ljava/lang/String;

    sget-object v2, Lcom/peel/main/Home;->d:Ljava/lang/String;

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1

    :cond_5
    move-object v10, v0

    goto/16 :goto_0
.end method

.method public onStop()V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 856
    invoke-virtual {p0}, Lcom/peel/main/Home;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Ltv/peel/app/c;

    .line 858
    if-eqz v0, :cond_0

    const/4 v2, 0x0

    iput-object v2, v0, Ltv/peel/app/c;->a:Ljava/lang/ref/WeakReference;

    .line 860
    :cond_0
    invoke-static {p0}, Lcom/peel/d/v;->a(Landroid/content/Context;)Lcom/peel/d/v;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/d/v;->c()V

    .line 861
    sput-boolean v1, Lcom/peel/util/b/a;->a:Z

    .line 864
    invoke-static {}, Lcom/peel/content/a;->c()[Lcom/peel/content/library/Library;

    move-result-object v2

    .line 865
    if-eqz v2, :cond_1

    .line 866
    array-length v3, v2

    move v0, v1

    :goto_0
    if-ge v0, v3, :cond_1

    aget-object v1, v2, v0

    .line 867
    invoke-virtual {v1}, Lcom/peel/content/library/Library;->g()V

    .line 866
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 871
    :cond_1
    invoke-super {p0}, Lcom/peel/util/a/d;->onStop()V

    .line 872
    return-void
.end method
