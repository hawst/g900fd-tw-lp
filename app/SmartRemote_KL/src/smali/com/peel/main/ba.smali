.class Lcom/peel/main/ba;
.super Landroid/content/BroadcastReceiver;


# instance fields
.field final synthetic a:Lcom/peel/main/Main;


# direct methods
.method private constructor <init>(Lcom/peel/main/Main;)V
    .locals 0

    .prologue
    .line 418
    iput-object p1, p0, Lcom/peel/main/ba;->a:Lcom/peel/main/Main;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/peel/main/Main;Lcom/peel/main/as;)V
    .locals 0

    .prologue
    .line 418
    invoke-direct {p0, p1}, Lcom/peel/main/ba;-><init>(Lcom/peel/main/Main;)V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3

    .prologue
    const/4 v2, -0x1

    .line 421
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 422
    const-string/jumbo v1, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 423
    const-string/jumbo v0, "networkType"

    invoke-virtual {p2, v0, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 424
    if-le v1, v2, :cond_0

    .line 425
    const-string/jumbo v0, "connectivity"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 427
    if-eqz v0, :cond_0

    iget-object v2, p0, Lcom/peel/main/ba;->a:Lcom/peel/main/Main;

    invoke-static {v2}, Lcom/peel/main/Main;->d(Lcom/peel/main/Main;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 428
    invoke-virtual {v0, v1}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v0

    .line 429
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 430
    iget-object v0, p0, Lcom/peel/main/ba;->a:Lcom/peel/main/Main;

    invoke-static {v0}, Lcom/peel/main/Main;->e(Lcom/peel/main/Main;)V

    .line 431
    invoke-static {}, Lcom/peel/util/a/f;->a()Lcom/peel/util/a/f;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/util/a/f;->d()V

    .line 437
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/peel/main/ba;->a:Lcom/peel/main/Main;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/peel/main/Main;->c(Lcom/peel/main/Main;Z)Z

    .line 439
    :cond_1
    return-void

    .line 433
    :cond_2
    iget-object v0, p0, Lcom/peel/main/ba;->a:Lcom/peel/main/Main;

    invoke-static {v0, p1}, Lcom/peel/main/Main;->a(Lcom/peel/main/Main;Landroid/content/Context;)V

    goto :goto_0
.end method
