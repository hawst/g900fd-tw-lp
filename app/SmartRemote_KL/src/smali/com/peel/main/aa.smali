.class Lcom/peel/main/aa;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic a:Lcom/peel/main/z;


# direct methods
.method constructor <init>(Lcom/peel/main/z;)V
    .locals 0

    .prologue
    .line 1557
    iput-object p1, p0, Lcom/peel/main/aa;->a:Lcom/peel/main/z;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 6

    .prologue
    .line 1560
    invoke-static {}, Lcom/peel/util/a/f;->a()Lcom/peel/util/a/f;

    move-result-object v0

    const/4 v1, 0x1

    const/16 v2, 0x40e

    const/16 v3, 0x7db

    const-string/jumbo v4, "Optional Upgrade"

    const/4 v5, -0x1

    invoke-virtual/range {v0 .. v5}, Lcom/peel/util/a/f;->a(IIILjava/lang/String;I)V

    .line 1562
    sget-object v0, Lcom/peel/c/a;->b:Lcom/peel/c/e;

    invoke-static {v0}, Lcom/peel/c/a;->a(Lcom/peel/c/e;)Ljava/lang/Object;

    move-result-object v0

    sget-object v1, Lcom/peel/c/b;->c:Lcom/peel/c/b;

    if-ne v0, v1, :cond_2

    .line 1563
    invoke-static {}, Lcom/peel/main/Home;->d()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/peel/main/aa;->a:Lcom/peel/main/z;

    iget-object v0, v0, Lcom/peel/main/z;->a:Lcom/peel/main/w;

    iget-object v0, v0, Lcom/peel/main/w;->b:Lcom/peel/main/Home;

    const-string/jumbo v1, "com.android.vending"

    invoke-static {v0, v1}, Lcom/peel/util/bx;->c(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1564
    iget-object v0, p0, Lcom/peel/main/aa;->a:Lcom/peel/main/z;

    iget-object v0, v0, Lcom/peel/main/z;->a:Lcom/peel/main/w;

    iget-object v0, v0, Lcom/peel/main/w;->b:Lcom/peel/main/Home;

    new-instance v1, Landroid/content/Intent;

    const-string/jumbo v2, "android.intent.action.VIEW"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "http://market.android.com/details?id="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/peel/main/aa;->a:Lcom/peel/main/z;

    iget-object v4, v4, Lcom/peel/main/z;->a:Lcom/peel/main/w;

    iget-object v4, v4, Lcom/peel/main/w;->b:Lcom/peel/main/Home;

    invoke-virtual {v4}, Lcom/peel/main/Home;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {v0, v1}, Lcom/peel/main/Home;->startActivity(Landroid/content/Intent;)V

    .line 1584
    :goto_0
    iget-object v0, p0, Lcom/peel/main/aa;->a:Lcom/peel/main/z;

    iget-object v0, v0, Lcom/peel/main/z;->a:Lcom/peel/main/w;

    iget-object v0, v0, Lcom/peel/main/w;->b:Lcom/peel/main/Home;

    invoke-virtual {v0}, Lcom/peel/main/Home;->finish()V

    .line 1585
    return-void

    .line 1565
    :cond_0
    sget-object v0, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "ZTE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "Z787"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1567
    iget-object v0, p0, Lcom/peel/main/aa;->a:Lcom/peel/main/z;

    iget-object v0, v0, Lcom/peel/main/z;->a:Lcom/peel/main/w;

    iget-object v0, v0, Lcom/peel/main/w;->b:Lcom/peel/main/Home;

    new-instance v1, Landroid/content/Intent;

    const-string/jumbo v2, "android.intent.action.VIEW"

    const-string/jumbo v3, "http://download.peel.com/app/PeelSmartRemoteTCL_ZTE.apk"

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {v0, v1}, Lcom/peel/main/Home;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 1569
    :cond_1
    iget-object v0, p0, Lcom/peel/main/aa;->a:Lcom/peel/main/z;

    iget-object v0, v0, Lcom/peel/main/z;->a:Lcom/peel/main/w;

    iget-object v0, v0, Lcom/peel/main/w;->b:Lcom/peel/main/Home;

    new-instance v1, Landroid/content/Intent;

    const-string/jumbo v2, "android.intent.action.VIEW"

    iget-object v3, p0, Lcom/peel/main/aa;->a:Lcom/peel/main/z;

    iget-object v3, v3, Lcom/peel/main/z;->a:Lcom/peel/main/w;

    iget-object v3, v3, Lcom/peel/main/w;->a:Ljava/lang/String;

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {v0, v1}, Lcom/peel/main/Home;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 1572
    :cond_2
    iget-object v0, p0, Lcom/peel/main/aa;->a:Lcom/peel/main/z;

    iget-object v0, v0, Lcom/peel/main/z;->a:Lcom/peel/main/w;

    iget-object v0, v0, Lcom/peel/main/w;->b:Lcom/peel/main/Home;

    const-string/jumbo v1, "com.android.vending"

    invoke-static {v0, v1}, Lcom/peel/util/bx;->c(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1573
    iget-object v0, p0, Lcom/peel/main/aa;->a:Lcom/peel/main/z;

    iget-object v0, v0, Lcom/peel/main/z;->a:Lcom/peel/main/w;

    iget-object v0, v0, Lcom/peel/main/w;->b:Lcom/peel/main/Home;

    new-instance v1, Landroid/content/Intent;

    const-string/jumbo v2, "android.intent.action.VIEW"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "http://market.android.com/details?id="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/peel/main/aa;->a:Lcom/peel/main/z;

    iget-object v4, v4, Lcom/peel/main/z;->a:Lcom/peel/main/w;

    iget-object v4, v4, Lcom/peel/main/w;->b:Lcom/peel/main/Home;

    invoke-virtual {v4}, Lcom/peel/main/Home;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {v0, v1}, Lcom/peel/main/Home;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 1574
    :cond_3
    iget-object v0, p0, Lcom/peel/main/aa;->a:Lcom/peel/main/z;

    iget-object v0, v0, Lcom/peel/main/z;->a:Lcom/peel/main/w;

    iget-object v0, v0, Lcom/peel/main/w;->b:Lcom/peel/main/Home;

    const-string/jumbo v1, "com.sec.android.app.samsungapps"

    invoke-static {v0, v1}, Lcom/peel/util/bx;->c(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1575
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 1576
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "samsungapps://ProductDetail/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/peel/main/aa;->a:Lcom/peel/main/z;

    iget-object v2, v2, Lcom/peel/main/z;->a:Lcom/peel/main/w;

    iget-object v2, v2, Lcom/peel/main/w;->b:Lcom/peel/main/Home;

    invoke-virtual {v2}, Lcom/peel/main/Home;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 1577
    const v1, 0x14000020

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 1578
    iget-object v1, p0, Lcom/peel/main/aa;->a:Lcom/peel/main/z;

    iget-object v1, v1, Lcom/peel/main/z;->a:Lcom/peel/main/w;

    iget-object v1, v1, Lcom/peel/main/w;->b:Lcom/peel/main/Home;

    invoke-virtual {v1, v0}, Lcom/peel/main/Home;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 1580
    :cond_4
    iget-object v0, p0, Lcom/peel/main/aa;->a:Lcom/peel/main/z;

    iget-object v0, v0, Lcom/peel/main/z;->a:Lcom/peel/main/w;

    iget-object v0, v0, Lcom/peel/main/w;->b:Lcom/peel/main/Home;

    new-instance v1, Landroid/content/Intent;

    const-string/jumbo v2, "android.intent.action.VIEW"

    const-string/jumbo v3, "http://download.peel.com/app/SamsungWatchOn.apk"

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {v0, v1}, Lcom/peel/main/Home;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0
.end method
