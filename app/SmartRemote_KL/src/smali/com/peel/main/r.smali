.class Lcom/peel/main/r;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/peel/main/Home;


# direct methods
.method constructor <init>(Lcom/peel/main/Home;)V
    .locals 0

    .prologue
    .line 1096
    iput-object p1, p0, Lcom/peel/main/r;->a:Lcom/peel/main/Home;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1099
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 1100
    iget-object v3, p0, Lcom/peel/main/r;->a:Lcom/peel/main/Home;

    invoke-virtual {v3}, Lcom/peel/main/Home;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v3

    const-string/jumbo v4, "setup_type"

    invoke-interface {v3, v4, v1}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v3

    if-ne v0, v3, :cond_0

    .line 1101
    :goto_0
    const-string/jumbo v1, "control_only_mode"

    invoke-virtual {v2, v1, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1102
    const-string/jumbo v1, "passback_clazz"

    const-class v3, Lcom/peel/ui/lq;

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v1, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1103
    iget-object v1, p0, Lcom/peel/main/r;->a:Lcom/peel/main/Home;

    if-eqz v0, :cond_1

    const-class v0, Lcom/peel/i/ai;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-static {v1, v0, v2}, Lcom/peel/d/e;->a(Landroid/support/v4/app/ae;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 1104
    return-void

    :cond_0
    move v0, v1

    .line 1100
    goto :goto_0

    .line 1103
    :cond_1
    const-class v0, Lcom/peel/i/gm;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method
