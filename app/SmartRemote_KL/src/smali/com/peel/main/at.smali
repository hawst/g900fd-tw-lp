.class Lcom/peel/main/at;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic a:Landroid/widget/CheckBox;

.field final synthetic b:Landroid/content/SharedPreferences;

.field final synthetic c:I

.field final synthetic d:Lcom/peel/main/Main;


# direct methods
.method constructor <init>(Lcom/peel/main/Main;Landroid/widget/CheckBox;Landroid/content/SharedPreferences;I)V
    .locals 0

    .prologue
    .line 220
    iput-object p1, p0, Lcom/peel/main/at;->d:Lcom/peel/main/Main;

    iput-object p2, p0, Lcom/peel/main/at;->a:Landroid/widget/CheckBox;

    iput-object p3, p0, Lcom/peel/main/at;->b:Landroid/content/SharedPreferences;

    iput p4, p0, Lcom/peel/main/at;->c:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    .line 223
    invoke-static {}, Lcom/peel/util/a/f;->a()Lcom/peel/util/a/f;

    move-result-object v2

    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v0}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v0

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    const/16 v3, 0x517

    const/16 v4, 0x7db

    invoke-virtual {v2, v0, v3, v4}, Lcom/peel/util/a/f;->a(III)V

    .line 225
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 226
    iget-object v0, p0, Lcom/peel/main/at;->a:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 227
    iget-object v0, p0, Lcom/peel/main/at;->b:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    iget v0, p0, Lcom/peel/main/at;->c:I

    if-nez v0, :cond_2

    const-string/jumbo v0, "network_dialog"

    :goto_1
    iget-object v3, p0, Lcom/peel/main/at;->d:Lcom/peel/main/Main;

    invoke-static {v3}, Lcom/peel/main/Main;->a(Lcom/peel/main/Main;)Z

    move-result v3

    invoke-interface {v2, v0, v3}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 228
    iget-object v0, p0, Lcom/peel/main/at;->b:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    iget v0, p0, Lcom/peel/main/at;->c:I

    if-nez v0, :cond_3

    const-string/jumbo v0, "mobile_network"

    :goto_2
    invoke-interface {v2, v0, v1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 229
    iget-object v0, p0, Lcom/peel/main/at;->b:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v2, "mobile_wifi_state"

    invoke-interface {v0, v2, v1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 231
    :cond_0
    iget-object v0, p0, Lcom/peel/main/at;->d:Lcom/peel/main/Main;

    invoke-static {v0}, Lcom/peel/main/Main;->b(Lcom/peel/main/Main;)V

    .line 232
    return-void

    .line 223
    :cond_1
    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v0}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/data/at;->f()I

    move-result v0

    goto :goto_0

    .line 227
    :cond_2
    const-string/jumbo v0, "roaming_dialog"

    goto :goto_1

    .line 228
    :cond_3
    const-string/jumbo v0, "roaming_network"

    goto :goto_2
.end method
