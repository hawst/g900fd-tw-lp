.class Lcom/peel/main/ac;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/peel/main/ab;


# direct methods
.method constructor <init>(Lcom/peel/main/ab;)V
    .locals 0

    .prologue
    .line 1610
    iput-object p1, p0, Lcom/peel/main/ac;->a:Lcom/peel/main/ab;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .prologue
    const/4 v5, 0x0

    const/4 v6, 0x1

    .line 1613
    iget-object v0, p0, Lcom/peel/main/ac;->a:Lcom/peel/main/ab;

    iget-object v0, v0, Lcom/peel/main/ab;->a:Lcom/peel/main/Home;

    invoke-static {v0}, Lcom/peel/main/Home;->b(Lcom/peel/main/Home;)Lcom/peel/d/i;

    move-result-object v0

    if-nez v0, :cond_1

    .line 1633
    :cond_0
    :goto_0
    return-void

    .line 1615
    :cond_1
    iget-object v0, p0, Lcom/peel/main/ac;->a:Lcom/peel/main/ab;

    iget-object v0, v0, Lcom/peel/main/ab;->a:Lcom/peel/main/Home;

    invoke-static {v0}, Lcom/peel/d/e;->a(Landroid/support/v4/app/ae;)Lcom/peel/d/u;

    move-result-object v0

    .line 1616
    if-eqz v0, :cond_0

    .line 1617
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    .line 1619
    const-class v1, Lcom/peel/ui/lq;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1620
    invoke-static {}, Lcom/peel/util/x;->b()Ljava/lang/String;

    move-result-object v1

    .line 1621
    iget-object v2, p0, Lcom/peel/main/ac;->a:Lcom/peel/main/ab;

    iget-object v2, v2, Lcom/peel/main/ab;->a:Lcom/peel/main/Home;

    invoke-static {v2}, Lcom/peel/main/Home;->b(Lcom/peel/main/Home;)Lcom/peel/d/i;

    move-result-object v2

    const-string/jumbo v3, "time"

    invoke-virtual {v2, v3}, Lcom/peel/d/i;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1622
    iget-object v2, p0, Lcom/peel/main/ac;->a:Lcom/peel/main/ab;

    iget-object v2, v2, Lcom/peel/main/ab;->a:Lcom/peel/main/Home;

    invoke-static {v2}, Lcom/peel/main/Home;->b(Lcom/peel/main/Home;)Lcom/peel/d/i;

    move-result-object v2

    const-string/jumbo v3, "time"

    invoke-virtual {v2, v3, v1}, Lcom/peel/d/i;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1624
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 1625
    const-string/jumbo v3, "time"

    invoke-virtual {v2, v3, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1626
    const-string/jumbo v1, "refresh"

    invoke-virtual {v2, v1, v6}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1627
    const-string/jumbo v1, "selective"

    invoke-virtual {v2, v1, v5}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1628
    const-string/jumbo v1, "refresh_fragments"

    const/4 v3, 0x4

    new-array v3, v3, [Ljava/lang/String;

    const-class v4, Lcom/peel/ui/kr;

    invoke-virtual {v4}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v5

    const-class v4, Lcom/peel/ui/cg;

    invoke-virtual {v4}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v6

    const/4 v4, 0x2

    const-class v5, Lcom/peel/ui/ci;

    invoke-virtual {v5}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x3

    const-class v5, Lcom/peel/ui/ch;

    invoke-virtual {v5}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v2, v1, v3}, Landroid/os/Bundle;->putStringArray(Ljava/lang/String;[Ljava/lang/String;)V

    .line 1629
    sput-boolean v6, Lcom/peel/ui/kj;->g:Z

    .line 1630
    iget-object v1, p0, Lcom/peel/main/ac;->a:Lcom/peel/main/ab;

    iget-object v1, v1, Lcom/peel/main/ab;->a:Lcom/peel/main/Home;

    invoke-static {v1, v0, v2}, Lcom/peel/d/e;->d(Landroid/support/v4/app/ae;Ljava/lang/String;Landroid/os/Bundle;)V

    goto/16 :goto_0
.end method
