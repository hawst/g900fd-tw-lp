.class Lcom/peel/main/e;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/common/api/ResultCallback;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/gms/common/api/ResultCallback",
        "<",
        "Lcom/google/android/gms/common/api/Status;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/peel/main/GoogleLoginActivity;


# direct methods
.method constructor <init>(Lcom/peel/main/GoogleLoginActivity;)V
    .locals 0

    .prologue
    .line 277
    iput-object p1, p0, Lcom/peel/main/e;->a:Lcom/peel/main/GoogleLoginActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Lcom/google/android/gms/common/api/Result;)V
    .locals 0

    .prologue
    .line 277
    check-cast p1, Lcom/google/android/gms/common/api/Status;

    invoke-virtual {p0, p1}, Lcom/peel/main/e;->a(Lcom/google/android/gms/common/api/Status;)V

    return-void
.end method

.method public a(Lcom/google/android/gms/common/api/Status;)V
    .locals 2

    .prologue
    .line 280
    const-string/jumbo v0, "MainActivity"

    const-string/jumbo v1, "User access revoked!"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 281
    iget-object v0, p0, Lcom/peel/main/e;->a:Lcom/peel/main/GoogleLoginActivity;

    invoke-static {v0}, Lcom/peel/main/GoogleLoginActivity;->a(Lcom/peel/main/GoogleLoginActivity;)Lcom/google/android/gms/common/api/GoogleApiClient;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/common/api/GoogleApiClient;->a()V

    .line 282
    return-void
.end method
