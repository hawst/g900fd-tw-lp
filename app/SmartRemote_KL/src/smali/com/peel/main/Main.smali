.class public Lcom/peel/main/Main;
.super Landroid/app/Activity;


# static fields
.field private static final d:Ljava/lang/String;


# instance fields
.field public a:Lcom/peel/widget/ag;

.field public b:Lcom/peel/widget/ag;

.field public c:Lcom/peel/widget/ag;

.field private e:Lcom/peel/main/ba;

.field private f:Z

.field private g:Z

.field private h:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 46
    const-class v0, Lcom/peel/main/Main;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/peel/main/Main;->d:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 45
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 49
    iput-object v1, p0, Lcom/peel/main/Main;->b:Lcom/peel/widget/ag;

    .line 50
    iput-object v1, p0, Lcom/peel/main/Main;->c:Lcom/peel/widget/ag;

    .line 54
    iput-boolean v0, p0, Lcom/peel/main/Main;->f:Z

    iput-boolean v0, p0, Lcom/peel/main/Main;->g:Z

    iput-boolean v0, p0, Lcom/peel/main/Main;->h:Z

    .line 418
    return-void
.end method

.method private a()V
    .locals 11

    .prologue
    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 80
    const-string/jumbo v0, "connectivity"

    invoke-virtual {p0, v0}, Lcom/peel/main/Main;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 81
    const-string/jumbo v1, "network_setup"

    invoke-virtual {p0, v1, v9}, Lcom/peel/main/Main;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    .line 83
    invoke-static {p0}, Lcom/peel/util/bx;->d(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    .line 85
    sget-object v1, Lcom/peel/main/Main;->d:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "\n\ncountry_code: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "\n\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 87
    const/4 v1, 0x0

    .line 88
    if-eqz v0, :cond_e

    .line 89
    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v0

    .line 91
    :goto_0
    const-string/jumbo v1, "wlan_network"

    invoke-interface {v2, v1, v9}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    .line 92
    const-string/jumbo v4, "wlan_dialog"

    invoke-interface {v2, v4, v9}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    .line 95
    if-eqz v0, :cond_7

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getType()I

    move-result v5

    if-nez v5, :cond_7

    .line 97
    const-string/jumbo v1, "mobile_network"

    invoke-interface {v2, v1, v9}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    .line 98
    const-string/jumbo v4, "roaming_network"

    invoke-interface {v2, v4, v9}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    .line 99
    const-string/jumbo v5, "network_dialog"

    invoke-interface {v2, v5, v9}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v5

    .line 100
    const-string/jumbo v6, "roaming_dialog"

    invoke-interface {v2, v6, v9}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    .line 101
    const-class v6, Lcom/peel/main/Main;

    invoke-virtual {v6}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "\n\n network type MOBILE path... network offline: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    sget-boolean v8, Lcom/peel/util/b/a;->a:Z

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, "\n\n, roamingdlgflag : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, ", roamingNetwork : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, ", digflag : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, ", mobileNetwork : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 104
    if-eqz v0, :cond_3

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isRoaming()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 105
    if-nez v2, :cond_1

    .line 106
    const-string/jumbo v0, "Hong Kong"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string/jumbo v0, "Taiwan"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 107
    invoke-direct {p0, p0, v10, v3}, Lcom/peel/main/Main;->a(Landroid/content/Context;ILjava/lang/String;)V

    .line 165
    :goto_1
    return-void

    .line 109
    :cond_0
    invoke-direct {p0}, Lcom/peel/main/Main;->b()V

    goto :goto_1

    .line 112
    :cond_1
    if-eqz v4, :cond_2

    .line 113
    invoke-direct {p0}, Lcom/peel/main/Main;->b()V

    goto :goto_1

    .line 115
    :cond_2
    sput-boolean v10, Lcom/peel/util/b/a;->a:Z

    goto :goto_1

    .line 119
    :cond_3
    if-nez v5, :cond_5

    .line 120
    const-string/jumbo v0, "Hong Kong"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_4

    const-string/jumbo v0, "Taiwan"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 121
    invoke-direct {p0, p0, v9, v3}, Lcom/peel/main/Main;->a(Landroid/content/Context;ILjava/lang/String;)V

    goto :goto_1

    .line 123
    :cond_4
    invoke-direct {p0}, Lcom/peel/main/Main;->b()V

    goto :goto_1

    .line 126
    :cond_5
    if-eqz v1, :cond_6

    .line 127
    invoke-direct {p0}, Lcom/peel/main/Main;->b()V

    goto :goto_1

    .line 129
    :cond_6
    sput-boolean v10, Lcom/peel/util/b/a;->a:Z

    goto :goto_1

    .line 133
    :cond_7
    if-eqz v0, :cond_b

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getType()I

    move-result v2

    if-ne v2, v10, :cond_b

    .line 134
    sget-object v0, Lcom/peel/main/Main;->d:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "\n\n network type WIFI path... network offline: "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-boolean v5, Lcom/peel/util/b/a;->a:Z

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v5, "\n\n"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v5, ", wlanDlgFlag :: "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v5, ", wlanNetwork :: "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 135
    const-string/jumbo v0, "china"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 136
    if-eqz v4, :cond_9

    .line 137
    if-eqz v1, :cond_8

    .line 138
    invoke-direct {p0}, Lcom/peel/main/Main;->b()V

    goto/16 :goto_1

    .line 140
    :cond_8
    invoke-virtual {p0}, Lcom/peel/main/Main;->finish()V

    goto/16 :goto_1

    .line 143
    :cond_9
    invoke-direct {p0, p0}, Lcom/peel/main/Main;->b(Landroid/content/Context;)V

    goto/16 :goto_1

    .line 146
    :cond_a
    invoke-direct {p0}, Lcom/peel/main/Main;->b()V

    goto/16 :goto_1

    .line 148
    :cond_b
    if-nez v0, :cond_c

    .line 150
    sput-boolean v10, Lcom/peel/util/b/a;->a:Z

    .line 151
    invoke-direct {p0, p0}, Lcom/peel/main/Main;->a(Landroid/content/Context;)V

    .line 152
    sget-object v0, Lcom/peel/main/Main;->d:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "\n\n else if networkInfo == null path... network offline: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-boolean v2, Lcom/peel/util/b/a;->a:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "\n\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 154
    :cond_c
    sput-boolean v10, Lcom/peel/util/b/a;->a:Z

    .line 156
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string/jumbo v1, "is_setup_complete"

    invoke-interface {v0, v1, v9}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 157
    if-eqz v0, :cond_d

    .line 159
    invoke-direct {p0}, Lcom/peel/main/Main;->b()V

    goto/16 :goto_1

    .line 161
    :cond_d
    invoke-virtual {p0}, Lcom/peel/main/Main;->finish()V

    goto/16 :goto_1

    :cond_e
    move-object v0, v1

    goto/16 :goto_0
.end method

.method private a(I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 263
    iget-object v0, p0, Lcom/peel/main/Main;->a:Lcom/peel/widget/ag;

    if-eqz v0, :cond_1

    if-eqz p1, :cond_1

    .line 264
    iget-object v0, p0, Lcom/peel/main/Main;->a:Lcom/peel/widget/ag;

    invoke-virtual {v0}, Lcom/peel/widget/ag;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 265
    iget-object v0, p0, Lcom/peel/main/Main;->a:Lcom/peel/widget/ag;

    invoke-virtual {v0}, Lcom/peel/widget/ag;->dismiss()V

    .line 267
    :cond_0
    iput-object v1, p0, Lcom/peel/main/Main;->a:Lcom/peel/widget/ag;

    .line 270
    :cond_1
    iget-object v0, p0, Lcom/peel/main/Main;->b:Lcom/peel/widget/ag;

    if-eqz v0, :cond_3

    const/4 v0, 0x1

    if-eq p1, v0, :cond_3

    .line 271
    iget-object v0, p0, Lcom/peel/main/Main;->b:Lcom/peel/widget/ag;

    invoke-virtual {v0}, Lcom/peel/widget/ag;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 272
    iget-object v0, p0, Lcom/peel/main/Main;->b:Lcom/peel/widget/ag;

    invoke-virtual {v0}, Lcom/peel/widget/ag;->dismiss()V

    .line 274
    :cond_2
    iput-object v1, p0, Lcom/peel/main/Main;->b:Lcom/peel/widget/ag;

    .line 277
    :cond_3
    iget-object v0, p0, Lcom/peel/main/Main;->c:Lcom/peel/widget/ag;

    if-eqz v0, :cond_5

    const/4 v0, 0x2

    if-eq p1, v0, :cond_5

    .line 278
    iget-object v0, p0, Lcom/peel/main/Main;->c:Lcom/peel/widget/ag;

    invoke-virtual {v0}, Lcom/peel/widget/ag;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 279
    iget-object v0, p0, Lcom/peel/main/Main;->c:Lcom/peel/widget/ag;

    invoke-virtual {v0}, Lcom/peel/widget/ag;->dismiss()V

    .line 281
    :cond_4
    iput-object v1, p0, Lcom/peel/main/Main;->c:Lcom/peel/widget/ag;

    .line 283
    :cond_5
    return-void
.end method

.method private a(Landroid/content/Context;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 286
    invoke-direct {p0, v3}, Lcom/peel/main/Main;->a(I)V

    .line 287
    iget-object v0, p0, Lcom/peel/main/Main;->a:Lcom/peel/widget/ag;

    if-nez v0, :cond_0

    .line 288
    new-instance v0, Lcom/peel/widget/ag;

    invoke-direct {v0, p1}, Lcom/peel/widget/ag;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/peel/main/Main;->a:Lcom/peel/widget/ag;

    .line 289
    iget-object v0, p0, Lcom/peel/main/Main;->a:Lcom/peel/widget/ag;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/peel/ui/ft;->no_internet:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/peel/widget/ag;->a(Ljava/lang/CharSequence;)Lcom/peel/widget/ag;

    move-result-object v0

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/peel/ui/ft;->no_internet_alert:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/peel/widget/ag;->b(Ljava/lang/CharSequence;)Lcom/peel/widget/ag;

    move-result-object v0

    sget v1, Lcom/peel/ui/ft;->label_settings:I

    new-instance v2, Lcom/peel/main/aw;

    invoke-direct {v2, p0, p1}, Lcom/peel/main/aw;-><init>(Lcom/peel/main/Main;Landroid/content/Context;)V

    invoke-virtual {v0, v1, v2}, Lcom/peel/widget/ag;->c(ILandroid/content/DialogInterface$OnClickListener;)Lcom/peel/widget/ag;

    move-result-object v0

    sget v1, Lcom/peel/ui/ft;->ok:I

    new-instance v2, Lcom/peel/main/av;

    invoke-direct {v2, p0}, Lcom/peel/main/av;-><init>(Lcom/peel/main/Main;)V

    .line 294
    invoke-virtual {v0, v1, v2}, Lcom/peel/widget/ag;->a(ILandroid/content/DialogInterface$OnClickListener;)Lcom/peel/widget/ag;

    move-result-object v0

    .line 306
    invoke-virtual {v0, v3}, Lcom/peel/widget/ag;->a(Z)Lcom/peel/widget/ag;

    .line 307
    iget-object v0, p0, Lcom/peel/main/Main;->a:Lcom/peel/widget/ag;

    invoke-virtual {v0, v3}, Lcom/peel/widget/ag;->setCanceledOnTouchOutside(Z)V

    .line 309
    :cond_0
    iget-object v0, p0, Lcom/peel/main/Main;->a:Lcom/peel/widget/ag;

    invoke-virtual {v0}, Lcom/peel/widget/ag;->isShowing()Z

    move-result v0

    if-nez v0, :cond_1

    .line 310
    iget-object v0, p0, Lcom/peel/main/Main;->a:Lcom/peel/widget/ag;

    iget-object v1, p0, Lcom/peel/main/Main;->a:Lcom/peel/widget/ag;

    invoke-virtual {v0, v1}, Lcom/peel/widget/ag;->a(Lcom/peel/widget/ag;)Lcom/peel/widget/ag;

    .line 311
    iget-object v0, p0, Lcom/peel/main/Main;->a:Lcom/peel/widget/ag;

    invoke-virtual {v0}, Lcom/peel/widget/ag;->show()V

    .line 313
    :cond_1
    return-void
.end method

.method private a(Landroid/content/Context;ILjava/lang/String;)V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 168
    sput-boolean v6, Lcom/peel/util/b/a;->a:Z

    .line 170
    invoke-direct {p0, v6}, Lcom/peel/main/Main;->a(I)V

    .line 172
    iget-object v0, p0, Lcom/peel/main/Main;->b:Lcom/peel/widget/ag;

    if-nez v0, :cond_1

    .line 173
    const-string/jumbo v0, "network_setup"

    invoke-virtual {p1, v0, v7}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 174
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 175
    sget v2, Lcom/peel/ui/fq;->network_dialog:I

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 178
    sget v0, Lcom/peel/ui/fp;->do_not_show_btn:I

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    .line 180
    new-instance v3, Lcom/peel/main/as;

    invoke-direct {v3, p0}, Lcom/peel/main/as;-><init>(Lcom/peel/main/Main;)V

    invoke-virtual {v0, v3}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 206
    new-instance v3, Lcom/peel/widget/ag;

    invoke-direct {v3, p1}, Lcom/peel/widget/ag;-><init>(Landroid/content/Context;)V

    iput-object v3, p0, Lcom/peel/main/Main;->b:Lcom/peel/widget/ag;

    .line 207
    iget-object v3, p0, Lcom/peel/main/Main;->b:Lcom/peel/widget/ag;

    sget v4, Lcom/peel/ui/ft;->network_mobile_title:I

    invoke-virtual {v3, v4}, Lcom/peel/widget/ag;->a(I)Lcom/peel/widget/ag;

    move-result-object v3

    sget v4, Lcom/peel/ui/ft;->cancel:I

    new-instance v5, Lcom/peel/main/au;

    invoke-direct {v5, p0, v1, p2}, Lcom/peel/main/au;-><init>(Lcom/peel/main/Main;Landroid/content/SharedPreferences;I)V

    invoke-virtual {v3, v4, v5}, Lcom/peel/widget/ag;->c(ILandroid/content/DialogInterface$OnClickListener;)Lcom/peel/widget/ag;

    move-result-object v3

    sget v4, Lcom/peel/ui/ft;->network_connect:I

    new-instance v5, Lcom/peel/main/at;

    invoke-direct {v5, p0, v0, v1, p2}, Lcom/peel/main/at;-><init>(Lcom/peel/main/Main;Landroid/widget/CheckBox;Landroid/content/SharedPreferences;I)V

    .line 220
    invoke-virtual {v3, v4, v5}, Lcom/peel/widget/ag;->a(ILandroid/content/DialogInterface$OnClickListener;)Lcom/peel/widget/ag;

    move-result-object v1

    .line 233
    invoke-virtual {v1, v7}, Lcom/peel/widget/ag;->a(Z)Lcom/peel/widget/ag;

    .line 235
    if-ne v6, p2, :cond_0

    .line 236
    sget v1, Lcom/peel/ui/fp;->msg:I

    invoke-virtual {v2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    sget v3, Lcom/peel/ui/ft;->network_extra_charge:I

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(I)V

    .line 239
    :cond_0
    const-string/jumbo v1, "china"

    invoke-virtual {v1, p3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 240
    sget v1, Lcom/peel/ui/fp;->msg:I

    invoke-virtual {v2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    .line 241
    sget v1, Lcom/peel/ui/fp;->msg:I

    invoke-virtual {v2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    const-string/jumbo v4, "Wi-Fi"

    const-string/jumbo v5, "WLAN"

    invoke-virtual {v3, v4, v5}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 243
    invoke-virtual {v0, v6}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 244
    iput-boolean v6, p0, Lcom/peel/main/Main;->g:Z

    .line 250
    :goto_0
    iget-object v0, p0, Lcom/peel/main/Main;->b:Lcom/peel/widget/ag;

    invoke-virtual {v0, v2}, Lcom/peel/widget/ag;->a(Landroid/view/View;)Lcom/peel/widget/ag;

    .line 251
    iget-object v0, p0, Lcom/peel/main/Main;->b:Lcom/peel/widget/ag;

    invoke-virtual {v0, v7}, Lcom/peel/widget/ag;->setCanceledOnTouchOutside(Z)V

    .line 254
    :cond_1
    iget-object v0, p0, Lcom/peel/main/Main;->b:Lcom/peel/widget/ag;

    invoke-virtual {v0}, Lcom/peel/widget/ag;->isShowing()Z

    move-result v0

    if-nez v0, :cond_2

    .line 255
    iget-object v0, p0, Lcom/peel/main/Main;->b:Lcom/peel/widget/ag;

    iget-object v1, p0, Lcom/peel/main/Main;->b:Lcom/peel/widget/ag;

    invoke-virtual {v0, v1}, Lcom/peel/widget/ag;->a(Lcom/peel/widget/ag;)Lcom/peel/widget/ag;

    .line 256
    iget-object v0, p0, Lcom/peel/main/Main;->b:Lcom/peel/widget/ag;

    invoke-virtual {v0}, Lcom/peel/widget/ag;->show()V

    .line 260
    :cond_2
    return-void

    .line 246
    :cond_3
    sget v0, Lcom/peel/ui/fp;->msg:I

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    .line 247
    sget v0, Lcom/peel/ui/fp;->msg:I

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const-string/jumbo v3, "WLAN"

    const-string/jumbo v4, "Wi-Fi"

    invoke-virtual {v1, v3, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method static synthetic a(Lcom/peel/main/Main;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 45
    invoke-direct {p0, p1}, Lcom/peel/main/Main;->a(Landroid/content/Context;)V

    return-void
.end method

.method static synthetic a(Lcom/peel/main/Main;)Z
    .locals 1

    .prologue
    .line 45
    iget-boolean v0, p0, Lcom/peel/main/Main;->g:Z

    return v0
.end method

.method static synthetic a(Lcom/peel/main/Main;Z)Z
    .locals 0

    .prologue
    .line 45
    iput-boolean p1, p0, Lcom/peel/main/Main;->g:Z

    return p1
.end method

.method private b()V
    .locals 2

    .prologue
    .line 407
    const/4 v0, 0x0

    sput-boolean v0, Lcom/peel/util/b/a;->a:Z

    .line 410
    new-instance v0, Landroid/content/Intent;

    const-class v1, Ltv/peel/widget/service/WidgetTimerService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 411
    invoke-virtual {p0, v0}, Lcom/peel/main/Main;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 413
    const/4 v0, -0x1

    invoke-direct {p0, v0}, Lcom/peel/main/Main;->a(I)V

    .line 414
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/peel/splash/LoadingSplash;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Lcom/peel/main/Main;->startActivity(Landroid/content/Intent;)V

    .line 415
    invoke-virtual {p0}, Lcom/peel/main/Main;->finish()V

    .line 416
    return-void
.end method

.method private b(Landroid/content/Context;)V
    .locals 7

    .prologue
    const/4 v4, 0x1

    const/4 v6, 0x0

    .line 329
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/peel/main/Main;->a(I)V

    .line 331
    iget-object v0, p0, Lcom/peel/main/Main;->c:Lcom/peel/widget/ag;

    if-nez v0, :cond_0

    .line 332
    const-string/jumbo v0, "network_setup"

    invoke-virtual {p1, v0, v6}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    .line 333
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 334
    sget v1, Lcom/peel/ui/fq;->network_dialog:I

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .line 336
    sget v0, Lcom/peel/ui/fp;->do_not_show_btn:I

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    .line 338
    new-instance v1, Lcom/peel/main/ax;

    invoke-direct {v1, p0}, Lcom/peel/main/ax;-><init>(Lcom/peel/main/Main;)V

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 346
    sget v1, Lcom/peel/ui/fp;->popup_linear:I

    invoke-virtual {v3, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    .line 352
    invoke-virtual {v0, v4}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 354
    iput-boolean v4, p0, Lcom/peel/main/Main;->h:Z

    .line 368
    new-instance v1, Lcom/peel/widget/ag;

    invoke-direct {v1, p1}, Lcom/peel/widget/ag;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/peel/main/Main;->c:Lcom/peel/widget/ag;

    .line 369
    iget-object v1, p0, Lcom/peel/main/Main;->c:Lcom/peel/widget/ag;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    sget v5, Lcom/peel/ui/ft;->network_connect_to_wlan:I

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/peel/widget/ag;->a(Ljava/lang/CharSequence;)Lcom/peel/widget/ag;

    move-result-object v1

    sget v4, Lcom/peel/ui/ft;->cancel:I

    new-instance v5, Lcom/peel/main/az;

    invoke-direct {v5, p0, v2}, Lcom/peel/main/az;-><init>(Lcom/peel/main/Main;Landroid/content/SharedPreferences;)V

    .line 370
    invoke-virtual {v1, v4, v5}, Lcom/peel/widget/ag;->c(ILandroid/content/DialogInterface$OnClickListener;)Lcom/peel/widget/ag;

    move-result-object v1

    sget v4, Lcom/peel/ui/ft;->network_connect:I

    new-instance v5, Lcom/peel/main/ay;

    invoke-direct {v5, p0, v0, v2}, Lcom/peel/main/ay;-><init>(Lcom/peel/main/Main;Landroid/widget/CheckBox;Landroid/content/SharedPreferences;)V

    .line 381
    invoke-virtual {v1, v4, v5}, Lcom/peel/widget/ag;->a(ILandroid/content/DialogInterface$OnClickListener;)Lcom/peel/widget/ag;

    move-result-object v0

    .line 392
    invoke-virtual {v0, v6}, Lcom/peel/widget/ag;->a(Z)Lcom/peel/widget/ag;

    .line 394
    sget v0, Lcom/peel/ui/fp;->msg:I

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    sget v1, Lcom/peel/ui/ft;->network_will_connect_to_wlan:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 396
    iget-object v0, p0, Lcom/peel/main/Main;->c:Lcom/peel/widget/ag;

    invoke-virtual {v0, v3}, Lcom/peel/widget/ag;->a(Landroid/view/View;)Lcom/peel/widget/ag;

    .line 397
    iget-object v0, p0, Lcom/peel/main/Main;->c:Lcom/peel/widget/ag;

    invoke-virtual {v0, v6}, Lcom/peel/widget/ag;->setCanceledOnTouchOutside(Z)V

    .line 400
    :cond_0
    iget-object v0, p0, Lcom/peel/main/Main;->c:Lcom/peel/widget/ag;

    invoke-virtual {v0}, Lcom/peel/widget/ag;->isShowing()Z

    move-result v0

    if-nez v0, :cond_1

    .line 401
    iget-object v0, p0, Lcom/peel/main/Main;->c:Lcom/peel/widget/ag;

    iget-object v1, p0, Lcom/peel/main/Main;->c:Lcom/peel/widget/ag;

    invoke-virtual {v0, v1}, Lcom/peel/widget/ag;->a(Lcom/peel/widget/ag;)Lcom/peel/widget/ag;

    .line 402
    iget-object v0, p0, Lcom/peel/main/Main;->c:Lcom/peel/widget/ag;

    invoke-virtual {v0}, Lcom/peel/widget/ag;->show()V

    .line 404
    :cond_1
    return-void
.end method

.method static synthetic b(Lcom/peel/main/Main;)V
    .locals 0

    .prologue
    .line 45
    invoke-direct {p0}, Lcom/peel/main/Main;->b()V

    return-void
.end method

.method static synthetic b(Lcom/peel/main/Main;Z)Z
    .locals 0

    .prologue
    .line 45
    iput-boolean p1, p0, Lcom/peel/main/Main;->h:Z

    return p1
.end method

.method static synthetic c(Lcom/peel/main/Main;)Z
    .locals 1

    .prologue
    .line 45
    iget-boolean v0, p0, Lcom/peel/main/Main;->h:Z

    return v0
.end method

.method static synthetic c(Lcom/peel/main/Main;Z)Z
    .locals 0

    .prologue
    .line 45
    iput-boolean p1, p0, Lcom/peel/main/Main;->f:Z

    return p1
.end method

.method static synthetic d(Lcom/peel/main/Main;)Z
    .locals 1

    .prologue
    .line 45
    iget-boolean v0, p0, Lcom/peel/main/Main;->f:Z

    return v0
.end method

.method static synthetic e(Lcom/peel/main/Main;)V
    .locals 0

    .prologue
    .line 45
    invoke-direct {p0}, Lcom/peel/main/Main;->a()V

    return-void
.end method


# virtual methods
.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 317
    invoke-super {p0, p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 319
    iget v0, p1, Landroid/content/res/Configuration;->orientation:I

    .line 320
    if-ne v0, v2, :cond_0

    .line 326
    :goto_0
    return-void

    .line 321
    :cond_0
    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    .line 322
    invoke-virtual {p0, v2}, Lcom/peel/main/Main;->setRequestedOrientation(I)V

    goto :goto_0

    .line 324
    :cond_1
    invoke-virtual {p0, v2}, Lcom/peel/main/Main;->setRequestedOrientation(I)V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 58
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 60
    invoke-direct {p0}, Lcom/peel/main/Main;->a()V

    .line 61
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/peel/main/Main;->f:Z

    .line 62
    new-instance v0, Lcom/peel/main/ba;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/peel/main/ba;-><init>(Lcom/peel/main/Main;Lcom/peel/main/as;)V

    iput-object v0, p0, Lcom/peel/main/Main;->e:Lcom/peel/main/ba;

    .line 63
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 64
    const-string/jumbo v1, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 65
    iget-object v1, p0, Lcom/peel/main/Main;->e:Lcom/peel/main/ba;

    invoke-virtual {p0, v1, v0}, Lcom/peel/main/Main;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 66
    return-void
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 70
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 72
    iget-object v0, p0, Lcom/peel/main/Main;->e:Lcom/peel/main/ba;

    if-eqz v0, :cond_0

    .line 73
    iget-object v0, p0, Lcom/peel/main/Main;->e:Lcom/peel/main/ba;

    invoke-virtual {p0, v0}, Lcom/peel/main/Main;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 75
    :cond_0
    return-void
.end method
