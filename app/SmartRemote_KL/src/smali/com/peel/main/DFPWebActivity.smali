.class public Lcom/peel/main/DFPWebActivity;
.super Landroid/app/Activity;


# static fields
.field private static final a:Ljava/lang/String;

.field private static b:Lcom/peel/widget/m;


# instance fields
.field private c:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27
    const-class v0, Lcom/peel/main/DFPWebActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/peel/main/DFPWebActivity;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method

.method static synthetic a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 25
    sget-object v0, Lcom/peel/main/DFPWebActivity;->a:Ljava/lang/String;

    return-object v0
.end method

.method public static a(Lcom/peel/widget/m;)V
    .locals 0

    .prologue
    .line 118
    sput-object p0, Lcom/peel/main/DFPWebActivity;->b:Lcom/peel/widget/m;

    .line 119
    return-void
.end method

.method static synthetic a(Lcom/peel/main/DFPWebActivity;Z)Z
    .locals 0

    .prologue
    .line 25
    iput-boolean p1, p0, Lcom/peel/main/DFPWebActivity;->c:Z

    return p1
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    const/16 v2, 0x400

    const/4 v0, 0x0

    .line 34
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 35
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/peel/main/DFPWebActivity;->requestWindowFeature(I)Z

    .line 36
    invoke-virtual {p0}, Lcom/peel/main/DFPWebActivity;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1, v2, v2}, Landroid/view/Window;->setFlags(II)V

    .line 38
    sget v1, Lcom/peel/ui/fq;->dfp_full_page:I

    invoke-virtual {p0, v1}, Lcom/peel/main/DFPWebActivity;->setContentView(I)V

    .line 40
    invoke-virtual {p0}, Lcom/peel/main/DFPWebActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/peel/main/DFPWebActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "adtimeout"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 42
    :cond_0
    if-lez v0, :cond_1

    .line 43
    sget-object v1, Lcom/peel/main/DFPWebActivity;->a:Ljava/lang/String;

    const-string/jumbo v2, "timeout killing ad"

    new-instance v3, Lcom/peel/main/a;

    invoke-direct {v3, p0}, Lcom/peel/main/a;-><init>(Lcom/peel/main/DFPWebActivity;)V

    int-to-long v4, v0

    invoke-static {v1, v2, v3, v4, v5}, Lcom/peel/util/i;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;J)Ljava/lang/Runnable;

    .line 51
    :cond_1
    sget v0, Lcom/peel/ui/fp;->btn_close:I

    invoke-virtual {p0, v0}, Lcom/peel/main/DFPWebActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/peel/main/b;

    invoke-direct {v1, p0}, Lcom/peel/main/b;-><init>(Lcom/peel/main/DFPWebActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 66
    sget-object v0, Lcom/peel/main/DFPWebActivity;->b:Lcom/peel/widget/m;

    if-eqz v0, :cond_3

    .line 67
    sget-object v0, Lcom/peel/main/DFPWebActivity;->b:Lcom/peel/widget/m;

    iget-object v0, v0, Lcom/peel/widget/m;->a:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 68
    sget-object v0, Lcom/peel/main/DFPWebActivity;->b:Lcom/peel/widget/m;

    iget-object v0, v0, Lcom/peel/widget/m;->a:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    sget-object v1, Lcom/peel/main/DFPWebActivity;->b:Lcom/peel/widget/m;

    iget-object v1, v1, Lcom/peel/widget/m;->a:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 70
    :cond_2
    sget v0, Lcom/peel/ui/fp;->ad_container:I

    invoke-virtual {p0, v0}, Lcom/peel/main/DFPWebActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    sget-object v1, Lcom/peel/main/DFPWebActivity;->b:Lcom/peel/widget/m;

    iget-object v1, v1, Lcom/peel/widget/m;->a:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 71
    sget-object v0, Lcom/peel/main/DFPWebActivity;->b:Lcom/peel/widget/m;

    new-instance v1, Lcom/peel/main/c;

    invoke-direct {v1, p0}, Lcom/peel/main/c;-><init>(Lcom/peel/main/DFPWebActivity;)V

    invoke-virtual {v0, v1}, Lcom/peel/widget/m;->a(Lcom/peel/widget/r;)V

    .line 95
    :cond_3
    return-void
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 113
    sget-object v0, Lcom/peel/main/DFPWebActivity;->b:Lcom/peel/widget/m;

    invoke-virtual {v0}, Lcom/peel/widget/m;->b()V

    .line 114
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 115
    return-void
.end method

.method protected onResume()V
    .locals 1

    .prologue
    .line 99
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 100
    iget-boolean v0, p0, Lcom/peel/main/DFPWebActivity;->c:Z

    if-eqz v0, :cond_0

    .line 101
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/peel/main/DFPWebActivity;->c:Z

    .line 102
    invoke-virtual {p0}, Lcom/peel/main/DFPWebActivity;->finish()V

    .line 104
    :cond_0
    return-void
.end method

.method protected onStop()V
    .locals 0

    .prologue
    .line 108
    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    .line 109
    return-void
.end method
