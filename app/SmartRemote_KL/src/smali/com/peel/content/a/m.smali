.class final Lcom/peel/content/a/m;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Landroid/os/Bundle;

.field final synthetic b:Lcom/peel/util/t;


# direct methods
.method constructor <init>(Landroid/os/Bundle;Lcom/peel/util/t;)V
    .locals 0

    .prologue
    .line 856
    iput-object p1, p0, Lcom/peel/content/a/m;->a:Landroid/os/Bundle;

    iput-object p2, p0, Lcom/peel/content/a/m;->b:Lcom/peel/util/t;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    const/4 v7, 0x2

    const/4 v6, 0x0

    .line 859
    iget-object v0, p0, Lcom/peel/content/a/m;->a:Landroid/os/Bundle;

    const-string/jumbo v1, "user"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 860
    if-nez v1, :cond_0

    .line 861
    iget-object v0, p0, Lcom/peel/content/a/m;->b:Lcom/peel/util/t;

    const/4 v1, 0x0

    const-string/jumbo v2, "missing user id"

    invoke-virtual {v0, v6, v1, v2}, Lcom/peel/util/t;->a(ZLjava/lang/Object;Ljava/lang/String;)V

    .line 884
    :goto_0
    return-void

    .line 864
    :cond_0
    iget-object v0, p0, Lcom/peel/content/a/m;->a:Landroid/os/Bundle;

    const-string/jumbo v2, "url"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 865
    iget-object v0, p0, Lcom/peel/content/a/m;->a:Landroid/os/Bundle;

    const-string/jumbo v3, "library"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 866
    iget-object v0, p0, Lcom/peel/content/a/m;->a:Landroid/os/Bundle;

    const-string/jumbo v4, "room"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/peel/data/ContentRoom;

    .line 868
    const/4 v4, 0x4

    new-array v4, v4, [Ljava/lang/Object;

    sget-object v5, Lcom/peel/content/a/j;->b:Ljava/lang/String;

    aput-object v5, v4, v6

    const/4 v5, 0x1

    aput-object v3, v4, v5

    aput-object v1, v4, v7

    const/4 v1, 0x3

    invoke-virtual {v0}, Lcom/peel/data/ContentRoom;->b()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v4, v1

    invoke-static {v2, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lcom/peel/content/a/n;

    invoke-direct {v1, p0, v7}, Lcom/peel/content/a/n;-><init>(Lcom/peel/content/a/m;I)V

    invoke-static {v0, v1}, Lcom/peel/util/b/a;->b(Ljava/lang/String;Lcom/peel/util/t;)V

    goto :goto_0
.end method
