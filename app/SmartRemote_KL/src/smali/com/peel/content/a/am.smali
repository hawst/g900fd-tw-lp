.class Lcom/peel/content/a/am;
.super Lcom/peel/util/t;


# instance fields
.field final synthetic a:Lcom/peel/content/a/al;


# direct methods
.method constructor <init>(Lcom/peel/content/a/al;I)V
    .locals 0

    .prologue
    .line 1416
    iput-object p1, p0, Lcom/peel/content/a/am;->a:Lcom/peel/content/a/al;

    invoke-direct {p0, p2}, Lcom/peel/util/t;-><init>(I)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x0

    .line 1419
    iget-boolean v0, p0, Lcom/peel/content/a/am;->i:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/peel/content/a/am;->j:Ljava/lang/Object;

    if-nez v0, :cond_1

    .line 1420
    :cond_0
    iget-object v0, p0, Lcom/peel/content/a/am;->a:Lcom/peel/content/a/al;

    iget-object v0, v0, Lcom/peel/content/a/al;->b:Lcom/peel/util/t;

    const-string/jumbo v1, "en"

    invoke-virtual {v0, v5, v1, v6}, Lcom/peel/util/t;->a(ZLjava/lang/Object;Ljava/lang/String;)V

    .line 1425
    :cond_1
    :try_start_0
    sget-object v1, Lcom/peel/content/a/j;->a:Lorg/codehaus/jackson/map/ObjectMapper;

    iget-object v0, p0, Lcom/peel/content/a/am;->j:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    const-class v2, Ljava/util/ArrayList;

    invoke-virtual {v1, v0, v2}, Lorg/codehaus/jackson/map/ObjectMapper;->readValue(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 1426
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/HashMap;

    const-string/jumbo v1, "languages"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1427
    iget-object v1, p0, Lcom/peel/content/a/am;->a:Lcom/peel/content/a/al;

    iget-object v2, v1, Lcom/peel/content/a/al;->b:Lcom/peel/util/t;

    const/4 v3, 0x1

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "EN"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    const-string/jumbo v1, "en,"

    :goto_0
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v0, v4}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v2, v3, v0, v1}, Lcom/peel/util/t;->a(ZLjava/lang/Object;Ljava/lang/String;)V

    .line 1431
    :goto_1
    return-void

    .line 1427
    :cond_2
    const-string/jumbo v1, ""
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1428
    :catch_0
    move-exception v0

    .line 1429
    iget-object v0, p0, Lcom/peel/content/a/am;->a:Lcom/peel/content/a/al;

    iget-object v0, v0, Lcom/peel/content/a/al;->b:Lcom/peel/util/t;

    const-string/jumbo v1, "en"

    invoke-virtual {v0, v5, v1, v6}, Lcom/peel/util/t;->a(ZLjava/lang/Object;Ljava/lang/String;)V

    goto :goto_1
.end method
