.class final Lcom/peel/content/a/au;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Landroid/os/Bundle;

.field final synthetic b:Lcom/peel/util/t;


# direct methods
.method constructor <init>(Landroid/os/Bundle;Lcom/peel/util/t;)V
    .locals 0

    .prologue
    .line 384
    iput-object p1, p0, Lcom/peel/content/a/au;->a:Landroid/os/Bundle;

    iput-object p2, p0, Lcom/peel/content/a/au;->b:Lcom/peel/util/t;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 15

    .prologue
    .line 387
    iget-object v0, p0, Lcom/peel/content/a/au;->a:Landroid/os/Bundle;

    const-string/jumbo v1, "library"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 388
    iget-object v0, p0, Lcom/peel/content/a/au;->a:Landroid/os/Bundle;

    const-string/jumbo v1, "user"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 389
    iget-object v0, p0, Lcom/peel/content/a/au;->a:Landroid/os/Bundle;

    const-string/jumbo v1, "start"

    const-wide/16 v4, -0x1

    invoke-virtual {v0, v1, v4, v5}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v8

    .line 390
    iget-object v0, p0, Lcom/peel/content/a/au;->a:Landroid/os/Bundle;

    const-string/jumbo v1, "room"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/peel/data/ContentRoom;

    .line 391
    iget-object v1, p0, Lcom/peel/content/a/au;->a:Landroid/os/Bundle;

    const-string/jumbo v3, "window"

    const/4 v4, -0x1

    invoke-virtual {v1, v3, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    .line 393
    if-eqz v7, :cond_0

    if-eqz v2, :cond_0

    if-nez v0, :cond_1

    .line 394
    :cond_0
    invoke-static {}, Lcom/peel/content/a/j;->a()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "getListigns missing library or user or room"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 395
    iget-object v0, p0, Lcom/peel/content/a/au;->b:Lcom/peel/util/t;

    const/4 v1, 0x0

    const/4 v2, 0x0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "getListigns missing arguments "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/peel/content/a/au;->a:Landroid/os/Bundle;

    invoke-virtual {v4}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/peel/util/t;->a(ZLjava/lang/Object;Ljava/lang/String;)V

    .line 490
    :goto_0
    return-void

    .line 398
    :cond_1
    const-wide/16 v4, -0x1

    cmp-long v3, v4, v8

    if-eqz v3, :cond_2

    const/4 v3, -0x1

    if-ne v3, v1, :cond_3

    :cond_2
    iget-object v3, p0, Lcom/peel/content/a/au;->a:Landroid/os/Bundle;

    const-string/jumbo v4, "path"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v4, "weeklyfavs"

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 399
    invoke-static {}, Lcom/peel/content/a/j;->a()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "getListigns missing start or window"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 400
    iget-object v0, p0, Lcom/peel/content/a/au;->b:Lcom/peel/util/t;

    const/4 v1, 0x0

    const/4 v2, 0x0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "getListigns missing arguments "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/peel/content/a/au;->a:Landroid/os/Bundle;

    invoke-virtual {v4}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/peel/util/t;->a(ZLjava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 405
    :cond_3
    div-int/lit8 v3, v1, 0x2

    rem-int/lit8 v1, v1, 0x2

    add-int/2addr v3, v1

    .line 407
    new-instance v4, Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v1, 0x1

    invoke-direct {v4, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    .line 409
    iget-object v1, p0, Lcom/peel/content/a/au;->a:Landroid/os/Bundle;

    const-string/jumbo v5, "path"

    const-string/jumbo v6, ""

    invoke-virtual {v1, v5, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 410
    const-string/jumbo v1, "top"

    invoke-virtual {v6, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 411
    iget-object v1, p0, Lcom/peel/content/a/au;->a:Landroid/os/Bundle;

    const-string/jumbo v3, "country"

    invoke-virtual {v1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 412
    iget-object v1, p0, Lcom/peel/content/a/au;->a:Landroid/os/Bundle;

    const-string/jumbo v5, "startofday"

    const-wide/16 v10, -0x1

    invoke-virtual {v1, v5, v10, v11}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v10

    .line 414
    const-string/jumbo v5, "%sepg/schedules/personalization/peelpicksforyou/%s?currentdate=%s&userid=%s&roomid=%d&currenttime=%s&country=%s"

    const/4 v1, 0x7

    new-array v12, v1, [Ljava/lang/Object;

    const/4 v1, 0x0

    sget-object v13, Lcom/peel/content/a/j;->b:Ljava/lang/String;

    aput-object v13, v12, v1

    const/4 v1, 0x1

    aput-object v7, v12, v1

    const/4 v13, 0x2

    sget-object v1, Lcom/peel/util/x;->j:Ljava/lang/ThreadLocal;

    invoke-virtual {v1}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/text/SimpleDateFormat;

    new-instance v14, Ljava/util/Date;

    invoke-direct {v14, v10, v11}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v1, v14}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v12, v13

    const/4 v1, 0x3

    aput-object v2, v12, v1

    const/4 v1, 0x4

    invoke-virtual {v0}, Lcom/peel/data/ContentRoom;->b()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v12, v1

    const/4 v1, 0x5

    sget-object v0, Lcom/peel/util/x;->j:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/text/SimpleDateFormat;

    new-instance v2, Ljava/util/Date;

    invoke-direct {v2, v8, v9}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v0, v2}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v12, v1

    const/4 v0, 0x6

    aput-object v3, v12, v0

    invoke-static {v5, v12}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 415
    const/4 v0, 0x3

    invoke-virtual {v4, v0}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V

    .line 430
    :goto_1
    invoke-static {}, Lcom/peel/content/a/j;->a()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, " *********** listingUrl **************\n "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 432
    new-instance v3, Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v0, 0x1

    invoke-direct {v3, v0}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    .line 435
    new-instance v0, Lcom/peel/content/a/av;

    const/4 v2, 0x2

    move-object v1, p0

    invoke-direct/range {v0 .. v7}, Lcom/peel/content/a/av;-><init>(Lcom/peel/content/a/au;ILjava/util/concurrent/atomic/AtomicInteger;Ljava/util/concurrent/atomic/AtomicInteger;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 489
    invoke-static {v5, v0}, Lcom/peel/util/b/a;->b(Ljava/lang/String;Lcom/peel/util/t;)V

    goto/16 :goto_0

    .line 416
    :cond_4
    const-string/jumbo v1, "weeklyfavs"

    invoke-virtual {v6, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 417
    const-string/jumbo v1, "%sepg/schedules/schedulesforweek/%s/%s?roomid=%d"

    const/4 v3, 0x4

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v5, 0x0

    sget-object v8, Lcom/peel/content/a/j;->b:Ljava/lang/String;

    aput-object v8, v3, v5

    const/4 v5, 0x1

    aput-object v2, v3, v5

    const/4 v2, 0x2

    aput-object v7, v3, v2

    const/4 v2, 0x3

    invoke-virtual {v0}, Lcom/peel/data/ContentRoom;->b()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v3, v2

    invoke-static {v1, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    goto :goto_1

    .line 418
    :cond_5
    const-string/jumbo v1, "channel"

    invoke-virtual {v6, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 419
    const-string/jumbo v0, "%sepg/schedules/schedulebychannel/%s?providerid=%s&limit=%d"

    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    sget-object v3, Lcom/peel/content/a/j;->b:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/peel/content/a/au;->a:Landroid/os/Bundle;

    const-string/jumbo v5, "callsign"

    invoke-virtual {v3, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v5, " "

    const-string/jumbo v8, "%20"

    invoke-virtual {v3, v5, v8}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string/jumbo v3, "&"

    const-string/jumbo v5, "%26"

    invoke-virtual {v7, v3, v5}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x3

    iget-object v3, p0, Lcom/peel/content/a/au;->a:Landroid/os/Bundle;

    const-string/jumbo v5, "limit"

    const/4 v8, 0x1

    invoke-virtual {v3, v5, v8}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    goto/16 :goto_1

    .line 420
    :cond_6
    const-string/jumbo v1, "curated"

    invoke-virtual {v6, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 421
    iget-object v1, p0, Lcom/peel/content/a/au;->a:Landroid/os/Bundle;

    const-string/jumbo v3, "country"

    invoke-virtual {v1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 422
    iget-object v3, p0, Lcom/peel/content/a/au;->a:Landroid/os/Bundle;

    const-string/jumbo v5, "startofday"

    const-wide/16 v10, -0x1

    invoke-virtual {v3, v5, v10, v11}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v10

    .line 423
    const-string/jumbo v3, "%sepg/schedules/getschedulesforcuratedshows/%s?providerid=%s&roomid=%d&currentdate=%s&currenttime=%s&country=%s"

    const/4 v5, 0x7

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v12, 0x0

    sget-object v13, Lcom/peel/content/a/j;->b:Ljava/lang/String;

    aput-object v13, v5, v12

    const/4 v12, 0x1

    aput-object v2, v5, v12

    const/4 v2, 0x2

    aput-object v7, v5, v2

    const/4 v2, 0x3

    invoke-virtual {v0}, Lcom/peel/data/ContentRoom;->b()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v5, v2

    const/4 v2, 0x4

    sget-object v0, Lcom/peel/util/x;->j:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/text/SimpleDateFormat;

    new-instance v12, Ljava/util/Date;

    invoke-direct {v12, v10, v11}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v0, v12}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v5, v2

    const/4 v2, 0x5

    sget-object v0, Lcom/peel/util/x;->j:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/text/SimpleDateFormat;

    new-instance v10, Ljava/util/Date;

    invoke-direct {v10, v8, v9}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v0, v10}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v5, v2

    const/4 v0, 0x6

    aput-object v1, v5, v0

    invoke-static {v3, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 424
    const/4 v0, 0x3

    invoke-virtual {v4, v0}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V

    goto/16 :goto_1

    .line 426
    :cond_7
    const-string/jumbo v5, "%sepg/schedules/stillrunning/%s?start=%s&window=%d&userid=%s&roomid=%d"

    const/4 v1, 0x6

    new-array v10, v1, [Ljava/lang/Object;

    const/4 v1, 0x0

    sget-object v11, Lcom/peel/content/a/j;->b:Ljava/lang/String;

    aput-object v11, v10, v1

    const/4 v1, 0x1

    aput-object v7, v10, v1

    const/4 v11, 0x2

    sget-object v1, Lcom/peel/util/x;->j:Ljava/lang/ThreadLocal;

    invoke-virtual {v1}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/text/SimpleDateFormat;

    new-instance v12, Ljava/util/Date;

    invoke-direct {v12, v8, v9}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v1, v12}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v10, v11

    const/4 v1, 0x3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v10, v1

    const/4 v1, 0x4

    aput-object v2, v10, v1

    const/4 v1, 0x5

    invoke-virtual {v0}, Lcom/peel/data/ContentRoom;->b()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v10, v1

    invoke-static {v5, v10}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 427
    const/4 v0, 0x3

    invoke-virtual {v4, v0}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V

    goto/16 :goto_1
.end method
