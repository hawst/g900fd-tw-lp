.class final Lcom/peel/content/a/aa;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Landroid/os/Bundle;

.field final synthetic b:Lcom/peel/util/t;


# direct methods
.method constructor <init>(Landroid/os/Bundle;Lcom/peel/util/t;)V
    .locals 0

    .prologue
    .line 342
    iput-object p1, p0, Lcom/peel/content/a/aa;->a:Landroid/os/Bundle;

    iput-object p2, p0, Lcom/peel/content/a/aa;->b:Lcom/peel/util/t;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 345
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 346
    const-string/jumbo v1, "remindertype"

    iget-object v2, p0, Lcom/peel/content/a/aa;->a:Landroid/os/Bundle;

    const-string/jumbo v3, "remindertype"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 349
    iget-object v1, p0, Lcom/peel/content/a/aa;->a:Landroid/os/Bundle;

    const-string/jumbo v2, "showid"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 350
    const-string/jumbo v1, "showid"

    iget-object v2, p0, Lcom/peel/content/a/aa;->a:Landroid/os/Bundle;

    const-string/jumbo v3, "showid"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 353
    :cond_0
    iget-object v1, p0, Lcom/peel/content/a/aa;->a:Landroid/os/Bundle;

    const-string/jumbo v2, "episodeid"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 354
    const-string/jumbo v1, "episodeid"

    iget-object v2, p0, Lcom/peel/content/a/aa;->a:Landroid/os/Bundle;

    const-string/jumbo v3, "episodeid"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 357
    :cond_1
    iget-object v1, p0, Lcom/peel/content/a/aa;->a:Landroid/os/Bundle;

    const-string/jumbo v2, "scheduletime"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 358
    const-string/jumbo v1, "scheduletime"

    iget-object v2, p0, Lcom/peel/content/a/aa;->a:Landroid/os/Bundle;

    const-string/jumbo v3, "scheduletime"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 361
    :cond_2
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "%susers/reminders/"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    sget-object v4, Lcom/peel/content/a/j;->b:Ljava/lang/String;

    aput-object v4, v3, v5

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/peel/content/a;->f:Lcom/peel/content/user/User;

    invoke-virtual {v2}, Lcom/peel/content/user/User;->x()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-instance v3, Lcom/peel/content/a/ab;

    invoke-direct {v3, p0}, Lcom/peel/content/a/ab;-><init>(Lcom/peel/content/a/aa;)V

    invoke-static {v1, v2, v0, v5, v3}, Lcom/peel/util/b/a;->a(Ljava/lang/String;Ljava/util/Map;Ljava/util/Map;ZLcom/peel/util/t;)V

    .line 371
    return-void
.end method
