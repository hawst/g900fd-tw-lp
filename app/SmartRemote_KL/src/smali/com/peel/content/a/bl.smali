.class Lcom/peel/content/a/bl;
.super Lcom/peel/util/t;


# instance fields
.field final synthetic a:[Lcom/peel/data/Channel;

.field final synthetic b:Lcom/peel/content/a/bk;


# direct methods
.method constructor <init>(Lcom/peel/content/a/bk;I[Lcom/peel/data/Channel;)V
    .locals 0

    .prologue
    .line 594
    iput-object p1, p0, Lcom/peel/content/a/bl;->b:Lcom/peel/content/a/bk;

    iput-object p3, p0, Lcom/peel/content/a/bl;->a:[Lcom/peel/data/Channel;

    invoke-direct {p0, p2}, Lcom/peel/util/t;-><init>(I)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 12

    .prologue
    const/4 v2, 0x0

    const/4 v11, 0x1

    .line 597
    iget-boolean v0, p0, Lcom/peel/content/a/bl;->i:Z

    if-nez v0, :cond_0

    .line 599
    iget-object v0, p0, Lcom/peel/content/a/bl;->b:Lcom/peel/content/a/bk;

    iget-object v0, v0, Lcom/peel/content/a/bk;->a:Lcom/peel/content/a/bj;

    iget-object v0, v0, Lcom/peel/content/a/bj;->b:Lcom/peel/util/t;

    iget-object v1, p0, Lcom/peel/content/a/bl;->a:[Lcom/peel/data/Channel;

    iget-object v2, p0, Lcom/peel/content/a/bl;->k:Ljava/lang/String;

    invoke-virtual {v0, v11, v1, v2}, Lcom/peel/util/t;->a(ZLjava/lang/Object;Ljava/lang/String;)V

    .line 601
    invoke-static {}, Lcom/peel/content/a/j;->a()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "getExcludedLineup error: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/peel/content/a/bl;->k:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 682
    :goto_0
    return-void

    .line 607
    :cond_0
    iget-object v0, p0, Lcom/peel/content/a/bl;->b:Lcom/peel/content/a/bk;

    iget-object v0, v0, Lcom/peel/content/a/bk;->a:Lcom/peel/content/a/bj;

    iget-object v0, v0, Lcom/peel/content/a/bj;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "country"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 608
    iget-object v0, p0, Lcom/peel/content/a/bl;->b:Lcom/peel/content/a/bk;

    iget-object v0, v0, Lcom/peel/content/a/bk;->a:Lcom/peel/content/a/bj;

    iget-object v0, v0, Lcom/peel/content/a/bj;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "country"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 609
    const-string/jumbo v1, "US"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    .line 610
    const-string/jumbo v3, "CA"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    move v3, v1

    move v1, v0

    .line 613
    :goto_1
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 614
    iget-object v0, p0, Lcom/peel/content/a/bl;->j:Ljava/lang/Object;

    check-cast v0, [Ljava/lang/String;

    check-cast v0, [Ljava/lang/String;

    .line 619
    iget-object v6, p0, Lcom/peel/content/a/bl;->a:[Lcom/peel/data/Channel;

    array-length v7, v6

    move v4, v2

    :goto_2
    if-ge v4, v7, :cond_6

    aget-object v8, v6, v4

    .line 621
    iget-object v9, p0, Lcom/peel/content/a/bl;->b:Lcom/peel/content/a/bk;

    iget-object v9, v9, Lcom/peel/content/a/bk;->a:Lcom/peel/content/a/bj;

    iget-object v9, v9, Lcom/peel/content/a/bj;->a:Ljava/lang/String;

    invoke-virtual {v8, v9}, Lcom/peel/data/Channel;->a(Ljava/lang/String;)V

    .line 624
    if-nez v3, :cond_1

    if-eqz v1, :cond_4

    .line 625
    :cond_1
    if-eqz v3, :cond_3

    invoke-virtual {v8}, Lcom/peel/data/Channel;->h()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v9

    const/4 v10, 0x2

    if-le v9, v10, :cond_3

    .line 626
    invoke-virtual {v8, v11}, Lcom/peel/data/Channel;->b(Z)V

    .line 619
    :cond_2
    :goto_3
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 630
    :cond_3
    invoke-virtual {v8}, Lcom/peel/data/Channel;->g()Ljava/lang/String;

    move-result-object v9

    const-string/jumbo v10, "en"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v9

    if-nez v9, :cond_4

    .line 631
    invoke-virtual {v8, v11}, Lcom/peel/data/Channel;->b(Z)V

    goto :goto_3

    .line 643
    :cond_4
    array-length v9, v0

    if-lez v9, :cond_5

    .line 644
    invoke-virtual {v8}, Lcom/peel/data/Channel;->c()Ljava/lang/String;

    move-result-object v9

    invoke-static {v0, v9}, Ljava/util/Arrays;->binarySearch([Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v9

    if-ltz v9, :cond_5

    .line 645
    invoke-virtual {v8, v11}, Lcom/peel/data/Channel;->b(Z)V

    goto :goto_3

    .line 650
    :cond_5
    invoke-virtual {v8}, Lcom/peel/data/Channel;->l()Z

    move-result v9

    if-nez v9, :cond_2

    .line 651
    invoke-virtual {v8}, Lcom/peel/data/Channel;->c()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string/jumbo v9, ","

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_3

    .line 655
    :cond_6
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    if-lez v0, :cond_a

    .line 657
    iget-object v0, p0, Lcom/peel/content/a/bl;->b:Lcom/peel/content/a/bk;

    iget-object v0, v0, Lcom/peel/content/a/bk;->a:Lcom/peel/content/a/bj;

    iget-object v0, v0, Lcom/peel/content/a/bj;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "prgids"

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->length()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->deleteCharAt(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 659
    iget-object v0, p0, Lcom/peel/content/a/bl;->b:Lcom/peel/content/a/bk;

    iget-object v0, v0, Lcom/peel/content/a/bk;->a:Lcom/peel/content/a/bj;

    iget-object v0, v0, Lcom/peel/content/a/bj;->c:Landroid/os/Bundle;

    invoke-static {v0}, Lcom/peel/content/a/j;->a(Landroid/os/Bundle;)V

    .line 661
    iget-object v0, p0, Lcom/peel/content/a/bl;->b:Lcom/peel/content/a/bk;

    iget-object v0, v0, Lcom/peel/content/a/bk;->a:Lcom/peel/content/a/bj;

    iget-object v0, v0, Lcom/peel/content/a/bj;->d:Lcom/peel/content/user/User;

    if-eqz v0, :cond_a

    iget-object v0, p0, Lcom/peel/content/a/bl;->a:[Lcom/peel/data/Channel;

    if-eqz v0, :cond_a

    iget-object v0, p0, Lcom/peel/content/a/bl;->a:[Lcom/peel/data/Channel;

    array-length v0, v0

    if-lez v0, :cond_a

    .line 662
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 663
    iget-object v0, p0, Lcom/peel/content/a/bl;->a:[Lcom/peel/data/Channel;

    array-length v1, v0

    :goto_4
    if-ge v2, v1, :cond_8

    aget-object v4, v0, v2

    .line 664
    invoke-virtual {v4}, Lcom/peel/data/Channel;->l()Z

    move-result v5

    if-eqz v5, :cond_7

    invoke-virtual {v4}, Lcom/peel/data/Channel;->a()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 663
    :cond_7
    add-int/lit8 v2, v2, 0x1

    goto :goto_4

    .line 667
    :cond_8
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_a

    .line 668
    iget-object v0, p0, Lcom/peel/content/a/bl;->b:Lcom/peel/content/a/bk;

    iget-object v0, v0, Lcom/peel/content/a/bk;->a:Lcom/peel/content/a/bj;

    iget-object v0, v0, Lcom/peel/content/a/bj;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "room"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/peel/data/ContentRoom;

    .line 669
    iget-object v1, p0, Lcom/peel/content/a/bl;->b:Lcom/peel/content/a/bk;

    iget-object v1, v1, Lcom/peel/content/a/bk;->a:Lcom/peel/content/a/bj;

    iget-object v1, v1, Lcom/peel/content/a/bj;->d:Lcom/peel/content/user/User;

    invoke-virtual {v1}, Lcom/peel/content/user/User;->f()Landroid/os/Bundle;

    move-result-object v2

    .line 670
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Lcom/peel/data/ContentRoom;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v4, "/"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v4, p0, Lcom/peel/content/a/bl;->b:Lcom/peel/content/a/bk;

    iget-object v4, v4, Lcom/peel/content/a/bk;->a:Lcom/peel/content/a/bj;

    iget-object v4, v4, Lcom/peel/content/a/bj;->a:Ljava/lang/String;

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    .line 671
    if-nez v1, :cond_9

    .line 672
    new-instance v1, Ljava/util/ArrayList;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v4

    invoke-direct {v1, v4}, Ljava/util/ArrayList;-><init>(I)V

    .line 673
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Lcom/peel/data/ContentRoom;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v4, "/"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v4, p0, Lcom/peel/content/a/bl;->b:Lcom/peel/content/a/bk;

    iget-object v4, v4, Lcom/peel/content/a/bk;->a:Lcom/peel/content/a/bj;

    iget-object v4, v4, Lcom/peel/content/a/bj;->a:Ljava/lang/String;

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object v0, v1

    check-cast v0, Ljava/util/ArrayList;

    invoke-virtual {v2, v4, v0}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 675
    :cond_9
    invoke-interface {v1, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 681
    :cond_a
    iget-object v0, p0, Lcom/peel/content/a/bl;->b:Lcom/peel/content/a/bk;

    iget-object v0, v0, Lcom/peel/content/a/bk;->a:Lcom/peel/content/a/bj;

    iget-object v0, v0, Lcom/peel/content/a/bj;->b:Lcom/peel/util/t;

    iget-object v1, p0, Lcom/peel/content/a/bl;->a:[Lcom/peel/data/Channel;

    const/4 v2, 0x0

    invoke-virtual {v0, v11, v1, v2}, Lcom/peel/util/t;->a(ZLjava/lang/Object;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_b
    move v1, v2

    move v3, v2

    goto/16 :goto_1
.end method
