.class Lcom/peel/content/a/ao;
.super Lcom/peel/util/t;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/peel/util/t",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/peel/content/a/an;


# direct methods
.method constructor <init>(Lcom/peel/content/a/an;)V
    .locals 0

    .prologue
    .line 1486
    iput-object p1, p0, Lcom/peel/content/a/ao;->a:Lcom/peel/content/a/an;

    invoke-direct {p0}, Lcom/peel/util/t;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 1489
    iget-boolean v0, p0, Lcom/peel/content/a/ao;->i:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/peel/content/a/ao;->j:Ljava/lang/Object;

    if-nez v0, :cond_3

    .line 1490
    :cond_0
    iget-object v0, p0, Lcom/peel/content/a/ao;->j:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    const-string/jumbo v1, "3"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1491
    iget-object v0, p0, Lcom/peel/content/a/ao;->a:Lcom/peel/content/a/an;

    iget-object v0, v0, Lcom/peel/content/a/an;->c:Lcom/peel/util/t;

    const/4 v1, 0x3

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iget-object v2, p0, Lcom/peel/content/a/ao;->k:Ljava/lang/String;

    invoke-virtual {v0, v5, v1, v2}, Lcom/peel/util/t;->a(ZLjava/lang/Object;Ljava/lang/String;)V

    .line 1506
    :cond_1
    :goto_0
    return-void

    .line 1493
    :cond_2
    iget-object v0, p0, Lcom/peel/content/a/ao;->a:Lcom/peel/content/a/an;

    iget-object v0, v0, Lcom/peel/content/a/an;->c:Lcom/peel/util/t;

    iget-object v1, p0, Lcom/peel/content/a/ao;->j:Ljava/lang/Object;

    iget-object v2, p0, Lcom/peel/content/a/ao;->k:Ljava/lang/String;

    invoke-virtual {v0, v5, v1, v2}, Lcom/peel/util/t;->a(ZLjava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 1496
    :cond_3
    iget-object v0, p0, Lcom/peel/content/a/ao;->j:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    .line 1497
    invoke-static {}, Lcom/peel/content/a/j;->a()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "cloud version response: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1499
    const-string/jumbo v1, "0"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 1500
    iget-object v0, p0, Lcom/peel/content/a/ao;->a:Lcom/peel/content/a/an;

    iget-object v0, v0, Lcom/peel/content/a/an;->c:Lcom/peel/util/t;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v4, v1, v6}, Lcom/peel/util/t;->a(ZLjava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 1501
    :cond_4
    const-string/jumbo v1, "1"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 1502
    iget-object v0, p0, Lcom/peel/content/a/ao;->a:Lcom/peel/content/a/an;

    iget-object v0, v0, Lcom/peel/content/a/an;->c:Lcom/peel/util/t;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v4, v1, v6}, Lcom/peel/util/t;->a(ZLjava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 1503
    :cond_5
    const-string/jumbo v1, "2"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1504
    iget-object v0, p0, Lcom/peel/content/a/ao;->a:Lcom/peel/content/a/an;

    iget-object v0, v0, Lcom/peel/content/a/an;->c:Lcom/peel/util/t;

    const/4 v1, 0x2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v4, v1, v6}, Lcom/peel/util/t;->a(ZLjava/lang/Object;Ljava/lang/String;)V

    goto :goto_0
.end method
