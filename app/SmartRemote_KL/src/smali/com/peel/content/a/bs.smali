.class Lcom/peel/content/a/bs;
.super Lcom/peel/util/t;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/peel/util/t",
        "<",
        "Ljava/util/Map",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/String;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Ljava/util/HashMap;

.field final synthetic b:Lcom/peel/content/a/br;


# direct methods
.method constructor <init>(Lcom/peel/content/a/br;Ljava/util/HashMap;)V
    .locals 0

    .prologue
    .line 818
    iput-object p1, p0, Lcom/peel/content/a/bs;->b:Lcom/peel/content/a/br;

    iput-object p2, p0, Lcom/peel/content/a/bs;->a:Ljava/util/HashMap;

    invoke-direct {p0}, Lcom/peel/util/t;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(ZLjava/lang/Object;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 818
    check-cast p2, Ljava/util/Map;

    invoke-virtual {p0, p1, p2, p3}, Lcom/peel/content/a/bs;->a(ZLjava/util/Map;Ljava/lang/String;)V

    return-void
.end method

.method public a(ZLjava/util/Map;Ljava/lang/String;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 821
    if-eqz p1, :cond_5

    .line 822
    if-eqz p2, :cond_2

    .line 823
    invoke-interface {p2}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 824
    iget-object v2, p0, Lcom/peel/content/a/bs;->a:Ljava/util/HashMap;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/peel/content/a/bs;->a:Ljava/util/HashMap;

    invoke-virtual {v2, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 825
    :cond_1
    invoke-static {}, Lcom/peel/data/m;->a()Lcom/peel/data/m;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/peel/data/m;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 830
    :cond_2
    iget-object v0, p0, Lcom/peel/content/a/bs;->a:Ljava/util/HashMap;

    if-eqz v0, :cond_5

    .line 831
    iget-object v0, p0, Lcom/peel/content/a/bs;->a:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_3
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 832
    if-eqz p2, :cond_4

    invoke-interface {p2, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 833
    :cond_4
    invoke-static {}, Lcom/peel/data/m;->a()Lcom/peel/data/m;

    move-result-object v3

    iget-object v1, p0, Lcom/peel/content/a/bs;->a:Ljava/util/HashMap;

    .line 834
    invoke-virtual {v1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iget-object v4, p0, Lcom/peel/content/a/bs;->b:Lcom/peel/content/a/br;

    iget-object v4, v4, Lcom/peel/content/a/br;->a:Lcom/peel/content/a/bq;

    iget v4, v4, Lcom/peel/content/a/bq;->c:I

    .line 833
    invoke-virtual {v3, v0, v1, v4}, Lcom/peel/data/m;->a(Ljava/lang/String;Ljava/lang/String;I)V

    goto :goto_1

    .line 839
    :cond_5
    return-void
.end method
