.class Lcom/peel/content/a/aq;
.super Lcom/peel/util/t;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/peel/util/t",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/peel/content/a/ap;


# direct methods
.method constructor <init>(Lcom/peel/content/a/ap;)V
    .locals 0

    .prologue
    .line 1527
    iput-object p1, p0, Lcom/peel/content/a/aq;->a:Lcom/peel/content/a/ap;

    invoke-direct {p0}, Lcom/peel/util/t;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 9

    .prologue
    const/4 v4, 0x0

    const/4 v8, 0x0

    .line 1530
    iget-boolean v0, p0, Lcom/peel/content/a/aq;->i:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/peel/content/a/aq;->j:Ljava/lang/Object;

    if-nez v0, :cond_1

    .line 1532
    :cond_0
    iget-object v0, p0, Lcom/peel/content/a/aq;->a:Lcom/peel/content/a/ap;

    iget-object v0, v0, Lcom/peel/content/a/ap;->b:Lcom/peel/util/t;

    invoke-virtual {v0, v4, v8, v8}, Lcom/peel/util/t;->a(ZLjava/lang/Object;Ljava/lang/String;)V

    .line 1578
    :goto_0
    return-void

    .line 1537
    :cond_1
    :try_start_0
    sget-object v1, Lcom/peel/content/a/j;->a:Lorg/codehaus/jackson/map/ObjectMapper;

    iget-object v0, p0, Lcom/peel/content/a/aq;->j:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    const-class v2, Ljava/util/ArrayList;

    invoke-virtual {v1, v0, v2}, Lorg/codehaus/jackson/map/ObjectMapper;->readValue(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 1538
    if-eqz v0, :cond_2

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-nez v1, :cond_3

    .line 1540
    :cond_2
    iget-object v0, p0, Lcom/peel/content/a/aq;->a:Lcom/peel/content/a/ap;

    iget-object v0, v0, Lcom/peel/content/a/ap;->b:Lcom/peel/util/t;

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lcom/peel/util/t;->a(ZLjava/lang/Object;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1572
    :catch_0
    move-exception v0

    .line 1573
    invoke-static {}, Lcom/peel/content/a/j;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {}, Lcom/peel/content/a/j;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1576
    iget-object v0, p0, Lcom/peel/content/a/aq;->a:Lcom/peel/content/a/ap;

    iget-object v0, v0, Lcom/peel/content/a/ap;->b:Lcom/peel/util/t;

    invoke-virtual {v0, v4, v8, v8}, Lcom/peel/util/t;->a(ZLjava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 1544
    :cond_3
    :try_start_1
    new-instance v5, Ljava/util/ArrayList;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    invoke-direct {v5, v1}, Ljava/util/ArrayList;-><init>(I)V

    move v3, v4

    .line 1545
    :goto_1
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-ge v3, v1, :cond_5

    .line 1546
    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map;

    .line 1547
    new-instance v6, Landroid/os/Bundle;

    invoke-direct {v6}, Landroid/os/Bundle;-><init>()V

    .line 1551
    const-string/jumbo v7, "name"

    const-string/jumbo v2, "countryName"

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v6, v7, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1552
    const-string/jumbo v7, "endpoint"

    const-string/jumbo v2, "endpoint"

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v6, v7, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1553
    const-string/jumbo v7, "iso"

    const-string/jumbo v2, "countryCode"

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v6, v7, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1554
    const-string/jumbo v7, "type"

    const-string/jumbo v2, "providers_support_type"

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v6, v7, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1556
    const-string/jumbo v2, "countryCode"

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    const-string/jumbo v2, "CN"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 1557
    const-string/jumbo v1, "urloverride"

    const-string/jumbo v2, "peelchina.com"

    invoke-virtual {v6, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1560
    :cond_4
    invoke-interface {v5, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1545
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_1

    .line 1564
    :cond_5
    new-instance v0, Lcom/peel/content/a/ar;

    invoke-direct {v0, p0}, Lcom/peel/content/a/ar;-><init>(Lcom/peel/content/a/aq;)V

    invoke-static {v5, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 1571
    iget-object v0, p0, Lcom/peel/content/a/aq;->a:Lcom/peel/content/a/ap;

    iget-object v0, v0, Lcom/peel/content/a/ap;->b:Lcom/peel/util/t;

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v5, v2}, Lcom/peel/util/t;->a(ZLjava/lang/Object;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0
.end method
