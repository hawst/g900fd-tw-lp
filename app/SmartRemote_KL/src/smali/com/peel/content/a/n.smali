.class Lcom/peel/content/a/n;
.super Lcom/peel/util/t;


# instance fields
.field final synthetic a:Lcom/peel/content/a/m;


# direct methods
.method constructor <init>(Lcom/peel/content/a/m;I)V
    .locals 0

    .prologue
    .line 868
    iput-object p1, p0, Lcom/peel/content/a/n;->a:Lcom/peel/content/a/m;

    invoke-direct {p0, p2}, Lcom/peel/util/t;-><init>(I)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 871
    iget-boolean v0, p0, Lcom/peel/content/a/n;->i:Z

    if-nez v0, :cond_0

    .line 872
    invoke-static {}, Lcom/peel/content/a/j;->a()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Error in getFavoriteChannels: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/peel/content/a/n;->k:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 873
    iget-object v0, p0, Lcom/peel/content/a/n;->a:Lcom/peel/content/a/m;

    iget-object v0, v0, Lcom/peel/content/a/m;->b:Lcom/peel/util/t;

    iget-object v1, p0, Lcom/peel/content/a/n;->k:Ljava/lang/String;

    invoke-virtual {v0, v4, v3, v1}, Lcom/peel/util/t;->a(ZLjava/lang/Object;Ljava/lang/String;)V

    .line 882
    :goto_0
    return-void

    .line 877
    :cond_0
    iget-object v0, p0, Lcom/peel/content/a/n;->j:Ljava/lang/Object;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/peel/content/a/n;->j:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    const-string/jumbo v1, "null"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 878
    iget-object v0, p0, Lcom/peel/content/a/n;->a:Lcom/peel/content/a/m;

    iget-object v0, v0, Lcom/peel/content/a/m;->b:Lcom/peel/util/t;

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/peel/content/a/n;->j:Ljava/lang/Object;

    invoke-virtual {v0, v1, v2, v3}, Lcom/peel/util/t;->a(ZLjava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 880
    :cond_1
    iget-object v0, p0, Lcom/peel/content/a/n;->a:Lcom/peel/content/a/m;

    iget-object v0, v0, Lcom/peel/content/a/m;->b:Lcom/peel/util/t;

    const-string/jumbo v1, "unable to parse getFavoriteChannels json"

    invoke-virtual {v0, v4, v3, v1}, Lcom/peel/util/t;->a(ZLjava/lang/Object;Ljava/lang/String;)V

    goto :goto_0
.end method
