.class Lcom/peel/content/a/af;
.super Lcom/peel/util/t;


# instance fields
.field final synthetic a:Lcom/peel/content/a/ae;


# direct methods
.method constructor <init>(Lcom/peel/content/a/ae;)V
    .locals 0

    .prologue
    .line 1285
    iput-object p1, p0, Lcom/peel/content/a/af;->a:Lcom/peel/content/a/ae;

    invoke-direct {p0}, Lcom/peel/util/t;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v1, 0x0

    .line 1289
    iget-boolean v0, p0, Lcom/peel/content/a/af;->i:Z

    if-nez v0, :cond_0

    .line 1290
    iget-object v0, p0, Lcom/peel/content/a/af;->a:Lcom/peel/content/a/ae;

    iget-object v0, v0, Lcom/peel/content/a/ae;->b:Lcom/peel/util/t;

    iget-object v2, p0, Lcom/peel/content/a/af;->k:Ljava/lang/String;

    invoke-virtual {v0, v1, v7, v2}, Lcom/peel/util/t;->a(ZLjava/lang/Object;Ljava/lang/String;)V

    .line 1311
    :goto_0
    return-void

    .line 1294
    :cond_0
    iget-object v0, p0, Lcom/peel/content/a/af;->j:Ljava/lang/Object;

    if-eqz v0, :cond_1

    const-string/jumbo v0, "null"

    iget-object v2, p0, Lcom/peel/content/a/af;->j:Ljava/lang/Object;

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1295
    :cond_1
    iget-object v0, p0, Lcom/peel/content/a/af;->a:Lcom/peel/content/a/ae;

    iget-object v0, v0, Lcom/peel/content/a/ae;->b:Lcom/peel/util/t;

    const-string/jumbo v2, "null from cloud"

    invoke-virtual {v0, v1, v7, v2}, Lcom/peel/util/t;->a(ZLjava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 1300
    :cond_2
    :try_start_0
    new-instance v2, Lorg/json/JSONObject;

    iget-object v0, p0, Lcom/peel/content/a/af;->j:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    invoke-direct {v2, v0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 1301
    const-string/jumbo v0, "regions"

    invoke-virtual {v2, v0}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v2

    .line 1302
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    move v0, v1

    .line 1303
    :goto_1
    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result v4

    if-ge v0, v4, :cond_3

    .line 1304
    invoke-virtual {v2, v0}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v4

    const-string/jumbo v5, "code"

    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v0}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v5

    const-string/jumbo v6, "region"

    invoke-virtual {v5, v6}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1303
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1306
    :cond_3
    iget-object v0, p0, Lcom/peel/content/a/af;->a:Lcom/peel/content/a/ae;

    iget-object v0, v0, Lcom/peel/content/a/ae;->b:Lcom/peel/util/t;

    const/4 v2, 0x1

    const/4 v4, 0x0

    invoke-virtual {v0, v2, v3, v4}, Lcom/peel/util/t;->a(ZLjava/lang/Object;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1307
    :catch_0
    move-exception v0

    .line 1308
    invoke-static {}, Lcom/peel/content/a/j;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {}, Lcom/peel/content/a/j;->a()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1309
    iget-object v2, p0, Lcom/peel/content/a/af;->a:Lcom/peel/content/a/ae;

    iget-object v2, v2, Lcom/peel/content/a/ae;->b:Lcom/peel/util/t;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v1, v7, v0}, Lcom/peel/util/t;->a(ZLjava/lang/Object;Ljava/lang/String;)V

    goto :goto_0
.end method
