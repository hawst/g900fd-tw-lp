.class Lcom/peel/content/a/bp;
.super Lcom/peel/util/t;


# instance fields
.field final synthetic a:Lcom/peel/content/a/bo;


# direct methods
.method constructor <init>(Lcom/peel/content/a/bo;I)V
    .locals 0

    .prologue
    .line 749
    iput-object p1, p0, Lcom/peel/content/a/bp;->a:Lcom/peel/content/a/bo;

    invoke-direct {p0, p2}, Lcom/peel/util/t;-><init>(I)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 752
    iget-boolean v0, p0, Lcom/peel/content/a/bp;->i:Z

    if-nez v0, :cond_0

    .line 753
    iget-object v0, p0, Lcom/peel/content/a/bp;->a:Lcom/peel/content/a/bo;

    iget-object v0, v0, Lcom/peel/content/a/bo;->c:Lcom/peel/util/t;

    iget-boolean v1, p0, Lcom/peel/content/a/bp;->i:Z

    iget-object v2, p0, Lcom/peel/content/a/bp;->j:Ljava/lang/Object;

    iget-object v3, p0, Lcom/peel/content/a/bp;->k:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3}, Lcom/peel/util/t;->a(ZLjava/lang/Object;Ljava/lang/String;)V

    .line 754
    invoke-static {}, Lcom/peel/content/a/j;->a()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "getExcludedLineup error: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/peel/content/a/bp;->k:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 767
    :goto_0
    return-void

    .line 758
    :cond_0
    iget-object v0, p0, Lcom/peel/content/a/bp;->j:Ljava/lang/Object;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/peel/content/a/bp;->j:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    const-string/jumbo v3, "null"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/peel/content/a/bp;->j:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    const-string/jumbo v3, ""

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 760
    invoke-static {}, Lcom/peel/content/a/j;->a()Ljava/lang/String;

    move-result-object v0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "getExcludedLineup response: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/peel/content/a/bp;->j:Ljava/lang/Object;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 761
    iget-object v0, p0, Lcom/peel/content/a/bp;->j:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/peel/content/a/j;->d(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 762
    iget-object v0, p0, Lcom/peel/content/a/bp;->a:Lcom/peel/content/a/bo;

    iget-object v4, v0, Lcom/peel/content/a/bo;->c:Lcom/peel/util/t;

    if-eqz v3, :cond_1

    :goto_1
    if-eqz v3, :cond_2

    const/4 v0, 0x0

    :goto_2
    invoke-virtual {v4, v1, v3, v0}, Lcom/peel/util/t;->a(ZLjava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    move v1, v2

    goto :goto_1

    :cond_2
    const-string/jumbo v0, "unable to parse exclusion list json"

    goto :goto_2

    .line 764
    :cond_3
    invoke-static {}, Lcom/peel/content/a/j;->a()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v3, "no exclusion list, return empty []"

    invoke-static {v0, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 765
    iget-object v0, p0, Lcom/peel/content/a/bp;->a:Lcom/peel/content/a/bo;

    iget-object v0, v0, Lcom/peel/content/a/bo;->c:Lcom/peel/util/t;

    new-array v2, v2, [Ljava/lang/String;

    const-string/jumbo v3, "no exclusion list, return empty []"

    invoke-virtual {v0, v1, v2, v3}, Lcom/peel/util/t;->a(ZLjava/lang/Object;Ljava/lang/String;)V

    goto :goto_0
.end method
