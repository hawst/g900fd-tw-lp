.class Lcom/peel/content/a/q;
.super Lcom/peel/util/t;


# instance fields
.field final synthetic a:Lcom/peel/content/a/p;


# direct methods
.method constructor <init>(Lcom/peel/content/a/p;I)V
    .locals 0

    .prologue
    .line 915
    iput-object p1, p0, Lcom/peel/content/a/q;->a:Lcom/peel/content/a/p;

    invoke-direct {p0, p2}, Lcom/peel/util/t;-><init>(I)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 15

    .prologue
    const/4 v14, 0x0

    const/4 v7, 0x0

    .line 918
    iget-boolean v0, p0, Lcom/peel/content/a/q;->i:Z

    if-nez v0, :cond_0

    .line 919
    iget-object v0, p0, Lcom/peel/content/a/q;->a:Lcom/peel/content/a/p;

    iget-object v0, v0, Lcom/peel/content/a/p;->b:Lcom/peel/content/a/o;

    iget-object v0, v0, Lcom/peel/content/a/o;->e:Lcom/peel/util/t;

    iget-boolean v1, p0, Lcom/peel/content/a/q;->i:Z

    iget-object v2, p0, Lcom/peel/content/a/q;->j:Ljava/lang/Object;

    iget-object v3, p0, Lcom/peel/content/a/q;->k:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3}, Lcom/peel/util/t;->a(ZLjava/lang/Object;Ljava/lang/String;)V

    .line 976
    :goto_0
    return-void

    .line 925
    :cond_0
    :try_start_0
    invoke-static {}, Lcom/peel/content/a/j;->a()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "\n\n##### GOT NEW USER ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/peel/content/a/q;->j:Ljava/lang/Object;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "\n\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 926
    iget-object v0, p0, Lcom/peel/content/a/q;->j:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v8

    .line 929
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 930
    const-string/jumbo v1, "udid"

    iget-object v2, p0, Lcom/peel/content/a/q;->a:Lcom/peel/content/a/p;

    iget-object v2, v2, Lcom/peel/content/a/p;->b:Lcom/peel/content/a/o;

    iget-object v2, v2, Lcom/peel/content/a/o;->a:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 931
    const-string/jumbo v1, "hubid"

    iget-object v2, p0, Lcom/peel/content/a/q;->a:Lcom/peel/content/a/p;

    iget-object v2, v2, Lcom/peel/content/a/p;->b:Lcom/peel/content/a/o;

    iget-object v2, v2, Lcom/peel/content/a/o;->a:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 932
    const/4 v1, 0x0

    invoke-static {v8, v0, v1}, Lcom/peel/content/a/j;->a(Ljava/lang/String;Ljava/util/Map;Lcom/peel/util/t;)V

    .line 935
    new-instance v9, Landroid/os/Bundle;

    invoke-direct {v9}, Landroid/os/Bundle;-><init>()V

    .line 936
    const-string/jumbo v0, "program"

    invoke-static {v0}, Lcom/peel/content/a;->d(Ljava/lang/String;)[[Ljava/lang/String;

    move-result-object v10

    .line 937
    array-length v0, v10

    new-array v11, v0, [Lcom/peel/data/Genre;

    move v6, v7

    .line 938
    :goto_1
    array-length v0, v10

    if-ge v6, v0, :cond_2

    .line 940
    iget-object v0, p0, Lcom/peel/content/a/q;->a:Lcom/peel/content/a/p;

    iget-object v0, v0, Lcom/peel/content/a/p;->b:Lcom/peel/content/a/o;

    iget-object v0, v0, Lcom/peel/content/a/o;->b:Ljava/lang/String;

    const-string/jumbo v1, "KR"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 941
    aget-object v0, v10, v6

    const/4 v1, 0x0

    aget-object v0, v0, v1

    const-string/jumbo v1, "Broadcast Channels"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 938
    :goto_2
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto :goto_1

    .line 946
    :cond_1
    new-instance v0, Lcom/peel/data/Genre;

    aget-object v1, v10, v6

    const/4 v2, 0x1

    aget-object v1, v1, v2

    aget-object v2, v10, v6

    const/4 v3, 0x0

    aget-object v2, v2, v3

    const/4 v3, 0x0

    aget-object v4, v10, v6

    const/4 v5, 0x2

    aget-object v4, v4, v5

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, Lcom/peel/data/Genre;-><init>(Ljava/lang/String;Ljava/lang/String;IIZ)V

    aput-object v0, v11, v6
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    .line 972
    :catch_0
    move-exception v0

    .line 973
    invoke-static {}, Lcom/peel/content/a/j;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {}, Lcom/peel/content/a/j;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 974
    iget-object v1, p0, Lcom/peel/content/a/q;->a:Lcom/peel/content/a/p;

    iget-object v1, v1, Lcom/peel/content/a/p;->b:Lcom/peel/content/a/o;

    iget-object v1, v1, Lcom/peel/content/a/o;->e:Lcom/peel/util/t;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v7, v14, v0}, Lcom/peel/util/t;->a(ZLjava/lang/Object;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 948
    :cond_2
    :try_start_1
    const-string/jumbo v0, "movie"

    invoke-static {v0}, Lcom/peel/content/a;->d(Ljava/lang/String;)[[Ljava/lang/String;

    move-result-object v10

    .line 949
    array-length v0, v10

    new-array v12, v0, [Lcom/peel/data/Genre;

    move v6, v7

    .line 950
    :goto_3
    array-length v0, v10

    if-ge v6, v0, :cond_4

    .line 952
    iget-object v0, p0, Lcom/peel/content/a/q;->a:Lcom/peel/content/a/p;

    iget-object v0, v0, Lcom/peel/content/a/p;->b:Lcom/peel/content/a/o;

    iget-object v0, v0, Lcom/peel/content/a/o;->b:Ljava/lang/String;

    const-string/jumbo v1, "KR"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 953
    aget-object v0, v10, v6

    const/4 v1, 0x0

    aget-object v0, v0, v1

    const-string/jumbo v1, "Broadcast Channels"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 950
    :goto_4
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto :goto_3

    .line 958
    :cond_3
    new-instance v0, Lcom/peel/data/Genre;

    aget-object v1, v10, v6

    const/4 v2, 0x1

    aget-object v1, v1, v2

    aget-object v2, v10, v6

    const/4 v3, 0x0

    aget-object v2, v2, v3

    const/4 v3, 0x0

    aget-object v4, v10, v6

    const/4 v5, 0x2

    aget-object v4, v4, v5

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, Lcom/peel/data/Genre;-><init>(Ljava/lang/String;Ljava/lang/String;IIZ)V

    aput-object v0, v12, v6

    goto :goto_4

    .line 960
    :cond_4
    const-string/jumbo v0, "sports"

    invoke-static {v0}, Lcom/peel/content/a;->d(Ljava/lang/String;)[[Ljava/lang/String;

    move-result-object v10

    .line 961
    array-length v0, v10

    new-array v13, v0, [Lcom/peel/data/Genre;

    move v6, v7

    .line 962
    :goto_5
    array-length v0, v10

    if-ge v6, v0, :cond_5

    .line 964
    new-instance v0, Lcom/peel/data/Genre;

    aget-object v1, v10, v6

    const/4 v2, 0x1

    aget-object v1, v1, v2

    aget-object v2, v10, v6

    const/4 v3, 0x0

    aget-object v2, v2, v3

    const/4 v3, 0x0

    aget-object v4, v10, v6

    const/4 v5, 0x2

    aget-object v4, v4, v5

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, Lcom/peel/data/Genre;-><init>(Ljava/lang/String;Ljava/lang/String;IIZ)V

    aput-object v0, v13, v6

    .line 962
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto :goto_5

    .line 966
    :cond_5
    const-string/jumbo v0, "programGenres"

    invoke-virtual {v9, v0, v11}, Landroid/os/Bundle;->putParcelableArray(Ljava/lang/String;[Landroid/os/Parcelable;)V

    .line 967
    const-string/jumbo v0, "movieGenres"

    invoke-virtual {v9, v0, v12}, Landroid/os/Bundle;->putParcelableArray(Ljava/lang/String;[Landroid/os/Parcelable;)V

    .line 968
    const-string/jumbo v0, "sportsGenres"

    invoke-virtual {v9, v0, v13}, Landroid/os/Bundle;->putParcelableArray(Ljava/lang/String;[Landroid/os/Parcelable;)V

    .line 969
    iget-object v0, p0, Lcom/peel/content/a/q;->a:Lcom/peel/content/a/p;

    iget-object v0, v0, Lcom/peel/content/a/p;->b:Lcom/peel/content/a/o;

    iget-object v0, v0, Lcom/peel/content/a/o;->e:Lcom/peel/util/t;

    const/4 v1, 0x1

    new-instance v2, Lcom/peel/content/user/User;

    invoke-direct {v2, v8, v9}, Lcom/peel/content/user/User;-><init>(Ljava/lang/String;Landroid/os/Bundle;)V

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lcom/peel/util/t;->a(ZLjava/lang/Object;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0
.end method
