.class Lcom/peel/content/a/av;
.super Lcom/peel/util/t;


# instance fields
.field final synthetic a:Ljava/util/concurrent/atomic/AtomicInteger;

.field final synthetic b:Ljava/util/concurrent/atomic/AtomicInteger;

.field final synthetic c:Ljava/lang/String;

.field final synthetic d:Ljava/lang/String;

.field final synthetic e:Ljava/lang/String;

.field final synthetic f:Lcom/peel/content/a/au;


# direct methods
.method constructor <init>(Lcom/peel/content/a/au;ILjava/util/concurrent/atomic/AtomicInteger;Ljava/util/concurrent/atomic/AtomicInteger;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 435
    iput-object p1, p0, Lcom/peel/content/a/av;->f:Lcom/peel/content/a/au;

    iput-object p3, p0, Lcom/peel/content/a/av;->a:Ljava/util/concurrent/atomic/AtomicInteger;

    iput-object p4, p0, Lcom/peel/content/a/av;->b:Ljava/util/concurrent/atomic/AtomicInteger;

    iput-object p5, p0, Lcom/peel/content/a/av;->c:Ljava/lang/String;

    iput-object p6, p0, Lcom/peel/content/a/av;->d:Ljava/lang/String;

    iput-object p7, p0, Lcom/peel/content/a/av;->e:Ljava/lang/String;

    invoke-direct {p0, p2}, Lcom/peel/util/t;-><init>(I)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 12

    .prologue
    const/4 v5, 0x0

    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 438
    iget-boolean v1, p0, Lcom/peel/content/a/av;->i:Z

    if-eqz v1, :cond_0

    const-string/jumbo v1, "null"

    iget-object v2, p0, Lcom/peel/content/a/av;->j:Ljava/lang/Object;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 439
    :cond_0
    iget-object v1, p0, Lcom/peel/content/a/av;->a:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v1

    iget-object v2, p0, Lcom/peel/content/a/av;->b:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v2

    if-ge v1, v2, :cond_1

    .line 440
    iget-object v1, p0, Lcom/peel/content/a/av;->a:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v1, v6}, Ljava/util/concurrent/atomic/AtomicInteger;->addAndGet(I)I

    .line 441
    iget-object v1, p0, Lcom/peel/content/a/av;->c:Ljava/lang/String;

    invoke-static {v1, p0}, Lcom/peel/util/b/a;->b(Ljava/lang/String;Lcom/peel/util/t;)V

    .line 487
    :goto_0
    return-void

    .line 444
    :cond_1
    iget-object v1, p0, Lcom/peel/content/a/av;->f:Lcom/peel/content/a/au;

    iget-object v1, v1, Lcom/peel/content/a/au;->b:Lcom/peel/util/t;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "URL: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/peel/content/a/av;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " -- msg: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/peel/content/a/av;->k:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v7, v5, v2}, Lcom/peel/util/t;->a(ZLjava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 449
    :cond_2
    iget-object v1, p0, Lcom/peel/content/a/av;->d:Ljava/lang/String;

    const-string/jumbo v2, "top"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 450
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 452
    :try_start_0
    sget-object v2, Lcom/peel/content/a/j;->a:Lorg/codehaus/jackson/map/ObjectMapper;

    iget-object v1, p0, Lcom/peel/content/a/av;->j:Ljava/lang/Object;

    check-cast v1, Ljava/lang/String;

    const-class v4, Ljava/util/LinkedHashMap;

    invoke-virtual {v2, v1, v4}, Lorg/codehaus/jackson/map/ObjectMapper;->readValue(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/LinkedHashMap;

    const-string/jumbo v2, "curatedshows"

    invoke-virtual {v1, v2}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 453
    if-eqz v1, :cond_6

    .line 454
    instance-of v2, v1, Ljava/util/ArrayList;

    if-eqz v2, :cond_5

    .line 455
    check-cast v1, Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_3
    :goto_1
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/LinkedHashMap;

    .line 456
    iget-object v2, p0, Lcom/peel/content/a/av;->e:Ljava/lang/String;

    const/4 v4, 0x1

    sget-object v9, Lcom/peel/content/a/j;->a:Lorg/codehaus/jackson/map/ObjectMapper;

    .line 457
    invoke-virtual {v9, v1}, Lorg/codehaus/jackson/map/ObjectMapper;->writeValueAsString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    .line 456
    invoke-static {v2, v4, v9}, Lcom/peel/content/a/j;->a(Ljava/lang/String;ZLjava/lang/String;)Ljava/util/List;

    move-result-object v9

    .line 458
    if-eqz v9, :cond_3

    .line 459
    move-object v0, v3

    check-cast v0, Ljava/util/ArrayList;

    move-object v2, v0

    new-instance v10, Lcom/peel/content/library/RankCategory;

    const-string/jumbo v4, "rank"

    invoke-virtual {v1, v4}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    const-string/jumbo v11, "category"

    invoke-virtual {v1, v11}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-direct {v10, v4, v1, v9}, Lcom/peel/content/library/RankCategory;-><init>(ILjava/lang/String;Ljava/util/List;)V

    invoke-virtual {v2, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 475
    :catch_0
    move-exception v1

    .line 476
    invoke-static {}, Lcom/peel/content/a/j;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {}, Lcom/peel/content/a/j;->a()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move-object v3, v5

    .line 485
    :cond_4
    :goto_2
    iget-object v1, p0, Lcom/peel/content/a/av;->f:Lcom/peel/content/a/au;

    iget-object v1, v1, Lcom/peel/content/a/au;->b:Lcom/peel/util/t;

    if-eqz v3, :cond_9

    :goto_3
    if-eqz v3, :cond_a

    :goto_4
    invoke-virtual {v1, v6, v3, v5}, Lcom/peel/util/t;->a(ZLjava/lang/Object;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 462
    :cond_5
    :try_start_1
    instance-of v2, v1, Ljava/util/LinkedHashMap;

    if-eqz v2, :cond_6

    .line 463
    iget-object v2, p0, Lcom/peel/content/a/av;->e:Ljava/lang/String;

    const/4 v4, 0x1

    sget-object v8, Lcom/peel/content/a/j;->a:Lorg/codehaus/jackson/map/ObjectMapper;

    .line 465
    invoke-virtual {v8, v1}, Lorg/codehaus/jackson/map/ObjectMapper;->writeValueAsString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    .line 463
    invoke-static {v2, v4, v8}, Lcom/peel/content/a/j;->a(Ljava/lang/String;ZLjava/lang/String;)Ljava/util/List;

    move-result-object v8

    .line 466
    check-cast v1, Ljava/util/LinkedHashMap;

    .line 467
    if-eqz v8, :cond_6

    .line 468
    move-object v0, v3

    check-cast v0, Ljava/util/ArrayList;

    move-object v2, v0

    new-instance v9, Lcom/peel/content/library/RankCategory;

    const-string/jumbo v4, "rank"

    invoke-virtual {v1, v4}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    const-string/jumbo v10, "category"

    invoke-virtual {v1, v10}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-direct {v9, v4, v1, v8}, Lcom/peel/content/library/RankCategory;-><init>(ILjava/lang/String;Ljava/util/List;)V

    invoke-virtual {v2, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 472
    :cond_6
    move-object v0, v3

    check-cast v0, Ljava/util/ArrayList;

    move-object v1, v0

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    move-result v1

    if-nez v1, :cond_4

    move-object v3, v5

    .line 473
    goto :goto_2

    .line 480
    :cond_7
    iget-object v3, p0, Lcom/peel/content/a/av;->e:Ljava/lang/String;

    const/4 v1, -0x1

    iget-object v2, p0, Lcom/peel/content/a/av;->f:Lcom/peel/content/a/au;

    iget-object v2, v2, Lcom/peel/content/a/au;->a:Landroid/os/Bundle;

    const-string/jumbo v4, "cacheWindow"

    invoke-virtual {v2, v4, v7}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v2

    if-ne v1, v2, :cond_8

    move v2, v6

    :goto_5
    iget-object v1, p0, Lcom/peel/content/a/av;->j:Ljava/lang/Object;

    check-cast v1, Ljava/lang/String;

    invoke-static {v3, v2, v1}, Lcom/peel/content/a/j;->a(Ljava/lang/String;ZLjava/lang/String;)Ljava/util/List;

    move-result-object v3

    .line 481
    if-nez v3, :cond_4

    .line 482
    invoke-static {}, Lcom/peel/content/a/j;->a()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "URL: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v4, p0, Lcom/peel/content/a/av;->c:Ljava/lang/String;

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v4, "\nno listings returned from legacy after parseListings()"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2

    :cond_8
    move v2, v7

    .line 480
    goto :goto_5

    :cond_9
    move v6, v7

    .line 485
    goto/16 :goto_3

    :cond_a
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "URL: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v4, p0, Lcom/peel/content/a/av;->c:Ljava/lang/String;

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v4, "\nno listings returned from legacy after parseListings()"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    goto/16 :goto_4
.end method
