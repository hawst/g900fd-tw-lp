.class public Lcom/peel/content/a/j;
.super Ljava/lang/Object;


# static fields
.field public static final a:Lorg/codehaus/jackson/map/ObjectMapper;

.field public static b:Ljava/lang/String;

.field private static final c:Ljava/lang/String;

.field private static final d:Ljava/lang/StringBuilder;

.field private static final e:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 106
    new-instance v0, Lorg/codehaus/jackson/map/ObjectMapper;

    invoke-direct {v0}, Lorg/codehaus/jackson/map/ObjectMapper;-><init>()V

    sput-object v0, Lcom/peel/content/a/j;->a:Lorg/codehaus/jackson/map/ObjectMapper;

    .line 107
    const-class v0, Lcom/peel/content/a/j;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/peel/content/a/j;->c:Ljava/lang/String;

    .line 178
    new-instance v0, Ljava/lang/StringBuilder;

    const-string/jumbo v1, "personalization/moreLikeThis?showID=%s&country=%s&limit=%s"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/peel/content/a/j;->d:Ljava/lang/StringBuilder;

    .line 182
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    sput-object v0, Lcom/peel/content/a/j;->e:Ljava/util/Set;

    .line 183
    const-string/jumbo v0, "http://peelapp.zelfy.com/"

    sput-object v0, Lcom/peel/content/a/j;->b:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 50
    sget-object v0, Lcom/peel/content/a/j;->c:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic a(Ljava/lang/String;ZLjava/lang/String;)Ljava/util/List;
    .locals 1

    .prologue
    .line 50
    invoke-static {p0, p1, p2}, Lcom/peel/content/a/j;->b(Ljava/lang/String;ZLjava/lang/String;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/Context;ILcom/peel/util/t;)V
    .locals 3

    .prologue
    .line 1473
    if-gtz p1, :cond_0

    .line 1475
    sget-object v0, Lcom/peel/content/a/j;->c:Ljava/lang/String;

    const-string/jumbo v1, "\n\n#####bad productId... Not checking product version"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1516
    :goto_0
    return-void

    .line 1478
    :cond_0
    sget-object v0, Lcom/peel/content/a/j;->c:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "getProductUpdateFlag product id: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/peel/content/a/an;

    invoke-direct {v2, p0, p1, p2}, Lcom/peel/content/a/an;-><init>(Landroid/content/Context;ILcom/peel/util/t;)V

    invoke-static {v0, v1, v2}, Lcom/peel/util/i;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    goto :goto_0
.end method

.method static synthetic a(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 50
    invoke-static {p0}, Lcom/peel/content/a/j;->b(Landroid/os/Bundle;)V

    return-void
.end method

.method public static a(Landroid/os/Bundle;Lcom/peel/util/t;)V
    .locals 5

    .prologue
    .line 193
    const-string/jumbo v0, "path"

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 194
    sget-object v1, Lcom/peel/content/a/j;->c:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "get() path: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 196
    if-eqz v0, :cond_6

    .line 197
    const-string/jumbo v1, "showtimes"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 198
    invoke-static {p0, p1}, Lcom/peel/content/a/j;->j(Landroid/os/Bundle;Lcom/peel/util/t;)V

    .line 248
    :goto_0
    return-void

    .line 202
    :cond_0
    const-string/jumbo v1, "listing"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 203
    invoke-static {p0, p1}, Lcom/peel/content/a/j;->i(Landroid/os/Bundle;Lcom/peel/util/t;)V

    goto :goto_0

    .line 207
    :cond_1
    const-string/jumbo v1, "lineup"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 208
    invoke-static {p0, p1}, Lcom/peel/content/a/j;->k(Landroid/os/Bundle;Lcom/peel/util/t;)V

    goto :goto_0

    .line 212
    :cond_2
    const-string/jumbo v1, "channel"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 213
    const-string/jumbo v1, "favorites"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 214
    const-string/jumbo v0, "url"

    const-string/jumbo v1, "%sepg/schedules/getfavchannels/%s?userid=%s&roomid=%d"

    invoke-virtual {p0, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 215
    invoke-static {p0, p1}, Lcom/peel/content/a/j;->m(Landroid/os/Bundle;Lcom/peel/util/t;)V

    goto :goto_0

    .line 220
    :cond_3
    const-string/jumbo v1, "search"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 230
    const-string/jumbo v1, "fullsearch"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 232
    invoke-static {p0, p1}, Lcom/peel/content/a/j;->n(Landroid/os/Bundle;Lcom/peel/util/t;)V

    goto :goto_0

    .line 235
    :cond_4
    const-string/jumbo v1, "exactsearch"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 237
    invoke-static {p0, p1}, Lcom/peel/content/a/j;->o(Landroid/os/Bundle;Lcom/peel/util/t;)V

    goto :goto_0

    .line 240
    :cond_5
    const-string/jumbo v1, "blendedsearch"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 241
    invoke-static {p0, p1}, Lcom/peel/content/a/j;->c(Landroid/os/Bundle;Lcom/peel/util/t;)V

    goto :goto_0

    .line 247
    :cond_6
    const/4 v1, 0x0

    const/4 v2, 0x0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "unrecognized path: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v1, v2, v0}, Lcom/peel/util/t;->a(ZLjava/lang/Object;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method public static a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 186
    sput-object p0, Lcom/peel/content/a/j;->b:Ljava/lang/String;

    .line 187
    return-void
.end method

.method public static a(Ljava/lang/String;ILcom/peel/util/t;)V
    .locals 3

    .prologue
    .line 781
    sget-object v0, Lcom/peel/content/a/j;->c:Ljava/lang/String;

    const-string/jumbo v1, "download favorites list"

    new-instance v2, Lcom/peel/content/a/bq;

    invoke-direct {v2, p0, p2, p1}, Lcom/peel/content/a/bq;-><init>(Ljava/lang/String;Lcom/peel/util/t;I)V

    invoke-static {v0, v1, v2}, Lcom/peel/util/i;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    .line 850
    return-void
.end method

.method public static a(Ljava/lang/String;Lcom/peel/util/t;)V
    .locals 3

    .prologue
    .line 1279
    sget-object v0, Lcom/peel/content/a/j;->c:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "get regions by country: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/peel/content/a/ae;

    invoke-direct {v2, p0, p1}, Lcom/peel/content/a/ae;-><init>(Ljava/lang/String;Lcom/peel/util/t;)V

    invoke-static {v0, v1, v2}, Lcom/peel/util/i;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    .line 1315
    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;Lcom/peel/util/t;)V
    .locals 3

    .prologue
    .line 1231
    sget-object v0, Lcom/peel/content/a/j;->c:Ljava/lang/String;

    const-string/jumbo v1, "get lineup by zip"

    new-instance v2, Lcom/peel/content/a/ac;

    invoke-direct {v2, p0, p1, p2}, Lcom/peel/content/a/ac;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/peel/util/t;)V

    invoke-static {v0, v1, v2}, Lcom/peel/util/i;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    .line 1276
    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILcom/peel/util/t;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "I",
            "Lcom/peel/util/t",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/peel/content/listing/Listing;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 525
    sget-object v6, Lcom/peel/content/a/j;->c:Ljava/lang/String;

    const-string/jumbo v7, "getShowTimes"

    new-instance v0, Lcom/peel/content/a/bh;

    move-object v1, p0

    move-object v2, p2

    move-object v3, p1

    move v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/peel/content/a/bh;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILcom/peel/util/t;)V

    invoke-static {v6, v7, v0}, Lcom/peel/util/i;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    .line 548
    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/peel/util/t;)V
    .locals 3

    .prologue
    .line 1359
    sget-object v0, Lcom/peel/content/a/j;->c:Ljava/lang/String;

    const-string/jumbo v1, "get lineup by zip"

    new-instance v2, Lcom/peel/content/a/ai;

    invoke-direct {v2, p1, p2, p0, p3}, Lcom/peel/content/a/ai;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/peel/util/t;)V

    invoke-static {v0, v1, v2}, Lcom/peel/util/i;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    .line 1403
    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/peel/util/t;)V
    .locals 8

    .prologue
    .line 894
    sget-object v6, Lcom/peel/content/a/j;->c:Ljava/lang/String;

    const-string/jumbo v7, "grab user by udid"

    new-instance v0, Lcom/peel/content/a/o;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/peel/content/a/o;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/peel/util/t;)V

    invoke-static {v6, v7, v0}, Lcom/peel/util/i;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    .line 982
    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/util/Map;Lcom/peel/util/t;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Lcom/peel/util/t;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1406
    sget-object v0, Lcom/peel/content/a/j;->c:Ljava/lang/String;

    const-string/jumbo v1, "update user id"

    new-instance v2, Lcom/peel/content/a/ak;

    invoke-direct {v2, p0, p1, p2}, Lcom/peel/content/a/ak;-><init>(Ljava/lang/String;Ljava/util/Map;Lcom/peel/util/t;)V

    invoke-static {v0, v1, v2}, Lcom/peel/util/i;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    .line 1410
    return-void
.end method

.method static synthetic a(Ljava/lang/String;Ljava/lang/String;)[Lcom/peel/data/Channel;
    .locals 1

    .prologue
    .line 50
    invoke-static {p0, p1}, Lcom/peel/content/a/j;->c(Ljava/lang/String;Ljava/lang/String;)[Lcom/peel/data/Channel;

    move-result-object v0

    return-object v0
.end method

.method static synthetic b()Ljava/lang/StringBuilder;
    .locals 1

    .prologue
    .line 50
    sget-object v0, Lcom/peel/content/a/j;->d:Ljava/lang/StringBuilder;

    return-object v0
.end method

.method static synthetic b(Ljava/lang/String;Ljava/lang/String;)Ljava/util/LinkedHashMap;
    .locals 1

    .prologue
    .line 50
    invoke-static {p0, p1}, Lcom/peel/content/a/j;->d(Ljava/lang/String;Ljava/lang/String;)Ljava/util/LinkedHashMap;

    move-result-object v0

    return-object v0
.end method

.method private static b(Ljava/lang/String;ZLjava/lang/String;)Ljava/util/List;
    .locals 40
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Z",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/peel/content/listing/Listing;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1673
    sget-object v4, Lcom/peel/content/a/j;->e:Ljava/util/Set;

    invoke-interface {v4}, Ljava/util/Set;->size()I

    move-result v4

    if-nez v4, :cond_2

    .line 1674
    new-instance v5, Ljava/util/HashSet;

    invoke-direct {v5}, Ljava/util/HashSet;-><init>()V

    .line 1675
    const-string/jumbo v4, "SBS"

    invoke-interface {v5, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1676
    const-string/jumbo v4, "KBS2"

    invoke-interface {v5, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1677
    const-string/jumbo v4, "KBS1"

    invoke-interface {v5, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1678
    const-string/jumbo v4, "EBS"

    invoke-interface {v5, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1679
    const-string/jumbo v4, "MBC"

    invoke-interface {v5, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1681
    invoke-static/range {p0 .. p0}, Lcom/peel/content/a;->b(Ljava/lang/String;)Lcom/peel/content/library/Library;

    move-result-object v4

    check-cast v4, Lcom/peel/content/library/LiveLibrary;

    .line 1682
    if-eqz v4, :cond_2

    .line 1683
    invoke-virtual {v4}, Lcom/peel/content/library/LiveLibrary;->i()[Lcom/peel/data/Channel;

    move-result-object v6

    .line 1684
    if-eqz v6, :cond_2

    .line 1685
    array-length v7, v6

    const/4 v4, 0x0

    :goto_0
    if-ge v4, v7, :cond_1

    aget-object v8, v6, v4

    .line 1686
    invoke-virtual {v8}, Lcom/peel/data/Channel;->d()Ljava/lang/String;

    move-result-object v9

    invoke-interface {v5, v9}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 1687
    sget-object v9, Lcom/peel/content/a/j;->e:Ljava/util/Set;

    invoke-virtual {v8}, Lcom/peel/data/Channel;->b()Ljava/lang/String;

    move-result-object v8

    invoke-interface {v9, v8}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1685
    :cond_0
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 1691
    :cond_1
    sget-object v4, Lcom/peel/content/a/j;->e:Ljava/util/Set;

    invoke-interface {v4}, Ljava/util/Set;->size()I

    move-result v4

    if-nez v4, :cond_2

    .line 1692
    sget-object v4, Lcom/peel/content/a/j;->e:Ljava/util/Set;

    const-string/jumbo v5, "n/a"

    invoke-interface {v4, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1703
    :cond_2
    :try_start_0
    sget-object v4, Lcom/peel/content/a/j;->a:Lorg/codehaus/jackson/map/ObjectMapper;

    const-class v5, Ljava/util/HashMap;

    move-object/from16 v0, p2

    invoke-virtual {v4, v0, v5}, Lorg/codehaus/jackson/map/ObjectMapper;->readValue(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/HashMap;

    const-string/jumbo v5, "schedules"

    invoke-virtual {v4, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    .line 1704
    instance-of v5, v4, Ljava/util/List;

    if-eqz v5, :cond_4

    .line 1705
    check-cast v4, Ljava/util/List;

    move-object/from16 v36, v4

    .line 1713
    :goto_1
    if-eqz v36, :cond_3

    invoke-interface/range {v36 .. v36}, Ljava/util/List;->size()I

    move-result v4

    const/4 v5, 0x1

    if-ge v4, v5, :cond_6

    :cond_3
    const/4 v4, 0x0

    .line 1902
    :goto_2
    return-object v4

    .line 1706
    :cond_4
    if-eqz v4, :cond_5

    .line 1707
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 1708
    invoke-interface {v5, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-object/from16 v36, v5

    goto :goto_1

    .line 1710
    :cond_5
    const/4 v4, 0x0

    move-object/from16 v36, v4

    goto :goto_1

    .line 1714
    :cond_6
    new-instance v31, Ljava/util/ArrayList;

    invoke-direct/range {v31 .. v31}, Ljava/util/ArrayList;-><init>()V

    .line 1715
    new-instance v37, Ljava/util/ArrayList;

    invoke-direct/range {v37 .. v37}, Ljava/util/ArrayList;-><init>()V

    .line 1717
    const/4 v4, 0x0

    move/from16 v35, v4

    :goto_3
    invoke-interface/range {v36 .. v36}, Ljava/util/List;->size()I

    move-result v4

    move/from16 v0, v35

    if-ge v0, v4, :cond_1d

    .line 1718
    move-object/from16 v0, v36

    move/from16 v1, v35

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    move-object v0, v4

    check-cast v0, Ljava/util/Map;

    move-object v13, v0

    .line 1719
    const-string/jumbo v4, "program"

    invoke-interface {v13, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/Map;

    .line 1722
    if-nez v4, :cond_7

    .line 1724
    sget-object v4, Lcom/peel/content/a/j;->c:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "missing program data: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, "16"

    invoke-interface {v13, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1717
    :goto_4
    add-int/lit8 v4, v35, 0x1

    move/from16 v35, v4

    goto :goto_3

    .line 1728
    :cond_7
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "18"

    invoke-interface {v13, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const/16 v6, 0x20

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, "19"

    invoke-interface {v13, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v28

    .line 1729
    invoke-static/range {v28 .. v28}, Lcom/peel/util/x;->c(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v8

    .line 1730
    if-nez v8, :cond_8

    .line 1732
    sget-object v4, Lcom/peel/content/a/j;->c:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "failed to parse ["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, "18"

    invoke-interface {v13, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const/16 v6, 0x20

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, "19"

    invoke-interface {v13, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, "]"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_4

    .line 1898
    :catch_0
    move-exception v4

    .line 1899
    sget-object v5, Lcom/peel/content/a/j;->c:Ljava/lang/String;

    sget-object v6, Lcom/peel/content/a/j;->c:Ljava/lang/String;

    invoke-static {v5, v6, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1900
    const/4 v4, 0x0

    goto/16 :goto_2

    .line 1736
    :cond_8
    :try_start_1
    const-string/jumbo v5, "4"

    invoke-interface {v4, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/String;

    .line 1737
    const-string/jumbo v5, "6"

    invoke-interface {v4, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    .line 1738
    const-string/jumbo v5, "11"

    invoke-interface {v4, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/String;

    .line 1739
    const-string/jumbo v5, "12"

    invoke-interface {v4, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Ljava/lang/String;

    .line 1740
    const-string/jumbo v5, "1"

    invoke-interface {v4, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    .line 1741
    const-string/jumbo v5, "16"

    invoke-interface {v13, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    const-string/jumbo v9, "^[0]*"

    const-string/jumbo v12, ""

    invoke-virtual {v5, v9, v12}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v22

    .line 1742
    const-string/jumbo v5, "28"

    invoke-interface {v4, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v29

    check-cast v29, Ljava/lang/String;

    .line 1747
    invoke-virtual {v8}, Ljava/util/Date;->getTime()J

    move-result-wide v19

    .line 1748
    const-string/jumbo v5, "15"

    invoke-interface {v13, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v21

    check-cast v21, Ljava/lang/String;

    .line 1749
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v21

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v22

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    .line 1752
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "live://"

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v8, "/"

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-wide/from16 v0, v19

    invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v8, "/"

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v8, "/"

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v23

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 1753
    move-object/from16 v0, v37

    invoke-interface {v0, v5}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_9

    .line 1754
    sget-object v4, Lcom/peel/content/a/j;->c:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "listing already exists: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_4

    .line 1758
    :cond_9
    const/4 v9, 0x0

    .line 1759
    const-wide/16 v14, 0x0

    .line 1762
    :try_start_2
    const-string/jumbo v8, "20"

    invoke-interface {v13, v8}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_13

    .line 1763
    const-string/jumbo v8, "20"

    invoke-interface {v13, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    const-string/jumbo v12, ":"

    invoke-virtual {v8, v12}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    move-result-object v8

    .line 1771
    :goto_5
    if-eqz v8, :cond_a

    .line 1772
    const/4 v9, 0x0

    :try_start_3
    aget-object v9, v8, v9

    invoke-static {v9}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v9

    const v12, 0x36ee80

    mul-int/2addr v9, v12

    int-to-long v14, v9

    .line 1773
    const/4 v9, 0x1

    aget-object v9, v8, v9

    invoke-static {v9}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v9

    const v12, 0xea60

    mul-int/2addr v9, v12

    int-to-long v0, v9

    move-wide/from16 v24, v0

    add-long v14, v14, v24

    .line 1774
    const/4 v9, 0x2

    aget-object v8, v8, v9

    invoke-static {v8}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v8

    mul-int/lit16 v8, v8, 0x3e8

    int-to-long v8, v8

    add-long/2addr v14, v8

    .line 1777
    :cond_a
    const-wide/16 v26, 0x0

    .line 1779
    const-string/jumbo v8, "9"

    invoke-interface {v4, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    if-eqz v8, :cond_b

    const-string/jumbo v8, "9"

    invoke-interface {v4, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    const-string/jumbo v9, "0"

    invoke-virtual {v8, v9}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0

    move-result v8

    if-nez v8, :cond_b

    .line 1781
    :try_start_4
    new-instance v9, Ljava/text/SimpleDateFormat;

    const-string/jumbo v8, "yyyy-MM-dd"

    sget-object v12, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v9, v8, v12}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 1782
    const-string/jumbo v8, "GMT"

    invoke-static {v8}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v8

    invoke-virtual {v9, v8}, Ljava/text/SimpleDateFormat;->setTimeZone(Ljava/util/TimeZone;)V

    .line 1784
    const-string/jumbo v8, "9"

    invoke-interface {v4, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    invoke-virtual {v9, v8}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v8

    .line 1785
    invoke-virtual {v8}, Ljava/util/Date;->getTime()J
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_2

    move-result-wide v26

    .line 1791
    :cond_b
    :goto_6
    const/16 v24, 0x0

    .line 1792
    :try_start_5
    const-string/jumbo v8, "22"

    invoke-interface {v13, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    .line 1793
    if-eqz v8, :cond_f

    .line 1794
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    .line 1795
    const-string/jumbo v12, "Live"

    invoke-virtual {v8, v12}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v12

    if-eqz v12, :cond_c

    .line 1796
    const-string/jumbo v12, "live|"

    invoke-virtual {v9, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1797
    :cond_c
    const-string/jumbo v12, "Premiere"

    invoke-virtual {v8, v12}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v12

    if-eqz v12, :cond_d

    .line 1798
    const-string/jumbo v12, "premiere|"

    invoke-virtual {v9, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1799
    :cond_d
    const-string/jumbo v12, "New"

    invoke-virtual {v8, v12}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_e

    .line 1800
    const-string/jumbo v8, "new|"

    invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1801
    :cond_e
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->length()I

    move-result v8

    if-lez v8, :cond_f

    const/4 v8, 0x0

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->length()I

    move-result v12

    add-int/lit8 v12, v12, -0x1

    invoke-virtual {v9, v8, v12}, Ljava/lang/StringBuilder;->substring(II)Ljava/lang/String;

    move-result-object v24

    .line 1804
    :cond_f
    const/16 v18, 0x1

    .line 1805
    const/16 v30, 0x0

    .line 1806
    const-string/jumbo v8, "13"

    invoke-interface {v4, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    if-eqz v8, :cond_10

    .line 1807
    const-string/jumbo v8, "13"

    invoke-interface {v4, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    const-string/jumbo v9, "Scifi"

    const-string/jumbo v12, "Sci-Fi"

    invoke-virtual {v8, v9, v12}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v8

    .line 1808
    const-string/jumbo v9, ","

    invoke-virtual {v8, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v30

    .line 1809
    const-string/jumbo v8, "13"

    invoke-interface {v4, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    invoke-virtual {v8}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v8

    const-string/jumbo v9, "news"

    invoke-virtual {v8, v9}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_15

    const/4 v8, 0x1

    :goto_7
    move/from16 v18, v8

    .line 1811
    :cond_10
    const-string/jumbo v8, "3"

    invoke-interface {v4, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    .line 1813
    const-string/jumbo v8, ""

    .line 1815
    const/16 v17, 0x0

    .line 1816
    const-string/jumbo v8, "2"

    invoke-interface {v4, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    .line 1817
    const-string/jumbo v12, "program"

    .line 1818
    const-string/jumbo v25, "movies"

    move-object/from16 v0, v25

    invoke-virtual {v0, v8}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v25

    if-eqz v25, :cond_16

    .line 1819
    const-string/jumbo v8, "movie"

    move-object/from16 v25, v17

    move/from16 v12, v18

    .line 1841
    :goto_8
    if-eqz v12, :cond_1f

    if-eqz v24, :cond_1f

    const-string/jumbo v12, "new"

    move-object/from16 v0, v24

    invoke-virtual {v0, v12}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v12

    if-nez v12, :cond_11

    const-string/jumbo v12, "premiere"

    move-object/from16 v0, v24

    invoke-virtual {v0, v12}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v12

    if-eqz v12, :cond_1f

    .line 1842
    :cond_11
    if-nez v30, :cond_17

    .line 1843
    const/4 v12, 0x1

    new-array v0, v12, [Ljava/lang/String;

    move-object/from16 v17, v0

    const/4 v12, 0x0

    const-string/jumbo v18, "First Run"

    aput-object v18, v17, v12

    .line 1853
    :goto_9
    sget-object v12, Lcom/peel/content/a/j;->e:Ljava/util/Set;

    move-object/from16 v0, v21

    invoke-interface {v12, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_1e

    .line 1854
    if-nez v17, :cond_18

    .line 1856
    const/4 v12, 0x1

    new-array v12, v12, [Ljava/lang/String;

    const/16 v17, 0x0

    const-string/jumbo v18, "Broadcast Channels"

    aput-object v18, v12, v17

    .line 1865
    :goto_a
    const-string/jumbo v17, "image_3x4"

    move-object/from16 v0, v17

    invoke-interface {v4, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_19

    const-string/jumbo v17, "image_3x4"

    move-object/from16 v0, v17

    invoke-interface {v4, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Ljava/lang/String;

    .line 1871
    :goto_b
    if-nez v7, :cond_12

    .line 1872
    :cond_12
    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    .line 1874
    const-string/jumbo v30, "24"

    move-object/from16 v0, v30

    invoke-interface {v4, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v30

    if-eqz v30, :cond_1c

    .line 1875
    new-instance v30, Ljava/util/ArrayList;

    invoke-direct/range {v30 .. v30}, Ljava/util/ArrayList;-><init>()V

    .line 1876
    const-string/jumbo v32, "24"

    move-object/from16 v0, v32

    invoke-interface {v4, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v32

    move-object/from16 v0, v32

    instance-of v0, v0, Ljava/util/List;

    move/from16 v32, v0

    if-eqz v32, :cond_1a

    .line 1877
    const-string/jumbo v32, "24"

    move-object/from16 v0, v32

    invoke-interface {v4, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/List;

    move-object/from16 v0, v30

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 1883
    :goto_c
    invoke-virtual/range {v30 .. v30}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v30

    :goto_d
    invoke-interface/range {v30 .. v30}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1b

    invoke-interface/range {v30 .. v30}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 1884
    new-instance v32, Ljava/lang/StringBuilder;

    invoke-direct/range {v32 .. v32}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v32

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v32, ","

    move-object/from16 v0, v32

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v18

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0

    goto :goto_d

    .line 1764
    :cond_13
    :try_start_6
    const-string/jumbo v8, "39"

    invoke-interface {v13, v8}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_14

    .line 1765
    const-string/jumbo v8, "39"

    invoke-interface {v13, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    const-string/jumbo v12, ":"

    invoke-virtual {v8, v12}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_1

    move-result-object v8

    goto/16 :goto_5

    .line 1767
    :catch_1
    move-exception v8

    .line 1768
    :try_start_7
    sget-object v12, Lcom/peel/content/a/j;->c:Ljava/lang/String;

    sget-object v17, Lcom/peel/content/a/j;->c:Ljava/lang/String;

    move-object/from16 v0, v17

    invoke-static {v12, v0, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_14
    move-object v8, v9

    goto/16 :goto_5

    .line 1786
    :catch_2
    move-exception v8

    .line 1787
    sget-object v9, Lcom/peel/content/a/j;->c:Ljava/lang/String;

    sget-object v12, Lcom/peel/content/a/j;->c:Ljava/lang/String;

    invoke-static {v9, v12, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_6

    .line 1809
    :cond_15
    const/4 v8, 0x0

    goto/16 :goto_7

    .line 1823
    :cond_16
    const-string/jumbo v25, "sports"

    move-object/from16 v0, v25

    invoke-virtual {v0, v8}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_22

    .line 1824
    const-string/jumbo v25, "sports"

    .line 1825
    const/16 v33, 0x0

    .line 1827
    const-string/jumbo v8, "teams"

    invoke-interface {v4, v8}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_21

    .line 1828
    const-string/jumbo v8, "teams"

    invoke-interface {v4, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/util/ArrayList;

    .line 1830
    if-eqz v8, :cond_21

    .line 1831
    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v12

    new-array v0, v12, [Lcom/peel/data/SportsTeam;

    move-object/from16 v32, v0

    .line 1832
    const/4 v12, 0x0

    move/from16 v34, v12

    :goto_e
    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v12

    move/from16 v0, v34

    if-ge v0, v12, :cond_20

    .line 1833
    move/from16 v0, v34

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Ljava/util/Map;

    .line 1834
    new-instance v38, Lcom/peel/data/SportsTeam;

    const-string/jumbo v17, "teamId"

    move-object/from16 v0, v17

    invoke-interface {v12, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Ljava/lang/String;

    const-string/jumbo v18, "teamName"

    move-object/from16 v0, v18

    invoke-interface {v12, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Ljava/lang/String;

    const-string/jumbo v39, "teamLogo"

    move-object/from16 v0, v39

    invoke-interface {v12, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Ljava/lang/String;

    move-object/from16 v0, v38

    move-object/from16 v1, v17

    move-object/from16 v2, v18

    invoke-direct {v0, v1, v2, v12}, Lcom/peel/data/SportsTeam;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v38, v32, v34

    .line 1832
    add-int/lit8 v12, v34, 0x1

    move/from16 v34, v12

    goto :goto_e

    .line 1845
    :cond_17
    move-object/from16 v0, v30

    array-length v12, v0

    add-int/lit8 v12, v12, 0x1

    new-array v0, v12, [Ljava/lang/String;

    move-object/from16 v17, v0

    .line 1846
    const/4 v12, 0x0

    const/16 v18, 0x0

    move-object/from16 v0, v30

    array-length v0, v0

    move/from16 v32, v0

    move-object/from16 v0, v30

    move-object/from16 v1, v17

    move/from16 v2, v18

    move/from16 v3, v32

    invoke-static {v0, v12, v1, v2, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1847
    move-object/from16 v0, v30

    array-length v12, v0

    const-string/jumbo v18, "First Run"

    aput-object v18, v17, v12

    goto/16 :goto_9

    .line 1858
    :cond_18
    move-object/from16 v0, v17

    array-length v12, v0

    add-int/lit8 v12, v12, 0x1

    new-array v12, v12, [Ljava/lang/String;

    .line 1859
    const/16 v18, 0x0

    const/16 v30, 0x0

    move-object/from16 v0, v17

    array-length v0, v0

    move/from16 v32, v0

    move-object/from16 v0, v17

    move/from16 v1, v18

    move/from16 v2, v30

    move/from16 v3, v32

    invoke-static {v0, v1, v12, v2, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1860
    move-object/from16 v0, v17

    array-length v0, v0

    move/from16 v17, v0

    const-string/jumbo v18, "Broadcast Channels"

    aput-object v18, v12, v17

    goto/16 :goto_a

    .line 1865
    :cond_19
    const-string/jumbo v17, "14"

    move-object/from16 v0, v17

    invoke-interface {v4, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Ljava/lang/String;

    goto/16 :goto_b

    .line 1879
    :cond_1a
    const-string/jumbo v32, "24"

    move-object/from16 v0, v32

    invoke-interface {v4, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    move-object/from16 v0, v30

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_c

    .line 1887
    :cond_1b
    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->length()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    move-object/from16 v0, v18

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->deleteCharAt(I)Ljava/lang/StringBuilder;

    .line 1890
    :cond_1c
    new-instance v4, Lcom/peel/content/listing/LiveListing;

    const-string/jumbo v30, "17"

    .line 1893
    move-object/from16 v0, v30

    invoke-interface {v13, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/lang/String;

    .line 1892
    invoke-static {v13}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/Integer;->intValue()I

    move-result v13

    .line 1893
    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    const/16 v30, 0x0

    invoke-direct/range {v4 .. v30}, Lcom/peel/content/listing/LiveListing;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;IJLjava/lang/String;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Lcom/peel/data/SportsTeam;JLjava/lang/String;Ljava/lang/String;Z)V

    .line 1890
    move-object/from16 v0, v31

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1896
    move-object/from16 v0, v37

    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_0

    goto/16 :goto_4

    :cond_1d
    move-object/from16 v4, v31

    .line 1902
    goto/16 :goto_2

    :cond_1e
    move-object/from16 v12, v17

    goto/16 :goto_a

    :cond_1f
    move-object/from16 v17, v30

    goto/16 :goto_9

    :cond_20
    move-object/from16 v8, v25

    move/from16 v12, v33

    move-object/from16 v25, v32

    goto/16 :goto_8

    :cond_21
    move-object/from16 v8, v25

    move/from16 v12, v33

    move-object/from16 v25, v17

    goto/16 :goto_8

    :cond_22
    move-object v8, v12

    move-object/from16 v25, v17

    move/from16 v12, v18

    goto/16 :goto_8
.end method

.method private static b(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    .line 695
    const-string/jumbo v0, "prgids"

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 696
    const-string/jumbo v0, "library"

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 697
    const-string/jumbo v0, "user"

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 698
    const-string/jumbo v0, "room"

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/peel/data/ContentRoom;

    .line 700
    if-eqz v1, :cond_0

    if-eqz v2, :cond_0

    if-nez v3, :cond_4

    .line 701
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "\n *** user: "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    if-nez v1, :cond_1

    const-string/jumbo v0, "NULL"

    :goto_0
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 702
    sget-object v1, Lcom/peel/content/a/j;->c:Ljava/lang/String;

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 703
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "\n *** prgids: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    if-nez v2, :cond_2

    const-string/jumbo v0, "NULL"

    :goto_1
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 704
    sget-object v1, Lcom/peel/content/a/j;->c:Ljava/lang/String;

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 705
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "\n *** library: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    if-nez v3, :cond_3

    const-string/jumbo v0, "NULL"

    :goto_2
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 706
    sget-object v1, Lcom/peel/content/a/j;->c:Ljava/lang/String;

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 727
    :goto_3
    return-void

    :cond_1
    move-object v0, v1

    .line 701
    goto :goto_0

    :cond_2
    move-object v0, v2

    .line 703
    goto :goto_1

    :cond_3
    move-object v0, v3

    .line 705
    goto :goto_2

    .line 710
    :cond_4
    sget-object v2, Lcom/peel/content/a/j;->c:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, " ... updateChannelLineup : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "prgids"

    invoke-virtual {p0, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 712
    sget-object v2, Lcom/peel/content/a/j;->c:Ljava/lang/String;

    const-string/jumbo v3, "add channels to cloud"

    new-instance v4, Lcom/peel/content/a/bm;

    invoke-direct {v4, p0, v1, v0}, Lcom/peel/content/a/bm;-><init>(Landroid/os/Bundle;Ljava/lang/String;Lcom/peel/data/ContentRoom;)V

    invoke-static {v2, v3, v4}, Lcom/peel/util/i;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    goto :goto_3
.end method

.method public static b(Landroid/os/Bundle;Lcom/peel/util/t;)V
    .locals 5

    .prologue
    .line 251
    const-string/jumbo v0, "path"

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 253
    sget-object v1, Lcom/peel/content/a/j;->c:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, " xxx post path: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 254
    if-eqz v0, :cond_9

    .line 255
    const-string/jumbo v1, "show"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 256
    const-string/jumbo v1, "fav"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string/jumbo v1, "dislike"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 257
    :cond_0
    invoke-static {p0, p1}, Lcom/peel/content/a/j;->p(Landroid/os/Bundle;Lcom/peel/util/t;)V

    .line 295
    :cond_1
    :goto_0
    return-void

    .line 262
    :cond_2
    const-string/jumbo v1, "genres"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 263
    invoke-static {p0, p1}, Lcom/peel/content/a/j;->q(Landroid/os/Bundle;Lcom/peel/util/t;)V

    goto :goto_0

    .line 267
    :cond_3
    const-string/jumbo v1, "channels/lineup"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 268
    invoke-static {p0}, Lcom/peel/content/a/j;->b(Landroid/os/Bundle;)V

    goto :goto_0

    .line 272
    :cond_4
    const-string/jumbo v1, "channel/fav"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_5

    const-string/jumbo v1, "channel/unfav"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 274
    :cond_5
    invoke-static {p0, p1}, Lcom/peel/content/a/j;->r(Landroid/os/Bundle;Lcom/peel/util/t;)V

    goto :goto_0

    .line 279
    :cond_6
    const-string/jumbo v1, "language"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 280
    invoke-static {p0, p1}, Lcom/peel/content/a/j;->s(Landroid/os/Bundle;Lcom/peel/util/t;)V

    goto :goto_0

    .line 284
    :cond_7
    const-string/jumbo v1, "add/setreminder"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 285
    invoke-static {p0, p1}, Lcom/peel/content/a/j;->g(Landroid/os/Bundle;Lcom/peel/util/t;)V

    .line 288
    :cond_8
    const-string/jumbo v1, "delete/setreminder"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 289
    invoke-static {p0, p1}, Lcom/peel/content/a/j;->h(Landroid/os/Bundle;Lcom/peel/util/t;)V

    .line 293
    :cond_9
    if-eqz p1, :cond_1

    .line 294
    const/4 v1, 0x0

    const/4 v2, 0x0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "unrecognized path: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v1, v2, v0}, Lcom/peel/util/t;->a(ZLjava/lang/Object;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static b(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 334
    sget-object v0, Lcom/peel/content/a/j;->d:Ljava/lang/StringBuilder;

    const/4 v1, 0x0

    invoke-virtual {v0, v1, p0}, Ljava/lang/StringBuilder;->insert(ILjava/lang/String;)Ljava/lang/StringBuilder;

    .line 335
    return-void
.end method

.method public static b(Ljava/lang/String;ILcom/peel/util/t;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I",
            "Lcom/peel/util/t",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2086
    sget-object v0, Lcom/peel/content/a/j;->c:Ljava/lang/String;

    const-string/jumbo v1, "get more like this shows"

    new-instance v2, Lcom/peel/content/a/ba;

    invoke-direct {v2, p0, p1, p2}, Lcom/peel/content/a/ba;-><init>(Ljava/lang/String;ILcom/peel/util/t;)V

    invoke-static {v0, v1, v2}, Lcom/peel/util/i;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    .line 2094
    return-void
.end method

.method public static b(Ljava/lang/String;Lcom/peel/util/t;)V
    .locals 3

    .prologue
    .line 1413
    sget-object v0, Lcom/peel/content/a/j;->c:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "get languages for country "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/peel/content/a/al;

    invoke-direct {v2, p0, p1}, Lcom/peel/content/a/al;-><init>(Ljava/lang/String;Lcom/peel/util/t;)V

    invoke-static {v0, v1, v2}, Lcom/peel/util/i;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    .line 1435
    return-void
.end method

.method public static b(Ljava/lang/String;Ljava/lang/String;Lcom/peel/util/t;)V
    .locals 3

    .prologue
    .line 1318
    sget-object v0, Lcom/peel/content/a/j;->c:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "get subregions by region: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " country: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/peel/content/a/ag;

    invoke-direct {v2, p0, p1, p2}, Lcom/peel/content/a/ag;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/peel/util/t;)V

    invoke-static {v0, v1, v2}, Lcom/peel/util/i;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    .line 1356
    return-void
.end method

.method public static b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/peel/util/t;)V
    .locals 3

    .prologue
    .line 1651
    sget-object v0, Lcom/peel/content/a/j;->c:Ljava/lang/String;

    const-string/jumbo v1, "get premium channels"

    new-instance v2, Lcom/peel/content/a/aw;

    invoke-direct {v2, p1, p0, p3, p2}, Lcom/peel/content/a/aw;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/peel/util/t;Ljava/lang/String;)V

    invoke-static {v0, v1, v2}, Lcom/peel/util/i;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    .line 1667
    return-void
.end method

.method public static c(Landroid/os/Bundle;Lcom/peel/util/t;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/Bundle;",
            "Lcom/peel/util/t",
            "<[",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1064
    new-instance v0, Lcom/peel/content/a/t;

    const/4 v1, 0x2

    invoke-direct {v0, v1, p0, p1}, Lcom/peel/content/a/t;-><init>(ILandroid/os/Bundle;Lcom/peel/util/t;)V

    invoke-static {p0, v0}, Lcom/peel/content/a/j;->o(Landroid/os/Bundle;Lcom/peel/util/t;)V

    .line 1091
    return-void
.end method

.method static synthetic c(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 50
    invoke-static {p0}, Lcom/peel/content/a/j;->h(Ljava/lang/String;)V

    return-void
.end method

.method public static c(Ljava/lang/String;Lcom/peel/util/t;)V
    .locals 3

    .prologue
    .line 1519
    sget-object v0, Lcom/peel/content/a/j;->c:Ljava/lang/String;

    const-string/jumbo v1, "grabbing turned on countries"

    new-instance v2, Lcom/peel/content/a/ap;

    invoke-direct {v2, p0, p1}, Lcom/peel/content/a/ap;-><init>(Ljava/lang/String;Lcom/peel/util/t;)V

    invoke-static {v0, v1, v2}, Lcom/peel/util/i;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    .line 1582
    return-void
.end method

.method public static c(Ljava/lang/String;Ljava/lang/String;Lcom/peel/util/t;)V
    .locals 3

    .prologue
    .line 1585
    sget-object v0, Lcom/peel/content/a/j;->c:Ljava/lang/String;

    const-string/jumbo v1, "grabbing turned on countries"

    new-instance v2, Lcom/peel/content/a/as;

    invoke-direct {v2, p0, p1, p2}, Lcom/peel/content/a/as;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/peel/util/t;)V

    invoke-static {v0, v1, v2}, Lcom/peel/util/i;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    .line 1648
    return-void
.end method

.method private static c(Ljava/lang/String;Ljava/lang/String;)[Lcom/peel/data/Channel;
    .locals 17

    .prologue
    .line 1907
    .line 1910
    :try_start_0
    const-string/jumbo v1, "["

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 1911
    sget-object v1, Lcom/peel/content/a/j;->a:Lorg/codehaus/jackson/map/ObjectMapper;

    const-class v2, Ljava/util/HashMap;

    move-object/from16 v0, p1

    invoke-virtual {v1, v0, v2}, Lorg/codehaus/jackson/map/ObjectMapper;->readValue(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/HashMap;

    const-string/jumbo v2, "channelLineup"

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    move-object v0, v1

    check-cast v0, Ljava/util/List;

    move-object v13, v0

    .line 1912
    if-eqz v13, :cond_0

    invoke-interface {v13}, Ljava/util/List;->size()I

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    const/4 v1, 0x0

    .line 1932
    :goto_0
    return-object v1

    .line 1914
    :cond_1
    invoke-interface {v13}, Ljava/util/List;->size()I

    move-result v1

    new-array v14, v1, [Lcom/peel/data/Channel;

    .line 1915
    const/4 v1, 0x0

    move v15, v1

    :goto_1
    invoke-interface {v13}, Ljava/util/List;->size()I

    move-result v1

    if-ge v15, v1, :cond_3

    .line 1916
    invoke-interface {v13, v15}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    move-object v0, v1

    check-cast v0, Ljava/util/Map;

    move-object v12, v0

    .line 1917
    const-string/jumbo v1, "callsign"

    invoke-interface {v12, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 1918
    const-string/jumbo v1, "channelnumber"

    invoke-interface {v12, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    .line 1919
    new-instance v1, Lcom/peel/data/Channel;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v4, "^[0]*"

    const-string/jumbo v5, ""

    invoke-virtual {v6, v4, v5}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v4, "prgsvcid"

    invoke-interface {v12, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    const-string/jumbo v5, "name"

    invoke-interface {v12, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    const-string/jumbo v7, "imageurl"

    invoke-interface {v12, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    const-string/jumbo v8, "type"

    invoke-interface {v12, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    const-string/jumbo v9, "HD"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    const/4 v8, 0x1

    :goto_2
    const-string/jumbo v9, "lang"

    invoke-interface {v12, v9}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    const-string/jumbo v10, "tier"

    invoke-interface {v12, v10}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/String;

    const-string/jumbo v11, ""

    const-string/jumbo v16, "source"

    move-object/from16 v0, v16

    invoke-interface {v12, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Ljava/lang/String;

    invoke-direct/range {v1 .. v12}, Lcom/peel/data/Channel;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v1, v14, v15

    .line 1915
    add-int/lit8 v1, v15, 0x1

    move v15, v1

    goto/16 :goto_1

    .line 1919
    :cond_2
    const/4 v8, 0x0

    goto :goto_2

    :cond_3
    move-object v1, v14

    .line 1921
    goto/16 :goto_0

    .line 1922
    :cond_4
    sget-object v1, Lcom/peel/content/a/j;->a:Lorg/codehaus/jackson/map/ObjectMapper;

    const-class v2, Ljava/util/HashMap;

    move-object/from16 v0, p1

    invoke-virtual {v1, v0, v2}, Lorg/codehaus/jackson/map/ObjectMapper;->readValue(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/HashMap;

    const-string/jumbo v2, "channelLineup"

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    move-object v0, v1

    check-cast v0, Ljava/util/LinkedHashMap;

    move-object v12, v0

    .line 1923
    const-string/jumbo v1, "callsign"

    invoke-virtual {v12, v1}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 1924
    const-string/jumbo v1, "channelnumber"

    invoke-virtual {v12, v1}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    .line 1925
    const/4 v1, 0x1

    new-array v13, v1, [Lcom/peel/data/Channel;

    const/4 v14, 0x0

    new-instance v1, Lcom/peel/data/Channel;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v4, "^[0]*"

    const-string/jumbo v5, ""

    invoke-virtual {v6, v4, v5}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v4, "prgsvcid"

    invoke-virtual {v12, v4}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    const-string/jumbo v5, "name"

    invoke-virtual {v12, v5}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    const-string/jumbo v7, "imageurl"

    invoke-virtual {v12, v7}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    const-string/jumbo v8, "type"

    invoke-virtual {v12, v8}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    const-string/jumbo v9, "HD"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_5

    const/4 v8, 0x1

    :goto_3
    const-string/jumbo v9, "lang"

    invoke-virtual {v12, v9}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    const-string/jumbo v10, "tier"

    invoke-virtual {v12, v10}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/String;

    const-string/jumbo v11, ""

    const-string/jumbo v15, "source"

    invoke-virtual {v12, v15}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Ljava/lang/String;

    invoke-direct/range {v1 .. v12}, Lcom/peel/data/Channel;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v1, v13, v14
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-object v1, v13

    goto/16 :goto_0

    :cond_5
    const/4 v8, 0x0

    goto :goto_3

    .line 1927
    :catch_0
    move-exception v1

    .line 1928
    sget-object v2, Lcom/peel/content/a/j;->c:Ljava/lang/String;

    sget-object v3, Lcom/peel/content/a/j;->c:Ljava/lang/String;

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1929
    const/4 v1, 0x0

    goto/16 :goto_0
.end method

.method private static d(Ljava/lang/String;Ljava/lang/String;)Ljava/util/LinkedHashMap;
    .locals 21
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/LinkedHashMap",
            "<",
            "Ljava/lang/String;",
            "[",
            "Lcom/peel/data/Channel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1936
    new-instance v17, Ljava/util/LinkedHashMap;

    invoke-direct/range {v17 .. v17}, Ljava/util/LinkedHashMap;-><init>()V

    .line 1938
    if-eqz p1, :cond_1

    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "null"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    .line 1940
    :try_start_0
    new-instance v18, Lorg/json/JSONArray;

    move-object/from16 v0, v18

    move-object/from16 v1, p1

    invoke-direct {v0, v1}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 1942
    sget-object v2, Lcom/peel/content/a/j;->c:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "\n\npremiumProviders.length(): "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual/range {v18 .. v18}, Lorg/json/JSONArray;->length()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1943
    const/4 v2, 0x0

    move/from16 v16, v2

    :goto_0
    invoke-virtual/range {v18 .. v18}, Lorg/json/JSONArray;->length()I

    move-result v2

    move/from16 v0, v16

    if-ge v0, v2, :cond_1

    .line 1944
    move-object/from16 v0, v18

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v19

    .line 1946
    sget-object v2, Lcom/peel/content/a/j;->c:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "\n\nprocess premium provider: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "name"

    move-object/from16 v0, v19

    invoke-virtual {v0, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1947
    sget-object v2, Lcom/peel/content/a/j;->a:Lorg/codehaus/jackson/map/ObjectMapper;

    invoke-virtual/range {v19 .. v19}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v3

    const-class v4, Ljava/util/HashMap;

    invoke-virtual {v2, v3, v4}, Lorg/codehaus/jackson/map/ObjectMapper;->readValue(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/HashMap;

    const-string/jumbo v3, "channels"

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    move-object v0, v2

    check-cast v0, Ljava/util/List;

    move-object v14, v0

    .line 1948
    invoke-interface {v14}, Ljava/util/List;->size()I

    move-result v2

    new-array v0, v2, [Lcom/peel/data/Channel;

    move-object/from16 v20, v0

    .line 1949
    sget-object v2, Lcom/peel/content/a/j;->c:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "\n\nobjects/channels size: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-interface {v14}, Ljava/util/List;->size()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1950
    const/4 v2, 0x0

    move v15, v2

    :goto_1
    invoke-interface {v14}, Ljava/util/List;->size()I

    move-result v2

    if-ge v15, v2, :cond_0

    .line 1951
    invoke-interface {v14, v15}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    move-object v0, v2

    check-cast v0, Ljava/util/Map;

    move-object v6, v0

    .line 1952
    const-string/jumbo v2, "callsign"

    invoke-interface {v6, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 1955
    new-instance v2, Lcom/peel/data/Channel;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v5, ""

    const-string/jumbo v7, "name"

    .line 1956
    invoke-interface {v6, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    const-string/jumbo v7, ""

    const-string/jumbo v8, ""

    const/4 v9, 0x0

    const-string/jumbo v10, ""

    const-string/jumbo v11, ""

    const-string/jumbo v12, ""

    const-string/jumbo v13, ""

    invoke-direct/range {v2 .. v13}, Lcom/peel/data/Channel;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v20, v15

    .line 1950
    add-int/lit8 v2, v15, 0x1

    move v15, v2

    goto :goto_1

    .line 1958
    :cond_0
    const-string/jumbo v2, "name"

    move-object/from16 v0, v19

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v17

    move-object/from16 v1, v20

    invoke-virtual {v0, v2, v1}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1943
    add-int/lit8 v2, v16, 0x1

    move/from16 v16, v2

    goto/16 :goto_0

    .line 1961
    :catch_0
    move-exception v2

    .line 1962
    sget-object v3, Lcom/peel/content/a/j;->c:Ljava/lang/String;

    sget-object v4, Lcom/peel/content/a/j;->c:Ljava/lang/String;

    invoke-static {v3, v4, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1965
    :cond_1
    return-object v17
.end method

.method public static d(Landroid/os/Bundle;Lcom/peel/util/t;)V
    .locals 6

    .prologue
    const/4 v5, 0x2

    .line 1209
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/peel/content/a/j;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "epg/schedules/kwhintall?term=%s&country=%s"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-array v1, v5, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string/jumbo v3, "keyword"

    invoke-virtual {p0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v4, "UTF-8"

    invoke-static {v3, v4}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string/jumbo v3, "country"

    invoke-virtual {p0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lcom/peel/content/a/z;

    invoke-direct {v1, v5, p1}, Lcom/peel/content/a/z;-><init>(ILcom/peel/util/t;)V

    invoke-static {v0, v1}, Lcom/peel/util/b/a;->b(Ljava/lang/String;Lcom/peel/util/t;)V

    .line 1228
    return-void
.end method

.method public static d(Ljava/lang/String;Lcom/peel/util/t;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/peel/util/t",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/peel/content/listing/Listing;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 2098
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 2099
    const-string/jumbo v1, "showids"

    invoke-interface {v0, v1, p0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2101
    sget-object v1, Lcom/peel/content/a/j;->c:Ljava/lang/String;

    const-string/jumbo v2, "get program infos"

    new-instance v3, Lcom/peel/content/a/bb;

    invoke-direct {v3, v0, p1}, Lcom/peel/content/a/bb;-><init>(Ljava/util/Map;Lcom/peel/util/t;)V

    invoke-static {v1, v2, v3}, Lcom/peel/util/i;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    .line 2117
    return-void
.end method

.method public static d(Ljava/lang/String;Ljava/lang/String;Lcom/peel/util/t;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/peel/util/t",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/peel/content/listing/Listing;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 2066
    sget-object v0, Lcom/peel/content/a/j;->c:Ljava/lang/String;

    const-string/jumbo v1, "getting schedule by channel"

    new-instance v2, Lcom/peel/content/a/ay;

    invoke-direct {v2, p0, p1, p2}, Lcom/peel/content/a/ay;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/peel/util/t;)V

    invoke-static {v0, v1, v2}, Lcom/peel/util/i;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    .line 2083
    return-void
.end method

.method static synthetic d(Ljava/lang/String;)[Ljava/lang/String;
    .locals 1

    .prologue
    .line 50
    invoke-static {p0}, Lcom/peel/content/a/j;->i(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic e(Ljava/lang/String;)Ljava/util/HashMap;
    .locals 1

    .prologue
    .line 50
    invoke-static {p0}, Lcom/peel/content/a/j;->j(Ljava/lang/String;)Ljava/util/HashMap;

    move-result-object v0

    return-object v0
.end method

.method static synthetic e(Landroid/os/Bundle;Lcom/peel/util/t;)V
    .locals 0

    .prologue
    .line 50
    invoke-static {p0, p1}, Lcom/peel/content/a/j;->l(Landroid/os/Bundle;Lcom/peel/util/t;)V

    return-void
.end method

.method public static e(Ljava/lang/String;Ljava/lang/String;Lcom/peel/util/t;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/peel/util/t",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/peel/content/listing/Listing;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 2143
    sget-object v0, Lcom/peel/content/a/j;->c:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "get program info: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/peel/content/a/bd;

    invoke-direct {v2, p0, p1, p2}, Lcom/peel/content/a/bd;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/peel/util/t;)V

    invoke-static {v0, v1, v2}, Lcom/peel/util/i;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    .line 2165
    return-void
.end method

.method static synthetic f(Ljava/lang/String;)Lcom/peel/content/user/User;
    .locals 1

    .prologue
    .line 50
    invoke-static {p0}, Lcom/peel/content/a/j;->k(Ljava/lang/String;)Lcom/peel/content/user/User;

    move-result-object v0

    return-object v0
.end method

.method static synthetic f(Landroid/os/Bundle;Lcom/peel/util/t;)V
    .locals 0

    .prologue
    .line 50
    invoke-static {p0, p1}, Lcom/peel/content/a/j;->n(Landroid/os/Bundle;Lcom/peel/util/t;)V

    return-void
.end method

.method static synthetic g(Ljava/lang/String;)Ljava/util/List;
    .locals 1

    .prologue
    .line 50
    invoke-static {p0}, Lcom/peel/content/a/j;->l(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method private static g(Landroid/os/Bundle;Lcom/peel/util/t;)V
    .locals 3

    .prologue
    .line 298
    sget-object v0, Lcom/peel/content/a/j;->c:Ljava/lang/String;

    const-string/jumbo v1, "set reminder"

    new-instance v2, Lcom/peel/content/a/k;

    invoke-direct {v2, p0, p1}, Lcom/peel/content/a/k;-><init>(Landroid/os/Bundle;Lcom/peel/util/t;)V

    invoke-static {v0, v1, v2}, Lcom/peel/util/i;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    .line 331
    return-void
.end method

.method private static h(Landroid/os/Bundle;Lcom/peel/util/t;)V
    .locals 3

    .prologue
    .line 342
    sget-object v0, Lcom/peel/content/a/j;->c:Ljava/lang/String;

    const-string/jumbo v1, "cancel reminder"

    new-instance v2, Lcom/peel/content/a/aa;

    invoke-direct {v2, p0, p1}, Lcom/peel/content/a/aa;-><init>(Landroid/os/Bundle;Lcom/peel/util/t;)V

    invoke-static {v0, v1, v2}, Lcom/peel/util/i;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    .line 373
    return-void
.end method

.method private static h(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 338
    sget-object v0, Lcom/peel/content/a;->f:Lcom/peel/content/user/User;

    invoke-virtual {v0, p0}, Lcom/peel/content/user/User;->c(Ljava/lang/String;)V

    .line 339
    return-void
.end method

.method private static i(Landroid/os/Bundle;Lcom/peel/util/t;)V
    .locals 3

    .prologue
    .line 384
    sget-object v0, Lcom/peel/content/a/j;->c:Ljava/lang/String;

    const-string/jumbo v1, "download listings"

    new-instance v2, Lcom/peel/content/a/au;

    invoke-direct {v2, p0, p1}, Lcom/peel/content/a/au;-><init>(Landroid/os/Bundle;Lcom/peel/util/t;)V

    invoke-static {v0, v1, v2}, Lcom/peel/util/i;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    .line 492
    return-void
.end method

.method private static i(Ljava/lang/String;)[Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 1969
    if-nez p0, :cond_0

    move-object v0, v1

    .line 1993
    :goto_0
    return-object v0

    .line 1971
    :cond_0
    :try_start_0
    new-instance v3, Lorg/json/JSONObject;

    invoke-direct {v3, p0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 1978
    const-string/jumbo v0, "["

    invoke-virtual {p0, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1979
    sget-object v0, Lcom/peel/content/a/j;->c:Ljava/lang/String;

    const-string/jumbo v4, "multiple exclusion channels found"

    invoke-static {v0, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1980
    const-string/jumbo v0, "channelLineup"

    invoke-virtual {v3, v0}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v3

    .line 1981
    invoke-virtual {v3}, Lorg/json/JSONArray;->length()I

    move-result v0

    new-array v0, v0, [Ljava/lang/String;

    .line 1982
    invoke-virtual {v3}, Lorg/json/JSONArray;->length()I

    move-result v4

    :goto_1
    if-ge v2, v4, :cond_1

    .line 1983
    invoke-virtual {v3, v2}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v5

    const-string/jumbo v6, "prgsvcid"

    invoke-virtual {v5, v6}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v0, v2

    .line 1982
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 1985
    :cond_1
    invoke-static {v0}, Ljava/util/Arrays;->sort([Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1991
    :catch_0
    move-exception v0

    .line 1992
    sget-object v2, Lcom/peel/content/a/j;->c:Ljava/lang/String;

    sget-object v3, Lcom/peel/content/a/j;->c:Ljava/lang/String;

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move-object v0, v1

    .line 1993
    goto :goto_0

    .line 1987
    :cond_2
    :try_start_1
    sget-object v0, Lcom/peel/content/a/j;->c:Ljava/lang/String;

    const-string/jumbo v2, "single exclusion channels found"

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1988
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v4, "channelLineup"

    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v3

    const-string/jumbo v4, "prgsvcid"

    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v0, v2
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method

.method private static j(Ljava/lang/String;)Ljava/util/HashMap;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1999
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    .line 2001
    :try_start_0
    sget-object v1, Lcom/peel/content/a/j;->a:Lorg/codehaus/jackson/map/ObjectMapper;

    const-class v2, Ljava/util/HashMap;

    invoke-virtual {v1, p0, v2}, Lorg/codehaus/jackson/map/ObjectMapper;->readValue(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/HashMap;

    const-string/jumbo v2, "showinfo"

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    .line 2003
    instance-of v1, v2, Ljava/util/List;

    if-eqz v1, :cond_0

    .line 2004
    move-object v0, v2

    check-cast v0, Ljava/util/List;

    move-object v1, v0

    .line 2006
    const/4 v3, 0x0

    move v5, v3

    :goto_0
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v3

    if-ge v5, v3, :cond_1

    .line 2007
    move-object v0, v2

    check-cast v0, Ljava/util/List;

    move-object v3, v0

    invoke-interface {v3, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Map;

    .line 2008
    const-string/jumbo v6, "showid"

    invoke-interface {v3, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    const-string/jumbo v7, "title"

    invoke-interface {v3, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v4, v6, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2006
    add-int/lit8 v3, v5, 0x1

    move v5, v3

    goto :goto_0

    .line 2012
    :cond_0
    check-cast v2, Ljava/util/Map;

    .line 2013
    const-string/jumbo v1, "showid"

    invoke-interface {v2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    const-string/jumbo v3, "title"

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v4, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_1
    move-object v1, v4

    .line 2021
    :goto_1
    return-object v1

    .line 2016
    :catch_0
    move-exception v1

    .line 2017
    sget-object v2, Lcom/peel/content/a/j;->c:Ljava/lang/String;

    sget-object v3, Lcom/peel/content/a/j;->c:Ljava/lang/String;

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 2018
    const/4 v1, 0x0

    goto :goto_1
.end method

.method private static j(Landroid/os/Bundle;Lcom/peel/util/t;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/Bundle;",
            "Lcom/peel/util/t",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/peel/content/listing/Listing;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 497
    sget-object v0, Lcom/peel/content/a/j;->c:Ljava/lang/String;

    const-string/jumbo v1, "getShowTimes"

    new-instance v2, Lcom/peel/content/a/bf;

    invoke-direct {v2, p0, p1}, Lcom/peel/content/a/bf;-><init>(Landroid/os/Bundle;Lcom/peel/util/t;)V

    invoke-static {v0, v1, v2}, Lcom/peel/util/i;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    .line 520
    return-void
.end method

.method private static k(Ljava/lang/String;)Lcom/peel/content/user/User;
    .locals 15

    .prologue
    const/4 v9, 0x0

    const/4 v10, 0x0

    .line 2026
    .line 2028
    :try_start_0
    sget-object v2, Lcom/peel/content/a/j;->a:Lorg/codehaus/jackson/map/ObjectMapper;

    const-class v3, Ljava/util/HashMap;

    invoke-virtual {v2, p0, v3}, Lorg/codehaus/jackson/map/ObjectMapper;->readValue(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/HashMap;

    const-string/jumbo v3, "users"

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    move-object v0, v2

    check-cast v0, Ljava/util/HashMap;

    move-object v8, v0

    .line 2029
    if-eqz v8, :cond_2

    .line 2030
    new-instance v12, Landroid/os/Bundle;

    invoke-direct {v12}, Landroid/os/Bundle;-><init>()V

    .line 2031
    const-string/jumbo v3, "age"

    const-string/jumbo v2, "age"

    invoke-virtual {v8, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-virtual {v12, v3, v4, v5}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 2035
    const-string/jumbo v2, "program"

    invoke-static {v2}, Lcom/peel/content/a;->d(Ljava/lang/String;)[[Ljava/lang/String;

    move-result-object v13

    .line 2036
    new-instance v14, Ljava/util/ArrayList;

    array-length v2, v13

    invoke-direct {v14, v2}, Ljava/util/ArrayList;-><init>(I)V

    move v11, v10

    .line 2037
    :goto_0
    array-length v2, v13

    if-ge v11, v2, :cond_0

    .line 2039
    new-instance v2, Lcom/peel/data/Genre;

    aget-object v3, v13, v11

    const/4 v4, 0x1

    aget-object v3, v3, v4

    aget-object v4, v13, v11

    const/4 v5, 0x0

    aget-object v4, v4, v5

    const/4 v5, 0x0

    aget-object v6, v13, v11

    const/4 v7, 0x2

    aget-object v6, v6, v7

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    const/4 v7, 0x0

    invoke-direct/range {v2 .. v7}, Lcom/peel/data/Genre;-><init>(Ljava/lang/String;Ljava/lang/String;IIZ)V

    invoke-interface {v14, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2037
    add-int/lit8 v2, v11, 0x1

    move v11, v2

    goto :goto_0

    .line 2041
    :cond_0
    const-string/jumbo v2, "sports"

    invoke-static {v2}, Lcom/peel/content/a;->d(Ljava/lang/String;)[[Ljava/lang/String;

    move-result-object v11

    .line 2042
    new-instance v13, Ljava/util/ArrayList;

    array-length v2, v11

    invoke-direct {v13, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 2043
    :goto_1
    array-length v2, v11

    if-ge v10, v2, :cond_1

    .line 2045
    new-instance v2, Lcom/peel/data/Genre;

    aget-object v3, v11, v10

    const/4 v4, 0x1

    aget-object v3, v3, v4

    aget-object v4, v11, v10

    const/4 v5, 0x0

    aget-object v4, v4, v5

    const/4 v5, 0x0

    aget-object v6, v11, v10

    const/4 v7, 0x2

    aget-object v6, v6, v7

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    const/4 v7, 0x0

    invoke-direct/range {v2 .. v7}, Lcom/peel/data/Genre;-><init>(Ljava/lang/String;Ljava/lang/String;IIZ)V

    invoke-interface {v13, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2043
    add-int/lit8 v2, v10, 0x1

    move v10, v2

    goto :goto_1

    .line 2048
    :cond_1
    new-instance v3, Lcom/peel/content/user/User;

    const-string/jumbo v2, "id"

    invoke-virtual {v8, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-direct {v3, v2, v12}, Lcom/peel/content/user/User;-><init>(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 2049
    invoke-virtual {v3, v14}, Lcom/peel/content/user/User;->b(Ljava/util/List;)V

    .line 2050
    invoke-virtual {v3, v13}, Lcom/peel/content/user/User;->c(Ljava/util/List;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-object v2, v3

    .line 2057
    :goto_2
    return-object v2

    .line 2052
    :catch_0
    move-exception v2

    .line 2053
    sget-object v3, Lcom/peel/content/a/j;->c:Ljava/lang/String;

    sget-object v4, Lcom/peel/content/a/j;->c:Ljava/lang/String;

    invoke-static {v3, v4, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move-object v2, v9

    .line 2054
    goto :goto_2

    :cond_2
    move-object v2, v9

    goto :goto_2
.end method

.method private static k(Landroid/os/Bundle;Lcom/peel/util/t;)V
    .locals 5

    .prologue
    .line 558
    const-string/jumbo v0, "library"

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 560
    const-string/jumbo v0, "content_user"

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/peel/content/user/User;

    .line 562
    if-eqz v1, :cond_0

    if-nez v0, :cond_1

    .line 563
    :cond_0
    const/4 v0, 0x0

    const/4 v1, 0x0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "getLineup missing arguments "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v0, v1, v2}, Lcom/peel/util/t;->a(ZLjava/lang/Object;Ljava/lang/String;)V

    .line 688
    :goto_0
    return-void

    .line 567
    :cond_1
    sget-object v2, Lcom/peel/content/a/j;->c:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "download channel lineup: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, Lcom/peel/content/a/j;->b:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "epg/schedules/channellineups/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Lcom/peel/content/a/bj;

    invoke-direct {v4, v1, p1, p0, v0}, Lcom/peel/content/a/bj;-><init>(Ljava/lang/String;Lcom/peel/util/t;Landroid/os/Bundle;Lcom/peel/content/user/User;)V

    invoke-static {v2, v3, v4}, Lcom/peel/util/i;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    goto :goto_0
.end method

.method private static l(Ljava/lang/String;)Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/peel/content/listing/Listing;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2120
    const/4 v1, 0x0

    .line 2123
    :try_start_0
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0, p0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 2124
    const-string/jumbo v2, "programs"

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v2

    .line 2125
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2127
    const/4 v1, 0x0

    :goto_0
    :try_start_1
    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result v3

    if-ge v1, v3, :cond_0

    .line 2128
    new-instance v3, Lcom/peel/content/listing/LiveListing;

    invoke-direct {v3}, Lcom/peel/content/listing/LiveListing;-><init>()V

    .line 2129
    invoke-virtual {v2, v1}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/peel/content/listing/LiveListing;->a(Lorg/json/JSONObject;)V

    .line 2130
    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1

    .line 2127
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2132
    :catch_0
    move-exception v0

    move-object v5, v0

    move-object v0, v1

    move-object v1, v5

    .line 2133
    :goto_1
    sget-object v2, Lcom/peel/content/a/j;->c:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "json error : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2136
    :cond_0
    return-object v0

    .line 2132
    :catch_1
    move-exception v1

    goto :goto_1
.end method

.method private static l(Landroid/os/Bundle;Lcom/peel/util/t;)V
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 735
    const-string/jumbo v0, "library"

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 736
    const-string/jumbo v1, "hdprefs"

    invoke-virtual {p0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 737
    const-string/jumbo v2, "user"

    invoke-virtual {p0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 739
    if-eqz v0, :cond_0

    if-eqz v2, :cond_0

    if-nez v1, :cond_1

    .line 741
    :cond_0
    sget-object v0, Lcom/peel/content/a/j;->c:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "getExcludedLineup missing arguments "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 742
    const/4 v0, 0x0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "getExcludedLineup missing arguments "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v7, v0, v1}, Lcom/peel/util/t;->a(ZLjava/lang/Object;Ljava/lang/String;)V

    .line 771
    :goto_0
    return-void

    .line 746
    :cond_1
    sget-object v2, Lcom/peel/content/a/j;->c:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "download exclusion list: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "%sepg/schedules/excludedchannels/%s?hdpreference=%s&maxtier=%s"

    const/4 v5, 0x4

    new-array v5, v5, [Ljava/lang/Object;

    sget-object v6, Lcom/peel/content/a/j;->b:Ljava/lang/String;

    aput-object v6, v5, v7

    const/4 v6, 0x1

    aput-object v0, v5, v6

    const/4 v6, 0x2

    aput-object v1, v5, v6

    const/4 v6, 0x3

    const-string/jumbo v7, "2"

    aput-object v7, v5, v6

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Lcom/peel/content/a/bo;

    invoke-direct {v4, v0, v1, p1}, Lcom/peel/content/a/bo;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/peel/util/t;)V

    invoke-static {v2, v3, v4}, Lcom/peel/util/i;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    goto :goto_0
.end method

.method private static m(Landroid/os/Bundle;Lcom/peel/util/t;)V
    .locals 3

    .prologue
    .line 856
    sget-object v0, Lcom/peel/content/a/j;->c:Ljava/lang/String;

    const-string/jumbo v1, "download favorites channels"

    new-instance v2, Lcom/peel/content/a/m;

    invoke-direct {v2, p0, p1}, Lcom/peel/content/a/m;-><init>(Landroid/os/Bundle;Lcom/peel/util/t;)V

    invoke-static {v0, v1, v2}, Lcom/peel/util/i;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    .line 886
    return-void
.end method

.method private static n(Landroid/os/Bundle;Lcom/peel/util/t;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/Bundle;",
            "Lcom/peel/util/t",
            "<[",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x2

    const/4 v6, 0x0

    .line 988
    const-string/jumbo v0, "library"

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 989
    const-string/jumbo v1, "user"

    invoke-virtual {p0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 990
    const-string/jumbo v2, "keyword"

    invoke-virtual {p0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 991
    const-string/jumbo v3, "country"

    invoke-virtual {p0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 992
    if-eqz v0, :cond_0

    if-eqz v1, :cond_0

    if-nez v2, :cond_1

    .line 993
    :cond_0
    const/4 v0, 0x0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "fullSearch missing arguments "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v6, v0, v1}, Lcom/peel/util/t;->a(ZLjava/lang/Object;Ljava/lang/String;)V

    .line 1022
    :goto_0
    return-void

    .line 1000
    :cond_1
    :try_start_0
    const-string/jumbo v0, "%sepg/schedules/searchallpartial?term=%s&country=%s"

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v4, 0x0

    sget-object v5, Lcom/peel/content/a/j;->b:Ljava/lang/String;

    aput-object v5, v1, v4

    const/4 v4, 0x1

    const-string/jumbo v5, "UTF-8"

    invoke-static {v2, v5}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v1, v4

    const/4 v4, 0x2

    const-string/jumbo v5, "UTF-8"

    invoke-static {v3, v5}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v4

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 1007
    :goto_1
    sget-object v1, Lcom/peel/content/a/j;->c:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, " *********** fullSearch listingUrl **************\n "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1008
    new-instance v1, Lcom/peel/content/a/r;

    invoke-direct {v1, v7, p1}, Lcom/peel/content/a/r;-><init>(ILcom/peel/util/t;)V

    invoke-static {v0, v1}, Lcom/peel/util/b/a;->b(Ljava/lang/String;Lcom/peel/util/t;)V

    goto :goto_0

    .line 1001
    :catch_0
    move-exception v0

    .line 1002
    sget-object v1, Lcom/peel/content/a/j;->c:Ljava/lang/String;

    sget-object v3, Lcom/peel/content/a/j;->c:Ljava/lang/String;

    invoke-static {v1, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1004
    const-string/jumbo v0, "%sepg/schedules/searchallpartial?term=%s&country=%s"

    new-array v1, v7, [Ljava/lang/Object;

    sget-object v3, Lcom/peel/content/a/j;->b:Ljava/lang/String;

    aput-object v3, v1, v6

    aput-object v2, v1, v8

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method private static o(Landroid/os/Bundle;Lcom/peel/util/t;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/Bundle;",
            "Lcom/peel/util/t",
            "<[",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x2

    const/4 v6, 0x0

    .line 1028
    const-string/jumbo v0, "library"

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1029
    const-string/jumbo v1, "user"

    invoke-virtual {p0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1030
    const-string/jumbo v2, "keyword"

    invoke-virtual {p0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1031
    const-string/jumbo v3, "country"

    invoke-virtual {p0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1032
    if-eqz v0, :cond_0

    if-eqz v1, :cond_0

    if-nez v2, :cond_1

    .line 1033
    :cond_0
    const/4 v0, 0x0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "exactTitleSearch missing arguments "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v6, v0, v1}, Lcom/peel/util/t;->a(ZLjava/lang/Object;Ljava/lang/String;)V

    .line 1060
    :goto_0
    return-void

    .line 1039
    :cond_1
    :try_start_0
    const-string/jumbo v0, "%sepg/schedules/searchexacttitlematchall?term=%s&country=%s"

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v4, 0x0

    sget-object v5, Lcom/peel/content/a/j;->b:Ljava/lang/String;

    aput-object v5, v1, v4

    const/4 v4, 0x1

    const-string/jumbo v5, "UTF-8"

    invoke-static {v2, v5}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v1, v4

    const/4 v4, 0x2

    const-string/jumbo v5, "UTF-8"

    invoke-static {v3, v5}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v4

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 1045
    :goto_1
    sget-object v1, Lcom/peel/content/a/j;->c:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "\n *********** exactTitleSearch 2.0 listingUrl **************\n "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1046
    new-instance v1, Lcom/peel/content/a/s;

    invoke-direct {v1, v7, p1}, Lcom/peel/content/a/s;-><init>(ILcom/peel/util/t;)V

    invoke-static {v0, v1}, Lcom/peel/util/b/a;->b(Ljava/lang/String;Lcom/peel/util/t;)V

    goto :goto_0

    .line 1040
    :catch_0
    move-exception v0

    .line 1041
    sget-object v1, Lcom/peel/content/a/j;->c:Ljava/lang/String;

    sget-object v3, Lcom/peel/content/a/j;->c:Ljava/lang/String;

    invoke-static {v1, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1042
    const-string/jumbo v0, "%sepg/schedules/searchexacttitlematchall?term=%s&country=%s"

    new-array v1, v7, [Ljava/lang/Object;

    sget-object v3, Lcom/peel/content/a/j;->b:Ljava/lang/String;

    aput-object v3, v1, v6

    aput-object v2, v1, v8

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method private static p(Landroid/os/Bundle;Lcom/peel/util/t;)V
    .locals 6

    .prologue
    .line 1098
    const-string/jumbo v0, "path"

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1099
    const-string/jumbo v1, "user"

    invoke-virtual {p0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1100
    const-string/jumbo v2, "show"

    invoke-virtual {p0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1102
    if-eqz p1, :cond_1

    if-eqz v0, :cond_0

    if-eqz v1, :cond_0

    if-nez v2, :cond_1

    .line 1103
    :cond_0
    const/4 v0, 0x0

    const/4 v1, 0x0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "updateShowInfo missing arguments "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v0, v1, v2}, Lcom/peel/util/t;->a(ZLjava/lang/Object;Ljava/lang/String;)V

    .line 1137
    :goto_0
    return-void

    .line 1107
    :cond_1
    sget-object v3, Lcom/peel/content/a/j;->c:Ljava/lang/String;

    const-string/jumbo v4, "updateShowInfo"

    new-instance v5, Lcom/peel/content/a/v;

    invoke-direct {v5, v2, v0, v1, p1}, Lcom/peel/content/a/v;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/peel/util/t;)V

    invoke-static {v3, v4, v5}, Lcom/peel/util/i;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    goto :goto_0
.end method

.method private static q(Landroid/os/Bundle;Lcom/peel/util/t;)V
    .locals 6

    .prologue
    .line 1143
    const-string/jumbo v0, "path"

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1144
    const-string/jumbo v1, "user"

    invoke-virtual {p0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1146
    const-string/jumbo v2, "genres"

    invoke-virtual {p0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1148
    if-eqz v0, :cond_0

    if-eqz v1, :cond_0

    if-nez v2, :cond_1

    .line 1149
    :cond_0
    const/4 v0, 0x0

    const/4 v1, 0x0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "updateGenres missing arguments "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v0, v1, v2}, Lcom/peel/util/t;->a(ZLjava/lang/Object;Ljava/lang/String;)V

    .line 1162
    :goto_0
    return-void

    .line 1153
    :cond_1
    sget-object v3, Lcom/peel/content/a/j;->c:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "updateGeneres: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Lcom/peel/content/a/w;

    invoke-direct {v5, v2, v0, v1, p1}, Lcom/peel/content/a/w;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/peel/util/t;)V

    invoke-static {v3, v4, v5}, Lcom/peel/util/i;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    goto :goto_0
.end method

.method private static r(Landroid/os/Bundle;Lcom/peel/util/t;)V
    .locals 8

    .prologue
    .line 1165
    const-string/jumbo v0, "user"

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1166
    const-string/jumbo v0, "prgsvcids"

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1167
    const-string/jumbo v0, "library"

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1168
    const-string/jumbo v0, "room"

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v4

    check-cast v4, Lcom/peel/data/ContentRoom;

    .line 1170
    if-eqz v3, :cond_0

    if-eqz v1, :cond_0

    if-nez v2, :cond_1

    .line 1171
    :cond_0
    sget-object v0, Lcom/peel/content/a/j;->c:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "updateChannelInfo missing arguments "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1172
    const/4 v0, 0x0

    const/4 v1, 0x0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "updateChannelInfo missing arguments "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v0, v1, v2}, Lcom/peel/util/t;->a(ZLjava/lang/Object;Ljava/lang/String;)V

    .line 1185
    :goto_0
    return-void

    .line 1175
    :cond_1
    sget-object v6, Lcom/peel/content/a/j;->c:Ljava/lang/String;

    const-string/jumbo v7, "updateChannelInfo"

    new-instance v0, Lcom/peel/content/a/x;

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, Lcom/peel/content/a/x;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/peel/data/ContentRoom;Lcom/peel/util/t;)V

    invoke-static {v6, v7, v0}, Lcom/peel/util/i;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    goto :goto_0
.end method

.method private static s(Landroid/os/Bundle;Lcom/peel/util/t;)V
    .locals 6

    .prologue
    .line 1188
    const-string/jumbo v0, "user"

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1189
    const-string/jumbo v0, "languages"

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1190
    const-string/jumbo v0, "room"

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/peel/data/ContentRoom;

    .line 1192
    if-eqz v1, :cond_0

    if-nez v2, :cond_1

    .line 1193
    :cond_0
    const/4 v0, 0x0

    const/4 v1, 0x0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "updateLanguages missing arguments "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v0, v1, v2}, Lcom/peel/util/t;->a(ZLjava/lang/Object;Ljava/lang/String;)V

    .line 1206
    :goto_0
    return-void

    .line 1197
    :cond_1
    sget-object v3, Lcom/peel/content/a/j;->c:Ljava/lang/String;

    const-string/jumbo v4, "updateLanguages"

    new-instance v5, Lcom/peel/content/a/y;

    invoke-direct {v5, v2, v1, v0, p1}, Lcom/peel/content/a/y;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/peel/data/ContentRoom;Lcom/peel/util/t;)V

    invoke-static {v3, v4, v5}, Lcom/peel/util/i;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    goto :goto_0
.end method
