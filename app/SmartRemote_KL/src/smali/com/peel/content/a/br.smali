.class Lcom/peel/content/a/br;
.super Lcom/peel/util/t;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/peel/util/t",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/peel/content/a/bq;


# direct methods
.method constructor <init>(Lcom/peel/content/a/bq;I)V
    .locals 0

    .prologue
    .line 793
    iput-object p1, p0, Lcom/peel/content/a/br;->a:Lcom/peel/content/a/bq;

    invoke-direct {p0, p2}, Lcom/peel/util/t;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(ZLjava/lang/Object;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 793
    check-cast p2, Ljava/lang/String;

    invoke-virtual {p0, p1, p2, p3}, Lcom/peel/content/a/br;->a(ZLjava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public a(ZLjava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 796
    if-nez p1, :cond_1

    .line 797
    iget-object v0, p0, Lcom/peel/content/a/br;->a:Lcom/peel/content/a/bq;

    iget-object v0, v0, Lcom/peel/content/a/bq;->b:Lcom/peel/util/t;

    if-eqz v0, :cond_0

    .line 798
    iget-object v0, p0, Lcom/peel/content/a/br;->a:Lcom/peel/content/a/bq;

    iget-object v0, v0, Lcom/peel/content/a/bq;->b:Lcom/peel/util/t;

    invoke-virtual {v0, v2, v3, p3}, Lcom/peel/util/t;->a(ZLjava/lang/Object;Ljava/lang/String;)V

    .line 846
    :cond_0
    :goto_0
    return-void

    .line 802
    :cond_1
    if-eqz p2, :cond_2

    const-string/jumbo v0, "null"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 803
    :cond_2
    iget-object v0, p0, Lcom/peel/content/a/br;->a:Lcom/peel/content/a/bq;

    iget-object v0, v0, Lcom/peel/content/a/bq;->b:Lcom/peel/util/t;

    if-eqz v0, :cond_0

    .line 804
    iget-object v0, p0, Lcom/peel/content/a/br;->a:Lcom/peel/content/a/bq;

    iget-object v0, v0, Lcom/peel/content/a/bq;->b:Lcom/peel/util/t;

    const-string/jumbo v1, "null returned from server"

    invoke-virtual {v0, v2, v3, v1}, Lcom/peel/util/t;->a(ZLjava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 808
    :cond_3
    invoke-static {p2}, Lcom/peel/content/a/j;->e(Ljava/lang/String;)Ljava/util/HashMap;

    move-result-object v0

    .line 810
    sget-object v1, Lcom/peel/content/a;->f:Lcom/peel/content/user/User;

    if-eqz v1, :cond_4

    .line 811
    iget-object v1, p0, Lcom/peel/content/a/br;->a:Lcom/peel/content/a/bq;

    iget v1, v1, Lcom/peel/content/a/bq;->c:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_5

    .line 812
    sget-object v1, Lcom/peel/content/a;->f:Lcom/peel/content/user/User;

    invoke-virtual {v1, v0}, Lcom/peel/content/user/User;->a(Ljava/util/Map;)V

    .line 818
    :cond_4
    :goto_1
    invoke-static {}, Lcom/peel/data/m;->a()Lcom/peel/data/m;

    move-result-object v1

    iget-object v2, p0, Lcom/peel/content/a/br;->a:Lcom/peel/content/a/bq;

    iget v2, v2, Lcom/peel/content/a/bq;->c:I

    new-instance v3, Lcom/peel/content/a/bs;

    invoke-direct {v3, p0, v0}, Lcom/peel/content/a/bs;-><init>(Lcom/peel/content/a/br;Ljava/util/HashMap;)V

    invoke-virtual {v1, v2, v3}, Lcom/peel/data/m;->a(ILcom/peel/util/t;)V

    .line 842
    iget-object v1, p0, Lcom/peel/content/a/br;->a:Lcom/peel/content/a/bq;

    iget-object v1, v1, Lcom/peel/content/a/bq;->b:Lcom/peel/util/t;

    if-eqz v1, :cond_0

    .line 843
    iget-object v1, p0, Lcom/peel/content/a/br;->a:Lcom/peel/content/a/bq;

    iget-object v1, v1, Lcom/peel/content/a/bq;->b:Lcom/peel/util/t;

    invoke-virtual {v1, p1, v0, p3}, Lcom/peel/util/t;->a(ZLjava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 813
    :cond_5
    iget-object v1, p0, Lcom/peel/content/a/br;->a:Lcom/peel/content/a/bq;

    iget v1, v1, Lcom/peel/content/a/bq;->c:I

    if-nez v1, :cond_4

    .line 814
    sget-object v1, Lcom/peel/content/a;->f:Lcom/peel/content/user/User;

    invoke-virtual {v1, v0}, Lcom/peel/content/user/User;->c(Ljava/util/Map;)V

    goto :goto_1
.end method
