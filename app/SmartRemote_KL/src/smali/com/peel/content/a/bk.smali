.class Lcom/peel/content/a/bk;
.super Lcom/peel/util/t;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/peel/util/t",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/peel/content/a/bj;


# direct methods
.method constructor <init>(Lcom/peel/content/a/bj;I)V
    .locals 0

    .prologue
    .line 570
    iput-object p1, p0, Lcom/peel/content/a/bk;->a:Lcom/peel/content/a/bj;

    invoke-direct {p0, p2}, Lcom/peel/util/t;-><init>(I)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 573
    iget-boolean v0, p0, Lcom/peel/content/a/bk;->i:Z

    if-nez v0, :cond_0

    .line 574
    iget-object v0, p0, Lcom/peel/content/a/bk;->a:Lcom/peel/content/a/bj;

    iget-object v0, v0, Lcom/peel/content/a/bj;->b:Lcom/peel/util/t;

    iget-object v1, p0, Lcom/peel/content/a/bk;->j:Ljava/lang/Object;

    iget-object v2, p0, Lcom/peel/content/a/bk;->k:Ljava/lang/String;

    invoke-virtual {v0, v3, v1, v2}, Lcom/peel/util/t;->a(ZLjava/lang/Object;Ljava/lang/String;)V

    .line 575
    invoke-static {}, Lcom/peel/content/a/j;->a()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "getLineup error: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/peel/content/a/bk;->k:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 684
    :goto_0
    return-void

    .line 579
    :cond_0
    iget-object v0, p0, Lcom/peel/content/a/bk;->j:Ljava/lang/Object;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/peel/content/a/bk;->j:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    const-string/jumbo v1, "null"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 580
    :cond_1
    iget-object v0, p0, Lcom/peel/content/a/bk;->a:Lcom/peel/content/a/bj;

    iget-object v0, v0, Lcom/peel/content/a/bj;->b:Lcom/peel/util/t;

    const/4 v1, 0x0

    const-string/jumbo v2, "get lineup result is null"

    invoke-virtual {v0, v3, v1, v2}, Lcom/peel/util/t;->a(ZLjava/lang/Object;Ljava/lang/String;)V

    .line 581
    invoke-static {}, Lcom/peel/content/a/j;->a()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "get lineup result is null"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 585
    :cond_2
    iget-object v0, p0, Lcom/peel/content/a/bk;->a:Lcom/peel/content/a/bj;

    iget-object v1, v0, Lcom/peel/content/a/bj;->a:Ljava/lang/String;

    iget-object v0, p0, Lcom/peel/content/a/bk;->j:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    invoke-static {v1, v0}, Lcom/peel/content/a/j;->a(Ljava/lang/String;Ljava/lang/String;)[Lcom/peel/data/Channel;

    move-result-object v0

    .line 586
    if-nez v0, :cond_3

    .line 587
    iget-object v1, p0, Lcom/peel/content/a/bk;->a:Lcom/peel/content/a/bj;

    iget-object v1, v1, Lcom/peel/content/a/bj;->b:Lcom/peel/util/t;

    const-string/jumbo v2, "unable to parse lineup json"

    invoke-virtual {v1, v3, v0, v2}, Lcom/peel/util/t;->a(ZLjava/lang/Object;Ljava/lang/String;)V

    .line 588
    invoke-static {}, Lcom/peel/content/a/j;->a()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "unable to parse lineup json"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 594
    :cond_3
    iget-object v1, p0, Lcom/peel/content/a/bk;->a:Lcom/peel/content/a/bj;

    iget-object v1, v1, Lcom/peel/content/a/bj;->c:Landroid/os/Bundle;

    new-instance v2, Lcom/peel/content/a/bl;

    const/4 v3, 0x2

    invoke-direct {v2, p0, v3, v0}, Lcom/peel/content/a/bl;-><init>(Lcom/peel/content/a/bk;I[Lcom/peel/data/Channel;)V

    invoke-static {v1, v2}, Lcom/peel/content/a/j;->e(Landroid/os/Bundle;Lcom/peel/util/t;)V

    goto :goto_0
.end method
