.class public Lcom/peel/content/a/g;
.super Ljava/lang/Object;


# static fields
.field public static final a:Lorg/codehaus/jackson/map/ObjectMapper;

.field private static final b:Ljava/lang/String;

.field private static c:Ljava/lang/String;

.field private static d:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27
    new-instance v0, Lorg/codehaus/jackson/map/ObjectMapper;

    invoke-direct {v0}, Lorg/codehaus/jackson/map/ObjectMapper;-><init>()V

    sput-object v0, Lcom/peel/content/a/g;->a:Lorg/codehaus/jackson/map/ObjectMapper;

    .line 28
    const-class v0, Lcom/peel/content/a/g;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/peel/content/a/g;->b:Ljava/lang/String;

    .line 29
    const-string/jumbo v0, "https://graph.peel.com/"

    sput-object v0, Lcom/peel/content/a/g;->c:Ljava/lang/String;

    .line 30
    const-string/jumbo v0, "https://graph.peel.com/"

    sput-object v0, Lcom/peel/content/a/g;->d:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 24
    sget-object v0, Lcom/peel/content/a/g;->b:Ljava/lang/String;

    return-object v0
.end method

.method public static final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 33
    sput-object p0, Lcom/peel/content/a/g;->c:Ljava/lang/String;

    .line 34
    sput-object p1, Lcom/peel/content/a/g;->d:Ljava/lang/String;

    .line 35
    return-void
.end method

.method static synthetic a(Ljava/lang/String;Ljava/lang/String;Lcom/peel/content/model/OVDWrapper;)V
    .locals 0

    .prologue
    .line 24
    invoke-static {p0, p1, p2}, Lcom/peel/content/a/g;->b(Ljava/lang/String;Ljava/lang/String;Lcom/peel/content/model/OVDWrapper;)V

    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;Lcom/peel/content/model/OVDWrapper;Lcom/peel/util/t;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/peel/content/model/OVDWrapper;",
            "Lcom/peel/util/t",
            "<",
            "Lcom/peel/content/model/OVDWrapper;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 66
    const-string/jumbo v0, "unknown"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 67
    const-string/jumbo p1, "Unknown"

    .line 70
    :cond_0
    if-nez p0, :cond_1

    .line 71
    const/4 v0, 0x0

    const/4 v1, 0x0

    const-string/jumbo v2, "id is NULL for getEpisodes call"

    invoke-virtual {p3, v0, v1, v2}, Lcom/peel/util/t;->a(ZLjava/lang/Object;Ljava/lang/String;)V

    .line 92
    :goto_0
    return-void

    .line 79
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/peel/content/a/g;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "/season/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lcom/peel/content/a/i;

    const/4 v2, 0x2

    invoke-direct {v1, v2, p3, p1, p2}, Lcom/peel/content/a/i;-><init>(ILcom/peel/util/t;Ljava/lang/String;Lcom/peel/content/model/OVDWrapper;)V

    invoke-static {v0, v1}, Lcom/peel/util/b/a;->b(Ljava/lang/String;Lcom/peel/util/t;)V

    goto :goto_0
.end method

.method public static a(Ljava/util/List;Lcom/peel/content/model/OVDWrapper;Lcom/peel/util/t;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/peel/content/model/OVDWrapper;",
            "Lcom/peel/util/t",
            "<",
            "Lcom/peel/content/model/OVDWrapper;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 39
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 41
    const-string/jumbo v1, "ids"

    const-string/jumbo v2, ","

    invoke-static {v2, p0}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 42
    const-string/jumbo v1, "gzip"

    const-string/jumbo v2, "true"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 43
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/peel/content/a/g;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "getShows"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/peel/content/a/h;

    const/4 v3, 0x2

    invoke-direct {v2, v3, p2, p0, p1}, Lcom/peel/content/a/h;-><init>(ILcom/peel/util/t;Ljava/util/List;Lcom/peel/content/model/OVDWrapper;)V

    invoke-static {v1, v0, v2}, Lcom/peel/util/b/a;->a(Ljava/lang/String;Ljava/util/Map;Lcom/peel/util/t;)V

    .line 62
    return-void
.end method

.method private static b(Ljava/lang/String;Ljava/lang/String;Lcom/peel/content/model/OVDWrapper;)V
    .locals 14

    .prologue
    .line 98
    :try_start_0
    new-instance v1, Ljava/util/LinkedList;

    invoke-direct {v1}, Ljava/util/LinkedList;-><init>()V

    .line 100
    sget-object v0, Lcom/peel/content/a/g;->a:Lorg/codehaus/jackson/map/ObjectMapper;

    const-class v2, Ljava/util/HashMap;

    invoke-virtual {v0, p0, v2}, Lorg/codehaus/jackson/map/ObjectMapper;->readValue(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/HashMap;

    const-string/jumbo v2, "parents"

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    .line 101
    if-eqz v0, :cond_0

    const-string/jumbo v2, "data"

    invoke-interface {v0, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 102
    const-string/jumbo v2, "data"

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 103
    :cond_0
    sget-object v0, Lcom/peel/content/a/g;->a:Lorg/codehaus/jackson/map/ObjectMapper;

    const-class v2, Ljava/util/HashMap;

    invoke-virtual {v0, p0, v2}, Lorg/codehaus/jackson/map/ObjectMapper;->readValue(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/HashMap;

    const-string/jumbo v2, "episodes"

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    .line 104
    if-eqz v0, :cond_1

    const-string/jumbo v2, "data"

    invoke-interface {v0, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 105
    const-string/jumbo v2, "data"

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 106
    :cond_1
    sget-object v0, Lcom/peel/content/a/g;->b:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "data size: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 107
    const/4 v7, 0x0

    .line 109
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :cond_2
    :goto_0
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 111
    :try_start_1
    const-string/jumbo v1, "0"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    const-string/jumbo v1, "hosts"

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 114
    :cond_3
    const-string/jumbo v1, "id"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 116
    const-string/jumbo v2, "0"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_11

    .line 119
    const-string/jumbo v2, "hosts"

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    instance-of v2, v2, Ljava/util/List;

    if-eqz v2, :cond_6

    .line 120
    const-string/jumbo v2, "hosts"

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/List;

    move-object v8, v2

    .line 126
    :goto_1
    new-instance v10, Ljava/util/HashMap;

    invoke-direct {v10}, Ljava/util/HashMap;-><init>()V

    .line 128
    invoke-interface {v8}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_a

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 129
    const/4 v3, 0x0

    new-array v4, v3, [Ljava/lang/String;

    const/4 v3, 0x0

    .line 131
    const-string/jumbo v6, ":"

    invoke-virtual {v2, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_15

    .line 132
    const-string/jumbo v4, ":"

    invoke-virtual {v2, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 135
    :goto_3
    array-length v4, v2

    const/4 v6, 0x1

    if-le v4, v6, :cond_14

    .line 136
    const/4 v3, 0x1

    aget-object v2, v2, v3

    const-string/jumbo v3, ","

    invoke-virtual {v2, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    move-object v4, v2

    .line 139
    :goto_4
    if-eqz v4, :cond_8

    .line 140
    array-length v2, v4

    add-int/lit8 v2, v2, -0x1

    move v3, v2

    :goto_5
    if-ltz v3, :cond_7

    .line 141
    aget-object v2, v4, v3

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    .line 142
    const-string/jumbo v6, "unknown"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    const-string/jumbo v2, "Unknown"

    .line 144
    :cond_4
    invoke-interface {v10, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_5

    .line 145
    new-instance v6, Lcom/peel/content/model/Season;

    invoke-direct {v6, v2}, Lcom/peel/content/model/Season;-><init>(Ljava/lang/String;)V

    invoke-interface {v10, v2, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 140
    :cond_5
    add-int/lit8 v2, v3, -0x1

    move v3, v2

    goto :goto_5

    .line 122
    :cond_6
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 123
    const-string/jumbo v2, "hosts"

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-object v8, v3

    goto :goto_1

    :cond_7
    move v2, v7

    :goto_6
    move v7, v2

    .line 154
    goto :goto_2

    .line 149
    :cond_8
    const-string/jumbo v2, "Unknown"

    invoke-interface {v10, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2

    move-result v2

    if-nez v2, :cond_13

    .line 150
    const/4 v2, 0x1

    .line 151
    :try_start_2
    const-string/jumbo v3, "Unknown"

    new-instance v4, Lcom/peel/content/model/Season;

    const-string/jumbo v6, "Unknown"

    invoke-direct {v4, v6}, Lcom/peel/content/model/Season;-><init>(Ljava/lang/String;)V

    invoke-interface {v10, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_6

    .line 209
    :catch_0
    move-exception v0

    move v7, v2

    .line 210
    :goto_7
    :try_start_3
    sget-object v1, Lcom/peel/content/a/g;->b:Ljava/lang/String;

    sget-object v2, Lcom/peel/content/a/g;->b:Ljava/lang/String;

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    goto/16 :goto_0

    .line 213
    :catch_1
    move-exception v0

    .line 214
    sget-object v1, Lcom/peel/content/a/g;->b:Ljava/lang/String;

    sget-object v2, Lcom/peel/content/a/g;->b:Ljava/lang/String;

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 216
    :cond_9
    return-void

    .line 156
    :cond_a
    :try_start_4
    sget-object v6, Lcom/peel/content/a;->a:Lcom/peel/util/bo;

    monitor-enter v6
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_2

    .line 157
    :try_start_5
    sget-object v2, Lcom/peel/content/a;->a:Lcom/peel/util/bo;

    invoke-virtual {v2, v1}, Lcom/peel/util/bo;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    if-nez v2, :cond_c

    .line 158
    if-eqz v7, :cond_b

    .line 159
    const-string/jumbo v2, "Unknown"

    invoke-interface {v10, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/peel/content/model/Season;

    .line 161
    new-instance v11, Lcom/peel/content/model/Episode;

    const-string/jumbo v3, "episode"

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    const-string/jumbo v4, "title"

    .line 162
    invoke-interface {v0, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    const-string/jumbo v5, "description"

    invoke-interface {v0, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-direct {v11, v1, v3, v4, v5}, Lcom/peel/content/model/Episode;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 161
    invoke-virtual {v2, v11}, Lcom/peel/content/model/Season;->a(Lcom/peel/content/model/Episode;)V

    .line 165
    :cond_b
    sget-object v2, Lcom/peel/content/a;->a:Lcom/peel/util/bo;

    new-instance v3, Lcom/peel/content/model/OVDWrapper;

    invoke-direct {v3, v1, v8, v10}, Lcom/peel/content/model/OVDWrapper;-><init>(Ljava/lang/String;Ljava/util/List;Ljava/util/Map;)V

    invoke-virtual {v2, v1, v3}, Lcom/peel/util/bo;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 167
    :cond_c
    monitor-exit v6
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 169
    const/4 v2, 0x0

    .line 170
    :try_start_6
    const-string/jumbo v3, "aliases"

    invoke-interface {v0, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_d

    .line 171
    const-string/jumbo v2, "aliases"

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    instance-of v2, v2, Ljava/util/List;

    if-eqz v2, :cond_f

    .line 172
    const-string/jumbo v2, "aliases"

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/List;

    .line 178
    :cond_d
    :goto_8
    if-eqz v2, :cond_2

    .line 179
    sget-object v11, Lcom/peel/content/a;->a:Lcom/peel/util/bo;

    monitor-enter v11
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_2

    .line 180
    :try_start_7
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v12

    :goto_9
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_10

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 181
    if-eqz v7, :cond_e

    .line 182
    const-string/jumbo v3, "Unknown"

    invoke-interface {v10, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/peel/content/model/Season;

    .line 184
    new-instance v13, Lcom/peel/content/model/Episode;

    const-string/jumbo v4, "episode"

    .line 185
    invoke-interface {v0, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    const-string/jumbo v5, "title"

    .line 186
    invoke-interface {v0, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    const-string/jumbo v6, "description"

    .line 187
    invoke-interface {v0, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    invoke-direct {v13, v1, v4, v5, v6}, Lcom/peel/content/model/Episode;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 184
    invoke-virtual {v3, v13}, Lcom/peel/content/model/Season;->a(Lcom/peel/content/model/Episode;)V

    .line 190
    :cond_e
    sget-object v3, Lcom/peel/content/a;->a:Lcom/peel/util/bo;

    new-instance v4, Lcom/peel/content/model/OVDWrapper;

    invoke-direct {v4, v2, v8, v10}, Lcom/peel/content/model/OVDWrapper;-><init>(Ljava/lang/String;Ljava/util/List;Ljava/util/Map;)V

    invoke-virtual {v3, v2, v4}, Lcom/peel/util/bo;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_9

    .line 193
    :catchall_0
    move-exception v0

    monitor-exit v11
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    :try_start_8
    throw v0
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_2

    .line 209
    :catch_2
    move-exception v0

    goto/16 :goto_7

    .line 167
    :catchall_1
    move-exception v0

    :try_start_9
    monitor-exit v6
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    :try_start_a
    throw v0

    .line 174
    :cond_f
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 175
    const-string/jumbo v2, "aliases"

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_2

    move-object v2, v3

    goto :goto_8

    .line 193
    :cond_10
    :try_start_b
    monitor-exit v11
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    goto/16 :goto_0

    .line 196
    :cond_11
    :try_start_c
    const-string/jumbo v2, "title"

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 197
    const-string/jumbo v3, "description"

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 198
    const-string/jumbo v4, "episode"

    invoke-interface {v0, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 200
    invoke-virtual/range {p2 .. p2}, Lcom/peel/content/model/OVDWrapper;->b()Ljava/util/Map;

    move-result-object v4

    .line 201
    invoke-interface {v4, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/peel/content/model/Season;

    .line 203
    if-eqz v4, :cond_2

    .line 204
    new-instance v5, Lcom/peel/content/model/Episode;

    if-nez v0, :cond_12

    const-string/jumbo v0, ""

    :cond_12
    invoke-direct {v5, v1, v0, v2, v3}, Lcom/peel/content/model/Episode;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v4, v5}, Lcom/peel/content/model/Season;->a(Lcom/peel/content/model/Episode;)V
    :try_end_c
    .catch Ljava/lang/Exception; {:try_start_c .. :try_end_c} :catch_2

    goto/16 :goto_0

    :cond_13
    move v2, v7

    goto/16 :goto_6

    :cond_14
    move-object v4, v3

    goto/16 :goto_4

    :cond_15
    move-object v2, v4

    goto/16 :goto_3
.end method
