.class Lcom/peel/content/a/bg;
.super Lcom/peel/util/t;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/peel/util/t",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic b:Ljava/lang/String;

.field final synthetic c:Lcom/peel/content/a/bf;


# direct methods
.method constructor <init>(Lcom/peel/content/a/bf;ILjava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 506
    iput-object p1, p0, Lcom/peel/content/a/bg;->c:Lcom/peel/content/a/bf;

    iput-object p3, p0, Lcom/peel/content/a/bg;->a:Ljava/lang/String;

    iput-object p4, p0, Lcom/peel/content/a/bg;->b:Ljava/lang/String;

    invoke-direct {p0, p2}, Lcom/peel/util/t;-><init>(I)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 509
    iget-boolean v0, p0, Lcom/peel/content/a/bg;->i:Z

    if-eqz v0, :cond_0

    const-string/jumbo v0, "null"

    iget-object v3, p0, Lcom/peel/content/a/bg;->j:Ljava/lang/Object;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 510
    :cond_0
    iget-object v0, p0, Lcom/peel/content/a/bg;->c:Lcom/peel/content/a/bf;

    iget-object v3, v0, Lcom/peel/content/a/bf;->b:Lcom/peel/util/t;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "URL: "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v4, p0, Lcom/peel/content/a/bg;->a:Ljava/lang/String;

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v4, "\nmsg: "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v4, p0, Lcom/peel/content/a/bg;->k:Ljava/lang/String;

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v4, "\nresult: "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v0, p0, Lcom/peel/content/a/bg;->j:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v2, v1, v0}, Lcom/peel/util/t;->a(ZLjava/lang/Object;Ljava/lang/String;)V

    .line 516
    :goto_0
    return-void

    .line 514
    :cond_1
    iget-object v3, p0, Lcom/peel/content/a/bg;->b:Ljava/lang/String;

    iget-object v0, p0, Lcom/peel/content/a/bg;->j:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    invoke-static {v3, v2, v0}, Lcom/peel/content/a/j;->a(Ljava/lang/String;ZLjava/lang/String;)Ljava/util/List;

    move-result-object v3

    .line 515
    iget-object v0, p0, Lcom/peel/content/a/bg;->c:Lcom/peel/content/a/bf;

    iget-object v4, v0, Lcom/peel/content/a/bf;->b:Lcom/peel/util/t;

    if-eqz v3, :cond_2

    const/4 v0, 0x1

    move v2, v0

    :cond_2
    if-eqz v3, :cond_3

    move-object v0, v1

    :goto_1
    invoke-virtual {v4, v2, v3, v0}, Lcom/peel/util/t;->a(ZLjava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "getShowTimes URL: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/content/a/bg;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "\nno listings returned from legacy after parseListings()"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method
