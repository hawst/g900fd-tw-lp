.class final Lcom/peel/content/a/bf;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Landroid/os/Bundle;

.field final synthetic b:Lcom/peel/util/t;


# direct methods
.method constructor <init>(Landroid/os/Bundle;Lcom/peel/util/t;)V
    .locals 0

    .prologue
    .line 497
    iput-object p1, p0, Lcom/peel/content/a/bf;->a:Landroid/os/Bundle;

    iput-object p2, p0, Lcom/peel/content/a/bf;->b:Lcom/peel/util/t;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 9

    .prologue
    const/4 v8, 0x2

    .line 500
    iget-object v0, p0, Lcom/peel/content/a/bf;->a:Landroid/os/Bundle;

    const-string/jumbo v1, "id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 501
    iget-object v0, p0, Lcom/peel/content/a/bf;->a:Landroid/os/Bundle;

    const-string/jumbo v2, "library"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 502
    iget-object v0, p0, Lcom/peel/content/a/bf;->a:Landroid/os/Bundle;

    const-string/jumbo v3, "user"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 503
    iget-object v0, p0, Lcom/peel/content/a/bf;->a:Landroid/os/Bundle;

    const-string/jumbo v4, "room_id"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v4

    .line 505
    invoke-static {v1}, Lcom/peel/util/eg;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "%sepg/schedules/scheduleforseriesid/%s?userid=%s&providerid=%s&roomid=%d"

    :goto_0
    const/4 v5, 0x5

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    sget-object v7, Lcom/peel/content/a/j;->b:Ljava/lang/String;

    aput-object v7, v5, v6

    const/4 v6, 0x1

    aput-object v1, v5, v6

    aput-object v3, v5, v8

    const/4 v1, 0x3

    const-string/jumbo v3, "&"

    const-string/jumbo v6, "%26"

    invoke-virtual {v2, v3, v6}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v5, v1

    const/4 v1, 0x4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v5, v1

    invoke-static {v0, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 506
    new-instance v1, Lcom/peel/content/a/bg;

    invoke-direct {v1, p0, v8, v0, v2}, Lcom/peel/content/a/bg;-><init>(Lcom/peel/content/a/bf;ILjava/lang/String;Ljava/lang/String;)V

    invoke-static {v0, v1}, Lcom/peel/util/b/a;->b(Ljava/lang/String;Lcom/peel/util/t;)V

    .line 518
    return-void

    .line 505
    :cond_0
    const-string/jumbo v0, "%sepg/schedules/scheduleforshowid/%s?userid=%s&providerid=%s&roomid=%d"

    goto :goto_0
.end method
