.class public Lcom/peel/content/a/bw;
.super Ljava/lang/Object;


# static fields
.field public static final a:Lorg/codehaus/jackson/map/ObjectMapper;

.field private static final b:Ljava/lang/String;

.field private static c:Ljava/lang/String;

.field private static d:Ljava/lang/String;

.field private static e:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 22
    new-instance v0, Lorg/codehaus/jackson/map/ObjectMapper;

    invoke-direct {v0}, Lorg/codehaus/jackson/map/ObjectMapper;-><init>()V

    sput-object v0, Lcom/peel/content/a/bw;->a:Lorg/codehaus/jackson/map/ObjectMapper;

    .line 23
    const-class v0, Lcom/peel/content/a/bw;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/peel/content/a/bw;->b:Ljava/lang/String;

    .line 24
    const-string/jumbo v0, "http://vod.peel-prod.com"

    sput-object v0, Lcom/peel/content/a/bw;->c:Ljava/lang/String;

    .line 25
    const-string/jumbo v0, "https://vod.peel-prod.com"

    sput-object v0, Lcom/peel/content/a/bw;->d:Ljava/lang/String;

    .line 26
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    sput-object v0, Lcom/peel/content/a/bw;->e:Ljava/util/HashSet;

    .line 29
    sget-object v0, Lcom/peel/content/a/bw;->e:Ljava/util/HashSet;

    const-string/jumbo v1, "amazon.com"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 30
    sget-object v0, Lcom/peel/content/a/bw;->e:Ljava/util/HashSet;

    const-string/jumbo v1, "hulu"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 31
    sget-object v0, Lcom/peel/content/a/bw;->e:Ljava/util/HashSet;

    const-string/jumbo v1, "netflix"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 32
    sget-object v0, Lcom/peel/content/a/bw;->e:Ljava/util/HashSet;

    const-string/jumbo v1, "youtube"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 33
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 20
    sget-object v0, Lcom/peel/content/a/bw;->b:Ljava/lang/String;

    return-object v0
.end method

.method public static a(Landroid/os/Bundle;Lcom/peel/util/t;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/Bundle;",
            "Lcom/peel/util/t",
            "<",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/peel/content/model/OVD;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 42
    const-string/jumbo v0, "showId"

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 43
    const-string/jumbo v1, "showName"

    invoke-virtual {p0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    .line 44
    if-nez v0, :cond_0

    .line 45
    const/4 v0, 0x0

    const/4 v1, 0x0

    const-string/jumbo v2, "showId is NULL for getVodOptions call"

    invoke-virtual {p1, v0, v1, v2}, Lcom/peel/util/t;->a(ZLjava/lang/Object;Ljava/lang/String;)V

    .line 99
    :goto_0
    return-void

    .line 49
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/peel/content/a/bw;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "/vod/options?showID="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/peel/content/a/bx;

    const/4 v3, 0x2

    invoke-direct {v2, v3, v0, p1}, Lcom/peel/content/a/bx;-><init>(ILjava/lang/String;Lcom/peel/util/t;)V

    invoke-static {v1, v2}, Lcom/peel/util/b/a;->a(Ljava/lang/String;Lcom/peel/util/t;)V

    goto :goto_0
.end method

.method static synthetic b()Ljava/util/HashSet;
    .locals 1

    .prologue
    .line 20
    sget-object v0, Lcom/peel/content/a/bw;->e:Ljava/util/HashSet;

    return-object v0
.end method
