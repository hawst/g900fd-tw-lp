.class Lcom/peel/content/a/at;
.super Lcom/peel/util/t;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/peel/util/t",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/peel/content/a/as;


# direct methods
.method constructor <init>(Lcom/peel/content/a/as;)V
    .locals 0

    .prologue
    .line 1589
    iput-object p1, p0, Lcom/peel/content/a/at;->a:Lcom/peel/content/a/as;

    invoke-direct {p0}, Lcom/peel/util/t;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 15

    .prologue
    const/4 v7, 0x0

    const/4 v4, 0x0

    .line 1592
    iget-boolean v1, p0, Lcom/peel/content/a/at;->i:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/peel/content/a/at;->j:Ljava/lang/Object;

    if-nez v1, :cond_1

    .line 1594
    :cond_0
    iget-object v1, p0, Lcom/peel/content/a/at;->a:Lcom/peel/content/a/as;

    iget-object v1, v1, Lcom/peel/content/a/as;->c:Lcom/peel/util/t;

    invoke-virtual {v1, v7, v4, v4}, Lcom/peel/util/t;->a(ZLjava/lang/Object;Ljava/lang/String;)V

    .line 1644
    :goto_0
    return-void

    .line 1603
    :cond_1
    :try_start_0
    new-instance v8, Lorg/json/JSONObject;

    iget-object v1, p0, Lcom/peel/content/a/at;->j:Ljava/lang/Object;

    check-cast v1, Ljava/lang/String;

    invoke-direct {v8, v1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 1604
    invoke-virtual {v8}, Lorg/json/JSONObject;->keys()Ljava/util/Iterator;

    move-result-object v9

    .line 1605
    new-instance v10, Ljava/util/HashMap;

    invoke-direct {v10}, Ljava/util/HashMap;-><init>()V

    move-object v2, v4

    .line 1606
    :goto_1
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 1607
    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v0, v1

    check-cast v0, Ljava/lang/String;

    move-object v3, v0

    .line 1609
    invoke-virtual {v8, v3}, Lorg/json/JSONObject;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    instance-of v1, v1, Lorg/json/JSONArray;

    if-eqz v1, :cond_3

    .line 1610
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 1613
    invoke-virtual {v8, v3}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v11

    move v6, v7

    .line 1614
    :goto_2
    invoke-virtual {v11}, Lorg/json/JSONArray;->length()I

    move-result v1

    if-ge v6, v1, :cond_5

    .line 1615
    invoke-virtual {v11, v6}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v12

    .line 1617
    invoke-virtual {v12}, Lorg/json/JSONObject;->keys()Ljava/util/Iterator;

    move-result-object v13

    .line 1618
    new-instance v14, Ljava/util/HashMap;

    invoke-direct {v14}, Ljava/util/HashMap;-><init>()V

    .line 1620
    :goto_3
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1623
    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 1626
    invoke-virtual {v12, v1}, Lorg/json/JSONObject;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v14, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_3

    .line 1638
    :catch_0
    move-exception v1

    .line 1639
    invoke-static {}, Lcom/peel/content/a/j;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {}, Lcom/peel/content/a/j;->a()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1642
    iget-object v1, p0, Lcom/peel/content/a/at;->a:Lcom/peel/content/a/as;

    iget-object v1, v1, Lcom/peel/content/a/as;->c:Lcom/peel/util/t;

    invoke-virtual {v1, v7, v4, v4}, Lcom/peel/util/t;->a(ZLjava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 1629
    :cond_2
    :try_start_1
    const-string/jumbo v1, "isFavorite"

    const-string/jumbo v2, "false"

    invoke-virtual {v14, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1630
    invoke-virtual {v5, v14}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1614
    add-int/lit8 v1, v6, 0x1

    move v6, v1

    goto :goto_2

    :cond_3
    move-object v1, v2

    .line 1635
    :goto_4
    invoke-virtual {v10, v3, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object v2, v1

    .line 1636
    goto :goto_1

    .line 1637
    :cond_4
    iget-object v1, p0, Lcom/peel/content/a/at;->a:Lcom/peel/content/a/as;

    iget-object v1, v1, Lcom/peel/content/a/as;->c:Lcom/peel/util/t;

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v10, v3}, Lcom/peel/util/t;->a(ZLjava/lang/Object;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0

    :cond_5
    move-object v1, v5

    goto :goto_4
.end method
