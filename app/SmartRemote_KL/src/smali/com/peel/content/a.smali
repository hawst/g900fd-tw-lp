.class public final Lcom/peel/content/a;
.super Ljava/lang/Object;


# static fields
.field public static final a:Lcom/peel/util/bo;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/peel/util/bo",
            "<",
            "Ljava/lang/String;",
            "Lcom/peel/content/model/OVDWrapper;",
            ">;"
        }
    .end annotation
.end field

.field public static final b:Lcom/peel/content/i;

.field public static c:Z

.field public static d:Z

.field public static e:Ljava/lang/String;

.field public static f:Lcom/peel/content/user/User;

.field public static g:Lcom/peel/content/user/User;

.field public static h:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private static final i:Ljava/lang/String;

.field private static j:Ljava/lang/String;

.field private static k:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/peel/content/library/Library;",
            ">;"
        }
    .end annotation
.end field

.field private static l:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 32
    new-instance v0, Lcom/peel/util/bo;

    const/16 v1, 0x100

    invoke-direct {v0, v1}, Lcom/peel/util/bo;-><init>(I)V

    sput-object v0, Lcom/peel/content/a;->a:Lcom/peel/util/bo;

    .line 34
    new-instance v0, Lcom/peel/content/i;

    invoke-direct {v0}, Lcom/peel/content/i;-><init>()V

    sput-object v0, Lcom/peel/content/a;->b:Lcom/peel/content/i;

    .line 35
    const-class v0, Lcom/peel/content/a;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/peel/content/a;->i:Ljava/lang/String;

    .line 36
    sput-boolean v2, Lcom/peel/content/a;->c:Z

    .line 37
    sput-boolean v2, Lcom/peel/content/a;->d:Z

    .line 38
    const-string/jumbo v0, "US"

    sput-object v0, Lcom/peel/content/a;->e:Ljava/lang/String;

    .line 42
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    sput-object v0, Lcom/peel/content/a;->h:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 43
    const-string/jumbo v0, ""

    sput-object v0, Lcom/peel/content/a;->j:Ljava/lang/String;

    .line 46
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/peel/content/a;->k:Ljava/util/List;

    .line 47
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/peel/content/a;->l:Ljava/util/Map;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 610
    return-void
.end method

.method public static a()Lcom/peel/data/ContentRoom;
    .locals 8

    .prologue
    const/4 v0, 0x0

    .line 216
    sget-object v1, Lcom/peel/content/a;->j:Ljava/lang/String;

    if-eqz v1, :cond_0

    sget-object v1, Lcom/peel/content/a;->f:Lcom/peel/content/user/User;

    if-eqz v1, :cond_0

    sget-object v1, Lcom/peel/content/a;->j:Ljava/lang/String;

    const-string/jumbo v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 225
    :cond_0
    :goto_0
    return-object v0

    .line 218
    :cond_1
    sget-object v3, Lcom/peel/content/a;->j:Ljava/lang/String;

    monitor-enter v3

    .line 219
    :try_start_0
    sget-object v1, Lcom/peel/content/a;->f:Lcom/peel/content/user/User;

    invoke-virtual {v1}, Lcom/peel/content/user/User;->l()[Lcom/peel/data/ContentRoom;

    move-result-object v4

    .line 220
    if-nez v4, :cond_2

    monitor-exit v3

    goto :goto_0

    .line 226
    :catchall_0
    move-exception v0

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 221
    :cond_2
    :try_start_1
    array-length v5, v4

    const/4 v1, 0x0

    move v2, v1

    :goto_1
    if-ge v2, v5, :cond_4

    aget-object v1, v4, v2

    .line 222
    invoke-virtual {v1}, Lcom/peel/data/ContentRoom;->a()Ljava/lang/String;

    move-result-object v6

    sget-object v7, Lcom/peel/content/a;->j:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_3

    .line 221
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_1

    .line 223
    :cond_3
    monitor-exit v3

    move-object v0, v1

    goto :goto_0

    .line 225
    :cond_4
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method public static declared-synchronized a(Lcom/peel/content/library/Library;Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 267
    const-class v1, Lcom/peel/content/a;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/peel/content/a;->k:Ljava/util/List;

    invoke-interface {v0, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 269
    sget-object v0, Lcom/peel/content/a;->f:Lcom/peel/content/user/User;

    invoke-virtual {v0}, Lcom/peel/content/user/User;->l()[Lcom/peel/data/ContentRoom;

    move-result-object v2

    .line 270
    array-length v3, v2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_1

    aget-object v4, v2, v0

    .line 271
    invoke-virtual {v4}, Lcom/peel/data/ContentRoom;->a()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 270
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 273
    :cond_0
    invoke-virtual {p0}, Lcom/peel/content/library/Library;->e()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Lcom/peel/data/ContentRoom;->b(Ljava/lang/String;)V

    .line 277
    :cond_1
    invoke-static {}, Lcom/peel/data/m;->a()Lcom/peel/data/m;

    move-result-object v0

    new-instance v2, Lcom/peel/data/k;

    invoke-virtual {p0}, Lcom/peel/content/library/Library;->f()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0}, Lcom/peel/content/library/Library;->e()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0}, Lcom/peel/content/library/Library;->a()Landroid/os/Bundle;

    move-result-object v5

    invoke-direct {v2, v3, v4, v5}, Lcom/peel/data/k;-><init>(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)V

    invoke-virtual {v0, v2}, Lcom/peel/data/m;->a(Lcom/peel/data/k;)V

    .line 279
    sget-object v0, Lcom/peel/content/a;->f:Lcom/peel/content/user/User;

    invoke-virtual {v0}, Lcom/peel/content/user/User;->u()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 280
    monitor-exit v1

    return-void

    .line 267
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static a(Lcom/peel/content/model/OVDWrapper;Ljava/lang/String;Lcom/peel/util/t;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/peel/content/model/OVDWrapper;",
            "Ljava/lang/String;",
            "Lcom/peel/util/t",
            "<",
            "Lcom/peel/content/model/OVDWrapper;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 547
    sget-object v0, Lcom/peel/content/a;->i:Ljava/lang/String;

    const-string/jumbo v1, "get episode list"

    new-instance v2, Lcom/peel/content/h;

    invoke-direct {v2, p0, p1, p2}, Lcom/peel/content/h;-><init>(Lcom/peel/content/model/OVDWrapper;Ljava/lang/String;Lcom/peel/util/t;)V

    invoke-static {v0, v1, v2}, Lcom/peel/util/i;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    .line 554
    return-void
.end method

.method public static a(Ljava/lang/String;)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 50
    sget-object v2, Lcom/peel/content/a;->h:Ljava/util/concurrent/atomic/AtomicBoolean;

    monitor-enter v2

    .line 51
    :try_start_0
    sget-object v0, Lcom/peel/content/a;->h:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 52
    sget-object v1, Lcom/peel/content/a;->b:Lcom/peel/content/i;

    const/4 v3, 0x1

    const/4 v4, 0x0

    const/4 v0, 0x0

    check-cast v0, [Ljava/lang/Object;

    invoke-virtual {v1, v3, v4, v0}, Lcom/peel/content/i;->a(ILjava/lang/Object;[Ljava/lang/Object;)V

    .line 53
    monitor-exit v2

    .line 80
    :goto_0
    return-void

    .line 55
    :cond_0
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 57
    sget-object v2, Lcom/peel/content/a;->b:Lcom/peel/content/i;

    const/4 v3, 0x0

    move-object v0, v1

    check-cast v0, [Ljava/lang/Object;

    invoke-virtual {v2, v3, v1, v0}, Lcom/peel/content/i;->a(ILjava/lang/Object;[Ljava/lang/Object;)V

    .line 58
    sget-object v0, Lcom/peel/content/a;->i:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "loading room "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/peel/content/b;

    invoke-direct {v2, p0}, Lcom/peel/content/b;-><init>(Ljava/lang/String;)V

    invoke-static {v0, v1, v2}, Lcom/peel/util/i;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/lang/Runnable;

    goto :goto_0

    .line 55
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public static declared-synchronized a(Ljava/lang/String;Lcom/peel/util/t;)V
    .locals 4

    .prologue
    .line 348
    const-class v1, Lcom/peel/content/a;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/peel/content/a;->j:Ljava/lang/String;

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 349
    const/4 v0, 0x1

    sget-object v2, Lcom/peel/content/a;->k:Ljava/util/List;

    sget-object v3, Lcom/peel/content/a;->k:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    new-array v3, v3, [Lcom/peel/content/library/Library;

    invoke-interface {v2, v3}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {p1, v0, v2, v3}, Lcom/peel/util/t;->a(ZLjava/lang/Object;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 407
    :goto_0
    monitor-exit v1

    return-void

    .line 353
    :cond_0
    :try_start_1
    sget-object v0, Lcom/peel/content/a;->i:Ljava/lang/String;

    const-string/jumbo v2, "grab libs from db"

    new-instance v3, Lcom/peel/content/d;

    invoke-direct {v3, p0, p1}, Lcom/peel/content/d;-><init>(Ljava/lang/String;Lcom/peel/util/t;)V

    invoke-static {v0, v2, v3}, Lcom/peel/util/i;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/lang/Runnable;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 348
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static declared-synchronized a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 283
    const-class v1, Lcom/peel/content/a;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/peel/content/a;->k:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    .line 333
    :goto_0
    monitor-exit v1

    return-void

    .line 285
    :cond_0
    :try_start_1
    invoke-static {}, Lcom/peel/content/a;->a()Lcom/peel/data/ContentRoom;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-static {}, Lcom/peel/content/a;->a()Lcom/peel/data/ContentRoom;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/data/ContentRoom;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 286
    sget-object v0, Lcom/peel/content/a;->k:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/content/library/Library;

    .line 287
    invoke-virtual {v0}, Lcom/peel/content/library/Library;->e()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 288
    sget-object v2, Lcom/peel/content/a;->k:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 294
    :cond_2
    sget-object v0, Lcom/peel/content/a;->f:Lcom/peel/content/user/User;

    invoke-virtual {v0}, Lcom/peel/content/user/User;->l()[Lcom/peel/data/ContentRoom;

    move-result-object v2

    .line 295
    array-length v3, v2

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v3, :cond_4

    aget-object v4, v2, v0

    .line 296
    invoke-virtual {v4}, Lcom/peel/data/ContentRoom;->a()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_3

    .line 295
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 298
    :cond_3
    invoke-virtual {v4, p0}, Lcom/peel/data/ContentRoom;->c(Ljava/lang/String;)V

    .line 302
    :cond_4
    sget-object v0, Lcom/peel/content/a;->f:Lcom/peel/content/user/User;

    invoke-virtual {v0}, Lcom/peel/content/user/User;->d()Landroid/os/Bundle;

    move-result-object v0

    .line 303
    if-eqz v0, :cond_5

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->remove(Ljava/lang/String;)V

    .line 305
    :cond_5
    sget-object v0, Lcom/peel/content/a;->f:Lcom/peel/content/user/User;

    invoke-virtual {v0}, Lcom/peel/content/user/User;->c()Landroid/os/Bundle;

    move-result-object v0

    .line 306
    if-eqz v0, :cond_6

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->remove(Ljava/lang/String;)V

    .line 308
    :cond_6
    sget-object v0, Lcom/peel/content/a;->f:Lcom/peel/content/user/User;

    invoke-virtual {v0}, Lcom/peel/content/user/User;->g()Landroid/os/Bundle;

    move-result-object v0

    .line 309
    if-eqz v0, :cond_7

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->remove(Ljava/lang/String;)V

    .line 311
    :cond_7
    sget-object v0, Lcom/peel/content/a;->f:Lcom/peel/content/user/User;

    invoke-virtual {v0}, Lcom/peel/content/user/User;->f()Landroid/os/Bundle;

    move-result-object v0

    .line 312
    if-eqz v0, :cond_8

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->remove(Ljava/lang/String;)V

    .line 317
    :cond_8
    sget-object v0, Lcom/peel/content/a;->f:Lcom/peel/content/user/User;

    invoke-virtual {v0}, Lcom/peel/content/user/User;->i()Landroid/os/Bundle;

    move-result-object v0

    .line 318
    if-eqz v0, :cond_9

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->remove(Ljava/lang/String;)V

    .line 320
    :cond_9
    sget-object v0, Lcom/peel/content/a;->f:Lcom/peel/content/user/User;

    invoke-virtual {v0}, Lcom/peel/content/user/User;->e()Landroid/os/Bundle;

    move-result-object v2

    .line 321
    if-eqz v2, :cond_b

    .line 322
    invoke-virtual {v2}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_a
    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_b

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 323
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_a

    .line 324
    invoke-virtual {v2, v0}, Landroid/os/Bundle;->remove(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2

    .line 283
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 332
    :cond_b
    :try_start_2
    sget-object v0, Lcom/peel/content/a;->f:Lcom/peel/content/user/User;

    invoke-virtual {v0}, Lcom/peel/content/user/User;->u()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_0
.end method

.method public static a(Ljava/lang/String;Z)V
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 230
    sget-object v1, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v1}, Lcom/peel/control/am;->c()[Lcom/peel/control/RoomControl;

    move-result-object v1

    .line 231
    sget-object v2, Lcom/peel/content/a;->j:Ljava/lang/String;

    invoke-virtual {v2, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    if-eqz p1, :cond_0

    array-length v1, v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 232
    sget-object v0, Lcom/peel/content/a;->i:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "can\'t remove current room "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 257
    :goto_0
    return-void

    .line 237
    :cond_0
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    .line 238
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 239
    sget-object v1, Lcom/peel/content/a;->f:Lcom/peel/content/user/User;

    invoke-virtual {v1}, Lcom/peel/content/user/User;->l()[Lcom/peel/data/ContentRoom;

    move-result-object v4

    move v1, v0

    .line 240
    :goto_1
    array-length v5, v4

    if-ge v0, v5, :cond_2

    .line 241
    aget-object v5, v4, v0

    .line 242
    invoke-virtual {v5}, Lcom/peel/data/ContentRoom;->a()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_1

    .line 243
    invoke-interface {v3, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 244
    invoke-virtual {v5}, Lcom/peel/data/ContentRoom;->f()Ljava/util/List;

    move-result-object v5

    invoke-interface {v2, v5}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 240
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    move v1, v0

    .line 246
    goto :goto_2

    .line 251
    :cond_2
    aget-object v0, v4, v1

    invoke-virtual {v0}, Lcom/peel/data/ContentRoom;->f()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_3
    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 252
    invoke-interface {v2, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_3

    invoke-static {v0, p0}, Lcom/peel/content/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    .line 255
    :cond_4
    sget-object v0, Lcom/peel/content/a;->f:Lcom/peel/content/user/User;

    invoke-virtual {v0, v3}, Lcom/peel/content/user/User;->a(Ljava/util/List;)V

    .line 256
    sget-object v0, Lcom/peel/content/a;->f:Lcom/peel/content/user/User;

    invoke-virtual {v0}, Lcom/peel/content/user/User;->u()V

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;ZZLcom/peel/util/t;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "ZZ",
            "Lcom/peel/util/t",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 84
    sget-object v0, Lcom/peel/content/a;->k:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/peel/content/a;->k:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 85
    :cond_0
    if-nez p1, :cond_4

    .line 86
    sget-object v0, Lcom/peel/content/a;->f:Lcom/peel/content/user/User;

    invoke-virtual {v0}, Lcom/peel/content/user/User;->l()[Lcom/peel/data/ContentRoom;

    move-result-object v3

    array-length v4, v3

    move v0, v2

    :goto_0
    if-ge v0, v4, :cond_2

    aget-object v2, v3, v0

    .line 87
    invoke-virtual {v2}, Lcom/peel/data/ContentRoom;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 86
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 88
    :cond_1
    sget-object v2, Lcom/peel/content/a;->j:Ljava/lang/String;

    monitor-enter v2

    .line 89
    :try_start_0
    sput-object p0, Lcom/peel/content/a;->j:Ljava/lang/String;

    .line 90
    monitor-exit v2

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 93
    :cond_2
    if-eqz p3, :cond_3

    const/4 v0, 0x1

    sget-object v2, Lcom/peel/content/a;->j:Ljava/lang/String;

    invoke-virtual {p3, v0, v2, v1}, Lcom/peel/util/t;->a(ZLjava/lang/Object;Ljava/lang/String;)V

    .line 213
    :cond_3
    :goto_2
    return-void

    .line 97
    :cond_4
    sget-object v3, Lcom/peel/content/a;->h:Ljava/util/concurrent/atomic/AtomicBoolean;

    monitor-enter v3

    .line 98
    :try_start_1
    sget-object v0, Lcom/peel/content/a;->h:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v4, 0x0

    invoke-virtual {v0, v4}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 99
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 100
    sget-object v3, Lcom/peel/content/a;->b:Lcom/peel/content/i;

    move-object v0, v1

    check-cast v0, [Ljava/lang/Object;

    invoke-virtual {v3, v2, v1, v0}, Lcom/peel/content/i;->a(ILjava/lang/Object;[Ljava/lang/Object;)V

    .line 101
    new-instance v0, Lcom/peel/content/c;

    invoke-direct {v0, p0, p2, p3}, Lcom/peel/content/c;-><init>(Ljava/lang/String;ZLcom/peel/util/t;)V

    .line 208
    invoke-static {}, Lcom/peel/util/i;->d()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 209
    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    goto :goto_2

    .line 99
    :catchall_1
    move-exception v0

    :try_start_2
    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0

    .line 211
    :cond_5
    sget-object v1, Lcom/peel/content/a;->i:Ljava/lang/String;

    const-string/jumbo v2, "set current room"

    invoke-static {v1, v2, v0}, Lcom/peel/util/i;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/lang/Runnable;

    goto :goto_2
.end method

.method public static declared-synchronized b(Ljava/lang/String;)Lcom/peel/content/library/Library;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 336
    const-class v2, Lcom/peel/content/a;

    monitor-enter v2

    :try_start_0
    sget-object v0, Lcom/peel/content/a;->k:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    move-object v0, v1

    .line 344
    :goto_0
    monitor-exit v2

    return-object v0

    .line 338
    :cond_0
    :try_start_1
    sget-object v0, Lcom/peel/content/a;->k:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/content/library/Library;

    .line 339
    invoke-virtual {v0}, Lcom/peel/content/library/Library;->e()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v4

    if-eqz v4, :cond_1

    goto :goto_0

    :cond_2
    move-object v0, v1

    .line 344
    goto :goto_0

    .line 336
    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0
.end method

.method public static b()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 260
    sget-object v0, Lcom/peel/content/a;->f:Lcom/peel/content/user/User;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/peel/content/a;->f:Lcom/peel/content/user/User;

    invoke-virtual {v0, v1}, Lcom/peel/content/user/User;->b(Ljava/lang/String;)V

    .line 261
    :cond_0
    sput-object v1, Lcom/peel/content/a;->g:Lcom/peel/content/user/User;

    .line 262
    sput-object v1, Lcom/peel/content/a;->f:Lcom/peel/content/user/User;

    .line 263
    sget-object v0, Lcom/peel/content/a;->k:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 264
    return-void
.end method

.method public static b(Ljava/lang/String;Lcom/peel/util/t;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/peel/util/t",
            "<[[",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v8, 0x0

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 430
    const-string/jumbo v0, "sports"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 431
    const/4 v0, 0x7

    new-array v0, v0, [[Ljava/lang/String;

    new-array v1, v7, [Ljava/lang/String;

    const-string/jumbo v2, "Football"

    aput-object v2, v1, v5

    const-string/jumbo v2, "111"

    aput-object v2, v1, v4

    const-string/jumbo v2, "1"

    aput-object v2, v1, v6

    aput-object v1, v0, v5

    new-array v1, v7, [Ljava/lang/String;

    const-string/jumbo v2, "Baseball"

    aput-object v2, v1, v5

    const-string/jumbo v2, "58"

    aput-object v2, v1, v4

    const-string/jumbo v2, "2"

    aput-object v2, v1, v6

    aput-object v1, v0, v4

    new-array v1, v7, [Ljava/lang/String;

    const-string/jumbo v2, "Basketball"

    aput-object v2, v1, v5

    const-string/jumbo v2, "59"

    aput-object v2, v1, v4

    const-string/jumbo v2, "3"

    aput-object v2, v1, v6

    aput-object v1, v0, v6

    new-array v1, v7, [Ljava/lang/String;

    const-string/jumbo v2, "Hockey"

    aput-object v2, v1, v5

    const-string/jumbo v2, "122"

    aput-object v2, v1, v4

    const-string/jumbo v2, "4"

    aput-object v2, v1, v6

    aput-object v1, v0, v7

    const/4 v1, 0x4

    new-array v2, v7, [Ljava/lang/String;

    const-string/jumbo v3, "Soccer"

    aput-object v3, v2, v5

    const-string/jumbo v3, "199"

    aput-object v3, v2, v4

    const-string/jumbo v3, "5"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/4 v1, 0x5

    new-array v2, v7, [Ljava/lang/String;

    const-string/jumbo v3, "News & Talk"

    aput-object v3, v2, v5

    const-string/jumbo v3, "156"

    aput-object v3, v2, v4

    const-string/jumbo v3, "6"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/4 v1, 0x6

    new-array v2, v7, [Ljava/lang/String;

    const-string/jumbo v3, "Other"

    aput-object v3, v2, v5

    const-string/jumbo v3, "1"

    aput-object v3, v2, v4

    const-string/jumbo v3, "7"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    invoke-virtual {p1, v4, v0, v8}, Lcom/peel/util/t;->a(ZLjava/lang/Object;Ljava/lang/String;)V

    .line 470
    :goto_0
    return-void

    .line 436
    :cond_0
    const-string/jumbo v0, "movie"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string/jumbo v0, "program"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 437
    :cond_1
    const/16 v0, 0x1d

    new-array v0, v0, [[Ljava/lang/String;

    new-array v1, v7, [Ljava/lang/String;

    const-string/jumbo v2, "Broadcast Channels"

    aput-object v2, v1, v5

    const-string/jumbo v2, "0"

    aput-object v2, v1, v4

    const-string/jumbo v2, "0"

    aput-object v2, v1, v6

    aput-object v1, v0, v5

    new-array v1, v7, [Ljava/lang/String;

    const-string/jumbo v2, "First Run"

    aput-object v2, v1, v5

    const-string/jumbo v2, "1"

    aput-object v2, v1, v4

    const-string/jumbo v2, "1"

    aput-object v2, v1, v6

    aput-object v1, v0, v4

    new-array v1, v7, [Ljava/lang/String;

    const-string/jumbo v2, "Comedy"

    aput-object v2, v1, v5

    const-string/jumbo v2, "4"

    aput-object v2, v1, v4

    const-string/jumbo v2, "2"

    aput-object v2, v1, v6

    aput-object v1, v0, v6

    new-array v1, v7, [Ljava/lang/String;

    const-string/jumbo v2, "Drama"

    aput-object v2, v1, v5

    const-string/jumbo v2, "5"

    aput-object v2, v1, v4

    const-string/jumbo v2, "3"

    aput-object v2, v1, v6

    aput-object v1, v0, v7

    const/4 v1, 0x4

    new-array v2, v7, [Ljava/lang/String;

    const-string/jumbo v3, "Kids"

    aput-object v3, v2, v5

    const-string/jumbo v3, "78"

    aput-object v3, v2, v4

    const-string/jumbo v3, "4"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/4 v1, 0x5

    new-array v2, v7, [Ljava/lang/String;

    const-string/jumbo v3, "Action & Adventure"

    aput-object v3, v2, v5

    const-string/jumbo v3, "1"

    aput-object v3, v2, v4

    const-string/jumbo v3, "5"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/4 v1, 0x6

    new-array v2, v7, [Ljava/lang/String;

    const-string/jumbo v3, "Sci-Fi & Fantasy"

    aput-object v3, v2, v5

    const-string/jumbo v3, "11"

    aput-object v3, v2, v4

    const-string/jumbo v3, "6"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/4 v1, 0x7

    new-array v2, v7, [Ljava/lang/String;

    const-string/jumbo v3, "News"

    aput-object v3, v2, v5

    const-string/jumbo v3, "156"

    aput-object v3, v2, v4

    const-string/jumbo v3, "7"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x8

    new-array v2, v7, [Ljava/lang/String;

    const-string/jumbo v3, "Animation"

    aput-object v3, v2, v5

    const-string/jumbo v3, "44"

    aput-object v3, v2, v4

    const-string/jumbo v3, "8"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x9

    new-array v2, v7, [Ljava/lang/String;

    const-string/jumbo v3, "Biography"

    aput-object v3, v2, v5

    const-string/jumbo v3, "175"

    aput-object v3, v2, v4

    const-string/jumbo v3, "9"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0xa

    new-array v2, v7, [Ljava/lang/String;

    const-string/jumbo v3, "Business"

    aput-object v3, v2, v5

    const-string/jumbo v3, "74"

    aput-object v3, v2, v4

    const-string/jumbo v3, "10"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0xb

    new-array v2, v7, [Ljava/lang/String;

    const-string/jumbo v3, "Documentary"

    aput-object v3, v2, v5

    const-string/jumbo v3, "8"

    aput-object v3, v2, v4

    const-string/jumbo v3, "11"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0xc

    new-array v2, v7, [Ljava/lang/String;

    const-string/jumbo v3, "Entertainment"

    aput-object v3, v2, v5

    const-string/jumbo v3, "100"

    aput-object v3, v2, v4

    const-string/jumbo v3, "12"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0xd

    new-array v2, v7, [Ljava/lang/String;

    const-string/jumbo v3, "Foreign"

    aput-object v3, v2, v5

    const-string/jumbo v3, "112"

    aput-object v3, v2, v4

    const-string/jumbo v3, "13"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0xe

    new-array v2, v7, [Ljava/lang/String;

    const-string/jumbo v3, "Game show"

    aput-object v3, v2, v5

    const-string/jumbo v3, "115"

    aput-object v3, v2, v4

    const-string/jumbo v3, "14"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0xf

    new-array v2, v7, [Ljava/lang/String;

    const-string/jumbo v3, "Holiday"

    aput-object v3, v2, v5

    const-string/jumbo v3, "176"

    aput-object v3, v2, v4

    const-string/jumbo v3, "15"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x10

    new-array v2, v7, [Ljava/lang/String;

    const-string/jumbo v3, "Lifestyle"

    aput-object v3, v2, v5

    const-string/jumbo v3, "87"

    aput-object v3, v2, v4

    const-string/jumbo v3, "16"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x11

    new-array v2, v7, [Ljava/lang/String;

    const-string/jumbo v3, "Music & Dance"

    aput-object v3, v2, v5

    const-string/jumbo v3, "14"

    aput-object v3, v2, v4

    const-string/jumbo v3, "17"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x12

    new-array v2, v7, [Ljava/lang/String;

    const-string/jumbo v3, "NonFiction"

    aput-object v3, v2, v5

    const-string/jumbo v3, "3"

    aput-object v3, v2, v4

    const-string/jumbo v3, "18"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x13

    new-array v2, v7, [Ljava/lang/String;

    const-string/jumbo v3, "Reality"

    aput-object v3, v2, v5

    const-string/jumbo v3, "174"

    aput-object v3, v2, v4

    const-string/jumbo v3, "19"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x14

    new-array v2, v7, [Ljava/lang/String;

    const-string/jumbo v3, "Soap"

    aput-object v3, v2, v5

    const-string/jumbo v3, "177"

    aput-object v3, v2, v4

    const-string/jumbo v3, "20"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x15

    new-array v2, v7, [Ljava/lang/String;

    const-string/jumbo v3, "Special"

    aput-object v3, v2, v5

    const-string/jumbo v3, "128"

    aput-object v3, v2, v4

    const-string/jumbo v3, "21"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x16

    new-array v2, v7, [Ljava/lang/String;

    const-string/jumbo v3, "Talk"

    aput-object v3, v2, v5

    const-string/jumbo v3, "137"

    aput-object v3, v2, v4

    const-string/jumbo v3, "22"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x17

    new-array v2, v7, [Ljava/lang/String;

    const-string/jumbo v3, "Thriller"

    aput-object v3, v2, v5

    const-string/jumbo v3, "19"

    aput-object v3, v2, v4

    const-string/jumbo v3, "23"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x18

    new-array v2, v7, [Ljava/lang/String;

    const-string/jumbo v3, "Variety"

    aput-object v3, v2, v5

    const-string/jumbo v3, "178"

    aput-object v3, v2, v4

    const-string/jumbo v3, "24"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x19

    new-array v2, v7, [Ljava/lang/String;

    const-string/jumbo v3, "War"

    aput-object v3, v2, v5

    const-string/jumbo v3, "20"

    aput-object v3, v2, v4

    const-string/jumbo v3, "25"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    new-array v2, v7, [Ljava/lang/String;

    const-string/jumbo v3, "Western"

    aput-object v3, v2, v5

    const-string/jumbo v3, "21"

    aput-object v3, v2, v4

    const-string/jumbo v3, "26"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    new-array v2, v7, [Ljava/lang/String;

    const-string/jumbo v3, "Event"

    aput-object v3, v2, v5

    const-string/jumbo v3, "55"

    aput-object v3, v2, v4

    const-string/jumbo v3, "27"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    new-array v2, v7, [Ljava/lang/String;

    const-string/jumbo v3, "Other"

    aput-object v3, v2, v5

    const-string/jumbo v3, "41"

    aput-object v3, v2, v4

    const-string/jumbo v3, "28"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    invoke-virtual {p1, v4, v0, v8}, Lcom/peel/util/t;->a(ZLjava/lang/Object;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 469
    :cond_2
    const-string/jumbo v0, "invalid genre type requested"

    invoke-virtual {p1, v5, v8, v0}, Lcom/peel/util/t;->a(ZLjava/lang/Object;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method public static declared-synchronized c(Ljava/lang/String;)Lcom/peel/content/library/Library;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/peel/content/library/Library;",
            ">(",
            "Ljava/lang/String;",
            ")TT;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 410
    const-class v2, Lcom/peel/content/a;

    monitor-enter v2

    :try_start_0
    sget-object v0, Lcom/peel/content/a;->k:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    move-object v0, v1

    .line 417
    :goto_0
    monitor-exit v2

    return-object v0

    .line 413
    :cond_0
    :try_start_1
    sget-object v0, Lcom/peel/content/a;->k:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/content/library/Library;

    .line 414
    invoke-virtual {v0}, Lcom/peel/content/library/Library;->f()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v4

    if-eqz v4, :cond_1

    goto :goto_0

    :cond_2
    move-object v0, v1

    .line 417
    goto :goto_0

    .line 410
    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0
.end method

.method public static c(Ljava/lang/String;Lcom/peel/util/t;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/peel/util/t",
            "<",
            "Lcom/peel/content/model/OVDWrapper;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 515
    sget-object v1, Lcom/peel/content/a;->a:Lcom/peel/util/bo;

    monitor-enter v1

    .line 516
    :try_start_0
    sget-object v0, Lcom/peel/content/a;->a:Lcom/peel/util/bo;

    invoke-virtual {v0, p0}, Lcom/peel/util/bo;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 517
    const/4 v0, 0x1

    sget-object v2, Lcom/peel/content/a;->a:Lcom/peel/util/bo;

    invoke-virtual {v2, p0}, Lcom/peel/util/bo;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {p1, v0, v2, v3}, Lcom/peel/util/t;->a(ZLjava/lang/Object;Ljava/lang/String;)V

    .line 518
    monitor-exit v1

    .line 543
    :goto_0
    return-void

    .line 520
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 522
    sget-object v0, Lcom/peel/content/a;->i:Ljava/lang/String;

    const-string/jumbo v1, "get graph info"

    new-instance v2, Lcom/peel/content/e;

    invoke-direct {v2, p0, p1}, Lcom/peel/content/e;-><init>(Ljava/lang/String;Lcom/peel/util/t;)V

    invoke-static {v0, v1, v2}, Lcom/peel/util/i;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    goto :goto_0

    .line 520
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public static declared-synchronized c()[Lcom/peel/content/library/Library;
    .locals 3

    .prologue
    .line 421
    const-class v1, Lcom/peel/content/a;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/peel/content/a;->k:Ljava/util/List;

    sget-object v2, Lcom/peel/content/a;->k:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    new-array v2, v2, [Lcom/peel/content/library/Library;

    invoke-interface {v0, v2}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/peel/content/library/Library;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static d()Ljava/lang/String;
    .locals 9

    .prologue
    .line 557
    sget-object v0, Lcom/peel/content/a;->f:Lcom/peel/content/user/User;

    if-nez v0, :cond_0

    const-string/jumbo v0, ""

    .line 580
    :goto_0
    return-object v0

    .line 559
    :cond_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 560
    sget-object v0, Lcom/peel/content/a;->f:Lcom/peel/content/user/User;

    invoke-virtual {v0}, Lcom/peel/content/user/User;->l()[Lcom/peel/data/ContentRoom;

    move-result-object v4

    .line 561
    if-eqz v4, :cond_3

    array-length v0, v4

    if-lez v0, :cond_3

    .line 562
    array-length v5, v4

    const/4 v0, 0x0

    move v2, v0

    :goto_1
    if-ge v2, v5, :cond_3

    aget-object v0, v4, v2

    .line 563
    const-string/jumbo v1, "\n\n**********************Content Room ID: "

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Lcom/peel/data/ContentRoom;->a()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v6, "**********************"

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v6, "\nRoom Name: "

    .line 564
    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Lcom/peel/data/ContentRoom;->c()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v6, "\nRoom intid :"

    .line 565
    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Lcom/peel/data/ContentRoom;->b()I

    move-result v6

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v6, "\nProvider ID: "

    .line 566
    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v6, ","

    invoke-virtual {v0}, Lcom/peel/data/ContentRoom;->f()Ljava/util/List;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v6, "\nControl Room Id: "

    .line 567
    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Lcom/peel/data/ContentRoom;->d()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 570
    invoke-virtual {v0}, Lcom/peel/data/ContentRoom;->f()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_1
    :goto_2
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 571
    invoke-static {v0}, Lcom/peel/content/a;->b(Ljava/lang/String;)Lcom/peel/content/library/Library;

    move-result-object v1

    .line 572
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/peel/content/library/Library;->f()Ljava/lang/String;

    move-result-object v7

    const-string/jumbo v8, "live"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 573
    const-string/jumbo v7, "\nservice provider headendid: "

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 574
    const-string/jumbo v0, "\nzipcode: "

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-object v0, v1

    check-cast v0, Lcom/peel/content/library/LiveLibrary;

    invoke-virtual {v0}, Lcom/peel/content/library/LiveLibrary;->h()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2

    .line 562
    :cond_2
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto/16 :goto_1

    .line 580
    :cond_3
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0
.end method

.method public static d(Ljava/lang/String;)[[Ljava/lang/String;
    .locals 8

    .prologue
    const/4 v3, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 473
    const-string/jumbo v0, "sports"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 474
    const/4 v0, 0x7

    new-array v0, v0, [[Ljava/lang/String;

    new-array v1, v7, [Ljava/lang/String;

    const-string/jumbo v2, "Football"

    aput-object v2, v1, v4

    const-string/jumbo v2, "111"

    aput-object v2, v1, v5

    const-string/jumbo v2, "1"

    aput-object v2, v1, v6

    aput-object v1, v0, v4

    new-array v1, v7, [Ljava/lang/String;

    const-string/jumbo v2, "Baseball"

    aput-object v2, v1, v4

    const-string/jumbo v2, "58"

    aput-object v2, v1, v5

    const-string/jumbo v2, "2"

    aput-object v2, v1, v6

    aput-object v1, v0, v5

    new-array v1, v7, [Ljava/lang/String;

    const-string/jumbo v2, "Basketball"

    aput-object v2, v1, v4

    const-string/jumbo v2, "59"

    aput-object v2, v1, v5

    const-string/jumbo v2, "3"

    aput-object v2, v1, v6

    aput-object v1, v0, v6

    new-array v1, v7, [Ljava/lang/String;

    const-string/jumbo v2, "Hockey"

    aput-object v2, v1, v4

    const-string/jumbo v2, "122"

    aput-object v2, v1, v5

    const-string/jumbo v2, "4"

    aput-object v2, v1, v6

    aput-object v1, v0, v7

    new-array v1, v7, [Ljava/lang/String;

    const-string/jumbo v2, "Soccer"

    aput-object v2, v1, v4

    const-string/jumbo v2, "199"

    aput-object v2, v1, v5

    const-string/jumbo v2, "5"

    aput-object v2, v1, v6

    aput-object v1, v0, v3

    const/4 v1, 0x5

    new-array v2, v7, [Ljava/lang/String;

    const-string/jumbo v3, "News & Talk"

    aput-object v3, v2, v4

    const-string/jumbo v3, "156"

    aput-object v3, v2, v5

    const-string/jumbo v3, "6"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/4 v1, 0x6

    new-array v2, v7, [Ljava/lang/String;

    const-string/jumbo v3, "Other"

    aput-object v3, v2, v4

    const-string/jumbo v3, "1"

    aput-object v3, v2, v5

    const-string/jumbo v3, "7"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    .line 510
    :goto_0
    return-object v0

    .line 478
    :cond_0
    const-string/jumbo v0, "program"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string/jumbo v0, "movie"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 479
    :cond_1
    const/16 v0, 0x1d

    new-array v0, v0, [[Ljava/lang/String;

    new-array v1, v7, [Ljava/lang/String;

    const-string/jumbo v2, "Broadcast Channels"

    aput-object v2, v1, v4

    const-string/jumbo v2, "0"

    aput-object v2, v1, v5

    const-string/jumbo v2, "0"

    aput-object v2, v1, v6

    aput-object v1, v0, v4

    new-array v1, v7, [Ljava/lang/String;

    const-string/jumbo v2, "First Run"

    aput-object v2, v1, v4

    const-string/jumbo v2, "1"

    aput-object v2, v1, v5

    const-string/jumbo v2, "1"

    aput-object v2, v1, v6

    aput-object v1, v0, v5

    new-array v1, v7, [Ljava/lang/String;

    const-string/jumbo v2, "Comedy"

    aput-object v2, v1, v4

    const-string/jumbo v2, "4"

    aput-object v2, v1, v5

    const-string/jumbo v2, "2"

    aput-object v2, v1, v6

    aput-object v1, v0, v6

    new-array v1, v7, [Ljava/lang/String;

    const-string/jumbo v2, "Drama"

    aput-object v2, v1, v4

    const-string/jumbo v2, "5"

    aput-object v2, v1, v5

    const-string/jumbo v2, "3"

    aput-object v2, v1, v6

    aput-object v1, v0, v7

    new-array v1, v7, [Ljava/lang/String;

    const-string/jumbo v2, "Kids"

    aput-object v2, v1, v4

    const-string/jumbo v2, "78"

    aput-object v2, v1, v5

    const-string/jumbo v2, "4"

    aput-object v2, v1, v6

    aput-object v1, v0, v3

    const/4 v1, 0x5

    new-array v2, v7, [Ljava/lang/String;

    const-string/jumbo v3, "Action & Adventure"

    aput-object v3, v2, v4

    const-string/jumbo v3, "1"

    aput-object v3, v2, v5

    const-string/jumbo v3, "5"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/4 v1, 0x6

    new-array v2, v7, [Ljava/lang/String;

    const-string/jumbo v3, "Sci-Fi & Fantasy"

    aput-object v3, v2, v4

    const-string/jumbo v3, "11"

    aput-object v3, v2, v5

    const-string/jumbo v3, "6"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/4 v1, 0x7

    new-array v2, v7, [Ljava/lang/String;

    const-string/jumbo v3, "News"

    aput-object v3, v2, v4

    const-string/jumbo v3, "156"

    aput-object v3, v2, v5

    const-string/jumbo v3, "7"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x8

    new-array v2, v7, [Ljava/lang/String;

    const-string/jumbo v3, "Animation"

    aput-object v3, v2, v4

    const-string/jumbo v3, "44"

    aput-object v3, v2, v5

    const-string/jumbo v3, "8"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x9

    new-array v2, v7, [Ljava/lang/String;

    const-string/jumbo v3, "Biography"

    aput-object v3, v2, v4

    const-string/jumbo v3, "175"

    aput-object v3, v2, v5

    const-string/jumbo v3, "9"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0xa

    new-array v2, v7, [Ljava/lang/String;

    const-string/jumbo v3, "Business"

    aput-object v3, v2, v4

    const-string/jumbo v3, "74"

    aput-object v3, v2, v5

    const-string/jumbo v3, "10"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0xb

    new-array v2, v7, [Ljava/lang/String;

    const-string/jumbo v3, "Documentary"

    aput-object v3, v2, v4

    const-string/jumbo v3, "8"

    aput-object v3, v2, v5

    const-string/jumbo v3, "11"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0xc

    new-array v2, v7, [Ljava/lang/String;

    const-string/jumbo v3, "Entertainment"

    aput-object v3, v2, v4

    const-string/jumbo v3, "100"

    aput-object v3, v2, v5

    const-string/jumbo v3, "12"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0xd

    new-array v2, v7, [Ljava/lang/String;

    const-string/jumbo v3, "Foreign"

    aput-object v3, v2, v4

    const-string/jumbo v3, "112"

    aput-object v3, v2, v5

    const-string/jumbo v3, "13"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0xe

    new-array v2, v7, [Ljava/lang/String;

    const-string/jumbo v3, "Game show"

    aput-object v3, v2, v4

    const-string/jumbo v3, "115"

    aput-object v3, v2, v5

    const-string/jumbo v3, "14"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0xf

    new-array v2, v7, [Ljava/lang/String;

    const-string/jumbo v3, "Holiday"

    aput-object v3, v2, v4

    const-string/jumbo v3, "176"

    aput-object v3, v2, v5

    const-string/jumbo v3, "15"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x10

    new-array v2, v7, [Ljava/lang/String;

    const-string/jumbo v3, "Lifestyle"

    aput-object v3, v2, v4

    const-string/jumbo v3, "87"

    aput-object v3, v2, v5

    const-string/jumbo v3, "16"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x11

    new-array v2, v7, [Ljava/lang/String;

    const-string/jumbo v3, "Music & Dance"

    aput-object v3, v2, v4

    const-string/jumbo v3, "14"

    aput-object v3, v2, v5

    const-string/jumbo v3, "17"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x12

    new-array v2, v7, [Ljava/lang/String;

    const-string/jumbo v3, "NonFiction"

    aput-object v3, v2, v4

    const-string/jumbo v3, "3"

    aput-object v3, v2, v5

    const-string/jumbo v3, "18"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x13

    new-array v2, v7, [Ljava/lang/String;

    const-string/jumbo v3, "Reality"

    aput-object v3, v2, v4

    const-string/jumbo v3, "174"

    aput-object v3, v2, v5

    const-string/jumbo v3, "19"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x14

    new-array v2, v7, [Ljava/lang/String;

    const-string/jumbo v3, "Soap"

    aput-object v3, v2, v4

    const-string/jumbo v3, "177"

    aput-object v3, v2, v5

    const-string/jumbo v3, "20"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x15

    new-array v2, v7, [Ljava/lang/String;

    const-string/jumbo v3, "Special"

    aput-object v3, v2, v4

    const-string/jumbo v3, "128"

    aput-object v3, v2, v5

    const-string/jumbo v3, "21"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x16

    new-array v2, v7, [Ljava/lang/String;

    const-string/jumbo v3, "Talk"

    aput-object v3, v2, v4

    const-string/jumbo v3, "137"

    aput-object v3, v2, v5

    const-string/jumbo v3, "22"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x17

    new-array v2, v7, [Ljava/lang/String;

    const-string/jumbo v3, "Thriller"

    aput-object v3, v2, v4

    const-string/jumbo v3, "19"

    aput-object v3, v2, v5

    const-string/jumbo v3, "23"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x18

    new-array v2, v7, [Ljava/lang/String;

    const-string/jumbo v3, "Variety"

    aput-object v3, v2, v4

    const-string/jumbo v3, "178"

    aput-object v3, v2, v5

    const-string/jumbo v3, "24"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x19

    new-array v2, v7, [Ljava/lang/String;

    const-string/jumbo v3, "War"

    aput-object v3, v2, v4

    const-string/jumbo v3, "20"

    aput-object v3, v2, v5

    const-string/jumbo v3, "25"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    new-array v2, v7, [Ljava/lang/String;

    const-string/jumbo v3, "Western"

    aput-object v3, v2, v4

    const-string/jumbo v3, "21"

    aput-object v3, v2, v5

    const-string/jumbo v3, "26"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    new-array v2, v7, [Ljava/lang/String;

    const-string/jumbo v3, "Event"

    aput-object v3, v2, v4

    const-string/jumbo v3, "55"

    aput-object v3, v2, v5

    const-string/jumbo v3, "27"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    new-array v2, v7, [Ljava/lang/String;

    const-string/jumbo v3, "Other"

    aput-object v3, v2, v4

    const-string/jumbo v3, "41"

    aput-object v3, v2, v5

    const-string/jumbo v3, "28"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    goto/16 :goto_0

    .line 510
    :cond_2
    const/4 v0, 0x0

    check-cast v0, [[Ljava/lang/String;

    goto/16 :goto_0
.end method

.method public static e()Ljava/util/Map;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 584
    const-string/jumbo v0, "live"

    invoke-static {v0}, Lcom/peel/content/a;->c(Ljava/lang/String;)Lcom/peel/content/library/Library;

    move-result-object v0

    check-cast v0, Lcom/peel/content/library/LiveLibrary;

    .line 586
    if-eqz v0, :cond_3

    .line 587
    sget-object v1, Lcom/peel/content/a;->l:Ljava/util/Map;

    invoke-virtual {v0}, Lcom/peel/content/library/LiveLibrary;->e()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 588
    sget-object v1, Lcom/peel/content/a;->l:Ljava/util/Map;

    invoke-virtual {v0}, Lcom/peel/content/library/LiveLibrary;->e()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    .line 601
    :goto_0
    return-object v0

    .line 590
    :cond_0
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 592
    invoke-virtual {v0}, Lcom/peel/content/library/LiveLibrary;->i()[Lcom/peel/data/Channel;

    move-result-object v3

    array-length v4, v3

    const/4 v1, 0x0

    :goto_1
    if-ge v1, v4, :cond_2

    aget-object v5, v3, v1

    .line 593
    invoke-virtual {v5}, Lcom/peel/data/Channel;->m()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5}, Lcom/peel/data/Channel;->e()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_1

    .line 594
    invoke-virtual {v5}, Lcom/peel/data/Channel;->e()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5}, Lcom/peel/data/Channel;->m()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v2, v6, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 592
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 597
    :cond_2
    sget-object v1, Lcom/peel/content/a;->l:Ljava/util/Map;

    invoke-virtual {v0}, Lcom/peel/content/library/LiveLibrary;->e()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v3, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 598
    sget-object v1, Lcom/peel/content/a;->l:Ljava/util/Map;

    invoke-virtual {v0}, Lcom/peel/content/library/LiveLibrary;->e()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    goto :goto_0

    .line 601
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static e(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 605
    sget-object v0, Lcom/peel/content/a;->l:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 606
    sget-object v0, Lcom/peel/content/a;->l:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 608
    :cond_0
    return-void
.end method

.method static synthetic f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 31
    sget-object v0, Lcom/peel/content/a;->i:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic f(Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 31
    sput-object p0, Lcom/peel/content/a;->j:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 31
    sget-object v0, Lcom/peel/content/a;->j:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic h()Ljava/util/List;
    .locals 1

    .prologue
    .line 31
    sget-object v0, Lcom/peel/content/a;->k:Ljava/util/List;

    return-object v0
.end method
