.class final Lcom/peel/content/listing/e;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Landroid/os/Parcelable;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 147
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/os/Parcelable;Landroid/os/Parcelable;)I
    .locals 5

    .prologue
    const-wide/16 v0, 0x0

    .line 149
    .line 151
    instance-of v2, p1, Lcom/peel/content/listing/LiveListing;

    if-eqz v2, :cond_2

    instance-of v2, p2, Lcom/peel/content/listing/LiveListing;

    if-eqz v2, :cond_2

    .line 152
    check-cast p1, Lcom/peel/content/listing/LiveListing;

    invoke-virtual {p1}, Lcom/peel/content/listing/LiveListing;->o()J

    move-result-wide v2

    .line 153
    check-cast p2, Lcom/peel/content/listing/LiveListing;

    invoke-virtual {p2}, Lcom/peel/content/listing/LiveListing;->o()J

    move-result-wide v0

    .line 156
    :goto_0
    cmp-long v4, v2, v0

    if-gez v4, :cond_0

    .line 157
    const/4 v0, -0x1

    .line 161
    :goto_1
    return v0

    .line 158
    :cond_0
    cmp-long v0, v2, v0

    if-lez v0, :cond_1

    .line 159
    const/4 v0, 0x1

    goto :goto_1

    .line 161
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    :cond_2
    move-wide v2, v0

    goto :goto_0
.end method

.method public synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 147
    check-cast p1, Landroid/os/Parcelable;

    check-cast p2, Landroid/os/Parcelable;

    invoke-virtual {p0, p1, p2}, Lcom/peel/content/listing/e;->a(Landroid/os/Parcelable;Landroid/os/Parcelable;)I

    move-result v0

    return v0
.end method
