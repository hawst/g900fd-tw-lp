.class public Lcom/peel/content/listing/a;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lcom/peel/content/listing/Listing;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/peel/content/listing/Listing;Lcom/peel/content/listing/Listing;)I
    .locals 4

    .prologue
    .line 13
    instance-of v0, p1, Lcom/peel/content/listing/DirecTVListing;

    if-eqz v0, :cond_0

    instance-of v0, p2, Lcom/peel/content/listing/DirecTVListing;

    if-eqz v0, :cond_0

    .line 14
    check-cast p1, Lcom/peel/content/listing/DirecTVListing;

    invoke-virtual {p1}, Lcom/peel/content/listing/DirecTVListing;->b()J

    move-result-wide v0

    check-cast p2, Lcom/peel/content/listing/DirecTVListing;

    invoke-virtual {p2}, Lcom/peel/content/listing/DirecTVListing;->b()J

    move-result-wide v2

    sub-long/2addr v0, v2

    .line 15
    long-to-int v0, v0

    neg-int v0, v0

    .line 19
    :goto_0
    return v0

    .line 16
    :cond_0
    instance-of v0, p1, Lcom/peel/content/listing/LiveListing;

    if-eqz v0, :cond_1

    instance-of v0, p2, Lcom/peel/content/listing/LiveListing;

    if-eqz v0, :cond_1

    .line 17
    check-cast p1, Lcom/peel/content/listing/LiveListing;

    invoke-virtual {p1}, Lcom/peel/content/listing/LiveListing;->o()J

    move-result-wide v0

    check-cast p2, Lcom/peel/content/listing/LiveListing;

    invoke-virtual {p2}, Lcom/peel/content/listing/LiveListing;->o()J

    move-result-wide v2

    sub-long/2addr v0, v2

    .line 18
    long-to-int v0, v0

    neg-int v0, v0

    goto :goto_0

    .line 19
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 10
    check-cast p1, Lcom/peel/content/listing/Listing;

    check-cast p2, Lcom/peel/content/listing/Listing;

    invoke-virtual {p0, p1, p2}, Lcom/peel/content/listing/a;->a(Lcom/peel/content/listing/Listing;Lcom/peel/content/listing/Listing;)I

    move-result v0

    return v0
.end method
