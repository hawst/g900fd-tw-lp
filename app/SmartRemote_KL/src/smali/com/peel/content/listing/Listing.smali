.class public abstract Lcom/peel/content/listing/Listing;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/os/Parcelable;
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable;",
        "Ljava/lang/Comparable",
        "<",
        "Lcom/peel/content/listing/Listing;",
        ">;"
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/peel/content/listing/Listing;",
            ">;"
        }
    .end annotation
.end field

.field private static final i:Ljava/lang/String;


# instance fields
.field protected a:Ljava/lang/String;

.field protected b:Ljava/lang/String;

.field protected c:Ljava/lang/String;

.field protected d:Ljava/lang/String;

.field protected e:[Ljava/lang/String;

.field protected f:I

.field protected g:J

.field protected h:Ljava/lang/String;

.field protected json:Landroid/os/Bundle;

.field protected title:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 14
    new-instance v0, Lcom/peel/content/listing/b;

    invoke-direct {v0}, Lcom/peel/content/listing/b;-><init>()V

    sput-object v0, Lcom/peel/content/listing/Listing;->CREATOR:Landroid/os/Parcelable$Creator;

    .line 19
    const-class v0, Lcom/peel/content/listing/Listing;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/peel/content/listing/Listing;->i:Ljava/lang/String;

    return-void
.end method

.method protected constructor <init>()V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method protected constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;IJ)V
    .locals 1

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    iput-object p1, p0, Lcom/peel/content/listing/Listing;->h:Ljava/lang/String;

    .line 39
    iput-object p2, p0, Lcom/peel/content/listing/Listing;->a:Ljava/lang/String;

    .line 40
    iput-object p3, p0, Lcom/peel/content/listing/Listing;->b:Ljava/lang/String;

    .line 41
    iput-object p4, p0, Lcom/peel/content/listing/Listing;->c:Ljava/lang/String;

    .line 42
    iput-object p5, p0, Lcom/peel/content/listing/Listing;->d:Ljava/lang/String;

    .line 43
    iput-object p6, p0, Lcom/peel/content/listing/Listing;->title:Ljava/lang/String;

    .line 44
    iput-object p7, p0, Lcom/peel/content/listing/Listing;->e:[Ljava/lang/String;

    .line 45
    iput p8, p0, Lcom/peel/content/listing/Listing;->f:I

    .line 46
    iput-wide p9, p0, Lcom/peel/content/listing/Listing;->g:J

    .line 47
    return-void
.end method

.method static synthetic a(Landroid/os/Parcel;)Lcom/peel/content/listing/Listing;
    .locals 1

    .prologue
    .line 13
    invoke-static {p0}, Lcom/peel/content/listing/Listing;->b(Landroid/os/Parcel;)Lcom/peel/content/listing/Listing;

    move-result-object v0

    return-object v0
.end method

.method private static b(Landroid/os/Parcel;)Lcom/peel/content/listing/Listing;
    .locals 12

    .prologue
    const/4 v0, 0x0

    .line 50
    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 51
    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 52
    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    .line 53
    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v4

    .line 54
    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    .line 55
    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v5

    .line 56
    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    move-result v6

    .line 58
    if-lez v6, :cond_1

    .line 59
    new-array v6, v6, [Ljava/lang/String;

    .line 60
    invoke-virtual {p0, v6}, Landroid/os/Parcel;->readStringArray([Ljava/lang/String;)V

    .line 62
    :goto_0
    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    move-result v7

    .line 63
    invoke-virtual {p0}, Landroid/os/Parcel;->readLong()J

    move-result-wide v8

    .line 64
    invoke-virtual {p0}, Landroid/os/Parcel;->readBundle()Landroid/os/Bundle;

    move-result-object v10

    .line 66
    const-string/jumbo v11, "dtv"

    invoke-virtual {v11, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_0

    .line 67
    new-instance v1, Lcom/peel/content/listing/DirecTVListing;

    invoke-direct/range {v1 .. v10}, Lcom/peel/content/listing/DirecTVListing;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;IJLandroid/os/Bundle;)V

    .line 71
    :goto_1
    return-object v1

    .line 69
    :cond_0
    sget-object v2, Lcom/peel/content/listing/Listing;->i:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "(crateListing) unrecognized listing type: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v1, v0

    .line 71
    goto :goto_1

    :cond_1
    move-object v6, v0

    goto :goto_0
.end method


# virtual methods
.method public a(Lcom/peel/content/listing/Listing;)I
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 152
    .line 156
    invoke-virtual {p1}, Lcom/peel/content/listing/Listing;->i()I

    move-result v1

    .line 157
    invoke-virtual {p0}, Lcom/peel/content/listing/Listing;->i()I

    move-result v2

    if-ne v2, v1, :cond_1

    .line 161
    :cond_0
    :goto_0
    return v0

    .line 158
    :cond_1
    invoke-virtual {p0}, Lcom/peel/content/listing/Listing;->i()I

    move-result v2

    if-ge v2, v1, :cond_2

    const/4 v0, -0x1

    goto :goto_0

    .line 159
    :cond_2
    invoke-virtual {p0}, Lcom/peel/content/listing/Listing;->i()I

    move-result v2

    if-ge v2, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public abstract a()Landroid/os/Bundle;
.end method

.method public a(Ljava/lang/String;)Ljava/net/URI;
    .locals 1

    .prologue
    .line 78
    invoke-virtual {p0}, Lcom/peel/content/listing/Listing;->e()Ljava/net/URI;

    move-result-object v0

    return-object v0
.end method

.method public synthetic compareTo(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 13
    check-cast p1, Lcom/peel/content/listing/Listing;

    invoke-virtual {p0, p1}, Lcom/peel/content/listing/Listing;->a(Lcom/peel/content/listing/Listing;)I

    move-result v0

    return v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 165
    const v0, 0x16f59

    return v0
.end method

.method public abstract e()Ljava/net/URI;
.end method

.method public f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lcom/peel/content/listing/Listing;->a:Ljava/lang/String;

    return-object v0
.end method

.method public g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lcom/peel/content/listing/Listing;->b:Ljava/lang/String;

    return-object v0
.end method

.method public h()Ljava/lang/String;
    .locals 1

    .prologue
    .line 112
    iget-object v0, p0, Lcom/peel/content/listing/Listing;->c:Ljava/lang/String;

    return-object v0
.end method

.method public i()I
    .locals 1

    .prologue
    .line 120
    iget v0, p0, Lcom/peel/content/listing/Listing;->f:I

    return v0
.end method

.method public j()J
    .locals 2

    .prologue
    .line 124
    iget-wide v0, p0, Lcom/peel/content/listing/Listing;->g:J

    return-wide v0
.end method

.method public k()Ljava/lang/String;
    .locals 1

    .prologue
    .line 128
    iget-object v0, p0, Lcom/peel/content/listing/Listing;->d:Ljava/lang/String;

    return-object v0
.end method

.method public l()Ljava/lang/String;
    .locals 1

    .prologue
    .line 136
    iget-object v0, p0, Lcom/peel/content/listing/Listing;->title:Ljava/lang/String;

    return-object v0
.end method

.method public m()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 140
    iget-object v0, p0, Lcom/peel/content/listing/Listing;->e:[Ljava/lang/String;

    return-object v0
.end method

.method public n()Ljava/lang/String;
    .locals 1

    .prologue
    .line 148
    iget-object v0, p0, Lcom/peel/content/listing/Listing;->h:Ljava/lang/String;

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 168
    iget-object v0, p0, Lcom/peel/content/listing/Listing;->h:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 169
    iget-object v0, p0, Lcom/peel/content/listing/Listing;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 170
    iget-object v0, p0, Lcom/peel/content/listing/Listing;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 171
    iget-object v0, p0, Lcom/peel/content/listing/Listing;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 172
    iget-object v0, p0, Lcom/peel/content/listing/Listing;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 173
    iget-object v0, p0, Lcom/peel/content/listing/Listing;->title:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 174
    iget-object v0, p0, Lcom/peel/content/listing/Listing;->e:[Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/peel/content/listing/Listing;->e:[Ljava/lang/String;

    array-length v0, v0

    if-nez v0, :cond_1

    .line 175
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 180
    :goto_0
    iget v0, p0, Lcom/peel/content/listing/Listing;->f:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 181
    iget-wide v0, p0, Lcom/peel/content/listing/Listing;->g:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 182
    invoke-virtual {p0}, Lcom/peel/content/listing/Listing;->a()Landroid/os/Bundle;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeBundle(Landroid/os/Bundle;)V

    .line 183
    return-void

    .line 177
    :cond_1
    iget-object v0, p0, Lcom/peel/content/listing/Listing;->e:[Ljava/lang/String;

    array-length v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 178
    iget-object v0, p0, Lcom/peel/content/listing/Listing;->e:[Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeStringArray([Ljava/lang/String;)V

    goto :goto_0
.end method
