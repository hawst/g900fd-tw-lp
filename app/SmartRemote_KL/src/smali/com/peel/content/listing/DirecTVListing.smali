.class public final Lcom/peel/content/listing/DirecTVListing;
.super Lcom/peel/content/listing/Listing;


# instance fields
.field private final callsign:Ljava/lang/String;

.field private final episodeTitle:Ljava/lang/String;

.field private final isRecording:Z

.field private final isViewed:Z

.field private final major:I

.field private final offset:I

.field private final startTime:J

.field private final uniqueId:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;IJLandroid/os/Bundle;)V
    .locals 15

    .prologue
    .line 129
    const-string/jumbo v4, "dtv"

    const/4 v8, 0x0

    move-object v3, p0

    move-object/from16 v5, p1

    move-object/from16 v6, p2

    move-object/from16 v7, p3

    move-object/from16 v9, p4

    move-object/from16 v10, p5

    move/from16 v11, p6

    move-wide/from16 v12, p7

    invoke-direct/range {v3 .. v13}, Lcom/peel/content/listing/Listing;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;IJ)V

    .line 130
    const-string/jumbo v2, "episodeTitle"

    move-object/from16 v0, p9

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/peel/content/listing/DirecTVListing;->episodeTitle:Ljava/lang/String;

    .line 131
    const-string/jumbo v2, "isViewed"

    move-object/from16 v0, p9

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    iput-boolean v2, p0, Lcom/peel/content/listing/DirecTVListing;->isViewed:Z

    .line 132
    const-string/jumbo v2, "isRecording"

    move-object/from16 v0, p9

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    iput-boolean v2, p0, Lcom/peel/content/listing/DirecTVListing;->isRecording:Z

    .line 133
    const-string/jumbo v2, "startTime"

    move-object/from16 v0, p9

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/peel/content/listing/DirecTVListing;->startTime:J

    .line 134
    const-string/jumbo v2, "uniqueId"

    move-object/from16 v0, p9

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/peel/content/listing/DirecTVListing;->uniqueId:Ljava/lang/String;

    .line 136
    const-string/jumbo v2, "offset"

    move-object/from16 v0, p9

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    iput v2, p0, Lcom/peel/content/listing/DirecTVListing;->offset:I

    .line 137
    const-string/jumbo v2, "callsign"

    move-object/from16 v0, p9

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/peel/content/listing/DirecTVListing;->callsign:Ljava/lang/String;

    .line 138
    const-string/jumbo v2, "major"

    move-object/from16 v0, p9

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    iput v2, p0, Lcom/peel/content/listing/DirecTVListing;->major:I

    .line 148
    return-void
.end method


# virtual methods
.method public a()Landroid/os/Bundle;
    .locals 4

    .prologue
    .line 151
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 152
    const-string/jumbo v1, "title"

    iget-object v2, p0, Lcom/peel/content/listing/DirecTVListing;->title:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 154
    const-string/jumbo v1, "episodeTitle"

    iget-object v2, p0, Lcom/peel/content/listing/DirecTVListing;->episodeTitle:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 155
    const-string/jumbo v1, "isViewed"

    iget-boolean v2, p0, Lcom/peel/content/listing/DirecTVListing;->isViewed:Z

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 157
    const-string/jumbo v1, "startTime"

    iget-wide v2, p0, Lcom/peel/content/listing/DirecTVListing;->startTime:J

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 158
    const-string/jumbo v1, "uniqueId"

    iget-object v2, p0, Lcom/peel/content/listing/DirecTVListing;->uniqueId:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 161
    const-string/jumbo v1, "offset"

    iget v2, p0, Lcom/peel/content/listing/DirecTVListing;->offset:I

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 163
    const-string/jumbo v1, "callsign"

    iget-object v2, p0, Lcom/peel/content/listing/DirecTVListing;->callsign:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 164
    const-string/jumbo v1, "major"

    iget v2, p0, Lcom/peel/content/listing/DirecTVListing;->major:I

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 166
    return-object v0
.end method

.method public b()J
    .locals 2

    .prologue
    .line 169
    iget-wide v0, p0, Lcom/peel/content/listing/DirecTVListing;->startTime:J

    return-wide v0
.end method

.method public c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 171
    iget-object v0, p0, Lcom/peel/content/listing/DirecTVListing;->callsign:Ljava/lang/String;

    return-object v0
.end method

.method public d()I
    .locals 1

    .prologue
    .line 173
    iget v0, p0, Lcom/peel/content/listing/DirecTVListing;->major:I

    return v0
.end method

.method public e()Ljava/net/URI;
    .locals 2

    .prologue
    .line 184
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "dtv://dtv/play?uniqueId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/content/listing/DirecTVListing;->uniqueId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "&offset="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/peel/content/listing/DirecTVListing;->offset:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/net/URI;->create(Ljava/lang/String;)Ljava/net/URI;

    move-result-object v0

    return-object v0
.end method
