.class public final Lcom/peel/content/listing/LiveListing;
.super Lcom/peel/content/listing/Listing;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/peel/content/listing/LiveListing;",
            ">;"
        }
    .end annotation
.end field

.field private static final i:Ljava/lang/String;


# instance fields
.field private channel:Ljava/lang/String;

.field private channelId:Ljava/lang/String;

.field private channelNumber:Ljava/lang/String;

.field private image:Ljava/lang/String;

.field private imageMissingTitle:Z

.field private j:Ljava/lang/String;

.field private k:Z

.field private l:Ljava/lang/String;

.field private m:Ljava/lang/String;

.field private n:Ljava/lang/String;

.field private o:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private orig_air_date:J

.field private p:Ljava/lang/String;

.field private reminded:Z

.field private reminders:[Landroid/os/Bundle;

.field private scheduledate:Ljava/lang/String;

.field private seasonal_show:Z

.field private start:J

.field private tags:Ljava/lang/String;

.field private teamInfo:[Lcom/peel/data/SportsTeam;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 30
    new-instance v0, Lcom/peel/content/listing/d;

    invoke-direct {v0}, Lcom/peel/content/listing/d;-><init>()V

    sput-object v0, Lcom/peel/content/listing/LiveListing;->CREATOR:Landroid/os/Parcelable$Creator;

    .line 36
    const-class v0, Lcom/peel/content/listing/LiveListing;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/peel/content/listing/LiveListing;->i:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 58
    invoke-direct {p0}, Lcom/peel/content/listing/Listing;-><init>()V

    .line 59
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;IJLjava/lang/String;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Lcom/peel/data/SportsTeam;JLjava/lang/String;Ljava/lang/String;Z)V
    .locals 14

    .prologue
    .line 112
    const-string/jumbo v4, "live"

    move-object v3, p0

    move-object v5, p1

    move-object/from16 v6, p2

    move-object/from16 v7, p3

    move-object/from16 v8, p4

    move-object/from16 v9, p7

    move-object/from16 v10, p8

    move/from16 v11, p9

    move-wide/from16 v12, p10

    invoke-direct/range {v3 .. v13}, Lcom/peel/content/listing/Listing;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;IJ)V

    .line 113
    move-object/from16 v0, p6

    iput-object v0, p0, Lcom/peel/content/listing/LiveListing;->m:Ljava/lang/String;

    .line 114
    move-object/from16 v0, p5

    iput-object v0, p0, Lcom/peel/content/listing/LiveListing;->j:Ljava/lang/String;

    .line 115
    move-object/from16 v0, p12

    iput-object v0, p0, Lcom/peel/content/listing/LiveListing;->l:Ljava/lang/String;

    .line 116
    move-object/from16 v0, p13

    iput-object v0, p0, Lcom/peel/content/listing/LiveListing;->image:Ljava/lang/String;

    .line 117
    move-object/from16 v0, p14

    iput-object v0, p0, Lcom/peel/content/listing/LiveListing;->n:Ljava/lang/String;

    .line 118
    move-wide/from16 v0, p15

    iput-wide v0, p0, Lcom/peel/content/listing/LiveListing;->start:J

    .line 119
    move-object/from16 v0, p17

    iput-object v0, p0, Lcom/peel/content/listing/LiveListing;->channel:Ljava/lang/String;

    .line 120
    move-object/from16 v0, p18

    iput-object v0, p0, Lcom/peel/content/listing/LiveListing;->channelNumber:Ljava/lang/String;

    .line 121
    move-object/from16 v0, p19

    iput-object v0, p0, Lcom/peel/content/listing/LiveListing;->channelId:Ljava/lang/String;

    .line 122
    move-object/from16 v0, p20

    iput-object v0, p0, Lcom/peel/content/listing/LiveListing;->tags:Ljava/lang/String;

    .line 123
    move-object/from16 v0, p21

    iput-object v0, p0, Lcom/peel/content/listing/LiveListing;->teamInfo:[Lcom/peel/data/SportsTeam;

    .line 124
    move-wide/from16 v0, p22

    iput-wide v0, p0, Lcom/peel/content/listing/LiveListing;->orig_air_date:J

    .line 125
    move-object/from16 v0, p25

    iput-object v0, p0, Lcom/peel/content/listing/LiveListing;->p:Ljava/lang/String;

    .line 127
    invoke-static/range {p13 .. p13}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 128
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/peel/content/listing/LiveListing;->imageMissingTitle:Z

    .line 133
    :goto_0
    if-eqz p21, :cond_0

    const-string/jumbo v2, "sports"

    move-object/from16 v0, p4

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    if-eqz p5, :cond_3

    const-string/jumbo v2, "0"

    .line 134
    move-object/from16 v0, p5

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    const-string/jumbo v2, "sports"

    move-object/from16 v0, p4

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 135
    :cond_1
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/peel/content/listing/LiveListing;->seasonal_show:Z

    .line 140
    :goto_1
    move-object/from16 v0, p24

    iput-object v0, p0, Lcom/peel/content/listing/LiveListing;->scheduledate:Ljava/lang/String;

    .line 141
    move/from16 v0, p26

    iput-boolean v0, p0, Lcom/peel/content/listing/LiveListing;->k:Z

    .line 143
    invoke-direct {p0}, Lcom/peel/content/listing/LiveListing;->C()V

    .line 144
    return-void

    .line 130
    :cond_2
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/peel/content/listing/LiveListing;->imageMissingTitle:Z

    goto :goto_0

    .line 137
    :cond_3
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/peel/content/listing/LiveListing;->seasonal_show:Z

    goto :goto_1
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;IJLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 14

    .prologue
    .line 64
    const-string/jumbo v4, "live"

    move-object v3, p0

    move-object v5, p1

    move-object/from16 v6, p2

    move-object/from16 v7, p3

    move-object/from16 v8, p4

    move-object/from16 v9, p5

    move-object/from16 v10, p6

    move/from16 v11, p7

    move-wide/from16 v12, p8

    invoke-direct/range {v3 .. v13}, Lcom/peel/content/listing/Listing;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;IJ)V

    .line 65
    const-string/jumbo v2, "start"

    move-object/from16 v0, p15

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/peel/content/listing/LiveListing;->start:J

    .line 66
    const-string/jumbo v2, "channel"

    move-object/from16 v0, p15

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/peel/content/listing/LiveListing;->channel:Ljava/lang/String;

    .line 67
    const-string/jumbo v2, "channelNumber"

    move-object/from16 v0, p15

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/peel/content/listing/LiveListing;->channelNumber:Ljava/lang/String;

    .line 68
    const-string/jumbo v2, "channelId"

    move-object/from16 v0, p15

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/peel/content/listing/LiveListing;->channelId:Ljava/lang/String;

    .line 69
    const-string/jumbo v2, "tags"

    move-object/from16 v0, p15

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/peel/content/listing/LiveListing;->tags:Ljava/lang/String;

    .line 70
    const-string/jumbo v2, "imageMissingTitle"

    const/4 v3, 0x0

    move-object/from16 v0, p15

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    iput-boolean v2, p0, Lcom/peel/content/listing/LiveListing;->imageMissingTitle:Z

    .line 71
    const-string/jumbo v2, "json"

    move-object/from16 v0, p15

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string/jumbo v2, "json"

    move-object/from16 v0, p15

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v2

    iput-object v2, p0, Lcom/peel/content/listing/LiveListing;->json:Landroid/os/Bundle;

    .line 72
    :cond_0
    const-string/jumbo v2, "reminders"

    move-object/from16 v0, p15

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 73
    const-string/jumbo v2, "reminders"

    move-object/from16 v0, p15

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getParcelableArray(Ljava/lang/String;)[Landroid/os/Parcelable;

    move-result-object v4

    .line 74
    array-length v2, v4

    new-array v2, v2, [Landroid/os/Bundle;

    iput-object v2, p0, Lcom/peel/content/listing/LiveListing;->reminders:[Landroid/os/Bundle;

    .line 75
    const/4 v2, 0x0

    move v3, v2

    :goto_0
    array-length v2, v4

    if-ge v3, v2, :cond_1

    .line 76
    iget-object v5, p0, Lcom/peel/content/listing/LiveListing;->reminders:[Landroid/os/Bundle;

    aget-object v2, v4, v3

    check-cast v2, Landroid/os/Bundle;

    aput-object v2, v5, v3

    .line 75
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_0

    .line 78
    :cond_1
    const-string/jumbo v2, "seasonal_show"

    const/4 v3, 0x0

    move-object/from16 v0, p15

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    iput-boolean v2, p0, Lcom/peel/content/listing/LiveListing;->seasonal_show:Z

    .line 79
    const-string/jumbo v2, "reminded"

    const/4 v3, 0x0

    move-object/from16 v0, p15

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    iput-boolean v2, p0, Lcom/peel/content/listing/LiveListing;->reminded:Z

    .line 80
    const-string/jumbo v2, "scheduledate"

    move-object/from16 v0, p15

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/peel/content/listing/LiveListing;->scheduledate:Ljava/lang/String;

    .line 82
    const-string/jumbo v2, "teamInfo"

    move-object/from16 v0, p15

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 83
    const-string/jumbo v2, "teamInfo"

    move-object/from16 v0, p15

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getParcelableArray(Ljava/lang/String;)[Landroid/os/Parcelable;

    move-result-object v4

    .line 84
    array-length v2, v4

    new-array v2, v2, [Lcom/peel/data/SportsTeam;

    iput-object v2, p0, Lcom/peel/content/listing/LiveListing;->teamInfo:[Lcom/peel/data/SportsTeam;

    .line 85
    const/4 v2, 0x0

    move v3, v2

    :goto_1
    array-length v2, v4

    if-ge v3, v2, :cond_3

    .line 86
    iget-object v5, p0, Lcom/peel/content/listing/LiveListing;->teamInfo:[Lcom/peel/data/SportsTeam;

    aget-object v2, v4, v3

    check-cast v2, Lcom/peel/data/SportsTeam;

    aput-object v2, v5, v3

    .line 85
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_1

    .line 89
    :cond_2
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/peel/content/listing/LiveListing;->teamInfo:[Lcom/peel/data/SportsTeam;

    .line 91
    :cond_3
    const-string/jumbo v2, "reminderundo"

    move-object/from16 v0, p15

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    iput-boolean v2, p0, Lcom/peel/content/listing/LiveListing;->k:Z

    .line 92
    const-string/jumbo v2, "episode_title"

    move-object/from16 v0, p15

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    const-string/jumbo v2, "episode_title"

    .line 93
    move-object/from16 v0, p15

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    :goto_2
    iput-object v2, p0, Lcom/peel/content/listing/LiveListing;->m:Ljava/lang/String;

    .line 94
    const-string/jumbo v2, "orig_air_date"

    move-object/from16 v0, p15

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_5

    const-string/jumbo v2, "orig_air_date"

    move-object/from16 v0, p15

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    :goto_3
    iput-wide v2, p0, Lcom/peel/content/listing/LiveListing;->orig_air_date:J

    .line 97
    move-object/from16 v0, p10

    iput-object v0, p0, Lcom/peel/content/listing/LiveListing;->l:Ljava/lang/String;

    .line 98
    move-object/from16 v0, p11

    iput-object v0, p0, Lcom/peel/content/listing/LiveListing;->image:Ljava/lang/String;

    .line 99
    move-object/from16 v0, p12

    iput-object v0, p0, Lcom/peel/content/listing/LiveListing;->n:Ljava/lang/String;

    .line 100
    move-object/from16 v0, p13

    iput-object v0, p0, Lcom/peel/content/listing/LiveListing;->j:Ljava/lang/String;

    .line 101
    move-object/from16 v0, p14

    iput-object v0, p0, Lcom/peel/content/listing/LiveListing;->p:Ljava/lang/String;

    .line 103
    invoke-direct {p0}, Lcom/peel/content/listing/LiveListing;->C()V

    .line 104
    return-void

    .line 93
    :cond_4
    const/4 v2, 0x0

    goto :goto_2

    .line 94
    :cond_5
    const-wide/16 v2, -0x1

    goto :goto_3
.end method

.method private C()V
    .locals 6

    .prologue
    .line 295
    iget-object v0, p0, Lcom/peel/content/listing/LiveListing;->n:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 296
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/peel/content/listing/LiveListing;->o:Ljava/util/Map;

    .line 297
    iget-object v0, p0, Lcom/peel/content/listing/LiveListing;->n:Ljava/lang/String;

    const-string/jumbo v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 299
    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 300
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v5, Lcom/peel/content/a/j;->b:Ljava/lang/String;

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v5, "epg/schedules/pictures/"

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v0, p0, Lcom/peel/content/listing/LiveListing;->c:Ljava/lang/String;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/peel/content/listing/LiveListing;->b:Ljava/lang/String;

    :goto_1
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v5, "/"

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v5, "?country="

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v5, Lcom/peel/content/a;->e:Ljava/lang/String;

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 303
    iget-object v5, p0, Lcom/peel/content/listing/LiveListing;->o:Ljava/util/Map;

    invoke-interface {v5, v4, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 299
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 300
    :cond_0
    iget-object v0, p0, Lcom/peel/content/listing/LiveListing;->c:Ljava/lang/String;

    goto :goto_1

    .line 306
    :cond_1
    return-void
.end method

.method public static a(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/os/Parcelable;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 147
    new-instance v0, Lcom/peel/content/listing/e;

    invoke-direct {v0}, Lcom/peel/content/listing/e;-><init>()V

    invoke-static {p0, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 166
    return-void
.end method

.method static synthetic b(Landroid/os/Parcel;)Lcom/peel/content/listing/LiveListing;
    .locals 1

    .prologue
    .line 29
    invoke-static {p0}, Lcom/peel/content/listing/LiveListing;->c(Landroid/os/Parcel;)Lcom/peel/content/listing/LiveListing;

    move-result-object v0

    return-object v0
.end method

.method private static c(Landroid/os/Parcel;)Lcom/peel/content/listing/LiveListing;
    .locals 29

    .prologue
    .line 169
    invoke-virtual/range {p0 .. p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    .line 170
    invoke-virtual/range {p0 .. p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v4

    .line 171
    invoke-virtual/range {p0 .. p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v5

    .line 172
    invoke-virtual/range {p0 .. p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v6

    .line 173
    invoke-virtual/range {p0 .. p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v9

    .line 174
    invoke-virtual/range {p0 .. p0}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 175
    const/4 v10, 0x0

    .line 176
    if-lez v2, :cond_0

    .line 177
    new-array v10, v2, [Ljava/lang/String;

    .line 178
    move-object/from16 v0, p0

    invoke-virtual {v0, v10}, Landroid/os/Parcel;->readStringArray([Ljava/lang/String;)V

    .line 180
    :cond_0
    invoke-virtual/range {p0 .. p0}, Landroid/os/Parcel;->readInt()I

    move-result v11

    .line 181
    invoke-virtual/range {p0 .. p0}, Landroid/os/Parcel;->readLong()J

    move-result-wide v12

    .line 182
    invoke-virtual/range {p0 .. p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v8

    .line 183
    invoke-virtual/range {p0 .. p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v14

    .line 184
    invoke-virtual/range {p0 .. p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v19

    .line 185
    invoke-virtual/range {p0 .. p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v20

    .line 186
    invoke-virtual/range {p0 .. p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v21

    .line 187
    invoke-virtual/range {p0 .. p0}, Landroid/os/Parcel;->readLong()J

    move-result-wide v17

    .line 188
    invoke-virtual/range {p0 .. p0}, Landroid/os/Parcel;->readLong()J

    move-result-wide v24

    .line 189
    invoke-virtual/range {p0 .. p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v15

    .line 190
    invoke-virtual/range {p0 .. p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v16

    .line 191
    invoke-virtual/range {p0 .. p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v7

    .line 192
    invoke-virtual/range {p0 .. p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v22

    .line 193
    const-class v2, Lcom/peel/data/SportsTeam;

    .line 194
    invoke-virtual {v2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v2

    .line 193
    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->readParcelableArray(Ljava/lang/ClassLoader;)[Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, [Lcom/peel/data/SportsTeam;

    move-object/from16 v23, v2

    check-cast v23, [Lcom/peel/data/SportsTeam;

    .line 195
    invoke-virtual/range {p0 .. p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v26

    .line 196
    invoke-virtual/range {p0 .. p0}, Landroid/os/Parcel;->readInt()I

    move-result v2

    const/16 v27, 0x1

    move/from16 v0, v27

    if-ne v2, v0, :cond_1

    const/16 v28, 0x1

    .line 197
    :goto_0
    invoke-virtual/range {p0 .. p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v27

    .line 199
    new-instance v2, Lcom/peel/content/listing/LiveListing;

    invoke-direct/range {v2 .. v28}, Lcom/peel/content/listing/LiveListing;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;IJLjava/lang/String;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Lcom/peel/data/SportsTeam;JLjava/lang/String;Ljava/lang/String;Z)V

    return-object v2

    .line 196
    :cond_1
    const/16 v28, 0x0

    goto :goto_0
.end method


# virtual methods
.method public A()Ljava/lang/String;
    .locals 1

    .prologue
    .line 321
    iget-object v0, p0, Lcom/peel/content/listing/LiveListing;->j:Ljava/lang/String;

    return-object v0
.end method

.method public B()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 325
    iget-object v0, p0, Lcom/peel/content/listing/LiveListing;->o:Ljava/util/Map;

    return-object v0
.end method

.method public a()Landroid/os/Bundle;
    .locals 6

    .prologue
    .line 329
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 330
    const-string/jumbo v1, "start"

    iget-wide v2, p0, Lcom/peel/content/listing/LiveListing;->start:J

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 331
    const-string/jumbo v1, "channel"

    iget-object v2, p0, Lcom/peel/content/listing/LiveListing;->channel:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 332
    const-string/jumbo v1, "channelNumber"

    iget-object v2, p0, Lcom/peel/content/listing/LiveListing;->channelNumber:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 333
    const-string/jumbo v1, "channelId"

    iget-object v2, p0, Lcom/peel/content/listing/LiveListing;->channelId:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 334
    const-string/jumbo v1, "imageMissingTitle"

    iget-boolean v2, p0, Lcom/peel/content/listing/LiveListing;->imageMissingTitle:Z

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 336
    const-string/jumbo v1, "seasonal_show"

    iget-boolean v2, p0, Lcom/peel/content/listing/LiveListing;->seasonal_show:Z

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 337
    const-string/jumbo v1, "reminded"

    iget-boolean v2, p0, Lcom/peel/content/listing/LiveListing;->reminded:Z

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 338
    const-string/jumbo v1, "scheduledate"

    iget-object v2, p0, Lcom/peel/content/listing/LiveListing;->scheduledate:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 339
    const-string/jumbo v1, "reminderundo"

    iget-boolean v2, p0, Lcom/peel/content/listing/LiveListing;->k:Z

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 340
    const-string/jumbo v1, "episode_title"

    iget-object v2, p0, Lcom/peel/content/listing/LiveListing;->m:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 341
    iget-object v1, p0, Lcom/peel/content/listing/LiveListing;->teamInfo:[Lcom/peel/data/SportsTeam;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/peel/content/listing/LiveListing;->teamInfo:[Lcom/peel/data/SportsTeam;

    array-length v1, v1

    if-lez v1, :cond_0

    .line 342
    const-string/jumbo v1, "teamInfo"

    iget-object v2, p0, Lcom/peel/content/listing/LiveListing;->teamInfo:[Lcom/peel/data/SportsTeam;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelableArray(Ljava/lang/String;[Landroid/os/Parcelable;)V

    .line 345
    :cond_0
    iget-object v1, p0, Lcom/peel/content/listing/LiveListing;->tags:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string/jumbo v1, "tags"

    iget-object v2, p0, Lcom/peel/content/listing/LiveListing;->tags:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 346
    :cond_1
    iget-object v1, p0, Lcom/peel/content/listing/LiveListing;->json:Landroid/os/Bundle;

    if-eqz v1, :cond_2

    const-string/jumbo v1, "json"

    iget-object v2, p0, Lcom/peel/content/listing/LiveListing;->json:Landroid/os/Bundle;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 347
    :cond_2
    iget-object v1, p0, Lcom/peel/content/listing/LiveListing;->reminders:[Landroid/os/Bundle;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/peel/content/listing/LiveListing;->reminders:[Landroid/os/Bundle;

    array-length v1, v1

    if-lez v1, :cond_3

    .line 348
    const-string/jumbo v1, "reminders"

    iget-object v2, p0, Lcom/peel/content/listing/LiveListing;->reminders:[Landroid/os/Bundle;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelableArray(Ljava/lang/String;[Landroid/os/Parcelable;)V

    .line 350
    :cond_3
    iget-wide v2, p0, Lcom/peel/content/listing/LiveListing;->orig_air_date:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-lez v1, :cond_4

    .line 351
    const-string/jumbo v1, "orig_air_date"

    iget-wide v2, p0, Lcom/peel/content/listing/LiveListing;->orig_air_date:J

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 352
    :cond_4
    return-object v0
.end method

.method public a(Ljava/lang/String;)Ljava/net/URI;
    .locals 2

    .prologue
    .line 355
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "live://channel/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/net/URI;->create(Ljava/lang/String;)Ljava/net/URI;

    move-result-object v0

    return-object v0
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 249
    iget-object v0, p0, Lcom/peel/content/listing/LiveListing;->reminders:[Landroid/os/Bundle;

    if-nez v0, :cond_0

    .line 250
    const/4 v0, 0x1

    new-array v0, v0, [Landroid/os/Bundle;

    aput-object p1, v0, v3

    iput-object v0, p0, Lcom/peel/content/listing/LiveListing;->reminders:[Landroid/os/Bundle;

    .line 257
    :goto_0
    return-void

    .line 252
    :cond_0
    iget-object v0, p0, Lcom/peel/content/listing/LiveListing;->reminders:[Landroid/os/Bundle;

    array-length v0, v0

    add-int/lit8 v0, v0, 0x1

    new-array v0, v0, [Landroid/os/Bundle;

    .line 253
    iget-object v1, p0, Lcom/peel/content/listing/LiveListing;->reminders:[Landroid/os/Bundle;

    iget-object v2, p0, Lcom/peel/content/listing/LiveListing;->reminders:[Landroid/os/Bundle;

    array-length v2, v2

    invoke-static {v1, v3, v0, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 254
    iget-object v1, p0, Lcom/peel/content/listing/LiveListing;->reminders:[Landroid/os/Bundle;

    array-length v1, v1

    aput-object p1, v0, v1

    .line 255
    iput-object v0, p0, Lcom/peel/content/listing/LiveListing;->reminders:[Landroid/os/Bundle;

    goto :goto_0
.end method

.method public a(Lorg/json/JSONObject;)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 395
    :try_start_0
    const-string/jumbo v0, "live"

    iput-object v0, p0, Lcom/peel/content/listing/LiveListing;->h:Ljava/lang/String;

    .line 396
    const-string/jumbo v0, "1"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/content/listing/LiveListing;->b:Ljava/lang/String;

    .line 397
    const-string/jumbo v0, "2"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/content/listing/LiveListing;->d:Ljava/lang/String;

    .line 398
    const-string/jumbo v0, "3"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/content/listing/LiveListing;->j:Ljava/lang/String;

    .line 399
    const-string/jumbo v0, "4"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/content/listing/LiveListing;->m:Ljava/lang/String;

    .line 400
    const-string/jumbo v0, "6"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/content/listing/LiveListing;->c:Ljava/lang/String;

    .line 402
    const-string/jumbo v0, "9"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 403
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string/jumbo v2, "0"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v2

    if-nez v2, :cond_0

    .line 405
    :try_start_1
    new-instance v2, Ljava/text/SimpleDateFormat;

    const-string/jumbo v3, "yyyy-MM-dd"

    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v2, v3, v4}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 406
    const-string/jumbo v3, "GMT"

    invoke-static {v3}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/text/SimpleDateFormat;->setTimeZone(Ljava/util/TimeZone;)V

    .line 408
    invoke-virtual {v2, v0}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    .line 409
    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/peel/content/listing/LiveListing;->orig_air_date:J
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1

    .line 415
    :cond_0
    :goto_0
    :try_start_2
    const-string/jumbo v0, "11"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/content/listing/LiveListing;->title:Ljava/lang/String;

    .line 416
    const-string/jumbo v0, "12"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/content/listing/LiveListing;->l:Ljava/lang/String;

    .line 417
    const-string/jumbo v0, "13"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 419
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 420
    const-string/jumbo v2, "Scifi"

    const-string/jumbo v3, "Sci-Fi"

    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 421
    const-string/jumbo v2, ","

    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/content/listing/LiveListing;->e:[Ljava/lang/String;

    .line 424
    :cond_1
    const-string/jumbo v0, "image_3x4"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string/jumbo v0, "image_3x4"

    .line 425
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 426
    :goto_1
    iput-object v0, p0, Lcom/peel/content/listing/LiveListing;->image:Ljava/lang/String;

    .line 428
    const-string/jumbo v0, "24"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 429
    const-string/jumbo v0, "24"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v2

    .line 430
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    move v0, v1

    .line 432
    :goto_2
    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result v1

    if-ge v0, v1, :cond_3

    .line 433
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v4, ","

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 432
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 410
    :catch_0
    move-exception v0

    .line 411
    sget-object v2, Lcom/peel/content/listing/LiveListing;->i:Ljava/lang/String;

    sget-object v3, Lcom/peel/content/listing/LiveListing;->i:Ljava/lang/String;

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_1

    goto/16 :goto_0

    .line 458
    :catch_1
    move-exception v0

    .line 459
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    .line 461
    :goto_3
    return-void

    .line 425
    :cond_2
    :try_start_3
    const-string/jumbo v0, "14"

    .line 426
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 436
    :cond_3
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->deleteCharAt(I)Ljava/lang/StringBuilder;

    .line 438
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/content/listing/LiveListing;->n:Ljava/lang/String;

    .line 441
    :cond_4
    const-string/jumbo v0, "28"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 442
    const-string/jumbo v0, "28"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/content/listing/LiveListing;->p:Ljava/lang/String;

    .line 445
    :cond_5
    const-string/jumbo v0, "3"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/content/listing/LiveListing;->j:Ljava/lang/String;

    .line 447
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "live://"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "live"

    .line 448
    invoke-static {v1}, Lcom/peel/content/a;->c(Ljava/lang/String;)Lcom/peel/content/library/Library;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/content/library/Library;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/content/listing/LiveListing;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/content/listing/LiveListing;->a:Ljava/lang/String;

    .line 450
    iget-object v0, p0, Lcom/peel/content/listing/LiveListing;->teamInfo:[Lcom/peel/data/SportsTeam;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/peel/content/listing/LiveListing;->d:Ljava/lang/String;

    const-string/jumbo v1, "sports"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_7

    :cond_6
    iget-object v0, p0, Lcom/peel/content/listing/LiveListing;->j:Ljava/lang/String;

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/peel/content/listing/LiveListing;->j:Ljava/lang/String;

    const-string/jumbo v1, "0"

    .line 451
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_8

    iget-object v0, p0, Lcom/peel/content/listing/LiveListing;->d:Ljava/lang/String;

    const-string/jumbo v1, "sports"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_8

    .line 452
    :cond_7
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/peel/content/listing/LiveListing;->seasonal_show:Z

    .line 457
    :goto_4
    invoke-direct {p0}, Lcom/peel/content/listing/LiveListing;->C()V

    goto/16 :goto_3

    .line 454
    :cond_8
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/peel/content/listing/LiveListing;->seasonal_show:Z
    :try_end_3
    .catch Lorg/json/JSONException; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_4
.end method

.method public a(Z)V
    .locals 0

    .prologue
    .line 235
    iput-boolean p1, p0, Lcom/peel/content/listing/LiveListing;->reminded:Z

    .line 236
    return-void
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 206
    iget-object v0, p0, Lcom/peel/content/listing/LiveListing;->c:Ljava/lang/String;

    return-object v0
.end method

.method public b(Ljava/lang/String;)V
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 280
    iget-object v1, p0, Lcom/peel/content/listing/LiveListing;->reminders:[Landroid/os/Bundle;

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/peel/content/listing/LiveListing;->reminders:[Landroid/os/Bundle;

    array-length v2, v2

    if-gt v1, v2, :cond_1

    .line 281
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/peel/content/listing/LiveListing;->reminders:[Landroid/os/Bundle;

    .line 292
    :goto_0
    return-void

    .line 283
    :cond_1
    iget-object v1, p0, Lcom/peel/content/listing/LiveListing;->reminders:[Landroid/os/Bundle;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    new-array v3, v1, [Landroid/os/Bundle;

    .line 285
    iget-object v4, p0, Lcom/peel/content/listing/LiveListing;->reminders:[Landroid/os/Bundle;

    array-length v5, v4

    move v2, v0

    :goto_1
    if-ge v2, v5, :cond_4

    aget-object v6, v4, v2

    .line 286
    const-string/jumbo v1, "calendar_uri"

    invoke-virtual {v6, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/peel/content/listing/LiveListing;->reminders:[Landroid/os/Bundle;

    array-length v1, v1

    if-ne v0, v1, :cond_3

    .line 285
    :cond_2
    :goto_2
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_1

    .line 288
    :cond_3
    add-int/lit8 v1, v0, 0x1

    aput-object v6, v3, v0

    move v0, v1

    goto :goto_2

    .line 290
    :cond_4
    iput-object v3, p0, Lcom/peel/content/listing/LiveListing;->reminders:[Landroid/os/Bundle;

    goto :goto_0
.end method

.method public b(Z)V
    .locals 0

    .prologue
    .line 241
    iput-boolean p1, p0, Lcom/peel/content/listing/LiveListing;->k:Z

    .line 242
    return-void
.end method

.method public c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 210
    iget-object v0, p0, Lcom/peel/content/listing/LiveListing;->channel:Ljava/lang/String;

    return-object v0
.end method

.method public d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 214
    iget-object v0, p0, Lcom/peel/content/listing/LiveListing;->channelNumber:Ljava/lang/String;

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 360
    const v0, 0x16f59

    return v0
.end method

.method public e()Ljava/net/URI;
    .locals 2

    .prologue
    .line 357
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "live://channel/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/content/listing/LiveListing;->channelNumber:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/net/URI;->create(Ljava/lang/String;)Ljava/net/URI;

    move-result-object v0

    return-object v0
.end method

.method public o()J
    .locals 2

    .prologue
    .line 217
    iget-wide v0, p0, Lcom/peel/content/listing/LiveListing;->start:J

    return-wide v0
.end method

.method public p()Z
    .locals 1

    .prologue
    .line 220
    iget-boolean v0, p0, Lcom/peel/content/listing/LiveListing;->seasonal_show:Z

    return v0
.end method

.method public q()Ljava/lang/String;
    .locals 1

    .prologue
    .line 223
    iget-object v0, p0, Lcom/peel/content/listing/LiveListing;->channelId:Ljava/lang/String;

    return-object v0
.end method

.method public r()Ljava/lang/String;
    .locals 1

    .prologue
    .line 226
    iget-object v0, p0, Lcom/peel/content/listing/LiveListing;->scheduledate:Ljava/lang/String;

    return-object v0
.end method

.method public s()[Lcom/peel/data/SportsTeam;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 230
    iget-object v0, p0, Lcom/peel/content/listing/LiveListing;->teamInfo:[Lcom/peel/data/SportsTeam;

    return-object v0
.end method

.method public t()Z
    .locals 1

    .prologue
    .line 238
    iget-boolean v0, p0, Lcom/peel/content/listing/LiveListing;->k:Z

    return v0
.end method

.method public u()Ljava/lang/String;
    .locals 1

    .prologue
    .line 260
    iget-object v0, p0, Lcom/peel/content/listing/LiveListing;->tags:Ljava/lang/String;

    return-object v0
.end method

.method public v()Ljava/lang/String;
    .locals 1

    .prologue
    .line 268
    iget-object v0, p0, Lcom/peel/content/listing/LiveListing;->m:Ljava/lang/String;

    return-object v0
.end method

.method public w()Ljava/lang/String;
    .locals 1

    .prologue
    .line 276
    iget-object v0, p0, Lcom/peel/content/listing/LiveListing;->p:Ljava/lang/String;

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 363
    iget-object v1, p0, Lcom/peel/content/listing/LiveListing;->a:Ljava/lang/String;

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 364
    iget-object v1, p0, Lcom/peel/content/listing/LiveListing;->b:Ljava/lang/String;

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 365
    iget-object v1, p0, Lcom/peel/content/listing/LiveListing;->c:Ljava/lang/String;

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 366
    iget-object v1, p0, Lcom/peel/content/listing/LiveListing;->d:Ljava/lang/String;

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 367
    iget-object v1, p0, Lcom/peel/content/listing/LiveListing;->title:Ljava/lang/String;

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 368
    iget-object v1, p0, Lcom/peel/content/listing/LiveListing;->e:[Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/peel/content/listing/LiveListing;->e:[Ljava/lang/String;

    array-length v1, v1

    if-nez v1, :cond_2

    .line 369
    :cond_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 374
    :goto_0
    iget v1, p0, Lcom/peel/content/listing/LiveListing;->f:I

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 375
    iget-wide v2, p0, Lcom/peel/content/listing/LiveListing;->g:J

    invoke-virtual {p1, v2, v3}, Landroid/os/Parcel;->writeLong(J)V

    .line 376
    iget-object v1, p0, Lcom/peel/content/listing/LiveListing;->m:Ljava/lang/String;

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 377
    iget-object v1, p0, Lcom/peel/content/listing/LiveListing;->l:Ljava/lang/String;

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 378
    iget-object v1, p0, Lcom/peel/content/listing/LiveListing;->channel:Ljava/lang/String;

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 379
    iget-object v1, p0, Lcom/peel/content/listing/LiveListing;->channelNumber:Ljava/lang/String;

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 380
    iget-object v1, p0, Lcom/peel/content/listing/LiveListing;->channelId:Ljava/lang/String;

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 381
    iget-wide v2, p0, Lcom/peel/content/listing/LiveListing;->start:J

    invoke-virtual {p1, v2, v3}, Landroid/os/Parcel;->writeLong(J)V

    .line 382
    iget-wide v2, p0, Lcom/peel/content/listing/LiveListing;->orig_air_date:J

    invoke-virtual {p1, v2, v3}, Landroid/os/Parcel;->writeLong(J)V

    .line 383
    iget-object v1, p0, Lcom/peel/content/listing/LiveListing;->image:Ljava/lang/String;

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 384
    iget-object v1, p0, Lcom/peel/content/listing/LiveListing;->n:Ljava/lang/String;

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 385
    iget-object v1, p0, Lcom/peel/content/listing/LiveListing;->j:Ljava/lang/String;

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 386
    iget-object v1, p0, Lcom/peel/content/listing/LiveListing;->tags:Ljava/lang/String;

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 387
    iget-object v1, p0, Lcom/peel/content/listing/LiveListing;->teamInfo:[Lcom/peel/data/SportsTeam;

    invoke-virtual {p1, v1, v0}, Landroid/os/Parcel;->writeParcelableArray([Landroid/os/Parcelable;I)V

    .line 388
    iget-object v1, p0, Lcom/peel/content/listing/LiveListing;->scheduledate:Ljava/lang/String;

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 389
    iget-boolean v1, p0, Lcom/peel/content/listing/LiveListing;->k:Z

    if-eqz v1, :cond_1

    const/4 v0, 0x1

    :cond_1
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 390
    iget-object v0, p0, Lcom/peel/content/listing/LiveListing;->p:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 391
    return-void

    .line 371
    :cond_2
    iget-object v1, p0, Lcom/peel/content/listing/LiveListing;->e:[Ljava/lang/String;

    array-length v1, v1

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 372
    iget-object v1, p0, Lcom/peel/content/listing/LiveListing;->e:[Ljava/lang/String;

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeStringArray([Ljava/lang/String;)V

    goto :goto_0
.end method

.method public x()Ljava/lang/String;
    .locals 1

    .prologue
    .line 309
    iget-object v0, p0, Lcom/peel/content/listing/LiveListing;->l:Ljava/lang/String;

    return-object v0
.end method

.method public y()Ljava/lang/String;
    .locals 1

    .prologue
    .line 313
    iget-object v0, p0, Lcom/peel/content/listing/LiveListing;->image:Ljava/lang/String;

    return-object v0
.end method

.method public z()Ljava/lang/String;
    .locals 1

    .prologue
    .line 317
    iget-object v0, p0, Lcom/peel/content/listing/LiveListing;->n:Ljava/lang/String;

    return-object v0
.end method
