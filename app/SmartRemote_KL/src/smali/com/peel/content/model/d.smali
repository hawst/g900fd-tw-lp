.class final Lcom/peel/content/model/d;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/peel/content/model/Season;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/os/Parcel;)Lcom/peel/content/model/Season;
    .locals 2

    .prologue
    .line 14
    new-instance v0, Lcom/peel/content/model/Season;

    const/4 v1, 0x0

    invoke-direct {v0, p1, v1}, Lcom/peel/content/model/Season;-><init>(Landroid/os/Parcel;Lcom/peel/content/model/d;)V

    return-object v0
.end method

.method public a(I)[Lcom/peel/content/model/Season;
    .locals 1

    .prologue
    .line 16
    new-array v0, p1, [Lcom/peel/content/model/Season;

    return-object v0
.end method

.method public synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 13
    invoke-virtual {p0, p1}, Lcom/peel/content/model/d;->a(Landroid/os/Parcel;)Lcom/peel/content/model/Season;

    move-result-object v0

    return-object v0
.end method

.method public synthetic newArray(I)[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 13
    invoke-virtual {p0, p1}, Lcom/peel/content/model/d;->a(I)[Lcom/peel/content/model/Season;

    move-result-object v0

    return-object v0
.end method
