.class final Lcom/peel/content/c;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic b:Z

.field final synthetic c:Lcom/peel/util/t;


# direct methods
.method constructor <init>(Ljava/lang/String;ZLcom/peel/util/t;)V
    .locals 0

    .prologue
    .line 101
    iput-object p1, p0, Lcom/peel/content/c;->a:Ljava/lang/String;

    iput-boolean p2, p0, Lcom/peel/content/c;->b:Z

    iput-object p3, p0, Lcom/peel/content/c;->c:Lcom/peel/util/t;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 15

    .prologue
    .line 104
    :try_start_0
    sget-object v0, Lcom/peel/content/a;->f:Lcom/peel/content/user/User;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_6

    if-nez v0, :cond_1

    .line 199
    sget-object v1, Lcom/peel/content/a;->h:Ljava/util/concurrent/atomic/AtomicBoolean;

    monitor-enter v1

    .line 200
    :try_start_1
    sget-object v0, Lcom/peel/content/a;->h:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 201
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 202
    sget-object v1, Lcom/peel/content/a;->b:Lcom/peel/content/i;

    const/4 v2, 0x1

    iget-boolean v0, p0, Lcom/peel/content/c;->b:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    const/4 v0, 0x0

    check-cast v0, [Ljava/lang/Object;

    invoke-virtual {v1, v2, v3, v0}, Lcom/peel/content/i;->a(ILjava/lang/Object;[Ljava/lang/Object;)V

    .line 203
    iget-object v0, p0, Lcom/peel/content/c;->c:Lcom/peel/util/t;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/peel/content/c;->c:Lcom/peel/util/t;

    const/4 v1, 0x1

    invoke-static {}, Lcom/peel/content/a;->g()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lcom/peel/util/t;->a(ZLjava/lang/Object;Ljava/lang/String;)V

    .line 205
    :cond_0
    :goto_0
    return-void

    .line 201
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 110
    :cond_1
    :try_start_3
    sget-object v0, Lcom/peel/content/a;->f:Lcom/peel/content/user/User;

    invoke-virtual {v0}, Lcom/peel/content/user/User;->k()Z
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_6

    move-result v0

    if-nez v0, :cond_2

    .line 199
    sget-object v1, Lcom/peel/content/a;->h:Ljava/util/concurrent/atomic/AtomicBoolean;

    monitor-enter v1

    .line 200
    :try_start_4
    sget-object v0, Lcom/peel/content/a;->h:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 201
    monitor-exit v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 202
    sget-object v1, Lcom/peel/content/a;->b:Lcom/peel/content/i;

    const/4 v2, 0x1

    iget-boolean v0, p0, Lcom/peel/content/c;->b:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    const/4 v0, 0x0

    check-cast v0, [Ljava/lang/Object;

    invoke-virtual {v1, v2, v3, v0}, Lcom/peel/content/i;->a(ILjava/lang/Object;[Ljava/lang/Object;)V

    .line 203
    iget-object v0, p0, Lcom/peel/content/c;->c:Lcom/peel/util/t;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/peel/content/c;->c:Lcom/peel/util/t;

    const/4 v1, 0x1

    invoke-static {}, Lcom/peel/content/a;->g()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lcom/peel/util/t;->a(ZLjava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 201
    :catchall_1
    move-exception v0

    :try_start_5
    monitor-exit v1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    throw v0

    .line 115
    :cond_2
    :try_start_6
    sget-object v0, Lcom/peel/content/a;->f:Lcom/peel/content/user/User;

    invoke-virtual {v0}, Lcom/peel/content/user/User;->l()[Lcom/peel/data/ContentRoom;

    move-result-object v3

    .line 117
    const/4 v1, 0x0

    .line 118
    array-length v4, v3

    const/4 v0, 0x0

    move v2, v0

    :goto_1
    if-ge v2, v4, :cond_3

    aget-object v0, v3, v2

    .line 120
    if-eqz v0, :cond_f

    invoke-virtual {v0}, Lcom/peel/data/ContentRoom;->a()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/peel/content/c;->a:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_f

    .line 118
    :goto_2
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move-object v1, v0

    goto :goto_1

    .line 126
    :cond_3
    if-nez v1, :cond_e

    array-length v0, v3

    add-int/lit8 v0, v0, -0x1

    aget-object v1, v3, v0
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_0
    .catchall {:try_start_6 .. :try_end_6} :catchall_6

    move-object v3, v1

    .line 128
    :goto_3
    if-nez v3, :cond_4

    .line 199
    sget-object v1, Lcom/peel/content/a;->h:Ljava/util/concurrent/atomic/AtomicBoolean;

    monitor-enter v1

    .line 200
    :try_start_7
    sget-object v0, Lcom/peel/content/a;->h:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 201
    monitor-exit v1
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 202
    sget-object v1, Lcom/peel/content/a;->b:Lcom/peel/content/i;

    const/4 v2, 0x1

    iget-boolean v0, p0, Lcom/peel/content/c;->b:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    const/4 v0, 0x0

    check-cast v0, [Ljava/lang/Object;

    invoke-virtual {v1, v2, v3, v0}, Lcom/peel/content/i;->a(ILjava/lang/Object;[Ljava/lang/Object;)V

    .line 203
    iget-object v0, p0, Lcom/peel/content/c;->c:Lcom/peel/util/t;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/peel/content/c;->c:Lcom/peel/util/t;

    const/4 v1, 0x1

    invoke-static {}, Lcom/peel/content/a;->g()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lcom/peel/util/t;->a(ZLjava/lang/Object;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 201
    :catchall_2
    move-exception v0

    :try_start_8
    monitor-exit v1
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    throw v0

    .line 134
    :cond_4
    :try_start_9
    invoke-virtual {v3}, Lcom/peel/data/ContentRoom;->g()[Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/peel/data/m;->a([Ljava/lang/String;)[Lcom/peel/data/k;

    move-result-object v4

    .line 135
    if-eqz v4, :cond_5

    array-length v0, v4

    if-nez v0, :cond_6

    .line 137
    :cond_5
    invoke-static {}, Lcom/peel/content/a;->f()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "......NO libs from PeelData.getLibraries for room: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v3}, Lcom/peel/data/ContentRoom;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 139
    invoke-static {}, Lcom/peel/content/a;->g()Ljava/lang/String;

    move-result-object v1

    monitor-enter v1
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_0
    .catchall {:try_start_9 .. :try_end_9} :catchall_6

    .line 140
    :try_start_a
    invoke-virtual {v3}, Lcom/peel/data/ContentRoom;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/peel/content/a;->f(Ljava/lang/String;)Ljava/lang/String;

    .line 141
    monitor-exit v1
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_3

    .line 199
    sget-object v1, Lcom/peel/content/a;->h:Ljava/util/concurrent/atomic/AtomicBoolean;

    monitor-enter v1

    .line 200
    :try_start_b
    sget-object v0, Lcom/peel/content/a;->h:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 201
    monitor-exit v1
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_4

    .line 202
    sget-object v1, Lcom/peel/content/a;->b:Lcom/peel/content/i;

    const/4 v2, 0x1

    iget-boolean v0, p0, Lcom/peel/content/c;->b:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    const/4 v0, 0x0

    check-cast v0, [Ljava/lang/Object;

    invoke-virtual {v1, v2, v3, v0}, Lcom/peel/content/i;->a(ILjava/lang/Object;[Ljava/lang/Object;)V

    .line 203
    iget-object v0, p0, Lcom/peel/content/c;->c:Lcom/peel/util/t;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/peel/content/c;->c:Lcom/peel/util/t;

    const/4 v1, 0x1

    invoke-static {}, Lcom/peel/content/a;->g()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lcom/peel/util/t;->a(ZLjava/lang/Object;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 141
    :catchall_3
    move-exception v0

    :try_start_c
    monitor-exit v1
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_3

    :try_start_d
    throw v0
    :try_end_d
    .catch Ljava/lang/Exception; {:try_start_d .. :try_end_d} :catch_0
    .catchall {:try_start_d .. :try_end_d} :catchall_6

    .line 196
    :catch_0
    move-exception v0

    .line 197
    :try_start_e
    invoke-static {}, Lcom/peel/content/a;->f()Ljava/lang/String;

    move-result-object v1

    invoke-static {}, Lcom/peel/content/a;->f()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_6

    .line 199
    sget-object v1, Lcom/peel/content/a;->h:Ljava/util/concurrent/atomic/AtomicBoolean;

    monitor-enter v1

    .line 200
    :try_start_f
    sget-object v0, Lcom/peel/content/a;->h:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 201
    monitor-exit v1
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_8

    .line 202
    sget-object v1, Lcom/peel/content/a;->b:Lcom/peel/content/i;

    const/4 v2, 0x1

    iget-boolean v0, p0, Lcom/peel/content/c;->b:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    const/4 v0, 0x0

    check-cast v0, [Ljava/lang/Object;

    invoke-virtual {v1, v2, v3, v0}, Lcom/peel/content/i;->a(ILjava/lang/Object;[Ljava/lang/Object;)V

    .line 203
    iget-object v0, p0, Lcom/peel/content/c;->c:Lcom/peel/util/t;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/peel/content/c;->c:Lcom/peel/util/t;

    const/4 v1, 0x1

    invoke-static {}, Lcom/peel/content/a;->g()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lcom/peel/util/t;->a(ZLjava/lang/Object;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 201
    :catchall_4
    move-exception v0

    :try_start_10
    monitor-exit v1
    :try_end_10
    .catchall {:try_start_10 .. :try_end_10} :catchall_4

    throw v0

    .line 146
    :cond_6
    :try_start_11
    invoke-static {}, Lcom/peel/content/a;->g()Ljava/lang/String;

    move-result-object v1

    monitor-enter v1
    :try_end_11
    .catch Ljava/lang/Exception; {:try_start_11 .. :try_end_11} :catch_0
    .catchall {:try_start_11 .. :try_end_11} :catchall_6

    .line 147
    :try_start_12
    invoke-virtual {v3}, Lcom/peel/data/ContentRoom;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/peel/content/a;->f(Ljava/lang/String;)Ljava/lang/String;

    .line 148
    monitor-exit v1
    :try_end_12
    .catchall {:try_start_12 .. :try_end_12} :catchall_5

    .line 151
    :try_start_13
    invoke-static {}, Lcom/peel/content/a;->f()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "......PeelData.libs length: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    array-length v2, v4

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 153
    sget-object v0, Lcom/peel/content/a;->f:Lcom/peel/content/user/User;

    invoke-virtual {v0}, Lcom/peel/content/user/User;->h()Landroid/os/Bundle;

    move-result-object v5

    .line 154
    sget-object v0, Lcom/peel/content/a;->f:Lcom/peel/content/user/User;

    invoke-virtual {v0}, Lcom/peel/content/user/User;->f()Landroid/os/Bundle;

    move-result-object v6

    .line 155
    sget-object v0, Lcom/peel/content/a;->f:Lcom/peel/content/user/User;

    invoke-virtual {v0}, Lcom/peel/content/user/User;->j()Landroid/os/Bundle;

    move-result-object v7

    .line 157
    array-length v0, v4

    new-array v8, v0, [Lcom/peel/content/library/Library;

    .line 158
    const/4 v0, 0x0

    move v2, v0

    :goto_4
    array-length v0, v8

    if-ge v2, v0, :cond_d

    .line 159
    aget-object v0, v4, v2

    invoke-virtual {v0}, Lcom/peel/data/k;->b()Ljava/lang/String;

    move-result-object v0

    aget-object v1, v4, v2

    invoke-virtual {v1}, Lcom/peel/data/k;->a()Ljava/lang/String;

    move-result-object v1

    aget-object v9, v4, v2

    invoke-virtual {v9}, Lcom/peel/data/k;->d()Landroid/os/Bundle;

    move-result-object v9

    invoke-static {v0, v1, v9}, Lcom/peel/content/library/Library;->a(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Lcom/peel/content/library/Library;

    move-result-object v0

    aput-object v0, v8, v2

    .line 161
    aget-object v0, v4, v2

    invoke-virtual {v0}, Lcom/peel/data/k;->b()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "live"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 162
    aget-object v0, v8, v2

    check-cast v0, Lcom/peel/content/library/LiveLibrary;

    .line 163
    invoke-virtual {v0}, Lcom/peel/content/library/LiveLibrary;->i()[Lcom/peel/data/Channel;

    move-result-object v9

    .line 164
    if-eqz v9, :cond_c

    array-length v1, v9

    if-lez v1, :cond_c

    .line 166
    const-string/jumbo v1, "favchannels"

    invoke-virtual {v5, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 167
    const-string/jumbo v1, "favchannels"

    invoke-virtual {v5, v1}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    .line 171
    :goto_5
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    .line 172
    invoke-virtual {v3}, Lcom/peel/data/ContentRoom;->a()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string/jumbo v11, "/"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v0}, Lcom/peel/content/library/LiveLibrary;->e()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 171
    invoke-virtual {v6, v0}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v10

    .line 173
    array-length v11, v9

    const/4 v0, 0x0

    :goto_6
    if-ge v0, v11, :cond_c

    aget-object v12, v9, v0

    .line 174
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3}, Lcom/peel/data/ContentRoom;->a()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string/jumbo v14, "/"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    aget-object v14, v8, v2

    invoke-virtual {v14}, Lcom/peel/content/library/Library;->e()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string/jumbo v14, "/"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v12}, Lcom/peel/data/Channel;->a()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v7, v13}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    .line 175
    if-eqz v13, :cond_7

    invoke-virtual {v12, v13}, Lcom/peel/data/Channel;->b(Ljava/lang/String;)V

    .line 177
    :cond_7
    if-eqz v1, :cond_a

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v12}, Lcom/peel/data/Channel;->a()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string/jumbo v14, "#"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v12}, Lcom/peel/data/Channel;->c()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string/jumbo v14, "#"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v12}, Lcom/peel/data/Channel;->e()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string/jumbo v14, "#"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v12}, Lcom/peel/data/Channel;->f()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string/jumbo v14, "#"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v12}, Lcom/peel/data/Channel;->i()I

    move-result v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-interface {v1, v13}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_a

    .line 178
    const/4 v13, 0x1

    invoke-virtual {v12, v13}, Lcom/peel/data/Channel;->a(Z)V
    :try_end_13
    .catch Ljava/lang/Exception; {:try_start_13 .. :try_end_13} :catch_0
    .catchall {:try_start_13 .. :try_end_13} :catchall_6

    .line 173
    :goto_7
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_6

    .line 148
    :catchall_5
    move-exception v0

    :try_start_14
    monitor-exit v1
    :try_end_14
    .catchall {:try_start_14 .. :try_end_14} :catchall_5

    :try_start_15
    throw v0
    :try_end_15
    .catch Ljava/lang/Exception; {:try_start_15 .. :try_end_15} :catch_0
    .catchall {:try_start_15 .. :try_end_15} :catchall_6

    .line 199
    :catchall_6
    move-exception v0

    move-object v1, v0

    sget-object v2, Lcom/peel/content/a;->h:Ljava/util/concurrent/atomic/AtomicBoolean;

    monitor-enter v2

    .line 200
    :try_start_16
    sget-object v0, Lcom/peel/content/a;->h:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 201
    monitor-exit v2
    :try_end_16
    .catchall {:try_start_16 .. :try_end_16} :catchall_9

    .line 202
    sget-object v2, Lcom/peel/content/a;->b:Lcom/peel/content/i;

    const/4 v3, 0x1

    iget-boolean v0, p0, Lcom/peel/content/c;->b:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    const/4 v0, 0x0

    check-cast v0, [Ljava/lang/Object;

    invoke-virtual {v2, v3, v4, v0}, Lcom/peel/content/i;->a(ILjava/lang/Object;[Ljava/lang/Object;)V

    .line 203
    iget-object v0, p0, Lcom/peel/content/c;->c:Lcom/peel/util/t;

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/peel/content/c;->c:Lcom/peel/util/t;

    const/4 v2, 0x1

    invoke-static {}, Lcom/peel/content/a;->g()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v0, v2, v3, v4}, Lcom/peel/util/t;->a(ZLjava/lang/Object;Ljava/lang/String;)V

    :cond_8
    throw v1

    .line 169
    :cond_9
    :try_start_17
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    goto/16 :goto_5

    .line 182
    :cond_a
    if-eqz v10, :cond_b

    invoke-virtual {v12}, Lcom/peel/data/Channel;->a()Ljava/lang/String;

    move-result-object v13

    invoke-interface {v10, v13}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_b

    .line 183
    const/4 v13, 0x1

    invoke-virtual {v12, v13}, Lcom/peel/data/Channel;->b(Z)V

    goto :goto_7

    .line 187
    :cond_b
    const/4 v13, 0x0

    invoke-virtual {v12, v13}, Lcom/peel/data/Channel;->b(Z)V

    .line 188
    const/4 v13, 0x0

    invoke-virtual {v12, v13}, Lcom/peel/data/Channel;->a(Z)V

    goto :goto_7

    .line 158
    :cond_c
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto/16 :goto_4

    .line 195
    :cond_d
    invoke-static {}, Lcom/peel/content/a;->h()Ljava/util/List;

    move-result-object v0

    invoke-static {v8}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z
    :try_end_17
    .catch Ljava/lang/Exception; {:try_start_17 .. :try_end_17} :catch_0
    .catchall {:try_start_17 .. :try_end_17} :catchall_6

    .line 199
    sget-object v1, Lcom/peel/content/a;->h:Ljava/util/concurrent/atomic/AtomicBoolean;

    monitor-enter v1

    .line 200
    :try_start_18
    sget-object v0, Lcom/peel/content/a;->h:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 201
    monitor-exit v1
    :try_end_18
    .catchall {:try_start_18 .. :try_end_18} :catchall_7

    .line 202
    sget-object v1, Lcom/peel/content/a;->b:Lcom/peel/content/i;

    const/4 v2, 0x1

    iget-boolean v0, p0, Lcom/peel/content/c;->b:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    const/4 v0, 0x0

    check-cast v0, [Ljava/lang/Object;

    invoke-virtual {v1, v2, v3, v0}, Lcom/peel/content/i;->a(ILjava/lang/Object;[Ljava/lang/Object;)V

    .line 203
    iget-object v0, p0, Lcom/peel/content/c;->c:Lcom/peel/util/t;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/peel/content/c;->c:Lcom/peel/util/t;

    const/4 v1, 0x1

    invoke-static {}, Lcom/peel/content/a;->g()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lcom/peel/util/t;->a(ZLjava/lang/Object;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 201
    :catchall_7
    move-exception v0

    :try_start_19
    monitor-exit v1
    :try_end_19
    .catchall {:try_start_19 .. :try_end_19} :catchall_7

    throw v0

    :catchall_8
    move-exception v0

    :try_start_1a
    monitor-exit v1
    :try_end_1a
    .catchall {:try_start_1a .. :try_end_1a} :catchall_8

    throw v0

    :catchall_9
    move-exception v0

    :try_start_1b
    monitor-exit v2
    :try_end_1b
    .catchall {:try_start_1b .. :try_end_1b} :catchall_9

    throw v0

    :cond_e
    move-object v3, v1

    goto/16 :goto_3

    :cond_f
    move-object v0, v1

    goto/16 :goto_2
.end method
