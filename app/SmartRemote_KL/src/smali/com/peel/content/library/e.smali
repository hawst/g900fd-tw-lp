.class Lcom/peel/content/library/e;
.super Landroid/util/LruCache;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/util/LruCache",
        "<",
        "Ljava/lang/String;",
        "Lcom/peel/content/listing/Listing;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/peel/content/library/LiveLibrary;


# direct methods
.method constructor <init>(Lcom/peel/content/library/LiveLibrary;I)V
    .locals 0

    .prologue
    .line 39
    iput-object p1, p0, Lcom/peel/content/library/e;->a:Lcom/peel/content/library/LiveLibrary;

    invoke-direct {p0, p2}, Landroid/util/LruCache;-><init>(I)V

    return-void
.end method


# virtual methods
.method protected a(ZLjava/lang/String;Lcom/peel/content/listing/Listing;Lcom/peel/content/listing/Listing;)V
    .locals 3

    .prologue
    .line 41
    if-eqz p1, :cond_0

    .line 42
    invoke-static {}, Lcom/peel/content/library/LiveLibrary;->p()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/peel/content/library/e;->a:Lcom/peel/content/library/LiveLibrary;

    invoke-static {v2}, Lcom/peel/content/library/LiveLibrary;->a(Lcom/peel/content/library/LiveLibrary;)Landroid/util/LruCache;

    move-result-object v2

    invoke-virtual {v2}, Landroid/util/LruCache;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "] evicted: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 44
    :cond_0
    return-void
.end method

.method protected synthetic entryRemoved(ZLjava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 39
    check-cast p2, Ljava/lang/String;

    check-cast p3, Lcom/peel/content/listing/Listing;

    check-cast p4, Lcom/peel/content/listing/Listing;

    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/peel/content/library/e;->a(ZLjava/lang/String;Lcom/peel/content/listing/Listing;Lcom/peel/content/listing/Listing;)V

    return-void
.end method
