.class public final Lcom/peel/content/library/DirecTVLibrary;
.super Lcom/peel/content/library/Library;


# static fields
.field private static final c:Ljava/lang/String;

.field private static d:Z


# instance fields
.field private final e:Ljava/lang/String;

.field private f:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/peel/content/listing/Listing;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 29
    const-class v0, Lcom/peel/content/library/DirecTVLibrary;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/peel/content/library/DirecTVLibrary;->c:Ljava/lang/String;

    .line 30
    const/4 v0, 0x0

    sput-boolean v0, Lcom/peel/content/library/DirecTVLibrary;->d:Z

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 35
    const-string/jumbo v0, "dtv"

    invoke-direct {p0, p1, v0}, Lcom/peel/content/library/Library;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 32
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/peel/content/library/DirecTVLibrary;->f:Ljava/util/Map;

    .line 36
    iput-object p2, p0, Lcom/peel/content/library/DirecTVLibrary;->e:Ljava/lang/String;

    .line 37
    return-void
.end method

.method static synthetic a(Lcom/peel/content/library/DirecTVLibrary;)Ljava/util/Map;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lcom/peel/content/library/DirecTVLibrary;->f:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic a(Lcom/peel/content/library/DirecTVLibrary;Landroid/os/Bundle;Lcom/peel/util/t;Lcom/peel/util/t;)V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0, p1, p2, p3}, Lcom/peel/content/library/DirecTVLibrary;->c(Landroid/os/Bundle;Lcom/peel/util/t;Lcom/peel/util/t;)V

    return-void
.end method

.method static synthetic a(Lcom/peel/content/library/DirecTVLibrary;[Ljava/lang/String;[Lcom/peel/content/listing/Listing;Landroid/os/Bundle;Lcom/peel/util/t;Z)V
    .locals 0

    .prologue
    .line 28
    invoke-direct/range {p0 .. p5}, Lcom/peel/content/library/DirecTVLibrary;->a([Ljava/lang/String;[Lcom/peel/content/listing/Listing;Landroid/os/Bundle;Lcom/peel/util/t;Z)V

    return-void
.end method

.method private a([Ljava/lang/String;[Lcom/peel/content/listing/Listing;Landroid/os/Bundle;Lcom/peel/util/t;Z)V
    .locals 5

    .prologue
    .line 214
    sget-object v0, Lcom/peel/content/library/DirecTVLibrary;->c:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "...updateListings... size: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    array-length v2, p2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 216
    array-length v1, p2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    aget-object v2, p2, v0

    .line 217
    iget-object v3, p0, Lcom/peel/content/library/DirecTVLibrary;->f:Ljava/util/Map;

    invoke-virtual {v2}, Lcom/peel/content/listing/Listing;->f()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 216
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 219
    :cond_0
    return-void
.end method

.method static synthetic a(Z)Z
    .locals 0

    .prologue
    .line 28
    sput-boolean p0, Lcom/peel/content/library/DirecTVLibrary;->d:Z

    return p0
.end method

.method private b(Landroid/os/Bundle;Lcom/peel/util/t;Lcom/peel/util/t;)V
    .locals 9

    .prologue
    .line 148
    sget-object v0, Lcom/peel/content/library/DirecTVLibrary;->c:Ljava/lang/String;

    const-string/jumbo v1, "...loadListingsFromDB..."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 149
    const/4 v0, 0x1

    sput-boolean v0, Lcom/peel/content/library/DirecTVLibrary;->d:Z

    .line 150
    invoke-static {}, Lcom/peel/data/m;->a()Lcom/peel/data/m;

    move-result-object v6

    iget-object v7, p0, Lcom/peel/content/library/DirecTVLibrary;->a:Ljava/lang/String;

    const/4 v8, 0x0

    new-instance v0, Lcom/peel/content/library/b;

    const/4 v2, 0x2

    move-object v1, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/peel/content/library/b;-><init>(Lcom/peel/content/library/DirecTVLibrary;ILandroid/os/Bundle;Lcom/peel/util/t;Lcom/peel/util/t;)V

    invoke-virtual {v6, v7, v8, v0}, Lcom/peel/data/m;->a(Ljava/lang/String;[Ljava/lang/String;Lcom/peel/util/t;)V

    .line 175
    return-void
.end method

.method private c(Landroid/os/Bundle;Lcom/peel/util/t;Lcom/peel/util/t;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 178
    sget-object v0, Lcom/peel/content/library/DirecTVLibrary;->c:Ljava/lang/String;

    const-string/jumbo v1, "...loadListingsFromDtv..."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 180
    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    iget-object v1, p0, Lcom/peel/content/library/DirecTVLibrary;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/peel/control/am;->c(Ljava/lang/String;)Lcom/peel/control/h;

    move-result-object v0

    .line 181
    invoke-static {}, Lcom/peel/control/c/a;->a()Ljava/net/InetAddress;

    move-result-object v1

    .line 182
    invoke-virtual {v1}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v3, "."

    invoke-virtual {v1, v3}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v2, v4, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v2

    invoke-virtual {v2}, Lcom/peel/data/g;->i()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/data/g;->i()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v3, "."

    invoke-virtual {v0, v3}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {v2, v4, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 183
    const/4 v0, 0x1

    sput-boolean v0, Lcom/peel/content/library/DirecTVLibrary;->d:Z

    .line 184
    iget-object v0, p0, Lcom/peel/content/library/DirecTVLibrary;->a:Ljava/lang/String;

    iget-object v1, p0, Lcom/peel/content/library/DirecTVLibrary;->e:Ljava/lang/String;

    new-instance v2, Lcom/peel/content/library/c;

    invoke-direct {v2, p0, v4, p2, p1}, Lcom/peel/content/library/c;-><init>(Lcom/peel/content/library/DirecTVLibrary;ILcom/peel/util/t;Landroid/os/Bundle;)V

    invoke-static {v0, v1, p3, v2}, Lcom/peel/content/a/b;->a(Ljava/lang/String;Ljava/lang/String;Lcom/peel/util/t;Lcom/peel/util/t;)V

    .line 211
    :goto_0
    return-void

    .line 209
    :cond_0
    const/4 v0, 0x0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "directv ip "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/peel/content/library/DirecTVLibrary;->e:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " is not on the same subnet as the device IP"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v4, v0, v1}, Lcom/peel/util/t;->a(ZLjava/lang/Object;Ljava/lang/String;)V

    goto :goto_0
.end method

.method static synthetic d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 28
    sget-object v0, Lcom/peel/content/library/DirecTVLibrary;->c:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public a()Landroid/os/Bundle;
    .locals 3

    .prologue
    .line 40
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 41
    const-string/jumbo v1, "directvDeviceId"

    iget-object v2, p0, Lcom/peel/content/library/DirecTVLibrary;->e:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 42
    return-object v0
.end method

.method public a(Landroid/os/Bundle;Lcom/peel/util/t;)V
    .locals 1

    .prologue
    .line 144
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lcom/peel/content/library/DirecTVLibrary;->a(Landroid/os/Bundle;Lcom/peel/util/t;Lcom/peel/util/t;)V

    .line 145
    return-void
.end method

.method public a(Landroid/os/Bundle;Lcom/peel/util/t;Lcom/peel/util/t;)V
    .locals 8

    .prologue
    const/4 v0, 0x5

    const/4 v7, 0x0

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 84
    sget-boolean v1, Lcom/peel/content/library/DirecTVLibrary;->d:Z

    if-eqz v1, :cond_0

    .line 85
    const-string/jumbo v0, "libray is in the process of loading content"

    invoke-virtual {p2, v5, v7, v0}, Lcom/peel/util/t;->a(ZLjava/lang/Object;Ljava/lang/String;)V

    .line 140
    :goto_0
    return-void

    .line 88
    :cond_0
    const-string/jumbo v1, "force_loading"

    invoke-virtual {p1, v1, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 89
    invoke-direct {p0, p1, p2, p3}, Lcom/peel/content/library/DirecTVLibrary;->c(Landroid/os/Bundle;Lcom/peel/util/t;Lcom/peel/util/t;)V

    goto :goto_0

    .line 93
    :cond_1
    iget-object v1, p0, Lcom/peel/content/library/DirecTVLibrary;->f:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->size()I

    move-result v1

    if-nez v1, :cond_2

    .line 94
    invoke-direct {p0, p1, p2, p3}, Lcom/peel/content/library/DirecTVLibrary;->b(Landroid/os/Bundle;Lcom/peel/util/t;Lcom/peel/util/t;)V

    goto :goto_0

    .line 96
    :cond_2
    const-string/jumbo v1, "path"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 97
    sget-object v2, Lcom/peel/content/library/DirecTVLibrary;->c:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "\ndtv lib path: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 98
    const-string/jumbo v2, "showtimes"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 99
    const-string/jumbo v0, "id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 100
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 101
    iget-object v0, p0, Lcom/peel/content/library/DirecTVLibrary;->f:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_3
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 102
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/content/listing/Listing;

    .line 103
    invoke-virtual {v0}, Lcom/peel/content/listing/Listing;->g()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_4

    invoke-virtual {v0}, Lcom/peel/content/listing/Listing;->g()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_3

    .line 104
    invoke-virtual {v0}, Lcom/peel/content/listing/Listing;->g()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 105
    :cond_4
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 108
    :cond_5
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [Lcom/peel/content/listing/Listing;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p2, v6, v0, v7}, Lcom/peel/util/t;->a(ZLjava/lang/Object;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 112
    :cond_6
    const-string/jumbo v2, "top"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 113
    iget-object v1, p0, Lcom/peel/content/library/DirecTVLibrary;->f:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->size()I

    move-result v1

    if-le v1, v0, :cond_7

    move v1, v0

    .line 114
    :goto_2
    iget-object v0, p0, Lcom/peel/content/library/DirecTVLibrary;->f:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    iget-object v2, p0, Lcom/peel/content/library/DirecTVLibrary;->f:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->size()I

    move-result v2

    new-array v2, v2, [Lcom/peel/content/listing/Listing;

    invoke-interface {v0, v2}, Ljava/util/Collection;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/peel/content/listing/Listing;

    .line 115
    new-instance v2, Lcom/peel/content/listing/a;

    invoke-direct {v2}, Lcom/peel/content/listing/a;-><init>()V

    invoke-static {v0, v2}, Ljava/util/Arrays;->sort([Ljava/lang/Object;Ljava/util/Comparator;)V

    .line 117
    add-int/lit8 v2, v1, -0x1

    invoke-static {v0, v5, v2}, Ljava/util/Arrays;->copyOfRange([Ljava/lang/Object;II)[Ljava/lang/Object;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "top "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " listings returned from dtv library"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v6, v0, v1}, Lcom/peel/util/t;->a(ZLjava/lang/Object;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 113
    :cond_7
    iget-object v0, p0, Lcom/peel/content/library/DirecTVLibrary;->f:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    move v1, v0

    goto :goto_2

    .line 122
    :cond_8
    iget-object v0, p0, Lcom/peel/content/library/DirecTVLibrary;->f:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/content/library/DirecTVLibrary;->f:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->size()I

    move-result v1

    new-array v1, v1, [Lcom/peel/content/listing/Listing;

    invoke-interface {v0, v1}, Ljava/util/Collection;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/peel/content/listing/Listing;

    .line 123
    new-instance v1, Lcom/peel/content/listing/a;

    invoke-direct {v1}, Lcom/peel/content/listing/a;-><init>()V

    invoke-static {v0, v1}, Ljava/util/Arrays;->sort([Ljava/lang/Object;Ljava/util/Comparator;)V

    .line 124
    invoke-virtual {p2, v6, v0, v7}, Lcom/peel/util/t;->a(ZLjava/lang/Object;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method public a(Landroid/os/Bundle;Ljava/util/List;Lcom/peel/util/t;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/Bundle;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/peel/util/t",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/peel/content/listing/Listing;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 228
    invoke-virtual {p0, p2}, Lcom/peel/content/library/DirecTVLibrary;->a(Ljava/util/List;)[Lcom/peel/content/listing/Listing;

    .line 229
    return-void
.end method

.method public a(Ljava/util/List;)[Lcom/peel/content/listing/Listing;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)[",
            "Lcom/peel/content/listing/Listing;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 67
    iget-object v0, p0, Lcom/peel/content/library/DirecTVLibrary;->f:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 79
    :goto_0
    return-object v0

    .line 69
    :cond_0
    if-nez p1, :cond_1

    .line 70
    iget-object v0, p0, Lcom/peel/content/library/DirecTVLibrary;->f:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/content/library/DirecTVLibrary;->f:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->size()I

    move-result v1

    new-array v1, v1, [Lcom/peel/content/listing/Listing;

    invoke-interface {v0, v1}, Ljava/util/Collection;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/peel/content/listing/Listing;

    .line 71
    new-instance v1, Lcom/peel/content/listing/a;

    invoke-direct {v1}, Lcom/peel/content/listing/a;-><init>()V

    invoke-static {v0, v1}, Ljava/util/Arrays;->sort([Ljava/lang/Object;Ljava/util/Comparator;)V

    goto :goto_0

    .line 74
    :cond_1
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 75
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 76
    iget-object v3, p0, Lcom/peel/content/library/DirecTVLibrary;->f:Ljava/util/Map;

    invoke-interface {v3, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/peel/content/library/DirecTVLibrary;->f:Ljava/util/Map;

    invoke-interface {v3, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 78
    :cond_3
    sget-object v0, Lcom/peel/content/library/DirecTVLibrary;->c:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "#### list to return: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 79
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [Lcom/peel/content/listing/Listing;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/peel/content/listing/Listing;

    goto :goto_0
.end method

.method public b()J
    .locals 2

    .prologue
    .line 48
    const-wide/32 v0, 0x36ee80

    return-wide v0
.end method

.method public b(Landroid/os/Bundle;Lcom/peel/util/t;)V
    .locals 0

    .prologue
    .line 223
    return-void
.end method

.method public c()V
    .locals 3

    .prologue
    .line 53
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 54
    const-string/jumbo v1, "path"

    const-string/jumbo v2, "listings"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 55
    const-string/jumbo v1, "force_loading"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 56
    new-instance v1, Lcom/peel/content/library/a;

    invoke-direct {v1, p0}, Lcom/peel/content/library/a;-><init>(Lcom/peel/content/library/DirecTVLibrary;)V

    invoke-virtual {p0, v0, v1}, Lcom/peel/content/library/DirecTVLibrary;->a(Landroid/os/Bundle;Lcom/peel/util/t;)V

    .line 62
    return-void
.end method
