.class public Lcom/peel/content/library/LiveLibrary;
.super Lcom/peel/content/library/Library;


# static fields
.field private static final c:Ljava/lang/String;


# instance fields
.field private final boxtype:Ljava/lang/String;

.field private final channeldifference:Ljava/lang/String;

.field private final d:Landroid/util/LruCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/LruCache",
            "<",
            "Ljava/lang/String;",
            "Lcom/peel/content/listing/Listing;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Landroid/util/LongSparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/LongSparseArray",
            "<",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private final f:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private final g:Landroid/util/LongSparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/LongSparseArray",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/peel/data/RankCategory;",
            ">;>;"
        }
    .end annotation
.end field

.field private final h:Landroid/util/LongSparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/LongSparseArray",
            "<",
            "Ljava/util/LinkedHashSet",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private final i:Landroid/util/LruCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/LruCache",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private final j:Landroid/util/LruCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/LruCache",
            "<",
            "Ljava/lang/String;",
            "Lcom/peel/content/listing/LiveListing;",
            ">;"
        }
    .end annotation
.end field

.field private k:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/peel/content/listing/Listing;",
            ">;"
        }
    .end annotation
.end field

.field private l:J

.field private lineup:[Lcom/peel/data/Channel;

.field private final lineupcount:I

.field private final location:Ljava/lang/String;

.field private final mso:Ljava/lang/String;

.field private final name:Ljava/lang/String;

.field private final type:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 38
    const-class v0, Lcom/peel/content/library/LiveLibrary;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/peel/content/library/LiveLibrary;->c:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 8

    .prologue
    .line 74
    const-string/jumbo v0, "name"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v0, "location"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v0, "mso"

    .line 75
    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string/jumbo v0, "boxtype"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 76
    invoke-static {p2}, Lcom/peel/content/library/LiveLibrary;->a(Landroid/os/Bundle;)[Lcom/peel/data/Channel;

    move-result-object v6

    move-object v0, p0

    move-object v1, p1

    move-object v7, p2

    .line 74
    invoke-direct/range {v0 .. v7}, Lcom/peel/content/library/LiveLibrary;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Lcom/peel/data/Channel;Landroid/os/Bundle;)V

    .line 77
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Lcom/peel/data/Channel;)V
    .locals 8

    .prologue
    .line 81
    new-instance v7, Landroid/os/Bundle;

    invoke-direct {v7}, Landroid/os/Bundle;-><init>()V

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    invoke-direct/range {v0 .. v7}, Lcom/peel/content/library/LiveLibrary;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Lcom/peel/data/Channel;Landroid/os/Bundle;)V

    .line 82
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Lcom/peel/data/Channel;Landroid/os/Bundle;)V
    .locals 12

    .prologue
    .line 86
    const-string/jumbo v2, "live"

    invoke-direct {p0, p1, v2}, Lcom/peel/content/library/Library;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 39
    new-instance v2, Lcom/peel/content/library/e;

    const/16 v3, 0x1000

    invoke-direct {v2, p0, v3}, Lcom/peel/content/library/e;-><init>(Lcom/peel/content/library/LiveLibrary;I)V

    iput-object v2, p0, Lcom/peel/content/library/LiveLibrary;->d:Landroid/util/LruCache;

    .line 49
    new-instance v2, Landroid/util/LongSparseArray;

    invoke-direct {v2}, Landroid/util/LongSparseArray;-><init>()V

    iput-object v2, p0, Lcom/peel/content/library/LiveLibrary;->e:Landroid/util/LongSparseArray;

    .line 50
    new-instance v2, Ljava/util/HashMap;

    const/16 v3, 0x400

    invoke-direct {v2, v3}, Ljava/util/HashMap;-><init>(I)V

    iput-object v2, p0, Lcom/peel/content/library/LiveLibrary;->f:Ljava/util/Map;

    .line 57
    new-instance v2, Landroid/util/LongSparseArray;

    invoke-direct {v2}, Landroid/util/LongSparseArray;-><init>()V

    iput-object v2, p0, Lcom/peel/content/library/LiveLibrary;->g:Landroid/util/LongSparseArray;

    .line 58
    new-instance v2, Landroid/util/LongSparseArray;

    invoke-direct {v2}, Landroid/util/LongSparseArray;-><init>()V

    iput-object v2, p0, Lcom/peel/content/library/LiveLibrary;->h:Landroid/util/LongSparseArray;

    .line 59
    new-instance v2, Landroid/util/LruCache;

    const/16 v3, 0x200

    invoke-direct {v2, v3}, Landroid/util/LruCache;-><init>(I)V

    iput-object v2, p0, Lcom/peel/content/library/LiveLibrary;->i:Landroid/util/LruCache;

    .line 60
    new-instance v2, Landroid/util/LruCache;

    const/16 v3, 0x200

    invoke-direct {v2, v3}, Landroid/util/LruCache;-><init>(I)V

    iput-object v2, p0, Lcom/peel/content/library/LiveLibrary;->j:Landroid/util/LruCache;

    .line 70
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/peel/content/library/LiveLibrary;->k:Ljava/util/List;

    .line 88
    iput-object p2, p0, Lcom/peel/content/library/LiveLibrary;->name:Ljava/lang/String;

    .line 89
    iput-object p3, p0, Lcom/peel/content/library/LiveLibrary;->location:Ljava/lang/String;

    .line 90
    const-string/jumbo v2, "channeldifference"

    move-object/from16 v0, p7

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/peel/content/library/LiveLibrary;->channeldifference:Ljava/lang/String;

    .line 91
    const-string/jumbo v2, "lineupcount"

    move-object/from16 v0, p7

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    long-to-int v2, v2

    iput v2, p0, Lcom/peel/content/library/LiveLibrary;->lineupcount:I

    .line 92
    move-object/from16 v0, p4

    iput-object v0, p0, Lcom/peel/content/library/LiveLibrary;->mso:Ljava/lang/String;

    .line 93
    const-string/jumbo v2, "type"

    move-object/from16 v0, p7

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/peel/content/library/LiveLibrary;->type:Ljava/lang/String;

    .line 95
    const-string/jumbo v2, "nextPoll"

    move-object/from16 v0, p7

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Number;

    .line 96
    if-nez v2, :cond_1

    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lcom/peel/content/library/LiveLibrary;->l:J

    .line 99
    :goto_0
    move-object/from16 v0, p5

    iput-object v0, p0, Lcom/peel/content/library/LiveLibrary;->boxtype:Ljava/lang/String;

    .line 101
    const-string/jumbo v2, "reqkeys"

    move-object/from16 v0, p7

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getLongArray(Ljava/lang/String;)[J

    move-result-object v3

    .line 102
    if-eqz v3, :cond_2

    .line 103
    array-length v4, v3

    const/4 v2, 0x0

    :goto_1
    if-ge v2, v4, :cond_2

    aget-wide v6, v3, v2

    .line 104
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "reqkeys"

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {v6, v7}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p7

    invoke-virtual {v0, v5}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 105
    iget-object v5, p0, Lcom/peel/content/library/LiveLibrary;->h:Landroid/util/LongSparseArray;

    new-instance v8, Ljava/util/LinkedHashSet;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v10, "reqkeys"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-static {v6, v7}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    move-object/from16 v0, p7

    invoke-virtual {v0, v9}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/util/LinkedHashSet;-><init>(Ljava/util/Collection;)V

    invoke-virtual {v5, v6, v7, v8}, Landroid/util/LongSparseArray;->put(JLjava/lang/Object;)V

    .line 103
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 97
    :cond_1
    invoke-virtual {v2}, Ljava/lang/Number;->longValue()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/peel/content/library/LiveLibrary;->l:J

    goto :goto_0

    .line 109
    :cond_2
    const-string/jumbo v2, "feedstopkeys"

    move-object/from16 v0, p7

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getLongArray(Ljava/lang/String;)[J

    move-result-object v6

    .line 110
    if-eqz v6, :cond_5

    .line 111
    array-length v7, v6

    const/4 v2, 0x0

    move v5, v2

    :goto_2
    if-ge v5, v7, :cond_5

    aget-wide v8, v6, v5

    .line 112
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "curatedshows"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {v8, v9}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p7

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 113
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "curatedshows"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {v8, v9}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p7

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Landroid/os/Parcelable;

    check-cast v2, [Landroid/os/Parcelable;

    .line 115
    if-eqz v2, :cond_4

    .line 116
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    .line 118
    array-length v11, v2

    const/4 v3, 0x0

    move v4, v3

    :goto_3
    if-ge v4, v11, :cond_3

    aget-object v3, v2, v4

    .line 119
    check-cast v3, Lcom/peel/data/RankCategory;

    invoke-interface {v10, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 118
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    goto :goto_3

    .line 122
    :cond_3
    iget-object v2, p0, Lcom/peel/content/library/LiveLibrary;->g:Landroid/util/LongSparseArray;

    invoke-virtual {v2, v8, v9, v10}, Landroid/util/LongSparseArray;->put(JLjava/lang/Object;)V

    .line 111
    :cond_4
    add-int/lit8 v2, v5, 0x1

    move v5, v2

    goto :goto_2

    .line 127
    :cond_5
    move-object/from16 v0, p6

    iput-object v0, p0, Lcom/peel/content/library/LiveLibrary;->lineup:[Lcom/peel/data/Channel;

    .line 129
    const-string/jumbo v2, "showtimes"

    move-object/from16 v0, p7

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    .line 130
    if-eqz v2, :cond_7

    .line 131
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_6
    :goto_4
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_7

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 132
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "showtimes_keys"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p7

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 133
    iget-object v4, p0, Lcom/peel/content/library/LiveLibrary;->f:Ljava/util/Map;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "showtimes_keys"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p7

    invoke-virtual {v0, v5}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v5

    invoke-interface {v4, v2, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_4

    .line 136
    :cond_7
    return-void
.end method

.method static synthetic a(Lcom/peel/content/library/LiveLibrary;)Landroid/util/LruCache;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/peel/content/library/LiveLibrary;->d:Landroid/util/LruCache;

    return-object v0
.end method

.method private declared-synchronized a(Landroid/util/LongSparseArray;Landroid/os/Bundle;)Ljava/util/List;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/LongSparseArray",
            "<",
            "Ljava/util/LinkedHashSet",
            "<",
            "Ljava/lang/String;",
            ">;>;",
            "Landroid/os/Bundle;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 550
    monitor-enter p0

    const/4 v2, 0x1

    .line 551
    :try_start_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 552
    const-string/jumbo v0, "start"

    const-wide/16 v4, -0x1

    invoke-virtual {p2, v0, v4, v5}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v6

    .line 553
    const-string/jumbo v0, "window"

    const/4 v3, -0x1

    invoke-virtual {p2, v0, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v3

    .line 554
    const-wide/16 v4, -0x1

    cmp-long v0, v6, v4

    if-lez v0, :cond_1

    const/4 v0, -0x1

    if-le v3, v0, :cond_1

    .line 556
    monitor-enter p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-wide v4, v6

    .line 557
    :goto_0
    int-to-long v8, v3

    const-wide/32 v10, 0x1b7740

    mul-long/2addr v8, v10

    add-long/2addr v8, v6

    cmp-long v0, v4, v8

    if-gez v0, :cond_2

    .line 558
    :try_start_1
    invoke-virtual {p1, v4, v5}, Landroid/util/LongSparseArray;->get(J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/LinkedHashSet;

    .line 559
    if-nez v0, :cond_0

    .line 560
    const/4 v0, 0x0

    .line 565
    :goto_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 567
    if-eqz v0, :cond_1

    :try_start_2
    invoke-interface {v1}, Ljava/util/List;->size()I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result v0

    if-lez v0, :cond_1

    move-object v0, v1

    .line 572
    :goto_2
    monitor-exit p0

    return-object v0

    .line 563
    :cond_0
    :try_start_3
    invoke-interface {v1, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 557
    const-wide/32 v8, 0x1b7740

    add-long/2addr v4, v8

    goto :goto_0

    .line 565
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 550
    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0

    .line 572
    :cond_1
    const/4 v0, 0x0

    goto :goto_2

    :cond_2
    move v0, v2

    goto :goto_1
.end method

.method static synthetic a(Lcom/peel/content/library/LiveLibrary;Ljava/util/List;)Ljava/util/List;
    .locals 0

    .prologue
    .line 37
    iput-object p1, p0, Lcom/peel/content/library/LiveLibrary;->k:Ljava/util/List;

    return-object p1
.end method

.method public static a([Lcom/peel/content/listing/Listing;)Ljava/util/Map;
    .locals 18
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Lcom/peel/content/listing/Listing;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/peel/content/listing/Listing;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 152
    new-instance v5, Ljava/util/HashMap;

    invoke-direct {v5}, Ljava/util/HashMap;-><init>()V

    .line 153
    invoke-static {}, Lcom/peel/util/x;->a()[Ljava/lang/String;

    move-result-object v6

    .line 154
    new-instance v2, Ljava/util/GregorianCalendar;

    invoke-direct {v2}, Ljava/util/GregorianCalendar;-><init>()V

    .line 155
    const/16 v3, 0xb

    invoke-virtual {v2, v3}, Ljava/util/GregorianCalendar;->get(I)I

    move-result v7

    .line 156
    invoke-virtual {v2}, Ljava/util/GregorianCalendar;->getTimeInMillis()J

    move-result-wide v8

    .line 158
    move-object/from16 v0, p0

    array-length v10, v0

    const/4 v2, 0x0

    move v4, v2

    :goto_0
    if-ge v4, v10, :cond_3

    aget-object v3, p0, v4

    .line 159
    instance-of v2, v3, Lcom/peel/content/listing/LiveListing;

    if-nez v2, :cond_1

    .line 158
    :cond_0
    :goto_1
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto :goto_0

    :cond_1
    move-object v2, v3

    .line 160
    check-cast v2, Lcom/peel/content/listing/LiveListing;

    .line 161
    invoke-virtual {v2}, Lcom/peel/content/listing/LiveListing;->o()J

    move-result-wide v12

    .line 162
    sub-long v14, v12, v8

    const-wide/32 v16, 0x36ee80

    div-long v14, v14, v16

    int-to-long v0, v7

    move-wide/from16 v16, v0

    add-long v14, v14, v16

    const-wide/16 v16, 0x18

    div-long v14, v14, v16

    .line 163
    const-wide/16 v16, 0x7

    cmp-long v2, v14, v16

    if-gez v2, :cond_0

    .line 165
    sget-object v2, Lcom/peel/content/library/LiveLibrary;->c:Ljava/lang/String;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    new-instance v16, Ljava/util/Date;

    move-object/from16 v0, v16

    invoke-direct {v0, v12, v13}, Ljava/util/Date;-><init>(J)V

    invoke-virtual/range {v16 .. v16}, Ljava/util/Date;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string/jumbo v12, " -------------- day idx: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v14, v15}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v2, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 167
    long-to-int v2, v14

    aget-object v2, v6, v2

    invoke-interface {v5, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 168
    long-to-int v2, v14

    aget-object v2, v6, v2

    invoke-interface {v5, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/List;

    .line 169
    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 170
    long-to-int v3, v14

    aget-object v3, v6, v3

    invoke-interface {v5, v3, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 172
    :cond_2
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 173
    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 174
    long-to-int v3, v14

    aget-object v3, v6, v3

    invoke-interface {v5, v3, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 177
    :cond_3
    return-object v5
.end method

.method static synthetic a(Lcom/peel/content/library/LiveLibrary;Lcom/peel/content/listing/LiveListing;)V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0, p1}, Lcom/peel/content/library/LiveLibrary;->a(Lcom/peel/content/listing/LiveListing;)V

    return-void
.end method

.method static synthetic a(Lcom/peel/content/library/LiveLibrary;Ljava/util/List;Lcom/peel/util/t;)V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0, p1, p2}, Lcom/peel/content/library/LiveLibrary;->b(Ljava/util/List;Lcom/peel/util/t;)V

    return-void
.end method

.method private a(Lcom/peel/content/listing/LiveListing;)V
    .locals 19

    .prologue
    .line 1224
    if-eqz p1, :cond_0

    .line 1225
    new-instance v18, Ljava/util/ArrayList;

    invoke-direct/range {v18 .. v18}, Ljava/util/ArrayList;-><init>()V

    .line 1227
    new-instance v2, Lcom/peel/data/l;

    invoke-virtual/range {p1 .. p1}, Lcom/peel/content/listing/LiveListing;->f()Ljava/lang/String;

    move-result-object v3

    invoke-virtual/range {p1 .. p1}, Lcom/peel/content/listing/LiveListing;->g()Ljava/lang/String;

    move-result-object v4

    invoke-virtual/range {p1 .. p1}, Lcom/peel/content/listing/LiveListing;->h()Ljava/lang/String;

    move-result-object v5

    .line 1228
    invoke-virtual/range {p1 .. p1}, Lcom/peel/content/listing/LiveListing;->k()Ljava/lang/String;

    move-result-object v6

    invoke-virtual/range {p1 .. p1}, Lcom/peel/content/listing/LiveListing;->l()Ljava/lang/String;

    move-result-object v7

    invoke-virtual/range {p1 .. p1}, Lcom/peel/content/listing/LiveListing;->m()[Ljava/lang/String;

    move-result-object v8

    invoke-virtual/range {p1 .. p1}, Lcom/peel/content/listing/LiveListing;->i()I

    move-result v9

    .line 1229
    invoke-virtual/range {p1 .. p1}, Lcom/peel/content/listing/LiveListing;->j()J

    move-result-wide v10

    invoke-virtual/range {p1 .. p1}, Lcom/peel/content/listing/LiveListing;->y()Ljava/lang/String;

    move-result-object v12

    invoke-virtual/range {p1 .. p1}, Lcom/peel/content/listing/LiveListing;->z()Ljava/lang/String;

    move-result-object v13

    invoke-virtual/range {p1 .. p1}, Lcom/peel/content/listing/LiveListing;->x()Ljava/lang/String;

    move-result-object v14

    .line 1230
    invoke-virtual/range {p1 .. p1}, Lcom/peel/content/listing/LiveListing;->A()Ljava/lang/String;

    move-result-object v15

    invoke-virtual/range {p1 .. p1}, Lcom/peel/content/listing/LiveListing;->w()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {p1 .. p1}, Lcom/peel/content/listing/LiveListing;->a()Landroid/os/Bundle;

    move-result-object v17

    invoke-direct/range {v2 .. v17}, Lcom/peel/data/l;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;IJLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 1227
    move-object/from16 v0, v18

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1233
    invoke-static {}, Lcom/peel/data/m;->a()Lcom/peel/data/m;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/peel/content/library/LiveLibrary;->a:Ljava/lang/String;

    const/4 v4, 0x1

    move-object/from16 v0, v18

    invoke-virtual {v2, v3, v0, v4}, Lcom/peel/data/m;->a(Ljava/lang/String;Ljava/util/List;Z)V

    .line 1235
    :cond_0
    return-void
.end method

.method private static a(Landroid/os/Bundle;)[Lcom/peel/data/Channel;
    .locals 4

    .prologue
    .line 139
    const/4 v0, 0x0

    .line 140
    const-string/jumbo v1, "lineup"

    invoke-virtual {p0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 141
    const-string/jumbo v0, "lineup"

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->getParcelableArray(Ljava/lang/String;)[Landroid/os/Parcelable;

    move-result-object v3

    .line 142
    array-length v0, v3

    new-array v2, v0, [Lcom/peel/data/Channel;

    .line 143
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    array-length v0, v3

    if-ge v1, v0, :cond_0

    .line 144
    aget-object v0, v3, v1

    check-cast v0, Lcom/peel/data/Channel;

    aput-object v0, v2, v1

    .line 143
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    move-object v0, v2

    .line 147
    :cond_1
    return-object v0
.end method

.method static synthetic a(Lcom/peel/content/library/LiveLibrary;[Lcom/peel/data/Channel;)[Lcom/peel/data/Channel;
    .locals 0

    .prologue
    .line 37
    iput-object p1, p0, Lcom/peel/content/library/LiveLibrary;->lineup:[Lcom/peel/data/Channel;

    return-object p1
.end method

.method static synthetic b(Lcom/peel/content/library/LiveLibrary;)Landroid/util/LongSparseArray;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/peel/content/library/LiveLibrary;->h:Landroid/util/LongSparseArray;

    return-object v0
.end method

.method private b(Ljava/util/List;Lcom/peel/util/t;)V
    .locals 20
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/peel/content/listing/Listing;",
            ">;",
            "Lcom/peel/util/t",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/peel/content/listing/Listing;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 1239
    new-instance v18, Ljava/util/ArrayList;

    invoke-direct/range {v18 .. v18}, Ljava/util/ArrayList;-><init>()V

    .line 1240
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_2

    .line 1241
    invoke-static {}, Lcom/peel/data/m;->a()Lcom/peel/data/m;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/peel/content/library/LiveLibrary;->a:Ljava/lang/String;

    invoke-virtual/range {p0 .. p0}, Lcom/peel/content/library/LiveLibrary;->a()Landroid/os/Bundle;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/peel/data/m;->a(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 1243
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v19

    :cond_0
    :goto_0
    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object/from16 v17, v2

    check-cast v17, Lcom/peel/content/listing/Listing;

    .line 1244
    if-eqz v17, :cond_0

    .line 1247
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/peel/content/library/LiveLibrary;->d:Landroid/util/LruCache;

    monitor-enter v3

    .line 1248
    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/peel/content/library/LiveLibrary;->d:Landroid/util/LruCache;

    invoke-virtual/range {v17 .. v17}, Lcom/peel/content/listing/Listing;->f()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v17

    invoke-virtual {v2, v4, v0}, Landroid/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1249
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1251
    new-instance v2, Lcom/peel/data/l;

    invoke-virtual/range {v17 .. v17}, Lcom/peel/content/listing/Listing;->f()Ljava/lang/String;

    move-result-object v3

    invoke-virtual/range {v17 .. v17}, Lcom/peel/content/listing/Listing;->g()Ljava/lang/String;

    move-result-object v4

    .line 1252
    invoke-virtual/range {v17 .. v17}, Lcom/peel/content/listing/Listing;->h()Ljava/lang/String;

    move-result-object v5

    invoke-virtual/range {v17 .. v17}, Lcom/peel/content/listing/Listing;->k()Ljava/lang/String;

    move-result-object v6

    invoke-virtual/range {v17 .. v17}, Lcom/peel/content/listing/Listing;->l()Ljava/lang/String;

    move-result-object v7

    .line 1253
    invoke-virtual/range {v17 .. v17}, Lcom/peel/content/listing/Listing;->m()[Ljava/lang/String;

    move-result-object v8

    invoke-virtual/range {v17 .. v17}, Lcom/peel/content/listing/Listing;->i()I

    move-result v9

    invoke-virtual/range {v17 .. v17}, Lcom/peel/content/listing/Listing;->j()J

    move-result-wide v10

    move-object/from16 v12, v17

    check-cast v12, Lcom/peel/content/listing/LiveListing;

    .line 1254
    invoke-virtual {v12}, Lcom/peel/content/listing/LiveListing;->y()Ljava/lang/String;

    move-result-object v12

    move-object/from16 v13, v17

    check-cast v13, Lcom/peel/content/listing/LiveListing;

    invoke-virtual {v13}, Lcom/peel/content/listing/LiveListing;->z()Ljava/lang/String;

    move-result-object v13

    move-object/from16 v14, v17

    check-cast v14, Lcom/peel/content/listing/LiveListing;

    .line 1255
    invoke-virtual {v14}, Lcom/peel/content/listing/LiveListing;->x()Ljava/lang/String;

    move-result-object v14

    move-object/from16 v15, v17

    check-cast v15, Lcom/peel/content/listing/LiveListing;

    .line 1256
    invoke-virtual {v15}, Lcom/peel/content/listing/LiveListing;->A()Ljava/lang/String;

    move-result-object v15

    move-object/from16 v16, v17

    check-cast v16, Lcom/peel/content/listing/LiveListing;

    .line 1257
    invoke-virtual/range {v16 .. v16}, Lcom/peel/content/listing/LiveListing;->w()Ljava/lang/String;

    move-result-object v16

    .line 1258
    invoke-virtual/range {v17 .. v17}, Lcom/peel/content/listing/Listing;->a()Landroid/os/Bundle;

    move-result-object v17

    invoke-direct/range {v2 .. v17}, Lcom/peel/data/l;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;IJLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 1251
    move-object/from16 v0, v18

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1249
    :catchall_0
    move-exception v2

    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v2

    .line 1261
    :cond_1
    invoke-static {}, Lcom/peel/data/m;->a()Lcom/peel/data/m;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/peel/content/library/LiveLibrary;->a:Ljava/lang/String;

    const/4 v4, 0x1

    move-object/from16 v0, v18

    invoke-virtual {v2, v3, v0, v4}, Lcom/peel/data/m;->a(Ljava/lang/String;Ljava/util/List;Z)V

    .line 1265
    :cond_2
    const/4 v2, 0x1

    const/4 v3, 0x0

    move-object/from16 v0, p2

    move-object/from16 v1, p1

    invoke-virtual {v0, v2, v1, v3}, Lcom/peel/util/t;->a(ZLjava/lang/Object;Ljava/lang/String;)V

    .line 1266
    return-void
.end method

.method static synthetic c(Lcom/peel/content/library/LiveLibrary;)Landroid/util/LongSparseArray;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/peel/content/library/LiveLibrary;->g:Landroid/util/LongSparseArray;

    return-object v0
.end method

.method private c(Landroid/os/Bundle;Lcom/peel/util/t;)V
    .locals 2

    .prologue
    .line 321
    new-instance v0, Lcom/peel/content/library/o;

    const/4 v1, 0x2

    invoke-direct {v0, p0, v1, p2, p1}, Lcom/peel/content/library/o;-><init>(Lcom/peel/content/library/LiveLibrary;ILcom/peel/util/t;Landroid/os/Bundle;)V

    invoke-static {p1, v0}, Lcom/peel/content/a/j;->a(Landroid/os/Bundle;Lcom/peel/util/t;)V

    .line 546
    return-void
.end method

.method static synthetic d(Lcom/peel/content/library/LiveLibrary;)Landroid/util/LongSparseArray;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/peel/content/library/LiveLibrary;->e:Landroid/util/LongSparseArray;

    return-object v0
.end method

.method static synthetic e(Lcom/peel/content/library/LiveLibrary;)Ljava/util/Map;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/peel/content/library/LiveLibrary;->f:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic f(Lcom/peel/content/library/LiveLibrary;)Landroid/util/LruCache;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/peel/content/library/LiveLibrary;->j:Landroid/util/LruCache;

    return-object v0
.end method

.method static synthetic g(Lcom/peel/content/library/LiveLibrary;)Landroid/util/LruCache;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/peel/content/library/LiveLibrary;->i:Landroid/util/LruCache;

    return-object v0
.end method

.method static synthetic p()Ljava/lang/String;
    .locals 1

    .prologue
    .line 37
    sget-object v0, Lcom/peel/content/library/LiveLibrary;->c:Ljava/lang/String;

    return-object v0
.end method

.method private q()V
    .locals 1

    .prologue
    .line 181
    iget-object v0, p0, Lcom/peel/content/library/LiveLibrary;->h:Landroid/util/LongSparseArray;

    invoke-virtual {v0}, Landroid/util/LongSparseArray;->clear()V

    .line 182
    iget-object v0, p0, Lcom/peel/content/library/LiveLibrary;->g:Landroid/util/LongSparseArray;

    invoke-virtual {v0}, Landroid/util/LongSparseArray;->clear()V

    .line 183
    return-void
.end method


# virtual methods
.method public a()Landroid/os/Bundle;
    .locals 10

    .prologue
    const/4 v1, 0x0

    .line 200
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 201
    const-string/jumbo v0, "name"

    iget-object v2, p0, Lcom/peel/content/library/LiveLibrary;->name:Ljava/lang/String;

    invoke-virtual {v3, v0, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 202
    const-string/jumbo v0, "location"

    iget-object v2, p0, Lcom/peel/content/library/LiveLibrary;->location:Ljava/lang/String;

    invoke-virtual {v3, v0, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 203
    const-string/jumbo v0, "channeldifference"

    iget-object v2, p0, Lcom/peel/content/library/LiveLibrary;->channeldifference:Ljava/lang/String;

    invoke-virtual {v3, v0, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 204
    const-string/jumbo v0, "lineupcount"

    iget v2, p0, Lcom/peel/content/library/LiveLibrary;->lineupcount:I

    int-to-long v4, v2

    invoke-virtual {v3, v0, v4, v5}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 205
    const-string/jumbo v0, "mso"

    iget-object v2, p0, Lcom/peel/content/library/LiveLibrary;->mso:Ljava/lang/String;

    invoke-virtual {v3, v0, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 206
    const-string/jumbo v0, "type"

    iget-object v2, p0, Lcom/peel/content/library/LiveLibrary;->type:Ljava/lang/String;

    invoke-virtual {v3, v0, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 207
    const-string/jumbo v0, "nextPoll"

    iget-wide v4, p0, Lcom/peel/content/library/LiveLibrary;->l:J

    invoke-virtual {v3, v0, v4, v5}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 208
    const-string/jumbo v0, "boxtype"

    iget-object v2, p0, Lcom/peel/content/library/LiveLibrary;->boxtype:Ljava/lang/String;

    invoke-virtual {v3, v0, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 210
    monitor-enter p0

    .line 212
    :try_start_0
    iget-object v0, p0, Lcom/peel/content/library/LiveLibrary;->h:Landroid/util/LongSparseArray;

    invoke-virtual {v0}, Landroid/util/LongSparseArray;->size()I

    move-result v0

    new-array v4, v0, [J

    move v2, v1

    .line 213
    :goto_0
    array-length v0, v4

    if-ge v2, v0, :cond_1

    .line 214
    iget-object v0, p0, Lcom/peel/content/library/LiveLibrary;->h:Landroid/util/LongSparseArray;

    invoke-virtual {v0, v2}, Landroid/util/LongSparseArray;->keyAt(I)J

    move-result-wide v6

    aput-wide v6, v4, v2

    .line 215
    iget-object v0, p0, Lcom/peel/content/library/LiveLibrary;->h:Landroid/util/LongSparseArray;

    aget-wide v6, v4, v2

    invoke-virtual {v0, v6, v7}, Landroid/util/LongSparseArray;->get(J)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 216
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "reqkeys"

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    aget-wide v6, v4, v2

    invoke-static {v6, v7}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    new-instance v6, Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/peel/content/library/LiveLibrary;->h:Landroid/util/LongSparseArray;

    aget-wide v8, v4, v2

    invoke-virtual {v0, v8, v9}, Landroid/util/LongSparseArray;->get(J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    invoke-direct {v6, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-virtual {v3, v5, v6}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 213
    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 218
    :cond_1
    array-length v0, v4

    if-lez v0, :cond_2

    const-string/jumbo v0, "reqkeys"

    invoke-virtual {v3, v0, v4}, Landroid/os/Bundle;->putLongArray(Ljava/lang/String;[J)V

    .line 221
    :cond_2
    iget-object v0, p0, Lcom/peel/content/library/LiveLibrary;->f:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    if-lez v0, :cond_4

    .line 222
    new-instance v2, Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/peel/content/library/LiveLibrary;->f:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    invoke-direct {v2, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 223
    iget-object v0, p0, Lcom/peel/content/library/LiveLibrary;->f:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 224
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 225
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "showtimes_keys"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/peel/content/library/LiveLibrary;->f:Ljava/util/Map;

    invoke-interface {v6, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    invoke-virtual {v3, v5, v0}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    goto :goto_1

    .line 246
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 227
    :cond_3
    :try_start_1
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_4

    .line 228
    const-string/jumbo v0, "showtimes"

    invoke-virtual {v3, v0, v2}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 231
    :cond_4
    iget-object v0, p0, Lcom/peel/content/library/LiveLibrary;->g:Landroid/util/LongSparseArray;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/peel/content/library/LiveLibrary;->g:Landroid/util/LongSparseArray;

    invoke-virtual {v0}, Landroid/util/LongSparseArray;->size()I

    move-result v0

    if-lez v0, :cond_6

    .line 232
    iget-object v0, p0, Lcom/peel/content/library/LiveLibrary;->g:Landroid/util/LongSparseArray;

    invoke-virtual {v0}, Landroid/util/LongSparseArray;->size()I

    move-result v0

    new-array v2, v0, [J

    .line 234
    :goto_2
    array-length v0, v2

    if-ge v1, v0, :cond_5

    .line 235
    iget-object v0, p0, Lcom/peel/content/library/LiveLibrary;->g:Landroid/util/LongSparseArray;

    invoke-virtual {v0, v1}, Landroid/util/LongSparseArray;->keyAt(I)J

    move-result-wide v4

    aput-wide v4, v2, v1

    .line 236
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "curatedshows"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    aget-wide v4, v2, v1

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    iget-object v0, p0, Lcom/peel/content/library/LiveLibrary;->g:Landroid/util/LongSparseArray;

    aget-wide v6, v2, v1

    invoke-virtual {v0, v6, v7}, Landroid/util/LongSparseArray;->get(J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/Serializable;

    invoke-virtual {v3, v4, v0}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 234
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 239
    :cond_5
    array-length v0, v2

    if-lez v0, :cond_6

    const-string/jumbo v0, "feedstopkeys"

    invoke-virtual {v3, v0, v2}, Landroid/os/Bundle;->putLongArray(Ljava/lang/String;[J)V

    .line 242
    :cond_6
    iget-object v0, p0, Lcom/peel/content/library/LiveLibrary;->lineup:[Lcom/peel/data/Channel;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/peel/content/library/LiveLibrary;->lineup:[Lcom/peel/data/Channel;

    array-length v0, v0

    if-lez v0, :cond_7

    .line 243
    iget-object v0, p0, Lcom/peel/content/library/LiveLibrary;->lineup:[Lcom/peel/data/Channel;

    invoke-static {v0}, Ljava/util/Arrays;->sort([Ljava/lang/Object;)V

    .line 244
    const-string/jumbo v0, "lineup"

    iget-object v1, p0, Lcom/peel/content/library/LiveLibrary;->lineup:[Lcom/peel/data/Channel;

    invoke-virtual {v3, v0, v1}, Landroid/os/Bundle;->putParcelableArray(Ljava/lang/String;[Landroid/os/Parcelable;)V

    .line 246
    :cond_7
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 248
    return-object v3
.end method

.method public a(IJ)V
    .locals 4

    .prologue
    .line 681
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 683
    packed-switch p1, :pswitch_data_0

    .line 718
    :cond_0
    :goto_0
    return-void

    .line 686
    :pswitch_0
    iget-object v0, p0, Lcom/peel/content/library/LiveLibrary;->g:Landroid/util/LongSparseArray;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/peel/content/library/LiveLibrary;->g:Landroid/util/LongSparseArray;

    invoke-virtual {v0, p2, p3}, Landroid/util/LongSparseArray;->get(J)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 687
    iget-object v0, p0, Lcom/peel/content/library/LiveLibrary;->g:Landroid/util/LongSparseArray;

    invoke-virtual {v0, p2, p3}, Landroid/util/LongSparseArray;->get(J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 689
    if-eqz v0, :cond_1

    .line 690
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/data/RankCategory;

    .line 691
    invoke-virtual {v0}, Lcom/peel/data/RankCategory;->c()Ljava/util/List;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    goto :goto_1

    .line 695
    :cond_1
    iget-object v0, p0, Lcom/peel/content/library/LiveLibrary;->g:Landroid/util/LongSparseArray;

    invoke-virtual {v0, p2, p3}, Landroid/util/LongSparseArray;->delete(J)V

    .line 697
    invoke-static {}, Lcom/peel/data/m;->a()Lcom/peel/data/m;

    move-result-object v0

    iget-object v2, p0, Lcom/peel/content/library/LiveLibrary;->a:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/peel/content/library/LiveLibrary;->a()Landroid/os/Bundle;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/peel/data/m;->a(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 698
    invoke-static {}, Lcom/peel/data/m;->a()Lcom/peel/data/m;

    move-result-object v2

    iget-object v3, p0, Lcom/peel/content/library/LiveLibrary;->a:Ljava/lang/String;

    invoke-interface {v1}, Ljava/util/Set;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/String;

    invoke-interface {v1, v0}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    invoke-virtual {v2, v3, v0}, Lcom/peel/data/m;->a(Ljava/lang/String;[Ljava/lang/String;)V

    goto :goto_0

    .line 703
    :pswitch_1
    iget-object v0, p0, Lcom/peel/content/library/LiveLibrary;->h:Landroid/util/LongSparseArray;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/peel/content/library/LiveLibrary;->h:Landroid/util/LongSparseArray;

    invoke-virtual {v0, p2, p3}, Landroid/util/LongSparseArray;->get(J)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 704
    iget-object v0, p0, Lcom/peel/content/library/LiveLibrary;->h:Landroid/util/LongSparseArray;

    invoke-virtual {v0, p2, p3}, Landroid/util/LongSparseArray;->get(J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/LinkedHashSet;

    .line 706
    if-eqz v0, :cond_2

    .line 707
    invoke-virtual {v0}, Ljava/util/LinkedHashSet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 708
    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 712
    :cond_2
    iget-object v0, p0, Lcom/peel/content/library/LiveLibrary;->h:Landroid/util/LongSparseArray;

    invoke-virtual {v0, p2, p3}, Landroid/util/LongSparseArray;->delete(J)V

    .line 713
    invoke-static {}, Lcom/peel/data/m;->a()Lcom/peel/data/m;

    move-result-object v0

    iget-object v2, p0, Lcom/peel/content/library/LiveLibrary;->a:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/peel/content/library/LiveLibrary;->a()Landroid/os/Bundle;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/peel/data/m;->a(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 714
    invoke-static {}, Lcom/peel/data/m;->a()Lcom/peel/data/m;

    move-result-object v2

    iget-object v3, p0, Lcom/peel/content/library/LiveLibrary;->a:Ljava/lang/String;

    invoke-interface {v1}, Ljava/util/Set;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/String;

    invoke-interface {v1, v0}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    invoke-virtual {v2, v3, v0}, Lcom/peel/data/m;->a(Ljava/lang/String;[Ljava/lang/String;)V

    goto/16 :goto_0

    .line 683
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public a(IZ)V
    .locals 6

    .prologue
    .line 590
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 591
    const-string/jumbo v0, "path"

    const-string/jumbo v2, "listing"

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 592
    const-string/jumbo v0, "ignoreIndex"

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 593
    const-string/jumbo v0, "force_refresh"

    invoke-virtual {v1, v0, p2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 596
    :try_start_0
    const-string/jumbo v2, "start"

    sget-object v0, Lcom/peel/util/x;->c:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/text/SimpleDateFormat;

    invoke-static {}, Lcom/peel/util/x;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 600
    :goto_0
    const-string/jumbo v0, "window"

    invoke-virtual {v1, v0, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 601
    new-instance v0, Lcom/peel/content/library/s;

    invoke-direct {v0, p0, v1}, Lcom/peel/content/library/s;-><init>(Lcom/peel/content/library/LiveLibrary;Landroid/os/Bundle;)V

    invoke-virtual {p0, v1, v0}, Lcom/peel/content/library/LiveLibrary;->a(Landroid/os/Bundle;Lcom/peel/util/t;)V

    .line 607
    return-void

    .line 597
    :catch_0
    move-exception v0

    .line 598
    sget-object v2, Lcom/peel/content/library/LiveLibrary;->c:Ljava/lang/String;

    sget-object v3, Lcom/peel/content/library/LiveLibrary;->c:Ljava/lang/String;

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public a(Landroid/os/Bundle;ILcom/peel/util/t;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/Bundle;",
            "I",
            "Lcom/peel/util/t",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/peel/content/listing/Listing;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 1328
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 1329
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 1330
    const-string/jumbo v0, "showId"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 1332
    iget-object v0, p0, Lcom/peel/content/library/LiveLibrary;->i:Landroid/util/LruCache;

    invoke-virtual {v0, v4}, Landroid/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 1334
    if-eqz v0, :cond_3

    .line 1335
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1336
    iget-object v1, p0, Lcom/peel/content/library/LiveLibrary;->j:Landroid/util/LruCache;

    invoke-virtual {v1, v0}, Landroid/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/peel/content/listing/LiveListing;

    .line 1338
    if-eqz v1, :cond_0

    .line 1339
    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1341
    :cond_0
    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1345
    :cond_1
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_2

    .line 1346
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p3, v0, v2, v1}, Lcom/peel/util/t;->a(ZLjava/lang/Object;Ljava/lang/String;)V

    .line 1382
    :goto_1
    return-void

    .line 1348
    :cond_2
    invoke-virtual {p0, v3, p3}, Lcom/peel/content/library/LiveLibrary;->a(Ljava/util/List;Lcom/peel/util/t;)V

    goto :goto_1

    .line 1352
    :cond_3
    new-instance v0, Lcom/peel/content/library/m;

    move-object v1, p0

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/peel/content/library/m;-><init>(Lcom/peel/content/library/LiveLibrary;Ljava/util/List;Ljava/util/List;Ljava/lang/String;Lcom/peel/util/t;)V

    invoke-static {v4, p2, v0}, Lcom/peel/content/a/j;->b(Ljava/lang/String;ILcom/peel/util/t;)V

    goto :goto_1
.end method

.method public a(Landroid/os/Bundle;Lcom/peel/util/t;)V
    .locals 9

    .prologue
    const/4 v8, 0x0

    const/4 v6, 0x1

    const/4 v2, 0x2

    const/4 v1, 0x0

    .line 721
    const-string/jumbo v0, "path"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 722
    const-string/jumbo v3, "force_refresh"

    invoke-virtual {p1, v3, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    .line 723
    sget-object v4, Lcom/peel/content/library/LiveLibrary;->c:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "get() path: "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 724
    const-string/jumbo v4, "listing"

    invoke-virtual {v0, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_e

    .line 725
    const-string/jumbo v4, "ids"

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 726
    const-string/jumbo v0, "ids"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {p0, p1, v0, p2}, Lcom/peel/content/library/LiveLibrary;->a(Landroid/os/Bundle;Ljava/util/List;Lcom/peel/util/t;)V

    .line 995
    :cond_0
    :goto_0
    return-void

    .line 730
    :cond_1
    const-string/jumbo v4, "genre"

    invoke-virtual {v0, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_3

    const-string/jumbo v4, "ignoreIndex"

    invoke-virtual {p1, v4, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    if-nez v4, :cond_3

    .line 731
    iget-object v0, p0, Lcom/peel/content/library/LiveLibrary;->h:Landroid/util/LongSparseArray;

    .line 732
    invoke-direct {p0, v0, p1}, Lcom/peel/content/library/LiveLibrary;->a(Landroid/util/LongSparseArray;Landroid/os/Bundle;)Ljava/util/List;

    move-result-object v0

    .line 734
    if-eqz v0, :cond_7

    .line 735
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_2

    if-nez v3, :cond_2

    .line 736
    invoke-virtual {p0, p1, v0, p2}, Lcom/peel/content/library/LiveLibrary;->a(Landroid/os/Bundle;Ljava/util/List;Lcom/peel/util/t;)V

    .line 737
    sget-object v0, Lcom/peel/content/library/LiveLibrary;->c:Ljava/lang/String;

    const-string/jumbo v1, "Loading listings from database"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 739
    :cond_2
    const-string/jumbo v0, "library"

    iget-object v1, p0, Lcom/peel/content/library/LiveLibrary;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 740
    const-string/jumbo v0, "user"

    sget-object v1, Lcom/peel/content/a;->f:Lcom/peel/content/user/User;

    invoke-virtual {v1}, Lcom/peel/content/user/User;->x()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 741
    invoke-direct {p0, p1, p2}, Lcom/peel/content/library/LiveLibrary;->c(Landroid/os/Bundle;Lcom/peel/util/t;)V

    .line 742
    sget-object v0, Lcom/peel/content/library/LiveLibrary;->c:Ljava/lang/String;

    const-string/jumbo v1, "Loading listings from cloud"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 746
    :cond_3
    const-string/jumbo v3, "top"

    invoke-virtual {v0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_8

    .line 749
    const-string/jumbo v0, "start"

    const-wide/16 v2, -0x1

    invoke-virtual {p1, v0, v2, v3}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    .line 750
    const-wide/32 v4, 0x36ee80

    rem-long v4, v2, v4

    sub-long/2addr v2, v4

    .line 753
    iget-object v0, p0, Lcom/peel/content/library/LiveLibrary;->g:Landroid/util/LongSparseArray;

    invoke-virtual {v0, v2, v3}, Landroid/util/LongSparseArray;->get(J)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 754
    new-instance v4, Ljava/util/concurrent/atomic/AtomicInteger;

    iget-object v0, p0, Lcom/peel/content/library/LiveLibrary;->g:Landroid/util/LongSparseArray;

    invoke-virtual {v0, v2, v3}, Landroid/util/LongSparseArray;->get(J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v4, v0}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    .line 755
    iget-object v0, p0, Lcom/peel/content/library/LiveLibrary;->g:Landroid/util/LongSparseArray;

    invoke-virtual {v0, v2, v3}, Landroid/util/LongSparseArray;->get(J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 757
    if-eqz v0, :cond_6

    .line 758
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 760
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    move v0, v1

    :cond_4
    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/peel/data/RankCategory;

    .line 761
    invoke-virtual {v3}, Lcom/peel/data/RankCategory;->c()Ljava/util/List;

    move-result-object v1

    if-eqz v1, :cond_4

    .line 762
    invoke-virtual {v3}, Lcom/peel/data/RankCategory;->c()Ljava/util/List;

    move-result-object v8

    new-instance v0, Lcom/peel/content/library/t;

    move-object v1, p0

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/peel/content/library/t;-><init>(Lcom/peel/content/library/LiveLibrary;Ljava/util/List;Lcom/peel/data/RankCategory;Ljava/util/concurrent/atomic/AtomicInteger;Lcom/peel/util/t;)V

    invoke-virtual {p0, p1, v8, v0}, Lcom/peel/content/library/LiveLibrary;->a(Landroid/os/Bundle;Ljava/util/List;Lcom/peel/util/t;)V

    move v0, v6

    .line 785
    goto :goto_1

    :cond_5
    move v1, v0

    .line 790
    :cond_6
    if-nez v1, :cond_0

    .line 907
    :cond_7
    const-string/jumbo v0, "library"

    iget-object v1, p0, Lcom/peel/content/library/LiveLibrary;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 908
    const-string/jumbo v0, "user"

    sget-object v1, Lcom/peel/content/a;->f:Lcom/peel/content/user/User;

    invoke-virtual {v1}, Lcom/peel/content/user/User;->x()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 909
    invoke-direct {p0, p1, p2}, Lcom/peel/content/library/LiveLibrary;->c(Landroid/os/Bundle;Lcom/peel/util/t;)V

    goto/16 :goto_0

    .line 791
    :cond_8
    const-string/jumbo v3, "channel"

    invoke-virtual {v0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_9

    .line 792
    invoke-static {}, Ljava/util/GregorianCalendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 793
    const/16 v2, 0xb

    invoke-virtual {v0, v2, v1}, Ljava/util/Calendar;->set(II)V

    .line 794
    iget-object v1, p0, Lcom/peel/content/library/LiveLibrary;->e:Landroid/util/LongSparseArray;

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Landroid/util/LongSparseArray;->get(J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 795
    if-eqz v0, :cond_7

    .line 796
    invoke-virtual {p0, p1, v0, p2}, Lcom/peel/content/library/LiveLibrary;->a(Landroid/os/Bundle;Ljava/util/List;Lcom/peel/util/t;)V

    goto/16 :goto_0

    .line 799
    :cond_9
    const-string/jumbo v3, "showtimes"

    invoke-virtual {v0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 801
    iget-object v0, p0, Lcom/peel/content/library/LiveLibrary;->f:Ljava/util/Map;

    const-string/jumbo v3, "id"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/List;

    .line 802
    if-eqz v3, :cond_d

    .line 803
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 804
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_b

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 805
    iget-object v7, p0, Lcom/peel/content/library/LiveLibrary;->d:Landroid/util/LruCache;

    invoke-virtual {v7, v0}, Landroid/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    if-eqz v7, :cond_a

    .line 806
    iget-object v7, p0, Lcom/peel/content/library/LiveLibrary;->d:Landroid/util/LruCache;

    invoke-virtual {v7, v0}, Landroid/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_a
    move v1, v6

    .line 813
    :cond_b
    if-nez v1, :cond_c

    .line 814
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Lcom/peel/content/listing/Listing;

    invoke-interface {v4, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p2, v6, v0, v8}, Lcom/peel/util/t;->a(ZLjava/lang/Object;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 818
    :cond_c
    new-instance v0, Lcom/peel/content/library/u;

    move-object v1, p0

    move-object v4, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/peel/content/library/u;-><init>(Lcom/peel/content/library/LiveLibrary;ILjava/util/List;Landroid/os/Bundle;Lcom/peel/util/t;)V

    invoke-virtual {p0, p1, v3, v0}, Lcom/peel/content/library/LiveLibrary;->a(Landroid/os/Bundle;Ljava/util/List;Lcom/peel/util/t;)V

    goto/16 :goto_0

    .line 877
    :cond_d
    const-string/jumbo v0, "library"

    iget-object v1, p0, Lcom/peel/content/library/LiveLibrary;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 878
    const-string/jumbo v0, "user"

    sget-object v1, Lcom/peel/content/a;->f:Lcom/peel/content/user/User;

    invoke-virtual {v1}, Lcom/peel/content/user/User;->x()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 879
    new-instance v0, Lcom/peel/content/library/w;

    invoke-direct {v0, p0, v2, p2, p1}, Lcom/peel/content/library/w;-><init>(Lcom/peel/content/library/LiveLibrary;ILcom/peel/util/t;Landroid/os/Bundle;)V

    invoke-static {p1, v0}, Lcom/peel/content/a/j;->a(Landroid/os/Bundle;Lcom/peel/util/t;)V

    goto/16 :goto_0

    .line 913
    :cond_e
    const-string/jumbo v4, "lineup"

    invoke-virtual {v0, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_10

    .line 915
    iget-object v0, p0, Lcom/peel/content/library/LiveLibrary;->lineup:[Lcom/peel/data/Channel;

    if-eqz v0, :cond_f

    if-nez v3, :cond_f

    .line 916
    iget-object v0, p0, Lcom/peel/content/library/LiveLibrary;->lineup:[Lcom/peel/data/Channel;

    invoke-virtual {p2, v6, v0, v8}, Lcom/peel/util/t;->a(ZLjava/lang/Object;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 920
    :cond_f
    const-string/jumbo v0, "library"

    iget-object v1, p0, Lcom/peel/content/library/LiveLibrary;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 921
    new-instance v0, Lcom/peel/content/library/x;

    invoke-direct {v0, p0, v2, p2}, Lcom/peel/content/library/x;-><init>(Lcom/peel/content/library/LiveLibrary;ILcom/peel/util/t;)V

    invoke-static {p1, v0}, Lcom/peel/content/a/j;->a(Landroid/os/Bundle;Lcom/peel/util/t;)V

    goto/16 :goto_0

    .line 941
    :cond_10
    const-string/jumbo v3, "channel"

    invoke-virtual {v0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_11

    .line 942
    const-string/jumbo v0, "library"

    iget-object v1, p0, Lcom/peel/content/library/LiveLibrary;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 943
    const-string/jumbo v0, "user"

    sget-object v1, Lcom/peel/content/a;->f:Lcom/peel/content/user/User;

    invoke-virtual {v1}, Lcom/peel/content/user/User;->x()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 944
    new-instance v0, Lcom/peel/content/library/f;

    invoke-direct {v0, p0, v2, p2}, Lcom/peel/content/library/f;-><init>(Lcom/peel/content/library/LiveLibrary;ILcom/peel/util/t;)V

    invoke-static {p1, v0}, Lcom/peel/content/a/j;->a(Landroid/os/Bundle;Lcom/peel/util/t;)V

    goto/16 :goto_0

    .line 957
    :cond_11
    const-string/jumbo v3, "search"

    invoke-virtual {v0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_13

    .line 958
    const-string/jumbo v1, "cached"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_12

    .line 967
    const-string/jumbo v0, "path"

    const-string/jumbo v1, "search/fullsearch"

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 970
    :cond_12
    const-string/jumbo v0, "library"

    iget-object v1, p0, Lcom/peel/content/library/LiveLibrary;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 971
    const-string/jumbo v0, "user"

    sget-object v1, Lcom/peel/content/a;->f:Lcom/peel/content/user/User;

    invoke-virtual {v1}, Lcom/peel/content/user/User;->x()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 973
    new-instance v0, Lcom/peel/content/library/g;

    invoke-direct {v0, p0, v2, p2}, Lcom/peel/content/library/g;-><init>(Lcom/peel/content/library/LiveLibrary;ILcom/peel/util/t;)V

    invoke-static {p1, v0}, Lcom/peel/content/a/j;->a(Landroid/os/Bundle;Lcom/peel/util/t;)V

    goto/16 :goto_0

    .line 993
    :cond_13
    sget-object v2, Lcom/peel/content/library/LiveLibrary;->c:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "unrecognized get : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 994
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "unrecognized get: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v1, v8, v0}, Lcom/peel/util/t;->a(ZLjava/lang/Object;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method public a(Landroid/os/Bundle;Ljava/util/List;Lcom/peel/util/t;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/Bundle;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/peel/util/t",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/peel/content/listing/Listing;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    const/4 v7, 0x0

    const/4 v0, 0x0

    const/4 v3, -0x1

    .line 265
    if-nez p2, :cond_0

    .line 266
    sget-object v1, Lcom/peel/content/library/LiveLibrary;->c:Ljava/lang/String;

    const-string/jumbo v2, "no ids passed to getListings"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 267
    const-string/jumbo v1, "no ids passed to getListings"

    invoke-virtual {p3, v0, v7, v1}, Lcom/peel/util/t;->a(ZLjava/lang/Object;Ljava/lang/String;)V

    .line 318
    :goto_0
    return-void

    .line 271
    :cond_0
    invoke-virtual {p0, p2}, Lcom/peel/content/library/LiveLibrary;->a(Ljava/util/List;)[Lcom/peel/content/listing/Listing;

    move-result-object v4

    .line 272
    new-instance v5, Ljava/util/HashMap;

    invoke-direct {v5}, Ljava/util/HashMap;-><init>()V

    .line 276
    :goto_1
    array-length v1, v4

    if-ge v0, v1, :cond_2

    .line 277
    aget-object v1, v4, v0

    if-nez v1, :cond_1

    invoke-interface {p2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v5, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 276
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 280
    :cond_2
    const-string/jumbo v0, "cacheWindow"

    invoke-virtual {p1, v0, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v6

    .line 281
    invoke-interface {v5}, Ljava/util/Map;->size()I

    move-result v0

    if-nez v0, :cond_4

    .line 282
    if-ne v3, v6, :cond_3

    .line 283
    const/4 v0, 0x1

    new-instance v1, Ljava/util/ArrayList;

    invoke-static {v4}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-virtual {p3, v0, v1, v7}, Lcom/peel/util/t;->a(ZLjava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 285
    :cond_3
    new-instance v0, Ljava/util/ArrayList;

    invoke-static {v4}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-direct {p0, v0, p3}, Lcom/peel/content/library/LiveLibrary;->b(Ljava/util/List;Lcom/peel/util/t;)V

    goto :goto_0

    .line 290
    :cond_4
    invoke-static {}, Lcom/peel/data/m;->a()Lcom/peel/data/m;

    move-result-object v8

    iget-object v9, p0, Lcom/peel/content/library/LiveLibrary;->a:Ljava/lang/String;

    invoke-interface {v5}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v5}, Ljava/util/Map;->size()I

    move-result v1

    new-array v1, v1, [Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, [Ljava/lang/String;

    new-instance v0, Lcom/peel/content/library/n;

    const/4 v2, 0x2

    move-object v1, p0

    move-object v3, p3

    invoke-direct/range {v0 .. v6}, Lcom/peel/content/library/n;-><init>(Lcom/peel/content/library/LiveLibrary;ILcom/peel/util/t;[Lcom/peel/content/listing/Listing;Ljava/util/Map;I)V

    invoke-virtual {v8, v9, v7, v0}, Lcom/peel/data/m;->a(Ljava/lang/String;[Ljava/lang/String;Lcom/peel/util/t;)V

    goto :goto_0
.end method

.method public a(Ljava/lang/String;Lcom/peel/util/t;)V
    .locals 11

    .prologue
    const/4 v10, 0x2

    const/4 v9, 0x0

    const/4 v8, 0x0

    .line 1270
    iget-object v0, p0, Lcom/peel/content/library/LiveLibrary;->k:Ljava/util/List;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/peel/content/library/LiveLibrary;->k:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-le v0, v10, :cond_1

    iget-object v0, p0, Lcom/peel/content/library/LiveLibrary;->k:Ljava/util/List;

    invoke-interface {v0, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/content/listing/LiveListing;

    .line 1271
    invoke-virtual {v0}, Lcom/peel/content/listing/LiveListing;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1272
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 1274
    iget-object v0, p0, Lcom/peel/content/library/LiveLibrary;->k:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 1276
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1277
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/content/listing/LiveListing;

    .line 1279
    invoke-virtual {v0}, Lcom/peel/content/listing/LiveListing;->o()J

    move-result-wide v4

    invoke-virtual {v0}, Lcom/peel/content/listing/LiveListing;->j()J

    move-result-wide v6

    add-long/2addr v4, v6

    cmp-long v0, v4, v2

    if-gez v0, :cond_0

    .line 1280
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    .line 1283
    :cond_1
    iget-object v0, p0, Lcom/peel/content/library/LiveLibrary;->k:Ljava/util/List;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/peel/content/library/LiveLibrary;->k:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_2

    iget-object v0, p0, Lcom/peel/content/library/LiveLibrary;->k:Ljava/util/List;

    invoke-interface {v0, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/content/listing/LiveListing;

    invoke-virtual {v0}, Lcom/peel/content/listing/LiveListing;->c()Ljava/lang/String;

    move-result-object v0

    .line 1284
    invoke-virtual {v0, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 1285
    iget-object v0, p0, Lcom/peel/content/library/LiveLibrary;->k:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 1288
    :cond_2
    iget-object v0, p0, Lcom/peel/content/library/LiveLibrary;->k:Ljava/util/List;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/peel/content/library/LiveLibrary;->k:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v0, v10, :cond_5

    .line 1289
    :cond_3
    iget-object v0, p0, Lcom/peel/content/library/LiveLibrary;->a:Ljava/lang/String;

    new-instance v1, Lcom/peel/content/library/l;

    invoke-direct {v1, p0, p2}, Lcom/peel/content/library/l;-><init>(Lcom/peel/content/library/LiveLibrary;Lcom/peel/util/t;)V

    invoke-static {p1, v0, v1}, Lcom/peel/content/a/j;->d(Ljava/lang/String;Ljava/lang/String;Lcom/peel/util/t;)V

    .line 1308
    :cond_4
    :goto_1
    return-void

    .line 1301
    :cond_5
    iget-object v0, p0, Lcom/peel/content/library/LiveLibrary;->k:Ljava/util/List;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/peel/content/library/LiveLibrary;->k:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-le v0, v10, :cond_6

    .line 1302
    if-eqz p2, :cond_4

    .line 1303
    const/4 v0, 0x1

    invoke-virtual {p2, v0, v8, v8}, Lcom/peel/util/t;->a(ZLjava/lang/Object;Ljava/lang/String;)V

    goto :goto_1

    .line 1305
    :cond_6
    if-eqz p2, :cond_4

    .line 1306
    invoke-virtual {p2, v9, v8, v8}, Lcom/peel/util/t;->a(ZLjava/lang/Object;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;Lcom/peel/util/t;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/peel/util/t",
            "<[",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1401
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 1402
    const-string/jumbo v1, "library"

    iget-object v2, p0, Lcom/peel/content/library/LiveLibrary;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1403
    const-string/jumbo v1, "user"

    sget-object v2, Lcom/peel/content/a;->f:Lcom/peel/content/user/User;

    invoke-virtual {v2}, Lcom/peel/content/user/User;->x()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1404
    const-string/jumbo v1, "keyword"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1405
    const-string/jumbo v1, "country"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1407
    invoke-static {v0, p3}, Lcom/peel/content/a/j;->c(Landroid/os/Bundle;Lcom/peel/util/t;)V

    .line 1408
    return-void
.end method

.method public a(Ljava/util/List;Lcom/peel/util/t;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/peel/util/t",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/peel/content/listing/Listing;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 1386
    if-eqz p1, :cond_1

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_1

    .line 1387
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 1389
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1390
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v3, ","

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 1393
    :cond_0
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->deleteCharAt(I)Ljava/lang/StringBuilder;

    .line 1395
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p2}, Lcom/peel/content/a/j;->d(Ljava/lang/String;Lcom/peel/util/t;)V

    .line 1397
    :cond_1
    return-void
.end method

.method public a(Ljava/util/List;)[Lcom/peel/content/listing/Listing;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)[",
            "Lcom/peel/content/listing/Listing;"
        }
    .end annotation

    .prologue
    .line 252
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    new-array v2, v0, [Lcom/peel/content/listing/Listing;

    .line 254
    iget-object v3, p0, Lcom/peel/content/library/LiveLibrary;->d:Landroid/util/LruCache;

    monitor-enter v3

    .line 255
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    :try_start_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 256
    iget-object v0, p0, Lcom/peel/content/library/LiveLibrary;->d:Landroid/util/LruCache;

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/content/listing/Listing;

    aput-object v0, v2, v1

    .line 255
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 258
    :cond_0
    monitor-exit v3

    .line 260
    return-object v2

    .line 258
    :catchall_0
    move-exception v0

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public b()J
    .locals 2

    .prologue
    .line 575
    iget-wide v0, p0, Lcom/peel/content/library/LiveLibrary;->l:J

    return-wide v0
.end method

.method public b(Landroid/os/Bundle;Lcom/peel/util/t;)V
    .locals 20

    .prologue
    .line 1004
    const-string/jumbo v2, "path"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 1005
    sget-object v2, Lcom/peel/content/library/LiveLibrary;->c:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, " xxx post path: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1007
    const-string/jumbo v2, "channels/lineup"

    invoke-virtual {v5, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 1008
    const-string/jumbo v2, "prgids"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1023
    :goto_0
    sget-object v2, Lcom/peel/content/library/LiveLibrary;->c:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, " ...... post to cloud: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "prgids"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1025
    const-string/jumbo v2, "prgids"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_0

    .line 1026
    const-string/jumbo v2, "library"

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/peel/content/library/LiveLibrary;->a:Ljava/lang/String;

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1027
    const/4 v2, 0x0

    move-object/from16 v0, p1

    invoke-static {v0, v2}, Lcom/peel/content/a/j;->b(Landroid/os/Bundle;Lcom/peel/util/t;)V

    .line 1029
    invoke-direct/range {p0 .. p0}, Lcom/peel/content/library/LiveLibrary;->q()V

    .line 1221
    :cond_0
    :goto_1
    return-void

    .line 1011
    :cond_1
    const-string/jumbo v2, "channels"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v3

    .line 1012
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 1013
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/peel/content/library/LiveLibrary;->lineup:[Lcom/peel/data/Channel;

    array-length v6, v5

    const/4 v2, 0x0

    :goto_2
    if-ge v2, v6, :cond_3

    aget-object v7, v5, v2

    .line 1014
    invoke-virtual {v7}, Lcom/peel/data/Channel;->a()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v3, v8}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 1015
    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Lcom/peel/data/Channel;->b(Z)V

    .line 1016
    invoke-virtual {v7}, Lcom/peel/data/Channel;->c()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, ","

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1013
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 1019
    :cond_3
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    if-lez v2, :cond_4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->deleteCharAt(I)Ljava/lang/StringBuilder;

    .line 1020
    :cond_4
    const-string/jumbo v2, "prgids"

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1034
    :cond_5
    const-string/jumbo v2, "channels/uncut"

    invoke-virtual {v5, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 1035
    const-string/jumbo v2, "channels"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v3

    .line 1036
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 1037
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/peel/content/library/LiveLibrary;->lineup:[Lcom/peel/data/Channel;

    array-length v6, v5

    const/4 v2, 0x0

    :goto_3
    if-ge v2, v6, :cond_7

    aget-object v7, v5, v2

    .line 1038
    invoke-virtual {v7}, Lcom/peel/data/Channel;->a()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v3, v8}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_6

    const/4 v8, 0x1

    invoke-virtual {v7, v8}, Lcom/peel/data/Channel;->b(Z)V

    .line 1037
    :goto_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 1039
    :cond_6
    invoke-virtual {v7}, Lcom/peel/data/Channel;->c()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, ","

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_4

    .line 1041
    :cond_7
    sget-object v2, Lcom/peel/content/library/LiveLibrary;->c:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, " ...... post to cloud: "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1042
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    if-lez v2, :cond_0

    .line 1043
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->deleteCharAt(I)Ljava/lang/StringBuilder;

    .line 1044
    const-string/jumbo v2, "library"

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/peel/content/library/LiveLibrary;->a:Ljava/lang/String;

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1045
    const-string/jumbo v2, "prgids"

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1046
    const/4 v2, 0x0

    move-object/from16 v0, p1

    invoke-static {v0, v2}, Lcom/peel/content/a/j;->b(Landroid/os/Bundle;Lcom/peel/util/t;)V

    .line 1048
    invoke-direct/range {p0 .. p0}, Lcom/peel/content/library/LiveLibrary;->q()V

    goto/16 :goto_1

    .line 1053
    :cond_8
    const-string/jumbo v2, "channel"

    invoke-virtual {v5, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_e

    .line 1055
    const-string/jumbo v2, "channel"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 1056
    const/4 v3, 0x0

    .line 1057
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/peel/content/library/LiveLibrary;->lineup:[Lcom/peel/data/Channel;

    array-length v8, v7

    const/4 v2, 0x0

    move v4, v2

    :goto_5
    if-ge v4, v8, :cond_19

    aget-object v2, v7, v4

    .line 1058
    invoke-virtual {v2}, Lcom/peel/data/Channel;->a()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_9

    .line 1064
    :goto_6
    if-nez v2, :cond_a

    .line 1066
    sget-object v2, Lcom/peel/content/library/LiveLibrary;->c:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "unable to find channel: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, " in library "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/peel/content/library/LiveLibrary;->a:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1067
    const/4 v2, 0x0

    const/4 v3, 0x0

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "unable to find channel: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, " in library "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/peel/content/library/LiveLibrary;->a:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p2

    invoke-virtual {v0, v2, v3, v4}, Lcom/peel/util/t;->a(ZLjava/lang/Object;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 1057
    :cond_9
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto :goto_5

    .line 1071
    :cond_a
    const-string/jumbo v3, "unfav"

    invoke-virtual {v5, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_b

    .line 1072
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/peel/content/library/LiveLibrary;->e:Landroid/util/LongSparseArray;

    invoke-virtual {v3}, Landroid/util/LongSparseArray;->clear()V

    .line 1073
    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/peel/data/Channel;->a(Z)V

    .line 1075
    const-string/jumbo v2, "prgsvcids"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1076
    const-string/jumbo v2, "library"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1077
    const-string/jumbo v2, "user"

    sget-object v3, Lcom/peel/content/a;->f:Lcom/peel/content/user/User;

    invoke-virtual {v3}, Lcom/peel/content/user/User;->x()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1078
    new-instance v2, Lcom/peel/content/library/h;

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v2, v0, v1}, Lcom/peel/content/library/h;-><init>(Lcom/peel/content/library/LiveLibrary;Lcom/peel/util/t;)V

    move-object/from16 v0, p1

    invoke-static {v0, v2}, Lcom/peel/content/a/j;->b(Landroid/os/Bundle;Lcom/peel/util/t;)V

    goto/16 :goto_1

    .line 1089
    :cond_b
    const-string/jumbo v3, "fav"

    invoke-virtual {v5, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_c

    .line 1090
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/peel/content/library/LiveLibrary;->e:Landroid/util/LongSparseArray;

    invoke-virtual {v3}, Landroid/util/LongSparseArray;->clear()V

    .line 1091
    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/peel/data/Channel;->a(Z)V

    .line 1093
    const-string/jumbo v2, "prgsvcids"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1094
    const-string/jumbo v2, "library"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1095
    const-string/jumbo v2, "user"

    sget-object v3, Lcom/peel/content/a;->f:Lcom/peel/content/user/User;

    invoke-virtual {v3}, Lcom/peel/content/user/User;->x()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1096
    new-instance v2, Lcom/peel/content/library/i;

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v2, v0, v1}, Lcom/peel/content/library/i;-><init>(Lcom/peel/content/library/LiveLibrary;Lcom/peel/util/t;)V

    move-object/from16 v0, p1

    invoke-static {v0, v2}, Lcom/peel/content/a/j;->b(Landroid/os/Bundle;Lcom/peel/util/t;)V

    goto/16 :goto_1

    .line 1107
    :cond_c
    const-string/jumbo v3, "uncut"

    invoke-virtual {v5, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_d

    .line 1108
    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/peel/data/Channel;->b(Z)V

    .line 1109
    if-eqz p2, :cond_0

    const/4 v2, 0x1

    const/4 v3, 0x0

    const/4 v4, 0x0

    move-object/from16 v0, p2

    invoke-virtual {v0, v2, v3, v4}, Lcom/peel/util/t;->a(ZLjava/lang/Object;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 1113
    :cond_d
    const-string/jumbo v3, "cut"

    invoke-virtual {v5, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_e

    .line 1114
    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/peel/data/Channel;->b(Z)V

    .line 1115
    if-eqz p2, :cond_0

    const/4 v2, 0x1

    const/4 v3, 0x0

    const/4 v4, 0x0

    move-object/from16 v0, p2

    invoke-virtual {v0, v2, v3, v4}, Lcom/peel/util/t;->a(ZLjava/lang/Object;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 1121
    :cond_e
    const-string/jumbo v2, "add/setreminder"

    invoke-virtual {v5, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_f

    .line 1122
    const-string/jumbo v2, "listing"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Lcom/peel/content/listing/LiveListing;

    .line 1124
    if-eqz v2, :cond_0

    .line 1125
    new-instance v3, Lcom/peel/content/library/j;

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v3, v0, v2, v1}, Lcom/peel/content/library/j;-><init>(Lcom/peel/content/library/LiveLibrary;Lcom/peel/content/listing/LiveListing;Lcom/peel/util/t;)V

    move-object/from16 v0, p1

    invoke-static {v0, v3}, Lcom/peel/content/a/j;->b(Landroid/os/Bundle;Lcom/peel/util/t;)V

    goto/16 :goto_1

    .line 1140
    :cond_f
    const-string/jumbo v2, "delete/setreminder"

    invoke-virtual {v5, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_10

    .line 1141
    const-string/jumbo v2, "listing"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Lcom/peel/content/listing/LiveListing;

    .line 1143
    if-eqz v2, :cond_0

    .line 1144
    new-instance v3, Lcom/peel/content/library/k;

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v3, v0, v2, v1}, Lcom/peel/content/library/k;-><init>(Lcom/peel/content/library/LiveLibrary;Lcom/peel/content/listing/LiveListing;Lcom/peel/util/t;)V

    move-object/from16 v0, p1

    invoke-static {v0, v3}, Lcom/peel/content/a/j;->b(Landroid/os/Bundle;Lcom/peel/util/t;)V

    goto/16 :goto_1

    .line 1159
    :cond_10
    const-string/jumbo v2, "add/reminder"

    invoke-virtual {v5, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_14

    .line 1160
    const-string/jumbo v2, "id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1161
    if-nez v3, :cond_11

    const/16 v18, 0x0

    .line 1163
    :goto_7
    if-eqz v18, :cond_12

    .line 1165
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 1166
    const-string/jumbo v2, "object_type"

    const-class v4, Landroid/os/Bundle;

    invoke-virtual {v4}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v2, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1167
    move-object/from16 v0, p1

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->putAll(Landroid/os/Bundle;)V

    .line 1168
    const-string/jumbo v2, "path"

    invoke-virtual {v3, v2}, Landroid/os/Bundle;->remove(Ljava/lang/String;)V

    move-object/from16 v2, v18

    .line 1169
    check-cast v2, Lcom/peel/content/listing/LiveListing;

    invoke-virtual {v2, v3}, Lcom/peel/content/listing/LiveListing;->a(Landroid/os/Bundle;)V

    .line 1171
    new-instance v19, Ljava/util/ArrayList;

    invoke-direct/range {v19 .. v19}, Ljava/util/ArrayList;-><init>()V

    .line 1172
    new-instance v2, Lcom/peel/data/l;

    invoke-virtual/range {v18 .. v18}, Lcom/peel/content/listing/Listing;->f()Ljava/lang/String;

    move-result-object v3

    .line 1173
    invoke-virtual/range {v18 .. v18}, Lcom/peel/content/listing/Listing;->g()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v5, v18

    check-cast v5, Lcom/peel/content/listing/LiveListing;

    invoke-virtual {v5}, Lcom/peel/content/listing/LiveListing;->b()Ljava/lang/String;

    move-result-object v5

    .line 1174
    invoke-virtual/range {v18 .. v18}, Lcom/peel/content/listing/Listing;->k()Ljava/lang/String;

    move-result-object v6

    invoke-virtual/range {v18 .. v18}, Lcom/peel/content/listing/Listing;->l()Ljava/lang/String;

    move-result-object v7

    invoke-virtual/range {v18 .. v18}, Lcom/peel/content/listing/Listing;->m()[Ljava/lang/String;

    move-result-object v8

    .line 1175
    invoke-virtual/range {v18 .. v18}, Lcom/peel/content/listing/Listing;->i()I

    move-result v9

    invoke-virtual/range {v18 .. v18}, Lcom/peel/content/listing/Listing;->j()J

    move-result-wide v10

    move-object/from16 v12, v18

    check-cast v12, Lcom/peel/content/listing/LiveListing;

    .line 1176
    invoke-virtual {v12}, Lcom/peel/content/listing/LiveListing;->y()Ljava/lang/String;

    move-result-object v12

    move-object/from16 v13, v18

    check-cast v13, Lcom/peel/content/listing/LiveListing;

    invoke-virtual {v13}, Lcom/peel/content/listing/LiveListing;->z()Ljava/lang/String;

    move-result-object v13

    move-object/from16 v14, v18

    check-cast v14, Lcom/peel/content/listing/LiveListing;

    .line 1177
    invoke-virtual {v14}, Lcom/peel/content/listing/LiveListing;->x()Ljava/lang/String;

    move-result-object v14

    move-object/from16 v15, v18

    check-cast v15, Lcom/peel/content/listing/LiveListing;

    .line 1178
    invoke-virtual {v15}, Lcom/peel/content/listing/LiveListing;->A()Ljava/lang/String;

    move-result-object v15

    move-object/from16 v16, v18

    check-cast v16, Lcom/peel/content/listing/LiveListing;

    .line 1179
    invoke-virtual/range {v16 .. v16}, Lcom/peel/content/listing/LiveListing;->w()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v18 .. v18}, Lcom/peel/content/listing/Listing;->a()Landroid/os/Bundle;

    move-result-object v17

    invoke-direct/range {v2 .. v17}, Lcom/peel/data/l;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;IJLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 1172
    move-object/from16 v0, v19

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1183
    invoke-static {}, Lcom/peel/data/m;->a()Lcom/peel/data/m;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/peel/content/library/LiveLibrary;->a:Ljava/lang/String;

    const/4 v4, 0x1

    move-object/from16 v0, v19

    invoke-virtual {v2, v3, v0, v4}, Lcom/peel/data/m;->a(Ljava/lang/String;Ljava/util/List;Z)V

    .line 1188
    :goto_8
    if-eqz p2, :cond_0

    if-eqz v18, :cond_13

    const/4 v2, 0x1

    :goto_9
    const/4 v3, 0x0

    const/4 v4, 0x0

    move-object/from16 v0, p2

    invoke-virtual {v0, v2, v3, v4}, Lcom/peel/util/t;->a(ZLjava/lang/Object;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 1161
    :cond_11
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/peel/content/library/LiveLibrary;->d:Landroid/util/LruCache;

    invoke-virtual {v2, v3}, Landroid/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/peel/content/listing/Listing;

    move-object/from16 v18, v2

    goto/16 :goto_7

    .line 1185
    :cond_12
    sget-object v2, Lcom/peel/content/library/LiveLibrary;->c:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "unable to add reminder to listing "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_8

    .line 1188
    :cond_13
    const/4 v2, 0x0

    goto :goto_9

    .line 1192
    :cond_14
    const-string/jumbo v2, "delete/reminder"

    invoke-virtual {v5, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_18

    .line 1193
    const-string/jumbo v2, "id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1194
    if-nez v3, :cond_15

    const/16 v18, 0x0

    .line 1195
    :goto_a
    if-eqz v18, :cond_16

    .line 1196
    const-string/jumbo v2, "calendar_uri"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v2, v18

    .line 1197
    check-cast v2, Lcom/peel/content/listing/LiveListing;

    invoke-virtual {v2, v3}, Lcom/peel/content/listing/LiveListing;->b(Ljava/lang/String;)V

    .line 1200
    new-instance v19, Ljava/util/ArrayList;

    invoke-direct/range {v19 .. v19}, Ljava/util/ArrayList;-><init>()V

    .line 1201
    new-instance v2, Lcom/peel/data/l;

    invoke-virtual/range {v18 .. v18}, Lcom/peel/content/listing/Listing;->f()Ljava/lang/String;

    move-result-object v3

    .line 1202
    invoke-virtual/range {v18 .. v18}, Lcom/peel/content/listing/Listing;->g()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v5, v18

    check-cast v5, Lcom/peel/content/listing/LiveListing;

    invoke-virtual {v5}, Lcom/peel/content/listing/LiveListing;->b()Ljava/lang/String;

    move-result-object v5

    .line 1203
    invoke-virtual/range {v18 .. v18}, Lcom/peel/content/listing/Listing;->k()Ljava/lang/String;

    move-result-object v6

    invoke-virtual/range {v18 .. v18}, Lcom/peel/content/listing/Listing;->l()Ljava/lang/String;

    move-result-object v7

    invoke-virtual/range {v18 .. v18}, Lcom/peel/content/listing/Listing;->m()[Ljava/lang/String;

    move-result-object v8

    .line 1204
    invoke-virtual/range {v18 .. v18}, Lcom/peel/content/listing/Listing;->i()I

    move-result v9

    invoke-virtual/range {v18 .. v18}, Lcom/peel/content/listing/Listing;->j()J

    move-result-wide v10

    move-object/from16 v12, v18

    check-cast v12, Lcom/peel/content/listing/LiveListing;

    .line 1205
    invoke-virtual {v12}, Lcom/peel/content/listing/LiveListing;->y()Ljava/lang/String;

    move-result-object v12

    move-object/from16 v13, v18

    check-cast v13, Lcom/peel/content/listing/LiveListing;

    invoke-virtual {v13}, Lcom/peel/content/listing/LiveListing;->z()Ljava/lang/String;

    move-result-object v13

    move-object/from16 v14, v18

    check-cast v14, Lcom/peel/content/listing/LiveListing;

    .line 1206
    invoke-virtual {v14}, Lcom/peel/content/listing/LiveListing;->x()Ljava/lang/String;

    move-result-object v14

    move-object/from16 v15, v18

    check-cast v15, Lcom/peel/content/listing/LiveListing;

    .line 1207
    invoke-virtual {v15}, Lcom/peel/content/listing/LiveListing;->A()Ljava/lang/String;

    move-result-object v15

    move-object/from16 v16, v18

    check-cast v16, Lcom/peel/content/listing/LiveListing;

    .line 1208
    invoke-virtual/range {v16 .. v16}, Lcom/peel/content/listing/LiveListing;->w()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v18 .. v18}, Lcom/peel/content/listing/Listing;->a()Landroid/os/Bundle;

    move-result-object v17

    invoke-direct/range {v2 .. v17}, Lcom/peel/data/l;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;IJLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 1201
    move-object/from16 v0, v19

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1210
    invoke-static {}, Lcom/peel/data/m;->a()Lcom/peel/data/m;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/peel/content/library/LiveLibrary;->a:Ljava/lang/String;

    const/4 v4, 0x1

    move-object/from16 v0, v19

    invoke-virtual {v2, v3, v0, v4}, Lcom/peel/data/m;->a(Ljava/lang/String;Ljava/util/List;Z)V

    .line 1215
    :goto_b
    if-eqz p2, :cond_0

    if-eqz v18, :cond_17

    const/4 v2, 0x1

    :goto_c
    const/4 v3, 0x0

    const/4 v4, 0x0

    move-object/from16 v0, p2

    invoke-virtual {v0, v2, v3, v4}, Lcom/peel/util/t;->a(ZLjava/lang/Object;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 1194
    :cond_15
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/peel/content/library/LiveLibrary;->d:Landroid/util/LruCache;

    invoke-virtual {v2, v3}, Landroid/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/peel/content/listing/Listing;

    move-object/from16 v18, v2

    goto/16 :goto_a

    .line 1212
    :cond_16
    sget-object v2, Lcom/peel/content/library/LiveLibrary;->c:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "unable to delete reminder from listing "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_b

    .line 1215
    :cond_17
    const/4 v2, 0x0

    goto :goto_c

    .line 1219
    :cond_18
    sget-object v2, Lcom/peel/content/library/LiveLibrary;->c:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "unrecognized post: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1220
    const/4 v2, 0x0

    const/4 v3, 0x0

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "unrecognized post: "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p2

    invoke-virtual {v0, v2, v3, v4}, Lcom/peel/util/t;->a(ZLjava/lang/Object;Ljava/lang/String;)V

    goto/16 :goto_1

    :cond_19
    move-object v2, v3

    goto/16 :goto_6
.end method

.method public c()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 578
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 579
    iget-wide v2, p0, Lcom/peel/content/library/LiveLibrary;->l:J

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v4

    cmp-long v1, v2, v4

    if-gez v1, :cond_0

    .line 580
    const/4 v1, 0x5

    invoke-virtual {v0, v1, v6}, Ljava/util/Calendar;->add(II)V

    .line 581
    const/16 v1, 0xb

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->set(II)V

    .line 582
    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/peel/content/library/LiveLibrary;->l:J

    .line 583
    invoke-static {}, Lcom/peel/data/m;->a()Lcom/peel/data/m;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/content/library/LiveLibrary;->a:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/peel/content/library/LiveLibrary;->a()Landroid/os/Bundle;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/peel/data/m;->a(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 585
    :cond_0
    const/16 v0, 0x30

    invoke-virtual {p0, v0, v6}, Lcom/peel/content/library/LiveLibrary;->a(IZ)V

    .line 586
    return-void
.end method

.method public d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 185
    iget-object v0, p0, Lcom/peel/content/library/LiveLibrary;->name:Ljava/lang/String;

    return-object v0
.end method

.method public g()V
    .locals 10

    .prologue
    const/4 v2, 0x0

    .line 633
    new-instance v3, Ljava/util/HashSet;

    invoke-direct {v3}, Ljava/util/HashSet;-><init>()V

    .line 634
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 635
    const/16 v1, 0xa

    const/16 v4, -0x18

    invoke-virtual {v0, v1, v4}, Ljava/util/Calendar;->add(II)V

    .line 636
    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v4

    .line 637
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 639
    iget-object v0, p0, Lcom/peel/content/library/LiveLibrary;->g:Landroid/util/LongSparseArray;

    if-eqz v0, :cond_3

    move v1, v2

    .line 640
    :goto_0
    iget-object v0, p0, Lcom/peel/content/library/LiveLibrary;->g:Landroid/util/LongSparseArray;

    invoke-virtual {v0}, Landroid/util/LongSparseArray;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 641
    iget-object v0, p0, Lcom/peel/content/library/LiveLibrary;->g:Landroid/util/LongSparseArray;

    invoke-virtual {v0, v1}, Landroid/util/LongSparseArray;->keyAt(I)J

    move-result-wide v8

    cmp-long v0, v8, v4

    if-gez v0, :cond_1

    .line 642
    iget-object v0, p0, Lcom/peel/content/library/LiveLibrary;->g:Landroid/util/LongSparseArray;

    invoke-virtual {v0, v1}, Landroid/util/LongSparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 644
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/data/RankCategory;

    .line 645
    invoke-virtual {v0}, Lcom/peel/data/RankCategory;->c()Ljava/util/List;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    goto :goto_1

    .line 648
    :cond_0
    iget-object v0, p0, Lcom/peel/content/library/LiveLibrary;->g:Landroid/util/LongSparseArray;

    invoke-virtual {v0, v1}, Landroid/util/LongSparseArray;->keyAt(I)J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 640
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 652
    :cond_2
    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    .line 653
    iget-object v0, p0, Lcom/peel/content/library/LiveLibrary;->g:Landroid/util/LongSparseArray;

    invoke-virtual {v0, v6, v7}, Landroid/util/LongSparseArray;->delete(J)V

    goto :goto_2

    .line 657
    :cond_3
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 658
    iget-object v0, p0, Lcom/peel/content/library/LiveLibrary;->h:Landroid/util/LongSparseArray;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/peel/content/library/LiveLibrary;->h:Landroid/util/LongSparseArray;

    invoke-virtual {v0}, Landroid/util/LongSparseArray;->size()I

    move-result v0

    if-lez v0, :cond_7

    .line 659
    :goto_3
    iget-object v0, p0, Lcom/peel/content/library/LiveLibrary;->h:Landroid/util/LongSparseArray;

    invoke-virtual {v0}, Landroid/util/LongSparseArray;->size()I

    move-result v0

    if-ge v2, v0, :cond_6

    .line 660
    iget-object v0, p0, Lcom/peel/content/library/LiveLibrary;->h:Landroid/util/LongSparseArray;

    invoke-virtual {v0, v2}, Landroid/util/LongSparseArray;->keyAt(I)J

    move-result-wide v6

    cmp-long v0, v6, v4

    if-gez v0, :cond_5

    .line 661
    iget-object v0, p0, Lcom/peel/content/library/LiveLibrary;->h:Landroid/util/LongSparseArray;

    invoke-virtual {v0, v2}, Landroid/util/LongSparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/LinkedHashSet;

    .line 663
    invoke-virtual {v0}, Ljava/util/LinkedHashSet;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_4
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 664
    invoke-interface {v3, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 667
    :cond_4
    iget-object v0, p0, Lcom/peel/content/library/LiveLibrary;->h:Landroid/util/LongSparseArray;

    invoke-virtual {v0, v2}, Landroid/util/LongSparseArray;->keyAt(I)J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 659
    :cond_5
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 671
    :cond_6
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_5
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    .line 672
    iget-object v0, p0, Lcom/peel/content/library/LiveLibrary;->h:Landroid/util/LongSparseArray;

    invoke-virtual {v0, v4, v5}, Landroid/util/LongSparseArray;->delete(J)V

    goto :goto_5

    .line 676
    :cond_7
    invoke-static {}, Lcom/peel/data/m;->a()Lcom/peel/data/m;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/content/library/LiveLibrary;->a:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/peel/content/library/LiveLibrary;->a()Landroid/os/Bundle;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/peel/data/m;->a(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 677
    invoke-static {}, Lcom/peel/data/m;->a()Lcom/peel/data/m;

    move-result-object v1

    iget-object v2, p0, Lcom/peel/content/library/LiveLibrary;->a:Ljava/lang/String;

    invoke-interface {v3}, Ljava/util/Set;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/String;

    invoke-interface {v3, v0}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Lcom/peel/data/m;->a(Ljava/lang/String;[Ljava/lang/String;)V

    .line 678
    return-void
.end method

.method public h()Ljava/lang/String;
    .locals 1

    .prologue
    .line 187
    iget-object v0, p0, Lcom/peel/content/library/LiveLibrary;->location:Ljava/lang/String;

    return-object v0
.end method

.method public i()[Lcom/peel/data/Channel;
    .locals 1

    .prologue
    .line 189
    iget-object v0, p0, Lcom/peel/content/library/LiveLibrary;->lineup:[Lcom/peel/data/Channel;

    return-object v0
.end method

.method public j()Ljava/lang/String;
    .locals 1

    .prologue
    .line 191
    iget-object v0, p0, Lcom/peel/content/library/LiveLibrary;->mso:Ljava/lang/String;

    return-object v0
.end method

.method public k()Ljava/lang/String;
    .locals 1

    .prologue
    .line 193
    iget-object v0, p0, Lcom/peel/content/library/LiveLibrary;->boxtype:Ljava/lang/String;

    return-object v0
.end method

.method public l()I
    .locals 1

    .prologue
    .line 195
    iget v0, p0, Lcom/peel/content/library/LiveLibrary;->lineupcount:I

    return v0
.end method

.method public m()Ljava/lang/String;
    .locals 1

    .prologue
    .line 197
    iget-object v0, p0, Lcom/peel/content/library/LiveLibrary;->channeldifference:Ljava/lang/String;

    return-object v0
.end method

.method public n()Lcom/peel/content/listing/Listing;
    .locals 2

    .prologue
    .line 1311
    iget-object v0, p0, Lcom/peel/content/library/LiveLibrary;->k:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/peel/content/library/LiveLibrary;->k:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 1312
    iget-object v0, p0, Lcom/peel/content/library/LiveLibrary;->k:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/content/listing/Listing;

    .line 1315
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public o()Lcom/peel/content/listing/Listing;
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 1319
    iget-object v0, p0, Lcom/peel/content/library/LiveLibrary;->k:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/peel/content/library/LiveLibrary;->k:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-le v0, v1, :cond_0

    .line 1320
    iget-object v0, p0, Lcom/peel/content/library/LiveLibrary;->k:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/content/listing/Listing;

    .line 1323
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
