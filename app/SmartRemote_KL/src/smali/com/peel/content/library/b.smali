.class Lcom/peel/content/library/b;
.super Lcom/peel/util/t;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/peel/util/t",
        "<[",
        "Lcom/peel/data/l;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Landroid/os/Bundle;

.field final synthetic b:Lcom/peel/util/t;

.field final synthetic c:Lcom/peel/util/t;

.field final synthetic d:Lcom/peel/content/library/DirecTVLibrary;


# direct methods
.method constructor <init>(Lcom/peel/content/library/DirecTVLibrary;ILandroid/os/Bundle;Lcom/peel/util/t;Lcom/peel/util/t;)V
    .locals 0

    .prologue
    .line 151
    iput-object p1, p0, Lcom/peel/content/library/b;->d:Lcom/peel/content/library/DirecTVLibrary;

    iput-object p3, p0, Lcom/peel/content/library/b;->a:Landroid/os/Bundle;

    iput-object p4, p0, Lcom/peel/content/library/b;->b:Lcom/peel/util/t;

    iput-object p5, p0, Lcom/peel/content/library/b;->c:Lcom/peel/util/t;

    invoke-direct {p0, p2}, Lcom/peel/util/t;-><init>(I)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 18

    .prologue
    .line 153
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/peel/content/library/b;->i:Z

    if-nez v2, :cond_0

    .line 154
    invoke-static {}, Lcom/peel/content/library/DirecTVLibrary;->d()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/peel/content/library/b;->k:Ljava/lang/String;

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 157
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/peel/content/library/b;->d:Lcom/peel/content/library/DirecTVLibrary;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/peel/content/library/b;->a:Landroid/os/Bundle;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/peel/content/library/b;->b:Lcom/peel/util/t;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/peel/content/library/b;->c:Lcom/peel/util/t;

    invoke-static {v2, v3, v4, v5}, Lcom/peel/content/library/DirecTVLibrary;->a(Lcom/peel/content/library/DirecTVLibrary;Landroid/os/Bundle;Lcom/peel/util/t;Lcom/peel/util/t;)V

    .line 173
    :goto_0
    return-void

    .line 160
    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/peel/content/library/b;->j:Ljava/lang/Object;

    check-cast v2, [Lcom/peel/data/l;

    .line 161
    array-length v3, v2

    new-array v15, v3, [Ljava/lang/String;

    .line 162
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/peel/content/library/b;->d:Lcom/peel/content/library/DirecTVLibrary;

    invoke-static {v3}, Lcom/peel/content/library/DirecTVLibrary;->a(Lcom/peel/content/library/DirecTVLibrary;)Ljava/util/Map;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Map;->clear()V

    .line 163
    const/4 v4, 0x0

    .line 164
    array-length v0, v2

    move/from16 v16, v0

    const/4 v3, 0x0

    move v13, v3

    move v14, v4

    :goto_1
    move/from16 v0, v16

    if-ge v13, v0, :cond_1

    aget-object v17, v2, v13

    .line 165
    new-instance v3, Lcom/peel/content/listing/DirecTVListing;

    invoke-virtual/range {v17 .. v17}, Lcom/peel/data/l;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual/range {v17 .. v17}, Lcom/peel/data/l;->b()Ljava/lang/String;

    move-result-object v5

    .line 166
    invoke-virtual/range {v17 .. v17}, Lcom/peel/data/l;->i()Ljava/lang/String;

    move-result-object v6

    invoke-virtual/range {v17 .. v17}, Lcom/peel/data/l;->d()Ljava/lang/String;

    move-result-object v7

    invoke-virtual/range {v17 .. v17}, Lcom/peel/data/l;->e()[Ljava/lang/String;

    move-result-object v8

    .line 167
    invoke-virtual/range {v17 .. v17}, Lcom/peel/data/l;->f()I

    move-result v9

    invoke-virtual/range {v17 .. v17}, Lcom/peel/data/l;->g()J

    move-result-wide v10

    invoke-virtual/range {v17 .. v17}, Lcom/peel/data/l;->h()Landroid/os/Bundle;

    move-result-object v12

    invoke-direct/range {v3 .. v12}, Lcom/peel/content/listing/DirecTVListing;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;IJLandroid/os/Bundle;)V

    .line 168
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/peel/content/library/b;->d:Lcom/peel/content/library/DirecTVLibrary;

    invoke-static {v4}, Lcom/peel/content/library/DirecTVLibrary;->a(Lcom/peel/content/library/DirecTVLibrary;)Ljava/util/Map;

    move-result-object v4

    invoke-virtual/range {v17 .. v17}, Lcom/peel/data/l;->a()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 169
    aget-object v3, v2, v14

    invoke-virtual {v3}, Lcom/peel/data/l;->b()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v15, v14

    .line 170
    add-int/lit8 v4, v14, 0x1

    .line 164
    add-int/lit8 v3, v13, 0x1

    move v13, v3

    move v14, v4

    goto :goto_1

    .line 172
    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/peel/content/library/b;->d:Lcom/peel/content/library/DirecTVLibrary;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/peel/content/library/b;->d:Lcom/peel/content/library/DirecTVLibrary;

    invoke-static {v3}, Lcom/peel/content/library/DirecTVLibrary;->a(Lcom/peel/content/library/DirecTVLibrary;)Ljava/util/Map;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/peel/content/library/b;->d:Lcom/peel/content/library/DirecTVLibrary;

    invoke-static {v4}, Lcom/peel/content/library/DirecTVLibrary;->a(Lcom/peel/content/library/DirecTVLibrary;)Ljava/util/Map;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Map;->size()I

    move-result v4

    new-array v4, v4, [Lcom/peel/content/listing/Listing;

    invoke-interface {v3, v4}, Ljava/util/Collection;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [Lcom/peel/content/listing/Listing;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/peel/content/library/b;->a:Landroid/os/Bundle;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/peel/content/library/b;->b:Lcom/peel/util/t;

    const/4 v7, 0x0

    move-object v3, v15

    invoke-static/range {v2 .. v7}, Lcom/peel/content/library/DirecTVLibrary;->a(Lcom/peel/content/library/DirecTVLibrary;[Ljava/lang/String;[Lcom/peel/content/listing/Listing;Landroid/os/Bundle;Lcom/peel/util/t;Z)V

    goto/16 :goto_0
.end method
