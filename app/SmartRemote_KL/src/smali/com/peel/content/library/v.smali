.class Lcom/peel/content/library/v;
.super Lcom/peel/util/t;


# instance fields
.field final synthetic a:Lcom/peel/content/library/u;


# direct methods
.method constructor <init>(Lcom/peel/content/library/u;I)V
    .locals 0

    .prologue
    .line 851
    iput-object p1, p0, Lcom/peel/content/library/v;->a:Lcom/peel/content/library/u;

    invoke-direct {p0, p2}, Lcom/peel/util/t;-><init>(I)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    .line 854
    invoke-static {}, Lcom/peel/content/library/LiveLibrary;->p()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Legacy getShowtime: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/peel/content/library/v;->i:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/peel/content/library/v;->k:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 855
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 856
    iget-boolean v0, p0, Lcom/peel/content/library/v;->i:Z

    if-eqz v0, :cond_2

    .line 857
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 858
    iget-object v0, p0, Lcom/peel/content/library/v;->j:Ljava/lang/Object;

    check-cast v0, [Lcom/peel/content/listing/LiveListing;

    check-cast v0, [Lcom/peel/content/listing/LiveListing;

    array-length v4, v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v4, :cond_1

    aget-object v5, v0, v1

    .line 859
    iget-object v6, p0, Lcom/peel/content/library/v;->a:Lcom/peel/content/library/u;

    iget-object v6, v6, Lcom/peel/content/library/u;->d:Lcom/peel/content/library/LiveLibrary;

    invoke-static {v6}, Lcom/peel/content/library/LiveLibrary;->a(Lcom/peel/content/library/LiveLibrary;)Landroid/util/LruCache;

    move-result-object v6

    invoke-virtual {v5}, Lcom/peel/content/listing/LiveListing;->f()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    if-nez v6, :cond_0

    .line 860
    iget-object v6, p0, Lcom/peel/content/library/v;->a:Lcom/peel/content/library/u;

    iget-object v6, v6, Lcom/peel/content/library/u;->d:Lcom/peel/content/library/LiveLibrary;

    invoke-static {v6}, Lcom/peel/content/library/LiveLibrary;->a(Lcom/peel/content/library/LiveLibrary;)Landroid/util/LruCache;

    move-result-object v6

    invoke-virtual {v5}, Lcom/peel/content/listing/LiveListing;->f()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7, v5}, Landroid/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 862
    :cond_0
    invoke-virtual {v5}, Lcom/peel/content/listing/LiveListing;->f()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 863
    iget-object v6, p0, Lcom/peel/content/library/v;->a:Lcom/peel/content/library/u;

    iget-object v6, v6, Lcom/peel/content/library/u;->d:Lcom/peel/content/library/LiveLibrary;

    invoke-static {v6}, Lcom/peel/content/library/LiveLibrary;->a(Lcom/peel/content/library/LiveLibrary;)Landroid/util/LruCache;

    move-result-object v6

    invoke-virtual {v5}, Lcom/peel/content/listing/LiveListing;->f()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v6, v5}, Landroid/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    invoke-interface {v2, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 858
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 865
    :cond_1
    iget-object v0, p0, Lcom/peel/content/library/v;->a:Lcom/peel/content/library/u;

    iget-object v0, v0, Lcom/peel/content/library/u;->d:Lcom/peel/content/library/LiveLibrary;

    invoke-static {v0}, Lcom/peel/content/library/LiveLibrary;->e(Lcom/peel/content/library/LiveLibrary;)Ljava/util/Map;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/content/library/v;->a:Lcom/peel/content/library/u;

    iget-object v1, v1, Lcom/peel/content/library/u;->b:Landroid/os/Bundle;

    const-string/jumbo v4, "id"

    invoke-virtual {v1, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 868
    :cond_2
    iget-object v0, p0, Lcom/peel/content/library/v;->a:Lcom/peel/content/library/u;

    iget-object v0, v0, Lcom/peel/content/library/u;->d:Lcom/peel/content/library/LiveLibrary;

    iget-object v1, p0, Lcom/peel/content/library/v;->a:Lcom/peel/content/library/u;

    iget-object v1, v1, Lcom/peel/content/library/u;->c:Lcom/peel/util/t;

    invoke-static {v0, v2, v1}, Lcom/peel/content/library/LiveLibrary;->a(Lcom/peel/content/library/LiveLibrary;Ljava/util/List;Lcom/peel/util/t;)V

    .line 869
    return-void
.end method
