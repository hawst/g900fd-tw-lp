.class Lcom/peel/content/library/c;
.super Lcom/peel/util/t;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/peel/util/t",
        "<[",
        "Lcom/peel/content/listing/Listing;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/peel/util/t;

.field final synthetic b:Landroid/os/Bundle;

.field final synthetic c:Lcom/peel/content/library/DirecTVLibrary;


# direct methods
.method constructor <init>(Lcom/peel/content/library/DirecTVLibrary;ILcom/peel/util/t;Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 185
    iput-object p1, p0, Lcom/peel/content/library/c;->c:Lcom/peel/content/library/DirecTVLibrary;

    iput-object p3, p0, Lcom/peel/content/library/c;->a:Lcom/peel/util/t;

    iput-object p4, p0, Lcom/peel/content/library/c;->b:Landroid/os/Bundle;

    invoke-direct {p0, p2}, Lcom/peel/util/t;-><init>(I)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    const/4 v0, 0x0

    .line 187
    iget-boolean v1, p0, Lcom/peel/content/library/c;->i:Z

    if-nez v1, :cond_0

    .line 189
    invoke-static {}, Lcom/peel/content/library/DirecTVLibrary;->d()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "error getting playlist off DTV DVR: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/peel/content/library/c;->k:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 190
    iget-object v1, p0, Lcom/peel/content/library/c;->a:Lcom/peel/util/t;

    iget-boolean v2, p0, Lcom/peel/content/library/c;->i:Z

    iget-object v3, p0, Lcom/peel/content/library/c;->j:Ljava/lang/Object;

    iget-object v4, p0, Lcom/peel/content/library/c;->k:Ljava/lang/String;

    invoke-virtual {v1, v2, v3, v4}, Lcom/peel/util/t;->a(ZLjava/lang/Object;Ljava/lang/String;)V

    .line 191
    invoke-static {v0}, Lcom/peel/content/library/DirecTVLibrary;->a(Z)Z

    .line 205
    :goto_0
    return-void

    .line 195
    :cond_0
    iget-object v2, p0, Lcom/peel/content/library/c;->j:Ljava/lang/Object;

    check-cast v2, [Lcom/peel/content/listing/Listing;

    .line 196
    array-length v1, v2

    new-array v1, v1, [Ljava/lang/String;

    .line 197
    iget-object v3, p0, Lcom/peel/content/library/c;->c:Lcom/peel/content/library/DirecTVLibrary;

    invoke-static {v3}, Lcom/peel/content/library/DirecTVLibrary;->a(Lcom/peel/content/library/DirecTVLibrary;)Ljava/util/Map;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Map;->clear()V

    .line 199
    array-length v4, v2

    move v3, v0

    :goto_1
    if-ge v0, v4, :cond_1

    aget-object v5, v2, v0

    .line 200
    iget-object v6, p0, Lcom/peel/content/library/c;->c:Lcom/peel/content/library/DirecTVLibrary;

    invoke-static {v6}, Lcom/peel/content/library/DirecTVLibrary;->a(Lcom/peel/content/library/DirecTVLibrary;)Ljava/util/Map;

    move-result-object v6

    invoke-virtual {v5}, Lcom/peel/content/listing/Listing;->f()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v6, v7, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 201
    invoke-virtual {v5}, Lcom/peel/content/listing/Listing;->g()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v1, v3

    .line 202
    add-int/lit8 v3, v3, 0x1

    .line 199
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 204
    :cond_1
    iget-object v0, p0, Lcom/peel/content/library/c;->c:Lcom/peel/content/library/DirecTVLibrary;

    iget-object v3, p0, Lcom/peel/content/library/c;->b:Landroid/os/Bundle;

    iget-object v4, p0, Lcom/peel/content/library/c;->a:Lcom/peel/util/t;

    const/4 v5, 0x1

    invoke-static/range {v0 .. v5}, Lcom/peel/content/library/DirecTVLibrary;->a(Lcom/peel/content/library/DirecTVLibrary;[Ljava/lang/String;[Lcom/peel/content/listing/Listing;Landroid/os/Bundle;Lcom/peel/util/t;Z)V

    goto :goto_0
.end method
