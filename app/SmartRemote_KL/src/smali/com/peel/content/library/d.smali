.class final Lcom/peel/content/library/d;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/peel/content/library/Library;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/os/Parcel;)Lcom/peel/content/library/Library;
    .locals 3

    .prologue
    .line 18
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 19
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 20
    const-class v2, Lcom/peel/content/library/Library;

    invoke-virtual {v2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v2

    invoke-virtual {p1, v2}, Landroid/os/Parcel;->readBundle(Ljava/lang/ClassLoader;)Landroid/os/Bundle;

    move-result-object v2

    .line 21
    invoke-static {v1, v0, v2}, Lcom/peel/content/library/Library;->a(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Lcom/peel/content/library/Library;

    move-result-object v0

    return-object v0
.end method

.method public a(I)[Lcom/peel/content/library/Library;
    .locals 1

    .prologue
    .line 24
    new-array v0, p1, [Lcom/peel/content/library/Library;

    return-object v0
.end method

.method public synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 16
    invoke-virtual {p0, p1}, Lcom/peel/content/library/d;->a(Landroid/os/Parcel;)Lcom/peel/content/library/Library;

    move-result-object v0

    return-object v0
.end method

.method public synthetic newArray(I)[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 16
    invoke-virtual {p0, p1}, Lcom/peel/content/library/d;->a(I)[Lcom/peel/content/library/Library;

    move-result-object v0

    return-object v0
.end method
