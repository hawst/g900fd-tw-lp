.class Lcom/peel/content/library/x;
.super Lcom/peel/util/t;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/peel/util/t",
        "<[",
        "Lcom/peel/data/Channel;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/peel/util/t;

.field final synthetic b:Lcom/peel/content/library/LiveLibrary;


# direct methods
.method constructor <init>(Lcom/peel/content/library/LiveLibrary;ILcom/peel/util/t;)V
    .locals 0

    .prologue
    .line 921
    iput-object p1, p0, Lcom/peel/content/library/x;->b:Lcom/peel/content/library/LiveLibrary;

    iput-object p3, p0, Lcom/peel/content/library/x;->a:Lcom/peel/util/t;

    invoke-direct {p0, p2}, Lcom/peel/util/t;-><init>(I)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 923
    iget-boolean v0, p0, Lcom/peel/content/library/x;->i:Z

    if-nez v0, :cond_0

    .line 924
    iget-object v0, p0, Lcom/peel/content/library/x;->a:Lcom/peel/util/t;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/peel/content/library/x;->j:Ljava/lang/Object;

    iget-object v3, p0, Lcom/peel/content/library/x;->k:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3}, Lcom/peel/util/t;->a(ZLjava/lang/Object;Ljava/lang/String;)V

    .line 926
    invoke-static {}, Lcom/peel/content/library/LiveLibrary;->p()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "LiveLegacySource.get(lineup) failed: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/peel/content/library/x;->k:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 936
    :goto_0
    return-void

    .line 930
    :cond_0
    iget-object v1, p0, Lcom/peel/content/library/x;->b:Lcom/peel/content/library/LiveLibrary;

    iget-object v0, p0, Lcom/peel/content/library/x;->j:Ljava/lang/Object;

    check-cast v0, [Lcom/peel/data/Channel;

    invoke-static {v1, v0}, Lcom/peel/content/library/LiveLibrary;->a(Lcom/peel/content/library/LiveLibrary;[Lcom/peel/data/Channel;)[Lcom/peel/data/Channel;

    .line 934
    iget-object v0, p0, Lcom/peel/content/library/x;->a:Lcom/peel/util/t;

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/peel/content/library/x;->j:Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lcom/peel/util/t;->a(ZLjava/lang/Object;Ljava/lang/String;)V

    goto :goto_0
.end method
