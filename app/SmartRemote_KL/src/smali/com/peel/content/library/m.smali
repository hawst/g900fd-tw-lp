.class Lcom/peel/content/library/m;
.super Lcom/peel/util/t;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/peel/util/t",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Ljava/util/List;

.field final synthetic b:Ljava/util/List;

.field final synthetic c:Ljava/lang/String;

.field final synthetic d:Lcom/peel/util/t;

.field final synthetic e:Lcom/peel/content/library/LiveLibrary;


# direct methods
.method constructor <init>(Lcom/peel/content/library/LiveLibrary;Ljava/util/List;Ljava/util/List;Ljava/lang/String;Lcom/peel/util/t;)V
    .locals 0

    .prologue
    .line 1352
    iput-object p1, p0, Lcom/peel/content/library/m;->e:Lcom/peel/content/library/LiveLibrary;

    iput-object p2, p0, Lcom/peel/content/library/m;->a:Ljava/util/List;

    iput-object p3, p0, Lcom/peel/content/library/m;->b:Ljava/util/List;

    iput-object p4, p0, Lcom/peel/content/library/m;->c:Ljava/lang/String;

    iput-object p5, p0, Lcom/peel/content/library/m;->d:Lcom/peel/util/t;

    invoke-direct {p0}, Lcom/peel/util/t;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(ZLjava/lang/Object;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1352
    check-cast p2, Ljava/lang/String;

    invoke-virtual {p0, p1, p2, p3}, Lcom/peel/content/library/m;->a(ZLjava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public a(ZLjava/lang/String;Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 1355
    if-eqz p1, :cond_1

    if-eqz p2, :cond_1

    .line 1357
    :try_start_0
    new-instance v1, Lorg/json/JSONArray;

    invoke-direct {v1, p2}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 1358
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 1360
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1}, Lorg/json/JSONArray;->length()I

    move-result v3

    if-ge v0, v3, :cond_2

    .line 1361
    invoke-virtual {v1, v0}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 1362
    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1364
    iget-object v4, p0, Lcom/peel/content/library/m;->e:Lcom/peel/content/library/LiveLibrary;

    invoke-static {v4}, Lcom/peel/content/library/LiveLibrary;->f(Lcom/peel/content/library/LiveLibrary;)Landroid/util/LruCache;

    move-result-object v4

    invoke-virtual {v4, v3}, Landroid/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 1365
    iget-object v4, p0, Lcom/peel/content/library/m;->a:Ljava/util/List;

    iget-object v5, p0, Lcom/peel/content/library/m;->e:Lcom/peel/content/library/LiveLibrary;

    invoke-static {v5}, Lcom/peel/content/library/LiveLibrary;->f(Lcom/peel/content/library/LiveLibrary;)Landroid/util/LruCache;

    move-result-object v5

    invoke-virtual {v5, v3}, Landroid/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v4, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1360
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1367
    :cond_0
    iget-object v4, p0, Lcom/peel/content/library/m;->b:Ljava/util/List;

    invoke-interface {v4, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 1375
    :catch_0
    move-exception v0

    .line 1376
    invoke-static {}, Lcom/peel/content/library/LiveLibrary;->p()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1379
    :cond_1
    :goto_2
    return-void

    .line 1371
    :cond_2
    :try_start_1
    iget-object v0, p0, Lcom/peel/content/library/m;->e:Lcom/peel/content/library/LiveLibrary;

    invoke-static {v0}, Lcom/peel/content/library/LiveLibrary;->g(Lcom/peel/content/library/LiveLibrary;)Landroid/util/LruCache;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/content/library/m;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1372
    iget-object v0, p0, Lcom/peel/content/library/m;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_1

    .line 1373
    iget-object v0, p0, Lcom/peel/content/library/m;->e:Lcom/peel/content/library/LiveLibrary;

    iget-object v1, p0, Lcom/peel/content/library/m;->b:Ljava/util/List;

    iget-object v2, p0, Lcom/peel/content/library/m;->d:Lcom/peel/util/t;

    invoke-virtual {v0, v1, v2}, Lcom/peel/content/library/LiveLibrary;->a(Ljava/util/List;Lcom/peel/util/t;)V
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2
.end method
