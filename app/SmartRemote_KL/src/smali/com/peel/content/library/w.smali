.class Lcom/peel/content/library/w;
.super Lcom/peel/util/t;


# instance fields
.field final synthetic a:Lcom/peel/util/t;

.field final synthetic b:Landroid/os/Bundle;

.field final synthetic c:Lcom/peel/content/library/LiveLibrary;


# direct methods
.method constructor <init>(Lcom/peel/content/library/LiveLibrary;ILcom/peel/util/t;Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 879
    iput-object p1, p0, Lcom/peel/content/library/w;->c:Lcom/peel/content/library/LiveLibrary;

    iput-object p3, p0, Lcom/peel/content/library/w;->a:Lcom/peel/util/t;

    iput-object p4, p0, Lcom/peel/content/library/w;->b:Landroid/os/Bundle;

    invoke-direct {p0, p2}, Lcom/peel/util/t;-><init>(I)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .prologue
    const/4 v4, 0x0

    const/4 v1, 0x0

    .line 882
    invoke-static {}, Lcom/peel/content/library/LiveLibrary;->p()Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Legacy getShowtime: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/peel/content/library/w;->i:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/peel/content/library/w;->k:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 883
    iget-boolean v0, p0, Lcom/peel/content/library/w;->i:Z

    if-nez v0, :cond_0

    .line 884
    iget-object v0, p0, Lcom/peel/content/library/w;->a:Lcom/peel/util/t;

    iget-object v2, p0, Lcom/peel/content/library/w;->k:Ljava/lang/String;

    invoke-virtual {v0, v1, v4, v2}, Lcom/peel/util/t;->a(ZLjava/lang/Object;Ljava/lang/String;)V

    .line 899
    :goto_0
    return-void

    .line 887
    :cond_0
    iget-object v0, p0, Lcom/peel/content/library/w;->j:Ljava/lang/Object;

    if-nez v0, :cond_1

    .line 888
    iget-object v0, p0, Lcom/peel/content/library/w;->a:Lcom/peel/util/t;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "no listing found in showtimes.get("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/peel/content/library/w;->c:Lcom/peel/content/library/LiveLibrary;

    iget-object v3, v3, Lcom/peel/content/library/LiveLibrary;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v4, v2}, Lcom/peel/util/t;->a(ZLjava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 891
    :cond_1
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 892
    iget-object v0, p0, Lcom/peel/content/library/w;->j:Ljava/lang/Object;

    check-cast v0, [Lcom/peel/content/listing/LiveListing;

    check-cast v0, [Lcom/peel/content/listing/LiveListing;

    array-length v3, v0

    :goto_1
    if-ge v1, v3, :cond_2

    aget-object v4, v0, v1

    .line 893
    iget-object v5, p0, Lcom/peel/content/library/w;->c:Lcom/peel/content/library/LiveLibrary;

    invoke-static {v5}, Lcom/peel/content/library/LiveLibrary;->a(Lcom/peel/content/library/LiveLibrary;)Landroid/util/LruCache;

    move-result-object v5

    invoke-virtual {v4}, Lcom/peel/content/listing/LiveListing;->f()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6, v4}, Landroid/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 894
    invoke-virtual {v4}, Lcom/peel/content/listing/LiveListing;->f()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 892
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 896
    :cond_2
    iget-object v0, p0, Lcom/peel/content/library/w;->c:Lcom/peel/content/library/LiveLibrary;

    invoke-static {v0}, Lcom/peel/content/library/LiveLibrary;->e(Lcom/peel/content/library/LiveLibrary;)Ljava/util/Map;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/content/library/w;->b:Landroid/os/Bundle;

    const-string/jumbo v3, "id"

    invoke-virtual {v1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 897
    iget-object v1, p0, Lcom/peel/content/library/w;->c:Lcom/peel/content/library/LiveLibrary;

    new-instance v2, Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/peel/content/library/w;->j:Ljava/lang/Object;

    check-cast v0, [Lcom/peel/content/listing/Listing;

    check-cast v0, [Lcom/peel/content/listing/Listing;

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iget-object v0, p0, Lcom/peel/content/library/w;->a:Lcom/peel/util/t;

    invoke-static {v1, v2, v0}, Lcom/peel/content/library/LiveLibrary;->a(Lcom/peel/content/library/LiveLibrary;Ljava/util/List;Lcom/peel/util/t;)V

    goto :goto_0
.end method
