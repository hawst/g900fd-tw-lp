.class Lcom/peel/content/library/o;
.super Lcom/peel/util/t;


# instance fields
.field final synthetic a:Lcom/peel/util/t;

.field final synthetic b:Landroid/os/Bundle;

.field final synthetic c:Lcom/peel/content/library/LiveLibrary;


# direct methods
.method constructor <init>(Lcom/peel/content/library/LiveLibrary;ILcom/peel/util/t;Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 322
    iput-object p1, p0, Lcom/peel/content/library/o;->c:Lcom/peel/content/library/LiveLibrary;

    iput-object p3, p0, Lcom/peel/content/library/o;->a:Lcom/peel/util/t;

    iput-object p4, p0, Lcom/peel/content/library/o;->b:Landroid/os/Bundle;

    invoke-direct {p0, p2}, Lcom/peel/util/t;-><init>(I)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 28

    .prologue
    .line 324
    invoke-static {}, Lcom/peel/content/library/LiveLibrary;->p()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "getListingsFromLegacy +"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 325
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/peel/content/library/o;->i:Z

    if-nez v2, :cond_0

    .line 326
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/peel/content/library/o;->a:Lcom/peel/util/t;

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/peel/content/library/o;->j:Ljava/lang/Object;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/peel/content/library/o;->k:Ljava/lang/String;

    invoke-virtual {v2, v3, v4, v5}, Lcom/peel/util/t;->a(ZLjava/lang/Object;Ljava/lang/String;)V

    .line 327
    invoke-static {}, Lcom/peel/content/library/LiveLibrary;->p()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "getListingsFromLegacy -"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 544
    :goto_0
    return-void

    .line 331
    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/peel/content/library/o;->b:Landroid/os/Bundle;

    const-string/jumbo v3, "path"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 333
    const-string/jumbo v3, "genre"

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_d

    .line 334
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/peel/content/library/o;->j:Ljava/lang/Object;

    check-cast v2, Ljava/util/List;

    .line 336
    new-instance v19, Ljava/util/ArrayList;

    invoke-direct/range {v19 .. v19}, Ljava/util/ArrayList;-><init>()V

    .line 339
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/peel/content/library/o;->c:Lcom/peel/content/library/LiveLibrary;

    invoke-static {v3}, Lcom/peel/content/library/LiveLibrary;->b(Lcom/peel/content/library/LiveLibrary;)Landroid/util/LongSparseArray;

    move-result-object v14

    .line 341
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/peel/content/library/o;->b:Landroid/os/Bundle;

    const-string/jumbo v4, "cacheWindow"

    const/4 v5, -0x1

    invoke-virtual {v3, v4, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v20

    .line 342
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/peel/content/library/o;->b:Landroid/os/Bundle;

    const-string/jumbo v4, "window"

    const/4 v5, -0x1

    invoke-virtual {v3, v4, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v4

    .line 343
    rem-int/lit8 v3, v4, 0x2

    if-nez v3, :cond_6

    move v3, v4

    .line 344
    :goto_1
    new-instance v21, Ljava/util/ArrayList;

    invoke-direct/range {v21 .. v21}, Ljava/util/ArrayList;-><init>()V

    .line 347
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/peel/content/library/o;->b:Landroid/os/Bundle;

    const-string/jumbo v6, "start"

    const-wide/16 v8, -0x1

    invoke-virtual {v5, v6, v8, v9}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v12

    .line 348
    int-to-long v6, v3

    const-wide/32 v8, 0x1b7740

    mul-long/2addr v6, v8

    add-long v8, v12, v6

    .line 349
    int-to-long v4, v4

    const-wide/32 v6, 0x1b7740

    mul-long/2addr v4, v6

    add-long v16, v12, v4

    .line 350
    move/from16 v0, v20

    int-to-long v4, v0

    const-wide/32 v6, 0x1b7740

    mul-long/2addr v4, v6

    add-long v22, v12, v4

    .line 352
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v15

    :goto_2
    invoke-interface {v15}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_9

    invoke-interface {v15}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/peel/content/listing/Listing;

    .line 353
    instance-of v4, v3, Lcom/peel/content/listing/LiveListing;

    if-eqz v4, :cond_7

    move-object v4, v3

    check-cast v4, Lcom/peel/content/listing/LiveListing;

    .line 354
    invoke-virtual {v4}, Lcom/peel/content/listing/LiveListing;->o()J

    move-result-wide v4

    .line 355
    :goto_3
    invoke-virtual {v3}, Lcom/peel/content/listing/Listing;->j()J

    move-result-wide v6

    add-long/2addr v6, v4

    .line 357
    cmp-long v10, v4, v22

    if-gez v10, :cond_1

    move-object/from16 v0, v21

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 358
    :cond_1
    cmp-long v10, v4, v16

    if-gez v10, :cond_2

    move-object/from16 v0, v19

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 361
    :cond_2
    const-wide/32 v10, 0x1b7740

    rem-long v10, v4, v10

    sub-long v10, v4, v10

    .line 362
    const-wide/32 v4, 0x1b7740

    const-wide/32 v24, 0x1b7740

    rem-long v24, v6, v24

    sub-long v4, v4, v24

    add-long/2addr v4, v6

    .line 365
    cmp-long v6, v12, v10

    if-lez v6, :cond_3

    move-wide v10, v12

    .line 366
    :cond_3
    cmp-long v6, v8, v4

    if-gez v6, :cond_15

    move-wide v6, v8

    .line 368
    :goto_4
    monitor-enter p0

    .line 369
    :goto_5
    cmp-long v4, v10, v6

    if-gez v4, :cond_8

    .line 370
    :try_start_0
    invoke-virtual {v14, v10, v11}, Landroid/util/LongSparseArray;->get(J)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/LinkedHashSet;

    .line 371
    if-nez v4, :cond_4

    .line 372
    new-instance v4, Ljava/util/LinkedHashSet;

    invoke-direct {v4}, Ljava/util/LinkedHashSet;-><init>()V

    .line 373
    invoke-virtual {v14, v10, v11, v4}, Landroid/util/LongSparseArray;->put(JLjava/lang/Object;)V

    .line 375
    :cond_4
    invoke-virtual {v3}, Lcom/peel/content/listing/Listing;->f()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/LinkedHashSet;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_5

    invoke-virtual {v3}, Lcom/peel/content/listing/Listing;->f()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/LinkedHashSet;->add(Ljava/lang/Object;)Z

    .line 369
    :cond_5
    const-wide/32 v4, 0x1b7740

    add-long/2addr v4, v10

    move-wide v10, v4

    goto :goto_5

    .line 343
    :cond_6
    add-int/lit8 v3, v4, 0x1

    goto/16 :goto_1

    .line 354
    :cond_7
    const-wide/16 v4, 0x0

    goto :goto_3

    .line 377
    :cond_8
    monitor-exit p0

    goto :goto_2

    :catchall_0
    move-exception v2

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    .line 380
    :cond_9
    new-instance v22, Ljava/util/ArrayList;

    invoke-direct/range {v22 .. v22}, Ljava/util/ArrayList;-><init>()V

    .line 382
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v23

    :goto_6
    invoke-interface/range {v23 .. v23}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_a

    invoke-interface/range {v23 .. v23}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/peel/content/listing/Listing;

    move-object/from16 v18, v2

    .line 383
    check-cast v18, Lcom/peel/content/listing/LiveListing;

    .line 384
    new-instance v2, Lcom/peel/data/l;

    invoke-virtual/range {v18 .. v18}, Lcom/peel/content/listing/LiveListing;->f()Ljava/lang/String;

    move-result-object v3

    .line 385
    invoke-virtual/range {v18 .. v18}, Lcom/peel/content/listing/LiveListing;->g()Ljava/lang/String;

    move-result-object v4

    invoke-virtual/range {v18 .. v18}, Lcom/peel/content/listing/LiveListing;->b()Ljava/lang/String;

    move-result-object v5

    .line 386
    invoke-virtual/range {v18 .. v18}, Lcom/peel/content/listing/LiveListing;->k()Ljava/lang/String;

    move-result-object v6

    invoke-virtual/range {v18 .. v18}, Lcom/peel/content/listing/LiveListing;->l()Ljava/lang/String;

    move-result-object v7

    .line 387
    invoke-virtual/range {v18 .. v18}, Lcom/peel/content/listing/LiveListing;->m()[Ljava/lang/String;

    move-result-object v8

    invoke-virtual/range {v18 .. v18}, Lcom/peel/content/listing/LiveListing;->i()I

    move-result v9

    .line 388
    invoke-virtual/range {v18 .. v18}, Lcom/peel/content/listing/LiveListing;->j()J

    move-result-wide v10

    invoke-virtual/range {v18 .. v18}, Lcom/peel/content/listing/LiveListing;->y()Ljava/lang/String;

    move-result-object v12

    .line 389
    invoke-virtual/range {v18 .. v18}, Lcom/peel/content/listing/LiveListing;->z()Ljava/lang/String;

    move-result-object v13

    invoke-virtual/range {v18 .. v18}, Lcom/peel/content/listing/LiveListing;->x()Ljava/lang/String;

    move-result-object v14

    .line 390
    invoke-virtual/range {v18 .. v18}, Lcom/peel/content/listing/LiveListing;->A()Ljava/lang/String;

    move-result-object v15

    invoke-virtual/range {v18 .. v18}, Lcom/peel/content/listing/LiveListing;->w()Ljava/lang/String;

    move-result-object v16

    .line 391
    invoke-virtual/range {v18 .. v18}, Lcom/peel/content/listing/LiveListing;->a()Landroid/os/Bundle;

    move-result-object v17

    invoke-direct/range {v2 .. v17}, Lcom/peel/data/l;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;IJLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 384
    move-object/from16 v0, v22

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 392
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/peel/content/library/o;->c:Lcom/peel/content/library/LiveLibrary;

    invoke-static {v2}, Lcom/peel/content/library/LiveLibrary;->a(Lcom/peel/content/library/LiveLibrary;)Landroid/util/LruCache;

    move-result-object v2

    invoke-virtual/range {v18 .. v18}, Lcom/peel/content/listing/LiveListing;->f()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v18

    invoke-virtual {v2, v3, v0}, Landroid/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_6

    .line 395
    :cond_a
    invoke-static {}, Lcom/peel/data/m;->a()Lcom/peel/data/m;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/peel/content/library/o;->c:Lcom/peel/content/library/LiveLibrary;

    iget-object v3, v3, Lcom/peel/content/library/LiveLibrary;->a:Ljava/lang/String;

    const/4 v4, 0x0

    move-object/from16 v0, v22

    invoke-virtual {v2, v3, v0, v4}, Lcom/peel/data/m;->a(Ljava/lang/String;Ljava/util/List;Z)V

    .line 398
    const/4 v2, -0x1

    move/from16 v0, v20

    if-ne v2, v0, :cond_b

    .line 399
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/peel/content/library/o;->a:Lcom/peel/util/t;

    const/4 v3, 0x1

    invoke-interface/range {v19 .. v19}, Ljava/util/List;->size()I

    move-result v4

    new-array v4, v4, [Lcom/peel/content/listing/Listing;

    move-object/from16 v0, v19

    invoke-interface {v0, v4}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v4, v5}, Lcom/peel/util/t;->a(ZLjava/lang/Object;Ljava/lang/String;)V

    .line 400
    sget-object v3, Lcom/peel/content/a;->b:Lcom/peel/content/i;

    const/16 v4, 0x14

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/peel/content/library/o;->c:Lcom/peel/content/library/LiveLibrary;

    iget-object v5, v2, Lcom/peel/content/library/LiveLibrary;->a:Ljava/lang/String;

    const/4 v2, 0x0

    check-cast v2, [Ljava/lang/Object;

    invoke-virtual {v3, v4, v5, v2}, Lcom/peel/content/i;->a(ILjava/lang/Object;[Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 404
    :cond_b
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/peel/content/library/o;->c:Lcom/peel/content/library/LiveLibrary;

    new-instance v3, Lcom/peel/content/library/p;

    const/4 v4, 0x2

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    invoke-direct {v3, v0, v4, v1}, Lcom/peel/content/library/p;-><init>(Lcom/peel/content/library/o;ILjava/util/List;)V

    move-object/from16 v0, v21

    invoke-static {v2, v0, v3}, Lcom/peel/content/library/LiveLibrary;->a(Lcom/peel/content/library/LiveLibrary;Ljava/util/List;Lcom/peel/util/t;)V

    .line 543
    :cond_c
    :goto_7
    invoke-static {}, Lcom/peel/content/library/LiveLibrary;->p()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "getListingsFromLegacy -"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 412
    :cond_d
    const-string/jumbo v3, "top"

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_10

    .line 413
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/peel/content/library/o;->j:Ljava/lang/Object;

    move-object/from16 v18, v2

    check-cast v18, Ljava/util/ArrayList;

    .line 414
    new-instance v23, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual/range {v18 .. v18}, Ljava/util/ArrayList;->size()I

    move-result v2

    move-object/from16 v0, v23

    invoke-direct {v0, v2}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    .line 416
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/peel/content/library/o;->b:Landroid/os/Bundle;

    const-string/jumbo v3, "start"

    const-wide/16 v4, -0x1

    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    .line 417
    const-wide/32 v4, 0x36ee80

    rem-long v4, v2, v4

    sub-long/2addr v2, v4

    .line 420
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/peel/content/library/o;->c:Lcom/peel/content/library/LiveLibrary;

    invoke-static {v4}, Lcom/peel/content/library/LiveLibrary;->c(Lcom/peel/content/library/LiveLibrary;)Landroid/util/LongSparseArray;

    move-result-object v4

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v4, v2, v3, v5}, Landroid/util/LongSparseArray;->put(JLjava/lang/Object;)V

    .line 421
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/peel/content/library/o;->c:Lcom/peel/content/library/LiveLibrary;

    invoke-static {v4}, Lcom/peel/content/library/LiveLibrary;->c(Lcom/peel/content/library/LiveLibrary;)Landroid/util/LongSparseArray;

    move-result-object v4

    invoke-virtual {v4, v2, v3}, Landroid/util/LongSparseArray;->get(J)Ljava/lang/Object;

    move-result-object v2

    move-object/from16 v19, v2

    check-cast v19, Ljava/util/List;

    .line 423
    invoke-virtual/range {v18 .. v18}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v24

    :goto_8
    invoke-interface/range {v24 .. v24}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_c

    invoke-interface/range {v24 .. v24}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object/from16 v20, v2

    check-cast v20, Lcom/peel/content/library/RankCategory;

    .line 424
    invoke-virtual/range {v20 .. v20}, Lcom/peel/content/library/RankCategory;->c()Ljava/util/List;

    move-result-object v25

    .line 431
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 433
    invoke-interface/range {v25 .. v25}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_9
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_e

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/peel/content/listing/Listing;

    .line 434
    invoke-virtual {v2}, Lcom/peel/content/listing/Listing;->f()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_9

    .line 437
    :cond_e
    new-instance v2, Lcom/peel/data/RankCategory;

    invoke-virtual/range {v20 .. v20}, Lcom/peel/content/library/RankCategory;->a()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual/range {v20 .. v20}, Lcom/peel/content/library/RankCategory;->b()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v2, v4, v5, v3}, Lcom/peel/data/RankCategory;-><init>(Ljava/lang/Integer;Ljava/lang/String;Ljava/util/List;)V

    move-object/from16 v0, v19

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 438
    new-instance v26, Ljava/util/ArrayList;

    invoke-direct/range {v26 .. v26}, Ljava/util/ArrayList;-><init>()V

    .line 441
    invoke-interface/range {v25 .. v25}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v27

    :goto_a
    invoke-interface/range {v27 .. v27}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_f

    invoke-interface/range {v27 .. v27}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object/from16 v21, v2

    check-cast v21, Lcom/peel/content/listing/Listing;

    move-object/from16 v22, v21

    .line 442
    check-cast v22, Lcom/peel/content/listing/LiveListing;

    .line 443
    new-instance v2, Lcom/peel/data/l;

    invoke-virtual/range {v22 .. v22}, Lcom/peel/content/listing/LiveListing;->f()Ljava/lang/String;

    move-result-object v3

    .line 444
    invoke-virtual/range {v22 .. v22}, Lcom/peel/content/listing/LiveListing;->g()Ljava/lang/String;

    move-result-object v4

    invoke-virtual/range {v22 .. v22}, Lcom/peel/content/listing/LiveListing;->b()Ljava/lang/String;

    move-result-object v5

    .line 445
    invoke-virtual/range {v22 .. v22}, Lcom/peel/content/listing/LiveListing;->k()Ljava/lang/String;

    move-result-object v6

    invoke-virtual/range {v22 .. v22}, Lcom/peel/content/listing/LiveListing;->l()Ljava/lang/String;

    move-result-object v7

    .line 446
    invoke-virtual/range {v22 .. v22}, Lcom/peel/content/listing/LiveListing;->m()[Ljava/lang/String;

    move-result-object v8

    invoke-virtual/range {v22 .. v22}, Lcom/peel/content/listing/LiveListing;->i()I

    move-result v9

    .line 447
    invoke-virtual/range {v22 .. v22}, Lcom/peel/content/listing/LiveListing;->j()J

    move-result-wide v10

    invoke-virtual/range {v22 .. v22}, Lcom/peel/content/listing/LiveListing;->y()Ljava/lang/String;

    move-result-object v12

    .line 448
    invoke-virtual/range {v22 .. v22}, Lcom/peel/content/listing/LiveListing;->z()Ljava/lang/String;

    move-result-object v13

    invoke-virtual/range {v22 .. v22}, Lcom/peel/content/listing/LiveListing;->x()Ljava/lang/String;

    move-result-object v14

    .line 449
    invoke-virtual/range {v22 .. v22}, Lcom/peel/content/listing/LiveListing;->A()Ljava/lang/String;

    move-result-object v15

    invoke-virtual/range {v22 .. v22}, Lcom/peel/content/listing/LiveListing;->w()Ljava/lang/String;

    move-result-object v16

    .line 450
    invoke-virtual/range {v22 .. v22}, Lcom/peel/content/listing/LiveListing;->a()Landroid/os/Bundle;

    move-result-object v17

    invoke-direct/range {v2 .. v17}, Lcom/peel/data/l;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;IJLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 443
    move-object/from16 v0, v26

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 451
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/peel/content/library/o;->c:Lcom/peel/content/library/LiveLibrary;

    invoke-static {v2}, Lcom/peel/content/library/LiveLibrary;->a(Lcom/peel/content/library/LiveLibrary;)Landroid/util/LruCache;

    move-result-object v2

    invoke-virtual/range {v22 .. v22}, Lcom/peel/content/listing/LiveListing;->f()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v21

    invoke-virtual {v2, v3, v0}, Landroid/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_a

    .line 453
    :cond_f
    invoke-static {}, Lcom/peel/data/m;->a()Lcom/peel/data/m;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/peel/content/library/o;->c:Lcom/peel/content/library/LiveLibrary;

    iget-object v3, v3, Lcom/peel/content/library/LiveLibrary;->a:Ljava/lang/String;

    const/4 v4, 0x0

    move-object/from16 v0, v26

    invoke-virtual {v2, v3, v0, v4}, Lcom/peel/data/m;->a(Ljava/lang/String;Ljava/util/List;Z)V

    .line 456
    invoke-static {}, Lcom/peel/content/library/LiveLibrary;->p()Ljava/lang/String;

    move-result-object v8

    const-string/jumbo v9, "multicast category updateListings"

    new-instance v2, Lcom/peel/content/library/q;

    move-object/from16 v3, p0

    move-object/from16 v4, v20

    move-object/from16 v5, v25

    move-object/from16 v6, v23

    move-object/from16 v7, v18

    invoke-direct/range {v2 .. v7}, Lcom/peel/content/library/q;-><init>(Lcom/peel/content/library/o;Lcom/peel/content/library/RankCategory;Ljava/util/List;Ljava/util/concurrent/atomic/AtomicInteger;Ljava/util/ArrayList;)V

    invoke-static {v8, v9, v2}, Lcom/peel/util/i;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    goto/16 :goto_8

    .line 486
    :cond_10
    const-string/jumbo v3, "weeklyfavs"

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_12

    .line 487
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/peel/content/library/o;->j:Ljava/lang/Object;

    move-object/from16 v18, v2

    check-cast v18, Ljava/util/List;

    .line 489
    invoke-static {}, Ljava/util/GregorianCalendar;->getInstance()Ljava/util/Calendar;

    move-result-object v2

    .line 490
    const/16 v3, 0xb

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Ljava/util/Calendar;->set(II)V

    .line 492
    new-instance v19, Ljava/util/ArrayList;

    invoke-direct/range {v19 .. v19}, Ljava/util/ArrayList;-><init>()V

    .line 493
    invoke-interface/range {v18 .. v18}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v20

    :goto_b
    invoke-interface/range {v20 .. v20}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_11

    invoke-interface/range {v20 .. v20}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/peel/content/listing/Listing;

    move-object/from16 v17, v2

    .line 494
    check-cast v17, Lcom/peel/content/listing/LiveListing;

    .line 495
    new-instance v2, Lcom/peel/data/l;

    invoke-virtual/range {v17 .. v17}, Lcom/peel/content/listing/LiveListing;->f()Ljava/lang/String;

    move-result-object v3

    .line 496
    invoke-virtual/range {v17 .. v17}, Lcom/peel/content/listing/LiveListing;->g()Ljava/lang/String;

    move-result-object v4

    invoke-virtual/range {v17 .. v17}, Lcom/peel/content/listing/LiveListing;->b()Ljava/lang/String;

    move-result-object v5

    .line 497
    invoke-virtual/range {v17 .. v17}, Lcom/peel/content/listing/LiveListing;->k()Ljava/lang/String;

    move-result-object v6

    invoke-virtual/range {v17 .. v17}, Lcom/peel/content/listing/LiveListing;->l()Ljava/lang/String;

    move-result-object v7

    .line 498
    invoke-virtual/range {v17 .. v17}, Lcom/peel/content/listing/LiveListing;->m()[Ljava/lang/String;

    move-result-object v8

    invoke-virtual/range {v17 .. v17}, Lcom/peel/content/listing/LiveListing;->i()I

    move-result v9

    .line 499
    invoke-virtual/range {v17 .. v17}, Lcom/peel/content/listing/LiveListing;->j()J

    move-result-wide v10

    invoke-virtual/range {v17 .. v17}, Lcom/peel/content/listing/LiveListing;->y()Ljava/lang/String;

    move-result-object v12

    .line 500
    invoke-virtual/range {v17 .. v17}, Lcom/peel/content/listing/LiveListing;->z()Ljava/lang/String;

    move-result-object v13

    invoke-virtual/range {v17 .. v17}, Lcom/peel/content/listing/LiveListing;->x()Ljava/lang/String;

    move-result-object v14

    .line 501
    invoke-virtual/range {v17 .. v17}, Lcom/peel/content/listing/LiveListing;->A()Ljava/lang/String;

    move-result-object v15

    invoke-virtual/range {v17 .. v17}, Lcom/peel/content/listing/LiveListing;->w()Ljava/lang/String;

    move-result-object v16

    .line 502
    invoke-virtual/range {v17 .. v17}, Lcom/peel/content/listing/LiveListing;->a()Landroid/os/Bundle;

    move-result-object v17

    invoke-direct/range {v2 .. v17}, Lcom/peel/data/l;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;IJLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 495
    move-object/from16 v0, v19

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_b

    .line 504
    :cond_11
    invoke-static {}, Lcom/peel/data/m;->a()Lcom/peel/data/m;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/peel/content/library/o;->c:Lcom/peel/content/library/LiveLibrary;

    iget-object v3, v3, Lcom/peel/content/library/LiveLibrary;->a:Ljava/lang/String;

    const/4 v4, 0x0

    move-object/from16 v0, v19

    invoke-virtual {v2, v3, v0, v4}, Lcom/peel/data/m;->a(Ljava/lang/String;Ljava/util/List;Z)V

    .line 506
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/peel/content/library/o;->c:Lcom/peel/content/library/LiveLibrary;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/peel/content/library/o;->a:Lcom/peel/util/t;

    move-object/from16 v0, v18

    invoke-static {v2, v0, v3}, Lcom/peel/content/library/LiveLibrary;->a(Lcom/peel/content/library/LiveLibrary;Ljava/util/List;Lcom/peel/util/t;)V

    goto/16 :goto_7

    .line 507
    :cond_12
    const-string/jumbo v3, "channel"

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_c

    .line 508
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/peel/content/library/o;->j:Ljava/lang/Object;

    move-object/from16 v18, v2

    check-cast v18, Ljava/util/List;

    .line 510
    invoke-static {}, Ljava/util/GregorianCalendar;->getInstance()Ljava/util/Calendar;

    move-result-object v3

    .line 511
    const/16 v2, 0xb

    const/4 v4, 0x0

    invoke-virtual {v3, v2, v4}, Ljava/util/Calendar;->set(II)V

    .line 513
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 514
    invoke-interface/range {v18 .. v18}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_c
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_13

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/peel/content/listing/Listing;

    .line 515
    invoke-virtual {v2}, Lcom/peel/content/listing/Listing;->f()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v4, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_c

    .line 517
    :cond_13
    monitor-enter p0

    .line 518
    :try_start_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/peel/content/library/o;->c:Lcom/peel/content/library/LiveLibrary;

    invoke-static {v2}, Lcom/peel/content/library/LiveLibrary;->d(Lcom/peel/content/library/LiveLibrary;)Landroid/util/LongSparseArray;

    move-result-object v2

    invoke-virtual {v3}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v6

    invoke-virtual {v2, v6, v7, v4}, Landroid/util/LongSparseArray;->put(JLjava/lang/Object;)V

    .line 519
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 521
    new-instance v19, Ljava/util/ArrayList;

    invoke-direct/range {v19 .. v19}, Ljava/util/ArrayList;-><init>()V

    .line 522
    invoke-interface/range {v18 .. v18}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v20

    :goto_d
    invoke-interface/range {v20 .. v20}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_14

    invoke-interface/range {v20 .. v20}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/peel/content/listing/Listing;

    move-object/from16 v17, v2

    .line 523
    check-cast v17, Lcom/peel/content/listing/LiveListing;

    .line 524
    new-instance v2, Lcom/peel/data/l;

    invoke-virtual/range {v17 .. v17}, Lcom/peel/content/listing/LiveListing;->f()Ljava/lang/String;

    move-result-object v3

    .line 525
    invoke-virtual/range {v17 .. v17}, Lcom/peel/content/listing/LiveListing;->g()Ljava/lang/String;

    move-result-object v4

    .line 526
    invoke-virtual/range {v17 .. v17}, Lcom/peel/content/listing/LiveListing;->b()Ljava/lang/String;

    move-result-object v5

    .line 527
    invoke-virtual/range {v17 .. v17}, Lcom/peel/content/listing/LiveListing;->k()Ljava/lang/String;

    move-result-object v6

    invoke-virtual/range {v17 .. v17}, Lcom/peel/content/listing/LiveListing;->l()Ljava/lang/String;

    move-result-object v7

    .line 528
    invoke-virtual/range {v17 .. v17}, Lcom/peel/content/listing/LiveListing;->m()[Ljava/lang/String;

    move-result-object v8

    .line 529
    invoke-virtual/range {v17 .. v17}, Lcom/peel/content/listing/LiveListing;->i()I

    move-result v9

    .line 530
    invoke-virtual/range {v17 .. v17}, Lcom/peel/content/listing/LiveListing;->j()J

    move-result-wide v10

    .line 531
    invoke-virtual/range {v17 .. v17}, Lcom/peel/content/listing/LiveListing;->y()Ljava/lang/String;

    move-result-object v12

    .line 532
    invoke-virtual/range {v17 .. v17}, Lcom/peel/content/listing/LiveListing;->z()Ljava/lang/String;

    move-result-object v13

    .line 533
    invoke-virtual/range {v17 .. v17}, Lcom/peel/content/listing/LiveListing;->x()Ljava/lang/String;

    move-result-object v14

    .line 534
    invoke-virtual/range {v17 .. v17}, Lcom/peel/content/listing/LiveListing;->A()Ljava/lang/String;

    move-result-object v15

    .line 535
    invoke-virtual/range {v17 .. v17}, Lcom/peel/content/listing/LiveListing;->w()Ljava/lang/String;

    move-result-object v16

    .line 536
    invoke-virtual/range {v17 .. v17}, Lcom/peel/content/listing/LiveListing;->a()Landroid/os/Bundle;

    move-result-object v17

    invoke-direct/range {v2 .. v17}, Lcom/peel/data/l;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;IJLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 524
    move-object/from16 v0, v19

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_d

    .line 519
    :catchall_1
    move-exception v2

    :try_start_2
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v2

    .line 538
    :cond_14
    invoke-static {}, Lcom/peel/data/m;->a()Lcom/peel/data/m;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/peel/content/library/o;->c:Lcom/peel/content/library/LiveLibrary;

    iget-object v3, v3, Lcom/peel/content/library/LiveLibrary;->a:Ljava/lang/String;

    const/4 v4, 0x0

    move-object/from16 v0, v19

    invoke-virtual {v2, v3, v0, v4}, Lcom/peel/data/m;->a(Ljava/lang/String;Ljava/util/List;Z)V

    .line 540
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/peel/content/library/o;->c:Lcom/peel/content/library/LiveLibrary;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/peel/content/library/o;->a:Lcom/peel/util/t;

    move-object/from16 v0, v18

    invoke-static {v2, v0, v3}, Lcom/peel/content/library/LiveLibrary;->a(Lcom/peel/content/library/LiveLibrary;Ljava/util/List;Lcom/peel/util/t;)V

    goto/16 :goto_7

    :cond_15
    move-wide v6, v4

    goto/16 :goto_4
.end method
