.class Lcom/peel/content/library/n;
.super Lcom/peel/util/t;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/peel/util/t",
        "<[",
        "Lcom/peel/data/l;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/peel/util/t;

.field final synthetic b:[Lcom/peel/content/listing/Listing;

.field final synthetic c:Ljava/util/Map;

.field final synthetic d:I

.field final synthetic e:Lcom/peel/content/library/LiveLibrary;


# direct methods
.method constructor <init>(Lcom/peel/content/library/LiveLibrary;ILcom/peel/util/t;[Lcom/peel/content/listing/Listing;Ljava/util/Map;I)V
    .locals 0

    .prologue
    .line 291
    iput-object p1, p0, Lcom/peel/content/library/n;->e:Lcom/peel/content/library/LiveLibrary;

    iput-object p3, p0, Lcom/peel/content/library/n;->a:Lcom/peel/util/t;

    iput-object p4, p0, Lcom/peel/content/library/n;->b:[Lcom/peel/content/listing/Listing;

    iput-object p5, p0, Lcom/peel/content/library/n;->c:Ljava/util/Map;

    iput p6, p0, Lcom/peel/content/library/n;->d:I

    invoke-direct {p0, p2}, Lcom/peel/util/t;-><init>(I)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 22

    .prologue
    .line 293
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/peel/content/library/n;->i:Z

    if-nez v2, :cond_0

    .line 294
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/peel/content/library/n;->a:Lcom/peel/util/t;

    const/4 v3, 0x0

    new-instance v4, Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/peel/content/library/n;->b:[Lcom/peel/content/listing/Listing;

    .line 295
    invoke-static {v5}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/peel/content/library/n;->k:Ljava/lang/String;

    .line 294
    invoke-virtual {v2, v3, v4, v5}, Lcom/peel/util/t;->a(ZLjava/lang/Object;Ljava/lang/String;)V

    .line 316
    :goto_0
    return-void

    .line 298
    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/peel/content/library/n;->j:Ljava/lang/Object;

    move-object/from16 v18, v2

    check-cast v18, [Lcom/peel/data/l;

    move-object/from16 v0, v18

    array-length v0, v0

    move/from16 v20, v0

    const/4 v2, 0x0

    move/from16 v19, v2

    :goto_1
    move/from16 v0, v19

    move/from16 v1, v20

    if-ge v0, v1, :cond_1

    aget-object v21, v18, v19

    .line 299
    new-instance v2, Lcom/peel/content/listing/LiveListing;

    invoke-virtual/range {v21 .. v21}, Lcom/peel/data/l;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual/range {v21 .. v21}, Lcom/peel/data/l;->b()Ljava/lang/String;

    move-result-object v4

    .line 300
    invoke-virtual/range {v21 .. v21}, Lcom/peel/data/l;->i()Ljava/lang/String;

    move-result-object v5

    invoke-virtual/range {v21 .. v21}, Lcom/peel/data/l;->c()Ljava/lang/String;

    move-result-object v6

    invoke-virtual/range {v21 .. v21}, Lcom/peel/data/l;->d()Ljava/lang/String;

    move-result-object v7

    .line 301
    invoke-virtual/range {v21 .. v21}, Lcom/peel/data/l;->e()[Ljava/lang/String;

    move-result-object v8

    invoke-virtual/range {v21 .. v21}, Lcom/peel/data/l;->f()I

    move-result v9

    invoke-virtual/range {v21 .. v21}, Lcom/peel/data/l;->g()J

    move-result-wide v10

    .line 302
    invoke-virtual/range {v21 .. v21}, Lcom/peel/data/l;->m()Ljava/lang/String;

    move-result-object v12

    invoke-virtual/range {v21 .. v21}, Lcom/peel/data/l;->j()Ljava/lang/String;

    move-result-object v13

    .line 303
    invoke-virtual/range {v21 .. v21}, Lcom/peel/data/l;->k()Ljava/lang/String;

    move-result-object v14

    invoke-virtual/range {v21 .. v21}, Lcom/peel/data/l;->l()Ljava/lang/String;

    move-result-object v15

    .line 304
    invoke-virtual/range {v21 .. v21}, Lcom/peel/data/l;->n()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v21 .. v21}, Lcom/peel/data/l;->h()Landroid/os/Bundle;

    move-result-object v17

    invoke-direct/range {v2 .. v17}, Lcom/peel/content/listing/LiveListing;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;IJLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 305
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/peel/content/library/n;->b:[Lcom/peel/content/listing/Listing;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/peel/content/library/n;->c:Ljava/util/Map;

    invoke-virtual/range {v21 .. v21}, Lcom/peel/data/l;->a()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v3, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    aput-object v2, v4, v3

    .line 306
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/peel/content/library/n;->e:Lcom/peel/content/library/LiveLibrary;

    invoke-static {v3}, Lcom/peel/content/library/LiveLibrary;->a(Lcom/peel/content/library/LiveLibrary;)Landroid/util/LruCache;

    move-result-object v3

    invoke-virtual {v2}, Lcom/peel/content/listing/LiveListing;->f()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4, v2}, Landroid/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 298
    add-int/lit8 v2, v19, 0x1

    move/from16 v19, v2

    goto :goto_1

    .line 309
    :cond_1
    new-instance v2, Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/peel/content/library/n;->b:[Lcom/peel/content/listing/Listing;

    .line 310
    invoke-static {v3}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 311
    const/4 v3, -0x1

    move-object/from16 v0, p0

    iget v4, v0, Lcom/peel/content/library/n;->d:I

    if-ne v3, v4, :cond_2

    .line 312
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/peel/content/library/n;->a:Lcom/peel/util/t;

    const/4 v4, 0x1

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v2, v5}, Lcom/peel/util/t;->a(ZLjava/lang/Object;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 314
    :cond_2
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/peel/content/library/n;->e:Lcom/peel/content/library/LiveLibrary;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/peel/content/library/n;->a:Lcom/peel/util/t;

    invoke-static {v3, v2, v4}, Lcom/peel/content/library/LiveLibrary;->a(Lcom/peel/content/library/LiveLibrary;Ljava/util/List;Lcom/peel/util/t;)V

    goto/16 :goto_0
.end method
