.class Lcom/peel/content/library/u;
.super Lcom/peel/util/t;


# instance fields
.field final synthetic a:Ljava/util/List;

.field final synthetic b:Landroid/os/Bundle;

.field final synthetic c:Lcom/peel/util/t;

.field final synthetic d:Lcom/peel/content/library/LiveLibrary;


# direct methods
.method constructor <init>(Lcom/peel/content/library/LiveLibrary;ILjava/util/List;Landroid/os/Bundle;Lcom/peel/util/t;)V
    .locals 0

    .prologue
    .line 818
    iput-object p1, p0, Lcom/peel/content/library/u;->d:Lcom/peel/content/library/LiveLibrary;

    iput-object p3, p0, Lcom/peel/content/library/u;->a:Ljava/util/List;

    iput-object p4, p0, Lcom/peel/content/library/u;->b:Landroid/os/Bundle;

    iput-object p5, p0, Lcom/peel/content/library/u;->c:Lcom/peel/util/t;

    invoke-direct {p0, p2}, Lcom/peel/util/t;-><init>(I)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 10

    .prologue
    .line 820
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 821
    iget-object v0, p0, Lcom/peel/content/library/u;->j:Ljava/lang/Object;

    if-eqz v0, :cond_5

    .line 822
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 823
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v6

    .line 825
    iget-object v0, p0, Lcom/peel/content/library/u;->j:Ljava/lang/Object;

    check-cast v0, [Lcom/peel/content/listing/Listing;

    check-cast v0, [Lcom/peel/content/listing/Listing;

    array-length v5, v0

    const/4 v1, 0x0

    move v2, v1

    :goto_0
    if-ge v2, v5, :cond_3

    aget-object v1, v0, v2

    .line 826
    if-eqz v1, :cond_0

    instance-of v8, v1, Lcom/peel/content/listing/LiveListing;

    if-nez v8, :cond_1

    .line 825
    :cond_0
    :goto_1
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    .line 828
    :cond_1
    check-cast v1, Lcom/peel/content/listing/LiveListing;

    .line 829
    invoke-virtual {v1}, Lcom/peel/content/listing/LiveListing;->o()J

    move-result-wide v8

    cmp-long v8, v8, v6

    if-ltz v8, :cond_2

    .line 830
    invoke-interface {v3, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 831
    iget-object v8, p0, Lcom/peel/content/library/u;->d:Lcom/peel/content/library/LiveLibrary;

    invoke-static {v8}, Lcom/peel/content/library/LiveLibrary;->a(Lcom/peel/content/library/LiveLibrary;)Landroid/util/LruCache;

    move-result-object v8

    invoke-virtual {v1}, Lcom/peel/content/listing/LiveListing;->f()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9, v1}, Landroid/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 834
    :cond_2
    invoke-virtual {v1}, Lcom/peel/content/listing/LiveListing;->f()Ljava/lang/String;

    move-result-object v8

    invoke-interface {v4, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 835
    iget-object v8, p0, Lcom/peel/content/library/u;->a:Ljava/util/List;

    invoke-virtual {v1}, Lcom/peel/content/listing/LiveListing;->f()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v8, v1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    goto :goto_1

    .line 838
    :cond_3
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_4

    .line 839
    invoke-static {}, Lcom/peel/data/m;->a()Lcom/peel/data/m;

    move-result-object v1

    iget-object v0, p0, Lcom/peel/content/library/u;->d:Lcom/peel/content/library/LiveLibrary;

    iget-object v2, v0, Lcom/peel/content/library/LiveLibrary;->a:Ljava/lang/String;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/String;

    invoke-interface {v4, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Lcom/peel/data/m;->a(Ljava/lang/String;[Ljava/lang/String;)V

    .line 842
    :cond_4
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_5

    .line 843
    iget-object v0, p0, Lcom/peel/content/library/u;->d:Lcom/peel/content/library/LiveLibrary;

    invoke-static {v0}, Lcom/peel/content/library/LiveLibrary;->e(Lcom/peel/content/library/LiveLibrary;)Ljava/util/Map;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/content/library/u;->b:Landroid/os/Bundle;

    const-string/jumbo v2, "id"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 849
    :cond_5
    iget-object v0, p0, Lcom/peel/content/library/u;->b:Landroid/os/Bundle;

    const-string/jumbo v1, "library"

    iget-object v2, p0, Lcom/peel/content/library/u;->d:Lcom/peel/content/library/LiveLibrary;

    iget-object v2, v2, Lcom/peel/content/library/LiveLibrary;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 850
    iget-object v0, p0, Lcom/peel/content/library/u;->b:Landroid/os/Bundle;

    const-string/jumbo v1, "user"

    sget-object v2, Lcom/peel/content/a;->f:Lcom/peel/content/user/User;

    invoke-virtual {v2}, Lcom/peel/content/user/User;->x()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 851
    iget-object v0, p0, Lcom/peel/content/library/u;->b:Landroid/os/Bundle;

    new-instance v1, Lcom/peel/content/library/v;

    const/4 v2, 0x2

    invoke-direct {v1, p0, v2}, Lcom/peel/content/library/v;-><init>(Lcom/peel/content/library/u;I)V

    invoke-static {v0, v1}, Lcom/peel/content/a/j;->a(Landroid/os/Bundle;Lcom/peel/util/t;)V

    .line 871
    return-void
.end method
