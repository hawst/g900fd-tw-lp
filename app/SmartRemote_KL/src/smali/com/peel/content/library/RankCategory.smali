.class public Lcom/peel/content/library/RankCategory;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/os/Parcelable;
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable;",
        "Ljava/lang/Comparable",
        "<",
        "Lcom/peel/content/library/RankCategory;",
        ">;"
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/peel/content/library/RankCategory;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/peel/content/listing/Listing;",
            ">;"
        }
    .end annotation
.end field

.field private final category:Ljava/lang/String;

.field private final rank:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 14
    new-instance v0, Lcom/peel/content/library/y;

    invoke-direct {v0}, Lcom/peel/content/library/y;-><init>()V

    sput-object v0, Lcom/peel/content/library/RankCategory;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(ILjava/lang/String;Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/peel/content/listing/Listing;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput p1, p0, Lcom/peel/content/library/RankCategory;->rank:I

    .line 25
    iput-object p2, p0, Lcom/peel/content/library/RankCategory;->category:Ljava/lang/String;

    .line 26
    iput-object p3, p0, Lcom/peel/content/library/RankCategory;->a:Ljava/util/List;

    .line 27
    return-void
.end method

.method static synthetic a(Landroid/os/Parcel;)Lcom/peel/content/library/RankCategory;
    .locals 1

    .prologue
    .line 13
    invoke-static {p0}, Lcom/peel/content/library/RankCategory;->b(Landroid/os/Parcel;)Lcom/peel/content/library/RankCategory;

    move-result-object v0

    return-object v0
.end method

.method private static b(Landroid/os/Parcel;)Lcom/peel/content/library/RankCategory;
    .locals 4

    .prologue
    .line 30
    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 31
    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 32
    const-class v2, Lcom/peel/content/listing/Listing;

    invoke-virtual {v2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v2

    invoke-virtual {p0, v2}, Landroid/os/Parcel;->readArrayList(Ljava/lang/ClassLoader;)Ljava/util/ArrayList;

    move-result-object v2

    .line 33
    new-instance v3, Lcom/peel/content/library/RankCategory;

    invoke-direct {v3, v0, v1, v2}, Lcom/peel/content/library/RankCategory;-><init>(ILjava/lang/String;Ljava/util/List;)V

    return-object v3
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 37
    iget v0, p0, Lcom/peel/content/library/RankCategory;->rank:I

    return v0
.end method

.method public a(Lcom/peel/content/library/RankCategory;)I
    .locals 2

    .prologue
    .line 54
    iget v0, p0, Lcom/peel/content/library/RankCategory;->rank:I

    invoke-virtual {p1}, Lcom/peel/content/library/RankCategory;->a()I

    move-result v1

    sub-int/2addr v0, v1

    return v0
.end method

.method public a(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/peel/content/listing/Listing;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 49
    iput-object p1, p0, Lcom/peel/content/library/RankCategory;->a:Ljava/util/List;

    .line 50
    return-void
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/peel/content/library/RankCategory;->category:Ljava/lang/String;

    return-object v0
.end method

.method public c()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/peel/content/listing/Listing;",
            ">;"
        }
    .end annotation

    .prologue
    .line 45
    iget-object v0, p0, Lcom/peel/content/library/RankCategory;->a:Ljava/util/List;

    return-object v0
.end method

.method public synthetic compareTo(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 13
    check-cast p1, Lcom/peel/content/library/RankCategory;

    invoke-virtual {p0, p1}, Lcom/peel/content/library/RankCategory;->a(Lcom/peel/content/library/RankCategory;)I

    move-result v0

    return v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 59
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 64
    iget v0, p0, Lcom/peel/content/library/RankCategory;->rank:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 65
    iget-object v0, p0, Lcom/peel/content/library/RankCategory;->category:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 66
    iget-object v0, p0, Lcom/peel/content/library/RankCategory;->a:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 67
    return-void
.end method
