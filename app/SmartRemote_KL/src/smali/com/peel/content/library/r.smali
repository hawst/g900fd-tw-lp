.class Lcom/peel/content/library/r;
.super Lcom/peel/util/t;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/peel/util/t",
        "<",
        "Ljava/util/List",
        "<",
        "Lcom/peel/content/listing/Listing;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/peel/content/library/q;


# direct methods
.method constructor <init>(Lcom/peel/content/library/q;)V
    .locals 0

    .prologue
    .line 459
    iput-object p1, p0, Lcom/peel/content/library/r;->a:Lcom/peel/content/library/q;

    invoke-direct {p0}, Lcom/peel/util/t;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    const/16 v5, 0x14

    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 463
    :try_start_0
    iget-boolean v0, p0, Lcom/peel/content/library/r;->i:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/peel/content/library/r;->j:Ljava/lang/Object;

    if-nez v0, :cond_3

    :cond_0
    iget-object v0, p0, Lcom/peel/content/library/r;->a:Lcom/peel/content/library/q;

    iget-object v0, v0, Lcom/peel/content/library/q;->b:Ljava/util/List;

    .line 468
    :goto_0
    iget-boolean v2, p0, Lcom/peel/content/library/r;->i:Z

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/peel/content/library/r;->j:Ljava/lang/Object;

    if-eqz v2, :cond_1

    .line 469
    iget-object v2, p0, Lcom/peel/content/library/r;->a:Lcom/peel/content/library/q;

    iget-object v2, v2, Lcom/peel/content/library/q;->a:Lcom/peel/content/library/RankCategory;

    invoke-virtual {v2, v0}, Lcom/peel/content/library/RankCategory;->a(Ljava/util/List;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 474
    :cond_1
    iget-object v0, p0, Lcom/peel/content/library/r;->a:Lcom/peel/content/library/q;

    iget-object v0, v0, Lcom/peel/content/library/q;->c:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->decrementAndGet()I

    move-result v0

    if-nez v0, :cond_2

    .line 475
    iget-object v0, p0, Lcom/peel/content/library/r;->a:Lcom/peel/content/library/q;

    iget-object v0, v0, Lcom/peel/content/library/q;->d:Ljava/util/ArrayList;

    invoke-static {v0}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 476
    iget-object v0, p0, Lcom/peel/content/library/r;->a:Lcom/peel/content/library/q;

    iget-object v0, v0, Lcom/peel/content/library/q;->e:Lcom/peel/content/library/o;

    iget-object v0, v0, Lcom/peel/content/library/o;->a:Lcom/peel/util/t;

    iget-object v2, p0, Lcom/peel/content/library/r;->a:Lcom/peel/content/library/q;

    iget-object v2, v2, Lcom/peel/content/library/q;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, v4, v2, v1}, Lcom/peel/util/t;->a(ZLjava/lang/Object;Ljava/lang/String;)V

    .line 478
    sget-object v2, Lcom/peel/content/a;->b:Lcom/peel/content/i;

    iget-object v0, p0, Lcom/peel/content/library/r;->a:Lcom/peel/content/library/q;

    iget-object v0, v0, Lcom/peel/content/library/q;->e:Lcom/peel/content/library/o;

    iget-object v0, v0, Lcom/peel/content/library/o;->c:Lcom/peel/content/library/LiveLibrary;

    iget-object v3, v0, Lcom/peel/content/library/LiveLibrary;->a:Ljava/lang/String;

    move-object v0, v1

    check-cast v0, [Ljava/lang/Object;

    invoke-virtual {v2, v5, v3, v0}, Lcom/peel/content/i;->a(ILjava/lang/Object;[Ljava/lang/Object;)V

    .line 481
    :cond_2
    :goto_1
    return-void

    .line 463
    :cond_3
    :try_start_1
    iget-object v0, p0, Lcom/peel/content/library/r;->j:Ljava/lang/Object;

    check-cast v0, Ljava/util/List;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 471
    :catch_0
    move-exception v0

    .line 472
    :try_start_2
    invoke-static {}, Lcom/peel/content/library/LiveLibrary;->p()Ljava/lang/String;

    move-result-object v2

    invoke-static {}, Lcom/peel/content/library/LiveLibrary;->p()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 474
    iget-object v0, p0, Lcom/peel/content/library/r;->a:Lcom/peel/content/library/q;

    iget-object v0, v0, Lcom/peel/content/library/q;->c:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->decrementAndGet()I

    move-result v0

    if-nez v0, :cond_2

    .line 475
    iget-object v0, p0, Lcom/peel/content/library/r;->a:Lcom/peel/content/library/q;

    iget-object v0, v0, Lcom/peel/content/library/q;->d:Ljava/util/ArrayList;

    invoke-static {v0}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 476
    iget-object v0, p0, Lcom/peel/content/library/r;->a:Lcom/peel/content/library/q;

    iget-object v0, v0, Lcom/peel/content/library/q;->e:Lcom/peel/content/library/o;

    iget-object v0, v0, Lcom/peel/content/library/o;->a:Lcom/peel/util/t;

    iget-object v2, p0, Lcom/peel/content/library/r;->a:Lcom/peel/content/library/q;

    iget-object v2, v2, Lcom/peel/content/library/q;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, v4, v2, v1}, Lcom/peel/util/t;->a(ZLjava/lang/Object;Ljava/lang/String;)V

    .line 478
    sget-object v0, Lcom/peel/content/a;->b:Lcom/peel/content/i;

    iget-object v2, p0, Lcom/peel/content/library/r;->a:Lcom/peel/content/library/q;

    iget-object v2, v2, Lcom/peel/content/library/q;->e:Lcom/peel/content/library/o;

    iget-object v2, v2, Lcom/peel/content/library/o;->c:Lcom/peel/content/library/LiveLibrary;

    iget-object v2, v2, Lcom/peel/content/library/LiveLibrary;->a:Ljava/lang/String;

    check-cast v1, [Ljava/lang/Object;

    invoke-virtual {v0, v5, v2, v1}, Lcom/peel/content/i;->a(ILjava/lang/Object;[Ljava/lang/Object;)V

    goto :goto_1

    .line 474
    :catchall_0
    move-exception v0

    iget-object v2, p0, Lcom/peel/content/library/r;->a:Lcom/peel/content/library/q;

    iget-object v2, v2, Lcom/peel/content/library/q;->c:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicInteger;->decrementAndGet()I

    move-result v2

    if-nez v2, :cond_4

    .line 475
    iget-object v2, p0, Lcom/peel/content/library/r;->a:Lcom/peel/content/library/q;

    iget-object v2, v2, Lcom/peel/content/library/q;->d:Ljava/util/ArrayList;

    invoke-static {v2}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 476
    iget-object v2, p0, Lcom/peel/content/library/r;->a:Lcom/peel/content/library/q;

    iget-object v2, v2, Lcom/peel/content/library/q;->e:Lcom/peel/content/library/o;

    iget-object v2, v2, Lcom/peel/content/library/o;->a:Lcom/peel/util/t;

    iget-object v3, p0, Lcom/peel/content/library/r;->a:Lcom/peel/content/library/q;

    iget-object v3, v3, Lcom/peel/content/library/q;->d:Ljava/util/ArrayList;

    invoke-virtual {v2, v4, v3, v1}, Lcom/peel/util/t;->a(ZLjava/lang/Object;Ljava/lang/String;)V

    .line 478
    sget-object v2, Lcom/peel/content/a;->b:Lcom/peel/content/i;

    iget-object v3, p0, Lcom/peel/content/library/r;->a:Lcom/peel/content/library/q;

    iget-object v3, v3, Lcom/peel/content/library/q;->e:Lcom/peel/content/library/o;

    iget-object v3, v3, Lcom/peel/content/library/o;->c:Lcom/peel/content/library/LiveLibrary;

    iget-object v3, v3, Lcom/peel/content/library/LiveLibrary;->a:Ljava/lang/String;

    check-cast v1, [Ljava/lang/Object;

    invoke-virtual {v2, v5, v3, v1}, Lcom/peel/content/i;->a(ILjava/lang/Object;[Ljava/lang/Object;)V

    :cond_4
    throw v0
.end method
