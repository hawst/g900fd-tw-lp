.class public abstract Lcom/peel/content/library/Library;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/peel/content/library/Library;",
            ">;"
        }
    .end annotation
.end field

.field private static final c:Ljava/lang/String;


# instance fields
.field protected final a:Ljava/lang/String;

.field protected final b:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 16
    new-instance v0, Lcom/peel/content/library/d;

    invoke-direct {v0}, Lcom/peel/content/library/d;-><init>()V

    sput-object v0, Lcom/peel/content/library/Library;->CREATOR:Landroid/os/Parcelable$Creator;

    .line 26
    const-class v0, Lcom/peel/content/library/Library;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/peel/content/library/Library;->c:Ljava/lang/String;

    return-void
.end method

.method protected constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iput-object p1, p0, Lcom/peel/content/library/Library;->a:Ljava/lang/String;

    .line 32
    iput-object p2, p0, Lcom/peel/content/library/Library;->b:Ljava/lang/String;

    .line 33
    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Lcom/peel/content/library/Library;
    .locals 3

    .prologue
    .line 36
    const-string/jumbo v0, "live"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 37
    new-instance v0, Lcom/peel/content/library/LiveLibrary;

    invoke-direct {v0, p1, p2}, Lcom/peel/content/library/LiveLibrary;-><init>(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 44
    :goto_0
    return-object v0

    .line 38
    :cond_0
    const-string/jumbo v0, "dtv"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 39
    const-string/jumbo v0, "directvDeviceId"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 40
    new-instance v0, Lcom/peel/content/library/DirecTVLibrary;

    invoke-direct {v0, p1, v1}, Lcom/peel/content/library/DirecTVLibrary;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 42
    :cond_1
    sget-object v0, Lcom/peel/content/library/Library;->c:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "(createLibrary) unrecognized library type: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 44
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public abstract a()Landroid/os/Bundle;
.end method

.method public a(IJ)V
    .locals 0

    .prologue
    .line 71
    return-void
.end method

.method public abstract a(Landroid/os/Bundle;Lcom/peel/util/t;)V
.end method

.method public abstract a(Landroid/os/Bundle;Ljava/util/List;Lcom/peel/util/t;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/Bundle;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/peel/util/t",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/peel/content/listing/Listing;",
            ">;>;)V"
        }
    .end annotation
.end method

.method public a(Ljava/lang/String;Lcom/peel/util/t;)V
    .locals 0

    .prologue
    .line 76
    return-void
.end method

.method public b()J
    .locals 2

    .prologue
    .line 57
    const-wide/16 v0, -0x1

    return-wide v0
.end method

.method public abstract b(Landroid/os/Bundle;Lcom/peel/util/t;)V
.end method

.method public c()V
    .locals 0

    .prologue
    .line 59
    return-void
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 79
    const v0, 0x16f32

    return v0
.end method

.method public e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/peel/content/library/Library;->a:Ljava/lang/String;

    return-object v0
.end method

.method public f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/peel/content/library/Library;->b:Ljava/lang/String;

    return-object v0
.end method

.method public g()V
    .locals 0

    .prologue
    .line 61
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lcom/peel/content/library/Library;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 84
    iget-object v0, p0, Lcom/peel/content/library/Library;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 85
    invoke-virtual {p0}, Lcom/peel/content/library/Library;->a()Landroid/os/Bundle;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeBundle(Landroid/os/Bundle;)V

    .line 86
    return-void
.end method
