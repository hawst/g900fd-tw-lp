.class Lcom/peel/content/library/t;
.super Lcom/peel/util/t;


# instance fields
.field final synthetic a:Ljava/util/List;

.field final synthetic b:Lcom/peel/data/RankCategory;

.field final synthetic c:Ljava/util/concurrent/atomic/AtomicInteger;

.field final synthetic d:Lcom/peel/util/t;

.field final synthetic e:Lcom/peel/content/library/LiveLibrary;


# direct methods
.method constructor <init>(Lcom/peel/content/library/LiveLibrary;Ljava/util/List;Lcom/peel/data/RankCategory;Ljava/util/concurrent/atomic/AtomicInteger;Lcom/peel/util/t;)V
    .locals 0

    .prologue
    .line 762
    iput-object p1, p0, Lcom/peel/content/library/t;->e:Lcom/peel/content/library/LiveLibrary;

    iput-object p2, p0, Lcom/peel/content/library/t;->a:Ljava/util/List;

    iput-object p3, p0, Lcom/peel/content/library/t;->b:Lcom/peel/data/RankCategory;

    iput-object p4, p0, Lcom/peel/content/library/t;->c:Ljava/util/concurrent/atomic/AtomicInteger;

    iput-object p5, p0, Lcom/peel/content/library/t;->d:Lcom/peel/util/t;

    invoke-direct {p0}, Lcom/peel/util/t;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 766
    :try_start_0
    iget-boolean v0, p0, Lcom/peel/content/library/t;->i:Z

    if-eqz v0, :cond_2

    .line 767
    iget-object v0, p0, Lcom/peel/content/library/t;->j:Ljava/lang/Object;

    check-cast v0, Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/content/listing/Listing;

    .line 768
    iget-object v2, p0, Lcom/peel/content/library/t;->e:Lcom/peel/content/library/LiveLibrary;

    invoke-static {v2}, Lcom/peel/content/library/LiveLibrary;->a(Lcom/peel/content/library/LiveLibrary;)Landroid/util/LruCache;

    move-result-object v2

    invoke-virtual {v0}, Lcom/peel/content/listing/Listing;->f()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3, v0}, Landroid/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 775
    :catch_0
    move-exception v0

    .line 776
    :try_start_1
    invoke-static {}, Lcom/peel/content/library/LiveLibrary;->p()Ljava/lang/String;

    move-result-object v1

    invoke-static {}, Lcom/peel/content/library/LiveLibrary;->p()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 778
    iget-object v0, p0, Lcom/peel/content/library/t;->c:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->decrementAndGet()I

    move-result v0

    if-nez v0, :cond_0

    .line 779
    iget-object v0, p0, Lcom/peel/content/library/t;->a:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 780
    iget-object v0, p0, Lcom/peel/content/library/t;->d:Lcom/peel/util/t;

    iget-object v1, p0, Lcom/peel/content/library/t;->a:Ljava/util/List;

    invoke-virtual {v0, v5, v1, v6}, Lcom/peel/util/t;->a(ZLjava/lang/Object;Ljava/lang/String;)V

    .line 783
    :cond_0
    :goto_1
    return-void

    .line 771
    :cond_1
    :try_start_2
    iget-object v1, p0, Lcom/peel/content/library/t;->a:Ljava/util/List;

    new-instance v2, Lcom/peel/content/library/RankCategory;

    iget-object v0, p0, Lcom/peel/content/library/t;->b:Lcom/peel/data/RankCategory;

    invoke-virtual {v0}, Lcom/peel/data/RankCategory;->b()Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v3

    iget-object v0, p0, Lcom/peel/content/library/t;->b:Lcom/peel/data/RankCategory;

    .line 772
    invoke-virtual {v0}, Lcom/peel/data/RankCategory;->a()Ljava/lang/String;

    move-result-object v4

    iget-object v0, p0, Lcom/peel/content/library/t;->j:Ljava/lang/Object;

    check-cast v0, Ljava/util/List;

    invoke-direct {v2, v3, v4, v0}, Lcom/peel/content/library/RankCategory;-><init>(ILjava/lang/String;Ljava/util/List;)V

    .line 771
    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 778
    :cond_2
    iget-object v0, p0, Lcom/peel/content/library/t;->c:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->decrementAndGet()I

    move-result v0

    if-nez v0, :cond_0

    .line 779
    iget-object v0, p0, Lcom/peel/content/library/t;->a:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 780
    iget-object v0, p0, Lcom/peel/content/library/t;->d:Lcom/peel/util/t;

    iget-object v1, p0, Lcom/peel/content/library/t;->a:Ljava/util/List;

    invoke-virtual {v0, v5, v1, v6}, Lcom/peel/util/t;->a(ZLjava/lang/Object;Ljava/lang/String;)V

    goto :goto_1

    .line 778
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/peel/content/library/t;->c:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->decrementAndGet()I

    move-result v1

    if-nez v1, :cond_3

    .line 779
    iget-object v1, p0, Lcom/peel/content/library/t;->a:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 780
    iget-object v1, p0, Lcom/peel/content/library/t;->d:Lcom/peel/util/t;

    iget-object v2, p0, Lcom/peel/content/library/t;->a:Ljava/util/List;

    invoke-virtual {v1, v5, v2, v6}, Lcom/peel/util/t;->a(ZLjava/lang/Object;Ljava/lang/String;)V

    :cond_3
    throw v0
.end method
