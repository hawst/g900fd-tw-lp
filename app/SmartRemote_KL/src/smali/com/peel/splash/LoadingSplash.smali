.class public Lcom/peel/splash/LoadingSplash;
.super Landroid/app/Activity;


# instance fields
.field private a:Z

.field private final b:Lcom/peel/util/s;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 18
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 21
    new-instance v0, Lcom/peel/splash/a;

    invoke-direct {v0, p0}, Lcom/peel/splash/a;-><init>(Lcom/peel/splash/LoadingSplash;)V

    iput-object v0, p0, Lcom/peel/splash/LoadingSplash;->b:Lcom/peel/util/s;

    return-void
.end method


# virtual methods
.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 36
    invoke-super {p0, p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 38
    iget v0, p1, Landroid/content/res/Configuration;->orientation:I

    .line 39
    if-ne v0, v2, :cond_0

    .line 45
    :goto_0
    return-void

    .line 40
    :cond_0
    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    .line 41
    invoke-virtual {p0, v2}, Lcom/peel/splash/LoadingSplash;->setRequestedOrientation(I)V

    goto :goto_0

    .line 43
    :cond_1
    invoke-virtual {p0, v2}, Lcom/peel/splash/LoadingSplash;->setRequestedOrientation(I)V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 61
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 64
    invoke-virtual {p0}, Lcom/peel/splash/LoadingSplash;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string/jumbo v1, "is_setup_complete"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/peel/content/a;->h:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 65
    :cond_0
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/peel/splash/LoadingSplash;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/peel/main/Home;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 66
    invoke-virtual {p0, v0}, Lcom/peel/splash/LoadingSplash;->startActivity(Landroid/content/Intent;)V

    .line 76
    :goto_0
    return-void

    .line 70
    :cond_1
    iget-boolean v0, p0, Lcom/peel/splash/LoadingSplash;->a:Z

    if-nez v0, :cond_2

    .line 71
    sget-object v0, Lcom/peel/control/am;->a:Lcom/peel/control/aq;

    iget-object v1, p0, Lcom/peel/splash/LoadingSplash;->b:Lcom/peel/util/s;

    invoke-virtual {v0, v1}, Lcom/peel/control/aq;->a(Ljava/lang/Object;)V

    .line 73
    :cond_2
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/peel/splash/LoadingSplash;->a:Z

    .line 75
    sget v0, Lcom/peel/ui/fq;->loading_splash:I

    invoke-virtual {p0, v0}, Lcom/peel/splash/LoadingSplash;->setContentView(I)V

    goto :goto_0
.end method

.method protected onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 49
    invoke-super {p0, p1}, Landroid/app/Activity;->onRestoreInstanceState(Landroid/os/Bundle;)V

    .line 50
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/peel/splash/LoadingSplash;->setRequestedOrientation(I)V

    .line 51
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 55
    invoke-super {p0, p1}, Landroid/app/Activity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 56
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/peel/splash/LoadingSplash;->setRequestedOrientation(I)V

    .line 57
    return-void
.end method

.method protected onStop()V
    .locals 2

    .prologue
    .line 80
    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    .line 81
    iget-boolean v0, p0, Lcom/peel/splash/LoadingSplash;->a:Z

    if-eqz v0, :cond_0

    .line 82
    sget-object v0, Lcom/peel/control/am;->a:Lcom/peel/control/aq;

    iget-object v1, p0, Lcom/peel/splash/LoadingSplash;->b:Lcom/peel/util/s;

    invoke-virtual {v0, v1}, Lcom/peel/control/aq;->b(Ljava/lang/Object;)V

    .line 83
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/peel/splash/LoadingSplash;->a:Z

    .line 85
    :cond_0
    return-void
.end method
