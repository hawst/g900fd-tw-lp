.class public Lcom/peel/control/e;
.super Lcom/peel/control/f;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 524
    invoke-direct {p0}, Lcom/peel/control/f;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/peel/control/a;I[Lcom/peel/control/h;)Z
    .locals 8

    .prologue
    const/4 v2, 0x0

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 540
    .line 543
    if-eqz p3, :cond_5

    invoke-static {p3}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    move-object v1, v0

    .line 544
    :goto_0
    iget-object v0, p1, Lcom/peel/control/a;->a:Lcom/peel/data/d;

    invoke-virtual {v0}, Lcom/peel/data/d;->c()Ljava/lang/Iterable;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    .line 545
    sget-object v3, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    const-string/jumbo v5, "id"

    invoke-interface {v0, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v3, v0}, Lcom/peel/control/am;->c(Ljava/lang/String;)Lcom/peel/control/h;

    move-result-object v3

    .line 546
    if-eqz v3, :cond_0

    if-eqz v1, :cond_1

    invoke-interface {v1, v3}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 548
    :cond_1
    if-eqz p2, :cond_9

    invoke-virtual {v3}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/data/g;->g()Z

    move-result v0

    if-nez v0, :cond_9

    .line 550
    invoke-virtual {v3}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v0

    const-string/jumbo v5, "PowerOff"

    invoke-virtual {v0, v5}, Lcom/peel/data/g;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 551
    const-string/jumbo v0, "PowerOff"

    invoke-virtual {v3, v0}, Lcom/peel/control/h;->c(Ljava/lang/String;)Z

    .line 558
    :cond_2
    :goto_2
    if-nez v2, :cond_8

    invoke-virtual {v3}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v0

    const-string/jumbo v5, "Delay"

    invoke-virtual {v0, v5}, Lcom/peel/data/g;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-virtual {v3}, Lcom/peel/control/h;->a()Z

    move-result v0

    if-eqz v0, :cond_8

    move-object v0, v3

    .line 560
    :goto_3
    if-eqz v0, :cond_3

    const-string/jumbo v2, "Delay"

    invoke-virtual {v0, v2}, Lcom/peel/control/h;->c(Ljava/lang/String;)Z

    .line 564
    :cond_3
    :goto_4
    invoke-virtual {v3}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v2

    invoke-virtual {v2}, Lcom/peel/data/g;->g()Z

    move-result v2

    if-nez v2, :cond_4

    invoke-virtual {v3, v6}, Lcom/peel/control/h;->a(I)V

    .line 566
    :cond_4
    invoke-virtual {v3}, Lcom/peel/control/h;->n()V

    move-object v2, v0

    .line 567
    goto :goto_1

    :cond_5
    move-object v1, v2

    .line 543
    goto :goto_0

    .line 552
    :cond_6
    const/4 v0, 0x2

    if-ne p2, v0, :cond_2

    invoke-virtual {v3}, Lcom/peel/control/h;->o()I

    move-result v0

    if-ne v7, v0, :cond_2

    .line 554
    invoke-virtual {v3}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v0

    const-string/jumbo v5, "Power"

    invoke-virtual {v0, v5}, Lcom/peel/data/g;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 555
    const-string/jumbo v0, "Power"

    invoke-virtual {v3, v0}, Lcom/peel/control/h;->c(Ljava/lang/String;)Z

    goto :goto_2

    .line 569
    :cond_7
    invoke-virtual {p1, v6}, Lcom/peel/control/a;->c(I)V

    .line 576
    return v7

    :cond_8
    move-object v0, v2

    goto :goto_3

    :cond_9
    move-object v0, v2

    goto :goto_4
.end method

.method public a(Lcom/peel/control/a;Ljava/lang/String;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 527
    sget-object v1, Lcom/peel/data/b;->a:Ljava/util/List;

    invoke-interface {v1, p2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 528
    invoke-static {p1}, Lcom/peel/control/a;->c(Lcom/peel/control/a;)Lcom/peel/control/h;

    move-result-object v1

    if-nez v1, :cond_0

    .line 535
    :goto_0
    return v0

    .line 528
    :cond_0
    invoke-static {p1}, Lcom/peel/control/a;->c(Lcom/peel/control/a;)Lcom/peel/control/h;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/peel/control/h;->c(Ljava/lang/String;)Z

    move-result v0

    goto :goto_0

    .line 529
    :cond_1
    invoke-static {p1}, Lcom/peel/control/a;->d(Lcom/peel/control/a;)Lcom/peel/control/h;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 530
    invoke-static {p1}, Lcom/peel/control/a;->d(Lcom/peel/control/a;)Lcom/peel/control/h;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/peel/control/h;->c(Ljava/lang/String;)Z

    move-result v0

    goto :goto_0

    .line 534
    :cond_2
    invoke-static {}, Lcom/peel/control/a;->h()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "control device is null for activity: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Lcom/peel/control/a;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " command: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public a(Lcom/peel/control/a;Ljava/net/URI;)Z
    .locals 1

    .prologue
    .line 581
    invoke-static {p1}, Lcom/peel/control/a;->d(Lcom/peel/control/a;)Lcom/peel/control/h;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 582
    invoke-static {p1}, Lcom/peel/control/a;->d(Lcom/peel/control/a;)Lcom/peel/control/h;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/peel/control/h;->a(Ljava/net/URI;)Z

    move-result v0

    .line 584
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
