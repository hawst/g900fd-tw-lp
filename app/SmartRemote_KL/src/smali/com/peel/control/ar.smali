.class public final Lcom/peel/control/ar;
.super Ljava/lang/Object;


# static fields
.field private static final a:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 442
    const-class v0, Lcom/peel/control/ar;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/peel/control/ar;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 441
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a()V
    .locals 11

    .prologue
    const/4 v10, 0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 445
    sget-object v0, Lcom/peel/control/ar;->a:Ljava/lang/String;

    const-string/jumbo v3, "\n*************Loader.load()"

    invoke-static {v0, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 447
    :try_start_0
    sget-object v3, Lcom/peel/control/am;->a:Lcom/peel/control/aq;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v0, 0x0

    check-cast v0, [Ljava/lang/Object;

    invoke-virtual {v3, v4, v5, v0}, Lcom/peel/control/aq;->a(ILjava/lang/Object;[Ljava/lang/Object;)V

    .line 448
    sget-object v0, Lcom/peel/control/ar;->a:Ljava/lang/String;

    const-string/jumbo v3, "load +"

    invoke-static {v0, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 449
    invoke-static {}, Lcom/peel/data/m;->a()Lcom/peel/data/m;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/data/m;->d()[Lcom/peel/data/at;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v5

    .line 450
    if-nez v5, :cond_0

    .line 484
    sget-object v0, Lcom/peel/control/ar;->a:Ljava/lang/String;

    const-string/jumbo v2, "load -"

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 485
    sget-object v2, Lcom/peel/control/am;->a:Lcom/peel/control/aq;

    move-object v0, v1

    check-cast v0, [Ljava/lang/Object;

    invoke-virtual {v2, v10, v1, v0}, Lcom/peel/control/aq;->a(ILjava/lang/Object;[Ljava/lang/Object;)V

    .line 487
    :goto_0
    return-void

    .line 455
    :cond_0
    :try_start_1
    invoke-static {}, Lcom/peel/data/m;->a()Lcom/peel/data/m;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/data/m;->c()[Lcom/peel/data/h;

    move-result-object v6

    .line 456
    array-length v0, v6

    new-array v0, v0, [Lcom/peel/control/o;

    invoke-static {v0}, Lcom/peel/control/am;->a([Lcom/peel/control/o;)[Lcom/peel/control/o;

    .line 458
    array-length v7, v6

    move v0, v2

    move v3, v2

    :goto_1
    if-ge v0, v7, :cond_1

    aget-object v8, v6, v0

    .line 459
    invoke-static {}, Lcom/peel/control/am;->g()[Lcom/peel/control/o;

    move-result-object v9

    add-int/lit8 v4, v3, 0x1

    invoke-static {v8}, Lcom/peel/control/o;->a(Lcom/peel/data/h;)Lcom/peel/control/o;

    move-result-object v8

    aput-object v8, v9, v3

    .line 458
    add-int/lit8 v0, v0, 0x1

    move v3, v4

    goto :goto_1

    .line 462
    :cond_1
    invoke-static {}, Lcom/peel/data/m;->a()Lcom/peel/data/m;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/data/m;->f()[Lcom/peel/data/g;

    move-result-object v3

    .line 463
    if-eqz v3, :cond_2

    .line 464
    array-length v4, v3

    move v0, v2

    :goto_2
    if-ge v0, v4, :cond_2

    aget-object v6, v3, v0

    .line 465
    invoke-static {}, Lcom/peel/control/am;->h()Ljava/util/Map;

    move-result-object v7

    invoke-virtual {v6}, Lcom/peel/data/g;->b()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6}, Lcom/peel/control/h;->a(Lcom/peel/data/g;)Lcom/peel/control/h;

    move-result-object v6

    invoke-interface {v7, v8, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 464
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 469
    :cond_2
    invoke-static {}, Lcom/peel/data/m;->a()Lcom/peel/data/m;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/data/m;->e()[Lcom/peel/data/d;

    move-result-object v3

    .line 470
    if-eqz v3, :cond_3

    .line 471
    array-length v4, v3

    move v0, v2

    :goto_3
    if-ge v0, v4, :cond_3

    aget-object v6, v3, v0

    .line 472
    invoke-static {}, Lcom/peel/control/am;->i()Ljava/util/Map;

    move-result-object v7

    invoke-virtual {v6}, Lcom/peel/data/d;->b()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6}, Lcom/peel/control/a;->a(Lcom/peel/data/d;)Lcom/peel/control/a;

    move-result-object v6

    invoke-interface {v7, v8, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 471
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 476
    :cond_3
    array-length v0, v5

    new-array v0, v0, [Lcom/peel/control/RoomControl;

    invoke-static {v0}, Lcom/peel/control/am;->a([Lcom/peel/control/RoomControl;)[Lcom/peel/control/RoomControl;

    .line 478
    array-length v4, v5

    move v0, v2

    :goto_4
    if-ge v0, v4, :cond_4

    aget-object v6, v5, v0

    .line 479
    invoke-static {}, Lcom/peel/control/am;->j()[Lcom/peel/control/RoomControl;

    move-result-object v7

    add-int/lit8 v3, v2, 0x1

    invoke-static {v6}, Lcom/peel/control/RoomControl;->a(Lcom/peel/data/at;)Lcom/peel/control/RoomControl;

    move-result-object v6

    aput-object v6, v7, v2
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 478
    add-int/lit8 v0, v0, 0x1

    move v2, v3

    goto :goto_4

    .line 484
    :cond_4
    sget-object v0, Lcom/peel/control/ar;->a:Ljava/lang/String;

    const-string/jumbo v2, "load -"

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 485
    sget-object v2, Lcom/peel/control/am;->a:Lcom/peel/control/aq;

    move-object v0, v1

    check-cast v0, [Ljava/lang/Object;

    invoke-virtual {v2, v10, v1, v0}, Lcom/peel/control/aq;->a(ILjava/lang/Object;[Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 481
    :catch_0
    move-exception v0

    .line 482
    :try_start_2
    sget-object v2, Lcom/peel/control/ar;->a:Ljava/lang/String;

    sget-object v3, Lcom/peel/control/ar;->a:Ljava/lang/String;

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 484
    sget-object v0, Lcom/peel/control/ar;->a:Ljava/lang/String;

    const-string/jumbo v2, "load -"

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 485
    sget-object v2, Lcom/peel/control/am;->a:Lcom/peel/control/aq;

    move-object v0, v1

    check-cast v0, [Ljava/lang/Object;

    invoke-virtual {v2, v10, v1, v0}, Lcom/peel/control/aq;->a(ILjava/lang/Object;[Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 484
    :catchall_0
    move-exception v0

    move-object v2, v0

    sget-object v0, Lcom/peel/control/ar;->a:Ljava/lang/String;

    const-string/jumbo v3, "load -"

    invoke-static {v0, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 485
    sget-object v3, Lcom/peel/control/am;->a:Lcom/peel/control/aq;

    move-object v0, v1

    check-cast v0, [Ljava/lang/Object;

    invoke-virtual {v3, v10, v1, v0}, Lcom/peel/control/aq;->a(ILjava/lang/Object;[Ljava/lang/Object;)V

    throw v2
.end method
