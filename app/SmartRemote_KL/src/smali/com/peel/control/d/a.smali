.class public Lcom/peel/control/d/a;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/peel/control/d/c;


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private b:Lcom/htc/circontrol/CIRControl;

.field private c:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 19
    const-class v0, Lcom/peel/control/d/a;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/peel/control/d/a;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/peel/control/o;)V
    .locals 2

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    new-instance v0, Lcom/htc/circontrol/CIRControl;

    new-instance v1, Lcom/peel/control/d/b;

    invoke-direct {v1, p0, p2}, Lcom/peel/control/d/b;-><init>(Lcom/peel/control/d/a;Lcom/peel/control/o;)V

    invoke-direct {v0, p1, v1}, Lcom/htc/circontrol/CIRControl;-><init>(Landroid/content/Context;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/peel/control/d/a;->b:Lcom/htc/circontrol/CIRControl;

    .line 103
    return-void
.end method

.method static synthetic a(Lcom/peel/control/d/a;)I
    .locals 1

    .prologue
    .line 17
    iget v0, p0, Lcom/peel/control/d/a;->c:I

    return v0
.end method

.method static synthetic b(Lcom/peel/control/d/a;)Lcom/htc/circontrol/CIRControl;
    .locals 1

    .prologue
    .line 17
    iget-object v0, p0, Lcom/peel/control/d/a;->b:Lcom/htc/circontrol/CIRControl;

    return-object v0
.end method

.method static synthetic e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 17
    sget-object v0, Lcom/peel/control/d/a;->a:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public a(Ljava/lang/String;)J
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 108
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 111
    const/16 v1, 0x2c

    :try_start_0
    invoke-virtual {p1, v1}, Ljava/lang/String;->indexOf(I)I

    move-result v1

    .line 112
    const/4 v4, 0x0

    invoke-virtual {p1, v4, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    .line 113
    add-int/lit8 v1, v1, 0x1

    invoke-virtual {p1, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v5, ","

    invoke-virtual {v1, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 114
    array-length v5, v1

    new-array v5, v5, [I

    .line 115
    :goto_0
    array-length v6, v5

    if-ge v0, v6, :cond_0

    .line 116
    aget-object v6, v1, v0

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    aput v6, v5, v0

    .line 115
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 118
    :cond_0
    iget-object v0, p0, Lcom/peel/control/d/a;->b:Lcom/htc/circontrol/CIRControl;

    new-instance v1, Lcom/htc/htcircontrol/HtcIrData;

    const/4 v6, 0x1

    invoke-direct {v1, v6, v4, v5}, Lcom/htc/htcircontrol/HtcIrData;-><init>(II[I)V

    const/4 v4, 0x1

    invoke-virtual {v0, v1, v4}, Lcom/htc/circontrol/CIRControl;->transmitIRCmd(Lcom/htc/htcircontrol/HtcIrData;Z)Ljava/util/UUID;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 122
    :goto_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    sub-long/2addr v0, v2

    return-wide v0

    .line 119
    :catch_0
    move-exception v0

    .line 120
    sget-object v1, Lcom/peel/control/d/a;->a:Ljava/lang/String;

    sget-object v4, Lcom/peel/control/d/a;->a:Ljava/lang/String;

    invoke-static {v1, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method public a()V
    .locals 1

    .prologue
    .line 127
    iget-object v0, p0, Lcom/peel/control/d/a;->b:Lcom/htc/circontrol/CIRControl;

    invoke-virtual {v0}, Lcom/htc/circontrol/CIRControl;->start()V

    .line 128
    return-void
.end method

.method public a(I)Z
    .locals 2

    .prologue
    .line 135
    iget-object v0, p0, Lcom/peel/control/d/a;->b:Lcom/htc/circontrol/CIRControl;

    invoke-virtual {v0, p1}, Lcom/htc/circontrol/CIRControl;->learnIRCmd(I)Ljava/util/UUID;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 136
    iput p1, p0, Lcom/peel/control/d/a;->c:I

    .line 137
    sget-object v0, Lcom/peel/control/d/a;->a:Ljava/lang/String;

    const-string/jumbo v1, "Triggered IR learning"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 138
    const/4 v0, 0x1

    .line 141
    :goto_0
    return v0

    .line 140
    :cond_0
    sget-object v0, Lcom/peel/control/d/a;->a:Ljava/lang/String;

    const-string/jumbo v1, "Failed to trigger IR learning"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 141
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()V
    .locals 1

    .prologue
    .line 131
    iget-object v0, p0, Lcom/peel/control/d/a;->b:Lcom/htc/circontrol/CIRControl;

    invoke-virtual {v0}, Lcom/htc/circontrol/CIRControl;->stop()V

    .line 132
    return-void
.end method

.method public c()Z
    .locals 2

    .prologue
    .line 146
    iget-object v0, p0, Lcom/peel/control/d/a;->b:Lcom/htc/circontrol/CIRControl;

    invoke-virtual {v0}, Lcom/htc/circontrol/CIRControl;->cancelCommand()Ljava/util/UUID;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 147
    sget-object v0, Lcom/peel/control/d/a;->a:Ljava/lang/String;

    const-string/jumbo v1, "Cancelled IR learning"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 148
    const/4 v0, 0x1

    .line 151
    :goto_0
    return v0

    .line 150
    :cond_0
    sget-object v0, Lcom/peel/control/d/a;->a:Ljava/lang/String;

    const-string/jumbo v1, "Failed to cancel IR learning"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 151
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public d()Z
    .locals 1

    .prologue
    .line 157
    const/4 v0, 0x1

    return v0
.end method
