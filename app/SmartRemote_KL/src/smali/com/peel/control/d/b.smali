.class Lcom/peel/control/d/b;
.super Landroid/os/Handler;


# instance fields
.field final synthetic a:Lcom/peel/control/o;

.field final synthetic b:Lcom/peel/control/d/a;


# direct methods
.method constructor <init>(Lcom/peel/control/d/a;Lcom/peel/control/o;)V
    .locals 0

    .prologue
    .line 24
    iput-object p1, p0, Lcom/peel/control/d/b;->b:Lcom/peel/control/d/a;

    iput-object p2, p0, Lcom/peel/control/d/b;->a:Lcom/peel/control/o;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 7

    .prologue
    const/16 v3, 0x10

    const/4 v1, 0x0

    .line 43
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 99
    :goto_0
    return-void

    .line 45
    :pswitch_0
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    const-string/jumbo v2, "Result"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/htc/htcircontrol/HtcIrData;

    .line 46
    if-eqz v0, :cond_0

    .line 48
    invoke-static {}, Lcom/peel/control/d/a;->e()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "LEARNED RepeatCount:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/htc/htcircontrol/HtcIrData;->getRepeatCount()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " Freq:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/htc/htcircontrol/HtcIrData;->getFrequency()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " FrameLength:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/htc/htcircontrol/HtcIrData;->getFrame()[I

    move-result-object v3

    array-length v3, v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 50
    invoke-static {}, Lcom/peel/control/d/a;->e()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "LEARNED Frame:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/htc/htcircontrol/HtcIrData;->getFrame()[I

    move-result-object v3

    invoke-static {v3}, Ljava/util/Arrays;->toString([I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 51
    sget-object v1, Lcom/peel/control/o;->a:Lcom/peel/control/s;

    const/16 v2, 0xe

    iget-object v3, p0, Lcom/peel/control/d/b;->a:Lcom/peel/control/o;

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-virtual {v0}, Lcom/htc/htcircontrol/HtcIrData;->getRepeatCount()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    invoke-virtual {v0}, Lcom/htc/htcircontrol/HtcIrData;->getFrequency()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x2

    invoke-virtual {v0}, Lcom/htc/htcircontrol/HtcIrData;->getFrame()[I

    move-result-object v0

    aput-object v0, v4, v5

    invoke-virtual {v1, v2, v3, v4}, Lcom/peel/control/s;->a(ILjava/lang/Object;[Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 53
    :cond_0
    iget v0, p1, Landroid/os/Message;->arg1:I

    sparse-switch v0, :sswitch_data_0

    .line 91
    invoke-static {}, Lcom/peel/control/d/a;->e()Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Learn IR error = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p1, Landroid/os/Message;->arg1:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ", trigger again"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 93
    sget-object v0, Lcom/peel/control/o;->a:Lcom/peel/control/s;

    const/16 v2, 0x11

    iget-object v3, p0, Lcom/peel/control/d/b;->a:Lcom/peel/control/o;

    check-cast v1, [Ljava/lang/Object;

    invoke-virtual {v0, v2, v3, v1}, Lcom/peel/control/s;->a(ILjava/lang/Object;[Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 57
    :sswitch_0
    invoke-static {}, Lcom/peel/control/d/a;->e()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v2, "Learn IR error = ERR_LEARNING_TIMEOUT, trigger again"

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 58
    sget-object v2, Lcom/peel/control/o;->a:Lcom/peel/control/s;

    const/16 v3, 0xf

    iget-object v4, p0, Lcom/peel/control/d/b;->a:Lcom/peel/control/o;

    move-object v0, v1

    check-cast v0, [Ljava/lang/Object;

    invoke-virtual {v2, v3, v4, v0}, Lcom/peel/control/s;->a(ILjava/lang/Object;[Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 65
    :sswitch_1
    invoke-static {}, Lcom/peel/control/d/a;->e()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v2, "Learn IR error = ERR_PULSE_ERROR, still continuing"

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 66
    iget-object v0, p0, Lcom/peel/control/d/b;->b:Lcom/peel/control/d/a;

    invoke-static {v0}, Lcom/peel/control/d/a;->b(Lcom/peel/control/d/a;)Lcom/htc/circontrol/CIRControl;

    move-result-object v0

    iget-object v2, p0, Lcom/peel/control/d/b;->b:Lcom/peel/control/d/a;

    invoke-static {v2}, Lcom/peel/control/d/a;->a(Lcom/peel/control/d/a;)I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/htc/circontrol/CIRControl;->learnIRCmd(I)Ljava/util/UUID;

    .line 67
    sget-object v0, Lcom/peel/control/o;->a:Lcom/peel/control/s;

    iget-object v2, p0, Lcom/peel/control/d/b;->a:Lcom/peel/control/o;

    check-cast v1, [Ljava/lang/Object;

    invoke-virtual {v0, v3, v2, v1}, Lcom/peel/control/s;->a(ILjava/lang/Object;[Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 73
    :sswitch_2
    invoke-static {}, Lcom/peel/control/d/a;->e()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v2, "Learn IR error = ERR_OUT_OF_FREQ, still continuing"

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 74
    iget-object v0, p0, Lcom/peel/control/d/b;->b:Lcom/peel/control/d/a;

    invoke-static {v0}, Lcom/peel/control/d/a;->b(Lcom/peel/control/d/a;)Lcom/htc/circontrol/CIRControl;

    move-result-object v0

    iget-object v2, p0, Lcom/peel/control/d/b;->b:Lcom/peel/control/d/a;

    invoke-static {v2}, Lcom/peel/control/d/a;->a(Lcom/peel/control/d/a;)I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/htc/circontrol/CIRControl;->learnIRCmd(I)Ljava/util/UUID;

    .line 75
    sget-object v0, Lcom/peel/control/o;->a:Lcom/peel/control/s;

    iget-object v2, p0, Lcom/peel/control/d/b;->a:Lcom/peel/control/o;

    check-cast v1, [Ljava/lang/Object;

    invoke-virtual {v0, v3, v2, v1}, Lcom/peel/control/s;->a(ILjava/lang/Object;[Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 80
    :sswitch_3
    invoke-static {}, Lcom/peel/control/d/a;->e()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "Learn IR error = ERR_CMD_FAILED due to retriggering, so sys ignores it"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 84
    :sswitch_4
    invoke-static {}, Lcom/peel/control/d/a;->e()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "Learn IR error = ERR_CANCEL, intentionally cancelled"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 43
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch

    .line 53
    :sswitch_data_0
    .sparse-switch
        0x2 -> :sswitch_3
        0x14 -> :sswitch_0
        0x17 -> :sswitch_2
        0x18 -> :sswitch_4
        0x19 -> :sswitch_1
    .end sparse-switch
.end method
