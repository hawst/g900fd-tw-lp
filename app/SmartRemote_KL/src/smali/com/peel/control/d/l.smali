.class Lcom/peel/control/d/l;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/hardware/SmartIr$ReceiveCallback;


# instance fields
.field final synthetic a:Lcom/peel/control/d/j;


# direct methods
.method private constructor <init>(Lcom/peel/control/d/j;)V
    .locals 0

    .prologue
    .line 165
    iput-object p1, p0, Lcom/peel/control/d/l;->a:Lcom/peel/control/d/j;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/peel/control/d/j;Lcom/peel/control/d/k;)V
    .locals 0

    .prologue
    .line 165
    invoke-direct {p0, p1}, Lcom/peel/control/d/l;-><init>(Lcom/peel/control/d/j;)V

    return-void
.end method


# virtual methods
.method public onFailure(Landroid/hardware/SmartIrFailure;)V
    .locals 5

    .prologue
    const/16 v4, 0x11

    const/4 v0, 0x0

    .line 206
    invoke-static {}, Lcom/peel/control/d/j;->e()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "ReceiveCallback.onFailure() statusCode: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Landroid/hardware/SmartIrFailure;->getStatusCode()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 207
    invoke-static {}, Lcom/peel/control/d/j;->e()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "ReceiveCallback.onFailure() message: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Landroid/hardware/SmartIrFailure;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 209
    invoke-virtual {p1}, Landroid/hardware/SmartIrFailure;->getStatusCode()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 220
    :goto_0
    :pswitch_0
    return-void

    .line 211
    :pswitch_1
    sget-object v1, Lcom/peel/control/o;->a:Lcom/peel/control/s;

    iget-object v2, p0, Lcom/peel/control/d/l;->a:Lcom/peel/control/d/j;

    invoke-static {v2}, Lcom/peel/control/d/j;->b(Lcom/peel/control/d/j;)Lcom/peel/control/o;

    move-result-object v2

    check-cast v0, [Ljava/lang/Object;

    invoke-virtual {v1, v4, v2, v0}, Lcom/peel/control/s;->a(ILjava/lang/Object;[Ljava/lang/Object;)V

    goto :goto_0

    .line 214
    :pswitch_2
    sget-object v1, Lcom/peel/control/o;->a:Lcom/peel/control/s;

    const/16 v2, 0xf

    iget-object v3, p0, Lcom/peel/control/d/l;->a:Lcom/peel/control/d/j;

    invoke-static {v3}, Lcom/peel/control/d/j;->b(Lcom/peel/control/d/j;)Lcom/peel/control/o;

    move-result-object v3

    check-cast v0, [Ljava/lang/Object;

    invoke-virtual {v1, v2, v3, v0}, Lcom/peel/control/s;->a(ILjava/lang/Object;[Ljava/lang/Object;)V

    goto :goto_0

    .line 217
    :pswitch_3
    sget-object v1, Lcom/peel/control/o;->a:Lcom/peel/control/s;

    iget-object v2, p0, Lcom/peel/control/d/l;->a:Lcom/peel/control/d/j;

    invoke-static {v2}, Lcom/peel/control/d/j;->b(Lcom/peel/control/d/j;)Lcom/peel/control/o;

    move-result-object v2

    check-cast v0, [Ljava/lang/Object;

    invoke-virtual {v1, v4, v2, v0}, Lcom/peel/control/s;->a(ILjava/lang/Object;[Ljava/lang/Object;)V

    goto :goto_0

    .line 209
    nop

    :pswitch_data_0
    .packed-switch 0x65
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public onSuccess(ILjava/lang/String;)V
    .locals 9

    .prologue
    const/4 v8, 0x3

    const/4 v7, 0x1

    const/4 v1, 0x0

    .line 168
    invoke-static {}, Lcom/peel/control/d/j;->e()Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "ReceiveCallback.onSuccess() frequency = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 169
    invoke-static {}, Lcom/peel/control/d/j;->e()Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "ReceiveCallback.onSuccess() pattern = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 171
    const-string/jumbo v0, ","

    invoke-virtual {p2, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 172
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    move v0, v1

    move v2, v1

    .line 175
    :goto_0
    array-length v5, v3

    if-ge v0, v5, :cond_0

    .line 176
    aget-object v5, v3, v0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    .line 177
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 179
    add-int/lit8 v0, v0, 0x1

    .line 180
    array-length v5, v3

    if-ne v0, v5, :cond_1

    .line 195
    :cond_0
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v0

    new-array v3, v0, [I

    move v2, v1

    .line 196
    :goto_1
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_3

    .line 197
    invoke-interface {v4, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    aput v0, v3, v2

    .line 196
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 184
    :cond_1
    aget-object v5, v3, v0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    .line 185
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v4, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 187
    const/16 v6, 0x2bc

    if-lt v5, v6, :cond_2

    .line 188
    add-int/lit8 v2, v2, 0x1

    .line 189
    if-eq v2, v8, :cond_0

    .line 175
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 201
    :cond_3
    sget-object v0, Lcom/peel/control/o;->a:Lcom/peel/control/s;

    const/16 v2, 0xe

    iget-object v4, p0, Lcom/peel/control/d/l;->a:Lcom/peel/control/d/j;

    invoke-static {v4}, Lcom/peel/control/d/j;->b(Lcom/peel/control/d/j;)Lcom/peel/control/o;

    move-result-object v4

    new-array v5, v8, [Ljava/lang/Object;

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v5, v7

    const/4 v1, 0x2

    aput-object v3, v5, v1

    invoke-virtual {v0, v2, v4, v5}, Lcom/peel/control/s;->a(ILjava/lang/Object;[Ljava/lang/Object;)V

    .line 202
    return-void
.end method
