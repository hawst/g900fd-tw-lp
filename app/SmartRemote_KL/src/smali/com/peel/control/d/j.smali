.class public Lcom/peel/control/d/j;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/peel/control/d/c;


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private b:Landroid/hardware/SmartIr;

.field private c:Ljava/lang/Object;

.field private d:Lcom/peel/control/o;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 15
    const-class v0, Lcom/peel/control/d/j;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/peel/control/d/j;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/peel/control/o;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-object p2, p0, Lcom/peel/control/d/j;->d:Lcom/peel/control/o;

    .line 24
    const-string/jumbo v0, "smart_ir"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/SmartIr;

    iput-object v0, p0, Lcom/peel/control/d/j;->b:Landroid/hardware/SmartIr;

    .line 25
    iget-object v0, p0, Lcom/peel/control/d/j;->b:Landroid/hardware/SmartIr;

    if-nez v0, :cond_0

    .line 26
    new-instance v0, Ljava/lang/NoClassDefFoundError;

    invoke-direct {v0}, Ljava/lang/NoClassDefFoundError;-><init>()V

    throw v0

    .line 29
    :cond_0
    iget-object v0, p0, Lcom/peel/control/d/j;->b:Landroid/hardware/SmartIr;

    if-eqz v0, :cond_1

    .line 30
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/peel/control/d/j;->c:Ljava/lang/Object;

    .line 31
    iget-object v0, p0, Lcom/peel/control/d/j;->b:Landroid/hardware/SmartIr;

    new-instance v1, Lcom/peel/control/d/m;

    invoke-direct {v1, p0, v2}, Lcom/peel/control/d/m;-><init>(Lcom/peel/control/d/j;Lcom/peel/control/d/k;)V

    invoke-virtual {v0, v1, v2}, Landroid/hardware/SmartIr;->registerTransmitCallback(Landroid/hardware/SmartIr$TransmitCallback;Landroid/os/Handler;)I

    .line 32
    iget-object v0, p0, Lcom/peel/control/d/j;->b:Landroid/hardware/SmartIr;

    new-instance v1, Lcom/peel/control/d/l;

    invoke-direct {v1, p0, v2}, Lcom/peel/control/d/l;-><init>(Lcom/peel/control/d/j;Lcom/peel/control/d/k;)V

    invoke-virtual {v0, v1, v2}, Landroid/hardware/SmartIr;->registerReceiveCallback(Landroid/hardware/SmartIr$ReceiveCallback;Landroid/os/Handler;)I

    .line 34
    :cond_1
    return-void
.end method

.method static synthetic a(Lcom/peel/control/d/j;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 14
    iget-object v0, p0, Lcom/peel/control/d/j;->c:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic b(Lcom/peel/control/d/j;)Lcom/peel/control/o;
    .locals 1

    .prologue
    .line 14
    iget-object v0, p0, Lcom/peel/control/d/j;->d:Lcom/peel/control/o;

    return-object v0
.end method

.method static synthetic e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 14
    sget-object v0, Lcom/peel/control/d/j;->a:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public a(Ljava/lang/String;)J
    .locals 10

    .prologue
    const/4 v4, 0x0

    .line 38
    .line 40
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    .line 41
    const/16 v0, 0x2c

    invoke-virtual {p1, v0}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    .line 42
    invoke-virtual {p1, v4, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 43
    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p1, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    .line 45
    iget-object v0, p0, Lcom/peel/control/d/j;->b:Landroid/hardware/SmartIr;

    const/4 v3, 0x0

    move v5, v4

    move v6, v4

    invoke-virtual/range {v0 .. v6}, Landroid/hardware/SmartIr;->transmit(ILjava/lang/String;Ljava/lang/String;III)I

    move-result v0

    .line 49
    sparse-switch v0, :sswitch_data_0

    .line 66
    :goto_0
    if-eqz v4, :cond_0

    .line 67
    iget-object v1, p0, Lcom/peel/control/d/j;->c:Ljava/lang/Object;

    monitor-enter v1

    .line 69
    :try_start_0
    iget-object v0, p0, Lcom/peel/control/d/j;->c:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->wait()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 72
    :goto_1
    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 75
    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    sub-long/2addr v0, v8

    return-wide v0

    .line 51
    :sswitch_0
    sget-object v0, Lcom/peel/control/d/j;->a:Ljava/lang/String;

    const-string/jumbo v1, "Transmitting..."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 52
    const/4 v4, 0x1

    .line 53
    goto :goto_0

    .line 55
    :sswitch_1
    sget-object v0, Lcom/peel/control/d/j;->a:Ljava/lang/String;

    const-string/jumbo v1, "Immediate transmit failure."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 58
    :sswitch_2
    sget-object v0, Lcom/peel/control/d/j;->a:Ljava/lang/String;

    const-string/jumbo v1, "Trasmitting already in progress"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 61
    :sswitch_3
    sget-object v0, Lcom/peel/control/d/j;->a:Ljava/lang/String;

    const-string/jumbo v1, "Receiving already in progress"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 72
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 70
    :catch_0
    move-exception v0

    goto :goto_1

    .line 49
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x7 -> :sswitch_1
        0x64 -> :sswitch_2
        0x68 -> :sswitch_3
    .end sparse-switch
.end method

.method public a()V
    .locals 2

    .prologue
    .line 81
    iget-object v0, p0, Lcom/peel/control/d/j;->b:Landroid/hardware/SmartIr;

    invoke-virtual {v0}, Landroid/hardware/SmartIr;->cancelContinuousTransmit()I

    move-result v0

    .line 82
    if-nez v0, :cond_0

    .line 83
    sget-object v0, Lcom/peel/control/d/j;->a:Ljava/lang/String;

    const-string/jumbo v1, "Canceling continuous transmit..."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 85
    :cond_0
    iget-object v0, p0, Lcom/peel/control/d/j;->b:Landroid/hardware/SmartIr;

    invoke-virtual {v0}, Landroid/hardware/SmartIr;->cancelReceiving()I

    move-result v0

    .line 86
    if-nez v0, :cond_1

    .line 87
    sget-object v0, Lcom/peel/control/d/j;->a:Ljava/lang/String;

    const-string/jumbo v1, "Canceling receive..."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 89
    :cond_1
    return-void
.end method

.method public a(I)Z
    .locals 3

    .prologue
    .line 106
    const/4 v0, 0x0

    .line 107
    iget-object v1, p0, Lcom/peel/control/d/j;->b:Landroid/hardware/SmartIr;

    invoke-virtual {v1, p1}, Landroid/hardware/SmartIr;->receive(I)I

    move-result v1

    .line 109
    packed-switch v1, :pswitch_data_0

    .line 120
    :pswitch_0
    sget-object v0, Lcom/peel/control/d/j;->a:Ljava/lang/String;

    const-string/jumbo v1, "Receiving..."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 121
    const/4 v0, 0x1

    .line 125
    :goto_0
    return v0

    .line 111
    :pswitch_1
    sget-object v1, Lcom/peel/control/d/j;->a:Ljava/lang/String;

    const-string/jumbo v2, "Immediate receive failure."

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 114
    :pswitch_2
    sget-object v1, Lcom/peel/control/d/j;->a:Ljava/lang/String;

    const-string/jumbo v2, "Trasmitting already in progress"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 117
    :pswitch_3
    sget-object v1, Lcom/peel/control/d/j;->a:Ljava/lang/String;

    const-string/jumbo v2, "Receiving already in progress"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 109
    nop

    :pswitch_data_0
    .packed-switch 0x64
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_3
    .end packed-switch
.end method

.method public b()V
    .locals 2

    .prologue
    .line 94
    iget-object v0, p0, Lcom/peel/control/d/j;->b:Landroid/hardware/SmartIr;

    invoke-virtual {v0}, Landroid/hardware/SmartIr;->cancelContinuousTransmit()I

    move-result v0

    .line 95
    if-nez v0, :cond_0

    .line 96
    sget-object v0, Lcom/peel/control/d/j;->a:Ljava/lang/String;

    const-string/jumbo v1, "Canceling continuous transmit..."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 98
    :cond_0
    iget-object v0, p0, Lcom/peel/control/d/j;->b:Landroid/hardware/SmartIr;

    invoke-virtual {v0}, Landroid/hardware/SmartIr;->cancelReceiving()I

    move-result v0

    .line 99
    if-nez v0, :cond_1

    .line 100
    sget-object v0, Lcom/peel/control/d/j;->a:Ljava/lang/String;

    const-string/jumbo v1, "Canceling receive..."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 102
    :cond_1
    return-void
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 131
    iget-object v0, p0, Lcom/peel/control/d/j;->b:Landroid/hardware/SmartIr;

    invoke-virtual {v0}, Landroid/hardware/SmartIr;->cancelReceiving()I

    .line 133
    const/4 v0, 0x0

    return v0
.end method

.method public d()Z
    .locals 1

    .prologue
    .line 138
    const/4 v0, 0x1

    return v0
.end method
