.class public Lcom/peel/control/d/f;
.super Lcom/peel/control/o;


# static fields
.field private static final c:Ljava/lang/String;

.field private static final d:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private static final e:Ljava/util/concurrent/atomic/AtomicBoolean;


# instance fields
.field private final f:Ljava/util/concurrent/ConcurrentLinkedQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentLinkedQueue",
            "<",
            "Lcom/peel/control/d/h;",
            ">;"
        }
    .end annotation
.end field

.field private final g:Ljava/lang/Runnable;

.field private h:Lcom/peel/control/d/c;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 22
    const-class v0, Lcom/peel/control/d/f;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/peel/control/d/f;->c:Ljava/lang/String;

    .line 24
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    sput-object v0, Lcom/peel/control/d/f;->d:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 25
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    sput-object v0, Lcom/peel/control/d/f;->e:Ljava/util/concurrent/atomic/AtomicBoolean;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 82
    const/4 v0, 0x0

    const-string/jumbo v1, "builtin"

    invoke-static {v0, v1, v2}, Lcom/peel/data/h;->a(ILjava/lang/String;Ljava/lang/String;)Lcom/peel/data/h;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/peel/control/o;-><init>(Lcom/peel/data/h;)V

    .line 29
    new-instance v0, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V

    iput-object v0, p0, Lcom/peel/control/d/f;->f:Ljava/util/concurrent/ConcurrentLinkedQueue;

    .line 30
    new-instance v0, Lcom/peel/control/d/g;

    invoke-direct {v0, p0}, Lcom/peel/control/d/g;-><init>(Lcom/peel/control/d/f;)V

    iput-object v0, p0, Lcom/peel/control/d/f;->g:Ljava/lang/Runnable;

    .line 80
    iput-object v2, p0, Lcom/peel/control/d/f;->h:Lcom/peel/control/d/c;

    .line 82
    return-void
.end method

.method public constructor <init>(Lcom/peel/data/h;)V
    .locals 1

    .prologue
    .line 84
    invoke-direct {p0, p1}, Lcom/peel/control/o;-><init>(Lcom/peel/data/h;)V

    .line 29
    new-instance v0, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V

    iput-object v0, p0, Lcom/peel/control/d/f;->f:Ljava/util/concurrent/ConcurrentLinkedQueue;

    .line 30
    new-instance v0, Lcom/peel/control/d/g;

    invoke-direct {v0, p0}, Lcom/peel/control/d/g;-><init>(Lcom/peel/control/d/f;)V

    iput-object v0, p0, Lcom/peel/control/d/f;->g:Ljava/lang/Runnable;

    .line 80
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/peel/control/d/f;->h:Lcom/peel/control/d/c;

    .line 84
    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)J
    .locals 6

    .prologue
    .line 320
    const-wide/16 v0, 0x0

    .line 321
    new-instance v2, Ljava/util/StringTokenizer;

    const-string/jumbo v3, ","

    invoke-direct {v2, p1, v3}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 322
    :goto_0
    invoke-virtual {v2}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 323
    invoke-virtual {v2}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    add-long/2addr v0, v4

    goto :goto_0

    .line 325
    :cond_0
    const-wide/16 v2, 0x3e8

    mul-long/2addr v0, v2

    invoke-static {p2}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    div-long/2addr v0, v2

    const-wide/16 v2, 0x1

    add-long/2addr v0, v2

    return-wide v0
.end method

.method private a(Ljava/lang/String;I)Ljava/lang/String;
    .locals 5

    .prologue
    const/16 v4, 0x2c

    const/16 v3, 0x20

    const/4 v0, 0x1

    .line 296
    if-le p2, v0, :cond_1

    .line 297
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1, p1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 298
    :goto_0
    if-ge v0, p2, :cond_0

    .line 299
    const-string/jumbo v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 298
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 301
    :cond_0
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, v3, v4}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v0

    .line 303
    :goto_1
    return-object v0

    :cond_1
    invoke-virtual {p1, v3, v4}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method static synthetic a(Lcom/peel/control/d/f;)Ljava/util/concurrent/ConcurrentLinkedQueue;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/peel/control/d/f;->f:Ljava/util/concurrent/ConcurrentLinkedQueue;

    return-object v0
.end method

.method static synthetic b(Lcom/peel/control/d/f;)Lcom/peel/control/d/c;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/peel/control/d/f;->h:Lcom/peel/control/d/c;

    return-object v0
.end method

.method static synthetic l()Ljava/util/concurrent/atomic/AtomicBoolean;
    .locals 1

    .prologue
    .line 21
    sget-object v0, Lcom/peel/control/d/f;->d:Ljava/util/concurrent/atomic/AtomicBoolean;

    return-object v0
.end method

.method static synthetic m()Ljava/util/concurrent/atomic/AtomicBoolean;
    .locals 1

    .prologue
    .line 21
    sget-object v0, Lcom/peel/control/d/f;->e:Ljava/util/concurrent/atomic/AtomicBoolean;

    return-object v0
.end method

.method static synthetic n()Ljava/lang/String;
    .locals 1

    .prologue
    .line 21
    sget-object v0, Lcom/peel/control/d/f;->c:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public a(I)Z
    .locals 1

    .prologue
    .line 308
    iget-object v0, p0, Lcom/peel/control/d/f;->h:Lcom/peel/control/d/c;

    invoke-interface {v0, p1}, Lcom/peel/control/d/c;->a(I)Z

    move-result v0

    return v0
.end method

.method public a(Ljava/util/List;)Z
    .locals 17
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;>;)Z"
        }
    .end annotation

    .prologue
    .line 211
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 212
    sget-object v1, Lcom/peel/control/d/f;->c:Ljava/lang/String;

    const-string/jumbo v2, "stop sending IR commands: empty commands."

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 213
    const/4 v1, 0x0

    .line 292
    :goto_0
    return v1

    .line 216
    :cond_0
    sget-object v2, Lcom/peel/control/d/f;->c:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "\n\nsendCommands: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const/4 v1, 0x0

    move-object/from16 v0, p1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 217
    const/4 v1, 0x0

    move-object/from16 v0, p1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 219
    sget-object v4, Lcom/peel/control/d/f;->c:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "\nkey: "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v5, " -- value: "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const/4 v2, 0x0

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map;

    invoke-interface {v2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v4, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 222
    :cond_1
    :try_start_0
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->size()I

    move-result v1

    new-array v12, v1, [Lcom/peel/control/d/h;

    .line 223
    const/4 v1, 0x0

    .line 224
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v13

    move v10, v1

    :goto_2
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v0, v1

    check-cast v0, Ljava/util/Map;

    move-object v9, v0

    .line 239
    const-string/jumbo v1, "frequency"

    invoke-interface {v9, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    const-string/jumbo v1, "frequency"

    invoke-interface {v9, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    move-object v5, v1

    .line 240
    :goto_3
    const-string/jumbo v1, "funName"

    invoke-interface {v9, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 241
    const-string/jumbo v2, "Delay"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    .line 248
    const-string/jumbo v2, "toggle"

    const-string/jumbo v1, "type"

    invoke-interface {v9, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v2, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 249
    const-string/jumbo v1, "repeatcount"

    invoke-interface {v9, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 250
    const-string/jumbo v1, "mainframe"

    invoke-interface {v9, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-direct {v0, v1, v2}, Lcom/peel/control/d/f;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v1

    move-object v2, v1

    .line 266
    :goto_4
    move-object/from16 v0, p0

    invoke-direct {v0, v2, v5}, Lcom/peel/control/d/f;->a(Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v3

    .line 269
    const-string/jumbo v1, "repeatframe"

    invoke-interface {v9, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 270
    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v6

    if-eqz v6, :cond_3

    .line 271
    :cond_2
    const-string/jumbo v1, "mainframe"

    invoke-interface {v9, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 273
    :cond_3
    const-string/jumbo v6, " "

    const-string/jumbo v7, ","

    invoke-virtual {v1, v6, v7}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v14

    .line 274
    move-object/from16 v0, p0

    invoke-direct {v0, v14, v5}, Lcom/peel/control/d/f;->a(Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v6

    .line 276
    add-int/lit8 v11, v10, 0x1

    new-instance v1, Lcom/peel/control/d/h;

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v15, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string/jumbo v16, ","

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v15, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v15, ","

    invoke-virtual {v5, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const-string/jumbo v14, "funName"

    invoke-interface {v9, v14}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    invoke-direct/range {v1 .. v9}, Lcom/peel/control/d/h;-><init>(Ljava/lang/String;JLjava/lang/String;JZLjava/lang/String;)V

    aput-object v1, v12, v10

    move v10, v11

    .line 277
    goto/16 :goto_2

    .line 239
    :cond_4
    const-string/jumbo v1, "38400"

    move-object v5, v1

    goto/16 :goto_3

    .line 252
    :cond_5
    const-string/jumbo v1, "ir"

    invoke-interface {v9, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    move-object v2, v1

    goto/16 :goto_4

    .line 279
    :cond_6
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/peel/control/d/f;->f:Ljava/util/concurrent/ConcurrentLinkedQueue;

    monitor-enter v2
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 280
    :try_start_1
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/peel/control/d/f;->f:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-static {v12}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/concurrent/ConcurrentLinkedQueue;->addAll(Ljava/util/Collection;)Z

    .line 283
    sget-object v1, Lcom/peel/control/d/f;->d:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v1

    if-nez v1, :cond_7

    .line 284
    sget-object v1, Lcom/peel/control/d/f;->e:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 285
    sget-object v1, Lcom/peel/control/d/f;->c:Ljava/lang/String;

    const-string/jumbo v3, "started sendIr runnable"

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/peel/control/d/f;->g:Ljava/lang/Runnable;

    invoke-static {v1, v3, v4}, Lcom/peel/util/i;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)V

    .line 287
    :cond_7
    monitor-exit v2

    .line 292
    :goto_5
    const/4 v1, 0x1

    goto/16 :goto_0

    .line 287
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v1
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    .line 288
    :catch_0
    move-exception v1

    .line 289
    sget-object v2, Lcom/peel/control/d/f;->c:Ljava/lang/String;

    sget-object v3, Lcom/peel/control/d/f;->c:Ljava/lang/String;

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_5
.end method

.method public c()V
    .locals 1

    .prologue
    .line 339
    iget-object v0, p0, Lcom/peel/control/d/f;->f:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->clear()V

    .line 340
    iget-object v0, p0, Lcom/peel/control/d/f;->h:Lcom/peel/control/d/c;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/peel/control/d/f;->h:Lcom/peel/control/d/c;

    instance-of v0, v0, Lcom/peel/control/d/a;

    if-eqz v0, :cond_0

    .line 341
    iget-object v0, p0, Lcom/peel/control/d/f;->h:Lcom/peel/control/d/c;

    invoke-interface {v0}, Lcom/peel/control/d/c;->b()V

    .line 342
    iget-object v0, p0, Lcom/peel/control/d/f;->h:Lcom/peel/control/d/c;

    invoke-interface {v0}, Lcom/peel/control/d/c;->a()V

    .line 344
    :cond_0
    return-void
.end method

.method public e()Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 88
    sget-object v1, Lcom/peel/control/d/f;->c:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "SDK_INT="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ", Manu="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ", model="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 101
    iget-object v1, p0, Lcom/peel/control/d/f;->h:Lcom/peel/control/d/c;

    if-nez v1, :cond_0

    .line 103
    :try_start_0
    new-instance v1, Lcom/peel/control/d/j;

    sget-object v2, Lcom/peel/control/am;->c:Landroid/content/Context;

    invoke-direct {v1, v2, p0}, Lcom/peel/control/d/j;-><init>(Landroid/content/Context;Lcom/peel/control/o;)V

    iput-object v1, p0, Lcom/peel/control/d/f;->h:Lcom/peel/control/d/c;

    .line 104
    sget-object v1, Lcom/peel/control/d/f;->c:Ljava/lang/String;

    const-string/jumbo v2, "SmartIr service found"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 112
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/peel/control/d/f;->h:Lcom/peel/control/d/c;

    if-nez v1, :cond_1

    sget-object v1, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    const-string/jumbo v2, "HTC"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 114
    :try_start_1
    new-instance v1, Lcom/peel/control/d/a;

    sget-object v2, Lcom/peel/control/am;->c:Landroid/content/Context;

    invoke-direct {v1, v2, p0}, Lcom/peel/control/d/a;-><init>(Landroid/content/Context;Lcom/peel/control/o;)V

    iput-object v1, p0, Lcom/peel/control/d/f;->h:Lcom/peel/control/d/c;

    .line 115
    sget-object v1, Lcom/peel/control/d/f;->c:Ljava/lang/String;

    const-string/jumbo v2, "HTC ir service found"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1

    .line 122
    :cond_1
    :goto_1
    iget-object v1, p0, Lcom/peel/control/d/f;->h:Lcom/peel/control/d/c;

    if-nez v1, :cond_2

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x13

    if-lt v1, v2, :cond_2

    .line 123
    sget-object v1, Lcom/peel/control/d/f;->c:Ljava/lang/String;

    const-string/jumbo v2, "KitKat in"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 125
    :try_start_2
    new-instance v1, Lcom/peel/control/d/d;

    sget-object v2, Lcom/peel/control/am;->c:Landroid/content/Context;

    invoke-direct {v1, v2}, Lcom/peel/control/d/d;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/peel/control/d/f;->h:Lcom/peel/control/d/c;

    .line 126
    sget-object v1, Lcom/peel/control/d/f;->c:Ljava/lang/String;

    const-string/jumbo v2, "KitKat ir service found"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_2

    .line 133
    :cond_2
    :goto_2
    iget-object v1, p0, Lcom/peel/control/d/f;->h:Lcom/peel/control/d/c;

    if-nez v1, :cond_3

    .line 135
    :try_start_3
    new-instance v1, Lcom/peel/control/d/i;

    sget-object v2, Lcom/peel/control/am;->c:Landroid/content/Context;

    invoke-direct {v1, v2}, Lcom/peel/control/d/i;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/peel/control/d/f;->h:Lcom/peel/control/d/c;

    .line 136
    sget-object v1, Lcom/peel/control/d/f;->c:Ljava/lang/String;

    const-string/jumbo v2, "Samsung ir service found"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_3

    .line 197
    :cond_3
    :goto_3
    iget-object v1, p0, Lcom/peel/control/d/f;->h:Lcom/peel/control/d/c;

    if-eqz v1, :cond_4

    .line 198
    iget-object v1, p0, Lcom/peel/control/d/f;->h:Lcom/peel/control/d/c;

    invoke-interface {v1}, Lcom/peel/control/d/c;->a()V

    .line 199
    sget-object v1, Lcom/peel/control/d/f;->c:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "connected to "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/peel/control/d/f;->h:Lcom/peel/control/d/c;

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 200
    sget-object v1, Lcom/peel/control/o;->a:Lcom/peel/control/s;

    const/16 v2, 0x17

    check-cast v0, [Ljava/lang/Object;

    invoke-virtual {v1, v2, p0, v0}, Lcom/peel/control/s;->a(ILjava/lang/Object;[Ljava/lang/Object;)V

    .line 201
    const/4 v0, 0x1

    .line 205
    :goto_4
    return v0

    .line 105
    :catch_0
    move-exception v1

    .line 106
    sget-object v2, Lcom/peel/control/d/f;->c:Ljava/lang/String;

    sget-object v3, Lcom/peel/control/d/f;->c:Ljava/lang/String;

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 107
    iput-object v0, p0, Lcom/peel/control/d/f;->h:Lcom/peel/control/d/c;

    goto/16 :goto_0

    .line 116
    :catch_1
    move-exception v1

    .line 117
    iput-object v0, p0, Lcom/peel/control/d/f;->h:Lcom/peel/control/d/c;

    goto :goto_1

    .line 127
    :catch_2
    move-exception v1

    .line 128
    sget-object v2, Lcom/peel/control/d/f;->c:Ljava/lang/String;

    sget-object v3, Lcom/peel/control/d/f;->c:Ljava/lang/String;

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 129
    iput-object v0, p0, Lcom/peel/control/d/f;->h:Lcom/peel/control/d/c;

    goto :goto_2

    .line 137
    :catch_3
    move-exception v1

    .line 139
    iput-object v0, p0, Lcom/peel/control/d/f;->h:Lcom/peel/control/d/c;

    goto :goto_3

    .line 203
    :cond_4
    sget-object v1, Lcom/peel/control/o;->a:Lcom/peel/control/s;

    const/16 v2, 0x15

    check-cast v0, [Ljava/lang/Object;

    invoke-virtual {v1, v2, p0, v0}, Lcom/peel/control/s;->a(ILjava/lang/Object;[Ljava/lang/Object;)V

    .line 204
    sget-object v0, Lcom/peel/control/d/f;->c:Ljava/lang/String;

    const-string/jumbo v1, "No service found"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 205
    const/4 v0, 0x0

    goto :goto_4
.end method

.method public f()Z
    .locals 1

    .prologue
    .line 312
    iget-object v0, p0, Lcom/peel/control/d/f;->h:Lcom/peel/control/d/c;

    invoke-interface {v0}, Lcom/peel/control/d/c;->c()Z

    move-result v0

    return v0
.end method

.method public g()Z
    .locals 1

    .prologue
    .line 316
    iget-object v0, p0, Lcom/peel/control/d/f;->h:Lcom/peel/control/d/c;

    invoke-interface {v0}, Lcom/peel/control/d/c;->d()Z

    move-result v0

    return v0
.end method

.method public h()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 329
    iget-object v1, p0, Lcom/peel/control/d/f;->h:Lcom/peel/control/d/c;

    if-eqz v1, :cond_0

    .line 330
    iget-object v1, p0, Lcom/peel/control/d/f;->h:Lcom/peel/control/d/c;

    invoke-interface {v1}, Lcom/peel/control/d/c;->b()V

    .line 331
    iput-object v0, p0, Lcom/peel/control/d/f;->h:Lcom/peel/control/d/c;

    .line 334
    :cond_0
    sget-object v1, Lcom/peel/control/o;->a:Lcom/peel/control/s;

    const/16 v2, 0x15

    check-cast v0, [Ljava/lang/Object;

    invoke-virtual {v1, v2, p0, v0}, Lcom/peel/control/s;->a(ILjava/lang/Object;[Ljava/lang/Object;)V

    .line 335
    const/4 v0, 0x1

    return v0
.end method
