.class public Lcom/peel/control/d/i;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/peel/control/d/c;


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private b:Ljava/lang/Object;

.field private c:Ljava/lang/reflect/Method;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 14
    const-class v0, Lcom/peel/control/d/i;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/peel/control/d/i;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    iput-object v0, p0, Lcom/peel/control/d/i;->b:Ljava/lang/Object;

    .line 17
    iput-object v0, p0, Lcom/peel/control/d/i;->c:Ljava/lang/reflect/Method;

    .line 20
    const-string/jumbo v0, "irda"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/control/d/i;->b:Ljava/lang/Object;

    .line 21
    iget-object v0, p0, Lcom/peel/control/d/i;->b:Ljava/lang/Object;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/NoClassDefFoundError;

    invoke-direct {v0}, Ljava/lang/NoClassDefFoundError;-><init>()V

    throw v0

    .line 23
    :cond_0
    iget-object v0, p0, Lcom/peel/control/d/i;->b:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    .line 24
    const-string/jumbo v1, "write_irsend"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Class;

    const/4 v3, 0x0

    const-class v4, Ljava/lang/String;

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/control/d/i;->c:Ljava/lang/reflect/Method;

    .line 25
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;)J
    .locals 6

    .prologue
    .line 30
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 32
    :try_start_0
    iget-object v0, p0, Lcom/peel/control/d/i;->c:Ljava/lang/reflect/Method;

    iget-object v1, p0, Lcom/peel/control/d/i;->b:Ljava/lang/Object;

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    invoke-virtual {v0, v1, v4}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 36
    :goto_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    sub-long/2addr v0, v2

    return-wide v0

    .line 33
    :catch_0
    move-exception v0

    .line 34
    sget-object v1, Lcom/peel/control/d/i;->a:Ljava/lang/String;

    sget-object v4, Lcom/peel/control/d/i;->a:Ljava/lang/String;

    invoke-static {v1, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public a()V
    .locals 0

    .prologue
    .line 43
    return-void
.end method

.method public a(I)Z
    .locals 1

    .prologue
    .line 53
    const/4 v0, 0x0

    return v0
.end method

.method public b()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 46
    iput-object v0, p0, Lcom/peel/control/d/i;->b:Ljava/lang/Object;

    .line 47
    iput-object v0, p0, Lcom/peel/control/d/i;->c:Ljava/lang/reflect/Method;

    .line 48
    return-void
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 59
    const/4 v0, 0x0

    return v0
.end method

.method public d()Z
    .locals 1

    .prologue
    .line 65
    const/4 v0, 0x0

    return v0
.end method
