.class Lcom/peel/control/d/m;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/hardware/SmartIr$TransmitCallback;


# instance fields
.field final synthetic a:Lcom/peel/control/d/j;


# direct methods
.method private constructor <init>(Lcom/peel/control/d/j;)V
    .locals 0

    .prologue
    .line 141
    iput-object p1, p0, Lcom/peel/control/d/m;->a:Lcom/peel/control/d/j;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/peel/control/d/j;Lcom/peel/control/d/k;)V
    .locals 0

    .prologue
    .line 141
    invoke-direct {p0, p1}, Lcom/peel/control/d/m;-><init>(Lcom/peel/control/d/j;)V

    return-void
.end method


# virtual methods
.method public onFailure(Landroid/hardware/SmartIrFailure;)V
    .locals 3

    .prologue
    .line 154
    invoke-static {}, Lcom/peel/control/d/j;->e()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "TransmitCallback.onFailure() statuscode: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/hardware/SmartIrFailure;->getStatusCode()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 155
    invoke-static {}, Lcom/peel/control/d/j;->e()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "TransmitCallback.onFailure() message: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/hardware/SmartIrFailure;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 158
    iget-object v0, p0, Lcom/peel/control/d/m;->a:Lcom/peel/control/d/j;

    invoke-static {v0}, Lcom/peel/control/d/j;->a(Lcom/peel/control/d/j;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 159
    :try_start_0
    iget-object v0, p0, Lcom/peel/control/d/m;->a:Lcom/peel/control/d/j;

    invoke-static {v0}, Lcom/peel/control/d/j;->a(Lcom/peel/control/d/j;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->notify()V

    .line 160
    monitor-exit v1

    .line 161
    return-void

    .line 160
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public onSuccess()V
    .locals 2

    .prologue
    .line 144
    invoke-static {}, Lcom/peel/control/d/j;->e()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "TransmitCallback.onSuccess()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 147
    iget-object v0, p0, Lcom/peel/control/d/m;->a:Lcom/peel/control/d/j;

    invoke-static {v0}, Lcom/peel/control/d/j;->a(Lcom/peel/control/d/j;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 148
    :try_start_0
    iget-object v0, p0, Lcom/peel/control/d/m;->a:Lcom/peel/control/d/j;

    invoke-static {v0}, Lcom/peel/control/d/j;->a(Lcom/peel/control/d/j;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->notify()V

    .line 149
    monitor-exit v1

    .line 150
    return-void

    .line 149
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
