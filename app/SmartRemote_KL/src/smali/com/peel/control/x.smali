.class public final enum Lcom/peel/control/x;
.super Ljava/lang/Enum;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/peel/control/x;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/peel/control/x;

.field public static final enum b:Lcom/peel/control/x;

.field public static final enum c:Lcom/peel/control/x;

.field private static final synthetic d:[Lcom/peel/control/x;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 625
    new-instance v0, Lcom/peel/control/x;

    const-string/jumbo v1, "OVERWRITE"

    invoke-direct {v0, v1, v2}, Lcom/peel/control/x;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/peel/control/x;->a:Lcom/peel/control/x;

    new-instance v0, Lcom/peel/control/x;

    const-string/jumbo v1, "CREATE_IF_NOT_FOUND"

    invoke-direct {v0, v1, v3}, Lcom/peel/control/x;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/peel/control/x;->b:Lcom/peel/control/x;

    new-instance v0, Lcom/peel/control/x;

    const-string/jumbo v1, "MEMORYFILE"

    invoke-direct {v0, v1, v4}, Lcom/peel/control/x;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/peel/control/x;->c:Lcom/peel/control/x;

    .line 624
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/peel/control/x;

    sget-object v1, Lcom/peel/control/x;->a:Lcom/peel/control/x;

    aput-object v1, v0, v2

    sget-object v1, Lcom/peel/control/x;->b:Lcom/peel/control/x;

    aput-object v1, v0, v3

    sget-object v1, Lcom/peel/control/x;->c:Lcom/peel/control/x;

    aput-object v1, v0, v4

    sput-object v0, Lcom/peel/control/x;->d:[Lcom/peel/control/x;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 624
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/peel/control/x;
    .locals 1

    .prologue
    .line 624
    const-class v0, Lcom/peel/control/x;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/peel/control/x;

    return-object v0
.end method

.method public static values()[Lcom/peel/control/x;
    .locals 1

    .prologue
    .line 624
    sget-object v0, Lcom/peel/control/x;->d:[Lcom/peel/control/x;

    invoke-virtual {v0}, [Lcom/peel/control/x;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/peel/control/x;

    return-object v0
.end method
