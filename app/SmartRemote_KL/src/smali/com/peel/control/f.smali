.class public abstract Lcom/peel/control/f;
.super Ljava/lang/Object;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 399
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/peel/control/a;I)Z
    .locals 2

    .prologue
    .line 401
    invoke-static {}, Lcom/peel/control/a;->h()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "start not handled"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 402
    const/4 v0, 0x0

    return v0
.end method

.method public a(Lcom/peel/control/a;I[Lcom/peel/control/h;)Z
    .locals 2

    .prologue
    .line 416
    invoke-static {}, Lcom/peel/control/a;->h()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "stop not handled"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 417
    const/4 v0, 0x0

    return v0
.end method

.method public a(Lcom/peel/control/a;Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 406
    invoke-static {}, Lcom/peel/control/a;->h()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "sendCommand not handled"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 407
    const/4 v0, 0x0

    return v0
.end method

.method public a(Lcom/peel/control/a;Ljava/net/URI;)Z
    .locals 2

    .prologue
    .line 411
    invoke-static {}, Lcom/peel/control/a;->h()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "sendCommand not handled"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 412
    const/4 v0, 0x0

    return v0
.end method
