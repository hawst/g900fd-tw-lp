.class public Lcom/peel/control/am;
.super Ljava/lang/Object;


# static fields
.field public static final a:Lcom/peel/control/aq;

.field public static b:Lcom/peel/control/am;

.field public static c:Landroid/content/Context;

.field private static final d:Ljava/lang/String;

.field private static final e:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/peel/control/h;",
            ">;"
        }
    .end annotation
.end field

.field private static final f:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/peel/control/a;",
            ">;"
        }
    .end annotation
.end field

.field private static g:Z

.field private static h:[Lcom/peel/control/RoomControl;

.field private static i:I

.field private static j:[Lcom/peel/control/o;


# instance fields
.field private k:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private l:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/Boolean;",
            "Ljava/lang/Boolean;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 26
    new-instance v0, Lcom/peel/control/aq;

    invoke-direct {v0}, Lcom/peel/control/aq;-><init>()V

    sput-object v0, Lcom/peel/control/am;->a:Lcom/peel/control/aq;

    .line 27
    const-class v0, Lcom/peel/control/am;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/peel/control/am;->d:Ljava/lang/String;

    .line 28
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/peel/control/am;->e:Ljava/util/Map;

    .line 29
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/peel/control/am;->f:Ljava/util/Map;

    .line 32
    const/4 v0, 0x0

    sput-boolean v0, Lcom/peel/control/am;->g:Z

    .line 35
    const/4 v0, -0x1

    sput v0, Lcom/peel/control/am;->i:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/peel/control/am;->k:Ljava/util/Map;

    .line 38
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/peel/control/am;->l:Ljava/util/Map;

    .line 42
    return-void
.end method

.method public static a()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 75
    invoke-static {}, Lcom/peel/data/m;->a()Lcom/peel/data/m;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/data/m;->b()V

    .line 77
    sput-object v1, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    .line 78
    sput-object v1, Lcom/peel/control/am;->c:Landroid/content/Context;

    .line 80
    sput-object v1, Lcom/peel/control/am;->h:[Lcom/peel/control/RoomControl;

    .line 81
    sput-object v1, Lcom/peel/control/am;->j:[Lcom/peel/control/o;

    .line 82
    sget-object v0, Lcom/peel/control/am;->e:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 83
    sget-object v0, Lcom/peel/control/am;->f:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 85
    const/4 v0, 0x0

    sput-boolean v0, Lcom/peel/control/am;->g:Z

    .line 86
    return-void
.end method

.method public static a(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 45
    sget-boolean v0, Lcom/peel/control/am;->g:Z

    if-eqz v0, :cond_0

    .line 46
    sget-object v0, Lcom/peel/control/am;->d:Ljava/lang/String;

    const-string/jumbo v1, "Loader.load"

    new-instance v2, Lcom/peel/control/an;

    invoke-direct {v2}, Lcom/peel/control/an;-><init>()V

    invoke-static {v0, v1, v2}, Lcom/peel/util/i;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/lang/Runnable;

    .line 69
    :goto_0
    return-void

    .line 54
    :cond_0
    const/4 v0, 0x1

    sput-boolean v0, Lcom/peel/control/am;->g:Z

    .line 55
    sput-object p0, Lcom/peel/control/am;->c:Landroid/content/Context;

    .line 56
    new-instance v0, Lcom/peel/control/am;

    invoke-direct {v0, p0}, Lcom/peel/control/am;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    .line 58
    sget-object v0, Lcom/peel/control/am;->d:Ljava/lang/String;

    const-string/jumbo v1, "load stuff from the database"

    new-instance v2, Lcom/peel/control/ao;

    invoke-direct {v2}, Lcom/peel/control/ao;-><init>()V

    invoke-static {v0, v1, v2}, Lcom/peel/util/i;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/lang/Runnable;

    goto :goto_0
.end method

.method static synthetic a([Lcom/peel/control/RoomControl;)[Lcom/peel/control/RoomControl;
    .locals 0

    .prologue
    .line 25
    sput-object p0, Lcom/peel/control/am;->h:[Lcom/peel/control/RoomControl;

    return-object p0
.end method

.method static synthetic a([Lcom/peel/control/o;)[Lcom/peel/control/o;
    .locals 0

    .prologue
    .line 25
    sput-object p0, Lcom/peel/control/am;->j:[Lcom/peel/control/o;

    return-object p0
.end method

.method public static b()V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 91
    sput-object v1, Lcom/peel/control/am;->h:[Lcom/peel/control/RoomControl;

    .line 92
    sput-object v1, Lcom/peel/control/am;->j:[Lcom/peel/control/o;

    .line 93
    sget-object v0, Lcom/peel/control/am;->e:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 94
    sget-object v0, Lcom/peel/control/am;->f:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 96
    invoke-static {}, Lcom/peel/data/m;->a()Lcom/peel/data/m;

    move-result-object v0

    sget-object v2, Lcom/peel/control/am;->c:Landroid/content/Context;

    invoke-virtual {v0, v2}, Lcom/peel/data/m;->b(Landroid/content/Context;)V

    .line 98
    const/4 v0, -0x1

    sput v0, Lcom/peel/control/am;->i:I

    .line 100
    sget-object v2, Lcom/peel/control/am;->a:Lcom/peel/control/aq;

    const/16 v3, 0x63

    move-object v0, v1

    check-cast v0, [Ljava/lang/Object;

    invoke-virtual {v2, v3, v1, v0}, Lcom/peel/control/aq;->a(ILjava/lang/Object;[Ljava/lang/Object;)V

    .line 101
    return-void
.end method

.method static synthetic g()[Lcom/peel/control/o;
    .locals 1

    .prologue
    .line 25
    sget-object v0, Lcom/peel/control/am;->j:[Lcom/peel/control/o;

    return-object v0
.end method

.method static synthetic h()Ljava/util/Map;
    .locals 1

    .prologue
    .line 25
    sget-object v0, Lcom/peel/control/am;->e:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic i()Ljava/util/Map;
    .locals 1

    .prologue
    .line 25
    sget-object v0, Lcom/peel/control/am;->f:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic j()[Lcom/peel/control/RoomControl;
    .locals 1

    .prologue
    .line 25
    sget-object v0, Lcom/peel/control/am;->h:[Lcom/peel/control/RoomControl;

    return-object v0
.end method


# virtual methods
.method public a(Ljava/lang/String;)Lcom/peel/control/RoomControl;
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 107
    sget-object v1, Lcom/peel/control/am;->h:[Lcom/peel/control/RoomControl;

    if-nez v1, :cond_1

    .line 111
    :cond_0
    :goto_0
    return-object v0

    .line 108
    :cond_1
    sget-object v3, Lcom/peel/control/am;->h:[Lcom/peel/control/RoomControl;

    array-length v4, v3

    const/4 v1, 0x0

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_0

    aget-object v1, v3, v2

    .line 109
    invoke-virtual {v1}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v5

    invoke-virtual {v5}, Lcom/peel/data/at;->b()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    move-object v0, v1

    goto :goto_0

    .line 108
    :cond_2
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_1
.end method

.method public a(Lcom/peel/control/RoomControl;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 115
    sget-object v0, Lcom/peel/control/am;->h:[Lcom/peel/control/RoomControl;

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    add-int/lit8 v0, v0, 0x1

    new-array v0, v0, [Lcom/peel/control/RoomControl;

    .line 116
    sget-object v2, Lcom/peel/control/am;->h:[Lcom/peel/control/RoomControl;

    if-eqz v2, :cond_0

    sget-object v2, Lcom/peel/control/am;->h:[Lcom/peel/control/RoomControl;

    sget-object v3, Lcom/peel/control/am;->h:[Lcom/peel/control/RoomControl;

    array-length v3, v3

    invoke-static {v2, v1, v0, v1, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 117
    :cond_0
    array-length v1, v0

    add-int/lit8 v1, v1, -0x1

    aput-object p1, v0, v1

    .line 118
    sput-object v0, Lcom/peel/control/am;->h:[Lcom/peel/control/RoomControl;

    .line 120
    invoke-static {}, Lcom/peel/data/m;->a()Lcom/peel/data/m;

    move-result-object v0

    invoke-virtual {p1}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/peel/data/m;->b(Lcom/peel/data/at;)V

    .line 125
    invoke-static {}, Lcom/peel/util/a/f;->a()Lcom/peel/util/a/f;

    move-result-object v0

    invoke-virtual {p1}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/data/at;->f()I

    move-result v1

    const/16 v2, 0x1450

    const/16 v3, 0x7e7

    invoke-virtual {p1}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v4

    invoke-virtual {v4}, Lcom/peel/data/at;->a()Ljava/lang/String;

    move-result-object v4

    const/4 v5, -0x1

    invoke-virtual/range {v0 .. v5}, Lcom/peel/util/a/f;->a(IIILjava/lang/String;I)V

    .line 127
    return-void

    .line 115
    :cond_1
    sget-object v0, Lcom/peel/control/am;->h:[Lcom/peel/control/RoomControl;

    array-length v0, v0

    goto :goto_0
.end method

.method public a(Lcom/peel/control/RoomControl;Lcom/peel/control/o;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 220
    sget-object v0, Lcom/peel/control/am;->j:[Lcom/peel/control/o;

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    add-int/lit8 v0, v0, 0x1

    new-array v0, v0, [Lcom/peel/control/o;

    .line 221
    sget-object v2, Lcom/peel/control/am;->j:[Lcom/peel/control/o;

    if-eqz v2, :cond_0

    sget-object v2, Lcom/peel/control/am;->j:[Lcom/peel/control/o;

    sget-object v3, Lcom/peel/control/am;->j:[Lcom/peel/control/o;

    array-length v3, v3

    invoke-static {v2, v1, v0, v1, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 222
    :cond_0
    array-length v1, v0

    add-int/lit8 v1, v1, -0x1

    aput-object p2, v0, v1

    .line 223
    sput-object v0, Lcom/peel/control/am;->j:[Lcom/peel/control/o;

    .line 225
    invoke-static {}, Lcom/peel/data/m;->a()Lcom/peel/data/m;

    move-result-object v0

    invoke-virtual {p2}, Lcom/peel/control/o;->d()Lcom/peel/data/h;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/peel/data/m;->a(Lcom/peel/data/h;)V

    .line 234
    return-void

    .line 220
    :cond_1
    sget-object v0, Lcom/peel/control/am;->j:[Lcom/peel/control/o;

    array-length v0, v0

    goto :goto_0
.end method

.method public a(Lcom/peel/control/RoomControl;Z)V
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 350
    sget-object v1, Lcom/peel/control/am;->h:[Lcom/peel/control/RoomControl;

    if-nez v1, :cond_1

    .line 376
    :cond_0
    :goto_0
    return-void

    .line 351
    :cond_1
    const/4 v1, 0x1

    sget-object v2, Lcom/peel/control/am;->h:[Lcom/peel/control/RoomControl;

    array-length v2, v2

    if-ne v1, v2, :cond_2

    if-nez p2, :cond_0

    .line 355
    :cond_2
    sget-object v1, Lcom/peel/control/am;->h:[Lcom/peel/control/RoomControl;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    new-array v4, v1, [Lcom/peel/control/RoomControl;

    .line 356
    sget-object v1, Lcom/peel/control/am;->h:[Lcom/peel/control/RoomControl;

    array-length v5, v1

    move v2, v0

    move v3, v0

    :goto_1
    if-ge v2, v5, :cond_4

    .line 357
    sget-object v1, Lcom/peel/control/am;->h:[Lcom/peel/control/RoomControl;

    aget-object v1, v1, v2

    invoke-virtual {v1}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/data/at;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v6

    invoke-virtual {v6}, Lcom/peel/data/at;->b()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    move v1, v2

    .line 356
    :goto_2
    add-int/lit8 v2, v2, 0x1

    move v3, v1

    goto :goto_1

    .line 360
    :cond_3
    add-int/lit8 v1, v0, 0x1

    sget-object v6, Lcom/peel/control/am;->h:[Lcom/peel/control/RoomControl;

    aget-object v6, v6, v2

    aput-object v6, v4, v0

    move v0, v1

    move v1, v3

    goto :goto_2

    .line 364
    :cond_4
    invoke-static {}, Lcom/peel/data/m;->a()Lcom/peel/data/m;

    move-result-object v0

    invoke-virtual {p1}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/peel/data/m;->a(Lcom/peel/data/at;)V

    .line 366
    sput-object v4, Lcom/peel/control/am;->h:[Lcom/peel/control/RoomControl;

    .line 368
    if-eqz p2, :cond_6

    .line 370
    sget v0, Lcom/peel/control/am;->i:I

    if-gt v3, v0, :cond_5

    sget v0, Lcom/peel/control/am;->i:I

    if-lez v0, :cond_5

    sget v0, Lcom/peel/control/am;->i:I

    add-int/lit8 v0, v0, -0x1

    :goto_3
    sput v0, Lcom/peel/control/am;->i:I

    .line 371
    sget-object v0, Lcom/peel/control/am;->h:[Lcom/peel/control/RoomControl;

    sget v1, Lcom/peel/control/am;->i:I

    aget-object v0, v0, v1

    invoke-virtual {p0, v0}, Lcom/peel/control/am;->b(Lcom/peel/control/RoomControl;)V

    goto :goto_0

    .line 370
    :cond_5
    sget v0, Lcom/peel/control/am;->i:I

    goto :goto_3

    .line 373
    :cond_6
    const/4 v0, -0x1

    sput v0, Lcom/peel/control/am;->i:I

    goto :goto_0
.end method

.method public a(Lcom/peel/control/a;)V
    .locals 3

    .prologue
    .line 325
    sget-object v0, Lcom/peel/control/am;->f:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsValue(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 326
    sget-object v0, Lcom/peel/control/am;->f:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/peel/control/a;->b()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 328
    invoke-static {}, Lcom/peel/data/m;->a()Lcom/peel/data/m;

    move-result-object v0

    invoke-virtual {p1}, Lcom/peel/control/a;->c()Lcom/peel/data/d;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/peel/data/m;->a(Lcom/peel/data/d;)V

    .line 332
    :try_start_0
    invoke-virtual {p1}, Lcom/peel/control/a;->d()[Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/peel/control/a;->d()[Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    const-string/jumbo v1, "live"

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 333
    iget-object v0, p0, Lcom/peel/control/am;->k:Ljava/util/Map;

    invoke-virtual {p0}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/data/at;->b()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 347
    :cond_0
    :goto_0
    return-void

    .line 335
    :catch_0
    move-exception v0

    .line 336
    sget-object v1, Lcom/peel/control/am;->d:Ljava/lang/String;

    sget-object v2, Lcom/peel/control/am;->d:Ljava/lang/String;

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 345
    :cond_1
    sget-object v0, Lcom/peel/control/am;->d:Ljava/lang/String;

    const-string/jumbo v1, "invalid activity removed"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public a(Lcom/peel/control/a;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 304
    sget-object v0, Lcom/peel/control/am;->f:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/peel/control/a;->b()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 306
    invoke-static {}, Lcom/peel/data/m;->a()Lcom/peel/data/m;

    move-result-object v0

    invoke-virtual {p1}, Lcom/peel/control/a;->c()Lcom/peel/data/d;

    move-result-object v1

    invoke-virtual {v0, v1, p2}, Lcom/peel/data/m;->a(Lcom/peel/data/d;Ljava/lang/String;)V

    .line 310
    :try_start_0
    invoke-virtual {p1}, Lcom/peel/control/a;->d()[Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/peel/control/a;->d()[Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    const-string/jumbo v1, "live"

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 311
    iget-object v0, p0, Lcom/peel/control/am;->k:Ljava/util/Map;

    invoke-interface {v0, p2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 322
    :cond_0
    :goto_0
    return-void

    .line 313
    :catch_0
    move-exception v0

    .line 314
    sget-object v1, Lcom/peel/control/am;->d:Ljava/lang/String;

    sget-object v2, Lcom/peel/control/am;->d:Ljava/lang/String;

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public a(Lcom/peel/control/h;)V
    .locals 2

    .prologue
    .line 256
    sget-object v0, Lcom/peel/control/am;->e:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/data/g;->b()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 258
    invoke-static {}, Lcom/peel/data/m;->a()Lcom/peel/data/m;

    move-result-object v0

    invoke-virtual {p1}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/peel/data/m;->a(Lcom/peel/data/g;)V

    .line 259
    return-void
.end method

.method public b(Ljava/lang/String;)Lcom/peel/control/o;
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 212
    sget-object v1, Lcom/peel/control/am;->j:[Lcom/peel/control/o;

    if-nez v1, :cond_1

    .line 216
    :cond_0
    :goto_0
    return-object v0

    .line 213
    :cond_1
    sget-object v3, Lcom/peel/control/am;->j:[Lcom/peel/control/o;

    array-length v4, v3

    const/4 v1, 0x0

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_0

    aget-object v1, v3, v2

    .line 214
    invoke-virtual {v1}, Lcom/peel/control/o;->a()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    move-object v0, v1

    goto :goto_0

    .line 213
    :cond_2
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_1
.end method

.method public b(Lcom/peel/control/RoomControl;)V
    .locals 6

    .prologue
    const/4 v5, -0x1

    .line 145
    if-nez p1, :cond_2

    .line 146
    sget-object v0, Lcom/peel/control/am;->d:Ljava/lang/String;

    const-string/jumbo v1, "\n*************setCurrentRoom(): null room, return"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 147
    sget v0, Lcom/peel/control/am;->i:I

    if-eq v5, v0, :cond_0

    sget-object v0, Lcom/peel/control/am;->h:[Lcom/peel/control/RoomControl;

    sget v1, Lcom/peel/control/am;->i:I

    aget-object v0, v0, v1

    invoke-virtual {v0}, Lcom/peel/control/RoomControl;->f()Z

    .line 148
    :cond_0
    sput v5, Lcom/peel/control/am;->i:I

    .line 171
    :cond_1
    :goto_0
    return-void

    .line 152
    :cond_2
    const/4 v0, 0x0

    :goto_1
    sget-object v1, Lcom/peel/control/am;->h:[Lcom/peel/control/RoomControl;

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 153
    sget-object v1, Lcom/peel/control/am;->h:[Lcom/peel/control/RoomControl;

    aget-object v1, v1, v0

    invoke-virtual {v1, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 155
    sget-object v1, Lcom/peel/control/am;->d:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "\n*************setCurrentRoom() found a room here: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lcom/peel/control/am;->h:[Lcom/peel/control/RoomControl;

    aget-object v3, v3, v0

    invoke-virtual {v3}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v3

    invoke-virtual {v3}, Lcom/peel/data/at;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 157
    sget v1, Lcom/peel/control/am;->i:I

    if-eq v5, v1, :cond_3

    sget v1, Lcom/peel/control/am;->i:I

    if-eq v1, v0, :cond_3

    sget-object v1, Lcom/peel/control/am;->h:[Lcom/peel/control/RoomControl;

    sget v2, Lcom/peel/control/am;->i:I

    aget-object v1, v1, v2

    invoke-virtual {v1}, Lcom/peel/control/RoomControl;->f()Z

    .line 158
    :cond_3
    sput v0, Lcom/peel/control/am;->i:I

    .line 159
    sget-object v1, Lcom/peel/control/am;->h:[Lcom/peel/control/RoomControl;

    aget-object v0, v1, v0

    invoke-virtual {v0}, Lcom/peel/control/RoomControl;->e()Z

    .line 164
    invoke-static {}, Lcom/peel/util/a/f;->a()Lcom/peel/util/a/f;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/util/a/f;->b()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 165
    invoke-static {}, Lcom/peel/util/a/f;->a()Lcom/peel/util/a/f;

    move-result-object v0

    invoke-virtual {p1}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/data/at;->f()I

    move-result v1

    const/16 v2, 0x1456

    const/16 v3, 0x7e7

    invoke-virtual {p1}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v4

    invoke-virtual {v4}, Lcom/peel/data/at;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual/range {v0 .. v5}, Lcom/peel/util/a/f;->a(IIILjava/lang/String;I)V

    goto :goto_0

    .line 152
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method public b(Lcom/peel/control/h;)V
    .locals 4

    .prologue
    const/4 v2, 0x1

    .line 262
    if-eqz p1, :cond_4

    sget-object v0, Lcom/peel/control/am;->e:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsValue(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 263
    sget-object v0, Lcom/peel/control/am;->f:Ljava/util/Map;

    if-eqz v0, :cond_0

    .line 265
    sget-object v0, Lcom/peel/control/am;->f:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v2

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/control/a;

    .line 266
    invoke-virtual {v0}, Lcom/peel/control/a;->e()[Lcom/peel/control/h;

    move-result-object v0

    .line 267
    if-eqz v0, :cond_2

    .line 268
    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v2

    .line 269
    :goto_1
    if-nez v0, :cond_3

    .line 273
    :goto_2
    if-eqz v0, :cond_0

    .line 274
    sget-object v0, Lcom/peel/control/am;->e:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/data/g;->b()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 275
    invoke-static {}, Lcom/peel/data/m;->a()Lcom/peel/data/m;

    move-result-object v0

    invoke-virtual {p1}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/peel/data/m;->b(Lcom/peel/data/g;)V

    .line 298
    :cond_0
    :goto_3
    return-void

    .line 268
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    :cond_2
    move v0, v1

    :cond_3
    move v1, v0

    .line 271
    goto :goto_0

    .line 296
    :cond_4
    sget-object v0, Lcom/peel/control/am;->d:Ljava/lang/String;

    const-string/jumbo v1, "invalid device removed"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    :cond_5
    move v0, v1

    goto :goto_2
.end method

.method public c(Ljava/lang/String;)Lcom/peel/control/h;
    .locals 1

    .prologue
    .line 237
    sget-object v0, Lcom/peel/control/am;->e:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/control/h;

    return-object v0
.end method

.method public c()[Lcom/peel/control/RoomControl;
    .locals 1

    .prologue
    .line 104
    sget-object v0, Lcom/peel/control/am;->h:[Lcom/peel/control/RoomControl;

    return-object v0
.end method

.method public c(Lcom/peel/control/RoomControl;)[Lcom/peel/control/h;
    .locals 10

    .prologue
    const/4 v1, 0x0

    .line 174
    invoke-virtual {p1}, Lcom/peel/control/RoomControl;->c()[Lcom/peel/control/a;

    move-result-object v3

    .line 175
    if-eqz v3, :cond_0

    array-length v0, v3

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    .line 201
    :goto_0
    return-object v0

    .line 177
    :cond_1
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    .line 178
    array-length v5, v3

    move v2, v1

    :goto_1
    if-ge v2, v5, :cond_4

    aget-object v0, v3, v2

    .line 179
    invoke-virtual {v0}, Lcom/peel/control/a;->e()[Lcom/peel/control/h;

    move-result-object v6

    array-length v7, v6

    move v0, v1

    :goto_2
    if-ge v0, v7, :cond_3

    aget-object v8, v6, v0

    .line 180
    invoke-virtual {v8}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v9

    invoke-virtual {v9}, Lcom/peel/data/g;->b()Ljava/lang/String;

    move-result-object v9

    invoke-interface {v4, v9}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_2

    .line 181
    invoke-virtual {v8}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v9

    invoke-virtual {v9}, Lcom/peel/data/g;->b()Ljava/lang/String;

    move-result-object v9

    invoke-interface {v4, v9, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 179
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 178
    :cond_3
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 185
    :cond_4
    new-instance v0, Ljava/util/ArrayList;

    invoke-interface {v4}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 186
    new-instance v1, Lcom/peel/control/ap;

    invoke-direct {v1, p0}, Lcom/peel/control/ap;-><init>(Lcom/peel/control/am;)V

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 201
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    new-array v1, v1, [Lcom/peel/control/h;

    invoke-interface {v0, v1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/peel/control/h;

    goto :goto_0
.end method

.method public d()Lcom/peel/control/RoomControl;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 138
    const/4 v1, -0x1

    sget v2, Lcom/peel/control/am;->i:I

    if-ne v1, v2, :cond_1

    .line 140
    :cond_0
    :goto_0
    return-object v0

    .line 139
    :cond_1
    sget-object v1, Lcom/peel/control/am;->h:[Lcom/peel/control/RoomControl;

    if-eqz v1, :cond_0

    .line 140
    sget-object v0, Lcom/peel/control/am;->h:[Lcom/peel/control/RoomControl;

    sget v1, Lcom/peel/control/am;->i:I

    aget-object v0, v0, v1

    goto :goto_0
.end method

.method public d(Ljava/lang/String;)[Lcom/peel/control/h;
    .locals 10

    .prologue
    const/4 v1, 0x0

    .line 242
    invoke-virtual {p0, p1}, Lcom/peel/control/am;->a(Ljava/lang/String;)Lcom/peel/control/RoomControl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/control/RoomControl;->c()[Lcom/peel/control/a;

    move-result-object v3

    .line 243
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    .line 244
    if-eqz v3, :cond_2

    array-length v0, v3

    if-lez v0, :cond_2

    .line 245
    array-length v5, v3

    move v2, v1

    :goto_0
    if-ge v2, v5, :cond_2

    aget-object v0, v3, v2

    .line 246
    invoke-virtual {v0}, Lcom/peel/control/a;->e()[Lcom/peel/control/h;

    move-result-object v6

    array-length v7, v6

    move v0, v1

    :goto_1
    if-ge v0, v7, :cond_1

    aget-object v8, v6, v0

    .line 247
    invoke-virtual {v8}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v9

    invoke-virtual {v9}, Lcom/peel/data/g;->b()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v4, v9}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_0

    .line 248
    invoke-virtual {v8}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v9

    invoke-virtual {v9}, Lcom/peel/data/g;->b()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v4, v9, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 246
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 245
    :cond_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 252
    :cond_2
    invoke-virtual {v4}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-virtual {v4}, Ljava/util/HashMap;->size()I

    move-result v1

    new-array v1, v1, [Lcom/peel/control/h;

    invoke-interface {v0, v1}, Ljava/util/Collection;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/peel/control/h;

    return-object v0
.end method

.method public e(Ljava/lang/String;)Lcom/peel/control/a;
    .locals 1

    .prologue
    .line 301
    sget-object v0, Lcom/peel/control/am;->f:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/control/a;

    return-object v0
.end method

.method public e()[Lcom/peel/control/h;
    .locals 1

    .prologue
    .line 205
    invoke-virtual {p0}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v0

    .line 206
    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 207
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0, v0}, Lcom/peel/control/am;->c(Lcom/peel/control/RoomControl;)[Lcom/peel/control/h;

    move-result-object v0

    goto :goto_0
.end method

.method public f()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/peel/control/h;",
            ">;"
        }
    .end annotation

    .prologue
    .line 239
    new-instance v0, Ljava/util/ArrayList;

    sget-object v1, Lcom/peel/control/am;->e:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 15

    .prologue
    const/4 v2, 0x0

    .line 379
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 381
    const-string/jumbo v0, "\n\n----------------------Time Zone Info--------------------"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "\nTime Zone: "

    .line 382
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/TimeZone;->getDisplayName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "\nUse Day Light Saving: "

    .line 383
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/TimeZone;->useDaylightTime()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/TimeZone;->getID()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 384
    sget-object v0, Lcom/peel/control/am;->h:[Lcom/peel/control/RoomControl;

    if-eqz v0, :cond_8

    .line 385
    sget-object v6, Lcom/peel/control/am;->h:[Lcom/peel/control/RoomControl;

    array-length v7, v6

    move v4, v2

    :goto_0
    if-ge v4, v7, :cond_8

    aget-object v0, v6, v4

    .line 387
    const-string/jumbo v1, "\n\n**********************Control Room ID: "

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v3

    invoke-virtual {v3}, Lcom/peel/data/at;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v3, "**********************"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 393
    invoke-virtual {v0}, Lcom/peel/control/RoomControl;->b()Lcom/peel/control/o;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 394
    const-string/jumbo v1, "\n\n----------------------Fruit Info------------------------"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 396
    const-string/jumbo v1, "\nfruit-id: "

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Lcom/peel/control/RoomControl;->b()Lcom/peel/control/o;

    move-result-object v3

    invoke-virtual {v3}, Lcom/peel/control/o;->d()Lcom/peel/data/h;

    move-result-object v3

    invoke-virtual {v3}, Lcom/peel/data/h;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v3, "\nversion: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Lcom/peel/control/RoomControl;->b()Lcom/peel/control/o;

    move-result-object v3

    invoke-virtual {v3}, Lcom/peel/control/o;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v3, "\ncategory: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Lcom/peel/control/RoomControl;->b()Lcom/peel/control/o;

    move-result-object v3

    invoke-virtual {v3}, Lcom/peel/control/o;->d()Lcom/peel/data/h;

    move-result-object v3

    invoke-virtual {v3}, Lcom/peel/data/h;->a()I

    move-result v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 400
    :cond_0
    invoke-virtual {v0}, Lcom/peel/control/RoomControl;->c()[Lcom/peel/control/a;

    move-result-object v1

    if-eqz v1, :cond_7

    .line 401
    const-string/jumbo v1, "\n\n----------------------Activity Info------------------------"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 402
    invoke-virtual {v0}, Lcom/peel/control/RoomControl;->c()[Lcom/peel/control/a;

    move-result-object v8

    array-length v9, v8

    move v3, v2

    :goto_1
    if-ge v3, v9, :cond_7

    aget-object v10, v8, v3

    .line 403
    const-string/jumbo v0, "\nactivity-id: "

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v10}, Lcom/peel/control/a;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "\nname: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v10}, Lcom/peel/control/a;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 404
    const-string/jumbo v0, "\n\n----------------------Activity Device Info------------------------"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 405
    invoke-virtual {v10}, Lcom/peel/control/a;->e()[Lcom/peel/control/h;

    move-result-object v11

    array-length v12, v11

    move v1, v2

    :goto_2
    if-ge v1, v12, :cond_2

    aget-object v13, v11, v1

    .line 406
    const-string/jumbo v0, "\ndevice-id: "

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v13}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v14

    invoke-virtual {v14}, Lcom/peel/data/g;->b()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v14, "\ndevice-type: "

    .line 407
    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v13}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v14

    invoke-virtual {v14}, Lcom/peel/data/g;->d()I

    move-result v14

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v14, "\ncodeset-id: "

    .line 408
    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v13}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v14

    invoke-virtual {v14}, Lcom/peel/data/g;->h()I

    move-result v14

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v14, "\nalways-on: "

    .line 409
    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v13}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/data/g;->g()Z

    move-result v0

    if-eqz v0, :cond_1

    const-string/jumbo v0, "true"

    :goto_3
    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v14, "\nbrand-name: "

    .line 410
    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v13}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v13

    invoke-virtual {v13}, Lcom/peel/data/g;->f()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 405
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 409
    :cond_1
    const-string/jumbo v0, "false"

    goto :goto_3

    .line 412
    :cond_2
    const-string/jumbo v0, "\n\n----------------------Activity Device Inputs and Modes Info------------------------"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 413
    invoke-virtual {v10}, Lcom/peel/control/a;->c()Lcom/peel/data/d;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/data/d;->c()Ljava/lang/Iterable;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :cond_3
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    .line 414
    const-string/jumbo v1, "\ndevice-id: "

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v11, "id"

    invoke-interface {v0, v11}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v11

    invoke-virtual {v1, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v11, "\ndevice-inputs: "

    invoke-virtual {v1, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v11, "input"

    invoke-interface {v0, v11}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v11

    invoke-virtual {v1, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 415
    const-string/jumbo v1, "modes"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 417
    const-string/jumbo v1, "modes"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/Integer;

    check-cast v0, [Ljava/lang/Integer;

    .line 418
    array-length v11, v0

    move v1, v2

    :goto_4
    if-ge v1, v11, :cond_3

    aget-object v12, v0, v1

    .line 419
    invoke-virtual {v12}, Ljava/lang/Integer;->intValue()I

    move-result v13

    if-nez v13, :cond_5

    .line 420
    const-string/jumbo v12, "\nMode: Audio"

    invoke-virtual {v5, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 418
    :cond_4
    :goto_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 421
    :cond_5
    invoke-virtual {v12}, Ljava/lang/Integer;->intValue()I

    move-result v12

    const/4 v13, 0x1

    if-ne v12, v13, :cond_4

    .line 422
    const-string/jumbo v12, "\nMode: Control"

    invoke-virtual {v5, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_5

    .line 402
    :cond_6
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto/16 :goto_1

    .line 385
    :cond_7
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto/16 :goto_0

    .line 437
    :cond_8
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
