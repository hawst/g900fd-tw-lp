.class public Lcom/peel/control/au;
.super Lcom/peel/control/av;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 393
    invoke-direct {p0}, Lcom/peel/control/av;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/peel/control/RoomControl;)Z
    .locals 1

    .prologue
    .line 395
    invoke-virtual {p1}, Lcom/peel/control/RoomControl;->b()Lcom/peel/control/o;

    move-result-object v0

    .line 396
    if-eqz v0, :cond_0

    .line 398
    invoke-virtual {v0}, Lcom/peel/control/o;->h()Z

    .line 400
    const/4 v0, 0x0

    sput-object v0, Lcom/peel/control/b/l;->c:Lcom/peel/control/o;

    .line 403
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/peel/control/RoomControl;->b(I)V

    .line 412
    const/4 v0, 0x1

    return v0
.end method

.method public a(Lcom/peel/control/RoomControl;I)Z
    .locals 2

    .prologue
    .line 460
    invoke-static {p1}, Lcom/peel/control/RoomControl;->a(Lcom/peel/control/RoomControl;)Lcom/peel/control/a;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {p1}, Lcom/peel/control/RoomControl;->a(Lcom/peel/control/RoomControl;)Lcom/peel/control/a;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, p2, v1}, Lcom/peel/control/a;->a(I[Lcom/peel/control/h;)Z

    .line 461
    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method public a(Lcom/peel/control/RoomControl;Lcom/peel/control/a;I)Z
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 416
    if-nez p2, :cond_0

    .line 417
    invoke-static {}, Lcom/peel/control/RoomControl;->i()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "attempted to start null activity"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 456
    :goto_0
    return v0

    .line 421
    :cond_0
    invoke-static {p1}, Lcom/peel/control/RoomControl;->a(Lcom/peel/control/RoomControl;)Lcom/peel/control/a;

    move-result-object v1

    if-eqz v1, :cond_3

    const/4 v1, 0x2

    invoke-static {p1}, Lcom/peel/control/RoomControl;->a(Lcom/peel/control/RoomControl;)Lcom/peel/control/a;

    move-result-object v2

    invoke-virtual {v2}, Lcom/peel/control/a;->f()I

    move-result v2

    if-ne v1, v2, :cond_3

    .line 423
    invoke-static {p1}, Lcom/peel/control/RoomControl;->a(Lcom/peel/control/RoomControl;)Lcom/peel/control/a;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/control/a;->e()[Lcom/peel/control/h;

    move-result-object v1

    .line 424
    invoke-virtual {p2}, Lcom/peel/control/a;->e()[Lcom/peel/control/h;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    .line 425
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 426
    array-length v4, v1

    :goto_1
    if-ge v0, v4, :cond_2

    aget-object v5, v1, v0

    .line 427
    invoke-interface {v2, v5}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 428
    invoke-interface {v3, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 426
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 440
    :cond_2
    invoke-static {p1}, Lcom/peel/control/RoomControl;->a(Lcom/peel/control/RoomControl;)Lcom/peel/control/a;

    move-result-object v1

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Lcom/peel/control/h;

    invoke-interface {v3, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/peel/control/h;

    invoke-virtual {v1, p3, v0}, Lcom/peel/control/a;->a(I[Lcom/peel/control/h;)Z

    .line 443
    :cond_3
    invoke-static {p1, p2}, Lcom/peel/control/RoomControl;->a(Lcom/peel/control/RoomControl;Lcom/peel/control/a;)Lcom/peel/control/a;

    .line 446
    invoke-static {p1}, Lcom/peel/control/RoomControl;->a(Lcom/peel/control/RoomControl;)Lcom/peel/control/a;

    move-result-object v0

    invoke-virtual {v0, p3}, Lcom/peel/control/a;->b(I)Z

    .line 456
    const/4 v0, 0x1

    goto :goto_0
.end method
