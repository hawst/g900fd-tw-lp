.class public abstract Lcom/peel/control/av;
.super Ljava/lang/Object;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 339
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/peel/control/RoomControl;)Z
    .locals 2

    .prologue
    .line 346
    invoke-static {}, Lcom/peel/control/RoomControl;->i()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "stop not handled"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 347
    const/4 v0, 0x0

    return v0
.end method

.method public a(Lcom/peel/control/RoomControl;I)Z
    .locals 2

    .prologue
    .line 356
    invoke-static {}, Lcom/peel/control/RoomControl;->i()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "stopActivity not handled"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 357
    const/4 v0, 0x0

    return v0
.end method

.method public a(Lcom/peel/control/RoomControl;Lcom/peel/control/a;I)Z
    .locals 2

    .prologue
    .line 351
    invoke-static {}, Lcom/peel/control/RoomControl;->i()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "startActivity not handled"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 352
    const/4 v0, 0x0

    return v0
.end method

.method public b(Lcom/peel/control/RoomControl;)Z
    .locals 2

    .prologue
    .line 341
    invoke-static {}, Lcom/peel/control/RoomControl;->i()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "start not handled"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 342
    const/4 v0, 0x0

    return v0
.end method
