.class public Lcom/peel/control/a;
.super Ljava/lang/Object;


# static fields
.field private static c:I

.field private static final e:Ljava/lang/String;

.field private static final f:[Lcom/peel/control/f;


# instance fields
.field protected a:Lcom/peel/data/d;

.field private final b:Ljava/util/concurrent/atomic/AtomicInteger;

.field private final d:Lcom/peel/util/s;

.field private volatile g:Lcom/peel/control/f;

.field private final h:Ljava/util/concurrent/atomic/AtomicInteger;

.field private final i:Lcom/peel/control/c;

.field private j:Lcom/peel/control/h;

.field private k:Lcom/peel/control/h;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 40
    sput v3, Lcom/peel/control/a;->c:I

    .line 70
    const-class v0, Lcom/peel/control/a;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/peel/control/a;->e:Ljava/lang/String;

    .line 74
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/peel/control/f;

    const/4 v1, 0x0

    new-instance v2, Lcom/peel/control/g;

    invoke-direct {v2}, Lcom/peel/control/g;-><init>()V

    aput-object v2, v0, v1

    new-instance v1, Lcom/peel/control/d;

    invoke-direct {v1}, Lcom/peel/control/d;-><init>()V

    aput-object v1, v0, v3

    const/4 v1, 0x2

    new-instance v2, Lcom/peel/control/e;

    invoke-direct {v2}, Lcom/peel/control/e;-><init>()V

    aput-object v2, v0, v1

    sput-object v0, Lcom/peel/control/a;->f:[Lcom/peel/control/f;

    return-void
.end method

.method protected constructor <init>(Lcom/peel/data/d;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 82
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0, v2}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    iput-object v0, p0, Lcom/peel/control/a;->b:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 41
    new-instance v0, Lcom/peel/control/b;

    invoke-direct {v0, p0}, Lcom/peel/control/b;-><init>(Lcom/peel/control/a;)V

    iput-object v0, p0, Lcom/peel/control/a;->d:Lcom/peel/util/s;

    .line 75
    sget-object v0, Lcom/peel/control/a;->f:[Lcom/peel/control/f;

    iget-object v1, p0, Lcom/peel/control/a;->b:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v1

    aget-object v0, v0, v1

    iput-object v0, p0, Lcom/peel/control/a;->g:Lcom/peel/control/f;

    .line 76
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0, v2}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    iput-object v0, p0, Lcom/peel/control/a;->h:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 77
    new-instance v0, Lcom/peel/control/c;

    invoke-direct {v0}, Lcom/peel/control/c;-><init>()V

    iput-object v0, p0, Lcom/peel/control/a;->i:Lcom/peel/control/c;

    .line 79
    iput-object v3, p0, Lcom/peel/control/a;->j:Lcom/peel/control/h;

    .line 80
    iput-object v3, p0, Lcom/peel/control/a;->k:Lcom/peel/control/h;

    .line 82
    iput-object p1, p0, Lcom/peel/control/a;->a:Lcom/peel/data/d;

    return-void
.end method

.method protected constructor <init>(Ljava/lang/String;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 89
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0, v2}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    iput-object v0, p0, Lcom/peel/control/a;->b:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 41
    new-instance v0, Lcom/peel/control/b;

    invoke-direct {v0, p0}, Lcom/peel/control/b;-><init>(Lcom/peel/control/a;)V

    iput-object v0, p0, Lcom/peel/control/a;->d:Lcom/peel/util/s;

    .line 75
    sget-object v0, Lcom/peel/control/a;->f:[Lcom/peel/control/f;

    iget-object v1, p0, Lcom/peel/control/a;->b:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v1

    aget-object v0, v0, v1

    iput-object v0, p0, Lcom/peel/control/a;->g:Lcom/peel/control/f;

    .line 76
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0, v2}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    iput-object v0, p0, Lcom/peel/control/a;->h:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 77
    new-instance v0, Lcom/peel/control/c;

    invoke-direct {v0}, Lcom/peel/control/c;-><init>()V

    iput-object v0, p0, Lcom/peel/control/a;->i:Lcom/peel/control/c;

    .line 79
    iput-object v3, p0, Lcom/peel/control/a;->j:Lcom/peel/control/h;

    .line 80
    iput-object v3, p0, Lcom/peel/control/a;->k:Lcom/peel/control/h;

    .line 89
    new-instance v0, Lcom/peel/data/d;

    invoke-direct {v0, p1}, Lcom/peel/data/d;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/peel/control/a;->a:Lcom/peel/data/d;

    return-void
.end method

.method protected constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 84
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0, v2}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    iput-object v0, p0, Lcom/peel/control/a;->b:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 41
    new-instance v0, Lcom/peel/control/b;

    invoke-direct {v0, p0}, Lcom/peel/control/b;-><init>(Lcom/peel/control/a;)V

    iput-object v0, p0, Lcom/peel/control/a;->d:Lcom/peel/util/s;

    .line 75
    sget-object v0, Lcom/peel/control/a;->f:[Lcom/peel/control/f;

    iget-object v1, p0, Lcom/peel/control/a;->b:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v1

    aget-object v0, v0, v1

    iput-object v0, p0, Lcom/peel/control/a;->g:Lcom/peel/control/f;

    .line 76
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0, v2}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    iput-object v0, p0, Lcom/peel/control/a;->h:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 77
    new-instance v0, Lcom/peel/control/c;

    invoke-direct {v0}, Lcom/peel/control/c;-><init>()V

    iput-object v0, p0, Lcom/peel/control/a;->i:Lcom/peel/control/c;

    .line 79
    iput-object v3, p0, Lcom/peel/control/a;->j:Lcom/peel/control/h;

    .line 80
    iput-object v3, p0, Lcom/peel/control/a;->k:Lcom/peel/control/h;

    .line 85
    new-instance v0, Lcom/peel/data/d;

    invoke-direct {v0, p2, p1}, Lcom/peel/data/d;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/peel/control/a;->a:Lcom/peel/data/d;

    .line 87
    return-void
.end method

.method public static a(Lcom/peel/data/d;)Lcom/peel/control/a;
    .locals 10

    .prologue
    .line 100
    new-instance v4, Lcom/peel/control/a/a;

    invoke-direct {v4, p0}, Lcom/peel/control/a/a;-><init>(Lcom/peel/data/d;)V

    .line 102
    invoke-virtual {p0}, Lcom/peel/data/d;->c()Ljava/lang/Iterable;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    .line 103
    const-string/jumbo v1, "modes"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Ljava/lang/Integer;

    check-cast v1, [Ljava/lang/Integer;

    .line 104
    if-eqz v1, :cond_2

    .line 105
    array-length v6, v1

    const/4 v2, 0x0

    move v3, v2

    :goto_1
    if-ge v3, v6, :cond_2

    aget-object v2, v1, v3

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 106
    packed-switch v2, :pswitch_data_0

    .line 108
    sget-object v7, Lcom/peel/control/a;->e:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "unrecognized mode in device "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v7, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 105
    :goto_2
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_1

    .line 112
    :pswitch_0
    iget-object v2, v4, Lcom/peel/control/a;->j:Lcom/peel/control/h;

    if-eqz v2, :cond_0

    .line 113
    sget-object v2, Lcom/peel/control/a;->e:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "overriding audio device "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, v4, Lcom/peel/control/a;->j:Lcom/peel/control/h;

    invoke-virtual {v8}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v8

    invoke-virtual {v8}, Lcom/peel/data/g;->b()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, " in activity "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v4}, Lcom/peel/control/a;->a()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v2, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 114
    :cond_0
    sget-object v7, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    const-string/jumbo v2, "id"

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v7, v2}, Lcom/peel/control/am;->c(Ljava/lang/String;)Lcom/peel/control/h;

    move-result-object v2

    iput-object v2, v4, Lcom/peel/control/a;->j:Lcom/peel/control/h;

    goto :goto_2

    .line 117
    :pswitch_1
    iget-object v2, v4, Lcom/peel/control/a;->k:Lcom/peel/control/h;

    if-eqz v2, :cond_1

    .line 118
    sget-object v2, Lcom/peel/control/a;->e:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "overriding control device "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, v4, Lcom/peel/control/a;->k:Lcom/peel/control/h;

    invoke-virtual {v8}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v8

    invoke-virtual {v8}, Lcom/peel/data/g;->b()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, " in activity "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v4}, Lcom/peel/control/a;->a()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v2, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 119
    :cond_1
    sget-object v7, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    const-string/jumbo v2, "id"

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v7, v2}, Lcom/peel/control/am;->c(Ljava/lang/String;)Lcom/peel/control/h;

    move-result-object v2

    iput-object v2, v4, Lcom/peel/control/a;->k:Lcom/peel/control/h;

    goto/16 :goto_2

    .line 125
    :cond_2
    sget-object v1, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    const-string/jumbo v2, "id"

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Lcom/peel/control/am;->c(Ljava/lang/String;)Lcom/peel/control/h;

    move-result-object v0

    .line 126
    iget-object v1, v4, Lcom/peel/control/a;->d:Lcom/peel/util/s;

    invoke-virtual {v0, v1}, Lcom/peel/control/h;->a(Lcom/peel/util/s;)V

    goto/16 :goto_0

    .line 128
    :cond_3
    return-object v4

    .line 106
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static a(Ljava/lang/String;)Lcom/peel/control/a;
    .locals 1

    .prologue
    .line 92
    new-instance v0, Lcom/peel/control/a/a;

    invoke-direct {v0, p0}, Lcom/peel/control/a/a;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;)Lcom/peel/control/a;
    .locals 1

    .prologue
    .line 96
    new-instance v0, Lcom/peel/control/a/a;

    invoke-direct {v0, p0, p1}, Lcom/peel/control/a/a;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method static synthetic a(Lcom/peel/control/a;)Ljava/util/concurrent/atomic/AtomicInteger;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lcom/peel/control/a;->h:Ljava/util/concurrent/atomic/AtomicInteger;

    return-object v0
.end method

.method static synthetic b(Lcom/peel/control/a;)Lcom/peel/control/c;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lcom/peel/control/a;->i:Lcom/peel/control/c;

    return-object v0
.end method

.method static synthetic c(Lcom/peel/control/a;)Lcom/peel/control/h;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lcom/peel/control/a;->j:Lcom/peel/control/h;

    return-object v0
.end method

.method static synthetic d(I)I
    .locals 0

    .prologue
    .line 23
    sput p0, Lcom/peel/control/a;->c:I

    return p0
.end method

.method static synthetic d(Lcom/peel/control/a;)Lcom/peel/control/h;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lcom/peel/control/a;->k:Lcom/peel/control/h;

    return-object v0
.end method

.method static synthetic g()I
    .locals 1

    .prologue
    .line 23
    sget v0, Lcom/peel/control/a;->c:I

    return v0
.end method

.method static synthetic h()Ljava/lang/String;
    .locals 1

    .prologue
    .line 23
    sget-object v0, Lcom/peel/control/a;->e:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public a(I)Lcom/peel/control/h;
    .locals 3

    .prologue
    .line 268
    const/4 v0, 0x1

    if-ne v0, p1, :cond_0

    .line 269
    iget-object v0, p0, Lcom/peel/control/a;->k:Lcom/peel/control/h;

    .line 272
    :goto_0
    return-object v0

    .line 271
    :cond_0
    if-nez p1, :cond_1

    .line 272
    iget-object v0, p0, Lcom/peel/control/a;->j:Lcom/peel/control/h;

    goto :goto_0

    .line 274
    :cond_1
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "unknown device type requested: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 133
    iget-object v0, p0, Lcom/peel/control/a;->a:Lcom/peel/data/d;

    invoke-virtual {v0}, Lcom/peel/data/d;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a(Lcom/peel/control/h;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 250
    iget-object v0, p0, Lcom/peel/control/a;->a:Lcom/peel/data/d;

    invoke-virtual {v0}, Lcom/peel/data/d;->c()Ljava/lang/Iterable;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    .line 251
    const-string/jumbo v2, "id"

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {p1}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v3

    invoke-virtual {v3}, Lcom/peel/data/g;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 252
    const-string/jumbo v1, "input"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 255
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Lcom/peel/util/s;)V
    .locals 1

    .prologue
    .line 392
    iget-object v0, p0, Lcom/peel/control/a;->i:Lcom/peel/control/c;

    invoke-virtual {v0, p1}, Lcom/peel/control/c;->a(Ljava/lang/Object;)V

    return-void
.end method

.method public a(I[Lcom/peel/control/h;)Z
    .locals 1

    .prologue
    .line 360
    iget-object v0, p0, Lcom/peel/control/a;->g:Lcom/peel/control/f;

    invoke-virtual {v0, p0, p1, p2}, Lcom/peel/control/f;->a(Lcom/peel/control/a;I[Lcom/peel/control/h;)Z

    move-result v0

    return v0
.end method

.method public a(Lcom/peel/control/h;Ljava/lang/String;[Ljava/lang/Integer;)Z
    .locals 12

    .prologue
    .line 178
    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {p1}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/data/g;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/peel/control/am;->c(Ljava/lang/String;)Lcom/peel/control/h;

    move-result-object v0

    if-nez v0, :cond_2

    .line 180
    const/4 v0, 0x0

    .line 181
    sget-object v1, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v1}, Lcom/peel/control/am;->f()Ljava/util/List;

    move-result-object v1

    .line 182
    if-eqz v1, :cond_1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_1

    .line 183
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/control/h;

    .line 184
    invoke-virtual {v0}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v3

    invoke-virtual {v3}, Lcom/peel/data/g;->c()I

    move-result v3

    if-le v3, v1, :cond_7

    .line 185
    invoke-virtual {v0}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/data/g;->c()I

    move-result v0

    :goto_1
    move v1, v0

    .line 187
    goto :goto_0

    :cond_0
    move v0, v1

    .line 190
    :cond_1
    sget-object v1, Lcom/peel/control/a;->e:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "\n\n new device int id from Activity: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    add-int/lit8 v3, v0, 0x1

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 191
    invoke-virtual {p1}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v1

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {v1, v0}, Lcom/peel/data/g;->a(I)V

    .line 193
    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v0, p1}, Lcom/peel/control/am;->a(Lcom/peel/control/h;)V

    .line 196
    :cond_2
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    .line 197
    if-eqz p3, :cond_5

    .line 198
    array-length v1, p3

    const/4 v0, 0x0

    :goto_2
    if-ge v0, v1, :cond_5

    aget-object v2, p3, v0

    .line 199
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v3

    if-nez v3, :cond_3

    .line 200
    iput-object p1, p0, Lcom/peel/control/a;->j:Lcom/peel/control/h;

    .line 201
    const-string/jumbo v3, "MODE_AUDIO,"

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 203
    :cond_3
    const/4 v3, 0x1

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-ne v3, v2, :cond_4

    .line 204
    iput-object p1, p0, Lcom/peel/control/a;->k:Lcom/peel/control/h;

    .line 205
    const-string/jumbo v2, "MODE_CONTROL,"

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 198
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 210
    :cond_5
    iget-object v0, p0, Lcom/peel/control/a;->a:Lcom/peel/data/d;

    invoke-virtual {p1}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v1

    invoke-virtual {v0, v1, p2, p3}, Lcom/peel/data/d;->a(Lcom/peel/data/g;Ljava/lang/String;[Ljava/lang/Integer;)Z

    .line 211
    iget-object v0, p0, Lcom/peel/control/a;->d:Lcom/peel/util/s;

    invoke-virtual {p1, v0}, Lcom/peel/control/h;->a(Lcom/peel/util/s;)V

    .line 222
    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v0}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 224
    :try_start_0
    invoke-static {}, Lcom/peel/util/a/f;->a()Lcom/peel/util/a/f;

    move-result-object v0

    sget-object v1, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v1}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/data/at;->f()I

    move-result v1

    const/16 v2, 0x139c

    const/16 v3, 0x7e5

    iget-object v4, p0, Lcom/peel/control/a;->a:Lcom/peel/data/d;

    .line 225
    invoke-virtual {v4}, Lcom/peel/data/d;->a()Ljava/lang/String;

    move-result-object v4

    .line 226
    invoke-virtual {p1}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v5

    invoke-virtual {v5}, Lcom/peel/data/g;->e()I

    move-result v5

    invoke-virtual {p1}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v6

    invoke-virtual {v6}, Lcom/peel/data/g;->f()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v7

    invoke-virtual {v7}, Lcom/peel/data/g;->d()I

    move-result v7

    .line 227
    invoke-virtual {p1}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v9

    invoke-virtual {v9}, Lcom/peel/data/g;->h()I

    move-result v9

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    const/4 v11, -0x1

    move-object v8, p2

    .line 224
    invoke-virtual/range {v0 .. v11}, Lcom/peel/util/a/f;->a(IIILjava/lang/String;ILjava/lang/String;ILjava/lang/String;ILjava/lang/String;I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 233
    :cond_6
    :goto_3
    const/4 v0, 0x1

    return v0

    .line 228
    :catch_0
    move-exception v0

    .line 229
    sget-object v1, Lcom/peel/control/a;->e:Ljava/lang/String;

    sget-object v2, Lcom/peel/control/a;->e:Ljava/lang/String;

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_3

    :cond_7
    move v0, v1

    goto/16 :goto_1
.end method

.method public a(Ljava/net/URI;)Z
    .locals 1

    .prologue
    .line 358
    iget-object v0, p0, Lcom/peel/control/a;->g:Lcom/peel/control/f;

    invoke-virtual {v0, p0, p1}, Lcom/peel/control/f;->a(Lcom/peel/control/a;Ljava/net/URI;)Z

    move-result v0

    return v0
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 135
    iget-object v0, p0, Lcom/peel/control/a;->a:Lcom/peel/data/d;

    invoke-virtual {v0}, Lcom/peel/data/d;->b()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public b(Lcom/peel/control/h;Ljava/lang/String;[Ljava/lang/Integer;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 237
    if-eqz p3, :cond_2

    .line 238
    array-length v1, p3

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_4

    aget-object v2, p3, v0

    .line 239
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v3

    if-nez v3, :cond_0

    iput-object p1, p0, Lcom/peel/control/a;->j:Lcom/peel/control/h;

    .line 240
    :cond_0
    const/4 v3, 0x1

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-ne v3, v2, :cond_1

    iput-object p1, p0, Lcom/peel/control/a;->k:Lcom/peel/control/h;

    .line 238
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 243
    :cond_2
    iget-object v0, p0, Lcom/peel/control/a;->j:Lcom/peel/control/h;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    iput-object v1, p0, Lcom/peel/control/a;->j:Lcom/peel/control/h;

    .line 244
    :cond_3
    iget-object v0, p0, Lcom/peel/control/a;->k:Lcom/peel/control/h;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    iput-object v1, p0, Lcom/peel/control/a;->k:Lcom/peel/control/h;

    .line 246
    :cond_4
    iget-object v0, p0, Lcom/peel/control/a;->a:Lcom/peel/data/d;

    invoke-virtual {p1}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v1

    invoke-virtual {v0, v1, p2, p3}, Lcom/peel/data/d;->b(Lcom/peel/data/g;Ljava/lang/String;[Ljava/lang/Integer;)V

    .line 247
    return-void
.end method

.method public b(Lcom/peel/util/s;)V
    .locals 1

    .prologue
    .line 394
    iget-object v0, p0, Lcom/peel/control/a;->i:Lcom/peel/control/c;

    invoke-virtual {v0, p1}, Lcom/peel/control/c;->b(Ljava/lang/Object;)V

    return-void
.end method

.method public b(I)Z
    .locals 1

    .prologue
    .line 354
    iget-object v0, p0, Lcom/peel/control/a;->g:Lcom/peel/control/f;

    invoke-virtual {v0, p0, p1}, Lcom/peel/control/f;->a(Lcom/peel/control/a;I)Z

    move-result v0

    return v0
.end method

.method public b(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 356
    iget-object v0, p0, Lcom/peel/control/a;->g:Lcom/peel/control/f;

    invoke-virtual {v0, p0, p1}, Lcom/peel/control/f;->a(Lcom/peel/control/a;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public b(Lcom/peel/control/h;)[Ljava/lang/Integer;
    .locals 4

    .prologue
    .line 259
    iget-object v0, p0, Lcom/peel/control/a;->a:Lcom/peel/data/d;

    invoke-virtual {v0}, Lcom/peel/data/d;->c()Ljava/lang/Iterable;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    .line 260
    const-string/jumbo v2, "id"

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {p1}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v3

    invoke-virtual {v3}, Lcom/peel/data/g;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 261
    const-string/jumbo v1, "modes"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/Integer;

    check-cast v0, [Ljava/lang/Integer;

    .line 264
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c()Lcom/peel/data/d;
    .locals 1

    .prologue
    .line 137
    iget-object v0, p0, Lcom/peel/control/a;->a:Lcom/peel/data/d;

    return-object v0
.end method

.method public declared-synchronized c(I)V
    .locals 3

    .prologue
    .line 367
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/peel/control/a;->b:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V

    .line 368
    sget-object v0, Lcom/peel/control/a;->f:[Lcom/peel/control/f;

    iget-object v1, p0, Lcom/peel/control/a;->b:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v1

    aget-object v0, v0, v1

    iput-object v0, p0, Lcom/peel/control/a;->g:Lcom/peel/control/f;

    .line 370
    packed-switch p1, :pswitch_data_0

    .line 381
    sget-object v0, Lcom/peel/control/a;->e:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "illegal state requested "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 390
    :goto_0
    monitor-exit p0

    return-void

    .line 372
    :pswitch_0
    :try_start_1
    sget-object v1, Lcom/peel/control/am;->a:Lcom/peel/control/aq;

    const/16 v2, 0x16

    const/4 v0, 0x0

    check-cast v0, [Ljava/lang/Object;

    invoke-virtual {v1, v2, p0, v0}, Lcom/peel/control/aq;->a(ILjava/lang/Object;[Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 367
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 375
    :pswitch_1
    :try_start_2
    sget-object v1, Lcom/peel/control/am;->a:Lcom/peel/control/aq;

    const/16 v2, 0x14

    const/4 v0, 0x0

    check-cast v0, [Ljava/lang/Object;

    invoke-virtual {v1, v2, p0, v0}, Lcom/peel/control/aq;->a(ILjava/lang/Object;[Ljava/lang/Object;)V

    goto :goto_0

    .line 378
    :pswitch_2
    sget-object v1, Lcom/peel/control/am;->a:Lcom/peel/control/aq;

    const/16 v2, 0x15

    const/4 v0, 0x0

    check-cast v0, [Ljava/lang/Object;

    invoke-virtual {v1, v2, p0, v0}, Lcom/peel/control/aq;->a(ILjava/lang/Object;[Ljava/lang/Object;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 370
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public c(Lcom/peel/control/h;)V
    .locals 10

    .prologue
    const/4 v5, 0x0

    const/16 v6, 0xc

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 286
    invoke-virtual {p0}, Lcom/peel/control/a;->e()[Lcom/peel/control/h;

    move-result-object v0

    .line 287
    if-eqz v0, :cond_0

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 343
    :cond_0
    :goto_0
    return-void

    .line 290
    :cond_1
    iget-object v1, p0, Lcom/peel/control/a;->k:Lcom/peel/control/h;

    invoke-virtual {p1, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 292
    iput-object v5, p0, Lcom/peel/control/a;->k:Lcom/peel/control/h;

    move v1, v2

    .line 295
    :goto_1
    iget-object v4, p0, Lcom/peel/control/a;->j:Lcom/peel/control/h;

    invoke-virtual {p1, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    iput-object v5, p0, Lcom/peel/control/a;->j:Lcom/peel/control/h;

    .line 324
    :cond_2
    if-eqz v1, :cond_4

    .line 325
    array-length v1, v0

    :goto_2
    if-ge v3, v1, :cond_3

    aget-object v2, v0, v3

    .line 326
    iget-object v4, p0, Lcom/peel/control/a;->a:Lcom/peel/data/d;

    invoke-virtual {v2}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/peel/data/d;->a(Lcom/peel/data/g;)V

    .line 327
    iget-object v4, p0, Lcom/peel/control/a;->d:Lcom/peel/util/s;

    invoke-virtual {v2, v4}, Lcom/peel/control/h;->b(Lcom/peel/util/s;)V

    .line 325
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 329
    :cond_3
    iget-object v1, p0, Lcom/peel/control/a;->i:Lcom/peel/control/c;

    check-cast v0, [Ljava/lang/Object;

    invoke-virtual {v1, v6, p0, v0}, Lcom/peel/control/c;->a(ILjava/lang/Object;[Ljava/lang/Object;)V

    .line 341
    :goto_3
    invoke-static {}, Lcom/peel/util/a/f;->a()Lcom/peel/util/a/f;

    move-result-object v0

    sget-object v1, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v1}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/data/at;->f()I

    move-result v1

    const/16 v2, 0x139e

    const/16 v3, 0x7e5

    iget-object v4, p0, Lcom/peel/control/a;->a:Lcom/peel/data/d;

    invoke-virtual {v4}, Lcom/peel/data/d;->a()Ljava/lang/String;

    move-result-object v4

    const/4 v5, -0x1

    invoke-virtual {p1}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v6

    invoke-virtual {v6}, Lcom/peel/data/g;->f()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v7

    invoke-virtual {v7}, Lcom/peel/data/g;->d()I

    move-result v7

    const-string/jumbo v8, ""

    invoke-virtual {p1}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v9

    invoke-virtual {v9}, Lcom/peel/data/g;->e()I

    move-result v9

    invoke-virtual/range {v0 .. v9}, Lcom/peel/util/a/f;->a(IIILjava/lang/String;ILjava/lang/String;ILjava/lang/String;I)V

    goto :goto_0

    .line 331
    :cond_4
    iget-object v0, p0, Lcom/peel/control/a;->a:Lcom/peel/data/d;

    invoke-virtual {p1}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/peel/data/d;->a(Lcom/peel/data/g;)V

    .line 332
    iget-object v0, p0, Lcom/peel/control/a;->d:Lcom/peel/util/s;

    invoke-virtual {p1, v0}, Lcom/peel/control/h;->b(Lcom/peel/util/s;)V

    .line 333
    iget-object v0, p0, Lcom/peel/control/a;->i:Lcom/peel/control/c;

    new-array v1, v2, [Ljava/lang/Object;

    aput-object p1, v1, v3

    invoke-virtual {v0, v6, p0, v1}, Lcom/peel/control/c;->a(ILjava/lang/Object;[Ljava/lang/Object;)V

    goto :goto_3

    :cond_5
    move v1, v3

    goto :goto_1
.end method

.method public d()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 140
    iget-object v0, p0, Lcom/peel/control/a;->k:Lcom/peel/control/h;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/peel/control/a;->k:Lcom/peel/control/h;

    invoke-virtual {v0}, Lcom/peel/control/h;->c()[Ljava/lang/String;

    move-result-object v0

    .line 141
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public e()[Lcom/peel/control/h;
    .locals 6

    .prologue
    .line 145
    new-instance v2, Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/peel/control/a;->a:Lcom/peel/data/d;

    invoke-virtual {v0}, Lcom/peel/data/d;->d()I

    move-result v0

    invoke-direct {v2, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 146
    const/4 v0, 0x0

    .line 147
    iget-object v1, p0, Lcom/peel/control/a;->a:Lcom/peel/data/d;

    invoke-virtual {v1}, Lcom/peel/data/d;->c()Ljava/lang/Iterable;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v0

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    .line 149
    :try_start_0
    sget-object v4, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    const-string/jumbo v5, "id"

    invoke-interface {v0, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v4, v0}, Lcom/peel/control/am;->c(Ljava/lang/String;)Lcom/peel/control/h;

    move-result-object v0

    .line 152
    invoke-virtual {v0}, Lcom/peel/control/h;->d()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/peel/control/a;->k:Lcom/peel/control/h;

    invoke-virtual {v5}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v5

    invoke-virtual {v5}, Lcom/peel/data/g;->b()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 153
    invoke-interface {v2, v1, v0}, Ljava/util/List;->add(ILjava/lang/Object;)V

    :goto_1
    move v0, v1

    :goto_2
    move v1, v0

    .line 164
    goto :goto_0

    .line 154
    :cond_0
    const/4 v4, 0x1

    invoke-virtual {v0}, Lcom/peel/control/h;->e()I

    move-result v5

    if-eq v4, v5, :cond_1

    const/16 v4, 0xa

    .line 155
    invoke-virtual {v0}, Lcom/peel/control/h;->e()I

    move-result v5

    if-ne v4, v5, :cond_2

    .line 156
    :cond_1
    invoke-interface {v2, v1, v0}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 157
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 159
    :cond_2
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 161
    :catch_0
    move-exception v0

    .line 162
    sget-object v4, Lcom/peel/control/a;->e:Ljava/lang/String;

    sget-object v5, Lcom/peel/control/a;->e:Ljava/lang/String;

    invoke-static {v4, v5, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move v0, v1

    goto :goto_2

    .line 166
    :cond_3
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Lcom/peel/control/h;

    invoke-interface {v2, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/peel/control/h;

    return-object v0
.end method

.method public declared-synchronized f()I
    .locals 1

    .prologue
    .line 362
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/peel/control/a;->b:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
