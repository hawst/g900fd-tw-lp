.class Lcom/peel/control/b;
.super Lcom/peel/util/s;


# instance fields
.field final synthetic a:Lcom/peel/control/a;


# direct methods
.method constructor <init>(Lcom/peel/control/a;)V
    .locals 0

    .prologue
    .line 41
    iput-object p1, p0, Lcom/peel/control/b;->a:Lcom/peel/control/a;

    invoke-direct {p0}, Lcom/peel/util/s;-><init>()V

    return-void
.end method


# virtual methods
.method public final varargs a(ILjava/lang/Object;[Ljava/lang/Object;)V
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 44
    sparse-switch p1, :sswitch_data_0

    .line 67
    :cond_0
    :goto_0
    return-void

    .line 46
    :sswitch_0
    iget-object v0, p0, Lcom/peel/control/b;->a:Lcom/peel/control/a;

    invoke-static {v0}, Lcom/peel/control/a;->a(Lcom/peel/control/a;)Ljava/util/concurrent/atomic/AtomicInteger;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->decrementAndGet()I

    move-result v0

    .line 47
    if-nez v0, :cond_0

    .line 48
    iget-object v0, p0, Lcom/peel/control/b;->a:Lcom/peel/control/a;

    invoke-virtual {v0, v3}, Lcom/peel/control/a;->c(I)V

    .line 49
    iget-object v0, p0, Lcom/peel/control/b;->a:Lcom/peel/control/a;

    invoke-static {}, Lcom/peel/control/a;->g()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/peel/control/a;->b(I)Z

    goto :goto_0

    .line 53
    :sswitch_1
    iget-object v0, p0, Lcom/peel/control/b;->a:Lcom/peel/control/a;

    invoke-static {v0}, Lcom/peel/control/a;->b(Lcom/peel/control/a;)Lcom/peel/control/c;

    move-result-object v0

    const/16 v1, 0xa

    iget-object v2, p0, Lcom/peel/control/b;->a:Lcom/peel/control/a;

    new-array v3, v3, [Ljava/lang/Object;

    aput-object p2, v3, v4

    invoke-virtual {v0, v1, v2, v3}, Lcom/peel/control/c;->a(ILjava/lang/Object;[Ljava/lang/Object;)V

    goto :goto_0

    .line 63
    :sswitch_2
    iget-object v0, p0, Lcom/peel/control/b;->a:Lcom/peel/control/a;

    invoke-static {v0}, Lcom/peel/control/a;->a(Lcom/peel/control/a;)Ljava/util/concurrent/atomic/AtomicInteger;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V

    .line 64
    iget-object v0, p0, Lcom/peel/control/b;->a:Lcom/peel/control/a;

    invoke-static {v0}, Lcom/peel/control/a;->b(Lcom/peel/control/a;)Lcom/peel/control/c;

    move-result-object v0

    const/16 v1, 0xb

    iget-object v2, p0, Lcom/peel/control/b;->a:Lcom/peel/control/a;

    new-array v3, v3, [Ljava/lang/Object;

    aput-object p2, v3, v4

    invoke-virtual {v0, v1, v2, v3}, Lcom/peel/control/c;->a(ILjava/lang/Object;[Ljava/lang/Object;)V

    goto :goto_0

    .line 44
    :sswitch_data_0
    .sparse-switch
        0x16 -> :sswitch_0
        0x19 -> :sswitch_1
        0x1d -> :sswitch_2
    .end sparse-switch
.end method
