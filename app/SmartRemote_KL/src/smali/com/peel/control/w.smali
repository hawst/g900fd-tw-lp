.class Lcom/peel/control/w;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/peel/control/u;

.field private b:Z


# direct methods
.method constructor <init>(Lcom/peel/control/u;)V
    .locals 1

    .prologue
    .line 82
    iput-object p1, p0, Lcom/peel/control/w;->a:Lcom/peel/control/u;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 83
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/peel/control/w;->b:Z

    return-void
.end method


# virtual methods
.method public run()V
    .locals 12

    .prologue
    const-wide/16 v10, 0x0

    const/4 v4, 0x1

    const/high16 v2, 0x3f800000    # 1.0f

    const/4 v5, 0x0

    .line 86
    invoke-static {}, Lcom/peel/control/u;->b()Ljava/util/concurrent/atomic/AtomicBoolean;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 90
    iget-object v0, p0, Lcom/peel/control/w;->a:Lcom/peel/control/u;

    invoke-static {v0}, Lcom/peel/control/u;->e(Lcom/peel/control/u;)Ljava/util/concurrent/ConcurrentLinkedQueue;

    move-result-object v3

    monitor-enter v3

    .line 91
    :try_start_0
    iget-object v0, p0, Lcom/peel/control/w;->a:Lcom/peel/control/u;

    invoke-static {v0}, Lcom/peel/control/u;->e(Lcom/peel/control/u;)Ljava/util/concurrent/ConcurrentLinkedQueue;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 92
    invoke-static {}, Lcom/peel/control/u;->b()Ljava/util/concurrent/atomic/AtomicBoolean;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 95
    iget-object v0, p0, Lcom/peel/control/w;->a:Lcom/peel/control/u;

    invoke-static {v0}, Lcom/peel/control/u;->g(Lcom/peel/control/u;)Landroid/media/AudioManager;

    move-result-object v0

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/peel/control/w;->a:Lcom/peel/control/u;

    invoke-static {v2}, Lcom/peel/control/u;->f(Lcom/peel/control/u;)I

    move-result v2

    const/4 v4, 0x0

    invoke-virtual {v0, v1, v2, v4}, Landroid/media/AudioManager;->setStreamVolume(III)V

    .line 96
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/peel/control/w;->b:Z

    .line 97
    monitor-exit v3

    .line 130
    :goto_0
    return-void

    .line 100
    :cond_0
    iget-object v0, p0, Lcom/peel/control/w;->a:Lcom/peel/control/u;

    invoke-static {v0}, Lcom/peel/control/u;->e(Lcom/peel/control/u;)Ljava/util/concurrent/ConcurrentLinkedQueue;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 102
    iget-boolean v0, p0, Lcom/peel/control/w;->b:Z

    if-nez v0, :cond_1

    .line 104
    iget-object v0, p0, Lcom/peel/control/w;->a:Lcom/peel/control/u;

    invoke-static {v0}, Lcom/peel/control/u;->h(Lcom/peel/control/u;)Landroid/util/SparseArray;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 105
    iget-object v6, p0, Lcom/peel/control/w;->a:Lcom/peel/control/u;

    iget-object v7, p0, Lcom/peel/control/w;->a:Lcom/peel/control/u;

    invoke-static {v7}, Lcom/peel/control/u;->g(Lcom/peel/control/u;)Landroid/media/AudioManager;

    move-result-object v7

    const/4 v8, 0x3

    invoke-virtual {v7, v8}, Landroid/media/AudioManager;->getStreamVolume(I)I

    move-result v7

    invoke-static {v6, v7}, Lcom/peel/control/u;->a(Lcom/peel/control/u;I)I

    .line 106
    iget-object v6, p0, Lcom/peel/control/w;->a:Lcom/peel/control/u;

    invoke-static {v6}, Lcom/peel/control/u;->g(Lcom/peel/control/u;)Landroid/media/AudioManager;

    move-result-object v6

    const/4 v7, 0x3

    iget-object v8, p0, Lcom/peel/control/w;->a:Lcom/peel/control/u;

    invoke-static {v8}, Lcom/peel/control/u;->g(Lcom/peel/control/u;)Landroid/media/AudioManager;

    move-result-object v8

    const/4 v9, 0x3

    invoke-virtual {v8, v9}, Landroid/media/AudioManager;->getStreamMaxVolume(I)I

    move-result v8

    int-to-float v8, v8

    int-to-float v0, v0

    const/high16 v9, 0x42c80000    # 100.0f

    div-float/2addr v0, v9

    mul-float/2addr v0, v8

    float-to-int v0, v0

    const/4 v8, 0x0

    invoke-virtual {v6, v7, v0, v8}, Landroid/media/AudioManager;->setStreamVolume(III)V

    .line 107
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/peel/control/w;->b:Z

    .line 110
    :cond_1
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 112
    iget-object v0, p0, Lcom/peel/control/w;->a:Lcom/peel/control/u;

    invoke-static {v0}, Lcom/peel/control/u;->b(Lcom/peel/control/u;)Landroid/util/SparseArray;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/peel/control/y;

    .line 113
    iget-wide v8, v7, Lcom/peel/control/y;->c:J

    .line 117
    iget-object v0, p0, Lcom/peel/control/w;->a:Lcom/peel/control/u;

    invoke-static {v0}, Lcom/peel/control/u;->b(Lcom/peel/control/u;)Landroid/util/SparseArray;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/control/y;

    iget-object v0, v0, Lcom/peel/control/y;->a:Ljava/lang/String;

    const-string/jumbo v3, "10000"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 118
    iget-object v0, p0, Lcom/peel/control/w;->a:Lcom/peel/control/u;

    invoke-static {v0}, Lcom/peel/control/u;->i(Lcom/peel/control/u;)Landroid/media/SoundPool;

    move-result-object v0

    move v3, v2

    move v6, v2

    invoke-virtual/range {v0 .. v6}, Landroid/media/SoundPool;->play(IFFIIF)I

    .line 122
    invoke-static {}, Lcom/peel/control/u;->c()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "sending ir "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v3, v7, Lcom/peel/control/y;->a:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v3, "("

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 127
    :goto_1
    invoke-static {}, Lcom/peel/control/u;->c()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "next send "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    add-long v2, v8, v10

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "ms"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 129
    invoke-static {}, Lcom/peel/control/u;->c()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "repeated sendIr runnable"

    add-long v2, v8, v10

    invoke-static {v0, v1, p0, v2, v3}, Lcom/peel/util/i;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;J)V

    goto/16 :goto_0

    .line 110
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 124
    :cond_2
    invoke-static {}, Lcom/peel/control/u;->c()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "sending Delay"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method
