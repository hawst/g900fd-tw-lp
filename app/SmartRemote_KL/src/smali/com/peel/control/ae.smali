.class Lcom/peel/control/ae;
.super Lcom/peel/util/t;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/peel/util/t",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/peel/control/ad;


# direct methods
.method constructor <init>(Lcom/peel/control/ad;)V
    .locals 0

    .prologue
    .line 130
    iput-object p1, p0, Lcom/peel/control/ae;->a:Lcom/peel/control/ad;

    invoke-direct {p0}, Lcom/peel/util/t;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 11

    .prologue
    const/4 v10, 0x0

    const/4 v3, 0x0

    .line 133
    iget-boolean v0, p0, Lcom/peel/control/ae;->i:Z

    if-nez v0, :cond_0

    .line 134
    iget-object v0, p0, Lcom/peel/control/ae;->a:Lcom/peel/control/ad;

    iget-object v0, v0, Lcom/peel/control/ad;->d:Lcom/peel/util/t;

    iget-boolean v1, p0, Lcom/peel/control/ae;->i:Z

    iget-object v2, p0, Lcom/peel/control/ae;->j:Ljava/lang/Object;

    iget-object v3, p0, Lcom/peel/control/ae;->k:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3}, Lcom/peel/util/t;->a(ZLjava/lang/Object;Ljava/lang/String;)V

    .line 135
    invoke-static {}, Lcom/peel/control/aa;->a()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/control/ae;->k:Ljava/lang/String;

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 175
    :goto_0
    return-void

    .line 139
    :cond_0
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 142
    :try_start_0
    iget-object v0, p0, Lcom/peel/control/ae;->j:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/peel/control/aa;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 143
    invoke-static {}, Lcom/peel/control/aa;->b()Lorg/codehaus/jackson/map/ObjectMapper;

    move-result-object v1

    iget-object v0, p0, Lcom/peel/control/ae;->j:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    const-class v2, Ljava/util/HashMap;

    invoke-virtual {v1, v0, v2}, Lorg/codehaus/jackson/map/ObjectMapper;->readValue(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/HashMap;

    const-string/jumbo v1, "brands"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/HashMap;

    const-string/jumbo v1, "brands"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    move-object v4, v0

    .line 152
    :goto_1
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v6

    move v2, v3

    :goto_2
    if-ge v2, v6, :cond_2

    .line 153
    invoke-interface {v4, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/HashMap;

    .line 161
    new-instance v7, Lcom/peel/f/a;

    const-string/jumbo v1, "id"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v8

    const-string/jumbo v1, "brandName"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    const-string/jumbo v9, "rank"

    invoke-virtual {v0, v9}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    invoke-direct {v7, v8, v1, v0}, Lcom/peel/f/a;-><init>(ILjava/lang/String;I)V

    invoke-interface {v5, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 152
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    .line 145
    :cond_1
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 146
    invoke-static {}, Lcom/peel/control/aa;->b()Lorg/codehaus/jackson/map/ObjectMapper;

    move-result-object v2

    iget-object v0, p0, Lcom/peel/control/ae;->j:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    const-class v4, Ljava/util/HashMap;

    invoke-virtual {v2, v0, v4}, Lorg/codehaus/jackson/map/ObjectMapper;->readValue(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/HashMap;

    const-string/jumbo v2, "brands"

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/HashMap;

    const-string/jumbo v2, "brands"

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-object v4, v1

    goto :goto_1

    .line 170
    :cond_2
    iget-object v0, p0, Lcom/peel/control/ae;->a:Lcom/peel/control/ad;

    iget-object v0, v0, Lcom/peel/control/ad;->d:Lcom/peel/util/t;

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v5, v2}, Lcom/peel/util/t;->a(ZLjava/lang/Object;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 171
    :catch_0
    move-exception v0

    .line 172
    invoke-static {}, Lcom/peel/control/aa;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {}, Lcom/peel/control/aa;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 173
    iget-object v1, p0, Lcom/peel/control/ae;->a:Lcom/peel/control/ad;

    iget-object v1, v1, Lcom/peel/control/ad;->d:Lcom/peel/util/t;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v3, v10, v0}, Lcom/peel/util/t;->a(ZLjava/lang/Object;Ljava/lang/String;)V

    goto/16 :goto_0
.end method
