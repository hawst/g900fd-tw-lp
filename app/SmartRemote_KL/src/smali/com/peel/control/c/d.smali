.class public Lcom/peel/control/c/d;
.super Ljava/lang/Object;


# instance fields
.field a:Ljava/net/SocketAddress;

.field b:Ljava/net/MulticastSocket;


# direct methods
.method public constructor <init>(Ljava/net/InetAddress;)V
    .locals 3

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Local address: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 21
    new-instance v0, Ljava/net/InetSocketAddress;

    const-string/jumbo v1, "239.255.255.250"

    const/16 v2, 0x76c

    invoke-direct {v0, v1, v2}, Ljava/net/InetSocketAddress;-><init>(Ljava/lang/String;I)V

    iput-object v0, p0, Lcom/peel/control/c/d;->a:Ljava/net/SocketAddress;

    .line 22
    new-instance v0, Ljava/net/MulticastSocket;

    new-instance v1, Ljava/net/InetSocketAddress;

    const/4 v2, 0x0

    invoke-direct {v1, p1, v2}, Ljava/net/InetSocketAddress;-><init>(Ljava/net/InetAddress;I)V

    invoke-direct {v0, v1}, Ljava/net/MulticastSocket;-><init>(Ljava/net/SocketAddress;)V

    iput-object v0, p0, Lcom/peel/control/c/d;->b:Ljava/net/MulticastSocket;

    .line 24
    invoke-static {p1}, Ljava/net/NetworkInterface;->getByInetAddress(Ljava/net/InetAddress;)Ljava/net/NetworkInterface;

    move-result-object v0

    .line 25
    iget-object v1, p0, Lcom/peel/control/c/d;->b:Ljava/net/MulticastSocket;

    iget-object v2, p0, Lcom/peel/control/c/d;->a:Ljava/net/SocketAddress;

    invoke-virtual {v1, v2, v0}, Ljava/net/MulticastSocket;->joinGroup(Ljava/net/SocketAddress;Ljava/net/NetworkInterface;)V

    .line 26
    iget-object v0, p0, Lcom/peel/control/c/d;->b:Ljava/net/MulticastSocket;

    const/16 v1, 0xfa0

    invoke-virtual {v0, v1}, Ljava/net/MulticastSocket;->setSoTimeout(I)V

    .line 27
    return-void
.end method


# virtual methods
.method public a()Ljava/net/DatagramPacket;
    .locals 3

    .prologue
    .line 40
    const/16 v0, 0x400

    new-array v0, v0, [B

    .line 41
    new-instance v1, Ljava/net/DatagramPacket;

    array-length v2, v0

    invoke-direct {v1, v0, v2}, Ljava/net/DatagramPacket;-><init>([BI)V

    .line 43
    iget-object v0, p0, Lcom/peel/control/c/d;->b:Ljava/net/MulticastSocket;

    invoke-virtual {v0, v1}, Ljava/net/MulticastSocket;->receive(Ljava/net/DatagramPacket;)V

    .line 45
    return-object v1
.end method

.method public a(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 32
    new-instance v0, Ljava/net/DatagramPacket;

    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    iget-object v3, p0, Lcom/peel/control/c/d;->a:Ljava/net/SocketAddress;

    invoke-direct {v0, v1, v2, v3}, Ljava/net/DatagramPacket;-><init>([BILjava/net/SocketAddress;)V

    .line 33
    iget-object v1, p0, Lcom/peel/control/c/d;->b:Ljava/net/MulticastSocket;

    invoke-virtual {v1, v0}, Ljava/net/MulticastSocket;->send(Ljava/net/DatagramPacket;)V

    .line 35
    return-void
.end method
