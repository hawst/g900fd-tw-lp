.class final Lcom/peel/control/i;
.super Lcom/peel/util/s;


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Lcom/peel/util/s;-><init>()V

    return-void
.end method


# virtual methods
.method public final varargs a(ILjava/lang/Object;[Ljava/lang/Object;)V
    .locals 11

    .prologue
    const/4 v4, 0x0

    const/4 v10, 0x0

    .line 35
    instance-of v1, p2, Lcom/peel/control/h;

    if-nez v1, :cond_0

    .line 106
    :goto_0
    return-void

    :cond_0
    move-object v9, p2

    .line 36
    check-cast v9, Lcom/peel/control/h;

    .line 38
    packed-switch p1, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    .line 43
    :pswitch_1
    invoke-static {v9}, Lcom/peel/control/h;->a(Lcom/peel/control/h;)Lcom/peel/control/k;

    move-result-object v2

    const/16 v3, 0x14

    move-object v1, v10

    check-cast v1, [Ljava/lang/Object;

    invoke-virtual {v2, v3, v9, v1}, Lcom/peel/control/k;->a(ILjava/lang/Object;[Ljava/lang/Object;)V

    goto :goto_0

    .line 46
    :pswitch_2
    invoke-static {v9}, Lcom/peel/control/h;->a(Lcom/peel/control/h;)Lcom/peel/control/k;

    move-result-object v1

    const/16 v2, 0x15

    check-cast v10, [Ljava/lang/Object;

    invoke-virtual {v1, v2, v9, v10}, Lcom/peel/control/k;->a(ILjava/lang/Object;[Ljava/lang/Object;)V

    goto :goto_0

    .line 49
    :pswitch_3
    invoke-static {v9}, Lcom/peel/control/h;->a(Lcom/peel/control/h;)Lcom/peel/control/k;

    move-result-object v1

    const/16 v2, 0x16

    check-cast v10, [Ljava/lang/Object;

    invoke-virtual {v1, v2, v9, v10}, Lcom/peel/control/k;->a(ILjava/lang/Object;[Ljava/lang/Object;)V

    goto :goto_0

    .line 52
    :pswitch_4
    invoke-static {v9}, Lcom/peel/control/h;->a(Lcom/peel/control/h;)Lcom/peel/control/k;

    move-result-object v1

    const/16 v2, 0x1a

    check-cast v10, [Ljava/lang/Object;

    invoke-virtual {v1, v2, v9, v10}, Lcom/peel/control/k;->a(ILjava/lang/Object;[Ljava/lang/Object;)V

    goto :goto_0

    .line 55
    :pswitch_5
    invoke-static {v9}, Lcom/peel/control/h;->a(Lcom/peel/control/h;)Lcom/peel/control/k;

    move-result-object v1

    const/16 v2, 0x1b

    check-cast v10, [Ljava/lang/Object;

    invoke-virtual {v1, v2, v9, v10}, Lcom/peel/control/k;->a(ILjava/lang/Object;[Ljava/lang/Object;)V

    goto :goto_0

    .line 58
    :pswitch_6
    invoke-static {v9}, Lcom/peel/control/h;->a(Lcom/peel/control/h;)Lcom/peel/control/k;

    move-result-object v1

    const/16 v2, 0x1c

    invoke-virtual {v1, v2, v9, p3}, Lcom/peel/control/k;->a(ILjava/lang/Object;[Ljava/lang/Object;)V

    goto :goto_0

    .line 61
    :pswitch_7
    invoke-static {v9}, Lcom/peel/control/h;->a(Lcom/peel/control/h;)Lcom/peel/control/k;

    move-result-object v1

    const/16 v2, 0x1d

    check-cast v10, [Ljava/lang/Object;

    invoke-virtual {v1, v2, v9, v10}, Lcom/peel/control/k;->a(ILjava/lang/Object;[Ljava/lang/Object;)V

    goto :goto_0

    .line 64
    :pswitch_8
    invoke-static {}, Lcom/peel/control/h;->p()Ljava/lang/String;

    move-result-object v2

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "device error "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    aget-object v1, p3, v4

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 65
    invoke-static {v9}, Lcom/peel/control/h;->a(Lcom/peel/control/h;)Lcom/peel/control/k;

    move-result-object v1

    const/16 v2, 0x19

    check-cast v10, [Ljava/lang/Object;

    invoke-virtual {v1, v2, v9, v10}, Lcom/peel/control/k;->a(ILjava/lang/Object;[Ljava/lang/Object;)V

    .line 70
    :try_start_0
    invoke-static {}, Lcom/peel/util/a/f;->a()Lcom/peel/util/a/f;

    move-result-object v2

    sget-object v1, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v1}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/data/at;->f()I

    move-result v3

    const/16 v4, 0x138a

    const/16 v5, 0x7e7

    const/4 v1, 0x0

    aget-object v1, p3, v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v2, v3, v4, v5, v1}, Lcom/peel/util/a/f;->a(IIILjava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 71
    :catch_0
    move-exception v1

    .line 72
    invoke-static {}, Lcom/peel/control/h;->p()Ljava/lang/String;

    move-result-object v2

    invoke-static {}, Lcom/peel/control/h;->p()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_0

    .line 76
    :pswitch_9
    invoke-static {v9}, Lcom/peel/control/h;->a(Lcom/peel/control/h;)Lcom/peel/control/k;

    move-result-object v1

    const/16 v2, 0x1e

    check-cast v10, [Ljava/lang/Object;

    invoke-virtual {v1, v2, v9, v10}, Lcom/peel/control/k;->a(ILjava/lang/Object;[Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 87
    :pswitch_a
    :try_start_1
    invoke-static {}, Lcom/peel/util/a/f;->a()Lcom/peel/util/a/f;

    move-result-object v1

    sget-object v2, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v2}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v2

    invoke-virtual {v2}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v2

    invoke-virtual {v2}, Lcom/peel/data/at;->f()I

    move-result v2

    const/16 v3, 0x1388

    const/16 v4, 0x7e7

    move-object v0, p2

    check-cast v0, Lcom/peel/control/h;

    move-object v5, v0

    invoke-virtual {v5}, Lcom/peel/control/h;->f()Ljava/lang/String;

    move-result-object v5

    check-cast p2, Lcom/peel/control/h;

    invoke-virtual {p2}, Lcom/peel/control/h;->e()I

    move-result v6

    const/4 v7, 0x0

    aget-object v7, p3, v7

    check-cast v7, Ljava/lang/String;

    const/4 v8, -0x1

    invoke-virtual/range {v1 .. v8}, Lcom/peel/util/a/f;->a(IIILjava/lang/String;ILjava/lang/String;I)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 103
    :goto_1
    invoke-static {v9}, Lcom/peel/control/h;->a(Lcom/peel/control/h;)Lcom/peel/control/k;

    move-result-object v1

    const/16 v2, 0x1f

    check-cast v10, [Ljava/lang/Object;

    invoke-virtual {v1, v2, v9, v10}, Lcom/peel/control/k;->a(ILjava/lang/Object;[Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 89
    :catch_1
    move-exception v1

    .line 90
    invoke-static {}, Lcom/peel/control/h;->p()Ljava/lang/String;

    move-result-object v2

    invoke-static {}, Lcom/peel/control/h;->p()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1

    .line 38
    nop

    :pswitch_data_0
    .packed-switch 0x14
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_8
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_9
        :pswitch_a
    .end packed-switch
.end method
