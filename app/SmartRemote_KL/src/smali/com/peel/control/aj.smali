.class final Lcom/peel/control/aj;
.super Lcom/peel/util/t;


# instance fields
.field final synthetic a:[Ljava/lang/String;

.field final synthetic b:Lcom/peel/util/t;


# direct methods
.method constructor <init>([Ljava/lang/String;Lcom/peel/util/t;)V
    .locals 0

    .prologue
    .line 308
    iput-object p1, p0, Lcom/peel/control/aj;->a:[Ljava/lang/String;

    iput-object p2, p0, Lcom/peel/control/aj;->b:Lcom/peel/util/t;

    invoke-direct {p0}, Lcom/peel/util/t;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 311
    invoke-static {}, Lcom/peel/control/aa;->c()I

    .line 312
    iget-boolean v0, p0, Lcom/peel/control/aj;->i:Z

    if-eqz v0, :cond_1

    .line 313
    invoke-static {}, Lcom/peel/control/aa;->a()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "adding IR maps..."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 314
    invoke-static {}, Lcom/peel/control/aa;->d()Ljava/util/ArrayList;

    move-result-object v1

    iget-object v0, p0, Lcom/peel/control/aj;->j:Ljava/lang/Object;

    check-cast v0, Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 318
    :goto_0
    invoke-static {}, Lcom/peel/control/aa;->e()I

    move-result v0

    iget-object v1, p0, Lcom/peel/control/aj;->a:[Ljava/lang/String;

    array-length v1, v1

    if-ne v0, v1, :cond_0

    .line 320
    invoke-static {}, Lcom/peel/control/aa;->a()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "return IR maps... size: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lcom/peel/control/aa;->d()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 321
    invoke-static {}, Lcom/peel/control/aa;->d()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_2

    const/4 v0, 0x1

    .line 322
    :goto_1
    iget-object v1, p0, Lcom/peel/control/aj;->b:Lcom/peel/util/t;

    invoke-static {}, Lcom/peel/control/aa;->d()Ljava/util/ArrayList;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v1, v0, v2, v3}, Lcom/peel/util/t;->a(ZLjava/lang/Object;Ljava/lang/String;)V

    .line 324
    :cond_0
    return-void

    .line 316
    :cond_1
    invoke-static {}, Lcom/peel/control/aa;->a()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/peel/control/aj;->j:Ljava/lang/Object;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/peel/control/aj;->k:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 321
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method
