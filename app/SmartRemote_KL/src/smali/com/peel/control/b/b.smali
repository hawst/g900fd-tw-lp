.class public Lcom/peel/control/b/b;
.super Lcom/peel/control/b/k;


# static fields
.field public static c:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final d:Ljava/lang/String;


# instance fields
.field private e:Ljava/lang/String;

.field private f:Ljava/lang/String;

.field private g:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 94
    const-class v0, Lcom/peel/control/b/b;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/peel/control/b/b;->d:Ljava/lang/String;

    .line 104
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/peel/control/b/b;->c:Ljava/util/Map;

    .line 107
    sget-object v0, Lcom/peel/control/b/b;->c:Ljava/util/Map;

    const-string/jumbo v1, "Power"

    const-string/jumbo v2, "power"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 108
    sget-object v0, Lcom/peel/control/b/b;->c:Ljava/util/Map;

    const-string/jumbo v1, "PowerOn"

    const-string/jumbo v2, "poweron"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 109
    sget-object v0, Lcom/peel/control/b/b;->c:Ljava/util/Map;

    const-string/jumbo v1, "PowerOff"

    const-string/jumbo v2, "poweroff"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 110
    sget-object v0, Lcom/peel/control/b/b;->c:Ljava/util/Map;

    const-string/jumbo v1, "Dot_DASh"

    const-string/jumbo v2, "dash"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 111
    sget-object v0, Lcom/peel/control/b/b;->c:Ljava/util/Map;

    const-string/jumbo v1, "Enter"

    const-string/jumbo v2, "enter"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 112
    sget-object v0, Lcom/peel/control/b/b;->c:Ljava/util/Map;

    const-string/jumbo v1, "Guide"

    const-string/jumbo v2, "guide"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 113
    sget-object v0, Lcom/peel/control/b/b;->c:Ljava/util/Map;

    const-string/jumbo v1, "Exit"

    const-string/jumbo v2, "exit"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 114
    sget-object v0, Lcom/peel/control/b/b;->c:Ljava/util/Map;

    const-string/jumbo v1, "Back"

    const-string/jumbo v2, "back"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 115
    sget-object v0, Lcom/peel/control/b/b;->c:Ljava/util/Map;

    const-string/jumbo v1, "Menu"

    const-string/jumbo v2, "menu"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 116
    sget-object v0, Lcom/peel/control/b/b;->c:Ljava/util/Map;

    const-string/jumbo v1, "Info"

    const-string/jumbo v2, "info"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 117
    sget-object v0, Lcom/peel/control/b/b;->c:Ljava/util/Map;

    const-string/jumbo v1, "Navigate_Up"

    const-string/jumbo v2, "up"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 118
    sget-object v0, Lcom/peel/control/b/b;->c:Ljava/util/Map;

    const-string/jumbo v1, "Navigate_Down"

    const-string/jumbo v2, "down"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 119
    sget-object v0, Lcom/peel/control/b/b;->c:Ljava/util/Map;

    const-string/jumbo v1, "Navigate_Left"

    const-string/jumbo v2, "left"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 120
    sget-object v0, Lcom/peel/control/b/b;->c:Ljava/util/Map;

    const-string/jumbo v1, "Navigate_Right"

    const-string/jumbo v2, "right"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 121
    sget-object v0, Lcom/peel/control/b/b;->c:Ljava/util/Map;

    const-string/jumbo v1, "Select"

    const-string/jumbo v2, "select"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 122
    sget-object v0, Lcom/peel/control/b/b;->c:Ljava/util/Map;

    const-string/jumbo v1, "Red"

    const-string/jumbo v2, "red"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 123
    sget-object v0, Lcom/peel/control/b/b;->c:Ljava/util/Map;

    const-string/jumbo v1, "Green"

    const-string/jumbo v2, "green"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 124
    sget-object v0, Lcom/peel/control/b/b;->c:Ljava/util/Map;

    const-string/jumbo v1, "Yellow"

    const-string/jumbo v2, "yellow"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 125
    sget-object v0, Lcom/peel/control/b/b;->c:Ljava/util/Map;

    const-string/jumbo v1, "Blue"

    const-string/jumbo v2, "blue"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 126
    sget-object v0, Lcom/peel/control/b/b;->c:Ljava/util/Map;

    const-string/jumbo v1, "Channel_Up"

    const-string/jumbo v2, "chanup"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 127
    sget-object v0, Lcom/peel/control/b/b;->c:Ljava/util/Map;

    const-string/jumbo v1, "Channel_Down"

    const-string/jumbo v2, "chandown"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 128
    sget-object v0, Lcom/peel/control/b/b;->c:Ljava/util/Map;

    const-string/jumbo v1, "Pause"

    const-string/jumbo v2, "pause"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 129
    sget-object v0, Lcom/peel/control/b/b;->c:Ljava/util/Map;

    const-string/jumbo v1, "Rewind"

    const-string/jumbo v2, "rew"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 130
    sget-object v0, Lcom/peel/control/b/b;->c:Ljava/util/Map;

    const-string/jumbo v1, "Stop"

    const-string/jumbo v2, "stop"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 131
    sget-object v0, Lcom/peel/control/b/b;->c:Ljava/util/Map;

    const-string/jumbo v1, "Fast_Forward"

    const-string/jumbo v2, "ffwd"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 132
    sget-object v0, Lcom/peel/control/b/b;->c:Ljava/util/Map;

    const-string/jumbo v1, "Record"

    const-string/jumbo v2, "record"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 133
    sget-object v0, Lcom/peel/control/b/b;->c:Ljava/util/Map;

    const-string/jumbo v1, "Play"

    const-string/jumbo v2, "play"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 134
    sget-object v0, Lcom/peel/control/b/b;->c:Ljava/util/Map;

    const-string/jumbo v1, "Active"

    const-string/jumbo v2, "active"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 135
    sget-object v0, Lcom/peel/control/b/b;->c:Ljava/util/Map;

    const-string/jumbo v1, "list"

    const-string/jumbo v2, "list"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 136
    sget-object v0, Lcom/peel/control/b/b;->c:Ljava/util/Map;

    const-string/jumbo v1, "advance"

    const-string/jumbo v2, "advance"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 137
    sget-object v0, Lcom/peel/control/b/b;->c:Ljava/util/Map;

    const-string/jumbo v1, "Previous"

    const-string/jumbo v2, "Previous"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 138
    sget-object v0, Lcom/peel/control/b/b;->c:Ljava/util/Map;

    const-string/jumbo v1, "replay"

    const-string/jumbo v2, "replay"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 139
    sget-object v0, Lcom/peel/control/b/b;->c:Ljava/util/Map;

    const-string/jumbo v1, "format"

    const-string/jumbo v2, "format"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 140
    sget-object v0, Lcom/peel/control/b/b;->c:Ljava/util/Map;

    const-string/jumbo v1, "0"

    const-string/jumbo v2, "0"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 141
    sget-object v0, Lcom/peel/control/b/b;->c:Ljava/util/Map;

    const-string/jumbo v1, "1"

    const-string/jumbo v2, "1"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 142
    sget-object v0, Lcom/peel/control/b/b;->c:Ljava/util/Map;

    const-string/jumbo v1, "2"

    const-string/jumbo v2, "2"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 143
    sget-object v0, Lcom/peel/control/b/b;->c:Ljava/util/Map;

    const-string/jumbo v1, "3"

    const-string/jumbo v2, "3"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 144
    sget-object v0, Lcom/peel/control/b/b;->c:Ljava/util/Map;

    const-string/jumbo v1, "4"

    const-string/jumbo v2, "4"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 145
    sget-object v0, Lcom/peel/control/b/b;->c:Ljava/util/Map;

    const-string/jumbo v1, "5"

    const-string/jumbo v2, "5"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 146
    sget-object v0, Lcom/peel/control/b/b;->c:Ljava/util/Map;

    const-string/jumbo v1, "6"

    const-string/jumbo v2, "6"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 147
    sget-object v0, Lcom/peel/control/b/b;->c:Ljava/util/Map;

    const-string/jumbo v1, "7"

    const-string/jumbo v2, "7"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 148
    sget-object v0, Lcom/peel/control/b/b;->c:Ljava/util/Map;

    const-string/jumbo v1, "8"

    const-string/jumbo v2, "8"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 149
    sget-object v0, Lcom/peel/control/b/b;->c:Ljava/util/Map;

    const-string/jumbo v1, "9"

    const-string/jumbo v2, "9"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 150
    return-void
.end method

.method public constructor <init>(ILjava/lang/String;ZLjava/lang/String;ILandroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 167
    invoke-direct/range {p0 .. p8}, Lcom/peel/control/b/k;-><init>(ILjava/lang/String;ZLjava/lang/String;ILandroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;)V

    .line 154
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/peel/control/b/b;->g:Z

    .line 169
    sget-object v0, Lcom/peel/control/b/b;->d:Ljava/lang/String;

    const-string/jumbo v1, "getLocationName"

    new-instance v2, Lcom/peel/control/b/c;

    invoke-direct {v2, p0}, Lcom/peel/control/b/c;-><init>(Lcom/peel/control/b/b;)V

    invoke-static {v0, v1, v2}, Lcom/peel/util/i;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    .line 177
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 178
    sget-object v0, Lcom/peel/control/b/b;->c:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const/4 v3, 0x0

    invoke-interface {v1, v0, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 180
    :cond_0
    invoke-virtual {p0}, Lcom/peel/control/b/b;->l()Lcom/peel/data/g;

    move-result-object v0

    const/16 v2, 0x3e9

    invoke-virtual {v0, v2, v1}, Lcom/peel/data/g;->a(ILjava/util/Map;)V

    .line 181
    return-void
.end method

.method public constructor <init>(Lcom/peel/data/g;)V
    .locals 4

    .prologue
    .line 157
    invoke-direct {p0, p1}, Lcom/peel/control/b/k;-><init>(Lcom/peel/data/g;)V

    .line 154
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/peel/control/b/b;->g:Z

    .line 160
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 161
    sget-object v0, Lcom/peel/control/b/b;->c:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const/4 v3, 0x0

    invoke-interface {v1, v0, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 163
    :cond_0
    invoke-virtual {p0}, Lcom/peel/control/b/b;->l()Lcom/peel/data/g;

    move-result-object v0

    const/16 v2, 0x3e9

    invoke-virtual {v0, v2, v1}, Lcom/peel/data/g;->a(ILjava/util/Map;)V

    .line 164
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;ILjava/lang/String;ZLjava/lang/String;ILandroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 184
    invoke-direct/range {p0 .. p9}, Lcom/peel/control/b/k;-><init>(Ljava/lang/String;ILjava/lang/String;ZLjava/lang/String;ILandroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;)V

    .line 154
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/peel/control/b/b;->g:Z

    .line 186
    sget-object v0, Lcom/peel/control/b/b;->d:Ljava/lang/String;

    const-string/jumbo v1, "getLocationName"

    new-instance v2, Lcom/peel/control/b/e;

    invoke-direct {v2, p0}, Lcom/peel/control/b/e;-><init>(Lcom/peel/control/b/b;)V

    invoke-static {v0, v1, v2}, Lcom/peel/util/i;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    .line 194
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 195
    sget-object v0, Lcom/peel/control/b/b;->c:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const/4 v3, 0x0

    invoke-interface {v1, v0, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 197
    :cond_0
    invoke-virtual {p0}, Lcom/peel/control/b/b;->l()Lcom/peel/data/g;

    move-result-object v0

    const/16 v2, 0x3e9

    invoke-virtual {v0, v2, v1}, Lcom/peel/data/g;->a(ILjava/util/Map;)V

    .line 198
    return-void
.end method

.method static synthetic a(Lcom/peel/control/b/b;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 29
    iput-object p1, p0, Lcom/peel/control/b/b;->e:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic a(Lcom/peel/control/b/b;)V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/peel/control/b/b;->r()V

    return-void
.end method

.method static synthetic a(Lcom/peel/control/b/b;Z)Z
    .locals 0

    .prologue
    .line 29
    iput-boolean p1, p0, Lcom/peel/control/b/b;->g:Z

    return p1
.end method

.method static synthetic b(Lcom/peel/control/b/b;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lcom/peel/control/b/b;->e:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic b(Lcom/peel/control/b/b;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 29
    iput-object p1, p0, Lcom/peel/control/b/b;->f:Ljava/lang/String;

    return-object p1
.end method

.method private e(Ljava/lang/String;)Z
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 274
    const-string/jumbo v0, "http://%s:%d/tv/tune?major=%s"

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/peel/control/b/b;->i()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {p0}, Lcom/peel/control/b/b;->j()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    const/4 v2, 0x2

    aput-object p1, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lcom/peel/control/b/g;

    invoke-direct {v1, p0, p1}, Lcom/peel/control/b/g;-><init>(Lcom/peel/control/b/b;Ljava/lang/String;)V

    invoke-static {v0, v1}, Lcom/peel/util/b/a;->b(Ljava/lang/String;Lcom/peel/util/t;)V

    .line 286
    return v4
.end method

.method static synthetic q()Ljava/lang/String;
    .locals 1

    .prologue
    .line 29
    sget-object v0, Lcom/peel/control/b/b;->d:Ljava/lang/String;

    return-object v0
.end method

.method private r()V
    .locals 4

    .prologue
    .line 314
    const-string/jumbo v0, "http://%s:%d/info/getLocations"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/peel/control/b/b;->i()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/peel/control/b/b;->j()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lcom/peel/control/b/h;

    invoke-direct {v1, p0}, Lcom/peel/control/b/h;-><init>(Lcom/peel/control/b/b;)V

    invoke-static {v0, v1}, Lcom/peel/util/b/a;->b(Ljava/lang/String;Lcom/peel/util/t;)V

    .line 331
    return-void
.end method


# virtual methods
.method public a(Lcom/peel/util/t;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 429
    invoke-virtual {p0}, Lcom/peel/control/b/b;->k()Landroid/os/Bundle;

    move-result-object v0

    const-string/jumbo v1, "usn"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 431
    if-nez v0, :cond_0

    .line 434
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "No USN found for device: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/peel/control/b/b;->f()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " - "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/peel/control/b/b;->e()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " - "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/peel/control/b/b;->i()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/peel/control/b/b;->j()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v3, v0, v1}, Lcom/peel/util/t;->a(ZLjava/lang/Object;Ljava/lang/String;)V

    .line 459
    :goto_0
    return-void

    .line 437
    :cond_0
    sget-object v1, Lcom/peel/control/b/b;->d:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "upnp lookup by usn: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/peel/control/b/j;

    invoke-direct {v3, p0, v0, p1}, Lcom/peel/control/b/j;-><init>(Lcom/peel/control/b/b;Ljava/lang/String;Lcom/peel/util/t;)V

    invoke-static {v1, v2, v3}, Lcom/peel/util/i;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/lang/Runnable;

    goto :goto_0
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 515
    iget-boolean v0, p0, Lcom/peel/control/b/b;->g:Z

    return v0
.end method

.method public a(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 309
    sget-object v0, Lcom/peel/control/b/b;->c:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;I)Z
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 397
    const-string/jumbo v0, "http://%s:%d/dvr/play?uniqueId=%s&playFrom=%s&offset=%d"

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/peel/control/b/b;->i()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-virtual {p0}, Lcom/peel/control/b/b;->j()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v5

    const/4 v2, 0x2

    aput-object p1, v1, v2

    const/4 v2, 0x3

    aput-object p2, v1, v2

    const/4 v2, 0x4

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "c0pi10t"

    const-string/jumbo v2, "8th5Bre$Wrus"

    new-instance v3, Lcom/peel/control/b/i;

    invoke-direct {v3, p0, p1, p2, p3}, Lcom/peel/control/b/i;-><init>(Lcom/peel/control/b/b;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v0, v1, v2, v4, v3}, Lcom/peel/util/b/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLcom/peel/util/t;)V

    .line 411
    return v5
.end method

.method public a(Ljava/net/URI;)Z
    .locals 6

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 236
    invoke-virtual {p1}, Ljava/net/URI;->getQuery()Ljava/lang/String;

    move-result-object v2

    .line 238
    sget-object v3, Lcom/peel/control/b/b;->d:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "\nscheme: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p1}, Ljava/net/URI;->getScheme()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "\nhost: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p1}, Ljava/net/URI;->getHost()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "\nauthority: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p1}, Ljava/net/URI;->getAuthority()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "\npath: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p1}, Ljava/net/URI;->getPath()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "\nquery: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 239
    invoke-virtual {p1}, Ljava/net/URI;->getScheme()Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v4, "live"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 240
    invoke-virtual {p1}, Ljava/net/URI;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/peel/control/b/b;->e(Ljava/lang/String;)Z

    .line 257
    :goto_0
    return v0

    .line 242
    :cond_0
    invoke-virtual {p1}, Ljava/net/URI;->getScheme()Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v4, "dtv"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 243
    if-eqz v2, :cond_2

    .line 244
    const-string/jumbo v3, "&"

    invoke-virtual {v2, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 245
    array-length v3, v2

    if-ne v3, v0, :cond_1

    .line 246
    aget-object v3, v2, v1

    aget-object v1, v2, v1

    const-string/jumbo v2, "="

    invoke-virtual {v1, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v3, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/peel/control/b/b;->d(Ljava/lang/String;)Z

    goto :goto_0

    .line 248
    :cond_1
    aget-object v3, v2, v0

    aget-object v4, v2, v0

    const-string/jumbo v5, "="

    invoke-virtual {v4, v5}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v4

    add-int/lit8 v4, v4, 0x1

    invoke-virtual {v3, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    .line 249
    aget-object v4, v2, v1

    aget-object v1, v2, v1

    const-string/jumbo v2, "="

    invoke-virtual {v1, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v4, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "offset"

    invoke-virtual {p0, v1, v2, v3}, Lcom/peel/control/b/b;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    goto :goto_0

    .line 255
    :cond_2
    sget-object v2, Lcom/peel/control/b/b;->d:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "unrecognized command: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 256
    sget-object v2, Lcom/peel/control/b/b;->a:Lcom/peel/control/k;

    const/16 v3, 0x19

    new-array v0, v0, [Ljava/lang/Object;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "unrecognized command: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v0, v1

    invoke-virtual {v2, v3, p0, v0}, Lcom/peel/control/k;->a(ILjava/lang/Object;[Ljava/lang/Object;)V

    move v0, v1

    .line 257
    goto/16 :goto_0
.end method

.method public b()V
    .locals 1

    .prologue
    .line 497
    new-instance v0, Lcom/peel/control/b/d;

    invoke-direct {v0, p0}, Lcom/peel/control/b/d;-><init>(Lcom/peel/control/b/b;)V

    invoke-virtual {p0, v0}, Lcom/peel/control/b/b;->a(Lcom/peel/util/t;)V

    .line 511
    return-void
.end method

.method public c(Ljava/lang/String;)Z
    .locals 9

    .prologue
    const/4 v8, 0x2

    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 205
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    if-le v2, v0, :cond_0

    invoke-static {p1}, Landroid/text/TextUtils;->isDigitsOnly(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-direct {p0, p1}, Lcom/peel/control/b/b;->e(Ljava/lang/String;)Z

    move-result v0

    .line 228
    :goto_0
    return v0

    .line 208
    :cond_0
    const-string/jumbo v2, "play:"

    invoke-virtual {p1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 209
    const-string/jumbo v2, ":"

    invoke-virtual {p1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 210
    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    sget-object v5, Lcom/peel/control/b/b;->d:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, " xxxxxxx token: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v5, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 211
    :cond_1
    aget-object v1, v2, v0

    const-string/jumbo v3, "offset"

    aget-object v2, v2, v8

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {p0, v1, v3, v2}, Lcom/peel/control/b/b;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    goto :goto_0

    .line 216
    :cond_2
    const-string/jumbo v2, "http://%s:%d/remote/processKey?key=%s&hold=keyPress"

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/peel/control/b/b;->i()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v1

    invoke-virtual {p0}, Lcom/peel/control/b/b;->j()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v3, v0

    sget-object v1, Lcom/peel/control/b/b;->c:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    aput-object v1, v3, v8

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/peel/control/b/f;

    invoke-direct {v2, p0, p1}, Lcom/peel/control/b/f;-><init>(Lcom/peel/control/b/b;Ljava/lang/String;)V

    invoke-static {v1, v2}, Lcom/peel/util/b/a;->b(Ljava/lang/String;Lcom/peel/util/t;)V

    goto :goto_0
.end method

.method public c()[Ljava/lang/String;
    .locals 3

    .prologue
    .line 200
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string/jumbo v2, "dtv"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string/jumbo v2, "live"

    aput-object v2, v0, v1

    return-object v0
.end method

.method public d(Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 394
    const-string/jumbo v0, "offset"

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v0, v1}, Lcom/peel/control/b/b;->a(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v0

    return v0
.end method
