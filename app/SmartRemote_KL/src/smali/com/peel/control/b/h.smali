.class Lcom/peel/control/b/h;
.super Lcom/peel/util/t;


# instance fields
.field final synthetic a:Lcom/peel/control/b/b;


# direct methods
.method constructor <init>(Lcom/peel/control/b/b;)V
    .locals 0

    .prologue
    .line 314
    iput-object p1, p0, Lcom/peel/control/b/h;->a:Lcom/peel/control/b/b;

    invoke-direct {p0}, Lcom/peel/util/t;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 318
    invoke-static {}, Lcom/peel/control/b/b;->q()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "GET_LOCATIONS_URL: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "http://%s:%d/info/getLocations"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/peel/control/b/h;->a:Lcom/peel/control/b/b;

    invoke-virtual {v4}, Lcom/peel/control/b/b;->i()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v5

    const/4 v4, 0x1

    iget-object v5, p0, Lcom/peel/control/b/h;->a:Lcom/peel/control/b/b;

    invoke-virtual {v5}, Lcom/peel/control/b/b;->j()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/peel/control/b/h;->j:Ljava/lang/Object;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 319
    iget-object v0, p0, Lcom/peel/control/b/h;->j:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    .line 320
    if-eqz v0, :cond_0

    const-string/jumbo v1, "locationName"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 322
    :try_start_0
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1, v0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 323
    iget-object v0, p0, Lcom/peel/control/b/h;->a:Lcom/peel/control/b/b;

    const-string/jumbo v2, "locations"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v1

    const-string/jumbo v2, "locationName"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/peel/control/b/b;->a(Lcom/peel/control/b/b;Ljava/lang/String;)Ljava/lang/String;

    .line 324
    iget-object v0, p0, Lcom/peel/control/b/h;->a:Lcom/peel/control/b/b;

    invoke-virtual {v0}, Lcom/peel/control/b/b;->k()Landroid/os/Bundle;

    move-result-object v0

    const-string/jumbo v1, "locationName"

    iget-object v2, p0, Lcom/peel/control/b/h;->a:Lcom/peel/control/b/b;

    invoke-static {v2}, Lcom/peel/control/b/b;->b(Lcom/peel/control/b/b;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 329
    :cond_0
    :goto_0
    return-void

    .line 325
    :catch_0
    move-exception v0

    .line 326
    invoke-static {}, Lcom/peel/control/b/b;->q()Ljava/lang/String;

    move-result-object v1

    invoke-static {}, Lcom/peel/control/b/b;->q()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method
