.class Lcom/peel/control/b/n;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic b:Lcom/peel/control/b/m;


# direct methods
.method constructor <init>(Lcom/peel/control/b/m;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 91
    iput-object p1, p0, Lcom/peel/control/b/n;->b:Lcom/peel/control/b/m;

    iput-object p2, p0, Lcom/peel/control/b/n;->a:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    const/4 v2, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x0

    .line 94
    iget-object v0, p0, Lcom/peel/control/b/n;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-le v0, v3, :cond_0

    iget-object v0, p0, Lcom/peel/control/b/n;->a:Ljava/lang/String;

    const-string/jumbo v1, "[0-9]+"

    invoke-virtual {v0, v1}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 95
    const-string/jumbo v0, "http://%s:%d/launch/%s"

    new-array v1, v2, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/peel/control/b/n;->b:Lcom/peel/control/b/m;

    invoke-virtual {v2}, Lcom/peel/control/b/m;->i()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v6

    iget-object v2, p0, Lcom/peel/control/b/n;->b:Lcom/peel/control/b/m;

    invoke-virtual {v2}, Lcom/peel/control/b/m;->j()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    iget-object v2, p0, Lcom/peel/control/b/n;->a:Ljava/lang/String;

    aput-object v2, v1, v7

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v5, v5}, Lcom/peel/util/b/a;->a(Ljava/lang/String;Ljava/util/Map;Lcom/peel/util/t;)V

    .line 101
    :goto_0
    return-void

    .line 96
    :cond_0
    sget-object v0, Lcom/peel/control/b/m;->c:Ljava/util/HashMap;

    iget-object v1, p0, Lcom/peel/control/b/n;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 97
    const-string/jumbo v0, "http://%s:%d/keypress/%s"

    new-array v1, v2, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/peel/control/b/n;->b:Lcom/peel/control/b/m;

    invoke-virtual {v2}, Lcom/peel/control/b/m;->i()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v6

    iget-object v2, p0, Lcom/peel/control/b/n;->b:Lcom/peel/control/b/m;

    invoke-virtual {v2}, Lcom/peel/control/b/m;->j()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    sget-object v2, Lcom/peel/control/b/m;->c:Ljava/util/HashMap;

    iget-object v3, p0, Lcom/peel/control/b/n;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    aput-object v2, v1, v7

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v5, v5}, Lcom/peel/util/b/a;->a(Ljava/lang/String;Ljava/util/Map;Lcom/peel/util/t;)V

    goto :goto_0

    .line 99
    :cond_1
    const-string/jumbo v0, "http://%s:%d/keypress/%s"

    new-array v1, v2, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/peel/control/b/n;->b:Lcom/peel/control/b/m;

    invoke-virtual {v2}, Lcom/peel/control/b/m;->i()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v6

    iget-object v2, p0, Lcom/peel/control/b/n;->b:Lcom/peel/control/b/m;

    invoke-virtual {v2}, Lcom/peel/control/b/m;->j()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    const-string/jumbo v2, "Lit_%s"

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/peel/control/b/n;->a:Ljava/lang/String;

    invoke-static {v4}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v7

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v5, v5}, Lcom/peel/util/b/a;->a(Ljava/lang/String;Ljava/util/Map;Lcom/peel/util/t;)V

    goto :goto_0
.end method
