.class public Lcom/peel/control/b/l;
.super Lcom/peel/control/h;


# static fields
.field public static c:Lcom/peel/control/o;

.field private static final d:Ljava/lang/String;

.field private static final e:Ljava/util/regex/Pattern;

.field private static f:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private g:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 35
    const-class v0, Lcom/peel/control/b/l;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/peel/control/b/l;->d:Ljava/lang/String;

    .line 36
    const-string/jumbo v0, "[0-9.](?=[0-9.])"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/peel/control/b/l;->e:Ljava/util/regex/Pattern;

    .line 37
    const/4 v0, 0x0

    sput-object v0, Lcom/peel/control/b/l;->c:Lcom/peel/control/o;

    .line 39
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/peel/control/b/l;->f:Ljava/util/HashMap;

    .line 42
    sget-object v0, Lcom/peel/control/b/l;->f:Ljava/util/HashMap;

    const-string/jumbo v1, "LLD_TEN"

    const-string/jumbo v2, "10"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 43
    sget-object v0, Lcom/peel/control/b/l;->f:Ljava/util/HashMap;

    const-string/jumbo v1, "LLD_ELEVEN"

    const-string/jumbo v2, "11"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 44
    sget-object v0, Lcom/peel/control/b/l;->f:Ljava/util/HashMap;

    const-string/jumbo v1, "LLD_TWELVE"

    const-string/jumbo v2, "12"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 45
    sget-object v0, Lcom/peel/control/b/l;->f:Ljava/util/HashMap;

    const-string/jumbo v1, "BS_ONE"

    const-string/jumbo v2, "BS1"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 46
    sget-object v0, Lcom/peel/control/b/l;->f:Ljava/util/HashMap;

    const-string/jumbo v1, "BS_TWO"

    const-string/jumbo v2, "BS2"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 47
    sget-object v0, Lcom/peel/control/b/l;->f:Ljava/util/HashMap;

    const-string/jumbo v1, "BS_THREE"

    const-string/jumbo v2, "BS3"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 48
    sget-object v0, Lcom/peel/control/b/l;->f:Ljava/util/HashMap;

    const-string/jumbo v1, "BS_FOUR"

    const-string/jumbo v2, "BS4"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 49
    sget-object v0, Lcom/peel/control/b/l;->f:Ljava/util/HashMap;

    const-string/jumbo v1, "BS_FIVE"

    const-string/jumbo v2, "BS5"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 50
    sget-object v0, Lcom/peel/control/b/l;->f:Ljava/util/HashMap;

    const-string/jumbo v1, "BS_SIX"

    const-string/jumbo v2, "BS6"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 51
    sget-object v0, Lcom/peel/control/b/l;->f:Ljava/util/HashMap;

    const-string/jumbo v1, "BS_SEVEN"

    const-string/jumbo v2, "BS7"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 52
    sget-object v0, Lcom/peel/control/b/l;->f:Ljava/util/HashMap;

    const-string/jumbo v1, "BS_EIGHT"

    const-string/jumbo v2, "BS8"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 53
    sget-object v0, Lcom/peel/control/b/l;->f:Ljava/util/HashMap;

    const-string/jumbo v1, "BS_NINE"

    const-string/jumbo v2, "BS9"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 54
    sget-object v0, Lcom/peel/control/b/l;->f:Ljava/util/HashMap;

    const-string/jumbo v1, "BS_TEN"

    const-string/jumbo v2, "BS10"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 55
    sget-object v0, Lcom/peel/control/b/l;->f:Ljava/util/HashMap;

    const-string/jumbo v1, "BS_ELEVEN"

    const-string/jumbo v2, "BS11"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 56
    sget-object v0, Lcom/peel/control/b/l;->f:Ljava/util/HashMap;

    const-string/jumbo v1, "BS_TWELVE"

    const-string/jumbo v2, "BS12"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 57
    sget-object v0, Lcom/peel/control/b/l;->f:Ljava/util/HashMap;

    const-string/jumbo v1, "CS_ONE"

    const-string/jumbo v2, "CS1"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 58
    sget-object v0, Lcom/peel/control/b/l;->f:Ljava/util/HashMap;

    const-string/jumbo v1, "CS_TWO"

    const-string/jumbo v2, "CS2"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 59
    sget-object v0, Lcom/peel/control/b/l;->f:Ljava/util/HashMap;

    const-string/jumbo v1, "CS_THREE"

    const-string/jumbo v2, "CS3"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 60
    sget-object v0, Lcom/peel/control/b/l;->f:Ljava/util/HashMap;

    const-string/jumbo v1, "CS_FOUR"

    const-string/jumbo v2, "CS4"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 61
    sget-object v0, Lcom/peel/control/b/l;->f:Ljava/util/HashMap;

    const-string/jumbo v1, "CS_FIVE"

    const-string/jumbo v2, "CS5"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 62
    sget-object v0, Lcom/peel/control/b/l;->f:Ljava/util/HashMap;

    const-string/jumbo v1, "CS_SIX"

    const-string/jumbo v2, "CS6"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 63
    sget-object v0, Lcom/peel/control/b/l;->f:Ljava/util/HashMap;

    const-string/jumbo v1, "CS_SEVEN"

    const-string/jumbo v2, "CS7"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 64
    sget-object v0, Lcom/peel/control/b/l;->f:Ljava/util/HashMap;

    const-string/jumbo v1, "CS_EIGHT"

    const-string/jumbo v2, "CS8"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 65
    sget-object v0, Lcom/peel/control/b/l;->f:Ljava/util/HashMap;

    const-string/jumbo v1, "CS_NINE"

    const-string/jumbo v2, "CS9"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 66
    sget-object v0, Lcom/peel/control/b/l;->f:Ljava/util/HashMap;

    const-string/jumbo v1, "CS_TEN"

    const-string/jumbo v2, "CS10"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 67
    sget-object v0, Lcom/peel/control/b/l;->f:Ljava/util/HashMap;

    const-string/jumbo v1, "CS_ELEVEN"

    const-string/jumbo v2, "CS11"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 68
    sget-object v0, Lcom/peel/control/b/l;->f:Ljava/util/HashMap;

    const-string/jumbo v1, "CS_TWELVE"

    const-string/jumbo v2, "CS12"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 69
    return-void
.end method

.method public constructor <init>(ILjava/lang/String;Z)V
    .locals 1

    .prologue
    .line 74
    new-instance v0, Lcom/peel/data/a/b;

    invoke-direct {v0, p1, p2, p3}, Lcom/peel/data/a/b;-><init>(ILjava/lang/String;Z)V

    invoke-direct {p0, v0}, Lcom/peel/control/h;-><init>(Lcom/peel/data/g;)V

    .line 71
    const/4 v0, 0x1

    iput v0, p0, Lcom/peel/control/b/l;->g:I

    .line 75
    return-void
.end method

.method public constructor <init>(Lcom/peel/data/g;)V
    .locals 1

    .prologue
    .line 81
    invoke-direct {p0, p1}, Lcom/peel/control/h;-><init>(Lcom/peel/data/g;)V

    .line 71
    const/4 v0, 0x1

    iput v0, p0, Lcom/peel/control/b/l;->g:I

    .line 81
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;ILjava/lang/String;Z)V
    .locals 1

    .prologue
    .line 78
    new-instance v0, Lcom/peel/data/a/b;

    invoke-direct {v0, p2, p1, p3, p4}, Lcom/peel/data/a/b;-><init>(ILjava/lang/String;Ljava/lang/String;Z)V

    invoke-direct {p0, v0}, Lcom/peel/control/h;-><init>(Lcom/peel/data/g;)V

    .line 71
    const/4 v0, 0x1

    iput v0, p0, Lcom/peel/control/b/l;->g:I

    .line 79
    return-void
.end method


# virtual methods
.method public a()Z
    .locals 1

    .prologue
    .line 83
    const/4 v0, 0x1

    return v0
.end method

.method public a(Ljava/lang/String;J)Z
    .locals 16

    .prologue
    .line 92
    if-nez p1, :cond_0

    sget-object v2, Lcom/peel/control/b/l;->d:Ljava/lang/String;

    const-string/jumbo v3, "unable to send command null"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 93
    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/peel/control/b/l;->b:Lcom/peel/data/g;

    invoke-virtual {v2}, Lcom/peel/data/g;->a()Ljava/util/Map;

    move-result-object v6

    .line 95
    sget-object v2, Lcom/peel/control/b/l;->d:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "\n ********** sendCommand("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 99
    const/4 v2, 0x0

    .line 100
    if-eqz p1, :cond_1

    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->length()I

    move-result v2

    const-string/jumbo v3, "_"

    const-string/jumbo v4, ""

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    sub-int/2addr v2, v3

    .line 102
    :cond_1
    if-eqz p1, :cond_3

    const-string/jumbo v3, "T_"

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 103
    sget-object v2, Lcom/peel/control/b/l;->d:Ljava/lang/String;

    const-string/jumbo v3, "\n ******* command.startsWith(\"T_\") path: "

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 104
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 105
    move-object/from16 v0, p1

    invoke-interface {v6, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 106
    move-object/from16 v0, p1

    invoke-interface {v6, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 107
    sget-object v3, Lcom/peel/control/b/l;->d:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "adding "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, " to commands"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 189
    :goto_0
    sget-object v3, Lcom/peel/control/b/l;->a:Lcom/peel/control/k;

    const/16 v4, 0x1e

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object p1, v5, v6

    move-object/from16 v0, p0

    invoke-virtual {v3, v4, v0, v5}, Lcom/peel/control/k;->a(ILjava/lang/Object;[Ljava/lang/Object;)V

    .line 191
    sget-object v3, Lcom/peel/control/b/l;->d:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "\n ********** ircommands.size(): "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 192
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "\n ********** fruit: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-object v3, Lcom/peel/control/b/l;->c:Lcom/peel/control/o;

    if-nez v3, :cond_f

    const-string/jumbo v3, "NULL"

    :goto_1
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 193
    sget-object v4, Lcom/peel/control/b/l;->d:Ljava/lang/String;

    invoke-static {v4, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 196
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    if-lez v3, :cond_10

    sget-object v3, Lcom/peel/control/b/l;->c:Lcom/peel/control/o;

    if-eqz v3, :cond_10

    sget-object v3, Lcom/peel/control/b/l;->c:Lcom/peel/control/o;

    invoke-virtual {v3, v2}, Lcom/peel/control/o;->a(Ljava/util/List;)Z

    move-result v2

    if-eqz v2, :cond_10

    .line 198
    sget-object v2, Lcom/peel/control/b/l;->a:Lcom/peel/control/k;

    const/16 v3, 0x1f

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    move-object/from16 v0, p0

    invoke-virtual {v2, v3, v0, v4}, Lcom/peel/control/k;->a(ILjava/lang/Object;[Ljava/lang/Object;)V

    .line 199
    const/4 v2, 0x1

    .line 202
    :goto_2
    return v2

    .line 109
    :cond_2
    sget-object v3, Lcom/peel/control/b/l;->d:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "commands does not contain command: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 111
    :cond_3
    if-eqz p1, :cond_5

    const/4 v3, 0x3

    if-lt v2, v3, :cond_5

    .line 112
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 113
    move-object/from16 v0, p1

    invoke-interface {v6, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 114
    move-object/from16 v0, p1

    invoke-interface {v6, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 116
    sget-object v3, Lcom/peel/control/b/l;->d:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "adding "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, " to commands"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, " -- underscoreCount >= 3"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 119
    :cond_4
    sget-object v3, Lcom/peel/control/b/l;->d:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "commands does not contain command: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, " -- underscoreCount >= 3"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 123
    :cond_5
    sget-object v2, Lcom/peel/control/b/l;->e:Ljava/util/regex/Pattern;

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v2

    const-string/jumbo v3, "$0,Delay,"

    invoke-virtual {v2, v3}, Ljava/util/regex/Matcher;->replaceAll(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 124
    const-string/jumbo v3, ","

    invoke-virtual {v2, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v7

    .line 125
    const/4 v2, 0x0

    move v3, v2

    :goto_3
    array-length v2, v7

    if-ge v3, v2, :cond_7

    .line 126
    sget-object v2, Lcom/peel/control/b/l;->f:Ljava/util/HashMap;

    aget-object v4, v7, v3

    invoke-virtual {v2, v4}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 127
    sget-object v2, Lcom/peel/control/b/l;->f:Ljava/util/HashMap;

    aget-object v4, v7, v3

    invoke-virtual {v2, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    aput-object v2, v7, v3

    .line 125
    :cond_6
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_3

    .line 130
    :cond_7
    new-instance v4, Ljava/util/ArrayList;

    array-length v2, v7

    invoke-direct {v4, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 132
    array-length v8, v7

    const/4 v2, 0x0

    move v5, v2

    :goto_4
    if-ge v5, v8, :cond_11

    aget-object v9, v7, v5

    .line 133
    sget-object v2, Lcom/peel/control/b/l;->d:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v10, "\n ******* irstring: "

    invoke-virtual {v3, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 135
    invoke-interface {v6, v9}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map;

    .line 136
    if-nez v2, :cond_9

    .line 138
    sget-object v2, Lcom/peel/control/b/l;->d:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v10, "commands.get("

    invoke-virtual {v3, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v9, ") NULL, continue/skip"

    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 132
    :cond_8
    :goto_5
    add-int/lit8 v2, v5, 0x1

    move v5, v2

    goto :goto_4

    .line 143
    :cond_9
    new-instance v10, Ljava/util/HashMap;

    invoke-direct {v10}, Ljava/util/HashMap;-><init>()V

    .line 144
    invoke-interface {v2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :goto_6
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_a

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Map$Entry;

    .line 145
    invoke-interface {v3}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v12

    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v10, v12, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_6

    .line 148
    :cond_a
    const-string/jumbo v3, "Delay"

    invoke-virtual {v3, v9}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_b

    .line 150
    const-wide/16 v12, 0x0

    cmp-long v3, p2, v12

    if-eqz v3, :cond_8

    .line 154
    const-wide/16 v12, -0x1

    cmp-long v3, p2, v12

    if-lez v3, :cond_b

    .line 157
    :try_start_0
    const-string/jumbo v3, "frequency"

    invoke-interface {v10, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v12

    mul-long v12, v12, p2

    const-wide/16 v14, 0x3e8

    div-long/2addr v12, v14

    .line 158
    const-string/jumbo v3, "ir"

    invoke-static {v12, v13}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v9

    invoke-interface {v10, v3, v9}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 166
    :cond_b
    :goto_7
    const-string/jumbo v9, "toggle"

    const-string/jumbo v3, "type"

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v9, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_e

    .line 169
    move-object/from16 v0, p0

    iget v2, v0, Lcom/peel/control/b/l;->g:I

    const/4 v3, 0x1

    if-ne v2, v3, :cond_d

    const-string/jumbo v2, "toggleframe1"

    invoke-interface {v10, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_d

    .line 170
    const-string/jumbo v2, "mainframe"

    const-string/jumbo v3, "toggleframe1"

    invoke-interface {v10, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v10, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 171
    const/4 v2, 0x2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/peel/control/b/l;->g:I

    .line 177
    :cond_c
    :goto_8
    sget-object v2, Lcom/peel/control/b/l;->d:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "\n ************ adding "

    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v9, " -- toggle type"

    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 178
    invoke-interface {v4, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_5

    .line 159
    :catch_0
    move-exception v3

    .line 161
    sget-object v9, Lcom/peel/control/b/l;->d:Ljava/lang/String;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v12, "custom delay unable to set: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    move-wide/from16 v0, p2

    invoke-virtual {v11, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v9, v11, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_7

    .line 172
    :cond_d
    move-object/from16 v0, p0

    iget v2, v0, Lcom/peel/control/b/l;->g:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_c

    const-string/jumbo v2, "toggleframe2"

    invoke-interface {v10, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_c

    .line 173
    const-string/jumbo v2, "mainframe"

    const-string/jumbo v3, "toggleframe2"

    invoke-interface {v10, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v10, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 174
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput v2, v0, Lcom/peel/control/b/l;->g:I

    goto :goto_8

    .line 182
    :cond_e
    sget-object v3, Lcom/peel/control/b/l;->d:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v11, "\n ************ adding "

    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v9, " -- regular type"

    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 183
    invoke-interface {v4, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_5

    .line 192
    :cond_f
    const-string/jumbo v3, "NOT NULL"

    goto/16 :goto_1

    .line 201
    :cond_10
    sget-object v2, Lcom/peel/control/b/l;->a:Lcom/peel/control/k;

    const/16 v3, 0x19

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "unable to send command ("

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, ")"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    move-object/from16 v0, p0

    invoke-virtual {v2, v3, v0, v4}, Lcom/peel/control/k;->a(ILjava/lang/Object;[Ljava/lang/Object;)V

    .line 202
    const/4 v2, 0x0

    goto/16 :goto_2

    :cond_11
    move-object v2, v4

    goto/16 :goto_0
.end method

.method public a(Ljava/net/URI;)Z
    .locals 12

    .prologue
    const-wide/16 v2, -0x1

    const/4 v4, 0x0

    .line 209
    const-string/jumbo v0, "live"

    invoke-virtual {p1}, Ljava/net/URI;->getScheme()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v4

    .line 236
    :goto_0
    return v0

    .line 211
    :cond_0
    invoke-virtual {p1}, Ljava/net/URI;->getQuery()Ljava/lang/String;

    move-result-object v0

    .line 213
    if-eqz v0, :cond_2

    .line 214
    const-string/jumbo v1, "&"

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    .line 215
    array-length v7, v6

    move v5, v4

    move-wide v0, v2

    :goto_1
    if-ge v5, v7, :cond_3

    aget-object v8, v6, v5

    .line 216
    const-string/jumbo v9, "="

    invoke-virtual {v8, v9}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v9

    .line 217
    const-string/jumbo v10, "delayms"

    invoke-virtual {v8, v4, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_1

    .line 219
    add-int/lit8 v0, v9, 0x1

    :try_start_0
    invoke-virtual {v8, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v0

    .line 215
    :cond_1
    :goto_2
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 220
    :catch_0
    move-exception v0

    move-wide v0, v2

    .line 222
    goto :goto_2

    :cond_2
    move-wide v0, v2

    .line 228
    :cond_3
    invoke-virtual {p1}, Ljava/net/URI;->getPath()Ljava/lang/String;

    move-result-object v4

    .line 229
    const/16 v5, 0x2f

    invoke-virtual {v4, v5}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v5

    add-int/lit8 v5, v5, 0x1

    invoke-virtual {v4, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4, v0, v1}, Lcom/peel/control/b/l;->a(Ljava/lang/String;J)Z

    .line 230
    const-string/jumbo v4, "command"

    invoke-virtual {p1}, Ljava/net/URI;->getHost()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_4

    .line 231
    const-string/jumbo v4, "Enter"

    invoke-virtual {p0, v4}, Lcom/peel/control/b/l;->a(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 232
    const-string/jumbo v4, "Delay"

    invoke-virtual {p0, v4, v0, v1}, Lcom/peel/control/b/l;->a(Ljava/lang/String;J)Z

    .line 233
    const-string/jumbo v0, "Enter"

    invoke-virtual {p0, v0, v2, v3}, Lcom/peel/control/b/l;->a(Ljava/lang/String;J)Z

    .line 236
    :cond_4
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public b(Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 87
    const-wide/16 v0, -0x1

    invoke-virtual {p0, p1, v0, v1}, Lcom/peel/control/b/l;->a(Ljava/lang/String;J)Z

    move-result v0

    return v0
.end method

.method public c(Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 89
    const-wide/16 v0, -0x1

    invoke-virtual {p0, p1, v0, v1}, Lcom/peel/control/b/l;->a(Ljava/lang/String;J)Z

    move-result v0

    return v0
.end method

.method public c()[Ljava/lang/String;
    .locals 3

    .prologue
    .line 240
    iget-object v0, p0, Lcom/peel/control/h;->b:Lcom/peel/data/g;

    invoke-virtual {v0}, Lcom/peel/data/g;->d()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 248
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 245
    :sswitch_0
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string/jumbo v2, "live"

    aput-object v2, v0, v1

    goto :goto_0

    .line 240
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_0
        0xa -> :sswitch_0
        0x14 -> :sswitch_0
    .end sparse-switch
.end method
