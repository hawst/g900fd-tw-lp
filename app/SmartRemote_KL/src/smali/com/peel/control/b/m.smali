.class public Lcom/peel/control/b/m;
.super Lcom/peel/control/b/k;


# static fields
.field public static c:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static d:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final e:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 45
    const-class v0, Lcom/peel/control/b/m;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/peel/control/b/m;->e:Ljava/lang/String;

    .line 51
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/peel/control/b/m;->c:Ljava/util/HashMap;

    .line 52
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/peel/control/b/m;->d:Ljava/util/HashMap;

    .line 55
    sget-object v0, Lcom/peel/control/b/m;->c:Ljava/util/HashMap;

    const-string/jumbo v1, "Home"

    const-string/jumbo v2, "Home"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 56
    sget-object v0, Lcom/peel/control/b/m;->c:Ljava/util/HashMap;

    const-string/jumbo v1, "Rewind"

    const-string/jumbo v2, "Rev"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 57
    sget-object v0, Lcom/peel/control/b/m;->c:Ljava/util/HashMap;

    const-string/jumbo v1, "Fast_Forward"

    const-string/jumbo v2, "Fwd"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 58
    sget-object v0, Lcom/peel/control/b/m;->c:Ljava/util/HashMap;

    const-string/jumbo v1, "Play"

    const-string/jumbo v2, "Play"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 59
    sget-object v0, Lcom/peel/control/b/m;->c:Ljava/util/HashMap;

    const-string/jumbo v1, "Select"

    const-string/jumbo v2, "Select"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 60
    sget-object v0, Lcom/peel/control/b/m;->c:Ljava/util/HashMap;

    const-string/jumbo v1, "Navigate_Left"

    const-string/jumbo v2, "Left"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 61
    sget-object v0, Lcom/peel/control/b/m;->c:Ljava/util/HashMap;

    const-string/jumbo v1, "Navigate_Right"

    const-string/jumbo v2, "Right"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 62
    sget-object v0, Lcom/peel/control/b/m;->c:Ljava/util/HashMap;

    const-string/jumbo v1, "Navigate_Up"

    const-string/jumbo v2, "Up"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 63
    sget-object v0, Lcom/peel/control/b/m;->c:Ljava/util/HashMap;

    const-string/jumbo v1, "Navigate_Down"

    const-string/jumbo v2, "Down"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 64
    sget-object v0, Lcom/peel/control/b/m;->c:Ljava/util/HashMap;

    const-string/jumbo v1, "Back"

    const-string/jumbo v2, "Back"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 65
    sget-object v0, Lcom/peel/control/b/m;->c:Ljava/util/HashMap;

    const-string/jumbo v1, "replay"

    const-string/jumbo v2, "InstantReplay"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 66
    sget-object v0, Lcom/peel/control/b/m;->c:Ljava/util/HashMap;

    const-string/jumbo v1, "Info"

    const-string/jumbo v2, "Info"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 67
    sget-object v0, Lcom/peel/control/b/m;->c:Ljava/util/HashMap;

    const-string/jumbo v1, "Enter"

    const-string/jumbo v2, "Enter"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 68
    sget-object v0, Lcom/peel/control/b/m;->c:Ljava/util/HashMap;

    const-string/jumbo v1, "Backspace"

    const-string/jumbo v2, "Backspace"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 69
    sget-object v0, Lcom/peel/control/b/m;->c:Ljava/util/HashMap;

    const-string/jumbo v1, "Search"

    const-string/jumbo v2, "Search"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 70
    return-void
.end method

.method public constructor <init>(ILjava/lang/String;ZLjava/lang/String;ILandroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;)V
    .locals 9

    .prologue
    .line 78
    const/4 v3, 0x1

    const-string/jumbo v7, "Roku"

    move-object v0, p0

    move v1, p1

    move-object v2, p2

    move-object v4, p4

    move v5, p5

    move-object v6, p6

    move-object/from16 v8, p8

    invoke-direct/range {v0 .. v8}, Lcom/peel/control/b/k;-><init>(ILjava/lang/String;ZLjava/lang/String;ILandroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;)V

    .line 79
    return-void
.end method

.method public constructor <init>(Lcom/peel/data/g;)V
    .locals 0

    .prologue
    .line 73
    invoke-direct {p0, p1}, Lcom/peel/control/b/k;-><init>(Lcom/peel/data/g;)V

    .line 74
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;ILjava/lang/String;ZLjava/lang/String;ILandroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;)V
    .locals 10

    .prologue
    .line 83
    const/4 v4, 0x1

    const-string/jumbo v8, "Roku"

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    move-object v5, p5

    move/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v9, p9

    invoke-direct/range {v0 .. v9}, Lcom/peel/control/b/k;-><init>(Ljava/lang/String;ILjava/lang/String;ZLjava/lang/String;ILandroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;)V

    .line 84
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 106
    sget-object v0, Lcom/peel/control/b/m;->c:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public c(Ljava/lang/String;)Z
    .locals 3

    .prologue
    .line 91
    sget-object v0, Lcom/peel/control/b/m;->e:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "send command: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/peel/control/b/n;

    invoke-direct {v2, p0, p1}, Lcom/peel/control/b/n;-><init>(Lcom/peel/control/b/m;Ljava/lang/String;)V

    invoke-static {v0, v1, v2}, Lcom/peel/util/i;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    .line 103
    const/4 v0, 0x1

    return v0
.end method
