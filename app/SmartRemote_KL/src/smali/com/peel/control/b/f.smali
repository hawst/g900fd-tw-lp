.class Lcom/peel/control/b/f;
.super Lcom/peel/util/t;


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic b:Lcom/peel/control/b/b;


# direct methods
.method constructor <init>(Lcom/peel/control/b/b;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 216
    iput-object p1, p0, Lcom/peel/control/b/f;->b:Lcom/peel/control/b/b;

    iput-object p2, p0, Lcom/peel/control/b/f;->a:Ljava/lang/String;

    invoke-direct {p0}, Lcom/peel/util/t;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 218
    iget-boolean v0, p0, Lcom/peel/control/b/f;->i:Z

    if-nez v0, :cond_0

    .line 219
    invoke-static {}, Lcom/peel/control/b/b;->q()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/control/b/f;->k:Ljava/lang/String;

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 220
    sget-object v0, Lcom/peel/control/h;->a:Lcom/peel/control/k;

    const/16 v1, 0x19

    new-array v2, v8, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/peel/control/b/f;->k:Ljava/lang/String;

    aput-object v3, v2, v7

    invoke-virtual {v0, v1, p0, v2}, Lcom/peel/control/k;->a(ILjava/lang/Object;[Ljava/lang/Object;)V

    .line 226
    :goto_0
    return-void

    .line 224
    :cond_0
    invoke-static {}, Lcom/peel/control/b/b;->q()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "PROCESS_KEY_URL: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "http://%s:%d/remote/processKey?key=%s&hold=keyPress"

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/peel/control/b/f;->b:Lcom/peel/control/b/b;

    invoke-virtual {v4}, Lcom/peel/control/b/b;->i()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v7

    iget-object v4, p0, Lcom/peel/control/b/f;->b:Lcom/peel/control/b/b;

    invoke-virtual {v4}, Lcom/peel/control/b/b;->j()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v8

    const/4 v4, 0x2

    sget-object v5, Lcom/peel/control/b/b;->c:Ljava/util/Map;

    iget-object v6, p0, Lcom/peel/control/b/f;->a:Ljava/lang/String;

    invoke-interface {v5, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/peel/control/b/f;->j:Ljava/lang/Object;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 225
    sget-object v0, Lcom/peel/control/h;->a:Lcom/peel/control/k;

    const/16 v1, 0x1f

    new-array v2, v8, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/peel/control/b/f;->a:Ljava/lang/String;

    aput-object v3, v2, v7

    invoke-virtual {v0, v1, p0, v2}, Lcom/peel/control/k;->a(ILjava/lang/Object;[Ljava/lang/Object;)V

    goto :goto_0
.end method
