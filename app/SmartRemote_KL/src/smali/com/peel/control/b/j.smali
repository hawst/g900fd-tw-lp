.class Lcom/peel/control/b/j;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic b:Lcom/peel/util/t;

.field final synthetic c:Lcom/peel/control/b/b;


# direct methods
.method constructor <init>(Lcom/peel/control/b/b;Ljava/lang/String;Lcom/peel/util/t;)V
    .locals 0

    .prologue
    .line 437
    iput-object p1, p0, Lcom/peel/control/b/j;->c:Lcom/peel/control/b/b;

    iput-object p2, p0, Lcom/peel/control/b/j;->a:Ljava/lang/String;

    iput-object p3, p0, Lcom/peel/control/b/j;->b:Lcom/peel/util/t;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 441
    const-string/jumbo v0, "st:urn:schemas-upnp-org:device:MediaRenderer:1"

    iget-object v1, p0, Lcom/peel/control/b/j;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/peel/control/c/a;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 444
    if-eqz v0, :cond_1

    .line 445
    iget-object v1, p0, Lcom/peel/control/b/j;->c:Lcom/peel/control/b/b;

    invoke-virtual {v1}, Lcom/peel/control/b/b;->i()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 448
    iget-object v1, p0, Lcom/peel/control/b/j;->c:Lcom/peel/control/b/b;

    invoke-virtual {v1}, Lcom/peel/control/b/b;->l()Lcom/peel/data/g;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/peel/data/g;->c(Ljava/lang/String;)V

    .line 449
    iget-object v0, p0, Lcom/peel/control/b/j;->b:Lcom/peel/util/t;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v3, v1, v2}, Lcom/peel/util/t;->a(ZLjava/lang/Object;Ljava/lang/String;)V

    .line 456
    :goto_0
    return-void

    .line 451
    :cond_0
    iget-object v0, p0, Lcom/peel/control/b/j;->b:Lcom/peel/util/t;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string/jumbo v2, "same IP, no update is needed"

    invoke-virtual {v0, v3, v1, v2}, Lcom/peel/util/t;->a(ZLjava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 455
    :cond_1
    iget-object v0, p0, Lcom/peel/control/b/j;->b:Lcom/peel/util/t;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "unknown error, ip coming back NULL for usn: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/peel/control/b/j;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v4, v1, v2}, Lcom/peel/util/t;->a(ZLjava/lang/Object;Ljava/lang/String;)V

    goto :goto_0
.end method
