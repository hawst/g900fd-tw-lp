.class public Lcom/peel/control/d;
.super Lcom/peel/control/f;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 448
    invoke-direct {p0}, Lcom/peel/control/f;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/peel/control/a;I)Z
    .locals 8

    .prologue
    const/4 v7, 0x2

    const/4 v3, 0x0

    const/4 v4, 0x1

    .line 451
    const/4 v2, 0x0

    .line 453
    iget-object v0, p1, Lcom/peel/control/a;->a:Lcom/peel/data/d;

    invoke-virtual {v0}, Lcom/peel/data/d;->c()Ljava/lang/Iterable;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_0
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    .line 454
    sget-object v1, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    const-string/jumbo v6, "id"

    invoke-interface {v0, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Lcom/peel/control/am;->c(Ljava/lang/String;)Lcom/peel/control/h;

    move-result-object v1

    .line 455
    if-eqz v1, :cond_0

    .line 457
    if-eqz p2, :cond_9

    .line 459
    invoke-virtual {v1}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v0

    const-string/jumbo v6, "PowerOn"

    invoke-virtual {v0, v6}, Lcom/peel/data/g;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 460
    const-string/jumbo v0, "PowerOn"

    invoke-virtual {v1, v0}, Lcom/peel/control/h;->c(Ljava/lang/String;)Z

    .line 466
    :cond_1
    :goto_1
    if-nez v2, :cond_9

    invoke-virtual {v1}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v0

    const-string/jumbo v6, "Delay"

    invoke-virtual {v0, v6}, Lcom/peel/data/g;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-virtual {v1}, Lcom/peel/control/h;->a()Z

    move-result v0

    if-eqz v0, :cond_9

    move-object v0, v1

    .line 471
    :goto_2
    invoke-virtual {v1, v4}, Lcom/peel/control/h;->a(I)V

    move-object v2, v0

    .line 472
    goto :goto_0

    .line 461
    :cond_2
    if-ne p2, v7, :cond_1

    invoke-virtual {v1}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/data/g;->g()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {v1}, Lcom/peel/control/h;->o()I

    move-result v0

    if-nez v0, :cond_1

    .line 463
    const-string/jumbo v0, "Power"

    invoke-virtual {v1, v0}, Lcom/peel/control/h;->c(Ljava/lang/String;)Z

    goto :goto_1

    .line 474
    :cond_3
    if-eqz p2, :cond_8

    move v1, v3

    .line 476
    :goto_3
    iget-object v0, p1, Lcom/peel/control/a;->a:Lcom/peel/data/d;

    invoke-virtual {v0}, Lcom/peel/data/d;->d()I

    move-result v0

    if-ge v1, v0, :cond_7

    .line 477
    iget-object v0, p1, Lcom/peel/control/a;->a:Lcom/peel/data/d;

    invoke-virtual {v0, v1}, Lcom/peel/data/d;->a(I)Ljava/util/Map;

    move-result-object v5

    .line 478
    sget-object v6, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    const-string/jumbo v0, "id"

    invoke-interface {v5, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v6, v0}, Lcom/peel/control/am;->c(Ljava/lang/String;)Lcom/peel/control/h;

    move-result-object v6

    .line 479
    if-nez v6, :cond_5

    .line 476
    :cond_4
    :goto_4
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    .line 481
    :cond_5
    const-string/jumbo v0, "input"

    invoke-interface {v5, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 482
    if-eqz v0, :cond_4

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v5

    if-eqz v5, :cond_4

    .line 485
    if-nez v3, :cond_6

    if-eqz v2, :cond_6

    .line 487
    const-string/jumbo v3, "Delay"

    invoke-virtual {v2, v3}, Lcom/peel/control/h;->c(Ljava/lang/String;)Z

    move v3, v4

    .line 489
    :cond_6
    invoke-virtual {v6, v0}, Lcom/peel/control/h;->b(Ljava/lang/String;)Z

    goto :goto_4

    .line 493
    :cond_7
    if-ne v3, v4, :cond_8

    if-eqz v2, :cond_8

    .line 495
    const-string/jumbo v0, "Delay"

    invoke-virtual {v2, v0}, Lcom/peel/control/h;->c(Ljava/lang/String;)Z

    .line 499
    :cond_8
    invoke-virtual {p1, v7}, Lcom/peel/control/a;->c(I)V

    .line 506
    return v4

    :cond_9
    move-object v0, v2

    goto :goto_2
.end method

.method public a(Lcom/peel/control/a;Ljava/lang/String;)Z
    .locals 3

    .prologue
    .line 512
    sget-object v0, Lcom/peel/data/b;->a:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 513
    invoke-static {p1}, Lcom/peel/control/a;->c(Lcom/peel/control/a;)Lcom/peel/control/h;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 520
    :goto_1
    return v0

    .line 513
    :cond_0
    invoke-static {p1}, Lcom/peel/control/a;->c(Lcom/peel/control/a;)Lcom/peel/control/h;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/peel/control/h;->c(Ljava/lang/String;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0

    .line 514
    :cond_1
    invoke-static {p1}, Lcom/peel/control/a;->d(Lcom/peel/control/a;)Lcom/peel/control/h;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 515
    invoke-static {p1}, Lcom/peel/control/a;->d(Lcom/peel/control/a;)Lcom/peel/control/h;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/peel/control/h;->c(Ljava/lang/String;)Z

    move-result v0

    goto :goto_1

    .line 519
    :cond_2
    invoke-static {}, Lcom/peel/control/a;->h()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "control device is null for activity: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/peel/control/a;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " command: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 520
    const/4 v0, 0x0

    goto :goto_1
.end method
