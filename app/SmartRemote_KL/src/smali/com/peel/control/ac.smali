.class Lcom/peel/control/ac;
.super Lcom/peel/util/t;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/peel/util/t",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/peel/control/ab;


# direct methods
.method constructor <init>(Lcom/peel/control/ab;)V
    .locals 0

    .prologue
    .line 92
    iput-object p1, p0, Lcom/peel/control/ac;->a:Lcom/peel/control/ab;

    invoke-direct {p0}, Lcom/peel/util/t;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 11

    .prologue
    const/4 v10, 0x0

    const/4 v4, 0x0

    .line 95
    iget-boolean v0, p0, Lcom/peel/control/ac;->i:Z

    if-nez v0, :cond_0

    .line 96
    iget-object v0, p0, Lcom/peel/control/ac;->a:Lcom/peel/control/ab;

    iget-object v0, v0, Lcom/peel/control/ab;->c:Lcom/peel/util/t;

    iget-boolean v1, p0, Lcom/peel/control/ac;->i:Z

    iget-object v2, p0, Lcom/peel/control/ac;->j:Ljava/lang/Object;

    iget-object v3, p0, Lcom/peel/control/ac;->k:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3}, Lcom/peel/util/t;->a(ZLjava/lang/Object;Ljava/lang/String;)V

    .line 97
    invoke-static {}, Lcom/peel/control/aa;->a()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/control/ac;->k:Ljava/lang/String;

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 120
    :goto_0
    return-void

    .line 102
    :cond_0
    :try_start_0
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 104
    iget-object v0, p0, Lcom/peel/control/ac;->j:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/peel/control/aa;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 105
    invoke-static {}, Lcom/peel/control/aa;->b()Lorg/codehaus/jackson/map/ObjectMapper;

    move-result-object v1

    iget-object v0, p0, Lcom/peel/control/ac;->j:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    const-class v2, Ljava/util/HashMap;

    invoke-virtual {v1, v0, v2}, Lorg/codehaus/jackson/map/ObjectMapper;->readValue(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/HashMap;

    const-string/jumbo v1, "providerBrand"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 106
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v6

    move v3, v4

    :goto_1
    if-ge v3, v6, :cond_2

    .line 107
    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/HashMap;

    .line 108
    new-instance v7, Lcom/peel/f/a;

    const-string/jumbo v2, "id"

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v8

    const-string/jumbo v2, "brandName"

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    const-string/jumbo v9, "rank"

    invoke-virtual {v1, v9}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    invoke-direct {v7, v8, v2, v1}, Lcom/peel/f/a;-><init>(ILjava/lang/String;I)V

    invoke-interface {v5, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 106
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_1

    .line 111
    :cond_1
    invoke-static {}, Lcom/peel/control/aa;->b()Lorg/codehaus/jackson/map/ObjectMapper;

    move-result-object v1

    iget-object v0, p0, Lcom/peel/control/ac;->j:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    const-class v2, Ljava/util/HashMap;

    invoke-virtual {v1, v0, v2}, Lorg/codehaus/jackson/map/ObjectMapper;->readValue(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/HashMap;

    const-string/jumbo v1, "providerBrand"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/HashMap;

    .line 112
    new-instance v2, Lcom/peel/f/a;

    const-string/jumbo v1, "id"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    const-string/jumbo v1, "brandName"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    const-string/jumbo v6, "rank"

    invoke-virtual {v0, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    invoke-direct {v2, v3, v1, v0}, Lcom/peel/f/a;-><init>(ILjava/lang/String;I)V

    invoke-interface {v5, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 115
    :cond_2
    iget-object v0, p0, Lcom/peel/control/ac;->a:Lcom/peel/control/ab;

    iget-object v0, v0, Lcom/peel/control/ab;->c:Lcom/peel/util/t;

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v5, v2}, Lcom/peel/util/t;->a(ZLjava/lang/Object;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 116
    :catch_0
    move-exception v0

    .line 117
    invoke-static {}, Lcom/peel/control/aa;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {}, Lcom/peel/control/aa;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 118
    iget-object v1, p0, Lcom/peel/control/ac;->a:Lcom/peel/control/ab;

    iget-object v1, v1, Lcom/peel/control/ab;->c:Lcom/peel/util/t;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v4, v10, v0}, Lcom/peel/util/t;->a(ZLjava/lang/Object;Ljava/lang/String;)V

    goto/16 :goto_0
.end method
