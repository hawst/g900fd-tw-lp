.class Lcom/peel/control/v;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/media/SoundPool$OnLoadCompleteListener;


# instance fields
.field final synthetic a:Lcom/peel/control/u;


# direct methods
.method constructor <init>(Lcom/peel/control/u;)V
    .locals 0

    .prologue
    .line 45
    iput-object p1, p0, Lcom/peel/control/v;->a:Lcom/peel/control/u;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onLoadComplete(Landroid/media/SoundPool;II)V
    .locals 4

    .prologue
    .line 52
    iget-object v0, p0, Lcom/peel/control/v;->a:Lcom/peel/control/u;

    invoke-static {v0}, Lcom/peel/control/u;->a(Lcom/peel/control/u;)Ljava/util/Map;

    move-result-object v2

    monitor-enter v2

    .line 53
    :try_start_0
    iget-object v0, p0, Lcom/peel/control/v;->a:Lcom/peel/control/u;

    invoke-static {v0}, Lcom/peel/control/u;->b(Lcom/peel/control/u;)Landroid/util/SparseArray;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/control/y;

    .line 54
    if-eqz v0, :cond_0

    .line 55
    iget-object v0, p0, Lcom/peel/control/v;->a:Lcom/peel/control/u;

    invoke-static {v0}, Lcom/peel/control/u;->a(Lcom/peel/control/u;)Ljava/util/Map;

    move-result-object v1

    iget-object v0, p0, Lcom/peel/control/v;->a:Lcom/peel/control/u;

    invoke-static {v0}, Lcom/peel/control/u;->b(Lcom/peel/control/u;)Landroid/util/SparseArray;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/control/y;

    iget-object v0, v0, Lcom/peel/control/y;->a:Ljava/lang/String;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/MemoryFile;

    .line 56
    if-eqz v0, :cond_0

    .line 57
    iget-object v1, p0, Lcom/peel/control/v;->a:Lcom/peel/control/u;

    invoke-static {v1}, Lcom/peel/control/u;->a(Lcom/peel/control/u;)Ljava/util/Map;

    move-result-object v3

    iget-object v1, p0, Lcom/peel/control/v;->a:Lcom/peel/control/u;

    invoke-static {v1}, Lcom/peel/control/u;->b(Lcom/peel/control/u;)Landroid/util/SparseArray;

    move-result-object v1

    invoke-virtual {v1, p2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/peel/control/y;

    iget-object v1, v1, Lcom/peel/control/y;->a:Ljava/lang/String;

    invoke-interface {v3, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 58
    invoke-virtual {v0}, Landroid/os/MemoryFile;->close()V

    .line 61
    :cond_0
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 64
    iget-object v0, p0, Lcom/peel/control/v;->a:Lcom/peel/control/u;

    invoke-static {v0}, Lcom/peel/control/u;->c(Lcom/peel/control/u;)Ljava/util/concurrent/atomic/AtomicInteger;

    move-result-object v1

    monitor-enter v1

    .line 65
    :try_start_1
    iget-object v0, p0, Lcom/peel/control/v;->a:Lcom/peel/control/u;

    invoke-static {v0}, Lcom/peel/control/u;->c(Lcom/peel/control/u;)Ljava/util/concurrent/atomic/AtomicInteger;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->decrementAndGet()I

    move-result v0

    if-lez v0, :cond_1

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    .line 73
    :goto_0
    return-void

    .line 61
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 66
    :cond_1
    :try_start_3
    iget-object v0, p0, Lcom/peel/control/v;->a:Lcom/peel/control/u;

    invoke-static {v0}, Lcom/peel/control/u;->c(Lcom/peel/control/u;)Ljava/util/concurrent/atomic/AtomicInteger;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V

    .line 67
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    .line 69
    iget-object v0, p0, Lcom/peel/control/v;->a:Lcom/peel/control/u;

    invoke-static {v0}, Lcom/peel/control/u;->d(Lcom/peel/control/u;)Ljava/util/List;

    move-result-object v1

    monitor-enter v1

    .line 70
    :try_start_4
    iget-object v2, p0, Lcom/peel/control/v;->a:Lcom/peel/control/u;

    iget-object v0, p0, Lcom/peel/control/v;->a:Lcom/peel/control/u;

    invoke-static {v0}, Lcom/peel/control/u;->d(Lcom/peel/control/u;)Ljava/util/List;

    move-result-object v0

    iget-object v3, p0, Lcom/peel/control/v;->a:Lcom/peel/control/u;

    invoke-static {v3}, Lcom/peel/control/u;->d(Lcom/peel/control/u;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    new-array v3, v3, [Ljava/lang/String;

    invoke-interface {v0, v3}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    invoke-static {v2, v0}, Lcom/peel/control/u;->a(Lcom/peel/control/u;[Ljava/lang/String;)V

    .line 71
    iget-object v0, p0, Lcom/peel/control/v;->a:Lcom/peel/control/u;

    invoke-static {v0}, Lcom/peel/control/u;->d(Lcom/peel/control/u;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 72
    monitor-exit v1

    goto :goto_0

    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    throw v0

    .line 67
    :catchall_2
    move-exception v0

    :try_start_5
    monitor-exit v1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    throw v0
.end method
