.class public Lcom/peel/control/RoomControl;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/peel/control/RoomControl;",
            ">;"
        }
    .end annotation
.end field

.field private static final c:Ljava/lang/String;

.field private static final d:[Lcom/peel/control/av;


# instance fields
.field protected a:Lcom/peel/data/at;

.field private final b:Ljava/util/concurrent/atomic/AtomicInteger;

.field private volatile e:Lcom/peel/control/av;

.field private final f:Lcom/peel/util/s;

.field private g:Lcom/peel/control/a;

.field private h:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 30
    new-instance v0, Lcom/peel/control/as;

    invoke-direct {v0}, Lcom/peel/control/as;-><init>()V

    sput-object v0, Lcom/peel/control/RoomControl;->CREATOR:Landroid/os/Parcelable$Creator;

    .line 35
    const-class v0, Lcom/peel/control/RoomControl;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/peel/control/RoomControl;->c:Ljava/lang/String;

    .line 36
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/peel/control/av;

    const/4 v1, 0x0

    new-instance v2, Lcom/peel/control/aw;

    invoke-direct {v2}, Lcom/peel/control/aw;-><init>()V

    aput-object v2, v0, v1

    const/4 v1, 0x1

    new-instance v2, Lcom/peel/control/au;

    invoke-direct {v2}, Lcom/peel/control/au;-><init>()V

    aput-object v2, v0, v1

    sput-object v0, Lcom/peel/control/RoomControl;->d:[Lcom/peel/control/av;

    return-void
.end method

.method protected constructor <init>(Lcom/peel/data/at;)V
    .locals 2

    .prologue
    .line 77
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    iput-object v0, p0, Lcom/peel/control/RoomControl;->b:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 37
    sget-object v0, Lcom/peel/control/RoomControl;->d:[Lcom/peel/control/av;

    iget-object v1, p0, Lcom/peel/control/RoomControl;->b:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v1

    aget-object v0, v0, v1

    iput-object v0, p0, Lcom/peel/control/RoomControl;->e:Lcom/peel/control/av;

    .line 38
    new-instance v0, Lcom/peel/control/at;

    invoke-direct {v0, p0}, Lcom/peel/control/at;-><init>(Lcom/peel/control/RoomControl;)V

    iput-object v0, p0, Lcom/peel/control/RoomControl;->f:Lcom/peel/util/s;

    .line 77
    iput-object p1, p0, Lcom/peel/control/RoomControl;->a:Lcom/peel/data/at;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 73
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    iput-object v0, p0, Lcom/peel/control/RoomControl;->b:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 37
    sget-object v0, Lcom/peel/control/RoomControl;->d:[Lcom/peel/control/av;

    iget-object v1, p0, Lcom/peel/control/RoomControl;->b:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v1

    aget-object v0, v0, v1

    iput-object v0, p0, Lcom/peel/control/RoomControl;->e:Lcom/peel/control/av;

    .line 38
    new-instance v0, Lcom/peel/control/at;

    invoke-direct {v0, p0}, Lcom/peel/control/at;-><init>(Lcom/peel/control/RoomControl;)V

    iput-object v0, p0, Lcom/peel/control/RoomControl;->f:Lcom/peel/util/s;

    .line 74
    new-instance v0, Lcom/peel/data/at;

    invoke-direct {v0, p1}, Lcom/peel/data/at;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/peel/control/RoomControl;->a:Lcom/peel/data/at;

    .line 75
    return-void
.end method

.method public static a(Landroid/os/Parcel;)Lcom/peel/control/RoomControl;
    .locals 8

    .prologue
    .line 110
    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 111
    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 112
    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 114
    new-instance v3, Lcom/peel/data/at;

    invoke-direct {v3, v0, v1, v2}, Lcom/peel/data/at;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    .line 115
    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 116
    if-lez v0, :cond_0

    .line 117
    new-array v0, v0, [Ljava/lang/String;

    invoke-virtual {v3, v0}, Lcom/peel/data/at;->a([Ljava/lang/String;)V

    .line 118
    invoke-virtual {v3}, Lcom/peel/data/at;->c()[Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/os/Parcel;->readStringArray([Ljava/lang/String;)V

    .line 120
    :cond_0
    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-lez v0, :cond_1

    .line 121
    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/peel/data/at;->b(Ljava/lang/String;)V

    .line 123
    :cond_1
    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-lez v0, :cond_2

    .line 124
    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/peel/data/at;->c(Ljava/lang/String;)V

    .line 127
    :cond_2
    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/peel/data/at;->d(Ljava/lang/String;)V

    .line 129
    new-instance v1, Lcom/peel/control/rooms/SimpleRoomControl;

    invoke-direct {v1, v3}, Lcom/peel/control/rooms/SimpleRoomControl;-><init>(Lcom/peel/data/at;)V

    .line 130
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    .line 131
    invoke-virtual {v1}, Lcom/peel/control/RoomControl;->c()[Lcom/peel/control/a;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 132
    invoke-virtual {v1}, Lcom/peel/control/RoomControl;->c()[Lcom/peel/control/a;

    move-result-object v3

    array-length v4, v3

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v4, :cond_5

    aget-object v5, v3, v0

    .line 133
    invoke-virtual {v5}, Lcom/peel/control/a;->d()[Ljava/lang/String;

    move-result-object v6

    .line 134
    if-eqz v6, :cond_3

    array-length v7, v6

    if-nez v7, :cond_4

    .line 132
    :cond_3
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 135
    :cond_4
    invoke-static {v6}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v6

    invoke-interface {v2, v6}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 136
    iget-object v6, v1, Lcom/peel/control/RoomControl;->f:Lcom/peel/util/s;

    invoke-virtual {v5, v6}, Lcom/peel/control/a;->a(Lcom/peel/util/s;)V

    goto :goto_1

    .line 138
    :cond_5
    invoke-interface {v2}, Ljava/util/Set;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/String;

    invoke-interface {v2, v0}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    iput-object v0, v1, Lcom/peel/control/RoomControl;->h:[Ljava/lang/String;

    .line 140
    :cond_6
    return-object v1
.end method

.method public static a(Lcom/peel/data/at;)Lcom/peel/control/RoomControl;
    .locals 8

    .prologue
    .line 95
    new-instance v1, Lcom/peel/control/rooms/SimpleRoomControl;

    invoke-direct {v1, p0}, Lcom/peel/control/rooms/SimpleRoomControl;-><init>(Lcom/peel/data/at;)V

    .line 96
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    .line 97
    invoke-virtual {p0}, Lcom/peel/data/at;->c()[Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 98
    invoke-virtual {v1}, Lcom/peel/control/RoomControl;->c()[Lcom/peel/control/a;

    move-result-object v3

    array-length v4, v3

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v4, :cond_2

    aget-object v5, v3, v0

    .line 99
    invoke-virtual {v5}, Lcom/peel/control/a;->d()[Ljava/lang/String;

    move-result-object v6

    .line 100
    if-eqz v6, :cond_0

    array-length v7, v6

    if-nez v7, :cond_1

    .line 98
    :cond_0
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 101
    :cond_1
    invoke-static {v6}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v6

    invoke-interface {v2, v6}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 102
    iget-object v6, v1, Lcom/peel/control/RoomControl;->f:Lcom/peel/util/s;

    invoke-virtual {v5, v6}, Lcom/peel/control/a;->a(Lcom/peel/util/s;)V

    goto :goto_1

    .line 104
    :cond_2
    invoke-interface {v2}, Ljava/util/Set;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/String;

    invoke-interface {v2, v0}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    iput-object v0, v1, Lcom/peel/control/RoomControl;->h:[Ljava/lang/String;

    .line 106
    :cond_3
    return-object v1
.end method

.method static synthetic a(Lcom/peel/control/RoomControl;)Lcom/peel/control/a;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lcom/peel/control/RoomControl;->g:Lcom/peel/control/a;

    return-object v0
.end method

.method static synthetic a(Lcom/peel/control/RoomControl;Lcom/peel/control/a;)Lcom/peel/control/a;
    .locals 0

    .prologue
    .line 23
    iput-object p1, p0, Lcom/peel/control/RoomControl;->g:Lcom/peel/control/a;

    return-object p1
.end method

.method private a([Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 261
    if-nez p1, :cond_0

    .line 262
    sget-object v0, Lcom/peel/control/RoomControl;->c:Ljava/lang/String;

    const-string/jumbo v1, "activity schemes NULL, cannot append schemes"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 281
    :goto_0
    return-void

    .line 266
    :cond_0
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 267
    iget-object v1, p0, Lcom/peel/control/RoomControl;->h:[Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 268
    iget-object v1, p0, Lcom/peel/control/RoomControl;->h:[Ljava/lang/String;

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 276
    :cond_1
    invoke-static {p1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 277
    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v1

    new-array v1, v1, [Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    iput-object v0, p0, Lcom/peel/control/RoomControl;->h:[Ljava/lang/String;

    goto :goto_0
.end method

.method static synthetic i()Ljava/lang/String;
    .locals 1

    .prologue
    .line 23
    sget-object v0, Lcom/peel/control/RoomControl;->c:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public a()Lcom/peel/data/at;
    .locals 1

    .prologue
    .line 145
    iget-object v0, p0, Lcom/peel/control/RoomControl;->a:Lcom/peel/data/at;

    return-object v0
.end method

.method public a(Lcom/peel/control/a;)V
    .locals 2

    .prologue
    .line 174
    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {p1}, Lcom/peel/control/a;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/peel/control/am;->e(Ljava/lang/String;)Lcom/peel/control/a;

    move-result-object v0

    if-nez v0, :cond_0

    .line 175
    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    iget-object v1, p0, Lcom/peel/control/RoomControl;->a:Lcom/peel/data/at;

    invoke-virtual {v1}, Lcom/peel/data/at;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/peel/control/am;->a(Lcom/peel/control/a;Ljava/lang/String;)V

    .line 177
    :cond_0
    iget-object v0, p0, Lcom/peel/control/RoomControl;->a:Lcom/peel/data/at;

    invoke-virtual {p1}, Lcom/peel/control/a;->c()Lcom/peel/data/d;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/peel/data/at;->a(Lcom/peel/data/d;)V

    .line 179
    iget-object v0, p0, Lcom/peel/control/RoomControl;->f:Lcom/peel/util/s;

    invoke-virtual {p1, v0}, Lcom/peel/control/a;->a(Lcom/peel/util/s;)V

    .line 181
    invoke-virtual {p1}, Lcom/peel/control/a;->d()[Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/peel/control/RoomControl;->a([Ljava/lang/String;)V

    .line 191
    return-void
.end method

.method public a(Lcom/peel/control/o;)V
    .locals 2

    .prologue
    .line 150
    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {p1}, Lcom/peel/control/o;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/peel/control/am;->b(Ljava/lang/String;)Lcom/peel/control/o;

    move-result-object v0

    if-nez v0, :cond_0

    .line 151
    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v0, p0, p1}, Lcom/peel/control/am;->a(Lcom/peel/control/RoomControl;Lcom/peel/control/o;)V

    .line 153
    :cond_0
    iget-object v0, p0, Lcom/peel/control/RoomControl;->a:Lcom/peel/data/at;

    invoke-virtual {p1}, Lcom/peel/control/o;->d()Lcom/peel/data/h;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/peel/data/at;->a(Lcom/peel/data/h;)V

    .line 159
    return-void
.end method

.method public a(I)Z
    .locals 1

    .prologue
    .line 289
    iget-object v0, p0, Lcom/peel/control/RoomControl;->e:Lcom/peel/control/av;

    invoke-virtual {v0, p0, p1}, Lcom/peel/control/av;->a(Lcom/peel/control/RoomControl;I)Z

    move-result v0

    return v0
.end method

.method public a(Lcom/peel/control/a;I)Z
    .locals 1

    .prologue
    .line 287
    iget-object v0, p0, Lcom/peel/control/RoomControl;->e:Lcom/peel/control/av;

    invoke-virtual {v0, p0, p1, p2}, Lcom/peel/control/av;->a(Lcom/peel/control/RoomControl;Lcom/peel/control/a;I)Z

    move-result v0

    return v0
.end method

.method public b()Lcom/peel/control/o;
    .locals 2

    .prologue
    .line 147
    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    iget-object v1, p0, Lcom/peel/control/RoomControl;->a:Lcom/peel/data/at;

    invoke-virtual {v1}, Lcom/peel/data/at;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/peel/control/am;->b(Ljava/lang/String;)Lcom/peel/control/o;

    move-result-object v0

    return-object v0
.end method

.method public declared-synchronized b(I)V
    .locals 2

    .prologue
    .line 293
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/peel/control/RoomControl;->b:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V

    .line 294
    sget-object v0, Lcom/peel/control/RoomControl;->d:[Lcom/peel/control/av;

    iget-object v1, p0, Lcom/peel/control/RoomControl;->b:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v1

    aget-object v0, v0, v1

    iput-object v0, p0, Lcom/peel/control/RoomControl;->e:Lcom/peel/control/av;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 301
    monitor-exit p0

    return-void

    .line 293
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public b(Lcom/peel/control/a;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v0, 0x0

    .line 194
    iget-object v1, p0, Lcom/peel/control/RoomControl;->g:Lcom/peel/control/a;

    invoke-virtual {p1, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iput-object v6, p0, Lcom/peel/control/RoomControl;->g:Lcom/peel/control/a;

    .line 196
    :cond_0
    iget-object v1, p0, Lcom/peel/control/RoomControl;->a:Lcom/peel/data/at;

    invoke-virtual {v1}, Lcom/peel/data/at;->c()[Ljava/lang/String;

    move-result-object v2

    .line 197
    if-nez v2, :cond_2

    .line 233
    :cond_1
    :goto_0
    return-void

    .line 200
    :cond_2
    array-length v3, v2

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_8

    aget-object v4, v2, v1

    .line 201
    invoke-virtual {p1}, Lcom/peel/control/a;->b()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 202
    const/4 v1, 0x1

    .line 207
    :goto_2
    if-eqz v1, :cond_1

    .line 209
    iget-object v1, p0, Lcom/peel/control/RoomControl;->a:Lcom/peel/data/at;

    invoke-virtual {p1}, Lcom/peel/control/a;->c()Lcom/peel/data/d;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/peel/data/at;->b(Lcom/peel/data/d;)V

    .line 210
    iget-object v1, p0, Lcom/peel/control/RoomControl;->f:Lcom/peel/util/s;

    invoke-virtual {p1, v1}, Lcom/peel/control/a;->b(Lcom/peel/util/s;)V

    .line 213
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 214
    invoke-virtual {p0}, Lcom/peel/control/RoomControl;->c()[Lcom/peel/control/a;

    move-result-object v2

    if-eqz v2, :cond_7

    .line 215
    invoke-virtual {p0}, Lcom/peel/control/RoomControl;->c()[Lcom/peel/control/a;

    move-result-object v2

    array-length v3, v2

    :goto_3
    if-ge v0, v3, :cond_6

    aget-object v4, v2, v0

    .line 216
    invoke-virtual {v4}, Lcom/peel/control/a;->d()[Ljava/lang/String;

    move-result-object v4

    .line 217
    if-eqz v4, :cond_3

    array-length v5, v4

    if-nez v5, :cond_5

    .line 215
    :cond_3
    :goto_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 200
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 218
    :cond_5
    invoke-static {v4}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v4

    invoke-interface {v1, v4}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    goto :goto_4

    .line 220
    :cond_6
    invoke-interface {v1}, Ljava/util/Set;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/String;

    invoke-interface {v1, v0}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    iput-object v0, p0, Lcom/peel/control/RoomControl;->h:[Ljava/lang/String;

    goto :goto_0

    .line 222
    :cond_7
    iput-object v6, p0, Lcom/peel/control/RoomControl;->h:[Ljava/lang/String;

    goto :goto_0

    :cond_8
    move v1, v0

    goto :goto_2
.end method

.method public c(Lcom/peel/control/a;)V
    .locals 3

    .prologue
    .line 238
    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/peel/control/RoomControl;->g:Lcom/peel/control/a;

    if-eqz v0, :cond_0

    .line 240
    iget-object v0, p0, Lcom/peel/control/RoomControl;->g:Lcom/peel/control/a;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/peel/control/RoomControl;->g:Lcom/peel/control/a;

    invoke-virtual {v2}, Lcom/peel/control/a;->e()[Lcom/peel/control/h;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/peel/control/a;->a(I[Lcom/peel/control/h;)Z

    .line 243
    :cond_0
    iput-object p1, p0, Lcom/peel/control/RoomControl;->g:Lcom/peel/control/a;

    .line 245
    iget-object v0, p0, Lcom/peel/control/RoomControl;->g:Lcom/peel/control/a;

    if-eqz v0, :cond_1

    .line 246
    iget-object v0, p0, Lcom/peel/control/RoomControl;->g:Lcom/peel/control/a;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/peel/control/a;->b(I)Z

    .line 258
    :cond_1
    return-void
.end method

.method public c()[Lcom/peel/control/a;
    .locals 8

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 162
    iget-object v2, p0, Lcom/peel/control/RoomControl;->a:Lcom/peel/data/at;

    invoke-virtual {v2}, Lcom/peel/data/at;->c()[Ljava/lang/String;

    move-result-object v4

    .line 163
    if-nez v4, :cond_1

    .line 170
    :cond_0
    return-object v0

    .line 164
    :cond_1
    array-length v2, v4

    if-eqz v2, :cond_0

    .line 165
    array-length v0, v4

    new-array v0, v0, [Lcom/peel/control/a;

    .line 167
    array-length v5, v4

    move v2, v1

    :goto_0
    if-ge v1, v5, :cond_0

    aget-object v6, v4, v1

    .line 168
    add-int/lit8 v3, v2, 0x1

    sget-object v7, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v7, v6}, Lcom/peel/control/am;->e(Ljava/lang/String;)Lcom/peel/control/a;

    move-result-object v6

    aput-object v6, v0, v2

    .line 167
    add-int/lit8 v1, v1, 0x1

    move v2, v3

    goto :goto_0
.end method

.method public d()Lcom/peel/control/a;
    .locals 1

    .prologue
    .line 235
    iget-object v0, p0, Lcom/peel/control/RoomControl;->g:Lcom/peel/control/a;

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 321
    const v0, 0x18044

    return v0
.end method

.method public e()Z
    .locals 1

    .prologue
    .line 283
    iget-object v0, p0, Lcom/peel/control/RoomControl;->e:Lcom/peel/control/av;

    invoke-virtual {v0, p0}, Lcom/peel/control/av;->b(Lcom/peel/control/RoomControl;)Z

    move-result v0

    return v0
.end method

.method public f()Z
    .locals 1

    .prologue
    .line 285
    iget-object v0, p0, Lcom/peel/control/RoomControl;->e:Lcom/peel/control/av;

    invoke-virtual {v0, p0}, Lcom/peel/control/av;->a(Lcom/peel/control/RoomControl;)Z

    move-result v0

    return v0
.end method

.method public g()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 308
    iget-object v0, p0, Lcom/peel/control/RoomControl;->h:[Ljava/lang/String;

    return-object v0
.end method

.method public h()V
    .locals 4

    .prologue
    .line 312
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/peel/control/RoomControl;->h:[Ljava/lang/String;

    .line 313
    invoke-virtual {p0}, Lcom/peel/control/RoomControl;->c()[Lcom/peel/control/a;

    move-result-object v1

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 314
    invoke-virtual {v3}, Lcom/peel/control/a;->d()[Ljava/lang/String;

    move-result-object v3

    .line 315
    if-nez v3, :cond_0

    .line 313
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 316
    :cond_0
    invoke-direct {p0, v3}, Lcom/peel/control/RoomControl;->a([Ljava/lang/String;)V

    goto :goto_1

    .line 318
    :cond_1
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 324
    iget-object v0, p0, Lcom/peel/control/RoomControl;->a:Lcom/peel/data/at;

    invoke-virtual {v0}, Lcom/peel/data/at;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 325
    iget-object v0, p0, Lcom/peel/control/RoomControl;->a:Lcom/peel/data/at;

    invoke-virtual {v0}, Lcom/peel/data/at;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 326
    iget-object v0, p0, Lcom/peel/control/RoomControl;->a:Lcom/peel/data/at;

    invoke-virtual {v0}, Lcom/peel/data/at;->f()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 327
    iget-object v0, p0, Lcom/peel/control/RoomControl;->a:Lcom/peel/data/at;

    invoke-virtual {v0}, Lcom/peel/data/at;->g()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 328
    iget-object v0, p0, Lcom/peel/control/RoomControl;->a:Lcom/peel/data/at;

    invoke-virtual {v0}, Lcom/peel/data/at;->c()[Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/peel/control/RoomControl;->a:Lcom/peel/data/at;

    invoke-virtual {v0}, Lcom/peel/data/at;->c()[Ljava/lang/String;

    move-result-object v0

    array-length v0, v0

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 329
    iget-object v0, p0, Lcom/peel/control/RoomControl;->a:Lcom/peel/data/at;

    invoke-virtual {v0}, Lcom/peel/data/at;->c()[Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/peel/control/RoomControl;->a:Lcom/peel/data/at;

    invoke-virtual {v0}, Lcom/peel/data/at;->c()[Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeStringArray([Ljava/lang/String;)V

    .line 330
    :cond_0
    iget-object v0, p0, Lcom/peel/control/RoomControl;->a:Lcom/peel/data/at;

    invoke-virtual {v0}, Lcom/peel/data/at;->d()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_4

    move v0, v2

    :goto_1
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 331
    iget-object v0, p0, Lcom/peel/control/RoomControl;->a:Lcom/peel/data/at;

    invoke-virtual {v0}, Lcom/peel/data/at;->d()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/peel/control/RoomControl;->a:Lcom/peel/data/at;

    invoke-virtual {v0}, Lcom/peel/data/at;->d()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 332
    :cond_1
    iget-object v0, p0, Lcom/peel/control/RoomControl;->a:Lcom/peel/data/at;

    invoke-virtual {v0}, Lcom/peel/data/at;->e()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_5

    :goto_2
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 333
    iget-object v0, p0, Lcom/peel/control/RoomControl;->a:Lcom/peel/data/at;

    invoke-virtual {v0}, Lcom/peel/data/at;->e()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/peel/control/RoomControl;->a:Lcom/peel/data/at;

    invoke-virtual {v0}, Lcom/peel/data/at;->e()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 334
    :cond_2
    return-void

    :cond_3
    move v0, v1

    .line 328
    goto :goto_0

    :cond_4
    move v0, v1

    .line 330
    goto :goto_1

    :cond_5
    move v2, v1

    .line 332
    goto :goto_2
.end method
