.class public Lcom/peel/control/u;
.super Ljava/lang/Object;


# static fields
.field private static final a:Ljava/lang/String;

.field private static final b:Ljava/util/concurrent/atomic/AtomicBoolean;


# instance fields
.field private final c:I

.field private final d:I

.field private final e:I

.field private final f:I

.field private final g:Ljava/util/concurrent/ConcurrentLinkedQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentLinkedQueue",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final h:Ljava/util/concurrent/atomic/AtomicInteger;

.field private final i:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final j:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Landroid/os/MemoryFile;",
            ">;"
        }
    .end annotation
.end field

.field private final k:Landroid/media/SoundPool$OnLoadCompleteListener;

.field private l:Landroid/media/SoundPool;

.field private m:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private n:Landroid/content/Context;

.field private o:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Lcom/peel/control/y;",
            ">;"
        }
    .end annotation
.end field

.field private p:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private q:Landroid/media/AudioManager;

.field private r:I

.field private final s:Ljava/lang/Runnable;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 33
    const-class v0, Lcom/peel/control/u;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/peel/control/u;->a:Ljava/lang/String;

    .line 36
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    sput-object v0, Lcom/peel/control/u;->b:Ljava/util/concurrent/atomic/AtomicBoolean;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v2, 0x2

    const/4 v4, 0x0

    const/4 v1, 0x1

    .line 133
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    new-instance v0, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V

    iput-object v0, p0, Lcom/peel/control/u;->g:Ljava/util/concurrent/ConcurrentLinkedQueue;

    .line 42
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0, v4}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    iput-object v0, p0, Lcom/peel/control/u;->h:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 43
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/peel/control/u;->i:Ljava/util/List;

    .line 44
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/peel/control/u;->j:Ljava/util/Map;

    .line 45
    new-instance v0, Lcom/peel/control/v;

    invoke-direct {v0, p0}, Lcom/peel/control/v;-><init>(Lcom/peel/control/u;)V

    iput-object v0, p0, Lcom/peel/control/u;->k:Landroid/media/SoundPool$OnLoadCompleteListener;

    .line 76
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/peel/control/u;->m:Ljava/util/HashMap;

    .line 78
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/peel/control/u;->o:Landroid/util/SparseArray;

    .line 79
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/peel/control/u;->p:Landroid/util/SparseArray;

    .line 82
    new-instance v0, Lcom/peel/control/w;

    invoke-direct {v0, p0}, Lcom/peel/control/w;-><init>(Lcom/peel/control/u;)V

    iput-object v0, p0, Lcom/peel/control/u;->s:Ljava/lang/Runnable;

    .line 134
    iput-object p1, p0, Lcom/peel/control/u;->n:Landroid/content/Context;

    .line 136
    iget-object v0, p0, Lcom/peel/control/u;->n:Landroid/content/Context;

    const-string/jumbo v3, "audio"

    invoke-virtual {v0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    iput-object v0, p0, Lcom/peel/control/u;->q:Landroid/media/AudioManager;

    .line 138
    iput v1, p0, Lcom/peel/control/u;->c:I

    .line 139
    iput v2, p0, Lcom/peel/control/u;->d:I

    .line 140
    iget v0, p0, Lcom/peel/control/u;->d:I

    if-ne v0, v5, :cond_1

    move v0, v1

    :goto_0
    iput v0, p0, Lcom/peel/control/u;->e:I

    .line 141
    iget v0, p0, Lcom/peel/control/u;->e:I

    mul-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/peel/control/u;->f:I

    .line 143
    new-instance v0, Landroid/media/SoundPool;

    invoke-direct {v0, v1, v5, v4}, Landroid/media/SoundPool;-><init>(III)V

    iput-object v0, p0, Lcom/peel/control/u;->l:Landroid/media/SoundPool;

    .line 144
    iget-object v0, p0, Lcom/peel/control/u;->l:Landroid/media/SoundPool;

    iget-object v1, p0, Lcom/peel/control/u;->k:Landroid/media/SoundPool$OnLoadCompleteListener;

    invoke-virtual {v0, v1}, Landroid/media/SoundPool;->setOnLoadCompleteListener(Landroid/media/SoundPool$OnLoadCompleteListener;)V

    .line 147
    new-instance v0, Ljava/io/File;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/peel/control/u;->n:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getExternalCacheDir()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "ir"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 148
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    .line 149
    :cond_0
    return-void

    :cond_1
    move v0, v2

    .line 140
    goto :goto_0
.end method

.method private a(III)I
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 254
    and-int v3, p1, p2

    .line 255
    const/4 v1, 0x1

    move v2, v1

    move v1, v0

    .line 257
    :goto_0
    if-ge v1, p3, :cond_1

    .line 258
    and-int v4, v3, v2

    if-eqz v4, :cond_0

    .line 259
    add-int/lit8 v0, v0, 0x1

    .line 261
    :cond_0
    shl-int/lit8 v2, v2, 0x1

    .line 257
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 263
    :cond_1
    return v0
.end method

.method static synthetic a(Lcom/peel/control/u;I)I
    .locals 0

    .prologue
    .line 31
    iput p1, p0, Lcom/peel/control/u;->r:I

    return p1
.end method

.method private a([BIIII)I
    .locals 8

    .prologue
    .line 506
    const/4 v0, 0x1

    add-int/lit8 v1, p5, -0x1

    shl-int v1, v0, v1

    .line 508
    const/4 v0, 0x0

    move v6, v0

    move v7, v1

    move v2, p2

    :goto_0
    if-ge v6, p5, :cond_1

    .line 511
    and-int v0, p3, v7

    if-eqz v0, :cond_0

    .line 512
    const/4 v3, 0x2

    sget-object v5, Lcom/peel/control/z;->a:Lcom/peel/control/z;

    move-object v0, p0

    move-object v1, p1

    move v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/peel/control/u;->a([BIIILcom/peel/control/z;)I

    move-result v2

    .line 513
    const/4 v3, 0x2

    sget-object v5, Lcom/peel/control/z;->b:Lcom/peel/control/z;

    move-object v0, p0

    move-object v1, p1

    move v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/peel/control/u;->a([BIIILcom/peel/control/z;)I

    move-result v0

    .line 519
    :goto_1
    shr-int/lit8 v2, v7, 0x1

    .line 508
    add-int/lit8 v1, v6, 0x1

    move v6, v1

    move v7, v2

    move v2, v0

    goto :goto_0

    .line 515
    :cond_0
    const/4 v3, 0x1

    sget-object v5, Lcom/peel/control/z;->a:Lcom/peel/control/z;

    move-object v0, p0

    move-object v1, p1

    move v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/peel/control/u;->a([BIIILcom/peel/control/z;)I

    move-result v2

    .line 516
    const/4 v3, 0x1

    sget-object v5, Lcom/peel/control/z;->b:Lcom/peel/control/z;

    move-object v0, p0

    move-object v1, p1

    move v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/peel/control/u;->a([BIIILcom/peel/control/z;)I

    move-result v0

    goto :goto_1

    .line 522
    :cond_1
    return v2
.end method

.method private a([BIIILcom/peel/control/z;)I
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 477
    mul-int v0, p3, p4

    div-int/lit16 v2, v0, 0x3e8

    .line 480
    iget v0, p0, Lcom/peel/control/u;->d:I

    const/4 v3, 0x2

    if-ne v0, v3, :cond_2

    iget v0, p0, Lcom/peel/control/u;->c:I

    const/4 v3, 0x1

    if-ne v0, v3, :cond_2

    .line 481
    sget-object v0, Lcom/peel/control/z;->b:Lcom/peel/control/z;

    if-ne p5, v0, :cond_0

    move v0, v1

    .line 482
    :goto_0
    if-ge v0, v2, :cond_3

    .line 483
    add-int/lit8 v3, p2, 0x1

    aput-byte v1, p1, p2

    .line 484
    add-int/lit8 p2, v3, 0x1

    const/16 v4, -0x80

    aput-byte v4, p1, v3

    .line 482
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 486
    :cond_0
    sget-object v0, Lcom/peel/control/z;->a:Lcom/peel/control/z;

    if-ne p5, v0, :cond_1

    .line 487
    :goto_1
    if-ge v1, v2, :cond_3

    .line 488
    add-int/lit8 v0, p2, 0x1

    const/4 v3, -0x1

    aput-byte v3, p1, p2

    .line 489
    add-int/lit8 p2, v0, 0x1

    const/16 v3, 0x7f

    aput-byte v3, p1, v0

    .line 487
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_1
    move v0, v1

    .line 492
    :goto_2
    if-ge v0, v2, :cond_3

    .line 493
    add-int/lit8 v3, p2, 0x1

    aput-byte v1, p1, p2

    .line 494
    add-int/lit8 p2, v3, 0x1

    aput-byte v1, p1, v3

    .line 492
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 498
    :cond_2
    sget-object v0, Lcom/peel/control/u;->a:Ljava/lang/String;

    const-string/jumbo v1, "IrAudioWave only supports 16-bit PCM (Mono)"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 501
    :cond_3
    return p2
.end method

.method private a([BI[II)I
    .locals 11

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 557
    move v7, v2

    move v0, v1

    move v4, p2

    .line 559
    :goto_0
    if-ge v7, p4, :cond_4

    .line 560
    array-length v8, p3

    move v5, v2

    move v6, v0

    move v0, v4

    :goto_1
    if-ge v5, v8, :cond_3

    aget v9, p3, v5

    move v3, v2

    move v4, v0

    .line 561
    :goto_2
    if-ge v3, v9, :cond_1

    .line 562
    if-eqz v6, :cond_0

    .line 563
    add-int/lit8 v10, v4, 0x1

    const/4 v0, -0x1

    aput-byte v0, p1, v4

    .line 564
    add-int/lit8 v0, v10, 0x1

    const/16 v4, 0x7f

    aput-byte v4, p1, v10

    .line 561
    :goto_3
    add-int/lit8 v3, v3, 0x1

    move v4, v0

    goto :goto_2

    .line 566
    :cond_0
    add-int/lit8 v10, v4, 0x1

    aput-byte v2, p1, v4

    .line 567
    add-int/lit8 v0, v10, 0x1

    const/16 v4, -0x80

    aput-byte v4, p1, v10

    goto :goto_3

    .line 571
    :cond_1
    if-nez v6, :cond_2

    move v0, v1

    .line 560
    :goto_4
    add-int/lit8 v3, v5, 0x1

    move v5, v3

    move v6, v0

    move v0, v4

    goto :goto_1

    :cond_2
    move v0, v2

    .line 571
    goto :goto_4

    .line 559
    :cond_3
    add-int/lit8 v3, v7, 0x1

    move v7, v3

    move v4, v0

    move v0, v6

    goto :goto_0

    .line 575
    :cond_4
    return v4
.end method

.method private a(IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/peel/control/x;Ljava/util/concurrent/atomic/AtomicLong;)Landroid/os/MemoryFile;
    .locals 17

    .prologue
    .line 268
    const-wide/16 v2, 0x0

    move-object/from16 v0, p7

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/atomic/AtomicLong;->set(J)V

    .line 269
    const-wide/16 v2, 0x0

    .line 272
    mul-int/lit8 v4, p1, 0x5

    div-int/lit16 v4, v4, 0x3e8

    int-to-long v4, v4

    add-long/2addr v2, v4

    .line 275
    move/from16 v0, p1

    div-int/lit16 v4, v0, 0x3e8

    int-to-long v4, v4

    add-long/2addr v2, v4

    .line 276
    mul-int/lit8 v4, p1, 0x3

    div-int/lit16 v4, v4, 0x3e8

    int-to-long v4, v4

    add-long/2addr v2, v4

    .line 281
    div-int/lit8 v4, p1, 0x64

    const/16 v5, 0xfff

    const/16 v6, 0xc

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v5, v6}, Lcom/peel/control/u;->a(III)I

    move-result v4

    .line 282
    rsub-int/lit8 v5, v4, 0xc

    .line 283
    mul-int/lit8 v5, v5, 0x2

    mul-int v5, v5, p1

    div-int/lit16 v5, v5, 0x3e8

    int-to-long v6, v5

    add-long/2addr v2, v6

    .line 284
    mul-int/lit8 v4, v4, 0x4

    mul-int v4, v4, p1

    div-int/lit16 v4, v4, 0x3e8

    int-to-long v4, v4

    add-long v8, v2, v4

    .line 289
    const/4 v4, 0x0

    .line 290
    const/4 v6, 0x0

    .line 293
    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-direct {v0, v1}, Lcom/peel/control/u;->a(Ljava/lang/String;)[I

    move-result-object v3

    .line 294
    const/4 v2, 0x0

    :goto_0
    array-length v5, v3

    if-ge v2, v5, :cond_0

    .line 295
    aget v5, v3, v2

    add-int/2addr v4, v5

    .line 294
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 298
    :cond_0
    const/4 v2, 0x0

    .line 299
    if-eqz p4, :cond_1

    invoke-virtual/range {p4 .. p4}, Ljava/lang/String;->length()I

    move-result v5

    if-nez v5, :cond_3

    .line 300
    :cond_1
    mul-int v4, v4, p2

    move-object/from16 v16, v2

    move v2, v4

    move-object/from16 v4, v16

    .line 309
    :goto_1
    int-to-long v6, v2

    add-long/2addr v8, v6

    .line 312
    mul-int/lit16 v2, v2, 0x3e8

    div-int v7, v2, p1

    .line 313
    const/16 v2, 0xfff

    const/16 v5, 0xc

    move-object/from16 v0, p0

    invoke-direct {v0, v7, v2, v5}, Lcom/peel/control/u;->a(III)I

    move-result v2

    .line 314
    rsub-int/lit8 v5, v2, 0xc

    .line 315
    mul-int/lit8 v5, v5, 0x2

    mul-int v5, v5, p1

    div-int/lit16 v5, v5, 0x3e8

    int-to-long v10, v5

    add-long/2addr v8, v10

    .line 316
    mul-int/lit8 v2, v2, 0x4

    mul-int v2, v2, p1

    div-int/lit16 v2, v2, 0x3e8

    int-to-long v10, v2

    add-long/2addr v8, v10

    .line 319
    move/from16 v0, p1

    div-int/lit16 v2, v0, 0x3e8

    int-to-long v10, v2

    add-long/2addr v10, v8

    .line 324
    move/from16 v0, p1

    int-to-long v8, v0

    mul-long/2addr v8, v10

    move-object/from16 v0, p0

    iget v2, v0, Lcom/peel/control/u;->e:I

    int-to-long v12, v2

    mul-long/2addr v8, v12

    move-object/from16 v0, p0

    iget v2, v0, Lcom/peel/control/u;->c:I

    int-to-long v12, v2

    mul-long/2addr v8, v12

    move/from16 v0, p1

    int-to-long v12, v0

    div-long/2addr v8, v12

    .line 327
    const-wide/16 v12, 0x3e8

    mul-long/2addr v10, v12

    move/from16 v0, p1

    int-to-long v12, v0

    div-long/2addr v10, v12

    move-object/from16 v0, p7

    invoke-virtual {v0, v10, v11}, Ljava/util/concurrent/atomic/AtomicLong;->set(J)V

    .line 330
    move-object/from16 v0, p0

    iget v2, v0, Lcom/peel/control/u;->c:I

    move-object/from16 v0, p0

    iget v5, v0, Lcom/peel/control/u;->e:I

    mul-int/2addr v2, v5

    .line 331
    int-to-long v10, v2

    rem-long v10, v8, v10

    .line 332
    const-wide/16 v12, 0x0

    cmp-long v5, v10, v12

    if-eqz v5, :cond_2

    .line 333
    int-to-long v12, v2

    sub-long v10, v12, v10

    add-long/2addr v8, v10

    .line 341
    :cond_2
    const-wide/16 v10, 0x2c

    add-long/2addr v10, v8

    .line 343
    long-to-int v5, v10

    invoke-static {v5}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v12

    .line 344
    sget-object v5, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v12, v5}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 347
    const-string/jumbo v5, "RIFF"

    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lcom/peel/control/u;->b(Ljava/lang/String;)[B

    move-result-object v5

    invoke-virtual {v12, v5}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 348
    const-wide/16 v14, 0x8

    sub-long v14, v10, v14

    long-to-int v5, v14

    invoke-virtual {v12, v5}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 349
    const-string/jumbo v5, "WAVE"

    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lcom/peel/control/u;->b(Ljava/lang/String;)[B

    move-result-object v5

    invoke-virtual {v12, v5}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 352
    const-string/jumbo v5, "fmt "

    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lcom/peel/control/u;->b(Ljava/lang/String;)[B

    move-result-object v5

    invoke-virtual {v12, v5}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 353
    const/16 v5, 0x10

    invoke-virtual {v12, v5}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 354
    const/4 v5, 0x1

    invoke-virtual {v12, v5}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    .line 355
    move-object/from16 v0, p0

    iget v5, v0, Lcom/peel/control/u;->c:I

    int-to-short v5, v5

    invoke-virtual {v12, v5}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    .line 356
    move/from16 v0, p1

    invoke-virtual {v12, v0}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 357
    mul-int v5, p1, v2

    invoke-virtual {v12, v5}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 358
    int-to-short v2, v2

    invoke-virtual {v12, v2}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    .line 359
    move-object/from16 v0, p0

    iget v2, v0, Lcom/peel/control/u;->f:I

    int-to-short v2, v2

    invoke-virtual {v12, v2}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    .line 362
    const-string/jumbo v2, "data"

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/peel/control/u;->b(Ljava/lang/String;)[B

    move-result-object v2

    invoke-virtual {v12, v2}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 363
    long-to-int v2, v8

    invoke-virtual {v12, v2}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 364
    long-to-int v8, v8

    move-object/from16 v0, p0

    iget v9, v0, Lcom/peel/control/u;->c:I

    move-object/from16 v2, p0

    move/from16 v5, p1

    move/from16 v6, p2

    invoke-direct/range {v2 .. v9}, Lcom/peel/control/u;->a([I[IIIIII)[B

    move-result-object v2

    invoke-virtual {v12, v2}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 366
    invoke-virtual {v12}, Ljava/nio/ByteBuffer;->flip()Ljava/nio/Buffer;

    .line 370
    :try_start_0
    sget-object v2, Lcom/peel/control/x;->c:Lcom/peel/control/x;

    move-object/from16 v0, p6

    if-ne v2, v0, :cond_5

    new-instance v2, Landroid/os/MemoryFile;

    long-to-int v3, v10

    move-object/from16 v0, p5

    invoke-direct {v2, v0, v3}, Landroid/os/MemoryFile;-><init>(Ljava/lang/String;I)V

    .line 371
    :goto_2
    invoke-virtual {v12}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    long-to-int v6, v10

    invoke-virtual {v2, v3, v4, v5, v6}, Landroid/os/MemoryFile;->writeBytes([BIII)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 377
    :goto_3
    return-object v2

    .line 303
    :cond_3
    move-object/from16 v0, p0

    move-object/from16 v1, p4

    invoke-direct {v0, v1}, Lcom/peel/control/u;->a(Ljava/lang/String;)[I

    move-result-object v2

    .line 304
    array-length v7, v2

    const/4 v5, 0x0

    :goto_4
    if-ge v5, v7, :cond_4

    aget v10, v2, v5

    .line 305
    add-int/2addr v6, v10

    .line 304
    add-int/lit8 v5, v5, 0x1

    goto :goto_4

    .line 307
    :cond_4
    mul-int v5, p2, v6

    add-int/2addr v4, v5

    move-object/from16 v16, v2

    move v2, v4

    move-object/from16 v4, v16

    goto/16 :goto_1

    .line 370
    :cond_5
    const/4 v2, 0x0

    goto :goto_2

    .line 372
    :catch_0
    move-exception v2

    .line 373
    sget-object v2, Lcom/peel/control/u;->a:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "Failed to create file: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p5

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 374
    const/4 v2, 0x0

    goto :goto_3
.end method

.method static synthetic a(Lcom/peel/control/u;)Ljava/util/Map;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/peel/control/u;->j:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic a(Lcom/peel/control/u;[Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0, p1}, Lcom/peel/control/u;->a([Ljava/lang/String;)V

    return-void
.end method

.method private a([Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 153
    iget-object v1, p0, Lcom/peel/control/u;->g:Ljava/util/concurrent/ConcurrentLinkedQueue;

    monitor-enter v1

    .line 154
    :try_start_0
    array-length v2, p1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, p1, v0

    .line 155
    iget-object v4, p0, Lcom/peel/control/u;->g:Ljava/util/concurrent/ConcurrentLinkedQueue;

    iget-object v5, p0, Lcom/peel/control/u;->m:Ljava/util/HashMap;

    invoke-virtual {v5, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v4, v3}, Ljava/util/concurrent/ConcurrentLinkedQueue;->offer(Ljava/lang/Object;)Z

    .line 154
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 157
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 159
    sget-object v1, Lcom/peel/control/u;->b:Ljava/util/concurrent/atomic/AtomicBoolean;

    monitor-enter v1

    .line 161
    :try_start_1
    sget-object v0, Lcom/peel/control/u;->b:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-nez v0, :cond_1

    .line 162
    sget-object v0, Lcom/peel/control/u;->a:Ljava/lang/String;

    const-string/jumbo v2, "started sendIr runnable"

    iget-object v3, p0, Lcom/peel/control/u;->s:Ljava/lang/Runnable;

    invoke-static {v0, v2, v3}, Lcom/peel/util/i;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)V

    .line 164
    :cond_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 165
    return-void

    .line 157
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 164
    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private a([I[IIIIII)[B
    .locals 8

    .prologue
    const/16 v7, 0xc

    const/4 v6, 0x1

    .line 581
    new-array v1, p6, [B

    .line 582
    const/4 v2, 0x0

    .line 585
    const/4 v3, 0x5

    sget-object v5, Lcom/peel/control/z;->c:Lcom/peel/control/z;

    move-object v0, p0

    move v4, p3

    invoke-direct/range {v0 .. v5}, Lcom/peel/control/u;->a([BIIILcom/peel/control/z;)I

    move-result v2

    .line 588
    sget-object v5, Lcom/peel/control/z;->a:Lcom/peel/control/z;

    move-object v0, p0

    move v3, v6

    move v4, p3

    invoke-direct/range {v0 .. v5}, Lcom/peel/control/u;->a([BIIILcom/peel/control/z;)I

    move-result v2

    .line 589
    const/4 v3, 0x3

    sget-object v5, Lcom/peel/control/z;->b:Lcom/peel/control/z;

    move-object v0, p0

    move v4, p3

    invoke-direct/range {v0 .. v5}, Lcom/peel/control/u;->a([BIIILcom/peel/control/z;)I

    move-result v2

    .line 592
    div-int/lit8 v3, p3, 0x64

    move-object v0, p0

    move v4, p3

    move v5, v7

    invoke-direct/range {v0 .. v5}, Lcom/peel/control/u;->a([BIIII)I

    move-result v2

    move-object v0, p0

    move v3, p5

    move v4, p3

    move v5, v7

    .line 595
    invoke-direct/range {v0 .. v5}, Lcom/peel/control/u;->a([BIIII)I

    move-result v0

    .line 598
    if-nez p2, :cond_0

    .line 599
    invoke-direct {p0, v1, v0, p1, p4}, Lcom/peel/control/u;->a([BI[II)I

    move-result v2

    .line 606
    :goto_0
    sget-object v5, Lcom/peel/control/z;->c:Lcom/peel/control/z;

    move-object v0, p0

    move v3, v6

    move v4, p3

    invoke-direct/range {v0 .. v5}, Lcom/peel/control/u;->a([BIIILcom/peel/control/z;)I

    .line 608
    return-object v1

    .line 601
    :cond_0
    invoke-direct {p0, v1, v0, p1, v6}, Lcom/peel/control/u;->a([BI[II)I

    move-result v0

    .line 602
    invoke-direct {p0, v1, v0, p2, p4}, Lcom/peel/control/u;->a([BI[II)I

    move-result v2

    goto :goto_0
.end method

.method private a(Ljava/lang/String;)[I
    .locals 4

    .prologue
    .line 465
    const-string/jumbo v0, "[ \t\n,]+"

    .line 466
    invoke-virtual {p1, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 468
    array-length v0, v1

    new-array v2, v0, [I

    .line 469
    const/4 v0, 0x0

    :goto_0
    array-length v3, v1

    if-ge v0, v3, :cond_0

    .line 470
    aget-object v3, v1, v0

    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    aput v3, v2, v0

    .line 469
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 473
    :cond_0
    return-object v2
.end method

.method static synthetic b(Lcom/peel/control/u;)Landroid/util/SparseArray;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/peel/control/u;->o:Landroid/util/SparseArray;

    return-object v0
.end method

.method static synthetic b()Ljava/util/concurrent/atomic/AtomicBoolean;
    .locals 1

    .prologue
    .line 31
    sget-object v0, Lcom/peel/control/u;->b:Ljava/util/concurrent/atomic/AtomicBoolean;

    return-object v0
.end method

.method private b(Ljava/lang/String;)[B
    .locals 3

    .prologue
    .line 615
    :try_start_0
    const-string/jumbo v0, "US-ASCII"

    invoke-virtual {p1, v0}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 621
    :goto_0
    return-object v0

    .line 616
    :catch_0
    move-exception v0

    .line 617
    sget-object v0, Lcom/peel/control/u;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Failed to convert to bytes: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 618
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 31
    sget-object v0, Lcom/peel/control/u;->a:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic c(Lcom/peel/control/u;)Ljava/util/concurrent/atomic/AtomicInteger;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/peel/control/u;->h:Ljava/util/concurrent/atomic/AtomicInteger;

    return-object v0
.end method

.method static synthetic d(Lcom/peel/control/u;)Ljava/util/List;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/peel/control/u;->i:Ljava/util/List;

    return-object v0
.end method

.method static synthetic e(Lcom/peel/control/u;)Ljava/util/concurrent/ConcurrentLinkedQueue;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/peel/control/u;->g:Ljava/util/concurrent/ConcurrentLinkedQueue;

    return-object v0
.end method

.method static synthetic f(Lcom/peel/control/u;)I
    .locals 1

    .prologue
    .line 31
    iget v0, p0, Lcom/peel/control/u;->r:I

    return v0
.end method

.method static synthetic g(Lcom/peel/control/u;)Landroid/media/AudioManager;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/peel/control/u;->q:Landroid/media/AudioManager;

    return-object v0
.end method

.method static synthetic h(Lcom/peel/control/u;)Landroid/util/SparseArray;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/peel/control/u;->p:Landroid/util/SparseArray;

    return-object v0
.end method

.method static synthetic i(Lcom/peel/control/u;)Landroid/media/SoundPool;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/peel/control/u;->l:Landroid/media/SoundPool;

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 168
    iget-object v1, p0, Lcom/peel/control/u;->g:Ljava/util/concurrent/ConcurrentLinkedQueue;

    monitor-enter v1

    .line 169
    :try_start_0
    iget-object v0, p0, Lcom/peel/control/u;->g:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->clear()V

    .line 170
    monitor-exit v1

    .line 171
    return-void

    .line 170
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public a([I[I[Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;[ILcom/peel/control/x;)V
    .locals 13

    .prologue
    .line 184
    new-instance v11, Ljava/util/HashSet;

    invoke-direct {v11}, Ljava/util/HashSet;-><init>()V

    .line 186
    const/4 v2, 0x0

    move v10, v2

    :goto_0
    move-object/from16 v0, p5

    array-length v2, v0

    if-ge v10, v2, :cond_5

    .line 187
    iget-object v3, p0, Lcom/peel/control/u;->i:Ljava/util/List;

    monitor-enter v3

    .line 188
    :try_start_0
    iget-object v2, p0, Lcom/peel/control/u;->i:Ljava/util/List;

    aget-object v4, p5, v10

    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 189
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 192
    aget-object v2, p5, v10

    invoke-interface {v11, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 186
    :cond_0
    :goto_1
    add-int/lit8 v2, v10, 0x1

    move v10, v2

    goto :goto_0

    .line 189
    :catchall_0
    move-exception v2

    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v2

    .line 193
    :cond_1
    aget-object v2, p5, v10

    invoke-interface {v11, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 195
    iget-object v2, p0, Lcom/peel/control/u;->m:Ljava/util/HashMap;

    aget-object v3, p5, v10

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    if-nez v2, :cond_0

    .line 197
    iget-object v3, p0, Lcom/peel/control/u;->h:Ljava/util/concurrent/atomic/AtomicInteger;

    monitor-enter v3

    .line 198
    :try_start_2
    iget-object v2, p0, Lcom/peel/control/u;->h:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    .line 199
    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 202
    new-instance v9, Ljava/util/concurrent/atomic/AtomicLong;

    const-wide/16 v2, 0x0

    invoke-direct {v9, v2, v3}, Ljava/util/concurrent/atomic/AtomicLong;-><init>(J)V

    .line 203
    sget-object v2, Lcom/peel/control/x;->c:Lcom/peel/control/x;

    move-object/from16 v0, p7

    if-ne v2, v0, :cond_2

    .line 204
    aget v3, p1, v10

    aget v4, p2, v10

    aget-object v5, p3, v10

    aget-object v6, p4, v10

    aget-object v7, p5, v10

    move-object v2, p0

    move-object/from16 v8, p7

    invoke-direct/range {v2 .. v9}, Lcom/peel/control/u;->a(IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/peel/control/x;Ljava/util/concurrent/atomic/AtomicLong;)Landroid/os/MemoryFile;

    move-result-object v2

    .line 206
    invoke-static {v2}, Lcom/peel/util/bp;->a(Landroid/os/MemoryFile;)Ljava/io/FileDescriptor;

    move-result-object v3

    .line 207
    invoke-static {v3}, Lcom/peel/util/bp;->a(Ljava/io/FileDescriptor;)I

    move-result v4

    int-to-long v6, v4

    .line 210
    iget-object v4, p0, Lcom/peel/control/u;->j:Ljava/util/Map;

    monitor-enter v4

    .line 211
    :try_start_3
    iget-object v5, p0, Lcom/peel/control/u;->j:Ljava/util/Map;

    aget-object v8, p5, v10

    invoke-interface {v5, v8, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 212
    monitor-exit v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    .line 214
    iget-object v2, p0, Lcom/peel/control/u;->l:Landroid/media/SoundPool;

    const-wide/16 v4, 0x0

    const/4 v8, 0x1

    invoke-virtual/range {v2 .. v8}, Landroid/media/SoundPool;->load(Ljava/io/FileDescriptor;JJI)I

    move-result v2

    move v8, v2

    .line 231
    :goto_2
    iget-object v2, p0, Lcom/peel/control/u;->m:Ljava/util/HashMap;

    aget-object v3, p5, v10

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 232
    iget-object v12, p0, Lcom/peel/control/u;->o:Landroid/util/SparseArray;

    new-instance v2, Lcom/peel/control/y;

    aget-object v3, p5, v10

    aget v4, p1, v10

    int-to-long v4, v4

    invoke-virtual {v9}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v6

    invoke-direct/range {v2 .. v7}, Lcom/peel/control/y;-><init>(Ljava/lang/String;JJ)V

    invoke-virtual {v12, v8, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 233
    iget-object v2, p0, Lcom/peel/control/u;->p:Landroid/util/SparseArray;

    aget v3, p6, v10

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v8, v3}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    goto/16 :goto_1

    .line 199
    :catchall_1
    move-exception v2

    :try_start_4
    monitor-exit v3
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    throw v2

    .line 212
    :catchall_2
    move-exception v2

    :try_start_5
    monitor-exit v4
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    throw v2

    .line 217
    :cond_2
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/peel/control/u;->n:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getExternalCacheDir()Ljava/io/File;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "ir"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    aget-object v3, p5, v10

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 218
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 219
    sget-object v3, Lcom/peel/control/x;->a:Lcom/peel/control/x;

    move-object/from16 v0, p7

    if-eq v0, v3, :cond_3

    sget-object v3, Lcom/peel/control/x;->b:Lcom/peel/control/x;

    move-object/from16 v0, p7

    if-ne v0, v3, :cond_4

    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v3

    if-nez v3, :cond_4

    .line 221
    :cond_3
    :try_start_6
    invoke-virtual {v2}, Ljava/io/File;->createNewFile()Z
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_0

    .line 225
    :goto_3
    aget v3, p1, v10

    aget v4, p2, v10

    aget-object v5, p3, v10

    aget-object v6, p4, v10

    move-object v2, p0

    move-object/from16 v8, p7

    invoke-direct/range {v2 .. v9}, Lcom/peel/control/u;->a(IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/peel/control/x;Ljava/util/concurrent/atomic/AtomicLong;)Landroid/os/MemoryFile;

    .line 227
    :cond_4
    iget-object v2, p0, Lcom/peel/control/u;->l:Landroid/media/SoundPool;

    const/4 v3, 0x1

    invoke-virtual {v2, v7, v3}, Landroid/media/SoundPool;->load(Ljava/lang/String;I)I

    move-result v2

    move v8, v2

    goto/16 :goto_2

    .line 222
    :catch_0
    move-exception v2

    .line 223
    sget-object v3, Lcom/peel/control/u;->a:Ljava/lang/String;

    sget-object v4, Lcom/peel/control/u;->a:Ljava/lang/String;

    invoke-static {v3, v4, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_3

    .line 237
    :cond_5
    iget-object v3, p0, Lcom/peel/control/u;->h:Ljava/util/concurrent/atomic/AtomicInteger;

    monitor-enter v3

    .line 238
    :try_start_7
    iget-object v2, p0, Lcom/peel/control/u;->h:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v2

    if-lez v2, :cond_6

    .line 239
    monitor-exit v3

    .line 246
    :goto_4
    return-void

    .line 241
    :cond_6
    monitor-exit v3
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_3

    .line 244
    iget-object v2, p0, Lcom/peel/control/u;->k:Landroid/media/SoundPool$OnLoadCompleteListener;

    iget-object v3, p0, Lcom/peel/control/u;->l:Landroid/media/SoundPool;

    const/4 v4, -0x1

    const/4 v5, -0x1

    invoke-interface {v2, v3, v4, v5}, Landroid/media/SoundPool$OnLoadCompleteListener;->onLoadComplete(Landroid/media/SoundPool;II)V

    goto :goto_4

    .line 241
    :catchall_3
    move-exception v2

    :try_start_8
    monitor-exit v3
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_3

    throw v2
.end method
