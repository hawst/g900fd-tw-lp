.class public Lcom/peel/control/h;
.super Ljava/lang/Object;


# static fields
.field public static final a:Lcom/peel/control/k;

.field private static final c:Ljava/lang/String;

.field private static final e:Lcom/peel/util/s;

.field private static final f:[Lcom/peel/control/n;


# instance fields
.field protected b:Lcom/peel/data/g;

.field private final d:Ljava/util/concurrent/atomic/AtomicInteger;

.field private volatile g:Lcom/peel/control/n;

.field private final h:Lcom/peel/control/k;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 22
    const-class v0, Lcom/peel/control/h;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/peel/control/h;->c:Ljava/lang/String;

    .line 32
    new-instance v0, Lcom/peel/control/i;

    invoke-direct {v0}, Lcom/peel/control/i;-><init>()V

    sput-object v0, Lcom/peel/control/h;->e:Lcom/peel/util/s;

    .line 110
    invoke-static {}, Lcom/peel/control/h;->q()Lcom/peel/control/k;

    move-result-object v0

    sput-object v0, Lcom/peel/control/h;->a:Lcom/peel/control/k;

    .line 117
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/peel/control/n;

    const/4 v1, 0x0

    new-instance v2, Lcom/peel/control/l;

    invoke-direct {v2}, Lcom/peel/control/l;-><init>()V

    aput-object v2, v0, v1

    const/4 v1, 0x1

    new-instance v2, Lcom/peel/control/m;

    invoke-direct {v2}, Lcom/peel/control/m;-><init>()V

    aput-object v2, v0, v1

    sput-object v0, Lcom/peel/control/h;->f:[Lcom/peel/control/n;

    return-void
.end method

.method protected constructor <init>(Lcom/peel/data/g;)V
    .locals 2

    .prologue
    .line 149
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    iput-object v0, p0, Lcom/peel/control/h;->d:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 118
    sget-object v0, Lcom/peel/control/h;->f:[Lcom/peel/control/n;

    iget-object v1, p0, Lcom/peel/control/h;->d:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v1

    aget-object v0, v0, v1

    iput-object v0, p0, Lcom/peel/control/h;->g:Lcom/peel/control/n;

    .line 119
    new-instance v0, Lcom/peel/control/k;

    invoke-direct {v0}, Lcom/peel/control/k;-><init>()V

    iput-object v0, p0, Lcom/peel/control/h;->h:Lcom/peel/control/k;

    .line 150
    iput-object p1, p0, Lcom/peel/control/h;->b:Lcom/peel/data/g;

    .line 151
    return-void
.end method

.method public static a(IILjava/lang/String;ZLjava/lang/String;ILandroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;)Lcom/peel/control/h;
    .locals 10

    .prologue
    .line 156
    packed-switch p0, :pswitch_data_0

    .line 158
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "bad device - category "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 161
    :pswitch_0
    new-instance v1, Lcom/peel/control/b/l;

    invoke-direct {v1, p1, p2, p3}, Lcom/peel/control/b/l;-><init>(ILjava/lang/String;Z)V

    .line 187
    :goto_0
    if-eqz v1, :cond_0

    .line 188
    invoke-virtual {v1}, Lcom/peel/control/h;->g()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/peel/control/h;->a(I)V

    .line 190
    :cond_0
    return-object v1

    .line 165
    :pswitch_1
    const-string/jumbo v1, "DirecTV"

    move-object/from16 v0, p7

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 166
    new-instance v1, Lcom/peel/control/b/b;

    const-string/jumbo v3, "DirecTV"

    move v2, p1

    move v4, p3

    move-object v5, p4

    move v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    invoke-direct/range {v1 .. v9}, Lcom/peel/control/b/b;-><init>(ILjava/lang/String;ZLjava/lang/String;ILandroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 167
    :cond_1
    const-string/jumbo v1, "Roku"

    move-object/from16 v0, p7

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 168
    new-instance v1, Lcom/peel/control/b/m;

    const-string/jumbo v3, "Roku"

    move v2, p1

    move v4, p3

    move-object v5, p4

    move v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    invoke-direct/range {v1 .. v9}, Lcom/peel/control/b/m;-><init>(ILjava/lang/String;ZLjava/lang/String;ILandroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 169
    :cond_2
    const-string/jumbo v1, "Chromecast"

    move-object/from16 v0, p7

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 170
    new-instance v1, Lcom/peel/control/b/a;

    const-string/jumbo v3, "Chromecast"

    move v2, p1

    move v4, p3

    move-object v5, p4

    move v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    invoke-direct/range {v1 .. v9}, Lcom/peel/control/b/a;-><init>(ILjava/lang/String;ZLjava/lang/String;ILandroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 172
    :cond_3
    const/4 v1, 0x0

    goto :goto_0

    .line 156
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static a(Lcom/peel/data/g;)Lcom/peel/control/h;
    .locals 3

    .prologue
    .line 235
    invoke-virtual {p0}, Lcom/peel/data/g;->e()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 237
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "bad device - category "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/peel/data/g;->e()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 240
    :pswitch_0
    new-instance v0, Lcom/peel/control/b/l;

    invoke-direct {v0, p0}, Lcom/peel/control/b/l;-><init>(Lcom/peel/data/g;)V

    .line 269
    :goto_0
    if-eqz v0, :cond_0

    .line 270
    invoke-virtual {v0}, Lcom/peel/control/h;->g()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/peel/control/h;->a(I)V

    .line 272
    :cond_0
    return-object v0

    .line 243
    :pswitch_1
    invoke-virtual {p0}, Lcom/peel/data/g;->l()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "DirecTV"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 244
    new-instance v0, Lcom/peel/control/b/b;

    invoke-direct {v0, p0}, Lcom/peel/control/b/b;-><init>(Lcom/peel/data/g;)V

    goto :goto_0

    .line 245
    :cond_1
    invoke-virtual {p0}, Lcom/peel/data/g;->l()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "Roku"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 246
    new-instance v0, Lcom/peel/control/b/m;

    invoke-direct {v0, p0}, Lcom/peel/control/b/m;-><init>(Lcom/peel/data/g;)V

    goto :goto_0

    .line 247
    :cond_2
    invoke-virtual {p0}, Lcom/peel/data/g;->l()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "Chromecast"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 248
    new-instance v0, Lcom/peel/control/b/a;

    invoke-direct {v0, p0}, Lcom/peel/control/b/a;-><init>(Lcom/peel/data/g;)V

    goto :goto_0

    .line 250
    :cond_3
    const/4 v0, 0x0

    goto :goto_0

    .line 235
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static a(Ljava/lang/String;IILjava/lang/String;ZLjava/lang/String;ILandroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;)Lcom/peel/control/h;
    .locals 11

    .prologue
    .line 196
    packed-switch p1, :pswitch_data_0

    .line 198
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "bad device - category "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 201
    :pswitch_0
    new-instance v1, Lcom/peel/control/b/l;

    invoke-direct {v1, p0, p2, p3, p4}, Lcom/peel/control/b/l;-><init>(Ljava/lang/String;ILjava/lang/String;Z)V

    .line 227
    :goto_0
    if-eqz v1, :cond_0

    .line 228
    invoke-virtual {v1}, Lcom/peel/control/h;->g()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/peel/control/h;->a(I)V

    .line 230
    :cond_0
    return-object v1

    .line 205
    :pswitch_1
    const-string/jumbo v1, "DirecTV"

    move-object/from16 v0, p8

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 206
    new-instance v1, Lcom/peel/control/b/b;

    const-string/jumbo v4, "DirecTV"

    move-object v2, p0

    move v3, p2

    move v5, p4

    move-object/from16 v6, p5

    move/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    invoke-direct/range {v1 .. v10}, Lcom/peel/control/b/b;-><init>(Ljava/lang/String;ILjava/lang/String;ZLjava/lang/String;ILandroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 207
    :cond_1
    const-string/jumbo v1, "Roku"

    move-object/from16 v0, p8

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 208
    new-instance v1, Lcom/peel/control/b/m;

    const-string/jumbo v4, "Roku"

    move-object v2, p0

    move v3, p2

    move v5, p4

    move-object/from16 v6, p5

    move/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    invoke-direct/range {v1 .. v10}, Lcom/peel/control/b/m;-><init>(Ljava/lang/String;ILjava/lang/String;ZLjava/lang/String;ILandroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 209
    :cond_2
    const-string/jumbo v1, "Chromecast"

    move-object/from16 v0, p8

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 210
    new-instance v1, Lcom/peel/control/b/a;

    const-string/jumbo v3, "Chromecast"

    move v2, p2

    move v4, p4

    move-object/from16 v5, p5

    move/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    invoke-direct/range {v1 .. v9}, Lcom/peel/control/b/a;-><init>(ILjava/lang/String;ZLjava/lang/String;ILandroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 212
    :cond_3
    const/4 v1, 0x0

    goto :goto_0

    .line 196
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method static synthetic a(Lcom/peel/control/h;)Lcom/peel/control/k;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/peel/control/h;->h:Lcom/peel/control/k;

    return-object v0
.end method

.method static synthetic p()Ljava/lang/String;
    .locals 1

    .prologue
    .line 21
    sget-object v0, Lcom/peel/control/h;->c:Ljava/lang/String;

    return-object v0
.end method

.method private static q()Lcom/peel/control/k;
    .locals 2

    .prologue
    .line 276
    new-instance v0, Lcom/peel/control/k;

    invoke-direct {v0}, Lcom/peel/control/k;-><init>()V

    .line 277
    sget-object v1, Lcom/peel/control/h;->e:Lcom/peel/util/s;

    invoke-virtual {v0, v1}, Lcom/peel/control/k;->a(Ljava/lang/Object;)V

    .line 278
    return-object v0
.end method


# virtual methods
.method public declared-synchronized a(I)V
    .locals 1

    .prologue
    .line 421
    monitor-enter p0

    :try_start_0
    sget-object v0, Lcom/peel/control/h;->f:[Lcom/peel/control/n;

    aget-object v0, v0, p1

    iput-object v0, p0, Lcom/peel/control/h;->g:Lcom/peel/control/n;

    .line 422
    iget-object v0, p0, Lcom/peel/control/h;->d:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 423
    monitor-exit p0

    return-void

    .line 421
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a(Lcom/peel/util/s;)V
    .locals 1

    .prologue
    .line 430
    iget-object v0, p0, Lcom/peel/control/h;->h:Lcom/peel/control/k;

    invoke-virtual {v0, p1}, Lcom/peel/control/k;->a(Ljava/lang/Object;)V

    .line 431
    return-void
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 299
    const/4 v0, 0x1

    return v0
.end method

.method public a(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 294
    iget-object v0, p0, Lcom/peel/control/h;->b:Lcom/peel/data/g;

    invoke-virtual {v0, p1}, Lcom/peel/data/g;->a(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public a(Ljava/net/URI;)Z
    .locals 1

    .prologue
    .line 335
    const/4 v0, 0x0

    return v0
.end method

.method public b()V
    .locals 0

    .prologue
    .line 309
    return-void
.end method

.method public b(Lcom/peel/util/s;)V
    .locals 1

    .prologue
    .line 434
    iget-object v0, p0, Lcom/peel/control/h;->h:Lcom/peel/control/k;

    invoke-virtual {v0, p1}, Lcom/peel/control/k;->b(Ljava/lang/Object;)V

    .line 435
    return-void
.end method

.method public b(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 325
    const/4 v0, 0x0

    return v0
.end method

.method public c(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 330
    const/4 v0, 0x0

    return v0
.end method

.method public c()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 339
    const/4 v0, 0x0

    return-object v0
.end method

.method public d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 347
    iget-object v0, p0, Lcom/peel/control/h;->b:Lcom/peel/data/g;

    invoke-virtual {v0}, Lcom/peel/data/g;->b()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public e()I
    .locals 1

    .prologue
    .line 351
    iget-object v0, p0, Lcom/peel/control/h;->b:Lcom/peel/data/g;

    invoke-virtual {v0}, Lcom/peel/data/g;->d()I

    move-result v0

    return v0
.end method

.method public f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 355
    iget-object v0, p0, Lcom/peel/control/h;->b:Lcom/peel/data/g;

    invoke-virtual {v0}, Lcom/peel/data/g;->f()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public g()Z
    .locals 1

    .prologue
    .line 359
    iget-object v0, p0, Lcom/peel/control/h;->b:Lcom/peel/data/g;

    invoke-virtual {v0}, Lcom/peel/data/g;->g()Z

    move-result v0

    return v0
.end method

.method public h()I
    .locals 1

    .prologue
    .line 363
    iget-object v0, p0, Lcom/peel/control/h;->b:Lcom/peel/data/g;

    invoke-virtual {v0}, Lcom/peel/data/g;->h()I

    move-result v0

    return v0
.end method

.method public i()Ljava/lang/String;
    .locals 1

    .prologue
    .line 367
    iget-object v0, p0, Lcom/peel/control/h;->b:Lcom/peel/data/g;

    invoke-virtual {v0}, Lcom/peel/data/g;->i()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public j()I
    .locals 1

    .prologue
    .line 371
    iget-object v0, p0, Lcom/peel/control/h;->b:Lcom/peel/data/g;

    invoke-virtual {v0}, Lcom/peel/data/g;->j()I

    move-result v0

    return v0
.end method

.method public k()Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 375
    iget-object v0, p0, Lcom/peel/control/h;->b:Lcom/peel/data/g;

    invoke-virtual {v0}, Lcom/peel/data/g;->k()Landroid/os/Bundle;

    move-result-object v0

    return-object v0
.end method

.method public l()Lcom/peel/data/g;
    .locals 1

    .prologue
    .line 395
    iget-object v0, p0, Lcom/peel/control/h;->b:Lcom/peel/data/g;

    return-object v0
.end method

.method protected m()V
    .locals 0

    .prologue
    .line 400
    return-void
.end method

.method public final n()V
    .locals 3

    .prologue
    .line 403
    invoke-static {}, Lcom/peel/util/i;->c()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/peel/control/h;->m()V

    .line 409
    :goto_0
    return-void

    .line 404
    :cond_0
    sget-object v0, Lcom/peel/control/h;->c:Ljava/lang/String;

    const-string/jumbo v1, "disconnect"

    new-instance v2, Lcom/peel/control/j;

    invoke-direct {v2, p0}, Lcom/peel/control/j;-><init>(Lcom/peel/control/h;)V

    invoke-static {v0, v1, v2}, Lcom/peel/util/i;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    goto :goto_0
.end method

.method public o()I
    .locals 1

    .prologue
    .line 426
    iget-object v0, p0, Lcom/peel/control/h;->d:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 438
    iget-object v0, p0, Lcom/peel/control/h;->b:Lcom/peel/data/g;

    if-nez v0, :cond_0

    const-string/jumbo v0, "null"

    .line 478
    :goto_0
    return-object v0

    .line 440
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/peel/control/h;->b:Lcom/peel/data/g;

    invoke-virtual {v2}, Lcom/peel/data/g;->f()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 441
    iget-object v1, p0, Lcom/peel/control/h;->b:Lcom/peel/data/g;

    invoke-virtual {v1}, Lcom/peel/data/g;->d()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 476
    :goto_1
    :pswitch_0
    const-string/jumbo v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 477
    iget-object v1, p0, Lcom/peel/control/h;->b:Lcom/peel/data/g;

    invoke-virtual {v1}, Lcom/peel/data/g;->g()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 478
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 443
    :pswitch_1
    const-string/jumbo v1, "AC"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 446
    :pswitch_2
    const-string/jumbo v1, "A/V Receiver"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 449
    :pswitch_3
    const-string/jumbo v1, "Bluray Player"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 452
    :pswitch_4
    const-string/jumbo v1, "Bluray HT"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 455
    :pswitch_5
    const-string/jumbo v1, "DVD Player"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 458
    :pswitch_6
    const-string/jumbo v1, "DVD HT"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 461
    :pswitch_7
    const-string/jumbo v1, "DVR"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 464
    :pswitch_8
    const-string/jumbo v1, "Projector"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 467
    :pswitch_9
    const-string/jumbo v1, "STB"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 470
    :pswitch_a
    const-string/jumbo v1, "Streaming Media Player"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 473
    :pswitch_b
    const-string/jumbo v1, "TV"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 441
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_b
        :pswitch_9
        :pswitch_5
        :pswitch_3
        :pswitch_2
        :pswitch_a
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_8
        :pswitch_0
        :pswitch_0
        :pswitch_6
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_7
    .end packed-switch
.end method
