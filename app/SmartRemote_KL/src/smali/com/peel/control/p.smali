.class Lcom/peel/control/p;
.super Lcom/peel/util/s;


# instance fields
.field final synthetic a:Lcom/peel/control/o;


# direct methods
.method constructor <init>(Lcom/peel/control/o;)V
    .locals 0

    .prologue
    .line 33
    iput-object p1, p0, Lcom/peel/control/p;->a:Lcom/peel/control/o;

    invoke-direct {p0}, Lcom/peel/util/s;-><init>()V

    return-void
.end method


# virtual methods
.method public final varargs a(ILjava/lang/Object;[Ljava/lang/Object;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v3, 0x0

    .line 36
    iget-object v0, p0, Lcom/peel/control/p;->a:Lcom/peel/control/o;

    iget-object v0, v0, Lcom/peel/control/o;->b:Lcom/peel/data/h;

    invoke-virtual {p2, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    instance-of v0, p2, Lcom/peel/control/h;

    if-nez v0, :cond_1

    .line 96
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 38
    :cond_1
    packed-switch p1, :pswitch_data_0

    :pswitch_1
    goto :goto_0

    .line 77
    :pswitch_2
    sget-object v0, Lcom/peel/control/o;->a:Lcom/peel/control/s;

    const/16 v1, 0xd

    invoke-virtual {v0, v1, p0, p3}, Lcom/peel/control/s;->a(ILjava/lang/Object;[Ljava/lang/Object;)V

    goto :goto_0

    .line 44
    :pswitch_3
    iget-object v0, p0, Lcom/peel/control/p;->a:Lcom/peel/control/o;

    invoke-virtual {v0}, Lcom/peel/control/o;->j()I

    move-result v0

    if-ne v1, v0, :cond_0

    iget-object v0, p0, Lcom/peel/control/p;->a:Lcom/peel/control/o;

    invoke-virtual {v0, v3}, Lcom/peel/control/o;->b(I)V

    goto :goto_0

    .line 50
    :pswitch_4
    iget-object v0, p0, Lcom/peel/control/p;->a:Lcom/peel/control/o;

    invoke-virtual {v0}, Lcom/peel/control/o;->j()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/peel/control/p;->a:Lcom/peel/control/o;

    invoke-virtual {v0, v1}, Lcom/peel/control/o;->b(I)V

    goto :goto_0

    .line 53
    :pswitch_5
    invoke-static {}, Lcom/peel/control/o;->k()Ljava/lang/String;

    move-result-object v1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "fruit error "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    aget-object v0, p3, v3

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 81
    :pswitch_6
    sget-object v0, Lcom/peel/control/o;->a:Lcom/peel/control/s;

    const/16 v1, 0xe

    invoke-virtual {v0, v1, p0, p3}, Lcom/peel/control/s;->a(ILjava/lang/Object;[Ljava/lang/Object;)V

    goto :goto_0

    .line 85
    :pswitch_7
    sget-object v0, Lcom/peel/control/o;->a:Lcom/peel/control/s;

    const/16 v1, 0xf

    invoke-virtual {v0, v1, p0, p3}, Lcom/peel/control/s;->a(ILjava/lang/Object;[Ljava/lang/Object;)V

    goto :goto_0

    .line 89
    :pswitch_8
    sget-object v0, Lcom/peel/control/o;->a:Lcom/peel/control/s;

    const/16 v1, 0x10

    invoke-virtual {v0, v1, p0, p3}, Lcom/peel/control/s;->a(ILjava/lang/Object;[Ljava/lang/Object;)V

    goto :goto_0

    .line 93
    :pswitch_9
    sget-object v0, Lcom/peel/control/o;->a:Lcom/peel/control/s;

    const/16 v1, 0x11

    invoke-virtual {v0, v1, p0, p3}, Lcom/peel/control/s;->a(ILjava/lang/Object;[Ljava/lang/Object;)V

    goto :goto_0

    .line 38
    nop

    :pswitch_data_0
    .packed-switch 0xd
        :pswitch_2
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_3
        :pswitch_1
        :pswitch_4
        :pswitch_5
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method
