.class public Lcom/peel/control/o;
.super Ljava/lang/Object;


# static fields
.field public static final a:Lcom/peel/control/s;

.field private static final d:Ljava/lang/String;

.field private static final e:[Lcom/peel/control/t;


# instance fields
.field protected b:Lcom/peel/data/h;

.field private final c:Ljava/util/concurrent/atomic/AtomicInteger;

.field private volatile f:Lcom/peel/control/t;

.field private final g:Lcom/peel/util/s;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 15
    new-instance v0, Lcom/peel/control/s;

    invoke-direct {v0}, Lcom/peel/control/s;-><init>()V

    sput-object v0, Lcom/peel/control/o;->a:Lcom/peel/control/s;

    .line 26
    const-class v0, Lcom/peel/control/o;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/peel/control/o;->d:Ljava/lang/String;

    .line 30
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/peel/control/t;

    const/4 v1, 0x0

    new-instance v2, Lcom/peel/control/r;

    invoke-direct {v2}, Lcom/peel/control/r;-><init>()V

    aput-object v2, v0, v1

    const/4 v1, 0x1

    new-instance v2, Lcom/peel/control/q;

    invoke-direct {v2}, Lcom/peel/control/q;-><init>()V

    aput-object v2, v0, v1

    sput-object v0, Lcom/peel/control/o;->e:[Lcom/peel/control/t;

    return-void
.end method

.method protected constructor <init>(Lcom/peel/data/h;)V
    .locals 2

    .prologue
    .line 99
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    iput-object v0, p0, Lcom/peel/control/o;->c:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 31
    sget-object v0, Lcom/peel/control/o;->e:[Lcom/peel/control/t;

    iget-object v1, p0, Lcom/peel/control/o;->c:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v1

    aget-object v0, v0, v1

    iput-object v0, p0, Lcom/peel/control/o;->f:Lcom/peel/control/t;

    .line 33
    new-instance v0, Lcom/peel/control/p;

    invoke-direct {v0, p0}, Lcom/peel/control/p;-><init>(Lcom/peel/control/o;)V

    iput-object v0, p0, Lcom/peel/control/o;->g:Lcom/peel/util/s;

    .line 99
    iput-object p1, p0, Lcom/peel/control/o;->b:Lcom/peel/data/h;

    return-void
.end method

.method public static a(ILjava/lang/String;)Lcom/peel/control/o;
    .locals 3

    .prologue
    .line 104
    sparse-switch p0, :sswitch_data_0

    .line 106
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "bad fruit - category "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 109
    :sswitch_0
    new-instance v0, Lcom/peel/control/d/f;

    invoke-direct {v0}, Lcom/peel/control/d/f;-><init>()V

    .line 116
    :goto_0
    invoke-virtual {v0}, Lcom/peel/control/o;->i()V

    .line 117
    return-object v0

    .line 113
    :sswitch_1
    new-instance v0, Lcom/peel/control/d/e;

    invoke-direct {v0}, Lcom/peel/control/d/e;-><init>()V

    goto :goto_0

    .line 104
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x14 -> :sswitch_1
    .end sparse-switch
.end method

.method public static a(Lcom/peel/data/h;)Lcom/peel/control/o;
    .locals 3

    .prologue
    .line 123
    invoke-virtual {p0}, Lcom/peel/data/h;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 125
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "bad fruit - category "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/peel/data/h;->a()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 128
    :sswitch_0
    new-instance v0, Lcom/peel/control/d/f;

    invoke-direct {v0, p0}, Lcom/peel/control/d/f;-><init>(Lcom/peel/data/h;)V

    .line 135
    :goto_0
    invoke-virtual {v0}, Lcom/peel/control/o;->i()V

    .line 136
    return-object v0

    .line 132
    :sswitch_1
    new-instance v0, Lcom/peel/control/d/e;

    invoke-direct {v0, p0}, Lcom/peel/control/d/e;-><init>(Lcom/peel/data/h;)V

    goto :goto_0

    .line 123
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x14 -> :sswitch_1
    .end sparse-switch
.end method

.method static synthetic k()Ljava/lang/String;
    .locals 1

    .prologue
    .line 14
    sget-object v0, Lcom/peel/control/o;->d:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 141
    iget-object v0, p0, Lcom/peel/control/o;->b:Lcom/peel/data/h;

    invoke-virtual {v0}, Lcom/peel/data/h;->c()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a(Lcom/peel/util/s;)V
    .locals 1

    .prologue
    .line 203
    sget-object v0, Lcom/peel/control/o;->a:Lcom/peel/control/s;

    invoke-virtual {v0, p1}, Lcom/peel/control/s;->a(Ljava/lang/Object;)V

    return-void
.end method

.method public a(ZLjava/lang/String;Z)V
    .locals 0

    .prologue
    .line 145
    return-void
.end method

.method public a(I)Z
    .locals 1

    .prologue
    .line 167
    const/4 v0, 0x0

    return v0
.end method

.method public a(Ljava/util/List;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;>;)Z"
        }
    .end annotation

    .prologue
    .line 160
    const/4 v0, 0x0

    return v0
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 143
    iget-object v0, p0, Lcom/peel/control/o;->b:Lcom/peel/data/h;

    invoke-virtual {v0}, Lcom/peel/data/h;->e()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public declared-synchronized b(I)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 191
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/peel/control/o;->c:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V

    .line 192
    sget-object v0, Lcom/peel/control/o;->e:[Lcom/peel/control/t;

    iget-object v1, p0, Lcom/peel/control/o;->c:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v1

    aget-object v0, v0, v1

    iput-object v0, p0, Lcom/peel/control/o;->f:Lcom/peel/control/t;

    .line 194
    if-nez p1, :cond_1

    .line 195
    sget-object v1, Lcom/peel/control/o;->a:Lcom/peel/control/s;

    const/4 v2, 0x0

    const/4 v0, 0x0

    check-cast v0, [Ljava/lang/Object;

    invoke-virtual {v1, v2, p0, v0}, Lcom/peel/control/s;->a(ILjava/lang/Object;[Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 199
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 196
    :cond_1
    if-ne v2, p1, :cond_0

    .line 197
    :try_start_1
    sget-object v1, Lcom/peel/control/o;->a:Lcom/peel/control/s;

    const/4 v2, 0x1

    const/4 v0, 0x0

    check-cast v0, [Ljava/lang/Object;

    invoke-virtual {v1, v2, p0, v0}, Lcom/peel/control/s;->a(ILjava/lang/Object;[Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 191
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public b(Lcom/peel/util/s;)V
    .locals 1

    .prologue
    .line 205
    sget-object v0, Lcom/peel/control/o;->a:Lcom/peel/control/s;

    invoke-virtual {v0, p1}, Lcom/peel/control/s;->b(Ljava/lang/Object;)V

    return-void
.end method

.method public c()V
    .locals 0

    .prologue
    .line 147
    return-void
.end method

.method public d()Lcom/peel/data/h;
    .locals 1

    .prologue
    .line 150
    iget-object v0, p0, Lcom/peel/control/o;->b:Lcom/peel/data/h;

    return-object v0
.end method

.method public e()Z
    .locals 1

    .prologue
    .line 155
    const/4 v0, 0x0

    return v0
.end method

.method public f()Z
    .locals 1

    .prologue
    .line 172
    const/4 v0, 0x0

    return v0
.end method

.method public g()Z
    .locals 1

    .prologue
    .line 177
    const/4 v0, 0x0

    return v0
.end method

.method public h()Z
    .locals 1

    .prologue
    .line 182
    const/4 v0, 0x0

    return v0
.end method

.method public i()V
    .locals 2

    .prologue
    .line 185
    sget-object v0, Lcom/peel/control/o;->a:Lcom/peel/control/s;

    iget-object v1, p0, Lcom/peel/control/o;->g:Lcom/peel/util/s;

    invoke-virtual {v0, v1}, Lcom/peel/control/s;->a(Ljava/lang/Object;)V

    return-void
.end method

.method public j()I
    .locals 1

    .prologue
    .line 201
    iget-object v0, p0, Lcom/peel/control/o;->c:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v0

    return v0
.end method
