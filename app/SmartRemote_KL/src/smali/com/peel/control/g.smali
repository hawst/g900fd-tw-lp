.class public Lcom/peel/control/g;
.super Lcom/peel/control/f;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 421
    invoke-direct {p0}, Lcom/peel/control/f;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/peel/control/a;I)Z
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 424
    invoke-static {p2}, Lcom/peel/control/a;->d(I)I

    .line 426
    invoke-static {p1}, Lcom/peel/control/a;->a(Lcom/peel/control/a;)Ljava/util/concurrent/atomic/AtomicInteger;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V

    .line 428
    iget-object v0, p1, Lcom/peel/control/a;->a:Lcom/peel/data/d;

    invoke-virtual {v0}, Lcom/peel/data/d;->c()Ljava/lang/Iterable;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    .line 429
    sget-object v2, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    const-string/jumbo v3, "id"

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v2, v0}, Lcom/peel/control/am;->c(Ljava/lang/String;)Lcom/peel/control/h;

    move-result-object v0

    .line 430
    if-eqz v0, :cond_0

    .line 432
    invoke-virtual {v0}, Lcom/peel/control/h;->a()Z

    move-result v2

    if-nez v2, :cond_0

    .line 433
    invoke-static {p1}, Lcom/peel/control/a;->a(Lcom/peel/control/a;)Ljava/util/concurrent/atomic/AtomicInteger;

    move-result-object v2

    invoke-virtual {v2, v4}, Ljava/util/concurrent/atomic/AtomicInteger;->addAndGet(I)I

    .line 434
    invoke-virtual {v0}, Lcom/peel/control/h;->b()V

    goto :goto_0

    .line 438
    :cond_1
    invoke-static {p1}, Lcom/peel/control/a;->a(Lcom/peel/control/a;)Ljava/util/concurrent/atomic/AtomicInteger;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v0

    if-nez v0, :cond_2

    .line 440
    invoke-virtual {p1, v4}, Lcom/peel/control/a;->c(I)V

    .line 441
    invoke-virtual {p1, p2}, Lcom/peel/control/a;->b(I)Z

    .line 444
    :cond_2
    return v4
.end method
