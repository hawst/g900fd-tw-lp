.class final enum Lcom/peel/control/z;
.super Ljava/lang/Enum;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/peel/control/z;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/peel/control/z;

.field public static final enum b:Lcom/peel/control/z;

.field public static final enum c:Lcom/peel/control/z;

.field private static final synthetic d:[Lcom/peel/control/z;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 629
    new-instance v0, Lcom/peel/control/z;

    const-string/jumbo v1, "ONE"

    invoke-direct {v0, v1, v2}, Lcom/peel/control/z;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/peel/control/z;->a:Lcom/peel/control/z;

    new-instance v0, Lcom/peel/control/z;

    const-string/jumbo v1, "MINUS_ONE"

    invoke-direct {v0, v1, v3}, Lcom/peel/control/z;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/peel/control/z;->b:Lcom/peel/control/z;

    new-instance v0, Lcom/peel/control/z;

    const-string/jumbo v1, "ZERO"

    invoke-direct {v0, v1, v4}, Lcom/peel/control/z;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/peel/control/z;->c:Lcom/peel/control/z;

    .line 628
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/peel/control/z;

    sget-object v1, Lcom/peel/control/z;->a:Lcom/peel/control/z;

    aput-object v1, v0, v2

    sget-object v1, Lcom/peel/control/z;->b:Lcom/peel/control/z;

    aput-object v1, v0, v3

    sget-object v1, Lcom/peel/control/z;->c:Lcom/peel/control/z;

    aput-object v1, v0, v4

    sput-object v0, Lcom/peel/control/z;->d:[Lcom/peel/control/z;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 628
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/peel/control/z;
    .locals 1

    .prologue
    .line 628
    const-class v0, Lcom/peel/control/z;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/peel/control/z;

    return-object v0
.end method

.method public static values()[Lcom/peel/control/z;
    .locals 1

    .prologue
    .line 628
    sget-object v0, Lcom/peel/control/z;->d:[Lcom/peel/control/z;

    invoke-virtual {v0}, [Lcom/peel/control/z;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/peel/control/z;

    return-object v0
.end method
