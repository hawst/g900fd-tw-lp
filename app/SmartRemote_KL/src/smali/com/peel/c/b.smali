.class public final enum Lcom/peel/c/b;
.super Ljava/lang/Enum;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/peel/c/b;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/peel/c/b;

.field public static final enum b:Lcom/peel/c/b;

.field public static final enum c:Lcom/peel/c/b;

.field private static final synthetic e:[Lcom/peel/c/b;


# instance fields
.field private d:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 13
    new-instance v0, Lcom/peel/c/b;

    const-string/jumbo v1, "SSR_S4"

    const-string/jumbo v2, "WatchON"

    invoke-direct {v0, v1, v3, v2}, Lcom/peel/c/b;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/peel/c/b;->a:Lcom/peel/c/b;

    .line 18
    new-instance v0, Lcom/peel/c/b;

    const-string/jumbo v1, "SSR"

    const-string/jumbo v2, "SamsungSmartRemote"

    invoke-direct {v0, v1, v4, v2}, Lcom/peel/c/b;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/peel/c/b;->b:Lcom/peel/c/b;

    .line 23
    new-instance v0, Lcom/peel/c/b;

    const-string/jumbo v1, "PSR"

    const-string/jumbo v2, "PeelSmartRemote"

    invoke-direct {v0, v1, v5, v2}, Lcom/peel/c/b;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/peel/c/b;->c:Lcom/peel/c/b;

    .line 8
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/peel/c/b;

    sget-object v1, Lcom/peel/c/b;->a:Lcom/peel/c/b;

    aput-object v1, v0, v3

    sget-object v1, Lcom/peel/c/b;->b:Lcom/peel/c/b;

    aput-object v1, v0, v4

    sget-object v1, Lcom/peel/c/b;->c:Lcom/peel/c/b;

    aput-object v1, v0, v5

    sput-object v0, Lcom/peel/c/b;->e:[Lcom/peel/c/b;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 27
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 28
    iput-object p3, p0, Lcom/peel/c/b;->d:Ljava/lang/String;

    .line 29
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/peel/c/b;
    .locals 1

    .prologue
    .line 8
    const-class v0, Lcom/peel/c/b;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/peel/c/b;

    return-object v0
.end method

.method public static values()[Lcom/peel/c/b;
    .locals 1

    .prologue
    .line 8
    sget-object v0, Lcom/peel/c/b;->e:[Lcom/peel/c/b;

    invoke-virtual {v0}, [Lcom/peel/c/b;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/peel/c/b;

    return-object v0
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/peel/c/b;->d:Ljava/lang/String;

    return-object v0
.end method
