.class public final enum Lcom/peel/c/d;
.super Ljava/lang/Enum;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/peel/c/d;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/peel/c/d;

.field public static final enum b:Lcom/peel/c/d;

.field public static final enum c:Lcom/peel/c/d;

.field public static final enum d:Lcom/peel/c/d;

.field private static final synthetic e:[Lcom/peel/c/d;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 8
    new-instance v0, Lcom/peel/c/d;

    const-string/jumbo v1, "PROD"

    invoke-direct {v0, v1, v2}, Lcom/peel/c/d;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/peel/c/d;->a:Lcom/peel/c/d;

    .line 9
    new-instance v0, Lcom/peel/c/d;

    const-string/jumbo v1, "STAGING"

    invoke-direct {v0, v1, v3}, Lcom/peel/c/d;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/peel/c/d;->b:Lcom/peel/c/d;

    .line 10
    new-instance v0, Lcom/peel/c/d;

    const-string/jumbo v1, "ANDROID_EMULATOR"

    invoke-direct {v0, v1, v4}, Lcom/peel/c/d;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/peel/c/d;->c:Lcom/peel/c/d;

    .line 11
    new-instance v0, Lcom/peel/c/d;

    const-string/jumbo v1, "TESTING"

    invoke-direct {v0, v1, v5}, Lcom/peel/c/d;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/peel/c/d;->d:Lcom/peel/c/d;

    .line 7
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/peel/c/d;

    sget-object v1, Lcom/peel/c/d;->a:Lcom/peel/c/d;

    aput-object v1, v0, v2

    sget-object v1, Lcom/peel/c/d;->b:Lcom/peel/c/d;

    aput-object v1, v0, v3

    sget-object v1, Lcom/peel/c/d;->c:Lcom/peel/c/d;

    aput-object v1, v0, v4

    sget-object v1, Lcom/peel/c/d;->d:Lcom/peel/c/d;

    aput-object v1, v0, v5

    sput-object v0, Lcom/peel/c/d;->e:[Lcom/peel/c/d;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 7
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/peel/c/d;
    .locals 1

    .prologue
    .line 7
    const-class v0, Lcom/peel/c/d;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/peel/c/d;

    return-object v0
.end method

.method public static values()[Lcom/peel/c/d;
    .locals 1

    .prologue
    .line 7
    sget-object v0, Lcom/peel/c/d;->e:[Lcom/peel/c/d;

    invoke-virtual {v0}, [Lcom/peel/c/d;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/peel/c/d;

    return-object v0
.end method
