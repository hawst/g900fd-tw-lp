.class public final Lcom/peel/c/a;
.super Ljava/lang/Object;


# static fields
.field public static final a:Lcom/peel/c/e;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/peel/c/e",
            "<",
            "Landroid/content/Context;",
            ">;"
        }
    .end annotation
.end field

.field public static final b:Lcom/peel/c/e;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/peel/c/e",
            "<",
            "Lcom/peel/c/b;",
            ">;"
        }
    .end annotation
.end field

.field public static final c:Lcom/peel/c/e;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/peel/c/e",
            "<",
            "Lcom/peel/c/d;",
            ">;"
        }
    .end annotation
.end field

.field public static final d:Lcom/peel/c/e;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/peel/c/e",
            "<",
            "Lcom/peel/ui/ni;",
            ">;"
        }
    .end annotation
.end field

.field public static final e:Lcom/peel/c/e;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/peel/c/e",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static final f:Lcom/peel/c/e;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/peel/c/e",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static final g:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private static final h:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 19
    new-instance v0, Lcom/peel/c/e;

    const-string/jumbo v1, "appContext"

    invoke-direct {v0, v1}, Lcom/peel/c/e;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/peel/c/a;->a:Lcom/peel/c/e;

    .line 20
    new-instance v0, Lcom/peel/c/e;

    const-string/jumbo v1, "peelAppType"

    invoke-direct {v0, v1}, Lcom/peel/c/e;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/peel/c/a;->b:Lcom/peel/c/e;

    .line 21
    new-instance v0, Lcom/peel/c/e;

    const-string/jumbo v1, "serverEnv"

    invoke-direct {v0, v1}, Lcom/peel/c/e;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/peel/c/a;->c:Lcom/peel/c/e;

    .line 22
    new-instance v0, Lcom/peel/c/e;

    const-string/jumbo v1, "viewMap"

    invoke-direct {v0, v1}, Lcom/peel/c/e;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/peel/c/a;->d:Lcom/peel/c/e;

    .line 23
    new-instance v0, Lcom/peel/c/e;

    const-string/jumbo v1, "socialEnabled"

    invoke-direct {v0, v1}, Lcom/peel/c/e;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/peel/c/a;->e:Lcom/peel/c/e;

    .line 24
    new-instance v0, Lcom/peel/c/e;

    const-string/jumbo v1, "productId"

    invoke-direct {v0, v1}, Lcom/peel/c/e;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/peel/c/a;->f:Lcom/peel/c/e;

    .line 26
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    sput-object v0, Lcom/peel/c/a;->g:Ljava/util/Map;

    .line 27
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    sput-object v0, Lcom/peel/c/a;->h:Ljava/util/Map;

    return-void
.end method

.method public static a(Lcom/peel/c/e;)Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/peel/c/e",
            "<TT;>;)TT;"
        }
    .end annotation

    .prologue
    .line 60
    invoke-virtual {p0}, Lcom/peel/c/e;->a()Ljava/lang/String;

    move-result-object v0

    .line 61
    sget-object v1, Lcom/peel/c/a;->g:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 62
    if-nez v1, :cond_0

    .line 63
    sget-object v2, Lcom/peel/c/a;->h:Ljava/util/Map;

    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/c/c;

    .line 64
    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/peel/c/c;->a()Ljava/lang/Object;

    move-result-object v0

    .line 66
    :goto_0
    return-object v0

    :cond_0
    move-object v0, v1

    goto :goto_0
.end method

.method public static a(Lcom/peel/c/e;Ljava/lang/Object;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/peel/c/e",
            "<TT;>;TT;)V"
        }
    .end annotation

    .prologue
    .line 30
    sget-object v0, Lcom/peel/c/a;->g:Ljava/util/Map;

    invoke-virtual {p0}, Lcom/peel/c/e;->a()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 31
    return-void
.end method
