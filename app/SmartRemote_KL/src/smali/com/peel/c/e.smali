.class public final Lcom/peel/c/e;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    invoke-static {p1}, Lcom/peel/util/dp;->a(Ljava/lang/Object;)V

    .line 17
    iput-object p1, p0, Lcom/peel/c/e;->a:Ljava/lang/String;

    .line 18
    return-void
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/peel/c/e;->a:Ljava/lang/String;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 31
    if-ne p0, p1, :cond_0

    .line 32
    const/4 v0, 0x1

    .line 38
    :goto_0
    return v0

    .line 34
    :cond_0
    if-nez p1, :cond_1

    .line 35
    const/4 v0, 0x0

    goto :goto_0

    .line 37
    :cond_1
    check-cast p1, Lcom/peel/c/e;

    .line 38
    iget-object v0, p0, Lcom/peel/c/e;->a:Ljava/lang/String;

    iget-object v1, p1, Lcom/peel/c/e;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lcom/peel/c/e;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/peel/c/e;->a:Ljava/lang/String;

    return-object v0
.end method
