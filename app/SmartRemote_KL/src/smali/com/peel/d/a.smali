.class public Lcom/peel/d/a;
.super Ljava/lang/Object;


# instance fields
.field public final a:Lcom/peel/d/d;

.field public final b:Lcom/peel/d/c;

.field public final c:Lcom/peel/d/b;

.field public final d:Ljava/lang/String;

.field public final e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public f:Z

.field public g:Z


# direct methods
.method public constructor <init>(Lcom/peel/d/d;Lcom/peel/d/b;Lcom/peel/d/c;Ljava/lang/String;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/peel/d/d;",
            "Lcom/peel/d/b;",
            "Lcom/peel/d/c;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-boolean v0, p0, Lcom/peel/d/a;->f:Z

    .line 22
    iput-boolean v0, p0, Lcom/peel/d/a;->g:Z

    .line 25
    iput-object p5, p0, Lcom/peel/d/a;->e:Ljava/util/List;

    .line 26
    iput-object p1, p0, Lcom/peel/d/a;->a:Lcom/peel/d/d;

    .line 27
    iput-object p2, p0, Lcom/peel/d/a;->c:Lcom/peel/d/b;

    .line 28
    iput-object p3, p0, Lcom/peel/d/a;->b:Lcom/peel/d/c;

    .line 29
    iput-object p4, p0, Lcom/peel/d/a;->d:Ljava/lang/String;

    .line 30
    iput-boolean v0, p0, Lcom/peel/d/a;->f:Z

    .line 31
    return-void
.end method

.method public constructor <init>(Lcom/peel/d/d;Lcom/peel/d/b;Lcom/peel/d/c;Ljava/lang/String;ZLjava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/peel/d/d;",
            "Lcom/peel/d/b;",
            "Lcom/peel/d/c;",
            "Ljava/lang/String;",
            "Z",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-boolean v0, p0, Lcom/peel/d/a;->f:Z

    .line 22
    iput-boolean v0, p0, Lcom/peel/d/a;->g:Z

    .line 34
    iput-object p6, p0, Lcom/peel/d/a;->e:Ljava/util/List;

    .line 35
    iput-object p1, p0, Lcom/peel/d/a;->a:Lcom/peel/d/d;

    .line 36
    iput-object p2, p0, Lcom/peel/d/a;->c:Lcom/peel/d/b;

    .line 37
    iput-object p3, p0, Lcom/peel/d/a;->b:Lcom/peel/d/c;

    .line 38
    iput-object p4, p0, Lcom/peel/d/a;->d:Ljava/lang/String;

    .line 39
    iput-boolean v0, p0, Lcom/peel/d/a;->f:Z

    .line 40
    if-nez p5, :cond_0

    const/4 v0, 0x1

    :cond_0
    iput-boolean v0, p0, Lcom/peel/d/a;->g:Z

    .line 41
    return-void
.end method
