.class public Lcom/peel/d/k;
.super Lcom/peel/d/u;


# static fields
.field private static final e:Ljava/lang/String;

.field private static f:Landroid/support/v7/widget/SearchView;

.field private static g:Landroid/view/MenuItem;


# instance fields
.field private aj:Z

.field private ak:Landroid/content/SharedPreferences;

.field private al:Landroid/widget/PopupWindow;

.field private am:Lcom/peel/widget/ag;

.field private h:Landroid/support/v7/app/ActionBar;

.field private i:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 67
    const-class v0, Lcom/peel/d/k;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/peel/d/k;->e:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 66
    invoke-direct {p0}, Lcom/peel/d/u;-><init>()V

    .line 71
    iput-object v1, p0, Lcom/peel/d/k;->h:Landroid/support/v7/app/ActionBar;

    .line 72
    iput-boolean v0, p0, Lcom/peel/d/k;->i:Z

    .line 73
    iput-boolean v0, p0, Lcom/peel/d/k;->aj:Z

    .line 76
    iput-object v1, p0, Lcom/peel/d/k;->am:Lcom/peel/widget/ag;

    return-void
.end method

.method static synthetic U()Landroid/support/v7/widget/SearchView;
    .locals 1

    .prologue
    .line 66
    sget-object v0, Lcom/peel/d/k;->f:Landroid/support/v7/widget/SearchView;

    return-object v0
.end method

.method static synthetic V()Ljava/lang/String;
    .locals 1

    .prologue
    .line 66
    sget-object v0, Lcom/peel/d/k;->e:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic a(Lcom/peel/d/k;)Landroid/widget/PopupWindow;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/peel/d/k;->al:Landroid/widget/PopupWindow;

    return-object v0
.end method

.method static synthetic a(Lcom/peel/d/k;Lcom/peel/widget/ag;)Lcom/peel/widget/ag;
    .locals 0

    .prologue
    .line 66
    iput-object p1, p0, Lcom/peel/d/k;->am:Lcom/peel/widget/ag;

    return-object p1
.end method

.method public static a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 246
    sget-object v0, Lcom/peel/d/k;->f:Landroid/support/v7/widget/SearchView;

    if-eqz v0, :cond_0

    .line 247
    sget-object v0, Lcom/peel/d/k;->f:Landroid/support/v7/widget/SearchView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/support/v7/widget/SearchView;->setQuery(Ljava/lang/CharSequence;Z)V

    .line 249
    :cond_0
    return-void
.end method

.method public static a(Z)V
    .locals 1

    .prologue
    .line 429
    sget-object v0, Lcom/peel/d/k;->g:Landroid/view/MenuItem;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/peel/d/k;->g:Landroid/view/MenuItem;

    invoke-interface {v0}, Landroid/view/MenuItem;->isVisible()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 430
    sget-object v0, Lcom/peel/d/k;->g:Landroid/view/MenuItem;

    invoke-interface {v0, p0}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 432
    :cond_0
    return-void
.end method

.method private ac()V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 436
    new-instance v0, Landroid/widget/PopupWindow;

    invoke-virtual {p0}, Lcom/peel/d/k;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/PopupWindow;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/peel/d/k;->al:Landroid/widget/PopupWindow;

    .line 437
    iget-object v0, p0, Lcom/peel/d/k;->al:Landroid/widget/PopupWindow;

    invoke-virtual {v0, v7}, Landroid/widget/PopupWindow;->setOutsideTouchable(Z)V

    .line 438
    iget-object v0, p0, Lcom/peel/d/k;->al:Landroid/widget/PopupWindow;

    invoke-virtual {v0, v7}, Landroid/widget/PopupWindow;->setFocusable(Z)V

    .line 440
    invoke-virtual {p0}, Lcom/peel/d/k;->n()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/peel/ui/fn;->overflow_menu_width:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    invoke-virtual {p0}, Lcom/peel/d/k;->n()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    invoke-static {v6, v0, v1}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    float-to-int v0, v0

    .line 441
    iget-object v1, p0, Lcom/peel/d/k;->al:Landroid/widget/PopupWindow;

    invoke-virtual {v1, v0}, Landroid/widget/PopupWindow;->setWidth(I)V

    .line 442
    iget-object v0, p0, Lcom/peel/d/k;->al:Landroid/widget/PopupWindow;

    const/4 v1, -0x2

    invoke-virtual {v0, v1}, Landroid/widget/PopupWindow;->setHeight(I)V

    .line 445
    iget-object v0, p0, Lcom/peel/d/k;->al:Landroid/widget/PopupWindow;

    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {v1, v6}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v0, v1}, Landroid/widget/PopupWindow;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 447
    const/4 v0, 0x4

    new-array v1, v0, [Ljava/lang/String;

    invoke-virtual {p0}, Lcom/peel/d/k;->n()Landroid/content/res/Resources;

    move-result-object v0

    sget v2, Lcom/peel/ui/ft;->label_change_room:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v1, v6

    invoke-virtual {p0}, Lcom/peel/d/k;->n()Landroid/content/res/Resources;

    move-result-object v0

    sget v2, Lcom/peel/ui/ft;->editchannels:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v1, v7

    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/peel/d/k;->n()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/peel/ui/ft;->label_settings:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    const/4 v0, 0x3

    invoke-virtual {p0}, Lcom/peel/d/k;->n()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/peel/ui/ft;->label_about:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    .line 448
    invoke-virtual {p0}, Lcom/peel/d/k;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    const-string/jumbo v2, "layout_inflater"

    invoke-virtual {v0, v2}, Landroid/support/v4/app/ae;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 449
    sget v2, Lcom/peel/ui/fq;->overflow_menu:I

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 450
    sget v0, Lcom/peel/ui/fp;->list:I

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    .line 451
    new-instance v3, Landroid/widget/ArrayAdapter;

    invoke-virtual {p0}, Lcom/peel/d/k;->m()Landroid/support/v4/app/ae;

    move-result-object v4

    sget v5, Lcom/peel/ui/fq;->menu_row:I

    invoke-direct {v3, v4, v5, v1}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    invoke-virtual {v0, v3}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 453
    new-instance v1, Lcom/peel/d/p;

    invoke-direct {v1, p0}, Lcom/peel/d/p;-><init>(Lcom/peel/d/k;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 492
    new-instance v3, Landroid/util/TypedValue;

    invoke-direct {v3}, Landroid/util/TypedValue;-><init>()V

    .line 493
    const/16 v1, 0xa

    .line 494
    invoke-virtual {p0}, Lcom/peel/d/k;->m()Landroid/support/v4/app/ae;

    move-result-object v4

    invoke-virtual {v4}, Landroid/support/v4/app/ae;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v4

    const v5, 0x10102eb

    invoke-virtual {v4, v5, v3, v7}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 495
    iget v1, v3, Landroid/util/TypedValue;->data:I

    invoke-virtual {p0}, Lcom/peel/d/k;->n()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/TypedValue;->complexToDimensionPixelSize(ILandroid/util/DisplayMetrics;)I

    move-result v1

    .line 497
    :cond_0
    new-instance v3, Lcom/peel/d/q;

    invoke-direct {v3, p0}, Lcom/peel/d/q;-><init>(Lcom/peel/d/k;)V

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 510
    iget-object v3, p0, Lcom/peel/d/k;->al:Landroid/widget/PopupWindow;

    invoke-virtual {v3, v2}, Landroid/widget/PopupWindow;->setContentView(Landroid/view/View;)V

    .line 512
    iget-object v2, p0, Lcom/peel/d/k;->al:Landroid/widget/PopupWindow;

    const/16 v3, 0x35

    invoke-virtual {v2, v0, v3, v6, v1}, Landroid/widget/PopupWindow;->showAtLocation(Landroid/view/View;III)V

    .line 513
    return-void
.end method

.method static synthetic b(Lcom/peel/d/k;)Lcom/peel/widget/ag;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/peel/d/k;->am:Lcom/peel/widget/ag;

    return-object v0
.end method

.method public static c()V
    .locals 3

    .prologue
    .line 312
    sget-object v0, Lcom/peel/d/k;->f:Landroid/support/v7/widget/SearchView;

    if-eqz v0, :cond_0

    .line 313
    sget-object v0, Lcom/peel/d/k;->f:Landroid/support/v7/widget/SearchView;

    const-string/jumbo v1, ""

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/support/v7/widget/SearchView;->setQuery(Ljava/lang/CharSequence;Z)V

    .line 315
    :cond_0
    return-void
.end method


# virtual methods
.method S()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v2, 0x0

    .line 515
    iget-object v0, p0, Lcom/peel/d/k;->am:Lcom/peel/widget/ag;

    if-eqz v0, :cond_2

    .line 516
    iget-object v0, p0, Lcom/peel/d/k;->am:Lcom/peel/widget/ag;

    invoke-virtual {v0}, Lcom/peel/widget/ag;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 571
    :cond_0
    :goto_0
    return-void

    .line 517
    :cond_1
    iget-object v0, p0, Lcom/peel/d/k;->am:Lcom/peel/widget/ag;

    invoke-virtual {v0}, Lcom/peel/widget/ag;->show()V

    goto :goto_0

    .line 520
    :cond_2
    invoke-virtual {p0}, Lcom/peel/d/k;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    const-string/jumbo v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/ae;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 522
    sget v1, Lcom/peel/ui/fq;->settings_adddevice_activities:I

    invoke-virtual {v0, v1, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ListView;

    .line 524
    sget-object v3, Lcom/peel/content/a;->f:Lcom/peel/content/user/User;

    invoke-virtual {v3}, Lcom/peel/content/user/User;->k()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 525
    sget-object v3, Lcom/peel/content/a;->f:Lcom/peel/content/user/User;

    invoke-virtual {v3}, Lcom/peel/content/user/User;->l()[Lcom/peel/data/ContentRoom;

    move-result-object v3

    .line 527
    sget-object v4, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v4}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v4

    .line 528
    if-eqz v4, :cond_0

    .line 530
    sget v5, Lcom/peel/ui/fq;->room_add_row:I

    invoke-virtual {v0, v5, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/ListView;->addFooterView(Landroid/view/View;)V

    .line 531
    new-instance v0, Lcom/peel/ui/gf;

    invoke-virtual {p0}, Lcom/peel/d/k;->m()Landroid/support/v4/app/ae;

    move-result-object v5

    sget v6, Lcom/peel/ui/fq;->settings_single_selection_row_wospace:I

    invoke-direct {v0, v5, v6, v3}, Lcom/peel/ui/gf;-><init>(Landroid/content/Context;I[Lcom/peel/data/ContentRoom;)V

    invoke-virtual {v1, v0}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    move v0, v2

    .line 534
    :goto_1
    array-length v5, v3

    if-ge v0, v5, :cond_3

    .line 535
    invoke-virtual {v4}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v5

    invoke-virtual {v5}, Lcom/peel/data/at;->b()Ljava/lang/String;

    move-result-object v5

    aget-object v6, v3, v0

    invoke-virtual {v6}, Lcom/peel/data/ContentRoom;->a()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 536
    const/4 v4, 0x1

    invoke-virtual {v1, v0, v4}, Landroid/widget/ListView;->setItemChecked(IZ)V

    .line 541
    :cond_3
    new-instance v0, Lcom/peel/widget/ag;

    invoke-virtual {p0}, Lcom/peel/d/k;->m()Landroid/support/v4/app/ae;

    move-result-object v4

    invoke-direct {v0, v4}, Lcom/peel/widget/ag;-><init>(Landroid/content/Context;)V

    sget v4, Lcom/peel/ui/ft;->label_select_room:I

    .line 542
    invoke-virtual {v0, v4}, Lcom/peel/widget/ag;->a(I)Lcom/peel/widget/ag;

    move-result-object v0

    .line 543
    invoke-virtual {v0, v1}, Lcom/peel/widget/ag;->a(Landroid/view/View;)Lcom/peel/widget/ag;

    move-result-object v0

    sget v4, Lcom/peel/ui/ft;->cancel:I

    new-instance v5, Lcom/peel/d/r;

    invoke-direct {v5, p0}, Lcom/peel/d/r;-><init>(Lcom/peel/d/k;)V

    .line 544
    invoke-virtual {v0, v4, v5}, Lcom/peel/widget/ag;->c(ILandroid/content/DialogInterface$OnClickListener;)Lcom/peel/widget/ag;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/d/k;->am:Lcom/peel/widget/ag;

    .line 552
    new-instance v0, Lcom/peel/d/s;

    invoke-direct {v0, p0, v3}, Lcom/peel/d/s;-><init>(Lcom/peel/d/k;[Lcom/peel/data/ContentRoom;)V

    invoke-virtual {v1, v0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 569
    iget-object v0, p0, Lcom/peel/d/k;->am:Lcom/peel/widget/ag;

    invoke-virtual {v0, v2}, Lcom/peel/widget/ag;->setCanceledOnTouchOutside(Z)V

    .line 570
    iget-object v0, p0, Lcom/peel/d/k;->am:Lcom/peel/widget/ag;

    invoke-virtual {v0}, Lcom/peel/widget/ag;->show()V

    goto/16 :goto_0

    .line 534
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method public T()V
    .locals 3

    .prologue
    .line 574
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 575
    const-string/jumbo v1, "add_room"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 576
    const-string/jumbo v1, "back_visibility"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 577
    invoke-virtual {p0}, Lcom/peel/d/k;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    const-class v2, Lcom/peel/i/fq;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lcom/peel/d/e;->c(Landroid/support/v4/app/ae;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 578
    return-void
.end method

.method public a(Landroid/view/Menu;)V
    .locals 6

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 253
    iget-object v0, p0, Lcom/peel/d/k;->b:Lcom/peel/d/i;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/peel/d/k;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    invoke-static {v0}, Lcom/peel/d/e;->a(Landroid/support/v4/app/ae;)Lcom/peel/d/u;

    move-result-object v0

    if-nez v0, :cond_1

    .line 309
    :cond_0
    :goto_0
    return-void

    .line 255
    :cond_1
    iget-object v0, p0, Lcom/peel/d/k;->b:Lcom/peel/d/i;

    invoke-virtual {v0}, Lcom/peel/d/i;->i()Lcom/peel/d/a;

    move-result-object v4

    .line 256
    if-eqz v4, :cond_0

    .line 259
    invoke-virtual {p0}, Lcom/peel/d/k;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    check-cast v0, Landroid/support/v7/app/ActionBarActivity;

    .line 260
    if-nez v0, :cond_2

    .line 261
    sget-object v0, Lcom/peel/d/k;->e:Ljava/lang/String;

    const-string/jumbo v1, "unable to set actionbar, activity is null"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 265
    :cond_2
    invoke-virtual {p0}, Lcom/peel/d/k;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    sget v5, Lcom/peel/ui/fp;->drawer_layout:I

    invoke-virtual {v1, v5}, Landroid/support/v4/app/ae;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/support/v4/widget/DrawerLayout;

    .line 267
    invoke-virtual {p0}, Lcom/peel/d/k;->m()Landroid/support/v4/app/ae;

    move-result-object v5

    invoke-static {v5}, Lcom/peel/d/e;->a(Landroid/support/v4/app/ae;)Lcom/peel/d/u;

    move-result-object v5

    invoke-virtual {v5}, Lcom/peel/d/u;->W()Z

    move-result v5

    if-eqz v5, :cond_3

    const v5, 0x800003

    invoke-virtual {v1, v5}, Landroid/support/v4/widget/DrawerLayout;->g(I)Z

    move-result v1

    if-nez v1, :cond_3

    .line 268
    invoke-virtual {p0}, Lcom/peel/d/k;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    invoke-static {v0}, Lcom/peel/d/e;->a(Landroid/support/v4/app/ae;)Lcom/peel/d/u;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/peel/d/u;->a(Landroid/view/Menu;)V

    goto :goto_0

    .line 272
    :cond_3
    invoke-virtual {v0}, Landroid/support/v7/app/ActionBarActivity;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/d/k;->h:Landroid/support/v7/app/ActionBar;

    .line 274
    iget-object v1, p0, Lcom/peel/d/k;->h:Landroid/support/v7/app/ActionBar;

    iget-object v0, v4, Lcom/peel/d/a;->c:Lcom/peel/d/b;

    sget-object v5, Lcom/peel/d/b;->a:Lcom/peel/d/b;

    if-ne v0, v5, :cond_5

    move v0, v2

    :goto_1
    invoke-virtual {v1, v0}, Landroid/support/v7/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 276
    sget-object v0, Lcom/peel/c/a;->b:Lcom/peel/c/e;

    invoke-static {v0}, Lcom/peel/c/a;->a(Lcom/peel/c/e;)Ljava/lang/Object;

    move-result-object v0

    sget-object v1, Lcom/peel/c/b;->b:Lcom/peel/c/b;

    if-ne v0, v1, :cond_6

    .line 277
    iget-object v0, p0, Lcom/peel/d/k;->h:Landroid/support/v7/app/ActionBar;

    invoke-virtual {v0, v3}, Landroid/support/v7/app/ActionBar;->setDisplayShowHomeEnabled(Z)V

    .line 282
    :goto_2
    iget-object v1, p0, Lcom/peel/d/k;->h:Landroid/support/v7/app/ActionBar;

    iget-object v0, v4, Lcom/peel/d/a;->c:Lcom/peel/d/b;

    sget-object v5, Lcom/peel/d/b;->a:Lcom/peel/d/b;

    if-ne v0, v5, :cond_8

    move v0, v2

    :goto_3
    invoke-virtual {v1, v0}, Landroid/support/v7/app/ActionBar;->setHomeButtonEnabled(Z)V

    .line 283
    iget-object v0, p0, Lcom/peel/d/k;->h:Landroid/support/v7/app/ActionBar;

    invoke-virtual {v0, v3}, Landroid/support/v7/app/ActionBar;->setDisplayShowCustomEnabled(Z)V

    .line 285
    iget-boolean v0, v4, Lcom/peel/d/a;->f:Z

    if-eqz v0, :cond_9

    .line 286
    iget-object v0, p0, Lcom/peel/d/k;->h:Landroid/support/v7/app/ActionBar;

    invoke-virtual {v0, v2}, Landroid/support/v7/app/ActionBar;->setNavigationMode(I)V

    .line 291
    :goto_4
    iget-object v0, v4, Lcom/peel/d/a;->e:Ljava/util/List;

    if-eqz v0, :cond_4

    iget-object v0, v4, Lcom/peel/d/a;->e:Ljava/util/List;

    sget v1, Lcom/peel/ui/fp;->menu_text:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 292
    invoke-virtual {p0}, Lcom/peel/d/k;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    invoke-static {v0}, Lcom/peel/d/e;->a(Landroid/support/v4/app/ae;)Lcom/peel/d/u;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/peel/d/u;->a(Landroid/view/Menu;)V

    .line 295
    :cond_4
    sget-object v0, Lcom/peel/d/d;->a:Lcom/peel/d/d;

    iget-object v1, v4, Lcom/peel/d/a;->a:Lcom/peel/d/d;

    if-ne v0, v1, :cond_a

    .line 296
    iget-object v0, p0, Lcom/peel/d/k;->h:Landroid/support/v7/app/ActionBar;

    invoke-virtual {v0, v3}, Landroid/support/v7/app/ActionBar;->setShowHideAnimationEnabled(Z)V

    .line 297
    iget-object v0, p0, Lcom/peel/d/k;->h:Landroid/support/v7/app/ActionBar;

    invoke-virtual {v0, v3}, Landroid/support/v7/app/ActionBar;->setDisplayShowTitleEnabled(Z)V

    .line 298
    iget-object v0, p0, Lcom/peel/d/k;->h:Landroid/support/v7/app/ActionBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v7/app/ActionBar;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 299
    iget-object v0, p0, Lcom/peel/d/k;->h:Landroid/support/v7/app/ActionBar;

    invoke-virtual {v0}, Landroid/support/v7/app/ActionBar;->hide()V

    goto/16 :goto_0

    :cond_5
    move v0, v3

    .line 274
    goto :goto_1

    .line 279
    :cond_6
    iget-object v1, p0, Lcom/peel/d/k;->h:Landroid/support/v7/app/ActionBar;

    iget-object v0, v4, Lcom/peel/d/a;->b:Lcom/peel/d/c;

    sget-object v5, Lcom/peel/d/c;->a:Lcom/peel/d/c;

    if-ne v0, v5, :cond_7

    move v0, v2

    :goto_5
    invoke-virtual {v1, v0}, Landroid/support/v7/app/ActionBar;->setDisplayShowHomeEnabled(Z)V

    goto :goto_2

    :cond_7
    move v0, v3

    goto :goto_5

    :cond_8
    move v0, v3

    .line 282
    goto :goto_3

    .line 288
    :cond_9
    iget-object v0, p0, Lcom/peel/d/k;->h:Landroid/support/v7/app/ActionBar;

    invoke-virtual {v0, v3}, Landroid/support/v7/app/ActionBar;->setNavigationMode(I)V

    goto :goto_4

    .line 301
    :cond_a
    iget-object v0, v4, Lcom/peel/d/a;->d:Ljava/lang/String;

    if-eqz v0, :cond_b

    .line 302
    iget-object v0, p0, Lcom/peel/d/k;->h:Landroid/support/v7/app/ActionBar;

    invoke-virtual {v0, v2}, Landroid/support/v7/app/ActionBar;->setDisplayShowTitleEnabled(Z)V

    .line 303
    iget-object v0, p0, Lcom/peel/d/k;->h:Landroid/support/v7/app/ActionBar;

    iget-object v1, v4, Lcom/peel/d/a;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/support/v7/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    .line 305
    :cond_b
    iget-object v0, p0, Lcom/peel/d/k;->h:Landroid/support/v7/app/ActionBar;

    invoke-virtual {p0}, Lcom/peel/d/k;->n()Landroid/content/res/Resources;

    move-result-object v1

    sget v3, Lcom/peel/ui/fo;->action_bar_background:I

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v7/app/ActionBar;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 306
    iget-object v0, p0, Lcom/peel/d/k;->h:Landroid/support/v7/app/ActionBar;

    invoke-virtual {v0, v2}, Landroid/support/v7/app/ActionBar;->setShowHideAnimationEnabled(Z)V

    .line 307
    iget-object v0, p0, Lcom/peel/d/k;->h:Landroid/support/v7/app/ActionBar;

    invoke-virtual {v0}, Landroid/support/v7/app/ActionBar;->show()V

    goto/16 :goto_0
.end method

.method public a(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 10

    .prologue
    const/4 v5, 0x1

    const/4 v7, 0x0

    .line 86
    invoke-super {p0, p1, p2}, Lcom/peel/d/u;->a(Landroid/view/Menu;Landroid/view/MenuInflater;)V

    .line 88
    invoke-virtual {p0}, Lcom/peel/d/k;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/peel/d/k;->b:Lcom/peel/d/i;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/peel/d/k;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    invoke-static {v0}, Lcom/peel/d/e;->a(Landroid/support/v4/app/ae;)Lcom/peel/d/u;

    move-result-object v0

    if-nez v0, :cond_1

    .line 243
    :cond_0
    :goto_0
    return-void

    .line 90
    :cond_1
    iget-object v0, p0, Lcom/peel/d/k;->b:Lcom/peel/d/i;

    invoke-virtual {v0}, Lcom/peel/d/i;->i()Lcom/peel/d/a;

    move-result-object v8

    .line 92
    if-eqz v8, :cond_0

    .line 94
    invoke-virtual {p0}, Lcom/peel/d/k;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    invoke-static {v0}, Lcom/peel/d/e;->a(Landroid/support/v4/app/ae;)Lcom/peel/d/u;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/d/u;->aa()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 95
    invoke-virtual {p0}, Lcom/peel/d/k;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    invoke-static {v0}, Lcom/peel/d/e;->a(Landroid/support/v4/app/ae;)Lcom/peel/d/u;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/peel/d/u;->a(Landroid/view/Menu;Landroid/view/MenuInflater;)V

    goto :goto_0

    .line 99
    :cond_2
    invoke-virtual {p0}, Lcom/peel/d/k;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/d/k;->ak:Landroid/content/SharedPreferences;

    .line 101
    invoke-interface {p1}, Landroid/view/Menu;->clear()V

    .line 103
    sget v0, Lcom/peel/ui/fr;->menu:I

    invoke-virtual {p2, v0, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    move v0, v7

    .line 106
    :goto_1
    invoke-interface {p1}, Landroid/view/Menu;->size()I

    move-result v1

    if-ge v0, v1, :cond_4

    .line 107
    invoke-interface {p1, v0}, Landroid/view/Menu;->getItem(I)Landroid/view/MenuItem;

    move-result-object v2

    .line 108
    iget-object v1, v8, Lcom/peel/d/a;->e:Ljava/util/List;

    if-eqz v1, :cond_3

    iget-object v1, v8, Lcom/peel/d/a;->e:Ljava/util/List;

    invoke-interface {v2}, Landroid/view/MenuItem;->getItemId()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    move v1, v5

    :goto_2
    invoke-interface {v2, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 106
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_3
    move v1, v7

    .line 108
    goto :goto_2

    .line 112
    :cond_4
    sget v0, Lcom/peel/ui/fp;->menu_search:I

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0}, Landroid/view/MenuItem;->getActionView()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/SearchView;

    sput-object v0, Lcom/peel/d/k;->f:Landroid/support/v7/widget/SearchView;

    .line 113
    invoke-virtual {p0}, Lcom/peel/d/k;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    const-string/jumbo v1, "search"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/ae;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/SearchManager;

    .line 114
    invoke-virtual {p0}, Lcom/peel/d/k;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/ae;->getComponentName()Landroid/content/ComponentName;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/SearchManager;->getSearchableInfo(Landroid/content/ComponentName;)Landroid/app/SearchableInfo;

    move-result-object v6

    .line 115
    invoke-virtual {p0}, Lcom/peel/d/k;->n()Landroid/content/res/Resources;

    move-result-object v0

    .line 116
    sget v1, Lcom/peel/ui/fn;->channel_search_width:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    invoke-static {v7, v1, v0}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    float-to-int v0, v0

    .line 117
    sget-object v1, Lcom/peel/d/k;->f:Landroid/support/v7/widget/SearchView;

    invoke-virtual {v1, v0}, Landroid/support/v7/widget/SearchView;->setMinimumWidth(I)V

    .line 118
    sget-object v0, Lcom/peel/d/k;->f:Landroid/support/v7/widget/SearchView;

    invoke-virtual {v0, v6}, Landroid/support/v7/widget/SearchView;->setSearchableInfo(Landroid/app/SearchableInfo;)V

    .line 120
    iget-object v0, p0, Lcom/peel/d/k;->b:Lcom/peel/d/i;

    if-eqz v0, :cond_5

    invoke-virtual {p0}, Lcom/peel/d/k;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    invoke-static {v0}, Lcom/peel/d/e;->a(Landroid/support/v4/app/ae;)Lcom/peel/d/u;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    const-class v1, Lcom/peel/ui/gm;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 121
    sget-object v0, Lcom/peel/d/k;->f:Landroid/support/v7/widget/SearchView;

    invoke-virtual {v0}, Landroid/support/v7/widget/SearchView;->onActionViewExpanded()V

    .line 122
    sget-object v0, Lcom/peel/d/k;->f:Landroid/support/v7/widget/SearchView;

    invoke-virtual {v0}, Landroid/support/v7/widget/SearchView;->clearFocus()V

    .line 127
    :goto_3
    sget-object v9, Lcom/peel/d/k;->f:Landroid/support/v7/widget/SearchView;

    new-instance v0, Lcom/peel/d/l;

    invoke-virtual {p0}, Lcom/peel/d/k;->m()Landroid/support/v4/app/ae;

    move-result-object v2

    sget v3, Lcom/peel/ui/fq;->search_row:I

    const/4 v4, 0x0

    move-object v1, p0

    invoke-direct/range {v0 .. v6}, Lcom/peel/d/l;-><init>(Lcom/peel/d/k;Landroid/content/Context;ILandroid/database/Cursor;ZLandroid/app/SearchableInfo;)V

    invoke-virtual {v9, v0}, Landroid/support/v7/widget/SearchView;->setSuggestionsAdapter(Landroid/support/v4/widget/e;)V

    .line 213
    sget v0, Lcom/peel/ui/fp;->menu_next:I

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    .line 214
    invoke-interface {v1}, Landroid/view/MenuItem;->getActionView()Landroid/view/View;

    move-result-object v0

    sget v2, Lcom/peel/ui/fp;->next_btn:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 215
    new-instance v2, Lcom/peel/d/n;

    invoke-direct {v2, p0, v8, v1}, Lcom/peel/d/n;-><init>(Lcom/peel/d/k;Lcom/peel/d/a;Landroid/view/MenuItem;)V

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 224
    iget-boolean v2, v8, Lcom/peel/d/a;->g:Z

    if-eqz v2, :cond_6

    .line 225
    invoke-interface {v1}, Landroid/view/MenuItem;->getActionView()Landroid/view/View;

    move-result-object v1

    sget v2, Lcom/peel/ui/fp;->next_view:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 226
    invoke-virtual {p0}, Lcom/peel/d/k;->n()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/peel/ui/fm;->abc_next_btn_bg_disabled:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setBackgroundColor(I)V

    .line 227
    invoke-virtual {p0}, Lcom/peel/d/k;->n()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/peel/ui/fm;->abc_next_btn_text_disabled:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 228
    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 233
    :goto_4
    sget v0, Lcom/peel/ui/fp;->menu_done:I

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    .line 234
    invoke-interface {v1}, Landroid/view/MenuItem;->getActionView()Landroid/view/View;

    move-result-object v0

    sget v2, Lcom/peel/ui/fp;->done_btn:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 235
    new-instance v2, Lcom/peel/d/o;

    invoke-direct {v2, p0, v1}, Lcom/peel/d/o;-><init>(Lcom/peel/d/k;Landroid/view/MenuItem;)V

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 242
    sget v0, Lcom/peel/ui/fp;->menu_remote:I

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    sput-object v0, Lcom/peel/d/k;->g:Landroid/view/MenuItem;

    goto/16 :goto_0

    .line 124
    :cond_5
    sget-object v0, Lcom/peel/d/k;->f:Landroid/support/v7/widget/SearchView;

    invoke-virtual {v0, v5}, Landroid/support/v7/widget/SearchView;->setIconifiedByDefault(Z)V

    goto :goto_3

    .line 230
    :cond_6
    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setEnabled(Z)V

    goto :goto_4
.end method

.method public a(Landroid/view/MenuItem;)Z
    .locals 6

    .prologue
    const/4 v4, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 318
    sget-object v0, Lcom/peel/content/a;->h:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/peel/d/k;->b:Lcom/peel/d/i;

    if-nez v0, :cond_2

    :cond_0
    move v1, v2

    .line 424
    :cond_1
    :goto_0
    return v1

    .line 319
    :cond_2
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v3, "dismiss_toast"

    invoke-direct {v0, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 320
    invoke-virtual {p0}, Lcom/peel/d/k;->m()Landroid/support/v4/app/ae;

    move-result-object v3

    invoke-virtual {v3}, Landroid/support/v4/app/ae;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Landroid/support/v4/a/q;->a(Landroid/content/Context;)Landroid/support/v4/a/q;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/support/v4/a/q;->a(Landroid/content/Intent;)Z

    .line 322
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    .line 323
    sget v3, Lcom/peel/ui/fp;->menu_remote:I

    if-ne v0, v3, :cond_a

    .line 324
    iget-object v0, p0, Lcom/peel/d/k;->c:Landroid/os/Bundle;

    const-string/jumbo v3, "context_id"

    invoke-virtual {v0, v3, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v3

    .line 325
    invoke-static {}, Lcom/peel/util/a/f;->a()Lcom/peel/util/a/f;

    move-result-object v4

    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v0}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v0

    if-nez v0, :cond_6

    move v0, v1

    :goto_1
    const/16 v5, 0x443

    invoke-virtual {v4, v0, v5, v3}, Lcom/peel/util/a/f;->a(III)V

    .line 330
    invoke-virtual {p0}, Lcom/peel/d/k;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string/jumbo v3, "setup_type"

    invoke-interface {v0, v3, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    if-ne v1, v0, :cond_7

    move v0, v1

    .line 331
    :goto_2
    invoke-virtual {p0}, Lcom/peel/d/k;->m()Landroid/support/v4/app/ae;

    move-result-object v3

    invoke-static {v3}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v3

    const-string/jumbo v4, "is_device_setup_complete"

    invoke-interface {v3, v4, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    .line 332
    sget-object v4, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v4}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v4

    invoke-virtual {v4}, Lcom/peel/control/RoomControl;->g()[Ljava/lang/String;

    move-result-object v4

    .line 333
    if-eqz v0, :cond_3

    if-nez v3, :cond_4

    :cond_3
    if-eqz v4, :cond_9

    invoke-static {v4}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v3

    const-string/jumbo v4, "live"

    invoke-interface {v3, v4}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_9

    .line 334
    :cond_4
    invoke-virtual {p0}, Lcom/peel/d/k;->m()Landroid/support/v4/app/ae;

    move-result-object v2

    invoke-static {v2}, Lcom/peel/d/e;->a(Landroid/support/v4/app/ae;)Lcom/peel/d/u;

    move-result-object v2

    invoke-virtual {v2}, Lcom/peel/d/u;->ab()Z

    move-result v2

    if-nez v2, :cond_5

    .line 335
    sget-object v2, Lcom/peel/d/k;->e:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/peel/d/k;->m()Landroid/support/v4/app/ae;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/peel/d/e;->a(Ljava/lang/String;Landroid/support/v4/app/ae;)V

    .line 337
    :cond_5
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 338
    const-string/jumbo v3, "control_only_mode"

    invoke-virtual {v2, v3, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 340
    invoke-virtual {p0}, Lcom/peel/d/k;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    invoke-static {v0}, Lcom/peel/d/e;->a(Landroid/support/v4/app/ae;)Lcom/peel/d/u;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    .line 342
    const-class v3, Lcom/peel/ui/b/ag;

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 343
    invoke-virtual {p0}, Lcom/peel/d/k;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    const-class v3, Lcom/peel/ui/gt;

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3, v2}, Lcom/peel/d/e;->c(Landroid/support/v4/app/ae;Ljava/lang/String;Landroid/os/Bundle;)V

    goto/16 :goto_0

    .line 325
    :cond_6
    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v0}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/data/at;->f()I

    move-result v0

    goto/16 :goto_1

    :cond_7
    move v0, v2

    .line 330
    goto/16 :goto_2

    .line 345
    :cond_8
    invoke-virtual {p0}, Lcom/peel/d/k;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    const-class v3, Lcom/peel/ui/gt;

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3, v2}, Lcom/peel/d/e;->b(Landroid/support/v4/app/ae;Ljava/lang/String;Landroid/os/Bundle;)V

    goto/16 :goto_0

    .line 350
    :cond_9
    invoke-static {v2}, Lcom/peel/d/k;->a(Z)V

    .line 351
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 352
    const-string/jumbo v3, "control_only_mode"

    invoke-virtual {v2, v3, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 353
    const-string/jumbo v0, "passback_clazz"

    invoke-virtual {p0}, Lcom/peel/d/k;->m()Landroid/support/v4/app/ae;

    move-result-object v3

    invoke-static {v3}, Lcom/peel/d/e;->a(Landroid/support/v4/app/ae;)Lcom/peel/d/u;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 354
    const-string/jumbo v0, "passback_bundle"

    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    invoke-virtual {v2, v0, v3}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 355
    invoke-virtual {p0}, Lcom/peel/d/k;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    const-class v3, Lcom/peel/i/gm;

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3, v2}, Lcom/peel/d/e;->c(Landroid/support/v4/app/ae;Ljava/lang/String;Landroid/os/Bundle;)V

    goto/16 :goto_0

    .line 359
    :cond_a
    sget v3, Lcom/peel/ui/fp;->menu_settings:I

    if-ne v0, v3, :cond_b

    .line 360
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 361
    const-string/jumbo v2, "addToBackStack"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 362
    invoke-virtual {p0}, Lcom/peel/d/k;->m()Landroid/support/v4/app/ae;

    move-result-object v2

    const-class v3, Lcom/peel/h/a/gr;

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v0}, Lcom/peel/d/e;->b(Landroid/support/v4/app/ae;Ljava/lang/String;Landroid/os/Bundle;)V

    goto/16 :goto_0

    .line 374
    :cond_b
    sget v3, Lcom/peel/ui/fp;->menu_time:I

    if-ne v0, v3, :cond_d

    .line 375
    invoke-virtual {p0}, Lcom/peel/d/k;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string/jumbo v3, "yosemite_enabled"

    invoke-interface {v0, v3, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 376
    iput-boolean v1, p0, Lcom/peel/d/k;->aj:Z

    .line 377
    iput-boolean v1, p0, Lcom/peel/d/k;->i:Z

    .line 381
    :goto_3
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 382
    const-string/jumbo v2, "yosemite_country"

    iget-boolean v3, p0, Lcom/peel/d/k;->aj:Z

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 383
    const-string/jumbo v2, "yosemite_installed"

    iget-boolean v3, p0, Lcom/peel/d/k;->i:Z

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 384
    invoke-virtual {p0}, Lcom/peel/d/k;->m()Landroid/support/v4/app/ae;

    move-result-object v2

    const-class v3, Lcom/peel/ui/kj;

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v0}, Lcom/peel/d/e;->b(Landroid/support/v4/app/ae;Ljava/lang/String;Landroid/os/Bundle;)V

    goto/16 :goto_0

    .line 379
    :cond_c
    iget-object v0, p0, Lcom/peel/d/k;->ak:Landroid/content/SharedPreferences;

    const-string/jumbo v3, "yosemite_available"

    invoke-interface {v0, v3, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/peel/d/k;->aj:Z

    goto :goto_3

    .line 386
    :cond_d
    sget v2, Lcom/peel/ui/fp;->overflow_menu_btn:I

    if-ne v0, v2, :cond_e

    .line 387
    invoke-direct {p0}, Lcom/peel/d/k;->ac()V

    goto/16 :goto_0

    .line 389
    :cond_e
    sget v2, Lcom/peel/ui/fp;->menu_change_room:I

    if-ne v0, v2, :cond_f

    .line 390
    invoke-virtual {p0}, Lcom/peel/d/k;->S()V

    goto/16 :goto_0

    .line 392
    :cond_f
    sget v2, Lcom/peel/ui/fp;->menu_channels:I

    if-ne v0, v2, :cond_10

    .line 393
    invoke-static {}, Lcom/peel/content/a;->a()Lcom/peel/data/ContentRoom;

    move-result-object v2

    .line 394
    const-string/jumbo v0, "live"

    invoke-static {v0}, Lcom/peel/content/a;->c(Ljava/lang/String;)Lcom/peel/content/library/Library;

    move-result-object v0

    check-cast v0, Lcom/peel/content/library/LiveLibrary;

    .line 395
    if-eqz v2, :cond_1

    if-eqz v0, :cond_1

    .line 398
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 399
    const-string/jumbo v4, "room"

    invoke-virtual {v3, v4, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 400
    const-string/jumbo v2, "library"

    invoke-virtual {v3, v2, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 401
    invoke-virtual {p0}, Lcom/peel/d/k;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    const-class v2, Lcom/peel/h/a/bb;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2, v3}, Lcom/peel/d/e;->b(Landroid/support/v4/app/ae;Ljava/lang/String;Landroid/os/Bundle;)V

    goto/16 :goto_0

    .line 409
    :cond_10
    sget v2, Lcom/peel/ui/fp;->menu_about:I

    if-ne v0, v2, :cond_11

    .line 410
    invoke-virtual {p0}, Lcom/peel/d/k;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    const-string/jumbo v2, "layout_inflater"

    invoke-virtual {v0, v2}, Landroid/support/v4/app/ae;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 411
    sget v2, Lcom/peel/ui/fq;->about:I

    invoke-virtual {v0, v2, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 412
    sget v0, Lcom/peel/ui/fp;->about_app_version:I

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/peel/d/k;->m()Landroid/support/v4/app/ae;

    move-result-object v3

    invoke-static {v3}, Lcom/peel/util/bx;->g(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 413
    new-instance v0, Lcom/peel/widget/ag;

    invoke-virtual {p0}, Lcom/peel/d/k;->m()Landroid/support/v4/app/ae;

    move-result-object v3

    invoke-direct {v0, v3}, Lcom/peel/widget/ag;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0}, Lcom/peel/widget/ag;->b()Lcom/peel/widget/ag;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/peel/widget/ag;->a(Landroid/view/View;)Lcom/peel/widget/ag;

    move-result-object v0

    sget v2, Lcom/peel/ui/ft;->ok:I

    invoke-virtual {v0, v2, v4}, Lcom/peel/widget/ag;->a(ILandroid/content/DialogInterface$OnClickListener;)Lcom/peel/widget/ag;

    move-result-object v0

    .line 414
    invoke-virtual {v0}, Lcom/peel/widget/ag;->show()V

    goto/16 :goto_0

    .line 417
    :cond_11
    invoke-virtual {p0}, Lcom/peel/d/k;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    invoke-static {v0}, Lcom/peel/d/e;->a(Landroid/support/v4/app/ae;)Lcom/peel/d/u;

    move-result-object v0

    .line 419
    if-eqz v0, :cond_12

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    const-class v2, Lcom/peel/h/a/gr;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_12

    .line 420
    sget-object v0, Lcom/peel/d/k;->e:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/peel/d/k;->m()Landroid/support/v4/app/ae;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/peel/d/e;->a(Ljava/lang/String;Landroid/support/v4/app/ae;)V

    goto/16 :goto_0

    .line 422
    :cond_12
    invoke-virtual {p0}, Lcom/peel/d/k;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    invoke-static {v0}, Lcom/peel/d/e;->a(Landroid/support/v4/app/ae;)Lcom/peel/d/u;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/peel/d/u;->a(Landroid/view/MenuItem;)Z

    goto/16 :goto_0
.end method

.method public a_(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 80
    invoke-super {p0, p1}, Lcom/peel/d/u;->a_(Landroid/os/Bundle;)V

    .line 81
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/peel/d/k;->d(Z)V

    .line 82
    return-void
.end method
