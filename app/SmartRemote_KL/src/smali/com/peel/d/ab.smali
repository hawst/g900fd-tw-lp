.class Lcom/peel/d/ab;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic b:I

.field final synthetic c:Ljava/lang/String;

.field final synthetic d:Lcom/peel/util/t;

.field final synthetic e:Lcom/peel/d/v;


# direct methods
.method constructor <init>(Lcom/peel/d/v;Ljava/lang/String;ILjava/lang/String;Lcom/peel/util/t;)V
    .locals 0

    .prologue
    .line 311
    iput-object p1, p0, Lcom/peel/d/ab;->e:Lcom/peel/d/v;

    iput-object p2, p0, Lcom/peel/d/ab;->a:Ljava/lang/String;

    iput p3, p0, Lcom/peel/d/ab;->b:I

    iput-object p4, p0, Lcom/peel/d/ab;->c:Ljava/lang/String;

    iput-object p5, p0, Lcom/peel/d/ab;->d:Lcom/peel/util/t;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    .line 314
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 317
    :try_start_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "https://api.twitter.com/1.1/search/tweets.json?q="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/peel/d/ab;->a:Ljava/lang/String;

    const-string/jumbo v3, "UTF-8"

    invoke-static {v2, v3}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "&result_type=recent&count="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/peel/d/ab;->b:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 319
    iget-object v1, p0, Lcom/peel/d/ab;->c:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 320
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "&since_id="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/peel/d/ab;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 323
    :cond_0
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/d/ab;->e:Lcom/peel/d/v;

    invoke-static {v1}, Lcom/peel/d/v;->c(Lcom/peel/d/v;)Lcom/peel/d/af;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/d/af;->c()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/peel/d/ab;->e:Lcom/peel/d/v;

    invoke-static {v2}, Lcom/peel/d/v;->c(Lcom/peel/d/v;)Lcom/peel/d/af;

    move-result-object v2

    invoke-virtual {v2}, Lcom/peel/d/af;->d()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/peel/d/ab;->e:Lcom/peel/d/v;

    invoke-static {v3}, Lcom/peel/d/v;->c(Lcom/peel/d/v;)Lcom/peel/d/af;

    move-result-object v3

    invoke-virtual {v3}, Lcom/peel/d/af;->a()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/peel/d/ab;->e:Lcom/peel/d/v;

    invoke-static {v4}, Lcom/peel/d/v;->c(Lcom/peel/d/v;)Lcom/peel/d/af;

    move-result-object v4

    invoke-virtual {v4}, Lcom/peel/d/af;->b()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Lcom/peel/d/ac;

    invoke-direct {v5, p0}, Lcom/peel/d/ac;-><init>(Lcom/peel/d/ab;)V

    invoke-static/range {v0 .. v5}, Lcom/peel/util/b/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/peel/util/t;)V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    .line 337
    :goto_0
    return-void

    .line 334
    :catch_0
    move-exception v0

    .line 335
    invoke-virtual {v0}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    goto :goto_0
.end method
