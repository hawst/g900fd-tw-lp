.class public final enum Lcom/peel/d/c;
.super Ljava/lang/Enum;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/peel/d/c;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/peel/d/c;

.field public static final enum b:Lcom/peel/d/c;

.field private static final synthetic c:[Lcom/peel/d/c;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 13
    new-instance v0, Lcom/peel/d/c;

    const-string/jumbo v1, "LogoShown"

    invoke-direct {v0, v1, v2}, Lcom/peel/d/c;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/peel/d/c;->a:Lcom/peel/d/c;

    new-instance v0, Lcom/peel/d/c;

    const-string/jumbo v1, "LogoHidden"

    invoke-direct {v0, v1, v3}, Lcom/peel/d/c;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/peel/d/c;->b:Lcom/peel/d/c;

    .line 12
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/peel/d/c;

    sget-object v1, Lcom/peel/d/c;->a:Lcom/peel/d/c;

    aput-object v1, v0, v2

    sget-object v1, Lcom/peel/d/c;->b:Lcom/peel/d/c;

    aput-object v1, v0, v3

    sput-object v0, Lcom/peel/d/c;->c:[Lcom/peel/d/c;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 12
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/peel/d/c;
    .locals 1

    .prologue
    .line 12
    const-class v0, Lcom/peel/d/c;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/peel/d/c;

    return-object v0
.end method

.method public static values()[Lcom/peel/d/c;
    .locals 1

    .prologue
    .line 12
    sget-object v0, Lcom/peel/d/c;->c:[Lcom/peel/d/c;

    invoke-virtual {v0}, [Lcom/peel/d/c;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/peel/d/c;

    return-object v0
.end method
