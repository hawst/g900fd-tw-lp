.class public final Lcom/peel/d/e;
.super Ljava/lang/Object;


# direct methods
.method public static a(Landroid/support/v4/app/ae;)Lcom/peel/d/u;
    .locals 2

    .prologue
    .line 175
    invoke-virtual {p0}, Landroid/support/v4/app/ae;->getSupportFragmentManager()Landroid/support/v4/app/aj;

    move-result-object v0

    sget v1, Lcom/peel/ui/fp;->content:I

    invoke-virtual {v0, v1}, Landroid/support/v4/app/aj;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/peel/d/u;

    return-object v0
.end method

.method public static a(Landroid/support/v4/app/ae;Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 166
    invoke-virtual {p0}, Landroid/support/v4/app/ae;->getSupportFragmentManager()Landroid/support/v4/app/aj;

    move-result-object v0

    const-string/jumbo v1, "menu"

    .line 167
    invoke-virtual {v0, v1}, Landroid/support/v4/app/aj;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/peel/d/u;

    .line 169
    if-eqz v0, :cond_0

    .line 170
    invoke-virtual {v0, p1}, Lcom/peel/d/u;->c(Landroid/os/Bundle;)V

    .line 172
    :cond_0
    return-void
.end method

.method public static a(Landroid/support/v4/app/ae;Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 27
    const/4 v0, 0x0

    invoke-static {p0, p1, p2, v0}, Lcom/peel/d/e;->a(Landroid/support/v4/app/ae;Ljava/lang/String;Landroid/os/Bundle;Z)V

    .line 28
    return-void
.end method

.method public static a(Landroid/support/v4/app/ae;Ljava/lang/String;Landroid/os/Bundle;Z)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    .line 31
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_1

    .line 71
    :cond_0
    :goto_0
    return-void

    .line 33
    :cond_1
    invoke-virtual {p0}, Landroid/support/v4/app/ae;->getSupportFragmentManager()Landroid/support/v4/app/aj;

    move-result-object v3

    .line 34
    invoke-virtual {v3}, Landroid/support/v4/app/aj;->a()Landroid/support/v4/app/ax;

    move-result-object v4

    .line 37
    :try_start_0
    const-string/jumbo v0, "dialog"

    invoke-virtual {v3, v0}, Landroid/support/v4/app/aj;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/aa;

    .line 38
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Landroid/support/v4/app/aa;->j_()Landroid/app/Dialog;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 39
    invoke-virtual {v0}, Landroid/support/v4/app/aa;->i_()V

    .line 42
    :cond_2
    const/4 v0, 0x0

    const/4 v2, 0x1

    invoke-virtual {v3, v0, v2}, Landroid/support/v4/app/aj;->b(Ljava/lang/String;I)Z

    .line 43
    sget-object v0, Lcom/peel/c/a;->d:Lcom/peel/c/e;

    invoke-static {v0}, Lcom/peel/c/a;->a(Lcom/peel/c/e;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/ui/ni;

    .line 44
    if-eqz p3, :cond_5

    .line 45
    const/4 v2, 0x0

    .line 46
    invoke-static {}, Lcom/peel/d/i;->a()Lcom/peel/d/i;

    move-result-object v5

    invoke-virtual {v5}, Lcom/peel/d/i;->f()Ljava/lang/String;

    move-result-object v5

    .line 47
    invoke-virtual {v3, v5}, Landroid/support/v4/app/aj;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v5

    .line 48
    if-eqz v5, :cond_3

    invoke-virtual {v4, v5}, Landroid/support/v4/app/ax;->b(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/ax;

    .line 49
    :cond_3
    invoke-virtual {v3, p1}, Landroid/support/v4/app/aj;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v3

    .line 50
    if-eqz v3, :cond_6

    .line 51
    invoke-virtual {v4, v3}, Landroid/support/v4/app/ax;->c(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/ax;

    .line 54
    :goto_1
    if-nez v1, :cond_4

    .line 55
    invoke-virtual {v0, p0, p1, p2}, Lcom/peel/ui/ni;->a(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 56
    sget v1, Lcom/peel/ui/fp;->content:I

    invoke-virtual {v4, v1, v0, p1}, Landroid/support/v4/app/ax;->a(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/ax;

    .line 62
    :cond_4
    :goto_2
    invoke-static {}, Lcom/peel/d/i;->a()Lcom/peel/d/i;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/peel/d/i;->f(Ljava/lang/String;)V

    .line 64
    invoke-virtual {v4, p1}, Landroid/support/v4/app/ax;->a(Ljava/lang/String;)Landroid/support/v4/app/ax;

    .line 65
    invoke-virtual {v4}, Landroid/support/v4/app/ax;->b()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 67
    :catch_0
    move-exception v0

    .line 69
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 59
    :cond_5
    :try_start_1
    invoke-virtual {v0, p0, p1, p2}, Lcom/peel/ui/ni;->a(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 60
    sget v1, Lcom/peel/ui/fp;->content:I

    invoke-virtual {v4, v1, v0, p1}, Landroid/support/v4/app/ax;->a(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/ax;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2

    :cond_6
    move v1, v2

    goto :goto_1
.end method

.method public static a(Ljava/lang/String;Landroid/support/v4/app/ae;)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 179
    invoke-virtual {p1}, Landroid/support/v4/app/ae;->getSupportFragmentManager()Landroid/support/v4/app/aj;

    move-result-object v0

    .line 181
    invoke-static {p1}, Lcom/peel/d/e;->a(Landroid/support/v4/app/ae;)Lcom/peel/d/u;

    move-result-object v3

    .line 182
    if-eqz v3, :cond_0

    invoke-interface {v3}, Lcom/peel/d/h;->b()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 231
    :cond_0
    :goto_0
    return-void

    .line 184
    :cond_1
    invoke-interface {v3}, Lcom/peel/d/h;->a()Ljava/lang/String;

    move-result-object v4

    .line 185
    invoke-virtual {v0}, Landroid/support/v4/app/aj;->c()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    .line 186
    if-lez v5, :cond_3

    .line 187
    if-nez v4, :cond_2

    .line 188
    const/4 v1, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/aj;->a(Ljava/lang/String;I)V

    .line 201
    :goto_1
    :try_start_0
    const-string/jumbo v0, "input_method"

    invoke-virtual {p1, v0}, Landroid/support/v4/app/ae;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 202
    invoke-virtual {p1}, Landroid/support/v4/app/ae;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 203
    :catch_0
    move-exception v0

    goto :goto_0

    .line 190
    :cond_2
    const-string/jumbo v1, "updating fragment"

    new-instance v2, Lcom/peel/d/f;

    invoke-direct {v2, v0, v4}, Lcom/peel/d/f;-><init>(Landroid/support/v4/app/aj;Ljava/lang/String;)V

    invoke-static {p0, v1, v2}, Lcom/peel/util/i;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/lang/Runnable;

    goto :goto_1

    .line 205
    :cond_3
    invoke-static {p1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v4

    .line 206
    const-string/jumbo v0, "setup_type"

    invoke-interface {v4, v0, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    if-ne v1, v0, :cond_4

    move v0, v1

    .line 207
    :goto_2
    const-string/jumbo v5, "is_setup_complete"

    invoke-interface {v4, v5, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    .line 211
    if-nez v0, :cond_5

    if-eqz v4, :cond_5

    .line 212
    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    const-class v4, Lcom/peel/ui/lq;

    invoke-virtual {v4}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 213
    const-class v0, Lcom/peel/ui/lq;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    invoke-static {p1, v0, v1}, Lcom/peel/d/e;->a(Landroid/support/v4/app/ae;Ljava/lang/String;Landroid/os/Bundle;)V

    goto :goto_0

    :cond_4
    move v0, v2

    .line 206
    goto :goto_2

    .line 216
    :cond_5
    invoke-static {}, Lcom/peel/d/i;->a()Lcom/peel/d/i;

    move-result-object v0

    .line 217
    iget-boolean v4, v0, Lcom/peel/d/i;->a:Z

    if-nez v4, :cond_6

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    const-class v4, Lcom/peel/ui/gt;

    invoke-virtual {v4}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 218
    :cond_6
    invoke-virtual {p1}, Landroid/support/v4/app/ae;->finish()V

    goto/16 :goto_0

    .line 220
    :cond_7
    iput-boolean v1, v0, Lcom/peel/d/i;->a:Z

    .line 221
    sget v1, Lcom/peel/ui/ft;->exit_app:I

    invoke-static {p1, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 222
    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    new-instance v2, Lcom/peel/d/g;

    invoke-direct {v2, v0}, Lcom/peel/d/g;-><init>(Lcom/peel/d/i;)V

    const-wide/16 v4, 0x7d0

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/16 :goto_0
.end method

.method public static b(Landroid/support/v4/app/ae;)I
    .locals 1

    .prologue
    .line 234
    invoke-virtual {p0}, Landroid/support/v4/app/ae;->getSupportFragmentManager()Landroid/support/v4/app/aj;

    move-result-object v0

    .line 235
    invoke-virtual {v0}, Landroid/support/v4/app/aj;->c()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    return v0
.end method

.method public static b(Landroid/support/v4/app/ae;Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v5, -0x1

    .line 74
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_1

    .line 108
    :cond_0
    :goto_0
    return-void

    .line 76
    :cond_1
    invoke-virtual {p0}, Landroid/support/v4/app/ae;->getSupportFragmentManager()Landroid/support/v4/app/aj;

    move-result-object v3

    .line 77
    invoke-virtual {v3}, Landroid/support/v4/app/aj;->a()Landroid/support/v4/app/ax;

    move-result-object v4

    .line 79
    const-string/jumbo v0, "dialog"

    invoke-virtual {v3, v0}, Landroid/support/v4/app/aj;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/aa;

    .line 80
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Landroid/support/v4/app/aa;->j_()Landroid/app/Dialog;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 81
    invoke-virtual {v0}, Landroid/support/v4/app/aa;->i_()V

    .line 84
    :cond_2
    const/4 v2, 0x0

    .line 85
    sget-object v0, Lcom/peel/c/a;->d:Lcom/peel/c/e;

    invoke-static {v0}, Lcom/peel/c/a;->a(Lcom/peel/c/e;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/ui/ni;

    .line 86
    invoke-virtual {v0, p0, p1, p2}, Lcom/peel/ui/ni;->a(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 87
    invoke-virtual {v3, p1, v1}, Landroid/support/v4/app/aj;->a(Ljava/lang/String;I)V

    .line 89
    instance-of v3, v0, Lcom/peel/d/t;

    if-eqz v3, :cond_4

    .line 91
    invoke-static {p0}, Lcom/peel/d/e;->a(Landroid/support/v4/app/ae;)Lcom/peel/d/u;

    move-result-object v2

    const/16 v3, 0x3e8

    invoke-virtual {v0, v2, v3}, Landroid/support/v4/app/Fragment;->a(Landroid/support/v4/app/Fragment;I)V

    .line 92
    const-string/jumbo v2, "dialog"

    invoke-virtual {v4, v0, v2}, Landroid/support/v4/app/ax;->a(Landroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/ax;

    move v0, v1

    .line 106
    :goto_1
    if-nez v0, :cond_3

    invoke-virtual {v4, p1}, Landroid/support/v4/app/ax;->a(Ljava/lang/String;)Landroid/support/v4/app/ax;

    .line 107
    :cond_3
    invoke-virtual {v4}, Landroid/support/v4/app/ax;->b()I

    goto :goto_0

    .line 95
    :cond_4
    invoke-static {p0}, Lcom/peel/d/e;->a(Landroid/support/v4/app/ae;)Lcom/peel/d/u;

    move-result-object v1

    .line 96
    if-eqz v1, :cond_5

    invoke-virtual {v4, v1}, Landroid/support/v4/app/ax;->b(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/ax;

    .line 97
    :cond_5
    if-eqz p2, :cond_6

    .line 98
    const-string/jumbo v1, "target_code"

    invoke-virtual {p2, v1, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    .line 99
    if-le v1, v5, :cond_6

    .line 100
    invoke-static {p0}, Lcom/peel/d/e;->a(Landroid/support/v4/app/ae;)Lcom/peel/d/u;

    move-result-object v3

    invoke-virtual {v0, v3, v1}, Landroid/support/v4/app/Fragment;->a(Landroid/support/v4/app/Fragment;I)V

    .line 104
    :cond_6
    sget v1, Lcom/peel/ui/fp;->content:I

    invoke-virtual {v4, v1, v0, p1}, Landroid/support/v4/app/ax;->b(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/ax;

    move v0, v2

    goto :goto_1
.end method

.method public static c(Landroid/support/v4/app/ae;Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 6

    .prologue
    const/4 v5, -0x1

    .line 113
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_1

    .line 153
    :cond_0
    :goto_0
    return-void

    .line 115
    :cond_1
    invoke-virtual {p0}, Landroid/support/v4/app/ae;->getSupportFragmentManager()Landroid/support/v4/app/aj;

    move-result-object v0

    .line 116
    invoke-virtual {v0}, Landroid/support/v4/app/aj;->a()Landroid/support/v4/app/ax;

    move-result-object v2

    .line 118
    const-string/jumbo v1, "dialog"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/aj;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/aa;

    .line 119
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Landroid/support/v4/app/aa;->j_()Landroid/app/Dialog;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 120
    invoke-virtual {v0}, Landroid/support/v4/app/aa;->i_()V

    .line 123
    :cond_2
    const/4 v1, 0x0

    .line 124
    sget-object v0, Lcom/peel/c/a;->d:Lcom/peel/c/e;

    invoke-static {v0}, Lcom/peel/c/a;->a(Lcom/peel/c/e;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/ui/ni;

    .line 125
    invoke-virtual {v0, p0, p1, p2}, Lcom/peel/ui/ni;->a(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;)Landroid/support/v4/app/Fragment;

    move-result-object v3

    .line 126
    invoke-virtual {p0}, Landroid/support/v4/app/ae;->getSupportFragmentManager()Landroid/support/v4/app/aj;

    move-result-object v0

    sget v4, Lcom/peel/ui/fp;->content:I

    invoke-virtual {v0, v4}, Landroid/support/v4/app/aj;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/peel/d/u;

    .line 127
    instance-of v4, v3, Lcom/peel/d/t;

    if-eqz v4, :cond_4

    .line 129
    const/16 v1, 0x3e8

    invoke-virtual {v3, v0, v1}, Landroid/support/v4/app/Fragment;->a(Landroid/support/v4/app/Fragment;I)V

    .line 130
    const-string/jumbo v0, "dialog"

    invoke-virtual {v2, v3, v0}, Landroid/support/v4/app/ax;->a(Landroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/ax;

    .line 131
    const/4 v0, 0x1

    .line 151
    :goto_1
    if-nez v0, :cond_3

    invoke-virtual {v2, p1}, Landroid/support/v4/app/ax;->a(Ljava/lang/String;)Landroid/support/v4/app/ax;

    .line 152
    :cond_3
    invoke-virtual {v2}, Landroid/support/v4/app/ax;->b()I

    goto :goto_0

    .line 134
    :cond_4
    if-eqz v0, :cond_5

    .line 135
    invoke-virtual {v2, v0}, Landroid/support/v4/app/ax;->b(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/ax;

    .line 138
    :cond_5
    if-eqz p2, :cond_6

    .line 139
    const-string/jumbo v4, "target_code"

    invoke-virtual {p2, v4, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v4

    .line 141
    if-le v4, v5, :cond_6

    .line 142
    invoke-virtual {v3, v0, v4}, Landroid/support/v4/app/Fragment;->a(Landroid/support/v4/app/Fragment;I)V

    .line 148
    :cond_6
    sget v0, Lcom/peel/ui/fp;->content:I

    invoke-virtual {v2, v0, v3, p1}, Landroid/support/v4/app/ax;->a(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/ax;

    move v0, v1

    goto :goto_1
.end method

.method public static d(Landroid/support/v4/app/ae;Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 156
    invoke-virtual {p0}, Landroid/support/v4/app/ae;->getSupportFragmentManager()Landroid/support/v4/app/aj;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/support/v4/app/aj;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/peel/d/u;

    .line 157
    if-nez v0, :cond_1

    .line 163
    :cond_0
    :goto_0
    return-void

    .line 158
    :cond_1
    invoke-virtual {v0}, Lcom/peel/d/u;->s()Z

    move-result v1

    if-nez v1, :cond_2

    const-class v1, Lcom/peel/ui/cj;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 159
    :cond_2
    invoke-virtual {v0}, Lcom/peel/d/u;->Y()Landroid/os/Bundle;

    move-result-object v1

    .line 160
    if-nez v1, :cond_3

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 161
    :cond_3
    invoke-virtual {v1, p2}, Landroid/os/Bundle;->putAll(Landroid/os/Bundle;)V

    .line 162
    invoke-virtual {v0, v1}, Lcom/peel/d/u;->c(Landroid/os/Bundle;)V

    goto :goto_0
.end method
