.class public Lcom/peel/d/v;
.super Ljava/lang/Object;


# static fields
.field private static final a:Ljava/lang/String;

.field private static b:Lcom/peel/d/v;

.field private static c:Ljava/lang/Object;

.field private static final j:[Ljava/lang/String;


# instance fields
.field private d:Lcom/peel/d/af;

.field private e:Landroid/content/Context;

.field private f:Landroid/content/SharedPreferences;

.field private g:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private h:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private i:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 40
    const-class v0, Lcom/peel/d/v;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/peel/d/v;->a:Ljava/lang/String;

    .line 43
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/peel/d/v;->c:Ljava/lang/Object;

    .line 357
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string/jumbo v2, "viggle"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string/jumbo v2, "tvtag"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string/jumbo v2, "getglue"

    aput-object v2, v0, v1

    sput-object v0, Lcom/peel/d/v;->j:[Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 62
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    iput-object v4, p0, Lcom/peel/d/v;->d:Lcom/peel/d/af;

    .line 59
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/peel/d/v;->h:Ljava/util/HashMap;

    .line 60
    iput-object v4, p0, Lcom/peel/d/v;->i:Ljava/util/Set;

    .line 63
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/d/v;->e:Landroid/content/Context;

    .line 64
    const-string/jumbo v0, "twitter_pref"

    invoke-virtual {p1, v0, v5}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/d/v;->f:Landroid/content/SharedPreferences;

    .line 65
    iget-object v0, p0, Lcom/peel/d/v;->f:Landroid/content/SharedPreferences;

    const-string/jumbo v1, "twitter_token"

    invoke-interface {v0, v1, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 67
    if-eqz v0, :cond_2

    .line 68
    new-instance v1, Lcom/peel/d/af;

    iget-object v2, p0, Lcom/peel/d/v;->f:Landroid/content/SharedPreferences;

    const-string/jumbo v3, "twitter_token_secret"

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, p0, v0, v2}, Lcom/peel/d/af;-><init>(Lcom/peel/d/v;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v1, p0, Lcom/peel/d/v;->d:Lcom/peel/d/af;

    .line 69
    iget-object v0, p0, Lcom/peel/d/v;->d:Lcom/peel/d/af;

    iget-object v1, p0, Lcom/peel/d/v;->f:Landroid/content/SharedPreferences;

    const-string/jumbo v2, "twitter_screen_name"

    invoke-interface {v1, v2, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/peel/d/af;->a(Ljava/lang/String;)V

    .line 70
    iget-object v0, p0, Lcom/peel/d/v;->d:Lcom/peel/d/af;

    iget-object v1, p0, Lcom/peel/d/v;->f:Landroid/content/SharedPreferences;

    const-string/jumbo v2, "twitter_user_name"

    invoke-interface {v1, v2, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/peel/d/af;->b(Ljava/lang/String;)V

    .line 71
    iget-object v0, p0, Lcom/peel/d/v;->d:Lcom/peel/d/af;

    iget-object v1, p0, Lcom/peel/d/v;->f:Landroid/content/SharedPreferences;

    const-string/jumbo v2, "twitter_profile_image"

    invoke-interface {v1, v2, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/peel/d/af;->c(Ljava/lang/String;)V

    .line 73
    iget-object v0, p0, Lcom/peel/d/v;->f:Landroid/content/SharedPreferences;

    const-string/jumbo v1, "favorite"

    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getStringSet(Ljava/lang/String;Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/d/v;->i:Ljava/util/Set;

    .line 74
    iget-object v0, p0, Lcom/peel/d/v;->f:Landroid/content/SharedPreferences;

    const-string/jumbo v1, "retweet"

    invoke-interface {v0, v1, v4}, Landroid/content/SharedPreferences;->getStringSet(Ljava/lang/String;Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    .line 76
    if-eqz v0, :cond_1

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v1

    if-lez v1, :cond_1

    .line 77
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 79
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 80
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 81
    const-string/jumbo v2, ":"

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 82
    const-string/jumbo v2, ":"

    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 83
    iget-object v2, p0, Lcom/peel/d/v;->h:Ljava/util/HashMap;

    aget-object v3, v0, v5

    const/4 v4, 0x1

    aget-object v0, v0, v4

    invoke-virtual {v2, v3, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 88
    :cond_1
    invoke-static {}, Lcom/peel/data/m;->a()Lcom/peel/data/m;

    move-result-object v0

    new-instance v1, Lcom/peel/d/w;

    invoke-direct {v1, p0}, Lcom/peel/d/w;-><init>(Lcom/peel/d/v;)V

    invoke-virtual {v0, v1}, Lcom/peel/data/m;->a(Lcom/peel/util/t;)V

    .line 104
    :cond_2
    return-void
.end method

.method private a(Ljava/lang/String;J)I
    .locals 4

    .prologue
    .line 342
    const-string/jumbo v0, "EEE MMM dd HH:mm:ss Z yyyy"

    .line 343
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string/jumbo v1, "EEE MMM dd HH:mm:ss Z yyyy"

    sget-object v2, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 344
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/text/SimpleDateFormat;->setLenient(Z)V

    .line 348
    :try_start_0
    invoke-virtual {v0, p1}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    .line 349
    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v0

    sub-long v0, p2, v0

    long-to-int v0, v0

    div-int/lit16 v0, v0, 0x3e8
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    .line 353
    :goto_0
    return v0

    .line 351
    :catch_0
    move-exception v0

    .line 352
    invoke-virtual {v0}, Ljava/text/ParseException;->printStackTrace()V

    .line 353
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;)Lcom/peel/d/v;
    .locals 2

    .prologue
    .line 107
    sget-object v0, Lcom/peel/d/v;->b:Lcom/peel/d/v;

    if-nez v0, :cond_1

    .line 108
    sget-object v1, Lcom/peel/d/v;->c:Ljava/lang/Object;

    monitor-enter v1

    .line 109
    :try_start_0
    sget-object v0, Lcom/peel/d/v;->b:Lcom/peel/d/v;

    if-nez v0, :cond_0

    .line 110
    new-instance v0, Lcom/peel/d/v;

    invoke-direct {v0, p0}, Lcom/peel/d/v;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/peel/d/v;->b:Lcom/peel/d/v;

    .line 112
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 115
    :cond_1
    sget-object v0, Lcom/peel/d/v;->b:Lcom/peel/d/v;

    return-object v0

    .line 112
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method static synthetic a(Lcom/peel/d/v;Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 39
    invoke-direct {p0, p1}, Lcom/peel/d/v;->a(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/peel/ui/model/TwittData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 360
    if-nez p1, :cond_1

    const/4 v0, 0x0

    .line 411
    :cond_0
    :goto_0
    return-object v0

    .line 362
    :cond_1
    const/4 v0, 0x0

    .line 365
    :try_start_0
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 366
    const-string/jumbo v2, "statuses"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v5

    .line 368
    if-eqz v5, :cond_0

    .line 369
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 370
    :try_start_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    .line 371
    const/4 v0, 0x0

    move v4, v0

    :goto_1
    invoke-virtual {v5}, Lorg/json/JSONArray;->length()I

    move-result v0

    if-ge v4, v0, :cond_9

    .line 372
    invoke-virtual {v5, v4}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v0

    invoke-virtual {p0, v0, v6, v7}, Lcom/peel/d/v;->a(Lorg/json/JSONObject;J)Lcom/peel/ui/model/TwittData;

    move-result-object v8

    .line 374
    if-eqz v8, :cond_3

    .line 375
    const/4 v2, 0x0

    .line 377
    const/4 v0, 0x0

    move v3, v0

    move v0, v2

    :goto_2
    sget-object v2, Lcom/peel/d/v;->j:[Ljava/lang/String;

    array-length v2, v2

    if-ge v3, v2, :cond_8

    .line 378
    invoke-virtual {v8}, Lcom/peel/ui/model/TwittData;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    sget-object v9, Lcom/peel/d/v;->j:[Ljava/lang/String;

    aget-object v9, v9, v3

    invoke-virtual {v2, v9}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 379
    const/4 v2, 0x1

    .line 393
    :goto_3
    iget-object v0, p0, Lcom/peel/d/v;->g:Ljava/util/HashSet;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/peel/d/v;->g:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->size()I

    move-result v0

    if-lez v0, :cond_7

    .line 394
    iget-object v0, p0, Lcom/peel/d/v;->g:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 395
    invoke-virtual {v8}, Lcom/peel/ui/model/TwittData;->a()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 396
    const/4 v2, 0x1

    .line 397
    sget-object v3, Lcom/peel/d/v;->a:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v10, "Blocking tweet <"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v9, ">: "

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v8}, Lcom/peel/ui/model/TwittData;->a()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v2

    .line 403
    :goto_4
    if-nez v0, :cond_3

    invoke-virtual {v1, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 371
    :cond_3
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto/16 :goto_1

    .line 383
    :cond_4
    invoke-virtual {v8}, Lcom/peel/ui/model/TwittData;->h()[Lcom/peel/ui/model/TwittData$TwittEntries;

    move-result-object v2

    if-eqz v2, :cond_5

    invoke-virtual {v8}, Lcom/peel/ui/model/TwittData;->h()[Lcom/peel/ui/model/TwittData$TwittEntries;

    move-result-object v2

    array-length v2, v2

    if-lez v2, :cond_5

    .line 384
    invoke-virtual {v8}, Lcom/peel/ui/model/TwittData;->h()[Lcom/peel/ui/model/TwittData$TwittEntries;

    move-result-object v9

    array-length v10, v9

    const/4 v2, 0x0

    :goto_5
    if-ge v2, v10, :cond_5

    aget-object v11, v9, v2

    .line 385
    invoke-virtual {v11}, Lcom/peel/ui/model/TwittData$TwittEntries;->c()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v11

    sget-object v12, Lcom/peel/d/v;->j:[Ljava/lang/String;

    aget-object v12, v12, v3

    invoke-virtual {v11, v12}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1

    move-result v11

    if-eqz v11, :cond_6

    .line 386
    const/4 v0, 0x1

    .line 377
    :cond_5
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto/16 :goto_2

    .line 384
    :cond_6
    add-int/lit8 v2, v2, 0x1

    goto :goto_5

    .line 407
    :catch_0
    move-exception v1

    .line 408
    :goto_6
    sget-object v2, Lcom/peel/d/v;->a:Ljava/lang/String;

    sget-object v3, Lcom/peel/d/v;->a:Ljava/lang/String;

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_0

    .line 407
    :catch_1
    move-exception v0

    move-object v13, v0

    move-object v0, v1

    move-object v1, v13

    goto :goto_6

    :cond_7
    move v0, v2

    goto :goto_4

    :cond_8
    move v2, v0

    goto/16 :goto_3

    :cond_9
    move-object v0, v1

    goto/16 :goto_0
.end method

.method static synthetic a(Lcom/peel/d/v;)Ljava/util/HashSet;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/peel/d/v;->g:Ljava/util/HashSet;

    return-object v0
.end method

.method static synthetic a(Lcom/peel/d/v;Ljava/util/HashSet;)Ljava/util/HashSet;
    .locals 0

    .prologue
    .line 39
    iput-object p1, p0, Lcom/peel/d/v;->g:Ljava/util/HashSet;

    return-object p1
.end method

.method static synthetic a(Lcom/peel/d/v;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0, p1, p2, p3}, Lcom/peel/d/v;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 137
    iget-object v0, p0, Lcom/peel/d/v;->f:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 138
    const-string/jumbo v1, "twitter_user_name"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 139
    const-string/jumbo v1, "twitter_screen_name"

    invoke-interface {v0, v1, p2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 140
    const-string/jumbo v1, "twitter_profile_image"

    invoke-interface {v0, v1, p3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 141
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 142
    return-void
.end method

.method static synthetic b(Lcom/peel/d/v;)Ljava/util/HashMap;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/peel/d/v;->h:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic c(Lcom/peel/d/v;)Lcom/peel/d/af;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/peel/d/v;->d:Lcom/peel/d/af;

    return-object v0
.end method

.method static synthetic d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 39
    sget-object v0, Lcom/peel/d/v;->a:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic d(Lcom/peel/d/v;)Ljava/util/Set;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/peel/d/v;->i:Ljava/util/Set;

    return-object v0
.end method


# virtual methods
.method public a(Lorg/json/JSONObject;J)Lcom/peel/ui/model/TwittData;
    .locals 12

    .prologue
    const/4 v1, 0x0

    .line 415
    new-instance v2, Lcom/peel/ui/model/TwittData;

    invoke-direct {v2}, Lcom/peel/ui/model/TwittData;-><init>()V

    .line 418
    :try_start_0
    const-string/jumbo v0, "id"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 419
    const-string/jumbo v0, "created_at"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/peel/ui/model/TwittData;->c(Ljava/lang/String;)V

    .line 420
    invoke-virtual {v2, v3}, Lcom/peel/ui/model/TwittData;->d(Ljava/lang/String;)V

    .line 422
    iget-object v0, p0, Lcom/peel/d/v;->h:Ljava/util/HashMap;

    if-eqz v0, :cond_1

    .line 423
    iget-object v0, p0, Lcom/peel/d/v;->h:Ljava/util/HashMap;

    invoke-virtual {v0, v3}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    invoke-virtual {v2, v0}, Lcom/peel/ui/model/TwittData;->a(Z)V

    .line 425
    iget-object v0, p0, Lcom/peel/d/v;->h:Ljava/util/HashMap;

    invoke-virtual {v0, v3}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 426
    iget-object v0, p0, Lcom/peel/d/v;->h:Ljava/util/HashMap;

    invoke-virtual {v0, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v2, v0}, Lcom/peel/ui/model/TwittData;->g(Ljava/lang/String;)V

    .line 432
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/peel/d/v;->i:Ljava/util/Set;

    if-eqz v0, :cond_2

    .line 433
    iget-object v0, p0, Lcom/peel/d/v;->i:Ljava/util/Set;

    invoke-interface {v0, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    invoke-virtual {v2, v0}, Lcom/peel/ui/model/TwittData;->b(Z)V

    .line 438
    :goto_1
    const-string/jumbo v0, "user"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    .line 439
    const-string/jumbo v3, "screen_name"

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/peel/ui/model/TwittData;->b(Ljava/lang/String;)V

    .line 440
    const-string/jumbo v3, "name"

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/peel/ui/model/TwittData;->f(Ljava/lang/String;)V

    .line 443
    const-string/jumbo v3, "profile_image_url"

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/peel/ui/model/TwittData;->e(Ljava/lang/String;)V

    .line 444
    const-string/jumbo v0, "created_at"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, p2, p3}, Lcom/peel/d/v;->a(Ljava/lang/String;J)I

    move-result v0

    invoke-virtual {v2, v0}, Lcom/peel/ui/model/TwittData;->a(I)V

    .line 445
    const-string/jumbo v0, "entities"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v3

    .line 447
    const-string/jumbo v0, "urls"

    invoke-virtual {v3, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 448
    const-string/jumbo v0, "urls"

    invoke-virtual {v3, v0}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v4

    .line 450
    invoke-virtual {v4}, Lorg/json/JSONArray;->length()I

    move-result v0

    new-array v5, v0, [Lcom/peel/ui/model/TwittData$TwittEntries;

    move v0, v1

    .line 452
    :goto_2
    invoke-virtual {v4}, Lorg/json/JSONArray;->length()I

    move-result v6

    if-ge v0, v6, :cond_3

    .line 453
    invoke-virtual {v4, v0}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v6

    .line 454
    const-string/jumbo v7, "indices"

    invoke-virtual {v6, v7}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v7

    .line 455
    new-instance v8, Lcom/peel/ui/model/TwittData$TwittEntries;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v9, 0x0

    invoke-virtual {v7, v9}, Lorg/json/JSONArray;->getInt(I)I

    move-result v9

    const/4 v10, 0x1

    invoke-virtual {v7, v10}, Lorg/json/JSONArray;->getInt(I)I

    move-result v7

    const-string/jumbo v10, "url"

    invoke-virtual {v6, v10}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v8, v2, v9, v7, v6}, Lcom/peel/ui/model/TwittData$TwittEntries;-><init>(Lcom/peel/ui/model/TwittData;IILjava/lang/String;)V

    .line 456
    aput-object v8, v5, v0

    .line 452
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 429
    :cond_1
    const/4 v0, 0x0

    invoke-virtual {v2, v0}, Lcom/peel/ui/model/TwittData;->a(Z)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 496
    :catch_0
    move-exception v0

    .line 497
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    .line 500
    :goto_3
    return-object v2

    .line 435
    :cond_2
    const/4 v0, 0x0

    :try_start_1
    invoke-virtual {v2, v0}, Lcom/peel/ui/model/TwittData;->b(Z)V

    goto/16 :goto_1

    .line 459
    :cond_3
    invoke-virtual {v2, v5}, Lcom/peel/ui/model/TwittData;->b([Lcom/peel/ui/model/TwittData$TwittEntries;)V

    .line 462
    :cond_4
    const-string/jumbo v0, "hashtags"

    invoke-virtual {v3, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 463
    const-string/jumbo v0, "hashtags"

    invoke-virtual {v3, v0}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v4

    .line 465
    invoke-virtual {v4}, Lorg/json/JSONArray;->length()I

    move-result v0

    new-array v5, v0, [Lcom/peel/ui/model/TwittData$TwittEntries;

    move v0, v1

    .line 467
    :goto_4
    invoke-virtual {v4}, Lorg/json/JSONArray;->length()I

    move-result v6

    if-ge v0, v6, :cond_5

    .line 468
    invoke-virtual {v4, v0}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v6

    .line 469
    const-string/jumbo v7, "indices"

    invoke-virtual {v6, v7}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v7

    .line 470
    new-instance v8, Lcom/peel/ui/model/TwittData$TwittEntries;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v9, 0x0

    invoke-virtual {v7, v9}, Lorg/json/JSONArray;->getInt(I)I

    move-result v9

    const/4 v10, 0x1

    invoke-virtual {v7, v10}, Lorg/json/JSONArray;->getInt(I)I

    move-result v7

    const-string/jumbo v10, "text"

    invoke-virtual {v6, v10}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v8, v2, v9, v7, v6}, Lcom/peel/ui/model/TwittData$TwittEntries;-><init>(Lcom/peel/ui/model/TwittData;IILjava/lang/String;)V

    .line 471
    aput-object v8, v5, v0

    .line 467
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 474
    :cond_5
    invoke-virtual {v2, v5}, Lcom/peel/ui/model/TwittData;->a([Lcom/peel/ui/model/TwittData$TwittEntries;)V

    .line 477
    :cond_6
    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v0, "text"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v4, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 478
    const-string/jumbo v0, "media"

    invoke-virtual {v3, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 479
    const-string/jumbo v0, "media"

    invoke-virtual {v3, v0}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v3

    .line 481
    invoke-virtual {v3}, Lorg/json/JSONArray;->length()I

    move-result v0

    new-array v5, v0, [Lcom/peel/ui/model/TwittData$TwittEntries;

    move v0, v1

    .line 483
    :goto_5
    invoke-virtual {v3}, Lorg/json/JSONArray;->length()I

    move-result v1

    if-ge v0, v1, :cond_7

    .line 484
    invoke-virtual {v3, v0}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v1

    .line 485
    const-string/jumbo v6, "indices"

    invoke-virtual {v1, v6}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v6

    .line 486
    const-string/jumbo v7, "media_url"

    invoke-virtual {v1, v7}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 487
    new-instance v7, Lcom/peel/ui/model/TwittData$TwittEntries;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v8, 0x0

    invoke-virtual {v6, v8}, Lorg/json/JSONArray;->getInt(I)I

    move-result v8

    const/4 v9, 0x1

    invoke-virtual {v6, v9}, Lorg/json/JSONArray;->getInt(I)I

    move-result v9

    invoke-direct {v7, v2, v8, v9, v1}, Lcom/peel/ui/model/TwittData$TwittEntries;-><init>(Lcom/peel/ui/model/TwittData;IILjava/lang/String;)V

    .line 488
    const/4 v1, 0x0

    invoke-virtual {v6, v1}, Lorg/json/JSONArray;->getInt(I)I

    move-result v1

    const/4 v8, 0x1

    invoke-virtual {v6, v8}, Lorg/json/JSONArray;->getInt(I)I

    move-result v6

    add-int/lit8 v6, v6, 0x1

    invoke-virtual {v4, v1, v6}, Ljava/lang/StringBuilder;->delete(II)Ljava/lang/StringBuilder;

    .line 489
    aput-object v7, v5, v0

    .line 483
    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    .line 492
    :cond_7
    invoke-virtual {v2, v5}, Lcom/peel/ui/model/TwittData;->c([Lcom/peel/ui/model/TwittData$TwittEntries;)V

    .line 495
    :cond_8
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/peel/ui/model/TwittData;->a(Ljava/lang/String;)V
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_3
.end method

.method public a()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 145
    iget-object v0, p0, Lcom/peel/d/v;->f:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 146
    const-string/jumbo v1, "twitter_token"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 147
    const-string/jumbo v1, "twitter_token_secret"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 148
    const-string/jumbo v1, "twitter_user_name"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 149
    const-string/jumbo v1, "twitter_screen_name"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 150
    const-string/jumbo v1, "twitter_profile_image"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 151
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 153
    iput-object v2, p0, Lcom/peel/d/v;->h:Ljava/util/HashMap;

    .line 154
    iput-object v2, p0, Lcom/peel/d/v;->i:Ljava/util/Set;

    .line 155
    iput-object v2, p0, Lcom/peel/d/v;->d:Lcom/peel/d/af;

    .line 156
    return-void
.end method

.method public a(IZLjava/lang/String;Ljava/util/HashMap;Lcom/peel/util/t;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IZ",
            "Ljava/lang/String;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Lcom/peel/util/t;",
            ")V"
        }
    .end annotation

    .prologue
    .line 212
    const-class v0, Lcom/peel/d/v;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v7

    const-string/jumbo v8, "post twitt"

    new-instance v0, Lcom/peel/d/x;

    move-object v1, p0

    move v2, p1

    move v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/peel/d/x;-><init>(Lcom/peel/d/v;IZLjava/lang/String;Ljava/util/HashMap;Lcom/peel/util/t;)V

    invoke-static {v7, v8, v0}, Lcom/peel/util/i;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    .line 272
    return-void
.end method

.method public a(Lcom/peel/util/t;)V
    .locals 3

    .prologue
    .line 504
    const-class v0, Lcom/peel/d/v;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "get User Info"

    new-instance v2, Lcom/peel/d/ad;

    invoke-direct {v2, p0, p1}, Lcom/peel/d/ad;-><init>(Lcom/peel/d/v;Lcom/peel/util/t;)V

    invoke-static {v0, v1, v2}, Lcom/peel/util/i;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    .line 544
    return-void
.end method

.method public a(Ljava/io/File;Ljava/util/Map;Lcom/peel/util/t;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/File;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Lcom/peel/util/t;",
            ")V"
        }
    .end annotation

    .prologue
    .line 275
    const-class v0, Lcom/peel/d/v;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "post tiwtt image"

    new-instance v2, Lcom/peel/d/z;

    invoke-direct {v2, p0, p1, p2, p3}, Lcom/peel/d/z;-><init>(Lcom/peel/d/v;Ljava/io/File;Ljava/util/Map;Lcom/peel/util/t;)V

    invoke-static {v0, v1, v2}, Lcom/peel/util/i;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    .line 286
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 119
    new-instance v0, Lcom/peel/d/af;

    invoke-direct {v0, p0, p1, p2}, Lcom/peel/d/af;-><init>(Lcom/peel/d/v;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/peel/d/v;->d:Lcom/peel/d/af;

    .line 121
    iget-object v0, p0, Lcom/peel/d/v;->f:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 122
    const-string/jumbo v1, "twitter_token"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 123
    const-string/jumbo v1, "twitter_token_secret"

    invoke-interface {v0, v1, p2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 125
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 127
    iget-object v0, p0, Lcom/peel/d/v;->h:Ljava/util/HashMap;

    if-nez v0, :cond_0

    .line 128
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/peel/d/v;->h:Ljava/util/HashMap;

    .line 131
    :cond_0
    iget-object v0, p0, Lcom/peel/d/v;->i:Ljava/util/Set;

    if-nez v0, :cond_1

    .line 132
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/peel/d/v;->i:Ljava/util/Set;

    .line 134
    :cond_1
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;ILcom/peel/util/t;)V
    .locals 8

    .prologue
    .line 311
    const-class v0, Lcom/peel/d/v;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v6

    const-string/jumbo v7, "search by twitter keyword"

    new-instance v0, Lcom/peel/d/ab;

    move-object v1, p0

    move-object v2, p1

    move v3, p3

    move-object v4, p2

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/peel/d/ab;-><init>(Lcom/peel/d/v;Ljava/lang/String;ILjava/lang/String;Lcom/peel/util/t;)V

    invoke-static {v6, v7, v0}, Lcom/peel/util/i;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    .line 339
    return-void
.end method

.method public b()Lcom/peel/d/af;
    .locals 1

    .prologue
    .line 159
    iget-object v0, p0, Lcom/peel/d/v;->d:Lcom/peel/d/af;

    return-object v0
.end method

.method public c()V
    .locals 6

    .prologue
    .line 289
    iget-object v0, p0, Lcom/peel/d/v;->e:Landroid/content/Context;

    const-string/jumbo v1, "twitter_pref"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 290
    iget-object v0, p0, Lcom/peel/d/v;->i:Ljava/util/Set;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/peel/d/v;->i:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 291
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v2, "favorite"

    iget-object v3, p0, Lcom/peel/d/v;->i:Ljava/util/Set;

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putStringSet(Ljava/lang/String;Ljava/util/Set;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 294
    :cond_0
    iget-object v0, p0, Lcom/peel/d/v;->h:Ljava/util/HashMap;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/peel/d/v;->h:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->size()I

    move-result v0

    if-lez v0, :cond_2

    .line 295
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    .line 297
    iget-object v0, p0, Lcom/peel/d/v;->h:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    .line 299
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .line 301
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 302
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 303
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, ":"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/peel/d/v;->h:Ljava/util/HashMap;

    invoke-virtual {v5, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 306
    :cond_1
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v1, "retweet"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putStringSet(Ljava/lang/String;Ljava/util/Set;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 308
    :cond_2
    return-void
.end method
