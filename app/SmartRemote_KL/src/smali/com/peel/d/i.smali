.class public final Lcom/peel/d/i;
.super Ljava/lang/Object;


# static fields
.field private static e:Lcom/peel/d/i;


# instance fields
.field public a:Z

.field protected b:Ljava/lang/String;

.field private final c:Lcom/peel/util/a/d;

.field private final d:Landroid/os/Bundle;

.field private f:Landroid/support/v4/app/a;

.field private g:Lcom/peel/d/a;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 50
    const/4 v0, 0x0

    sput-object v0, Lcom/peel/d/i;->e:Lcom/peel/d/i;

    return-void
.end method

.method private constructor <init>(Lcom/peel/util/a/d;)V
    .locals 1

    .prologue
    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, Lcom/peel/d/i;->d:Landroid/os/Bundle;

    .line 51
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/peel/d/i;->a:Z

    .line 56
    iput-object p1, p0, Lcom/peel/d/i;->c:Lcom/peel/util/a/d;

    .line 57
    return-void
.end method

.method static synthetic a(Lcom/peel/d/i;)Landroid/support/v4/app/a;
    .locals 1

    .prologue
    .line 13
    iget-object v0, p0, Lcom/peel/d/i;->f:Landroid/support/v4/app/a;

    return-object v0
.end method

.method public static a()Lcom/peel/d/i;
    .locals 1

    .prologue
    .line 58
    sget-object v0, Lcom/peel/d/i;->e:Lcom/peel/d/i;

    return-object v0
.end method

.method public static a(Lcom/peel/util/a/d;)V
    .locals 2

    .prologue
    .line 16
    new-instance v0, Lcom/peel/d/i;

    invoke-direct {v0, p0}, Lcom/peel/d/i;-><init>(Lcom/peel/util/a/d;)V

    sput-object v0, Lcom/peel/d/i;->e:Lcom/peel/d/i;

    .line 18
    invoke-virtual {p0}, Lcom/peel/util/a/d;->getSupportFragmentManager()Landroid/support/v4/app/aj;

    move-result-object v0

    .line 19
    new-instance v1, Lcom/peel/d/j;

    invoke-direct {v1, p0}, Lcom/peel/d/j;-><init>(Lcom/peel/util/a/d;)V

    invoke-virtual {v0, v1}, Landroid/support/v4/app/aj;->a(Landroid/support/v4/app/ak;)V

    .line 39
    return-void
.end method

.method static synthetic b(Lcom/peel/d/i;)Lcom/peel/d/a;
    .locals 1

    .prologue
    .line 13
    iget-object v0, p0, Lcom/peel/d/i;->g:Lcom/peel/d/a;

    return-object v0
.end method

.method public static b()V
    .locals 1

    .prologue
    .line 59
    const/4 v0, 0x0

    sput-object v0, Lcom/peel/d/i;->e:Lcom/peel/d/i;

    return-void
.end method

.method static synthetic j()Lcom/peel/d/i;
    .locals 1

    .prologue
    .line 13
    sget-object v0, Lcom/peel/d/i;->e:Lcom/peel/d/i;

    return-object v0
.end method


# virtual methods
.method public declared-synchronized a(Ljava/lang/String;I)I
    .locals 1

    .prologue
    .line 73
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/peel/d/i;->d:Landroid/os/Bundle;

    invoke-virtual {v0, p1, p2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 65
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/peel/d/i;->d:Landroid/os/Bundle;

    invoke-virtual {v0, p1}, Landroid/os/Bundle;->putAll(Landroid/os/Bundle;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a(Landroid/support/v4/app/a;)V
    .locals 0

    .prologue
    .line 122
    iput-object p1, p0, Lcom/peel/d/i;->f:Landroid/support/v4/app/a;

    .line 123
    return-void
.end method

.method public a(Lcom/peel/d/a;)V
    .locals 3

    .prologue
    .line 126
    iput-object p1, p0, Lcom/peel/d/i;->g:Lcom/peel/d/a;

    .line 127
    iget-object v0, p0, Lcom/peel/d/i;->c:Lcom/peel/util/a/d;

    invoke-virtual {v0}, Lcom/peel/util/a/d;->invalidateOptionsMenu()V

    .line 130
    iget-object v0, p0, Lcom/peel/d/i;->c:Lcom/peel/util/a/d;

    invoke-static {v0}, Lcom/peel/d/e;->b(Landroid/support/v4/app/ae;)I

    move-result v0

    .line 131
    sget-object v1, Lcom/peel/d/i;->e:Lcom/peel/d/i;

    iget-object v1, v1, Lcom/peel/d/i;->f:Landroid/support/v4/app/a;

    if-nez v0, :cond_0

    if-eqz p1, :cond_0

    sget-object v0, Lcom/peel/d/b;->a:Lcom/peel/d/b;

    iget-object v2, p1, Lcom/peel/d/a;->c:Lcom/peel/d/b;

    if-ne v0, v2, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Landroid/support/v4/app/a;->a(Z)V

    .line 134
    return-void

    .line 131
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public declared-synchronized a(Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 82
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/peel/d/i;->d:Landroid/os/Bundle;

    invoke-virtual {v0, p1, p2}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 83
    monitor-exit p0

    return-void

    .line 82
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 70
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/peel/d/i;->d:Landroid/os/Bundle;

    invoke-virtual {v0, p1, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 71
    monitor-exit p0

    return-void

    .line 70
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(Ljava/lang/String;Ljava/util/ArrayList;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 89
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/peel/d/i;->d:Landroid/os/Bundle;

    invoke-virtual {v0, p1, p2}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 90
    monitor-exit p0

    return-void

    .line 89
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a(Z)V
    .locals 2

    .prologue
    .line 101
    iget-object v0, p0, Lcom/peel/d/i;->c:Lcom/peel/util/a/d;

    if-eqz v0, :cond_0

    .line 102
    iget-object v0, p0, Lcom/peel/d/i;->c:Lcom/peel/util/a/d;

    sget v1, Lcom/peel/ui/fp;->drawer_layout:I

    invoke-virtual {v0, v1}, Lcom/peel/util/a/d;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/widget/DrawerLayout;

    .line 103
    if-eqz v0, :cond_0

    .line 104
    if-eqz p1, :cond_1

    const/4 v1, 0x1

    :goto_0
    invoke-virtual {v0, v1}, Landroid/support/v4/widget/DrawerLayout;->setDrawerLockMode(I)V

    .line 108
    :cond_0
    return-void

    .line 104
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public declared-synchronized a(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 64
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/peel/d/i;->d:Landroid/os/Bundle;

    invoke-virtual {v0, p1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized b(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 67
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/peel/d/i;->d:Landroid/os/Bundle;

    invoke-virtual {v0, p1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized b(Ljava/lang/String;I)V
    .locals 1

    .prologue
    .line 76
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/peel/d/i;->d:Landroid/os/Bundle;

    invoke-virtual {v0, p1, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 77
    monitor-exit p0

    return-void

    .line 76
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized c(Ljava/lang/String;)Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 79
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/peel/d/i;->d:Landroid/os/Bundle;

    invoke-virtual {v0, p1}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public c()Landroid/support/v4/app/ae;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/peel/d/i;->c:Lcom/peel/util/a/d;

    return-object v0
.end method

.method public declared-synchronized d()V
    .locals 1

    .prologue
    .line 62
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/peel/d/i;->d:Landroid/os/Bundle;

    invoke-virtual {v0}, Landroid/os/Bundle;->clear()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized d(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 84
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/peel/d/i;->d:Landroid/os/Bundle;

    invoke-virtual {v0, p1}, Landroid/os/Bundle;->remove(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized e()Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 63
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/peel/d/i;->d:Landroid/os/Bundle;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized e(Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 86
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/peel/d/i;->d:Landroid/os/Bundle;

    invoke-virtual {v0, p1}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lcom/peel/d/i;->b:Ljava/lang/String;

    return-object v0
.end method

.method public f(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 97
    iput-object p1, p0, Lcom/peel/d/i;->b:Ljava/lang/String;

    .line 98
    return-void
.end method

.method public g()V
    .locals 2

    .prologue
    .line 111
    iget-object v0, p0, Lcom/peel/d/i;->c:Lcom/peel/util/a/d;

    if-eqz v0, :cond_0

    .line 112
    iget-object v0, p0, Lcom/peel/d/i;->c:Lcom/peel/util/a/d;

    sget v1, Lcom/peel/ui/fp;->drawer_layout:I

    invoke-virtual {v0, v1}, Lcom/peel/util/a/d;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/widget/DrawerLayout;

    .line 113
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/support/v4/widget/DrawerLayout;->b()V

    .line 115
    :cond_0
    return-void
.end method

.method public h()Landroid/support/v4/app/a;
    .locals 1

    .prologue
    .line 118
    sget-object v0, Lcom/peel/d/i;->e:Lcom/peel/d/i;

    iget-object v0, v0, Lcom/peel/d/i;->f:Landroid/support/v4/app/a;

    return-object v0
.end method

.method public i()Lcom/peel/d/a;
    .locals 1

    .prologue
    .line 137
    iget-object v0, p0, Lcom/peel/d/i;->g:Lcom/peel/d/a;

    return-object v0
.end method
