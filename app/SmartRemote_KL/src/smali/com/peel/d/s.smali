.class Lcom/peel/d/s;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field final synthetic a:[Lcom/peel/data/ContentRoom;

.field final synthetic b:Lcom/peel/d/k;


# direct methods
.method constructor <init>(Lcom/peel/d/k;[Lcom/peel/data/ContentRoom;)V
    .locals 0

    .prologue
    .line 552
    iput-object p1, p0, Lcom/peel/d/s;->b:Lcom/peel/d/k;

    iput-object p2, p0, Lcom/peel/d/s;->a:[Lcom/peel/data/ContentRoom;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 555
    sget-object v0, Lcom/peel/content/a;->h:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/peel/d/s;->b:Lcom/peel/d/k;

    iget-object v0, v0, Lcom/peel/d/k;->b:Lcom/peel/d/i;

    if-nez v0, :cond_1

    .line 566
    :cond_0
    :goto_0
    return-void

    .line 557
    :cond_1
    iget-object v0, p0, Lcom/peel/d/s;->b:Lcom/peel/d/k;

    invoke-static {v0}, Lcom/peel/d/k;->b(Lcom/peel/d/k;)Lcom/peel/widget/ag;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 558
    iget-object v0, p0, Lcom/peel/d/s;->b:Lcom/peel/d/k;

    invoke-static {v0}, Lcom/peel/d/k;->b(Lcom/peel/d/k;)Lcom/peel/widget/ag;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/widget/ag;->dismiss()V

    .line 559
    iget-object v0, p0, Lcom/peel/d/s;->b:Lcom/peel/d/k;

    invoke-static {v0, v2}, Lcom/peel/d/k;->a(Lcom/peel/d/k;Lcom/peel/widget/ag;)Lcom/peel/widget/ag;

    .line 561
    :cond_2
    invoke-virtual {p1}, Landroid/widget/AdapterView;->getAdapter()Landroid/widget/Adapter;

    move-result-object v0

    invoke-interface {v0}, Landroid/widget/Adapter;->getCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-ne v0, p3, :cond_3

    .line 562
    iget-object v0, p0, Lcom/peel/d/s;->b:Lcom/peel/d/k;

    invoke-virtual {v0}, Lcom/peel/d/k;->T()V

    goto :goto_0

    .line 564
    :cond_3
    iget-object v0, p0, Lcom/peel/d/s;->a:[Lcom/peel/data/ContentRoom;

    aget-object v0, v0, p3

    invoke-virtual {v0}, Lcom/peel/data/ContentRoom;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v1, v1, v2}, Lcom/peel/content/a;->a(Ljava/lang/String;ZZLcom/peel/util/t;)V

    goto :goto_0
.end method
