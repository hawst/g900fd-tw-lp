.class Lcom/peel/d/ae;
.super Lcom/peel/util/t;


# instance fields
.field final synthetic a:Lcom/peel/d/ad;


# direct methods
.method constructor <init>(Lcom/peel/d/ad;)V
    .locals 0

    .prologue
    .line 507
    iput-object p1, p0, Lcom/peel/d/ae;->a:Lcom/peel/d/ad;

    invoke-direct {p0}, Lcom/peel/util/t;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    const/4 v2, 0x1

    .line 510
    const/4 v1, 0x0

    .line 512
    iget-boolean v0, p0, Lcom/peel/d/ae;->i:Z

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/peel/d/ae;->j:Ljava/lang/Object;

    if-eqz v0, :cond_5

    .line 514
    :try_start_0
    new-instance v3, Lorg/json/JSONObject;

    iget-object v0, p0, Lcom/peel/d/ae;->j:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    invoke-direct {v3, v0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 515
    const-string/jumbo v0, "name"

    invoke-virtual {v3, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 516
    const-string/jumbo v0, "profile_image_url"

    invoke-virtual {v3, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 517
    const-string/jumbo v0, "screen_name"

    invoke-virtual {v3, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 519
    if-eqz v4, :cond_0

    if-eqz v5, :cond_0

    if-eqz v6, :cond_0

    iget-object v0, p0, Lcom/peel/d/ae;->a:Lcom/peel/d/ad;

    iget-object v0, v0, Lcom/peel/d/ad;->b:Lcom/peel/d/v;

    invoke-static {v0}, Lcom/peel/d/v;->c(Lcom/peel/d/v;)Lcom/peel/d/af;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/d/af;->g()Ljava/lang/String;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    if-nez v0, :cond_3

    :cond_0
    move v0, v2

    .line 525
    :goto_0
    :try_start_1
    iget-object v1, p0, Lcom/peel/d/ae;->a:Lcom/peel/d/ad;

    iget-object v1, v1, Lcom/peel/d/ad;->b:Lcom/peel/d/v;

    invoke-static {v1}, Lcom/peel/d/v;->c(Lcom/peel/d/v;)Lcom/peel/d/af;

    move-result-object v1

    const-string/jumbo v2, "name"

    invoke-virtual {v3, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/peel/d/af;->b(Ljava/lang/String;)V

    .line 527
    const-string/jumbo v1, "profile_image_url"

    invoke-virtual {v3, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 528
    iget-object v1, p0, Lcom/peel/d/ae;->a:Lcom/peel/d/ad;

    iget-object v1, v1, Lcom/peel/d/ad;->b:Lcom/peel/d/v;

    invoke-static {v1}, Lcom/peel/d/v;->c(Lcom/peel/d/v;)Lcom/peel/d/af;

    move-result-object v1

    const-string/jumbo v2, "profile_image_url"

    invoke-virtual {v3, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/peel/d/af;->c(Ljava/lang/String;)V

    .line 531
    :cond_1
    iget-object v1, p0, Lcom/peel/d/ae;->a:Lcom/peel/d/ad;

    iget-object v1, v1, Lcom/peel/d/ad;->b:Lcom/peel/d/v;

    invoke-static {v1}, Lcom/peel/d/v;->c(Lcom/peel/d/v;)Lcom/peel/d/af;

    move-result-object v1

    const-string/jumbo v2, "screen_name"

    invoke-virtual {v3, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/peel/d/af;->a(Ljava/lang/String;)V

    .line 532
    iget-object v1, p0, Lcom/peel/d/ae;->a:Lcom/peel/d/ad;

    iget-object v1, v1, Lcom/peel/d/ad;->b:Lcom/peel/d/v;

    invoke-static {v1, v4, v6, v5}, Lcom/peel/d/v;->a(Lcom/peel/d/v;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1

    .line 538
    :goto_1
    iget-object v1, p0, Lcom/peel/d/ae;->a:Lcom/peel/d/ad;

    iget-object v1, v1, Lcom/peel/d/ad;->a:Lcom/peel/util/t;

    if-eqz v1, :cond_2

    .line 539
    iget-object v1, p0, Lcom/peel/d/ae;->a:Lcom/peel/d/ad;

    iget-object v1, v1, Lcom/peel/d/ad;->a:Lcom/peel/util/t;

    iget-boolean v2, p0, Lcom/peel/d/ae;->i:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iget-object v3, p0, Lcom/peel/d/ae;->k:Ljava/lang/String;

    invoke-virtual {v1, v2, v0, v3}, Lcom/peel/util/t;->a(ZLjava/lang/Object;Ljava/lang/String;)V

    .line 540
    :cond_2
    return-void

    .line 521
    :cond_3
    :try_start_2
    iget-object v0, p0, Lcom/peel/d/ae;->a:Lcom/peel/d/ad;

    iget-object v0, v0, Lcom/peel/d/ad;->b:Lcom/peel/d/v;

    invoke-static {v0}, Lcom/peel/d/v;->c(Lcom/peel/d/v;)Lcom/peel/d/af;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/d/af;->g()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/peel/d/ae;->a:Lcom/peel/d/ad;

    iget-object v0, v0, Lcom/peel/d/ad;->b:Lcom/peel/d/v;

    invoke-static {v0}, Lcom/peel/d/v;->c(Lcom/peel/d/v;)Lcom/peel/d/af;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/d/af;->g()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_0

    move-result v0

    if-nez v0, :cond_4

    move v0, v2

    .line 522
    goto :goto_0

    .line 533
    :catch_0
    move-exception v0

    move-object v7, v0

    move v0, v1

    move-object v1, v7

    .line 534
    :goto_2
    invoke-static {}, Lcom/peel/d/v;->d()Ljava/lang/String;

    move-result-object v2

    invoke-static {}, Lcom/peel/d/v;->d()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1

    .line 533
    :catch_1
    move-exception v1

    goto :goto_2

    :cond_4
    move v0, v1

    goto/16 :goto_0

    :cond_5
    move v0, v1

    goto :goto_1
.end method
