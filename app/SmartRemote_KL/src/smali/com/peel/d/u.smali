.class public Lcom/peel/d/u;
.super Landroid/support/v4/app/Fragment;

# interfaces
.implements Lcom/peel/d/h;


# instance fields
.field protected final a:Ljava/lang/String;

.field public b:Lcom/peel/d/i;

.field public final c:Landroid/os/Bundle;

.field public d:Lcom/peel/d/a;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 9
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 10
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/d/u;->a:Ljava/lang/String;

    .line 13
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, Lcom/peel/d/u;->c:Landroid/os/Bundle;

    .line 66
    return-void
.end method


# virtual methods
.method public W()Z
    .locals 1

    .prologue
    .line 36
    const/4 v0, 0x0

    return v0
.end method

.method public X()Z
    .locals 1

    .prologue
    .line 41
    const/4 v0, 0x0

    return v0
.end method

.method public Y()Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/peel/d/u;->c:Landroid/os/Bundle;

    return-object v0
.end method

.method public Z()V
    .locals 0

    .prologue
    .line 73
    return-void
.end method

.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 52
    const/4 v0, 0x0

    return-object v0
.end method

.method public a_(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 17
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->a_(Landroid/os/Bundle;)V

    .line 18
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/peel/d/u;->d(Z)V

    .line 20
    invoke-virtual {p0}, Lcom/peel/d/u;->j()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/peel/d/u;->c:Landroid/os/Bundle;

    invoke-virtual {p0}, Lcom/peel/d/u;->j()Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->putAll(Landroid/os/Bundle;)V

    .line 21
    :cond_0
    return-void
.end method

.method public aa()Z
    .locals 1

    .prologue
    .line 75
    const/4 v0, 0x0

    return v0
.end method

.method public ab()Z
    .locals 1

    .prologue
    .line 76
    const/4 v0, 0x1

    return v0
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 53
    const/4 v0, 0x0

    return v0
.end method

.method public c(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 58
    if-nez p1, :cond_0

    .line 59
    iget-object v0, p0, Lcom/peel/d/u;->c:Landroid/os/Bundle;

    invoke-virtual {v0}, Landroid/os/Bundle;->clear()V

    .line 63
    :goto_0
    return-void

    .line 61
    :cond_0
    iget-object v0, p0, Lcom/peel/d/u;->c:Landroid/os/Bundle;

    invoke-virtual {v0, p1}, Landroid/os/Bundle;->putAll(Landroid/os/Bundle;)V

    goto :goto_0
.end method

.method public d(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 25
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->d(Landroid/os/Bundle;)V

    .line 26
    invoke-static {}, Lcom/peel/d/i;->a()Lcom/peel/d/i;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/d/u;->b:Lcom/peel/d/i;

    .line 27
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 46
    return-void
.end method

.method public y()V
    .locals 1

    .prologue
    .line 31
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/peel/d/u;->b:Lcom/peel/d/i;

    .line 32
    iget-object v0, p0, Lcom/peel/d/u;->c:Landroid/os/Bundle;

    invoke-virtual {v0}, Landroid/os/Bundle;->clear()V

    .line 33
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->y()V

    .line 34
    return-void
.end method
