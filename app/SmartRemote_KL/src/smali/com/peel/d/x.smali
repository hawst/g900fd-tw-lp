.class Lcom/peel/d/x;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:I

.field final synthetic b:Z

.field final synthetic c:Ljava/lang/String;

.field final synthetic d:Ljava/util/HashMap;

.field final synthetic e:Lcom/peel/util/t;

.field final synthetic f:Lcom/peel/d/v;


# direct methods
.method constructor <init>(Lcom/peel/d/v;IZLjava/lang/String;Ljava/util/HashMap;Lcom/peel/util/t;)V
    .locals 0

    .prologue
    .line 212
    iput-object p1, p0, Lcom/peel/d/x;->f:Lcom/peel/d/v;

    iput p2, p0, Lcom/peel/d/x;->a:I

    iput-boolean p3, p0, Lcom/peel/d/x;->b:Z

    iput-object p4, p0, Lcom/peel/d/x;->c:Ljava/lang/String;

    iput-object p5, p0, Lcom/peel/d/x;->d:Ljava/util/HashMap;

    iput-object p6, p0, Lcom/peel/d/x;->e:Lcom/peel/util/t;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .prologue
    .line 215
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 216
    iget v0, p0, Lcom/peel/d/x;->a:I

    packed-switch v0, :pswitch_data_0

    .line 236
    :cond_0
    :goto_0
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    if-nez v0, :cond_3

    .line 270
    :goto_1
    return-void

    .line 219
    :pswitch_0
    iget-boolean v0, p0, Lcom/peel/d/x;->b:Z

    if-nez v0, :cond_1

    .line 220
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "https://api.twitter.com/1.1/statuses/retweet/"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/peel/d/x;->c:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v2, ".json"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 221
    :cond_1
    iget-object v0, p0, Lcom/peel/d/x;->f:Lcom/peel/d/v;

    invoke-static {v0}, Lcom/peel/d/v;->b(Lcom/peel/d/v;)Ljava/util/HashMap;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/peel/d/x;->f:Lcom/peel/d/v;

    invoke-static {v0}, Lcom/peel/d/v;->b(Lcom/peel/d/v;)Ljava/util/HashMap;

    move-result-object v0

    iget-object v2, p0, Lcom/peel/d/x;->c:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 222
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "https://api.twitter.com/1.1/statuses/destroy/"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v0, p0, Lcom/peel/d/x;->f:Lcom/peel/d/v;

    invoke-static {v0}, Lcom/peel/d/v;->b(Lcom/peel/d/v;)Ljava/util/HashMap;

    move-result-object v0

    iget-object v3, p0, Lcom/peel/d/x;->c:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v2, ".json"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 228
    :pswitch_1
    iget-boolean v0, p0, Lcom/peel/d/x;->b:Z

    if-eqz v0, :cond_2

    const-string/jumbo v0, "https://api.twitter.com/1.1/favorites/destroy.json"

    :goto_2
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    :cond_2
    const-string/jumbo v0, "https://api.twitter.com/1.1/favorites/create.json"

    goto :goto_2

    .line 231
    :pswitch_2
    const-string/jumbo v0, "https://api.twitter.com/1.1/statuses/update.json"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_0

    .line 238
    :cond_3
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 239
    iget-object v1, p0, Lcom/peel/d/x;->f:Lcom/peel/d/v;

    invoke-static {v1}, Lcom/peel/d/v;->c(Lcom/peel/d/v;)Lcom/peel/d/af;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/d/af;->c()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/peel/d/x;->f:Lcom/peel/d/v;

    invoke-static {v2}, Lcom/peel/d/v;->c(Lcom/peel/d/v;)Lcom/peel/d/af;

    move-result-object v2

    invoke-virtual {v2}, Lcom/peel/d/af;->d()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/peel/d/x;->f:Lcom/peel/d/v;

    invoke-static {v3}, Lcom/peel/d/v;->c(Lcom/peel/d/v;)Lcom/peel/d/af;

    move-result-object v3

    invoke-virtual {v3}, Lcom/peel/d/af;->a()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/peel/d/x;->f:Lcom/peel/d/v;

    invoke-static {v4}, Lcom/peel/d/v;->c(Lcom/peel/d/v;)Lcom/peel/d/af;

    move-result-object v4

    invoke-virtual {v4}, Lcom/peel/d/af;->b()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/peel/d/x;->d:Ljava/util/HashMap;

    new-instance v6, Lcom/peel/d/y;

    invoke-direct {v6, p0, v0}, Lcom/peel/d/y;-><init>(Lcom/peel/d/x;Ljava/lang/String;)V

    invoke-static/range {v0 .. v6}, Lcom/peel/util/b/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;Lcom/peel/util/t;)V

    goto/16 :goto_1

    .line 216
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
