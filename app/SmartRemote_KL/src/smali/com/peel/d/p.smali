.class Lcom/peel/d/p;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field final synthetic a:Lcom/peel/d/k;


# direct methods
.method constructor <init>(Lcom/peel/d/k;)V
    .locals 0

    .prologue
    .line 453
    iput-object p1, p0, Lcom/peel/d/p;->a:Lcom/peel/d/k;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 456
    iget-object v0, p0, Lcom/peel/d/p;->a:Lcom/peel/d/k;

    iget-object v0, v0, Lcom/peel/d/k;->b:Lcom/peel/d/i;

    if-nez v0, :cond_0

    .line 489
    :goto_0
    return-void

    .line 458
    :cond_0
    packed-switch p3, :pswitch_data_0

    .line 488
    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/peel/d/p;->a:Lcom/peel/d/k;

    invoke-static {v0}, Lcom/peel/d/k;->a(Lcom/peel/d/k;)Landroid/widget/PopupWindow;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->dismiss()V

    goto :goto_0

    .line 460
    :pswitch_0
    iget-object v0, p0, Lcom/peel/d/p;->a:Lcom/peel/d/k;

    invoke-virtual {v0}, Lcom/peel/d/k;->S()V

    goto :goto_1

    .line 463
    :pswitch_1
    invoke-static {}, Lcom/peel/content/a;->a()Lcom/peel/data/ContentRoom;

    move-result-object v1

    .line 464
    const-string/jumbo v0, "live"

    invoke-static {v0}, Lcom/peel/content/a;->c(Ljava/lang/String;)Lcom/peel/content/library/Library;

    move-result-object v0

    check-cast v0, Lcom/peel/content/library/LiveLibrary;

    .line 465
    if-eqz v1, :cond_1

    if-eqz v0, :cond_1

    .line 468
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 469
    const-string/jumbo v3, "room"

    invoke-virtual {v2, v3, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 470
    const-string/jumbo v1, "library"

    invoke-virtual {v2, v1, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 471
    iget-object v0, p0, Lcom/peel/d/p;->a:Lcom/peel/d/k;

    invoke-virtual {v0}, Lcom/peel/d/k;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    const-class v1, Lcom/peel/h/a/bb;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v2}, Lcom/peel/d/e;->b(Landroid/support/v4/app/ae;Ljava/lang/String;Landroid/os/Bundle;)V

    goto :goto_1

    .line 475
    :pswitch_2
    iget-object v0, p0, Lcom/peel/d/p;->a:Lcom/peel/d/k;

    invoke-virtual {v0}, Lcom/peel/d/k;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    const-class v1, Lcom/peel/h/a/gr;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v3}, Lcom/peel/d/e;->b(Landroid/support/v4/app/ae;Ljava/lang/String;Landroid/os/Bundle;)V

    goto :goto_1

    .line 480
    :pswitch_3
    iget-object v0, p0, Lcom/peel/d/p;->a:Lcom/peel/d/k;

    invoke-virtual {v0}, Lcom/peel/d/k;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    const-string/jumbo v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/ae;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 481
    sget v1, Lcom/peel/ui/fq;->about:I

    invoke-virtual {v0, v1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 482
    sget v0, Lcom/peel/ui/fp;->about_app_version:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v2, p0, Lcom/peel/d/p;->a:Lcom/peel/d/k;

    invoke-virtual {v2}, Lcom/peel/d/k;->m()Landroid/support/v4/app/ae;

    move-result-object v2

    invoke-static {v2}, Lcom/peel/util/bx;->g(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 483
    new-instance v0, Lcom/peel/widget/ag;

    iget-object v2, p0, Lcom/peel/d/p;->a:Lcom/peel/d/k;

    invoke-virtual {v2}, Lcom/peel/d/k;->m()Landroid/support/v4/app/ae;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/peel/widget/ag;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0}, Lcom/peel/widget/ag;->b()Lcom/peel/widget/ag;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/peel/widget/ag;->a(Landroid/view/View;)Lcom/peel/widget/ag;

    move-result-object v0

    sget v1, Lcom/peel/ui/ft;->ok:I

    invoke-virtual {v0, v1, v3}, Lcom/peel/widget/ag;->a(ILandroid/content/DialogInterface$OnClickListener;)Lcom/peel/widget/ag;

    move-result-object v0

    .line 484
    invoke-virtual {v0}, Lcom/peel/widget/ag;->show()V

    goto/16 :goto_1

    .line 458
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
