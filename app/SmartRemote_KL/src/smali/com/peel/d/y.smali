.class Lcom/peel/d/y;
.super Lcom/peel/util/t;


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic b:Lcom/peel/d/x;


# direct methods
.method constructor <init>(Lcom/peel/d/x;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 239
    iput-object p1, p0, Lcom/peel/d/y;->b:Lcom/peel/d/x;

    iput-object p2, p0, Lcom/peel/d/y;->a:Ljava/lang/String;

    invoke-direct {p0}, Lcom/peel/util/t;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    .line 242
    iget-boolean v0, p0, Lcom/peel/d/y;->i:Z

    if-eqz v0, :cond_5

    .line 243
    iget-object v0, p0, Lcom/peel/d/y;->a:Ljava/lang/String;

    const-string/jumbo v1, "https://api.twitter.com/1.1/statuses/destroy/"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 244
    iget-object v0, p0, Lcom/peel/d/y;->b:Lcom/peel/d/x;

    iget-object v0, v0, Lcom/peel/d/x;->f:Lcom/peel/d/v;

    invoke-static {v0}, Lcom/peel/d/v;->b(Lcom/peel/d/v;)Ljava/util/HashMap;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/d/y;->b:Lcom/peel/d/x;

    iget-object v1, v1, Lcom/peel/d/x;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 245
    iget-object v0, p0, Lcom/peel/d/y;->b:Lcom/peel/d/x;

    iget-object v0, v0, Lcom/peel/d/x;->f:Lcom/peel/d/v;

    invoke-static {v0}, Lcom/peel/d/v;->b(Lcom/peel/d/v;)Ljava/util/HashMap;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/d/y;->b:Lcom/peel/d/x;

    iget-object v1, v1, Lcom/peel/d/x;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 247
    :cond_0
    iget-object v0, p0, Lcom/peel/d/y;->b:Lcom/peel/d/x;

    iget-object v0, v0, Lcom/peel/d/x;->e:Lcom/peel/util/t;

    iget-boolean v1, p0, Lcom/peel/d/y;->i:Z

    iget-object v2, p0, Lcom/peel/d/y;->j:Ljava/lang/Object;

    iget-object v3, p0, Lcom/peel/d/y;->k:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3}, Lcom/peel/util/t;->a(ZLjava/lang/Object;Ljava/lang/String;)V

    .line 268
    :goto_0
    return-void

    .line 248
    :cond_1
    iget-object v0, p0, Lcom/peel/d/y;->a:Ljava/lang/String;

    const-string/jumbo v1, "https://api.twitter.com/1.1/statuses/retweet/"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/peel/d/y;->j:Ljava/lang/Object;

    if-eqz v0, :cond_2

    .line 250
    :try_start_0
    iget-object v0, p0, Lcom/peel/d/y;->b:Lcom/peel/d/x;

    iget-object v1, v0, Lcom/peel/d/x;->f:Lcom/peel/d/v;

    new-instance v2, Lorg/json/JSONObject;

    iget-object v0, p0, Lcom/peel/d/y;->j:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    invoke-direct {v2, v0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-virtual {v1, v2, v4, v5}, Lcom/peel/d/v;->a(Lorg/json/JSONObject;J)Lcom/peel/ui/model/TwittData;

    move-result-object v0

    .line 251
    iget-object v1, p0, Lcom/peel/d/y;->b:Lcom/peel/d/x;

    iget-object v1, v1, Lcom/peel/d/x;->f:Lcom/peel/d/v;

    invoke-static {v1}, Lcom/peel/d/v;->b(Lcom/peel/d/v;)Ljava/util/HashMap;

    move-result-object v1

    iget-object v2, p0, Lcom/peel/d/y;->b:Lcom/peel/d/x;

    iget-object v2, v2, Lcom/peel/d/x;->c:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/peel/ui/model/TwittData;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 252
    iget-object v1, p0, Lcom/peel/d/y;->b:Lcom/peel/d/x;

    iget-object v1, v1, Lcom/peel/d/x;->e:Lcom/peel/util/t;

    iget-boolean v2, p0, Lcom/peel/d/y;->i:Z

    invoke-virtual {v0}, Lcom/peel/ui/model/TwittData;->c()Ljava/lang/String;

    move-result-object v0

    iget-object v3, p0, Lcom/peel/d/y;->k:Ljava/lang/String;

    invoke-virtual {v1, v2, v0, v3}, Lcom/peel/util/t;->a(ZLjava/lang/Object;Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 253
    :catch_0
    move-exception v0

    .line 254
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0

    .line 257
    :cond_2
    iget-object v0, p0, Lcom/peel/d/y;->a:Ljava/lang/String;

    const-string/jumbo v1, "https://api.twitter.com/1.1/favorites/create.json"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 258
    iget-object v0, p0, Lcom/peel/d/y;->b:Lcom/peel/d/x;

    iget-object v0, v0, Lcom/peel/d/x;->f:Lcom/peel/d/v;

    invoke-static {v0}, Lcom/peel/d/v;->d(Lcom/peel/d/v;)Ljava/util/Set;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/d/y;->b:Lcom/peel/d/x;

    iget-object v1, v1, Lcom/peel/d/x;->c:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 263
    :cond_3
    :goto_1
    iget-object v0, p0, Lcom/peel/d/y;->b:Lcom/peel/d/x;

    iget-object v0, v0, Lcom/peel/d/x;->e:Lcom/peel/util/t;

    iget-boolean v1, p0, Lcom/peel/d/y;->i:Z

    iget-object v2, p0, Lcom/peel/d/y;->j:Ljava/lang/Object;

    iget-object v3, p0, Lcom/peel/d/y;->k:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3}, Lcom/peel/util/t;->a(ZLjava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 259
    :cond_4
    iget-object v0, p0, Lcom/peel/d/y;->a:Ljava/lang/String;

    const-string/jumbo v1, "https://api.twitter.com/1.1/favorites/destroy.json"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 260
    iget-object v0, p0, Lcom/peel/d/y;->b:Lcom/peel/d/x;

    iget-object v0, v0, Lcom/peel/d/x;->f:Lcom/peel/d/v;

    invoke-static {v0}, Lcom/peel/d/v;->d(Lcom/peel/d/v;)Ljava/util/Set;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/d/y;->b:Lcom/peel/d/x;

    iget-object v1, v1, Lcom/peel/d/x;->c:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    goto :goto_1

    .line 266
    :cond_5
    iget-object v0, p0, Lcom/peel/d/y;->b:Lcom/peel/d/x;

    iget-object v0, v0, Lcom/peel/d/x;->e:Lcom/peel/util/t;

    const/4 v1, 0x0

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/peel/d/y;->k:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3}, Lcom/peel/util/t;->a(ZLjava/lang/Object;Ljava/lang/String;)V

    goto/16 :goto_0
.end method
