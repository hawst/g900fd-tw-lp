.class Lcom/peel/h/a/i;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/peel/control/RoomControl;

.field final synthetic b:Lcom/peel/h/a/a;


# direct methods
.method constructor <init>(Lcom/peel/h/a/a;Lcom/peel/control/RoomControl;)V
    .locals 0

    .prologue
    .line 671
    iput-object p1, p0, Lcom/peel/h/a/i;->b:Lcom/peel/h/a/a;

    iput-object p2, p0, Lcom/peel/h/a/i;->a:Lcom/peel/control/RoomControl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 9

    .prologue
    const/4 v8, 0x1

    .line 674
    iget-object v0, p0, Lcom/peel/h/a/i;->b:Lcom/peel/h/a/a;

    invoke-static {v0}, Lcom/peel/h/a/a;->x(Lcom/peel/h/a/a;)Landroid/widget/Button;

    move-result-object v0

    invoke-static {}, Lcom/peel/h/a/a;->c()Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0x3e8

    invoke-static {v0, v1, v2}, Lcom/peel/util/bx;->a(Landroid/view/View;Ljava/lang/String;I)V

    .line 676
    iget-object v0, p0, Lcom/peel/h/a/i;->b:Lcom/peel/h/a/a;

    invoke-virtual {v0}, Lcom/peel/h/a/a;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    invoke-virtual {v0, v8}, Landroid/support/v4/app/ae;->setProgressBarIndeterminateVisibility(Z)V

    .line 677
    invoke-static {}, Lcom/peel/util/a/f;->a()Lcom/peel/util/a/f;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/h/a/i;->a:Lcom/peel/control/RoomControl;

    if-nez v1, :cond_0

    move v1, v8

    :goto_0
    const/16 v2, 0x431

    const/16 v3, 0x7d8

    iget-object v4, p0, Lcom/peel/h/a/i;->b:Lcom/peel/h/a/a;

    invoke-static {v4}, Lcom/peel/h/a/a;->n(Lcom/peel/h/a/a;)Lcom/peel/f/a;

    move-result-object v4

    invoke-virtual {v4}, Lcom/peel/f/a;->b()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/peel/h/a/i;->b:Lcom/peel/h/a/a;

    invoke-static {v5}, Lcom/peel/h/a/a;->d(Lcom/peel/h/a/a;)I

    move-result v5

    const-string/jumbo v6, ""

    iget-object v7, p0, Lcom/peel/h/a/i;->b:Lcom/peel/h/a/a;

    invoke-static {v7}, Lcom/peel/h/a/a;->w(Lcom/peel/h/a/a;)I

    move-result v7

    invoke-virtual/range {v0 .. v7}, Lcom/peel/util/a/f;->a(IIILjava/lang/String;ILjava/lang/String;I)V

    .line 679
    iget-object v0, p0, Lcom/peel/h/a/i;->b:Lcom/peel/h/a/a;

    invoke-static {v0}, Lcom/peel/h/a/a;->v(Lcom/peel/h/a/a;)Landroid/widget/TextView;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 680
    const-string/jumbo v0, "https://partners-ir.peel.com"

    iget-object v1, p0, Lcom/peel/h/a/i;->b:Lcom/peel/h/a/a;

    invoke-static {v1}, Lcom/peel/h/a/a;->w(Lcom/peel/h/a/a;)I

    move-result v1

    new-instance v2, Lcom/peel/h/a/j;

    invoke-direct {v2, p0, v8}, Lcom/peel/h/a/j;-><init>(Lcom/peel/h/a/i;I)V

    invoke-static {v0, v1, v2}, Lcom/peel/control/aa;->a(Ljava/lang/String;ILcom/peel/util/t;)V

    .line 891
    return-void

    .line 677
    :cond_0
    iget-object v1, p0, Lcom/peel/h/a/i;->a:Lcom/peel/control/RoomControl;

    invoke-virtual {v1}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/data/at;->f()I

    move-result v1

    goto :goto_0
.end method
