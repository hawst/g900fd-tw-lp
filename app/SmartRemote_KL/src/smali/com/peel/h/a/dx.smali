.class Lcom/peel/h/a/dx;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/peel/h/a/dv;


# direct methods
.method constructor <init>(Lcom/peel/h/a/dv;)V
    .locals 0

    .prologue
    .line 279
    iput-object p1, p0, Lcom/peel/h/a/dx;->a:Lcom/peel/h/a/dv;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 282
    iget-object v0, p0, Lcom/peel/h/a/dx;->a:Lcom/peel/h/a/dv;

    iget-object v0, v0, Lcom/peel/h/a/dv;->b:Lcom/peel/h/a/du;

    invoke-virtual {v0}, Lcom/peel/h/a/du;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/peel/h/a/dx;->a:Lcom/peel/h/a/dv;

    iget-object v0, v0, Lcom/peel/h/a/dv;->b:Lcom/peel/h/a/du;

    invoke-virtual {v0}, Lcom/peel/h/a/du;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/ae;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 283
    invoke-static {}, Lcom/peel/h/a/du;->c()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/h/a/dx;->a:Lcom/peel/h/a/dv;

    iget-object v1, v1, Lcom/peel/h/a/dv;->b:Lcom/peel/h/a/du;

    invoke-virtual {v1}, Lcom/peel/h/a/du;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    invoke-static {v0, v1, v7}, Lcom/peel/util/bx;->a(Ljava/lang/String;Landroid/app/Activity;Z)V

    .line 284
    iget-object v0, p0, Lcom/peel/h/a/dx;->a:Lcom/peel/h/a/dv;

    iget-object v0, v0, Lcom/peel/h/a/dv;->b:Lcom/peel/h/a/du;

    new-instance v1, Lcom/peel/widget/ag;

    iget-object v2, p0, Lcom/peel/h/a/dx;->a:Lcom/peel/h/a/dv;

    iget-object v2, v2, Lcom/peel/h/a/dv;->b:Lcom/peel/h/a/du;

    invoke-virtual {v2}, Lcom/peel/h/a/du;->m()Landroid/support/v4/app/ae;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/peel/widget/ag;-><init>(Landroid/content/Context;)V

    sget v2, Lcom/peel/ui/ft;->confirm_title:I

    invoke-virtual {v1, v2}, Lcom/peel/widget/ag;->a(I)Lcom/peel/widget/ag;

    move-result-object v1

    sget v2, Lcom/peel/ui/ft;->feedback_confirm_msg:I

    invoke-virtual {v1, v2}, Lcom/peel/widget/ag;->b(I)Lcom/peel/widget/ag;

    move-result-object v1

    sget v2, Lcom/peel/ui/ft;->okay:I

    new-instance v3, Lcom/peel/h/a/dy;

    invoke-direct {v3, p0}, Lcom/peel/h/a/dy;-><init>(Lcom/peel/h/a/dx;)V

    invoke-virtual {v1, v2, v3}, Lcom/peel/widget/ag;->a(ILandroid/content/DialogInterface$OnClickListener;)Lcom/peel/widget/ag;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/peel/h/a/du;->a(Lcom/peel/h/a/du;Lcom/peel/widget/ag;)Lcom/peel/widget/ag;

    .line 290
    iget-object v0, p0, Lcom/peel/h/a/dx;->a:Lcom/peel/h/a/dv;

    iget-object v0, v0, Lcom/peel/h/a/dv;->b:Lcom/peel/h/a/du;

    invoke-static {v0}, Lcom/peel/h/a/du;->j(Lcom/peel/h/a/du;)Landroid/widget/ViewFlipper;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ViewFlipper;->setDisplayedChild(I)V

    .line 291
    iget-object v0, p0, Lcom/peel/h/a/dx;->a:Lcom/peel/h/a/dv;

    iget-object v6, v0, Lcom/peel/h/a/dv;->b:Lcom/peel/h/a/du;

    new-instance v0, Lcom/peel/d/a;

    sget-object v1, Lcom/peel/d/d;->b:Lcom/peel/d/d;

    sget-object v2, Lcom/peel/d/b;->a:Lcom/peel/d/b;

    sget-object v3, Lcom/peel/d/c;->b:Lcom/peel/d/c;

    iget-object v4, p0, Lcom/peel/h/a/dx;->a:Lcom/peel/h/a/dv;

    iget-object v4, v4, Lcom/peel/h/a/dv;->b:Lcom/peel/h/a/du;

    iget-object v4, v4, Lcom/peel/h/a/du;->c:Landroid/os/Bundle;

    const-string/jumbo v5, "category"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, Lcom/peel/d/a;-><init>(Lcom/peel/d/d;Lcom/peel/d/b;Lcom/peel/d/c;Ljava/lang/String;Ljava/util/List;)V

    iput-object v0, v6, Lcom/peel/h/a/du;->d:Lcom/peel/d/a;

    .line 292
    iget-object v0, p0, Lcom/peel/h/a/dx;->a:Lcom/peel/h/a/dv;

    iget-object v0, v0, Lcom/peel/h/a/dv;->b:Lcom/peel/h/a/du;

    invoke-virtual {v0}, Lcom/peel/h/a/du;->Z()V

    .line 294
    :cond_0
    iget-object v0, p0, Lcom/peel/h/a/dx;->a:Lcom/peel/h/a/dv;

    iget-object v0, v0, Lcom/peel/h/a/dv;->b:Lcom/peel/h/a/du;

    invoke-static {v0, v7}, Lcom/peel/h/a/du;->a(Lcom/peel/h/a/du;Z)Z

    .line 295
    return-void
.end method
