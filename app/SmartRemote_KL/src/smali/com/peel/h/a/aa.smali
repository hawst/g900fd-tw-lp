.class Lcom/peel/h/a/aa;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field final synthetic a:Lcom/peel/control/RoomControl;

.field final synthetic b:Lcom/peel/h/a/a;


# direct methods
.method constructor <init>(Lcom/peel/h/a/a;Lcom/peel/control/RoomControl;)V
    .locals 0

    .prologue
    .line 418
    iput-object p1, p0, Lcom/peel/h/a/aa;->b:Lcom/peel/h/a/a;

    iput-object p2, p0, Lcom/peel/h/a/aa;->a:Lcom/peel/control/RoomControl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    .line 421
    iget-object v0, p0, Lcom/peel/h/a/aa;->b:Lcom/peel/h/a/a;

    invoke-virtual {v0}, Lcom/peel/h/a/a;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    const-string/jumbo v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/ae;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 422
    iget-object v1, p0, Lcom/peel/h/a/aa;->b:Lcom/peel/h/a/a;

    invoke-static {v1}, Lcom/peel/h/a/a;->j(Lcom/peel/h/a/a;)Landroid/widget/AutoCompleteTextView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/AutoCompleteTextView;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 423
    iget-object v0, p0, Lcom/peel/h/a/aa;->b:Lcom/peel/h/a/a;

    invoke-static {v0}, Lcom/peel/h/a/a;->i(Lcom/peel/h/a/a;)Landroid/widget/ListView;

    move-result-object v0

    invoke-static {}, Lcom/peel/h/a/a;->c()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/peel/util/bx;->a(Landroid/view/View;Ljava/lang/String;)V

    .line 425
    iget-object v1, p0, Lcom/peel/h/a/aa;->b:Lcom/peel/h/a/a;

    invoke-virtual {p1}, Landroid/widget/AdapterView;->getAdapter()Landroid/widget/Adapter;

    move-result-object v0

    invoke-interface {v0, p3}, Landroid/widget/Adapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/f/a;

    invoke-static {v1, v0}, Lcom/peel/h/a/a;->a(Lcom/peel/h/a/a;Lcom/peel/f/a;)Lcom/peel/f/a;

    .line 426
    iget-object v0, p0, Lcom/peel/h/a/aa;->b:Lcom/peel/h/a/a;

    invoke-static {v0}, Lcom/peel/h/a/a;->k(Lcom/peel/h/a/a;)Lcom/peel/i/a/a;

    move-result-object v0

    invoke-virtual {v0, p3}, Lcom/peel/i/a/a;->b(I)V

    .line 427
    iget-object v0, p0, Lcom/peel/h/a/aa;->b:Lcom/peel/h/a/a;

    iget-object v1, p0, Lcom/peel/h/a/aa;->a:Lcom/peel/control/RoomControl;

    invoke-static {v0, v1}, Lcom/peel/h/a/a;->a(Lcom/peel/h/a/a;Lcom/peel/control/RoomControl;)Lcom/peel/control/RoomControl;

    .line 428
    iget-object v0, p0, Lcom/peel/h/a/aa;->b:Lcom/peel/h/a/a;

    iget-object v1, p0, Lcom/peel/h/a/aa;->b:Lcom/peel/h/a/a;

    invoke-virtual {v1}, Lcom/peel/h/a/a;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    iget-object v2, p0, Lcom/peel/h/a/aa;->b:Lcom/peel/h/a/a;

    invoke-static {v2}, Lcom/peel/h/a/a;->d(Lcom/peel/h/a/a;)I

    move-result v2

    invoke-static {v1, v2}, Lcom/peel/util/bx;->c(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v3, v3}, Lcom/peel/h/a/a;->a(Lcom/peel/h/a/a;Ljava/lang/String;ZZ)V

    .line 429
    return-void
.end method
