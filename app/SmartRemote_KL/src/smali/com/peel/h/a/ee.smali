.class public Lcom/peel/h/a/ee;
.super Landroid/widget/ArrayAdapter;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Landroid/util/Pair",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/String;",
        ">;>;"
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private c:Landroid/view/LayoutInflater;

.field private d:Lcom/peel/control/h;

.field private e:Lcom/peel/d/i;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21
    const-class v0, Lcom/peel/h/a/ee;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/peel/h/a/ee;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;ILjava/util/List;Lcom/peel/control/h;Lcom/peel/d/i;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Ljava/util/List",
            "<",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;",
            "Lcom/peel/control/h;",
            "Lcom/peel/d/i;",
            ")V"
        }
    .end annotation

    .prologue
    .line 29
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 30
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/h/a/ee;->c:Landroid/view/LayoutInflater;

    .line 31
    iput-object p3, p0, Lcom/peel/h/a/ee;->b:Ljava/util/List;

    .line 32
    iput-object p4, p0, Lcom/peel/h/a/ee;->d:Lcom/peel/control/h;

    .line 33
    iput-object p5, p0, Lcom/peel/h/a/ee;->e:Lcom/peel/d/i;

    .line 34
    return-void
.end method

.method static synthetic a(Lcom/peel/h/a/ee;)Lcom/peel/control/h;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lcom/peel/h/a/ee;->d:Lcom/peel/control/h;

    return-object v0
.end method

.method static synthetic a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 20
    sget-object v0, Lcom/peel/h/a/ee;->a:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic b(Lcom/peel/h/a/ee;)Ljava/util/List;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lcom/peel/h/a/ee;->b:Ljava/util/List;

    return-object v0
.end method

.method static synthetic c(Lcom/peel/h/a/ee;)Lcom/peel/d/i;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lcom/peel/h/a/ee;->e:Lcom/peel/d/i;

    return-object v0
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 38
    if-nez p2, :cond_0

    iget-object v0, p0, Lcom/peel/h/a/ee;->c:Landroid/view/LayoutInflater;

    sget v1, Lcom/peel/ui/fq;->ir_learning_row:I

    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 40
    :cond_0
    sget v0, Lcom/peel/ui/fp;->cmd:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v1, p0, Lcom/peel/h/a/ee;->b:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/util/Pair;

    iget-object v1, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 43
    sget v0, Lcom/peel/ui/fp;->learn_btn:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/peel/h/a/ef;

    invoke-direct {v1, p0, p1}, Lcom/peel/h/a/ef;-><init>(Lcom/peel/h/a/ee;I)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 54
    sget v0, Lcom/peel/ui/fp;->test_btn:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/peel/h/a/eg;

    invoke-direct {v1, p0, p1}, Lcom/peel/h/a/eg;-><init>(Lcom/peel/h/a/ee;I)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 71
    iget-object v0, p0, Lcom/peel/h/a/ee;->d:Lcom/peel/control/h;

    invoke-virtual {v0}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/data/g;->a()Ljava/util/Map;

    move-result-object v1

    iget-object v0, p0, Lcom/peel/h/a/ee;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    iget-object v0, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    invoke-interface {v1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 72
    sget v0, Lcom/peel/ui/fp;->test_btn:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setEnabled(Z)V

    .line 78
    :goto_0
    return-object p2

    .line 75
    :cond_1
    sget v0, Lcom/peel/ui/fp;->test_btn:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    goto :goto_0
.end method
