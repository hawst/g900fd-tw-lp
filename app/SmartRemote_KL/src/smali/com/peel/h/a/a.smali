.class public Lcom/peel/h/a/a;
.super Lcom/peel/d/u;


# static fields
.field private static final e:Ljava/lang/String;


# instance fields
.field private aA:Ljava/lang/String;

.field private aB:Landroid/widget/Button;

.field private aC:Landroid/widget/Button;

.field private aD:Z

.field private aE:Z

.field private aF:Z

.field private aG:Landroid/widget/AutoCompleteTextView;

.field private aH:Landroid/widget/ImageView;

.field private aI:Landroid/widget/ImageView;

.field private aJ:Landroid/widget/RelativeLayout;

.field private aK:Lcom/peel/ui/dg;

.field private aL:Lcom/peel/widget/ag;

.field private aM:Lcom/peel/widget/ag;

.field private aN:Landroid/widget/TextView;

.field private aO:Landroid/widget/TextView;

.field private aP:Landroid/widget/TextView;

.field private aQ:Landroid/widget/ImageView;

.field private aR:Landroid/widget/TextView;

.field private aS:Landroid/widget/RelativeLayout;

.field private aT:Landroid/widget/RelativeLayout;

.field private aU:Lcom/peel/widget/TestBtnViewPager;

.field private aV:Landroid/widget/Button;

.field private aW:Landroid/widget/Button;

.field private aX:Lcom/peel/h/a/ah;

.field private aY:I

.field private aZ:Lcom/peel/control/RoomControl;

.field private aj:I

.field private ak:I

.field private al:Lcom/peel/control/a;

.field private am:Lcom/peel/f/a;

.field private an:Landroid/widget/ListView;

.field private ao:Landroid/widget/ListView;

.field private ap:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/peel/f/a;",
            ">;"
        }
    .end annotation
.end field

.field private aq:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/peel/f/a;",
            ">;"
        }
    .end annotation
.end field

.field private ar:Lcom/peel/control/h;

.field private as:Lcom/peel/control/h;

.field private at:Lcom/peel/control/h;

.field private au:Lcom/peel/widget/ag;

.field private av:Lcom/peel/widget/ag;

.field private aw:Lcom/peel/i/a/a;

.field private ax:Lcom/peel/i/a/a;

.field private ay:Ljava/lang/String;

.field private az:Ljava/lang/String;

.field private ba:Lcom/peel/control/RoomControl;

.field private f:Landroid/widget/ViewFlipper;

.field private g:I

.field private h:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;>;"
        }
    .end annotation
.end field

.field private i:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 83
    const-class v0, Lcom/peel/h/a/a;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/peel/h/a/a;->e:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 123
    invoke-direct {p0}, Lcom/peel/d/u;-><init>()V

    .line 89
    const/4 v0, -0x1

    iput v0, p0, Lcom/peel/h/a/a;->g:I

    .line 96
    iput-object v1, p0, Lcom/peel/h/a/a;->as:Lcom/peel/control/h;

    iput-object v1, p0, Lcom/peel/h/a/a;->at:Lcom/peel/control/h;

    .line 101
    iput-boolean v2, p0, Lcom/peel/h/a/a;->aE:Z

    iput-boolean v2, p0, Lcom/peel/h/a/a;->aF:Z

    .line 106
    iput-object v1, p0, Lcom/peel/h/a/a;->aK:Lcom/peel/ui/dg;

    .line 107
    iput-object v1, p0, Lcom/peel/h/a/a;->aL:Lcom/peel/widget/ag;

    iput-object v1, p0, Lcom/peel/h/a/a;->aM:Lcom/peel/widget/ag;

    .line 119
    iput v2, p0, Lcom/peel/h/a/a;->aY:I

    .line 124
    iput-boolean v2, p0, Lcom/peel/h/a/a;->aD:Z

    .line 125
    return-void
.end method

.method static synthetic A(Lcom/peel/h/a/a;)Landroid/widget/RelativeLayout;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/peel/h/a/a;->aT:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method static synthetic B(Lcom/peel/h/a/a;)Landroid/widget/RelativeLayout;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/peel/h/a/a;->aS:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method static synthetic C(Lcom/peel/h/a/a;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/peel/h/a/a;->aN:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic D(Lcom/peel/h/a/a;)Landroid/widget/Button;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/peel/h/a/a;->aV:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic E(Lcom/peel/h/a/a;)Landroid/widget/Button;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/peel/h/a/a;->aW:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic F(Lcom/peel/h/a/a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/peel/h/a/a;->ay:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic G(Lcom/peel/h/a/a;)Lcom/peel/h/a/ah;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/peel/h/a/a;->aX:Lcom/peel/h/a/ah;

    return-object v0
.end method

.method static synthetic H(Lcom/peel/h/a/a;)I
    .locals 1

    .prologue
    .line 82
    iget v0, p0, Lcom/peel/h/a/a;->aY:I

    return v0
.end method

.method static synthetic I(Lcom/peel/h/a/a;)I
    .locals 1

    .prologue
    .line 82
    iget v0, p0, Lcom/peel/h/a/a;->aj:I

    return v0
.end method

.method static synthetic J(Lcom/peel/h/a/a;)V
    .locals 0

    .prologue
    .line 82
    invoke-direct {p0}, Lcom/peel/h/a/a;->S()V

    return-void
.end method

.method static synthetic K(Lcom/peel/h/a/a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/peel/h/a/a;->aA:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic L(Lcom/peel/h/a/a;)Lcom/peel/widget/ag;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/peel/h/a/a;->av:Lcom/peel/widget/ag;

    return-object v0
.end method

.method static synthetic M(Lcom/peel/h/a/a;)Lcom/peel/widget/ag;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/peel/h/a/a;->au:Lcom/peel/widget/ag;

    return-object v0
.end method

.method static synthetic N(Lcom/peel/h/a/a;)V
    .locals 0

    .prologue
    .line 82
    invoke-direct {p0}, Lcom/peel/h/a/a;->V()V

    return-void
.end method

.method private S()V
    .locals 5

    .prologue
    .line 1045
    iget-object v1, p0, Lcom/peel/h/a/a;->aO:Landroid/widget/TextView;

    iget v0, p0, Lcom/peel/h/a/a;->aj:I

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1046
    return-void

    .line 1045
    :cond_0
    sget v0, Lcom/peel/ui/ft;->ir_send_code:I

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget v4, p0, Lcom/peel/h/a/a;->aj:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {p0, v0, v2}, Lcom/peel/h/a/a;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private T()V
    .locals 3

    .prologue
    .line 1219
    iget-object v0, p0, Lcom/peel/h/a/a;->av:Lcom/peel/widget/ag;

    if-nez v0, :cond_0

    .line 1220
    new-instance v0, Lcom/peel/widget/ag;

    invoke-virtual {p0}, Lcom/peel/h/a/a;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/peel/widget/ag;-><init>(Landroid/content/Context;)V

    sget v1, Lcom/peel/ui/ft;->warning:I

    invoke-virtual {v0, v1}, Lcom/peel/widget/ag;->a(I)Lcom/peel/widget/ag;

    move-result-object v0

    sget v1, Lcom/peel/ui/ft;->label_no_codes_found:I

    invoke-virtual {v0, v1}, Lcom/peel/widget/ag;->b(I)Lcom/peel/widget/ag;

    move-result-object v0

    sget v1, Lcom/peel/ui/ft;->label_report:I

    new-instance v2, Lcom/peel/h/a/q;

    invoke-direct {v2, p0}, Lcom/peel/h/a/q;-><init>(Lcom/peel/h/a/a;)V

    invoke-virtual {v0, v1, v2}, Lcom/peel/widget/ag;->a(ILandroid/content/DialogInterface$OnClickListener;)Lcom/peel/widget/ag;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/h/a/a;->av:Lcom/peel/widget/ag;

    .line 1228
    :cond_0
    const-class v0, Lcom/peel/h/a/a;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "showNoCodesFoundDialog"

    new-instance v2, Lcom/peel/h/a/r;

    invoke-direct {v2, p0}, Lcom/peel/h/a/r;-><init>(Lcom/peel/h/a/a;)V

    invoke-static {v0, v1, v2}, Lcom/peel/util/i;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/lang/Runnable;

    .line 1234
    return-void
.end method

.method private U()V
    .locals 3

    .prologue
    .line 1237
    iget-object v0, p0, Lcom/peel/h/a/a;->au:Lcom/peel/widget/ag;

    if-nez v0, :cond_1

    .line 1238
    new-instance v0, Lcom/peel/widget/ag;

    invoke-virtual {p0}, Lcom/peel/h/a/a;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/peel/widget/ag;-><init>(Landroid/content/Context;)V

    invoke-virtual {p0}, Lcom/peel/h/a/a;->n()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/peel/ui/ft;->no_internet:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/peel/widget/ag;->a(Ljava/lang/CharSequence;)Lcom/peel/widget/ag;

    move-result-object v0

    invoke-virtual {p0}, Lcom/peel/h/a/a;->n()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/peel/ui/ft;->no_internet_alert:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/peel/widget/ag;->b(Ljava/lang/CharSequence;)Lcom/peel/widget/ag;

    move-result-object v0

    sget v1, Lcom/peel/ui/ft;->label_settings:I

    new-instance v2, Lcom/peel/h/a/t;

    invoke-direct {v2, p0}, Lcom/peel/h/a/t;-><init>(Lcom/peel/h/a/a;)V

    invoke-virtual {v0, v1, v2}, Lcom/peel/widget/ag;->c(ILandroid/content/DialogInterface$OnClickListener;)Lcom/peel/widget/ag;

    move-result-object v0

    sget v1, Lcom/peel/ui/ft;->ok:I

    new-instance v2, Lcom/peel/h/a/s;

    invoke-direct {v2, p0}, Lcom/peel/h/a/s;-><init>(Lcom/peel/h/a/a;)V

    .line 1243
    invoke-virtual {v0, v1, v2}, Lcom/peel/widget/ag;->a(ILandroid/content/DialogInterface$OnClickListener;)Lcom/peel/widget/ag;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/h/a/a;->au:Lcom/peel/widget/ag;

    .line 1252
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/peel/h/a/a;->au:Lcom/peel/widget/ag;

    invoke-virtual {v0}, Lcom/peel/widget/ag;->show()V

    .line 1253
    return-void

    .line 1249
    :cond_1
    iget-object v0, p0, Lcom/peel/h/a/a;->au:Lcom/peel/widget/ag;

    invoke-virtual {v0}, Lcom/peel/widget/ag;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1250
    iget-object v0, p0, Lcom/peel/h/a/a;->au:Lcom/peel/widget/ag;

    invoke-virtual {v0}, Lcom/peel/widget/ag;->dismiss()V

    goto :goto_0
.end method

.method private V()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1265
    iget-object v0, p0, Lcom/peel/h/a/a;->ar:Lcom/peel/control/h;

    if-eqz v0, :cond_0

    .line 1266
    iget-object v0, p0, Lcom/peel/h/a/a;->ar:Lcom/peel/control/h;

    iget-object v1, p0, Lcom/peel/h/a/a;->ay:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/peel/control/h;->c(Ljava/lang/String;)Z

    .line 1268
    sget-object v0, Lcom/peel/h/a/a;->e:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/peel/h/a/a;->az:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/peel/h/a/a;->ay:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " cmd pressed codeIdx:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/peel/h/a/a;->aj:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "/codesetId:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/peel/h/a/a;->i:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1270
    iget-object v0, p0, Lcom/peel/h/a/a;->aO:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1271
    iget-object v0, p0, Lcom/peel/h/a/a;->aO:Landroid/widget/TextView;

    sget v1, Lcom/peel/ui/ft;->ir_send_code:I

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    iget v3, p0, Lcom/peel/h/a/a;->aj:I

    add-int/lit8 v3, v3, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {p0, v1, v2}, Lcom/peel/h/a/a;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1272
    iget-object v0, p0, Lcom/peel/h/a/a;->aT:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v4}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1273
    iget-object v0, p0, Lcom/peel/h/a/a;->aS:Landroid/widget/RelativeLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1275
    :cond_0
    return-void
.end method

.method static synthetic a(Lcom/peel/h/a/a;I)I
    .locals 0

    .prologue
    .line 82
    iput p1, p0, Lcom/peel/h/a/a;->ak:I

    return p1
.end method

.method static synthetic a(Lcom/peel/h/a/a;)Landroid/widget/ListView;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/peel/h/a/a;->an:Landroid/widget/ListView;

    return-object v0
.end method

.method static synthetic a(Lcom/peel/h/a/a;Lcom/peel/control/RoomControl;)Lcom/peel/control/RoomControl;
    .locals 0

    .prologue
    .line 82
    iput-object p1, p0, Lcom/peel/h/a/a;->aZ:Lcom/peel/control/RoomControl;

    return-object p1
.end method

.method static synthetic a(Lcom/peel/h/a/a;Lcom/peel/control/a;)Lcom/peel/control/a;
    .locals 0

    .prologue
    .line 82
    iput-object p1, p0, Lcom/peel/h/a/a;->al:Lcom/peel/control/a;

    return-object p1
.end method

.method static synthetic a(Lcom/peel/h/a/a;Lcom/peel/f/a;)Lcom/peel/f/a;
    .locals 0

    .prologue
    .line 82
    iput-object p1, p0, Lcom/peel/h/a/a;->am:Lcom/peel/f/a;

    return-object p1
.end method

.method static synthetic a(Lcom/peel/h/a/a;Lcom/peel/h/a/ah;)Lcom/peel/h/a/ah;
    .locals 0

    .prologue
    .line 82
    iput-object p1, p0, Lcom/peel/h/a/a;->aX:Lcom/peel/h/a/ah;

    return-object p1
.end method

.method static synthetic a(Lcom/peel/h/a/a;Lcom/peel/i/a/a;)Lcom/peel/i/a/a;
    .locals 0

    .prologue
    .line 82
    iput-object p1, p0, Lcom/peel/h/a/a;->aw:Lcom/peel/i/a/a;

    return-object p1
.end method

.method static synthetic a(Lcom/peel/h/a/a;Lcom/peel/ui/dg;)Lcom/peel/ui/dg;
    .locals 0

    .prologue
    .line 82
    iput-object p1, p0, Lcom/peel/h/a/a;->aK:Lcom/peel/ui/dg;

    return-object p1
.end method

.method static synthetic a(Lcom/peel/h/a/a;Lcom/peel/widget/ag;)Lcom/peel/widget/ag;
    .locals 0

    .prologue
    .line 82
    iput-object p1, p0, Lcom/peel/h/a/a;->aL:Lcom/peel/widget/ag;

    return-object p1
.end method

.method static synthetic a(Lcom/peel/h/a/a;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 82
    iput-object p1, p0, Lcom/peel/h/a/a;->az:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic a(Lcom/peel/h/a/a;Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 0

    .prologue
    .line 82
    iput-object p1, p0, Lcom/peel/h/a/a;->h:Ljava/util/ArrayList;

    return-object p1
.end method

.method static synthetic a(Lcom/peel/h/a/a;Ljava/util/List;)Ljava/util/List;
    .locals 0

    .prologue
    .line 82
    iput-object p1, p0, Lcom/peel/h/a/a;->ap:Ljava/util/List;

    return-object p1
.end method

.method private a(Lcom/peel/control/RoomControl;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v2, 0x0

    .line 279
    iget-object v0, p0, Lcom/peel/h/a/a;->f:Landroid/widget/ViewFlipper;

    invoke-virtual {v0, v2}, Landroid/widget/ViewFlipper;->setDisplayedChild(I)V

    .line 280
    iput v2, p0, Lcom/peel/h/a/a;->g:I

    .line 281
    invoke-virtual {p0}, Lcom/peel/h/a/a;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    iget v1, p0, Lcom/peel/h/a/a;->ak:I

    invoke-static {v0, v1}, Lcom/peel/util/bx;->c(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, v4, v2}, Lcom/peel/h/a/a;->a(Ljava/lang/String;ZZ)V

    .line 283
    invoke-virtual {p0}, Lcom/peel/h/a/a;->v()Landroid/view/View;

    move-result-object v0

    sget v1, Lcom/peel/ui/fp;->brand_list:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/peel/h/a/a;->an:Landroid/widget/ListView;

    .line 285
    iget-object v0, p0, Lcom/peel/h/a/a;->an:Landroid/widget/ListView;

    new-instance v1, Lcom/peel/h/a/p;

    invoke-direct {v1, p0, p1}, Lcom/peel/h/a/p;-><init>(Lcom/peel/h/a/a;Lcom/peel/control/RoomControl;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 296
    iget-object v0, p0, Lcom/peel/h/a/a;->f:Landroid/widget/ViewFlipper;

    sget v1, Lcom/peel/ui/fp;->other_brand_btn:I

    invoke-virtual {v0, v1}, Landroid/widget/ViewFlipper;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    sget v1, Lcom/peel/ui/ft;->other_device_brand:I

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    .line 297
    iget-object v0, p0, Lcom/peel/h/a/a;->f:Landroid/widget/ViewFlipper;

    sget v1, Lcom/peel/ui/fp;->other_brand_btn:I

    invoke-virtual {v0, v1}, Landroid/widget/ViewFlipper;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/peel/h/a/w;

    invoke-direct {v1, p0, p1}, Lcom/peel/h/a/w;-><init>(Lcom/peel/h/a/a;Lcom/peel/control/RoomControl;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 304
    iget-object v0, p0, Lcom/peel/h/a/a;->ap:Ljava/util/List;

    if-nez v0, :cond_1

    .line 305
    const-class v0, Lcom/peel/h/a/a;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/peel/h/a/a;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    invoke-static {v0, v1, v4}, Lcom/peel/util/bx;->a(Ljava/lang/String;Landroid/app/Activity;Z)V

    .line 306
    invoke-virtual {p0}, Lcom/peel/h/a/a;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string/jumbo v1, "country_ISO"

    const-string/jumbo v2, "US"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 307
    const-string/jumbo v1, "https://partners-ir.peel.com"

    iget v2, p0, Lcom/peel/h/a/a;->ak:I

    new-instance v3, Lcom/peel/h/a/x;

    invoke-direct {v3, p0, v4, p1}, Lcom/peel/h/a/x;-><init>(Lcom/peel/h/a/a;ILcom/peel/control/RoomControl;)V

    invoke-static {v1, v2, v0, v3}, Lcom/peel/control/aa;->a(Ljava/lang/String;ILjava/lang/String;Lcom/peel/util/t;)V

    .line 348
    :cond_0
    :goto_0
    return-void

    .line 343
    :cond_1
    new-instance v0, Lcom/peel/i/a/a;

    invoke-virtual {p0}, Lcom/peel/h/a/a;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    sget v2, Lcom/peel/ui/fq;->brand_row:I

    iget-object v3, p0, Lcom/peel/h/a/a;->ap:Ljava/util/List;

    invoke-direct {v0, v1, v2, v3}, Lcom/peel/i/a/a;-><init>(Landroid/content/Context;ILjava/util/List;)V

    iput-object v0, p0, Lcom/peel/h/a/a;->aw:Lcom/peel/i/a/a;

    .line 344
    iget-object v0, p0, Lcom/peel/h/a/a;->an:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/peel/h/a/a;->aw:Lcom/peel/i/a/a;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 345
    iget-object v0, p0, Lcom/peel/h/a/a;->ap:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_0

    .line 346
    invoke-direct {p0, p1}, Lcom/peel/h/a/a;->b(Lcom/peel/control/RoomControl;)V

    goto :goto_0
.end method

.method private a(Lcom/peel/control/a;Lcom/peel/control/RoomControl;)V
    .locals 4

    .prologue
    const/4 v3, 0x5

    .line 1278
    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v0}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v0

    .line 1279
    if-eqz p2, :cond_0

    if-eqz v0, :cond_0

    .line 1280
    invoke-virtual {p2}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/data/at;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v2

    invoke-virtual {v2}, Lcom/peel/data/at;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1281
    iget-boolean v1, p0, Lcom/peel/h/a/a;->aF:Z

    if-eqz v1, :cond_1

    .line 1282
    iget v1, p0, Lcom/peel/h/a/a;->ak:I

    if-eq v1, v3, :cond_0

    .line 1283
    const-class v1, Lcom/peel/h/a/a;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "start activity "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Lcom/peel/control/a;->c()Lcom/peel/data/d;

    move-result-object v3

    invoke-virtual {v3}, Lcom/peel/data/d;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/peel/h/a/u;

    invoke-direct {v3, p0, v0, p1}, Lcom/peel/h/a/u;-><init>(Lcom/peel/h/a/a;Lcom/peel/control/RoomControl;Lcom/peel/control/a;)V

    invoke-static {v1, v2, v3}, Lcom/peel/util/i;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/lang/Runnable;

    .line 1304
    :cond_0
    :goto_0
    return-void

    .line 1292
    :cond_1
    iget v1, p0, Lcom/peel/h/a/a;->ak:I

    const/4 v2, 0x1

    if-eq v1, v2, :cond_0

    iget v1, p0, Lcom/peel/h/a/a;->ak:I

    if-eq v1, v3, :cond_0

    .line 1293
    const-class v1, Lcom/peel/h/a/a;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "start activity "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Lcom/peel/control/a;->c()Lcom/peel/data/d;

    move-result-object v3

    invoke-virtual {v3}, Lcom/peel/data/d;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/peel/h/a/v;

    invoke-direct {v3, p0, v0, p1}, Lcom/peel/h/a/v;-><init>(Lcom/peel/h/a/a;Lcom/peel/control/RoomControl;Lcom/peel/control/a;)V

    invoke-static {v1, v2, v3}, Lcom/peel/util/i;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/lang/Runnable;

    goto :goto_0
.end method

.method static synthetic a(Lcom/peel/h/a/a;Lcom/peel/control/a;Lcom/peel/control/RoomControl;)V
    .locals 0

    .prologue
    .line 82
    invoke-direct {p0, p1, p2}, Lcom/peel/h/a/a;->a(Lcom/peel/control/a;Lcom/peel/control/RoomControl;)V

    return-void
.end method

.method static synthetic a(Lcom/peel/h/a/a;Ljava/lang/String;ZZ)V
    .locals 0

    .prologue
    .line 82
    invoke-direct {p0, p1, p2, p3}, Lcom/peel/h/a/a;->a(Ljava/lang/String;ZZ)V

    return-void
.end method

.method private a(Ljava/lang/String;ZZ)V
    .locals 7

    .prologue
    .line 1153
    const/4 v6, 0x0

    .line 1154
    if-eqz p2, :cond_0

    .line 1155
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 1156
    sget v0, Lcom/peel/ui/fp;->menu_next:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1158
    :cond_0
    new-instance v0, Lcom/peel/d/a;

    sget-object v1, Lcom/peel/d/d;->b:Lcom/peel/d/d;

    sget-object v2, Lcom/peel/d/b;->a:Lcom/peel/d/b;

    sget-object v3, Lcom/peel/d/c;->b:Lcom/peel/d/c;

    move-object v4, p1

    move v5, p3

    invoke-direct/range {v0 .. v6}, Lcom/peel/d/a;-><init>(Lcom/peel/d/d;Lcom/peel/d/b;Lcom/peel/d/c;Ljava/lang/String;ZLjava/util/List;)V

    iput-object v0, p0, Lcom/peel/h/a/a;->d:Lcom/peel/d/a;

    .line 1159
    invoke-virtual {p0}, Lcom/peel/h/a/a;->Z()V

    .line 1160
    return-void
.end method

.method static synthetic a(Lcom/peel/h/a/a;Z)Z
    .locals 0

    .prologue
    .line 82
    iput-boolean p1, p0, Lcom/peel/h/a/a;->aE:Z

    return p1
.end method

.method static synthetic b(Lcom/peel/h/a/a;I)I
    .locals 0

    .prologue
    .line 82
    iput p1, p0, Lcom/peel/h/a/a;->aj:I

    return p1
.end method

.method static synthetic b(Lcom/peel/h/a/a;)Lcom/peel/i/a/a;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/peel/h/a/a;->aw:Lcom/peel/i/a/a;

    return-object v0
.end method

.method static synthetic b(Lcom/peel/h/a/a;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 82
    iput-object p1, p0, Lcom/peel/h/a/a;->ay:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic b(Lcom/peel/h/a/a;Ljava/util/List;)Ljava/util/List;
    .locals 0

    .prologue
    .line 82
    iput-object p1, p0, Lcom/peel/h/a/a;->aq:Ljava/util/List;

    return-object p1
.end method

.method private b(Lcom/peel/control/RoomControl;)V
    .locals 5

    .prologue
    const/16 v2, 0x8

    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 351
    iget-object v0, p0, Lcom/peel/h/a/a;->f:Landroid/widget/ViewFlipper;

    invoke-virtual {v0, v4}, Landroid/widget/ViewFlipper;->setDisplayedChild(I)V

    .line 352
    iput v4, p0, Lcom/peel/h/a/a;->g:I

    .line 353
    invoke-virtual {p0}, Lcom/peel/h/a/a;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    iget v3, p0, Lcom/peel/h/a/a;->ak:I

    invoke-static {v0, v3}, Lcom/peel/util/bx;->c(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, v4, v1}, Lcom/peel/h/a/a;->a(Ljava/lang/String;ZZ)V

    .line 355
    iget-object v0, p0, Lcom/peel/h/a/a;->aG:Landroid/widget/AutoCompleteTextView;

    if-nez v0, :cond_2

    .line 356
    invoke-virtual {p0}, Lcom/peel/h/a/a;->v()Landroid/view/View;

    move-result-object v0

    sget v3, Lcom/peel/ui/fp;->search_other_list_filter:I

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/AutoCompleteTextView;

    iput-object v0, p0, Lcom/peel/h/a/a;->aG:Landroid/widget/AutoCompleteTextView;

    .line 360
    :goto_0
    invoke-virtual {p0}, Lcom/peel/h/a/a;->v()Landroid/view/View;

    move-result-object v0

    sget v3, Lcom/peel/ui/fp;->search_icon:I

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/peel/h/a/a;->aH:Landroid/widget/ImageView;

    .line 361
    iget-object v0, p0, Lcom/peel/h/a/a;->aG:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v0, v2}, Landroid/widget/AutoCompleteTextView;->setVisibility(I)V

    .line 362
    iget-object v0, p0, Lcom/peel/h/a/a;->aH:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 364
    iget-object v0, p0, Lcom/peel/h/a/a;->aq:Ljava/util/List;

    if-eqz v0, :cond_1

    .line 365
    invoke-virtual {p0}, Lcom/peel/h/a/a;->v()Landroid/view/View;

    move-result-object v0

    sget v3, Lcom/peel/ui/fp;->other_list:I

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/peel/h/a/a;->ao:Landroid/widget/ListView;

    .line 366
    invoke-virtual {p0}, Lcom/peel/h/a/a;->v()Landroid/view/View;

    move-result-object v0

    sget v3, Lcom/peel/ui/fp;->search_cancel_btn_other:I

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/peel/h/a/a;->aI:Landroid/widget/ImageView;

    .line 367
    invoke-virtual {p0}, Lcom/peel/h/a/a;->v()Landroid/view/View;

    move-result-object v0

    sget v3, Lcom/peel/ui/fp;->search_layout_other:I

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/peel/h/a/a;->aJ:Landroid/widget/RelativeLayout;

    .line 368
    iget-object v0, p0, Lcom/peel/h/a/a;->aG:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v0, v1}, Landroid/widget/AutoCompleteTextView;->setVisibility(I)V

    .line 369
    iget-object v0, p0, Lcom/peel/h/a/a;->aH:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 370
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v0

    .line 371
    const-string/jumbo v3, "iw"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    const-string/jumbo v3, "ar"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 372
    :cond_0
    iget-object v0, p0, Lcom/peel/h/a/a;->aG:Landroid/widget/AutoCompleteTextView;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget v4, Lcom/peel/ui/ft;->hint_search_box:I

    invoke-virtual {p0, v4}, Lcom/peel/h/a/a;->a(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/AutoCompleteTextView;->setHint(Ljava/lang/CharSequence;)V

    .line 376
    :goto_1
    iget-object v0, p0, Lcom/peel/h/a/a;->aG:Landroid/widget/AutoCompleteTextView;

    new-instance v3, Lcom/peel/h/a/y;

    invoke-direct {v3, p0}, Lcom/peel/h/a/y;-><init>(Lcom/peel/h/a/a;)V

    invoke-virtual {v0, v3}, Landroid/widget/AutoCompleteTextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 400
    iget-object v0, p0, Lcom/peel/h/a/a;->aI:Landroid/widget/ImageView;

    new-instance v3, Lcom/peel/h/a/z;

    invoke-direct {v3, p0}, Lcom/peel/h/a/z;-><init>(Lcom/peel/h/a/a;)V

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 413
    iget-object v3, p0, Lcom/peel/h/a/a;->aJ:Landroid/widget/RelativeLayout;

    iget-object v0, p0, Lcom/peel/h/a/a;->aq:Ljava/util/List;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/peel/h/a/a;->aq:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/16 v4, 0x9

    if-le v0, v4, :cond_4

    move v0, v1

    :goto_2
    invoke-virtual {v3, v0}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 415
    iget-object v0, p0, Lcom/peel/h/a/a;->aq:Ljava/util/List;

    new-instance v1, Lcom/peel/f/b;

    invoke-direct {v1}, Lcom/peel/f/b;-><init>()V

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 416
    new-instance v0, Lcom/peel/i/a/a;

    invoke-virtual {p0}, Lcom/peel/h/a/a;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    sget v2, Lcom/peel/ui/fq;->brand_row:I

    iget-object v3, p0, Lcom/peel/h/a/a;->aq:Ljava/util/List;

    invoke-direct {v0, v1, v2, v3}, Lcom/peel/i/a/a;-><init>(Landroid/content/Context;ILjava/util/List;)V

    iput-object v0, p0, Lcom/peel/h/a/a;->ax:Lcom/peel/i/a/a;

    .line 417
    iget-object v0, p0, Lcom/peel/h/a/a;->ao:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/peel/h/a/a;->ax:Lcom/peel/i/a/a;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 418
    iget-object v0, p0, Lcom/peel/h/a/a;->ao:Landroid/widget/ListView;

    new-instance v1, Lcom/peel/h/a/aa;

    invoke-direct {v1, p0, p1}, Lcom/peel/h/a/aa;-><init>(Lcom/peel/h/a/a;Lcom/peel/control/RoomControl;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 432
    invoke-virtual {p0}, Lcom/peel/h/a/a;->v()Landroid/view/View;

    move-result-object v0

    sget v1, Lcom/peel/ui/fp;->missing_device_brand_btn:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/peel/h/a/ab;

    invoke-direct {v1, p0, p1}, Lcom/peel/h/a/ab;-><init>(Lcom/peel/h/a/a;Lcom/peel/control/RoomControl;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 449
    :cond_1
    return-void

    .line 358
    :cond_2
    iget-object v0, p0, Lcom/peel/h/a/a;->aG:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v0}, Landroid/widget/AutoCompleteTextView;->getEditableText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->clear()V

    goto/16 :goto_0

    .line 374
    :cond_3
    iget-object v0, p0, Lcom/peel/h/a/a;->aG:Landroid/widget/AutoCompleteTextView;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "       "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget v4, Lcom/peel/ui/ft;->hint_search_box:I

    invoke-virtual {p0, v4}, Lcom/peel/h/a/a;->a(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/AutoCompleteTextView;->setHint(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    :cond_4
    move v0, v2

    .line 413
    goto :goto_2
.end method

.method static synthetic b(Lcom/peel/h/a/a;Lcom/peel/control/RoomControl;)V
    .locals 0

    .prologue
    .line 82
    invoke-direct {p0, p1}, Lcom/peel/h/a/a;->b(Lcom/peel/control/RoomControl;)V

    return-void
.end method

.method static synthetic c(Lcom/peel/h/a/a;I)I
    .locals 0

    .prologue
    .line 82
    iput p1, p0, Lcom/peel/h/a/a;->i:I

    return p1
.end method

.method static synthetic c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 82
    sget-object v0, Lcom/peel/h/a/a;->e:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic c(Lcom/peel/h/a/a;)Ljava/util/List;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/peel/h/a/a;->ap:Ljava/util/List;

    return-object v0
.end method

.method private c(Lcom/peel/control/RoomControl;)V
    .locals 9

    .prologue
    .line 459
    iget v0, p0, Lcom/peel/h/a/a;->ak:I

    packed-switch v0, :pswitch_data_0

    .line 556
    const/4 v3, 0x0

    .line 558
    iget v0, p0, Lcom/peel/h/a/a;->ak:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/peel/h/a/a;->am:Lcom/peel/f/a;

    invoke-virtual {v0}, Lcom/peel/f/a;->b()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "TiVo"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 559
    const/4 v3, 0x1

    .line 560
    :cond_0
    const/4 v0, 0x0

    iget v1, p0, Lcom/peel/h/a/a;->ak:I

    iget-object v2, p0, Lcom/peel/h/a/a;->am:Lcom/peel/f/a;

    invoke-virtual {v2}, Lcom/peel/f/a;->b()Ljava/lang/String;

    move-result-object v2

    const/4 v4, 0x0

    const/4 v5, -0x1

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-static/range {v0 .. v8}, Lcom/peel/control/h;->a(IILjava/lang/String;ZLjava/lang/String;ILandroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;)Lcom/peel/control/h;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/h/a/a;->ar:Lcom/peel/control/h;

    .line 563
    iget-object v0, p0, Lcom/peel/h/a/a;->f:Landroid/widget/ViewFlipper;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/widget/ViewFlipper;->setDisplayedChild(I)V

    .line 564
    invoke-virtual {p0}, Lcom/peel/h/a/a;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    const-string/jumbo v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/ae;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 565
    invoke-virtual {p0}, Lcom/peel/h/a/a;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/ae;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 567
    const/4 v0, 0x0

    iput v0, p0, Lcom/peel/h/a/a;->aY:I

    .line 568
    invoke-virtual {p0}, Lcom/peel/h/a/a;->v()Landroid/view/View;

    move-result-object v0

    sget v1, Lcom/peel/ui/fp;->layout_device_setup_test:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    .line 569
    sget v1, Lcom/peel/ui/fp;->device_visual:I

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/peel/h/a/a;->aQ:Landroid/widget/ImageView;

    .line 570
    invoke-virtual {p0}, Lcom/peel/h/a/a;->v()Landroid/view/View;

    move-result-object v1

    sget v2, Lcom/peel/ui/fp;->coverView:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    new-instance v2, Lcom/peel/h/a/e;

    invoke-direct {v2, p0}, Lcom/peel/h/a/e;-><init>(Lcom/peel/h/a/a;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 576
    iget v1, p0, Lcom/peel/h/a/a;->ak:I

    sparse-switch v1, :sswitch_data_0

    .line 597
    iget-object v1, p0, Lcom/peel/h/a/a;->aQ:Landroid/widget/ImageView;

    sget v2, Lcom/peel/ui/fo;->test_tv_drawing:I

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 601
    :goto_0
    sget v1, Lcom/peel/ui/fp;->testing_turn_on_msg:I

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/peel/h/a/a;->aP:Landroid/widget/TextView;

    .line 602
    iget-object v1, p0, Lcom/peel/h/a/a;->aP:Landroid/widget/TextView;

    sget v2, Lcom/peel/ui/ft;->testing_tv_msg:I

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/peel/h/a/a;->az:Ljava/lang/String;

    aput-object v5, v3, v4

    invoke-virtual {p0, v2, v3}, Lcom/peel/h/a/a;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 604
    sget v1, Lcom/peel/ui/fp;->test_btn_viewpager:I

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/peel/widget/TestBtnViewPager;

    iput-object v1, p0, Lcom/peel/h/a/a;->aU:Lcom/peel/widget/TestBtnViewPager;

    .line 605
    iget-object v1, p0, Lcom/peel/h/a/a;->aU:Lcom/peel/widget/TestBtnViewPager;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/peel/widget/TestBtnViewPager;->setEnabledSwipe(Z)V

    .line 606
    iget-object v1, p0, Lcom/peel/h/a/a;->aU:Lcom/peel/widget/TestBtnViewPager;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Lcom/peel/widget/TestBtnViewPager;->setVisibility(I)V

    .line 608
    sget v1, Lcom/peel/ui/fp;->test_pager_left_btn:I

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/peel/h/a/a;->aV:Landroid/widget/Button;

    .line 609
    iget-object v1, p0, Lcom/peel/h/a/a;->aV:Landroid/widget/Button;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setEnabled(Z)V

    .line 610
    iget-object v1, p0, Lcom/peel/h/a/a;->aV:Landroid/widget/Button;

    new-instance v2, Lcom/peel/h/a/f;

    invoke-direct {v2, p0}, Lcom/peel/h/a/f;-><init>(Lcom/peel/h/a/a;)V

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 618
    sget v1, Lcom/peel/ui/fp;->test_pager_right_btn:I

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/peel/h/a/a;->aW:Landroid/widget/Button;

    .line 619
    iget-object v1, p0, Lcom/peel/h/a/a;->aW:Landroid/widget/Button;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setEnabled(Z)V

    .line 620
    iget-object v1, p0, Lcom/peel/h/a/a;->aW:Landroid/widget/Button;

    new-instance v2, Lcom/peel/h/a/g;

    invoke-direct {v2, p0}, Lcom/peel/h/a/g;-><init>(Lcom/peel/h/a/a;)V

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 628
    iget-object v1, p0, Lcom/peel/h/a/a;->aV:Landroid/widget/Button;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 629
    iget-object v1, p0, Lcom/peel/h/a/a;->aW:Landroid/widget/Button;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 631
    sget v1, Lcom/peel/ui/fp;->test_question_msg:I

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/peel/h/a/a;->aR:Landroid/widget/TextView;

    .line 632
    const/4 v1, 0x2

    iget v2, p0, Lcom/peel/h/a/a;->ak:I

    if-eq v1, v2, :cond_1

    const/16 v1, 0x14

    iget v2, p0, Lcom/peel/h/a/a;->ak:I

    if-ne v1, v2, :cond_6

    .line 633
    :cond_1
    iget-object v1, p0, Lcom/peel/h/a/a;->aR:Landroid/widget/TextView;

    sget v2, Lcom/peel/ui/ft;->device_test_channel_change_question_msg:I

    invoke-virtual {p0, v2}, Lcom/peel/h/a/a;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 637
    :goto_1
    sget v1, Lcom/peel/ui/fp;->layout_test_btn:I

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    iput-object v1, p0, Lcom/peel/h/a/a;->aT:Landroid/widget/RelativeLayout;

    .line 638
    sget v1, Lcom/peel/ui/fp;->layout_test_msg:I

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    iput-object v1, p0, Lcom/peel/h/a/a;->aS:Landroid/widget/RelativeLayout;

    .line 640
    iget-object v1, p0, Lcom/peel/h/a/a;->aT:Landroid/widget/RelativeLayout;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 641
    iget-object v1, p0, Lcom/peel/h/a/a;->aS:Landroid/widget/RelativeLayout;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 643
    sget v1, Lcom/peel/ui/fp;->turn_on_msg:I

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/peel/h/a/a;->aN:Landroid/widget/TextView;

    .line 646
    sget v1, Lcom/peel/ui/fp;->test_status_msg:I

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/peel/h/a/a;->aO:Landroid/widget/TextView;

    .line 647
    iget-object v1, p0, Lcom/peel/h/a/a;->aO:Landroid/widget/TextView;

    new-instance v2, Lcom/peel/h/a/h;

    invoke-direct {v2, p0}, Lcom/peel/h/a/h;-><init>(Lcom/peel/h/a/a;)V

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 657
    sget v1, Lcom/peel/ui/fp;->yes_btn:I

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/peel/h/a/a;->aB:Landroid/widget/Button;

    .line 658
    sget v1, Lcom/peel/ui/fp;->no_btn:I

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/peel/h/a/a;->aC:Landroid/widget/Button;

    .line 660
    invoke-virtual {p0}, Lcom/peel/h/a/a;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/support/v4/app/ae;->setProgressBarIndeterminateVisibility(Z)V

    .line 662
    iget-object v0, p0, Lcom/peel/h/a/a;->ay:Ljava/lang/String;

    const-string/jumbo v1, "Power"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/peel/h/a/a;->ay:Ljava/lang/String;

    const-string/jumbo v1, "PowerOn"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 663
    :cond_2
    sget v0, Lcom/peel/ui/ft;->turn_on_device:I

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/peel/h/a/a;->am:Lcom/peel/f/a;

    invoke-virtual {v3}, Lcom/peel/f/a;->b()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/peel/h/a/a;->az:Ljava/lang/String;

    aput-object v3, v1, v2

    invoke-virtual {p0, v0, v1}, Lcom/peel/h/a/a;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-direct {p0, v0, v1, v2}, Lcom/peel/h/a/a;->a(Ljava/lang/String;ZZ)V

    .line 664
    iget-object v0, p0, Lcom/peel/h/a/a;->aN:Landroid/widget/TextView;

    sget v1, Lcom/peel/ui/ft;->testing_key_power:I

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/peel/h/a/a;->am:Lcom/peel/f/a;

    invoke-virtual {v4}, Lcom/peel/f/a;->b()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/peel/h/a/a;->az:Ljava/lang/String;

    aput-object v4, v2, v3

    invoke-virtual {p0, v1, v2}, Lcom/peel/h/a/a;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 671
    :cond_3
    :goto_2
    iget-object v0, p0, Lcom/peel/h/a/a;->aB:Landroid/widget/Button;

    new-instance v1, Lcom/peel/h/a/i;

    invoke-direct {v1, p0, p1}, Lcom/peel/h/a/i;-><init>(Lcom/peel/h/a/a;Lcom/peel/control/RoomControl;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 893
    iget-object v0, p0, Lcom/peel/h/a/a;->aC:Landroid/widget/Button;

    new-instance v1, Lcom/peel/h/a/k;

    invoke-direct {v1, p0, p1}, Lcom/peel/h/a/k;-><init>(Lcom/peel/h/a/a;Lcom/peel/control/RoomControl;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 924
    const/4 v0, 0x1

    new-array v1, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    iget-object v2, p0, Lcom/peel/h/a/a;->ay:Ljava/lang/String;

    aput-object v2, v1, v0

    .line 926
    invoke-virtual {p0}, Lcom/peel/h/a/a;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string/jumbo v2, "country_ISO"

    const-string/jumbo v3, "US"

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 927
    const-string/jumbo v0, "https://partners-ir.peel.com"

    iget v2, p0, Lcom/peel/h/a/a;->ak:I

    iget-object v3, p0, Lcom/peel/h/a/a;->am:Lcom/peel/f/a;

    invoke-virtual {v3}, Lcom/peel/f/a;->a()I

    move-result v3

    new-instance v5, Lcom/peel/h/a/l;

    const/4 v6, 0x1

    invoke-direct {v5, p0, v6}, Lcom/peel/h/a/l;-><init>(Lcom/peel/h/a/a;I)V

    invoke-static/range {v0 .. v5}, Lcom/peel/control/aa;->a(Ljava/lang/String;[Ljava/lang/String;IILjava/lang/String;Lcom/peel/util/t;)V

    .line 1042
    :goto_3
    return-void

    .line 461
    :pswitch_0
    const/4 v0, 0x0

    iget v1, p0, Lcom/peel/h/a/a;->ak:I

    iget-object v2, p0, Lcom/peel/h/a/a;->am:Lcom/peel/f/a;

    invoke-virtual {v2}, Lcom/peel/f/a;->b()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    const/4 v4, 0x0

    const/4 v5, -0x1

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-static/range {v0 .. v8}, Lcom/peel/control/h;->a(IILjava/lang/String;ZLjava/lang/String;ILandroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;)Lcom/peel/control/h;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/h/a/a;->ar:Lcom/peel/control/h;

    .line 462
    iget-object v0, p0, Lcom/peel/h/a/a;->am:Lcom/peel/f/a;

    invoke-virtual {v0}, Lcom/peel/f/a;->b()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v0, v1}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "apple"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 463
    const-class v0, Lcom/peel/h/a/a;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "start scanning for google tv"

    new-instance v2, Lcom/peel/h/a/ad;

    invoke-direct {v2, p0}, Lcom/peel/h/a/ad;-><init>(Lcom/peel/h/a/a;)V

    invoke-static {v0, v1, v2}, Lcom/peel/util/i;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/lang/Runnable;

    .line 477
    :cond_4
    iget-object v0, p0, Lcom/peel/h/a/a;->am:Lcom/peel/f/a;

    invoke-virtual {v0}, Lcom/peel/f/a;->b()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v0, v1}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "roku"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 478
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 479
    const-string/jumbo v1, "brandName"

    iget-object v2, p0, Lcom/peel/h/a/a;->am:Lcom/peel/f/a;

    invoke-virtual {v2}, Lcom/peel/f/a;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 480
    const-string/jumbo v1, "device_type"

    iget v2, p0, Lcom/peel/h/a/a;->ak:I

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 481
    const-string/jumbo v1, "brandId"

    iget-object v2, p0, Lcom/peel/h/a/a;->am:Lcom/peel/f/a;

    invoke-virtual {v2}, Lcom/peel/f/a;->a()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 482
    const-string/jumbo v1, "back_to_clazz"

    iget-object v2, p0, Lcom/peel/h/a/a;->aA:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 483
    invoke-virtual {p0}, Lcom/peel/h/a/a;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    const-class v2, Lcom/peel/i/fc;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lcom/peel/d/e;->c(Landroid/support/v4/app/ae;Ljava/lang/String;Landroid/os/Bundle;)V

    goto/16 :goto_3

    .line 485
    :cond_5
    invoke-virtual {p0}, Lcom/peel/h/a/a;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string/jumbo v1, "country_ISO"

    const-string/jumbo v2, "US"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 486
    const-string/jumbo v1, "https://partners-ir.peel.com"

    iget-object v2, p0, Lcom/peel/h/a/a;->am:Lcom/peel/f/a;

    invoke-virtual {v2}, Lcom/peel/f/a;->a()I

    move-result v2

    iget v3, p0, Lcom/peel/h/a/a;->ak:I

    new-instance v4, Lcom/peel/h/a/c;

    invoke-direct {v4, p0, p1}, Lcom/peel/h/a/c;-><init>(Lcom/peel/h/a/a;Lcom/peel/control/RoomControl;)V

    invoke-static {v1, v2, v3, v0, v4}, Lcom/peel/control/aa;->a(Ljava/lang/String;IILjava/lang/String;Lcom/peel/util/t;)V

    goto/16 :goto_3

    .line 579
    :sswitch_0
    iget-object v1, p0, Lcom/peel/h/a/a;->aQ:Landroid/widget/ImageView;

    sget v2, Lcom/peel/ui/fo;->test_stb_drawing:I

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_0

    .line 582
    :sswitch_1
    iget-object v1, p0, Lcom/peel/h/a/a;->aQ:Landroid/widget/ImageView;

    sget v2, Lcom/peel/ui/fo;->test_av_drawing:I

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_0

    .line 585
    :sswitch_2
    iget-object v1, p0, Lcom/peel/h/a/a;->aQ:Landroid/widget/ImageView;

    sget v2, Lcom/peel/ui/fo;->test_dvd_drawing:I

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_0

    .line 588
    :sswitch_3
    iget-object v1, p0, Lcom/peel/h/a/a;->aQ:Landroid/widget/ImageView;

    sget v2, Lcom/peel/ui/fo;->test_bluray_drawing:I

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_0

    .line 591
    :sswitch_4
    iget-object v1, p0, Lcom/peel/h/a/a;->aQ:Landroid/widget/ImageView;

    sget v2, Lcom/peel/ui/fo;->test_projector_drawing:I

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_0

    .line 594
    :sswitch_5
    iget-object v1, p0, Lcom/peel/h/a/a;->aQ:Landroid/widget/ImageView;

    sget v2, Lcom/peel/ui/fo;->test_ac_drawing:I

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_0

    .line 635
    :cond_6
    iget-object v1, p0, Lcom/peel/h/a/a;->aR:Landroid/widget/TextView;

    sget v2, Lcom/peel/ui/ft;->device_test_turn_on_question_msg:I

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/peel/h/a/a;->az:Ljava/lang/String;

    aput-object v5, v3, v4

    invoke-virtual {p0, v2, v3}, Lcom/peel/h/a/a;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 665
    :cond_7
    iget-object v0, p0, Lcom/peel/h/a/a;->ay:Ljava/lang/String;

    const-string/jumbo v1, "Channel_Up"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 666
    invoke-virtual {p0}, Lcom/peel/h/a/a;->n()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/peel/ui/ft;->testing_device:I

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/peel/h/a/a;->az:Ljava/lang/String;

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-direct {p0, v0, v1, v2}, Lcom/peel/h/a/a;->a(Ljava/lang/String;ZZ)V

    .line 667
    iget-object v0, p0, Lcom/peel/h/a/a;->aN:Landroid/widget/TextView;

    sget v1, Lcom/peel/ui/ft;->testing_key_stb:I

    invoke-virtual {p0, v1}, Lcom/peel/h/a/a;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    .line 459
    nop

    :pswitch_data_0
    .packed-switch 0x6
        :pswitch_0
    .end packed-switch

    .line 576
    :sswitch_data_0
    .sparse-switch
        0x2 -> :sswitch_0
        0x3 -> :sswitch_2
        0x4 -> :sswitch_3
        0x5 -> :sswitch_1
        0xa -> :sswitch_4
        0x12 -> :sswitch_5
        0x14 -> :sswitch_0
    .end sparse-switch
.end method

.method static synthetic c(Lcom/peel/h/a/a;Lcom/peel/control/RoomControl;)V
    .locals 0

    .prologue
    .line 82
    invoke-direct {p0, p1}, Lcom/peel/h/a/a;->c(Lcom/peel/control/RoomControl;)V

    return-void
.end method

.method static synthetic d(Lcom/peel/h/a/a;)I
    .locals 1

    .prologue
    .line 82
    iget v0, p0, Lcom/peel/h/a/a;->ak:I

    return v0
.end method

.method static synthetic d(Lcom/peel/h/a/a;I)I
    .locals 0

    .prologue
    .line 82
    iput p1, p0, Lcom/peel/h/a/a;->aY:I

    return p1
.end method

.method private d(Lcom/peel/control/RoomControl;)V
    .locals 2

    .prologue
    .line 1049
    .line 1052
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/peel/h/a/a;->aE:Z

    .line 1053
    iget-object v0, p0, Lcom/peel/h/a/a;->b:Lcom/peel/d/i;

    if-eqz v0, :cond_0

    .line 1054
    sget-object v0, Lcom/peel/h/a/a;->e:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/peel/h/a/a;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/peel/d/e;->a(Ljava/lang/String;Landroid/support/v4/app/ae;)V

    .line 1149
    :cond_0
    return-void
.end method

.method static synthetic d(Lcom/peel/h/a/a;Lcom/peel/control/RoomControl;)V
    .locals 0

    .prologue
    .line 82
    invoke-direct {p0, p1}, Lcom/peel/h/a/a;->d(Lcom/peel/control/RoomControl;)V

    return-void
.end method

.method static synthetic e(Lcom/peel/h/a/a;)V
    .locals 0

    .prologue
    .line 82
    invoke-direct {p0}, Lcom/peel/h/a/a;->U()V

    return-void
.end method

.method static synthetic f(Lcom/peel/h/a/a;)V
    .locals 0

    .prologue
    .line 82
    invoke-direct {p0}, Lcom/peel/h/a/a;->T()V

    return-void
.end method

.method static synthetic g(Lcom/peel/h/a/a;)Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/peel/h/a/a;->aI:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic h(Lcom/peel/h/a/a;)Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/peel/h/a/a;->aH:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic i(Lcom/peel/h/a/a;)Landroid/widget/ListView;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/peel/h/a/a;->ao:Landroid/widget/ListView;

    return-object v0
.end method

.method static synthetic j(Lcom/peel/h/a/a;)Landroid/widget/AutoCompleteTextView;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/peel/h/a/a;->aG:Landroid/widget/AutoCompleteTextView;

    return-object v0
.end method

.method static synthetic k(Lcom/peel/h/a/a;)Lcom/peel/i/a/a;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/peel/h/a/a;->ax:Lcom/peel/i/a/a;

    return-object v0
.end method

.method static synthetic l(Lcom/peel/h/a/a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/peel/h/a/a;->az:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic m(Lcom/peel/h/a/a;)Ljava/util/List;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/peel/h/a/a;->aq:Ljava/util/List;

    return-object v0
.end method

.method static synthetic n(Lcom/peel/h/a/a;)Lcom/peel/f/a;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/peel/h/a/a;->am:Lcom/peel/f/a;

    return-object v0
.end method

.method static synthetic o(Lcom/peel/h/a/a;)Lcom/peel/widget/ag;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/peel/h/a/a;->aL:Lcom/peel/widget/ag;

    return-object v0
.end method

.method static synthetic p(Lcom/peel/h/a/a;)Lcom/peel/control/h;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/peel/h/a/a;->ar:Lcom/peel/control/h;

    return-object v0
.end method

.method static synthetic q(Lcom/peel/h/a/a;)Lcom/peel/control/a;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/peel/h/a/a;->al:Lcom/peel/control/a;

    return-object v0
.end method

.method static synthetic r(Lcom/peel/h/a/a;)Lcom/peel/control/h;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/peel/h/a/a;->as:Lcom/peel/control/h;

    return-object v0
.end method

.method static synthetic s(Lcom/peel/h/a/a;)Lcom/peel/control/h;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/peel/h/a/a;->at:Lcom/peel/control/h;

    return-object v0
.end method

.method static synthetic t(Lcom/peel/h/a/a;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/peel/h/a/a;->h:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic u(Lcom/peel/h/a/a;)Lcom/peel/widget/TestBtnViewPager;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/peel/h/a/a;->aU:Lcom/peel/widget/TestBtnViewPager;

    return-object v0
.end method

.method static synthetic v(Lcom/peel/h/a/a;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/peel/h/a/a;->aO:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic w(Lcom/peel/h/a/a;)I
    .locals 1

    .prologue
    .line 82
    iget v0, p0, Lcom/peel/h/a/a;->i:I

    return v0
.end method

.method static synthetic x(Lcom/peel/h/a/a;)Landroid/widget/Button;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/peel/h/a/a;->aB:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic y(Lcom/peel/h/a/a;)Z
    .locals 1

    .prologue
    .line 82
    iget-boolean v0, p0, Lcom/peel/h/a/a;->aD:Z

    return v0
.end method

.method static synthetic z(Lcom/peel/h/a/a;)Z
    .locals 1

    .prologue
    .line 82
    iget-boolean v0, p0, Lcom/peel/h/a/a;->aF:Z

    return v0
.end method


# virtual methods
.method public Z()V
    .locals 6

    .prologue
    .line 1258
    iget-object v0, p0, Lcom/peel/h/a/a;->d:Lcom/peel/d/a;

    if-nez v0, :cond_0

    .line 1259
    new-instance v0, Lcom/peel/d/a;

    sget-object v1, Lcom/peel/d/d;->b:Lcom/peel/d/d;

    sget-object v2, Lcom/peel/d/b;->a:Lcom/peel/d/b;

    sget-object v3, Lcom/peel/d/c;->b:Lcom/peel/d/c;

    sget v4, Lcom/peel/ui/ft;->title_add_device:I

    invoke-virtual {p0, v4}, Lcom/peel/h/a/a;->a(I)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, Lcom/peel/d/a;-><init>(Lcom/peel/d/d;Lcom/peel/d/b;Lcom/peel/d/c;Ljava/lang/String;Ljava/util/List;)V

    iput-object v0, p0, Lcom/peel/h/a/a;->d:Lcom/peel/d/a;

    .line 1261
    :cond_0
    iget-object v0, p0, Lcom/peel/h/a/a;->b:Lcom/peel/d/i;

    iget-object v1, p0, Lcom/peel/h/a/a;->d:Lcom/peel/d/a;

    invoke-virtual {v0, v1}, Lcom/peel/d/i;->a(Lcom/peel/d/a;)V

    .line 1262
    return-void
.end method

.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3

    .prologue
    .line 132
    iget-object v0, p0, Lcom/peel/h/a/a;->b:Lcom/peel/d/i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/peel/h/a/a;->b:Lcom/peel/d/i;

    const-class v1, Lcom/peel/h/a/a;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/peel/d/i;->c(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 133
    new-instance v0, Landroid/os/Bundle;

    iget-object v1, p0, Lcom/peel/h/a/a;->b:Lcom/peel/d/i;

    const-class v2, Lcom/peel/h/a/a;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/peel/d/i;->c(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Bundle;-><init>(Landroid/os/Bundle;)V

    invoke-super {p0, v0}, Lcom/peel/d/u;->c(Landroid/os/Bundle;)V

    .line 136
    iget-object v0, p0, Lcom/peel/h/a/a;->b:Lcom/peel/d/i;

    const-class v1, Lcom/peel/h/a/a;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/peel/d/i;->c(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Bundle;->clear()V

    .line 139
    :cond_0
    sget v0, Lcom/peel/ui/fq;->device_setup:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ViewFlipper;

    iput-object v0, p0, Lcom/peel/h/a/a;->f:Landroid/widget/ViewFlipper;

    .line 140
    iget-object v0, p0, Lcom/peel/h/a/a;->f:Landroid/widget/ViewFlipper;

    new-instance v1, Lcom/peel/h/a/b;

    invoke-direct {v1, p0}, Lcom/peel/h/a/b;-><init>(Lcom/peel/h/a/a;)V

    invoke-virtual {v0, v1}, Landroid/widget/ViewFlipper;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 146
    iget-object v0, p0, Lcom/peel/h/a/a;->f:Landroid/widget/ViewFlipper;

    return-object v0
.end method

.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 453
    iget-object v0, p0, Lcom/peel/h/a/a;->aA:Ljava/lang/String;

    return-object v0
.end method

.method public a(Landroid/view/MenuItem;)Z
    .locals 8

    .prologue
    const/4 v1, 0x1

    .line 1165
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    .line 1166
    sget v2, Lcom/peel/ui/fp;->menu_next:I

    if-ne v0, v2, :cond_1

    .line 1167
    iget-object v0, p0, Lcom/peel/h/a/a;->f:Landroid/widget/ViewFlipper;

    invoke-virtual {v0}, Landroid/widget/ViewFlipper;->getDisplayedChild()I

    move-result v0

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/peel/h/a/a;->f:Landroid/widget/ViewFlipper;

    invoke-virtual {v0}, Landroid/widget/ViewFlipper;->getDisplayedChild()I

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/peel/h/a/a;->am:Lcom/peel/f/a;

    if-eqz v0, :cond_1

    .line 1168
    invoke-static {}, Lcom/peel/util/a/f;->a()Lcom/peel/util/a/f;

    move-result-object v0

    iget-object v2, p0, Lcom/peel/h/a/a;->aZ:Lcom/peel/control/RoomControl;

    if-nez v2, :cond_2

    :goto_0
    const/16 v2, 0xc29

    const/16 v3, 0x7d8

    iget-object v4, p0, Lcom/peel/h/a/a;->am:Lcom/peel/f/a;

    .line 1169
    invoke-virtual {v4}, Lcom/peel/f/a;->b()Ljava/lang/String;

    move-result-object v4

    iget v5, p0, Lcom/peel/h/a/a;->ak:I

    iget-object v6, p0, Lcom/peel/h/a/a;->az:Ljava/lang/String;

    const/4 v7, -0x1

    .line 1168
    invoke-virtual/range {v0 .. v7}, Lcom/peel/util/a/f;->a(IIILjava/lang/String;ILjava/lang/String;I)V

    .line 1170
    iget-object v0, p0, Lcom/peel/h/a/a;->aZ:Lcom/peel/control/RoomControl;

    invoke-direct {p0, v0}, Lcom/peel/h/a/a;->c(Lcom/peel/control/RoomControl;)V

    .line 1173
    :cond_1
    invoke-super {p0, p1}, Lcom/peel/d/u;->a(Landroid/view/MenuItem;)Z

    move-result v0

    return v0

    .line 1168
    :cond_2
    iget-object v1, p0, Lcom/peel/h/a/a;->aZ:Lcom/peel/control/RoomControl;

    invoke-virtual {v1}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/data/at;->f()I

    move-result v1

    goto :goto_0
.end method

.method public b()Z
    .locals 5

    .prologue
    const/4 v4, -0x1

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1178
    invoke-virtual {p0}, Lcom/peel/h/a/a;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    const-string/jumbo v3, "input_method"

    invoke-virtual {v0, v3}, Landroid/support/v4/app/ae;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 1179
    invoke-virtual {p0}, Lcom/peel/h/a/a;->m()Landroid/support/v4/app/ae;

    move-result-object v3

    invoke-virtual {v3}, Landroid/support/v4/app/ae;->getWindow()Landroid/view/Window;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v3

    invoke-virtual {v0, v3, v1}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 1180
    iget-boolean v0, p0, Lcom/peel/h/a/a;->aE:Z

    if-eqz v0, :cond_0

    .line 1181
    iput-boolean v1, p0, Lcom/peel/h/a/a;->aE:Z

    move v0, v1

    .line 1203
    :goto_0
    return v0

    .line 1185
    :cond_0
    iget-object v0, p0, Lcom/peel/h/a/a;->f:Landroid/widget/ViewFlipper;

    invoke-virtual {v0}, Landroid/widget/ViewFlipper;->getDisplayedChild()I

    move-result v0

    const/4 v3, 0x2

    if-ne v0, v3, :cond_2

    .line 1186
    iget v0, p0, Lcom/peel/h/a/a;->g:I

    if-ltz v0, :cond_1

    .line 1187
    iget-object v0, p0, Lcom/peel/h/a/a;->f:Landroid/widget/ViewFlipper;

    iget v1, p0, Lcom/peel/h/a/a;->g:I

    invoke-virtual {v0, v1}, Landroid/widget/ViewFlipper;->setDisplayedChild(I)V

    .line 1188
    iput v4, p0, Lcom/peel/h/a/a;->g:I

    .line 1192
    :goto_1
    iget-object v0, p0, Lcom/peel/h/a/a;->ba:Lcom/peel/control/RoomControl;

    invoke-direct {p0, v0}, Lcom/peel/h/a/a;->a(Lcom/peel/control/RoomControl;)V

    move v0, v2

    .line 1193
    goto :goto_0

    .line 1190
    :cond_1
    iget-object v0, p0, Lcom/peel/h/a/a;->f:Landroid/widget/ViewFlipper;

    invoke-virtual {v0, v1}, Landroid/widget/ViewFlipper;->setDisplayedChild(I)V

    goto :goto_1

    .line 1194
    :cond_2
    iget-object v0, p0, Lcom/peel/h/a/a;->f:Landroid/widget/ViewFlipper;

    invoke-virtual {v0}, Landroid/widget/ViewFlipper;->getDisplayedChild()I

    move-result v0

    if-ne v0, v2, :cond_4

    iget-object v0, p0, Lcom/peel/h/a/a;->ap:Ljava/util/List;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/peel/h/a/a;->ap:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_4

    .line 1195
    iget v0, p0, Lcom/peel/h/a/a;->g:I

    if-ltz v0, :cond_3

    .line 1196
    iput v4, p0, Lcom/peel/h/a/a;->g:I

    .line 1198
    :cond_3
    iget-object v0, p0, Lcom/peel/h/a/a;->f:Landroid/widget/ViewFlipper;

    invoke-virtual {v0, v1}, Landroid/widget/ViewFlipper;->setDisplayedChild(I)V

    .line 1199
    iget-object v0, p0, Lcom/peel/h/a/a;->ba:Lcom/peel/control/RoomControl;

    invoke-direct {p0, v0}, Lcom/peel/h/a/a;->a(Lcom/peel/control/RoomControl;)V

    move v0, v2

    .line 1200
    goto :goto_0

    .line 1203
    :cond_4
    invoke-super {p0}, Lcom/peel/d/u;->b()Z

    move-result v0

    goto :goto_0
.end method

.method public c(Landroid/os/Bundle;)V
    .locals 8

    .prologue
    const/4 v7, 0x1

    .line 229
    invoke-super {p0, p1}, Lcom/peel/d/u;->c(Landroid/os/Bundle;)V

    .line 231
    sget-object v0, Lcom/peel/content/a;->h:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/peel/h/a/a;->b:Lcom/peel/d/i;

    if-nez v0, :cond_1

    .line 261
    :cond_0
    :goto_0
    return-void

    .line 234
    :cond_1
    const-string/jumbo v0, "room"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    const-string/jumbo v1, "room"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/peel/control/am;->a(Ljava/lang/String;)Lcom/peel/control/RoomControl;

    move-result-object v0

    move-object v1, v0

    .line 235
    :goto_1
    if-nez v1, :cond_2

    sget-object v0, Lcom/peel/h/a/a;->e:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/peel/h/a/a;->m()Landroid/support/v4/app/ae;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/peel/d/e;->a(Ljava/lang/String;Landroid/support/v4/app/ae;)V

    .line 237
    :cond_2
    iget-boolean v0, p0, Lcom/peel/h/a/a;->aD:Z

    if-eqz v0, :cond_3

    const-string/jumbo v0, "activity_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 238
    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    const-string/jumbo v2, "activity_id"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/peel/control/am;->e(Ljava/lang/String;)Lcom/peel/control/a;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/h/a/a;->al:Lcom/peel/control/a;

    .line 241
    :cond_3
    iget v0, p0, Lcom/peel/h/a/a;->ak:I

    const/4 v2, -0x1

    if-ne v0, v2, :cond_4

    .line 242
    sget-object v0, Lcom/peel/h/a/a;->e:Ljava/lang/String;

    const-string/jumbo v2, "NO device_type specified in bundle()!"

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 243
    sget-object v0, Lcom/peel/h/a/a;->e:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/peel/h/a/a;->m()Landroid/support/v4/app/ae;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/peel/d/e;->a(Ljava/lang/String;Landroid/support/v4/app/ae;)V

    .line 247
    :cond_4
    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v0, v1}, Lcom/peel/control/am;->c(Lcom/peel/control/RoomControl;)[Lcom/peel/control/h;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    :goto_2
    if-ge v0, v3, :cond_9

    aget-object v4, v2, v0

    .line 248
    invoke-virtual {v4}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v5

    invoke-virtual {v5}, Lcom/peel/data/g;->d()I

    move-result v5

    const/4 v6, 0x5

    if-ne v5, v6, :cond_7

    .line 249
    iput-object v4, p0, Lcom/peel/h/a/a;->as:Lcom/peel/control/h;

    .line 247
    :cond_5
    :goto_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 234
    :cond_6
    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v0}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v0

    move-object v1, v0

    goto :goto_1

    .line 250
    :cond_7
    invoke-virtual {v4}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v5

    invoke-virtual {v5}, Lcom/peel/data/g;->d()I

    move-result v5

    if-eq v5, v7, :cond_8

    .line 251
    invoke-virtual {v4}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v5

    invoke-virtual {v5}, Lcom/peel/data/g;->d()I

    move-result v5

    const/16 v6, 0xa

    if-ne v5, v6, :cond_5

    .line 252
    :cond_8
    iput-object v4, p0, Lcom/peel/h/a/a;->at:Lcom/peel/control/h;

    goto :goto_3

    .line 256
    :cond_9
    const-string/jumbo v0, "refresh"

    invoke-virtual {p1, v0, v7}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 257
    const-string/jumbo v0, "refresh"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->remove(Ljava/lang/String;)V

    .line 258
    iput-object v1, p0, Lcom/peel/h/a/a;->ba:Lcom/peel/control/RoomControl;

    .line 259
    invoke-direct {p0, v1}, Lcom/peel/h/a/a;->a(Lcom/peel/control/RoomControl;)V

    goto/16 :goto_0
.end method

.method public d(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 185
    invoke-super {p0, p1}, Lcom/peel/d/u;->d(Landroid/os/Bundle;)V

    .line 187
    invoke-virtual {p0}, Lcom/peel/h/a/a;->Z()V

    .line 188
    return-void
.end method

.method public e()V
    .locals 6

    .prologue
    const-wide/16 v4, 0xfa

    .line 151
    invoke-super {p0}, Lcom/peel/d/u;->e()V

    .line 152
    iget-object v0, p0, Lcom/peel/h/a/a;->aG:Landroid/widget/AutoCompleteTextView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/peel/h/a/a;->aG:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v0}, Landroid/widget/AutoCompleteTextView;->isFocused()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 153
    invoke-virtual {p0}, Lcom/peel/h/a/a;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    const-class v1, Lcom/peel/h/a/a;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/peel/h/a/a;->aG:Landroid/widget/AutoCompleteTextView;

    invoke-static {v0, v1, v2, v4, v5}, Lcom/peel/util/bx;->a(Landroid/content/Context;Ljava/lang/String;Landroid/widget/EditText;J)V

    .line 156
    :cond_0
    iget-object v0, p0, Lcom/peel/h/a/a;->aK:Lcom/peel/ui/dg;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/peel/h/a/a;->aK:Lcom/peel/ui/dg;

    invoke-virtual {v0}, Lcom/peel/ui/dg;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 157
    iget-object v0, p0, Lcom/peel/h/a/a;->aK:Lcom/peel/ui/dg;

    iget-object v0, v0, Lcom/peel/ui/dg;->b:Landroid/widget/EditText;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/peel/h/a/a;->aK:Lcom/peel/ui/dg;

    iget-object v0, v0, Lcom/peel/ui/dg;->b:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/peel/h/a/a;->aK:Lcom/peel/ui/dg;

    iget-object v0, v0, Lcom/peel/ui/dg;->b:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->isFocused()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 158
    invoke-virtual {p0}, Lcom/peel/h/a/a;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    const-class v1, Lcom/peel/h/a/a;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/peel/h/a/a;->aK:Lcom/peel/ui/dg;

    iget-object v2, v2, Lcom/peel/ui/dg;->b:Landroid/widget/EditText;

    invoke-static {v0, v1, v2, v4, v5}, Lcom/peel/util/bx;->a(Landroid/content/Context;Ljava/lang/String;Landroid/widget/EditText;J)V

    .line 163
    :cond_1
    :goto_0
    return-void

    .line 159
    :cond_2
    iget-object v0, p0, Lcom/peel/h/a/a;->aK:Lcom/peel/ui/dg;

    iget-object v0, v0, Lcom/peel/ui/dg;->c:Landroid/widget/EditText;

    if-eqz v0, :cond_1

    .line 160
    invoke-virtual {p0}, Lcom/peel/h/a/a;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    const-class v1, Lcom/peel/h/a/a;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/peel/h/a/a;->aK:Lcom/peel/ui/dg;

    iget-object v2, v2, Lcom/peel/ui/dg;->b:Landroid/widget/EditText;

    invoke-static {v0, v1, v2, v4, v5}, Lcom/peel/util/bx;->a(Landroid/content/Context;Ljava/lang/String;Landroid/widget/EditText;J)V

    goto :goto_0
.end method

.method public f()V
    .locals 3

    .prologue
    .line 167
    invoke-super {p0}, Lcom/peel/d/u;->f()V

    .line 168
    invoke-virtual {p0}, Lcom/peel/h/a/a;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    const-string/jumbo v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/ae;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 169
    invoke-virtual {p0}, Lcom/peel/h/a/a;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/ae;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 170
    iget-object v0, p0, Lcom/peel/h/a/a;->av:Lcom/peel/widget/ag;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/peel/h/a/a;->av:Lcom/peel/widget/ag;

    invoke-virtual {v0}, Lcom/peel/widget/ag;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 172
    iget-object v0, p0, Lcom/peel/h/a/a;->av:Lcom/peel/widget/ag;

    invoke-virtual {v0}, Lcom/peel/widget/ag;->dismiss()V

    .line 178
    :cond_0
    iget-object v0, p0, Lcom/peel/h/a/a;->aM:Lcom/peel/widget/ag;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/peel/h/a/a;->aM:Lcom/peel/widget/ag;

    invoke-virtual {v0}, Lcom/peel/widget/ag;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 179
    iget-object v0, p0, Lcom/peel/h/a/a;->aM:Lcom/peel/widget/ag;

    invoke-virtual {v0}, Lcom/peel/widget/ag;->dismiss()V

    .line 181
    :cond_1
    return-void
.end method

.method public g()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1208
    iget-object v0, p0, Lcom/peel/h/a/a;->ax:Lcom/peel/i/a/a;

    if-eqz v0, :cond_0

    .line 1209
    iput-object v1, p0, Lcom/peel/h/a/a;->ax:Lcom/peel/i/a/a;

    .line 1212
    :cond_0
    iget-object v0, p0, Lcom/peel/h/a/a;->ao:Landroid/widget/ListView;

    if-eqz v0, :cond_1

    .line 1213
    iput-object v1, p0, Lcom/peel/h/a/a;->ao:Landroid/widget/ListView;

    .line 1215
    :cond_1
    invoke-super {p0}, Lcom/peel/d/u;->g()V

    .line 1216
    return-void
.end method

.method public h(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 192
    invoke-super {p0, p1}, Lcom/peel/d/u;->h(Landroid/os/Bundle;)V

    .line 194
    invoke-virtual {p0}, Lcom/peel/h/a/a;->m()Landroid/support/v4/app/ae;

    move-result-object v2

    invoke-static {v2}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v2

    const-string/jumbo v3, "setup_type"

    invoke-interface {v2, v3, v1}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v2

    if-ne v0, v2, :cond_0

    :goto_0
    iput-boolean v0, p0, Lcom/peel/h/a/a;->aF:Z

    .line 196
    iget-object v0, p0, Lcom/peel/h/a/a;->c:Landroid/os/Bundle;

    const-string/jumbo v2, "from_audio_setup"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/peel/h/a/a;->aD:Z

    .line 198
    iget-object v0, p0, Lcom/peel/h/a/a;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "back_to_clazz"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/peel/h/a/a;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "back_to_clazz"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_1
    iput-object v0, p0, Lcom/peel/h/a/a;->aA:Ljava/lang/String;

    .line 201
    iget-object v0, p0, Lcom/peel/h/a/a;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "device_type"

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/peel/h/a/a;->ak:I

    .line 203
    iget v0, p0, Lcom/peel/h/a/a;->ak:I

    sparse-switch v0, :sswitch_data_0

    .line 218
    const-string/jumbo v0, "Power"

    iput-object v0, p0, Lcom/peel/h/a/a;->ay:Ljava/lang/String;

    .line 222
    :goto_2
    invoke-virtual {p0}, Lcom/peel/h/a/a;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    iget v1, p0, Lcom/peel/h/a/a;->ak:I

    invoke-static {v0, v1}, Lcom/peel/util/bx;->a(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/h/a/a;->az:Ljava/lang/String;

    .line 224
    iget-object v0, p0, Lcom/peel/h/a/a;->c:Landroid/os/Bundle;

    invoke-virtual {p0, v0}, Lcom/peel/h/a/a;->c(Landroid/os/Bundle;)V

    .line 225
    return-void

    :cond_0
    move v0, v1

    .line 194
    goto :goto_0

    .line 198
    :cond_1
    const-class v0, Lcom/peel/h/a/fn;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 209
    :sswitch_0
    const-string/jumbo v0, "Power"

    iput-object v0, p0, Lcom/peel/h/a/a;->ay:Ljava/lang/String;

    goto :goto_2

    .line 212
    :sswitch_1
    const-string/jumbo v0, "Channel_Up"

    iput-object v0, p0, Lcom/peel/h/a/a;->ay:Ljava/lang/String;

    goto :goto_2

    .line 215
    :sswitch_2
    const-string/jumbo v0, "PowerOn"

    iput-object v0, p0, Lcom/peel/h/a/a;->ay:Ljava/lang/String;

    goto :goto_2

    .line 203
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_1
        0x3 -> :sswitch_0
        0x4 -> :sswitch_0
        0x5 -> :sswitch_0
        0xa -> :sswitch_0
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public w()V
    .locals 6

    .prologue
    const-wide/16 v4, 0xfa

    .line 265
    invoke-super {p0}, Lcom/peel/d/u;->w()V

    .line 266
    invoke-virtual {p0}, Lcom/peel/h/a/a;->Z()V

    .line 267
    iget-object v0, p0, Lcom/peel/h/a/a;->aK:Lcom/peel/ui/dg;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/peel/h/a/a;->aK:Lcom/peel/ui/dg;

    invoke-virtual {v0}, Lcom/peel/ui/dg;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 268
    iget-object v0, p0, Lcom/peel/h/a/a;->aK:Lcom/peel/ui/dg;

    iget-object v0, v0, Lcom/peel/ui/dg;->b:Landroid/widget/EditText;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/peel/h/a/a;->aK:Lcom/peel/ui/dg;

    iget-object v0, v0, Lcom/peel/ui/dg;->b:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->isFocused()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 269
    invoke-virtual {p0}, Lcom/peel/h/a/a;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    const-class v1, Lcom/peel/h/a/a;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/peel/h/a/a;->aK:Lcom/peel/ui/dg;

    iget-object v2, v2, Lcom/peel/ui/dg;->b:Landroid/widget/EditText;

    invoke-static {v0, v1, v2, v4, v5}, Lcom/peel/util/bx;->a(Landroid/content/Context;Ljava/lang/String;Landroid/widget/EditText;J)V

    .line 276
    :cond_0
    :goto_0
    return-void

    .line 270
    :cond_1
    iget-object v0, p0, Lcom/peel/h/a/a;->aK:Lcom/peel/ui/dg;

    iget-object v0, v0, Lcom/peel/ui/dg;->c:Landroid/widget/EditText;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/peel/h/a/a;->aK:Lcom/peel/ui/dg;

    iget-object v0, v0, Lcom/peel/ui/dg;->c:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->isFocused()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 271
    invoke-virtual {p0}, Lcom/peel/h/a/a;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    const-class v1, Lcom/peel/h/a/a;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/peel/h/a/a;->aK:Lcom/peel/ui/dg;

    iget-object v2, v2, Lcom/peel/ui/dg;->c:Landroid/widget/EditText;

    invoke-static {v0, v1, v2, v4, v5}, Lcom/peel/util/bx;->a(Landroid/content/Context;Ljava/lang/String;Landroid/widget/EditText;J)V

    goto :goto_0

    .line 272
    :cond_2
    iget-object v0, p0, Lcom/peel/h/a/a;->aK:Lcom/peel/ui/dg;

    iget-object v0, v0, Lcom/peel/ui/dg;->d:Landroid/widget/EditText;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/peel/h/a/a;->aK:Lcom/peel/ui/dg;

    iget-object v0, v0, Lcom/peel/ui/dg;->d:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->isFocused()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 273
    invoke-virtual {p0}, Lcom/peel/h/a/a;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    const-class v1, Lcom/peel/h/a/a;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/peel/h/a/a;->aK:Lcom/peel/ui/dg;

    iget-object v2, v2, Lcom/peel/ui/dg;->d:Landroid/widget/EditText;

    invoke-static {v0, v1, v2, v4, v5}, Lcom/peel/util/bx;->a(Landroid/content/Context;Ljava/lang/String;Landroid/widget/EditText;J)V

    goto :goto_0
.end method
