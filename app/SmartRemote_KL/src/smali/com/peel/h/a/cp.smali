.class public Lcom/peel/h/a/cp;
.super Lcom/peel/d/u;


# static fields
.field private static aw:I

.field private static final e:Ljava/lang/String;


# instance fields
.field private aj:Landroid/view/LayoutInflater;

.field private ak:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/peel/data/Channel;",
            ">;"
        }
    .end annotation
.end field

.field private al:Lcom/peel/widget/TestBtnViewPager;

.field private am:Landroid/widget/Button;

.field private an:Landroid/widget/Button;

.field private ao:Landroid/widget/Button;

.field private ap:Landroid/widget/Button;

.field private aq:Landroid/widget/TextView;

.field private ar:Landroid/widget/TextView;

.field private as:Landroid/widget/TextView;

.field private at:Landroid/widget/TextView;

.field private au:Landroid/widget/RelativeLayout;

.field private av:Landroid/widget/RelativeLayout;

.field private f:Ljava/lang/String;

.field private g:Landroid/view/View;

.field private h:I

.field private i:Lcom/peel/content/library/Library;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 47
    const-class v0, Lcom/peel/h/a/cp;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/peel/h/a/cp;->e:Ljava/lang/String;

    .line 61
    const/16 v0, 0xc8

    sput v0, Lcom/peel/h/a/cp;->aw:I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 45
    invoke-direct {p0}, Lcom/peel/d/u;-><init>()V

    .line 308
    return-void
.end method

.method static synthetic S()I
    .locals 1

    .prologue
    .line 45
    sget v0, Lcom/peel/h/a/cp;->aw:I

    return v0
.end method

.method private T()V
    .locals 7

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 249
    iget-object v0, p0, Lcom/peel/h/a/cp;->ar:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget v4, Lcom/peel/ui/ft;->testing_key_tune:I

    invoke-virtual {p0, v4}, Lcom/peel/h/a/cp;->a(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v4, "<br/><b>"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget v4, Lcom/peel/ui/ft;->testing_key_tune_channel_num:I

    new-array v5, v3, [Ljava/lang/Object;

    iget-object v6, p0, Lcom/peel/h/a/cp;->f:Ljava/lang/String;

    aput-object v6, v5, v2

    invoke-virtual {p0, v4, v5}, Lcom/peel/h/a/cp;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v4, "</b>"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 250
    iget-object v0, p0, Lcom/peel/h/a/cp;->at:Landroid/widget/TextView;

    sget v1, Lcom/peel/ui/ft;->tunein_check_question:I

    new-array v4, v3, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/peel/h/a/cp;->f:Ljava/lang/String;

    aput-object v5, v4, v2

    invoke-virtual {p0, v1, v4}, Lcom/peel/h/a/cp;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 251
    iget-object v0, p0, Lcom/peel/h/a/cp;->al:Lcom/peel/widget/TestBtnViewPager;

    invoke-virtual {v0}, Lcom/peel/widget/TestBtnViewPager;->getAdapter()Landroid/support/v4/view/av;

    move-result-object v0

    check-cast v0, Lcom/peel/h/a/cy;

    iget-object v1, p0, Lcom/peel/h/a/cp;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/peel/h/a/cy;->a(Ljava/lang/String;)V

    .line 252
    iget-object v0, p0, Lcom/peel/h/a/cp;->al:Lcom/peel/widget/TestBtnViewPager;

    invoke-virtual {v0, v2}, Lcom/peel/widget/TestBtnViewPager;->setVisibility(I)V

    .line 253
    iget-object v0, p0, Lcom/peel/h/a/cp;->am:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setEnabled(Z)V

    .line 254
    iget-object v0, p0, Lcom/peel/h/a/cp;->an:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setEnabled(Z)V

    .line 256
    invoke-virtual {p0}, Lcom/peel/h/a/cp;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    const-string/jumbo v1, "custom_delay"

    invoke-static {v0, v1}, Lcom/peel/util/dq;->a(Landroid/content/Context;Ljava/lang/String;)I

    move-result v1

    .line 258
    if-lez v1, :cond_0

    move v0, v1

    :goto_0
    sput v0, Lcom/peel/h/a/cp;->aw:I

    .line 261
    if-lez v1, :cond_4

    .line 262
    const/16 v0, 0x32

    if-ne v1, v0, :cond_1

    .line 263
    const/4 v0, 0x2

    .line 271
    :goto_1
    if-lez v0, :cond_3

    .line 272
    iget-object v1, p0, Lcom/peel/h/a/cp;->al:Lcom/peel/widget/TestBtnViewPager;

    invoke-virtual {v1, v0}, Lcom/peel/widget/TestBtnViewPager;->setCurrentItem(I)V

    .line 277
    :goto_2
    return-void

    .line 258
    :cond_0
    const/16 v0, 0xc8

    goto :goto_0

    .line 264
    :cond_1
    const/16 v0, 0x1f4

    if-ne v1, v0, :cond_2

    move v0, v2

    .line 265
    goto :goto_1

    :cond_2
    move v0, v3

    .line 267
    goto :goto_1

    .line 274
    :cond_3
    iget-object v0, p0, Lcom/peel/h/a/cp;->al:Lcom/peel/widget/TestBtnViewPager;

    invoke-virtual {v0, v2}, Lcom/peel/widget/TestBtnViewPager;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    sget v1, Lcom/peel/ui/fp;->test_tune_btn_large_view:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 275
    iget-object v0, p0, Lcom/peel/h/a/cp;->al:Lcom/peel/widget/TestBtnViewPager;

    invoke-virtual {v0, v2}, Lcom/peel/widget/TestBtnViewPager;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    sget v1, Lcom/peel/ui/fp;->test_tune_btn_small_view:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_2

    :cond_4
    move v0, v2

    goto :goto_1
.end method

.method static synthetic a(Lcom/peel/h/a/cp;)Lcom/peel/widget/TestBtnViewPager;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/peel/h/a/cp;->al:Lcom/peel/widget/TestBtnViewPager;

    return-object v0
.end method

.method static synthetic a(Lcom/peel/h/a/cp;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 45
    iput-object p1, p0, Lcom/peel/h/a/cp;->f:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic b(I)I
    .locals 0

    .prologue
    .line 45
    sput p0, Lcom/peel/h/a/cp;->aw:I

    return p0
.end method

.method static synthetic b(Lcom/peel/h/a/cp;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/peel/h/a/cp;->as:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic c(Lcom/peel/h/a/cp;)Landroid/widget/RelativeLayout;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/peel/h/a/cp;->av:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method static synthetic c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 45
    sget-object v0, Lcom/peel/h/a/cp;->e:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic d(Lcom/peel/h/a/cp;)Landroid/widget/RelativeLayout;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/peel/h/a/cp;->au:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method static synthetic e(Lcom/peel/h/a/cp;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/peel/h/a/cp;->f:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic f(Lcom/peel/h/a/cp;)I
    .locals 1

    .prologue
    .line 45
    iget v0, p0, Lcom/peel/h/a/cp;->h:I

    return v0
.end method

.method static synthetic g(Lcom/peel/h/a/cp;)V
    .locals 0

    .prologue
    .line 45
    invoke-direct {p0}, Lcom/peel/h/a/cp;->T()V

    return-void
.end method

.method static synthetic h(Lcom/peel/h/a/cp;)Ljava/util/List;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/peel/h/a/cp;->ak:Ljava/util/List;

    return-object v0
.end method


# virtual methods
.method public Z()V
    .locals 6

    .prologue
    .line 301
    iget-object v0, p0, Lcom/peel/h/a/cp;->d:Lcom/peel/d/a;

    if-nez v0, :cond_0

    .line 302
    new-instance v0, Lcom/peel/d/a;

    sget-object v1, Lcom/peel/d/d;->b:Lcom/peel/d/d;

    sget-object v2, Lcom/peel/d/b;->a:Lcom/peel/d/b;

    sget-object v3, Lcom/peel/d/c;->b:Lcom/peel/d/c;

    sget v4, Lcom/peel/ui/ft;->actionbar_title_tune:I

    invoke-virtual {p0, v4}, Lcom/peel/h/a/cp;->a(I)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, Lcom/peel/d/a;-><init>(Lcom/peel/d/d;Lcom/peel/d/b;Lcom/peel/d/c;Ljava/lang/String;Ljava/util/List;)V

    iput-object v0, p0, Lcom/peel/h/a/cp;->d:Lcom/peel/d/a;

    .line 304
    :cond_0
    iget-object v0, p0, Lcom/peel/h/a/cp;->b:Lcom/peel/d/i;

    iget-object v1, p0, Lcom/peel/h/a/cp;->d:Lcom/peel/d/a;

    invoke-virtual {v0, v1}, Lcom/peel/d/i;->a(Lcom/peel/d/a;)V

    .line 306
    return-void
.end method

.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 93
    sget v0, Lcom/peel/ui/fq;->delay_settings_view:I

    invoke-virtual {p1, v0, p2, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/h/a/cp;->g:Landroid/view/View;

    .line 94
    iget-object v0, p0, Lcom/peel/h/a/cp;->g:Landroid/view/View;

    sget v1, Lcom/peel/ui/fp;->testing_turn_on_msg:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/peel/h/a/cp;->aq:Landroid/widget/TextView;

    .line 95
    iget-object v0, p0, Lcom/peel/h/a/cp;->aq:Landroid/widget/TextView;

    sget v1, Lcom/peel/ui/ft;->testing_tv_msg:I

    new-array v2, v5, [Ljava/lang/Object;

    invoke-static {v5}, Lcom/peel/util/bx;->a(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {p0, v1, v2}, Lcom/peel/h/a/cp;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 96
    iget-object v0, p0, Lcom/peel/h/a/cp;->g:Landroid/view/View;

    sget v1, Lcom/peel/ui/fp;->test_btn_viewpager:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/peel/widget/TestBtnViewPager;

    iput-object v0, p0, Lcom/peel/h/a/cp;->al:Lcom/peel/widget/TestBtnViewPager;

    .line 97
    iget-object v0, p0, Lcom/peel/h/a/cp;->al:Lcom/peel/widget/TestBtnViewPager;

    invoke-virtual {v0, v5}, Lcom/peel/widget/TestBtnViewPager;->setEnabledSwipe(Z)V

    .line 98
    iget-object v0, p0, Lcom/peel/h/a/cp;->al:Lcom/peel/widget/TestBtnViewPager;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/peel/widget/TestBtnViewPager;->setVisibility(I)V

    .line 99
    iget-object v0, p0, Lcom/peel/h/a/cp;->al:Lcom/peel/widget/TestBtnViewPager;

    invoke-virtual {p0}, Lcom/peel/h/a/cp;->n()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/peel/ui/fn;->test_btn_pager_margin:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    mul-int/lit8 v1, v1, 0x2

    invoke-virtual {p0}, Lcom/peel/h/a/cp;->n()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/peel/ui/fn;->test_btn_pager_margin:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v2

    mul-int/lit8 v2, v2, 0x2

    invoke-virtual {v0, v1, v4, v2, v4}, Lcom/peel/widget/TestBtnViewPager;->setPadding(IIII)V

    .line 100
    iget-object v0, p0, Lcom/peel/h/a/cp;->al:Lcom/peel/widget/TestBtnViewPager;

    invoke-virtual {v0, v4}, Lcom/peel/widget/TestBtnViewPager;->setClipToPadding(Z)V

    .line 101
    iget-object v0, p0, Lcom/peel/h/a/cp;->al:Lcom/peel/widget/TestBtnViewPager;

    invoke-virtual {p0}, Lcom/peel/h/a/cp;->n()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/peel/ui/fn;->test_btn_pager_margin:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/peel/widget/TestBtnViewPager;->setPageMargin(I)V

    .line 102
    iget-object v0, p0, Lcom/peel/h/a/cp;->al:Lcom/peel/widget/TestBtnViewPager;

    invoke-virtual {v0, v4}, Lcom/peel/widget/TestBtnViewPager;->setClipChildren(Z)V

    .line 103
    iget-object v0, p0, Lcom/peel/h/a/cp;->al:Lcom/peel/widget/TestBtnViewPager;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/peel/widget/TestBtnViewPager;->setOffscreenPageLimit(I)V

    .line 104
    iget-object v0, p0, Lcom/peel/h/a/cp;->al:Lcom/peel/widget/TestBtnViewPager;

    new-instance v1, Lcom/peel/h/a/cy;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/peel/h/a/cy;-><init>(Lcom/peel/h/a/cp;Lcom/peel/h/a/cq;)V

    invoke-virtual {v0, v1}, Lcom/peel/widget/TestBtnViewPager;->setAdapter(Landroid/support/v4/view/av;)V

    .line 105
    iget-object v0, p0, Lcom/peel/h/a/cp;->g:Landroid/view/View;

    sget v1, Lcom/peel/ui/fp;->coverView:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/peel/h/a/cq;

    invoke-direct {v1, p0}, Lcom/peel/h/a/cq;-><init>(Lcom/peel/h/a/cp;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 111
    iget-object v0, p0, Lcom/peel/h/a/cp;->g:Landroid/view/View;

    sget v1, Lcom/peel/ui/fp;->test_pager_left_btn:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/peel/h/a/cp;->am:Landroid/widget/Button;

    .line 112
    iget-object v0, p0, Lcom/peel/h/a/cp;->am:Landroid/widget/Button;

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setEnabled(Z)V

    .line 113
    iget-object v0, p0, Lcom/peel/h/a/cp;->am:Landroid/widget/Button;

    new-instance v1, Lcom/peel/h/a/cr;

    invoke-direct {v1, p0}, Lcom/peel/h/a/cr;-><init>(Lcom/peel/h/a/cp;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 121
    iget-object v0, p0, Lcom/peel/h/a/cp;->g:Landroid/view/View;

    sget v1, Lcom/peel/ui/fp;->test_pager_right_btn:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/peel/h/a/cp;->an:Landroid/widget/Button;

    .line 122
    iget-object v0, p0, Lcom/peel/h/a/cp;->an:Landroid/widget/Button;

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setEnabled(Z)V

    .line 123
    iget-object v0, p0, Lcom/peel/h/a/cp;->an:Landroid/widget/Button;

    new-instance v1, Lcom/peel/h/a/cs;

    invoke-direct {v1, p0}, Lcom/peel/h/a/cs;-><init>(Lcom/peel/h/a/cp;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 131
    iget-object v0, p0, Lcom/peel/h/a/cp;->g:Landroid/view/View;

    sget v1, Lcom/peel/ui/fp;->test_question_msg:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/peel/h/a/cp;->at:Landroid/widget/TextView;

    .line 132
    iget-object v0, p0, Lcom/peel/h/a/cp;->at:Landroid/widget/TextView;

    sget v1, Lcom/peel/ui/ft;->tunein_check_question:I

    new-array v2, v5, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/peel/h/a/cp;->f:Ljava/lang/String;

    aput-object v3, v2, v4

    invoke-virtual {p0, v1, v2}, Lcom/peel/h/a/cp;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 133
    iget-object v0, p0, Lcom/peel/h/a/cp;->g:Landroid/view/View;

    sget v1, Lcom/peel/ui/fp;->layout_test_btn:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/peel/h/a/cp;->av:Landroid/widget/RelativeLayout;

    .line 134
    iget-object v0, p0, Lcom/peel/h/a/cp;->g:Landroid/view/View;

    sget v1, Lcom/peel/ui/fp;->layout_test_msg:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/peel/h/a/cp;->au:Landroid/widget/RelativeLayout;

    .line 135
    iget-object v0, p0, Lcom/peel/h/a/cp;->av:Landroid/widget/RelativeLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 136
    iget-object v0, p0, Lcom/peel/h/a/cp;->au:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v4}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 137
    iget-object v0, p0, Lcom/peel/h/a/cp;->g:Landroid/view/View;

    sget v1, Lcom/peel/ui/fp;->turn_on_msg:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/peel/h/a/cp;->ar:Landroid/widget/TextView;

    .line 138
    iget-object v0, p0, Lcom/peel/h/a/cp;->g:Landroid/view/View;

    sget v1, Lcom/peel/ui/fp;->test_status_msg:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/peel/h/a/cp;->as:Landroid/widget/TextView;

    .line 139
    iget-object v0, p0, Lcom/peel/h/a/cp;->g:Landroid/view/View;

    sget v1, Lcom/peel/ui/fp;->yes_btn:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/peel/h/a/cp;->ao:Landroid/widget/Button;

    .line 140
    iget-object v0, p0, Lcom/peel/h/a/cp;->g:Landroid/view/View;

    sget v1, Lcom/peel/ui/fp;->no_btn:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/peel/h/a/cp;->ap:Landroid/widget/Button;

    .line 142
    iget-object v0, p0, Lcom/peel/h/a/cp;->ao:Landroid/widget/Button;

    new-instance v1, Lcom/peel/h/a/ct;

    invoke-direct {v1, p0}, Lcom/peel/h/a/ct;-><init>(Lcom/peel/h/a/cp;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 149
    iget-object v0, p0, Lcom/peel/h/a/cp;->ap:Landroid/widget/Button;

    new-instance v1, Lcom/peel/h/a/cu;

    invoke-direct {v1, p0}, Lcom/peel/h/a/cu;-><init>(Lcom/peel/h/a/cp;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 162
    iget-object v0, p0, Lcom/peel/h/a/cp;->al:Lcom/peel/widget/TestBtnViewPager;

    new-instance v1, Lcom/peel/h/a/cv;

    invoke-direct {v1, p0}, Lcom/peel/h/a/cv;-><init>(Lcom/peel/h/a/cp;)V

    invoke-virtual {v0, v1}, Lcom/peel/widget/TestBtnViewPager;->setOnPageChangeListener(Landroid/support/v4/view/cs;)V

    .line 209
    iget-object v0, p0, Lcom/peel/h/a/cp;->f:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 210
    sget-object v0, Lcom/peel/h/a/cp;->e:Ljava/lang/String;

    const-string/jumbo v1, "init view pager"

    new-instance v2, Lcom/peel/h/a/cw;

    invoke-direct {v2, p0}, Lcom/peel/h/a/cw;-><init>(Lcom/peel/h/a/cp;)V

    invoke-static {v0, v1, v2}, Lcom/peel/util/i;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/lang/Runnable;

    .line 245
    :goto_0
    iget-object v0, p0, Lcom/peel/h/a/cp;->g:Landroid/view/View;

    return-object v0

    .line 218
    :cond_0
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 219
    const-string/jumbo v1, "content_user"

    sget-object v2, Lcom/peel/content/a;->f:Lcom/peel/content/user/User;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 220
    const-string/jumbo v1, "path"

    const-string/jumbo v2, "lineup"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 221
    iget-object v1, p0, Lcom/peel/h/a/cp;->i:Lcom/peel/content/library/Library;

    new-instance v2, Lcom/peel/h/a/cx;

    invoke-direct {v2, p0, v5}, Lcom/peel/h/a/cx;-><init>(Lcom/peel/h/a/cp;I)V

    invoke-virtual {v1, v0, v2}, Lcom/peel/content/library/Library;->a(Landroid/os/Bundle;Lcom/peel/util/t;)V

    goto :goto_0
.end method

.method public a_(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 75
    invoke-super {p0, p1}, Lcom/peel/d/u;->a_(Landroid/os/Bundle;)V

    .line 77
    invoke-virtual {p0}, Lcom/peel/h/a/cp;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/h/a/cp;->aj:Landroid/view/LayoutInflater;

    .line 78
    iget-object v0, p0, Lcom/peel/h/a/cp;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "context_id"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/peel/h/a/cp;->h:I

    .line 79
    iget-object v0, p0, Lcom/peel/h/a/cp;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "channel_number"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/h/a/cp;->f:Ljava/lang/String;

    .line 80
    const-string/jumbo v0, "live"

    invoke-static {v0}, Lcom/peel/content/a;->c(Ljava/lang/String;)Lcom/peel/content/library/Library;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/h/a/cp;->i:Lcom/peel/content/library/Library;

    .line 81
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/peel/h/a/cp;->ak:Ljava/util/List;

    .line 83
    iget-object v0, p0, Lcom/peel/h/a/cp;->f:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 84
    invoke-static {}, Lcom/peel/util/a/f;->a()Lcom/peel/util/a/f;

    move-result-object v1

    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v0}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    .line 85
    :goto_0
    const/16 v2, 0x4e8

    const/16 v3, 0x7d8

    .line 84
    invoke-virtual {v1, v0, v2, v3}, Lcom/peel/util/a/f;->a(III)V

    .line 88
    :cond_0
    return-void

    .line 84
    :cond_1
    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    .line 85
    invoke-virtual {v0}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/data/at;->f()I

    move-result v0

    goto :goto_0
.end method

.method public c(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 281
    invoke-super {p0, p1}, Lcom/peel/d/u;->c(Landroid/os/Bundle;)V

    .line 282
    return-void
.end method

.method public d(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 286
    invoke-super {p0, p1}, Lcom/peel/d/u;->d(Landroid/os/Bundle;)V

    .line 287
    sget-object v0, Lcom/peel/content/a;->h:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 288
    iget-object v0, p0, Lcom/peel/h/a/cp;->c:Landroid/os/Bundle;

    invoke-virtual {p0, v0}, Lcom/peel/h/a/cp;->c(Landroid/os/Bundle;)V

    .line 290
    :cond_0
    return-void
.end method

.method public e()V
    .locals 2

    .prologue
    .line 68
    invoke-super {p0}, Lcom/peel/d/u;->e()V

    .line 69
    invoke-virtual {p0}, Lcom/peel/h/a/cp;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/ae;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/view/Window;->setSoftInputMode(I)V

    .line 71
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 14

    .prologue
    const/4 v13, 0x1

    const/4 v1, 0x0

    .line 360
    iget-object v0, p0, Lcom/peel/h/a/cp;->b:Lcom/peel/d/i;

    if-nez v0, :cond_1

    .line 398
    :cond_0
    :goto_0
    return-void

    .line 363
    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    .line 364
    sget v2, Lcom/peel/ui/fp;->tune_channel_btn:I

    if-ne v0, v2, :cond_0

    .line 365
    iget-object v0, p0, Lcom/peel/h/a/cp;->as:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 366
    iget-object v0, p0, Lcom/peel/h/a/cp;->as:Landroid/widget/TextView;

    sget v2, Lcom/peel/ui/ft;->ir_send_code:I

    new-array v3, v13, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/peel/h/a/cp;->al:Lcom/peel/widget/TestBtnViewPager;

    invoke-virtual {v4}, Lcom/peel/widget/TestBtnViewPager;->getCurrentItem()I

    move-result v4

    add-int/lit8 v4, v4, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v1

    invoke-virtual {p0, v2, v3}, Lcom/peel/h/a/cp;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 367
    iget-object v0, p0, Lcom/peel/h/a/cp;->av:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 368
    iget-object v0, p0, Lcom/peel/h/a/cp;->au:Landroid/widget/RelativeLayout;

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 369
    iget-object v0, p0, Lcom/peel/h/a/cp;->f:Ljava/lang/String;

    if-nez v0, :cond_2

    .line 370
    sget-object v0, Lcom/peel/h/a/cp;->e:Ljava/lang/String;

    const-string/jumbo v1, "channel number is NULL, return"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 374
    :cond_2
    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v0}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v0

    .line 375
    if-eqz v0, :cond_0

    .line 377
    invoke-virtual {v0}, Lcom/peel/control/RoomControl;->c()[Lcom/peel/control/a;

    move-result-object v3

    .line 378
    if-eqz v3, :cond_0

    .line 380
    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_0

    aget-object v5, v3, v2

    .line 381
    if-nez v5, :cond_4

    .line 380
    :cond_3
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 383
    :cond_4
    invoke-virtual {v5}, Lcom/peel/control/a;->d()[Ljava/lang/String;

    move-result-object v6

    .line 384
    if-eqz v6, :cond_3

    .line 386
    array-length v7, v6

    move v0, v1

    :goto_2
    if-ge v0, v7, :cond_3

    aget-object v8, v6, v0

    .line 387
    const-string/jumbo v9, "live"

    invoke-virtual {v9, v8}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_5

    .line 388
    invoke-virtual {v5, v13}, Lcom/peel/control/a;->a(I)Lcom/peel/control/h;

    move-result-object v8

    .line 389
    if-nez v8, :cond_6

    .line 386
    :cond_5
    :goto_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 391
    :cond_6
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v10, "live://channel/"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, p0, Lcom/peel/h/a/cp;->f:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string/jumbo v10, "?delayms="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    sget v10, Lcom/peel/h/a/cp;->aw:I

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 392
    sget-object v10, Lcom/peel/h/a/cp;->e:Ljava/lang/String;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v12, "\n ****** LIVE URI: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 393
    invoke-static {v9}, Ljava/net/URI;->create(Ljava/lang/String;)Ljava/net/URI;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/peel/control/h;->a(Ljava/net/URI;)Z

    goto :goto_3
.end method

.method public w()V
    .locals 0

    .prologue
    .line 294
    invoke-super {p0}, Lcom/peel/d/u;->w()V

    .line 295
    invoke-virtual {p0}, Lcom/peel/h/a/cp;->Z()V

    .line 296
    return-void
.end method
