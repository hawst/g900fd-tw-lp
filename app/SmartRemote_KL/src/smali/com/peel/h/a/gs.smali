.class Lcom/peel/h/a/gs;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field final synthetic a:Lcom/peel/h/a/gr;


# direct methods
.method constructor <init>(Lcom/peel/h/a/gr;)V
    .locals 0

    .prologue
    .line 93
    iput-object p1, p0, Lcom/peel/h/a/gs;->a:Lcom/peel/h/a/gr;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 97
    iget-object v0, p0, Lcom/peel/h/a/gs;->a:Lcom/peel/h/a/gr;

    invoke-static {v0}, Lcom/peel/h/a/gr;->a(Lcom/peel/h/a/gr;)Landroid/util/SparseArray;

    move-result-object v0

    invoke-virtual {v0, p3}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    .line 98
    if-nez v0, :cond_1

    .line 133
    :cond_0
    :goto_0
    return-void

    .line 100
    :cond_1
    const-string/jumbo v1, "clazz"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 101
    iget-object v1, p0, Lcom/peel/h/a/gs;->a:Lcom/peel/h/a/gr;

    invoke-virtual {v1}, Lcom/peel/h/a/gr;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    const-string/jumbo v2, "clazz"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lcom/peel/d/e;->c(Landroid/support/v4/app/ae;Ljava/lang/String;Landroid/os/Bundle;)V

    goto :goto_0

    .line 102
    :cond_2
    const-string/jumbo v1, "help_outside"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 103
    iget-object v0, p0, Lcom/peel/h/a/gs;->a:Lcom/peel/h/a/gr;

    new-instance v1, Landroid/content/Intent;

    const-string/jumbo v2, "android.intent.action.VIEW"

    const-string/jumbo v3, "http://help.peel.com/mobile/home"

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {v0, v1}, Lcom/peel/h/a/gr;->a(Landroid/content/Intent;)V

    goto :goto_0

    .line 104
    :cond_3
    const-string/jumbo v1, "twitter"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 105
    iget-object v0, p0, Lcom/peel/h/a/gs;->a:Lcom/peel/h/a/gr;

    invoke-static {v0, p2}, Lcom/peel/h/a/gr;->a(Lcom/peel/h/a/gr;Landroid/view/View;)V

    goto :goto_0

    .line 106
    :cond_4
    const-string/jumbo v1, "account"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 107
    const-string/jumbo v1, "account"

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 108
    sget v1, Lcom/peel/ui/ft;->google_plus_label:I

    if-ne v0, v1, :cond_5

    .line 109
    iget-object v0, p0, Lcom/peel/h/a/gs;->a:Lcom/peel/h/a/gr;

    invoke-static {v0, p2}, Lcom/peel/h/a/gr;->b(Lcom/peel/h/a/gr;Landroid/view/View;)V

    goto :goto_0

    .line 110
    :cond_5
    sget v1, Lcom/peel/ui/ft;->samsung_label:I

    if-ne v0, v1, :cond_6

    .line 111
    iget-object v0, p0, Lcom/peel/h/a/gs;->a:Lcom/peel/h/a/gr;

    invoke-static {v0, p2}, Lcom/peel/h/a/gr;->c(Lcom/peel/h/a/gr;Landroid/view/View;)V

    goto :goto_0

    .line 112
    :cond_6
    sget v1, Lcom/peel/ui/ft;->facebook_label:I

    if-ne v0, v1, :cond_0

    .line 113
    iget-object v0, p0, Lcom/peel/h/a/gs;->a:Lcom/peel/h/a/gr;

    invoke-static {v0, p2}, Lcom/peel/h/a/gr;->d(Lcom/peel/h/a/gr;Landroid/view/View;)V

    goto :goto_0

    .line 115
    :cond_7
    iget-object v1, p0, Lcom/peel/h/a/gs;->a:Lcom/peel/h/a/gr;

    sget v2, Lcom/peel/ui/ft;->samsung_account:I

    invoke-virtual {v1, v2}, Lcom/peel/h/a/gr;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 116
    iget-object v0, p0, Lcom/peel/h/a/gs;->a:Lcom/peel/h/a/gr;

    invoke-static {v0, p2}, Lcom/peel/h/a/gr;->c(Lcom/peel/h/a/gr;Landroid/view/View;)V

    goto/16 :goto_0

    .line 117
    :cond_8
    const-string/jumbo v1, "browse_forum"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_b

    .line 118
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 119
    sget-object v1, Lcom/peel/c/a;->b:Lcom/peel/c/e;

    invoke-static {v1}, Lcom/peel/c/a;->a(Lcom/peel/c/e;)Ljava/lang/Object;

    move-result-object v1

    sget-object v2, Lcom/peel/c/b;->a:Lcom/peel/c/b;

    if-ne v1, v2, :cond_9

    .line 120
    const-string/jumbo v1, "url"

    const-string/jumbo v2, "http://help.peel.com/categories/20150906-Samsung-WatchON"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 126
    :goto_1
    const-string/jumbo v1, "title"

    iget-object v2, p0, Lcom/peel/h/a/gs;->a:Lcom/peel/h/a/gr;

    invoke-virtual {v2}, Lcom/peel/h/a/gr;->n()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/peel/ui/ft;->browseonlinesupport:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 127
    iget-object v1, p0, Lcom/peel/h/a/gs;->a:Lcom/peel/h/a/gr;

    invoke-virtual {v1}, Lcom/peel/h/a/gr;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    const-class v2, Lcom/peel/h/a/fe;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lcom/peel/d/e;->c(Landroid/support/v4/app/ae;Ljava/lang/String;Landroid/os/Bundle;)V

    goto/16 :goto_0

    .line 121
    :cond_9
    sget-object v1, Lcom/peel/c/a;->b:Lcom/peel/c/e;

    invoke-static {v1}, Lcom/peel/c/a;->a(Lcom/peel/c/e;)Ljava/lang/Object;

    move-result-object v1

    sget-object v2, Lcom/peel/c/b;->b:Lcom/peel/c/b;

    if-ne v1, v2, :cond_a

    .line 122
    const-string/jumbo v1, "url"

    const-string/jumbo v2, "http://help.peel.com/categories/20151486-Smart-remote-for-Samsung-Galaxy-S5"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 124
    :cond_a
    const-string/jumbo v1, "url"

    const-string/jumbo v2, "http://help.peel.com/categories/9272-Peel-Smart-remote"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 129
    :cond_b
    const-string/jumbo v1, "about"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 130
    iget-object v0, p0, Lcom/peel/h/a/gs;->a:Lcom/peel/h/a/gr;

    invoke-static {v0}, Lcom/peel/h/a/gr;->b(Lcom/peel/h/a/gr;)V

    goto/16 :goto_0
.end method
