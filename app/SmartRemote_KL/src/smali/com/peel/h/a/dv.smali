.class Lcom/peel/h/a/dv;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Landroid/content/SharedPreferences;

.field final synthetic b:Lcom/peel/h/a/du;


# direct methods
.method constructor <init>(Lcom/peel/h/a/du;Landroid/content/SharedPreferences;)V
    .locals 0

    .prologue
    .line 168
    iput-object p1, p0, Lcom/peel/h/a/dv;->b:Lcom/peel/h/a/du;

    iput-object p2, p0, Lcom/peel/h/a/dv;->a:Landroid/content/SharedPreferences;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 9

    .prologue
    .line 172
    :try_start_0
    iget-object v0, p0, Lcom/peel/h/a/dv;->b:Lcom/peel/h/a/du;

    iget-object v1, p0, Lcom/peel/h/a/dv;->b:Lcom/peel/h/a/du;

    invoke-static {v1}, Lcom/peel/h/a/du;->a(Lcom/peel/h/a/du;)Landroid/widget/EditText;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/peel/h/a/du;->a(Lcom/peel/h/a/du;Ljava/lang/String;)Ljava/lang/String;

    .line 173
    iget-object v0, p0, Lcom/peel/h/a/dv;->a:Landroid/content/SharedPreferences;

    const-string/jumbo v1, "zipcode"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 174
    iget-object v0, p0, Lcom/peel/h/a/dv;->b:Lcom/peel/h/a/du;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/peel/h/a/dv;->b:Lcom/peel/h/a/du;

    invoke-static {v2}, Lcom/peel/h/a/du;->b(Lcom/peel/h/a/du;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "\nZipcode: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/peel/h/a/dv;->a:Landroid/content/SharedPreferences;

    const-string/jumbo v3, "zipcode"

    const-string/jumbo v4, ""

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/peel/h/a/du;->a(Lcom/peel/h/a/du;Ljava/lang/String;)Ljava/lang/String;

    .line 175
    :cond_0
    iget-object v0, p0, Lcom/peel/h/a/dv;->a:Landroid/content/SharedPreferences;

    const-string/jumbo v1, "country"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 176
    iget-object v0, p0, Lcom/peel/h/a/dv;->b:Lcom/peel/h/a/du;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/peel/h/a/dv;->b:Lcom/peel/h/a/du;

    invoke-static {v2}, Lcom/peel/h/a/du;->b(Lcom/peel/h/a/du;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "\nCountry: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/peel/h/a/dv;->a:Landroid/content/SharedPreferences;

    const-string/jumbo v3, "country"

    const-string/jumbo v4, ""

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/peel/h/a/du;->a(Lcom/peel/h/a/du;Ljava/lang/String;)Ljava/lang/String;

    .line 177
    :cond_1
    iget-object v0, p0, Lcom/peel/h/a/dv;->b:Lcom/peel/h/a/du;

    invoke-static {v0}, Lcom/peel/h/a/du;->c(Lcom/peel/h/a/du;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_2

    .line 178
    iget-object v0, p0, Lcom/peel/h/a/dv;->b:Lcom/peel/h/a/du;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/peel/h/a/dv;->b:Lcom/peel/h/a/du;

    invoke-static {v2}, Lcom/peel/h/a/du;->b(Lcom/peel/h/a/du;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "Issue Type: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/peel/h/a/dv;->a:Landroid/content/SharedPreferences;

    const-string/jumbo v3, "issue_type"

    const-string/jumbo v4, ""

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/peel/h/a/du;->a(Lcom/peel/h/a/du;Ljava/lang/String;)Ljava/lang/String;

    .line 180
    :cond_2
    iget-object v1, p0, Lcom/peel/h/a/dv;->b:Lcom/peel/h/a/du;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/peel/h/a/dv;->b:Lcom/peel/h/a/du;

    invoke-static {v2}, Lcom/peel/h/a/du;->b(Lcom/peel/h/a/du;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v2, "\nModel Number: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v0, p0, Lcom/peel/h/a/dv;->b:Lcom/peel/h/a/du;

    invoke-static {v0}, Lcom/peel/h/a/du;->d(Lcom/peel/h/a/du;)Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_b

    iget-object v0, p0, Lcom/peel/h/a/dv;->b:Lcom/peel/h/a/du;

    invoke-static {v0}, Lcom/peel/h/a/du;->d(Lcom/peel/h/a/du;)Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/peel/h/a/du;->a(Lcom/peel/h/a/du;Ljava/lang/String;)Ljava/lang/String;

    .line 182
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 183
    sget-object v2, Lcom/peel/h/a/eb;->a:[I

    sget-object v0, Lcom/peel/c/a;->b:Lcom/peel/c/e;

    invoke-static {v0}, Lcom/peel/c/a;->a(Lcom/peel/c/e;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/c/b;

    invoke-virtual {v0}, Lcom/peel/c/b;->ordinal()I

    move-result v0

    aget v0, v2, v0

    packed-switch v0, :pswitch_data_0

    .line 195
    :goto_1
    iget-object v0, p0, Lcom/peel/h/a/dv;->b:Lcom/peel/h/a/du;

    iget-object v2, p0, Lcom/peel/h/a/dv;->b:Lcom/peel/h/a/du;

    invoke-static {v2}, Lcom/peel/h/a/du;->e(Lcom/peel/h/a/du;)Landroid/widget/Spinner;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Spinner;->getSelectedItem()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/peel/h/a/du;->c(Lcom/peel/h/a/du;Ljava/lang/String;)Ljava/lang/String;

    .line 197
    iget-object v0, p0, Lcom/peel/h/a/dv;->b:Lcom/peel/h/a/du;

    invoke-static {v0}, Lcom/peel/h/a/du;->f(Lcom/peel/h/a/du;)Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/peel/h/a/dv;->b:Lcom/peel/h/a/du;

    sget v3, Lcom/peel/ui/ft;->issue_type1:I

    invoke-virtual {v2, v3}, Lcom/peel/h/a/du;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 198
    iget-object v0, p0, Lcom/peel/h/a/dv;->b:Lcom/peel/h/a/du;

    iget-object v2, p0, Lcom/peel/h/a/dv;->b:Lcom/peel/h/a/du;

    sget v3, Lcom/peel/ui/ft;->tag1:I

    invoke-virtual {v2, v3}, Lcom/peel/h/a/du;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/peel/h/a/du;->d(Lcom/peel/h/a/du;Ljava/lang/String;)Ljava/lang/String;

    .line 199
    :cond_3
    iget-object v0, p0, Lcom/peel/h/a/dv;->b:Lcom/peel/h/a/du;

    invoke-static {v0}, Lcom/peel/h/a/du;->f(Lcom/peel/h/a/du;)Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/peel/h/a/dv;->b:Lcom/peel/h/a/du;

    sget v3, Lcom/peel/ui/ft;->issue_type2:I

    invoke-virtual {v2, v3}, Lcom/peel/h/a/du;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 200
    iget-object v0, p0, Lcom/peel/h/a/dv;->b:Lcom/peel/h/a/du;

    iget-object v2, p0, Lcom/peel/h/a/dv;->b:Lcom/peel/h/a/du;

    sget v3, Lcom/peel/ui/ft;->tag2:I

    invoke-virtual {v2, v3}, Lcom/peel/h/a/du;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/peel/h/a/du;->d(Lcom/peel/h/a/du;Ljava/lang/String;)Ljava/lang/String;

    .line 201
    :cond_4
    iget-object v0, p0, Lcom/peel/h/a/dv;->b:Lcom/peel/h/a/du;

    invoke-static {v0}, Lcom/peel/h/a/du;->f(Lcom/peel/h/a/du;)Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/peel/h/a/dv;->b:Lcom/peel/h/a/du;

    sget v3, Lcom/peel/ui/ft;->issue_type3:I

    invoke-virtual {v2, v3}, Lcom/peel/h/a/du;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 202
    iget-object v0, p0, Lcom/peel/h/a/dv;->b:Lcom/peel/h/a/du;

    iget-object v2, p0, Lcom/peel/h/a/dv;->b:Lcom/peel/h/a/du;

    sget v3, Lcom/peel/ui/ft;->tag3:I

    invoke-virtual {v2, v3}, Lcom/peel/h/a/du;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/peel/h/a/du;->d(Lcom/peel/h/a/du;Ljava/lang/String;)Ljava/lang/String;

    .line 203
    :cond_5
    iget-object v0, p0, Lcom/peel/h/a/dv;->b:Lcom/peel/h/a/du;

    invoke-static {v0}, Lcom/peel/h/a/du;->f(Lcom/peel/h/a/du;)Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/peel/h/a/dv;->b:Lcom/peel/h/a/du;

    sget v3, Lcom/peel/ui/ft;->issue_type4:I

    invoke-virtual {v2, v3}, Lcom/peel/h/a/du;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 204
    iget-object v0, p0, Lcom/peel/h/a/dv;->b:Lcom/peel/h/a/du;

    iget-object v2, p0, Lcom/peel/h/a/dv;->b:Lcom/peel/h/a/du;

    sget v3, Lcom/peel/ui/ft;->tag4:I

    invoke-virtual {v2, v3}, Lcom/peel/h/a/du;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/peel/h/a/du;->d(Lcom/peel/h/a/du;Ljava/lang/String;)Ljava/lang/String;

    .line 205
    :cond_6
    iget-object v0, p0, Lcom/peel/h/a/dv;->b:Lcom/peel/h/a/du;

    invoke-static {v0}, Lcom/peel/h/a/du;->f(Lcom/peel/h/a/du;)Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/peel/h/a/dv;->b:Lcom/peel/h/a/du;

    sget v3, Lcom/peel/ui/ft;->issue_type5:I

    invoke-virtual {v2, v3}, Lcom/peel/h/a/du;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 206
    iget-object v0, p0, Lcom/peel/h/a/dv;->b:Lcom/peel/h/a/du;

    iget-object v2, p0, Lcom/peel/h/a/dv;->b:Lcom/peel/h/a/du;

    sget v3, Lcom/peel/ui/ft;->tag5:I

    invoke-virtual {v2, v3}, Lcom/peel/h/a/du;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/peel/h/a/du;->d(Lcom/peel/h/a/du;Ljava/lang/String;)Ljava/lang/String;

    .line 207
    :cond_7
    iget-object v0, p0, Lcom/peel/h/a/dv;->b:Lcom/peel/h/a/du;

    invoke-static {v0}, Lcom/peel/h/a/du;->f(Lcom/peel/h/a/du;)Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/peel/h/a/dv;->b:Lcom/peel/h/a/du;

    sget v3, Lcom/peel/ui/ft;->issue_type6:I

    invoke-virtual {v2, v3}, Lcom/peel/h/a/du;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 208
    iget-object v0, p0, Lcom/peel/h/a/dv;->b:Lcom/peel/h/a/du;

    iget-object v2, p0, Lcom/peel/h/a/dv;->b:Lcom/peel/h/a/du;

    sget v3, Lcom/peel/ui/ft;->tag6:I

    invoke-virtual {v2, v3}, Lcom/peel/h/a/du;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/peel/h/a/du;->d(Lcom/peel/h/a/du;Ljava/lang/String;)Ljava/lang/String;

    .line 209
    :cond_8
    iget-object v0, p0, Lcom/peel/h/a/dv;->b:Lcom/peel/h/a/du;

    invoke-static {v0}, Lcom/peel/h/a/du;->f(Lcom/peel/h/a/du;)Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/peel/h/a/dv;->b:Lcom/peel/h/a/du;

    sget v3, Lcom/peel/ui/ft;->issue_type7:I

    invoke-virtual {v2, v3}, Lcom/peel/h/a/du;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 210
    iget-object v0, p0, Lcom/peel/h/a/dv;->b:Lcom/peel/h/a/du;

    iget-object v2, p0, Lcom/peel/h/a/dv;->b:Lcom/peel/h/a/du;

    sget v3, Lcom/peel/ui/ft;->tag7:I

    invoke-virtual {v2, v3}, Lcom/peel/h/a/du;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/peel/h/a/du;->d(Lcom/peel/h/a/du;Ljava/lang/String;)Ljava/lang/String;

    .line 211
    :cond_9
    iget-object v0, p0, Lcom/peel/h/a/dv;->b:Lcom/peel/h/a/du;

    invoke-static {v0}, Lcom/peel/h/a/du;->f(Lcom/peel/h/a/du;)Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/peel/h/a/dv;->b:Lcom/peel/h/a/du;

    sget v3, Lcom/peel/ui/ft;->issue_type8:I

    invoke-virtual {v2, v3}, Lcom/peel/h/a/du;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 212
    iget-object v0, p0, Lcom/peel/h/a/dv;->b:Lcom/peel/h/a/du;

    iget-object v2, p0, Lcom/peel/h/a/dv;->b:Lcom/peel/h/a/du;

    sget v3, Lcom/peel/ui/ft;->tag8:I

    invoke-virtual {v2, v3}, Lcom/peel/h/a/du;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/peel/h/a/du;->d(Lcom/peel/h/a/du;Ljava/lang/String;)Ljava/lang/String;

    .line 213
    :cond_a
    const-string/jumbo v0, "desc"

    iget-object v2, p0, Lcom/peel/h/a/dv;->b:Lcom/peel/h/a/du;

    invoke-static {v2}, Lcom/peel/h/a/du;->b(Lcom/peel/h/a/du;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 214
    const-string/jumbo v0, "email"

    iget-object v2, p0, Lcom/peel/h/a/dv;->b:Lcom/peel/h/a/du;

    invoke-static {v2}, Lcom/peel/h/a/du;->g(Lcom/peel/h/a/du;)Landroid/widget/EditText;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 215
    const-string/jumbo v2, "codename"

    sget-object v0, Lcom/peel/c/a;->b:Lcom/peel/c/e;

    invoke-static {v0}, Lcom/peel/c/a;->a(Lcom/peel/c/e;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/c/b;

    invoke-virtual {v0}, Lcom/peel/c/b;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 218
    :try_start_1
    const-string/jumbo v0, "sex"

    sget-object v2, Lcom/peel/content/a;->f:Lcom/peel/content/user/User;

    invoke-virtual {v2}, Lcom/peel/content/user/User;->b()C

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 219
    const-string/jumbo v0, "user-id"

    sget-object v2, Lcom/peel/content/a;->f:Lcom/peel/content/user/User;

    invoke-virtual {v2}, Lcom/peel/content/user/User;->x()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2

    .line 225
    :goto_2
    :try_start_2
    iget-object v0, p0, Lcom/peel/h/a/dv;->b:Lcom/peel/h/a/du;

    invoke-static {v0}, Lcom/peel/h/a/du;->c(Lcom/peel/h/a/du;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_c

    .line 226
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "android handset feedback "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/peel/h/a/dv;->b:Lcom/peel/h/a/du;

    invoke-static {v2}, Lcom/peel/h/a/du;->h(Lcom/peel/h/a/du;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v2, " "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/peel/h/a/dv;->b:Lcom/peel/h/a/du;

    invoke-static {v2}, Lcom/peel/h/a/du;->i(Lcom/peel/h/a/du;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v2, " "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 227
    const-string/jumbo v2, "tags"

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "android handset feedback "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v3, p0, Lcom/peel/h/a/dv;->b:Lcom/peel/h/a/du;

    invoke-static {v3}, Lcom/peel/h/a/du;->h(Lcom/peel/h/a/du;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v3, " "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v3, p0, Lcom/peel/h/a/dv;->b:Lcom/peel/h/a/du;

    invoke-static {v3}, Lcom/peel/h/a/du;->i(Lcom/peel/h/a/du;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v3, " "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v0, Lcom/peel/c/a;->b:Lcom/peel/c/e;

    invoke-static {v0}, Lcom/peel/c/a;->a(Lcom/peel/c/e;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/c/b;

    invoke-virtual {v0}, Lcom/peel/c/b;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    .line 239
    :goto_3
    :try_start_3
    const-string/jumbo v0, "content-string"

    invoke-static {}, Lcom/peel/content/a;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 240
    const-string/jumbo v0, "control-string"

    sget-object v2, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v2}, Lcom/peel/control/am;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    .line 260
    :goto_4
    :try_start_4
    iget-object v0, p0, Lcom/peel/h/a/dv;->b:Lcom/peel/h/a/du;

    invoke-virtual {v0}, Lcom/peel/h/a/du;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    const/4 v2, 0x0

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/io/File;

    const/4 v4, 0x0

    new-instance v5, Ljava/io/File;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v7, p0, Lcom/peel/h/a/dv;->b:Lcom/peel/h/a/du;

    invoke-virtual {v7}, Lcom/peel/h/a/du;->m()Landroid/support/v4/app/ae;

    move-result-object v7

    invoke-virtual {v7}, Landroid/support/v4/app/ae;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v7

    iget-object v7, v7, Landroid/content/pm/ApplicationInfo;->dataDir:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, "/shared_prefs"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v8, p0, Lcom/peel/h/a/dv;->b:Lcom/peel/h/a/du;

    invoke-virtual {v8}, Lcom/peel/h/a/du;->m()Landroid/support/v4/app/ae;

    move-result-object v8

    invoke-virtual {v8}, Landroid/support/v4/app/ae;->getPackageName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, "_preferences.xml"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v5, v6, v7}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v5, v3, v4

    invoke-static {v0, v1, v2, v3}, Lcom/peel/util/c/a;->a(Landroid/content/Context;Ljava/util/HashMap;Ljava/util/Map;[Ljava/io/File;)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0

    .line 266
    :goto_5
    iget-object v0, p0, Lcom/peel/h/a/dv;->b:Lcom/peel/h/a/du;

    invoke-static {v0}, Lcom/peel/h/a/du;->j(Lcom/peel/h/a/du;)Landroid/widget/ViewFlipper;

    move-result-object v0

    sget v1, Lcom/peel/ui/fp;->support_btn:I

    invoke-virtual {v0, v1}, Landroid/widget/ViewFlipper;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/peel/h/a/dw;

    invoke-direct {v1, p0}, Lcom/peel/h/a/dw;-><init>(Lcom/peel/h/a/dv;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 279
    invoke-static {}, Lcom/peel/h/a/du;->c()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "confirmation"

    new-instance v2, Lcom/peel/h/a/dx;

    invoke-direct {v2, p0}, Lcom/peel/h/a/dx;-><init>(Lcom/peel/h/a/dv;)V

    invoke-static {v0, v1, v2}, Lcom/peel/util/i;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/lang/Runnable;

    .line 297
    return-void

    .line 180
    :cond_b
    :try_start_5
    const-string/jumbo v0, "N/A"

    goto/16 :goto_0

    .line 185
    :pswitch_0
    iget-object v0, p0, Lcom/peel/h/a/dv;->b:Lcom/peel/h/a/du;

    const-string/jumbo v2, "S4"

    invoke-static {v0, v2}, Lcom/peel/h/a/du;->b(Lcom/peel/h/a/du;Ljava/lang/String;)Ljava/lang/String;
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0

    goto/16 :goto_1

    .line 262
    :catch_0
    move-exception v0

    .line 263
    invoke-static {}, Lcom/peel/h/a/du;->c()Ljava/lang/String;

    move-result-object v1

    invoke-static {}, Lcom/peel/h/a/du;->c()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_5

    .line 188
    :pswitch_1
    :try_start_6
    iget-object v0, p0, Lcom/peel/h/a/dv;->b:Lcom/peel/h/a/du;

    const-string/jumbo v2, "S5"

    invoke-static {v0, v2}, Lcom/peel/h/a/du;->b(Lcom/peel/h/a/du;Ljava/lang/String;)Ljava/lang/String;

    goto/16 :goto_1

    .line 191
    :pswitch_2
    iget-object v0, p0, Lcom/peel/h/a/dv;->b:Lcom/peel/h/a/du;

    const-string/jumbo v2, "Peel"

    invoke-static {v0, v2}, Lcom/peel/h/a/du;->b(Lcom/peel/h/a/du;Ljava/lang/String;)Ljava/lang/String;

    goto/16 :goto_1

    .line 234
    :cond_c
    const-string/jumbo v2, "subject"

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/peel/h/a/dv;->b:Lcom/peel/h/a/du;

    invoke-virtual {v3}, Lcom/peel/h/a/du;->m()Landroid/support/v4/app/ae;

    move-result-object v3

    invoke-static {v3}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v3

    const-string/jumbo v4, "country"

    const-string/jumbo v5, "N/A"

    invoke-interface {v3, v4, v5}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v3, " -- "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v3, p0, Lcom/peel/h/a/dv;->b:Lcom/peel/h/a/du;

    iget-object v3, v3, Lcom/peel/h/a/du;->c:Landroid/os/Bundle;

    const-string/jumbo v4, "feedback_tags"

    const-string/jumbo v5, ""

    invoke-virtual {v3, v4, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v3, " -- "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v3, p0, Lcom/peel/h/a/dv;->b:Lcom/peel/h/a/du;

    invoke-static {v3}, Lcom/peel/h/a/du;->d(Lcom/peel/h/a/du;)Landroid/widget/EditText;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v3, " -- "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v0, "user-id"

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 235
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "android handset ircode_notworking "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/peel/h/a/dv;->b:Lcom/peel/h/a/du;

    invoke-static {v2}, Lcom/peel/h/a/du;->h(Lcom/peel/h/a/du;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v2, " "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/peel/h/a/dv;->b:Lcom/peel/h/a/du;

    invoke-static {v2}, Lcom/peel/h/a/du;->i(Lcom/peel/h/a/du;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v2, " "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 236
    const-string/jumbo v2, "tags"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v0, Lcom/peel/c/a;->b:Lcom/peel/c/e;

    invoke-static {v0}, Lcom/peel/c/a;->a(Lcom/peel/c/e;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/c/b;

    invoke-virtual {v0}, Lcom/peel/c/b;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_0

    goto/16 :goto_3

    .line 241
    :catch_1
    move-exception v0

    goto/16 :goto_4

    .line 220
    :catch_2
    move-exception v0

    goto/16 :goto_2

    .line 183
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
