.class Lcom/peel/h/a/dm;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/squareup/picasso/Target;


# instance fields
.field final synthetic a:Lcom/peel/h/a/dn;

.field final synthetic b:Lcom/peel/h/a/dl;


# direct methods
.method constructor <init>(Lcom/peel/h/a/dl;Lcom/peel/h/a/dn;)V
    .locals 0

    .prologue
    .line 102
    iput-object p1, p0, Lcom/peel/h/a/dm;->b:Lcom/peel/h/a/dl;

    iput-object p2, p0, Lcom/peel/h/a/dm;->a:Lcom/peel/h/a/dn;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onBitmapFailed(Landroid/graphics/drawable/Drawable;)V
    .locals 0

    .prologue
    .line 111
    return-void
.end method

.method public onBitmapLoaded(Landroid/graphics/Bitmap;Lcom/squareup/picasso/Picasso$LoadedFrom;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 104
    new-instance v0, Landroid/graphics/drawable/BitmapDrawable;

    iget-object v1, p0, Lcom/peel/h/a/dm;->b:Lcom/peel/h/a/dl;

    invoke-static {v1}, Lcom/peel/h/a/dl;->a(Lcom/peel/h/a/dl;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    .line 105
    iget-object v1, p0, Lcom/peel/h/a/dm;->b:Lcom/peel/h/a/dl;

    invoke-static {v1}, Lcom/peel/h/a/dl;->b(Lcom/peel/h/a/dl;)I

    move-result v1

    iget-object v2, p0, Lcom/peel/h/a/dm;->b:Lcom/peel/h/a/dl;

    invoke-static {v2}, Lcom/peel/h/a/dl;->c(Lcom/peel/h/a/dl;)I

    move-result v2

    invoke-virtual {v0, v4, v4, v1, v2}, Landroid/graphics/drawable/BitmapDrawable;->setBounds(IIII)V

    .line 106
    iget-object v1, p0, Lcom/peel/h/a/dm;->a:Lcom/peel/h/a/dn;

    iget-object v1, v1, Lcom/peel/h/a/dn;->a:Landroid/widget/TextView;

    invoke-virtual {v1, v0, v3, v3, v3}, Landroid/widget/TextView;->setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 107
    return-void
.end method

.method public onPrepareLoad(Landroid/graphics/drawable/Drawable;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 114
    iget-object v0, p0, Lcom/peel/h/a/dm;->b:Lcom/peel/h/a/dl;

    invoke-static {v0}, Lcom/peel/h/a/dl;->b(Lcom/peel/h/a/dl;)I

    move-result v0

    iget-object v1, p0, Lcom/peel/h/a/dm;->b:Lcom/peel/h/a/dl;

    invoke-static {v1}, Lcom/peel/h/a/dl;->c(Lcom/peel/h/a/dl;)I

    move-result v1

    invoke-virtual {p1, v3, v3, v0, v1}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 115
    iget-object v0, p0, Lcom/peel/h/a/dm;->a:Lcom/peel/h/a/dn;

    iget-object v0, v0, Lcom/peel/h/a/dn;->a:Landroid/widget/TextView;

    invoke-virtual {v0, p1, v2, v2, v2}, Landroid/widget/TextView;->setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 116
    return-void
.end method
