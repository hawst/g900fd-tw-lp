.class public Lcom/peel/h/a/ec;
.super Lcom/peel/d/u;


# instance fields
.field private aj:Z

.field private ak:Z

.field private al:Lcom/peel/widget/ag;

.field private e:I

.field private f:Lcom/peel/widget/DynamicListView;

.field private g:Lcom/peel/ui/mn;

.field private h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/peel/data/Genre;",
            ">;"
        }
    .end annotation
.end field

.field private i:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 44
    invoke-direct {p0}, Lcom/peel/d/u;-><init>()V

    .line 51
    iput-boolean v0, p0, Lcom/peel/h/a/ec;->i:Z

    iput-boolean v0, p0, Lcom/peel/h/a/ec;->aj:Z

    return-void
.end method

.method static synthetic a(Lcom/peel/h/a/ec;Lcom/peel/widget/ag;)Lcom/peel/widget/ag;
    .locals 0

    .prologue
    .line 44
    iput-object p1, p0, Lcom/peel/h/a/ec;->al:Lcom/peel/widget/ag;

    return-object p1
.end method

.method static synthetic a(Lcom/peel/h/a/ec;)Ljava/util/List;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/peel/h/a/ec;->h:Ljava/util/List;

    return-object v0
.end method

.method static synthetic a(Lcom/peel/h/a/ec;Z)Z
    .locals 0

    .prologue
    .line 44
    iput-boolean p1, p0, Lcom/peel/h/a/ec;->i:Z

    return p1
.end method

.method static synthetic b(Lcom/peel/h/a/ec;)Lcom/peel/ui/mn;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/peel/h/a/ec;->g:Lcom/peel/ui/mn;

    return-object v0
.end method

.method static synthetic c(Lcom/peel/h/a/ec;)I
    .locals 1

    .prologue
    .line 44
    iget v0, p0, Lcom/peel/h/a/ec;->e:I

    return v0
.end method

.method private c()V
    .locals 8

    .prologue
    const/16 v3, 0x7d8

    const/16 v2, 0x7d5

    const/4 v1, 0x1

    .line 148
    const/4 v0, 0x0

    iget-object v4, p0, Lcom/peel/h/a/ec;->h:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v5

    move v4, v0

    :goto_0
    if-ge v4, v5, :cond_0

    .line 149
    iget-object v0, p0, Lcom/peel/h/a/ec;->h:Ljava/util/List;

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/data/Genre;

    invoke-virtual {v0, v4}, Lcom/peel/data/Genre;->a(I)V

    .line 148
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_0

    .line 153
    :cond_0
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 154
    iget-object v0, p0, Lcom/peel/h/a/ec;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_1
    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/data/Genre;

    .line 155
    invoke-virtual {v0}, Lcom/peel/data/Genre;->c()Z

    move-result v6

    if-nez v6, :cond_1

    .line 156
    invoke-virtual {v0}, Lcom/peel/data/Genre;->b()Ljava/lang/String;

    move-result-object v6

    const-string/jumbo v7, "Broadcast Channels"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_1

    .line 157
    invoke-virtual {v0}, Lcom/peel/data/Genre;->b()Ljava/lang/String;

    move-result-object v6

    const-string/jumbo v7, "First Run"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_1

    .line 159
    invoke-virtual {v0}, Lcom/peel/data/Genre;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v6, ","

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 163
    :cond_2
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    if-lez v0, :cond_3

    .line 164
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->deleteCharAt(I)Ljava/lang/StringBuilder;

    .line 167
    :cond_3
    new-instance v5, Landroid/os/Bundle;

    invoke-direct {v5}, Landroid/os/Bundle;-><init>()V

    .line 168
    const-string/jumbo v0, "user"

    sget-object v6, Lcom/peel/content/a;->f:Lcom/peel/content/user/User;

    invoke-virtual {v6}, Lcom/peel/content/user/User;->x()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v0, v6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 169
    const-string/jumbo v0, "genres"

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v5, v0, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 171
    iget v0, p0, Lcom/peel/h/a/ec;->e:I

    if-nez v0, :cond_6

    .line 172
    const-string/jumbo v0, "path"

    const-string/jumbo v4, "genres/program"

    invoke-virtual {v5, v0, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 174
    invoke-static {}, Lcom/peel/util/a/f;->a()Lcom/peel/util/a/f;

    move-result-object v4

    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v0}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v0

    if-nez v0, :cond_4

    move v0, v1

    :goto_2
    const/16 v6, 0xbca

    iget-boolean v1, p0, Lcom/peel/h/a/ec;->ak:Z

    if-eqz v1, :cond_5

    move v1, v2

    :goto_3
    const-string/jumbo v2, "Shows"

    invoke-virtual {v4, v0, v6, v1, v2}, Lcom/peel/util/a/f;->a(IIILjava/lang/String;)V

    .line 187
    :goto_4
    const/4 v0, 0x0

    invoke-static {v5, v0}, Lcom/peel/content/a/j;->b(Landroid/os/Bundle;Lcom/peel/util/t;)V

    .line 189
    sget-object v0, Lcom/peel/content/a;->f:Lcom/peel/content/user/User;

    invoke-virtual {v0}, Lcom/peel/content/user/User;->u()V

    .line 193
    return-void

    .line 174
    :cond_4
    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v0}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/data/at;->f()I

    move-result v0

    goto :goto_2

    :cond_5
    move v1, v3

    goto :goto_3

    .line 179
    :cond_6
    const-string/jumbo v0, "path"

    const-string/jumbo v4, "genres/sports"

    invoke-virtual {v5, v0, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 181
    invoke-static {}, Lcom/peel/util/a/f;->a()Lcom/peel/util/a/f;

    move-result-object v0

    sget-object v4, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v4}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v4

    if-nez v4, :cond_7

    :goto_5
    const/16 v4, 0xbd4

    iget-boolean v6, p0, Lcom/peel/h/a/ec;->ak:Z

    if-eqz v6, :cond_8

    :goto_6
    const-string/jumbo v3, "Sports"

    invoke-virtual {v0, v1, v4, v2, v3}, Lcom/peel/util/a/f;->a(IIILjava/lang/String;)V

    goto :goto_4

    :cond_7
    sget-object v1, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v1}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/data/at;->f()I

    move-result v1

    goto :goto_5

    :cond_8
    move v2, v3

    goto :goto_6
.end method

.method static synthetic d(Lcom/peel/h/a/ec;)Lcom/peel/widget/ag;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/peel/h/a/ec;->al:Lcom/peel/widget/ag;

    return-object v0
.end method


# virtual methods
.method public Z()V
    .locals 7

    .prologue
    .line 259
    iget-object v0, p0, Lcom/peel/h/a/ec;->d:Lcom/peel/d/a;

    if-nez v0, :cond_1

    .line 260
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 261
    iget-boolean v0, p0, Lcom/peel/h/a/ec;->aj:Z

    if-eqz v0, :cond_0

    .line 262
    sget v0, Lcom/peel/ui/fp;->menu_next:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 265
    :cond_0
    new-instance v0, Lcom/peel/d/a;

    sget-object v1, Lcom/peel/d/d;->b:Lcom/peel/d/d;

    sget-object v2, Lcom/peel/d/b;->a:Lcom/peel/d/b;

    sget-object v3, Lcom/peel/d/c;->b:Lcom/peel/d/c;

    iget-object v4, p0, Lcom/peel/h/a/ec;->c:Landroid/os/Bundle;

    const-string/jumbo v6, "category"

    invoke-virtual {v4, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-direct/range {v0 .. v5}, Lcom/peel/d/a;-><init>(Lcom/peel/d/d;Lcom/peel/d/b;Lcom/peel/d/c;Ljava/lang/String;Ljava/util/List;)V

    iput-object v0, p0, Lcom/peel/h/a/ec;->d:Lcom/peel/d/a;

    .line 267
    :cond_1
    iget-object v0, p0, Lcom/peel/h/a/ec;->b:Lcom/peel/d/i;

    iget-object v1, p0, Lcom/peel/h/a/ec;->d:Lcom/peel/d/a;

    invoke-virtual {v0, v1}, Lcom/peel/d/i;->a(Lcom/peel/d/a;)V

    .line 268
    return-void
.end method

.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4

    .prologue
    .line 64
    sget v0, Lcom/peel/ui/fq;->settings_genres:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 65
    sget v0, Lcom/peel/ui/fp;->desc:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/peel/h/a/ec;->n()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/peel/ui/ft;->reordergenres:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 67
    sget v0, Lcom/peel/ui/fp;->genre_list:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/peel/widget/DynamicListView;

    iput-object v0, p0, Lcom/peel/h/a/ec;->f:Lcom/peel/widget/DynamicListView;

    .line 68
    return-object v1
.end method

.method public a(Landroid/view/MenuItem;)Z
    .locals 2

    .prologue
    .line 232
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    .line 233
    sget v1, Lcom/peel/ui/fp;->menu_next:I

    if-ne v0, v1, :cond_0

    .line 234
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    invoke-virtual {p0, v0}, Lcom/peel/h/a/ec;->c(Landroid/os/Bundle;)V

    .line 236
    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method public b()Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 197
    sget-object v1, Lcom/peel/content/a;->h:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/peel/h/a/ec;->b:Lcom/peel/d/i;

    if-nez v1, :cond_2

    :cond_0
    const/4 v0, 0x1

    .line 227
    :cond_1
    :goto_0
    return v0

    .line 199
    :cond_2
    iget-object v1, p0, Lcom/peel/h/a/ec;->c:Landroid/os/Bundle;

    const-string/jumbo v2, "from_setup"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-nez v1, :cond_1

    .line 202
    invoke-direct {p0}, Lcom/peel/h/a/ec;->c()V

    .line 205
    iget v1, p0, Lcom/peel/h/a/ec;->e:I

    if-nez v1, :cond_1

    iget-boolean v1, p0, Lcom/peel/h/a/ec;->aj:Z

    if-eqz v1, :cond_1

    .line 208
    sget-boolean v1, Lcom/peel/util/a;->g:Z

    if-eqz v1, :cond_3

    .line 209
    iget-object v1, p0, Lcom/peel/h/a/ec;->b:Lcom/peel/d/i;

    invoke-virtual {p0}, Lcom/peel/h/a/ec;->m()Landroid/support/v4/app/ae;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/ae;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/peel/util/bx;->b(Lcom/peel/d/i;Landroid/content/Context;)V

    goto :goto_0

    .line 211
    :cond_3
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 212
    const-string/jumbo v2, "type"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 213
    invoke-virtual {p0}, Lcom/peel/h/a/ec;->m()Landroid/support/v4/app/ae;

    move-result-object v2

    const-class v3, Lcom/peel/ui/lq;

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v1}, Lcom/peel/d/e;->a(Landroid/support/v4/app/ae;Ljava/lang/String;Landroid/os/Bundle;)V

    goto :goto_0
.end method

.method public c(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 241
    iget-object v0, p0, Lcom/peel/h/a/ec;->b:Lcom/peel/d/i;

    if-nez v0, :cond_0

    .line 254
    :goto_0
    return-void

    .line 243
    :cond_0
    invoke-direct {p0}, Lcom/peel/h/a/ec;->c()V

    .line 244
    iget v0, p0, Lcom/peel/h/a/ec;->e:I

    if-nez v0, :cond_1

    .line 245
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 246
    const-string/jumbo v1, "type"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 247
    const-string/jumbo v1, "has_next"

    iget-boolean v2, p0, Lcom/peel/h/a/ec;->aj:Z

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 248
    invoke-virtual {p0}, Lcom/peel/h/a/ec;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    const-class v2, Lcom/peel/h/a/ec;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lcom/peel/d/e;->c(Landroid/support/v4/app/ae;Ljava/lang/String;Landroid/os/Bundle;)V

    goto :goto_0

    .line 250
    :cond_1
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 251
    const-string/jumbo v1, "has_done"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 252
    invoke-virtual {p0}, Lcom/peel/h/a/ec;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    const-class v2, Lcom/peel/h/a/av;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lcom/peel/d/e;->c(Landroid/support/v4/app/ae;Ljava/lang/String;Landroid/os/Bundle;)V

    goto :goto_0
.end method

.method public d(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 81
    invoke-super {p0, p1}, Lcom/peel/d/u;->d(Landroid/os/Bundle;)V

    .line 82
    iget-object v0, p0, Lcom/peel/h/a/ec;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "type"

    invoke-virtual {v0, v1, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/peel/h/a/ec;->e:I

    .line 83
    iget-object v0, p0, Lcom/peel/h/a/ec;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "has_next"

    invoke-virtual {v0, v1, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/peel/h/a/ec;->aj:Z

    .line 84
    iget-object v1, p0, Lcom/peel/h/a/ec;->c:Landroid/os/Bundle;

    const-string/jumbo v2, "category"

    iget v0, p0, Lcom/peel/h/a/ec;->e:I

    if-nez v0, :cond_0

    sget v0, Lcom/peel/ui/ft;->header_customize_genres:I

    :goto_0
    invoke-virtual {p0, v0}, Lcom/peel/h/a/ec;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 86
    iget-object v0, p0, Lcom/peel/h/a/ec;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "from_setup"

    invoke-virtual {v0, v1, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/peel/h/a/ec;->ak:Z

    .line 87
    return-void

    .line 84
    :cond_0
    sget v0, Lcom/peel/ui/ft;->header_customize_sports:I

    goto :goto_0
.end method

.method public f()V
    .locals 1

    .prologue
    .line 73
    invoke-super {p0}, Lcom/peel/d/u;->f()V

    .line 74
    iget-object v0, p0, Lcom/peel/h/a/ec;->al:Lcom/peel/widget/ag;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/peel/h/a/ec;->al:Lcom/peel/widget/ag;

    invoke-virtual {v0}, Lcom/peel/widget/ag;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 75
    iget-object v0, p0, Lcom/peel/h/a/ec;->al:Lcom/peel/widget/ag;

    invoke-virtual {v0}, Lcom/peel/widget/ag;->dismiss()V

    .line 77
    :cond_0
    return-void
.end method

.method public h(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 91
    invoke-super {p0, p1}, Lcom/peel/d/u;->h(Landroid/os/Bundle;)V

    .line 93
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/peel/h/a/ec;->h:Ljava/util/List;

    .line 94
    iget-object v1, p0, Lcom/peel/h/a/ec;->h:Ljava/util/List;

    iget v0, p0, Lcom/peel/h/a/ec;->e:I

    if-nez v0, :cond_0

    sget-object v0, Lcom/peel/content/a;->f:Lcom/peel/content/user/User;

    invoke-virtual {v0}, Lcom/peel/content/user/User;->m()Ljava/util/List;

    move-result-object v0

    :goto_0
    invoke-interface {v1, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 97
    new-instance v0, Lcom/peel/ui/mn;

    invoke-virtual {p0}, Lcom/peel/h/a/ec;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    sget v2, Lcom/peel/ui/fq;->dynamic_list_view_row:I

    iget-object v3, p0, Lcom/peel/h/a/ec;->h:Ljava/util/List;

    invoke-direct {v0, v1, v2, v3}, Lcom/peel/ui/mn;-><init>(Landroid/content/Context;ILjava/util/List;)V

    iput-object v0, p0, Lcom/peel/h/a/ec;->g:Lcom/peel/ui/mn;

    .line 98
    iget-object v1, p0, Lcom/peel/h/a/ec;->f:Lcom/peel/widget/DynamicListView;

    iget-object v0, p0, Lcom/peel/h/a/ec;->h:Ljava/util/List;

    check-cast v0, Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Lcom/peel/widget/DynamicListView;->setCheeseList(Ljava/util/ArrayList;)V

    .line 99
    iget-object v0, p0, Lcom/peel/h/a/ec;->f:Lcom/peel/widget/DynamicListView;

    iget-object v1, p0, Lcom/peel/h/a/ec;->g:Lcom/peel/ui/mn;

    invoke-virtual {v0, v1}, Lcom/peel/widget/DynamicListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 100
    iget-object v0, p0, Lcom/peel/h/a/ec;->f:Lcom/peel/widget/DynamicListView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/peel/widget/DynamicListView;->setChoiceMode(I)V

    .line 121
    iget-object v0, p0, Lcom/peel/h/a/ec;->f:Lcom/peel/widget/DynamicListView;

    new-instance v1, Lcom/peel/h/a/ed;

    invoke-direct {v1, p0}, Lcom/peel/h/a/ed;-><init>(Lcom/peel/h/a/ec;)V

    invoke-virtual {v0, v1}, Lcom/peel/widget/DynamicListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 142
    invoke-virtual {p0}, Lcom/peel/h/a/ec;->Z()V

    .line 143
    return-void

    .line 94
    :cond_0
    sget-object v0, Lcom/peel/content/a;->f:Lcom/peel/content/user/User;

    .line 95
    invoke-virtual {v0}, Lcom/peel/content/user/User;->n()Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method

.method public w()V
    .locals 0

    .prologue
    .line 272
    invoke-super {p0}, Lcom/peel/d/u;->w()V

    .line 273
    invoke-virtual {p0}, Lcom/peel/h/a/ec;->Z()V

    .line 274
    return-void
.end method
