.class Lcom/peel/h/a/ga;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic a:Lcom/peel/h/a/fz;


# direct methods
.method constructor <init>(Lcom/peel/h/a/fz;)V
    .locals 0

    .prologue
    .line 538
    iput-object p1, p0, Lcom/peel/h/a/ga;->a:Lcom/peel/h/a/fz;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 541
    iget-object v0, p0, Lcom/peel/h/a/ga;->a:Lcom/peel/h/a/fz;

    iget-object v0, v0, Lcom/peel/h/a/fz;->c:Lcom/peel/h/a/fn;

    invoke-static {v0}, Lcom/peel/h/a/fn;->e(Lcom/peel/h/a/fn;)Lcom/peel/data/ContentRoom;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/h/a/ga;->a:Lcom/peel/h/a/fz;

    iget-object v1, v1, Lcom/peel/h/a/fz;->c:Lcom/peel/h/a/fn;

    invoke-static {v1}, Lcom/peel/h/a/fn;->l(Lcom/peel/h/a/fn;)Lcom/peel/data/ContentRoom;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 542
    iget-object v0, p0, Lcom/peel/h/a/ga;->a:Lcom/peel/h/a/fz;

    iget-object v1, v0, Lcom/peel/h/a/fz;->a:[Lcom/peel/data/ContentRoom;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 543
    iget-object v4, p0, Lcom/peel/h/a/ga;->a:Lcom/peel/h/a/fz;

    iget-object v4, v4, Lcom/peel/h/a/fz;->c:Lcom/peel/h/a/fn;

    invoke-static {v4}, Lcom/peel/h/a/fn;->e(Lcom/peel/h/a/fn;)Lcom/peel/data/ContentRoom;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 542
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 545
    :cond_0
    iget-object v0, p0, Lcom/peel/h/a/ga;->a:Lcom/peel/h/a/fz;

    iget-object v0, v0, Lcom/peel/h/a/fz;->c:Lcom/peel/h/a/fn;

    invoke-static {v0, v3}, Lcom/peel/h/a/fn;->a(Lcom/peel/h/a/fn;Lcom/peel/data/ContentRoom;)Lcom/peel/data/ContentRoom;

    .line 551
    :cond_1
    iget-object v0, p0, Lcom/peel/h/a/ga;->a:Lcom/peel/h/a/fz;

    iget-object v0, v0, Lcom/peel/h/a/fz;->c:Lcom/peel/h/a/fn;

    invoke-static {v0}, Lcom/peel/h/a/fn;->c(Lcom/peel/h/a/fn;)Landroid/widget/CheckedTextView;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/h/a/ga;->a:Lcom/peel/h/a/fz;

    iget-object v1, v1, Lcom/peel/h/a/fz;->c:Lcom/peel/h/a/fn;

    invoke-static {v1}, Lcom/peel/h/a/fn;->e(Lcom/peel/h/a/fn;)Lcom/peel/data/ContentRoom;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/data/ContentRoom;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/CheckedTextView;->setText(Ljava/lang/CharSequence;)V

    .line 557
    iget-object v0, p0, Lcom/peel/h/a/ga;->a:Lcom/peel/h/a/fz;

    iget-object v0, v0, Lcom/peel/h/a/fz;->c:Lcom/peel/h/a/fn;

    invoke-virtual {v0}, Lcom/peel/h/a/fn;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/ae;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/peel/social/w;->f(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 558
    new-instance v0, Lcom/peel/backup/c;

    iget-object v1, p0, Lcom/peel/h/a/ga;->a:Lcom/peel/h/a/fz;

    iget-object v1, v1, Lcom/peel/h/a/fz;->c:Lcom/peel/h/a/fn;

    invoke-virtual {v1}, Lcom/peel/h/a/fn;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/ae;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/peel/backup/c;-><init>(Landroid/content/Context;)V

    .line 559
    iget-object v1, p0, Lcom/peel/h/a/ga;->a:Lcom/peel/h/a/fz;

    iget-object v1, v1, Lcom/peel/h/a/fz;->b:Lcom/peel/control/RoomControl;

    invoke-virtual {v0, v1}, Lcom/peel/backup/c;->a(Lcom/peel/control/RoomControl;)V

    .line 562
    :cond_2
    iget-object v0, p0, Lcom/peel/h/a/ga;->a:Lcom/peel/h/a/fz;

    iget-object v0, v0, Lcom/peel/h/a/fz;->c:Lcom/peel/h/a/fn;

    invoke-static {v0}, Lcom/peel/h/a/fn;->l(Lcom/peel/h/a/fn;)Lcom/peel/data/ContentRoom;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/data/ContentRoom;->a()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lcom/peel/h/a/gb;

    invoke-direct {v1, p0}, Lcom/peel/h/a/gb;-><init>(Lcom/peel/h/a/ga;)V

    invoke-static {v0, v5, v5, v1}, Lcom/peel/content/a;->a(Ljava/lang/String;ZZLcom/peel/util/t;)V

    .line 585
    return-void
.end method
