.class Lcom/peel/h/a/el;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic a:Landroid/widget/EditText;

.field final synthetic b:Lcom/peel/h/a/ej;


# direct methods
.method constructor <init>(Lcom/peel/h/a/ej;Landroid/widget/EditText;)V
    .locals 0

    .prologue
    .line 159
    iput-object p1, p0, Lcom/peel/h/a/el;->b:Lcom/peel/h/a/ej;

    iput-object p2, p0, Lcom/peel/h/a/el;->a:Landroid/widget/EditText;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3

    .prologue
    .line 162
    iget-object v0, p0, Lcom/peel/h/a/el;->a:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/peel/h/a/el;->a:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "Delay"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 163
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 164
    const-string/jumbo v1, "id"

    iget-object v2, p0, Lcom/peel/h/a/el;->b:Lcom/peel/h/a/ej;

    iget-object v2, v2, Lcom/peel/h/a/ej;->c:Lcom/peel/h/a/ei;

    invoke-static {v2}, Lcom/peel/h/a/ei;->a(Lcom/peel/h/a/ei;)Lcom/peel/control/h;

    move-result-object v2

    invoke-virtual {v2}, Lcom/peel/control/h;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 165
    const-string/jumbo v1, "cmd"

    iget-object v2, p0, Lcom/peel/h/a/el;->a:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 167
    iget-object v1, p0, Lcom/peel/h/a/el;->b:Lcom/peel/h/a/ej;

    iget-object v1, v1, Lcom/peel/h/a/ej;->c:Lcom/peel/h/a/ei;

    iget-object v1, v1, Lcom/peel/h/a/ei;->b:Lcom/peel/d/i;

    invoke-virtual {v1}, Lcom/peel/d/i;->c()Landroid/support/v4/app/ae;

    move-result-object v1

    const-class v2, Lcom/peel/h/a/em;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lcom/peel/d/e;->c(Landroid/support/v4/app/ae;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 169
    :cond_0
    return-void
.end method
