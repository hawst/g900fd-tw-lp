.class public Lcom/peel/h/a/fn;
.super Lcom/peel/d/u;


# instance fields
.field private aj:Landroid/widget/Button;

.field private ak:Landroid/widget/Button;

.field private al:Lcom/peel/data/ContentRoom;

.field private am:Lcom/peel/data/ContentRoom;

.field private an:Lcom/peel/control/h;

.field private ao:Lcom/peel/control/h;

.field private ap:Ljava/lang/String;

.field private aq:Lcom/peel/widget/ag;

.field private ar:Lcom/peel/widget/ag;

.field private as:Lcom/peel/widget/ag;

.field private at:Lcom/peel/widget/ag;

.field private final au:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private av:Z

.field private e:Landroid/widget/TextView;

.field private f:Landroid/widget/TextView;

.field private g:Landroid/widget/CheckedTextView;

.field private h:Landroid/widget/LinearLayout;

.field private i:Landroid/view/LayoutInflater;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 57
    invoke-direct {p0}, Lcom/peel/d/u;-><init>()V

    .line 67
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/peel/h/a/fn;->aq:Lcom/peel/widget/ag;

    .line 70
    new-instance v0, Landroid/util/SparseArray;

    const/16 v1, 0xa

    invoke-direct {v0, v1}, Landroid/util/SparseArray;-><init>(I)V

    iput-object v0, p0, Lcom/peel/h/a/fn;->au:Landroid/util/SparseArray;

    return-void
.end method

.method static synthetic a(Lcom/peel/h/a/fn;)Landroid/widget/LinearLayout;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/peel/h/a/fn;->h:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic a(Lcom/peel/h/a/fn;Lcom/peel/data/ContentRoom;)Lcom/peel/data/ContentRoom;
    .locals 0

    .prologue
    .line 57
    iput-object p1, p0, Lcom/peel/h/a/fn;->am:Lcom/peel/data/ContentRoom;

    return-object p1
.end method

.method static synthetic a(Lcom/peel/h/a/fn;Lcom/peel/widget/ag;)Lcom/peel/widget/ag;
    .locals 0

    .prologue
    .line 57
    iput-object p1, p0, Lcom/peel/h/a/fn;->ar:Lcom/peel/widget/ag;

    return-object p1
.end method

.method static synthetic a(Lcom/peel/h/a/fn;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 57
    iput-object p1, p0, Lcom/peel/h/a/fn;->ap:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic a(Lcom/peel/h/a/fn;Lcom/peel/widget/ag;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 57
    invoke-direct {p0, p1, p2}, Lcom/peel/h/a/fn;->a(Lcom/peel/widget/ag;Ljava/lang/String;)V

    return-void
.end method

.method private a(Lcom/peel/widget/ag;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 734
    new-instance v0, Lcom/peel/h/a/fq;

    invoke-direct {v0, p0, p2}, Lcom/peel/h/a/fq;-><init>(Lcom/peel/h/a/fn;Ljava/lang/String;)V

    .line 764
    sget v1, Lcom/peel/ui/ft;->ok:I

    invoke-virtual {p1, v1, v0}, Lcom/peel/widget/ag;->a(ILandroid/content/DialogInterface$OnClickListener;)Lcom/peel/widget/ag;

    .line 766
    return-void
.end method

.method static synthetic b(Lcom/peel/h/a/fn;)Landroid/view/LayoutInflater;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/peel/h/a/fn;->i:Landroid/view/LayoutInflater;

    return-object v0
.end method

.method static synthetic b(Lcom/peel/h/a/fn;Lcom/peel/widget/ag;)Lcom/peel/widget/ag;
    .locals 0

    .prologue
    .line 57
    iput-object p1, p0, Lcom/peel/h/a/fn;->as:Lcom/peel/widget/ag;

    return-object p1
.end method

.method static synthetic c(Lcom/peel/h/a/fn;)Landroid/widget/CheckedTextView;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/peel/h/a/fn;->g:Landroid/widget/CheckedTextView;

    return-object v0
.end method

.method static synthetic c(Lcom/peel/h/a/fn;Lcom/peel/widget/ag;)Lcom/peel/widget/ag;
    .locals 0

    .prologue
    .line 57
    iput-object p1, p0, Lcom/peel/h/a/fn;->at:Lcom/peel/widget/ag;

    return-object p1
.end method

.method private c()V
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 769
    sget-object v0, Lcom/peel/content/a;->h:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/peel/h/a/fn;->g:Landroid/widget/CheckedTextView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/peel/h/a/fn;->al:Lcom/peel/data/ContentRoom;

    if-nez v0, :cond_1

    .line 797
    :cond_0
    :goto_0
    return-void

    .line 772
    :cond_1
    invoke-virtual {p0}, Lcom/peel/h/a/fn;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    const-string/jumbo v2, "input_method"

    invoke-virtual {v0, v2}, Landroid/support/v4/app/ae;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 773
    iget-object v2, p0, Lcom/peel/h/a/fn;->g:Landroid/widget/CheckedTextView;

    invoke-virtual {v2}, Landroid/widget/CheckedTextView;->getWindowToken()Landroid/os/IBinder;

    move-result-object v2

    invoke-virtual {v0, v2, v1}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 775
    iget-object v0, p0, Lcom/peel/h/a/fn;->al:Lcom/peel/data/ContentRoom;

    if-eqz v0, :cond_0

    .line 776
    iget-object v0, p0, Lcom/peel/h/a/fn;->g:Landroid/widget/CheckedTextView;

    invoke-virtual {v0}, Landroid/widget/CheckedTextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    .line 779
    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v0}, Lcom/peel/control/am;->c()[Lcom/peel/control/RoomControl;

    move-result-object v3

    .line 780
    array-length v4, v3

    move v0, v1

    :goto_1
    if-ge v0, v4, :cond_2

    aget-object v1, v3, v0

    .line 781
    invoke-virtual {v1}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v5

    invoke-virtual {v5}, Lcom/peel/data/at;->b()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/peel/h/a/fn;->al:Lcom/peel/data/ContentRoom;

    invoke-virtual {v6}, Lcom/peel/data/ContentRoom;->a()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 782
    invoke-virtual {v1}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/peel/data/at;->a(Ljava/lang/String;)V

    .line 783
    invoke-static {}, Lcom/peel/data/m;->a()Lcom/peel/data/m;

    move-result-object v0

    invoke-virtual {v1}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/peel/data/m;->c(Lcom/peel/data/at;)V

    .line 789
    :cond_2
    iget-object v0, p0, Lcom/peel/h/a/fn;->al:Lcom/peel/data/ContentRoom;

    invoke-virtual {v0, v2}, Lcom/peel/data/ContentRoom;->a(Ljava/lang/String;)V

    .line 790
    sget-object v0, Lcom/peel/content/a;->f:Lcom/peel/content/user/User;

    invoke-virtual {v0}, Lcom/peel/content/user/User;->u()V

    .line 793
    iget-object v0, p0, Lcom/peel/h/a/fn;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "category"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 794
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/peel/h/a/fn;->d:Lcom/peel/d/a;

    .line 795
    invoke-virtual {p0}, Lcom/peel/h/a/fn;->Z()V

    goto :goto_0

    .line 780
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method static synthetic d(Lcom/peel/h/a/fn;)Lcom/peel/widget/ag;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/peel/h/a/fn;->ar:Lcom/peel/widget/ag;

    return-object v0
.end method

.method static synthetic d(Lcom/peel/h/a/fn;Lcom/peel/widget/ag;)Lcom/peel/widget/ag;
    .locals 0

    .prologue
    .line 57
    iput-object p1, p0, Lcom/peel/h/a/fn;->aq:Lcom/peel/widget/ag;

    return-object p1
.end method

.method static synthetic e(Lcom/peel/h/a/fn;)Lcom/peel/data/ContentRoom;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/peel/h/a/fn;->al:Lcom/peel/data/ContentRoom;

    return-object v0
.end method

.method static synthetic f(Lcom/peel/h/a/fn;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/peel/h/a/fn;->ap:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic g(Lcom/peel/h/a/fn;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/peel/h/a/fn;->e:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic h(Lcom/peel/h/a/fn;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/peel/h/a/fn;->f:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic i(Lcom/peel/h/a/fn;)Lcom/peel/control/h;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/peel/h/a/fn;->an:Lcom/peel/control/h;

    return-object v0
.end method

.method static synthetic j(Lcom/peel/h/a/fn;)Lcom/peel/control/h;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/peel/h/a/fn;->ao:Lcom/peel/control/h;

    return-object v0
.end method

.method static synthetic k(Lcom/peel/h/a/fn;)Lcom/peel/widget/ag;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/peel/h/a/fn;->as:Lcom/peel/widget/ag;

    return-object v0
.end method

.method static synthetic l(Lcom/peel/h/a/fn;)Lcom/peel/data/ContentRoom;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/peel/h/a/fn;->am:Lcom/peel/data/ContentRoom;

    return-object v0
.end method

.method static synthetic m(Lcom/peel/h/a/fn;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/peel/h/a/fn;->a:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic n(Lcom/peel/h/a/fn;)Lcom/peel/widget/ag;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/peel/h/a/fn;->at:Lcom/peel/widget/ag;

    return-object v0
.end method

.method static synthetic o(Lcom/peel/h/a/fn;)Lcom/peel/widget/ag;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/peel/h/a/fn;->aq:Lcom/peel/widget/ag;

    return-object v0
.end method

.method static synthetic p(Lcom/peel/h/a/fn;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/peel/h/a/fn;->a:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic q(Lcom/peel/h/a/fn;)V
    .locals 0

    .prologue
    .line 57
    invoke-direct {p0}, Lcom/peel/h/a/fn;->c()V

    return-void
.end method


# virtual methods
.method public Z()V
    .locals 6

    .prologue
    .line 809
    iget-object v0, p0, Lcom/peel/h/a/fn;->d:Lcom/peel/d/a;

    if-nez v0, :cond_0

    .line 810
    new-instance v0, Lcom/peel/d/a;

    sget-object v1, Lcom/peel/d/d;->b:Lcom/peel/d/d;

    sget-object v2, Lcom/peel/d/b;->a:Lcom/peel/d/b;

    sget-object v3, Lcom/peel/d/c;->b:Lcom/peel/d/c;

    iget-object v4, p0, Lcom/peel/h/a/fn;->al:Lcom/peel/data/ContentRoom;

    invoke-virtual {v4}, Lcom/peel/data/ContentRoom;->c()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, Lcom/peel/d/a;-><init>(Lcom/peel/d/d;Lcom/peel/d/b;Lcom/peel/d/c;Ljava/lang/String;Ljava/util/List;)V

    iput-object v0, p0, Lcom/peel/h/a/fn;->d:Lcom/peel/d/a;

    .line 812
    :cond_0
    iget-object v0, p0, Lcom/peel/h/a/fn;->c:Landroid/os/Bundle;

    invoke-virtual {p0, v0}, Lcom/peel/h/a/fn;->c(Landroid/os/Bundle;)V

    .line 813
    iget-object v0, p0, Lcom/peel/h/a/fn;->b:Lcom/peel/d/i;

    iget-object v1, p0, Lcom/peel/h/a/fn;->d:Lcom/peel/d/a;

    invoke-virtual {v0, v1}, Lcom/peel/d/i;->a(Lcom/peel/d/a;)V

    .line 814
    return-void
.end method

.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    .line 75
    iput-object p1, p0, Lcom/peel/h/a/fn;->i:Landroid/view/LayoutInflater;

    .line 76
    sget v0, Lcom/peel/ui/fq;->room_overview:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 77
    sget v0, Lcom/peel/ui/fp;->ll:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/peel/h/a/fn;->h:Landroid/widget/LinearLayout;

    .line 78
    sget v0, Lcom/peel/ui/fp;->add_device:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/peel/h/a/fn;->aj:Landroid/widget/Button;

    .line 79
    sget v0, Lcom/peel/ui/fp;->delete_room:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/peel/h/a/fn;->ak:Landroid/widget/Button;

    .line 80
    return-object v1
.end method

.method public b()Z
    .locals 3

    .prologue
    .line 818
    invoke-virtual {p0}, Lcom/peel/h/a/fn;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    const-class v1, Lcom/peel/h/a/gr;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/peel/h/a/fn;->c:Landroid/os/Bundle;

    invoke-static {v0, v1, v2}, Lcom/peel/d/e;->b(Landroid/support/v4/app/ae;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 819
    const/4 v0, 0x1

    return v0
.end method

.method public c(Landroid/os/Bundle;)V
    .locals 11
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "WrongViewCast"
        }
    .end annotation

    .prologue
    .line 145
    invoke-super {p0, p1}, Lcom/peel/d/u;->c(Landroid/os/Bundle;)V

    .line 147
    sget-object v0, Lcom/peel/content/a;->h:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-nez v0, :cond_1

    .line 730
    :cond_0
    :goto_0
    return-void

    .line 150
    :cond_1
    const-string/jumbo v0, "next_clicked"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 151
    const-string/jumbo v0, "next_clicked"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->remove(Ljava/lang/String;)V

    .line 152
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 153
    const-string/jumbo v1, "add_room"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 154
    const-string/jumbo v1, "back_visibility"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 155
    invoke-virtual {p0}, Lcom/peel/h/a/fn;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    const-class v2, Lcom/peel/i/fq;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lcom/peel/d/e;->b(Landroid/support/v4/app/ae;Ljava/lang/String;Landroid/os/Bundle;)V

    goto :goto_0

    .line 159
    :cond_2
    const-string/jumbo v0, "refresh"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 161
    const-string/jumbo v0, "refresh"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 163
    iget-object v0, p0, Lcom/peel/h/a/fn;->h:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 165
    iget-object v0, p0, Lcom/peel/h/a/fn;->i:Landroid/view/LayoutInflater;

    sget v1, Lcom/peel/ui/fq;->roomoverview_settings_header_row:I

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 166
    sget v0, Lcom/peel/ui/fp;->text:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    sget v2, Lcom/peel/ui/ft;->room_name:I

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    .line 167
    iget-object v0, p0, Lcom/peel/h/a/fn;->h:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 169
    iget-object v0, p0, Lcom/peel/h/a/fn;->i:Landroid/view/LayoutInflater;

    sget v1, Lcom/peel/ui/fq;->roomoverview_settings_edit_room_row:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 170
    sget v0, Lcom/peel/ui/fp;->name:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckedTextView;

    iput-object v0, p0, Lcom/peel/h/a/fn;->g:Landroid/widget/CheckedTextView;

    .line 172
    sget v0, Lcom/peel/ui/fp;->rename_icon:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v2, Lcom/peel/h/a/fo;

    invoke-direct {v2, p0}, Lcom/peel/h/a/fo;-><init>(Lcom/peel/h/a/fn;)V

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 248
    iget-object v0, p0, Lcom/peel/h/a/fn;->h:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 250
    iget-object v0, p0, Lcom/peel/h/a/fn;->g:Landroid/widget/CheckedTextView;

    iget-object v1, p0, Lcom/peel/h/a/fn;->al:Lcom/peel/data/ContentRoom;

    invoke-virtual {v1}, Lcom/peel/data/ContentRoom;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/CheckedTextView;->setText(Ljava/lang/CharSequence;)V

    .line 252
    iget-boolean v0, p0, Lcom/peel/h/a/fn;->av:Z

    if-nez v0, :cond_4

    .line 253
    iget-object v0, p0, Lcom/peel/h/a/fn;->i:Landroid/view/LayoutInflater;

    sget v1, Lcom/peel/ui/fq;->roomoverview_settings_header_row:I

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 254
    sget v0, Lcom/peel/ui/fp;->text:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 255
    sget v2, Lcom/peel/ui/ft;->provider:I

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    .line 256
    iget-object v0, p0, Lcom/peel/h/a/fn;->h:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 258
    iget-object v0, p0, Lcom/peel/h/a/fn;->i:Landroid/view/LayoutInflater;

    sget v1, Lcom/peel/ui/fq;->roomoverview_settings_provider_row:I

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/LinearLayout;

    .line 259
    sget v0, Lcom/peel/ui/fp;->text:I

    invoke-virtual {v7, v0}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    sget v1, Lcom/peel/ui/ft;->provider:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 260
    sget v0, Lcom/peel/ui/fp;->text2:I

    invoke-virtual {v7, v0}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/peel/h/a/fn;->e:Landroid/widget/TextView;

    .line 261
    const/4 v0, 0x0

    invoke-virtual {v7, v0}, Landroid/widget/LinearLayout;->setClickable(Z)V

    .line 262
    iget-object v0, p0, Lcom/peel/h/a/fn;->h:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v7}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 264
    iget-object v0, p0, Lcom/peel/h/a/fn;->i:Landroid/view/LayoutInflater;

    sget v1, Lcom/peel/ui/fq;->roomoverview_settings_provider_row:I

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 265
    sget v0, Lcom/peel/ui/fp;->text:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    sget v2, Lcom/peel/ui/ft;->channels:I

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    .line 266
    sget v0, Lcom/peel/ui/fp;->text2:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/peel/h/a/fn;->f:Landroid/widget/TextView;

    .line 268
    const/4 v0, 0x1

    invoke-virtual {v1, v0}, Landroid/view/View;->setClickable(Z)V

    .line 269
    new-instance v0, Lcom/peel/h/a/fs;

    invoke-direct {v0, p0, v7}, Lcom/peel/h/a/fs;-><init>(Lcom/peel/h/a/fn;Landroid/widget/LinearLayout;)V

    invoke-virtual {v1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 294
    iget-object v0, p0, Lcom/peel/h/a/fn;->h:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 297
    new-instance v3, Landroid/view/View;

    invoke-virtual {p0}, Lcom/peel/h/a/fn;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    invoke-direct {v3, v0}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 298
    iget-object v0, p0, Lcom/peel/h/a/fn;->i:Landroid/view/LayoutInflater;

    sget v1, Lcom/peel/ui/fq;->roomoverview_room_overview_layout:I

    const/4 v2, 0x0

    const/4 v4, 0x0

    invoke-virtual {v0, v1, v2, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v4

    .line 299
    new-instance v5, Landroid/view/View;

    invoke-virtual {p0}, Lcom/peel/h/a/fn;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    invoke-direct {v5, v0}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 300
    iget-object v0, p0, Lcom/peel/h/a/fn;->i:Landroid/view/LayoutInflater;

    sget v1, Lcom/peel/ui/fq;->roomoverview_room_overview_layout:I

    const/4 v2, 0x0

    const/4 v6, 0x0

    invoke-virtual {v0, v1, v2, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v6

    .line 301
    invoke-virtual {p0}, Lcom/peel/h/a/fn;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string/jumbo v1, "country"

    const-string/jumbo v2, ""

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "Japan"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 303
    sget v0, Lcom/peel/ui/fp;->text:I

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    sget v1, Lcom/peel/ui/ft;->preset_keys:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 304
    sget v0, Lcom/peel/ui/fp;->text2:I

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const-string/jumbo v1, "BS"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 305
    sget v0, Lcom/peel/ui/fp;->arrow:I

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 306
    const/4 v0, 0x1

    invoke-virtual {v4, v0}, Landroid/view/View;->setClickable(Z)V

    .line 307
    new-instance v0, Lcom/peel/h/a/ft;

    invoke-direct {v0, p0, v7}, Lcom/peel/h/a/ft;-><init>(Lcom/peel/h/a/fn;Landroid/widget/LinearLayout;)V

    invoke-virtual {v4, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 317
    iget-object v0, p0, Lcom/peel/h/a/fn;->h:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 319
    sget v0, Lcom/peel/ui/fp;->text:I

    invoke-virtual {v6, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    sget v1, Lcom/peel/ui/ft;->preset_keys:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 320
    sget v0, Lcom/peel/ui/fp;->text2:I

    invoke-virtual {v6, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const-string/jumbo v1, "CS"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 321
    sget v0, Lcom/peel/ui/fp;->arrow:I

    invoke-virtual {v6, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 322
    const/4 v0, 0x1

    invoke-virtual {v6, v0}, Landroid/view/View;->setClickable(Z)V

    .line 323
    new-instance v0, Lcom/peel/h/a/fu;

    invoke-direct {v0, p0, v7}, Lcom/peel/h/a/fu;-><init>(Lcom/peel/h/a/fn;Landroid/widget/LinearLayout;)V

    invoke-virtual {v6, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 333
    iget-object v0, p0, Lcom/peel/h/a/fn;->h:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v6}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 336
    :cond_3
    iget-object v0, p0, Lcom/peel/h/a/fn;->al:Lcom/peel/data/ContentRoom;

    invoke-virtual {v0}, Lcom/peel/data/ContentRoom;->a()Ljava/lang/String;

    move-result-object v8

    new-instance v0, Lcom/peel/h/a/fv;

    const/4 v2, 0x1

    move-object v1, p0

    invoke-direct/range {v0 .. v7}, Lcom/peel/h/a/fv;-><init>(Lcom/peel/h/a/fn;ILandroid/view/View;Landroid/view/View;Landroid/view/View;Landroid/view/View;Landroid/widget/LinearLayout;)V

    invoke-static {v8, v0}, Lcom/peel/content/a;->a(Ljava/lang/String;Lcom/peel/util/t;)V

    .line 396
    :cond_4
    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    iget-object v1, p0, Lcom/peel/h/a/fn;->al:Lcom/peel/data/ContentRoom;

    invoke-virtual {v1}, Lcom/peel/data/ContentRoom;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/peel/control/am;->a(Ljava/lang/String;)Lcom/peel/control/RoomControl;

    move-result-object v3

    .line 397
    if-eqz v3, :cond_0

    .line 401
    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v0, v3}, Lcom/peel/control/am;->c(Lcom/peel/control/RoomControl;)[Lcom/peel/control/h;

    move-result-object v4

    .line 403
    if-eqz v4, :cond_15

    array-length v0, v4

    if-lez v0, :cond_15

    .line 404
    iget-object v0, p0, Lcom/peel/h/a/fn;->i:Landroid/view/LayoutInflater;

    sget v1, Lcom/peel/ui/fq;->roomoverview_settings_header_row:I

    const/4 v2, 0x0

    const/4 v5, 0x0

    invoke-virtual {v0, v1, v2, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 405
    sget v0, Lcom/peel/ui/fp;->text:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    sget v2, Lcom/peel/ui/ft;->devices:I

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    .line 406
    iget-object v0, p0, Lcom/peel/h/a/fn;->h:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 408
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    array-length v0, v4

    if-ge v1, v0, :cond_9

    .line 409
    aget-object v2, v4, v1

    .line 410
    iget-object v0, p0, Lcom/peel/h/a/fn;->i:Landroid/view/LayoutInflater;

    sget v5, Lcom/peel/ui/fq;->roomoverview_device_row_layout:I

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual {v0, v5, v6, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v5

    .line 411
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v6

    invoke-virtual {v6}, Lcom/peel/data/g;->f()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v6, " "

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/peel/h/a/fn;->m()Landroid/support/v4/app/ae;

    move-result-object v6

    invoke-virtual {v2}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v7

    invoke-virtual {v7}, Lcom/peel/data/g;->d()I

    move-result v7

    invoke-static {v6, v7}, Lcom/peel/util/bx;->a(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 412
    invoke-virtual {v2}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/data/g;->i()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 413
    sget v0, Lcom/peel/ui/fp;->text:I

    invoke-virtual {v5, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, " ("

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v2}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v7

    invoke-virtual {v7}, Lcom/peel/data/g;->i()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, ")"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 418
    :goto_2
    const/4 v0, 0x1

    invoke-virtual {v2}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v6

    invoke-virtual {v6}, Lcom/peel/data/g;->d()I

    move-result v6

    if-eq v0, v6, :cond_5

    const/16 v0, 0xa

    invoke-virtual {v2}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v6

    invoke-virtual {v6}, Lcom/peel/data/g;->d()I

    move-result v6

    if-ne v0, v6, :cond_8

    .line 420
    :cond_5
    iput-object v2, p0, Lcom/peel/h/a/fn;->an:Lcom/peel/control/h;

    .line 427
    :cond_6
    :goto_3
    new-instance v0, Lcom/peel/h/a/fx;

    invoke-direct {v0, p0, v2}, Lcom/peel/h/a/fx;-><init>(Lcom/peel/h/a/fn;Lcom/peel/control/h;)V

    invoke-virtual {v5, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 437
    iget-object v0, p0, Lcom/peel/h/a/fn;->h:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 408
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto/16 :goto_1

    .line 415
    :cond_7
    sget v0, Lcom/peel/ui/fp;->text:I

    invoke-virtual {v5, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2

    .line 422
    :cond_8
    const/4 v0, 0x5

    invoke-virtual {v2}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v6

    invoke-virtual {v6}, Lcom/peel/data/g;->d()I

    move-result v6

    if-ne v0, v6, :cond_6

    .line 423
    iput-object v2, p0, Lcom/peel/h/a/fn;->ao:Lcom/peel/control/h;

    goto :goto_3

    .line 445
    :cond_9
    iget-object v0, p0, Lcom/peel/h/a/fn;->i:Landroid/view/LayoutInflater;

    sget v1, Lcom/peel/ui/fq;->roomoverview_settings_header_row:I

    const/4 v2, 0x0

    const/4 v5, 0x0

    invoke-virtual {v0, v1, v2, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 446
    sget v0, Lcom/peel/ui/fp;->text:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    sget v2, Lcom/peel/ui/ft;->label_activities:I

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    .line 447
    iget-object v0, p0, Lcom/peel/h/a/fn;->h:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 449
    const/4 v0, 0x0

    move v1, v0

    :goto_4
    invoke-virtual {v3}, Lcom/peel/control/RoomControl;->c()[Lcom/peel/control/a;

    move-result-object v0

    array-length v0, v0

    if-ge v1, v0, :cond_15

    .line 450
    invoke-virtual {v3}, Lcom/peel/control/RoomControl;->c()[Lcom/peel/control/a;

    move-result-object v0

    aget-object v5, v0, v1

    .line 451
    const/4 v0, 0x1

    invoke-virtual {v5, v0}, Lcom/peel/control/a;->a(I)Lcom/peel/control/h;

    move-result-object v2

    .line 452
    if-nez v2, :cond_b

    .line 449
    :cond_a
    :goto_5
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_4

    .line 454
    :cond_b
    invoke-virtual {v2}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/data/g;->d()I

    move-result v0

    const/4 v6, 0x5

    if-eq v0, v6, :cond_a

    invoke-virtual {v2}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/data/g;->d()I

    move-result v0

    const/16 v6, 0x12

    if-eq v0, v6, :cond_a

    .line 458
    iget-object v0, p0, Lcom/peel/h/a/fn;->i:Landroid/view/LayoutInflater;

    sget v6, Lcom/peel/ui/fq;->roomoverview_room_overview_layout:I

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual {v0, v6, v7, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v6

    .line 460
    invoke-virtual {v5}, Lcom/peel/control/a;->a()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v7, "Player"

    const-string/jumbo v8, ""

    invoke-virtual {v0, v7, v8}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 461
    invoke-virtual {v5}, Lcom/peel/control/a;->d()[Ljava/lang/String;

    move-result-object v0

    .line 462
    if-eqz v0, :cond_11

    array-length v8, v0

    if-lez v8, :cond_11

    .line 463
    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    .line 464
    const-string/jumbo v8, "nflx"

    invoke-interface {v0, v8}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_e

    .line 465
    sget v0, Lcom/peel/ui/fp;->text:I

    invoke-virtual {v6, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    sget v7, Lcom/peel/ui/ft;->watch_fmt:I

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    sget v10, Lcom/peel/ui/ft;->netflix_label:I

    invoke-virtual {p0, v10}, Lcom/peel/h/a/fn;->a(I)Ljava/lang/String;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-virtual {p0, v7, v8}, Lcom/peel/h/a/fn;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 474
    :goto_6
    invoke-virtual {v2}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/data/g;->d()I

    move-result v0

    const/4 v7, 0x6

    if-ne v0, v7, :cond_c

    .line 475
    sget v0, Lcom/peel/ui/fp;->text:I

    invoke-virtual {v6, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    sget v7, Lcom/peel/ui/ft;->watch_fmt:I

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    invoke-virtual {v2}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v2

    invoke-virtual {v2}, Lcom/peel/data/g;->f()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v8, v9

    invoke-virtual {p0, v7, v8}, Lcom/peel/h/a/fn;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 478
    :cond_c
    iget-object v0, p0, Lcom/peel/h/a/fn;->an:Lcom/peel/control/h;

    if-eqz v0, :cond_12

    iget-object v0, p0, Lcom/peel/h/a/fn;->an:Lcom/peel/control/h;

    invoke-virtual {v0}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/data/g;->d()I

    move-result v0

    const/16 v2, 0xa

    if-ne v0, v2, :cond_12

    iget-object v0, p0, Lcom/peel/h/a/fn;->ao:Lcom/peel/control/h;

    if-nez v0, :cond_12

    .line 479
    const/4 v0, 0x0

    invoke-virtual {v6, v0}, Landroid/view/View;->setEnabled(Z)V

    .line 480
    const/4 v0, 0x0

    invoke-virtual {v6, v0}, Landroid/view/View;->setClickable(Z)V

    .line 496
    :cond_d
    :goto_7
    new-instance v0, Lcom/peel/h/a/fy;

    invoke-direct {v0, p0, v6, v5}, Lcom/peel/h/a/fy;-><init>(Lcom/peel/h/a/fn;Landroid/view/View;Lcom/peel/control/a;)V

    invoke-virtual {v6, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 527
    iget-object v0, p0, Lcom/peel/h/a/fn;->h:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v6}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    goto/16 :goto_5

    .line 466
    :cond_e
    const-string/jumbo v8, "live"

    invoke-interface {v0, v8}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_f

    const-string/jumbo v8, "dtv"

    invoke-interface {v0, v8}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 467
    :cond_f
    sget v0, Lcom/peel/ui/fp;->text:I

    invoke-virtual {v6, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    sget v7, Lcom/peel/ui/ft;->watch_fmt:I

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    invoke-virtual {v5}, Lcom/peel/control/a;->a()Ljava/lang/String;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-virtual {p0, v7, v8}, Lcom/peel/h/a/fn;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_6

    .line 469
    :cond_10
    sget v0, Lcom/peel/ui/fp;->text:I

    invoke-virtual {v6, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    sget v8, Lcom/peel/ui/ft;->watch_fmt:I

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    aput-object v7, v9, v10

    invoke-virtual {p0, v8, v9}, Lcom/peel/h/a/fn;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_6

    .line 472
    :cond_11
    sget v0, Lcom/peel/ui/fp;->text:I

    invoke-virtual {v6, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    sget v8, Lcom/peel/ui/ft;->watch_fmt:I

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    aput-object v7, v9, v10

    invoke-virtual {p0, v8, v9}, Lcom/peel/h/a/fn;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_6

    .line 483
    :cond_12
    const/4 v0, 0x0

    .line 484
    invoke-virtual {v5}, Lcom/peel/control/a;->e()[Lcom/peel/control/h;

    move-result-object v7

    array-length v8, v7

    const/4 v2, 0x0

    :goto_8
    if-ge v2, v8, :cond_13

    aget-object v9, v7, v2

    .line 485
    invoke-virtual {v5, v9}, Lcom/peel/control/a;->a(Lcom/peel/control/h;)Ljava/lang/String;

    move-result-object v9

    if-eqz v9, :cond_14

    .line 486
    const/4 v0, 0x1

    .line 490
    :cond_13
    if-nez v0, :cond_d

    .line 491
    sget v0, Lcom/peel/ui/fp;->text2:I

    invoke-virtual {v6, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    sget v2, Lcom/peel/ui/ft;->tap_to_config:I

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    goto/16 :goto_7

    .line 484
    :cond_14
    add-int/lit8 v2, v2, 0x1

    goto :goto_8

    .line 531
    :cond_15
    sget-object v0, Lcom/peel/content/a;->f:Lcom/peel/content/user/User;

    invoke-virtual {v0}, Lcom/peel/content/user/User;->l()[Lcom/peel/data/ContentRoom;

    move-result-object v0

    .line 532
    array-length v1, v0

    const/4 v2, 0x1

    if-le v1, v2, :cond_16

    .line 533
    iget-object v1, p0, Lcom/peel/h/a/fn;->ak:Landroid/widget/Button;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 534
    iget-object v1, p0, Lcom/peel/h/a/fn;->ak:Landroid/widget/Button;

    new-instance v2, Lcom/peel/h/a/fz;

    invoke-direct {v2, p0, v0, v3}, Lcom/peel/h/a/fz;-><init>(Lcom/peel/h/a/fn;[Lcom/peel/data/ContentRoom;Lcom/peel/control/RoomControl;)V

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 594
    :cond_16
    new-instance v1, Landroid/util/SparseArray;

    invoke-direct {v1}, Landroid/util/SparseArray;-><init>()V

    .line 595
    const/4 v0, 0x0

    :goto_9
    iget-object v2, p0, Lcom/peel/h/a/fn;->au:Landroid/util/SparseArray;

    invoke-virtual {v2}, Landroid/util/SparseArray;->size()I

    move-result v2

    if-ge v0, v2, :cond_17

    .line 596
    iget-object v2, p0, Lcom/peel/h/a/fn;->au:Landroid/util/SparseArray;

    invoke-virtual {v2, v0}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v2

    .line 597
    iget-object v3, p0, Lcom/peel/h/a/fn;->au:Landroid/util/SparseArray;

    invoke-virtual {v3, v2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 595
    add-int/lit8 v0, v0, 0x1

    goto :goto_9

    .line 618
    :cond_17
    if-eqz v4, :cond_18

    array-length v0, v4

    if-lez v0, :cond_18

    .line 619
    array-length v2, v4

    const/4 v0, 0x0

    :goto_a
    if-ge v0, v2, :cond_18

    aget-object v3, v4, v0

    .line 620
    invoke-virtual {v3}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v5

    invoke-virtual {v5}, Lcom/peel/data/g;->d()I

    move-result v5

    sparse-switch v5, :sswitch_data_0

    .line 635
    invoke-virtual {v3}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v3

    invoke-virtual {v3}, Lcom/peel/data/g;->d()I

    move-result v3

    invoke-virtual {v1, v3}, Landroid/util/SparseArray;->remove(I)V

    .line 619
    :goto_b
    :sswitch_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_a

    .line 626
    :sswitch_1
    const/4 v3, 0x2

    invoke-virtual {v1, v3}, Landroid/util/SparseArray;->remove(I)V

    .line 627
    const/16 v3, 0x14

    invoke-virtual {v1, v3}, Landroid/util/SparseArray;->remove(I)V

    goto :goto_b

    .line 631
    :sswitch_2
    const/4 v3, 0x1

    invoke-virtual {v1, v3}, Landroid/util/SparseArray;->remove(I)V

    .line 632
    const/16 v3, 0xa

    invoke-virtual {v1, v3}, Landroid/util/SparseArray;->remove(I)V

    goto :goto_b

    .line 641
    :cond_18
    iget-object v2, p0, Lcom/peel/h/a/fn;->aj:Landroid/widget/Button;

    if-eqz v1, :cond_19

    invoke-virtual {v1}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-lez v0, :cond_19

    const/4 v0, 0x0

    :goto_c
    invoke-virtual {v2, v0}, Landroid/widget/Button;->setVisibility(I)V

    .line 642
    iget-object v0, p0, Lcom/peel/h/a/fn;->aj:Landroid/widget/Button;

    new-instance v2, Lcom/peel/h/a/gd;

    invoke-direct {v2, p0, p1, v1}, Lcom/peel/h/a/gd;-><init>(Lcom/peel/h/a/fn;Landroid/os/Bundle;Landroid/util/SparseArray;)V

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_0

    .line 641
    :cond_19
    const/16 v0, 0x8

    goto :goto_c

    .line 620
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_2
        0x2 -> :sswitch_1
        0x6 -> :sswitch_0
        0xa -> :sswitch_2
        0x14 -> :sswitch_1
    .end sparse-switch
.end method

.method public d(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 111
    invoke-super {p0, p1}, Lcom/peel/d/u;->d(Landroid/os/Bundle;)V

    .line 112
    iget-object v0, p0, Lcom/peel/h/a/fn;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "room"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/peel/data/ContentRoom;

    iput-object v0, p0, Lcom/peel/h/a/fn;->al:Lcom/peel/data/ContentRoom;

    .line 113
    iget-object v0, p0, Lcom/peel/h/a/fn;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "oldroom"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/peel/data/ContentRoom;

    iput-object v0, p0, Lcom/peel/h/a/fn;->am:Lcom/peel/data/ContentRoom;

    .line 115
    iget-object v0, p0, Lcom/peel/h/a/fn;->al:Lcom/peel/data/ContentRoom;

    if-eqz v0, :cond_0

    .line 116
    iget-object v0, p0, Lcom/peel/h/a/fn;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "category"

    iget-object v2, p0, Lcom/peel/h/a/fn;->al:Lcom/peel/data/ContentRoom;

    invoke-virtual {v2}, Lcom/peel/data/ContentRoom;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 120
    :cond_0
    iget-object v0, p0, Lcom/peel/h/a/fn;->au:Landroid/util/SparseArray;

    sget v1, Lcom/peel/ui/ft;->DeviceType1:I

    invoke-virtual {p0, v1}, Lcom/peel/h/a/fn;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v3, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 121
    iget-object v0, p0, Lcom/peel/h/a/fn;->au:Landroid/util/SparseArray;

    const/4 v1, 0x2

    sget v2, Lcom/peel/ui/ft;->DeviceType2:I

    invoke-virtual {p0, v2}, Lcom/peel/h/a/fn;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 122
    iget-object v0, p0, Lcom/peel/h/a/fn;->au:Landroid/util/SparseArray;

    const/4 v1, 0x3

    sget v2, Lcom/peel/ui/ft;->DeviceType3:I

    invoke-virtual {p0, v2}, Lcom/peel/h/a/fn;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 123
    iget-object v0, p0, Lcom/peel/h/a/fn;->au:Landroid/util/SparseArray;

    const/4 v1, 0x4

    sget v2, Lcom/peel/ui/ft;->DeviceType4:I

    invoke-virtual {p0, v2}, Lcom/peel/h/a/fn;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 124
    iget-object v0, p0, Lcom/peel/h/a/fn;->au:Landroid/util/SparseArray;

    const/4 v1, 0x5

    sget v2, Lcom/peel/ui/ft;->DeviceType5:I

    invoke-virtual {p0, v2}, Lcom/peel/h/a/fn;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 125
    iget-object v0, p0, Lcom/peel/h/a/fn;->au:Landroid/util/SparseArray;

    const/4 v1, 0x6

    sget v2, Lcom/peel/ui/ft;->DeviceType6:I

    invoke-virtual {p0, v2}, Lcom/peel/h/a/fn;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 126
    iget-object v0, p0, Lcom/peel/h/a/fn;->au:Landroid/util/SparseArray;

    const/16 v1, 0xa

    sget v2, Lcom/peel/ui/ft;->DeviceType10:I

    invoke-virtual {p0, v2}, Lcom/peel/h/a/fn;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 127
    iget-object v0, p0, Lcom/peel/h/a/fn;->au:Landroid/util/SparseArray;

    const/16 v1, 0x12

    sget v2, Lcom/peel/ui/ft;->DeviceType18:I

    invoke-virtual {p0, v2}, Lcom/peel/h/a/fn;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 129
    sget-object v0, Lcom/peel/content/a;->h:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 131
    iget-object v0, p0, Lcom/peel/h/a/fn;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "refresh"

    invoke-virtual {v0, v1, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 132
    iget-object v0, p0, Lcom/peel/h/a/fn;->c:Landroid/os/Bundle;

    invoke-virtual {p0, v0}, Lcom/peel/h/a/fn;->c(Landroid/os/Bundle;)V

    .line 134
    :cond_1
    return-void
.end method

.method public f()V
    .locals 3

    .prologue
    .line 94
    invoke-super {p0}, Lcom/peel/d/u;->f()V

    .line 95
    invoke-virtual {p0}, Lcom/peel/h/a/fn;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    const-string/jumbo v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/ae;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 96
    invoke-virtual {p0}, Lcom/peel/h/a/fn;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/ae;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 97
    iget-object v0, p0, Lcom/peel/h/a/fn;->ar:Lcom/peel/widget/ag;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/peel/h/a/fn;->ar:Lcom/peel/widget/ag;

    invoke-virtual {v0}, Lcom/peel/widget/ag;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 99
    iget-object v0, p0, Lcom/peel/h/a/fn;->ar:Lcom/peel/widget/ag;

    invoke-virtual {v0}, Lcom/peel/widget/ag;->dismiss()V

    .line 101
    :cond_0
    iget-object v0, p0, Lcom/peel/h/a/fn;->as:Lcom/peel/widget/ag;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/peel/h/a/fn;->as:Lcom/peel/widget/ag;

    invoke-virtual {v0}, Lcom/peel/widget/ag;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 102
    iget-object v0, p0, Lcom/peel/h/a/fn;->as:Lcom/peel/widget/ag;

    invoke-virtual {v0}, Lcom/peel/widget/ag;->dismiss()V

    .line 104
    :cond_1
    iget-object v0, p0, Lcom/peel/h/a/fn;->at:Lcom/peel/widget/ag;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/peel/h/a/fn;->at:Lcom/peel/widget/ag;

    invoke-virtual {v0}, Lcom/peel/widget/ag;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 105
    iget-object v0, p0, Lcom/peel/h/a/fn;->at:Lcom/peel/widget/ag;

    invoke-virtual {v0}, Lcom/peel/widget/ag;->dismiss()V

    .line 107
    :cond_2
    return-void
.end method

.method public h(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 138
    invoke-super {p0, p1}, Lcom/peel/d/u;->h(Landroid/os/Bundle;)V

    .line 139
    invoke-virtual {p0}, Lcom/peel/h/a/fn;->Z()V

    .line 140
    return-void
.end method

.method public w()V
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 85
    invoke-super {p0}, Lcom/peel/d/u;->w()V

    .line 87
    invoke-virtual {p0}, Lcom/peel/h/a/fn;->m()Landroid/support/v4/app/ae;

    move-result-object v2

    invoke-static {v2}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v2

    const-string/jumbo v3, "setup_type"

    invoke-interface {v2, v3, v1}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v2

    if-ne v0, v2, :cond_0

    :goto_0
    iput-boolean v0, p0, Lcom/peel/h/a/fn;->av:Z

    .line 89
    invoke-virtual {p0}, Lcom/peel/h/a/fn;->Z()V

    .line 90
    return-void

    :cond_0
    move v0, v1

    .line 87
    goto :goto_0
.end method

.method public x()V
    .locals 3

    .prologue
    .line 801
    invoke-virtual {p0}, Lcom/peel/h/a/fn;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    const-string/jumbo v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/ae;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 802
    invoke-virtual {p0}, Lcom/peel/h/a/fn;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/ae;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 803
    invoke-super {p0}, Lcom/peel/d/u;->x()V

    .line 804
    return-void
.end method
