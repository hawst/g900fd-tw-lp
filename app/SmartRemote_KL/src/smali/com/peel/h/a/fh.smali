.class Lcom/peel/h/a/fh;
.super Lcom/peel/util/t;


# instance fields
.field final synthetic a:Lcom/peel/h/a/ff;


# direct methods
.method constructor <init>(Lcom/peel/h/a/ff;I)V
    .locals 0

    .prologue
    .line 202
    iput-object p1, p0, Lcom/peel/h/a/fh;->a:Lcom/peel/h/a/ff;

    invoke-direct {p0, p2}, Lcom/peel/util/t;-><init>(I)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 12

    .prologue
    .line 205
    iget-boolean v0, p0, Lcom/peel/h/a/fh;->i:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/peel/h/a/fh;->j:Ljava/lang/Object;

    if-nez v0, :cond_1

    .line 260
    :cond_0
    return-void

    .line 209
    :cond_1
    iget-object v0, p0, Lcom/peel/h/a/fh;->j:Ljava/lang/Object;

    check-cast v0, Ljava/util/HashMap;

    .line 211
    invoke-virtual {v0}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    .line 214
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 215
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 216
    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/ArrayList;

    .line 219
    iget-object v3, p0, Lcom/peel/h/a/fh;->a:Lcom/peel/h/a/ff;

    invoke-static {v3}, Lcom/peel/h/a/ff;->e(Lcom/peel/h/a/ff;)Landroid/view/LayoutInflater;

    move-result-object v3

    sget v4, Lcom/peel/ui/fq;->tile_bar_personalize:I

    const/4 v6, 0x0

    invoke-virtual {v3, v4, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/LinearLayout;

    .line 220
    sget v4, Lcom/peel/ui/fp;->title:I

    invoke-virtual {v3, v4}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 221
    const-string/jumbo v6, "Pick Your Favorite Shows"

    invoke-virtual {v1, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 222
    iget-object v1, p0, Lcom/peel/h/a/fh;->a:Lcom/peel/h/a/ff;

    invoke-virtual {v1}, Lcom/peel/h/a/ff;->n()Landroid/content/res/Resources;

    move-result-object v1

    sget v6, Lcom/peel/ui/ft;->pick_top_fav_shows:I

    invoke-virtual {v1, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 254
    :cond_2
    :goto_1
    iget-object v1, p0, Lcom/peel/h/a/fh;->a:Lcom/peel/h/a/ff;

    new-instance v4, Lcom/peel/ui/d;

    iget-object v6, p0, Lcom/peel/h/a/fh;->a:Lcom/peel/h/a/ff;

    invoke-virtual {v6}, Lcom/peel/h/a/ff;->m()Landroid/support/v4/app/ae;

    move-result-object v6

    const/4 v7, -0x1

    invoke-direct {v4, v6, v7, v2}, Lcom/peel/ui/d;-><init>(Landroid/content/Context;ILjava/util/List;)V

    invoke-static {v1, v4}, Lcom/peel/h/a/ff;->a(Lcom/peel/h/a/ff;Lcom/peel/ui/d;)Lcom/peel/ui/d;

    .line 255
    iget-object v2, p0, Lcom/peel/h/a/fh;->a:Lcom/peel/h/a/ff;

    sget v1, Lcom/peel/ui/fp;->tiles_grid:I

    invoke-virtual {v3, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/jess/ui/TwoWayGridView;

    invoke-static {v2, v1}, Lcom/peel/h/a/ff;->a(Lcom/peel/h/a/ff;Lcom/jess/ui/TwoWayGridView;)Lcom/jess/ui/TwoWayGridView;

    .line 256
    iget-object v1, p0, Lcom/peel/h/a/fh;->a:Lcom/peel/h/a/ff;

    invoke-static {v1}, Lcom/peel/h/a/ff;->h(Lcom/peel/h/a/ff;)Lcom/jess/ui/TwoWayGridView;

    move-result-object v1

    iget-object v2, p0, Lcom/peel/h/a/fh;->a:Lcom/peel/h/a/ff;

    invoke-static {v2}, Lcom/peel/h/a/ff;->g(Lcom/peel/h/a/ff;)Lcom/peel/ui/d;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/jess/ui/TwoWayGridView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 257
    iget-object v1, p0, Lcom/peel/h/a/fh;->a:Lcom/peel/h/a/ff;

    invoke-static {v1}, Lcom/peel/h/a/ff;->h(Lcom/peel/h/a/ff;)Lcom/jess/ui/TwoWayGridView;

    move-result-object v1

    iget-object v2, p0, Lcom/peel/h/a/fh;->a:Lcom/peel/h/a/ff;

    invoke-static {v2}, Lcom/peel/h/a/ff;->i(Lcom/peel/h/a/ff;)Lcom/jess/ui/z;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/jess/ui/TwoWayGridView;->setOnItemClickListener(Lcom/jess/ui/z;)V

    .line 258
    iget-object v1, p0, Lcom/peel/h/a/fh;->a:Lcom/peel/h/a/ff;

    invoke-static {v1}, Lcom/peel/h/a/ff;->j(Lcom/peel/h/a/ff;)Landroid/widget/LinearLayout;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    goto/16 :goto_0

    .line 223
    :cond_3
    const-string/jumbo v6, "Pick Your Favorite Channels"

    invoke-virtual {v1, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 224
    iget-object v1, p0, Lcom/peel/h/a/fh;->a:Lcom/peel/h/a/ff;

    invoke-virtual {v1}, Lcom/peel/h/a/ff;->n()Landroid/content/res/Resources;

    move-result-object v1

    sget v6, Lcom/peel/ui/ft;->pick_top_fav_channels:I

    invoke-virtual {v1, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 226
    sget-object v1, Lcom/peel/content/a;->f:Lcom/peel/content/user/User;

    invoke-virtual {v1}, Lcom/peel/content/user/User;->h()Landroid/os/Bundle;

    move-result-object v1

    .line 227
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 228
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 229
    if-eqz v1, :cond_4

    .line 232
    const-string/jumbo v7, "favchannels"

    invoke-virtual {v1, v7}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v7

    if-eqz v7, :cond_4

    .line 233
    const-string/jumbo v7, "favchannels"

    invoke-virtual {v1, v7}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 234
    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 235
    const-string/jumbo v7, "#"

    invoke-virtual {v1, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    const/4 v7, 0x1

    aget-object v1, v1, v7

    invoke-virtual {v6, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 240
    :cond_4
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_5
    :goto_3
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/HashMap;

    .line 241
    invoke-virtual {v1}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_4
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_6

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 242
    iget-object v9, p0, Lcom/peel/h/a/fh;->a:Lcom/peel/h/a/ff;

    invoke-static {v9}, Lcom/peel/h/a/ff;->f(Lcom/peel/h/a/ff;)Ljava/lang/String;

    move-result-object v9

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v11, "map prgsvice: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string/jumbo v11, "  "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    .line 243
    invoke-virtual {v1, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-virtual {v10, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 242
    invoke-static {v9, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_4

    .line 245
    :cond_6
    const-string/jumbo v4, "source"

    invoke-virtual {v1, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v6, v4}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 246
    const-string/jumbo v4, "isFavorite"

    const-string/jumbo v8, "true"

    invoke-virtual {v1, v4, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_3

    .line 252
    :cond_7
    invoke-virtual {v4, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1
.end method
