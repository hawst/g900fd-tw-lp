.class Lcom/peel/h/a/x;
.super Lcom/peel/util/t;


# instance fields
.field final synthetic a:Lcom/peel/control/RoomControl;

.field final synthetic b:Lcom/peel/h/a/a;


# direct methods
.method constructor <init>(Lcom/peel/h/a/a;ILcom/peel/control/RoomControl;)V
    .locals 0

    .prologue
    .line 307
    iput-object p1, p0, Lcom/peel/h/a/x;->b:Lcom/peel/h/a/a;

    iput-object p3, p0, Lcom/peel/h/a/x;->a:Lcom/peel/control/RoomControl;

    invoke-direct {p0, p2}, Lcom/peel/util/t;-><init>(I)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 311
    const-class v0, Lcom/peel/h/a/a;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/h/a/x;->b:Lcom/peel/h/a/a;

    invoke-virtual {v1}, Lcom/peel/h/a/a;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    invoke-static {v0, v1, v2}, Lcom/peel/util/bx;->a(Ljava/lang/String;Landroid/app/Activity;Z)V

    .line 313
    iget-boolean v0, p0, Lcom/peel/h/a/x;->i:Z

    if-nez v0, :cond_3

    .line 314
    iget-object v0, p0, Lcom/peel/h/a/x;->k:Ljava/lang/String;

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/peel/h/a/a;->c()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "getBrandsByDeviceType: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/peel/h/a/x;->k:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 315
    :cond_0
    sget-boolean v0, Lcom/peel/util/b/a;->a:Z

    if-eqz v0, :cond_2

    .line 316
    iget-object v0, p0, Lcom/peel/h/a/x;->b:Lcom/peel/h/a/a;

    invoke-static {v0}, Lcom/peel/h/a/a;->e(Lcom/peel/h/a/a;)V

    .line 340
    :cond_1
    :goto_0
    return-void

    .line 318
    :cond_2
    iget-object v0, p0, Lcom/peel/h/a/x;->b:Lcom/peel/h/a/a;

    invoke-static {v0}, Lcom/peel/h/a/a;->f(Lcom/peel/h/a/a;)V

    goto :goto_0

    .line 323
    :cond_3
    iget-object v1, p0, Lcom/peel/h/a/x;->b:Lcom/peel/h/a/a;

    iget-object v0, p0, Lcom/peel/h/a/x;->j:Ljava/lang/Object;

    check-cast v0, Ljava/util/List;

    invoke-static {v1, v0}, Lcom/peel/h/a/a;->a(Lcom/peel/h/a/a;Ljava/util/List;)Ljava/util/List;

    .line 325
    iget-object v0, p0, Lcom/peel/h/a/x;->b:Lcom/peel/h/a/a;

    invoke-static {v0}, Lcom/peel/h/a/a;->c(Lcom/peel/h/a/a;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    move v1, v2

    .line 327
    :goto_1
    iget-object v0, p0, Lcom/peel/h/a/x;->b:Lcom/peel/h/a/a;

    invoke-static {v0}, Lcom/peel/h/a/a;->c(Lcom/peel/h/a/a;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_4

    .line 328
    iget-object v0, p0, Lcom/peel/h/a/x;->b:Lcom/peel/h/a/a;

    invoke-static {v0}, Lcom/peel/h/a/a;->c(Lcom/peel/h/a/a;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/f/a;

    invoke-virtual {v0}, Lcom/peel/f/a;->c()I

    move-result v0

    const/16 v3, 0x3e7

    if-ne v0, v3, :cond_5

    .line 329
    iget-object v0, p0, Lcom/peel/h/a/x;->b:Lcom/peel/h/a/a;

    iget-object v3, p0, Lcom/peel/h/a/x;->b:Lcom/peel/h/a/a;

    invoke-static {v3}, Lcom/peel/h/a/a;->c(Lcom/peel/h/a/a;)Ljava/util/List;

    move-result-object v3

    iget-object v4, p0, Lcom/peel/h/a/x;->b:Lcom/peel/h/a/a;

    invoke-static {v4}, Lcom/peel/h/a/a;->c(Lcom/peel/h/a/a;)Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    invoke-interface {v3, v2, v4}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/peel/h/a/a;->b(Lcom/peel/h/a/a;Ljava/util/List;)Ljava/util/List;

    .line 330
    iget-object v0, p0, Lcom/peel/h/a/x;->b:Lcom/peel/h/a/a;

    iget-object v3, p0, Lcom/peel/h/a/x;->b:Lcom/peel/h/a/a;

    invoke-static {v3}, Lcom/peel/h/a/a;->c(Lcom/peel/h/a/a;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3, v2, v1}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/peel/h/a/a;->a(Lcom/peel/h/a/a;Ljava/util/List;)Ljava/util/List;

    .line 335
    :cond_4
    iget-object v0, p0, Lcom/peel/h/a/x;->b:Lcom/peel/h/a/a;

    new-instance v1, Lcom/peel/i/a/a;

    iget-object v2, p0, Lcom/peel/h/a/x;->b:Lcom/peel/h/a/a;

    invoke-virtual {v2}, Lcom/peel/h/a/a;->m()Landroid/support/v4/app/ae;

    move-result-object v2

    sget v3, Lcom/peel/ui/fq;->brand_row:I

    iget-object v4, p0, Lcom/peel/h/a/x;->b:Lcom/peel/h/a/a;

    invoke-static {v4}, Lcom/peel/h/a/a;->c(Lcom/peel/h/a/a;)Ljava/util/List;

    move-result-object v4

    invoke-direct {v1, v2, v3, v4}, Lcom/peel/i/a/a;-><init>(Landroid/content/Context;ILjava/util/List;)V

    invoke-static {v0, v1}, Lcom/peel/h/a/a;->a(Lcom/peel/h/a/a;Lcom/peel/i/a/a;)Lcom/peel/i/a/a;

    .line 336
    iget-object v0, p0, Lcom/peel/h/a/x;->b:Lcom/peel/h/a/a;

    invoke-static {v0}, Lcom/peel/h/a/a;->a(Lcom/peel/h/a/a;)Landroid/widget/ListView;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/h/a/x;->b:Lcom/peel/h/a/a;

    invoke-static {v1}, Lcom/peel/h/a/a;->b(Lcom/peel/h/a/a;)Lcom/peel/i/a/a;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 338
    iget-object v0, p0, Lcom/peel/h/a/x;->b:Lcom/peel/h/a/a;

    invoke-static {v0}, Lcom/peel/h/a/a;->c(Lcom/peel/h/a/a;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_1

    .line 339
    iget-object v0, p0, Lcom/peel/h/a/x;->b:Lcom/peel/h/a/a;

    iget-object v1, p0, Lcom/peel/h/a/x;->a:Lcom/peel/control/RoomControl;

    invoke-static {v0, v1}, Lcom/peel/h/a/a;->b(Lcom/peel/h/a/a;Lcom/peel/control/RoomControl;)V

    goto/16 :goto_0

    .line 327
    :cond_5
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto/16 :goto_1
.end method
