.class Lcom/peel/h/a/n;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic a:Ljava/lang/StringBuilder;

.field final synthetic b:Lcom/peel/control/RoomControl;

.field final synthetic c:Lcom/peel/h/a/a;


# direct methods
.method constructor <init>(Lcom/peel/h/a/a;Ljava/lang/StringBuilder;Lcom/peel/control/RoomControl;)V
    .locals 0

    .prologue
    .line 1126
    iput-object p1, p0, Lcom/peel/h/a/n;->c:Lcom/peel/h/a/a;

    iput-object p2, p0, Lcom/peel/h/a/n;->a:Ljava/lang/StringBuilder;

    iput-object p3, p0, Lcom/peel/h/a/n;->b:Lcom/peel/control/RoomControl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3

    .prologue
    .line 1129
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 1130
    const-string/jumbo v1, "cycle_activities"

    iget-object v2, p0, Lcom/peel/h/a/n;->a:Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1131
    const-string/jumbo v1, "room"

    iget-object v2, p0, Lcom/peel/h/a/n;->b:Lcom/peel/control/RoomControl;

    invoke-virtual {v2}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v2

    invoke-virtual {v2}, Lcom/peel/data/at;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1132
    const-string/jumbo v1, "back_to_clazz"

    iget-object v2, p0, Lcom/peel/h/a/n;->c:Lcom/peel/h/a/a;

    invoke-static {v2}, Lcom/peel/h/a/a;->K(Lcom/peel/h/a/a;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1133
    iget-object v1, p0, Lcom/peel/h/a/n;->c:Lcom/peel/h/a/a;

    invoke-virtual {v1}, Lcom/peel/h/a/a;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    const-class v2, Lcom/peel/h/a/bz;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lcom/peel/d/e;->b(Landroid/support/v4/app/ae;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 1134
    return-void
.end method
