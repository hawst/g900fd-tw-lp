.class Lcom/peel/h/a/ed;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field final synthetic a:Lcom/peel/h/a/ec;


# direct methods
.method constructor <init>(Lcom/peel/h/a/ec;)V
    .locals 0

    .prologue
    .line 121
    iput-object p1, p0, Lcom/peel/h/a/ed;->a:Lcom/peel/h/a/ec;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 124
    iget-object v0, p0, Lcom/peel/h/a/ed;->a:Lcom/peel/h/a/ec;

    invoke-static {v0}, Lcom/peel/h/a/ec;->a(Lcom/peel/h/a/ec;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/data/Genre;

    iget-object v1, p0, Lcom/peel/h/a/ed;->a:Lcom/peel/h/a/ec;

    invoke-static {v1}, Lcom/peel/h/a/ec;->a(Lcom/peel/h/a/ec;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, p3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/peel/data/Genre;

    invoke-virtual {v1}, Lcom/peel/data/Genre;->c()Z

    move-result v1

    if-nez v1, :cond_1

    move v1, v2

    :goto_0
    invoke-virtual {v0, v1}, Lcom/peel/data/Genre;->a(Z)V

    .line 125
    iget-object v0, p0, Lcom/peel/h/a/ed;->a:Lcom/peel/h/a/ec;

    invoke-static {v0}, Lcom/peel/h/a/ec;->a(Lcom/peel/h/a/ec;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/data/Genre;

    .line 126
    invoke-virtual {v0}, Lcom/peel/data/Genre;->c()Z

    move-result v0

    if-nez v0, :cond_0

    .line 127
    iget-object v0, p0, Lcom/peel/h/a/ed;->a:Lcom/peel/h/a/ec;

    invoke-static {v0}, Lcom/peel/h/a/ec;->b(Lcom/peel/h/a/ec;)Lcom/peel/ui/mn;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/ui/mn;->notifyDataSetChanged()V

    .line 128
    iget-object v0, p0, Lcom/peel/h/a/ed;->a:Lcom/peel/h/a/ec;

    invoke-static {v0, v2}, Lcom/peel/h/a/ec;->a(Lcom/peel/h/a/ec;Z)Z

    .line 139
    :goto_1
    return-void

    :cond_1
    move v1, v3

    .line 124
    goto :goto_0

    .line 132
    :cond_2
    iget-object v0, p0, Lcom/peel/h/a/ed;->a:Lcom/peel/h/a/ec;

    invoke-static {v0}, Lcom/peel/h/a/ec;->a(Lcom/peel/h/a/ec;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/data/Genre;

    invoke-virtual {v0, v3}, Lcom/peel/data/Genre;->a(Z)V

    .line 133
    iget-object v0, p0, Lcom/peel/h/a/ed;->a:Lcom/peel/h/a/ec;

    new-instance v1, Lcom/peel/widget/ag;

    iget-object v2, p0, Lcom/peel/h/a/ed;->a:Lcom/peel/h/a/ec;

    invoke-virtual {v2}, Lcom/peel/h/a/ec;->m()Landroid/support/v4/app/ae;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/peel/widget/ag;-><init>(Landroid/content/Context;)V

    sget v2, Lcom/peel/ui/ft;->error:I

    invoke-virtual {v1, v2}, Lcom/peel/widget/ag;->a(I)Lcom/peel/widget/ag;

    move-result-object v1

    sget v2, Lcom/peel/ui/ft;->okay:I

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Lcom/peel/widget/ag;->a(ILandroid/content/DialogInterface$OnClickListener;)Lcom/peel/widget/ag;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/peel/h/a/ec;->a(Lcom/peel/h/a/ec;Lcom/peel/widget/ag;)Lcom/peel/widget/ag;

    .line 134
    iget-object v0, p0, Lcom/peel/h/a/ed;->a:Lcom/peel/h/a/ec;

    invoke-static {v0}, Lcom/peel/h/a/ec;->c(Lcom/peel/h/a/ec;)I

    move-result v0

    if-nez v0, :cond_3

    .line 135
    iget-object v0, p0, Lcom/peel/h/a/ed;->a:Lcom/peel/h/a/ec;

    invoke-static {v0}, Lcom/peel/h/a/ec;->d(Lcom/peel/h/a/ec;)Lcom/peel/widget/ag;

    move-result-object v0

    sget v1, Lcom/peel/ui/ft;->genre_error:I

    invoke-virtual {v0, v1}, Lcom/peel/widget/ag;->b(I)Lcom/peel/widget/ag;

    .line 138
    :goto_2
    iget-object v0, p0, Lcom/peel/h/a/ed;->a:Lcom/peel/h/a/ec;

    invoke-static {v0}, Lcom/peel/h/a/ec;->d(Lcom/peel/h/a/ec;)Lcom/peel/widget/ag;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/h/a/ed;->a:Lcom/peel/h/a/ec;

    invoke-static {v1}, Lcom/peel/h/a/ec;->d(Lcom/peel/h/a/ec;)Lcom/peel/widget/ag;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/peel/widget/ag;->a(Lcom/peel/widget/ag;)Lcom/peel/widget/ag;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/widget/ag;->show()V

    goto :goto_1

    .line 137
    :cond_3
    iget-object v0, p0, Lcom/peel/h/a/ed;->a:Lcom/peel/h/a/ec;

    invoke-static {v0}, Lcom/peel/h/a/ec;->d(Lcom/peel/h/a/ec;)Lcom/peel/widget/ag;

    move-result-object v0

    sget v1, Lcom/peel/ui/ft;->sports_error:I

    invoke-virtual {v0, v1}, Lcom/peel/widget/ag;->b(I)Lcom/peel/widget/ag;

    goto :goto_2
.end method
