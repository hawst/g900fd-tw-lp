.class Lcom/peel/h/a/as;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# instance fields
.field final synthetic a:Lcom/peel/h/a/au;

.field final synthetic b:Lcom/peel/h/a/aq;


# direct methods
.method constructor <init>(Lcom/peel/h/a/aq;Lcom/peel/h/a/au;)V
    .locals 0

    .prologue
    .line 177
    iput-object p1, p0, Lcom/peel/h/a/as;->b:Lcom/peel/h/a/aq;

    iput-object p2, p0, Lcom/peel/h/a/as;->a:Lcom/peel/h/a/au;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 5

    .prologue
    const/4 v3, 0x0

    const/4 v4, 0x0

    .line 180
    iget-object v0, p0, Lcom/peel/h/a/as;->a:Lcom/peel/h/a/au;

    invoke-virtual {v0}, Lcom/peel/h/a/au;->f()Z

    move-result v0

    if-eq p2, v0, :cond_0

    .line 181
    if-eqz p2, :cond_1

    .line 182
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 183
    const-string/jumbo v1, "wifi_name"

    iget-object v2, p0, Lcom/peel/h/a/as;->a:Lcom/peel/h/a/au;

    invoke-virtual {v2}, Lcom/peel/h/a/au;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 184
    const-string/jumbo v1, "wifi_bssid"

    iget-object v2, p0, Lcom/peel/h/a/as;->a:Lcom/peel/h/a/au;

    invoke-virtual {v2}, Lcom/peel/h/a/au;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 185
    const-string/jumbo v1, "type"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 186
    const-string/jumbo v1, "timestamp"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 187
    const-string/jumbo v1, "name"

    iget-object v2, p0, Lcom/peel/h/a/as;->a:Lcom/peel/h/a/au;

    invoke-virtual {v2}, Lcom/peel/h/a/au;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 188
    iget-object v1, p0, Lcom/peel/h/a/as;->b:Lcom/peel/h/a/aq;

    iget-object v1, v1, Lcom/peel/h/a/aq;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Lcom/peel/provider/a;->a:Landroid/net/Uri;

    invoke-virtual {v1, v2, v0}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    .line 191
    iget-object v0, p0, Lcom/peel/h/a/as;->a:Lcom/peel/h/a/au;

    invoke-virtual {v0}, Lcom/peel/h/a/au;->c()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/h/a/as;->b:Lcom/peel/h/a/aq;

    iget-object v1, v1, Lcom/peel/h/a/aq;->b:Lcom/peel/h/a/am;

    iget-object v1, v1, Lcom/peel/h/a/am;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/peel/h/a/as;->b:Lcom/peel/h/a/aq;

    iget-object v0, v0, Lcom/peel/h/a/aq;->b:Lcom/peel/h/a/am;

    invoke-static {v0}, Lcom/peel/h/a/am;->d(Lcom/peel/h/a/am;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string/jumbo v1, "lockscreen_enabled"

    invoke-interface {v0, v1, v4}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 192
    iget-object v0, p0, Lcom/peel/h/a/as;->b:Lcom/peel/h/a/aq;

    iget-object v0, v0, Lcom/peel/h/a/aq;->b:Lcom/peel/h/a/am;

    invoke-virtual {v0}, Lcom/peel/h/a/am;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    const-string/jumbo v2, "com.android.internal.policy.impl.keyguard.sec.REMOTE_ADDED"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/support/v4/app/ae;->sendBroadcast(Landroid/content/Intent;)V

    .line 201
    :cond_0
    :goto_0
    return-void

    .line 195
    :cond_1
    iget-object v0, p0, Lcom/peel/h/a/as;->b:Lcom/peel/h/a/aq;

    iget-object v0, v0, Lcom/peel/h/a/aq;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "content://com.peel.provider.MyLocation/locations/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/peel/h/a/as;->a:Lcom/peel/h/a/au;

    invoke-virtual {v2}, Lcom/peel/h/a/au;->a()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1, v3, v3}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 196
    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/peel/h/a/as;->a:Lcom/peel/h/a/au;

    invoke-virtual {v0}, Lcom/peel/h/a/au;->c()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/h/a/as;->b:Lcom/peel/h/a/aq;

    iget-object v1, v1, Lcom/peel/h/a/aq;->b:Lcom/peel/h/a/am;

    iget-object v1, v1, Lcom/peel/h/a/am;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/peel/h/a/as;->b:Lcom/peel/h/a/aq;

    iget-object v0, v0, Lcom/peel/h/a/aq;->b:Lcom/peel/h/a/am;

    invoke-static {v0}, Lcom/peel/h/a/am;->d(Lcom/peel/h/a/am;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string/jumbo v1, "lockscreen_enabled"

    invoke-interface {v0, v1, v4}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 197
    iget-object v0, p0, Lcom/peel/h/a/as;->b:Lcom/peel/h/a/aq;

    iget-object v0, v0, Lcom/peel/h/a/aq;->b:Lcom/peel/h/a/am;

    invoke-virtual {v0}, Lcom/peel/h/a/am;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    const-string/jumbo v2, "com.android.internal.policy.impl.keyguard.sec.REMOTE_REMOVED"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/support/v4/app/ae;->sendBroadcast(Landroid/content/Intent;)V

    goto :goto_0
.end method
