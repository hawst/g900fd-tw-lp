.class Lcom/peel/h/a/dd;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic a:Lcom/peel/h/a/dc;


# direct methods
.method constructor <init>(Lcom/peel/h/a/dc;)V
    .locals 0

    .prologue
    .line 168
    iput-object p1, p0, Lcom/peel/h/a/dd;->a:Lcom/peel/h/a/dc;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 12

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 171
    iget-object v0, p0, Lcom/peel/h/a/dd;->a:Lcom/peel/h/a/dc;

    iget-object v0, v0, Lcom/peel/h/a/dc;->c:Lcom/peel/h/a/cz;

    invoke-virtual {v0}, Lcom/peel/h/a/cz;->s()Z

    move-result v0

    if-nez v0, :cond_0

    .line 243
    :goto_0
    return-void

    .line 174
    :cond_0
    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v0}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/control/RoomControl;->d()Lcom/peel/control/a;

    move-result-object v5

    .line 175
    if-eqz v5, :cond_3

    invoke-virtual {v5}, Lcom/peel/control/a;->b()Ljava/lang/String;

    move-result-object v0

    .line 178
    :goto_1
    iget-object v1, p0, Lcom/peel/h/a/dd;->a:Lcom/peel/h/a/dc;

    iget-object v1, v1, Lcom/peel/h/a/dc;->c:Lcom/peel/h/a/cz;

    invoke-static {v1}, Lcom/peel/h/a/cz;->b(Lcom/peel/h/a/cz;)[Lcom/peel/control/a;

    move-result-object v6

    array-length v7, v6

    move v4, v3

    move v1, v3

    :goto_2
    if-ge v4, v7, :cond_4

    aget-object v8, v6, v4

    .line 179
    invoke-static {}, Lcom/peel/h/a/cz;->c()Ljava/lang/String;

    move-result-object v9

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v11, " ***************** in activity: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v8}, Lcom/peel/control/a;->a()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 180
    invoke-virtual {v8, v2}, Lcom/peel/control/a;->a(I)Lcom/peel/control/h;

    move-result-object v9

    .line 181
    invoke-virtual {v9}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v9

    invoke-virtual {v9}, Lcom/peel/data/g;->b()Ljava/lang/String;

    move-result-object v9

    iget-object v10, p0, Lcom/peel/h/a/dd;->a:Lcom/peel/h/a/dc;

    iget-object v10, v10, Lcom/peel/h/a/dc;->a:Lcom/peel/control/h;

    invoke-virtual {v10}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v10

    invoke-virtual {v10}, Lcom/peel/data/g;->b()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 182
    invoke-static {}, Lcom/peel/h/a/cz;->c()Ljava/lang/String;

    move-result-object v9

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v11, " ***************** id matches: removing activity: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v8}, Lcom/peel/control/a;->a()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 183
    if-eqz v0, :cond_1

    invoke-virtual {v8}, Lcom/peel/control/a;->b()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v0, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_1

    .line 185
    sget-object v1, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v1}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v1

    invoke-virtual {v1, v3}, Lcom/peel/control/RoomControl;->a(I)Z

    move v1, v2

    .line 188
    :cond_1
    sget-object v9, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    iget-object v10, p0, Lcom/peel/h/a/dd;->a:Lcom/peel/h/a/dc;

    iget-object v10, v10, Lcom/peel/h/a/dc;->c:Lcom/peel/h/a/cz;

    invoke-static {v10}, Lcom/peel/h/a/cz;->c(Lcom/peel/h/a/cz;)Lcom/peel/data/ContentRoom;

    move-result-object v10

    invoke-virtual {v10}, Lcom/peel/data/ContentRoom;->d()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Lcom/peel/control/am;->a(Ljava/lang/String;)Lcom/peel/control/RoomControl;

    move-result-object v9

    invoke-virtual {v9}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v9

    invoke-virtual {v8}, Lcom/peel/control/a;->c()Lcom/peel/data/d;

    move-result-object v10

    invoke-virtual {v9, v10}, Lcom/peel/data/at;->b(Lcom/peel/data/d;)V

    .line 189
    sget-object v9, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    iget-object v10, p0, Lcom/peel/h/a/dd;->a:Lcom/peel/h/a/dc;

    iget-object v10, v10, Lcom/peel/h/a/dc;->c:Lcom/peel/h/a/cz;

    invoke-static {v10}, Lcom/peel/h/a/cz;->c(Lcom/peel/h/a/cz;)Lcom/peel/data/ContentRoom;

    move-result-object v10

    invoke-virtual {v10}, Lcom/peel/data/ContentRoom;->d()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Lcom/peel/control/am;->a(Ljava/lang/String;)Lcom/peel/control/RoomControl;

    move-result-object v9

    invoke-virtual {v9, v8}, Lcom/peel/control/RoomControl;->b(Lcom/peel/control/a;)V

    .line 190
    sget-object v9, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v9, v8}, Lcom/peel/control/am;->a(Lcom/peel/control/a;)V

    .line 191
    sget-object v8, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    iget-object v9, p0, Lcom/peel/h/a/dd;->a:Lcom/peel/h/a/dc;

    iget-object v9, v9, Lcom/peel/h/a/dc;->a:Lcom/peel/control/h;

    invoke-virtual {v8, v9}, Lcom/peel/control/am;->b(Lcom/peel/control/h;)V

    .line 178
    :cond_2
    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_2

    .line 175
    :cond_3
    const/4 v0, 0x0

    goto/16 :goto_1

    .line 195
    :cond_4
    iget-object v0, p0, Lcom/peel/h/a/dd;->a:Lcom/peel/h/a/dc;

    iget-object v0, v0, Lcom/peel/h/a/dc;->c:Lcom/peel/h/a/cz;

    sget-object v2, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    iget-object v4, p0, Lcom/peel/h/a/dd;->a:Lcom/peel/h/a/dc;

    iget-object v4, v4, Lcom/peel/h/a/dc;->c:Lcom/peel/h/a/cz;

    invoke-static {v4}, Lcom/peel/h/a/cz;->c(Lcom/peel/h/a/cz;)Lcom/peel/data/ContentRoom;

    move-result-object v4

    invoke-virtual {v4}, Lcom/peel/data/ContentRoom;->d()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/peel/control/am;->a(Ljava/lang/String;)Lcom/peel/control/RoomControl;

    move-result-object v2

    invoke-virtual {v2}, Lcom/peel/control/RoomControl;->c()[Lcom/peel/control/a;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/peel/h/a/cz;->a(Lcom/peel/h/a/cz;[Lcom/peel/control/a;)[Lcom/peel/control/a;

    .line 198
    invoke-static {}, Lcom/peel/h/a/cz;->c()Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, " *************** isCurrentActivityRemoved: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v4, " -- replacement: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v4, p0, Lcom/peel/h/a/dd;->a:Lcom/peel/h/a/dc;

    iget-object v4, v4, Lcom/peel/h/a/dc;->c:Lcom/peel/h/a/cz;

    invoke-static {v4}, Lcom/peel/h/a/cz;->b(Lcom/peel/h/a/cz;)[Lcom/peel/control/a;

    move-result-object v4

    aget-object v4, v4, v3

    invoke-virtual {v4}, Lcom/peel/control/a;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 201
    iget-object v0, p0, Lcom/peel/h/a/dd;->a:Lcom/peel/h/a/dc;

    iget-object v0, v0, Lcom/peel/h/a/dc;->a:Lcom/peel/control/h;

    invoke-virtual {v0}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/data/g;->d()I

    move-result v0

    const/16 v2, 0x12

    if-eq v0, v2, :cond_6

    .line 203
    iget-object v0, p0, Lcom/peel/h/a/dd;->a:Lcom/peel/h/a/dc;

    iget-object v0, v0, Lcom/peel/h/a/dc;->c:Lcom/peel/h/a/cz;

    invoke-static {v0}, Lcom/peel/h/a/cz;->b(Lcom/peel/h/a/cz;)[Lcom/peel/control/a;

    move-result-object v2

    array-length v4, v2

    move v0, v3

    :goto_3
    if-ge v0, v4, :cond_6

    aget-object v6, v2, v0

    .line 204
    invoke-static {}, Lcom/peel/h/a/cz;->c()Ljava/lang/String;

    move-result-object v7

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "\n\n######## ****** deleting device: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/peel/h/a/dd;->a:Lcom/peel/h/a/dc;

    iget-object v9, v9, Lcom/peel/h/a/dc;->a:Lcom/peel/control/h;

    invoke-virtual {v9}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v9

    invoke-virtual {v9}, Lcom/peel/data/g;->f()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string/jumbo v9, " from activity: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v6}, Lcom/peel/control/a;->a()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 205
    iget-object v7, p0, Lcom/peel/h/a/dd;->a:Lcom/peel/h/a/dc;

    iget-object v7, v7, Lcom/peel/h/a/dc;->a:Lcom/peel/control/h;

    invoke-virtual {v6, v7}, Lcom/peel/control/a;->c(Lcom/peel/control/h;)V

    .line 207
    iget-object v6, p0, Lcom/peel/h/a/dd;->a:Lcom/peel/h/a/dc;

    iget-object v6, v6, Lcom/peel/h/a/dc;->c:Lcom/peel/h/a/cz;

    invoke-virtual {v6}, Lcom/peel/h/a/cz;->m()Landroid/support/v4/app/ae;

    move-result-object v6

    invoke-virtual {v6}, Landroid/support/v4/app/ae;->getApplicationContext()Landroid/content/Context;

    move-result-object v6

    invoke-static {v6}, Lcom/peel/social/w;->f(Landroid/content/Context;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 208
    new-instance v6, Lcom/peel/backup/c;

    iget-object v7, p0, Lcom/peel/h/a/dd;->a:Lcom/peel/h/a/dc;

    iget-object v7, v7, Lcom/peel/h/a/dc;->c:Lcom/peel/h/a/cz;

    invoke-virtual {v7}, Lcom/peel/h/a/cz;->m()Landroid/support/v4/app/ae;

    move-result-object v7

    invoke-virtual {v7}, Landroid/support/v4/app/ae;->getApplicationContext()Landroid/content/Context;

    move-result-object v7

    invoke-direct {v6, v7}, Lcom/peel/backup/c;-><init>(Landroid/content/Context;)V

    .line 209
    sget-object v7, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    iget-object v8, p0, Lcom/peel/h/a/dd;->a:Lcom/peel/h/a/dc;

    iget-object v8, v8, Lcom/peel/h/a/dc;->c:Lcom/peel/h/a/cz;

    invoke-static {v8}, Lcom/peel/h/a/cz;->c(Lcom/peel/h/a/cz;)Lcom/peel/data/ContentRoom;

    move-result-object v8

    invoke-virtual {v8}, Lcom/peel/data/ContentRoom;->d()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/peel/control/am;->a(Ljava/lang/String;)Lcom/peel/control/RoomControl;

    move-result-object v7

    iget-object v8, p0, Lcom/peel/h/a/dd;->a:Lcom/peel/h/a/dc;

    iget-object v8, v8, Lcom/peel/h/a/dc;->b:Landroid/os/Bundle;

    const-string/jumbo v9, "providername"

    invoke-virtual {v8, v9}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    iget-object v9, p0, Lcom/peel/h/a/dd;->a:Lcom/peel/h/a/dc;

    iget-object v9, v9, Lcom/peel/h/a/dc;->a:Lcom/peel/control/h;

    invoke-virtual {v6, v7, v8, v9}, Lcom/peel/backup/c;->a(Lcom/peel/control/RoomControl;Ljava/lang/String;Lcom/peel/control/h;)V

    .line 203
    :cond_5
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_3

    .line 215
    :cond_6
    if-eqz v1, :cond_8

    .line 217
    :try_start_0
    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v0}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/h/a/dd;->a:Lcom/peel/h/a/dc;

    iget-object v1, v1, Lcom/peel/h/a/dc;->c:Lcom/peel/h/a/cz;

    invoke-static {v1}, Lcom/peel/h/a/cz;->b(Lcom/peel/h/a/cz;)[Lcom/peel/control/a;

    move-result-object v1

    const/4 v2, 0x0

    aget-object v1, v1, v2

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/peel/control/RoomControl;->a(Lcom/peel/control/a;I)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 229
    :goto_4
    iget-object v0, p0, Lcom/peel/h/a/dd;->a:Lcom/peel/h/a/dc;

    iget-object v0, v0, Lcom/peel/h/a/dc;->c:Lcom/peel/h/a/cz;

    invoke-virtual {v0}, Lcom/peel/h/a/cz;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    sget-object v1, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    iget-object v2, p0, Lcom/peel/h/a/dd;->a:Lcom/peel/h/a/dc;

    iget-object v2, v2, Lcom/peel/h/a/dc;->c:Lcom/peel/h/a/cz;

    invoke-static {v2}, Lcom/peel/h/a/cz;->c(Lcom/peel/h/a/cz;)Lcom/peel/data/ContentRoom;

    move-result-object v2

    invoke-virtual {v2}, Lcom/peel/data/ContentRoom;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/peel/control/am;->a(Ljava/lang/String;)Lcom/peel/control/RoomControl;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/peel/util/bx;->a(Landroid/content/Context;Lcom/peel/control/RoomControl;)V

    .line 231
    sget-object v0, Lcom/peel/c/a;->b:Lcom/peel/c/e;

    invoke-static {v0}, Lcom/peel/c/a;->a(Lcom/peel/c/e;)Ljava/lang/Object;

    move-result-object v0

    sget-object v1, Lcom/peel/c/b;->c:Lcom/peel/c/b;

    if-ne v0, v1, :cond_7

    iget-object v0, p0, Lcom/peel/h/a/dd;->a:Lcom/peel/h/a/dc;

    iget-object v0, v0, Lcom/peel/h/a/dc;->c:Lcom/peel/h/a/cz;

    invoke-virtual {v0}, Lcom/peel/h/a/cz;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    const-string/jumbo v1, "widget_pref"

    invoke-virtual {v0, v1, v3}, Landroid/support/v4/app/ae;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string/jumbo v1, "notification"

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 232
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "tv.peel.notification.EXPANDED"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 233
    iget-object v1, p0, Lcom/peel/h/a/dd;->a:Lcom/peel/h/a/dc;

    iget-object v1, v1, Lcom/peel/h/a/dc;->c:Lcom/peel/h/a/cz;

    invoke-virtual {v1}, Lcom/peel/h/a/cz;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/support/v4/app/ae;->sendBroadcast(Landroid/content/Intent;)V

    .line 237
    :cond_7
    invoke-static {}, Lcom/peel/h/a/cz;->c()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "delay back"

    new-instance v2, Lcom/peel/h/a/de;

    invoke-direct {v2, p0}, Lcom/peel/h/a/de;-><init>(Lcom/peel/h/a/dd;)V

    invoke-static {v0, v1, v2}, Lcom/peel/util/i;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/lang/Runnable;

    goto/16 :goto_0

    .line 223
    :cond_8
    :try_start_1
    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v0}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v5, v1}, Lcom/peel/control/RoomControl;->a(Lcom/peel/control/a;I)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_4

    .line 224
    :catch_0
    move-exception v0

    goto :goto_4

    .line 218
    :catch_1
    move-exception v0

    goto :goto_4
.end method
