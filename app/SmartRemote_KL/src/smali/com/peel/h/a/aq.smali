.class public Lcom/peel/h/a/aq;
.super Landroid/widget/BaseAdapter;


# instance fields
.field a:Landroid/content/Context;

.field final synthetic b:Lcom/peel/h/a/am;


# direct methods
.method public constructor <init>(Lcom/peel/h/a/am;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 120
    iput-object p1, p0, Lcom/peel/h/a/aq;->b:Lcom/peel/h/a/am;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 121
    iput-object p2, p0, Lcom/peel/h/a/aq;->a:Landroid/content/Context;

    .line 122
    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 126
    iget-object v0, p0, Lcom/peel/h/a/aq;->b:Lcom/peel/h/a/am;

    invoke-static {v0}, Lcom/peel/h/a/am;->a(Lcom/peel/h/a/am;)Ljava/util/List;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/peel/h/a/aq;->b:Lcom/peel/h/a/am;

    invoke-static {v0}, Lcom/peel/h/a/am;->a(Lcom/peel/h/a/am;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    goto :goto_0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 131
    iget-object v0, p0, Lcom/peel/h/a/aq;->b:Lcom/peel/h/a/am;

    invoke-static {v0}, Lcom/peel/h/a/am;->a(Lcom/peel/h/a/am;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2

    .prologue
    .line 136
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 6

    .prologue
    .line 141
    if-nez p2, :cond_1

    new-instance v0, Lcom/peel/h/a/at;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/peel/h/a/at;-><init>(Lcom/peel/h/a/aq;Lcom/peel/h/a/an;)V

    move-object v2, v0

    .line 143
    :goto_0
    if-nez p2, :cond_0

    .line 144
    iget-object v0, p0, Lcom/peel/h/a/aq;->b:Lcom/peel/h/a/am;

    invoke-static {v0}, Lcom/peel/h/a/am;->b(Lcom/peel/h/a/am;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lcom/peel/ui/fq;->auto_list_item:I

    const/4 v3, 0x0

    invoke-virtual {v0, v1, p3, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 145
    sget v0, Lcom/peel/ui/fp;->location_check:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, v2, Lcom/peel/h/a/at;->c:Landroid/widget/CheckBox;

    .line 146
    sget v0, Lcom/peel/ui/fp;->text1:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v2, Lcom/peel/h/a/at;->a:Landroid/widget/TextView;

    .line 147
    sget v0, Lcom/peel/ui/fp;->text2:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v2, Lcom/peel/h/a/at;->b:Landroid/widget/TextView;

    .line 148
    sget v0, Lcom/peel/ui/fp;->wifi_logo:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, v2, Lcom/peel/h/a/at;->d:Landroid/widget/ImageView;

    .line 149
    invoke-virtual {p2, v2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 152
    :cond_0
    invoke-virtual {p0, p1}, Lcom/peel/h/a/aq;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/h/a/au;

    .line 154
    iget-object v1, v2, Lcom/peel/h/a/at;->c:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Lcom/peel/h/a/au;->f()Z

    move-result v3

    invoke-virtual {v1, v3}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 155
    iget-object v1, v2, Lcom/peel/h/a/at;->a:Landroid/widget/TextView;

    invoke-virtual {v0}, Lcom/peel/h/a/au;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 158
    iget-object v1, p0, Lcom/peel/h/a/aq;->b:Lcom/peel/h/a/am;

    iget-object v1, v1, Lcom/peel/h/a/am;->e:Ljava/lang/String;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/peel/h/a/aq;->b:Lcom/peel/h/a/am;

    iget-object v1, v1, Lcom/peel/h/a/am;->e:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/peel/h/a/au;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 159
    iget-object v1, v2, Lcom/peel/h/a/at;->d:Landroid/widget/ImageView;

    sget v3, Lcom/peel/ui/fo;->wifi_logo:I

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 164
    :goto_1
    iget-object v1, p0, Lcom/peel/h/a/aq;->b:Lcom/peel/h/a/am;

    invoke-static {v1}, Lcom/peel/h/a/am;->c(Lcom/peel/h/a/am;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 165
    iget-object v3, v2, Lcom/peel/h/a/at;->b:Landroid/widget/TextView;

    invoke-virtual {v0}, Lcom/peel/h/a/au;->e()I

    move-result v1

    if-nez v1, :cond_3

    iget-object v1, p0, Lcom/peel/h/a/aq;->b:Lcom/peel/h/a/am;

    sget v4, Lcom/peel/ui/ft;->wifi:I

    invoke-virtual {v1, v4}, Lcom/peel/h/a/am;->a(I)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v4, "Wi-Fi"

    const-string/jumbo v5, "WLAN"

    invoke-virtual {v1, v4, v5}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    :goto_2
    invoke-virtual {v3, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 170
    :goto_3
    new-instance v1, Lcom/peel/h/a/ar;

    invoke-direct {v1, p0, v2}, Lcom/peel/h/a/ar;-><init>(Lcom/peel/h/a/aq;Lcom/peel/h/a/at;)V

    invoke-virtual {p2, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 177
    iget-object v1, v2, Lcom/peel/h/a/at;->c:Landroid/widget/CheckBox;

    new-instance v2, Lcom/peel/h/a/as;

    invoke-direct {v2, p0, v0}, Lcom/peel/h/a/as;-><init>(Lcom/peel/h/a/aq;Lcom/peel/h/a/au;)V

    invoke-virtual {v1, v2}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 203
    return-object p2

    .line 141
    :cond_1
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/h/a/at;

    move-object v2, v0

    goto/16 :goto_0

    .line 161
    :cond_2
    iget-object v1, v2, Lcom/peel/h/a/at;->d:Landroid/widget/ImageView;

    sget v3, Lcom/peel/ui/fo;->wifi_logo_noconneted:I

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_1

    .line 165
    :cond_3
    const-string/jumbo v1, ""

    goto :goto_2

    .line 167
    :cond_4
    iget-object v3, v2, Lcom/peel/h/a/at;->b:Landroid/widget/TextView;

    invoke-virtual {v0}, Lcom/peel/h/a/au;->e()I

    move-result v1

    if-nez v1, :cond_5

    iget-object v1, p0, Lcom/peel/h/a/aq;->b:Lcom/peel/h/a/am;

    sget v4, Lcom/peel/ui/ft;->wifi:I

    invoke-virtual {v1, v4}, Lcom/peel/h/a/am;->a(I)Ljava/lang/String;

    move-result-object v1

    :goto_4
    invoke-virtual {v3, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_3

    :cond_5
    const-string/jumbo v1, ""

    goto :goto_4
.end method
