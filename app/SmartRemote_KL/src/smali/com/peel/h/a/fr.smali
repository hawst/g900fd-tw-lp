.class Lcom/peel/h/a/fr;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/peel/h/a/fq;


# direct methods
.method constructor <init>(Lcom/peel/h/a/fq;)V
    .locals 0

    .prologue
    .line 738
    iput-object p1, p0, Lcom/peel/h/a/fr;->a:Lcom/peel/h/a/fq;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 740
    iget-object v0, p0, Lcom/peel/h/a/fr;->a:Lcom/peel/h/a/fq;

    iget-object v0, v0, Lcom/peel/h/a/fq;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/peel/h/a/fr;->a:Lcom/peel/h/a/fq;

    iget-object v0, v0, Lcom/peel/h/a/fq;->a:Ljava/lang/String;

    iget-object v2, p0, Lcom/peel/h/a/fr;->a:Lcom/peel/h/a/fq;

    iget-object v2, v2, Lcom/peel/h/a/fq;->b:Lcom/peel/h/a/fn;

    invoke-static {v2}, Lcom/peel/h/a/fn;->e(Lcom/peel/h/a/fn;)Lcom/peel/data/ContentRoom;

    move-result-object v2

    invoke-virtual {v2}, Lcom/peel/data/ContentRoom;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 757
    :cond_0
    :goto_0
    return-void

    .line 744
    :cond_1
    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v0}, Lcom/peel/control/am;->c()[Lcom/peel/control/RoomControl;

    move-result-object v2

    .line 745
    array-length v3, v2

    move v0, v1

    :goto_1
    if-ge v0, v3, :cond_3

    aget-object v4, v2, v0

    .line 746
    invoke-virtual {v4}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v4

    invoke-virtual {v4}, Lcom/peel/data/at;->a()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/peel/h/a/fr;->a:Lcom/peel/h/a/fq;

    iget-object v5, v5, Lcom/peel/h/a/fq;->a:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 747
    new-instance v0, Lcom/peel/widget/ag;

    iget-object v2, p0, Lcom/peel/h/a/fr;->a:Lcom/peel/h/a/fq;

    iget-object v2, v2, Lcom/peel/h/a/fq;->b:Lcom/peel/h/a/fn;

    invoke-virtual {v2}, Lcom/peel/h/a/fn;->m()Landroid/support/v4/app/ae;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/peel/widget/ag;-><init>(Landroid/content/Context;)V

    sget v2, Lcom/peel/ui/ft;->room_name_exists_title:I

    .line 748
    invoke-virtual {v0, v2}, Lcom/peel/widget/ag;->a(I)Lcom/peel/widget/ag;

    move-result-object v0

    iget-object v2, p0, Lcom/peel/h/a/fr;->a:Lcom/peel/h/a/fq;

    iget-object v2, v2, Lcom/peel/h/a/fq;->b:Lcom/peel/h/a/fn;

    .line 749
    invoke-virtual {v2}, Lcom/peel/h/a/fn;->n()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/peel/ui/ft;->room_name_exists_msg:I

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/peel/h/a/fr;->a:Lcom/peel/h/a/fq;

    iget-object v5, v5, Lcom/peel/h/a/fq;->a:Ljava/lang/String;

    aput-object v5, v4, v1

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/peel/widget/ag;->b(Ljava/lang/CharSequence;)Lcom/peel/widget/ag;

    move-result-object v0

    sget v1, Lcom/peel/ui/ft;->ok:I

    const/4 v2, 0x0

    .line 750
    invoke-virtual {v0, v1, v2}, Lcom/peel/widget/ag;->a(ILandroid/content/DialogInterface$OnClickListener;)Lcom/peel/widget/ag;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/widget/ag;->d()Lcom/peel/widget/ag;

    goto :goto_0

    .line 745
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 755
    :cond_3
    iget-object v0, p0, Lcom/peel/h/a/fr;->a:Lcom/peel/h/a/fq;

    iget-object v0, v0, Lcom/peel/h/a/fq;->b:Lcom/peel/h/a/fn;

    invoke-static {v0}, Lcom/peel/h/a/fn;->c(Lcom/peel/h/a/fn;)Landroid/widget/CheckedTextView;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/h/a/fr;->a:Lcom/peel/h/a/fq;

    iget-object v1, v1, Lcom/peel/h/a/fq;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/CheckedTextView;->setText(Ljava/lang/CharSequence;)V

    .line 756
    iget-object v0, p0, Lcom/peel/h/a/fr;->a:Lcom/peel/h/a/fq;

    iget-object v0, v0, Lcom/peel/h/a/fq;->b:Lcom/peel/h/a/fn;

    invoke-static {v0}, Lcom/peel/h/a/fn;->q(Lcom/peel/h/a/fn;)V

    goto :goto_0
.end method
