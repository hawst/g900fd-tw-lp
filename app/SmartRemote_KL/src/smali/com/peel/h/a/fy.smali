.class Lcom/peel/h/a/fy;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Landroid/view/View;

.field final synthetic b:Lcom/peel/control/a;

.field final synthetic c:Lcom/peel/h/a/fn;


# direct methods
.method constructor <init>(Lcom/peel/h/a/fn;Landroid/view/View;Lcom/peel/control/a;)V
    .locals 0

    .prologue
    .line 496
    iput-object p1, p0, Lcom/peel/h/a/fy;->c:Lcom/peel/h/a/fn;

    iput-object p2, p0, Lcom/peel/h/a/fy;->a:Landroid/view/View;

    iput-object p3, p0, Lcom/peel/h/a/fy;->b:Lcom/peel/control/a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 504
    iget-object v0, p0, Lcom/peel/h/a/fy;->c:Lcom/peel/h/a/fn;

    invoke-static {v0}, Lcom/peel/h/a/fn;->i(Lcom/peel/h/a/fn;)Lcom/peel/control/h;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 507
    iget-object v0, p0, Lcom/peel/h/a/fy;->c:Lcom/peel/h/a/fn;

    invoke-static {v0}, Lcom/peel/h/a/fn;->i(Lcom/peel/h/a/fn;)Lcom/peel/control/h;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/data/g;->d()I

    move-result v0

    const/16 v1, 0xa

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/peel/h/a/fy;->c:Lcom/peel/h/a/fn;

    invoke-static {v0}, Lcom/peel/h/a/fn;->j(Lcom/peel/h/a/fn;)Lcom/peel/control/h;

    move-result-object v0

    if-nez v0, :cond_0

    .line 509
    iget-object v0, p0, Lcom/peel/h/a/fy;->a:Landroid/view/View;

    sget v1, Lcom/peel/ui/fp;->text2:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const-string/jumbo v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 510
    iget-object v0, p0, Lcom/peel/h/a/fy;->a:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setEnabled(Z)V

    .line 511
    iget-object v0, p0, Lcom/peel/h/a/fy;->a:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setClickable(Z)V

    .line 524
    :goto_0
    return-void

    .line 514
    :cond_0
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 515
    const-string/jumbo v1, "activity_id"

    iget-object v2, p0, Lcom/peel/h/a/fy;->b:Lcom/peel/control/a;

    invoke-virtual {v2}, Lcom/peel/control/a;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 516
    const-string/jumbo v1, "room"

    iget-object v2, p0, Lcom/peel/h/a/fy;->c:Lcom/peel/h/a/fn;

    invoke-static {v2}, Lcom/peel/h/a/fn;->e(Lcom/peel/h/a/fn;)Lcom/peel/data/ContentRoom;

    move-result-object v2

    invoke-virtual {v2}, Lcom/peel/data/ContentRoom;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 517
    const-string/jumbo v1, "back_to_clazz"

    const-class v2, Lcom/peel/h/a/fn;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 518
    const-string/jumbo v1, "providername"

    iget-object v2, p0, Lcom/peel/h/a/fy;->c:Lcom/peel/h/a/fn;

    invoke-static {v2}, Lcom/peel/h/a/fn;->f(Lcom/peel/h/a/fn;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 519
    iget-object v1, p0, Lcom/peel/h/a/fy;->c:Lcom/peel/h/a/fn;

    invoke-virtual {v1}, Lcom/peel/h/a/fn;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    const-class v2, Lcom/peel/h/a/bz;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lcom/peel/d/e;->c(Landroid/support/v4/app/ae;Ljava/lang/String;Landroid/os/Bundle;)V

    goto :goto_0

    .line 521
    :cond_1
    iget-object v0, p0, Lcom/peel/h/a/fy;->c:Lcom/peel/h/a/fn;

    new-instance v1, Lcom/peel/widget/ag;

    iget-object v2, p0, Lcom/peel/h/a/fy;->c:Lcom/peel/h/a/fn;

    invoke-virtual {v2}, Lcom/peel/h/a/fn;->m()Landroid/support/v4/app/ae;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/peel/widget/ag;-><init>(Landroid/content/Context;)V

    sget v2, Lcom/peel/ui/ft;->error:I

    invoke-virtual {v1, v2}, Lcom/peel/widget/ag;->a(I)Lcom/peel/widget/ag;

    move-result-object v1

    sget v2, Lcom/peel/ui/ft;->add_tv_for_inputs:I

    invoke-virtual {v1, v2}, Lcom/peel/widget/ag;->b(I)Lcom/peel/widget/ag;

    move-result-object v1

    sget v2, Lcom/peel/ui/ft;->okay:I

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Lcom/peel/widget/ag;->a(ILandroid/content/DialogInterface$OnClickListener;)Lcom/peel/widget/ag;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/peel/h/a/fn;->b(Lcom/peel/h/a/fn;Lcom/peel/widget/ag;)Lcom/peel/widget/ag;

    .line 522
    iget-object v0, p0, Lcom/peel/h/a/fy;->c:Lcom/peel/h/a/fn;

    invoke-static {v0}, Lcom/peel/h/a/fn;->k(Lcom/peel/h/a/fn;)Lcom/peel/widget/ag;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/h/a/fy;->c:Lcom/peel/h/a/fn;

    invoke-static {v1}, Lcom/peel/h/a/fn;->k(Lcom/peel/h/a/fn;)Lcom/peel/widget/ag;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/peel/widget/ag;->a(Lcom/peel/widget/ag;)Lcom/peel/widget/ag;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/widget/ag;->d()Lcom/peel/widget/ag;

    goto :goto_0
.end method
