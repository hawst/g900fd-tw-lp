.class Lcom/peel/h/a/dc;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/peel/control/h;

.field final synthetic b:Landroid/os/Bundle;

.field final synthetic c:Lcom/peel/h/a/cz;


# direct methods
.method constructor <init>(Lcom/peel/h/a/cz;Lcom/peel/control/h;Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 162
    iput-object p1, p0, Lcom/peel/h/a/dc;->c:Lcom/peel/h/a/cz;

    iput-object p2, p0, Lcom/peel/h/a/dc;->a:Lcom/peel/control/h;

    iput-object p3, p0, Lcom/peel/h/a/dc;->b:Landroid/os/Bundle;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 165
    iget-object v0, p0, Lcom/peel/h/a/dc;->c:Lcom/peel/h/a/cz;

    new-instance v1, Lcom/peel/widget/ag;

    iget-object v2, p0, Lcom/peel/h/a/dc;->c:Lcom/peel/h/a/cz;

    invoke-virtual {v2}, Lcom/peel/h/a/cz;->m()Landroid/support/v4/app/ae;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/peel/widget/ag;-><init>(Landroid/content/Context;)V

    sget v2, Lcom/peel/ui/ft;->warning:I

    .line 166
    invoke-virtual {v1, v2}, Lcom/peel/widget/ag;->a(I)Lcom/peel/widget/ag;

    move-result-object v1

    sget v2, Lcom/peel/ui/ft;->delete_device_confirmation:I

    .line 167
    invoke-virtual {v1, v2}, Lcom/peel/widget/ag;->b(I)Lcom/peel/widget/ag;

    move-result-object v1

    sget v2, Lcom/peel/ui/ft;->yes:I

    new-instance v3, Lcom/peel/h/a/dd;

    invoke-direct {v3, p0}, Lcom/peel/h/a/dd;-><init>(Lcom/peel/h/a/dc;)V

    .line 168
    invoke-virtual {v1, v2, v3}, Lcom/peel/widget/ag;->a(ILandroid/content/DialogInterface$OnClickListener;)Lcom/peel/widget/ag;

    move-result-object v1

    sget v2, Lcom/peel/ui/ft;->cancel:I

    const/4 v3, 0x0

    .line 245
    invoke-virtual {v1, v2, v3}, Lcom/peel/widget/ag;->c(ILandroid/content/DialogInterface$OnClickListener;)Lcom/peel/widget/ag;

    move-result-object v1

    .line 246
    invoke-virtual {v1, v4}, Lcom/peel/widget/ag;->a(Z)Lcom/peel/widget/ag;

    move-result-object v1

    .line 165
    invoke-static {v0, v1}, Lcom/peel/h/a/cz;->a(Lcom/peel/h/a/cz;Lcom/peel/widget/ag;)Lcom/peel/widget/ag;

    .line 247
    iget-object v0, p0, Lcom/peel/h/a/dc;->c:Lcom/peel/h/a/cz;

    invoke-static {v0}, Lcom/peel/h/a/cz;->d(Lcom/peel/h/a/cz;)Lcom/peel/widget/ag;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/peel/widget/ag;->setCanceledOnTouchOutside(Z)V

    .line 248
    iget-object v0, p0, Lcom/peel/h/a/dc;->c:Lcom/peel/h/a/cz;

    invoke-static {v0}, Lcom/peel/h/a/cz;->d(Lcom/peel/h/a/cz;)Lcom/peel/widget/ag;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/h/a/dc;->c:Lcom/peel/h/a/cz;

    invoke-static {v1}, Lcom/peel/h/a/cz;->d(Lcom/peel/h/a/cz;)Lcom/peel/widget/ag;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/peel/widget/ag;->a(Lcom/peel/widget/ag;)Lcom/peel/widget/ag;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/widget/ag;->d()Lcom/peel/widget/ag;

    .line 249
    return-void
.end method
