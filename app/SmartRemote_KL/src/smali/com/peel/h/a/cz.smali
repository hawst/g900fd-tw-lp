.class public Lcom/peel/h/a/cz;
.super Lcom/peel/d/u;


# static fields
.field private static final e:Ljava/lang/String;


# instance fields
.field private aj:[Lcom/peel/control/a;

.field private ak:Lcom/peel/widget/ag;

.field private f:Landroid/widget/LinearLayout;

.field private g:Landroid/view/LayoutInflater;

.field private h:Lcom/peel/control/a;

.field private i:Lcom/peel/data/ContentRoom;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 37
    const-class v0, Lcom/peel/h/a/cz;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/peel/h/a/cz;->e:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0}, Lcom/peel/d/u;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/peel/h/a/cz;)Lcom/peel/control/a;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/peel/h/a/cz;->h:Lcom/peel/control/a;

    return-object v0
.end method

.method static synthetic a(Lcom/peel/h/a/cz;Lcom/peel/widget/ag;)Lcom/peel/widget/ag;
    .locals 0

    .prologue
    .line 36
    iput-object p1, p0, Lcom/peel/h/a/cz;->ak:Lcom/peel/widget/ag;

    return-object p1
.end method

.method private a(Lcom/peel/control/a;Lcom/peel/control/h;Landroid/view/View;)V
    .locals 3

    .prologue
    .line 255
    invoke-virtual {p2}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/data/g;->l()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "DirecTV"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 256
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 257
    const-string/jumbo v1, "id"

    invoke-virtual {p2}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v2

    invoke-virtual {v2}, Lcom/peel/data/g;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 258
    invoke-virtual {p0}, Lcom/peel/h/a/cz;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    const-class v2, Lcom/peel/h/a/df;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lcom/peel/d/e;->b(Landroid/support/v4/app/ae;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 260
    :cond_0
    return-void
.end method

.method static synthetic a(Lcom/peel/h/a/cz;Lcom/peel/control/a;Lcom/peel/control/h;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0, p1, p2, p3}, Lcom/peel/h/a/cz;->a(Lcom/peel/control/a;Lcom/peel/control/h;Landroid/view/View;)V

    return-void
.end method

.method static synthetic a(Lcom/peel/h/a/cz;[Lcom/peel/control/a;)[Lcom/peel/control/a;
    .locals 0

    .prologue
    .line 36
    iput-object p1, p0, Lcom/peel/h/a/cz;->aj:[Lcom/peel/control/a;

    return-object p1
.end method

.method static synthetic b(Lcom/peel/h/a/cz;)[Lcom/peel/control/a;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/peel/h/a/cz;->aj:[Lcom/peel/control/a;

    return-object v0
.end method

.method static synthetic c(Lcom/peel/h/a/cz;)Lcom/peel/data/ContentRoom;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/peel/h/a/cz;->i:Lcom/peel/data/ContentRoom;

    return-object v0
.end method

.method static synthetic c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 36
    sget-object v0, Lcom/peel/h/a/cz;->e:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic d(Lcom/peel/h/a/cz;)Lcom/peel/widget/ag;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/peel/h/a/cz;->ak:Lcom/peel/widget/ag;

    return-object v0
.end method


# virtual methods
.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    .line 48
    iput-object p1, p0, Lcom/peel/h/a/cz;->g:Landroid/view/LayoutInflater;

    .line 49
    sget v0, Lcom/peel/ui/fq;->single_linear_layout:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/peel/h/a/cz;->f:Landroid/widget/LinearLayout;

    .line 50
    iget-object v0, p0, Lcom/peel/h/a/cz;->f:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method public c(Landroid/os/Bundle;)V
    .locals 11
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "WrongViewCast"
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 78
    invoke-super {p0, p1}, Lcom/peel/d/u;->c(Landroid/os/Bundle;)V

    .line 79
    sget-object v0, Lcom/peel/content/a;->h:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "refresh"

    invoke-virtual {p1, v0, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_1

    .line 252
    :cond_0
    :goto_0
    return-void

    .line 81
    :cond_1
    const-string/jumbo v0, "refresh"

    invoke-virtual {p1, v0, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 82
    const-string/jumbo v0, "room"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/peel/data/ContentRoom;

    iput-object v0, p0, Lcom/peel/h/a/cz;->i:Lcom/peel/data/ContentRoom;

    .line 84
    iget-object v0, p0, Lcom/peel/h/a/cz;->f:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 86
    const-string/jumbo v0, "device"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 87
    if-nez v6, :cond_2

    move-object v4, v5

    .line 88
    :goto_1
    if-nez v4, :cond_3

    .line 89
    sget-object v0, Lcom/peel/h/a/cz;->e:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/peel/h/a/cz;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/peel/d/e;->a(Ljava/lang/String;Landroid/support/v4/app/ae;)V

    goto :goto_0

    .line 87
    :cond_2
    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v0, v6}, Lcom/peel/control/am;->c(Ljava/lang/String;)Lcom/peel/control/h;

    move-result-object v0

    move-object v4, v0

    goto :goto_1

    .line 93
    :cond_3
    iget-object v0, p0, Lcom/peel/h/a/cz;->g:Landroid/view/LayoutInflater;

    sget v1, Lcom/peel/ui/fq;->room_overview_layout:I

    iget-object v7, p0, Lcom/peel/h/a/cz;->f:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1, v7, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 95
    sget v1, Lcom/peel/ui/fp;->text:I

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    sget v7, Lcom/peel/ui/ft;->brand:I

    invoke-virtual {v1, v7}, Landroid/widget/TextView;->setText(I)V

    .line 97
    sget v1, Lcom/peel/ui/fp;->text2:I

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-virtual {v4}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v7

    invoke-virtual {v7}, Lcom/peel/data/g;->f()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v1, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 98
    sget v1, Lcom/peel/ui/fp;->arrow:I

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const/16 v7, 0x8

    invoke-virtual {v1, v7}, Landroid/view/View;->setVisibility(I)V

    .line 99
    iget-object v1, p0, Lcom/peel/h/a/cz;->f:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 101
    invoke-virtual {v4}, Lcom/peel/control/h;->e()I

    move-result v0

    const/16 v1, 0x12

    if-eq v0, v1, :cond_4

    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v0}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/control/RoomControl;->b()Lcom/peel/control/o;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/control/o;->g()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 102
    iget-object v0, p0, Lcom/peel/h/a/cz;->g:Landroid/view/LayoutInflater;

    sget v1, Lcom/peel/ui/fq;->device_ir_row:I

    invoke-virtual {v0, v1, v5, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 103
    sget v1, Lcom/peel/ui/fp;->text:I

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    sget v5, Lcom/peel/ui/ft;->edit_ir:I

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setText(I)V

    .line 104
    sget v1, Lcom/peel/ui/fp;->btn:I

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    sget v5, Lcom/peel/ui/ft;->label_learn:I

    invoke-virtual {v1, v5}, Landroid/widget/Button;->setText(I)V

    .line 105
    sget v1, Lcom/peel/ui/fp;->btn:I

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    new-instance v5, Lcom/peel/h/a/da;

    invoke-direct {v5, p0, v6}, Lcom/peel/h/a/da;-><init>(Lcom/peel/h/a/cz;Ljava/lang/String;)V

    invoke-virtual {v1, v5}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 112
    iget-object v1, p0, Lcom/peel/h/a/cz;->f:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 116
    :cond_4
    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    iget-object v1, p0, Lcom/peel/h/a/cz;->i:Lcom/peel/data/ContentRoom;

    invoke-virtual {v1}, Lcom/peel/data/ContentRoom;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/peel/control/am;->a(Ljava/lang/String;)Lcom/peel/control/RoomControl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/control/RoomControl;->c()[Lcom/peel/control/a;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/h/a/cz;->aj:[Lcom/peel/control/a;

    .line 117
    iget-object v5, p0, Lcom/peel/h/a/cz;->aj:[Lcom/peel/control/a;

    array-length v6, v5

    move v1, v3

    :goto_2
    if-ge v1, v6, :cond_9

    aget-object v7, v5, v1

    .line 118
    invoke-virtual {v7}, Lcom/peel/control/a;->e()[Lcom/peel/control/h;

    move-result-object v8

    .line 119
    if-eqz v8, :cond_5

    array-length v0, v8

    if-ge v0, v2, :cond_6

    .line 117
    :cond_5
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 120
    :cond_6
    array-length v9, v8

    move v0, v3

    :goto_3
    if-ge v0, v9, :cond_8

    aget-object v10, v8, v0

    .line 121
    invoke-virtual {v10, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_7

    .line 122
    iput-object v7, p0, Lcom/peel/h/a/cz;->h:Lcom/peel/control/a;

    .line 120
    :cond_7
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 126
    :cond_8
    iget-object v0, p0, Lcom/peel/h/a/cz;->h:Lcom/peel/control/a;

    if-eqz v0, :cond_5

    .line 129
    :cond_9
    iget-object v0, p0, Lcom/peel/h/a/cz;->h:Lcom/peel/control/a;

    if-eqz v0, :cond_0

    .line 131
    invoke-virtual {v4}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/data/g;->i()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_d

    move v0, v2

    .line 132
    :goto_4
    if-eqz v0, :cond_b

    .line 133
    iget-object v0, p0, Lcom/peel/h/a/cz;->g:Landroid/view/LayoutInflater;

    sget v1, Lcom/peel/ui/fq;->settings_device_row:I

    iget-object v5, p0, Lcom/peel/h/a/cz;->f:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1, v5, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 135
    sget v1, Lcom/peel/ui/fp;->text:I

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    sget v5, Lcom/peel/ui/ft;->ip_address:I

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setText(I)V

    .line 136
    sget v1, Lcom/peel/ui/fp;->text2:I

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-virtual {v4}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v5

    invoke-virtual {v5}, Lcom/peel/data/g;->i()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 137
    sget v1, Lcom/peel/ui/fp;->arrow:I

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 139
    invoke-virtual {v4}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/data/g;->l()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v5, "Roku"

    invoke-virtual {v1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_a

    .line 140
    new-instance v1, Lcom/peel/h/a/db;

    invoke-direct {v1, p0, v4, v0}, Lcom/peel/h/a/db;-><init>(Lcom/peel/h/a/cz;Lcom/peel/control/h;Landroid/widget/LinearLayout;)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 148
    :cond_a
    iget-object v1, p0, Lcom/peel/h/a/cz;->f:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 152
    :cond_b
    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    iget-object v1, p0, Lcom/peel/h/a/cz;->i:Lcom/peel/data/ContentRoom;

    invoke-virtual {v1}, Lcom/peel/data/ContentRoom;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/peel/control/am;->a(Ljava/lang/String;)Lcom/peel/control/RoomControl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/control/RoomControl;->c()[Lcom/peel/control/a;

    move-result-object v0

    array-length v0, v0

    if-ne v0, v2, :cond_c

    iget-object v0, p0, Lcom/peel/h/a/cz;->h:Lcom/peel/control/a;

    invoke-virtual {v0, v2}, Lcom/peel/control/a;->a(I)Lcom/peel/control/h;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 155
    :cond_c
    const/16 v0, 0xa

    invoke-virtual {v4}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/data/g;->d()I

    move-result v1

    if-eq v0, v1, :cond_0

    .line 156
    invoke-virtual {v4}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/data/g;->d()I

    move-result v0

    if-eq v2, v0, :cond_0

    const/4 v0, 0x2

    .line 157
    invoke-virtual {v4}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/data/g;->d()I

    move-result v1

    if-eq v0, v1, :cond_0

    const/16 v0, 0x14

    .line 158
    invoke-virtual {v4}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/data/g;->d()I

    move-result v1

    if-eq v0, v1, :cond_0

    .line 161
    iget-object v0, p0, Lcom/peel/h/a/cz;->g:Landroid/view/LayoutInflater;

    sget v1, Lcom/peel/ui/fq;->delete_device_button:I

    iget-object v2, p0, Lcom/peel/h/a/cz;->f:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 162
    sget v1, Lcom/peel/ui/fp;->delete_button:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    new-instance v2, Lcom/peel/h/a/dc;

    invoke-direct {v2, p0, v4, p1}, Lcom/peel/h/a/dc;-><init>(Lcom/peel/h/a/cz;Lcom/peel/control/h;Landroid/os/Bundle;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 251
    iget-object v1, p0, Lcom/peel/h/a/cz;->f:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    goto/16 :goto_0

    :cond_d
    move v0, v3

    .line 131
    goto/16 :goto_4
.end method

.method public d(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 55
    invoke-super {p0, p1}, Lcom/peel/d/u;->d(Landroid/os/Bundle;)V

    .line 57
    iget-object v0, p0, Lcom/peel/h/a/cz;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "category"

    sget v2, Lcom/peel/ui/ft;->edit:I

    invoke-virtual {p0, v2}, Lcom/peel/h/a/cz;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 60
    iget-object v0, p0, Lcom/peel/h/a/cz;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "refresh"

    invoke-virtual {v0, v1, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 61
    sget-object v0, Lcom/peel/content/a;->h:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-ne v0, v3, :cond_0

    .line 62
    iget-object v0, p0, Lcom/peel/h/a/cz;->c:Landroid/os/Bundle;

    invoke-virtual {p0, v0}, Lcom/peel/h/a/cz;->c(Landroid/os/Bundle;)V

    .line 64
    :cond_0
    return-void
.end method

.method public f()V
    .locals 3

    .prologue
    .line 67
    invoke-super {p0}, Lcom/peel/d/u;->f()V

    .line 68
    invoke-virtual {p0}, Lcom/peel/h/a/cz;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    const-string/jumbo v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/ae;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 69
    invoke-virtual {p0}, Lcom/peel/h/a/cz;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/ae;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 70
    iget-object v0, p0, Lcom/peel/h/a/cz;->ak:Lcom/peel/widget/ag;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/peel/h/a/cz;->ak:Lcom/peel/widget/ag;

    invoke-virtual {v0}, Lcom/peel/widget/ag;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 71
    iget-object v0, p0, Lcom/peel/h/a/cz;->ak:Lcom/peel/widget/ag;

    invoke-virtual {v0}, Lcom/peel/widget/ag;->dismiss()V

    .line 73
    :cond_0
    return-void
.end method
