.class public Lcom/peel/h/a/gh;
.super Landroid/widget/BaseAdapter;


# static fields
.field public static a:Lcom/peel/widget/ag;

.field private static final b:Ljava/lang/String;


# instance fields
.field private c:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private d:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private e:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private f:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private g:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private h:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private i:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private j:Landroid/view/LayoutInflater;

.field private k:Landroid/support/v4/app/ae;

.field private l:Lcom/peel/d/i;

.field private m:Lcom/peel/h/a/hg;

.field private n:Landroid/view/View$OnClickListener;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 55
    const-class v0, Lcom/peel/h/a/gh;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/peel/h/a/gh;->b:Ljava/lang/String;

    .line 75
    const/4 v0, 0x0

    sput-object v0, Lcom/peel/h/a/gh;->a:Lcom/peel/widget/ag;

    return-void
.end method

.method public constructor <init>(Landroid/support/v4/app/ae;Lcom/peel/d/i;)V
    .locals 1

    .prologue
    .line 77
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 65
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/peel/h/a/gh;->c:Ljava/util/HashSet;

    .line 66
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/peel/h/a/gh;->d:Ljava/util/HashSet;

    .line 67
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/peel/h/a/gh;->e:Ljava/util/HashSet;

    .line 68
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/peel/h/a/gh;->f:Ljava/util/HashSet;

    .line 69
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/peel/h/a/gh;->g:Ljava/util/HashSet;

    .line 70
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/peel/h/a/gh;->h:Ljava/util/HashMap;

    .line 284
    new-instance v0, Lcom/peel/h/a/gn;

    invoke-direct {v0, p0}, Lcom/peel/h/a/gn;-><init>(Lcom/peel/h/a/gh;)V

    iput-object v0, p0, Lcom/peel/h/a/gh;->n:Landroid/view/View$OnClickListener;

    .line 78
    iput-object p1, p0, Lcom/peel/h/a/gh;->k:Landroid/support/v4/app/ae;

    .line 79
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/h/a/gh;->j:Landroid/view/LayoutInflater;

    .line 80
    iput-object p2, p0, Lcom/peel/h/a/gh;->l:Lcom/peel/d/i;

    .line 81
    return-void
.end method

.method static synthetic a(Lcom/peel/h/a/gh;)Landroid/support/v4/app/ae;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/peel/h/a/gh;->k:Landroid/support/v4/app/ae;

    return-object v0
.end method

.method static synthetic a(Lcom/peel/h/a/gh;Z)V
    .locals 0

    .prologue
    .line 54
    invoke-direct {p0, p1}, Lcom/peel/h/a/gh;->a(Z)V

    return-void
.end method

.method private a(Z)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 331
    iget-object v0, p0, Lcom/peel/h/a/gh;->k:Landroid/support/v4/app/ae;

    invoke-virtual {v0}, Landroid/support/v4/app/ae;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string/jumbo v1, "widget_pref"

    invoke-virtual {v0, v1, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 332
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 333
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 334
    if-eqz p1, :cond_1

    .line 335
    const-string/jumbo v2, "notification"

    const/4 v3, 0x1

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 336
    sget-object v0, Lcom/peel/c/a;->b:Lcom/peel/c/e;

    invoke-static {v0}, Lcom/peel/c/a;->a(Lcom/peel/c/e;)Ljava/lang/Object;

    move-result-object v0

    sget-object v2, Lcom/peel/c/b;->c:Lcom/peel/c/b;

    if-ne v0, v2, :cond_0

    const-string/jumbo v0, "tv.peel.notification.EXPANDED"

    :goto_0
    invoke-virtual {v1, v0}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 342
    :goto_1
    iget-object v0, p0, Lcom/peel/h/a/gh;->k:Landroid/support/v4/app/ae;

    invoke-virtual {v0, v1}, Landroid/support/v4/app/ae;->sendBroadcast(Landroid/content/Intent;)V

    .line 343
    return-void

    .line 336
    :cond_0
    const-string/jumbo v0, "tv.peel.samsung.notification.EXPANDED"

    goto :goto_0

    .line 338
    :cond_1
    const-string/jumbo v2, "notification"

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 339
    sget-object v0, Lcom/peel/c/a;->b:Lcom/peel/c/e;

    invoke-static {v0}, Lcom/peel/c/a;->a(Lcom/peel/c/e;)Ljava/lang/Object;

    move-result-object v0

    sget-object v2, Lcom/peel/c/b;->c:Lcom/peel/c/b;

    if-ne v0, v2, :cond_2

    const-string/jumbo v0, "tv.peel.notification.COLLAPSED"

    :goto_2
    invoke-virtual {v1, v0}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_1

    :cond_2
    const-string/jumbo v0, "tv.peel.samsung.notification.COLLAPSED"

    goto :goto_2
.end method

.method private b()V
    .locals 3

    .prologue
    .line 379
    new-instance v0, Lcom/peel/widget/ag;

    iget-object v1, p0, Lcom/peel/h/a/gh;->k:Landroid/support/v4/app/ae;

    invoke-direct {v0, v1}, Lcom/peel/widget/ag;-><init>(Landroid/content/Context;)V

    sget v1, Lcom/peel/ui/ft;->warning:I

    invoke-virtual {v0, v1}, Lcom/peel/widget/ag;->a(I)Lcom/peel/widget/ag;

    move-result-object v0

    sget v1, Lcom/peel/ui/ft;->reset_peel:I

    invoke-virtual {v0, v1}, Lcom/peel/widget/ag;->b(I)Lcom/peel/widget/ag;

    move-result-object v0

    sget v1, Lcom/peel/ui/ft;->ok:I

    new-instance v2, Lcom/peel/h/a/go;

    invoke-direct {v2, p0}, Lcom/peel/h/a/go;-><init>(Lcom/peel/h/a/gh;)V

    invoke-virtual {v0, v1, v2}, Lcom/peel/widget/ag;->a(ILandroid/content/DialogInterface$OnClickListener;)Lcom/peel/widget/ag;

    move-result-object v0

    sget v1, Lcom/peel/ui/ft;->cancel:I

    const/4 v2, 0x0

    .line 482
    invoke-virtual {v0, v1, v2}, Lcom/peel/widget/ag;->c(ILandroid/content/DialogInterface$OnClickListener;)Lcom/peel/widget/ag;

    move-result-object v0

    sput-object v0, Lcom/peel/h/a/gh;->a:Lcom/peel/widget/ag;

    .line 483
    sget-object v0, Lcom/peel/h/a/gh;->a:Lcom/peel/widget/ag;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/peel/widget/ag;->setCanceledOnTouchOutside(Z)V

    .line 484
    sget-object v0, Lcom/peel/h/a/gh;->a:Lcom/peel/widget/ag;

    sget-object v1, Lcom/peel/h/a/gh;->a:Lcom/peel/widget/ag;

    invoke-virtual {v0, v1}, Lcom/peel/widget/ag;->a(Lcom/peel/widget/ag;)Lcom/peel/widget/ag;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/widget/ag;->show()V

    .line 486
    return-void
.end method

.method static synthetic b(Lcom/peel/h/a/gh;)V
    .locals 0

    .prologue
    .line 54
    invoke-direct {p0}, Lcom/peel/h/a/gh;->c()V

    return-void
.end method

.method static synthetic b(Lcom/peel/h/a/gh;Z)V
    .locals 0

    .prologue
    .line 54
    invoke-direct {p0, p1}, Lcom/peel/h/a/gh;->b(Z)V

    return-void
.end method

.method private b(Z)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 507
    if-eqz p1, :cond_0

    .line 508
    iget-object v0, p0, Lcom/peel/h/a/gh;->k:Landroid/support/v4/app/ae;

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v1, "always_remote_widget_enabled"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 512
    :goto_0
    sput-boolean v3, Ltv/peel/widget/WidgetService;->e:Z

    .line 513
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/peel/h/a/gh;->k:Landroid/support/v4/app/ae;

    const-class v2, Ltv/peel/widget/WidgetService;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 514
    const-string/jumbo v1, "tv.peel.widget.action.UPDATE_WIDGETS"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 515
    iget-object v1, p0, Lcom/peel/h/a/gh;->k:Landroid/support/v4/app/ae;

    invoke-virtual {v1, v0}, Landroid/support/v4/app/ae;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 516
    return-void

    .line 510
    :cond_0
    iget-object v0, p0, Lcom/peel/h/a/gh;->k:Landroid/support/v4/app/ae;

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v1, "always_remote_widget_enabled"

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto :goto_0
.end method

.method private c()V
    .locals 3

    .prologue
    .line 489
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 490
    const-string/jumbo v1, "add_room"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 491
    const-string/jumbo v1, "back_visibility"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 492
    iget-object v1, p0, Lcom/peel/h/a/gh;->k:Landroid/support/v4/app/ae;

    const-class v2, Lcom/peel/i/fq;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lcom/peel/d/e;->c(Landroid/support/v4/app/ae;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 493
    return-void
.end method

.method static synthetic c(Lcom/peel/h/a/gh;)V
    .locals 0

    .prologue
    .line 54
    invoke-direct {p0}, Lcom/peel/h/a/gh;->b()V

    return-void
.end method

.method static synthetic d(Lcom/peel/h/a/gh;)Lcom/peel/h/a/hg;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/peel/h/a/gh;->m:Lcom/peel/h/a/hg;

    return-object v0
.end method

.method static synthetic e(Lcom/peel/h/a/gh;)Lcom/peel/d/i;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/peel/h/a/gh;->l:Lcom/peel/d/i;

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 496
    iget-object v0, p0, Lcom/peel/h/a/gh;->c:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->clear()V

    .line 497
    iget-object v0, p0, Lcom/peel/h/a/gh;->f:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->clear()V

    .line 498
    iget-object v0, p0, Lcom/peel/h/a/gh;->d:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->clear()V

    .line 499
    iget-object v0, p0, Lcom/peel/h/a/gh;->e:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->clear()V

    .line 500
    iget-object v0, p0, Lcom/peel/h/a/gh;->h:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 501
    iget-object v0, p0, Lcom/peel/h/a/gh;->i:Landroid/util/SparseArray;

    if-eqz v0, :cond_0

    .line 502
    iget-object v0, p0, Lcom/peel/h/a/gh;->i:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->clear()V

    .line 504
    :cond_0
    return-void
.end method

.method public a(I)V
    .locals 2

    .prologue
    .line 327
    iget-object v0, p0, Lcom/peel/h/a/gh;->c:Ljava/util/HashSet;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 328
    return-void
.end method

.method public a(II)V
    .locals 3

    .prologue
    .line 367
    iget-object v0, p0, Lcom/peel/h/a/gh;->h:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 368
    return-void
.end method

.method public a(Landroid/util/SparseArray;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 371
    iput-object p1, p0, Lcom/peel/h/a/gh;->i:Landroid/util/SparseArray;

    .line 372
    return-void
.end method

.method public a(Lcom/peel/h/a/hg;)V
    .locals 0

    .prologue
    .line 101
    iput-object p1, p0, Lcom/peel/h/a/gh;->m:Lcom/peel/h/a/hg;

    .line 102
    return-void
.end method

.method public b(I)V
    .locals 2

    .prologue
    .line 355
    iget-object v0, p0, Lcom/peel/h/a/gh;->d:Ljava/util/HashSet;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 356
    return-void
.end method

.method public c(I)V
    .locals 2

    .prologue
    .line 359
    iget-object v0, p0, Lcom/peel/h/a/gh;->e:Ljava/util/HashSet;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 360
    return-void
.end method

.method public d(I)V
    .locals 2

    .prologue
    .line 363
    iget-object v0, p0, Lcom/peel/h/a/gh;->f:Ljava/util/HashSet;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 364
    return-void
.end method

.method public e(I)V
    .locals 2

    .prologue
    .line 375
    iget-object v0, p0, Lcom/peel/h/a/gh;->g:Ljava/util/HashSet;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 376
    return-void
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lcom/peel/h/a/gh;->i:Landroid/util/SparseArray;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/peel/h/a/gh;->i:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    goto :goto_0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 91
    const/4 v0, 0x0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2

    .prologue
    .line 97
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public getItemViewType(I)I
    .locals 2

    .prologue
    .line 304
    iget-object v0, p0, Lcom/peel/h/a/gh;->c:Ljava/util/HashSet;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 305
    const/4 v0, 0x0

    .line 318
    :goto_0
    return v0

    .line 306
    :cond_0
    iget-object v0, p0, Lcom/peel/h/a/gh;->d:Ljava/util/HashSet;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 307
    const/4 v0, 0x2

    goto :goto_0

    .line 308
    :cond_1
    iget-object v0, p0, Lcom/peel/h/a/gh;->e:Ljava/util/HashSet;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 309
    const/4 v0, 0x5

    goto :goto_0

    .line 310
    :cond_2
    iget-object v0, p0, Lcom/peel/h/a/gh;->f:Ljava/util/HashSet;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 311
    const/4 v0, 0x3

    goto :goto_0

    .line 312
    :cond_3
    iget-object v0, p0, Lcom/peel/h/a/gh;->g:Ljava/util/HashSet;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 313
    const/4 v0, 0x4

    goto :goto_0

    .line 314
    :cond_4
    iget-object v0, p0, Lcom/peel/h/a/gh;->h:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 315
    const/4 v0, 0x6

    goto :goto_0

    .line 318
    :cond_5
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 10

    .prologue
    const/4 v9, 0x3

    const/4 v8, 0x4

    const/4 v7, 0x0

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 108
    invoke-virtual {p0, p1}, Lcom/peel/h/a/gh;->getItemViewType(I)I

    move-result v5

    .line 109
    iget-object v0, p0, Lcom/peel/h/a/gh;->i:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 111
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    instance-of v1, v1, Lcom/peel/h/a/gq;

    if-nez v1, :cond_4

    .line 112
    :cond_0
    new-instance v4, Lcom/peel/h/a/gq;

    invoke-direct {v4}, Lcom/peel/h/a/gq;-><init>()V

    .line 113
    packed-switch v5, :pswitch_data_0

    .line 156
    :cond_1
    :goto_0
    invoke-virtual {p2, v4}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    move-object v1, v4

    .line 161
    :goto_1
    const/4 v4, 0x2

    if-ne v5, v4, :cond_5

    .line 162
    iget-object v3, v1, Lcom/peel/h/a/gq;->b:Landroid/widget/Button;

    invoke-virtual {v3, v0}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 163
    iget-object v3, p0, Lcom/peel/h/a/gh;->k:Landroid/support/v4/app/ae;

    sget v4, Lcom/peel/ui/ft;->resetpeelapp:I

    invoke-virtual {v3, v4}, Landroid/support/v4/app/ae;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 165
    const/16 v0, 0x1e

    invoke-virtual {p2, v2, v2, v2, v0}, Landroid/view/View;->setPadding(IIII)V

    .line 167
    :cond_2
    iget-object v0, v1, Lcom/peel/h/a/gq;->b:Landroid/widget/Button;

    new-instance v1, Lcom/peel/h/a/gi;

    invoke-direct {v1, p0}, Lcom/peel/h/a/gi;-><init>(Lcom/peel/h/a/gh;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 281
    :cond_3
    :goto_2
    return-object p2

    .line 115
    :pswitch_0
    iget-object v1, p0, Lcom/peel/h/a/gh;->j:Landroid/view/LayoutInflater;

    sget v6, Lcom/peel/ui/fq;->settings_header_row_main:I

    invoke-virtual {v1, v6, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 116
    sget v1, Lcom/peel/ui/fp;->text:I

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, v4, Lcom/peel/h/a/gq;->a:Landroid/widget/TextView;

    .line 117
    sget v1, Lcom/peel/ui/fp;->seperator:I

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, v4, Lcom/peel/h/a/gq;->c:Landroid/view/View;

    goto :goto_0

    .line 120
    :pswitch_1
    iget-object v1, p0, Lcom/peel/h/a/gh;->h:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v1, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 122
    sget v6, Lcom/peel/ui/fq;->settings_login:I

    if-ne v1, v6, :cond_1

    .line 123
    iget-object v1, p0, Lcom/peel/h/a/gh;->j:Landroid/view/LayoutInflater;

    sget v6, Lcom/peel/ui/fq;->settings_login:I

    invoke-virtual {v1, v6, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 124
    sget v1, Lcom/peel/ui/fp;->profile_parent_logout:I

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    iput-object v1, v4, Lcom/peel/h/a/gq;->f:Landroid/widget/RelativeLayout;

    goto :goto_0

    .line 128
    :pswitch_2
    iget-object v1, p0, Lcom/peel/h/a/gh;->j:Landroid/view/LayoutInflater;

    sget v6, Lcom/peel/ui/fq;->settings_row:I

    invoke-virtual {v1, v6, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 129
    sget v1, Lcom/peel/ui/fp;->text:I

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, v4, Lcom/peel/h/a/gq;->a:Landroid/widget/TextView;

    .line 130
    sget v1, Lcom/peel/ui/fp;->seperator:I

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, v4, Lcom/peel/h/a/gq;->c:Landroid/view/View;

    goto/16 :goto_0

    .line 133
    :pswitch_3
    iget-object v1, p0, Lcom/peel/h/a/gh;->j:Landroid/view/LayoutInflater;

    sget v6, Lcom/peel/ui/fq;->settings_button:I

    invoke-virtual {v1, v6, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 134
    sget v1, Lcom/peel/ui/fp;->settings_button:I

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, v4, Lcom/peel/h/a/gq;->b:Landroid/widget/Button;

    goto/16 :goto_0

    .line 138
    :pswitch_4
    iget-object v1, p0, Lcom/peel/h/a/gh;->j:Landroid/view/LayoutInflater;

    sget v6, Lcom/peel/ui/fq;->settings_add_room_view:I

    invoke-virtual {v1, v6, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 139
    sget v1, Lcom/peel/ui/fp;->text_add_room:I

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, v4, Lcom/peel/h/a/gq;->a:Landroid/widget/TextView;

    goto/16 :goto_0

    .line 142
    :pswitch_5
    iget-object v1, p0, Lcom/peel/h/a/gh;->j:Landroid/view/LayoutInflater;

    sget v6, Lcom/peel/ui/fq;->settings_single_selection_list_row:I

    invoke-virtual {v1, v6, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 143
    sget v1, Lcom/peel/ui/fp;->toggleButton1:I

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ToggleButton;

    iput-object v1, v4, Lcom/peel/h/a/gq;->e:Landroid/widget/ToggleButton;

    .line 144
    sget v1, Lcom/peel/ui/fp;->text:I

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, v4, Lcom/peel/h/a/gq;->a:Landroid/widget/TextView;

    .line 145
    sget v1, Lcom/peel/ui/fp;->check_row:I

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, v4, Lcom/peel/h/a/gq;->d:Landroid/view/View;

    .line 146
    sget v1, Lcom/peel/ui/fp;->seperator:I

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, v4, Lcom/peel/h/a/gq;->c:Landroid/view/View;

    goto/16 :goto_0

    .line 149
    :pswitch_6
    iget-object v1, p0, Lcom/peel/h/a/gh;->j:Landroid/view/LayoutInflater;

    sget v6, Lcom/peel/ui/fq;->settings_single_selection_list_row:I

    invoke-virtual {v1, v6, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 150
    sget v1, Lcom/peel/ui/fp;->text:I

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, v4, Lcom/peel/h/a/gq;->a:Landroid/widget/TextView;

    .line 151
    sget v1, Lcom/peel/ui/fp;->toggleButton1:I

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ToggleButton;

    iput-object v1, v4, Lcom/peel/h/a/gq;->e:Landroid/widget/ToggleButton;

    .line 152
    sget v1, Lcom/peel/ui/fp;->seperator:I

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, v4, Lcom/peel/h/a/gq;->c:Landroid/view/View;

    .line 153
    sget v1, Lcom/peel/ui/fp;->check_row:I

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, v4, Lcom/peel/h/a/gq;->d:Landroid/view/View;

    goto/16 :goto_0

    .line 158
    :cond_4
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/peel/h/a/gq;

    goto/16 :goto_1

    .line 182
    :cond_5
    const/4 v4, 0x5

    if-ne v5, v4, :cond_6

    .line 183
    iget-object v2, v1, Lcom/peel/h/a/gq;->a:Landroid/widget/TextView;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 184
    iget-object v0, v1, Lcom/peel/h/a/gq;->a:Landroid/widget/TextView;

    new-instance v1, Lcom/peel/h/a/gj;

    invoke-direct {v1, p0}, Lcom/peel/h/a/gj;-><init>(Lcom/peel/h/a/gh;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_2

    .line 197
    :cond_6
    if-eq v5, v3, :cond_7

    if-nez v5, :cond_9

    .line 198
    :cond_7
    iget-object v4, v1, Lcom/peel/h/a/gq;->a:Landroid/widget/TextView;

    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 199
    if-ne v5, v3, :cond_8

    add-int/lit8 v0, p1, 0x1

    invoke-virtual {p0, v0}, Lcom/peel/h/a/gh;->getItemViewType(I)I

    move-result v0

    if-eq v0, v3, :cond_8

    .line 200
    iget-object v0, v1, Lcom/peel/h/a/gq;->c:Landroid/view/View;

    invoke-virtual {v0, v8}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_2

    .line 202
    :cond_8
    iget-object v0, v1, Lcom/peel/h/a/gq;->c:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_2

    .line 204
    :cond_9
    if-ne v5, v9, :cond_e

    .line 205
    iget-object v4, v1, Lcom/peel/h/a/gq;->a:Landroid/widget/TextView;

    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 206
    add-int/lit8 v4, p1, 0x1

    invoke-virtual {p0, v4}, Lcom/peel/h/a/gh;->getItemViewType(I)I

    move-result v4

    if-eq v4, v9, :cond_a

    .line 207
    iget-object v4, v1, Lcom/peel/h/a/gq;->c:Landroid/view/View;

    invoke-virtual {v4, v8}, Landroid/view/View;->setVisibility(I)V

    .line 212
    :goto_3
    iget-object v4, v1, Lcom/peel/h/a/gq;->e:Landroid/widget/ToggleButton;

    invoke-virtual {v4, v2}, Landroid/widget/ToggleButton;->setClickable(Z)V

    .line 213
    iget-object v4, p0, Lcom/peel/h/a/gh;->k:Landroid/support/v4/app/ae;

    sget v5, Lcom/peel/ui/ft;->twitter_label:I

    invoke-virtual {v4, v5}, Landroid/support/v4/app/ae;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_c

    .line 214
    iget-object v0, p0, Lcom/peel/h/a/gh;->k:Landroid/support/v4/app/ae;

    invoke-static {v0}, Lcom/peel/d/v;->a(Landroid/content/Context;)Lcom/peel/d/v;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/d/v;->b()Lcom/peel/d/af;

    move-result-object v0

    if-nez v0, :cond_b

    .line 215
    iget-object v0, v1, Lcom/peel/h/a/gq;->e:Landroid/widget/ToggleButton;

    invoke-virtual {v0, v2}, Landroid/widget/ToggleButton;->setChecked(Z)V

    goto/16 :goto_2

    .line 209
    :cond_a
    iget-object v4, v1, Lcom/peel/h/a/gq;->c:Landroid/view/View;

    invoke-virtual {v4, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_3

    .line 217
    :cond_b
    iget-object v0, v1, Lcom/peel/h/a/gq;->e:Landroid/widget/ToggleButton;

    invoke-virtual {v0, v3}, Landroid/widget/ToggleButton;->setChecked(Z)V

    goto/16 :goto_2

    .line 219
    :cond_c
    iget-object v2, p0, Lcom/peel/h/a/gh;->k:Landroid/support/v4/app/ae;

    sget v4, Lcom/peel/ui/ft;->samsung_label:I

    invoke-virtual {v2, v4}, Landroid/support/v4/app/ae;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_d

    iget-object v2, p0, Lcom/peel/h/a/gh;->k:Landroid/support/v4/app/ae;

    sget v4, Lcom/peel/ui/ft;->facebook_label:I

    invoke-virtual {v2, v4}, Landroid/support/v4/app/ae;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_d

    iget-object v2, p0, Lcom/peel/h/a/gh;->k:Landroid/support/v4/app/ae;

    sget v4, Lcom/peel/ui/ft;->google_plus_label:I

    invoke-virtual {v2, v4}, Landroid/support/v4/app/ae;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 220
    :cond_d
    iget-object v0, v1, Lcom/peel/h/a/gq;->e:Landroid/widget/ToggleButton;

    invoke-virtual {v0, v3}, Landroid/widget/ToggleButton;->setChecked(Z)V

    goto/16 :goto_2

    .line 222
    :cond_e
    if-ne v5, v8, :cond_14

    .line 223
    iget-object v4, v1, Lcom/peel/h/a/gq;->a:Landroid/widget/TextView;

    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 224
    iget-object v4, v1, Lcom/peel/h/a/gq;->e:Landroid/widget/ToggleButton;

    .line 226
    invoke-virtual {v4, v3}, Landroid/widget/ToggleButton;->setClickable(Z)V

    .line 227
    iget-object v5, v1, Lcom/peel/h/a/gq;->d:Landroid/view/View;

    new-instance v6, Lcom/peel/h/a/gk;

    invoke-direct {v6, p0, v4}, Lcom/peel/h/a/gk;-><init>(Lcom/peel/h/a/gh;Landroid/widget/ToggleButton;)V

    invoke-virtual {v5, v6}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 233
    iget-object v4, p0, Lcom/peel/h/a/gh;->k:Landroid/support/v4/app/ae;

    sget v5, Lcom/peel/ui/ft;->label_notification:I

    invoke-virtual {v4, v5}, Landroid/support/v4/app/ae;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_f

    .line 234
    iget-object v0, p0, Lcom/peel/h/a/gh;->k:Landroid/support/v4/app/ae;

    invoke-virtual {v0}, Landroid/support/v4/app/ae;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string/jumbo v3, "widget_pref"

    invoke-virtual {v0, v3, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string/jumbo v3, "notification"

    invoke-interface {v0, v3, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 235
    iget-object v2, v1, Lcom/peel/h/a/gq;->c:Landroid/view/View;

    invoke-virtual {v2, v8}, Landroid/view/View;->setVisibility(I)V

    .line 238
    iget-object v2, v1, Lcom/peel/h/a/gq;->e:Landroid/widget/ToggleButton;

    invoke-virtual {v2, v0}, Landroid/widget/ToggleButton;->setChecked(Z)V

    .line 239
    iget-object v0, v1, Lcom/peel/h/a/gq;->e:Landroid/widget/ToggleButton;

    new-instance v1, Lcom/peel/h/a/gl;

    invoke-direct {v1, p0}, Lcom/peel/h/a/gl;-><init>(Lcom/peel/h/a/gh;)V

    invoke-virtual {v0, v1}, Landroid/widget/ToggleButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    goto/16 :goto_2

    .line 248
    :cond_f
    iget-object v4, p0, Lcom/peel/h/a/gh;->k:Landroid/support/v4/app/ae;

    sget v5, Lcom/peel/ui/ft;->auto_display_lock:I

    invoke-virtual {v4, v5}, Landroid/support/v4/app/ae;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_12

    .line 250
    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v0}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v0

    if-nez v0, :cond_10

    move v0, v2

    .line 251
    :goto_4
    iget-object v2, v1, Lcom/peel/h/a/gq;->d:Landroid/view/View;

    invoke-virtual {v2, v0}, Landroid/view/View;->setEnabled(Z)V

    .line 252
    iget-object v2, v1, Lcom/peel/h/a/gq;->e:Landroid/widget/ToggleButton;

    invoke-virtual {v2, v0}, Landroid/widget/ToggleButton;->setEnabled(Z)V

    .line 253
    iget-object v2, v1, Lcom/peel/h/a/gq;->a:Landroid/widget/TextView;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 254
    iget-object v2, v1, Lcom/peel/h/a/gq;->c:Landroid/view/View;

    invoke-virtual {v2, v8}, Landroid/view/View;->setVisibility(I)V

    .line 255
    iget-object v2, p0, Lcom/peel/h/a/gh;->k:Landroid/support/v4/app/ae;

    invoke-static {v0, v1, v2}, Lcom/peel/util/bb;->a(ZLcom/peel/h/a/gq;Landroid/content/Context;)V

    goto/16 :goto_2

    .line 250
    :cond_10
    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v0}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/control/RoomControl;->c()[Lcom/peel/control/a;

    move-result-object v0

    if-nez v0, :cond_11

    move v0, v2

    goto :goto_4

    :cond_11
    move v0, v3

    goto :goto_4

    .line 258
    :cond_12
    iget-object v0, p0, Lcom/peel/h/a/gh;->k:Landroid/support/v4/app/ae;

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string/jumbo v4, "always_remote_widget_enabled"

    invoke-interface {v0, v4, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_13

    iget-object v0, p0, Lcom/peel/h/a/gh;->k:Landroid/support/v4/app/ae;

    .line 259
    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string/jumbo v4, "is_device_setup_complete"

    invoke-interface {v0, v4, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_13

    .line 260
    :goto_5
    iget-object v0, p0, Lcom/peel/h/a/gh;->k:Landroid/support/v4/app/ae;

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string/jumbo v4, "is_device_setup_complete"

    invoke-interface {v0, v4, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 261
    iget-object v2, v1, Lcom/peel/h/a/gq;->d:Landroid/view/View;

    invoke-virtual {v2, v0}, Landroid/view/View;->setEnabled(Z)V

    .line 262
    iget-object v2, v1, Lcom/peel/h/a/gq;->a:Landroid/widget/TextView;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 263
    iget-object v2, v1, Lcom/peel/h/a/gq;->e:Landroid/widget/ToggleButton;

    invoke-virtual {v2, v0}, Landroid/widget/ToggleButton;->setClickable(Z)V

    .line 264
    iget-object v0, v1, Lcom/peel/h/a/gq;->e:Landroid/widget/ToggleButton;

    invoke-virtual {v0, v3}, Landroid/widget/ToggleButton;->setChecked(Z)V

    .line 265
    iget-object v0, v1, Lcom/peel/h/a/gq;->e:Landroid/widget/ToggleButton;

    new-instance v2, Lcom/peel/h/a/gm;

    invoke-direct {v2, p0}, Lcom/peel/h/a/gm;-><init>(Lcom/peel/h/a/gh;)V

    invoke-virtual {v0, v2}, Landroid/widget/ToggleButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 271
    iget-object v0, v1, Lcom/peel/h/a/gq;->c:Landroid/view/View;

    invoke-virtual {v0, v8}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_2

    :cond_13
    move v3, v2

    .line 259
    goto :goto_5

    .line 273
    :cond_14
    const/4 v0, 0x6

    if-ne v5, v0, :cond_3

    .line 274
    iget-object v0, p0, Lcom/peel/h/a/gh;->h:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    sget v2, Lcom/peel/ui/fq;->settings_login:I

    if-ne v0, v2, :cond_3

    .line 276
    iget-object v0, v1, Lcom/peel/h/a/gq;->f:Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/peel/h/a/gh;->n:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_2

    .line 113
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_5
        :pswitch_6
        :pswitch_4
        :pswitch_1
    .end packed-switch
.end method

.method public getViewTypeCount()I
    .locals 1

    .prologue
    .line 323
    const/4 v0, 0x7

    return v0
.end method

.method public isEnabled(I)Z
    .locals 1

    .prologue
    .line 295
    invoke-virtual {p0, p1}, Lcom/peel/h/a/gh;->getItemViewType(I)I

    move-result v0

    if-nez v0, :cond_0

    .line 296
    const/4 v0, 0x0

    .line 299
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Landroid/widget/BaseAdapter;->isEnabled(I)Z

    move-result v0

    goto :goto_0
.end method
