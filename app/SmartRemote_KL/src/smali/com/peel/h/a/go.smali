.class Lcom/peel/h/a/go;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic a:Lcom/peel/h/a/gh;


# direct methods
.method constructor <init>(Lcom/peel/h/a/gh;)V
    .locals 0

    .prologue
    .line 379
    iput-object p1, p0, Lcom/peel/h/a/go;->a:Lcom/peel/h/a/gh;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v7, 0x0

    const/4 v2, 0x0

    .line 383
    iget-object v0, p0, Lcom/peel/h/a/go;->a:Lcom/peel/h/a/gh;

    invoke-static {v0}, Lcom/peel/h/a/gh;->a(Lcom/peel/h/a/gh;)Landroid/support/v4/app/ae;

    move-result-object v0

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string/jumbo v3, "setup_type"

    invoke-interface {v0, v3, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    if-ne v1, v0, :cond_6

    move v0, v1

    .line 385
    :goto_0
    invoke-static {}, Lcom/peel/util/a/f;->a()Lcom/peel/util/a/f;

    move-result-object v4

    sget-object v3, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v3}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v3

    if-nez v3, :cond_7

    move v3, v1

    :goto_1
    const/16 v5, 0xbd3

    const/16 v6, 0x7d8

    invoke-virtual {v4, v3, v5, v6}, Lcom/peel/util/a/f;->a(III)V

    .line 386
    iget-object v3, p0, Lcom/peel/h/a/go;->a:Lcom/peel/h/a/gh;

    invoke-static {v3}, Lcom/peel/h/a/gh;->a(Lcom/peel/h/a/gh;)Landroid/support/v4/app/ae;

    move-result-object v3

    invoke-static {v3}, Lcom/peel/social/w;->j(Landroid/content/Context;)V

    .line 389
    iget-object v3, p0, Lcom/peel/h/a/go;->a:Lcom/peel/h/a/gh;

    invoke-static {v3}, Lcom/peel/h/a/gh;->a(Lcom/peel/h/a/gh;)Landroid/support/v4/app/ae;

    move-result-object v3

    invoke-static {v3}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v3

    const-string/jumbo v4, "always_remote_widget_enabled"

    invoke-interface {v3, v4, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 390
    new-instance v3, Landroid/content/Intent;

    iget-object v4, p0, Lcom/peel/h/a/go;->a:Lcom/peel/h/a/gh;

    invoke-static {v4}, Lcom/peel/h/a/gh;->a(Lcom/peel/h/a/gh;)Landroid/support/v4/app/ae;

    move-result-object v4

    const-class v5, Ltv/peel/widget/WidgetService;

    invoke-direct {v3, v4, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 391
    const-string/jumbo v4, "tv.peel.widget.action.UPDATE_WIDGETS"

    invoke-virtual {v3, v4}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 392
    iget-object v4, p0, Lcom/peel/h/a/go;->a:Lcom/peel/h/a/gh;

    invoke-static {v4}, Lcom/peel/h/a/gh;->a(Lcom/peel/h/a/gh;)Landroid/support/v4/app/ae;

    move-result-object v4

    invoke-virtual {v4, v3}, Landroid/support/v4/app/ae;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 396
    :cond_0
    iget-object v3, p0, Lcom/peel/h/a/go;->a:Lcom/peel/h/a/gh;

    invoke-static {v3}, Lcom/peel/h/a/gh;->a(Lcom/peel/h/a/gh;)Landroid/support/v4/app/ae;

    move-result-object v3

    invoke-static {v3}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v3

    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    invoke-interface {v3}, Landroid/content/SharedPreferences$Editor;->clear()Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    invoke-interface {v3}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 397
    sput-object v7, Lcom/peel/util/a;->e:[I

    .line 400
    sget-object v3, Lcom/peel/util/v;->a:Lcom/peel/util/v;

    invoke-virtual {v3}, Lcom/peel/util/v;->a()V

    .line 402
    new-instance v3, Lcom/peel/util/au;

    iget-object v4, p0, Lcom/peel/h/a/go;->a:Lcom/peel/h/a/gh;

    invoke-static {v4}, Lcom/peel/h/a/gh;->a(Lcom/peel/h/a/gh;)Landroid/support/v4/app/ae;

    move-result-object v4

    invoke-virtual {v4}, Landroid/support/v4/app/ae;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/peel/util/au;-><init>(Landroid/content/Context;)V

    .line 403
    iget-object v4, p0, Lcom/peel/h/a/go;->a:Lcom/peel/h/a/gh;

    invoke-static {v4}, Lcom/peel/h/a/gh;->a(Lcom/peel/h/a/gh;)Landroid/support/v4/app/ae;

    move-result-object v4

    invoke-virtual {v4}, Landroid/support/v4/app/ae;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/peel/util/au;->c(Landroid/content/Context;)V

    .line 404
    iget-object v3, p0, Lcom/peel/h/a/go;->a:Lcom/peel/h/a/gh;

    invoke-static {v3}, Lcom/peel/h/a/gh;->a(Lcom/peel/h/a/gh;)Landroid/support/v4/app/ae;

    move-result-object v3

    const-string/jumbo v4, "widget_pref"

    invoke-virtual {v3, v4, v2}, Landroid/support/v4/app/ae;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v3

    const-string/jumbo v4, "showquickpanel"

    invoke-interface {v3, v4, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    .line 405
    iget-object v4, p0, Lcom/peel/h/a/go;->a:Lcom/peel/h/a/gh;

    invoke-static {v4}, Lcom/peel/h/a/gh;->a(Lcom/peel/h/a/gh;)Landroid/support/v4/app/ae;

    move-result-object v4

    const-string/jumbo v5, "peel_private"

    invoke-virtual {v4, v5, v2}, Landroid/support/v4/app/ae;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v4

    invoke-interface {v4}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v4

    invoke-interface {v4}, Landroid/content/SharedPreferences$Editor;->clear()Landroid/content/SharedPreferences$Editor;

    move-result-object v4

    invoke-interface {v4}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 406
    iget-object v4, p0, Lcom/peel/h/a/go;->a:Lcom/peel/h/a/gh;

    invoke-static {v4}, Lcom/peel/h/a/gh;->a(Lcom/peel/h/a/gh;)Landroid/support/v4/app/ae;

    move-result-object v4

    const-string/jumbo v5, "network_setup"

    invoke-virtual {v4, v5, v2}, Landroid/support/v4/app/ae;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v4

    invoke-interface {v4}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v4

    invoke-interface {v4}, Landroid/content/SharedPreferences$Editor;->clear()Landroid/content/SharedPreferences$Editor;

    move-result-object v4

    invoke-interface {v4}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 407
    iget-object v4, p0, Lcom/peel/h/a/go;->a:Lcom/peel/h/a/gh;

    invoke-static {v4}, Lcom/peel/h/a/gh;->a(Lcom/peel/h/a/gh;)Landroid/support/v4/app/ae;

    move-result-object v4

    const-string/jumbo v5, "twitter_pref"

    invoke-virtual {v4, v5, v2}, Landroid/support/v4/app/ae;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v4

    invoke-interface {v4}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v4

    invoke-interface {v4}, Landroid/content/SharedPreferences$Editor;->clear()Landroid/content/SharedPreferences$Editor;

    move-result-object v4

    invoke-interface {v4}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 408
    iget-object v4, p0, Lcom/peel/h/a/go;->a:Lcom/peel/h/a/gh;

    invoke-static {v4}, Lcom/peel/h/a/gh;->a(Lcom/peel/h/a/gh;)Landroid/support/v4/app/ae;

    move-result-object v4

    const-string/jumbo v5, "widget_pref"

    invoke-virtual {v4, v5, v2}, Landroid/support/v4/app/ae;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v4

    invoke-interface {v4}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v4

    invoke-interface {v4}, Landroid/content/SharedPreferences$Editor;->clear()Landroid/content/SharedPreferences$Editor;

    move-result-object v4

    invoke-interface {v4}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 409
    iget-object v4, p0, Lcom/peel/h/a/go;->a:Lcom/peel/h/a/gh;

    invoke-static {v4}, Lcom/peel/h/a/gh;->a(Lcom/peel/h/a/gh;)Landroid/support/v4/app/ae;

    move-result-object v4

    const-string/jumbo v5, "social_accounts_setup"

    invoke-virtual {v4, v5, v2}, Landroid/support/v4/app/ae;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v4

    invoke-interface {v4}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v4

    invoke-interface {v4}, Landroid/content/SharedPreferences$Editor;->clear()Landroid/content/SharedPreferences$Editor;

    move-result-object v4

    invoke-interface {v4}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 410
    iget-object v4, p0, Lcom/peel/h/a/go;->a:Lcom/peel/h/a/gh;

    invoke-static {v4}, Lcom/peel/h/a/gh;->a(Lcom/peel/h/a/gh;)Landroid/support/v4/app/ae;

    move-result-object v4

    invoke-static {v4}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v4

    .line 412
    if-eqz v3, :cond_1

    .line 413
    iget-object v3, p0, Lcom/peel/h/a/go;->a:Lcom/peel/h/a/gh;

    invoke-static {v3}, Lcom/peel/h/a/gh;->a(Lcom/peel/h/a/gh;)Landroid/support/v4/app/ae;

    move-result-object v3

    const-string/jumbo v5, "widget_pref"

    invoke-virtual {v3, v5, v2}, Landroid/support/v4/app/ae;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v3

    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    const-string/jumbo v5, "notification"

    invoke-interface {v3, v5, v1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 414
    :cond_1
    invoke-interface {v4}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string/jumbo v3, "lockscreen_enabled"

    invoke-interface {v1, v3, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 415
    iget-object v1, p0, Lcom/peel/h/a/go;->a:Lcom/peel/h/a/gh;

    invoke-static {v1}, Lcom/peel/h/a/gh;->a(Lcom/peel/h/a/gh;)Landroid/support/v4/app/ae;

    move-result-object v1

    new-instance v2, Landroid/content/Intent;

    const-string/jumbo v3, "com.android.internal.policy.impl.keyguard.sec.REMOTE_REMOVED"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Landroid/support/v4/app/ae;->sendBroadcast(Landroid/content/Intent;)V

    .line 417
    sget-object v1, Lcom/peel/c/a;->b:Lcom/peel/c/e;

    invoke-static {v1}, Lcom/peel/c/a;->a(Lcom/peel/c/e;)Ljava/lang/Object;

    move-result-object v1

    sget-object v2, Lcom/peel/c/b;->b:Lcom/peel/c/b;

    if-ne v1, v2, :cond_2

    .line 418
    iget-object v1, p0, Lcom/peel/h/a/go;->a:Lcom/peel/h/a/gh;

    invoke-static {v1}, Lcom/peel/h/a/gh;->a(Lcom/peel/h/a/gh;)Landroid/support/v4/app/ae;

    move-result-object v1

    invoke-static {v1}, Lcom/peel/util/bb;->b(Landroid/content/Context;)V

    .line 421
    :cond_2
    if-nez v0, :cond_3

    .line 422
    iget-object v1, p0, Lcom/peel/h/a/go;->a:Lcom/peel/h/a/gh;

    invoke-static {v1}, Lcom/peel/h/a/gh;->a(Lcom/peel/h/a/gh;)Landroid/support/v4/app/ae;

    move-result-object v1

    invoke-static {v1}, Lcom/peel/d/v;->a(Landroid/content/Context;)Lcom/peel/d/v;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/d/v;->a()V

    .line 424
    :cond_3
    const/4 v1, -0x1

    sput v1, Lcom/peel/ui/kj;->f:I

    .line 426
    sget-object v1, Lcom/peel/content/a;->f:Lcom/peel/content/user/User;

    invoke-virtual {v1}, Lcom/peel/content/user/User;->x()Ljava/lang/String;

    move-result-object v1

    .line 428
    if-nez v0, :cond_4

    if-eqz v1, :cond_4

    .line 429
    const-class v2, Lcom/peel/h/a/gr;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "reset reminder"

    new-instance v4, Lcom/peel/h/a/gp;

    invoke-direct {v4, p0, v1}, Lcom/peel/h/a/gp;-><init>(Lcom/peel/h/a/go;Ljava/lang/String;)V

    invoke-static {v2, v3, v4}, Lcom/peel/util/i;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    .line 440
    :cond_4
    sget-object v1, Lcom/peel/c/a;->b:Lcom/peel/c/e;

    invoke-static {v1}, Lcom/peel/c/a;->a(Lcom/peel/c/e;)Ljava/lang/Object;

    move-result-object v1

    sget-object v2, Lcom/peel/c/b;->a:Lcom/peel/c/b;

    if-ne v1, v2, :cond_5

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x15

    if-ge v1, v2, :cond_5

    .line 441
    iget-object v1, p0, Lcom/peel/h/a/go;->a:Lcom/peel/h/a/gh;

    invoke-static {v1}, Lcom/peel/h/a/gh;->a(Lcom/peel/h/a/gh;)Landroid/support/v4/app/ae;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/ae;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Lcom/peel/provider/a;->a:Landroid/net/Uri;

    invoke-virtual {v1, v2, v7, v7}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 446
    :cond_5
    invoke-static {}, Lcom/peel/control/am;->b()V

    .line 454
    invoke-static {}, Lcom/peel/content/a;->b()V

    .line 455
    invoke-static {}, Lcom/peel/util/a;->a()V

    .line 457
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    .line 458
    sget-object v1, Lcom/peel/c/a;->b:Lcom/peel/c/e;

    invoke-static {v1}, Lcom/peel/c/a;->a(Lcom/peel/c/e;)Ljava/lang/Object;

    move-result-object v1

    sget-object v3, Lcom/peel/c/b;->c:Lcom/peel/c/b;

    if-ne v1, v3, :cond_8

    const-string/jumbo v1, "tv.peel.settings.RESET"

    :goto_2
    invoke-virtual {v2, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 459
    iget-object v1, p0, Lcom/peel/h/a/go;->a:Lcom/peel/h/a/gh;

    invoke-static {v1}, Lcom/peel/h/a/gh;->a(Lcom/peel/h/a/gh;)Landroid/support/v4/app/ae;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/support/v4/app/ae;->sendBroadcast(Landroid/content/Intent;)V

    .line 462
    sput-object v7, Lcom/peel/util/b/a;->b:Ljava/lang/String;

    .line 465
    iget-object v1, p0, Lcom/peel/h/a/go;->a:Lcom/peel/h/a/gh;

    invoke-static {v1}, Lcom/peel/h/a/gh;->e(Lcom/peel/h/a/gh;)Lcom/peel/d/i;

    move-result-object v1

    if-eqz v1, :cond_a

    .line 466
    if-eqz v0, :cond_9

    .line 471
    iget-object v0, p0, Lcom/peel/h/a/go;->a:Lcom/peel/h/a/gh;

    invoke-static {v0}, Lcom/peel/h/a/gh;->a(Lcom/peel/h/a/gh;)Landroid/support/v4/app/ae;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/ae;->finish()V

    .line 481
    :goto_3
    return-void

    :cond_6
    move v0, v2

    .line 383
    goto/16 :goto_0

    .line 385
    :cond_7
    sget-object v3, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v3}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v3

    invoke-virtual {v3}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v3

    invoke-virtual {v3}, Lcom/peel/data/at;->f()I

    move-result v3

    goto/16 :goto_1

    .line 458
    :cond_8
    const-string/jumbo v1, "tv.peel.samsung.settings.RESET"

    goto :goto_2

    .line 474
    :cond_9
    iget-object v0, p0, Lcom/peel/h/a/go;->a:Lcom/peel/h/a/gh;

    invoke-static {v0}, Lcom/peel/h/a/gh;->e(Lcom/peel/h/a/gh;)Lcom/peel/d/i;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/d/i;->d()V

    .line 475
    iget-object v0, p0, Lcom/peel/h/a/go;->a:Lcom/peel/h/a/gh;

    invoke-static {v0}, Lcom/peel/h/a/gh;->a(Lcom/peel/h/a/gh;)Landroid/support/v4/app/ae;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/h/a/go;->a:Lcom/peel/h/a/gh;

    invoke-static {v1}, Lcom/peel/h/a/gh;->e(Lcom/peel/h/a/gh;)Lcom/peel/d/i;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/peel/util/bx;->a(Landroid/content/Context;Lcom/peel/d/i;)V

    .line 476
    iget-object v0, p0, Lcom/peel/h/a/go;->a:Lcom/peel/h/a/gh;

    invoke-static {v0}, Lcom/peel/h/a/gh;->a(Lcom/peel/h/a/gh;)Landroid/support/v4/app/ae;

    move-result-object v0

    const-class v1, Lcom/peel/i/fv;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v7}, Lcom/peel/d/e;->a(Landroid/support/v4/app/ae;Ljava/lang/String;Landroid/os/Bundle;)V

    goto :goto_3

    .line 479
    :cond_a
    iget-object v0, p0, Lcom/peel/h/a/go;->a:Lcom/peel/h/a/gh;

    invoke-static {v0}, Lcom/peel/h/a/gh;->a(Lcom/peel/h/a/gh;)Landroid/support/v4/app/ae;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/ae;->finish()V

    goto :goto_3
.end method
