.class Lcom/peel/h/a/dh;
.super Lcom/peel/util/t;


# instance fields
.field final synthetic a:Lcom/peel/h/a/dg;


# direct methods
.method constructor <init>(Lcom/peel/h/a/dg;I)V
    .locals 0

    .prologue
    .line 43
    iput-object p1, p0, Lcom/peel/h/a/dh;->a:Lcom/peel/h/a/dg;

    invoke-direct {p0, p2}, Lcom/peel/util/t;-><init>(I)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 46
    iget-boolean v0, p0, Lcom/peel/h/a/dh;->i:Z

    if-nez v0, :cond_0

    .line 47
    invoke-static {}, Lcom/peel/h/a/df;->c()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/h/a/dh;->k:Ljava/lang/String;

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 48
    new-instance v0, Lcom/peel/widget/ag;

    iget-object v1, p0, Lcom/peel/h/a/dh;->a:Lcom/peel/h/a/dg;

    iget-object v1, v1, Lcom/peel/h/a/dg;->a:Lcom/peel/h/a/df;

    invoke-virtual {v1}, Lcom/peel/h/a/df;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/peel/widget/ag;-><init>(Landroid/content/Context;)V

    sget v1, Lcom/peel/ui/ft;->error:I

    .line 49
    invoke-virtual {v0, v1}, Lcom/peel/widget/ag;->a(I)Lcom/peel/widget/ag;

    move-result-object v0

    const-string/jumbo v1, "DIRECTV box couldn\'t be found. Make sure your device is on the same WiFi network as your DIRECTV box and try again!"

    .line 50
    invoke-virtual {v0, v1}, Lcom/peel/widget/ag;->b(Ljava/lang/CharSequence;)Lcom/peel/widget/ag;

    move-result-object v0

    sget v1, Lcom/peel/ui/ft;->okay:I

    new-instance v2, Lcom/peel/h/a/di;

    invoke-direct {v2, p0}, Lcom/peel/h/a/di;-><init>(Lcom/peel/h/a/dh;)V

    .line 51
    invoke-virtual {v0, v1, v2}, Lcom/peel/widget/ag;->a(ILandroid/content/DialogInterface$OnClickListener;)Lcom/peel/widget/ag;

    move-result-object v0

    .line 56
    invoke-virtual {v0}, Lcom/peel/widget/ag;->show()V

    .line 69
    :goto_0
    return-void

    .line 58
    :cond_0
    iget-object v0, p0, Lcom/peel/h/a/dh;->j:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "Your DIRECTV has been updated to use this new IP address: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/h/a/dh;->a:Lcom/peel/h/a/dg;

    iget-object v1, v1, Lcom/peel/h/a/dg;->a:Lcom/peel/h/a/df;

    invoke-static {v1}, Lcom/peel/h/a/df;->a(Lcom/peel/h/a/df;)Lcom/peel/control/h;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/data/g;->i()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 59
    :goto_1
    new-instance v1, Lcom/peel/widget/ag;

    iget-object v2, p0, Lcom/peel/h/a/dh;->a:Lcom/peel/h/a/dg;

    iget-object v2, v2, Lcom/peel/h/a/dg;->a:Lcom/peel/h/a/df;

    invoke-virtual {v2}, Lcom/peel/h/a/df;->m()Landroid/support/v4/app/ae;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/peel/widget/ag;-><init>(Landroid/content/Context;)V

    sget v2, Lcom/peel/ui/ft;->label_success:I

    .line 60
    invoke-virtual {v1, v2}, Lcom/peel/widget/ag;->a(I)Lcom/peel/widget/ag;

    move-result-object v1

    .line 61
    invoke-virtual {v1, v0}, Lcom/peel/widget/ag;->b(Ljava/lang/CharSequence;)Lcom/peel/widget/ag;

    move-result-object v0

    sget v1, Lcom/peel/ui/ft;->okay:I

    new-instance v2, Lcom/peel/h/a/dj;

    invoke-direct {v2, p0}, Lcom/peel/h/a/dj;-><init>(Lcom/peel/h/a/dh;)V

    .line 62
    invoke-virtual {v0, v1, v2}, Lcom/peel/widget/ag;->a(ILandroid/content/DialogInterface$OnClickListener;)Lcom/peel/widget/ag;

    move-result-object v0

    .line 67
    invoke-virtual {v0}, Lcom/peel/widget/ag;->show()V

    goto :goto_0

    .line 58
    :cond_1
    const-string/jumbo v0, "There\'s no change in your DIRECTV box IP address, no update is needed at this time"

    goto :goto_1
.end method
