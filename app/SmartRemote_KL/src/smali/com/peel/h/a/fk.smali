.class Lcom/peel/h/a/fk;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field final synthetic a:Lcom/peel/h/a/fj;


# direct methods
.method constructor <init>(Lcom/peel/h/a/fj;)V
    .locals 0

    .prologue
    .line 127
    iput-object p1, p0, Lcom/peel/h/a/fk;->a:Lcom/peel/h/a/fj;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 130
    iget-object v0, p0, Lcom/peel/h/a/fk;->a:Lcom/peel/h/a/fj;

    new-instance v1, Lcom/peel/widget/ag;

    iget-object v2, p0, Lcom/peel/h/a/fk;->a:Lcom/peel/h/a/fj;

    invoke-virtual {v2}, Lcom/peel/h/a/fj;->m()Landroid/support/v4/app/ae;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/peel/widget/ag;-><init>(Landroid/content/Context;)V

    invoke-static {v0, v1}, Lcom/peel/h/a/fj;->a(Lcom/peel/h/a/fj;Lcom/peel/widget/ag;)Lcom/peel/widget/ag;

    .line 131
    iget-object v0, p0, Lcom/peel/h/a/fk;->a:Lcom/peel/h/a/fj;

    invoke-static {v0}, Lcom/peel/h/a/fj;->b(Lcom/peel/h/a/fj;)Lcom/peel/widget/ag;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/h/a/fk;->a:Lcom/peel/h/a/fj;

    invoke-virtual {v1}, Lcom/peel/h/a/fj;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/ae;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/peel/ui/ft;->assign_channel_to_key:I

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/peel/h/a/fk;->a:Lcom/peel/h/a/fj;

    invoke-static {v4}, Lcom/peel/h/a/fj;->a(Lcom/peel/h/a/fj;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v6

    const/4 v4, 0x1

    add-int/lit8 v5, p3, 0x1

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/peel/widget/ag;->a(Ljava/lang/CharSequence;)Lcom/peel/widget/ag;

    .line 132
    iget-object v0, p0, Lcom/peel/h/a/fk;->a:Lcom/peel/h/a/fj;

    invoke-static {v0}, Lcom/peel/h/a/fj;->b(Lcom/peel/h/a/fj;)Lcom/peel/widget/ag;

    move-result-object v0

    invoke-virtual {v0, v6}, Lcom/peel/widget/ag;->setCanceledOnTouchOutside(Z)V

    .line 134
    iget-object v0, p0, Lcom/peel/h/a/fk;->a:Lcom/peel/h/a/fj;

    invoke-static {v0}, Lcom/peel/h/a/fj;->d(Lcom/peel/h/a/fj;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lcom/peel/ui/fq;->preset_key_view:I

    iget-object v2, p0, Lcom/peel/h/a/fk;->a:Lcom/peel/h/a/fj;

    invoke-static {v2}, Lcom/peel/h/a/fj;->c(Lcom/peel/h/a/fj;)Landroid/widget/LinearLayout;

    move-result-object v2

    invoke-virtual {v0, v1, v2, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 135
    sget v0, Lcom/peel/ui/fp;->edittext:I

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    .line 136
    iget-object v1, p0, Lcom/peel/h/a/fk;->a:Lcom/peel/h/a/fj;

    invoke-static {v1}, Lcom/peel/h/a/fj;->e(Lcom/peel/h/a/fj;)Ljava/util/Map;

    move-result-object v1

    add-int/lit8 v3, p3, 0x1

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 137
    new-instance v1, Lcom/peel/h/a/fl;

    invoke-direct {v1, p0, v0, p3}, Lcom/peel/h/a/fl;-><init>(Lcom/peel/h/a/fk;Landroid/widget/EditText;I)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 148
    iget-object v1, p0, Lcom/peel/h/a/fk;->a:Lcom/peel/h/a/fj;

    invoke-static {v1}, Lcom/peel/h/a/fj;->b(Lcom/peel/h/a/fj;)Lcom/peel/widget/ag;

    move-result-object v1

    invoke-virtual {v1, v2}, Lcom/peel/widget/ag;->a(Landroid/view/View;)Lcom/peel/widget/ag;

    .line 149
    iget-object v1, p0, Lcom/peel/h/a/fk;->a:Lcom/peel/h/a/fj;

    invoke-static {v1}, Lcom/peel/h/a/fj;->b(Lcom/peel/h/a/fj;)Lcom/peel/widget/ag;

    move-result-object v1

    sget v2, Lcom/peel/ui/ft;->cancel:I

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Lcom/peel/widget/ag;->c(ILandroid/content/DialogInterface$OnClickListener;)Lcom/peel/widget/ag;

    .line 150
    iget-object v1, p0, Lcom/peel/h/a/fk;->a:Lcom/peel/h/a/fj;

    invoke-static {v1}, Lcom/peel/h/a/fj;->b(Lcom/peel/h/a/fj;)Lcom/peel/widget/ag;

    move-result-object v1

    sget v2, Lcom/peel/ui/ft;->done:I

    new-instance v3, Lcom/peel/h/a/fm;

    invoke-direct {v3, p0, v0, p3}, Lcom/peel/h/a/fm;-><init>(Lcom/peel/h/a/fk;Landroid/widget/EditText;I)V

    invoke-virtual {v1, v2, v3}, Lcom/peel/widget/ag;->a(ILandroid/content/DialogInterface$OnClickListener;)Lcom/peel/widget/ag;

    .line 155
    iget-object v0, p0, Lcom/peel/h/a/fk;->a:Lcom/peel/h/a/fj;

    invoke-static {v0}, Lcom/peel/h/a/fj;->b(Lcom/peel/h/a/fj;)Lcom/peel/widget/ag;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/widget/ag;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Landroid/view/Window;->setSoftInputMode(I)V

    .line 156
    iget-object v0, p0, Lcom/peel/h/a/fk;->a:Lcom/peel/h/a/fj;

    invoke-static {v0}, Lcom/peel/h/a/fj;->b(Lcom/peel/h/a/fj;)Lcom/peel/widget/ag;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/h/a/fk;->a:Lcom/peel/h/a/fj;

    invoke-static {v1}, Lcom/peel/h/a/fj;->b(Lcom/peel/h/a/fj;)Lcom/peel/widget/ag;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/peel/widget/ag;->a(Lcom/peel/widget/ag;)Lcom/peel/widget/ag;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/widget/ag;->d()Lcom/peel/widget/ag;

    .line 157
    return-void
.end method
