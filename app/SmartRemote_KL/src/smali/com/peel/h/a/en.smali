.class Lcom/peel/h/a/en;
.super Lcom/peel/util/s;


# instance fields
.field final synthetic a:Lcom/peel/h/a/em;


# direct methods
.method constructor <init>(Lcom/peel/h/a/em;)V
    .locals 0

    .prologue
    .line 45
    iput-object p1, p0, Lcom/peel/h/a/en;->a:Lcom/peel/h/a/em;

    invoke-direct {p0}, Lcom/peel/util/s;-><init>()V

    return-void
.end method


# virtual methods
.method public final varargs a(ILjava/lang/Object;[Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 48
    packed-switch p1, :pswitch_data_0

    .line 98
    :goto_0
    :pswitch_0
    return-void

    .line 55
    :pswitch_1
    const/4 v0, 0x1

    aget-object v0, p3, v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 56
    const/4 v0, 0x2

    aget-object v0, p3, v0

    check-cast v0, [I

    check-cast v0, [I

    .line 57
    iget-object v2, p0, Lcom/peel/h/a/en;->a:Lcom/peel/h/a/em;

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/peel/h/a/em;->a(Lcom/peel/h/a/em;Ljava/lang/String;)Ljava/lang/String;

    .line 58
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 59
    const/4 v1, 0x0

    :goto_1
    array-length v3, v0

    if-ge v1, v3, :cond_1

    .line 60
    aget v3, v0, v1

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 61
    array-length v3, v0

    add-int/lit8 v3, v3, -0x1

    if-eq v1, v3, :cond_0

    .line 62
    const-string/jumbo v3, ","

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 59
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 65
    :cond_1
    iget-object v0, p0, Lcom/peel/h/a/en;->a:Lcom/peel/h/a/em;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/peel/h/a/em;->b(Lcom/peel/h/a/em;Ljava/lang/String;)Ljava/lang/String;

    .line 67
    const-class v0, Lcom/peel/h/a/em;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "enable test button"

    new-instance v2, Lcom/peel/h/a/eo;

    invoke-direct {v2, p0}, Lcom/peel/h/a/eo;-><init>(Lcom/peel/h/a/en;)V

    invoke-static {v0, v1, v2}, Lcom/peel/util/i;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/lang/Runnable;

    goto :goto_0

    .line 78
    :pswitch_2
    const-class v0, Lcom/peel/h/a/em;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "timeout relearn dialog"

    new-instance v2, Lcom/peel/h/a/ep;

    invoke-direct {v2, p0}, Lcom/peel/h/a/ep;-><init>(Lcom/peel/h/a/en;)V

    invoke-static {v0, v1, v2}, Lcom/peel/util/i;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/lang/Runnable;

    goto :goto_0

    .line 90
    :pswitch_3
    const-class v0, Lcom/peel/h/a/em;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "error relearn dialog"

    new-instance v2, Lcom/peel/h/a/eq;

    invoke-direct {v2, p0}, Lcom/peel/h/a/eq;-><init>(Lcom/peel/h/a/en;)V

    invoke-static {v0, v1, v2}, Lcom/peel/util/i;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/lang/Runnable;

    goto :goto_0

    .line 48
    :pswitch_data_0
    .packed-switch 0xd
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method
