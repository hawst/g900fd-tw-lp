.class public Lcom/peel/h/a/gr;
.super Lcom/peel/d/u;


# instance fields
.field private aj:Lcom/peel/h/a/hg;

.field private ak:Landroid/content/BroadcastReceiver;

.field private e:Landroid/widget/ListView;

.field private f:Z

.field private g:Lcom/peel/h/a/gh;

.field private h:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Landroid/os/Bundle;",
            ">;"
        }
    .end annotation
.end field

.field private i:Lcom/peel/widget/ag;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 58
    invoke-direct {p0}, Lcom/peel/d/u;-><init>()V

    .line 60
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/peel/h/a/gr;->f:Z

    .line 62
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/peel/h/a/gr;->h:Landroid/util/SparseArray;

    .line 486
    new-instance v0, Lcom/peel/h/a/ha;

    invoke-direct {v0, p0}, Lcom/peel/h/a/ha;-><init>(Lcom/peel/h/a/gr;)V

    iput-object v0, p0, Lcom/peel/h/a/gr;->ak:Landroid/content/BroadcastReceiver;

    return-void
.end method

.method private S()V
    .locals 2

    .prologue
    .line 465
    invoke-virtual {p0}, Lcom/peel/h/a/gr;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    new-instance v1, Lcom/peel/h/a/gy;

    invoke-direct {v1, p0}, Lcom/peel/h/a/gy;-><init>(Lcom/peel/h/a/gr;)V

    invoke-static {v0, v1}, Lcom/peel/social/w;->a(Landroid/content/Context;Lcom/peel/util/t;)V

    .line 484
    return-void
.end method

.method private T()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 500
    invoke-virtual {p0}, Lcom/peel/h/a/gr;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    const-string/jumbo v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/ae;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 501
    sget v1, Lcom/peel/ui/fq;->about:I

    invoke-virtual {v0, v1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 502
    sget v0, Lcom/peel/ui/fp;->about_app_version:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/peel/h/a/gr;->m()Landroid/support/v4/app/ae;

    move-result-object v2

    invoke-static {v2}, Lcom/peel/util/bx;->g(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 503
    new-instance v0, Lcom/peel/h/a/hb;

    invoke-direct {v0, p0}, Lcom/peel/h/a/hb;-><init>(Lcom/peel/h/a/gr;)V

    invoke-virtual {v1, v0}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 582
    new-instance v0, Lcom/peel/widget/ag;

    invoke-virtual {p0}, Lcom/peel/h/a/gr;->m()Landroid/support/v4/app/ae;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/peel/widget/ag;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0}, Lcom/peel/widget/ag;->b()Lcom/peel/widget/ag;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/peel/widget/ag;->a(Landroid/view/View;)Lcom/peel/widget/ag;

    move-result-object v0

    sget v1, Lcom/peel/ui/ft;->ok:I

    invoke-virtual {v0, v1, v3}, Lcom/peel/widget/ag;->a(ILandroid/content/DialogInterface$OnClickListener;)Lcom/peel/widget/ag;

    move-result-object v0

    .line 583
    invoke-virtual {v0}, Lcom/peel/widget/ag;->show()V

    .line 584
    return-void
.end method

.method static synthetic a(Lcom/peel/h/a/gr;)Landroid/util/SparseArray;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/peel/h/a/gr;->h:Landroid/util/SparseArray;

    return-object v0
.end method

.method private a(Landroid/os/Bundle;Ljava/lang/String;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 262
    if-eqz p2, :cond_0

    .line 263
    sget v0, Lcom/peel/ui/ft;->settings_basic_info:I

    invoke-virtual {p0, v0}, Lcom/peel/h/a/gr;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 264
    const-string/jumbo v0, "clazz"

    const-class v1, Lcom/peel/h/a/av;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 280
    :cond_0
    :goto_0
    return-void

    .line 265
    :cond_1
    sget v0, Lcom/peel/ui/ft;->personalization:I

    invoke-virtual {p0, v0}, Lcom/peel/h/a/gr;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 266
    const-string/jumbo v0, "clazz"

    const-class v1, Lcom/peel/h/a/ff;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 267
    :cond_2
    sget v0, Lcom/peel/ui/ft;->settings_liked_programs:I

    invoke-virtual {p0, v0}, Lcom/peel/h/a/gr;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 268
    const-string/jumbo v0, "clazz"

    const-class v1, Lcom/peel/h/a/do;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 269
    const-string/jumbo v0, "type"

    invoke-virtual {p1, v0, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    goto :goto_0

    .line 270
    :cond_3
    sget v0, Lcom/peel/ui/ft;->settings_genres:I

    invoke-virtual {p0, v0}, Lcom/peel/h/a/gr;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 271
    const-string/jumbo v0, "clazz"

    const-class v1, Lcom/peel/h/a/ec;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 272
    const-string/jumbo v0, "type"

    invoke-virtual {p1, v0, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    goto :goto_0

    .line 273
    :cond_4
    sget v0, Lcom/peel/ui/ft;->settings_sports:I

    invoke-virtual {p0, v0}, Lcom/peel/h/a/gr;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 274
    const-string/jumbo v0, "clazz"

    const-class v1, Lcom/peel/h/a/ec;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 275
    const-string/jumbo v0, "type"

    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    goto :goto_0

    .line 276
    :cond_5
    sget v0, Lcom/peel/ui/ft;->settings_followed_team:I

    invoke-virtual {p0, v0}, Lcom/peel/h/a/gr;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 277
    const-string/jumbo v0, "clazz"

    const-class v1, Lcom/peel/h/a/hh;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method private a(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 178
    sget-boolean v0, Lcom/peel/util/b/a;->a:Z

    if-nez v0, :cond_1

    .line 179
    invoke-virtual {p0}, Lcom/peel/h/a/gr;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    invoke-static {v0}, Lcom/peel/d/v;->a(Landroid/content/Context;)Lcom/peel/d/v;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/d/v;->b()Lcom/peel/d/af;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 180
    new-instance v0, Lcom/peel/widget/ag;

    invoke-virtual {p0}, Lcom/peel/h/a/gr;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/peel/widget/ag;-><init>(Landroid/content/Context;)V

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget v2, Lcom/peel/ui/ft;->logout_twitter:I

    invoke-virtual {p0, v2}, Lcom/peel/h/a/gr;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "?"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/peel/widget/ag;->a(Ljava/lang/CharSequence;)Lcom/peel/widget/ag;

    move-result-object v0

    sget v1, Lcom/peel/ui/ft;->confirm_logout_twitter:I

    invoke-virtual {v0, v1}, Lcom/peel/widget/ag;->b(I)Lcom/peel/widget/ag;

    move-result-object v0

    sget v1, Lcom/peel/ui/ft;->ok:I

    new-instance v2, Lcom/peel/h/a/gx;

    invoke-direct {v2, p0, p1}, Lcom/peel/h/a/gx;-><init>(Lcom/peel/h/a/gr;Landroid/view/View;)V

    invoke-virtual {v0, v1, v2}, Lcom/peel/widget/ag;->a(ILandroid/content/DialogInterface$OnClickListener;)Lcom/peel/widget/ag;

    move-result-object v0

    sget v1, Lcom/peel/ui/ft;->cancel:I

    const/4 v2, 0x0

    .line 189
    invoke-virtual {v0, v1, v2}, Lcom/peel/widget/ag;->c(ILandroid/content/DialogInterface$OnClickListener;)Lcom/peel/widget/ag;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/h/a/gr;->i:Lcom/peel/widget/ag;

    .line 190
    iget-object v0, p0, Lcom/peel/h/a/gr;->i:Lcom/peel/widget/ag;

    invoke-virtual {v0}, Lcom/peel/widget/ag;->show()V

    .line 201
    :goto_0
    return-void

    .line 192
    :cond_0
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 193
    const-string/jumbo v1, "context_id"

    const/16 v2, 0x7d8

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 194
    invoke-virtual {p0}, Lcom/peel/h/a/gr;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    const-class v2, Lcom/peel/i/is;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lcom/peel/d/e;->c(Landroid/support/v4/app/ae;Ljava/lang/String;Landroid/os/Bundle;)V

    goto :goto_0

    .line 199
    :cond_1
    invoke-virtual {p0}, Lcom/peel/h/a/gr;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    invoke-virtual {p0}, Lcom/peel/h/a/gr;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/peel/util/bx;->a(Landroid/content/Context;Landroid/support/v4/app/ae;)V

    goto :goto_0
.end method

.method static synthetic a(Lcom/peel/h/a/gr;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 58
    invoke-direct {p0, p1}, Lcom/peel/h/a/gr;->a(Landroid/view/View;)V

    return-void
.end method

.method private b(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 204
    invoke-virtual {p0}, Lcom/peel/h/a/gr;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/ae;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/peel/social/w;->i(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 205
    invoke-virtual {p0}, Lcom/peel/h/a/gr;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/ae;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/peel/social/w;->e(Landroid/content/Context;)V

    .line 206
    invoke-direct {p0}, Lcom/peel/h/a/gr;->c()V

    .line 208
    :cond_0
    return-void
.end method

.method static synthetic b(Lcom/peel/h/a/gr;)V
    .locals 0

    .prologue
    .line 58
    invoke-direct {p0}, Lcom/peel/h/a/gr;->T()V

    return-void
.end method

.method static synthetic b(Lcom/peel/h/a/gr;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 58
    invoke-direct {p0, p1}, Lcom/peel/h/a/gr;->c(Landroid/view/View;)V

    return-void
.end method

.method static synthetic c(Lcom/peel/h/a/gr;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/peel/h/a/gr;->a:Ljava/lang/String;

    return-object v0
.end method

.method private c()V
    .locals 15

    .prologue
    const/4 v6, 0x1

    const/4 v1, 0x0

    .line 282
    iget-object v0, p0, Lcom/peel/h/a/gr;->g:Lcom/peel/h/a/gh;

    invoke-virtual {v0}, Lcom/peel/h/a/gh;->a()V

    .line 283
    invoke-virtual {p0}, Lcom/peel/h/a/gr;->n()Landroid/content/res/Resources;

    move-result-object v0

    sget v2, Lcom/peel/ui/fk;->settings_header_array:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v7

    .line 284
    new-instance v8, Landroid/util/SparseArray;

    invoke-direct {v8}, Landroid/util/SparseArray;-><init>()V

    move v0, v1

    move v2, v1

    .line 287
    :goto_0
    array-length v3, v7

    if-ge v0, v3, :cond_1b

    .line 288
    aget-object v9, v7, v0

    .line 290
    sget v3, Lcom/peel/ui/ft;->profile:I

    invoke-virtual {p0, v3}, Lcom/peel/h/a/gr;->a(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v9, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_9

    .line 291
    iget-object v3, p0, Lcom/peel/h/a/gr;->g:Lcom/peel/h/a/gh;

    invoke-virtual {v3, v2}, Lcom/peel/h/a/gh;->a(I)V

    .line 292
    add-int/lit8 v4, v2, 0x1

    invoke-virtual {v8, v2, v9}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 294
    invoke-virtual {p0}, Lcom/peel/h/a/gr;->m()Landroid/support/v4/app/ae;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/ae;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/peel/social/w;->f(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 295
    const/4 v2, -0x1

    .line 296
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 298
    invoke-virtual {p0}, Lcom/peel/h/a/gr;->m()Landroid/support/v4/app/ae;

    move-result-object v5

    invoke-virtual {v5}, Landroid/support/v4/app/ae;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5}, Lcom/peel/social/w;->g(Landroid/content/Context;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 299
    sget v2, Lcom/peel/ui/ft;->google_plus_label:I

    .line 306
    :cond_0
    :goto_1
    iget-object v5, p0, Lcom/peel/h/a/gr;->g:Lcom/peel/h/a/gh;

    invoke-virtual {v5, v4}, Lcom/peel/h/a/gh;->d(I)V

    .line 307
    const-string/jumbo v5, "account"

    invoke-virtual {v3, v5, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 308
    iget-object v5, p0, Lcom/peel/h/a/gr;->h:Landroid/util/SparseArray;

    invoke-virtual {v5, v4, v3}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 309
    add-int/lit8 v3, v4, 0x1

    invoke-virtual {p0, v2}, Lcom/peel/h/a/gr;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v8, v4, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    move v2, v3

    .line 315
    :goto_2
    iget-boolean v3, p0, Lcom/peel/h/a/gr;->f:Z

    if-nez v3, :cond_8

    .line 316
    invoke-virtual {p0}, Lcom/peel/h/a/gr;->m()Landroid/support/v4/app/ae;

    move-result-object v3

    invoke-static {v3}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v3

    const-string/jumbo v4, "country_ISO"

    const-string/jumbo v5, "US"

    invoke-interface {v3, v4, v5}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 317
    const-string/jumbo v3, "us"

    invoke-virtual {v9, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-virtual {p0}, Lcom/peel/h/a/gr;->n()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/peel/ui/fk;->settings_profile:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v3

    :goto_3
    move v4, v2

    move v2, v1

    .line 319
    :goto_4
    array-length v5, v3

    if-ge v2, v5, :cond_5

    .line 320
    new-instance v5, Landroid/os/Bundle;

    invoke-direct {v5}, Landroid/os/Bundle;-><init>()V

    .line 321
    aget-object v10, v3, v2

    invoke-direct {p0, v5, v10}, Lcom/peel/h/a/gr;->a(Landroid/os/Bundle;Ljava/lang/String;)V

    .line 322
    iget-object v10, p0, Lcom/peel/h/a/gr;->h:Landroid/util/SparseArray;

    invoke-virtual {v10, v4, v5}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 323
    add-int/lit8 v5, v4, 0x1

    aget-object v10, v3, v2

    invoke-virtual {v8, v4, v10}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 319
    add-int/lit8 v2, v2, 0x1

    move v4, v5

    goto :goto_4

    .line 300
    :cond_1
    invoke-virtual {p0}, Lcom/peel/h/a/gr;->m()Landroid/support/v4/app/ae;

    move-result-object v5

    invoke-virtual {v5}, Landroid/support/v4/app/ae;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5}, Lcom/peel/social/w;->i(Landroid/content/Context;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 301
    sget v2, Lcom/peel/ui/ft;->samsung_label:I

    goto :goto_1

    .line 302
    :cond_2
    invoke-virtual {p0}, Lcom/peel/h/a/gr;->m()Landroid/support/v4/app/ae;

    move-result-object v5

    invoke-virtual {v5}, Landroid/support/v4/app/ae;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5}, Lcom/peel/social/w;->h(Landroid/content/Context;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 303
    sget v2, Lcom/peel/ui/ft;->facebook_label:I

    goto/16 :goto_1

    .line 311
    :cond_3
    sget v2, Lcom/peel/ui/ft;->login:I

    invoke-virtual {p0, v2}, Lcom/peel/h/a/gr;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v8, v4, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 312
    iget-object v3, p0, Lcom/peel/h/a/gr;->g:Lcom/peel/h/a/gh;

    add-int/lit8 v2, v4, 0x1

    sget v5, Lcom/peel/ui/fq;->settings_login:I

    invoke-virtual {v3, v4, v5}, Lcom/peel/h/a/gh;->a(II)V

    goto :goto_2

    .line 317
    :cond_4
    invoke-virtual {p0}, Lcom/peel/h/a/gr;->n()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/peel/ui/fk;->settings_profile_without_personalizatioin:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v3

    goto :goto_3

    .line 325
    :cond_5
    const-string/jumbo v2, "us"

    invoke-virtual {v9, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_6

    const-string/jumbo v2, "CA"

    invoke-virtual {v9, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 326
    :cond_6
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 327
    sget v3, Lcom/peel/ui/ft;->settings_followed_team:I

    invoke-virtual {p0, v3}, Lcom/peel/h/a/gr;->a(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v2, v3}, Lcom/peel/h/a/gr;->a(Landroid/os/Bundle;Ljava/lang/String;)V

    .line 328
    iget-object v3, p0, Lcom/peel/h/a/gr;->h:Landroid/util/SparseArray;

    invoke-virtual {v3, v4, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 329
    add-int/lit8 v2, v4, 0x1

    sget v3, Lcom/peel/ui/ft;->settings_followed_team:I

    invoke-virtual {p0, v3}, Lcom/peel/h/a/gr;->a(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v8, v4, v3}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    move v4, v2

    :cond_7
    move v2, v4

    .line 287
    :cond_8
    :goto_5
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_0

    .line 332
    :cond_9
    sget v3, Lcom/peel/ui/ft;->label_help:I

    invoke-virtual {p0, v3}, Lcom/peel/h/a/gr;->a(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v9, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_e

    invoke-virtual {p0}, Lcom/peel/h/a/gr;->m()Landroid/support/v4/app/ae;

    move-result-object v3

    invoke-static {v3}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v3

    const-string/jumbo v4, "country_ISO"

    const-string/jumbo v5, "US"

    invoke-interface {v3, v4, v5}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v4, "XX"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_e

    .line 333
    iget-object v3, p0, Lcom/peel/h/a/gr;->g:Lcom/peel/h/a/gh;

    invoke-virtual {v3, v2}, Lcom/peel/h/a/gh;->a(I)V

    .line 334
    add-int/lit8 v3, v2, 0x1

    invoke-virtual {v8, v2, v9}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 336
    invoke-virtual {p0}, Lcom/peel/h/a/gr;->n()Landroid/content/res/Resources;

    move-result-object v2

    sget v4, Lcom/peel/ui/fk;->settings_help_2:I

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v5

    move v2, v1

    .line 338
    :goto_6
    array-length v4, v5

    if-ge v2, v4, :cond_d

    .line 339
    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    .line 340
    aget-object v9, v5, v2

    .line 341
    if-nez v2, :cond_b

    .line 342
    const-string/jumbo v10, "clazz"

    const-class v11, Lcom/peel/h/a/du;

    invoke-virtual {v11}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v4, v10, v11}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 343
    const-string/jumbo v10, "category"

    invoke-virtual {v4, v10, v9}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 353
    :cond_a
    :goto_7
    iget-object v10, p0, Lcom/peel/h/a/gr;->h:Landroid/util/SparseArray;

    invoke-virtual {v10, v3, v4}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 354
    add-int/lit8 v4, v3, 0x1

    invoke-virtual {v8, v3, v9}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 338
    add-int/lit8 v2, v2, 0x1

    move v3, v4

    goto :goto_6

    .line 347
    :cond_b
    if-ne v2, v6, :cond_c

    .line 348
    const-string/jumbo v10, "browse_forum"

    invoke-virtual {v4, v10, v6}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    goto :goto_7

    .line 349
    :cond_c
    const/4 v10, 0x2

    if-ne v2, v10, :cond_a

    .line 350
    const-string/jumbo v10, "about"

    invoke-virtual {v4, v10, v6}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    goto :goto_7

    :cond_d
    move v2, v3

    .line 356
    goto :goto_5

    :cond_e
    sget v3, Lcom/peel/ui/ft;->rooms:I

    invoke-virtual {p0, v3}, Lcom/peel/h/a/gr;->a(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v9, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_11

    .line 357
    iget-object v3, p0, Lcom/peel/h/a/gr;->g:Lcom/peel/h/a/gh;

    invoke-virtual {v3, v2}, Lcom/peel/h/a/gh;->a(I)V

    .line 358
    add-int/lit8 v3, v2, 0x1

    invoke-virtual {v8, v2, v9}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 360
    sget-object v2, Lcom/peel/content/a;->f:Lcom/peel/content/user/User;

    invoke-virtual {v2}, Lcom/peel/content/user/User;->k()Z

    move-result v2

    if-eqz v2, :cond_f

    .line 361
    sget-object v2, Lcom/peel/content/a;->f:Lcom/peel/content/user/User;

    invoke-virtual {v2}, Lcom/peel/content/user/User;->l()[Lcom/peel/data/ContentRoom;

    move-result-object v5

    array-length v9, v5

    move v2, v3

    move v3, v1

    :goto_8
    if-ge v3, v9, :cond_10

    aget-object v10, v5, v3

    .line 362
    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    .line 363
    const-string/jumbo v11, "clazz"

    const-class v12, Lcom/peel/h/a/fn;

    invoke-virtual {v12}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v4, v11, v12}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 364
    const-string/jumbo v11, "room"

    invoke-virtual {v4, v11, v10}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 365
    const-string/jumbo v11, "oldroom"

    invoke-static {}, Lcom/peel/content/a;->a()Lcom/peel/data/ContentRoom;

    move-result-object v12

    invoke-virtual {v4, v11, v12}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 366
    iget-object v11, p0, Lcom/peel/h/a/gr;->h:Landroid/util/SparseArray;

    invoke-virtual {v11, v2, v4}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 367
    add-int/lit8 v4, v2, 0x1

    invoke-virtual {v10}, Lcom/peel/data/ContentRoom;->c()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v2, v10}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 361
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    move v2, v4

    goto :goto_8

    :cond_f
    move v2, v3

    .line 370
    :cond_10
    iget-object v3, p0, Lcom/peel/h/a/gr;->g:Lcom/peel/h/a/gh;

    invoke-virtual {v3, v2}, Lcom/peel/h/a/gh;->c(I)V

    .line 371
    add-int/lit8 v4, v2, 0x1

    sget v3, Lcom/peel/ui/ft;->label_add_room:I

    invoke-virtual {p0, v3}, Lcom/peel/h/a/gr;->a(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v8, v2, v3}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    move v2, v4

    goto/16 :goto_5

    .line 372
    :cond_11
    sget v3, Lcom/peel/ui/ft;->settings_social_accounts:I

    invoke-virtual {p0, v3}, Lcom/peel/h/a/gr;->a(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v9, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_16

    .line 378
    invoke-virtual {p0}, Lcom/peel/h/a/gr;->n()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/peel/ui/fk;->settings_social:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v10

    .line 379
    array-length v11, v10

    move v5, v1

    move v3, v1

    move v4, v2

    :goto_9
    if-ge v5, v11, :cond_14

    aget-object v12, v10, v5

    .line 380
    iget-boolean v13, p0, Lcom/peel/h/a/gr;->f:Z

    if-nez v13, :cond_13

    sget v13, Lcom/peel/ui/ft;->twitter_label:I

    invoke-virtual {p0, v13}, Lcom/peel/h/a/gr;->a(I)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_13

    .line 381
    invoke-virtual {p0}, Lcom/peel/h/a/gr;->m()Landroid/support/v4/app/ae;

    move-result-object v13

    invoke-static {v13}, Lcom/peel/util/bx;->c(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v13

    .line 382
    if-eqz v13, :cond_12

    const-string/jumbo v14, "china"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v14

    if-nez v14, :cond_13

    const-string/jumbo v14, "cn"

    .line 383
    invoke-virtual {v13, v14}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v13

    if-nez v13, :cond_13

    .line 384
    :cond_12
    iget-object v3, p0, Lcom/peel/h/a/gr;->c:Landroid/os/Bundle;

    const-string/jumbo v13, "twitter"

    invoke-virtual {v3, v13, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 386
    iget-object v13, p0, Lcom/peel/h/a/gr;->h:Landroid/util/SparseArray;

    add-int/lit8 v3, v4, 0x1

    iget-object v4, p0, Lcom/peel/h/a/gr;->c:Landroid/os/Bundle;

    invoke-virtual {v13, v3, v4}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 387
    iget-object v4, p0, Lcom/peel/h/a/gr;->g:Lcom/peel/h/a/gh;

    invoke-virtual {v4, v3}, Lcom/peel/h/a/gh;->d(I)V

    .line 388
    invoke-virtual {v8, v3, v12}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    move v4, v3

    move v3, v6

    .line 379
    :cond_13
    add-int/lit8 v5, v5, 0x1

    goto :goto_9

    .line 393
    :cond_14
    if-eqz v3, :cond_15

    invoke-virtual {v8, v2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v3

    if-nez v3, :cond_15

    .line 394
    iget-object v3, p0, Lcom/peel/h/a/gr;->g:Lcom/peel/h/a/gh;

    invoke-virtual {v3, v2}, Lcom/peel/h/a/gh;->a(I)V

    .line 395
    invoke-virtual {v8, v2, v9}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 396
    add-int/lit8 v4, v4, 0x1

    :cond_15
    move v2, v4

    .line 398
    goto/16 :goto_5

    :cond_16
    sget v3, Lcom/peel/ui/ft;->accessibility_remote_control:I

    invoke-virtual {p0, v3}, Lcom/peel/h/a/gr;->a(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v9, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_8

    .line 399
    iget-object v3, p0, Lcom/peel/h/a/gr;->g:Lcom/peel/h/a/gh;

    invoke-virtual {v3, v2}, Lcom/peel/h/a/gh;->a(I)V

    .line 400
    add-int/lit8 v3, v2, 0x1

    invoke-virtual {v8, v2, v9}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 402
    invoke-virtual {p0}, Lcom/peel/h/a/gr;->n()Landroid/content/res/Resources;

    move-result-object v2

    sget v4, Lcom/peel/ui/fk;->settings_remote:I

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v5

    .line 404
    array-length v9, v5

    move v4, v1

    move v2, v3

    :goto_a
    if-ge v4, v9, :cond_8

    aget-object v10, v5, v4

    .line 405
    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v11, 0x15

    if-lt v3, v11, :cond_17

    sget v3, Lcom/peel/ui/ft;->auto_display_lock:I

    invoke-virtual {p0, v3}, Lcom/peel/h/a/gr;->a(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v10, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_17

    .line 404
    :goto_b
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    goto :goto_a

    .line 409
    :cond_17
    iget-object v3, p0, Lcom/peel/h/a/gr;->g:Lcom/peel/h/a/gh;

    invoke-virtual {v3, v2}, Lcom/peel/h/a/gh;->e(I)V

    .line 410
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 411
    sget v11, Lcom/peel/ui/ft;->label_notification:I

    invoke-virtual {p0, v11}, Lcom/peel/h/a/gr;->a(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_18

    .line 412
    const-string/jumbo v11, "extra"

    const-string/jumbo v12, "notification"

    invoke-virtual {v3, v11, v12}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 420
    :goto_c
    iget-object v11, p0, Lcom/peel/h/a/gr;->h:Landroid/util/SparseArray;

    invoke-virtual {v11, v2, v3}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 421
    add-int/lit8 v3, v2, 0x1

    invoke-virtual {v8, v2, v10}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    move v2, v3

    goto :goto_b

    .line 414
    :cond_18
    sget-object v11, Lcom/peel/c/a;->b:Lcom/peel/c/e;

    invoke-static {v11}, Lcom/peel/c/a;->a(Lcom/peel/c/e;)Ljava/lang/Object;

    move-result-object v11

    sget-object v12, Lcom/peel/c/b;->a:Lcom/peel/c/b;

    if-eq v11, v12, :cond_19

    sget-object v11, Lcom/peel/c/a;->b:Lcom/peel/c/e;

    invoke-static {v11}, Lcom/peel/c/a;->a(Lcom/peel/c/e;)Ljava/lang/Object;

    move-result-object v11

    sget-object v12, Lcom/peel/c/b;->b:Lcom/peel/c/b;

    if-ne v11, v12, :cond_1a

    .line 415
    :cond_19
    const-string/jumbo v11, "extra"

    const-string/jumbo v12, "lockscreen"

    invoke-virtual {v3, v11, v12}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_c

    .line 417
    :cond_1a
    const-string/jumbo v11, "extra"

    const-string/jumbo v12, "alwaysonremotewidget"

    invoke-virtual {v3, v11, v12}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_c

    .line 425
    :cond_1b
    iget-object v0, p0, Lcom/peel/h/a/gr;->g:Lcom/peel/h/a/gh;

    invoke-virtual {v0, v2}, Lcom/peel/h/a/gh;->b(I)V

    .line 426
    sget v0, Lcom/peel/ui/ft;->resetpeelapp:I

    invoke-virtual {p0, v0}, Lcom/peel/h/a/gr;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v8, v2, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 428
    iget-object v0, p0, Lcom/peel/h/a/gr;->g:Lcom/peel/h/a/gh;

    invoke-virtual {v0, v8}, Lcom/peel/h/a/gh;->a(Landroid/util/SparseArray;)V

    .line 429
    iget-object v0, p0, Lcom/peel/h/a/gr;->g:Lcom/peel/h/a/gh;

    invoke-virtual {v0}, Lcom/peel/h/a/gh;->notifyDataSetChanged()V

    .line 430
    return-void
.end method

.method private c(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 211
    invoke-virtual {p0}, Lcom/peel/h/a/gr;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    invoke-static {v0}, Lcom/peel/social/w;->g(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 212
    invoke-virtual {p0}, Lcom/peel/h/a/gr;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    invoke-static {v0}, Lcom/peel/social/w;->c(Landroid/content/Context;)V

    .line 214
    invoke-direct {p0}, Lcom/peel/h/a/gr;->c()V

    .line 216
    :cond_0
    return-void
.end method

.method static synthetic c(Lcom/peel/h/a/gr;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 58
    invoke-direct {p0, p1}, Lcom/peel/h/a/gr;->b(Landroid/view/View;)V

    return-void
.end method

.method private d(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 219
    invoke-virtual {p0}, Lcom/peel/h/a/gr;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    invoke-static {v0}, Lcom/peel/social/w;->h(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 220
    invoke-virtual {p0}, Lcom/peel/h/a/gr;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    invoke-static {v0}, Lcom/peel/social/w;->d(Landroid/content/Context;)V

    .line 222
    invoke-direct {p0}, Lcom/peel/h/a/gr;->c()V

    .line 224
    :cond_0
    return-void
.end method

.method static synthetic d(Lcom/peel/h/a/gr;)V
    .locals 0

    .prologue
    .line 58
    invoke-direct {p0}, Lcom/peel/h/a/gr;->c()V

    return-void
.end method

.method static synthetic d(Lcom/peel/h/a/gr;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 58
    invoke-direct {p0, p1}, Lcom/peel/h/a/gr;->d(Landroid/view/View;)V

    return-void
.end method

.method static synthetic e(Lcom/peel/h/a/gr;)V
    .locals 0

    .prologue
    .line 58
    invoke-direct {p0}, Lcom/peel/h/a/gr;->S()V

    return-void
.end method


# virtual methods
.method public X()Z
    .locals 1

    .prologue
    .line 72
    const/4 v0, 0x0

    return v0
.end method

.method public Z()V
    .locals 6

    .prologue
    .line 453
    iget-object v0, p0, Lcom/peel/h/a/gr;->d:Lcom/peel/d/a;

    if-nez v0, :cond_0

    .line 454
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 456
    new-instance v0, Lcom/peel/d/a;

    sget-object v1, Lcom/peel/d/d;->b:Lcom/peel/d/d;

    sget-object v2, Lcom/peel/d/b;->a:Lcom/peel/d/b;

    sget-object v3, Lcom/peel/d/c;->b:Lcom/peel/d/c;

    sget v4, Lcom/peel/ui/ft;->setting:I

    .line 457
    invoke-virtual {p0, v4}, Lcom/peel/h/a/gr;->a(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct/range {v0 .. v5}, Lcom/peel/d/a;-><init>(Lcom/peel/d/d;Lcom/peel/d/b;Lcom/peel/d/c;Ljava/lang/String;Ljava/util/List;)V

    iput-object v0, p0, Lcom/peel/h/a/gr;->d:Lcom/peel/d/a;

    .line 459
    :cond_0
    iget-object v0, p0, Lcom/peel/h/a/gr;->b:Lcom/peel/d/i;

    iget-object v1, p0, Lcom/peel/h/a/gr;->d:Lcom/peel/d/a;

    invoke-virtual {v0, v1}, Lcom/peel/d/i;->a(Lcom/peel/d/a;)V

    .line 461
    iget-object v0, p0, Lcom/peel/h/a/gr;->g:Lcom/peel/h/a/gh;

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/peel/h/a/gr;->c()V

    .line 462
    :cond_1
    return-void
.end method

.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    .line 77
    sget v0, Lcom/peel/ui/fq;->settings_main_view:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 78
    sget v0, Lcom/peel/ui/fp;->settings_list:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/peel/h/a/gr;->e:Landroid/widget/ListView;

    .line 79
    return-object v1
.end method

.method public a(IILandroid/content/Intent;)V
    .locals 3

    .prologue
    .line 166
    const/16 v0, 0x1f41

    if-eq p1, v0, :cond_0

    sget v0, Lcom/peel/social/w;->b:I

    if-ne p1, v0, :cond_1

    .line 168
    :cond_0
    iget-object v0, p0, Lcom/peel/h/a/gr;->a:Ljava/lang/String;

    const-string/jumbo v1, "refresh adapter"

    new-instance v2, Lcom/peel/h/a/gw;

    invoke-direct {v2, p0}, Lcom/peel/h/a/gw;-><init>(Lcom/peel/h/a/gr;)V

    invoke-static {v0, v1, v2}, Lcom/peel/util/i;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/lang/Runnable;

    .line 175
    :cond_1
    return-void
.end method

.method public a(Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 440
    invoke-super {p0, p1}, Lcom/peel/d/u;->a(Landroid/view/MenuItem;)Z

    move-result v0

    return v0
.end method

.method public c(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 254
    invoke-super {p0, p1}, Lcom/peel/d/u;->c(Landroid/os/Bundle;)V

    .line 255
    sget-object v0, Lcom/peel/content/a;->h:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/peel/h/a/gr;->b:Lcom/peel/d/i;

    if-nez v0, :cond_1

    .line 259
    :cond_0
    :goto_0
    return-void

    .line 258
    :cond_1
    invoke-direct {p0}, Lcom/peel/h/a/gr;->c()V

    goto :goto_0
.end method

.method public d(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 84
    invoke-super {p0, p1}, Lcom/peel/d/u;->d(Landroid/os/Bundle;)V

    .line 85
    iget-object v2, p0, Lcom/peel/h/a/gr;->c:Landroid/os/Bundle;

    const-string/jumbo v3, "category"

    sget v4, Lcom/peel/ui/ft;->label_settings:I

    invoke-virtual {p0, v4}, Lcom/peel/h/a/gr;->a(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 87
    invoke-virtual {p0}, Lcom/peel/h/a/gr;->m()Landroid/support/v4/app/ae;

    move-result-object v2

    invoke-static {v2}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v2

    .line 88
    const-string/jumbo v3, "setup_type"

    invoke-interface {v2, v3, v1}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v2

    if-ne v0, v2, :cond_1

    :goto_0
    iput-boolean v0, p0, Lcom/peel/h/a/gr;->f:Z

    .line 89
    new-instance v0, Lcom/peel/h/a/gh;

    invoke-virtual {p0}, Lcom/peel/h/a/gr;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    iget-object v2, p0, Lcom/peel/h/a/gr;->b:Lcom/peel/d/i;

    invoke-direct {v0, v1, v2}, Lcom/peel/h/a/gh;-><init>(Landroid/support/v4/app/ae;Lcom/peel/d/i;)V

    iput-object v0, p0, Lcom/peel/h/a/gr;->g:Lcom/peel/h/a/gh;

    .line 90
    iget-object v0, p0, Lcom/peel/h/a/gr;->e:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/peel/h/a/gr;->g:Lcom/peel/h/a/gh;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 91
    iget-object v0, p0, Lcom/peel/h/a/gr;->e:Landroid/widget/ListView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setDivider(Landroid/graphics/drawable/Drawable;)V

    .line 93
    iget-object v0, p0, Lcom/peel/h/a/gr;->e:Landroid/widget/ListView;

    new-instance v1, Lcom/peel/h/a/gs;

    invoke-direct {v1, p0}, Lcom/peel/h/a/gs;-><init>(Lcom/peel/h/a/gr;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 136
    new-instance v0, Lcom/peel/h/a/gt;

    invoke-direct {v0, p0}, Lcom/peel/h/a/gt;-><init>(Lcom/peel/h/a/gr;)V

    iput-object v0, p0, Lcom/peel/h/a/gr;->aj:Lcom/peel/h/a/hg;

    .line 157
    iget-object v0, p0, Lcom/peel/h/a/gr;->g:Lcom/peel/h/a/gh;

    iget-object v1, p0, Lcom/peel/h/a/gr;->aj:Lcom/peel/h/a/hg;

    invoke-virtual {v0, v1}, Lcom/peel/h/a/gh;->a(Lcom/peel/h/a/hg;)V

    .line 159
    iget-object v0, p0, Lcom/peel/h/a/gr;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "type"

    const/4 v2, 0x4

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 160
    sget-object v0, Lcom/peel/content/a;->h:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 161
    iget-object v0, p0, Lcom/peel/h/a/gr;->c:Landroid/os/Bundle;

    invoke-virtual {p0, v0}, Lcom/peel/h/a/gr;->c(Landroid/os/Bundle;)V

    .line 162
    :cond_0
    return-void

    :cond_1
    move v0, v1

    .line 88
    goto :goto_0
.end method

.method public f()V
    .locals 3

    .prologue
    .line 229
    invoke-super {p0}, Lcom/peel/d/u;->f()V

    .line 231
    invoke-virtual {p0}, Lcom/peel/h/a/gr;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    const-string/jumbo v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/ae;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 232
    invoke-virtual {p0}, Lcom/peel/h/a/gr;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/ae;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 234
    iget-object v0, p0, Lcom/peel/h/a/gr;->i:Lcom/peel/widget/ag;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/peel/h/a/gr;->i:Lcom/peel/widget/ag;

    invoke-virtual {v0}, Lcom/peel/widget/ag;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 235
    iget-object v0, p0, Lcom/peel/h/a/gr;->i:Lcom/peel/widget/ag;

    invoke-virtual {v0}, Lcom/peel/widget/ag;->dismiss()V

    .line 237
    :cond_0
    sget-object v0, Lcom/peel/h/a/gh;->a:Lcom/peel/widget/ag;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/peel/h/a/gh;->a:Lcom/peel/widget/ag;

    invoke-virtual {v0}, Lcom/peel/widget/ag;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 238
    sget-object v0, Lcom/peel/h/a/gh;->a:Lcom/peel/widget/ag;

    invoke-virtual {v0}, Lcom/peel/widget/ag;->dismiss()V

    .line 240
    :cond_1
    return-void
.end method

.method public h(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 245
    invoke-super {p0, p1}, Lcom/peel/d/u;->h(Landroid/os/Bundle;)V

    .line 246
    sget-object v0, Lcom/peel/content/a;->h:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 247
    iget-object v0, p0, Lcom/peel/h/a/gr;->b:Lcom/peel/d/i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/peel/h/a/gr;->b:Lcom/peel/d/i;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/peel/d/i;->a(Z)V

    .line 249
    :cond_0
    invoke-virtual {p0}, Lcom/peel/h/a/gr;->Z()V

    .line 250
    return-void
.end method

.method public w()V
    .locals 4

    .prologue
    .line 445
    invoke-super {p0}, Lcom/peel/d/u;->w()V

    .line 446
    invoke-virtual {p0}, Lcom/peel/h/a/gr;->Z()V

    .line 447
    invoke-virtual {p0}, Lcom/peel/h/a/gr;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    invoke-static {v0}, Landroid/support/v4/a/q;->a(Landroid/content/Context;)Landroid/support/v4/a/q;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/h/a/gr;->ak:Landroid/content/BroadcastReceiver;

    new-instance v2, Landroid/content/IntentFilter;

    const-string/jumbo v3, "samsung_account"

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/a/q;->a(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)V

    .line 448
    return-void
.end method

.method public x()V
    .locals 2

    .prologue
    .line 434
    invoke-super {p0}, Lcom/peel/d/u;->x()V

    .line 435
    invoke-virtual {p0}, Lcom/peel/h/a/gr;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    invoke-static {v0}, Landroid/support/v4/a/q;->a(Landroid/content/Context;)Landroid/support/v4/a/q;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/h/a/gr;->ak:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/support/v4/a/q;->a(Landroid/content/BroadcastReceiver;)V

    .line 436
    return-void
.end method
