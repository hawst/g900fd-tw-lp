.class Lcom/peel/h/a/cv;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/support/v4/view/cs;


# instance fields
.field final synthetic a:Lcom/peel/h/a/cp;


# direct methods
.method constructor <init>(Lcom/peel/h/a/cp;)V
    .locals 0

    .prologue
    .line 162
    iput-object p1, p0, Lcom/peel/h/a/cv;->a:Lcom/peel/h/a/cp;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(I)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v5, 0x0

    const/16 v4, 0x8

    .line 170
    iget-object v0, p0, Lcom/peel/h/a/cv;->a:Lcom/peel/h/a/cp;

    invoke-static {v0}, Lcom/peel/h/a/cp;->b(Lcom/peel/h/a/cp;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 171
    iget-object v0, p0, Lcom/peel/h/a/cv;->a:Lcom/peel/h/a/cp;

    invoke-static {v0}, Lcom/peel/h/a/cp;->c(Lcom/peel/h/a/cp;)Landroid/widget/RelativeLayout;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 172
    iget-object v0, p0, Lcom/peel/h/a/cv;->a:Lcom/peel/h/a/cp;

    invoke-static {v0}, Lcom/peel/h/a/cp;->d(Lcom/peel/h/a/cp;)Landroid/widget/RelativeLayout;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 174
    iget-object v0, p0, Lcom/peel/h/a/cv;->a:Lcom/peel/h/a/cp;

    invoke-static {v0}, Lcom/peel/h/a/cp;->a(Lcom/peel/h/a/cp;)Lcom/peel/widget/TestBtnViewPager;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "btnView"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/peel/h/a/cv;->a:Lcom/peel/h/a/cp;

    invoke-static {v3}, Lcom/peel/h/a/cp;->a(Lcom/peel/h/a/cp;)Lcom/peel/widget/TestBtnViewPager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/peel/widget/TestBtnViewPager;->getCurrentItem()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/peel/widget/TestBtnViewPager;->findViewWithTag(Ljava/lang/Object;)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 175
    iget-object v0, p0, Lcom/peel/h/a/cv;->a:Lcom/peel/h/a/cp;

    invoke-static {v0}, Lcom/peel/h/a/cp;->a(Lcom/peel/h/a/cp;)Lcom/peel/widget/TestBtnViewPager;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "btnView"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/peel/h/a/cv;->a:Lcom/peel/h/a/cp;

    invoke-static {v3}, Lcom/peel/h/a/cp;->a(Lcom/peel/h/a/cp;)Lcom/peel/widget/TestBtnViewPager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/peel/widget/TestBtnViewPager;->getCurrentItem()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/peel/widget/TestBtnViewPager;->findViewWithTag(Ljava/lang/Object;)Landroid/view/View;

    move-result-object v0

    sget v2, Lcom/peel/ui/fp;->test_tune_btn_large_view:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    .line 176
    iget-object v0, p0, Lcom/peel/h/a/cv;->a:Lcom/peel/h/a/cp;

    invoke-static {v0}, Lcom/peel/h/a/cp;->a(Lcom/peel/h/a/cp;)Lcom/peel/widget/TestBtnViewPager;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "btnView"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/peel/h/a/cv;->a:Lcom/peel/h/a/cp;

    invoke-static {v3}, Lcom/peel/h/a/cp;->a(Lcom/peel/h/a/cp;)Lcom/peel/widget/TestBtnViewPager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/peel/widget/TestBtnViewPager;->getCurrentItem()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/peel/widget/TestBtnViewPager;->findViewWithTag(Ljava/lang/Object;)Landroid/view/View;

    move-result-object v0

    sget v2, Lcom/peel/ui/fp;->test_tune_btn_small_view:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 179
    :cond_0
    iget-object v0, p0, Lcom/peel/h/a/cv;->a:Lcom/peel/h/a/cp;

    invoke-static {v0}, Lcom/peel/h/a/cp;->a(Lcom/peel/h/a/cp;)Lcom/peel/widget/TestBtnViewPager;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "btnView"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/peel/h/a/cv;->a:Lcom/peel/h/a/cp;

    invoke-static {v3}, Lcom/peel/h/a/cp;->a(Lcom/peel/h/a/cp;)Lcom/peel/widget/TestBtnViewPager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/peel/widget/TestBtnViewPager;->getCurrentItem()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/peel/widget/TestBtnViewPager;->findViewWithTag(Ljava/lang/Object;)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 180
    iget-object v0, p0, Lcom/peel/h/a/cv;->a:Lcom/peel/h/a/cp;

    invoke-static {v0}, Lcom/peel/h/a/cp;->a(Lcom/peel/h/a/cp;)Lcom/peel/widget/TestBtnViewPager;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "btnView"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/peel/h/a/cv;->a:Lcom/peel/h/a/cp;

    invoke-static {v3}, Lcom/peel/h/a/cp;->a(Lcom/peel/h/a/cp;)Lcom/peel/widget/TestBtnViewPager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/peel/widget/TestBtnViewPager;->getCurrentItem()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/peel/widget/TestBtnViewPager;->findViewWithTag(Ljava/lang/Object;)Landroid/view/View;

    move-result-object v0

    sget v2, Lcom/peel/ui/fp;->test_tune_btn_large_view:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 181
    iget-object v0, p0, Lcom/peel/h/a/cv;->a:Lcom/peel/h/a/cp;

    invoke-static {v0}, Lcom/peel/h/a/cp;->a(Lcom/peel/h/a/cp;)Lcom/peel/widget/TestBtnViewPager;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "btnView"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/peel/h/a/cv;->a:Lcom/peel/h/a/cp;

    invoke-static {v3}, Lcom/peel/h/a/cp;->a(Lcom/peel/h/a/cp;)Lcom/peel/widget/TestBtnViewPager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/peel/widget/TestBtnViewPager;->getCurrentItem()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/peel/widget/TestBtnViewPager;->findViewWithTag(Ljava/lang/Object;)Landroid/view/View;

    move-result-object v0

    sget v2, Lcom/peel/ui/fp;->test_tune_btn_small_view:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    .line 182
    iget-object v0, p0, Lcom/peel/h/a/cv;->a:Lcom/peel/h/a/cp;

    invoke-static {v0}, Lcom/peel/h/a/cp;->a(Lcom/peel/h/a/cp;)Lcom/peel/widget/TestBtnViewPager;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "btnView"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/peel/h/a/cv;->a:Lcom/peel/h/a/cp;

    invoke-static {v3}, Lcom/peel/h/a/cp;->a(Lcom/peel/h/a/cp;)Lcom/peel/widget/TestBtnViewPager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/peel/widget/TestBtnViewPager;->getCurrentItem()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/peel/widget/TestBtnViewPager;->findViewWithTag(Ljava/lang/Object;)Landroid/view/View;

    move-result-object v0

    sget v2, Lcom/peel/ui/fp;->channel_num_text_small:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v2, p0, Lcom/peel/h/a/cv;->a:Lcom/peel/h/a/cp;

    invoke-static {v2}, Lcom/peel/h/a/cp;->e(Lcom/peel/h/a/cp;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 185
    :cond_1
    iget-object v0, p0, Lcom/peel/h/a/cv;->a:Lcom/peel/h/a/cp;

    invoke-static {v0}, Lcom/peel/h/a/cp;->a(Lcom/peel/h/a/cp;)Lcom/peel/widget/TestBtnViewPager;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "btnView"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/peel/h/a/cv;->a:Lcom/peel/h/a/cp;

    invoke-static {v3}, Lcom/peel/h/a/cp;->a(Lcom/peel/h/a/cp;)Lcom/peel/widget/TestBtnViewPager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/peel/widget/TestBtnViewPager;->getCurrentItem()I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/peel/widget/TestBtnViewPager;->findViewWithTag(Ljava/lang/Object;)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 186
    iget-object v0, p0, Lcom/peel/h/a/cv;->a:Lcom/peel/h/a/cp;

    invoke-static {v0}, Lcom/peel/h/a/cp;->a(Lcom/peel/h/a/cp;)Lcom/peel/widget/TestBtnViewPager;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "btnView"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/peel/h/a/cv;->a:Lcom/peel/h/a/cp;

    invoke-static {v3}, Lcom/peel/h/a/cp;->a(Lcom/peel/h/a/cp;)Lcom/peel/widget/TestBtnViewPager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/peel/widget/TestBtnViewPager;->getCurrentItem()I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/peel/widget/TestBtnViewPager;->findViewWithTag(Ljava/lang/Object;)Landroid/view/View;

    move-result-object v0

    sget v2, Lcom/peel/ui/fp;->test_tune_btn_large_view:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 187
    iget-object v0, p0, Lcom/peel/h/a/cv;->a:Lcom/peel/h/a/cp;

    invoke-static {v0}, Lcom/peel/h/a/cp;->a(Lcom/peel/h/a/cp;)Lcom/peel/widget/TestBtnViewPager;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "btnView"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/peel/h/a/cv;->a:Lcom/peel/h/a/cp;

    invoke-static {v3}, Lcom/peel/h/a/cp;->a(Lcom/peel/h/a/cp;)Lcom/peel/widget/TestBtnViewPager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/peel/widget/TestBtnViewPager;->getCurrentItem()I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/peel/widget/TestBtnViewPager;->findViewWithTag(Ljava/lang/Object;)Landroid/view/View;

    move-result-object v0

    sget v2, Lcom/peel/ui/fp;->test_tune_btn_small_view:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    .line 188
    iget-object v0, p0, Lcom/peel/h/a/cv;->a:Lcom/peel/h/a/cp;

    invoke-static {v0}, Lcom/peel/h/a/cp;->a(Lcom/peel/h/a/cp;)Lcom/peel/widget/TestBtnViewPager;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "btnView"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/peel/h/a/cv;->a:Lcom/peel/h/a/cp;

    invoke-static {v3}, Lcom/peel/h/a/cp;->a(Lcom/peel/h/a/cp;)Lcom/peel/widget/TestBtnViewPager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/peel/widget/TestBtnViewPager;->getCurrentItem()I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/peel/widget/TestBtnViewPager;->findViewWithTag(Ljava/lang/Object;)Landroid/view/View;

    move-result-object v0

    sget v2, Lcom/peel/ui/fp;->channel_num_text_small:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v2, p0, Lcom/peel/h/a/cv;->a:Lcom/peel/h/a/cp;

    invoke-static {v2}, Lcom/peel/h/a/cp;->e(Lcom/peel/h/a/cp;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 191
    :cond_2
    if-nez p1, :cond_3

    .line 192
    const/16 v0, 0x1f4

    invoke-static {v0}, Lcom/peel/h/a/cp;->b(I)I

    .line 198
    :goto_0
    iget-object v0, p0, Lcom/peel/h/a/cv;->a:Lcom/peel/h/a/cp;

    invoke-virtual {v0}, Lcom/peel/h/a/cp;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    const-string/jumbo v2, "custom_delay"

    invoke-static {}, Lcom/peel/h/a/cp;->S()I

    move-result v3

    invoke-static {v0, v2, v3}, Lcom/peel/util/dq;->a(Landroid/content/Context;Ljava/lang/String;I)V

    .line 199
    invoke-static {}, Lcom/peel/util/a/f;->a()Lcom/peel/util/a/f;

    move-result-object v0

    sget-object v2, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v2}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v2

    if-nez v2, :cond_5

    :goto_1
    const/16 v2, 0x4e5

    iget-object v3, p0, Lcom/peel/h/a/cv;->a:Lcom/peel/h/a/cp;

    .line 200
    invoke-static {v3}, Lcom/peel/h/a/cp;->f(Lcom/peel/h/a/cp;)I

    move-result v3

    const-string/jumbo v4, "custom_delay"

    invoke-static {}, Lcom/peel/h/a/cp;->S()I

    move-result v5

    .line 199
    invoke-virtual/range {v0 .. v5}, Lcom/peel/util/a/f;->a(IIILjava/lang/String;I)V

    .line 201
    return-void

    .line 193
    :cond_3
    if-ne p1, v1, :cond_4

    .line 194
    const/16 v0, 0xc8

    invoke-static {v0}, Lcom/peel/h/a/cp;->b(I)I

    goto :goto_0

    .line 196
    :cond_4
    const/16 v0, 0x32

    invoke-static {v0}, Lcom/peel/h/a/cp;->b(I)I

    goto :goto_0

    .line 199
    :cond_5
    sget-object v1, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v1}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/data/at;->f()I

    move-result v1

    goto :goto_1
.end method

.method public a(IFI)V
    .locals 0

    .prologue
    .line 166
    return-void
.end method

.method public b(I)V
    .locals 0

    .prologue
    .line 206
    return-void
.end method
