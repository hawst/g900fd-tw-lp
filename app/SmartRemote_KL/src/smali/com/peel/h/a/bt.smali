.class Lcom/peel/h/a/bt;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic a:I

.field final synthetic b:Landroid/view/View;

.field final synthetic c:Landroid/widget/AdapterView;

.field final synthetic d:Lcom/peel/h/a/bs;


# direct methods
.method constructor <init>(Lcom/peel/h/a/bs;ILandroid/view/View;Landroid/widget/AdapterView;)V
    .locals 0

    .prologue
    .line 585
    iput-object p1, p0, Lcom/peel/h/a/bt;->d:Lcom/peel/h/a/bs;

    iput p2, p0, Lcom/peel/h/a/bt;->a:I

    iput-object p3, p0, Lcom/peel/h/a/bt;->b:Landroid/view/View;

    iput-object p4, p0, Lcom/peel/h/a/bt;->c:Landroid/widget/AdapterView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 5

    .prologue
    .line 588
    iget-object v0, p0, Lcom/peel/h/a/bt;->d:Lcom/peel/h/a/bs;

    iget-object v0, v0, Lcom/peel/h/a/bs;->a:Lcom/peel/h/a/bb;

    invoke-static {v0}, Lcom/peel/h/a/bb;->a(Lcom/peel/h/a/bb;)Landroid/widget/ListView;

    move-result-object v0

    iget v1, p0, Lcom/peel/h/a/bt;->a:I

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/data/Channel;

    .line 590
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 591
    const-string/jumbo v1, "path"

    const-string/jumbo v3, "channel/setalias"

    invoke-virtual {v2, v1, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 592
    const-string/jumbo v1, "id"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/peel/h/a/bt;->d:Lcom/peel/h/a/bs;

    iget-object v4, v4, Lcom/peel/h/a/bs;->a:Lcom/peel/h/a/bb;

    invoke-static {v4}, Lcom/peel/h/a/bb;->h(Lcom/peel/h/a/bb;)Lcom/peel/data/ContentRoom;

    move-result-object v4

    invoke-virtual {v4}, Lcom/peel/data/ContentRoom;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/peel/h/a/bt;->d:Lcom/peel/h/a/bs;

    iget-object v4, v4, Lcom/peel/h/a/bs;->a:Lcom/peel/h/a/bb;

    invoke-static {v4}, Lcom/peel/h/a/bb;->e(Lcom/peel/h/a/bb;)Lcom/peel/content/library/LiveLibrary;

    move-result-object v4

    invoke-virtual {v4}, Lcom/peel/content/library/LiveLibrary;->e()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Lcom/peel/data/Channel;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v1, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 594
    iget-object v1, p0, Lcom/peel/h/a/bt;->b:Landroid/view/View;

    sget v3, Lcom/peel/ui/fp;->edittext:I

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    .line 595
    if-eqz v1, :cond_0

    invoke-interface {v1}, Landroid/text/Editable;->length()I

    move-result v3

    if-nez v3, :cond_1

    .line 596
    :cond_0
    const-string/jumbo v1, ""

    invoke-virtual {v0, v1}, Lcom/peel/data/Channel;->b(Ljava/lang/String;)V

    .line 597
    const-string/jumbo v1, "alias"

    const-string/jumbo v3, ""

    invoke-virtual {v2, v1, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 603
    :goto_0
    sget-object v1, Lcom/peel/content/a;->f:Lcom/peel/content/user/User;

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Lcom/peel/content/user/User;->a(Landroid/os/Bundle;Lcom/peel/util/t;)V

    .line 604
    iget-object v1, p0, Lcom/peel/h/a/bt;->b:Landroid/view/View;

    sget v2, Lcom/peel/ui/fp;->edittext:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    invoke-virtual {v0}, Lcom/peel/data/Channel;->m()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 606
    iget-object v0, p0, Lcom/peel/h/a/bt;->c:Landroid/widget/AdapterView;

    invoke-virtual {v0}, Landroid/widget/AdapterView;->getAdapter()Landroid/widget/Adapter;

    move-result-object v0

    check-cast v0, Landroid/widget/HeaderViewListAdapter;

    invoke-virtual {v0}, Landroid/widget/HeaderViewListAdapter;->getWrappedAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lcom/peel/h/a/bw;

    invoke-virtual {v0}, Lcom/peel/h/a/bw;->a()V

    .line 609
    iget-object v0, p0, Lcom/peel/h/a/bt;->d:Lcom/peel/h/a/bs;

    iget-object v0, v0, Lcom/peel/h/a/bs;->a:Lcom/peel/h/a/bb;

    invoke-static {v0}, Lcom/peel/h/a/bb;->e(Lcom/peel/h/a/bb;)Lcom/peel/content/library/LiveLibrary;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/content/library/LiveLibrary;->e()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/peel/content/a;->e(Ljava/lang/String;)V

    .line 611
    return-void

    .line 599
    :cond_1
    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/peel/data/Channel;->b(Ljava/lang/String;)V

    .line 600
    const-string/jumbo v3, "alias"

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v3, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
