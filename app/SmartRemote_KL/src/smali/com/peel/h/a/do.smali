.class public Lcom/peel/h/a/do;
.super Lcom/peel/d/u;


# static fields
.field private static final aj:Ljava/lang/String;


# instance fields
.field private ak:Lcom/peel/content/library/LiveLibrary;

.field private e:I

.field private f:Lcom/peel/h/a/dl;

.field private g:Landroid/widget/TextView;

.field private h:Landroid/widget/ListView;

.field private i:Landroid/widget/LinearLayout;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 40
    const-class v0, Lcom/peel/h/a/do;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/peel/h/a/do;->aj:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Lcom/peel/d/u;-><init>()V

    return-void
.end method

.method private S()V
    .locals 3

    .prologue
    .line 167
    iget-object v1, p0, Lcom/peel/h/a/do;->g:Landroid/widget/TextView;

    iget v0, p0, Lcom/peel/h/a/do;->e:I

    if-nez v0, :cond_0

    sget v0, Lcom/peel/ui/ft;->nofavoriteprogramcurrently:I

    invoke-virtual {p0, v0}, Lcom/peel/h/a/do;->a(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 168
    iget-object v0, p0, Lcom/peel/h/a/do;->h:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/peel/h/a/do;->g:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setEmptyView(Landroid/view/View;)V

    .line 169
    sget-object v0, Lcom/peel/h/a/do;->aj:Ljava/lang/String;

    const-string/jumbo v1, "populate adapter"

    new-instance v2, Lcom/peel/h/a/dt;

    invoke-direct {v2, p0}, Lcom/peel/h/a/dt;-><init>(Lcom/peel/h/a/do;)V

    invoke-static {v0, v1, v2}, Lcom/peel/util/i;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/lang/Runnable;

    .line 182
    return-void

    .line 167
    :cond_0
    sget v0, Lcom/peel/ui/ft;->nocutprogramcurrently:I

    invoke-virtual {p0, v0}, Lcom/peel/h/a/do;->a(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method static synthetic a(Lcom/peel/h/a/do;)Lcom/peel/h/a/dl;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/peel/h/a/do;->f:Lcom/peel/h/a/dl;

    return-object v0
.end method

.method static synthetic a(Lcom/peel/h/a/do;Lcom/peel/h/a/dl;)Lcom/peel/h/a/dl;
    .locals 0

    .prologue
    .line 34
    iput-object p1, p0, Lcom/peel/h/a/do;->f:Lcom/peel/h/a/dl;

    return-object p1
.end method

.method static synthetic a(Lcom/peel/h/a/do;I)V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0, p1}, Lcom/peel/h/a/do;->b(I)V

    return-void
.end method

.method private b(I)V
    .locals 7

    .prologue
    const/4 v1, 0x1

    .line 130
    const/4 v5, 0x0

    .line 131
    if-lez p1, :cond_0

    .line 132
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 133
    if-ne p1, v1, :cond_1

    .line 134
    sget v0, Lcom/peel/ui/fp;->menu_edit:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 139
    :cond_0
    :goto_0
    new-instance v0, Lcom/peel/d/a;

    sget-object v1, Lcom/peel/d/d;->b:Lcom/peel/d/d;

    sget-object v2, Lcom/peel/d/b;->a:Lcom/peel/d/b;

    sget-object v3, Lcom/peel/d/c;->b:Lcom/peel/d/c;

    iget-object v4, p0, Lcom/peel/h/a/do;->c:Landroid/os/Bundle;

    const-string/jumbo v6, "category"

    invoke-virtual {v4, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-direct/range {v0 .. v5}, Lcom/peel/d/a;-><init>(Lcom/peel/d/d;Lcom/peel/d/b;Lcom/peel/d/c;Ljava/lang/String;Ljava/util/List;)V

    iput-object v0, p0, Lcom/peel/h/a/do;->d:Lcom/peel/d/a;

    .line 140
    invoke-virtual {p0}, Lcom/peel/h/a/do;->Z()V

    .line 141
    return-void

    .line 135
    :cond_1
    const/4 v0, 0x2

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/peel/h/a/do;->f:Lcom/peel/h/a/dl;

    invoke-virtual {v0}, Lcom/peel/h/a/dl;->a()Ljava/util/List;

    move-result-object v0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 136
    sget v0, Lcom/peel/ui/fp;->menu_delete:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method static synthetic b(Lcom/peel/h/a/do;)V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Lcom/peel/h/a/do;->S()V

    return-void
.end method

.method static synthetic c(Lcom/peel/h/a/do;)I
    .locals 1

    .prologue
    .line 34
    iget v0, p0, Lcom/peel/h/a/do;->e:I

    return v0
.end method

.method static synthetic c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 34
    sget-object v0, Lcom/peel/h/a/do;->aj:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic d(Lcom/peel/h/a/do;)Landroid/widget/ListView;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/peel/h/a/do;->h:Landroid/widget/ListView;

    return-object v0
.end method

.method static synthetic e(Lcom/peel/h/a/do;)Landroid/widget/LinearLayout;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/peel/h/a/do;->i:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic f(Lcom/peel/h/a/do;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/peel/h/a/do;->g:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic g(Lcom/peel/h/a/do;)Lcom/peel/content/library/LiveLibrary;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/peel/h/a/do;->ak:Lcom/peel/content/library/LiveLibrary;

    return-object v0
.end method


# virtual methods
.method public Z()V
    .locals 2

    .prologue
    .line 223
    iget-object v0, p0, Lcom/peel/h/a/do;->d:Lcom/peel/d/a;

    if-nez v0, :cond_0

    .line 228
    :goto_0
    return-void

    .line 226
    :cond_0
    iget-object v0, p0, Lcom/peel/h/a/do;->b:Lcom/peel/d/i;

    iget-object v1, p0, Lcom/peel/h/a/do;->d:Lcom/peel/d/a;

    invoke-virtual {v0, v1}, Lcom/peel/d/i;->a(Lcom/peel/d/a;)V

    goto :goto_0
.end method

.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    .line 59
    sget v0, Lcom/peel/ui/fq;->fav_cut_progam_list:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 61
    sget v0, Lcom/peel/ui/fp;->desc_layout:I

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/peel/h/a/do;->i:Landroid/widget/LinearLayout;

    .line 62
    sget v0, Lcom/peel/ui/fp;->desc:I

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget v1, p0, Lcom/peel/h/a/do;->e:I

    if-nez v1, :cond_0

    sget v1, Lcom/peel/ui/ft;->howtoremoveliked:I

    :goto_0
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 64
    sget v0, Lcom/peel/ui/fp;->node_list:I

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/peel/h/a/do;->h:Landroid/widget/ListView;

    .line 66
    iget-object v0, p0, Lcom/peel/h/a/do;->h:Landroid/widget/ListView;

    new-instance v1, Lcom/peel/h/a/dp;

    invoke-direct {v1, p0}, Lcom/peel/h/a/dp;-><init>(Lcom/peel/h/a/do;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 75
    sget v0, Lcom/peel/ui/fp;->empty_list_msg:I

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/peel/h/a/do;->g:Landroid/widget/TextView;

    .line 77
    sget-object v0, Lcom/peel/content/a;->f:Lcom/peel/content/user/User;

    .line 78
    invoke-virtual {v0}, Lcom/peel/content/user/User;->v()Ljava/util/Map;

    move-result-object v0

    .line 80
    if-eqz v0, :cond_1

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v1

    if-lez v1, :cond_1

    .line 81
    const-class v1, Lcom/peel/h/a/do;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v3, "getNodes"

    new-instance v4, Lcom/peel/h/a/dq;

    invoke-direct {v4, p0, v0}, Lcom/peel/h/a/dq;-><init>(Lcom/peel/h/a/do;Ljava/util/Map;)V

    invoke-static {v1, v3, v4}, Lcom/peel/util/i;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    .line 126
    :goto_1
    return-object v2

    .line 62
    :cond_0
    sget v1, Lcom/peel/ui/ft;->howtoremovedisliked:I

    goto :goto_0

    .line 123
    :cond_1
    invoke-direct {p0}, Lcom/peel/h/a/do;->S()V

    goto :goto_1
.end method

.method public a(Landroid/view/MenuItem;)Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 151
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 153
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    .line 154
    sget v2, Lcom/peel/ui/fp;->menu_edit:I

    if-ne v1, v2, :cond_1

    .line 155
    const-string/jumbo v1, "remove_action"

    invoke-virtual {v0, v1, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 156
    const/4 v1, 0x3

    invoke-direct {p0, v1}, Lcom/peel/h/a/do;->b(I)V

    .line 160
    :cond_0
    :goto_0
    invoke-virtual {p0, v0}, Lcom/peel/h/a/do;->c(Landroid/os/Bundle;)V

    .line 162
    return v3

    .line 157
    :cond_1
    sget v2, Lcom/peel/ui/fp;->menu_delete:I

    if-ne v1, v2, :cond_0

    .line 158
    const-string/jumbo v1, "remove_action"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    goto :goto_0
.end method

.method public a_(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 45
    invoke-super {p0, p1}, Lcom/peel/d/u;->a_(Landroid/os/Bundle;)V

    .line 47
    iget-object v0, p0, Lcom/peel/h/a/do;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "type"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/peel/h/a/do;->e:I

    .line 48
    iget v0, p0, Lcom/peel/h/a/do;->e:I

    if-nez v0, :cond_0

    .line 49
    iget-object v0, p0, Lcom/peel/h/a/do;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "category"

    sget v2, Lcom/peel/ui/ft;->favoriteprograms:I

    invoke-virtual {p0, v2}, Lcom/peel/h/a/do;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 54
    :goto_0
    const-string/jumbo v0, "live"

    invoke-static {v0}, Lcom/peel/content/a;->c(Ljava/lang/String;)Lcom/peel/content/library/Library;

    move-result-object v0

    check-cast v0, Lcom/peel/content/library/LiveLibrary;

    iput-object v0, p0, Lcom/peel/h/a/do;->ak:Lcom/peel/content/library/LiveLibrary;

    .line 55
    return-void

    .line 51
    :cond_0
    iget-object v0, p0, Lcom/peel/h/a/do;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "category"

    sget v2, Lcom/peel/ui/ft;->cutprograms:I

    invoke-virtual {p0, v2}, Lcom/peel/h/a/do;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public c(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 186
    invoke-super {p0, p1}, Lcom/peel/d/u;->c(Landroid/os/Bundle;)V

    .line 187
    if-nez p1, :cond_1

    .line 218
    :cond_0
    :goto_0
    return-void

    .line 189
    :cond_1
    const-string/jumbo v0, "remove_action"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 190
    const-string/jumbo v0, "remove_action"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    .line 191
    if-nez v0, :cond_3

    .line 193
    iget-object v0, p0, Lcom/peel/h/a/do;->b:Lcom/peel/d/i;

    const-string/jumbo v1, "favcut_refresh"

    invoke-virtual {v0, v1, v2}, Lcom/peel/d/i;->b(Ljava/lang/String;I)V

    .line 195
    iget-object v0, p0, Lcom/peel/h/a/do;->f:Lcom/peel/h/a/dl;

    invoke-virtual {v0}, Lcom/peel/h/a/dl;->b()V

    .line 196
    iget-object v0, p0, Lcom/peel/h/a/do;->f:Lcom/peel/h/a/dl;

    invoke-virtual {v0, v3}, Lcom/peel/h/a/dl;->a(Z)V

    .line 198
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 199
    iget-object v1, p0, Lcom/peel/h/a/do;->f:Lcom/peel/h/a/dl;

    invoke-virtual {v1}, Lcom/peel/h/a/dl;->getCount()I

    move-result v1

    if-nez v1, :cond_2

    .line 200
    invoke-direct {p0}, Lcom/peel/h/a/do;->S()V

    .line 201
    const-string/jumbo v1, "has_next"

    invoke-virtual {v0, v1, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    goto :goto_0

    .line 203
    :cond_2
    const-string/jumbo v1, "has_next"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 204
    const-string/jumbo v1, "is_removing"

    invoke-virtual {v0, v1, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 205
    invoke-direct {p0, v2}, Lcom/peel/h/a/do;->b(I)V

    goto :goto_0

    .line 209
    :cond_3
    if-ne v0, v2, :cond_0

    .line 210
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 211
    const-string/jumbo v1, "has_next"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 212
    const-string/jumbo v1, "is_removing"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 215
    iget-object v0, p0, Lcom/peel/h/a/do;->f:Lcom/peel/h/a/dl;

    invoke-virtual {v0, v2}, Lcom/peel/h/a/dl;->a(Z)V

    goto :goto_0
.end method

.method public w()V
    .locals 0

    .prologue
    .line 145
    invoke-super {p0}, Lcom/peel/d/u;->w()V

    .line 146
    invoke-virtual {p0}, Lcom/peel/h/a/do;->Z()V

    .line 147
    return-void
.end method
