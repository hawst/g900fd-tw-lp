.class public Lcom/peel/h/a/bw;
.super Landroid/widget/ArrayAdapter;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lcom/peel/data/Channel;",
        ">;"
    }
.end annotation


# instance fields
.field final a:Landroid/view/LayoutInflater;

.field final b:Lcom/peel/content/library/Library;

.field final c:Lcom/peel/data/ContentRoom;

.field final d:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;I[Lcom/peel/data/Channel;Lcom/peel/content/library/Library;Lcom/peel/data/ContentRoom;)V
    .locals 1

    .prologue
    .line 33
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    .line 34
    iput-object p1, p0, Lcom/peel/h/a/bw;->d:Landroid/content/Context;

    .line 35
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/h/a/bw;->a:Landroid/view/LayoutInflater;

    .line 36
    iput-object p4, p0, Lcom/peel/h/a/bw;->b:Lcom/peel/content/library/Library;

    .line 37
    iput-object p5, p0, Lcom/peel/h/a/bw;->c:Lcom/peel/data/ContentRoom;

    .line 38
    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 41
    new-instance v0, Lcom/peel/h/a/bx;

    invoke-direct {v0, p0}, Lcom/peel/h/a/bx;-><init>(Lcom/peel/h/a/bw;)V

    invoke-super {p0, v0}, Landroid/widget/ArrayAdapter;->sort(Ljava/util/Comparator;)V

    .line 47
    invoke-super {p0}, Landroid/widget/ArrayAdapter;->notifyDataSetChanged()V

    .line 48
    return-void
.end method

.method public a(ILandroid/view/View;)V
    .locals 5

    .prologue
    .line 51
    invoke-virtual {p0, p1}, Lcom/peel/h/a/bw;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/data/Channel;

    .line 52
    invoke-virtual {v0}, Lcom/peel/data/Channel;->l()Z

    move-result v2

    .line 54
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 55
    const-string/jumbo v1, "library"

    iget-object v4, p0, Lcom/peel/h/a/bw;->b:Lcom/peel/content/library/Library;

    invoke-virtual {v4}, Lcom/peel/content/library/Library;->e()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v1, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 56
    const-string/jumbo v1, "channel"

    invoke-virtual {v0}, Lcom/peel/data/Channel;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v1, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 57
    const-string/jumbo v4, "path"

    if-eqz v2, :cond_0

    const-string/jumbo v1, "channel/uncut"

    :goto_0
    invoke-virtual {v3, v4, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 58
    const-string/jumbo v1, "room"

    iget-object v4, p0, Lcom/peel/h/a/bw;->c:Lcom/peel/data/ContentRoom;

    invoke-virtual {v3, v1, v4}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 59
    sget-object v1, Lcom/peel/content/a;->f:Lcom/peel/content/user/User;

    new-instance v4, Lcom/peel/h/a/by;

    invoke-direct {v4, p0, v3}, Lcom/peel/h/a/by;-><init>(Lcom/peel/h/a/bw;Landroid/os/Bundle;)V

    invoke-virtual {v1, v3, v4}, Lcom/peel/content/user/User;->a(Landroid/os/Bundle;Lcom/peel/util/t;)V

    .line 64
    if-nez v2, :cond_1

    const/4 v1, 0x1

    :goto_1
    invoke-virtual {v0, v1}, Lcom/peel/data/Channel;->b(Z)V

    .line 65
    sget v1, Lcom/peel/ui/fp;->icon:I

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    invoke-virtual {v0}, Lcom/peel/data/Channel;->l()Z

    move-result v0

    if-eqz v0, :cond_2

    sget v0, Lcom/peel/ui/fo;->btn_checkbox_off_states:I

    :goto_2
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 67
    invoke-static {}, Lcom/peel/util/a/f;->a()Lcom/peel/util/a/f;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/h/a/bw;->c:Lcom/peel/data/ContentRoom;

    invoke-virtual {v1}, Lcom/peel/data/ContentRoom;->b()I

    move-result v1

    const/16 v2, 0xbc9

    const/16 v3, 0x7d8

    iget-object v4, p0, Lcom/peel/h/a/bw;->b:Lcom/peel/content/library/Library;

    invoke-virtual {v4}, Lcom/peel/content/library/Library;->e()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/peel/util/a/f;->a(IIILjava/lang/String;)V

    .line 69
    return-void

    .line 57
    :cond_0
    const-string/jumbo v1, "channel/cut"

    goto :goto_0

    .line 64
    :cond_1
    const/4 v1, 0x0

    goto :goto_1

    .line 65
    :cond_2
    sget v0, Lcom/peel/ui/fo;->btn_checkbox_on_states:I

    goto :goto_2
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 73
    if-nez p2, :cond_0

    iget-object v0, p0, Lcom/peel/h/a/bw;->a:Landroid/view/LayoutInflater;

    sget v1, Lcom/peel/ui/fq;->channel_setting_row:I

    invoke-virtual {v0, v1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 75
    :cond_0
    invoke-virtual {p0, p1}, Lcom/peel/h/a/bw;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/data/Channel;

    .line 77
    invoke-virtual {v0}, Lcom/peel/data/Channel;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 79
    sget v1, Lcom/peel/ui/fp;->name:I

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-virtual {v0}, Lcom/peel/data/Channel;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 80
    sget v1, Lcom/peel/ui/fp;->type:I

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    const/4 v2, 0x1

    invoke-virtual {v0}, Lcom/peel/data/Channel;->i()I

    move-result v4

    if-ne v2, v4, :cond_3

    const-string/jumbo v2, "HD"

    :goto_0
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 81
    sget v1, Lcom/peel/ui/fp;->channel_name:I

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-virtual {v0}, Lcom/peel/data/Channel;->m()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 82
    sget v1, Lcom/peel/ui/fp;->icon:I

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 83
    sget v1, Lcom/peel/ui/fp;->icon:I

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    invoke-virtual {v0}, Lcom/peel/data/Channel;->l()Z

    move-result v2

    if-eqz v2, :cond_4

    sget v2, Lcom/peel/ui/fo;->btn_checkbox_off_states:I

    :goto_1
    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 85
    invoke-virtual {v0}, Lcom/peel/data/Channel;->f()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Lcom/peel/data/Channel;->f()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_5

    :cond_1
    move-object v1, v3

    .line 87
    :goto_2
    iget-object v0, p0, Lcom/peel/h/a/bw;->d:Landroid/content/Context;

    invoke-static {v0}, Lcom/peel/util/b/e;->a(Landroid/content/Context;)Lcom/squareup/picasso/Picasso;

    move-result-object v0

    .line 88
    invoke-virtual {v0, v1}, Lcom/squareup/picasso/Picasso;->load(Ljava/lang/String;)Lcom/squareup/picasso/RequestCreator;

    move-result-object v0

    sget v2, Lcom/peel/ui/fo;->myroom_channel_empty_set:I

    .line 89
    invoke-virtual {v0, v2}, Lcom/squareup/picasso/RequestCreator;->placeholder(I)Lcom/squareup/picasso/RequestCreator;

    move-result-object v2

    sget v0, Lcom/peel/ui/fp;->image:I

    .line 90
    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-virtual {v2, v0}, Lcom/squareup/picasso/RequestCreator;->into(Landroid/widget/ImageView;)V

    .line 92
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 93
    sget v0, Lcom/peel/ui/fp;->image:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 96
    :cond_2
    return-object p2

    .line 80
    :cond_3
    const-string/jumbo v2, "SD"

    goto :goto_0

    .line 83
    :cond_4
    sget v2, Lcom/peel/ui/fo;->btn_checkbox_on_states:I

    goto :goto_1

    .line 85
    :cond_5
    invoke-virtual {v0}, Lcom/peel/data/Channel;->f()Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    goto :goto_2
.end method
