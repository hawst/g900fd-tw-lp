.class Lcom/peel/h/a/k;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/peel/control/RoomControl;

.field final synthetic b:Lcom/peel/h/a/a;


# direct methods
.method constructor <init>(Lcom/peel/h/a/a;Lcom/peel/control/RoomControl;)V
    .locals 0

    .prologue
    .line 893
    iput-object p1, p0, Lcom/peel/h/a/k;->b:Lcom/peel/h/a/a;

    iput-object p2, p0, Lcom/peel/h/a/k;->a:Lcom/peel/control/RoomControl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 11

    .prologue
    const/4 v10, 0x0

    const/16 v9, 0x8

    const/4 v8, 0x0

    .line 896
    invoke-static {}, Lcom/peel/util/a/f;->a()Lcom/peel/util/a/f;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/h/a/k;->a:Lcom/peel/control/RoomControl;

    if-nez v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    const/16 v2, 0x432

    const/16 v3, 0x7d8

    iget-object v4, p0, Lcom/peel/h/a/k;->b:Lcom/peel/h/a/a;

    invoke-static {v4}, Lcom/peel/h/a/a;->n(Lcom/peel/h/a/a;)Lcom/peel/f/a;

    move-result-object v4

    invoke-virtual {v4}, Lcom/peel/f/a;->b()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/peel/h/a/k;->b:Lcom/peel/h/a/a;

    invoke-static {v5}, Lcom/peel/h/a/a;->d(Lcom/peel/h/a/a;)I

    move-result v5

    const-string/jumbo v6, ""

    iget-object v7, p0, Lcom/peel/h/a/k;->b:Lcom/peel/h/a/a;

    invoke-static {v7}, Lcom/peel/h/a/a;->w(Lcom/peel/h/a/a;)I

    move-result v7

    invoke-virtual/range {v0 .. v7}, Lcom/peel/util/a/f;->a(IIILjava/lang/String;ILjava/lang/String;I)V

    .line 898
    iget-object v0, p0, Lcom/peel/h/a/k;->b:Lcom/peel/h/a/a;

    invoke-static {v0}, Lcom/peel/h/a/a;->u(Lcom/peel/h/a/a;)Lcom/peel/widget/TestBtnViewPager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/widget/TestBtnViewPager;->getCurrentItem()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    iget-object v1, p0, Lcom/peel/h/a/k;->b:Lcom/peel/h/a/a;

    invoke-static {v1}, Lcom/peel/h/a/a;->t(Lcom/peel/h/a/a;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 899
    iget-object v0, p0, Lcom/peel/h/a/k;->b:Lcom/peel/h/a/a;

    invoke-static {v0}, Lcom/peel/h/a/a;->u(Lcom/peel/h/a/a;)Lcom/peel/widget/TestBtnViewPager;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/h/a/k;->b:Lcom/peel/h/a/a;

    invoke-static {v1}, Lcom/peel/h/a/a;->u(Lcom/peel/h/a/a;)Lcom/peel/widget/TestBtnViewPager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/widget/TestBtnViewPager;->getCurrentItem()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v1}, Lcom/peel/widget/TestBtnViewPager;->setCurrentItem(I)V

    .line 921
    :goto_1
    return-void

    .line 896
    :cond_0
    iget-object v1, p0, Lcom/peel/h/a/k;->a:Lcom/peel/control/RoomControl;

    invoke-virtual {v1}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/data/at;->f()I

    move-result v1

    goto :goto_0

    .line 901
    :cond_1
    invoke-static {}, Lcom/peel/h/a/a;->c()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "show missing IR code screen"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 904
    iget-object v0, p0, Lcom/peel/h/a/k;->b:Lcom/peel/h/a/a;

    invoke-static {v0}, Lcom/peel/h/a/a;->A(Lcom/peel/h/a/a;)Landroid/widget/RelativeLayout;

    move-result-object v0

    invoke-virtual {v0, v9}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 905
    iget-object v0, p0, Lcom/peel/h/a/k;->b:Lcom/peel/h/a/a;

    invoke-static {v0}, Lcom/peel/h/a/a;->B(Lcom/peel/h/a/a;)Landroid/widget/RelativeLayout;

    move-result-object v0

    invoke-virtual {v0, v8}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 906
    iget-object v0, p0, Lcom/peel/h/a/k;->b:Lcom/peel/h/a/a;

    invoke-static {v0}, Lcom/peel/h/a/a;->C(Lcom/peel/h/a/a;)Landroid/widget/TextView;

    move-result-object v0

    new-instance v1, Lcom/peel/h/a/ag;

    iget-object v2, p0, Lcom/peel/h/a/k;->b:Lcom/peel/h/a/a;

    invoke-direct {v1, v2, v10}, Lcom/peel/h/a/ag;-><init>(Lcom/peel/h/a/a;Lcom/peel/h/a/b;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 908
    iget-object v0, p0, Lcom/peel/h/a/k;->b:Lcom/peel/h/a/a;

    sget v1, Lcom/peel/ui/ft;->ir_report_missing_code:I

    invoke-virtual {v0, v1}, Lcom/peel/h/a/a;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    .line 909
    new-instance v2, Landroid/text/SpannableStringBuilder;

    invoke-direct {v2, v0}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 911
    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    const-class v1, Landroid/text/style/URLSpan;

    invoke-virtual {v2, v8, v0, v1}, Landroid/text/SpannableStringBuilder;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/text/style/URLSpan;

    .line 912
    array-length v3, v0

    move v1, v8

    :goto_2
    if-ge v1, v3, :cond_2

    aget-object v4, v0, v1

    .line 913
    new-instance v5, Lcom/peel/h/a/af;

    iget-object v6, p0, Lcom/peel/h/a/k;->b:Lcom/peel/h/a/a;

    invoke-direct {v5, v6, v10}, Lcom/peel/h/a/af;-><init>(Lcom/peel/h/a/a;Lcom/peel/h/a/b;)V

    .line 914
    invoke-virtual {v2, v4}, Landroid/text/SpannableStringBuilder;->getSpanStart(Ljava/lang/Object;)I

    move-result v6

    .line 915
    invoke-virtual {v2, v4}, Landroid/text/SpannableStringBuilder;->getSpanEnd(Ljava/lang/Object;)I

    move-result v4

    const/16 v7, 0x22

    .line 914
    invoke-virtual {v2, v5, v6, v4, v7}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 912
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 917
    :cond_2
    iget-object v0, p0, Lcom/peel/h/a/k;->b:Lcom/peel/h/a/a;

    invoke-static {v0}, Lcom/peel/h/a/a;->C(Lcom/peel/h/a/a;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 918
    iget-object v0, p0, Lcom/peel/h/a/k;->b:Lcom/peel/h/a/a;

    invoke-static {v0}, Lcom/peel/h/a/a;->v(Lcom/peel/h/a/a;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v9}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_1
.end method
