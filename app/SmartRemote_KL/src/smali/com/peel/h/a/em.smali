.class public Lcom/peel/h/a/em;
.super Lcom/peel/d/u;


# instance fields
.field private aj:Ljava/lang/String;

.field private ak:Lcom/peel/widget/ag;

.field private al:Lcom/peel/widget/ag;

.field private am:Landroid/app/ProgressDialog;

.field private an:Landroid/widget/Button;

.field private final ao:Lcom/peel/util/s;

.field e:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private final f:I

.field private g:Lcom/peel/control/h;

.field private h:Ljava/lang/String;

.field private i:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/peel/d/u;-><init>()V

    .line 36
    const/16 v0, 0x3c

    iput v0, p0, Lcom/peel/h/a/em;->f:I

    .line 45
    new-instance v0, Lcom/peel/h/a/en;

    invoke-direct {v0, p0}, Lcom/peel/h/a/en;-><init>(Lcom/peel/h/a/em;)V

    iput-object v0, p0, Lcom/peel/h/a/em;->ao:Lcom/peel/util/s;

    return-void
.end method

.method private S()V
    .locals 3

    .prologue
    .line 204
    iget-object v0, p0, Lcom/peel/h/a/em;->ak:Lcom/peel/widget/ag;

    if-nez v0, :cond_0

    .line 205
    new-instance v0, Lcom/peel/widget/ag;

    invoke-virtual {p0}, Lcom/peel/h/a/em;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/peel/widget/ag;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/peel/h/a/em;->ak:Lcom/peel/widget/ag;

    .line 206
    iget-object v0, p0, Lcom/peel/h/a/em;->ak:Lcom/peel/widget/ag;

    sget v1, Lcom/peel/ui/ft;->failed_to_learn:I

    invoke-virtual {v0, v1}, Lcom/peel/widget/ag;->b(I)Lcom/peel/widget/ag;

    .line 207
    iget-object v0, p0, Lcom/peel/h/a/em;->ak:Lcom/peel/widget/ag;

    sget v1, Lcom/peel/ui/ft;->learn:I

    new-instance v2, Lcom/peel/h/a/ev;

    invoke-direct {v2, p0}, Lcom/peel/h/a/ev;-><init>(Lcom/peel/h/a/em;)V

    invoke-virtual {v0, v1, v2}, Lcom/peel/widget/ag;->a(ILandroid/content/DialogInterface$OnClickListener;)Lcom/peel/widget/ag;

    .line 213
    iget-object v0, p0, Lcom/peel/h/a/em;->ak:Lcom/peel/widget/ag;

    sget v1, Lcom/peel/ui/ft;->tv_remotecontrol_caption_back:I

    new-instance v2, Lcom/peel/h/a/ew;

    invoke-direct {v2, p0}, Lcom/peel/h/a/ew;-><init>(Lcom/peel/h/a/em;)V

    invoke-virtual {v0, v1, v2}, Lcom/peel/widget/ag;->c(ILandroid/content/DialogInterface$OnClickListener;)Lcom/peel/widget/ag;

    .line 220
    :cond_0
    iget-object v0, p0, Lcom/peel/h/a/em;->ak:Lcom/peel/widget/ag;

    invoke-virtual {v0}, Lcom/peel/widget/ag;->show()V

    .line 221
    iget-object v0, p0, Lcom/peel/h/a/em;->ak:Lcom/peel/widget/ag;

    iget-object v1, p0, Lcom/peel/h/a/em;->ak:Lcom/peel/widget/ag;

    invoke-virtual {v0, v1}, Lcom/peel/widget/ag;->a(Lcom/peel/widget/ag;)Lcom/peel/widget/ag;

    .line 222
    return-void
.end method

.method private T()V
    .locals 3

    .prologue
    .line 225
    iget-object v0, p0, Lcom/peel/h/a/em;->al:Lcom/peel/widget/ag;

    if-nez v0, :cond_0

    .line 226
    new-instance v0, Lcom/peel/widget/ag;

    invoke-virtual {p0}, Lcom/peel/h/a/em;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/peel/widget/ag;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/peel/h/a/em;->al:Lcom/peel/widget/ag;

    .line 227
    iget-object v0, p0, Lcom/peel/h/a/em;->al:Lcom/peel/widget/ag;

    sget v1, Lcom/peel/ui/ft;->did_it_work:I

    invoke-virtual {v0, v1}, Lcom/peel/widget/ag;->b(I)Lcom/peel/widget/ag;

    .line 228
    iget-object v0, p0, Lcom/peel/h/a/em;->al:Lcom/peel/widget/ag;

    sget v1, Lcom/peel/ui/ft;->yes:I

    new-instance v2, Lcom/peel/h/a/ex;

    invoke-direct {v2, p0}, Lcom/peel/h/a/ex;-><init>(Lcom/peel/h/a/em;)V

    invoke-virtual {v0, v1, v2}, Lcom/peel/widget/ag;->a(ILandroid/content/DialogInterface$OnClickListener;)Lcom/peel/widget/ag;

    .line 252
    iget-object v0, p0, Lcom/peel/h/a/em;->al:Lcom/peel/widget/ag;

    sget v1, Lcom/peel/ui/ft;->no:I

    new-instance v2, Lcom/peel/h/a/ey;

    invoke-direct {v2, p0}, Lcom/peel/h/a/ey;-><init>(Lcom/peel/h/a/em;)V

    invoke-virtual {v0, v1, v2}, Lcom/peel/widget/ag;->c(ILandroid/content/DialogInterface$OnClickListener;)Lcom/peel/widget/ag;

    .line 262
    iget-object v0, p0, Lcom/peel/h/a/em;->al:Lcom/peel/widget/ag;

    sget v1, Lcom/peel/ui/ft;->retry:I

    new-instance v2, Lcom/peel/h/a/ez;

    invoke-direct {v2, p0}, Lcom/peel/h/a/ez;-><init>(Lcom/peel/h/a/em;)V

    invoke-virtual {v0, v1, v2}, Lcom/peel/widget/ag;->b(ILandroid/content/DialogInterface$OnClickListener;)Lcom/peel/widget/ag;

    .line 269
    :cond_0
    iget-object v0, p0, Lcom/peel/h/a/em;->al:Lcom/peel/widget/ag;

    invoke-virtual {v0}, Lcom/peel/widget/ag;->show()V

    .line 270
    iget-object v0, p0, Lcom/peel/h/a/em;->al:Lcom/peel/widget/ag;

    iget-object v1, p0, Lcom/peel/h/a/em;->al:Lcom/peel/widget/ag;

    invoke-virtual {v0, v1}, Lcom/peel/widget/ag;->a(Lcom/peel/widget/ag;)Lcom/peel/widget/ag;

    .line 271
    return-void
.end method

.method static synthetic a(Lcom/peel/h/a/em;)Lcom/peel/control/h;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/peel/h/a/em;->g:Lcom/peel/control/h;

    return-object v0
.end method

.method static synthetic a(Lcom/peel/h/a/em;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 35
    iput-object p1, p0, Lcom/peel/h/a/em;->i:Ljava/lang/String;

    return-object p1
.end method

.method private a(J)V
    .locals 5

    .prologue
    const/4 v3, 0x1

    .line 186
    iget-object v0, p0, Lcom/peel/h/a/em;->am:Landroid/app/ProgressDialog;

    if-nez v0, :cond_0

    .line 187
    new-instance v0, Landroid/app/ProgressDialog;

    invoke-virtual {p0}, Lcom/peel/h/a/em;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/peel/h/a/em;->am:Landroid/app/ProgressDialog;

    .line 188
    iget-object v0, p0, Lcom/peel/h/a/em;->am:Landroid/app/ProgressDialog;

    invoke-virtual {p0}, Lcom/peel/h/a/em;->n()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/peel/ui/ft;->sending_ir:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 189
    iget-object v0, p0, Lcom/peel/h/a/em;->am:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v3}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    .line 190
    iget-object v0, p0, Lcom/peel/h/a/em;->am:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v3}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 192
    :cond_0
    iget-object v0, p0, Lcom/peel/h/a/em;->am:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    .line 194
    const-class v0, Lcom/peel/h/a/em;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "did it work dialog"

    new-instance v2, Lcom/peel/h/a/eu;

    invoke-direct {v2, p0}, Lcom/peel/h/a/eu;-><init>(Lcom/peel/h/a/em;)V

    invoke-static {v0, v1, v2, p1, p2}, Lcom/peel/util/i;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;J)Ljava/lang/Runnable;

    .line 201
    return-void
.end method

.method static synthetic a(Lcom/peel/h/a/em;J)V
    .locals 1

    .prologue
    .line 35
    invoke-direct {p0, p1, p2}, Lcom/peel/h/a/em;->a(J)V

    return-void
.end method

.method static synthetic b(Lcom/peel/h/a/em;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/peel/h/a/em;->h:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic b(Lcom/peel/h/a/em;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 35
    iput-object p1, p0, Lcom/peel/h/a/em;->aj:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic c(Lcom/peel/h/a/em;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/peel/h/a/em;->i:Ljava/lang/String;

    return-object v0
.end method

.method private c()V
    .locals 5

    .prologue
    .line 163
    iget-object v0, p0, Lcom/peel/h/a/em;->aj:Ljava/lang/String;

    iget-object v1, p0, Lcom/peel/h/a/em;->i:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/peel/util/ay;->a(Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v0

    const-wide/16 v2, 0x64

    add-long/2addr v0, v2

    .line 165
    invoke-static {}, Lcom/peel/util/i;->c()Z

    move-result v2

    if-nez v2, :cond_0

    .line 166
    iget-object v2, p0, Lcom/peel/h/a/em;->g:Lcom/peel/control/h;

    iget-object v3, p0, Lcom/peel/h/a/em;->h:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/peel/control/h;->c(Ljava/lang/String;)Z

    .line 167
    invoke-direct {p0, v0, v1}, Lcom/peel/h/a/em;->a(J)V

    .line 183
    :goto_0
    return-void

    .line 169
    :cond_0
    const-class v2, Lcom/peel/h/a/em;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "sendCommand"

    new-instance v4, Lcom/peel/h/a/es;

    invoke-direct {v4, p0, v0, v1}, Lcom/peel/h/a/es;-><init>(Lcom/peel/h/a/em;J)V

    invoke-static {v2, v3, v4}, Lcom/peel/util/i;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/lang/Runnable;

    goto :goto_0
.end method

.method static synthetic d(Lcom/peel/h/a/em;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/peel/h/a/em;->aj:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic e(Lcom/peel/h/a/em;)Landroid/widget/Button;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/peel/h/a/em;->an:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic f(Lcom/peel/h/a/em;)V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/peel/h/a/em;->S()V

    return-void
.end method

.method static synthetic g(Lcom/peel/h/a/em;)V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/peel/h/a/em;->c()V

    return-void
.end method

.method static synthetic h(Lcom/peel/h/a/em;)Landroid/app/ProgressDialog;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/peel/h/a/em;->am:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method static synthetic i(Lcom/peel/h/a/em;)V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/peel/h/a/em;->T()V

    return-void
.end method

.method static synthetic j(Lcom/peel/h/a/em;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/peel/h/a/em;->a:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 141
    sget v0, Lcom/peel/ui/fq;->ir_learning:I

    invoke-virtual {p1, v0, p2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 143
    sget v0, Lcom/peel/ui/fp;->test_btn:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/peel/h/a/em;->an:Landroid/widget/Button;

    .line 144
    iget-object v0, p0, Lcom/peel/h/a/em;->an:Landroid/widget/Button;

    new-instance v2, Lcom/peel/h/a/er;

    invoke-direct {v2, p0}, Lcom/peel/h/a/er;-><init>(Lcom/peel/h/a/em;)V

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 150
    iget-object v0, p0, Lcom/peel/h/a/em;->an:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setEnabled(Z)V

    .line 152
    return-object v1
.end method

.method public a_(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 103
    invoke-super {p0, p1}, Lcom/peel/d/u;->a_(Landroid/os/Bundle;)V

    .line 104
    iget-object v0, p0, Lcom/peel/h/a/em;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 105
    sget-object v1, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v1, v0}, Lcom/peel/control/am;->c(Ljava/lang/String;)Lcom/peel/control/h;

    move-result-object v1

    iput-object v1, p0, Lcom/peel/h/a/em;->g:Lcom/peel/control/h;

    .line 106
    iget-object v1, p0, Lcom/peel/h/a/em;->c:Landroid/os/Bundle;

    const-string/jumbo v2, "cmd"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/peel/h/a/em;->h:Ljava/lang/String;

    .line 108
    iget-object v1, p0, Lcom/peel/h/a/em;->g:Lcom/peel/control/h;

    if-nez v1, :cond_0

    .line 109
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "device not found for id: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 110
    iget-object v1, p0, Lcom/peel/h/a/em;->a:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/peel/h/a/em;->m()Landroid/support/v4/app/ae;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/peel/d/e;->a(Ljava/lang/String;Landroid/support/v4/app/ae;)V

    .line 113
    :cond_0
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "device id: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 115
    return-void
.end method

.method public b()Z
    .locals 2

    .prologue
    .line 275
    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v0}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/control/RoomControl;->b()Lcom/peel/control/o;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/h/a/em;->ao:Lcom/peel/util/s;

    invoke-virtual {v0, v1}, Lcom/peel/control/o;->b(Lcom/peel/util/s;)V

    .line 276
    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v0}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/control/RoomControl;->b()Lcom/peel/control/o;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/control/o;->f()Z

    .line 277
    invoke-super {p0}, Lcom/peel/d/u;->b()Z

    move-result v0

    return v0
.end method

.method public c(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 157
    invoke-super {p0, p1}, Lcom/peel/d/u;->c(Landroid/os/Bundle;)V

    .line 159
    sget-object v0, Lcom/peel/content/a;->h:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/peel/h/a/em;->b:Lcom/peel/d/i;

    if-nez v0, :cond_0

    .line 160
    :cond_0
    return-void
.end method

.method public d(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 136
    invoke-super {p0, p1}, Lcom/peel/d/u;->d(Landroid/os/Bundle;)V

    .line 137
    return-void
.end method

.method public h(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 119
    invoke-super {p0, p1}, Lcom/peel/d/u;->h(Landroid/os/Bundle;)V

    .line 121
    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v0}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/control/RoomControl;->b()Lcom/peel/control/o;

    move-result-object v0

    .line 122
    iget-object v1, p0, Lcom/peel/h/a/em;->ao:Lcom/peel/util/s;

    invoke-virtual {v0, v1}, Lcom/peel/control/o;->a(Lcom/peel/util/s;)V

    .line 123
    const/16 v1, 0x3c

    invoke-virtual {v0, v1}, Lcom/peel/control/o;->a(I)Z

    .line 126
    iget-object v0, p0, Lcom/peel/h/a/em;->g:Lcom/peel/control/h;

    invoke-virtual {v0}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/data/g;->a()Ljava/util/Map;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/h/a/em;->h:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    iput-object v0, p0, Lcom/peel/h/a/em;->e:Ljava/util/Map;

    .line 129
    sget-object v0, Lcom/peel/content/a;->h:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 130
    iget-object v0, p0, Lcom/peel/h/a/em;->c:Landroid/os/Bundle;

    invoke-virtual {p0, v0}, Lcom/peel/h/a/em;->c(Landroid/os/Bundle;)V

    .line 132
    :cond_0
    return-void
.end method
