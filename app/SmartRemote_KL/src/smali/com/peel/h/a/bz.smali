.class public Lcom/peel/h/a/bz;
.super Lcom/peel/d/u;


# static fields
.field private static final g:Ljava/lang/String;


# instance fields
.field private aA:Landroid/widget/ListView;

.field private aB:Lcom/peel/h/a/fa;

.field private aC:Landroid/widget/TextView;

.field private aD:Landroid/widget/TextView;

.field private aE:Landroid/widget/TextView;

.field private aF:Lcom/peel/widget/ag;

.field private aG:Lcom/peel/widget/ag;

.field private aH:Lcom/peel/widget/ag;

.field private aI:Lcom/peel/widget/ag;

.field private aj:[Lcom/peel/f/d;

.field private ak:[Lcom/peel/f/d;

.field private al:I

.field private am:I

.field private an:Lcom/peel/control/h;

.field private ao:Lcom/peel/control/h;

.field private ap:Lcom/peel/control/h;

.field private aq:Lcom/peel/control/h;

.field private ar:Ljava/lang/String;

.field private as:Ljava/lang/String;

.field private at:Ljava/lang/String;

.field private au:Ljava/lang/String;

.field private av:Lcom/peel/control/a;

.field private aw:Lcom/peel/control/RoomControl;

.field private ax:[Ljava/lang/String;

.field private ay:[Ljava/lang/String;

.field private az:[Ljava/lang/String;

.field e:Z

.field f:Z

.field private h:Landroid/view/View;

.field private i:Landroid/view/View;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 42
    const-class v0, Lcom/peel/h/a/bz;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/peel/h/a/bz;->g:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 41
    invoke-direct {p0}, Lcom/peel/d/u;-><init>()V

    .line 47
    iput v1, p0, Lcom/peel/h/a/bz;->al:I

    iput v1, p0, Lcom/peel/h/a/bz;->am:I

    .line 49
    iput-object v0, p0, Lcom/peel/h/a/bz;->ar:Ljava/lang/String;

    iput-object v0, p0, Lcom/peel/h/a/bz;->as:Ljava/lang/String;

    .line 51
    iput-object v0, p0, Lcom/peel/h/a/bz;->aw:Lcom/peel/control/RoomControl;

    .line 57
    iput-boolean v2, p0, Lcom/peel/h/a/bz;->e:Z

    iput-boolean v2, p0, Lcom/peel/h/a/bz;->f:Z

    return-void
.end method

.method static synthetic a(Lcom/peel/h/a/bz;I)I
    .locals 0

    .prologue
    .line 41
    iput p1, p0, Lcom/peel/h/a/bz;->al:I

    return p1
.end method

.method static synthetic a(Lcom/peel/h/a/bz;Lcom/peel/control/h;)Lcom/peel/control/h;
    .locals 0

    .prologue
    .line 41
    iput-object p1, p0, Lcom/peel/h/a/bz;->ap:Lcom/peel/control/h;

    return-object p1
.end method

.method static synthetic a(Lcom/peel/h/a/bz;)Lcom/peel/widget/ag;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/peel/h/a/bz;->aH:Lcom/peel/widget/ag;

    return-object v0
.end method

.method static synthetic a(Lcom/peel/h/a/bz;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 41
    iput-object p1, p0, Lcom/peel/h/a/bz;->ar:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic a(Lcom/peel/h/a/bz;[Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 41
    invoke-direct {p0, p1}, Lcom/peel/h/a/bz;->a([Ljava/lang/String;)V

    return-void
.end method

.method private a([Ljava/lang/String;)V
    .locals 7

    .prologue
    const/4 v0, 0x2

    const/4 v5, 0x1

    .line 704
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 705
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 706
    aget-object v3, p1, v5

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 707
    array-length v3, p1

    if-le v3, v0, :cond_0

    .line 708
    :goto_0
    array-length v3, p1

    if-ge v0, v3, :cond_0

    const-string/jumbo v3, ","

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    aget-object v4, p1, v0

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 710
    :cond_0
    sget-object v0, Lcom/peel/h/a/bz;->g:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "tokens..."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 712
    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    aget-object v3, p1, v5

    invoke-virtual {v0, v3}, Lcom/peel/control/am;->e(Ljava/lang/String;)Lcom/peel/control/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/control/a;->a()Ljava/lang/String;

    move-result-object v0

    .line 713
    new-instance v3, Lcom/peel/widget/ag;

    invoke-virtual {p0}, Lcom/peel/h/a/bz;->m()Landroid/support/v4/app/ae;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/peel/widget/ag;-><init>(Landroid/content/Context;)V

    sget v4, Lcom/peel/ui/ft;->set_up:I

    invoke-virtual {v3, v4}, Lcom/peel/widget/ag;->a(I)Lcom/peel/widget/ag;

    move-result-object v3

    sget v4, Lcom/peel/ui/ft;->input_setup_for_activity:I

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v0, v5, v6

    .line 714
    invoke-virtual {p0, v4, v5}, Lcom/peel/h/a/bz;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/peel/widget/ag;->b(Ljava/lang/CharSequence;)Lcom/peel/widget/ag;

    move-result-object v0

    sget v3, Lcom/peel/ui/ft;->label_skip:I

    new-instance v4, Lcom/peel/h/a/cg;

    invoke-direct {v4, p0, p1}, Lcom/peel/h/a/cg;-><init>(Lcom/peel/h/a/bz;[Ljava/lang/String;)V

    .line 715
    invoke-virtual {v0, v3, v4}, Lcom/peel/widget/ag;->c(ILandroid/content/DialogInterface$OnClickListener;)Lcom/peel/widget/ag;

    move-result-object v0

    sget v3, Lcom/peel/ui/ft;->yes:I

    new-instance v4, Lcom/peel/h/a/cf;

    invoke-direct {v4, p0, v1, v2}, Lcom/peel/h/a/cf;-><init>(Lcom/peel/h/a/bz;Landroid/os/Bundle;Ljava/lang/StringBuilder;)V

    .line 728
    invoke-virtual {v0, v3, v4}, Lcom/peel/widget/ag;->a(ILandroid/content/DialogInterface$OnClickListener;)Lcom/peel/widget/ag;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/h/a/bz;->aI:Lcom/peel/widget/ag;

    .line 737
    iget-object v0, p0, Lcom/peel/h/a/bz;->aI:Lcom/peel/widget/ag;

    iget-object v1, p0, Lcom/peel/h/a/bz;->aI:Lcom/peel/widget/ag;

    invoke-virtual {v0, v1}, Lcom/peel/widget/ag;->a(Lcom/peel/widget/ag;)Lcom/peel/widget/ag;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/widget/ag;->d()Lcom/peel/widget/ag;

    .line 738
    return-void
.end method

.method static synthetic b(Lcom/peel/h/a/bz;)Lcom/peel/widget/ag;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/peel/h/a/bz;->aF:Lcom/peel/widget/ag;

    return-object v0
.end method

.method static synthetic b(Lcom/peel/h/a/bz;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 41
    iput-object p1, p0, Lcom/peel/h/a/bz;->as:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic c(Lcom/peel/h/a/bz;)Lcom/peel/widget/ag;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/peel/h/a/bz;->aG:Lcom/peel/widget/ag;

    return-object v0
.end method

.method static synthetic c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 41
    sget-object v0, Lcom/peel/h/a/bz;->g:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic d(Lcom/peel/h/a/bz;)I
    .locals 1

    .prologue
    .line 41
    iget v0, p0, Lcom/peel/h/a/bz;->al:I

    return v0
.end method

.method static synthetic e(Lcom/peel/h/a/bz;)Lcom/peel/control/h;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/peel/h/a/bz;->ao:Lcom/peel/control/h;

    return-object v0
.end method

.method static synthetic f(Lcom/peel/h/a/bz;)Lcom/peel/control/RoomControl;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/peel/h/a/bz;->aw:Lcom/peel/control/RoomControl;

    return-object v0
.end method

.method static synthetic g(Lcom/peel/h/a/bz;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/peel/h/a/bz;->at:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic h(Lcom/peel/h/a/bz;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/peel/h/a/bz;->aC:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic i(Lcom/peel/h/a/bz;)[Lcom/peel/f/d;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/peel/h/a/bz;->aj:[Lcom/peel/f/d;

    return-object v0
.end method

.method static synthetic j(Lcom/peel/h/a/bz;)Lcom/peel/control/h;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/peel/h/a/bz;->an:Lcom/peel/control/h;

    return-object v0
.end method

.method static synthetic k(Lcom/peel/h/a/bz;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/peel/h/a/bz;->aD:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic l(Lcom/peel/h/a/bz;)[Lcom/peel/f/d;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/peel/h/a/bz;->ak:[Lcom/peel/f/d;

    return-object v0
.end method

.method static synthetic m(Lcom/peel/h/a/bz;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/peel/h/a/bz;->aE:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic n(Lcom/peel/h/a/bz;)Lcom/peel/control/h;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/peel/h/a/bz;->aq:Lcom/peel/control/h;

    return-object v0
.end method

.method static synthetic o(Lcom/peel/h/a/bz;)Lcom/peel/control/h;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/peel/h/a/bz;->ap:Lcom/peel/control/h;

    return-object v0
.end method

.method static synthetic p(Lcom/peel/h/a/bz;)Lcom/peel/control/a;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/peel/h/a/bz;->av:Lcom/peel/control/a;

    return-object v0
.end method

.method static synthetic q(Lcom/peel/h/a/bz;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/peel/h/a/bz;->ar:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic r(Lcom/peel/h/a/bz;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/peel/h/a/bz;->as:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic s(Lcom/peel/h/a/bz;)[Ljava/lang/String;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/peel/h/a/bz;->ax:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic t(Lcom/peel/h/a/bz;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/peel/h/a/bz;->au:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public Z()V
    .locals 6

    .prologue
    .line 757
    iget-object v0, p0, Lcom/peel/h/a/bz;->d:Lcom/peel/d/a;

    if-nez v0, :cond_0

    .line 758
    invoke-virtual {p0}, Lcom/peel/h/a/bz;->n()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/peel/ui/ft;->edit_something:I

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/peel/h/a/bz;->av:Lcom/peel/control/a;

    invoke-virtual {v4}, Lcom/peel/control/a;->a()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 759
    new-instance v0, Lcom/peel/d/a;

    sget-object v1, Lcom/peel/d/d;->b:Lcom/peel/d/d;

    sget-object v2, Lcom/peel/d/b;->a:Lcom/peel/d/b;

    sget-object v3, Lcom/peel/d/c;->b:Lcom/peel/d/c;

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, Lcom/peel/d/a;-><init>(Lcom/peel/d/d;Lcom/peel/d/b;Lcom/peel/d/c;Ljava/lang/String;Ljava/util/List;)V

    iput-object v0, p0, Lcom/peel/h/a/bz;->d:Lcom/peel/d/a;

    .line 763
    :cond_0
    iget-object v0, p0, Lcom/peel/h/a/bz;->b:Lcom/peel/d/i;

    iget-object v1, p0, Lcom/peel/h/a/bz;->d:Lcom/peel/d/a;

    invoke-virtual {v0, v1}, Lcom/peel/d/i;->a(Lcom/peel/d/a;)V

    .line 764
    return-void
.end method

.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 10

    .prologue
    .line 67
    iget-object v0, p0, Lcom/peel/h/a/bz;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "room"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    iget-object v1, p0, Lcom/peel/h/a/bz;->c:Landroid/os/Bundle;

    const-string/jumbo v2, "room"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/peel/control/am;->a(Ljava/lang/String;)Lcom/peel/control/RoomControl;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/h/a/bz;->aw:Lcom/peel/control/RoomControl;

    .line 70
    :goto_0
    iget-object v0, p0, Lcom/peel/h/a/bz;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "back_to_clazz"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/peel/h/a/bz;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "back_to_clazz"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_1
    iput-object v0, p0, Lcom/peel/h/a/bz;->au:Ljava/lang/String;

    .line 72
    iget-object v0, p0, Lcom/peel/h/a/bz;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "cycle_activities"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 73
    iget-object v0, p0, Lcom/peel/h/a/bz;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "cycle_activities"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 74
    const-string/jumbo v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/h/a/bz;->ax:[Ljava/lang/String;

    .line 75
    iget-object v0, p0, Lcom/peel/h/a/bz;->ax:[Ljava/lang/String;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    iput-object v0, p0, Lcom/peel/h/a/bz;->at:Ljava/lang/String;

    .line 79
    :goto_2
    iget-object v0, p0, Lcom/peel/h/a/bz;->at:Ljava/lang/String;

    if-nez v0, :cond_3

    .line 80
    sget-object v0, Lcom/peel/h/a/bz;->g:Ljava/lang/String;

    const-string/jumbo v1, "no activity_id specified. this is bad."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 81
    const/4 v0, 0x0

    .line 664
    :goto_3
    return-object v0

    .line 68
    :cond_0
    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v0}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/h/a/bz;->aw:Lcom/peel/control/RoomControl;

    goto :goto_0

    .line 70
    :cond_1
    const-class v0, Lcom/peel/h/a/fn;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 77
    :cond_2
    iget-object v0, p0, Lcom/peel/h/a/bz;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "activity_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/h/a/bz;->at:Ljava/lang/String;

    goto :goto_2

    .line 83
    :cond_3
    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    iget-object v1, p0, Lcom/peel/h/a/bz;->at:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/peel/control/am;->e(Ljava/lang/String;)Lcom/peel/control/a;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/h/a/bz;->av:Lcom/peel/control/a;

    .line 86
    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    iget-object v1, p0, Lcom/peel/h/a/bz;->aw:Lcom/peel/control/RoomControl;

    invoke-virtual {v0, v1}, Lcom/peel/control/am;->c(Lcom/peel/control/RoomControl;)[Lcom/peel/control/h;

    move-result-object v2

    .line 87
    if-eqz v2, :cond_a

    array-length v0, v2

    if-lez v0, :cond_a

    .line 89
    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_4
    if-ge v1, v3, :cond_a

    aget-object v0, v2, v1

    .line 91
    invoke-virtual {v0}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v4

    invoke-virtual {v4}, Lcom/peel/data/g;->d()I

    move-result v4

    const/4 v5, 0x1

    if-ne v4, v5, :cond_7

    .line 92
    iput-object v0, p0, Lcom/peel/h/a/bz;->an:Lcom/peel/control/h;

    .line 93
    iget-object v0, p0, Lcom/peel/h/a/bz;->an:Lcom/peel/control/h;

    invoke-virtual {v0}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/data/g;->o()[Lcom/peel/f/d;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/h/a/bz;->aj:[Lcom/peel/f/d;

    .line 94
    iget-object v0, p0, Lcom/peel/h/a/bz;->aj:[Lcom/peel/f/d;

    array-length v0, v0

    new-array v0, v0, [Ljava/lang/String;

    iput-object v0, p0, Lcom/peel/h/a/bz;->ay:[Ljava/lang/String;

    .line 95
    const/4 v0, 0x0

    :goto_5
    iget-object v4, p0, Lcom/peel/h/a/bz;->aj:[Lcom/peel/f/d;

    array-length v4, v4

    if-ge v0, v4, :cond_4

    .line 96
    iget-object v4, p0, Lcom/peel/h/a/bz;->ay:[Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    sget v6, Lcom/peel/ui/ft;->input:I

    invoke-virtual {p0, v6}, Lcom/peel/h/a/bz;->a(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    add-int/lit8 v6, v0, 0x1

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v0

    .line 95
    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    .line 98
    :cond_4
    iget-object v0, p0, Lcom/peel/h/a/bz;->aj:[Lcom/peel/f/d;

    array-length v0, v0

    if-lez v0, :cond_5

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/peel/h/a/bz;->e:Z

    .line 99
    :cond_5
    iget-object v0, p0, Lcom/peel/h/a/bz;->av:Lcom/peel/control/a;

    iget-object v4, p0, Lcom/peel/h/a/bz;->an:Lcom/peel/control/h;

    invoke-virtual {v0, v4}, Lcom/peel/control/a;->a(Lcom/peel/control/h;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/h/a/bz;->ar:Ljava/lang/String;

    .line 89
    :cond_6
    :goto_6
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_4

    .line 100
    :cond_7
    invoke-virtual {v0}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v4

    invoke-virtual {v4}, Lcom/peel/data/g;->d()I

    move-result v4

    const/4 v5, 0x5

    if-ne v4, v5, :cond_6

    .line 101
    iput-object v0, p0, Lcom/peel/h/a/bz;->ao:Lcom/peel/control/h;

    .line 102
    iget-object v0, p0, Lcom/peel/h/a/bz;->ao:Lcom/peel/control/h;

    invoke-virtual {v0}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/data/g;->o()[Lcom/peel/f/d;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/h/a/bz;->ak:[Lcom/peel/f/d;

    .line 103
    iget-object v0, p0, Lcom/peel/h/a/bz;->ak:[Lcom/peel/f/d;

    array-length v0, v0

    new-array v0, v0, [Ljava/lang/String;

    iput-object v0, p0, Lcom/peel/h/a/bz;->az:[Ljava/lang/String;

    .line 104
    const/4 v0, 0x0

    :goto_7
    iget-object v4, p0, Lcom/peel/h/a/bz;->ak:[Lcom/peel/f/d;

    array-length v4, v4

    if-ge v0, v4, :cond_8

    .line 105
    iget-object v4, p0, Lcom/peel/h/a/bz;->az:[Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    sget v6, Lcom/peel/ui/ft;->input:I

    invoke-virtual {p0, v6}, Lcom/peel/h/a/bz;->a(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    add-int/lit8 v6, v0, 0x1

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v0

    .line 104
    add-int/lit8 v0, v0, 0x1

    goto :goto_7

    .line 107
    :cond_8
    iget-object v0, p0, Lcom/peel/h/a/bz;->ak:[Lcom/peel/f/d;

    array-length v0, v0

    if-lez v0, :cond_9

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/peel/h/a/bz;->f:Z

    .line 108
    :cond_9
    iget-object v0, p0, Lcom/peel/h/a/bz;->av:Lcom/peel/control/a;

    iget-object v4, p0, Lcom/peel/h/a/bz;->ao:Lcom/peel/control/h;

    invoke-virtual {v0, v4}, Lcom/peel/control/a;->a(Lcom/peel/control/h;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/h/a/bz;->as:Ljava/lang/String;

    goto :goto_6

    .line 113
    :cond_a
    sget v0, Lcom/peel/ui/fq;->config_activity_view:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v8

    .line 114
    sget v0, Lcom/peel/ui/fq;->input_list_layout:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/h/a/bz;->h:Landroid/view/View;

    .line 115
    iget-object v0, p0, Lcom/peel/h/a/bz;->h:Landroid/view/View;

    sget v1, Lcom/peel/ui/fp;->msg:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    sget v1, Lcom/peel/ui/ft;->desc_input_list:I

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    sget v4, Lcom/peel/ui/ft;->DeviceType1:I

    invoke-virtual {p0, v4}, Lcom/peel/h/a/bz;->a(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {p0, v1, v2}, Lcom/peel/h/a/bz;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 116
    sget v0, Lcom/peel/ui/fq;->input_list_layout:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/h/a/bz;->i:Landroid/view/View;

    .line 117
    iget-object v0, p0, Lcom/peel/h/a/bz;->i:Landroid/view/View;

    sget v1, Lcom/peel/ui/fp;->msg:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    sget v1, Lcom/peel/ui/ft;->desc_input_list:I

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    sget v4, Lcom/peel/ui/ft;->DeviceType5:I

    invoke-virtual {p0, v4}, Lcom/peel/h/a/bz;->a(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {p0, v1, v2}, Lcom/peel/h/a/bz;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 119
    sget v0, Lcom/peel/ui/fp;->list:I

    invoke-virtual {v8, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/peel/h/a/bz;->aA:Landroid/widget/ListView;

    .line 120
    iget-object v0, p0, Lcom/peel/h/a/bz;->aA:Landroid/widget/ListView;

    invoke-virtual {p0}, Lcom/peel/h/a/bz;->n()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/peel/ui/fm;->list_background:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setBackgroundColor(I)V

    .line 121
    iget-object v0, p0, Lcom/peel/h/a/bz;->aA:Landroid/widget/ListView;

    new-instance v1, Lcom/peel/h/a/ca;

    invoke-direct {v1, p0}, Lcom/peel/h/a/ca;-><init>(Lcom/peel/h/a/bz;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 153
    new-instance v0, Lcom/peel/h/a/fa;

    invoke-direct {v0}, Lcom/peel/h/a/fa;-><init>()V

    iput-object v0, p0, Lcom/peel/h/a/bz;->aB:Lcom/peel/h/a/fa;

    .line 154
    sget v0, Lcom/peel/ui/fq;->roomoverview_settings_header_row:I

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {p1, v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 155
    sget v0, Lcom/peel/ui/fp;->text:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    sget v2, Lcom/peel/ui/ft;->header_volume_setting:I

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    .line 156
    iget-object v0, p0, Lcom/peel/h/a/bz;->aB:Lcom/peel/h/a/fa;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/peel/h/a/fa;->a(Landroid/view/View;Z)V

    .line 158
    sget v0, Lcom/peel/ui/fq;->two_vertical_textviews:I

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {p1, v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 159
    sget v1, Lcom/peel/ui/fp;->text1:I

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    sget v2, Lcom/peel/ui/ft;->volume_controlled_on:I

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 161
    iget-object v1, p0, Lcom/peel/h/a/bz;->an:Lcom/peel/control/h;

    if-nez v1, :cond_b

    iget-object v1, p0, Lcom/peel/h/a/bz;->ao:Lcom/peel/control/h;

    if-nez v1, :cond_b

    .line 162
    sget-object v1, Lcom/peel/h/a/bz;->g:Ljava/lang/String;

    const-string/jumbo v2, "er... no tv and no av, just go back"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 163
    iget-object v1, p0, Lcom/peel/h/a/bz;->b:Lcom/peel/d/i;

    if-eqz v1, :cond_b

    sget-object v1, Lcom/peel/h/a/bz;->g:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/peel/h/a/bz;->m()Landroid/support/v4/app/ae;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/peel/d/e;->a(Ljava/lang/String;Landroid/support/v4/app/ae;)V

    .line 165
    :cond_b
    sget v1, Lcom/peel/ui/fp;->text2:I

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/peel/h/a/bz;->aC:Landroid/widget/TextView;

    .line 166
    iget-object v1, p0, Lcom/peel/h/a/bz;->av:Lcom/peel/control/a;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/peel/control/a;->a(I)Lcom/peel/control/h;

    move-result-object v1

    iput-object v1, p0, Lcom/peel/h/a/bz;->aq:Lcom/peel/control/h;

    .line 167
    const/4 v1, 0x2

    new-array v4, v1, [Ljava/lang/String;

    .line 168
    const/4 v2, 0x0

    iget-object v1, p0, Lcom/peel/h/a/bz;->an:Lcom/peel/control/h;

    if-nez v1, :cond_13

    sget v1, Lcom/peel/ui/ft;->DeviceType1:I

    invoke-virtual {p0, v1}, Lcom/peel/h/a/bz;->a(I)Ljava/lang/String;

    move-result-object v1

    :goto_8
    aput-object v1, v4, v2

    .line 169
    const/4 v2, 0x1

    iget-object v1, p0, Lcom/peel/h/a/bz;->ao:Lcom/peel/control/h;

    if-nez v1, :cond_14

    sget v1, Lcom/peel/ui/ft;->DeviceType5:I

    invoke-virtual {p0, v1}, Lcom/peel/h/a/bz;->a(I)Ljava/lang/String;

    move-result-object v1

    :goto_9
    aput-object v1, v4, v2

    .line 171
    iget-object v1, p0, Lcom/peel/h/a/bz;->aq:Lcom/peel/control/h;

    if-eqz v1, :cond_15

    .line 172
    iget-object v1, p0, Lcom/peel/h/a/bz;->aC:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/peel/h/a/bz;->aq:Lcom/peel/control/h;

    invoke-virtual {v3}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v3

    invoke-virtual {v3}, Lcom/peel/data/g;->f()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/peel/h/a/bz;->m()Landroid/support/v4/app/ae;

    move-result-object v3

    iget-object v5, p0, Lcom/peel/h/a/bz;->aq:Lcom/peel/control/h;

    invoke-virtual {v5}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v5

    invoke-virtual {v5}, Lcom/peel/data/g;->d()I

    move-result v5

    invoke-static {v3, v5}, Lcom/peel/util/bx;->a(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 178
    :cond_c
    :goto_a
    iget-object v1, p0, Lcom/peel/h/a/bz;->aB:Lcom/peel/h/a/fa;

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, Lcom/peel/h/a/fa;->a(Landroid/view/View;Z)V

    .line 182
    sget v0, Lcom/peel/ui/fq;->settings_adddevice_activities:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v9

    .line 183
    sget v0, Lcom/peel/ui/fp;->activities_lv:I

    invoke-virtual {v9, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Landroid/widget/ListView;

    .line 184
    new-instance v0, Lcom/peel/h/a/ch;

    invoke-virtual {p0}, Lcom/peel/h/a/bz;->m()Landroid/support/v4/app/ae;

    move-result-object v2

    sget v3, Lcom/peel/ui/fq;->settings_single_selection_row_wospace:I

    move-object v1, p0

    move-object v5, p1

    move-object v6, v4

    invoke-direct/range {v0 .. v6}, Lcom/peel/h/a/ch;-><init>(Lcom/peel/h/a/bz;Landroid/content/Context;I[Ljava/lang/String;Landroid/view/LayoutInflater;[Ljava/lang/String;)V

    invoke-virtual {v7, v0}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 199
    iget-object v0, p0, Lcom/peel/h/a/bz;->aq:Lcom/peel/control/h;

    if-eqz v0, :cond_18

    .line 200
    iget-object v0, p0, Lcom/peel/h/a/bz;->aq:Lcom/peel/control/h;

    invoke-virtual {v0}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/data/g;->d()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_17

    .line 201
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {v7, v0, v1}, Landroid/widget/ListView;->setItemChecked(IZ)V

    .line 202
    const/4 v0, 0x0

    iput v0, p0, Lcom/peel/h/a/bz;->al:I

    .line 216
    :cond_d
    :goto_b
    new-instance v0, Lcom/peel/widget/ag;

    invoke-virtual {p0}, Lcom/peel/h/a/bz;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/peel/widget/ag;-><init>(Landroid/content/Context;)V

    sget v1, Lcom/peel/ui/ft;->volume_controlled_on:I

    .line 217
    invoke-virtual {v0, v1}, Lcom/peel/widget/ag;->a(I)Lcom/peel/widget/ag;

    move-result-object v0

    .line 218
    invoke-virtual {v0, v9}, Lcom/peel/widget/ag;->a(Landroid/view/View;)Lcom/peel/widget/ag;

    move-result-object v0

    sget v1, Lcom/peel/ui/ft;->cancel:I

    new-instance v2, Lcom/peel/h/a/cj;

    invoke-direct {v2, p0, v7}, Lcom/peel/h/a/cj;-><init>(Lcom/peel/h/a/bz;Landroid/widget/ListView;)V

    .line 219
    invoke-virtual {v0, v1, v2}, Lcom/peel/widget/ag;->c(ILandroid/content/DialogInterface$OnClickListener;)Lcom/peel/widget/ag;

    move-result-object v0

    sget v1, Lcom/peel/ui/ft;->done:I

    new-instance v2, Lcom/peel/h/a/ci;

    invoke-direct {v2, p0, v7, v4}, Lcom/peel/h/a/ci;-><init>(Lcom/peel/h/a/bz;Landroid/widget/ListView;[Ljava/lang/String;)V

    .line 244
    invoke-virtual {v0, v1, v2}, Lcom/peel/widget/ag;->a(ILandroid/content/DialogInterface$OnClickListener;)Lcom/peel/widget/ag;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/h/a/bz;->aH:Lcom/peel/widget/ag;

    .line 269
    iget-object v0, p0, Lcom/peel/h/a/bz;->an:Lcom/peel/control/h;

    if-eqz v0, :cond_e

    iget-object v0, p0, Lcom/peel/h/a/bz;->aj:[Lcom/peel/f/d;

    array-length v0, v0

    if-nez v0, :cond_f

    .line 270
    :cond_e
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/peel/h/a/bz;->e:Z

    .line 272
    :cond_f
    iget-object v0, p0, Lcom/peel/h/a/bz;->ao:Lcom/peel/control/h;

    if-eqz v0, :cond_10

    iget-object v0, p0, Lcom/peel/h/a/bz;->ak:[Lcom/peel/f/d;

    array-length v0, v0

    if-nez v0, :cond_11

    .line 273
    :cond_10
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/peel/h/a/bz;->f:Z

    .line 277
    :cond_11
    iget-boolean v0, p0, Lcom/peel/h/a/bz;->e:Z

    if-nez v0, :cond_1a

    iget-boolean v0, p0, Lcom/peel/h/a/bz;->f:Z

    if-nez v0, :cond_1a

    .line 446
    :cond_12
    :goto_c
    sget v0, Lcom/peel/ui/fp;->action_btn:I

    invoke-virtual {v8, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/peel/h/a/ce;

    invoke-direct {v1, p0}, Lcom/peel/h/a/ce;-><init>(Lcom/peel/h/a/bz;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 662
    iget-object v0, p0, Lcom/peel/h/a/bz;->aA:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/peel/h/a/bz;->aB:Lcom/peel/h/a/fa;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    move-object v0, v8

    .line 664
    goto/16 :goto_3

    .line 168
    :cond_13
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/peel/h/a/bz;->an:Lcom/peel/control/h;

    invoke-virtual {v3}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v3

    invoke-virtual {v3}, Lcom/peel/data/g;->f()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v3, " "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget v3, Lcom/peel/ui/ft;->DeviceType1:I

    invoke-virtual {p0, v3}, Lcom/peel/h/a/bz;->a(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_8

    .line 169
    :cond_14
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/peel/h/a/bz;->ao:Lcom/peel/control/h;

    invoke-virtual {v3}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v3

    invoke-virtual {v3}, Lcom/peel/data/g;->f()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v3, " "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget v3, Lcom/peel/ui/ft;->DeviceType5:I

    invoke-virtual {p0, v3}, Lcom/peel/h/a/bz;->a(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_9

    .line 173
    :cond_15
    iget-object v1, p0, Lcom/peel/h/a/bz;->ao:Lcom/peel/control/h;

    if-eqz v1, :cond_16

    .line 174
    iget-object v1, p0, Lcom/peel/h/a/bz;->aC:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/peel/h/a/bz;->ao:Lcom/peel/control/h;

    invoke-virtual {v3}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v3

    invoke-virtual {v3}, Lcom/peel/data/g;->f()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget v3, Lcom/peel/ui/ft;->DeviceType5:I

    invoke-virtual {p0, v3}, Lcom/peel/h/a/bz;->a(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_a

    .line 175
    :cond_16
    iget-object v1, p0, Lcom/peel/h/a/bz;->an:Lcom/peel/control/h;

    if-eqz v1, :cond_c

    .line 176
    iget-object v1, p0, Lcom/peel/h/a/bz;->aC:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/peel/h/a/bz;->an:Lcom/peel/control/h;

    invoke-virtual {v3}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v3

    invoke-virtual {v3}, Lcom/peel/data/g;->f()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget v3, Lcom/peel/ui/ft;->DeviceType1:I

    invoke-virtual {p0, v3}, Lcom/peel/h/a/bz;->a(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_a

    .line 204
    :cond_17
    iget-object v0, p0, Lcom/peel/h/a/bz;->aq:Lcom/peel/control/h;

    invoke-virtual {v0}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/data/g;->d()I

    move-result v0

    const/4 v1, 0x5

    if-ne v0, v1, :cond_d

    .line 205
    const/4 v0, 0x1

    const/4 v1, 0x1

    invoke-virtual {v7, v0, v1}, Landroid/widget/ListView;->setItemChecked(IZ)V

    .line 206
    const/4 v0, 0x1

    iput v0, p0, Lcom/peel/h/a/bz;->al:I

    goto/16 :goto_b

    .line 208
    :cond_18
    iget-object v0, p0, Lcom/peel/h/a/bz;->ao:Lcom/peel/control/h;

    if-eqz v0, :cond_19

    .line 209
    const/4 v0, 0x1

    const/4 v1, 0x1

    invoke-virtual {v7, v0, v1}, Landroid/widget/ListView;->setItemChecked(IZ)V

    .line 210
    const/4 v0, 0x1

    iput v0, p0, Lcom/peel/h/a/bz;->al:I

    goto/16 :goto_b

    .line 212
    :cond_19
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {v7, v0, v1}, Landroid/widget/ListView;->setItemChecked(IZ)V

    .line 213
    const/4 v0, 0x0

    iput v0, p0, Lcom/peel/h/a/bz;->al:I

    goto/16 :goto_b

    .line 280
    :cond_1a
    sget v0, Lcom/peel/ui/fq;->roomoverview_settings_header_row:I

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {p1, v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 281
    sget v0, Lcom/peel/ui/fp;->text:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    sget v2, Lcom/peel/ui/ft;->header_input_setting:I

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    .line 282
    iget-object v0, p0, Lcom/peel/h/a/bz;->aB:Lcom/peel/h/a/fa;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/peel/h/a/fa;->a(Landroid/view/View;Z)V

    .line 284
    iget-boolean v0, p0, Lcom/peel/h/a/bz;->e:Z

    if-eqz v0, :cond_1c

    .line 285
    sget v0, Lcom/peel/ui/fq;->two_vertical_textviews:I

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {p1, v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 286
    sget v1, Lcom/peel/ui/fp;->text1:I

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/peel/h/a/bz;->an:Lcom/peel/control/h;

    invoke-virtual {v3}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v3

    invoke-virtual {v3}, Lcom/peel/data/g;->f()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget v3, Lcom/peel/ui/ft;->label_tv_input:I

    invoke-virtual {p0, v3}, Lcom/peel/h/a/bz;->a(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 287
    sget v1, Lcom/peel/ui/fp;->text2:I

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/peel/h/a/bz;->aD:Landroid/widget/TextView;

    .line 288
    iget-object v1, p0, Lcom/peel/h/a/bz;->aD:Landroid/widget/TextView;

    sget v2, Lcom/peel/ui/ft;->not_configured:I

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 289
    iget-object v1, p0, Lcom/peel/h/a/bz;->ar:Ljava/lang/String;

    if-eqz v1, :cond_1b

    .line 290
    const/4 v1, 0x0

    :goto_d
    iget-object v2, p0, Lcom/peel/h/a/bz;->aj:[Lcom/peel/f/d;

    array-length v2, v2

    if-ge v1, v2, :cond_1b

    .line 291
    iget-object v2, p0, Lcom/peel/h/a/bz;->ar:Ljava/lang/String;

    iget-object v3, p0, Lcom/peel/h/a/bz;->aj:[Lcom/peel/f/d;

    aget-object v3, v3, v1

    invoke-virtual {v3}, Lcom/peel/f/d;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1e

    .line 292
    iget-object v2, p0, Lcom/peel/h/a/bz;->aD:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/peel/h/a/bz;->ay:[Ljava/lang/String;

    aget-object v1, v3, v1

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 297
    :cond_1b
    iget-object v1, p0, Lcom/peel/h/a/bz;->aB:Lcom/peel/h/a/fa;

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, Lcom/peel/h/a/fa;->a(Landroid/view/View;Z)V

    .line 301
    iget-object v0, p0, Lcom/peel/h/a/bz;->h:Landroid/view/View;

    sget v1, Lcom/peel/ui/fp;->input_list:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    .line 302
    new-instance v1, Landroid/widget/ArrayAdapter;

    invoke-virtual {p0}, Lcom/peel/h/a/bz;->m()Landroid/support/v4/app/ae;

    move-result-object v2

    sget v3, Lcom/peel/ui/fq;->settings_single_selection_row_wospace:I

    iget-object v4, p0, Lcom/peel/h/a/bz;->ay:[Ljava/lang/String;

    invoke-direct {v1, v2, v3, v4}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    .line 303
    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 304
    new-instance v1, Lcom/peel/h/a/ck;

    invoke-direct {v1, p0}, Lcom/peel/h/a/ck;-><init>(Lcom/peel/h/a/bz;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 315
    new-instance v1, Lcom/peel/widget/ag;

    invoke-virtual {p0}, Lcom/peel/h/a/bz;->m()Landroid/support/v4/app/ae;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/peel/widget/ag;-><init>(Landroid/content/Context;)V

    iget-object v2, p0, Lcom/peel/h/a/bz;->h:Landroid/view/View;

    invoke-virtual {v1, v2}, Lcom/peel/widget/ag;->a(Landroid/view/View;)Lcom/peel/widget/ag;

    move-result-object v1

    sget v2, Lcom/peel/ui/ft;->label_tv_input:I

    invoke-virtual {v1, v2}, Lcom/peel/widget/ag;->a(I)Lcom/peel/widget/ag;

    move-result-object v1

    sget v2, Lcom/peel/ui/ft;->done:I

    new-instance v3, Lcom/peel/h/a/cm;

    invoke-direct {v3, p0}, Lcom/peel/h/a/cm;-><init>(Lcom/peel/h/a/bz;)V

    invoke-virtual {v1, v2, v3}, Lcom/peel/widget/ag;->a(ILandroid/content/DialogInterface$OnClickListener;)Lcom/peel/widget/ag;

    move-result-object v1

    sget v2, Lcom/peel/ui/ft;->label_skip:I

    new-instance v3, Lcom/peel/h/a/cl;

    invoke-direct {v3, p0, v0}, Lcom/peel/h/a/cl;-><init>(Lcom/peel/h/a/bz;Landroid/widget/ListView;)V

    .line 322
    invoke-virtual {v1, v2, v3}, Lcom/peel/widget/ag;->c(ILandroid/content/DialogInterface$OnClickListener;)Lcom/peel/widget/ag;

    move-result-object v1

    iput-object v1, p0, Lcom/peel/h/a/bz;->aF:Lcom/peel/widget/ag;

    .line 335
    iget-object v1, p0, Lcom/peel/h/a/bz;->aF:Lcom/peel/widget/ag;

    invoke-virtual {v1}, Lcom/peel/widget/ag;->f()Landroid/widget/Button;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setEnabled(Z)V

    .line 336
    iget-object v1, p0, Lcom/peel/h/a/bz;->aF:Lcom/peel/widget/ag;

    invoke-virtual {v1}, Lcom/peel/widget/ag;->f()Landroid/widget/Button;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setClickable(Z)V

    .line 339
    iget-object v1, p0, Lcom/peel/h/a/bz;->aF:Lcom/peel/widget/ag;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/peel/widget/ag;->a(Z)Lcom/peel/widget/ag;

    move-result-object v1

    iget-object v2, p0, Lcom/peel/h/a/bz;->aF:Lcom/peel/widget/ag;

    invoke-virtual {v1, v2}, Lcom/peel/widget/ag;->a(Lcom/peel/widget/ag;)Lcom/peel/widget/ag;

    .line 340
    iget-object v1, p0, Lcom/peel/h/a/bz;->aF:Lcom/peel/widget/ag;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/peel/widget/ag;->setCanceledOnTouchOutside(Z)V

    .line 342
    iget-object v1, p0, Lcom/peel/h/a/bz;->aD:Landroid/widget/TextView;

    new-instance v2, Lcom/peel/h/a/cn;

    invoke-direct {v2, p0}, Lcom/peel/h/a/cn;-><init>(Lcom/peel/h/a/bz;)V

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 350
    iget-object v1, p0, Lcom/peel/h/a/bz;->av:Lcom/peel/control/a;

    iget-object v2, p0, Lcom/peel/h/a/bz;->an:Lcom/peel/control/h;

    invoke-virtual {v1, v2}, Lcom/peel/control/a;->a(Lcom/peel/control/h;)Ljava/lang/String;

    move-result-object v2

    .line 352
    if-eqz v2, :cond_1c

    .line 353
    const/4 v1, 0x0

    :goto_e
    iget-object v3, p0, Lcom/peel/h/a/bz;->aj:[Lcom/peel/f/d;

    array-length v3, v3

    if-ge v1, v3, :cond_1c

    .line 354
    iget-object v3, p0, Lcom/peel/h/a/bz;->aj:[Lcom/peel/f/d;

    aget-object v3, v3, v1

    invoke-virtual {v3}, Lcom/peel/f/d;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1f

    .line 355
    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/widget/ListView;->setItemChecked(IZ)V

    .line 356
    iget-object v0, p0, Lcom/peel/h/a/bz;->aD:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/peel/h/a/bz;->ay:[Ljava/lang/String;

    aget-object v1, v2, v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 363
    :cond_1c
    iget-boolean v0, p0, Lcom/peel/h/a/bz;->f:Z

    if-eqz v0, :cond_12

    .line 364
    sget v0, Lcom/peel/ui/fq;->two_vertical_textviews:I

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {p1, v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 365
    sget v1, Lcom/peel/ui/fp;->text1:I

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/peel/h/a/bz;->ao:Lcom/peel/control/h;

    invoke-virtual {v3}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v3

    invoke-virtual {v3}, Lcom/peel/data/g;->f()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget v3, Lcom/peel/ui/ft;->label_stereo_input:I

    invoke-virtual {p0, v3}, Lcom/peel/h/a/bz;->a(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 366
    sget v1, Lcom/peel/ui/fp;->text2:I

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/peel/h/a/bz;->aE:Landroid/widget/TextView;

    .line 367
    iget-object v1, p0, Lcom/peel/h/a/bz;->aE:Landroid/widget/TextView;

    sget v2, Lcom/peel/ui/ft;->not_configured:I

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 368
    iget-object v1, p0, Lcom/peel/h/a/bz;->as:Ljava/lang/String;

    if-eqz v1, :cond_1d

    .line 369
    const/4 v1, 0x0

    :goto_f
    iget-object v2, p0, Lcom/peel/h/a/bz;->ak:[Lcom/peel/f/d;

    array-length v2, v2

    if-ge v1, v2, :cond_1d

    .line 370
    iget-object v2, p0, Lcom/peel/h/a/bz;->as:Ljava/lang/String;

    iget-object v3, p0, Lcom/peel/h/a/bz;->ak:[Lcom/peel/f/d;

    aget-object v3, v3, v1

    invoke-virtual {v3}, Lcom/peel/f/d;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_20

    .line 371
    iget-object v2, p0, Lcom/peel/h/a/bz;->aE:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/peel/h/a/bz;->az:[Ljava/lang/String;

    aget-object v1, v3, v1

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 376
    :cond_1d
    iget-object v1, p0, Lcom/peel/h/a/bz;->aB:Lcom/peel/h/a/fa;

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, Lcom/peel/h/a/fa;->a(Landroid/view/View;Z)V

    .line 380
    iget-object v0, p0, Lcom/peel/h/a/bz;->i:Landroid/view/View;

    sget v1, Lcom/peel/ui/fp;->input_list:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    .line 381
    new-instance v1, Landroid/widget/ArrayAdapter;

    invoke-virtual {p0}, Lcom/peel/h/a/bz;->m()Landroid/support/v4/app/ae;

    move-result-object v2

    sget v3, Lcom/peel/ui/fq;->settings_single_selection_row_wospace:I

    iget-object v4, p0, Lcom/peel/h/a/bz;->az:[Ljava/lang/String;

    invoke-direct {v1, v2, v3, v4}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 385
    new-instance v1, Lcom/peel/h/a/co;

    invoke-direct {v1, p0}, Lcom/peel/h/a/co;-><init>(Lcom/peel/h/a/bz;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 402
    new-instance v1, Lcom/peel/widget/ag;

    invoke-virtual {p0}, Lcom/peel/h/a/bz;->m()Landroid/support/v4/app/ae;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/peel/widget/ag;-><init>(Landroid/content/Context;)V

    iget-object v2, p0, Lcom/peel/h/a/bz;->i:Landroid/view/View;

    invoke-virtual {v1, v2}, Lcom/peel/widget/ag;->a(Landroid/view/View;)Lcom/peel/widget/ag;

    move-result-object v1

    sget v2, Lcom/peel/ui/ft;->label_stereo_input:I

    invoke-virtual {v1, v2}, Lcom/peel/widget/ag;->a(I)Lcom/peel/widget/ag;

    move-result-object v1

    sget v2, Lcom/peel/ui/ft;->done:I

    new-instance v3, Lcom/peel/h/a/cc;

    invoke-direct {v3, p0}, Lcom/peel/h/a/cc;-><init>(Lcom/peel/h/a/bz;)V

    invoke-virtual {v1, v2, v3}, Lcom/peel/widget/ag;->a(ILandroid/content/DialogInterface$OnClickListener;)Lcom/peel/widget/ag;

    move-result-object v1

    sget v2, Lcom/peel/ui/ft;->label_skip:I

    new-instance v3, Lcom/peel/h/a/cb;

    invoke-direct {v3, p0, v0}, Lcom/peel/h/a/cb;-><init>(Lcom/peel/h/a/bz;Landroid/widget/ListView;)V

    .line 409
    invoke-virtual {v1, v2, v3}, Lcom/peel/widget/ag;->c(ILandroid/content/DialogInterface$OnClickListener;)Lcom/peel/widget/ag;

    move-result-object v1

    iput-object v1, p0, Lcom/peel/h/a/bz;->aG:Lcom/peel/widget/ag;

    .line 421
    iget-object v1, p0, Lcom/peel/h/a/bz;->aG:Lcom/peel/widget/ag;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/peel/widget/ag;->a(Z)Lcom/peel/widget/ag;

    move-result-object v1

    iget-object v2, p0, Lcom/peel/h/a/bz;->aG:Lcom/peel/widget/ag;

    invoke-virtual {v1, v2}, Lcom/peel/widget/ag;->a(Lcom/peel/widget/ag;)Lcom/peel/widget/ag;

    .line 422
    iget-object v1, p0, Lcom/peel/h/a/bz;->aG:Lcom/peel/widget/ag;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/peel/widget/ag;->setCanceledOnTouchOutside(Z)V

    .line 423
    iget-object v1, p0, Lcom/peel/h/a/bz;->aG:Lcom/peel/widget/ag;

    invoke-virtual {v1}, Lcom/peel/widget/ag;->f()Landroid/widget/Button;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setEnabled(Z)V

    .line 424
    iget-object v1, p0, Lcom/peel/h/a/bz;->aE:Landroid/widget/TextView;

    new-instance v2, Lcom/peel/h/a/cd;

    invoke-direct {v2, p0}, Lcom/peel/h/a/cd;-><init>(Lcom/peel/h/a/bz;)V

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 432
    iget-object v1, p0, Lcom/peel/h/a/bz;->av:Lcom/peel/control/a;

    iget-object v2, p0, Lcom/peel/h/a/bz;->ao:Lcom/peel/control/h;

    invoke-virtual {v1, v2}, Lcom/peel/control/a;->a(Lcom/peel/control/h;)Ljava/lang/String;

    move-result-object v2

    .line 434
    if-eqz v2, :cond_12

    .line 435
    const/4 v1, 0x0

    :goto_10
    iget-object v3, p0, Lcom/peel/h/a/bz;->ak:[Lcom/peel/f/d;

    array-length v3, v3

    if-ge v1, v3, :cond_12

    .line 436
    iget-object v3, p0, Lcom/peel/h/a/bz;->ak:[Lcom/peel/f/d;

    aget-object v3, v3, v1

    invoke-virtual {v3}, Lcom/peel/f/d;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_21

    .line 437
    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/widget/ListView;->setItemChecked(IZ)V

    .line 438
    iget-object v0, p0, Lcom/peel/h/a/bz;->aE:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/peel/h/a/bz;->az:[Ljava/lang/String;

    aget-object v1, v2, v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_c

    .line 290
    :cond_1e
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_d

    .line 353
    :cond_1f
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_e

    .line 369
    :cond_20
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_f

    .line 435
    :cond_21
    add-int/lit8 v1, v1, 0x1

    goto :goto_10
.end method

.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 751
    iget-object v0, p0, Lcom/peel/h/a/bz;->au:Ljava/lang/String;

    return-object v0
.end method

.method public a_(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 61
    invoke-super {p0, p1}, Lcom/peel/d/u;->a_(Landroid/os/Bundle;)V

    .line 62
    return-void
.end method

.method public c(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 742
    invoke-super {p0, p1}, Lcom/peel/d/u;->c(Landroid/os/Bundle;)V

    .line 744
    sget-object v0, Lcom/peel/content/a;->h:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "refresh"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_1

    .line 747
    :cond_0
    :goto_0
    return-void

    .line 746
    :cond_1
    iget-object v0, p0, Lcom/peel/h/a/bz;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "refresh"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->remove(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public d(Landroid/os/Bundle;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 695
    invoke-super {p0, p1}, Lcom/peel/d/u;->d(Landroid/os/Bundle;)V

    .line 696
    iget-object v0, p0, Lcom/peel/h/a/bz;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "category"

    sget v2, Lcom/peel/ui/ft;->edit_something:I

    new-array v3, v6, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/peel/h/a/bz;->av:Lcom/peel/control/a;

    invoke-virtual {v5}, Lcom/peel/control/a;->a()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {p0, v2, v3}, Lcom/peel/h/a/bz;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 699
    iget-object v0, p0, Lcom/peel/h/a/bz;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "refresh"

    invoke-virtual {v0, v1, v6}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 700
    sget-object v0, Lcom/peel/content/a;->h:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/peel/h/a/bz;->c:Landroid/os/Bundle;

    invoke-virtual {p0, v0}, Lcom/peel/h/a/bz;->c(Landroid/os/Bundle;)V

    .line 701
    :cond_0
    return-void
.end method

.method public f()V
    .locals 3

    .prologue
    .line 675
    invoke-super {p0}, Lcom/peel/d/u;->f()V

    .line 676
    invoke-virtual {p0}, Lcom/peel/h/a/bz;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    const-string/jumbo v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/ae;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 677
    invoke-virtual {p0}, Lcom/peel/h/a/bz;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/ae;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 678
    iget-object v0, p0, Lcom/peel/h/a/bz;->aF:Lcom/peel/widget/ag;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/peel/h/a/bz;->aF:Lcom/peel/widget/ag;

    invoke-virtual {v0}, Lcom/peel/widget/ag;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 680
    iget-object v0, p0, Lcom/peel/h/a/bz;->aF:Lcom/peel/widget/ag;

    invoke-virtual {v0}, Lcom/peel/widget/ag;->dismiss()V

    .line 682
    :cond_0
    iget-object v0, p0, Lcom/peel/h/a/bz;->aG:Lcom/peel/widget/ag;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/peel/h/a/bz;->aG:Lcom/peel/widget/ag;

    invoke-virtual {v0}, Lcom/peel/widget/ag;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 683
    iget-object v0, p0, Lcom/peel/h/a/bz;->aG:Lcom/peel/widget/ag;

    invoke-virtual {v0}, Lcom/peel/widget/ag;->dismiss()V

    .line 685
    :cond_1
    iget-object v0, p0, Lcom/peel/h/a/bz;->aH:Lcom/peel/widget/ag;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/peel/h/a/bz;->aH:Lcom/peel/widget/ag;

    invoke-virtual {v0}, Lcom/peel/widget/ag;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 686
    iget-object v0, p0, Lcom/peel/h/a/bz;->aH:Lcom/peel/widget/ag;

    invoke-virtual {v0}, Lcom/peel/widget/ag;->dismiss()V

    .line 688
    :cond_2
    iget-object v0, p0, Lcom/peel/h/a/bz;->aI:Lcom/peel/widget/ag;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/peel/h/a/bz;->aI:Lcom/peel/widget/ag;

    invoke-virtual {v0}, Lcom/peel/widget/ag;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 689
    iget-object v0, p0, Lcom/peel/h/a/bz;->aI:Lcom/peel/widget/ag;

    invoke-virtual {v0}, Lcom/peel/widget/ag;->dismiss()V

    .line 691
    :cond_3
    return-void
.end method

.method public h(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 768
    invoke-super {p0, p1}, Lcom/peel/d/u;->h(Landroid/os/Bundle;)V

    .line 770
    invoke-virtual {p0}, Lcom/peel/h/a/bz;->Z()V

    .line 771
    return-void
.end method

.method public w()V
    .locals 0

    .prologue
    .line 669
    invoke-super {p0}, Lcom/peel/d/u;->w()V

    .line 670
    invoke-virtual {p0}, Lcom/peel/h/a/bz;->Z()V

    .line 671
    return-void
.end method
