.class Lcom/peel/h/a/fv;
.super Lcom/peel/util/t;


# instance fields
.field final synthetic a:Landroid/view/View;

.field final synthetic b:Landroid/view/View;

.field final synthetic c:Landroid/view/View;

.field final synthetic d:Landroid/view/View;

.field final synthetic e:Landroid/widget/LinearLayout;

.field final synthetic f:Lcom/peel/h/a/fn;


# direct methods
.method constructor <init>(Lcom/peel/h/a/fn;ILandroid/view/View;Landroid/view/View;Landroid/view/View;Landroid/view/View;Landroid/widget/LinearLayout;)V
    .locals 0

    .prologue
    .line 336
    iput-object p1, p0, Lcom/peel/h/a/fv;->f:Lcom/peel/h/a/fn;

    iput-object p3, p0, Lcom/peel/h/a/fv;->a:Landroid/view/View;

    iput-object p4, p0, Lcom/peel/h/a/fv;->b:Landroid/view/View;

    iput-object p5, p0, Lcom/peel/h/a/fv;->c:Landroid/view/View;

    iput-object p6, p0, Lcom/peel/h/a/fv;->d:Landroid/view/View;

    iput-object p7, p0, Lcom/peel/h/a/fv;->e:Landroid/widget/LinearLayout;

    invoke-direct {p0, p2}, Lcom/peel/util/t;-><init>(I)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    const/4 v2, 0x0

    const/16 v7, 0x8

    .line 339
    iget-boolean v0, p0, Lcom/peel/h/a/fv;->i:Z

    if-nez v0, :cond_0

    .line 392
    :goto_0
    return-void

    .line 343
    :cond_0
    iget-object v0, p0, Lcom/peel/h/a/fv;->j:Ljava/lang/Object;

    check-cast v0, [Lcom/peel/content/library/Library;

    check-cast v0, [Lcom/peel/content/library/Library;

    array-length v4, v0

    const/4 v1, 0x0

    move v3, v1

    :goto_1
    if-ge v3, v4, :cond_6

    aget-object v1, v0, v3

    .line 344
    invoke-virtual {v1}, Lcom/peel/content/library/Library;->f()Ljava/lang/String;

    move-result-object v5

    const-string/jumbo v6, "live"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    move-object v0, v1

    .line 345
    check-cast v0, Lcom/peel/content/library/LiveLibrary;

    .line 352
    :goto_2
    iget-object v1, p0, Lcom/peel/h/a/fv;->f:Lcom/peel/h/a/fn;

    invoke-virtual {v0}, Lcom/peel/content/library/LiveLibrary;->d()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/peel/h/a/fn;->a(Lcom/peel/h/a/fn;Ljava/lang/String;)Ljava/lang/String;

    .line 353
    iget-object v1, p0, Lcom/peel/h/a/fv;->f:Lcom/peel/h/a/fn;

    invoke-virtual {v1}, Lcom/peel/h/a/fn;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/peel/h/a/fv;->f:Lcom/peel/h/a/fn;

    invoke-virtual {v1}, Lcom/peel/h/a/fn;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/ae;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    .line 354
    :goto_3
    iget-object v3, p0, Lcom/peel/h/a/fv;->f:Lcom/peel/h/a/fn;

    invoke-static {v3}, Lcom/peel/h/a/fn;->g(Lcom/peel/h/a/fn;)Landroid/widget/TextView;

    move-result-object v3

    iget-object v4, p0, Lcom/peel/h/a/fv;->f:Lcom/peel/h/a/fn;

    invoke-static {v4}, Lcom/peel/h/a/fn;->f(Lcom/peel/h/a/fn;)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/peel/h/a/fv;->f:Lcom/peel/h/a/fn;

    invoke-virtual {v5}, Lcom/peel/h/a/fn;->n()Landroid/content/res/Resources;

    move-result-object v5

    invoke-static {v4, v1, v5}, Lcom/peel/util/bx;->a(Ljava/lang/String;Ljava/lang/String;Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 357
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 358
    const-string/jumbo v3, "content_user"

    sget-object v4, Lcom/peel/content/a;->f:Lcom/peel/content/user/User;

    invoke-virtual {v1, v3, v4}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 359
    sget-object v3, Lcom/peel/content/a;->f:Lcom/peel/content/user/User;

    invoke-virtual {v3}, Lcom/peel/content/user/User;->c()Landroid/os/Bundle;

    move-result-object v3

    .line 360
    const-string/jumbo v4, "hdprefs"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v6, p0, Lcom/peel/h/a/fv;->f:Lcom/peel/h/a/fn;

    invoke-static {v6}, Lcom/peel/h/a/fn;->e(Lcom/peel/h/a/fn;)Lcom/peel/data/ContentRoom;

    move-result-object v6

    invoke-virtual {v6}, Lcom/peel/data/ContentRoom;->a()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, "/"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Lcom/peel/content/library/LiveLibrary;->e()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v4, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 361
    const-string/jumbo v3, "path"

    const-string/jumbo v4, "lineup"

    invoke-virtual {v1, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 362
    const-string/jumbo v3, "user"

    sget-object v4, Lcom/peel/content/a;->f:Lcom/peel/content/user/User;

    invoke-virtual {v4}, Lcom/peel/content/user/User;->x()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 363
    const-string/jumbo v3, "country"

    iget-object v4, p0, Lcom/peel/h/a/fv;->f:Lcom/peel/h/a/fn;

    invoke-virtual {v4}, Lcom/peel/h/a/fn;->m()Landroid/support/v4/app/ae;

    move-result-object v4

    invoke-static {v4}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v4

    const-string/jumbo v5, "country_ISO"

    const-string/jumbo v6, "US"

    invoke-interface {v4, v5, v6}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 364
    new-instance v3, Lcom/peel/h/a/fw;

    const/4 v4, 0x1

    invoke-direct {v3, p0, v4}, Lcom/peel/h/a/fw;-><init>(Lcom/peel/h/a/fv;I)V

    invoke-virtual {v0, v1, v3}, Lcom/peel/content/library/LiveLibrary;->a(Landroid/os/Bundle;Lcom/peel/util/t;)V

    .line 378
    iget-object v1, p0, Lcom/peel/h/a/fv;->f:Lcom/peel/h/a/fn;

    invoke-virtual {v1}, Lcom/peel/h/a/fn;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    invoke-static {v1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    const-string/jumbo v3, "country"

    const-string/jumbo v4, ""

    invoke-interface {v1, v3, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v3, "Japan"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 379
    iget-object v1, p0, Lcom/peel/h/a/fv;->f:Lcom/peel/h/a/fn;

    invoke-virtual {v1}, Lcom/peel/h/a/fn;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/peel/h/a/fv;->f:Lcom/peel/h/a/fn;

    invoke-virtual {v1}, Lcom/peel/h/a/fn;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/ae;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    .line 380
    :cond_1
    invoke-virtual {v0}, Lcom/peel/content/library/LiveLibrary;->j()Ljava/lang/String;

    move-result-object v1

    iget-object v3, p0, Lcom/peel/h/a/fv;->f:Lcom/peel/h/a/fn;

    invoke-virtual {v3}, Lcom/peel/h/a/fn;->m()Landroid/support/v4/app/ae;

    move-result-object v3

    invoke-virtual {v3}, Landroid/support/v4/app/ae;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/peel/util/bx;->a(Ljava/lang/String;Ljava/lang/String;Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v1

    .line 381
    const-string/jumbo v2, "JP_BS"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 382
    iget-object v2, p0, Lcom/peel/h/a/fv;->a:Landroid/view/View;

    invoke-virtual {v2, v7}, Landroid/view/View;->setVisibility(I)V

    .line 383
    iget-object v2, p0, Lcom/peel/h/a/fv;->b:Landroid/view/View;

    invoke-virtual {v2, v7}, Landroid/view/View;->setVisibility(I)V

    .line 385
    :cond_2
    const-string/jumbo v2, "JP_CS"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 386
    iget-object v1, p0, Lcom/peel/h/a/fv;->c:Landroid/view/View;

    invoke-virtual {v1, v7}, Landroid/view/View;->setVisibility(I)V

    .line 387
    iget-object v1, p0, Lcom/peel/h/a/fv;->d:Landroid/view/View;

    invoke-virtual {v1, v7}, Landroid/view/View;->setVisibility(I)V

    .line 391
    :cond_3
    iget-object v1, p0, Lcom/peel/h/a/fv;->e:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setTag(Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 343
    :cond_4
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto/16 :goto_1

    :cond_5
    move-object v1, v2

    .line 353
    goto/16 :goto_3

    :cond_6
    move-object v0, v2

    goto/16 :goto_2
.end method
