.class Lcom/peel/h/a/bs;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/widget/AdapterView$OnItemLongClickListener;


# instance fields
.field final synthetic a:Lcom/peel/h/a/bb;


# direct methods
.method constructor <init>(Lcom/peel/h/a/bb;)V
    .locals 0

    .prologue
    .line 573
    iput-object p1, p0, Lcom/peel/h/a/bs;->a:Lcom/peel/h/a/bb;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemLongClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)Z
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)Z"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    const/4 v4, 0x1

    .line 576
    if-le p3, v4, :cond_0

    .line 577
    iget-object v0, p0, Lcom/peel/h/a/bs;->a:Lcom/peel/h/a/bb;

    new-instance v1, Lcom/peel/widget/ag;

    iget-object v2, p0, Lcom/peel/h/a/bs;->a:Lcom/peel/h/a/bb;

    invoke-virtual {v2}, Lcom/peel/h/a/bb;->m()Landroid/support/v4/app/ae;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/peel/widget/ag;-><init>(Landroid/content/Context;)V

    invoke-static {v0, v1}, Lcom/peel/h/a/bb;->d(Lcom/peel/h/a/bb;Lcom/peel/widget/ag;)Lcom/peel/widget/ag;

    .line 578
    iget-object v0, p0, Lcom/peel/h/a/bs;->a:Lcom/peel/h/a/bb;

    invoke-static {v0}, Lcom/peel/h/a/bb;->d(Lcom/peel/h/a/bb;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lcom/peel/ui/fq;->rename_room_new:I

    iget-object v2, p0, Lcom/peel/h/a/bs;->a:Lcom/peel/h/a/bb;

    invoke-static {v2}, Lcom/peel/h/a/bb;->a(Lcom/peel/h/a/bb;)Landroid/widget/ListView;

    move-result-object v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 579
    sget v0, Lcom/peel/ui/fp;->edittext:I

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iget-object v1, p0, Lcom/peel/h/a/bs;->a:Lcom/peel/h/a/bb;

    invoke-static {v1}, Lcom/peel/h/a/bb;->a(Lcom/peel/h/a/bb;)Landroid/widget/ListView;

    move-result-object v1

    invoke-virtual {v1, p3}, Landroid/widget/ListView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/peel/data/Channel;

    invoke-virtual {v1}, Lcom/peel/data/Channel;->m()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 580
    sget v0, Lcom/peel/ui/fp;->edittext:I

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    invoke-static {v4, v4}, Landroid/text/method/DigitsKeyListener;->getInstance(ZZ)Landroid/text/method/DigitsKeyListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setKeyListener(Landroid/text/method/KeyListener;)V

    .line 581
    iget-object v0, p0, Lcom/peel/h/a/bs;->a:Lcom/peel/h/a/bb;

    invoke-static {v0}, Lcom/peel/h/a/bb;->r(Lcom/peel/h/a/bb;)Lcom/peel/widget/ag;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/peel/widget/ag;->setCanceledOnTouchOutside(Z)V

    .line 582
    iget-object v0, p0, Lcom/peel/h/a/bs;->a:Lcom/peel/h/a/bb;

    invoke-static {v0}, Lcom/peel/h/a/bb;->r(Lcom/peel/h/a/bb;)Lcom/peel/widget/ag;

    move-result-object v0

    sget v1, Lcom/peel/ui/ft;->update_channel_number_title:I

    invoke-virtual {v0, v1}, Lcom/peel/widget/ag;->a(I)Lcom/peel/widget/ag;

    .line 583
    iget-object v0, p0, Lcom/peel/h/a/bs;->a:Lcom/peel/h/a/bb;

    invoke-static {v0}, Lcom/peel/h/a/bb;->r(Lcom/peel/h/a/bb;)Lcom/peel/widget/ag;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/peel/widget/ag;->a(Landroid/view/View;)Lcom/peel/widget/ag;

    .line 584
    iget-object v0, p0, Lcom/peel/h/a/bs;->a:Lcom/peel/h/a/bb;

    invoke-static {v0}, Lcom/peel/h/a/bb;->r(Lcom/peel/h/a/bb;)Lcom/peel/widget/ag;

    move-result-object v0

    sget v1, Lcom/peel/ui/ft;->cancel:I

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v3}, Lcom/peel/widget/ag;->c(ILandroid/content/DialogInterface$OnClickListener;)Lcom/peel/widget/ag;

    .line 585
    iget-object v0, p0, Lcom/peel/h/a/bs;->a:Lcom/peel/h/a/bb;

    invoke-static {v0}, Lcom/peel/h/a/bb;->r(Lcom/peel/h/a/bb;)Lcom/peel/widget/ag;

    move-result-object v0

    sget v1, Lcom/peel/ui/ft;->done:I

    new-instance v3, Lcom/peel/h/a/bt;

    invoke-direct {v3, p0, p3, v2, p1}, Lcom/peel/h/a/bt;-><init>(Lcom/peel/h/a/bs;ILandroid/view/View;Landroid/widget/AdapterView;)V

    invoke-virtual {v0, v1, v3}, Lcom/peel/widget/ag;->a(ILandroid/content/DialogInterface$OnClickListener;)Lcom/peel/widget/ag;

    .line 613
    iget-object v0, p0, Lcom/peel/h/a/bs;->a:Lcom/peel/h/a/bb;

    invoke-static {v0}, Lcom/peel/h/a/bb;->r(Lcom/peel/h/a/bb;)Lcom/peel/widget/ag;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/widget/ag;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Landroid/view/Window;->setSoftInputMode(I)V

    .line 614
    iget-object v0, p0, Lcom/peel/h/a/bs;->a:Lcom/peel/h/a/bb;

    invoke-static {v0}, Lcom/peel/h/a/bb;->r(Lcom/peel/h/a/bb;)Lcom/peel/widget/ag;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/h/a/bs;->a:Lcom/peel/h/a/bb;

    invoke-static {v1}, Lcom/peel/h/a/bb;->r(Lcom/peel/h/a/bb;)Lcom/peel/widget/ag;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/peel/widget/ag;->a(Lcom/peel/widget/ag;)Lcom/peel/widget/ag;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/widget/ag;->show()V

    .line 616
    :cond_0
    return v4
.end method
