.class Lcom/peel/h/a/j;
.super Lcom/peel/util/t;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/peel/util/t",
        "<",
        "Ljava/util/Map",
        "<",
        "Ljava/lang/String;",
        "Ljava/util/Map",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Object;",
        ">;>;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/peel/h/a/i;


# direct methods
.method constructor <init>(Lcom/peel/h/a/i;I)V
    .locals 0

    .prologue
    .line 680
    iput-object p1, p0, Lcom/peel/h/a/j;->a:Lcom/peel/h/a/i;

    invoke-direct {p0, p2}, Lcom/peel/util/t;-><init>(I)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 14

    .prologue
    const/4 v13, 0x5

    const/4 v12, 0x2

    const/4 v11, 0x0

    const/4 v1, 0x0

    const/4 v10, 0x1

    .line 685
    :try_start_0
    iget-object v0, p0, Lcom/peel/h/a/j;->a:Lcom/peel/h/a/i;

    iget-object v0, v0, Lcom/peel/h/a/i;->b:Lcom/peel/h/a/a;

    invoke-virtual {v0}, Lcom/peel/h/a/a;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/support/v4/app/ae;->setProgressBarIndeterminateVisibility(Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 690
    :goto_0
    iget-boolean v0, p0, Lcom/peel/h/a/j;->i:Z

    if-nez v0, :cond_2

    .line 691
    iget-object v0, p0, Lcom/peel/h/a/j;->k:Ljava/lang/String;

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/peel/h/a/a;->c()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "unable to getAllIrCodesByCodesetid"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 692
    :cond_0
    iget-object v0, p0, Lcom/peel/h/a/j;->a:Lcom/peel/h/a/i;

    iget-object v0, v0, Lcom/peel/h/a/i;->b:Lcom/peel/h/a/a;

    invoke-static {v0}, Lcom/peel/h/a/a;->e(Lcom/peel/h/a/a;)V

    .line 888
    :cond_1
    :goto_1
    return-void

    .line 696
    :cond_2
    iget-object v0, p0, Lcom/peel/h/a/j;->a:Lcom/peel/h/a/i;

    iget-object v0, v0, Lcom/peel/h/a/i;->b:Lcom/peel/h/a/a;

    invoke-static {v0}, Lcom/peel/h/a/a;->p(Lcom/peel/h/a/a;)Lcom/peel/control/h;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v2

    iget-object v0, p0, Lcom/peel/h/a/j;->a:Lcom/peel/h/a/i;

    iget-object v0, v0, Lcom/peel/h/a/i;->b:Lcom/peel/h/a/a;

    invoke-static {v0}, Lcom/peel/h/a/a;->w(Lcom/peel/h/a/a;)I

    move-result v3

    iget-object v0, p0, Lcom/peel/h/a/j;->j:Ljava/lang/Object;

    check-cast v0, Ljava/util/Map;

    invoke-virtual {v2, v3, v0}, Lcom/peel/data/g;->a(ILjava/util/Map;)V

    .line 697
    iget-object v0, p0, Lcom/peel/h/a/j;->a:Lcom/peel/h/a/i;

    iget-object v0, v0, Lcom/peel/h/a/i;->b:Lcom/peel/h/a/a;

    invoke-static {v0}, Lcom/peel/h/a/a;->p(Lcom/peel/h/a/a;)Lcom/peel/control/h;

    move-result-object v0

    invoke-virtual {v0, v10}, Lcom/peel/control/h;->a(I)V

    .line 698
    iget-object v0, p0, Lcom/peel/h/a/j;->a:Lcom/peel/h/a/i;

    iget-object v0, v0, Lcom/peel/h/a/i;->b:Lcom/peel/h/a/a;

    invoke-static {v0}, Lcom/peel/h/a/a;->y(Lcom/peel/h/a/a;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 701
    iget-object v0, p0, Lcom/peel/h/a/j;->a:Lcom/peel/h/a/i;

    iget-object v0, v0, Lcom/peel/h/a/i;->b:Lcom/peel/h/a/a;

    invoke-static {v0}, Lcom/peel/h/a/a;->q(Lcom/peel/h/a/a;)Lcom/peel/control/a;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 702
    iget-object v0, p0, Lcom/peel/h/a/j;->a:Lcom/peel/h/a/i;

    iget-object v0, v0, Lcom/peel/h/a/i;->b:Lcom/peel/h/a/a;

    invoke-static {v0}, Lcom/peel/h/a/a;->q(Lcom/peel/h/a/a;)Lcom/peel/control/a;

    move-result-object v0

    iget-object v2, p0, Lcom/peel/h/a/j;->a:Lcom/peel/h/a/i;

    iget-object v2, v2, Lcom/peel/h/a/i;->b:Lcom/peel/h/a/a;

    invoke-static {v2}, Lcom/peel/h/a/a;->p(Lcom/peel/h/a/a;)Lcom/peel/control/h;

    move-result-object v2

    new-array v3, v10, [Ljava/lang/Integer;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v1

    invoke-virtual {v0, v2, v11, v3}, Lcom/peel/control/a;->a(Lcom/peel/control/h;Ljava/lang/String;[Ljava/lang/Integer;)Z

    .line 706
    :cond_3
    iget-object v0, p0, Lcom/peel/h/a/j;->a:Lcom/peel/h/a/i;

    iget-object v0, v0, Lcom/peel/h/a/i;->b:Lcom/peel/h/a/a;

    invoke-static {v0}, Lcom/peel/h/a/a;->l(Lcom/peel/h/a/a;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/peel/control/a;->a(Ljava/lang/String;)Lcom/peel/control/a;

    move-result-object v0

    .line 707
    iget-object v2, p0, Lcom/peel/h/a/j;->a:Lcom/peel/h/a/i;

    iget-object v2, v2, Lcom/peel/h/a/i;->a:Lcom/peel/control/RoomControl;

    invoke-virtual {v2, v0}, Lcom/peel/control/RoomControl;->a(Lcom/peel/control/a;)V

    .line 710
    iget-object v2, p0, Lcom/peel/h/a/j;->a:Lcom/peel/h/a/i;

    iget-object v2, v2, Lcom/peel/h/a/i;->b:Lcom/peel/h/a/a;

    invoke-static {v2}, Lcom/peel/h/a/a;->p(Lcom/peel/h/a/a;)Lcom/peel/control/h;

    move-result-object v2

    new-array v3, v12, [Ljava/lang/Integer;

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v3, v10

    invoke-virtual {v0, v2, v11, v3}, Lcom/peel/control/a;->a(Lcom/peel/control/h;Ljava/lang/String;[Ljava/lang/Integer;)Z

    .line 712
    iget-object v0, p0, Lcom/peel/h/a/j;->a:Lcom/peel/h/a/i;

    iget-object v0, v0, Lcom/peel/h/a/i;->b:Lcom/peel/h/a/a;

    invoke-virtual {v0}, Lcom/peel/h/a/a;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/ae;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/peel/social/w;->f(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 713
    new-instance v0, Lcom/peel/backup/c;

    iget-object v1, p0, Lcom/peel/h/a/j;->a:Lcom/peel/h/a/i;

    iget-object v1, v1, Lcom/peel/h/a/i;->b:Lcom/peel/h/a/a;

    invoke-virtual {v1}, Lcom/peel/h/a/a;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/peel/backup/c;-><init>(Landroid/content/Context;)V

    .line 714
    iget-object v1, p0, Lcom/peel/h/a/j;->a:Lcom/peel/h/a/i;

    iget-object v1, v1, Lcom/peel/h/a/i;->a:Lcom/peel/control/RoomControl;

    iget-object v2, p0, Lcom/peel/h/a/j;->a:Lcom/peel/h/a/i;

    iget-object v2, v2, Lcom/peel/h/a/i;->b:Lcom/peel/h/a/a;

    iget-object v2, v2, Lcom/peel/h/a/a;->c:Landroid/os/Bundle;

    const-string/jumbo v3, "providername"

    invoke-virtual {v2, v3, v11}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/peel/h/a/j;->a:Lcom/peel/h/a/i;

    iget-object v3, v3, Lcom/peel/h/a/i;->b:Lcom/peel/h/a/a;

    invoke-static {v3}, Lcom/peel/h/a/a;->q(Lcom/peel/h/a/a;)Lcom/peel/control/a;

    move-result-object v3

    iget-object v4, p0, Lcom/peel/h/a/j;->a:Lcom/peel/h/a/i;

    iget-object v4, v4, Lcom/peel/h/a/i;->b:Lcom/peel/h/a/a;

    invoke-static {v4}, Lcom/peel/h/a/a;->p(Lcom/peel/h/a/a;)Lcom/peel/control/h;

    move-result-object v4

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/peel/backup/c;->a(Lcom/peel/control/RoomControl;Ljava/lang/String;Lcom/peel/control/a;Lcom/peel/control/h;)V

    .line 716
    :cond_4
    iget-object v0, p0, Lcom/peel/h/a/j;->a:Lcom/peel/h/a/i;

    iget-object v0, v0, Lcom/peel/h/a/i;->b:Lcom/peel/h/a/a;

    invoke-static {v0, v10}, Lcom/peel/h/a/a;->a(Lcom/peel/h/a/a;Z)Z

    .line 717
    invoke-static {}, Lcom/peel/h/a/a;->c()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/h/a/j;->a:Lcom/peel/h/a/i;

    iget-object v1, v1, Lcom/peel/h/a/i;->b:Lcom/peel/h/a/a;

    invoke-virtual {v1}, Lcom/peel/h/a/a;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/peel/d/e;->a(Ljava/lang/String;Landroid/support/v4/app/ae;)V

    goto/16 :goto_1

    .line 720
    :cond_5
    invoke-static {}, Lcom/peel/h/a/a;->c()Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "\n #### control_only_mode: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/peel/h/a/j;->a:Lcom/peel/h/a/i;

    iget-object v3, v3, Lcom/peel/h/a/i;->b:Lcom/peel/h/a/a;

    invoke-static {v3}, Lcom/peel/h/a/a;->z(Lcom/peel/h/a/a;)Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " -- device type: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/peel/h/a/j;->a:Lcom/peel/h/a/i;

    iget-object v3, v3, Lcom/peel/h/a/i;->b:Lcom/peel/h/a/a;

    invoke-static {v3}, Lcom/peel/h/a/a;->d(Lcom/peel/h/a/a;)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 723
    iget-object v0, p0, Lcom/peel/h/a/j;->a:Lcom/peel/h/a/i;

    iget-object v0, v0, Lcom/peel/h/a/i;->b:Lcom/peel/h/a/a;

    invoke-static {v0}, Lcom/peel/h/a/a;->z(Lcom/peel/h/a/a;)Z

    move-result v0

    if-nez v0, :cond_a

    iget-object v0, p0, Lcom/peel/h/a/j;->a:Lcom/peel/h/a/i;

    iget-object v0, v0, Lcom/peel/h/a/i;->b:Lcom/peel/h/a/a;

    invoke-static {v0}, Lcom/peel/h/a/a;->d(Lcom/peel/h/a/a;)I

    move-result v0

    if-eq v0, v12, :cond_6

    iget-object v0, p0, Lcom/peel/h/a/j;->a:Lcom/peel/h/a/i;

    iget-object v0, v0, Lcom/peel/h/a/i;->b:Lcom/peel/h/a/a;

    invoke-static {v0}, Lcom/peel/h/a/a;->d(Lcom/peel/h/a/a;)I

    move-result v0

    const/16 v2, 0x14

    if-ne v0, v2, :cond_a

    .line 725
    :cond_6
    iget-object v0, p0, Lcom/peel/h/a/j;->a:Lcom/peel/h/a/i;

    iget-object v0, v0, Lcom/peel/h/a/i;->a:Lcom/peel/control/RoomControl;

    invoke-virtual {v0}, Lcom/peel/control/RoomControl;->c()[Lcom/peel/control/a;

    move-result-object v2

    array-length v3, v2

    move v0, v1

    :goto_2
    if-ge v0, v3, :cond_7

    aget-object v4, v2, v0

    .line 726
    invoke-virtual {v4, v10}, Lcom/peel/control/a;->a(I)Lcom/peel/control/h;

    move-result-object v5

    .line 727
    if-eqz v5, :cond_9

    invoke-virtual {v5}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v6

    invoke-virtual {v6}, Lcom/peel/data/g;->d()I

    move-result v6

    if-ne v6, v10, :cond_9

    .line 731
    iget-object v0, p0, Lcom/peel/h/a/j;->a:Lcom/peel/h/a/i;

    iget-object v0, v0, Lcom/peel/h/a/i;->b:Lcom/peel/h/a/a;

    invoke-static {v0}, Lcom/peel/h/a/a;->s(Lcom/peel/h/a/a;)Lcom/peel/control/h;

    move-result-object v0

    invoke-virtual {v4, v0}, Lcom/peel/control/a;->a(Lcom/peel/control/h;)Ljava/lang/String;

    move-result-object v0

    .line 734
    invoke-virtual {v4, v5}, Lcom/peel/control/a;->b(Lcom/peel/control/h;)[Ljava/lang/Integer;

    move-result-object v2

    .line 736
    array-length v2, v2

    if-ne v2, v10, :cond_8

    .line 738
    iget-object v2, p0, Lcom/peel/h/a/j;->a:Lcom/peel/h/a/i;

    iget-object v2, v2, Lcom/peel/h/a/i;->b:Lcom/peel/h/a/a;

    invoke-static {v2}, Lcom/peel/h/a/a;->s(Lcom/peel/h/a/a;)Lcom/peel/control/h;

    move-result-object v2

    invoke-virtual {v4, v2, v0, v11}, Lcom/peel/control/a;->b(Lcom/peel/control/h;Ljava/lang/String;[Ljava/lang/Integer;)V

    .line 740
    invoke-static {}, Lcom/peel/h/a/a;->c()Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "\nupdate watch tv activity: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v4}, Lcom/peel/control/a;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " with tv device: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v5}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v3

    invoke-virtual {v3}, Lcom/peel/data/g;->f()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " with NULL modes"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 749
    :goto_3
    iget-object v0, p0, Lcom/peel/h/a/j;->a:Lcom/peel/h/a/i;

    iget-object v0, v0, Lcom/peel/h/a/i;->b:Lcom/peel/h/a/a;

    invoke-static {v0}, Lcom/peel/h/a/a;->p(Lcom/peel/h/a/a;)Lcom/peel/control/h;

    move-result-object v0

    new-array v2, v10, [Ljava/lang/Integer;

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v1

    invoke-virtual {v4, v0, v11, v2}, Lcom/peel/control/a;->a(Lcom/peel/control/h;Ljava/lang/String;[Ljava/lang/Integer;)Z

    .line 751
    iget-object v0, p0, Lcom/peel/h/a/j;->a:Lcom/peel/h/a/i;

    iget-object v0, v0, Lcom/peel/h/a/i;->b:Lcom/peel/h/a/a;

    invoke-static {v0, v4}, Lcom/peel/h/a/a;->a(Lcom/peel/h/a/a;Lcom/peel/control/a;)Lcom/peel/control/a;

    .line 880
    :cond_7
    :goto_4
    iget-object v0, p0, Lcom/peel/h/a/j;->a:Lcom/peel/h/a/i;

    iget-object v0, v0, Lcom/peel/h/a/i;->b:Lcom/peel/h/a/a;

    iget-object v1, p0, Lcom/peel/h/a/j;->a:Lcom/peel/h/a/i;

    iget-object v1, v1, Lcom/peel/h/a/i;->a:Lcom/peel/control/RoomControl;

    invoke-static {v0, v1}, Lcom/peel/h/a/a;->d(Lcom/peel/h/a/a;Lcom/peel/control/RoomControl;)V

    .line 881
    iget-object v0, p0, Lcom/peel/h/a/j;->a:Lcom/peel/h/a/i;

    iget-object v0, v0, Lcom/peel/h/a/i;->b:Lcom/peel/h/a/a;

    iget-object v1, p0, Lcom/peel/h/a/j;->a:Lcom/peel/h/a/i;

    iget-object v1, v1, Lcom/peel/h/a/i;->b:Lcom/peel/h/a/a;

    invoke-static {v1}, Lcom/peel/h/a/a;->q(Lcom/peel/h/a/a;)Lcom/peel/control/a;

    move-result-object v1

    iget-object v2, p0, Lcom/peel/h/a/j;->a:Lcom/peel/h/a/i;

    iget-object v2, v2, Lcom/peel/h/a/i;->a:Lcom/peel/control/RoomControl;

    invoke-static {v0, v1, v2}, Lcom/peel/h/a/a;->a(Lcom/peel/h/a/a;Lcom/peel/control/a;Lcom/peel/control/RoomControl;)V

    .line 883
    iget-object v0, p0, Lcom/peel/h/a/j;->a:Lcom/peel/h/a/i;

    iget-object v0, v0, Lcom/peel/h/a/i;->b:Lcom/peel/h/a/a;

    invoke-virtual {v0}, Lcom/peel/h/a/a;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/ae;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/peel/social/w;->f(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 884
    new-instance v0, Lcom/peel/backup/c;

    iget-object v1, p0, Lcom/peel/h/a/j;->a:Lcom/peel/h/a/i;

    iget-object v1, v1, Lcom/peel/h/a/i;->b:Lcom/peel/h/a/a;

    invoke-virtual {v1}, Lcom/peel/h/a/a;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/peel/backup/c;-><init>(Landroid/content/Context;)V

    .line 885
    iget-object v1, p0, Lcom/peel/h/a/j;->a:Lcom/peel/h/a/i;

    iget-object v1, v1, Lcom/peel/h/a/i;->a:Lcom/peel/control/RoomControl;

    iget-object v2, p0, Lcom/peel/h/a/j;->a:Lcom/peel/h/a/i;

    iget-object v2, v2, Lcom/peel/h/a/i;->b:Lcom/peel/h/a/a;

    iget-object v2, v2, Lcom/peel/h/a/a;->c:Landroid/os/Bundle;

    const-string/jumbo v3, "providername"

    invoke-virtual {v2, v3, v11}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/peel/h/a/j;->a:Lcom/peel/h/a/i;

    iget-object v3, v3, Lcom/peel/h/a/i;->b:Lcom/peel/h/a/a;

    invoke-static {v3}, Lcom/peel/h/a/a;->q(Lcom/peel/h/a/a;)Lcom/peel/control/a;

    move-result-object v3

    iget-object v4, p0, Lcom/peel/h/a/j;->a:Lcom/peel/h/a/i;

    iget-object v4, v4, Lcom/peel/h/a/i;->b:Lcom/peel/h/a/a;

    invoke-static {v4}, Lcom/peel/h/a/a;->p(Lcom/peel/h/a/a;)Lcom/peel/control/h;

    move-result-object v4

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/peel/backup/c;->a(Lcom/peel/control/RoomControl;Ljava/lang/String;Lcom/peel/control/a;Lcom/peel/control/h;)V

    goto/16 :goto_1

    .line 743
    :cond_8
    iget-object v2, p0, Lcom/peel/h/a/j;->a:Lcom/peel/h/a/i;

    iget-object v2, v2, Lcom/peel/h/a/i;->b:Lcom/peel/h/a/a;

    invoke-static {v2}, Lcom/peel/h/a/a;->s(Lcom/peel/h/a/a;)Lcom/peel/control/h;

    move-result-object v2

    new-array v3, v10, [Ljava/lang/Integer;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v3, v1

    invoke-virtual {v4, v2, v0, v3}, Lcom/peel/control/a;->b(Lcom/peel/control/h;Ljava/lang/String;[Ljava/lang/Integer;)V

    .line 745
    invoke-static {}, Lcom/peel/h/a/a;->c()Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "\nupdate watch tv activity: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v4}, Lcom/peel/control/a;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " with audio device: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v5}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v3

    invoke-virtual {v3}, Lcom/peel/data/g;->f()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " with ONLY audio mode"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_3

    .line 725
    :cond_9
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_2

    .line 758
    :cond_a
    iget-object v0, p0, Lcom/peel/h/a/j;->a:Lcom/peel/h/a/i;

    iget-object v0, v0, Lcom/peel/h/a/i;->b:Lcom/peel/h/a/a;

    iget-object v2, p0, Lcom/peel/h/a/j;->a:Lcom/peel/h/a/i;

    iget-object v2, v2, Lcom/peel/h/a/i;->b:Lcom/peel/h/a/a;

    invoke-static {v2}, Lcom/peel/h/a/a;->l(Lcom/peel/h/a/a;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/peel/control/a;->a(Ljava/lang/String;)Lcom/peel/control/a;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/peel/h/a/a;->a(Lcom/peel/h/a/a;Lcom/peel/control/a;)Lcom/peel/control/a;

    .line 759
    iget-object v0, p0, Lcom/peel/h/a/j;->a:Lcom/peel/h/a/i;

    iget-object v0, v0, Lcom/peel/h/a/i;->a:Lcom/peel/control/RoomControl;

    iget-object v2, p0, Lcom/peel/h/a/j;->a:Lcom/peel/h/a/i;

    iget-object v2, v2, Lcom/peel/h/a/i;->b:Lcom/peel/h/a/a;

    invoke-static {v2}, Lcom/peel/h/a/a;->q(Lcom/peel/h/a/a;)Lcom/peel/control/a;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/peel/control/RoomControl;->a(Lcom/peel/control/a;)V

    .line 761
    iget-object v0, p0, Lcom/peel/h/a/j;->a:Lcom/peel/h/a/i;

    iget-object v0, v0, Lcom/peel/h/a/i;->b:Lcom/peel/h/a/a;

    invoke-virtual {v0}, Lcom/peel/h/a/a;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    iget-object v2, p0, Lcom/peel/h/a/j;->a:Lcom/peel/h/a/i;

    iget-object v2, v2, Lcom/peel/h/a/i;->a:Lcom/peel/control/RoomControl;

    invoke-static {v0, v2}, Lcom/peel/util/bx;->a(Landroid/content/Context;Lcom/peel/control/RoomControl;)V

    .line 762
    invoke-static {}, Lcom/peel/h/a/a;->c()Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "\n\ndevice type: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/peel/h/a/j;->a:Lcom/peel/h/a/i;

    iget-object v3, v3, Lcom/peel/h/a/i;->b:Lcom/peel/h/a/a;

    invoke-static {v3}, Lcom/peel/h/a/a;->d(Lcom/peel/h/a/a;)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 763
    iget-object v0, p0, Lcom/peel/h/a/j;->a:Lcom/peel/h/a/i;

    iget-object v0, v0, Lcom/peel/h/a/i;->b:Lcom/peel/h/a/a;

    invoke-static {v0}, Lcom/peel/h/a/a;->d(Lcom/peel/h/a/a;)I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 815
    iget-object v0, p0, Lcom/peel/h/a/j;->a:Lcom/peel/h/a/i;

    iget-object v0, v0, Lcom/peel/h/a/i;->b:Lcom/peel/h/a/a;

    invoke-static {v0}, Lcom/peel/h/a/a;->q(Lcom/peel/h/a/a;)Lcom/peel/control/a;

    move-result-object v0

    iget-object v2, p0, Lcom/peel/h/a/j;->a:Lcom/peel/h/a/i;

    iget-object v2, v2, Lcom/peel/h/a/i;->b:Lcom/peel/h/a/a;

    invoke-static {v2}, Lcom/peel/h/a/a;->p(Lcom/peel/h/a/a;)Lcom/peel/control/h;

    move-result-object v2

    new-array v3, v10, [Ljava/lang/Integer;

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v1

    invoke-virtual {v0, v2, v11, v3}, Lcom/peel/control/a;->a(Lcom/peel/control/h;Ljava/lang/String;[Ljava/lang/Integer;)Z

    .line 817
    invoke-static {}, Lcom/peel/h/a/a;->c()Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "\n\nDefault: adding device: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/peel/h/a/j;->a:Lcom/peel/h/a/i;

    iget-object v3, v3, Lcom/peel/h/a/i;->b:Lcom/peel/h/a/a;

    invoke-static {v3}, Lcom/peel/h/a/a;->p(Lcom/peel/h/a/a;)Lcom/peel/control/h;

    move-result-object v3

    invoke-virtual {v3}, Lcom/peel/control/h;->f()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " to activity: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/peel/h/a/j;->a:Lcom/peel/h/a/i;

    iget-object v3, v3, Lcom/peel/h/a/i;->b:Lcom/peel/h/a/a;

    invoke-static {v3}, Lcom/peel/h/a/a;->q(Lcom/peel/h/a/a;)Lcom/peel/control/a;

    move-result-object v3

    invoke-virtual {v3}, Lcom/peel/control/a;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 821
    :goto_5
    iget-object v0, p0, Lcom/peel/h/a/j;->a:Lcom/peel/h/a/i;

    iget-object v0, v0, Lcom/peel/h/a/i;->b:Lcom/peel/h/a/a;

    invoke-static {v0}, Lcom/peel/h/a/a;->p(Lcom/peel/h/a/a;)Lcom/peel/control/h;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/data/g;->d()I

    move-result v0

    if-eq v0, v13, :cond_b

    iget-object v0, p0, Lcom/peel/h/a/j;->a:Lcom/peel/h/a/i;

    iget-object v0, v0, Lcom/peel/h/a/i;->b:Lcom/peel/h/a/a;

    invoke-static {v0}, Lcom/peel/h/a/a;->p(Lcom/peel/h/a/a;)Lcom/peel/control/h;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/data/g;->d()I

    move-result v0

    const/16 v2, 0x12

    if-eq v0, v2, :cond_b

    iget-object v0, p0, Lcom/peel/h/a/j;->a:Lcom/peel/h/a/i;

    iget-object v0, v0, Lcom/peel/h/a/i;->b:Lcom/peel/h/a/a;

    invoke-static {v0}, Lcom/peel/h/a/a;->p(Lcom/peel/h/a/a;)Lcom/peel/control/h;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/data/g;->d()I

    move-result v0

    if-eq v0, v10, :cond_b

    iget-object v0, p0, Lcom/peel/h/a/j;->a:Lcom/peel/h/a/i;

    iget-object v0, v0, Lcom/peel/h/a/i;->b:Lcom/peel/h/a/a;

    .line 822
    invoke-static {v0}, Lcom/peel/h/a/a;->p(Lcom/peel/h/a/a;)Lcom/peel/control/h;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/data/g;->d()I

    move-result v0

    const/16 v2, 0xa

    if-ne v0, v2, :cond_13

    .line 826
    :cond_b
    invoke-static {}, Lcom/peel/h/a/a;->c()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v2, "\nno audio mode for these devices or they are already set up... "

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 849
    :cond_c
    :goto_6
    iget-object v0, p0, Lcom/peel/h/a/j;->a:Lcom/peel/h/a/i;

    iget-object v0, v0, Lcom/peel/h/a/i;->b:Lcom/peel/h/a/a;

    invoke-static {v0}, Lcom/peel/h/a/a;->z(Lcom/peel/h/a/a;)Z

    move-result v0

    if-eqz v0, :cond_16

    .line 850
    invoke-static {}, Lcom/peel/h/a/a;->c()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v2, "\ncontrol only setup\n"

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 851
    iget-object v0, p0, Lcom/peel/h/a/j;->a:Lcom/peel/h/a/i;

    iget-object v0, v0, Lcom/peel/h/a/i;->b:Lcom/peel/h/a/a;

    invoke-static {v0}, Lcom/peel/h/a/a;->d(Lcom/peel/h/a/a;)I

    move-result v0

    if-eq v0, v10, :cond_d

    iget-object v0, p0, Lcom/peel/h/a/j;->a:Lcom/peel/h/a/i;

    iget-object v0, v0, Lcom/peel/h/a/i;->b:Lcom/peel/h/a/a;

    invoke-static {v0}, Lcom/peel/h/a/a;->d(Lcom/peel/h/a/a;)I

    move-result v0

    const/16 v2, 0xa

    if-ne v0, v2, :cond_16

    .line 853
    :cond_d
    invoke-static {}, Lcom/peel/h/a/a;->c()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v2, "\nif device is tv or projector, adding them as display for all"

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 854
    iget-object v0, p0, Lcom/peel/h/a/j;->a:Lcom/peel/h/a/i;

    iget-object v0, v0, Lcom/peel/h/a/i;->a:Lcom/peel/control/RoomControl;

    invoke-virtual {v0}, Lcom/peel/control/RoomControl;->c()[Lcom/peel/control/a;

    move-result-object v2

    array-length v3, v2

    move v0, v1

    :goto_7
    if-ge v0, v3, :cond_16

    aget-object v1, v2, v0

    .line 855
    invoke-virtual {v1}, Lcom/peel/control/a;->b()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/peel/h/a/j;->a:Lcom/peel/h/a/i;

    iget-object v5, v5, Lcom/peel/h/a/i;->b:Lcom/peel/h/a/a;

    invoke-static {v5}, Lcom/peel/h/a/a;->q(Lcom/peel/h/a/a;)Lcom/peel/control/a;

    move-result-object v5

    invoke-virtual {v5}, Lcom/peel/control/a;->b()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_15

    .line 854
    :cond_e
    :goto_8
    add-int/lit8 v0, v0, 0x1

    goto :goto_7

    .line 765
    :sswitch_0
    iget-object v0, p0, Lcom/peel/h/a/j;->a:Lcom/peel/h/a/i;

    iget-object v0, v0, Lcom/peel/h/a/i;->b:Lcom/peel/h/a/a;

    invoke-static {v0}, Lcom/peel/h/a/a;->q(Lcom/peel/h/a/a;)Lcom/peel/control/a;

    move-result-object v0

    iget-object v2, p0, Lcom/peel/h/a/j;->a:Lcom/peel/h/a/i;

    iget-object v2, v2, Lcom/peel/h/a/i;->b:Lcom/peel/h/a/a;

    invoke-static {v2}, Lcom/peel/h/a/a;->p(Lcom/peel/h/a/a;)Lcom/peel/control/h;

    move-result-object v2

    new-array v3, v12, [Ljava/lang/Integer;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v1

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v10

    invoke-virtual {v0, v2, v11, v3}, Lcom/peel/control/a;->a(Lcom/peel/control/h;Ljava/lang/String;[Ljava/lang/Integer;)Z

    .line 767
    invoke-static {}, Lcom/peel/h/a/a;->c()Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "\n\nadding tv to: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/peel/h/a/j;->a:Lcom/peel/h/a/i;

    iget-object v3, v3, Lcom/peel/h/a/i;->b:Lcom/peel/h/a/a;

    invoke-static {v3}, Lcom/peel/h/a/a;->q(Lcom/peel/h/a/a;)Lcom/peel/control/a;

    move-result-object v3

    invoke-virtual {v3}, Lcom/peel/control/a;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_5

    .line 771
    :sswitch_1
    iget-object v0, p0, Lcom/peel/h/a/j;->a:Lcom/peel/h/a/i;

    iget-object v0, v0, Lcom/peel/h/a/i;->a:Lcom/peel/control/RoomControl;

    invoke-virtual {v0}, Lcom/peel/control/RoomControl;->c()[Lcom/peel/control/a;

    move-result-object v2

    array-length v3, v2

    move v0, v1

    :goto_9
    if-ge v0, v3, :cond_12

    aget-object v4, v2, v0

    .line 773
    invoke-static {}, Lcom/peel/h/a/a;->c()Ljava/lang/String;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "\nactivity: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v4}, Lcom/peel/control/a;->a()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 774
    invoke-virtual {v4}, Lcom/peel/control/a;->b()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/peel/h/a/j;->a:Lcom/peel/h/a/i;

    iget-object v6, v6, Lcom/peel/h/a/i;->b:Lcom/peel/h/a/a;

    invoke-static {v6}, Lcom/peel/h/a/a;->q(Lcom/peel/h/a/a;)Lcom/peel/control/a;

    move-result-object v6

    invoke-virtual {v6}, Lcom/peel/control/a;->b()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_10

    .line 776
    invoke-static {}, Lcom/peel/h/a/a;->c()Ljava/lang/String;

    move-result-object v4

    const-string/jumbo v5, "\nsame activity, continue..."

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 771
    :cond_f
    :goto_a
    add-int/lit8 v0, v0, 0x1

    goto :goto_9

    .line 779
    :cond_10
    invoke-virtual {v4, v1}, Lcom/peel/control/a;->a(I)Lcom/peel/control/h;

    move-result-object v5

    .line 780
    if-eqz v5, :cond_f

    .line 784
    invoke-virtual {v5}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v6

    invoke-virtual {v6}, Lcom/peel/data/g;->d()I

    move-result v6

    if-eq v6, v13, :cond_f

    invoke-virtual {v5}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v6

    invoke-virtual {v6}, Lcom/peel/data/g;->d()I

    move-result v6

    const/16 v7, 0x12

    if-eq v6, v7, :cond_f

    .line 788
    invoke-virtual {v4, v5}, Lcom/peel/control/a;->b(Lcom/peel/control/h;)[Ljava/lang/Integer;

    move-result-object v6

    .line 790
    invoke-static {}, Lcom/peel/h/a/a;->c()Ljava/lang/String;

    move-result-object v7

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "\nmodes length: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    array-length v9, v6

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 791
    array-length v6, v6

    if-ne v6, v10, :cond_11

    .line 794
    invoke-virtual {v4, v5, v11, v11}, Lcom/peel/control/a;->b(Lcom/peel/control/h;Ljava/lang/String;[Ljava/lang/Integer;)V

    .line 796
    invoke-static {}, Lcom/peel/h/a/a;->c()Ljava/lang/String;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "\nupdate activity: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v4}, Lcom/peel/control/a;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v7, " with audio device: "

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v5}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v5

    invoke-virtual {v5}, Lcom/peel/data/g;->f()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, " with NULL modes"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v6, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_a

    .line 800
    :cond_11
    new-array v6, v10, [Ljava/lang/Integer;

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v1

    invoke-virtual {v4, v5, v11, v6}, Lcom/peel/control/a;->b(Lcom/peel/control/h;Ljava/lang/String;[Ljava/lang/Integer;)V

    .line 802
    invoke-static {}, Lcom/peel/h/a/a;->c()Ljava/lang/String;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "\nupdate activity: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v4}, Lcom/peel/control/a;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v7, " with audio device: "

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v5}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v5

    invoke-virtual {v5}, Lcom/peel/data/g;->f()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, " with ONLY Control modes"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v6, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_a

    .line 806
    :cond_12
    invoke-static {}, Lcom/peel/h/a/a;->c()Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "\n\nactivity: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/peel/h/a/j;->a:Lcom/peel/h/a/i;

    iget-object v3, v3, Lcom/peel/h/a/i;->b:Lcom/peel/h/a/a;

    invoke-static {v3}, Lcom/peel/h/a/a;->q(Lcom/peel/h/a/a;)Lcom/peel/control/a;

    move-result-object v3

    invoke-virtual {v3}, Lcom/peel/control/a;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " add device: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/peel/h/a/j;->a:Lcom/peel/h/a/i;

    iget-object v3, v3, Lcom/peel/h/a/i;->b:Lcom/peel/h/a/a;

    invoke-static {v3}, Lcom/peel/h/a/a;->p(Lcom/peel/h/a/a;)Lcom/peel/control/h;

    move-result-object v3

    invoke-virtual {v3}, Lcom/peel/control/h;->f()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " with BOTH modes"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 807
    iget-object v0, p0, Lcom/peel/h/a/j;->a:Lcom/peel/h/a/i;

    iget-object v0, v0, Lcom/peel/h/a/i;->b:Lcom/peel/h/a/a;

    invoke-static {v0}, Lcom/peel/h/a/a;->q(Lcom/peel/h/a/a;)Lcom/peel/control/a;

    move-result-object v0

    iget-object v2, p0, Lcom/peel/h/a/j;->a:Lcom/peel/h/a/i;

    iget-object v2, v2, Lcom/peel/h/a/i;->b:Lcom/peel/h/a/a;

    invoke-static {v2}, Lcom/peel/h/a/a;->p(Lcom/peel/h/a/a;)Lcom/peel/control/h;

    move-result-object v2

    new-array v3, v12, [Ljava/lang/Integer;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v1

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v10

    invoke-virtual {v0, v2, v11, v3}, Lcom/peel/control/a;->a(Lcom/peel/control/h;Ljava/lang/String;[Ljava/lang/Integer;)Z

    goto/16 :goto_5

    .line 810
    :sswitch_2
    iget-object v0, p0, Lcom/peel/h/a/j;->a:Lcom/peel/h/a/i;

    iget-object v0, v0, Lcom/peel/h/a/i;->b:Lcom/peel/h/a/a;

    invoke-static {v0}, Lcom/peel/h/a/a;->q(Lcom/peel/h/a/a;)Lcom/peel/control/a;

    move-result-object v0

    iget-object v2, p0, Lcom/peel/h/a/j;->a:Lcom/peel/h/a/i;

    iget-object v2, v2, Lcom/peel/h/a/i;->b:Lcom/peel/h/a/a;

    invoke-static {v2}, Lcom/peel/h/a/a;->p(Lcom/peel/h/a/a;)Lcom/peel/control/h;

    move-result-object v2

    new-array v3, v10, [Ljava/lang/Integer;

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v1

    invoke-virtual {v0, v2, v11, v3}, Lcom/peel/control/a;->a(Lcom/peel/control/h;Ljava/lang/String;[Ljava/lang/Integer;)Z

    .line 812
    invoke-static {}, Lcom/peel/h/a/a;->c()Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "\n\nadding projector to: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/peel/h/a/j;->a:Lcom/peel/h/a/i;

    iget-object v3, v3, Lcom/peel/h/a/i;->b:Lcom/peel/h/a/a;

    invoke-static {v3}, Lcom/peel/h/a/a;->q(Lcom/peel/h/a/a;)Lcom/peel/control/a;

    move-result-object v3

    invoke-virtual {v3}, Lcom/peel/control/a;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_5

    .line 828
    :cond_13
    iget-object v0, p0, Lcom/peel/h/a/j;->a:Lcom/peel/h/a/i;

    iget-object v0, v0, Lcom/peel/h/a/i;->b:Lcom/peel/h/a/a;

    invoke-static {v0}, Lcom/peel/h/a/a;->r(Lcom/peel/h/a/a;)Lcom/peel/control/h;

    move-result-object v0

    if-eqz v0, :cond_14

    .line 829
    invoke-static {}, Lcom/peel/h/a/a;->c()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v2, "\nif stereo is not null"

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 830
    iget-object v0, p0, Lcom/peel/h/a/j;->a:Lcom/peel/h/a/i;

    iget-object v0, v0, Lcom/peel/h/a/i;->b:Lcom/peel/h/a/a;

    invoke-static {v0}, Lcom/peel/h/a/a;->q(Lcom/peel/h/a/a;)Lcom/peel/control/a;

    move-result-object v0

    iget-object v2, p0, Lcom/peel/h/a/j;->a:Lcom/peel/h/a/i;

    iget-object v2, v2, Lcom/peel/h/a/i;->b:Lcom/peel/h/a/a;

    invoke-static {v2}, Lcom/peel/h/a/a;->r(Lcom/peel/h/a/a;)Lcom/peel/control/h;

    move-result-object v2

    new-array v3, v10, [Ljava/lang/Integer;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v1

    invoke-virtual {v0, v2, v11, v3}, Lcom/peel/control/a;->a(Lcom/peel/control/h;Ljava/lang/String;[Ljava/lang/Integer;)Z

    .line 832
    invoke-static {}, Lcom/peel/h/a/a;->c()Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "\n\nadding stereo: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/peel/h/a/j;->a:Lcom/peel/h/a/i;

    iget-object v3, v3, Lcom/peel/h/a/i;->b:Lcom/peel/h/a/a;

    invoke-static {v3}, Lcom/peel/h/a/a;->r(Lcom/peel/h/a/a;)Lcom/peel/control/h;

    move-result-object v3

    invoke-virtual {v3}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v3

    invoke-virtual {v3}, Lcom/peel/data/g;->f()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " to activity: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/peel/h/a/j;->a:Lcom/peel/h/a/i;

    iget-object v3, v3, Lcom/peel/h/a/i;->b:Lcom/peel/h/a/a;

    invoke-static {v3}, Lcom/peel/h/a/a;->q(Lcom/peel/h/a/a;)Lcom/peel/control/a;

    move-result-object v3

    invoke-virtual {v3}, Lcom/peel/control/a;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 834
    iget-object v0, p0, Lcom/peel/h/a/j;->a:Lcom/peel/h/a/i;

    iget-object v0, v0, Lcom/peel/h/a/i;->b:Lcom/peel/h/a/a;

    invoke-static {v0}, Lcom/peel/h/a/a;->s(Lcom/peel/h/a/a;)Lcom/peel/control/h;

    move-result-object v0

    if-eqz v0, :cond_c

    .line 835
    iget-object v0, p0, Lcom/peel/h/a/j;->a:Lcom/peel/h/a/i;

    iget-object v0, v0, Lcom/peel/h/a/i;->b:Lcom/peel/h/a/a;

    invoke-static {v0}, Lcom/peel/h/a/a;->q(Lcom/peel/h/a/a;)Lcom/peel/control/a;

    move-result-object v0

    iget-object v2, p0, Lcom/peel/h/a/j;->a:Lcom/peel/h/a/i;

    iget-object v2, v2, Lcom/peel/h/a/i;->b:Lcom/peel/h/a/a;

    invoke-static {v2}, Lcom/peel/h/a/a;->s(Lcom/peel/h/a/a;)Lcom/peel/control/h;

    move-result-object v2

    invoke-virtual {v0, v2, v11, v11}, Lcom/peel/control/a;->a(Lcom/peel/control/h;Ljava/lang/String;[Ljava/lang/Integer;)Z

    .line 837
    invoke-static {}, Lcom/peel/h/a/a;->c()Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "\n\nadding tv: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/peel/h/a/j;->a:Lcom/peel/h/a/i;

    iget-object v3, v3, Lcom/peel/h/a/i;->b:Lcom/peel/h/a/a;

    invoke-static {v3}, Lcom/peel/h/a/a;->s(Lcom/peel/h/a/a;)Lcom/peel/control/h;

    move-result-object v3

    invoke-virtual {v3}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v3

    invoke-virtual {v3}, Lcom/peel/data/g;->f()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " to activity: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/peel/h/a/j;->a:Lcom/peel/h/a/i;

    iget-object v3, v3, Lcom/peel/h/a/i;->b:Lcom/peel/h/a/a;

    invoke-static {v3}, Lcom/peel/h/a/a;->q(Lcom/peel/h/a/a;)Lcom/peel/control/a;

    move-result-object v3

    invoke-virtual {v3}, Lcom/peel/control/a;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_6

    .line 839
    :cond_14
    iget-object v0, p0, Lcom/peel/h/a/j;->a:Lcom/peel/h/a/i;

    iget-object v0, v0, Lcom/peel/h/a/i;->b:Lcom/peel/h/a/a;

    invoke-static {v0}, Lcom/peel/h/a/a;->s(Lcom/peel/h/a/a;)Lcom/peel/control/h;

    move-result-object v0

    if-eqz v0, :cond_c

    .line 841
    invoke-static {}, Lcom/peel/h/a/a;->c()Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "\nif tv is not null... add tv: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/peel/h/a/j;->a:Lcom/peel/h/a/i;

    iget-object v3, v3, Lcom/peel/h/a/i;->b:Lcom/peel/h/a/a;

    invoke-static {v3}, Lcom/peel/h/a/a;->s(Lcom/peel/h/a/a;)Lcom/peel/control/h;

    move-result-object v3

    invoke-virtual {v3}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v3

    invoke-virtual {v3}, Lcom/peel/data/g;->f()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " to activity: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/peel/h/a/j;->a:Lcom/peel/h/a/i;

    iget-object v3, v3, Lcom/peel/h/a/i;->b:Lcom/peel/h/a/a;

    invoke-static {v3}, Lcom/peel/h/a/a;->q(Lcom/peel/h/a/a;)Lcom/peel/control/a;

    move-result-object v3

    invoke-virtual {v3}, Lcom/peel/control/a;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 842
    iget-object v0, p0, Lcom/peel/h/a/j;->a:Lcom/peel/h/a/i;

    iget-object v0, v0, Lcom/peel/h/a/i;->b:Lcom/peel/h/a/a;

    invoke-static {v0}, Lcom/peel/h/a/a;->q(Lcom/peel/h/a/a;)Lcom/peel/control/a;

    move-result-object v0

    iget-object v2, p0, Lcom/peel/h/a/j;->a:Lcom/peel/h/a/i;

    iget-object v2, v2, Lcom/peel/h/a/i;->b:Lcom/peel/h/a/a;

    invoke-static {v2}, Lcom/peel/h/a/a;->s(Lcom/peel/h/a/a;)Lcom/peel/control/h;

    move-result-object v2

    new-array v3, v10, [Ljava/lang/Integer;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v1

    invoke-virtual {v0, v2, v11, v3}, Lcom/peel/control/a;->a(Lcom/peel/control/h;Ljava/lang/String;[Ljava/lang/Integer;)Z

    goto/16 :goto_6

    .line 857
    :cond_15
    invoke-virtual {v1, v10}, Lcom/peel/control/a;->a(I)Lcom/peel/control/h;

    move-result-object v4

    .line 858
    if-eqz v4, :cond_e

    .line 867
    invoke-virtual {v4}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v5

    invoke-virtual {v5}, Lcom/peel/data/g;->d()I

    move-result v5

    if-eq v5, v13, :cond_e

    invoke-virtual {v4}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v4

    invoke-virtual {v4}, Lcom/peel/data/g;->d()I

    move-result v4

    const/16 v5, 0x12

    if-eq v4, v5, :cond_e

    .line 870
    iget-object v4, p0, Lcom/peel/h/a/j;->a:Lcom/peel/h/a/i;

    iget-object v4, v4, Lcom/peel/h/a/i;->b:Lcom/peel/h/a/a;

    invoke-static {v4}, Lcom/peel/h/a/a;->p(Lcom/peel/h/a/a;)Lcom/peel/control/h;

    move-result-object v4

    invoke-virtual {v1, v4, v11, v11}, Lcom/peel/control/a;->a(Lcom/peel/control/h;Ljava/lang/String;[Ljava/lang/Integer;)Z

    .line 872
    invoke-static {}, Lcom/peel/h/a/a;->c()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "\n\n\n adding device: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/peel/h/a/j;->a:Lcom/peel/h/a/i;

    iget-object v6, v6, Lcom/peel/h/a/i;->b:Lcom/peel/h/a/a;

    invoke-static {v6}, Lcom/peel/h/a/a;->p(Lcom/peel/h/a/a;)Lcom/peel/control/h;

    move-result-object v6

    invoke-virtual {v6}, Lcom/peel/control/h;->f()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, " to activity: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v4, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_8

    .line 877
    :cond_16
    invoke-static {}, Lcom/peel/h/a/a;->c()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "\n\nshow device setup success dialog\n\n"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_4

    .line 686
    :catch_0
    move-exception v0

    goto/16 :goto_0

    .line 763
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x5 -> :sswitch_1
        0xa -> :sswitch_2
    .end sparse-switch
.end method
