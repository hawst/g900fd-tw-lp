.class Lcom/peel/h/a/gy;
.super Lcom/peel/util/t;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/peel/util/t",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/peel/h/a/gr;


# direct methods
.method constructor <init>(Lcom/peel/h/a/gr;)V
    .locals 0

    .prologue
    .line 465
    iput-object p1, p0, Lcom/peel/h/a/gy;->a:Lcom/peel/h/a/gr;

    invoke-direct {p0}, Lcom/peel/util/t;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .prologue
    const/4 v1, 0x1

    .line 468
    iget-boolean v0, p0, Lcom/peel/h/a/gy;->i:Z

    if-eqz v0, :cond_1

    .line 469
    new-instance v2, Lcom/peel/backup/c;

    iget-object v0, p0, Lcom/peel/h/a/gy;->a:Lcom/peel/h/a/gr;

    invoke-virtual {v0}, Lcom/peel/h/a/gr;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/ae;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/peel/backup/c;-><init>(Landroid/content/Context;)V

    .line 470
    invoke-static {}, Lcom/peel/util/a/f;->a()Lcom/peel/util/a/f;

    move-result-object v3

    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v0}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    const/16 v4, 0xbf5

    const/16 v5, 0x7d5

    sget-object v6, Lcom/peel/social/e;->d:Ljava/lang/String;

    invoke-virtual {v3, v0, v4, v5, v6}, Lcom/peel/util/a/f;->a(IIILjava/lang/String;)V

    .line 471
    invoke-virtual {v2}, Lcom/peel/backup/c;->a()V

    .line 472
    iget-object v0, p0, Lcom/peel/h/a/gy;->a:Lcom/peel/h/a/gr;

    invoke-virtual {v0}, Lcom/peel/h/a/gr;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    new-instance v2, Lcom/peel/h/a/gz;

    invoke-direct {v2, p0}, Lcom/peel/h/a/gz;-><init>(Lcom/peel/h/a/gy;)V

    invoke-virtual {v0, v2}, Landroid/support/v4/app/ae;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 478
    iget-object v0, p0, Lcom/peel/h/a/gy;->a:Lcom/peel/h/a/gr;

    invoke-virtual {v0}, Lcom/peel/h/a/gr;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    iget-object v2, p0, Lcom/peel/h/a/gy;->k:Ljava/lang/String;

    invoke-static {v0, v2, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 482
    :goto_1
    return-void

    .line 470
    :cond_0
    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v0}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/data/at;->f()I

    move-result v0

    goto :goto_0

    .line 480
    :cond_1
    iget-object v0, p0, Lcom/peel/h/a/gy;->a:Lcom/peel/h/a/gr;

    invoke-virtual {v0}, Lcom/peel/h/a/gr;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    iget-object v2, p0, Lcom/peel/h/a/gy;->k:Ljava/lang/String;

    invoke-static {v0, v2, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_1
.end method
