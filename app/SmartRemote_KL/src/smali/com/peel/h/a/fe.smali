.class public Lcom/peel/h/a/fe;
.super Lcom/peel/d/u;


# instance fields
.field private e:Landroid/webkit/WebView;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Lcom/peel/d/u;-><init>()V

    return-void
.end method


# virtual methods
.method public Z()V
    .locals 6

    .prologue
    .line 52
    iget-object v0, p0, Lcom/peel/h/a/fe;->d:Lcom/peel/d/a;

    if-nez v0, :cond_0

    .line 53
    const-string/jumbo v0, ""

    .line 56
    iget-object v0, p0, Lcom/peel/h/a/fe;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "title"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 57
    iget-object v0, p0, Lcom/peel/h/a/fe;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "title"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 61
    :goto_0
    new-instance v0, Lcom/peel/d/a;

    sget-object v1, Lcom/peel/d/d;->b:Lcom/peel/d/d;

    sget-object v2, Lcom/peel/d/b;->a:Lcom/peel/d/b;

    sget-object v3, Lcom/peel/d/c;->b:Lcom/peel/d/c;

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, Lcom/peel/d/a;-><init>(Lcom/peel/d/d;Lcom/peel/d/b;Lcom/peel/d/c;Ljava/lang/String;Ljava/util/List;)V

    iput-object v0, p0, Lcom/peel/h/a/fe;->d:Lcom/peel/d/a;

    .line 64
    :cond_0
    iget-object v0, p0, Lcom/peel/h/a/fe;->b:Lcom/peel/d/i;

    iget-object v1, p0, Lcom/peel/h/a/fe;->d:Lcom/peel/d/a;

    invoke-virtual {v0, v1}, Lcom/peel/d/i;->a(Lcom/peel/d/a;)V

    .line 65
    return-void

    .line 59
    :cond_1
    sget v0, Lcom/peel/ui/ft;->privacy_policy:I

    invoke-virtual {p0, v0}, Lcom/peel/h/a/fe;->a(I)Ljava/lang/String;

    move-result-object v4

    goto :goto_0
.end method

.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3

    .prologue
    .line 23
    sget v0, Lcom/peel/ui/fq;->privacy_xml:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 24
    sget v0, Lcom/peel/ui/fp;->webView:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/webkit/WebView;

    iput-object v0, p0, Lcom/peel/h/a/fe;->e:Landroid/webkit/WebView;

    .line 25
    iget-object v0, p0, Lcom/peel/h/a/fe;->e:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V

    .line 26
    iget-object v0, p0, Lcom/peel/h/a/fe;->e:Landroid/webkit/WebView;

    new-instance v2, Landroid/webkit/WebViewClient;

    invoke-direct {v2}, Landroid/webkit/WebViewClient;-><init>()V

    invoke-virtual {v0, v2}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 27
    iget-object v0, p0, Lcom/peel/h/a/fe;->e:Landroid/webkit/WebView;

    new-instance v2, Landroid/webkit/WebChromeClient;

    invoke-direct {v2}, Landroid/webkit/WebChromeClient;-><init>()V

    invoke-virtual {v0, v2}, Landroid/webkit/WebView;->setWebChromeClient(Landroid/webkit/WebChromeClient;)V

    .line 28
    invoke-virtual {p0}, Lcom/peel/h/a/fe;->j()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/peel/h/a/fe;->c:Landroid/os/Bundle;

    invoke-virtual {p0}, Lcom/peel/h/a/fe;->j()Landroid/os/Bundle;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->putAll(Landroid/os/Bundle;)V

    .line 29
    :cond_0
    return-object v1
.end method

.method public h(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 40
    invoke-super {p0, p1}, Lcom/peel/d/u;->h(Landroid/os/Bundle;)V

    .line 41
    iget-object v0, p0, Lcom/peel/h/a/fe;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "url"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 42
    iget-object v0, p0, Lcom/peel/h/a/fe;->e:Landroid/webkit/WebView;

    iget-object v1, p0, Lcom/peel/h/a/fe;->c:Landroid/os/Bundle;

    const-string/jumbo v2, "url"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 46
    :goto_0
    invoke-virtual {p0}, Lcom/peel/h/a/fe;->Z()V

    .line 47
    return-void

    .line 44
    :cond_0
    iget-object v0, p0, Lcom/peel/h/a/fe;->e:Landroid/webkit/WebView;

    const-string/jumbo v1, "http://www.peel.com/privacy.html"

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public w()V
    .locals 0

    .prologue
    .line 34
    invoke-super {p0}, Lcom/peel/d/u;->w()V

    .line 35
    invoke-virtual {p0}, Lcom/peel/h/a/fe;->Z()V

    .line 36
    return-void
.end method
