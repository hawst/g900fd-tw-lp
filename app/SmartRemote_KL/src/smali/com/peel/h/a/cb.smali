.class Lcom/peel/h/a/cb;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic a:Landroid/widget/ListView;

.field final synthetic b:Lcom/peel/h/a/bz;


# direct methods
.method constructor <init>(Lcom/peel/h/a/bz;Landroid/widget/ListView;)V
    .locals 0

    .prologue
    .line 409
    iput-object p1, p0, Lcom/peel/h/a/cb;->b:Lcom/peel/h/a/bz;

    iput-object p2, p0, Lcom/peel/h/a/cb;->a:Landroid/widget/ListView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 2

    .prologue
    .line 413
    iget-object v0, p0, Lcom/peel/h/a/cb;->b:Lcom/peel/h/a/bz;

    invoke-virtual {v0}, Lcom/peel/h/a/bz;->s()Z

    move-result v0

    if-nez v0, :cond_0

    .line 419
    :goto_0
    return-void

    .line 414
    :cond_0
    iget-object v0, p0, Lcom/peel/h/a/cb;->b:Lcom/peel/h/a/bz;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/peel/h/a/bz;->b(Lcom/peel/h/a/bz;Ljava/lang/String;)Ljava/lang/String;

    .line 415
    iget-object v0, p0, Lcom/peel/h/a/cb;->b:Lcom/peel/h/a/bz;

    invoke-static {v0}, Lcom/peel/h/a/bz;->m(Lcom/peel/h/a/bz;)Landroid/widget/TextView;

    move-result-object v0

    sget v1, Lcom/peel/ui/ft;->do_not_switch:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 416
    iget-object v0, p0, Lcom/peel/h/a/cb;->a:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->clearChoices()V

    .line 417
    iget-object v0, p0, Lcom/peel/h/a/cb;->b:Lcom/peel/h/a/bz;

    invoke-static {v0}, Lcom/peel/h/a/bz;->c(Lcom/peel/h/a/bz;)Lcom/peel/widget/ag;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/widget/ag;->dismiss()V

    .line 418
    iget-object v0, p0, Lcom/peel/h/a/cb;->b:Lcom/peel/h/a/bz;

    invoke-static {v0}, Lcom/peel/h/a/bz;->c(Lcom/peel/h/a/bz;)Lcom/peel/widget/ag;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/widget/ag;->f()Landroid/widget/Button;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_0
.end method
