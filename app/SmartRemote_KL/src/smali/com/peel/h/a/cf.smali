.class Lcom/peel/h/a/cf;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic a:Landroid/os/Bundle;

.field final synthetic b:Ljava/lang/StringBuilder;

.field final synthetic c:Lcom/peel/h/a/bz;


# direct methods
.method constructor <init>(Lcom/peel/h/a/bz;Landroid/os/Bundle;Ljava/lang/StringBuilder;)V
    .locals 0

    .prologue
    .line 728
    iput-object p1, p0, Lcom/peel/h/a/cf;->c:Lcom/peel/h/a/bz;

    iput-object p2, p0, Lcom/peel/h/a/cf;->a:Landroid/os/Bundle;

    iput-object p3, p0, Lcom/peel/h/a/cf;->b:Ljava/lang/StringBuilder;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3

    .prologue
    .line 731
    iget-object v0, p0, Lcom/peel/h/a/cf;->a:Landroid/os/Bundle;

    const-string/jumbo v1, "cycle_activities"

    iget-object v2, p0, Lcom/peel/h/a/cf;->b:Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 732
    iget-object v0, p0, Lcom/peel/h/a/cf;->a:Landroid/os/Bundle;

    const-string/jumbo v1, "room"

    iget-object v2, p0, Lcom/peel/h/a/cf;->c:Lcom/peel/h/a/bz;

    invoke-static {v2}, Lcom/peel/h/a/bz;->f(Lcom/peel/h/a/bz;)Lcom/peel/control/RoomControl;

    move-result-object v2

    invoke-virtual {v2}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v2

    invoke-virtual {v2}, Lcom/peel/data/at;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 733
    iget-object v0, p0, Lcom/peel/h/a/cf;->a:Landroid/os/Bundle;

    const-string/jumbo v1, "back_to_clazz"

    iget-object v2, p0, Lcom/peel/h/a/cf;->c:Lcom/peel/h/a/bz;

    invoke-static {v2}, Lcom/peel/h/a/bz;->t(Lcom/peel/h/a/bz;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 734
    iget-object v0, p0, Lcom/peel/h/a/cf;->c:Lcom/peel/h/a/bz;

    invoke-virtual {v0}, Lcom/peel/h/a/bz;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    const-class v1, Lcom/peel/h/a/bz;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/peel/h/a/cf;->a:Landroid/os/Bundle;

    invoke-static {v0, v1, v2}, Lcom/peel/d/e;->b(Landroid/support/v4/app/ae;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 735
    return-void
.end method
