.class public Lcom/peel/h/a/hh;
.super Lcom/peel/d/u;


# instance fields
.field private aj:[Z

.field private ak:Lcom/peel/h/a/ho;

.field private e:Landroid/widget/TextView;

.field private f:Landroid/widget/TextView;

.field private g:Landroid/widget/ListView;

.field private h:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private i:[Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/peel/d/u;-><init>()V

    .line 189
    return-void
.end method

.method private S()V
    .locals 5

    .prologue
    .line 93
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 94
    const-string/jumbo v0, "remindertype"

    const-string/jumbo v2, "team"

    invoke-interface {v1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 95
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 97
    const/4 v0, 0x0

    :goto_0
    iget-object v3, p0, Lcom/peel/h/a/hh;->i:[Ljava/lang/String;

    array-length v3, v3

    if-ge v0, v3, :cond_1

    .line 98
    iget-object v3, p0, Lcom/peel/h/a/hh;->aj:[Z

    aget-boolean v3, v3, v0

    if-eqz v3, :cond_0

    .line 99
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/peel/h/a/hh;->i:[Ljava/lang/String;

    aget-object v4, v4, v0

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, ","

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 97
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 103
    :cond_1
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    if-lez v0, :cond_2

    .line 104
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->deleteCharAt(I)Ljava/lang/StringBuilder;

    .line 105
    const-string/jumbo v0, "teamids"

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v0, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 106
    const-class v0, Lcom/peel/h/a/hh;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v3, "remove reminder"

    new-instance v4, Lcom/peel/h/a/hm;

    invoke-direct {v4, p0, v1, v2}, Lcom/peel/h/a/hm;-><init>(Lcom/peel/h/a/hh;Ljava/util/Map;Ljava/lang/StringBuilder;)V

    invoke-static {v0, v3, v4}, Lcom/peel/util/i;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    .line 124
    :cond_2
    return-void
.end method

.method private T()V
    .locals 3

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 126
    sget-object v0, Lcom/peel/content/a;->f:Lcom/peel/content/user/User;

    invoke-virtual {v0}, Lcom/peel/content/user/User;->p()Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/h/a/hh;->h:Ljava/util/Map;

    .line 127
    sget-object v0, Lcom/peel/content/a;->f:Lcom/peel/content/user/User;

    invoke-virtual {v0}, Lcom/peel/content/user/User;->o()Z

    move-result v0

    if-nez v0, :cond_0

    .line 128
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/peel/h/a/hh;->i:[Ljava/lang/String;

    .line 129
    iget-object v0, p0, Lcom/peel/h/a/hh;->f:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 130
    iget-object v0, p0, Lcom/peel/h/a/hh;->g:Landroid/widget/ListView;

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setVisibility(I)V

    .line 131
    const/4 v0, -0x1

    invoke-direct {p0, v0}, Lcom/peel/h/a/hh;->b(I)V

    .line 140
    :goto_0
    return-void

    .line 133
    :cond_0
    iget-object v0, p0, Lcom/peel/h/a/hh;->f:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 134
    iget-object v0, p0, Lcom/peel/h/a/hh;->g:Landroid/widget/ListView;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setVisibility(I)V

    .line 135
    iget-object v0, p0, Lcom/peel/h/a/hh;->h:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/String;

    iput-object v0, p0, Lcom/peel/h/a/hh;->i:[Ljava/lang/String;

    .line 136
    iget-object v0, p0, Lcom/peel/h/a/hh;->h:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    new-array v0, v0, [Z

    iput-object v0, p0, Lcom/peel/h/a/hh;->aj:[Z

    .line 137
    iget-object v0, p0, Lcom/peel/h/a/hh;->h:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/h/a/hh;->i:[Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 138
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/peel/h/a/hh;->b(I)V

    goto :goto_0
.end method

.method static synthetic a(Lcom/peel/h/a/hh;)[Z
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/peel/h/a/hh;->aj:[Z

    return-object v0
.end method

.method static synthetic b(Lcom/peel/h/a/hh;)Lcom/peel/h/a/ho;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/peel/h/a/hh;->ak:Lcom/peel/h/a/ho;

    return-object v0
.end method

.method private b(I)V
    .locals 6

    .prologue
    .line 170
    const/4 v5, 0x0

    .line 171
    if-lez p1, :cond_0

    .line 172
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 173
    const/4 v0, 0x1

    if-ne p1, v0, :cond_1

    .line 174
    sget v0, Lcom/peel/ui/fp;->menu_edit:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 179
    :cond_0
    :goto_0
    new-instance v0, Lcom/peel/d/a;

    sget-object v1, Lcom/peel/d/d;->b:Lcom/peel/d/d;

    sget-object v2, Lcom/peel/d/b;->a:Lcom/peel/d/b;

    sget-object v3, Lcom/peel/d/c;->b:Lcom/peel/d/c;

    sget v4, Lcom/peel/ui/ft;->settings_followed_team:I

    invoke-virtual {p0, v4}, Lcom/peel/h/a/hh;->a(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct/range {v0 .. v5}, Lcom/peel/d/a;-><init>(Lcom/peel/d/d;Lcom/peel/d/b;Lcom/peel/d/c;Ljava/lang/String;Ljava/util/List;)V

    iput-object v0, p0, Lcom/peel/h/a/hh;->d:Lcom/peel/d/a;

    .line 180
    invoke-virtual {p0}, Lcom/peel/h/a/hh;->Z()V

    .line 181
    return-void

    .line 176
    :cond_1
    sget v0, Lcom/peel/ui/fp;->menu_delete:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method private c()V
    .locals 3

    .prologue
    .line 68
    const-class v0, Lcom/peel/h/a/hh;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "get team data"

    new-instance v2, Lcom/peel/h/a/hj;

    invoke-direct {v2, p0}, Lcom/peel/h/a/hj;-><init>(Lcom/peel/h/a/hh;)V

    invoke-static {v0, v1, v2}, Lcom/peel/util/i;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    .line 90
    return-void
.end method

.method static synthetic c(Lcom/peel/h/a/hh;)V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/peel/h/a/hh;->T()V

    return-void
.end method

.method static synthetic d(Lcom/peel/h/a/hh;)Landroid/widget/ListView;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/peel/h/a/hh;->g:Landroid/widget/ListView;

    return-object v0
.end method

.method static synthetic e(Lcom/peel/h/a/hh;)V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/peel/h/a/hh;->c()V

    return-void
.end method

.method static synthetic f(Lcom/peel/h/a/hh;)[Ljava/lang/String;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/peel/h/a/hh;->i:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic g(Lcom/peel/h/a/hh;)Ljava/util/Map;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/peel/h/a/hh;->h:Ljava/util/Map;

    return-object v0
.end method


# virtual methods
.method public Z()V
    .locals 2

    .prologue
    .line 252
    iget-object v0, p0, Lcom/peel/h/a/hh;->d:Lcom/peel/d/a;

    if-nez v0, :cond_0

    .line 257
    :goto_0
    return-void

    .line 255
    :cond_0
    iget-object v0, p0, Lcom/peel/h/a/hh;->b:Lcom/peel/d/i;

    iget-object v1, p0, Lcom/peel/h/a/hh;->d:Lcom/peel/d/a;

    invoke-virtual {v0, v1}, Lcom/peel/d/i;->a(Lcom/peel/d/a;)V

    goto :goto_0
.end method

.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3

    .prologue
    .line 143
    sget v0, Lcom/peel/ui/fq;->fav_cut_progam_list:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 144
    sget v0, Lcom/peel/ui/fp;->desc:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/peel/h/a/hh;->e:Landroid/widget/TextView;

    .line 145
    sget v0, Lcom/peel/ui/fp;->node_list:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/peel/h/a/hh;->g:Landroid/widget/ListView;

    .line 146
    sget v0, Lcom/peel/ui/fp;->empty_list_msg:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/peel/h/a/hh;->f:Landroid/widget/TextView;

    .line 147
    iget-object v0, p0, Lcom/peel/h/a/hh;->f:Landroid/widget/TextView;

    sget v2, Lcom/peel/ui/ft;->no_follow_team:I

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    .line 149
    return-object v1
.end method

.method public a(Landroid/view/MenuItem;)Z
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 155
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    .line 156
    sget v1, Lcom/peel/ui/fp;->menu_edit:I

    if-ne v0, v1, :cond_1

    .line 157
    iget-object v0, p0, Lcom/peel/h/a/hh;->ak:Lcom/peel/h/a/ho;

    invoke-virtual {v0, v2}, Lcom/peel/h/a/ho;->a(Z)V

    .line 158
    iget-object v0, p0, Lcom/peel/h/a/hh;->ak:Lcom/peel/h/a/ho;

    invoke-virtual {v0}, Lcom/peel/h/a/ho;->notifyDataSetChanged()V

    .line 159
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/peel/h/a/hh;->b(I)V

    .line 166
    :cond_0
    :goto_0
    return v2

    .line 160
    :cond_1
    sget v1, Lcom/peel/ui/fp;->menu_delete:I

    if-ne v0, v1, :cond_0

    .line 161
    iget-object v0, p0, Lcom/peel/h/a/hh;->ak:Lcom/peel/h/a/ho;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/peel/h/a/ho;->a(Z)V

    .line 162
    iget-object v0, p0, Lcom/peel/h/a/hh;->ak:Lcom/peel/h/a/ho;

    invoke-virtual {v0}, Lcom/peel/h/a/ho;->notifyDataSetChanged()V

    .line 163
    invoke-direct {p0, v2}, Lcom/peel/h/a/hh;->b(I)V

    .line 164
    invoke-direct {p0}, Lcom/peel/h/a/hh;->S()V

    goto :goto_0
.end method

.method public d(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 43
    invoke-super {p0, p1}, Lcom/peel/d/u;->d(Landroid/os/Bundle;)V

    .line 44
    iget-object v0, p0, Lcom/peel/h/a/hh;->e:Landroid/widget/TextView;

    sget v1, Lcom/peel/ui/ft;->follow_team_desc:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 46
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/peel/h/a/hh;->b(I)V

    .line 47
    invoke-direct {p0}, Lcom/peel/h/a/hh;->T()V

    .line 49
    new-instance v0, Lcom/peel/h/a/ho;

    invoke-direct {v0, p0}, Lcom/peel/h/a/ho;-><init>(Lcom/peel/h/a/hh;)V

    iput-object v0, p0, Lcom/peel/h/a/hh;->ak:Lcom/peel/h/a/ho;

    .line 50
    iget-object v0, p0, Lcom/peel/h/a/hh;->g:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/peel/h/a/hh;->ak:Lcom/peel/h/a/ho;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 51
    iget-object v0, p0, Lcom/peel/h/a/hh;->g:Landroid/widget/ListView;

    new-instance v1, Lcom/peel/h/a/hi;

    invoke-direct {v1, p0}, Lcom/peel/h/a/hi;-><init>(Lcom/peel/h/a/hh;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 64
    invoke-direct {p0}, Lcom/peel/h/a/hh;->c()V

    .line 65
    return-void
.end method

.method public w()V
    .locals 0

    .prologue
    .line 185
    invoke-super {p0}, Lcom/peel/d/u;->w()V

    .line 186
    invoke-virtual {p0}, Lcom/peel/h/a/hh;->Z()V

    .line 187
    return-void
.end method
