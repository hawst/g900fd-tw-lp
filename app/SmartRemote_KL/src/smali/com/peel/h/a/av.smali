.class public Lcom/peel/h/a/av;
.super Lcom/peel/d/u;


# instance fields
.field private aj:Landroid/widget/GridView;

.field private ak:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private al:Landroid/widget/TextView;

.field private am:Landroid/widget/TextView;

.field private an:Lcom/peel/ui/bt;

.field private ao:Lcom/peel/ui/bt;

.field private e:[Ljava/lang/String;

.field private f:[Ljava/lang/String;

.field private g:Z

.field private h:Landroid/view/View;

.field private i:Landroid/widget/GridView;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 43
    invoke-direct {p0}, Lcom/peel/d/u;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/peel/h/a/av;)Lcom/peel/ui/bt;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/peel/h/a/av;->an:Lcom/peel/ui/bt;

    return-object v0
.end method

.method static synthetic b(Lcom/peel/h/a/av;)Lcom/peel/ui/bt;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/peel/h/a/av;->ao:Lcom/peel/ui/bt;

    return-object v0
.end method

.method private c()V
    .locals 3

    .prologue
    .line 235
    const-class v0, Lcom/peel/h/a/av;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "update user age and gender in cloud"

    new-instance v2, Lcom/peel/h/a/ba;

    invoke-direct {v2, p0}, Lcom/peel/h/a/ba;-><init>(Lcom/peel/h/a/av;)V

    invoke-static {v0, v1, v2}, Lcom/peel/util/i;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    .line 258
    return-void
.end method

.method static synthetic c(Lcom/peel/h/a/av;)[Ljava/lang/String;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/peel/h/a/av;->e:[Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public Z()V
    .locals 6

    .prologue
    .line 263
    iget-object v0, p0, Lcom/peel/h/a/av;->d:Lcom/peel/d/a;

    if-nez v0, :cond_1

    .line 264
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 265
    iget-object v0, p0, Lcom/peel/h/a/av;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "has_done"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 266
    sget v0, Lcom/peel/ui/fp;->menu_done:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 269
    :cond_0
    iget-boolean v0, p0, Lcom/peel/h/a/av;->g:Z

    if-eqz v0, :cond_2

    .line 270
    new-instance v0, Lcom/peel/d/a;

    sget-object v1, Lcom/peel/d/d;->b:Lcom/peel/d/d;

    sget-object v2, Lcom/peel/d/b;->a:Lcom/peel/d/b;

    sget-object v3, Lcom/peel/d/c;->b:Lcom/peel/d/c;

    sget v4, Lcom/peel/ui/ft;->personalization:I

    invoke-virtual {p0, v4}, Lcom/peel/h/a/av;->a(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct/range {v0 .. v5}, Lcom/peel/d/a;-><init>(Lcom/peel/d/d;Lcom/peel/d/b;Lcom/peel/d/c;Ljava/lang/String;Ljava/util/List;)V

    iput-object v0, p0, Lcom/peel/h/a/av;->d:Lcom/peel/d/a;

    .line 275
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/peel/h/a/av;->b:Lcom/peel/d/i;

    iget-object v1, p0, Lcom/peel/h/a/av;->d:Lcom/peel/d/a;

    invoke-virtual {v0, v1}, Lcom/peel/d/i;->a(Lcom/peel/d/a;)V

    .line 276
    return-void

    .line 272
    :cond_2
    new-instance v0, Lcom/peel/d/a;

    sget-object v1, Lcom/peel/d/d;->b:Lcom/peel/d/d;

    sget-object v2, Lcom/peel/d/b;->a:Lcom/peel/d/b;

    sget-object v3, Lcom/peel/d/c;->b:Lcom/peel/d/c;

    sget v4, Lcom/peel/ui/ft;->basic_profile:I

    invoke-virtual {p0, v4}, Lcom/peel/h/a/av;->a(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct/range {v0 .. v5}, Lcom/peel/d/a;-><init>(Lcom/peel/d/d;Lcom/peel/d/b;Lcom/peel/d/c;Ljava/lang/String;Ljava/util/List;)V

    iput-object v0, p0, Lcom/peel/h/a/av;->d:Lcom/peel/d/a;

    goto :goto_0
.end method

.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    .line 55
    sget v0, Lcom/peel/ui/fq;->settings_age_gender:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/h/a/av;->h:Landroid/view/View;

    .line 56
    iget-object v0, p0, Lcom/peel/h/a/av;->h:Landroid/view/View;

    sget v1, Lcom/peel/ui/fp;->gender_age_selection_male_grid:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/GridView;

    iput-object v0, p0, Lcom/peel/h/a/av;->i:Landroid/widget/GridView;

    .line 57
    iget-object v0, p0, Lcom/peel/h/a/av;->h:Landroid/view/View;

    sget v1, Lcom/peel/ui/fp;->gender_age_selection_female_grid:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/GridView;

    iput-object v0, p0, Lcom/peel/h/a/av;->aj:Landroid/widget/GridView;

    .line 58
    iget-object v0, p0, Lcom/peel/h/a/av;->h:Landroid/view/View;

    sget v1, Lcom/peel/ui/fp;->title_male:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/peel/h/a/av;->al:Landroid/widget/TextView;

    .line 59
    iget-object v0, p0, Lcom/peel/h/a/av;->h:Landroid/view/View;

    sget v1, Lcom/peel/ui/fp;->title_female:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/peel/h/a/av;->am:Landroid/widget/TextView;

    .line 61
    iget-object v0, p0, Lcom/peel/h/a/av;->h:Landroid/view/View;

    sget v1, Lcom/peel/ui/fp;->here:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/peel/h/a/aw;

    invoke-direct {v1, p0}, Lcom/peel/h/a/aw;-><init>(Lcom/peel/h/a/av;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 67
    iget-object v0, p0, Lcom/peel/h/a/av;->h:Landroid/view/View;

    return-object v0
.end method

.method public a(Landroid/view/MenuItem;)Z
    .locals 2

    .prologue
    .line 227
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    .line 228
    sget v1, Lcom/peel/ui/fp;->menu_done:I

    if-ne v0, v1, :cond_0

    .line 229
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    invoke-virtual {p0, v0}, Lcom/peel/h/a/av;->c(Landroid/os/Bundle;)V

    .line 231
    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method public b()Z
    .locals 4

    .prologue
    .line 172
    sget-object v0, Lcom/peel/content/a;->f:Lcom/peel/content/user/User;

    invoke-virtual {v0}, Lcom/peel/content/user/User;->u()V

    .line 175
    invoke-direct {p0}, Lcom/peel/h/a/av;->c()V

    .line 177
    invoke-static {}, Lcom/peel/util/a/f;->a()Lcom/peel/util/a/f;

    move-result-object v2

    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v0}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const/16 v3, 0xbc8

    iget-boolean v1, p0, Lcom/peel/h/a/av;->g:Z

    if-eqz v1, :cond_1

    const/16 v1, 0x7d5

    :goto_1
    invoke-virtual {v2, v0, v3, v1}, Lcom/peel/util/a/f;->a(III)V

    .line 181
    invoke-super {p0}, Lcom/peel/d/u;->b()Z

    move-result v0

    return v0

    .line 177
    :cond_0
    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v0}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/data/at;->f()I

    move-result v0

    goto :goto_0

    :cond_1
    const/16 v1, 0x7d8

    goto :goto_1
.end method

.method public c(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 187
    sget-object v0, Lcom/peel/content/a;->h:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-nez v0, :cond_0

    .line 217
    :goto_0
    return-void

    .line 191
    :cond_0
    sget-object v0, Lcom/peel/content/a;->f:Lcom/peel/content/user/User;

    invoke-virtual {v0}, Lcom/peel/content/user/User;->u()V

    .line 192
    invoke-direct {p0}, Lcom/peel/h/a/av;->c()V

    .line 194
    invoke-static {}, Lcom/peel/util/a/f;->a()Lcom/peel/util/a/f;

    move-result-object v2

    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v0}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v0

    if-nez v0, :cond_2

    const/4 v0, 0x1

    :goto_1
    const/16 v3, 0xbc8

    iget-boolean v1, p0, Lcom/peel/h/a/av;->g:Z

    if-eqz v1, :cond_3

    const/16 v1, 0x7d5

    :goto_2
    invoke-virtual {v2, v0, v3, v1}, Lcom/peel/util/a/f;->a(III)V

    .line 199
    sget-boolean v0, Lcom/peel/util/a;->h:Z

    if-eqz v0, :cond_1

    .line 200
    const-string/jumbo v0, "tuneinoptions"

    .line 201
    invoke-virtual {p0}, Lcom/peel/h/a/av;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/ae;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    new-instance v2, Lcom/peel/h/a/az;

    invoke-direct {v2, p0}, Lcom/peel/h/a/az;-><init>(Lcom/peel/h/a/av;)V

    .line 200
    invoke-static {v0, v1, v2}, Lcom/peel/util/a;->a(Ljava/lang/String;Landroid/content/Context;Lcom/peel/util/t;)V

    .line 210
    :cond_1
    sget-boolean v0, Lcom/peel/util/a;->g:Z

    if-eqz v0, :cond_4

    .line 211
    iget-object v0, p0, Lcom/peel/h/a/av;->b:Lcom/peel/d/i;

    invoke-virtual {p0}, Lcom/peel/h/a/av;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/ae;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/peel/util/bx;->b(Lcom/peel/d/i;Landroid/content/Context;)V

    goto :goto_0

    .line 194
    :cond_2
    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v0}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/data/at;->f()I

    move-result v0

    goto :goto_1

    :cond_3
    const/16 v1, 0x7d8

    goto :goto_2

    .line 213
    :cond_4
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 214
    const-string/jumbo v1, "type"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 215
    invoke-virtual {p0}, Lcom/peel/h/a/av;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    const-class v2, Lcom/peel/ui/lq;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lcom/peel/d/e;->a(Landroid/support/v4/app/ae;Ljava/lang/String;Landroid/os/Bundle;)V

    goto :goto_0
.end method

.method public d(Landroid/os/Bundle;)V
    .locals 8

    .prologue
    const/4 v2, 0x2

    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 87
    invoke-super {p0, p1}, Lcom/peel/d/u;->d(Landroid/os/Bundle;)V

    .line 89
    iget-object v3, p0, Lcom/peel/h/a/av;->c:Landroid/os/Bundle;

    const-string/jumbo v4, "from_setup"

    invoke-virtual {v3, v4, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    iput-boolean v3, p0, Lcom/peel/h/a/av;->g:Z

    .line 90
    iget-object v3, p0, Lcom/peel/h/a/av;->c:Landroid/os/Bundle;

    const-string/jumbo v4, "category"

    sget v5, Lcom/peel/ui/ft;->basic_profile:I

    invoke-virtual {p0, v5}, Lcom/peel/h/a/av;->a(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 91
    invoke-virtual {p0}, Lcom/peel/h/a/av;->n()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/peel/ui/fk;->age_array:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/peel/h/a/av;->e:[Ljava/lang/String;

    .line 92
    invoke-virtual {p0}, Lcom/peel/h/a/av;->n()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/peel/ui/fk;->gender_array:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/peel/h/a/av;->f:[Ljava/lang/String;

    .line 94
    iget-object v3, p0, Lcom/peel/h/a/av;->f:[Ljava/lang/String;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/peel/h/a/av;->f:[Ljava/lang/String;

    array-length v3, v3

    if-le v3, v1, :cond_0

    .line 95
    iget-object v3, p0, Lcom/peel/h/a/av;->al:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/peel/h/a/av;->f:[Ljava/lang/String;

    aget-object v4, v4, v0

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 96
    iget-object v3, p0, Lcom/peel/h/a/av;->am:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/peel/h/a/av;->f:[Ljava/lang/String;

    aget-object v4, v4, v1

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 99
    :cond_0
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iput-object v3, p0, Lcom/peel/h/a/av;->ak:Ljava/util/ArrayList;

    .line 100
    iget-object v4, p0, Lcom/peel/h/a/av;->e:[Ljava/lang/String;

    array-length v5, v4

    move v3, v0

    :goto_0
    if-ge v3, v5, :cond_2

    aget-object v6, v4, v3

    .line 101
    iget-object v7, p0, Lcom/peel/h/a/av;->e:[Ljava/lang/String;

    aget-object v7, v7, v0

    if-ne v6, v7, :cond_1

    .line 100
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 102
    :cond_1
    iget-object v7, p0, Lcom/peel/h/a/av;->ak:Ljava/util/ArrayList;

    invoke-virtual {v7, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 105
    :cond_2
    new-instance v3, Lcom/peel/ui/bt;

    invoke-virtual {p0}, Lcom/peel/h/a/av;->m()Landroid/support/v4/app/ae;

    move-result-object v4

    iget-object v5, p0, Lcom/peel/h/a/av;->ak:Ljava/util/ArrayList;

    invoke-direct {v3, v4, v5, v1}, Lcom/peel/ui/bt;-><init>(Landroid/content/Context;Ljava/util/ArrayList;Z)V

    iput-object v3, p0, Lcom/peel/h/a/av;->an:Lcom/peel/ui/bt;

    .line 106
    iget-object v3, p0, Lcom/peel/h/a/av;->i:Landroid/widget/GridView;

    iget-object v4, p0, Lcom/peel/h/a/av;->an:Lcom/peel/ui/bt;

    invoke-virtual {v3, v4}, Landroid/widget/GridView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 108
    new-instance v3, Lcom/peel/ui/bt;

    invoke-virtual {p0}, Lcom/peel/h/a/av;->m()Landroid/support/v4/app/ae;

    move-result-object v4

    iget-object v5, p0, Lcom/peel/h/a/av;->ak:Ljava/util/ArrayList;

    invoke-direct {v3, v4, v5, v0}, Lcom/peel/ui/bt;-><init>(Landroid/content/Context;Ljava/util/ArrayList;Z)V

    iput-object v3, p0, Lcom/peel/h/a/av;->ao:Lcom/peel/ui/bt;

    .line 109
    iget-object v3, p0, Lcom/peel/h/a/av;->aj:Landroid/widget/GridView;

    iget-object v4, p0, Lcom/peel/h/a/av;->ao:Lcom/peel/ui/bt;

    invoke-virtual {v3, v4}, Landroid/widget/GridView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 112
    iget-object v3, p0, Lcom/peel/h/a/av;->i:Landroid/widget/GridView;

    new-instance v4, Lcom/peel/h/a/ax;

    invoke-direct {v4, p0}, Lcom/peel/h/a/ax;-><init>(Lcom/peel/h/a/av;)V

    invoke-virtual {v3, v4}, Landroid/widget/GridView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 134
    iget-object v3, p0, Lcom/peel/h/a/av;->aj:Landroid/widget/GridView;

    new-instance v4, Lcom/peel/h/a/ay;

    invoke-direct {v4, p0}, Lcom/peel/h/a/ay;-><init>(Lcom/peel/h/a/av;)V

    invoke-virtual {v3, v4}, Landroid/widget/GridView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 156
    sget-object v3, Lcom/peel/content/a;->f:Lcom/peel/content/user/User;

    invoke-virtual {v3}, Lcom/peel/content/user/User;->b()C

    move-result v3

    .line 157
    const/16 v4, 0x6e

    if-ne v3, v4, :cond_5

    .line 159
    :goto_2
    if-eq v0, v1, :cond_3

    if-ne v0, v2, :cond_4

    :cond_3
    iget-object v2, p0, Lcom/peel/h/a/av;->e:[Ljava/lang/String;

    array-length v2, v2

    sget-object v3, Lcom/peel/content/a;->f:Lcom/peel/content/user/User;

    invoke-virtual {v3}, Lcom/peel/content/user/User;->a()I

    move-result v3

    if-lt v2, v3, :cond_4

    sget-object v2, Lcom/peel/content/a;->f:Lcom/peel/content/user/User;

    invoke-virtual {v2}, Lcom/peel/content/user/User;->a()I

    move-result v2

    if-lez v2, :cond_4

    .line 160
    sget-object v2, Lcom/peel/content/a;->f:Lcom/peel/content/user/User;

    invoke-virtual {v2}, Lcom/peel/content/user/User;->a()I

    move-result v2

    .line 162
    if-ne v0, v1, :cond_7

    .line 163
    iget-object v0, p0, Lcom/peel/h/a/av;->an:Lcom/peel/ui/bt;

    add-int/lit8 v1, v2, -0x1

    invoke-virtual {v0, v1}, Lcom/peel/ui/bt;->a(I)V

    .line 168
    :cond_4
    :goto_3
    return-void

    .line 157
    :cond_5
    const/16 v0, 0x6d

    if-ne v3, v0, :cond_6

    move v0, v1

    goto :goto_2

    :cond_6
    move v0, v2

    goto :goto_2

    .line 165
    :cond_7
    iget-object v0, p0, Lcom/peel/h/a/av;->ao:Lcom/peel/ui/bt;

    add-int/lit8 v1, v2, -0x1

    invoke-virtual {v0, v1}, Lcom/peel/ui/bt;->a(I)V

    goto :goto_3
.end method

.method public f()V
    .locals 3

    .prologue
    .line 80
    invoke-super {p0}, Lcom/peel/d/u;->f()V

    .line 81
    invoke-virtual {p0}, Lcom/peel/h/a/av;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    const-string/jumbo v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/ae;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 82
    invoke-virtual {p0}, Lcom/peel/h/a/av;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/ae;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 83
    return-void
.end method

.method public h(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 221
    invoke-super {p0, p1}, Lcom/peel/d/u;->h(Landroid/os/Bundle;)V

    .line 222
    invoke-virtual {p0}, Lcom/peel/h/a/av;->Z()V

    .line 223
    return-void
.end method

.method public w()V
    .locals 3

    .prologue
    .line 72
    invoke-super {p0}, Lcom/peel/d/u;->w()V

    .line 73
    invoke-virtual {p0}, Lcom/peel/h/a/av;->Z()V

    .line 74
    invoke-virtual {p0}, Lcom/peel/h/a/av;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    const-string/jumbo v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/ae;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 75
    invoke-virtual {p0}, Lcom/peel/h/a/av;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/ae;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 76
    return-void
.end method
