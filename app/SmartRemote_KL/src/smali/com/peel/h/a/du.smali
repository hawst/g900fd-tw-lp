.class public Lcom/peel/h/a/du;
.super Lcom/peel/d/u;


# static fields
.field private static final e:Ljava/lang/String;


# instance fields
.field private aj:Landroid/widget/EditText;

.field private ak:Landroid/widget/Spinner;

.field private al:Ljava/lang/String;

.field private am:Ljava/lang/String;

.field private an:Ljava/lang/String;

.field private ao:Ljava/lang/String;

.field private ap:Ljava/lang/String;

.field private aq:Ljava/lang/String;

.field private ar:Z

.field private as:Z

.field private at:Lcom/peel/widget/ag;

.field private au:Landroid/widget/TextView;

.field private av:Landroid/widget/TextView;

.field private aw:Landroid/widget/TextView;

.field private ax:Landroid/widget/TextView;

.field private f:Landroid/widget/ViewFlipper;

.field private g:Landroid/widget/EditText;

.field private h:Landroid/widget/EditText;

.field private i:Landroid/widget/EditText;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 49
    const-class v0, Lcom/peel/h/a/du;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/peel/h/a/du;->e:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 48
    invoke-direct {p0}, Lcom/peel/d/u;-><init>()V

    .line 56
    iput-boolean v0, p0, Lcom/peel/h/a/du;->ar:Z

    iput-boolean v0, p0, Lcom/peel/h/a/du;->as:Z

    return-void
.end method

.method private S()V
    .locals 4

    .prologue
    const/4 v2, 0x1

    .line 116
    iget-boolean v0, p0, Lcom/peel/h/a/du;->as:Z

    if-eqz v0, :cond_0

    .line 302
    :goto_0
    return-void

    .line 118
    :cond_0
    iget-object v0, p0, Lcom/peel/h/a/du;->h:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/peel/util/bx;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 126
    invoke-virtual {p0}, Lcom/peel/h/a/du;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    sget v1, Lcom/peel/ui/ft;->enter_valid_email:I

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 131
    :cond_1
    iget-object v0, p0, Lcom/peel/h/a/du;->an:Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/peel/h/a/du;->aj:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    if-nez v0, :cond_2

    .line 139
    invoke-virtual {p0}, Lcom/peel/h/a/du;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/h/a/du;->an:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 143
    :cond_2
    iget-object v0, p0, Lcom/peel/h/a/du;->an:Ljava/lang/String;

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/peel/h/a/du;->i:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    if-nez v0, :cond_3

    .line 151
    invoke-virtual {p0}, Lcom/peel/h/a/du;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    sget v1, Lcom/peel/ui/ft;->enter_desc:I

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 155
    :cond_3
    sget-boolean v0, Lcom/peel/util/b/a;->a:Z

    if-nez v0, :cond_4

    .line 156
    iput-boolean v2, p0, Lcom/peel/h/a/du;->as:Z

    .line 157
    sget-object v0, Lcom/peel/h/a/du;->e:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/peel/h/a/du;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    invoke-static {v0, v1, v2}, Lcom/peel/util/bx;->a(Ljava/lang/String;Landroid/app/Activity;Z)V

    .line 159
    invoke-virtual {p0}, Lcom/peel/h/a/du;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    const-string/jumbo v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/ae;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 160
    iget-object v1, p0, Lcom/peel/h/a/du;->i:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 162
    invoke-virtual {p0}, Lcom/peel/h/a/du;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 163
    iget-object v1, p0, Lcom/peel/h/a/du;->ak:Landroid/widget/Spinner;

    invoke-virtual {v1}, Landroid/widget/Spinner;->getSelectedItem()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/peel/h/a/du;->ao:Ljava/lang/String;

    .line 164
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string/jumbo v2, "user_email"

    iget-object v3, p0, Lcom/peel/h/a/du;->h:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 165
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string/jumbo v2, "issue_type"

    iget-object v3, p0, Lcom/peel/h/a/du;->ao:Ljava/lang/String;

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 168
    sget-object v1, Lcom/peel/h/a/du;->e:Ljava/lang/String;

    const-string/jumbo v2, "report feedback"

    new-instance v3, Lcom/peel/h/a/dv;

    invoke-direct {v3, p0, v0}, Lcom/peel/h/a/dv;-><init>(Lcom/peel/h/a/du;Landroid/content/SharedPreferences;)V

    invoke-static {v1, v2, v3}, Lcom/peel/util/i;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    goto/16 :goto_0

    .line 300
    :cond_4
    invoke-virtual {p0}, Lcom/peel/h/a/du;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    invoke-virtual {p0}, Lcom/peel/h/a/du;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/peel/util/bx;->a(Landroid/content/Context;Landroid/support/v4/app/ae;)V

    goto/16 :goto_0
.end method

.method private T()V
    .locals 5

    .prologue
    const/16 v4, 0x8

    .line 305
    iget-object v0, p0, Lcom/peel/h/a/du;->f:Landroid/widget/ViewFlipper;

    sget v1, Lcom/peel/ui/fp;->email_tv:I

    invoke-virtual {v0, v1}, Landroid/widget/ViewFlipper;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/peel/h/a/du;->au:Landroid/widget/TextView;

    .line 306
    iget-object v0, p0, Lcom/peel/h/a/du;->f:Landroid/widget/ViewFlipper;

    sget v1, Lcom/peel/ui/fp;->subject:I

    invoke-virtual {v0, v1}, Landroid/widget/ViewFlipper;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/peel/h/a/du;->av:Landroid/widget/TextView;

    .line 307
    iget-object v0, p0, Lcom/peel/h/a/du;->f:Landroid/widget/ViewFlipper;

    sget v1, Lcom/peel/ui/fp;->feedback_label:I

    invoke-virtual {v0, v1}, Landroid/widget/ViewFlipper;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/peel/h/a/du;->aw:Landroid/widget/TextView;

    .line 308
    iget-object v0, p0, Lcom/peel/h/a/du;->f:Landroid/widget/ViewFlipper;

    sget v1, Lcom/peel/ui/fp;->model_number:I

    invoke-virtual {v0, v1}, Landroid/widget/ViewFlipper;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/peel/h/a/du;->ax:Landroid/widget/TextView;

    .line 310
    iget-object v0, p0, Lcom/peel/h/a/du;->au:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/peel/h/a/du;->au:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, ":"

    const-string/jumbo v3, " "

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 311
    iget-object v0, p0, Lcom/peel/h/a/du;->av:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/peel/h/a/du;->av:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, ":"

    const-string/jumbo v3, " "

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 312
    iget-object v0, p0, Lcom/peel/h/a/du;->aw:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/peel/h/a/du;->aw:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, ":"

    const-string/jumbo v3, " "

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 313
    iget-object v0, p0, Lcom/peel/h/a/du;->ax:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/peel/h/a/du;->ax:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, ":"

    const-string/jumbo v3, " "

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 315
    iget-object v0, p0, Lcom/peel/h/a/du;->f:Landroid/widget/ViewFlipper;

    sget v1, Lcom/peel/ui/fp;->msg_email:I

    invoke-virtual {v0, v1}, Landroid/widget/ViewFlipper;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/peel/h/a/du;->h:Landroid/widget/EditText;

    .line 316
    iget-object v0, p0, Lcom/peel/h/a/du;->f:Landroid/widget/ViewFlipper;

    sget v1, Lcom/peel/ui/fp;->msg_subject:I

    invoke-virtual {v0, v1}, Landroid/widget/ViewFlipper;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/peel/h/a/du;->g:Landroid/widget/EditText;

    .line 317
    iget-object v0, p0, Lcom/peel/h/a/du;->al:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 318
    iget-object v0, p0, Lcom/peel/h/a/du;->g:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/peel/h/a/du;->al:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 321
    :goto_0
    iget-object v0, p0, Lcom/peel/h/a/du;->f:Landroid/widget/ViewFlipper;

    sget v1, Lcom/peel/ui/fp;->msg_desc:I

    invoke-virtual {v0, v1}, Landroid/widget/ViewFlipper;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/peel/h/a/du;->i:Landroid/widget/EditText;

    .line 322
    iget-object v0, p0, Lcom/peel/h/a/du;->am:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 323
    iget-object v0, p0, Lcom/peel/h/a/du;->i:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/peel/h/a/du;->am:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 326
    :goto_1
    iget-object v0, p0, Lcom/peel/h/a/du;->h:Landroid/widget/EditText;

    invoke-virtual {p0}, Lcom/peel/h/a/du;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    invoke-static {v1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    const-string/jumbo v2, "user_email"

    const-string/jumbo v3, ""

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 328
    iget-object v0, p0, Lcom/peel/h/a/du;->f:Landroid/widget/ViewFlipper;

    sget v1, Lcom/peel/ui/fp;->model_subject:I

    invoke-virtual {v0, v1}, Landroid/widget/ViewFlipper;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/peel/h/a/du;->aj:Landroid/widget/EditText;

    .line 331
    iget-object v0, p0, Lcom/peel/h/a/du;->f:Landroid/widget/ViewFlipper;

    sget v1, Lcom/peel/ui/fp;->spinner_issue_type:I

    invoke-virtual {v0, v1}, Landroid/widget/ViewFlipper;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lcom/peel/h/a/du;->ak:Landroid/widget/Spinner;

    .line 333
    iget-object v0, p0, Lcom/peel/h/a/du;->au:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/peel/h/a/du;->au:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, ":"

    const-string/jumbo v3, " "

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 334
    iget-object v0, p0, Lcom/peel/h/a/du;->av:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/peel/h/a/du;->av:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, ":"

    const-string/jumbo v3, " "

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 335
    iget-object v0, p0, Lcom/peel/h/a/du;->aw:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/peel/h/a/du;->aw:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, ":"

    const-string/jumbo v3, " "

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 336
    iget-object v0, p0, Lcom/peel/h/a/du;->ax:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/peel/h/a/du;->ax:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, ":"

    const-string/jumbo v3, " "

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 337
    iget-object v0, p0, Lcom/peel/h/a/du;->an:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 338
    iget-object v0, p0, Lcom/peel/h/a/du;->ak:Landroid/widget/Spinner;

    invoke-virtual {v0, v4}, Landroid/widget/Spinner;->setVisibility(I)V

    .line 339
    iget-object v0, p0, Lcom/peel/h/a/du;->f:Landroid/widget/ViewFlipper;

    sget v1, Lcom/peel/ui/fp;->feedback_label:I

    invoke-virtual {v0, v1}, Landroid/widget/ViewFlipper;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 342
    :cond_0
    invoke-virtual {p0}, Lcom/peel/h/a/du;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    sget v1, Lcom/peel/ui/fk;->issue_types:I

    sget v2, Lcom/peel/ui/fq;->issue_type_spinner_item:I

    invoke-static {v0, v1, v2}, Landroid/widget/ArrayAdapter;->createFromResource(Landroid/content/Context;II)Landroid/widget/ArrayAdapter;

    move-result-object v0

    .line 344
    iget-object v1, p0, Lcom/peel/h/a/du;->ak:Landroid/widget/Spinner;

    invoke-virtual {v1, v0}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 345
    iget-object v0, p0, Lcom/peel/h/a/du;->ak:Landroid/widget/Spinner;

    new-instance v1, Lcom/peel/h/a/dz;

    invoke-direct {v1, p0}, Lcom/peel/h/a/dz;-><init>(Lcom/peel/h/a/du;)V

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 357
    return-void

    .line 320
    :cond_1
    iget-object v0, p0, Lcom/peel/h/a/du;->g:Landroid/widget/EditText;

    const-string/jumbo v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 325
    :cond_2
    iget-object v0, p0, Lcom/peel/h/a/du;->i:Landroid/widget/EditText;

    const-string/jumbo v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1
.end method

.method private U()V
    .locals 4

    .prologue
    .line 375
    iget-object v0, p0, Lcom/peel/h/a/du;->f:Landroid/widget/ViewFlipper;

    invoke-virtual {v0}, Landroid/widget/ViewFlipper;->getDisplayedChild()I

    move-result v0

    if-nez v0, :cond_0

    .line 376
    iget-object v0, p0, Lcom/peel/h/a/du;->h:Landroid/widget/EditText;

    new-instance v1, Lcom/peel/h/a/ea;

    invoke-direct {v1, p0}, Lcom/peel/h/a/ea;-><init>(Lcom/peel/h/a/du;)V

    const-wide/16 v2, 0x1c2

    invoke-virtual {v0, v1, v2, v3}, Landroid/widget/EditText;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 387
    iget-object v0, p0, Lcom/peel/h/a/du;->h:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    .line 389
    :cond_0
    return-void
.end method

.method static synthetic a(Lcom/peel/h/a/du;)Landroid/widget/EditText;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/peel/h/a/du;->i:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic a(Lcom/peel/h/a/du;Lcom/peel/widget/ag;)Lcom/peel/widget/ag;
    .locals 0

    .prologue
    .line 48
    iput-object p1, p0, Lcom/peel/h/a/du;->at:Lcom/peel/widget/ag;

    return-object p1
.end method

.method static synthetic a(Lcom/peel/h/a/du;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 48
    iput-object p1, p0, Lcom/peel/h/a/du;->am:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic a(Lcom/peel/h/a/du;Z)Z
    .locals 0

    .prologue
    .line 48
    iput-boolean p1, p0, Lcom/peel/h/a/du;->as:Z

    return p1
.end method

.method static synthetic b(Lcom/peel/h/a/du;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/peel/h/a/du;->am:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic b(Lcom/peel/h/a/du;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 48
    iput-object p1, p0, Lcom/peel/h/a/du;->aq:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 48
    sget-object v0, Lcom/peel/h/a/du;->e:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic c(Lcom/peel/h/a/du;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/peel/h/a/du;->an:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic c(Lcom/peel/h/a/du;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 48
    iput-object p1, p0, Lcom/peel/h/a/du;->ao:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic d(Lcom/peel/h/a/du;)Landroid/widget/EditText;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/peel/h/a/du;->aj:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic d(Lcom/peel/h/a/du;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 48
    iput-object p1, p0, Lcom/peel/h/a/du;->ap:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic e(Lcom/peel/h/a/du;)Landroid/widget/Spinner;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/peel/h/a/du;->ak:Landroid/widget/Spinner;

    return-object v0
.end method

.method static synthetic f(Lcom/peel/h/a/du;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/peel/h/a/du;->ao:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic g(Lcom/peel/h/a/du;)Landroid/widget/EditText;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/peel/h/a/du;->h:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic h(Lcom/peel/h/a/du;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/peel/h/a/du;->ap:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic i(Lcom/peel/h/a/du;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/peel/h/a/du;->aq:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic j(Lcom/peel/h/a/du;)Landroid/widget/ViewFlipper;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/peel/h/a/du;->f:Landroid/widget/ViewFlipper;

    return-object v0
.end method


# virtual methods
.method public Z()V
    .locals 7

    .prologue
    .line 394
    iget-object v0, p0, Lcom/peel/h/a/du;->d:Lcom/peel/d/a;

    if-nez v0, :cond_0

    .line 395
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 396
    sget v0, Lcom/peel/ui/fp;->menu_send:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 397
    new-instance v0, Lcom/peel/d/a;

    sget-object v1, Lcom/peel/d/d;->b:Lcom/peel/d/d;

    sget-object v2, Lcom/peel/d/b;->a:Lcom/peel/d/b;

    sget-object v3, Lcom/peel/d/c;->b:Lcom/peel/d/c;

    iget-object v4, p0, Lcom/peel/h/a/du;->c:Landroid/os/Bundle;

    const-string/jumbo v6, "category"

    invoke-virtual {v4, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-direct/range {v0 .. v5}, Lcom/peel/d/a;-><init>(Lcom/peel/d/d;Lcom/peel/d/b;Lcom/peel/d/c;Ljava/lang/String;Ljava/util/List;)V

    iput-object v0, p0, Lcom/peel/h/a/du;->d:Lcom/peel/d/a;

    .line 399
    :cond_0
    iget-object v0, p0, Lcom/peel/h/a/du;->b:Lcom/peel/d/i;

    iget-object v1, p0, Lcom/peel/h/a/du;->d:Lcom/peel/d/a;

    invoke-virtual {v0, v1}, Lcom/peel/d/i;->a(Lcom/peel/d/a;)V

    .line 400
    return-void
.end method

.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    .line 74
    sget v0, Lcom/peel/ui/fq;->feedback:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ViewFlipper;

    iput-object v0, p0, Lcom/peel/h/a/du;->f:Landroid/widget/ViewFlipper;

    .line 76
    invoke-direct {p0}, Lcom/peel/h/a/du;->T()V

    .line 77
    invoke-virtual {p0}, Lcom/peel/h/a/du;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    invoke-static {v0}, Lcom/peel/util/g;->a(Landroid/app/Activity;)V

    .line 79
    iget-object v0, p0, Lcom/peel/h/a/du;->f:Landroid/widget/ViewFlipper;

    return-object v0
.end method

.method public a(Landroid/view/MenuItem;)Z
    .locals 2

    .prologue
    .line 361
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    .line 362
    sget v1, Lcom/peel/ui/fp;->menu_send:I

    if-ne v0, v1, :cond_0

    .line 363
    invoke-direct {p0}, Lcom/peel/h/a/du;->S()V

    .line 365
    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method public a_(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 62
    invoke-super {p0, p1}, Lcom/peel/d/u;->a_(Landroid/os/Bundle;)V

    .line 64
    iget-object v0, p0, Lcom/peel/h/a/du;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "feedback_subject"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 65
    iget-object v0, p0, Lcom/peel/h/a/du;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "feedback_subject"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/h/a/du;->al:Ljava/lang/String;

    .line 66
    :cond_0
    iget-object v0, p0, Lcom/peel/h/a/du;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "feedback_desc"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 67
    iget-object v0, p0, Lcom/peel/h/a/du;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "feedback_desc"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/h/a/du;->am:Ljava/lang/String;

    .line 69
    :cond_1
    iget-object v0, p0, Lcom/peel/h/a/du;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "feedback_model"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/h/a/du;->an:Ljava/lang/String;

    .line 70
    return-void
.end method

.method public d(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 103
    invoke-super {p0, p1}, Lcom/peel/d/u;->d(Landroid/os/Bundle;)V

    .line 106
    iget-object v0, p0, Lcom/peel/h/a/du;->al:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/peel/h/a/du;->am:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 107
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/peel/h/a/du;->ar:Z

    .line 108
    sget-object v0, Lcom/peel/h/a/du;->e:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, " ### from setup: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/peel/h/a/du;->ar:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 110
    iget-object v0, p0, Lcom/peel/h/a/du;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "category"

    sget v2, Lcom/peel/ui/ft;->submitabug:I

    invoke-virtual {p0, v2}, Lcom/peel/h/a/du;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 112
    :cond_0
    return-void
.end method

.method public f()V
    .locals 3

    .prologue
    .line 93
    invoke-super {p0}, Lcom/peel/d/u;->f()V

    .line 94
    invoke-virtual {p0}, Lcom/peel/h/a/du;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    const-string/jumbo v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/ae;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 95
    invoke-virtual {p0}, Lcom/peel/h/a/du;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/ae;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 96
    iget-object v0, p0, Lcom/peel/h/a/du;->at:Lcom/peel/widget/ag;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/peel/h/a/du;->at:Lcom/peel/widget/ag;

    invoke-virtual {v0}, Lcom/peel/widget/ag;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 97
    iget-object v0, p0, Lcom/peel/h/a/du;->at:Lcom/peel/widget/ag;

    invoke-virtual {v0}, Lcom/peel/widget/ag;->dismiss()V

    .line 99
    :cond_0
    return-void
.end method

.method public h(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 370
    invoke-super {p0, p1}, Lcom/peel/d/u;->h(Landroid/os/Bundle;)V

    .line 371
    invoke-virtual {p0}, Lcom/peel/h/a/du;->Z()V

    .line 372
    return-void
.end method

.method public w()V
    .locals 0

    .prologue
    .line 84
    invoke-super {p0}, Lcom/peel/d/u;->w()V

    .line 87
    invoke-direct {p0}, Lcom/peel/h/a/du;->U()V

    .line 88
    invoke-virtual {p0}, Lcom/peel/h/a/du;->Z()V

    .line 89
    return-void
.end method
