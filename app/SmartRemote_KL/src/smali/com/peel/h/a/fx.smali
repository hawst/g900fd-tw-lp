.class Lcom/peel/h/a/fx;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/peel/control/h;

.field final synthetic b:Lcom/peel/h/a/fn;


# direct methods
.method constructor <init>(Lcom/peel/h/a/fn;Lcom/peel/control/h;)V
    .locals 0

    .prologue
    .line 427
    iput-object p1, p0, Lcom/peel/h/a/fx;->b:Lcom/peel/h/a/fn;

    iput-object p2, p0, Lcom/peel/h/a/fx;->a:Lcom/peel/control/h;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 430
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 431
    const-string/jumbo v1, "device"

    iget-object v2, p0, Lcom/peel/h/a/fx;->a:Lcom/peel/control/h;

    invoke-virtual {v2}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v2

    invoke-virtual {v2}, Lcom/peel/data/g;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 432
    const-string/jumbo v1, "room"

    iget-object v2, p0, Lcom/peel/h/a/fx;->b:Lcom/peel/h/a/fn;

    invoke-static {v2}, Lcom/peel/h/a/fn;->e(Lcom/peel/h/a/fn;)Lcom/peel/data/ContentRoom;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 433
    const-string/jumbo v1, "providername"

    iget-object v2, p0, Lcom/peel/h/a/fx;->b:Lcom/peel/h/a/fn;

    invoke-static {v2}, Lcom/peel/h/a/fn;->f(Lcom/peel/h/a/fn;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 434
    iget-object v1, p0, Lcom/peel/h/a/fx;->b:Lcom/peel/h/a/fn;

    invoke-virtual {v1}, Lcom/peel/h/a/fn;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    const-class v2, Lcom/peel/h/a/cz;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lcom/peel/d/e;->c(Landroid/support/v4/app/ae;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 435
    return-void
.end method
