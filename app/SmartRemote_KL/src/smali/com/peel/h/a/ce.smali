.class Lcom/peel/h/a/ce;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/peel/h/a/bz;


# direct methods
.method constructor <init>(Lcom/peel/h/a/bz;)V
    .locals 0

    .prologue
    .line 446
    iput-object p1, p0, Lcom/peel/h/a/ce;->a:Lcom/peel/h/a/bz;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 9

    .prologue
    const/4 v8, 0x2

    const/4 v4, 0x0

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 449
    iget-object v0, p0, Lcom/peel/h/a/ce;->a:Lcom/peel/h/a/bz;

    invoke-virtual {v0}, Lcom/peel/h/a/bz;->s()Z

    move-result v0

    if-nez v0, :cond_0

    .line 659
    :goto_0
    return-void

    .line 452
    :cond_0
    invoke-static {}, Lcom/peel/h/a/bz;->c()Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "\n##### volume idx: "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v5, p0, Lcom/peel/h/a/ce;->a:Lcom/peel/h/a/bz;

    invoke-static {v5}, Lcom/peel/h/a/bz;->d(Lcom/peel/h/a/bz;)I

    move-result v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 453
    iget-object v0, p0, Lcom/peel/h/a/ce;->a:Lcom/peel/h/a/bz;

    invoke-static {v0}, Lcom/peel/h/a/bz;->d(Lcom/peel/h/a/bz;)I

    move-result v0

    if-ne v0, v3, :cond_6

    iget-object v0, p0, Lcom/peel/h/a/ce;->a:Lcom/peel/h/a/bz;

    invoke-static {v0}, Lcom/peel/h/a/bz;->e(Lcom/peel/h/a/bz;)Lcom/peel/control/h;

    move-result-object v0

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/peel/h/a/ce;->a:Lcom/peel/h/a/bz;

    iget-object v2, p0, Lcom/peel/h/a/ce;->a:Lcom/peel/h/a/bz;

    invoke-static {v2}, Lcom/peel/h/a/bz;->e(Lcom/peel/h/a/bz;)Lcom/peel/control/h;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/peel/h/a/bz;->a(Lcom/peel/h/a/bz;Lcom/peel/control/h;)Lcom/peel/control/h;

    .line 475
    :cond_1
    :goto_1
    invoke-static {}, Lcom/peel/h/a/bz;->c()Ljava/lang/String;

    move-result-object v2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "\n\ncurrent audio: "

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v0, p0, Lcom/peel/h/a/ce;->a:Lcom/peel/h/a/bz;

    invoke-static {v0}, Lcom/peel/h/a/bz;->n(Lcom/peel/h/a/bz;)Lcom/peel/control/h;

    move-result-object v0

    if-nez v0, :cond_7

    const-string/jumbo v0, "NULL"

    :goto_2
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 476
    invoke-static {}, Lcom/peel/h/a/bz;->c()Ljava/lang/String;

    move-result-object v2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "\n\nvolume device: "

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v0, p0, Lcom/peel/h/a/ce;->a:Lcom/peel/h/a/bz;

    invoke-static {v0}, Lcom/peel/h/a/bz;->o(Lcom/peel/h/a/bz;)Lcom/peel/control/h;

    move-result-object v0

    if-nez v0, :cond_8

    const-string/jumbo v0, "NULL"

    :goto_3
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 477
    iget-object v0, p0, Lcom/peel/h/a/ce;->a:Lcom/peel/h/a/bz;

    invoke-static {v0}, Lcom/peel/h/a/bz;->o(Lcom/peel/h/a/bz;)Lcom/peel/control/h;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 479
    iget-object v0, p0, Lcom/peel/h/a/ce;->a:Lcom/peel/h/a/bz;

    invoke-static {v0}, Lcom/peel/h/a/bz;->n(Lcom/peel/h/a/bz;)Lcom/peel/control/h;

    move-result-object v0

    if-nez v0, :cond_d

    .line 481
    iget-object v0, p0, Lcom/peel/h/a/ce;->a:Lcom/peel/h/a/bz;

    invoke-static {v0}, Lcom/peel/h/a/bz;->o(Lcom/peel/h/a/bz;)Lcom/peel/control/h;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/data/g;->d()I

    move-result v0

    if-ne v0, v3, :cond_c

    .line 482
    iget-object v0, p0, Lcom/peel/h/a/ce;->a:Lcom/peel/h/a/bz;

    invoke-static {v0}, Lcom/peel/h/a/bz;->p(Lcom/peel/h/a/bz;)Lcom/peel/control/a;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/peel/control/a;->a(I)Lcom/peel/control/h;

    move-result-object v0

    .line 484
    if-eqz v0, :cond_29

    invoke-virtual {v0}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/data/g;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/peel/h/a/ce;->a:Lcom/peel/h/a/bz;

    invoke-static {v2}, Lcom/peel/h/a/bz;->o(Lcom/peel/h/a/bz;)Lcom/peel/control/h;

    move-result-object v2

    invoke-virtual {v2}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v2

    invoke-virtual {v2}, Lcom/peel/data/g;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_29

    move v0, v3

    .line 487
    :goto_4
    if-eqz v0, :cond_9

    .line 488
    iget-object v0, p0, Lcom/peel/h/a/ce;->a:Lcom/peel/h/a/bz;

    invoke-static {v0}, Lcom/peel/h/a/bz;->p(Lcom/peel/h/a/bz;)Lcom/peel/control/a;

    move-result-object v0

    iget-object v2, p0, Lcom/peel/h/a/ce;->a:Lcom/peel/h/a/bz;

    invoke-static {v2}, Lcom/peel/h/a/bz;->o(Lcom/peel/h/a/bz;)Lcom/peel/control/h;

    move-result-object v2

    iget-object v5, p0, Lcom/peel/h/a/ce;->a:Lcom/peel/h/a/bz;

    invoke-static {v5}, Lcom/peel/h/a/bz;->p(Lcom/peel/h/a/bz;)Lcom/peel/control/a;

    move-result-object v5

    iget-object v6, p0, Lcom/peel/h/a/ce;->a:Lcom/peel/h/a/bz;

    invoke-static {v6}, Lcom/peel/h/a/bz;->o(Lcom/peel/h/a/bz;)Lcom/peel/control/h;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/peel/control/a;->a(Lcom/peel/control/h;)Ljava/lang/String;

    move-result-object v5

    new-array v6, v8, [Ljava/lang/Integer;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v3

    invoke-virtual {v0, v2, v5, v6}, Lcom/peel/control/a;->b(Lcom/peel/control/h;Ljava/lang/String;[Ljava/lang/Integer;)V

    .line 591
    :cond_2
    :goto_5
    iget-object v0, p0, Lcom/peel/h/a/ce;->a:Lcom/peel/h/a/bz;

    invoke-static {v0}, Lcom/peel/h/a/bz;->j(Lcom/peel/h/a/bz;)Lcom/peel/control/h;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 592
    iget-object v0, p0, Lcom/peel/h/a/ce;->a:Lcom/peel/h/a/bz;

    invoke-static {v0}, Lcom/peel/h/a/bz;->p(Lcom/peel/h/a/bz;)Lcom/peel/control/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/control/a;->e()[Lcom/peel/control/h;

    move-result-object v2

    array-length v5, v2

    move v0, v1

    :goto_6
    if-ge v0, v5, :cond_23

    aget-object v6, v2, v0

    .line 593
    invoke-virtual {v6}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v6

    invoke-virtual {v6}, Lcom/peel/data/g;->b()Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lcom/peel/h/a/ce;->a:Lcom/peel/h/a/bz;

    invoke-static {v7}, Lcom/peel/h/a/bz;->j(Lcom/peel/h/a/bz;)Lcom/peel/control/h;

    move-result-object v7

    invoke-virtual {v7}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v7

    invoke-virtual {v7}, Lcom/peel/data/g;->b()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_19

    move v0, v3

    .line 599
    :goto_7
    if-eqz v0, :cond_1a

    .line 600
    iget-object v0, p0, Lcom/peel/h/a/ce;->a:Lcom/peel/h/a/bz;

    invoke-static {v0}, Lcom/peel/h/a/bz;->p(Lcom/peel/h/a/bz;)Lcom/peel/control/a;

    move-result-object v0

    iget-object v2, p0, Lcom/peel/h/a/ce;->a:Lcom/peel/h/a/bz;

    invoke-static {v2}, Lcom/peel/h/a/bz;->j(Lcom/peel/h/a/bz;)Lcom/peel/control/h;

    move-result-object v2

    iget-object v5, p0, Lcom/peel/h/a/ce;->a:Lcom/peel/h/a/bz;

    invoke-static {v5}, Lcom/peel/h/a/bz;->q(Lcom/peel/h/a/bz;)Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/peel/h/a/ce;->a:Lcom/peel/h/a/bz;

    invoke-static {v6}, Lcom/peel/h/a/bz;->p(Lcom/peel/h/a/bz;)Lcom/peel/control/a;

    move-result-object v6

    iget-object v7, p0, Lcom/peel/h/a/ce;->a:Lcom/peel/h/a/bz;

    invoke-static {v7}, Lcom/peel/h/a/bz;->j(Lcom/peel/h/a/bz;)Lcom/peel/control/h;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/peel/control/a;->b(Lcom/peel/control/h;)[Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v0, v2, v5, v6}, Lcom/peel/control/a;->b(Lcom/peel/control/h;Ljava/lang/String;[Ljava/lang/Integer;)V

    .line 609
    :cond_3
    :goto_8
    iget-object v0, p0, Lcom/peel/h/a/ce;->a:Lcom/peel/h/a/bz;

    invoke-static {v0}, Lcom/peel/h/a/bz;->e(Lcom/peel/h/a/bz;)Lcom/peel/control/h;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 610
    iget-object v0, p0, Lcom/peel/h/a/ce;->a:Lcom/peel/h/a/bz;

    invoke-static {v0}, Lcom/peel/h/a/bz;->p(Lcom/peel/h/a/bz;)Lcom/peel/control/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/control/a;->e()[Lcom/peel/control/h;

    move-result-object v2

    array-length v5, v2

    move v0, v1

    :goto_9
    if-ge v0, v5, :cond_22

    aget-object v6, v2, v0

    .line 611
    invoke-virtual {v6}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v6

    invoke-virtual {v6}, Lcom/peel/data/g;->b()Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lcom/peel/h/a/ce;->a:Lcom/peel/h/a/bz;

    invoke-static {v7}, Lcom/peel/h/a/bz;->e(Lcom/peel/h/a/bz;)Lcom/peel/control/h;

    move-result-object v7

    invoke-virtual {v7}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v7

    invoke-virtual {v7}, Lcom/peel/data/g;->b()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1b

    move v0, v3

    .line 617
    :goto_a
    if-eqz v0, :cond_1e

    .line 618
    iget-object v0, p0, Lcom/peel/h/a/ce;->a:Lcom/peel/h/a/bz;

    invoke-static {v0}, Lcom/peel/h/a/bz;->r(Lcom/peel/h/a/bz;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1d

    .line 622
    iget-object v0, p0, Lcom/peel/h/a/ce;->a:Lcom/peel/h/a/bz;

    invoke-static {v0}, Lcom/peel/h/a/bz;->p(Lcom/peel/h/a/bz;)Lcom/peel/control/a;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/peel/control/a;->a(I)Lcom/peel/control/h;

    move-result-object v0

    .line 623
    if-eqz v0, :cond_1c

    invoke-virtual {v0}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/data/g;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/peel/h/a/ce;->a:Lcom/peel/h/a/bz;

    invoke-static {v2}, Lcom/peel/h/a/bz;->e(Lcom/peel/h/a/bz;)Lcom/peel/control/h;

    move-result-object v2

    invoke-virtual {v2}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v2

    invoke-virtual {v2}, Lcom/peel/data/g;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1c

    .line 638
    :cond_4
    :goto_b
    iget-object v0, p0, Lcom/peel/h/a/ce;->a:Lcom/peel/h/a/bz;

    invoke-virtual {v0}, Lcom/peel/h/a/bz;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/ae;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/peel/social/w;->f(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 639
    new-instance v0, Lcom/peel/backup/c;

    iget-object v2, p0, Lcom/peel/h/a/ce;->a:Lcom/peel/h/a/bz;

    invoke-virtual {v2}, Lcom/peel/h/a/bz;->m()Landroid/support/v4/app/ae;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/ae;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/peel/backup/c;-><init>(Landroid/content/Context;)V

    .line 640
    iget-object v2, p0, Lcom/peel/h/a/ce;->a:Lcom/peel/h/a/bz;

    invoke-static {v2}, Lcom/peel/h/a/bz;->f(Lcom/peel/h/a/bz;)Lcom/peel/control/RoomControl;

    move-result-object v2

    iget-object v5, p0, Lcom/peel/h/a/ce;->a:Lcom/peel/h/a/bz;

    iget-object v5, v5, Lcom/peel/h/a/bz;->c:Landroid/os/Bundle;

    const-string/jumbo v6, "providername"

    invoke-virtual {v5, v6, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v2, v4}, Lcom/peel/backup/c;->a(Lcom/peel/control/RoomControl;Ljava/lang/String;)V

    .line 642
    :cond_5
    iget-object v0, p0, Lcom/peel/h/a/ce;->a:Lcom/peel/h/a/bz;

    invoke-static {v0}, Lcom/peel/h/a/bz;->s(Lcom/peel/h/a/bz;)[Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_21

    .line 643
    invoke-static {}, Lcom/peel/h/a/bz;->c()Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "tokens length: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v4, p0, Lcom/peel/h/a/ce;->a:Lcom/peel/h/a/bz;

    invoke-static {v4}, Lcom/peel/h/a/bz;->s(Lcom/peel/h/a/bz;)[Ljava/lang/String;

    move-result-object v4

    array-length v4, v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 644
    :goto_c
    iget-object v0, p0, Lcom/peel/h/a/ce;->a:Lcom/peel/h/a/bz;

    invoke-static {v0}, Lcom/peel/h/a/bz;->s(Lcom/peel/h/a/bz;)[Ljava/lang/String;

    move-result-object v0

    array-length v0, v0

    if-ge v1, v0, :cond_1f

    .line 645
    invoke-static {}, Lcom/peel/h/a/bz;->c()Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "token["

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v4, "]: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v4, p0, Lcom/peel/h/a/ce;->a:Lcom/peel/h/a/bz;

    invoke-static {v4}, Lcom/peel/h/a/bz;->s(Lcom/peel/h/a/bz;)[Ljava/lang/String;

    move-result-object v4

    aget-object v4, v4, v1

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 644
    add-int/lit8 v1, v1, 0x1

    goto :goto_c

    .line 454
    :cond_6
    iget-object v0, p0, Lcom/peel/h/a/ce;->a:Lcom/peel/h/a/bz;

    invoke-static {v0}, Lcom/peel/h/a/bz;->j(Lcom/peel/h/a/bz;)Lcom/peel/control/h;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/peel/h/a/ce;->a:Lcom/peel/h/a/bz;

    iget-object v2, p0, Lcom/peel/h/a/ce;->a:Lcom/peel/h/a/bz;

    invoke-static {v2}, Lcom/peel/h/a/bz;->j(Lcom/peel/h/a/bz;)Lcom/peel/control/h;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/peel/h/a/bz;->a(Lcom/peel/h/a/bz;Lcom/peel/control/h;)Lcom/peel/control/h;

    goto/16 :goto_1

    .line 475
    :cond_7
    iget-object v0, p0, Lcom/peel/h/a/ce;->a:Lcom/peel/h/a/bz;

    invoke-static {v0}, Lcom/peel/h/a/bz;->n(Lcom/peel/h/a/bz;)Lcom/peel/control/h;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/data/g;->f()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    .line 476
    :cond_8
    iget-object v0, p0, Lcom/peel/h/a/ce;->a:Lcom/peel/h/a/bz;

    invoke-static {v0}, Lcom/peel/h/a/bz;->o(Lcom/peel/h/a/bz;)Lcom/peel/control/h;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/data/g;->f()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_3

    .line 492
    :cond_9
    iget-object v0, p0, Lcom/peel/h/a/ce;->a:Lcom/peel/h/a/bz;

    invoke-static {v0}, Lcom/peel/h/a/bz;->p(Lcom/peel/h/a/bz;)Lcom/peel/control/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/control/a;->e()[Lcom/peel/control/h;

    move-result-object v2

    array-length v5, v2

    move v0, v1

    :goto_d
    if-ge v0, v5, :cond_28

    aget-object v6, v2, v0

    .line 493
    invoke-virtual {v6}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v6

    invoke-virtual {v6}, Lcom/peel/data/g;->d()I

    move-result v6

    if-ne v6, v3, :cond_a

    move v0, v3

    .line 498
    :goto_e
    if-eqz v0, :cond_b

    .line 499
    iget-object v0, p0, Lcom/peel/h/a/ce;->a:Lcom/peel/h/a/bz;

    invoke-static {v0}, Lcom/peel/h/a/bz;->p(Lcom/peel/h/a/bz;)Lcom/peel/control/a;

    move-result-object v0

    iget-object v2, p0, Lcom/peel/h/a/ce;->a:Lcom/peel/h/a/bz;

    invoke-static {v2}, Lcom/peel/h/a/bz;->o(Lcom/peel/h/a/bz;)Lcom/peel/control/h;

    move-result-object v2

    iget-object v5, p0, Lcom/peel/h/a/ce;->a:Lcom/peel/h/a/bz;

    invoke-static {v5}, Lcom/peel/h/a/bz;->p(Lcom/peel/h/a/bz;)Lcom/peel/control/a;

    move-result-object v5

    iget-object v6, p0, Lcom/peel/h/a/ce;->a:Lcom/peel/h/a/bz;

    invoke-static {v6}, Lcom/peel/h/a/bz;->o(Lcom/peel/h/a/bz;)Lcom/peel/control/h;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/peel/control/a;->a(Lcom/peel/control/h;)Ljava/lang/String;

    move-result-object v5

    new-array v6, v3, [Ljava/lang/Integer;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v1

    invoke-virtual {v0, v2, v5, v6}, Lcom/peel/control/a;->b(Lcom/peel/control/h;Ljava/lang/String;[Ljava/lang/Integer;)V

    goto/16 :goto_5

    .line 492
    :cond_a
    add-int/lit8 v0, v0, 0x1

    goto :goto_d

    .line 501
    :cond_b
    iget-object v0, p0, Lcom/peel/h/a/ce;->a:Lcom/peel/h/a/bz;

    invoke-static {v0}, Lcom/peel/h/a/bz;->p(Lcom/peel/h/a/bz;)Lcom/peel/control/a;

    move-result-object v0

    iget-object v2, p0, Lcom/peel/h/a/ce;->a:Lcom/peel/h/a/bz;

    invoke-static {v2}, Lcom/peel/h/a/bz;->o(Lcom/peel/h/a/bz;)Lcom/peel/control/h;

    move-result-object v2

    new-array v5, v3, [Ljava/lang/Integer;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v1

    invoke-virtual {v0, v2, v4, v5}, Lcom/peel/control/a;->a(Lcom/peel/control/h;Ljava/lang/String;[Ljava/lang/Integer;)Z

    goto/16 :goto_5

    .line 505
    :cond_c
    const-class v0, Lcom/peel/h/a/bz;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "\n\nnot tv? just add, right?! : "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v5, p0, Lcom/peel/h/a/ce;->a:Lcom/peel/h/a/bz;

    invoke-static {v5}, Lcom/peel/h/a/bz;->p(Lcom/peel/h/a/bz;)Lcom/peel/control/a;

    move-result-object v5

    invoke-virtual {v5}, Lcom/peel/control/a;->a()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v5, " adding... "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v5, p0, Lcom/peel/h/a/ce;->a:Lcom/peel/h/a/bz;

    invoke-static {v5}, Lcom/peel/h/a/bz;->o(Lcom/peel/h/a/bz;)Lcom/peel/control/h;

    move-result-object v5

    invoke-virtual {v5}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v5

    invoke-virtual {v5}, Lcom/peel/data/g;->f()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v5, " as mode audio"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 506
    iget-object v0, p0, Lcom/peel/h/a/ce;->a:Lcom/peel/h/a/bz;

    invoke-static {v0}, Lcom/peel/h/a/bz;->p(Lcom/peel/h/a/bz;)Lcom/peel/control/a;

    move-result-object v0

    iget-object v2, p0, Lcom/peel/h/a/ce;->a:Lcom/peel/h/a/bz;

    invoke-static {v2}, Lcom/peel/h/a/bz;->o(Lcom/peel/h/a/bz;)Lcom/peel/control/h;

    move-result-object v2

    new-array v5, v3, [Ljava/lang/Integer;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v1

    invoke-virtual {v0, v2, v4, v5}, Lcom/peel/control/a;->a(Lcom/peel/control/h;Ljava/lang/String;[Ljava/lang/Integer;)Z

    goto/16 :goto_5

    .line 509
    :cond_d
    iget-object v0, p0, Lcom/peel/h/a/ce;->a:Lcom/peel/h/a/bz;

    invoke-static {v0}, Lcom/peel/h/a/bz;->o(Lcom/peel/h/a/bz;)Lcom/peel/control/h;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/data/g;->d()I

    move-result v0

    if-ne v0, v3, :cond_13

    .line 510
    iget-object v0, p0, Lcom/peel/h/a/ce;->a:Lcom/peel/h/a/bz;

    invoke-static {v0}, Lcom/peel/h/a/bz;->n(Lcom/peel/h/a/bz;)Lcom/peel/control/h;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/data/g;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/peel/h/a/ce;->a:Lcom/peel/h/a/bz;

    invoke-static {v2}, Lcom/peel/h/a/bz;->j(Lcom/peel/h/a/bz;)Lcom/peel/control/h;

    move-result-object v2

    invoke-virtual {v2}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v2

    invoke-virtual {v2}, Lcom/peel/data/g;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 514
    iget-object v0, p0, Lcom/peel/h/a/ce;->a:Lcom/peel/h/a/bz;

    invoke-static {v0}, Lcom/peel/h/a/bz;->p(Lcom/peel/h/a/bz;)Lcom/peel/control/a;

    move-result-object v0

    iget-object v2, p0, Lcom/peel/h/a/ce;->a:Lcom/peel/h/a/bz;

    invoke-static {v2}, Lcom/peel/h/a/bz;->n(Lcom/peel/h/a/bz;)Lcom/peel/control/h;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/peel/control/a;->c(Lcom/peel/control/h;)V

    .line 517
    iget-object v0, p0, Lcom/peel/h/a/ce;->a:Lcom/peel/h/a/bz;

    invoke-static {v0}, Lcom/peel/h/a/bz;->p(Lcom/peel/h/a/bz;)Lcom/peel/control/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/control/a;->e()[Lcom/peel/control/h;

    move-result-object v2

    array-length v5, v2

    move v0, v1

    :goto_f
    if-ge v0, v5, :cond_27

    aget-object v6, v2, v0

    .line 518
    invoke-virtual {v6}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v6

    invoke-virtual {v6}, Lcom/peel/data/g;->b()Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lcom/peel/h/a/ce;->a:Lcom/peel/h/a/bz;

    invoke-static {v7}, Lcom/peel/h/a/bz;->j(Lcom/peel/h/a/bz;)Lcom/peel/control/h;

    move-result-object v7

    invoke-virtual {v7}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v7

    invoke-virtual {v7}, Lcom/peel/data/g;->b()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_e

    move v0, v3

    .line 524
    :goto_10
    if-eqz v0, :cond_12

    .line 525
    iget-object v0, p0, Lcom/peel/h/a/ce;->a:Lcom/peel/h/a/bz;

    invoke-static {v0}, Lcom/peel/h/a/bz;->p(Lcom/peel/h/a/bz;)Lcom/peel/control/a;

    move-result-object v0

    iget-object v2, p0, Lcom/peel/h/a/ce;->a:Lcom/peel/h/a/bz;

    invoke-static {v2}, Lcom/peel/h/a/bz;->j(Lcom/peel/h/a/bz;)Lcom/peel/control/h;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/peel/control/a;->b(Lcom/peel/control/h;)[Ljava/lang/Integer;

    move-result-object v2

    .line 526
    if-nez v2, :cond_f

    .line 527
    new-array v0, v3, [Ljava/lang/Integer;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    .line 528
    iget-object v2, p0, Lcom/peel/h/a/ce;->a:Lcom/peel/h/a/bz;

    invoke-static {v2}, Lcom/peel/h/a/bz;->p(Lcom/peel/h/a/bz;)Lcom/peel/control/a;

    move-result-object v2

    iget-object v5, p0, Lcom/peel/h/a/ce;->a:Lcom/peel/h/a/bz;

    invoke-static {v5}, Lcom/peel/h/a/bz;->j(Lcom/peel/h/a/bz;)Lcom/peel/control/h;

    move-result-object v5

    iget-object v6, p0, Lcom/peel/h/a/ce;->a:Lcom/peel/h/a/bz;

    invoke-static {v6}, Lcom/peel/h/a/bz;->p(Lcom/peel/h/a/bz;)Lcom/peel/control/a;

    move-result-object v6

    iget-object v7, p0, Lcom/peel/h/a/ce;->a:Lcom/peel/h/a/bz;

    invoke-static {v7}, Lcom/peel/h/a/bz;->j(Lcom/peel/h/a/bz;)Lcom/peel/control/h;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/peel/control/a;->a(Lcom/peel/control/h;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v5, v6, v0}, Lcom/peel/control/a;->b(Lcom/peel/control/h;Ljava/lang/String;[Ljava/lang/Integer;)V

    goto/16 :goto_5

    .line 517
    :cond_e
    add-int/lit8 v0, v0, 0x1

    goto :goto_f

    :cond_f
    move v0, v1

    .line 531
    :goto_11
    array-length v5, v2

    if-ge v0, v5, :cond_26

    .line 532
    aget-object v5, v2, v0

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    if-nez v5, :cond_10

    move v0, v3

    .line 537
    :goto_12
    if-eqz v0, :cond_11

    move-object v0, v2

    .line 543
    :goto_13
    iget-object v2, p0, Lcom/peel/h/a/ce;->a:Lcom/peel/h/a/bz;

    invoke-static {v2}, Lcom/peel/h/a/bz;->p(Lcom/peel/h/a/bz;)Lcom/peel/control/a;

    move-result-object v2

    iget-object v5, p0, Lcom/peel/h/a/ce;->a:Lcom/peel/h/a/bz;

    invoke-static {v5}, Lcom/peel/h/a/bz;->j(Lcom/peel/h/a/bz;)Lcom/peel/control/h;

    move-result-object v5

    iget-object v6, p0, Lcom/peel/h/a/ce;->a:Lcom/peel/h/a/bz;

    invoke-static {v6}, Lcom/peel/h/a/bz;->p(Lcom/peel/h/a/bz;)Lcom/peel/control/a;

    move-result-object v6

    iget-object v7, p0, Lcom/peel/h/a/ce;->a:Lcom/peel/h/a/bz;

    invoke-static {v7}, Lcom/peel/h/a/bz;->j(Lcom/peel/h/a/bz;)Lcom/peel/control/h;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/peel/control/a;->a(Lcom/peel/control/h;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v5, v6, v0}, Lcom/peel/control/a;->b(Lcom/peel/control/h;Ljava/lang/String;[Ljava/lang/Integer;)V

    goto/16 :goto_5

    .line 531
    :cond_10
    add-int/lit8 v0, v0, 0x1

    goto :goto_11

    .line 541
    :cond_11
    new-array v0, v8, [Ljava/lang/Integer;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v3

    goto :goto_13

    .line 546
    :cond_12
    iget-object v0, p0, Lcom/peel/h/a/ce;->a:Lcom/peel/h/a/bz;

    invoke-static {v0}, Lcom/peel/h/a/bz;->p(Lcom/peel/h/a/bz;)Lcom/peel/control/a;

    move-result-object v0

    iget-object v2, p0, Lcom/peel/h/a/ce;->a:Lcom/peel/h/a/bz;

    invoke-static {v2}, Lcom/peel/h/a/bz;->j(Lcom/peel/h/a/bz;)Lcom/peel/control/h;

    move-result-object v2

    new-array v5, v3, [Ljava/lang/Integer;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v1

    invoke-virtual {v0, v2, v4, v5}, Lcom/peel/control/a;->a(Lcom/peel/control/h;Ljava/lang/String;[Ljava/lang/Integer;)Z

    goto/16 :goto_5

    .line 549
    :cond_13
    iget-object v0, p0, Lcom/peel/h/a/ce;->a:Lcom/peel/h/a/bz;

    invoke-static {v0}, Lcom/peel/h/a/bz;->o(Lcom/peel/h/a/bz;)Lcom/peel/control/h;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/data/g;->d()I

    move-result v0

    const/4 v2, 0x5

    if-ne v0, v2, :cond_2

    .line 550
    invoke-static {}, Lcom/peel/h/a/bz;->c()Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "current audio: "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v5, p0, Lcom/peel/h/a/ce;->a:Lcom/peel/h/a/bz;

    invoke-static {v5}, Lcom/peel/h/a/bz;->n(Lcom/peel/h/a/bz;)Lcom/peel/control/h;

    move-result-object v5

    invoke-virtual {v5}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v5

    invoke-virtual {v5}, Lcom/peel/data/g;->f()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 551
    invoke-static {}, Lcom/peel/h/a/bz;->c()Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "volume device: "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v5, p0, Lcom/peel/h/a/ce;->a:Lcom/peel/h/a/bz;

    invoke-static {v5}, Lcom/peel/h/a/bz;->o(Lcom/peel/h/a/bz;)Lcom/peel/control/h;

    move-result-object v5

    invoke-virtual {v5}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v5

    invoke-virtual {v5}, Lcom/peel/data/g;->f()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 552
    iget-object v0, p0, Lcom/peel/h/a/ce;->a:Lcom/peel/h/a/bz;

    invoke-static {v0}, Lcom/peel/h/a/bz;->n(Lcom/peel/h/a/bz;)Lcom/peel/control/h;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/data/g;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/peel/h/a/ce;->a:Lcom/peel/h/a/bz;

    invoke-static {v2}, Lcom/peel/h/a/bz;->e(Lcom/peel/h/a/bz;)Lcom/peel/control/h;

    move-result-object v2

    invoke-virtual {v2}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v2

    invoke-virtual {v2}, Lcom/peel/data/g;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 555
    iget-object v0, p0, Lcom/peel/h/a/ce;->a:Lcom/peel/h/a/bz;

    invoke-static {v0}, Lcom/peel/h/a/bz;->n(Lcom/peel/h/a/bz;)Lcom/peel/control/h;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/data/g;->d()I

    move-result v0

    if-ne v0, v3, :cond_16

    .line 558
    iget-object v0, p0, Lcom/peel/h/a/ce;->a:Lcom/peel/h/a/bz;

    invoke-static {v0}, Lcom/peel/h/a/bz;->p(Lcom/peel/h/a/bz;)Lcom/peel/control/a;

    move-result-object v0

    iget-object v2, p0, Lcom/peel/h/a/ce;->a:Lcom/peel/h/a/bz;

    invoke-static {v2}, Lcom/peel/h/a/bz;->j(Lcom/peel/h/a/bz;)Lcom/peel/control/h;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/peel/control/a;->b(Lcom/peel/control/h;)[Ljava/lang/Integer;

    move-result-object v2

    .line 560
    if-eqz v2, :cond_25

    move v0, v1

    .line 561
    :goto_14
    array-length v5, v2

    if-ge v0, v5, :cond_25

    .line 562
    aget-object v5, v2, v0

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    if-ne v5, v3, :cond_14

    move v0, v3

    .line 568
    :goto_15
    iget-object v2, p0, Lcom/peel/h/a/ce;->a:Lcom/peel/h/a/bz;

    invoke-static {v2}, Lcom/peel/h/a/bz;->p(Lcom/peel/h/a/bz;)Lcom/peel/control/a;

    move-result-object v2

    iget-object v5, p0, Lcom/peel/h/a/ce;->a:Lcom/peel/h/a/bz;

    invoke-static {v5}, Lcom/peel/h/a/bz;->j(Lcom/peel/h/a/bz;)Lcom/peel/control/h;

    move-result-object v5

    iget-object v6, p0, Lcom/peel/h/a/ce;->a:Lcom/peel/h/a/bz;

    invoke-static {v6}, Lcom/peel/h/a/bz;->p(Lcom/peel/h/a/bz;)Lcom/peel/control/a;

    move-result-object v6

    iget-object v7, p0, Lcom/peel/h/a/ce;->a:Lcom/peel/h/a/bz;

    invoke-static {v7}, Lcom/peel/h/a/bz;->j(Lcom/peel/h/a/bz;)Lcom/peel/control/h;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/peel/control/a;->a(Lcom/peel/control/h;)Ljava/lang/String;

    move-result-object v6

    if-eqz v0, :cond_15

    new-array v0, v3, [Ljava/lang/Integer;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v0, v1

    :goto_16
    invoke-virtual {v2, v5, v6, v0}, Lcom/peel/control/a;->b(Lcom/peel/control/h;Ljava/lang/String;[Ljava/lang/Integer;)V

    .line 574
    :goto_17
    iget-object v0, p0, Lcom/peel/h/a/ce;->a:Lcom/peel/h/a/bz;

    invoke-static {v0}, Lcom/peel/h/a/bz;->p(Lcom/peel/h/a/bz;)Lcom/peel/control/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/control/a;->e()[Lcom/peel/control/h;

    move-result-object v2

    array-length v5, v2

    move v0, v1

    :goto_18
    if-ge v0, v5, :cond_24

    aget-object v6, v2, v0

    .line 575
    invoke-virtual {v6}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v6

    invoke-virtual {v6}, Lcom/peel/data/g;->b()Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lcom/peel/h/a/ce;->a:Lcom/peel/h/a/bz;

    invoke-static {v7}, Lcom/peel/h/a/bz;->o(Lcom/peel/h/a/bz;)Lcom/peel/control/h;

    move-result-object v7

    invoke-virtual {v7}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v7

    invoke-virtual {v7}, Lcom/peel/data/g;->b()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_17

    move v0, v3

    .line 580
    :goto_19
    if-eqz v0, :cond_18

    .line 581
    iget-object v0, p0, Lcom/peel/h/a/ce;->a:Lcom/peel/h/a/bz;

    invoke-static {v0}, Lcom/peel/h/a/bz;->p(Lcom/peel/h/a/bz;)Lcom/peel/control/a;

    move-result-object v0

    iget-object v2, p0, Lcom/peel/h/a/ce;->a:Lcom/peel/h/a/bz;

    invoke-static {v2}, Lcom/peel/h/a/bz;->o(Lcom/peel/h/a/bz;)Lcom/peel/control/h;

    move-result-object v2

    iget-object v5, p0, Lcom/peel/h/a/ce;->a:Lcom/peel/h/a/bz;

    invoke-static {v5}, Lcom/peel/h/a/bz;->p(Lcom/peel/h/a/bz;)Lcom/peel/control/a;

    move-result-object v5

    iget-object v6, p0, Lcom/peel/h/a/ce;->a:Lcom/peel/h/a/bz;

    invoke-static {v6}, Lcom/peel/h/a/bz;->o(Lcom/peel/h/a/bz;)Lcom/peel/control/h;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/peel/control/a;->a(Lcom/peel/control/h;)Ljava/lang/String;

    move-result-object v5

    new-array v6, v3, [Ljava/lang/Integer;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v1

    invoke-virtual {v0, v2, v5, v6}, Lcom/peel/control/a;->b(Lcom/peel/control/h;Ljava/lang/String;[Ljava/lang/Integer;)V

    goto/16 :goto_5

    .line 561
    :cond_14
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_14

    :cond_15
    move-object v0, v4

    .line 568
    goto :goto_16

    .line 570
    :cond_16
    iget-object v0, p0, Lcom/peel/h/a/ce;->a:Lcom/peel/h/a/bz;

    invoke-static {v0}, Lcom/peel/h/a/bz;->p(Lcom/peel/h/a/bz;)Lcom/peel/control/a;

    move-result-object v0

    iget-object v2, p0, Lcom/peel/h/a/ce;->a:Lcom/peel/h/a/bz;

    invoke-static {v2}, Lcom/peel/h/a/bz;->n(Lcom/peel/h/a/bz;)Lcom/peel/control/h;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/peel/control/a;->c(Lcom/peel/control/h;)V

    goto :goto_17

    .line 574
    :cond_17
    add-int/lit8 v0, v0, 0x1

    goto :goto_18

    .line 583
    :cond_18
    iget-object v0, p0, Lcom/peel/h/a/ce;->a:Lcom/peel/h/a/bz;

    invoke-static {v0}, Lcom/peel/h/a/bz;->p(Lcom/peel/h/a/bz;)Lcom/peel/control/a;

    move-result-object v0

    iget-object v2, p0, Lcom/peel/h/a/ce;->a:Lcom/peel/h/a/bz;

    invoke-static {v2}, Lcom/peel/h/a/bz;->o(Lcom/peel/h/a/bz;)Lcom/peel/control/h;

    move-result-object v2

    new-array v5, v3, [Ljava/lang/Integer;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v1

    invoke-virtual {v0, v2, v4, v5}, Lcom/peel/control/a;->a(Lcom/peel/control/h;Ljava/lang/String;[Ljava/lang/Integer;)Z

    goto/16 :goto_5

    .line 592
    :cond_19
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_6

    .line 603
    :cond_1a
    iget-object v0, p0, Lcom/peel/h/a/ce;->a:Lcom/peel/h/a/bz;

    invoke-static {v0}, Lcom/peel/h/a/bz;->p(Lcom/peel/h/a/bz;)Lcom/peel/control/a;

    move-result-object v0

    iget-object v2, p0, Lcom/peel/h/a/ce;->a:Lcom/peel/h/a/bz;

    invoke-static {v2}, Lcom/peel/h/a/bz;->j(Lcom/peel/h/a/bz;)Lcom/peel/control/h;

    move-result-object v2

    iget-object v5, p0, Lcom/peel/h/a/ce;->a:Lcom/peel/h/a/bz;

    invoke-static {v5}, Lcom/peel/h/a/bz;->q(Lcom/peel/h/a/bz;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v2, v5, v4}, Lcom/peel/control/a;->a(Lcom/peel/control/h;Ljava/lang/String;[Ljava/lang/Integer;)Z

    goto/16 :goto_8

    .line 610
    :cond_1b
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_9

    .line 626
    :cond_1c
    iget-object v0, p0, Lcom/peel/h/a/ce;->a:Lcom/peel/h/a/bz;

    invoke-static {v0}, Lcom/peel/h/a/bz;->p(Lcom/peel/h/a/bz;)Lcom/peel/control/a;

    move-result-object v0

    iget-object v2, p0, Lcom/peel/h/a/ce;->a:Lcom/peel/h/a/bz;

    invoke-static {v2}, Lcom/peel/h/a/bz;->e(Lcom/peel/h/a/bz;)Lcom/peel/control/h;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/peel/control/a;->c(Lcom/peel/control/h;)V

    goto/16 :goto_b

    .line 629
    :cond_1d
    iget-object v0, p0, Lcom/peel/h/a/ce;->a:Lcom/peel/h/a/bz;

    invoke-static {v0}, Lcom/peel/h/a/bz;->p(Lcom/peel/h/a/bz;)Lcom/peel/control/a;

    move-result-object v0

    iget-object v2, p0, Lcom/peel/h/a/ce;->a:Lcom/peel/h/a/bz;

    invoke-static {v2}, Lcom/peel/h/a/bz;->e(Lcom/peel/h/a/bz;)Lcom/peel/control/h;

    move-result-object v2

    iget-object v5, p0, Lcom/peel/h/a/ce;->a:Lcom/peel/h/a/bz;

    invoke-static {v5}, Lcom/peel/h/a/bz;->r(Lcom/peel/h/a/bz;)Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/peel/h/a/ce;->a:Lcom/peel/h/a/bz;

    invoke-static {v6}, Lcom/peel/h/a/bz;->p(Lcom/peel/h/a/bz;)Lcom/peel/control/a;

    move-result-object v6

    iget-object v7, p0, Lcom/peel/h/a/ce;->a:Lcom/peel/h/a/bz;

    invoke-static {v7}, Lcom/peel/h/a/bz;->e(Lcom/peel/h/a/bz;)Lcom/peel/control/h;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/peel/control/a;->b(Lcom/peel/control/h;)[Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v0, v2, v5, v6}, Lcom/peel/control/a;->b(Lcom/peel/control/h;Ljava/lang/String;[Ljava/lang/Integer;)V

    goto/16 :goto_b

    .line 634
    :cond_1e
    iget-object v0, p0, Lcom/peel/h/a/ce;->a:Lcom/peel/h/a/bz;

    invoke-static {v0}, Lcom/peel/h/a/bz;->r(Lcom/peel/h/a/bz;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/peel/h/a/ce;->a:Lcom/peel/h/a/bz;

    invoke-static {v0}, Lcom/peel/h/a/bz;->p(Lcom/peel/h/a/bz;)Lcom/peel/control/a;

    move-result-object v0

    iget-object v2, p0, Lcom/peel/h/a/ce;->a:Lcom/peel/h/a/bz;

    invoke-static {v2}, Lcom/peel/h/a/bz;->e(Lcom/peel/h/a/bz;)Lcom/peel/control/h;

    move-result-object v2

    iget-object v5, p0, Lcom/peel/h/a/ce;->a:Lcom/peel/h/a/bz;

    invoke-static {v5}, Lcom/peel/h/a/bz;->r(Lcom/peel/h/a/bz;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v2, v5, v4}, Lcom/peel/control/a;->a(Lcom/peel/control/h;Ljava/lang/String;[Ljava/lang/Integer;)Z

    goto/16 :goto_b

    .line 648
    :cond_1f
    iget-object v0, p0, Lcom/peel/h/a/ce;->a:Lcom/peel/h/a/bz;

    invoke-static {v0}, Lcom/peel/h/a/bz;->s(Lcom/peel/h/a/bz;)[Ljava/lang/String;

    move-result-object v0

    array-length v0, v0

    if-ne v0, v3, :cond_20

    .line 651
    invoke-static {}, Lcom/peel/h/a/bz;->c()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/h/a/ce;->a:Lcom/peel/h/a/bz;

    invoke-virtual {v1}, Lcom/peel/h/a/bz;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/peel/d/e;->a(Ljava/lang/String;Landroid/support/v4/app/ae;)V

    goto/16 :goto_0

    .line 653
    :cond_20
    iget-object v0, p0, Lcom/peel/h/a/ce;->a:Lcom/peel/h/a/bz;

    iget-object v1, p0, Lcom/peel/h/a/ce;->a:Lcom/peel/h/a/bz;

    invoke-static {v1}, Lcom/peel/h/a/bz;->s(Lcom/peel/h/a/bz;)[Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/peel/h/a/bz;->a(Lcom/peel/h/a/bz;[Ljava/lang/String;)V

    goto/16 :goto_0

    .line 657
    :cond_21
    invoke-static {}, Lcom/peel/h/a/bz;->c()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/h/a/ce;->a:Lcom/peel/h/a/bz;

    invoke-virtual {v1}, Lcom/peel/h/a/bz;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/peel/d/e;->a(Ljava/lang/String;Landroid/support/v4/app/ae;)V

    goto/16 :goto_0

    :cond_22
    move v0, v1

    goto/16 :goto_a

    :cond_23
    move v0, v1

    goto/16 :goto_7

    :cond_24
    move v0, v1

    goto/16 :goto_19

    :cond_25
    move v0, v1

    goto/16 :goto_15

    :cond_26
    move v0, v1

    goto/16 :goto_12

    :cond_27
    move v0, v1

    goto/16 :goto_10

    :cond_28
    move v0, v1

    goto/16 :goto_e

    :cond_29
    move v0, v1

    goto/16 :goto_4
.end method
