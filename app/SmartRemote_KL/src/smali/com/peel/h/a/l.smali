.class Lcom/peel/h/a/l;
.super Lcom/peel/util/t;


# instance fields
.field final synthetic a:Lcom/peel/h/a/a;


# direct methods
.method constructor <init>(Lcom/peel/h/a/a;I)V
    .locals 0

    .prologue
    .line 927
    iput-object p1, p0, Lcom/peel/h/a/l;->a:Lcom/peel/h/a/a;

    invoke-direct {p0, p2}, Lcom/peel/util/t;-><init>(I)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    const/16 v7, 0x8

    const/4 v2, 0x4

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 931
    iget-boolean v0, p0, Lcom/peel/h/a/l;->i:Z

    if-nez v0, :cond_2

    .line 932
    iget-object v0, p0, Lcom/peel/h/a/l;->k:Ljava/lang/String;

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/peel/h/a/a;->c()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/h/a/l;->k:Ljava/lang/String;

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 933
    :cond_0
    iget-object v0, p0, Lcom/peel/h/a/l;->a:Lcom/peel/h/a/a;

    invoke-virtual {v0}, Lcom/peel/h/a/a;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/support/v4/app/ae;->setProgressBarIndeterminateVisibility(Z)V

    .line 934
    sget-boolean v0, Lcom/peel/util/b/a;->a:Z

    if-eqz v0, :cond_1

    .line 935
    iget-object v0, p0, Lcom/peel/h/a/l;->a:Lcom/peel/h/a/a;

    invoke-static {v0}, Lcom/peel/h/a/a;->e(Lcom/peel/h/a/a;)V

    .line 1040
    :goto_0
    return-void

    .line 937
    :cond_1
    iget-object v0, p0, Lcom/peel/h/a/l;->a:Lcom/peel/h/a/a;

    invoke-static {v0}, Lcom/peel/h/a/a;->f(Lcom/peel/h/a/a;)V

    goto :goto_0

    .line 939
    :cond_2
    iget-object v0, p0, Lcom/peel/h/a/l;->a:Lcom/peel/h/a/a;

    invoke-virtual {v0}, Lcom/peel/h/a/a;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/support/v4/app/ae;->setProgressBarIndeterminateVisibility(Z)V

    .line 940
    iget-object v1, p0, Lcom/peel/h/a/l;->a:Lcom/peel/h/a/a;

    iget-object v0, p0, Lcom/peel/h/a/l;->j:Ljava/lang/Object;

    check-cast v0, Ljava/util/ArrayList;

    invoke-static {v1, v0}, Lcom/peel/h/a/a;->a(Lcom/peel/h/a/a;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    .line 941
    iget-object v0, p0, Lcom/peel/h/a/l;->a:Lcom/peel/h/a/a;

    invoke-static {v0}, Lcom/peel/h/a/a;->t(Lcom/peel/h/a/a;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ne v0, v6, :cond_3

    .line 942
    iget-object v0, p0, Lcom/peel/h/a/l;->a:Lcom/peel/h/a/a;

    invoke-static {v0}, Lcom/peel/h/a/a;->D(Lcom/peel/h/a/a;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 943
    iget-object v0, p0, Lcom/peel/h/a/l;->a:Lcom/peel/h/a/a;

    invoke-static {v0}, Lcom/peel/h/a/a;->E(Lcom/peel/h/a/a;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 945
    :cond_3
    iget-object v0, p0, Lcom/peel/h/a/l;->a:Lcom/peel/h/a/a;

    invoke-static {v0, v5}, Lcom/peel/h/a/a;->b(Lcom/peel/h/a/a;I)I

    .line 946
    iget-object v1, p0, Lcom/peel/h/a/l;->a:Lcom/peel/h/a/a;

    iget-object v0, p0, Lcom/peel/h/a/l;->a:Lcom/peel/h/a/a;

    invoke-static {v0}, Lcom/peel/h/a/a;->t(Lcom/peel/h/a/a;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    const-string/jumbo v2, "codesetid"

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v1, v0}, Lcom/peel/h/a/a;->c(Lcom/peel/h/a/a;I)I

    .line 947
    iget-object v1, p0, Lcom/peel/h/a/l;->a:Lcom/peel/h/a/a;

    iget-object v0, p0, Lcom/peel/h/a/l;->a:Lcom/peel/h/a/a;

    invoke-static {v0}, Lcom/peel/h/a/a;->t(Lcom/peel/h/a/a;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    const-string/jumbo v2, "funName"

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v1, v0}, Lcom/peel/h/a/a;->b(Lcom/peel/h/a/a;Ljava/lang/String;)Ljava/lang/String;

    .line 948
    iget-object v0, p0, Lcom/peel/h/a/l;->a:Lcom/peel/h/a/a;

    invoke-static {v0}, Lcom/peel/h/a/a;->p(Lcom/peel/h/a/a;)Lcom/peel/control/h;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v1

    iget-object v0, p0, Lcom/peel/h/a/l;->a:Lcom/peel/h/a/a;

    invoke-static {v0}, Lcom/peel/h/a/a;->F(Lcom/peel/h/a/a;)Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Lcom/peel/h/a/l;->a:Lcom/peel/h/a/a;

    invoke-static {v0}, Lcom/peel/h/a/a;->t(Lcom/peel/h/a/a;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    invoke-virtual {v1, v2, v0}, Lcom/peel/data/g;->a(Ljava/lang/String;Ljava/util/Map;)V

    .line 950
    iget-object v0, p0, Lcom/peel/h/a/l;->a:Lcom/peel/h/a/a;

    invoke-static {v0}, Lcom/peel/h/a/a;->t(Lcom/peel/h/a/a;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    .line 951
    iget-object v0, p0, Lcom/peel/h/a/l;->a:Lcom/peel/h/a/a;

    new-instance v1, Lcom/peel/h/a/ah;

    iget-object v2, p0, Lcom/peel/h/a/l;->a:Lcom/peel/h/a/a;

    iget-object v3, p0, Lcom/peel/h/a/l;->a:Lcom/peel/h/a/a;

    invoke-static {v3}, Lcom/peel/h/a/a;->d(Lcom/peel/h/a/a;)I

    move-result v3

    iget-object v4, p0, Lcom/peel/h/a/l;->a:Lcom/peel/h/a/a;

    invoke-static {v4}, Lcom/peel/h/a/a;->t(Lcom/peel/h/a/a;)Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    invoke-direct {v1, v2, v3, v4}, Lcom/peel/h/a/ah;-><init>(Lcom/peel/h/a/a;II)V

    invoke-static {v0, v1}, Lcom/peel/h/a/a;->a(Lcom/peel/h/a/a;Lcom/peel/h/a/ah;)Lcom/peel/h/a/ah;

    .line 952
    iget-object v0, p0, Lcom/peel/h/a/l;->a:Lcom/peel/h/a/a;

    invoke-static {v0}, Lcom/peel/h/a/a;->u(Lcom/peel/h/a/a;)Lcom/peel/widget/TestBtnViewPager;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/h/a/l;->a:Lcom/peel/h/a/a;

    invoke-static {v1}, Lcom/peel/h/a/a;->G(Lcom/peel/h/a/a;)Lcom/peel/h/a/ah;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/peel/widget/TestBtnViewPager;->setAdapter(Landroid/support/v4/view/av;)V

    .line 953
    iget-object v0, p0, Lcom/peel/h/a/l;->a:Lcom/peel/h/a/a;

    invoke-static {v0}, Lcom/peel/h/a/a;->u(Lcom/peel/h/a/a;)Lcom/peel/widget/TestBtnViewPager;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/h/a/l;->a:Lcom/peel/h/a/a;

    invoke-virtual {v1}, Lcom/peel/h/a/a;->n()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/peel/ui/fn;->test_btn_pager_margin:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    mul-int/lit8 v1, v1, 0x2

    iget-object v2, p0, Lcom/peel/h/a/l;->a:Lcom/peel/h/a/a;

    invoke-virtual {v2}, Lcom/peel/h/a/a;->n()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/peel/ui/fn;->test_btn_pager_margin:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v2

    mul-int/lit8 v2, v2, 0x2

    invoke-virtual {v0, v1, v5, v2, v5}, Lcom/peel/widget/TestBtnViewPager;->setPadding(IIII)V

    .line 954
    iget-object v0, p0, Lcom/peel/h/a/l;->a:Lcom/peel/h/a/a;

    invoke-static {v0}, Lcom/peel/h/a/a;->u(Lcom/peel/h/a/a;)Lcom/peel/widget/TestBtnViewPager;

    move-result-object v0

    invoke-virtual {v0, v5}, Lcom/peel/widget/TestBtnViewPager;->setClipToPadding(Z)V

    .line 955
    iget-object v0, p0, Lcom/peel/h/a/l;->a:Lcom/peel/h/a/a;

    invoke-static {v0}, Lcom/peel/h/a/a;->u(Lcom/peel/h/a/a;)Lcom/peel/widget/TestBtnViewPager;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/h/a/l;->a:Lcom/peel/h/a/a;

    invoke-virtual {v1}, Lcom/peel/h/a/a;->n()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/peel/ui/fn;->test_btn_pager_margin:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/peel/widget/TestBtnViewPager;->setPageMargin(I)V

    .line 956
    iget-object v0, p0, Lcom/peel/h/a/l;->a:Lcom/peel/h/a/a;

    invoke-static {v0}, Lcom/peel/h/a/a;->u(Lcom/peel/h/a/a;)Lcom/peel/widget/TestBtnViewPager;

    move-result-object v0

    invoke-virtual {v0, v5}, Lcom/peel/widget/TestBtnViewPager;->setClipChildren(Z)V

    .line 957
    iget-object v0, p0, Lcom/peel/h/a/l;->a:Lcom/peel/h/a/a;

    invoke-static {v0}, Lcom/peel/h/a/a;->u(Lcom/peel/h/a/a;)Lcom/peel/widget/TestBtnViewPager;

    move-result-object v0

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/peel/widget/TestBtnViewPager;->setOffscreenPageLimit(I)V

    .line 958
    iget-object v0, p0, Lcom/peel/h/a/l;->a:Lcom/peel/h/a/a;

    invoke-static {v0}, Lcom/peel/h/a/a;->u(Lcom/peel/h/a/a;)Lcom/peel/widget/TestBtnViewPager;

    move-result-object v0

    invoke-virtual {v0, v5}, Lcom/peel/widget/TestBtnViewPager;->setVisibility(I)V

    .line 960
    iget-object v0, p0, Lcom/peel/h/a/l;->a:Lcom/peel/h/a/a;

    invoke-static {v0}, Lcom/peel/h/a/a;->D(Lcom/peel/h/a/a;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/widget/Button;->setEnabled(Z)V

    .line 961
    iget-object v0, p0, Lcom/peel/h/a/l;->a:Lcom/peel/h/a/a;

    invoke-static {v0}, Lcom/peel/h/a/a;->E(Lcom/peel/h/a/a;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/widget/Button;->setEnabled(Z)V

    .line 964
    :cond_4
    iget-object v0, p0, Lcom/peel/h/a/l;->a:Lcom/peel/h/a/a;

    invoke-static {v0}, Lcom/peel/h/a/a;->d(Lcom/peel/h/a/a;)I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_5

    .line 965
    iget-object v0, p0, Lcom/peel/h/a/l;->a:Lcom/peel/h/a/a;

    invoke-static {v0}, Lcom/peel/h/a/a;->u(Lcom/peel/h/a/a;)Lcom/peel/widget/TestBtnViewPager;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "btnView"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/peel/h/a/l;->a:Lcom/peel/h/a/a;

    invoke-static {v2}, Lcom/peel/h/a/a;->H(Lcom/peel/h/a/a;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/peel/widget/TestBtnViewPager;->findViewWithTag(Ljava/lang/Object;)Landroid/view/View;

    move-result-object v0

    sget v1, Lcom/peel/ui/fp;->test_other_btn_large_view:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    .line 966
    iget-object v0, p0, Lcom/peel/h/a/l;->a:Lcom/peel/h/a/a;

    invoke-static {v0}, Lcom/peel/h/a/a;->u(Lcom/peel/h/a/a;)Lcom/peel/widget/TestBtnViewPager;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "btnView"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/peel/h/a/l;->a:Lcom/peel/h/a/a;

    invoke-static {v2}, Lcom/peel/h/a/a;->H(Lcom/peel/h/a/a;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/peel/widget/TestBtnViewPager;->findViewWithTag(Ljava/lang/Object;)Landroid/view/View;

    move-result-object v0

    sget v1, Lcom/peel/ui/fp;->test_other_btn_small_view:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v7}, Landroid/view/View;->setVisibility(I)V

    .line 972
    :goto_1
    iget-object v0, p0, Lcom/peel/h/a/l;->a:Lcom/peel/h/a/a;

    invoke-static {v0}, Lcom/peel/h/a/a;->u(Lcom/peel/h/a/a;)Lcom/peel/widget/TestBtnViewPager;

    move-result-object v0

    new-instance v1, Lcom/peel/h/a/m;

    invoke-direct {v1, p0}, Lcom/peel/h/a/m;-><init>(Lcom/peel/h/a/l;)V

    invoke-virtual {v0, v1}, Lcom/peel/widget/TestBtnViewPager;->setOnPageChangeListener(Landroid/support/v4/view/cs;)V

    .line 1038
    iget-object v0, p0, Lcom/peel/h/a/l;->a:Lcom/peel/h/a/a;

    invoke-static {v0}, Lcom/peel/h/a/a;->J(Lcom/peel/h/a/a;)V

    goto/16 :goto_0

    .line 968
    :cond_5
    iget-object v0, p0, Lcom/peel/h/a/l;->a:Lcom/peel/h/a/a;

    invoke-static {v0}, Lcom/peel/h/a/a;->u(Lcom/peel/h/a/a;)Lcom/peel/widget/TestBtnViewPager;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "btnView"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/peel/h/a/l;->a:Lcom/peel/h/a/a;

    invoke-static {v2}, Lcom/peel/h/a/a;->H(Lcom/peel/h/a/a;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/peel/widget/TestBtnViewPager;->findViewWithTag(Ljava/lang/Object;)Landroid/view/View;

    move-result-object v0

    sget v1, Lcom/peel/ui/fp;->test_pw_btn_large_view:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    .line 969
    iget-object v0, p0, Lcom/peel/h/a/l;->a:Lcom/peel/h/a/a;

    invoke-static {v0}, Lcom/peel/h/a/a;->u(Lcom/peel/h/a/a;)Lcom/peel/widget/TestBtnViewPager;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "btnView"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/peel/h/a/l;->a:Lcom/peel/h/a/a;

    invoke-static {v2}, Lcom/peel/h/a/a;->H(Lcom/peel/h/a/a;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/peel/widget/TestBtnViewPager;->findViewWithTag(Ljava/lang/Object;)Landroid/view/View;

    move-result-object v0

    sget v1, Lcom/peel/ui/fp;->test_pw_btn_small_view:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v7}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1
.end method
