.class Lcom/peel/h/a/bh;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field final synthetic a:Landroid/widget/ListView;

.field final synthetic b:Lcom/peel/h/a/bd;


# direct methods
.method constructor <init>(Lcom/peel/h/a/bd;Landroid/widget/ListView;)V
    .locals 0

    .prologue
    .line 284
    iput-object p1, p0, Lcom/peel/h/a/bh;->b:Lcom/peel/h/a/bd;

    iput-object p2, p0, Lcom/peel/h/a/bh;->a:Landroid/widget/ListView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v5, 0x0

    .line 287
    iget-object v0, p0, Lcom/peel/h/a/bh;->b:Lcom/peel/h/a/bd;

    iget-object v0, v0, Lcom/peel/h/a/bd;->b:Lcom/peel/h/a/bb;

    invoke-static {v0, v1}, Lcom/peel/h/a/bb;->a(Lcom/peel/h/a/bb;Z)Z

    .line 288
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 289
    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 290
    iget-object v0, p0, Lcom/peel/h/a/bh;->b:Lcom/peel/h/a/bd;

    iget-object v0, v0, Lcom/peel/h/a/bd;->b:Lcom/peel/h/a/bb;

    invoke-static {v0}, Lcom/peel/h/a/bb;->g(Lcom/peel/h/a/bb;)Ljava/util/ArrayList;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/h/a/bh;->b:Lcom/peel/h/a/bd;

    iget-object v1, v1, Lcom/peel/h/a/bd;->b:Lcom/peel/h/a/bb;

    invoke-static {v1}, Lcom/peel/h/a/bb;->g(Lcom/peel/h/a/bb;)Ljava/util/ArrayList;

    move-result-object v1

    iget-object v2, p0, Lcom/peel/h/a/bh;->b:Lcom/peel/h/a/bd;

    iget-object v2, v2, Lcom/peel/h/a/bd;->b:Lcom/peel/h/a/bb;

    invoke-static {v2}, Lcom/peel/h/a/bb;->f(Lcom/peel/h/a/bb;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 298
    :goto_0
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 299
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/peel/h/a/bh;->b:Lcom/peel/h/a/bd;

    iget-object v2, v2, Lcom/peel/h/a/bd;->b:Lcom/peel/h/a/bb;

    invoke-static {v2}, Lcom/peel/h/a/bb;->h(Lcom/peel/h/a/bb;)Lcom/peel/data/ContentRoom;

    move-result-object v2

    invoke-virtual {v2}, Lcom/peel/data/ContentRoom;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/peel/h/a/bh;->b:Lcom/peel/h/a/bd;

    iget-object v2, v2, Lcom/peel/h/a/bd;->b:Lcom/peel/h/a/bb;

    invoke-static {v2}, Lcom/peel/h/a/bb;->e(Lcom/peel/h/a/bb;)Lcom/peel/content/library/LiveLibrary;

    move-result-object v2

    invoke-virtual {v2}, Lcom/peel/content/library/LiveLibrary;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/peel/h/a/bh;->b:Lcom/peel/h/a/bd;

    iget-object v2, v2, Lcom/peel/h/a/bd;->b:Lcom/peel/h/a/bb;

    invoke-static {v2}, Lcom/peel/h/a/bb;->g(Lcom/peel/h/a/bb;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 300
    sget-object v1, Lcom/peel/content/a;->f:Lcom/peel/content/user/User;

    invoke-virtual {v1, v0}, Lcom/peel/content/user/User;->a(Landroid/os/Bundle;)V

    .line 301
    iget-object v0, p0, Lcom/peel/h/a/bh;->a:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lcom/peel/ui/ay;

    iget-object v1, p0, Lcom/peel/h/a/bh;->b:Lcom/peel/h/a/bd;

    iget-object v1, v1, Lcom/peel/h/a/bd;->b:Lcom/peel/h/a/bb;

    invoke-static {v1}, Lcom/peel/h/a/bb;->g(Lcom/peel/h/a/bb;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/peel/ui/ay;->b(Ljava/util/ArrayList;)V

    .line 302
    return-void

    .line 292
    :cond_0
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 293
    iget-object v0, p0, Lcom/peel/h/a/bh;->b:Lcom/peel/h/a/bd;

    iget-object v0, v0, Lcom/peel/h/a/bd;->b:Lcom/peel/h/a/bb;

    invoke-static {v0}, Lcom/peel/h/a/bb;->g(Lcom/peel/h/a/bb;)Ljava/util/ArrayList;

    move-result-object v0

    iget-object v2, p0, Lcom/peel/h/a/bh;->b:Lcom/peel/h/a/bd;

    iget-object v2, v2, Lcom/peel/h/a/bd;->b:Lcom/peel/h/a/bb;

    invoke-static {v2}, Lcom/peel/h/a/bb;->f(Lcom/peel/h/a/bb;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 294
    invoke-static {}, Lcom/peel/util/a/f;->a()Lcom/peel/util/a/f;

    move-result-object v0

    sget-object v2, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v2}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v2

    if-nez v2, :cond_1

    :goto_1
    const/16 v2, 0x528

    const/16 v3, 0x7d8

    const-string/jumbo v4, "Premium Channel"

    iget-object v6, p0, Lcom/peel/h/a/bh;->b:Lcom/peel/h/a/bd;

    iget-object v6, v6, Lcom/peel/h/a/bd;->b:Lcom/peel/h/a/bb;

    .line 295
    invoke-static {v6}, Lcom/peel/h/a/bb;->f(Lcom/peel/h/a/bb;)Ljava/util/ArrayList;

    move-result-object v6

    invoke-virtual {v6, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    move v7, v5

    .line 294
    invoke-virtual/range {v0 .. v7}, Lcom/peel/util/a/f;->a(IIILjava/lang/String;ILjava/lang/String;I)V

    goto/16 :goto_0

    :cond_1
    sget-object v1, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v1}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/data/at;->f()I

    move-result v1

    goto :goto_1
.end method
