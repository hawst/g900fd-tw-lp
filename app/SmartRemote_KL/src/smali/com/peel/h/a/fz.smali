.class Lcom/peel/h/a/fz;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:[Lcom/peel/data/ContentRoom;

.field final synthetic b:Lcom/peel/control/RoomControl;

.field final synthetic c:Lcom/peel/h/a/fn;


# direct methods
.method constructor <init>(Lcom/peel/h/a/fn;[Lcom/peel/data/ContentRoom;Lcom/peel/control/RoomControl;)V
    .locals 0

    .prologue
    .line 534
    iput-object p1, p0, Lcom/peel/h/a/fz;->c:Lcom/peel/h/a/fn;

    iput-object p2, p0, Lcom/peel/h/a/fz;->a:[Lcom/peel/data/ContentRoom;

    iput-object p3, p0, Lcom/peel/h/a/fz;->b:Lcom/peel/control/RoomControl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 537
    iget-object v0, p0, Lcom/peel/h/a/fz;->c:Lcom/peel/h/a/fn;

    new-instance v1, Lcom/peel/widget/ag;

    iget-object v2, p0, Lcom/peel/h/a/fz;->c:Lcom/peel/h/a/fn;

    invoke-virtual {v2}, Lcom/peel/h/a/fn;->m()Landroid/support/v4/app/ae;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/peel/widget/ag;-><init>(Landroid/content/Context;)V

    sget v2, Lcom/peel/ui/ft;->warning:I

    invoke-virtual {v1, v2}, Lcom/peel/widget/ag;->a(I)Lcom/peel/widget/ag;

    move-result-object v1

    sget v2, Lcom/peel/ui/ft;->delete_room_confirmation:I

    invoke-virtual {v1, v2}, Lcom/peel/widget/ag;->b(I)Lcom/peel/widget/ag;

    move-result-object v1

    sget v2, Lcom/peel/ui/ft;->ok:I

    new-instance v3, Lcom/peel/h/a/ga;

    invoke-direct {v3, p0}, Lcom/peel/h/a/ga;-><init>(Lcom/peel/h/a/fz;)V

    .line 538
    invoke-virtual {v1, v2, v3}, Lcom/peel/widget/ag;->a(ILandroid/content/DialogInterface$OnClickListener;)Lcom/peel/widget/ag;

    move-result-object v1

    sget v2, Lcom/peel/ui/ft;->cancel:I

    const/4 v3, 0x0

    .line 586
    invoke-virtual {v1, v2, v3}, Lcom/peel/widget/ag;->c(ILandroid/content/DialogInterface$OnClickListener;)Lcom/peel/widget/ag;

    move-result-object v1

    invoke-virtual {v1, v4}, Lcom/peel/widget/ag;->a(Z)Lcom/peel/widget/ag;

    move-result-object v1

    .line 537
    invoke-static {v0, v1}, Lcom/peel/h/a/fn;->c(Lcom/peel/h/a/fn;Lcom/peel/widget/ag;)Lcom/peel/widget/ag;

    .line 587
    iget-object v0, p0, Lcom/peel/h/a/fz;->c:Lcom/peel/h/a/fn;

    invoke-static {v0}, Lcom/peel/h/a/fn;->n(Lcom/peel/h/a/fn;)Lcom/peel/widget/ag;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/peel/widget/ag;->setCanceledOnTouchOutside(Z)V

    .line 588
    iget-object v0, p0, Lcom/peel/h/a/fz;->c:Lcom/peel/h/a/fn;

    invoke-static {v0}, Lcom/peel/h/a/fn;->n(Lcom/peel/h/a/fn;)Lcom/peel/widget/ag;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/h/a/fz;->c:Lcom/peel/h/a/fn;

    invoke-static {v1}, Lcom/peel/h/a/fn;->n(Lcom/peel/h/a/fn;)Lcom/peel/widget/ag;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/peel/widget/ag;->a(Lcom/peel/widget/ag;)Lcom/peel/widget/ag;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/widget/ag;->show()V

    .line 589
    return-void
.end method
