.class Lcom/peel/h/a/bn;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field final synthetic a:Landroid/widget/ListView;

.field final synthetic b:Lcom/peel/h/a/bd;


# direct methods
.method constructor <init>(Lcom/peel/h/a/bd;Landroid/widget/ListView;)V
    .locals 0

    .prologue
    .line 434
    iput-object p1, p0, Lcom/peel/h/a/bn;->b:Lcom/peel/h/a/bd;

    iput-object p2, p0, Lcom/peel/h/a/bn;->a:Landroid/widget/ListView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    const/16 v3, 0x7d8

    const/16 v2, 0x529

    const/4 v1, 0x1

    const/4 v5, 0x0

    .line 437
    iget-object v0, p0, Lcom/peel/h/a/bn;->b:Lcom/peel/h/a/bd;

    iget-object v0, v0, Lcom/peel/h/a/bd;->b:Lcom/peel/h/a/bb;

    invoke-static {v0, v1}, Lcom/peel/h/a/bb;->a(Lcom/peel/h/a/bb;Z)Z

    .line 438
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Landroid/widget/AdapterView;->getAdapter()Landroid/widget/Adapter;

    move-result-object v0

    check-cast v0, Lcom/peel/ui/ay;

    iget v0, v0, Lcom/peel/ui/ay;->a:I

    if-le v0, v1, :cond_2

    .line 439
    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 440
    iget-object v0, p0, Lcom/peel/h/a/bn;->b:Lcom/peel/h/a/bd;

    iget-object v0, v0, Lcom/peel/h/a/bd;->b:Lcom/peel/h/a/bb;

    invoke-static {v0}, Lcom/peel/h/a/bb;->k(Lcom/peel/h/a/bb;)Ljava/util/ArrayList;

    move-result-object v0

    iget-object v4, p0, Lcom/peel/h/a/bn;->b:Lcom/peel/h/a/bd;

    iget-object v4, v4, Lcom/peel/h/a/bd;->b:Lcom/peel/h/a/bb;

    invoke-static {v4}, Lcom/peel/h/a/bb;->k(Lcom/peel/h/a/bb;)Ljava/util/ArrayList;

    move-result-object v4

    iget-object v6, p0, Lcom/peel/h/a/bn;->b:Lcom/peel/h/a/bd;

    iget-object v6, v6, Lcom/peel/h/a/bd;->b:Lcom/peel/h/a/bb;

    invoke-static {v6}, Lcom/peel/h/a/bb;->j(Lcom/peel/h/a/bb;)Ljava/util/ArrayList;

    move-result-object v6

    invoke-virtual {v6, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v4

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 441
    invoke-virtual {p1}, Landroid/widget/AdapterView;->getAdapter()Landroid/widget/Adapter;

    move-result-object v0

    check-cast v0, Lcom/peel/ui/ay;

    iget v4, v0, Lcom/peel/ui/ay;->a:I

    add-int/lit8 v4, v4, -0x1

    iput v4, v0, Lcom/peel/ui/ay;->a:I

    .line 446
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/peel/h/a/bn;->b:Lcom/peel/h/a/bd;

    iget-object v0, v0, Lcom/peel/h/a/bd;->b:Lcom/peel/h/a/bb;

    invoke-static {v0}, Lcom/peel/h/a/bb;->k(Lcom/peel/h/a/bb;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v4, p0, Lcom/peel/h/a/bn;->b:Lcom/peel/h/a/bd;

    iget-object v4, v4, Lcom/peel/h/a/bd;->b:Lcom/peel/h/a/bb;

    sget v6, Lcom/peel/ui/ft;->shortlabel_hd:I

    invoke-virtual {v4, v6}, Lcom/peel/h/a/bb;->a(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/peel/h/a/bn;->b:Lcom/peel/h/a/bd;

    iget-object v0, v0, Lcom/peel/h/a/bd;->b:Lcom/peel/h/a/bb;

    invoke-static {v0}, Lcom/peel/h/a/bb;->k(Lcom/peel/h/a/bb;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v4, p0, Lcom/peel/h/a/bn;->b:Lcom/peel/h/a/bd;

    iget-object v4, v4, Lcom/peel/h/a/bd;->b:Lcom/peel/h/a/bb;

    sget v6, Lcom/peel/ui/ft;->shortlabel_sd:I

    invoke-virtual {v4, v6}, Lcom/peel/h/a/bb;->a(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 447
    iget-object v0, p0, Lcom/peel/h/a/bn;->b:Lcom/peel/h/a/bd;

    iget-object v0, v0, Lcom/peel/h/a/bd;->b:Lcom/peel/h/a/bb;

    invoke-static {v0}, Lcom/peel/h/a/bb;->l(Lcom/peel/h/a/bb;)Landroid/os/Bundle;

    move-result-object v0

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v6, p0, Lcom/peel/h/a/bn;->b:Lcom/peel/h/a/bd;

    iget-object v6, v6, Lcom/peel/h/a/bd;->b:Lcom/peel/h/a/bb;

    invoke-static {v6}, Lcom/peel/h/a/bb;->h(Lcom/peel/h/a/bb;)Lcom/peel/data/ContentRoom;

    move-result-object v6

    invoke-virtual {v6}, Lcom/peel/data/ContentRoom;->a()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v6, "/"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v6, p0, Lcom/peel/h/a/bn;->b:Lcom/peel/h/a/bd;

    iget-object v6, v6, Lcom/peel/h/a/bd;->b:Lcom/peel/h/a/bb;

    invoke-static {v6}, Lcom/peel/h/a/bb;->e(Lcom/peel/h/a/bb;)Lcom/peel/content/library/LiveLibrary;

    move-result-object v6

    invoke-virtual {v6}, Lcom/peel/content/library/LiveLibrary;->e()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const-string/jumbo v6, "B"

    invoke-virtual {v0, v4, v6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 448
    invoke-static {}, Lcom/peel/util/a/f;->a()Lcom/peel/util/a/f;

    move-result-object v0

    sget-object v4, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v4}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v4

    if-nez v4, :cond_3

    :goto_1
    const-string/jumbo v4, "HD/SD"

    const-string/jumbo v6, "HD/SD"

    move v7, v5

    invoke-virtual/range {v0 .. v7}, Lcom/peel/util/a/f;->a(IIILjava/lang/String;ILjava/lang/String;I)V

    .line 461
    :cond_1
    :goto_2
    iget-object v0, p0, Lcom/peel/h/a/bn;->a:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lcom/peel/ui/ay;

    iget-object v1, p0, Lcom/peel/h/a/bn;->b:Lcom/peel/h/a/bd;

    iget-object v1, v1, Lcom/peel/h/a/bd;->b:Lcom/peel/h/a/bb;

    invoke-static {v1}, Lcom/peel/h/a/bb;->k(Lcom/peel/h/a/bb;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/peel/ui/ay;->b(Ljava/util/ArrayList;)V

    .line 462
    return-void

    .line 442
    :cond_2
    iget-object v0, p0, Lcom/peel/h/a/bn;->b:Lcom/peel/h/a/bd;

    iget-object v0, v0, Lcom/peel/h/a/bd;->b:Lcom/peel/h/a/bb;

    invoke-static {v0}, Lcom/peel/h/a/bb;->k(Lcom/peel/h/a/bb;)Ljava/util/ArrayList;

    move-result-object v0

    iget-object v4, p0, Lcom/peel/h/a/bn;->b:Lcom/peel/h/a/bd;

    iget-object v4, v4, Lcom/peel/h/a/bd;->b:Lcom/peel/h/a/bb;

    invoke-static {v4}, Lcom/peel/h/a/bb;->j(Lcom/peel/h/a/bb;)Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 443
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 444
    iget-object v0, p0, Lcom/peel/h/a/bn;->b:Lcom/peel/h/a/bd;

    iget-object v0, v0, Lcom/peel/h/a/bd;->b:Lcom/peel/h/a/bb;

    invoke-static {v0}, Lcom/peel/h/a/bb;->k(Lcom/peel/h/a/bb;)Ljava/util/ArrayList;

    move-result-object v0

    iget-object v4, p0, Lcom/peel/h/a/bn;->b:Lcom/peel/h/a/bd;

    iget-object v4, v4, Lcom/peel/h/a/bd;->b:Lcom/peel/h/a/bb;

    invoke-static {v4}, Lcom/peel/h/a/bb;->j(Lcom/peel/h/a/bb;)Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 448
    :cond_3
    sget-object v1, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v1}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/data/at;->f()I

    move-result v1

    goto :goto_1

    .line 451
    :cond_4
    iget-object v0, p0, Lcom/peel/h/a/bn;->b:Lcom/peel/h/a/bd;

    iget-object v0, v0, Lcom/peel/h/a/bd;->b:Lcom/peel/h/a/bb;

    invoke-static {v0}, Lcom/peel/h/a/bb;->k(Lcom/peel/h/a/bb;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v4, p0, Lcom/peel/h/a/bn;->b:Lcom/peel/h/a/bd;

    iget-object v4, v4, Lcom/peel/h/a/bd;->b:Lcom/peel/h/a/bb;

    sget v6, Lcom/peel/ui/ft;->shortlabel_hd:I

    invoke-virtual {v4, v6}, Lcom/peel/h/a/bb;->a(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 452
    iget-object v0, p0, Lcom/peel/h/a/bn;->b:Lcom/peel/h/a/bd;

    iget-object v0, v0, Lcom/peel/h/a/bd;->b:Lcom/peel/h/a/bb;

    invoke-static {v0}, Lcom/peel/h/a/bb;->l(Lcom/peel/h/a/bb;)Landroid/os/Bundle;

    move-result-object v0

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v6, p0, Lcom/peel/h/a/bn;->b:Lcom/peel/h/a/bd;

    iget-object v6, v6, Lcom/peel/h/a/bd;->b:Lcom/peel/h/a/bb;

    invoke-static {v6}, Lcom/peel/h/a/bb;->h(Lcom/peel/h/a/bb;)Lcom/peel/data/ContentRoom;

    move-result-object v6

    invoke-virtual {v6}, Lcom/peel/data/ContentRoom;->a()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v6, "/"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v6, p0, Lcom/peel/h/a/bn;->b:Lcom/peel/h/a/bd;

    iget-object v6, v6, Lcom/peel/h/a/bd;->b:Lcom/peel/h/a/bb;

    invoke-static {v6}, Lcom/peel/h/a/bb;->e(Lcom/peel/h/a/bb;)Lcom/peel/content/library/LiveLibrary;

    move-result-object v6

    invoke-virtual {v6}, Lcom/peel/content/library/LiveLibrary;->e()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const-string/jumbo v6, "HD"

    invoke-virtual {v0, v4, v6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 453
    invoke-static {}, Lcom/peel/util/a/f;->a()Lcom/peel/util/a/f;

    move-result-object v0

    sget-object v4, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v4}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v4

    if-nez v4, :cond_5

    :goto_3
    const-string/jumbo v4, "HD/SD"

    const-string/jumbo v6, "HD"

    move v7, v5

    invoke-virtual/range {v0 .. v7}, Lcom/peel/util/a/f;->a(IIILjava/lang/String;ILjava/lang/String;I)V

    goto/16 :goto_2

    :cond_5
    sget-object v1, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v1}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/data/at;->f()I

    move-result v1

    goto :goto_3

    .line 456
    :cond_6
    iget-object v0, p0, Lcom/peel/h/a/bn;->b:Lcom/peel/h/a/bd;

    iget-object v0, v0, Lcom/peel/h/a/bd;->b:Lcom/peel/h/a/bb;

    invoke-static {v0}, Lcom/peel/h/a/bb;->k(Lcom/peel/h/a/bb;)Ljava/util/ArrayList;

    move-result-object v0

    iget-object v4, p0, Lcom/peel/h/a/bn;->b:Lcom/peel/h/a/bd;

    iget-object v4, v4, Lcom/peel/h/a/bd;->b:Lcom/peel/h/a/bb;

    sget v6, Lcom/peel/ui/ft;->shortlabel_sd:I

    invoke-virtual {v4, v6}, Lcom/peel/h/a/bb;->a(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 457
    iget-object v0, p0, Lcom/peel/h/a/bn;->b:Lcom/peel/h/a/bd;

    iget-object v0, v0, Lcom/peel/h/a/bd;->b:Lcom/peel/h/a/bb;

    invoke-static {v0}, Lcom/peel/h/a/bb;->l(Lcom/peel/h/a/bb;)Landroid/os/Bundle;

    move-result-object v0

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v6, p0, Lcom/peel/h/a/bn;->b:Lcom/peel/h/a/bd;

    iget-object v6, v6, Lcom/peel/h/a/bd;->b:Lcom/peel/h/a/bb;

    invoke-static {v6}, Lcom/peel/h/a/bb;->h(Lcom/peel/h/a/bb;)Lcom/peel/data/ContentRoom;

    move-result-object v6

    invoke-virtual {v6}, Lcom/peel/data/ContentRoom;->a()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v6, "/"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v6, p0, Lcom/peel/h/a/bn;->b:Lcom/peel/h/a/bd;

    iget-object v6, v6, Lcom/peel/h/a/bd;->b:Lcom/peel/h/a/bb;

    invoke-static {v6}, Lcom/peel/h/a/bb;->e(Lcom/peel/h/a/bb;)Lcom/peel/content/library/LiveLibrary;

    move-result-object v6

    invoke-virtual {v6}, Lcom/peel/content/library/LiveLibrary;->e()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const-string/jumbo v6, "SD"

    invoke-virtual {v0, v4, v6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 458
    invoke-static {}, Lcom/peel/util/a/f;->a()Lcom/peel/util/a/f;

    move-result-object v0

    sget-object v4, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v4}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v4

    if-nez v4, :cond_7

    :goto_4
    const-string/jumbo v4, "HD/SD"

    const-string/jumbo v6, "SD"

    move v7, v5

    invoke-virtual/range {v0 .. v7}, Lcom/peel/util/a/f;->a(IIILjava/lang/String;ILjava/lang/String;I)V

    goto/16 :goto_2

    :cond_7
    sget-object v1, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v1}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/data/at;->f()I

    move-result v1

    goto :goto_4
.end method
