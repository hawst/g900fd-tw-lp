.class Lcom/peel/h/a/cx;
.super Lcom/peel/util/t;


# instance fields
.field final synthetic a:Lcom/peel/h/a/cp;


# direct methods
.method constructor <init>(Lcom/peel/h/a/cp;I)V
    .locals 0

    .prologue
    .line 221
    iput-object p1, p0, Lcom/peel/h/a/cx;->a:Lcom/peel/h/a/cp;

    invoke-direct {p0, p2}, Lcom/peel/util/t;-><init>(I)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .prologue
    .line 224
    invoke-static {}, Lcom/peel/h/a/cp;->c()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "channel.getlineup: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/peel/h/a/cx;->i:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 225
    iget-boolean v0, p0, Lcom/peel/h/a/cx;->i:Z

    if-eqz v0, :cond_2

    .line 226
    iget-object v0, p0, Lcom/peel/h/a/cx;->j:Ljava/lang/Object;

    check-cast v0, [Lcom/peel/data/Channel;

    check-cast v0, [Lcom/peel/data/Channel;

    .line 227
    array-length v2, v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_1

    aget-object v3, v0, v1

    .line 228
    invoke-virtual {v3}, Lcom/peel/data/Channel;->l()Z

    move-result v4

    if-nez v4, :cond_0

    .line 229
    iget-object v4, p0, Lcom/peel/h/a/cx;->a:Lcom/peel/h/a/cp;

    invoke-static {v4}, Lcom/peel/h/a/cp;->h(Lcom/peel/h/a/cp;)Ljava/util/List;

    move-result-object v4

    invoke-interface {v4, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 232
    :cond_0
    invoke-static {}, Lcom/peel/h/a/cp;->c()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "channel: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v3}, Lcom/peel/data/Channel;->b()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, " -- "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v3}, Lcom/peel/data/Channel;->d()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, " -- "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v3}, Lcom/peel/data/Channel;->e()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, " -- "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v3}, Lcom/peel/data/Channel;->m()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, " -- "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v3}, Lcom/peel/data/Channel;->k()Z

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, " -- "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v3}, Lcom/peel/data/Channel;->l()Z

    move-result v3

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v4, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 227
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 236
    :cond_1
    iget-object v0, p0, Lcom/peel/h/a/cx;->a:Lcom/peel/h/a/cp;

    invoke-static {v0}, Lcom/peel/h/a/cp;->h(Lcom/peel/h/a/cp;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_2

    .line 237
    iget-object v0, p0, Lcom/peel/h/a/cx;->a:Lcom/peel/h/a/cp;

    invoke-static {v0}, Lcom/peel/h/a/cp;->h(Lcom/peel/h/a/cp;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    div-int/lit8 v1, v0, 0x3

    .line 238
    iget-object v2, p0, Lcom/peel/h/a/cx;->a:Lcom/peel/h/a/cp;

    iget-object v0, p0, Lcom/peel/h/a/cx;->a:Lcom/peel/h/a/cp;

    invoke-static {v0}, Lcom/peel/h/a/cp;->h(Lcom/peel/h/a/cp;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/data/Channel;

    invoke-virtual {v0}, Lcom/peel/data/Channel;->m()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/peel/h/a/cx;->a:Lcom/peel/h/a/cp;

    invoke-static {v0}, Lcom/peel/h/a/cp;->h(Lcom/peel/h/a/cp;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/data/Channel;

    invoke-virtual {v0}, Lcom/peel/data/Channel;->m()Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-static {v2, v0}, Lcom/peel/h/a/cp;->a(Lcom/peel/h/a/cp;Ljava/lang/String;)Ljava/lang/String;

    .line 239
    iget-object v0, p0, Lcom/peel/h/a/cx;->a:Lcom/peel/h/a/cp;

    invoke-static {v0}, Lcom/peel/h/a/cp;->g(Lcom/peel/h/a/cp;)V

    .line 242
    :cond_2
    return-void

    .line 238
    :cond_3
    iget-object v0, p0, Lcom/peel/h/a/cx;->a:Lcom/peel/h/a/cp;

    invoke-static {v0}, Lcom/peel/h/a/cp;->h(Lcom/peel/h/a/cp;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/data/Channel;

    invoke-virtual {v0}, Lcom/peel/data/Channel;->e()Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method
