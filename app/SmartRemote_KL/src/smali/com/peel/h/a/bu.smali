.class Lcom/peel/h/a/bu;
.super Lcom/peel/util/t;


# instance fields
.field final synthetic a:Ljava/lang/StringBuilder;

.field final synthetic b:Z

.field final synthetic c:Lcom/peel/h/a/bb;


# direct methods
.method constructor <init>(Lcom/peel/h/a/bb;Ljava/lang/StringBuilder;Z)V
    .locals 0

    .prologue
    .line 673
    iput-object p1, p0, Lcom/peel/h/a/bu;->c:Lcom/peel/h/a/bb;

    iput-object p2, p0, Lcom/peel/h/a/bu;->a:Ljava/lang/StringBuilder;

    iput-boolean p3, p0, Lcom/peel/h/a/bu;->b:Z

    invoke-direct {p0}, Lcom/peel/util/t;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 13

    .prologue
    .line 676
    iget-boolean v0, p0, Lcom/peel/h/a/bu;->i:Z

    if-eqz v0, :cond_d

    .line 677
    iget-object v0, p0, Lcom/peel/h/a/bu;->j:Ljava/lang/Object;

    check-cast v0, [Lcom/peel/data/Channel;

    check-cast v0, [Lcom/peel/data/Channel;

    .line 678
    iget-object v1, p0, Lcom/peel/h/a/bu;->c:Lcom/peel/h/a/bb;

    invoke-static {v1}, Lcom/peel/h/a/bb;->s(Lcom/peel/h/a/bb;)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "B"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x0

    move v2, v1

    .line 679
    :goto_0
    iget-object v1, p0, Lcom/peel/h/a/bu;->c:Lcom/peel/h/a/bb;

    invoke-static {v1}, Lcom/peel/h/a/bb;->s(Lcom/peel/h/a/bb;)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v3, "SD"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x0

    move v3, v1

    .line 680
    :goto_1
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 681
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 682
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 683
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 684
    iget-object v1, p0, Lcom/peel/h/a/bu;->c:Lcom/peel/h/a/bb;

    invoke-static {v1}, Lcom/peel/h/a/bb;->f(Lcom/peel/h/a/bb;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :cond_0
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 685
    iget-object v4, p0, Lcom/peel/h/a/bu;->c:Lcom/peel/h/a/bb;

    invoke-static {v4}, Lcom/peel/h/a/bb;->g(Lcom/peel/h/a/bb;)Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 686
    iget-object v4, p0, Lcom/peel/h/a/bu;->c:Lcom/peel/h/a/bb;

    invoke-static {v4}, Lcom/peel/h/a/bb;->t(Lcom/peel/h/a/bb;)Ljava/util/LinkedHashMap;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Lcom/peel/data/Channel;

    array-length v10, v1

    const/4 v4, 0x0

    :goto_2
    if-ge v4, v10, :cond_0

    aget-object v11, v1, v4

    .line 687
    invoke-virtual {v11}, Lcom/peel/data/Channel;->b()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v8, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 686
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 678
    :cond_1
    const/4 v1, 0x1

    move v2, v1

    goto :goto_0

    .line 679
    :cond_2
    const/4 v1, 0x1

    move v3, v1

    goto :goto_1

    .line 690
    :cond_3
    iget-object v4, p0, Lcom/peel/h/a/bu;->c:Lcom/peel/h/a/bb;

    invoke-static {v4}, Lcom/peel/h/a/bb;->t(Lcom/peel/h/a/bb;)Ljava/util/LinkedHashMap;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Lcom/peel/data/Channel;

    array-length v10, v1

    const/4 v4, 0x0

    :goto_3
    if-ge v4, v10, :cond_0

    aget-object v11, v1, v4

    .line 691
    invoke-virtual {v11}, Lcom/peel/data/Channel;->b()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v7, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 690
    add-int/lit8 v4, v4, 0x1

    goto :goto_3

    .line 696
    :cond_4
    array-length v4, v0

    const/4 v1, 0x0

    :goto_4
    if-ge v1, v4, :cond_a

    aget-object v9, v0, v1

    .line 706
    invoke-virtual {v9}, Lcom/peel/data/Channel;->b()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_5

    .line 707
    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Lcom/peel/data/Channel;->b(Z)V

    .line 708
    invoke-virtual {v9}, Lcom/peel/data/Channel;->a()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v6, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 696
    :goto_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 712
    :cond_5
    invoke-virtual {v9}, Lcom/peel/data/Channel;->l()Z

    move-result v10

    if-nez v10, :cond_6

    invoke-virtual {v9}, Lcom/peel/data/Channel;->b()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v7, v10}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_6

    .line 713
    const/4 v10, 0x1

    invoke-virtual {v9, v10}, Lcom/peel/data/Channel;->b(Z)V

    .line 714
    invoke-virtual {v9}, Lcom/peel/data/Channel;->a()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v5, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_5

    .line 718
    :cond_6
    invoke-static {}, Lcom/peel/h/a/bb;->c()Ljava/lang/String;

    move-result-object v10

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v12, "\n\nneedResolutionCheck: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string/jumbo v12, "\nc.getType(): "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v9}, Lcom/peel/data/Channel;->i()I

    move-result v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 719
    invoke-virtual {v9}, Lcom/peel/data/Channel;->i()I

    move-result v10

    if-eq v3, v10, :cond_7

    if-eqz v2, :cond_7

    .line 720
    invoke-static {}, Lcom/peel/h/a/bb;->c()Ljava/lang/String;

    move-result-object v10

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v12, "cutting channel: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v9}, Lcom/peel/data/Channel;->d()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 721
    const/4 v10, 0x1

    invoke-virtual {v9, v10}, Lcom/peel/data/Channel;->b(Z)V

    .line 722
    invoke-virtual {v9}, Lcom/peel/data/Channel;->a()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v5, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_5

    .line 725
    :cond_7
    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Lcom/peel/data/Channel;->b(Z)V

    .line 726
    invoke-virtual {v9}, Lcom/peel/data/Channel;->a()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v6, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 727
    invoke-virtual {v9}, Lcom/peel/data/Channel;->a()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v5, v10}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 731
    invoke-virtual {v9}, Lcom/peel/data/Channel;->g()Ljava/lang/String;

    move-result-object v10

    if-eqz v10, :cond_8

    invoke-virtual {v9}, Lcom/peel/data/Channel;->g()Ljava/lang/String;

    move-result-object v10

    const-string/jumbo v11, "0"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_8

    iget-object v10, p0, Lcom/peel/h/a/bu;->c:Lcom/peel/h/a/bb;

    invoke-static {v10}, Lcom/peel/h/a/bb;->o(Lcom/peel/h/a/bb;)Ljava/util/ArrayList;

    move-result-object v10

    invoke-virtual {v9}, Lcom/peel/data/Channel;->g()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_8

    .line 732
    const/4 v10, 0x1

    invoke-virtual {v9, v10}, Lcom/peel/data/Channel;->b(Z)V

    .line 733
    invoke-virtual {v9}, Lcom/peel/data/Channel;->a()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v5, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_5

    .line 737
    :cond_8
    invoke-virtual {v9}, Lcom/peel/data/Channel;->l()Z

    move-result v10

    if-eqz v10, :cond_9

    .line 738
    invoke-static {}, Lcom/peel/h/a/bb;->c()Ljava/lang/String;

    move-result-object v10

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v12, "already cut: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v9}, Lcom/peel/data/Channel;->d()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 739
    invoke-virtual {v9}, Lcom/peel/data/Channel;->a()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v5, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_5

    .line 743
    :cond_9
    invoke-virtual {v9}, Lcom/peel/data/Channel;->a()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v6, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_5

    .line 747
    :cond_a
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 748
    const-string/jumbo v1, "path"

    const-string/jumbo v2, "channels/lineup"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 749
    const-string/jumbo v1, "library"

    iget-object v2, p0, Lcom/peel/h/a/bu;->c:Lcom/peel/h/a/bb;

    invoke-static {v2}, Lcom/peel/h/a/bb;->e(Lcom/peel/h/a/bb;)Lcom/peel/content/library/LiveLibrary;

    move-result-object v2

    invoke-virtual {v2}, Lcom/peel/content/library/LiveLibrary;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 750
    const-string/jumbo v1, "user"

    sget-object v2, Lcom/peel/content/a;->f:Lcom/peel/content/user/User;

    invoke-virtual {v2}, Lcom/peel/content/user/User;->x()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 751
    const-string/jumbo v1, "hdprefs"

    iget-object v2, p0, Lcom/peel/h/a/bu;->c:Lcom/peel/h/a/bb;

    invoke-static {v2}, Lcom/peel/h/a/bb;->l(Lcom/peel/h/a/bb;)Landroid/os/Bundle;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/peel/h/a/bu;->c:Lcom/peel/h/a/bb;

    invoke-static {v4}, Lcom/peel/h/a/bb;->h(Lcom/peel/h/a/bb;)Lcom/peel/data/ContentRoom;

    move-result-object v4

    invoke-virtual {v4}, Lcom/peel/data/ContentRoom;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/peel/h/a/bu;->c:Lcom/peel/h/a/bb;

    invoke-static {v4}, Lcom/peel/h/a/bb;->e(Lcom/peel/h/a/bb;)Lcom/peel/content/library/LiveLibrary;

    move-result-object v4

    invoke-virtual {v4}, Lcom/peel/content/library/LiveLibrary;->e()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 752
    const-string/jumbo v1, "content_user"

    sget-object v2, Lcom/peel/content/a;->f:Lcom/peel/content/user/User;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 753
    const-string/jumbo v1, "channels"

    invoke-virtual {v0, v1, v6}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 754
    const-string/jumbo v1, "room"

    iget-object v2, p0, Lcom/peel/h/a/bu;->c:Lcom/peel/h/a/bb;

    invoke-static {v2}, Lcom/peel/h/a/bb;->h(Lcom/peel/h/a/bb;)Lcom/peel/data/ContentRoom;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 755
    const-string/jumbo v1, "prgids"

    iget-object v2, p0, Lcom/peel/h/a/bu;->a:Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 756
    iget-object v1, p0, Lcom/peel/h/a/bu;->c:Lcom/peel/h/a/bb;

    invoke-static {v1}, Lcom/peel/h/a/bb;->e(Lcom/peel/h/a/bb;)Lcom/peel/content/library/LiveLibrary;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Lcom/peel/content/library/LiveLibrary;->b(Landroid/os/Bundle;Lcom/peel/util/t;)V

    .line 759
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 760
    const-string/jumbo v1, "path"

    const-string/jumbo v2, "channels/cut"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 761
    const-string/jumbo v1, "library"

    iget-object v2, p0, Lcom/peel/h/a/bu;->c:Lcom/peel/h/a/bb;

    invoke-static {v2}, Lcom/peel/h/a/bb;->e(Lcom/peel/h/a/bb;)Lcom/peel/content/library/LiveLibrary;

    move-result-object v2

    invoke-virtual {v2}, Lcom/peel/content/library/LiveLibrary;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 762
    const-string/jumbo v1, "user"

    sget-object v2, Lcom/peel/content/a;->f:Lcom/peel/content/user/User;

    invoke-virtual {v2}, Lcom/peel/content/user/User;->x()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 763
    const-string/jumbo v1, "channels"

    invoke-virtual {v0, v1, v5}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 764
    const-string/jumbo v1, "room"

    iget-object v2, p0, Lcom/peel/h/a/bu;->c:Lcom/peel/h/a/bb;

    invoke-static {v2}, Lcom/peel/h/a/bb;->h(Lcom/peel/h/a/bb;)Lcom/peel/data/ContentRoom;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 765
    sget-object v1, Lcom/peel/content/a;->f:Lcom/peel/content/user/User;

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Lcom/peel/content/user/User;->a(Landroid/os/Bundle;Lcom/peel/util/t;)V

    .line 767
    sget-object v1, Lcom/peel/content/a;->b:Lcom/peel/content/i;

    const/16 v2, 0x1e

    iget-object v0, p0, Lcom/peel/h/a/bu;->c:Lcom/peel/h/a/bb;

    invoke-static {v0}, Lcom/peel/h/a/bb;->e(Lcom/peel/h/a/bb;)Lcom/peel/content/library/LiveLibrary;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/content/library/LiveLibrary;->e()Ljava/lang/String;

    move-result-object v3

    const/4 v0, 0x0

    check-cast v0, [Ljava/lang/Object;

    invoke-virtual {v1, v2, v3, v0}, Lcom/peel/content/i;->a(ILjava/lang/Object;[Ljava/lang/Object;)V

    .line 768
    iget-boolean v0, p0, Lcom/peel/h/a/bu;->b:Z

    if-nez v0, :cond_b

    .line 769
    iget-object v0, p0, Lcom/peel/h/a/bu;->c:Lcom/peel/h/a/bb;

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    invoke-virtual {v0, v1}, Lcom/peel/h/a/bb;->c(Landroid/os/Bundle;)V

    .line 775
    :cond_b
    :goto_6
    iget-boolean v0, p0, Lcom/peel/h/a/bu;->b:Z

    if-eqz v0, :cond_c

    .line 776
    const-class v0, Lcom/peel/h/a/bb;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/h/a/bu;->c:Lcom/peel/h/a/bb;

    invoke-virtual {v1}, Lcom/peel/h/a/bb;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/peel/util/bx;->a(Ljava/lang/String;Landroid/app/Activity;Z)V

    .line 777
    invoke-static {}, Lcom/peel/h/a/bb;->c()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/h/a/bu;->c:Lcom/peel/h/a/bb;

    invoke-virtual {v1}, Lcom/peel/h/a/bb;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/peel/d/e;->a(Ljava/lang/String;Landroid/support/v4/app/ae;)V

    .line 779
    :cond_c
    return-void

    .line 772
    :cond_d
    invoke-static {}, Lcom/peel/h/a/bb;->c()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "error getting lineups"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_6
.end method
