.class Lcom/peel/h/a/gd;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Landroid/os/Bundle;

.field final synthetic b:Landroid/util/SparseArray;

.field final synthetic c:Lcom/peel/h/a/fn;


# direct methods
.method constructor <init>(Lcom/peel/h/a/fn;Landroid/os/Bundle;Landroid/util/SparseArray;)V
    .locals 0

    .prologue
    .line 642
    iput-object p1, p0, Lcom/peel/h/a/gd;->c:Lcom/peel/h/a/fn;

    iput-object p2, p0, Lcom/peel/h/a/gd;->a:Landroid/os/Bundle;

    iput-object p3, p0, Lcom/peel/h/a/gd;->b:Landroid/util/SparseArray;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 650
    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    iget-object v1, p0, Lcom/peel/h/a/gd;->c:Lcom/peel/h/a/fn;

    invoke-static {v1}, Lcom/peel/h/a/fn;->e(Lcom/peel/h/a/fn;)Lcom/peel/data/ContentRoom;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/data/ContentRoom;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/peel/control/am;->a(Ljava/lang/String;)Lcom/peel/control/RoomControl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/control/RoomControl;->c()[Lcom/peel/control/a;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 651
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 652
    const-string/jumbo v0, "room"

    iget-object v1, p0, Lcom/peel/h/a/gd;->a:Landroid/os/Bundle;

    const-string/jumbo v4, "room"

    invoke-virtual {v1, v4}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    invoke-virtual {v3, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 655
    new-instance v4, Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/peel/h/a/gd;->b:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    invoke-direct {v4, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 656
    iget-object v0, p0, Lcom/peel/h/a/gd;->b:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    new-array v5, v0, [Ljava/lang/String;

    move v1, v2

    .line 657
    :goto_0
    iget-object v0, p0, Lcom/peel/h/a/gd;->b:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 658
    iget-object v0, p0, Lcom/peel/h/a/gd;->b:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 659
    iget-object v0, p0, Lcom/peel/h/a/gd;->b:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    aput-object v0, v5, v1

    .line 657
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 661
    :cond_0
    const-string/jumbo v0, "valid_device_types"

    invoke-virtual {v3, v0, v4}, Landroid/os/Bundle;->putIntegerArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 662
    iget-object v0, p0, Lcom/peel/h/a/gd;->c:Lcom/peel/h/a/fn;

    new-instance v1, Lcom/peel/widget/ag;

    iget-object v6, p0, Lcom/peel/h/a/gd;->c:Lcom/peel/h/a/fn;

    invoke-virtual {v6}, Lcom/peel/h/a/fn;->m()Landroid/support/v4/app/ae;

    move-result-object v6

    invoke-direct {v1, v6}, Lcom/peel/widget/ag;-><init>(Landroid/content/Context;)V

    sget v6, Lcom/peel/ui/ft;->label_device_type:I

    invoke-virtual {v1, v6}, Lcom/peel/widget/ag;->a(I)Lcom/peel/widget/ag;

    move-result-object v1

    sget v6, Lcom/peel/ui/ft;->cancel:I

    new-instance v7, Lcom/peel/h/a/ge;

    invoke-direct {v7, p0}, Lcom/peel/h/a/ge;-><init>(Lcom/peel/h/a/gd;)V

    invoke-virtual {v1, v6, v7}, Lcom/peel/widget/ag;->c(ILandroid/content/DialogInterface$OnClickListener;)Lcom/peel/widget/ag;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/peel/h/a/fn;->d(Lcom/peel/h/a/fn;Lcom/peel/widget/ag;)Lcom/peel/widget/ag;

    .line 669
    iget-object v0, p0, Lcom/peel/h/a/gd;->c:Lcom/peel/h/a/fn;

    invoke-virtual {v0}, Lcom/peel/h/a/fn;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    const-string/jumbo v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/ae;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 670
    if-nez v0, :cond_1

    .line 728
    :goto_1
    return-void

    .line 672
    :cond_1
    sget v1, Lcom/peel/ui/fq;->device_list_view:I

    const/4 v6, 0x0

    invoke-virtual {v0, v1, v6, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    .line 673
    new-instance v1, Lcom/peel/ui/ax;

    iget-object v2, p0, Lcom/peel/h/a/gd;->c:Lcom/peel/h/a/fn;

    invoke-virtual {v2}, Lcom/peel/h/a/fn;->m()Landroid/support/v4/app/ae;

    move-result-object v2

    sget v6, Lcom/peel/ui/fq;->device_row:I

    invoke-direct {v1, v2, v6, v5, v4}, Lcom/peel/ui/ax;-><init>(Landroid/content/Context;I[Ljava/lang/String;Ljava/util/ArrayList;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 674
    new-instance v1, Lcom/peel/h/a/gf;

    invoke-direct {v1, p0, v3, v4}, Lcom/peel/h/a/gf;-><init>(Lcom/peel/h/a/gd;Landroid/os/Bundle;Ljava/util/ArrayList;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 690
    iget-object v1, p0, Lcom/peel/h/a/gd;->c:Lcom/peel/h/a/fn;

    invoke-static {v1}, Lcom/peel/h/a/fn;->o(Lcom/peel/h/a/fn;)Lcom/peel/widget/ag;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/peel/widget/ag;->a(Landroid/view/View;)Lcom/peel/widget/ag;

    .line 691
    iget-object v0, p0, Lcom/peel/h/a/gd;->c:Lcom/peel/h/a/fn;

    invoke-static {v0}, Lcom/peel/h/a/fn;->o(Lcom/peel/h/a/fn;)Lcom/peel/widget/ag;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/widget/ag;->show()V

    goto :goto_1

    .line 724
    :cond_2
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 725
    const-string/jumbo v1, "room"

    iget-object v2, p0, Lcom/peel/h/a/gd;->c:Lcom/peel/h/a/fn;

    invoke-static {v2}, Lcom/peel/h/a/fn;->e(Lcom/peel/h/a/fn;)Lcom/peel/data/ContentRoom;

    move-result-object v2

    invoke-virtual {v2}, Lcom/peel/data/ContentRoom;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 726
    iget-object v1, p0, Lcom/peel/h/a/gd;->c:Lcom/peel/h/a/fn;

    invoke-virtual {v1}, Lcom/peel/h/a/fn;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    const-class v2, Lcom/peel/i/dr;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lcom/peel/d/e;->b(Landroid/support/v4/app/ae;Ljava/lang/String;Landroid/os/Bundle;)V

    goto :goto_1
.end method
