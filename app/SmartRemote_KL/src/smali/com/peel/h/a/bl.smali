.class Lcom/peel/h/a/bl;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field final synthetic a:Landroid/widget/ListView;

.field final synthetic b:Lcom/peel/h/a/bd;


# direct methods
.method constructor <init>(Lcom/peel/h/a/bd;Landroid/widget/ListView;)V
    .locals 0

    .prologue
    .line 386
    iput-object p1, p0, Lcom/peel/h/a/bl;->b:Lcom/peel/h/a/bd;

    iput-object p2, p0, Lcom/peel/h/a/bl;->a:Landroid/widget/ListView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v1, 0x1

    .line 389
    iget-object v0, p0, Lcom/peel/h/a/bl;->b:Lcom/peel/h/a/bd;

    iget-object v0, v0, Lcom/peel/h/a/bd;->b:Lcom/peel/h/a/bb;

    invoke-static {v0, v1}, Lcom/peel/h/a/bb;->a(Lcom/peel/h/a/bb;Z)Z

    .line 390
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 391
    const-string/jumbo v3, "language"

    iget-object v0, p0, Lcom/peel/h/a/bl;->b:Lcom/peel/h/a/bd;

    iget-object v0, v0, Lcom/peel/h/a/bd;->b:Lcom/peel/h/a/bb;

    invoke-static {v0}, Lcom/peel/h/a/bb;->n(Lcom/peel/h/a/bb;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v2, v3, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 392
    const-string/jumbo v0, "library"

    iget-object v3, p0, Lcom/peel/h/a/bl;->b:Lcom/peel/h/a/bd;

    iget-object v3, v3, Lcom/peel/h/a/bd;->b:Lcom/peel/h/a/bb;

    invoke-static {v3}, Lcom/peel/h/a/bb;->e(Lcom/peel/h/a/bb;)Lcom/peel/content/library/LiveLibrary;

    move-result-object v3

    invoke-virtual {v3}, Lcom/peel/content/library/LiveLibrary;->e()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 393
    const-string/jumbo v0, "room"

    iget-object v3, p0, Lcom/peel/h/a/bl;->b:Lcom/peel/h/a/bd;

    iget-object v3, v3, Lcom/peel/h/a/bd;->b:Lcom/peel/h/a/bb;

    invoke-static {v3}, Lcom/peel/h/a/bb;->h(Lcom/peel/h/a/bb;)Lcom/peel/data/ContentRoom;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 394
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Landroid/widget/AdapterView;->getAdapter()Landroid/widget/Adapter;

    move-result-object v0

    check-cast v0, Lcom/peel/ui/ay;

    iget v0, v0, Lcom/peel/ui/ay;->a:I

    if-le v0, v1, :cond_1

    .line 395
    iget-object v0, p0, Lcom/peel/h/a/bl;->b:Lcom/peel/h/a/bd;

    iget-object v0, v0, Lcom/peel/h/a/bd;->b:Lcom/peel/h/a/bb;

    invoke-static {v0}, Lcom/peel/h/a/bb;->o(Lcom/peel/h/a/bb;)Ljava/util/ArrayList;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/h/a/bl;->b:Lcom/peel/h/a/bd;

    iget-object v1, v1, Lcom/peel/h/a/bd;->b:Lcom/peel/h/a/bb;

    invoke-static {v1}, Lcom/peel/h/a/bb;->o(Lcom/peel/h/a/bb;)Ljava/util/ArrayList;

    move-result-object v1

    iget-object v3, p0, Lcom/peel/h/a/bl;->b:Lcom/peel/h/a/bd;

    iget-object v3, v3, Lcom/peel/h/a/bd;->b:Lcom/peel/h/a/bb;

    invoke-static {v3}, Lcom/peel/h/a/bb;->n(Lcom/peel/h/a/bb;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 396
    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 397
    invoke-virtual {p1}, Landroid/widget/AdapterView;->getAdapter()Landroid/widget/Adapter;

    move-result-object v0

    check-cast v0, Lcom/peel/ui/ay;

    iget v1, v0, Lcom/peel/ui/ay;->a:I

    add-int/lit8 v1, v1, -0x1

    iput v1, v0, Lcom/peel/ui/ay;->a:I

    .line 398
    const-string/jumbo v0, "path"

    const-string/jumbo v1, "language/uncheck"

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 399
    sget-object v0, Lcom/peel/content/a;->f:Lcom/peel/content/user/User;

    invoke-virtual {v0, v2, v4}, Lcom/peel/content/user/User;->a(Landroid/os/Bundle;Lcom/peel/util/t;)V

    .line 409
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/peel/h/a/bl;->a:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lcom/peel/ui/ay;

    iget-object v1, p0, Lcom/peel/h/a/bl;->b:Lcom/peel/h/a/bd;

    iget-object v1, v1, Lcom/peel/h/a/bd;->b:Lcom/peel/h/a/bb;

    invoke-static {v1}, Lcom/peel/h/a/bb;->o(Lcom/peel/h/a/bb;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/peel/ui/ay;->b(Ljava/util/ArrayList;)V

    .line 410
    return-void

    .line 401
    :cond_1
    iget-object v0, p0, Lcom/peel/h/a/bl;->b:Lcom/peel/h/a/bd;

    iget-object v0, v0, Lcom/peel/h/a/bd;->b:Lcom/peel/h/a/bb;

    invoke-static {v0}, Lcom/peel/h/a/bb;->o(Lcom/peel/h/a/bb;)Ljava/util/ArrayList;

    move-result-object v0

    iget-object v3, p0, Lcom/peel/h/a/bl;->b:Lcom/peel/h/a/bd;

    iget-object v3, v3, Lcom/peel/h/a/bd;->b:Lcom/peel/h/a/bb;

    invoke-static {v3}, Lcom/peel/h/a/bb;->n(Lcom/peel/h/a/bb;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 402
    iget-object v0, p0, Lcom/peel/h/a/bl;->b:Lcom/peel/h/a/bd;

    iget-object v0, v0, Lcom/peel/h/a/bd;->b:Lcom/peel/h/a/bb;

    invoke-static {v0}, Lcom/peel/h/a/bb;->o(Lcom/peel/h/a/bb;)Ljava/util/ArrayList;

    move-result-object v0

    iget-object v3, p0, Lcom/peel/h/a/bl;->b:Lcom/peel/h/a/bd;

    iget-object v3, v3, Lcom/peel/h/a/bd;->b:Lcom/peel/h/a/bb;

    invoke-static {v3}, Lcom/peel/h/a/bb;->n(Lcom/peel/h/a/bb;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 403
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 404
    const-string/jumbo v0, "path"

    const-string/jumbo v3, "language/check"

    invoke-virtual {v2, v0, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 405
    sget-object v0, Lcom/peel/content/a;->f:Lcom/peel/content/user/User;

    invoke-virtual {v0, v2, v4}, Lcom/peel/content/user/User;->a(Landroid/os/Bundle;Lcom/peel/util/t;)V

    .line 406
    invoke-static {}, Lcom/peel/util/a/f;->a()Lcom/peel/util/a/f;

    move-result-object v0

    sget-object v2, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v2}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v2

    if-nez v2, :cond_2

    :goto_1
    const/16 v2, 0x52a

    const/16 v3, 0x7d8

    const-string/jumbo v4, "Foreign Language"

    iget-object v6, p0, Lcom/peel/h/a/bl;->b:Lcom/peel/h/a/bd;

    iget-object v6, v6, Lcom/peel/h/a/bd;->b:Lcom/peel/h/a/bb;

    .line 407
    invoke-static {v6}, Lcom/peel/h/a/bb;->n(Lcom/peel/h/a/bb;)Ljava/util/ArrayList;

    move-result-object v6

    invoke-virtual {v6, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    move v7, v5

    .line 406
    invoke-virtual/range {v0 .. v7}, Lcom/peel/util/a/f;->a(IIILjava/lang/String;ILjava/lang/String;I)V

    goto :goto_0

    :cond_2
    sget-object v1, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v1}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/data/at;->f()I

    move-result v1

    goto :goto_1
.end method
