.class public Lcom/peel/h/a/am;
.super Lcom/peel/d/u;

# interfaces
.implements Landroid/support/v4/app/bg;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/peel/d/u;",
        "Landroid/support/v4/app/bg",
        "<",
        "Landroid/database/Cursor;",
        ">;"
    }
.end annotation


# instance fields
.field private aj:Landroid/net/wifi/WifiInfo;

.field private ak:Landroid/net/wifi/WifiManager;

.field private al:Landroid/widget/TextView;

.field private am:Z

.field private an:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/peel/h/a/au;",
            ">;"
        }
    .end annotation
.end field

.field private ao:Landroid/view/View;

.field private ap:Z

.field private aq:Landroid/content/SharedPreferences;

.field private ar:Landroid/widget/RelativeLayout;

.field e:Ljava/lang/String;

.field f:Ljava/lang/String;

.field private g:Landroid/widget/ListView;

.field private h:Landroid/view/LayoutInflater;

.field private i:Lcom/peel/h/a/aq;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 40
    invoke-direct {p0}, Lcom/peel/d/u;-><init>()V

    .line 48
    iput-boolean v0, p0, Lcom/peel/h/a/am;->am:Z

    .line 51
    iput-boolean v0, p0, Lcom/peel/h/a/am;->ap:Z

    .line 52
    iput-object v1, p0, Lcom/peel/h/a/am;->e:Ljava/lang/String;

    iput-object v1, p0, Lcom/peel/h/a/am;->f:Ljava/lang/String;

    .line 263
    return-void
.end method

.method static synthetic a(Lcom/peel/h/a/am;)Ljava/util/List;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/peel/h/a/am;->an:Ljava/util/List;

    return-object v0
.end method

.method static synthetic b(Lcom/peel/h/a/am;)Landroid/view/LayoutInflater;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/peel/h/a/am;->h:Landroid/view/LayoutInflater;

    return-object v0
.end method

.method static synthetic c(Lcom/peel/h/a/am;)Z
    .locals 1

    .prologue
    .line 40
    iget-boolean v0, p0, Lcom/peel/h/a/am;->ap:Z

    return v0
.end method

.method static synthetic d(Lcom/peel/h/a/am;)Landroid/content/SharedPreferences;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/peel/h/a/am;->aq:Landroid/content/SharedPreferences;

    return-object v0
.end method

.method static synthetic e(Lcom/peel/h/a/am;)Lcom/peel/h/a/aq;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/peel/h/a/am;->i:Lcom/peel/h/a/aq;

    return-object v0
.end method


# virtual methods
.method public Z()V
    .locals 6

    .prologue
    .line 309
    iget-object v0, p0, Lcom/peel/h/a/am;->d:Lcom/peel/d/a;

    if-nez v0, :cond_0

    .line 310
    new-instance v0, Lcom/peel/d/a;

    sget-object v1, Lcom/peel/d/d;->b:Lcom/peel/d/d;

    sget-object v2, Lcom/peel/d/b;->a:Lcom/peel/d/b;

    sget-object v3, Lcom/peel/d/c;->b:Lcom/peel/d/c;

    sget v4, Lcom/peel/ui/ft;->auto_display_title:I

    invoke-virtual {p0, v4}, Lcom/peel/h/a/am;->a(I)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, Lcom/peel/d/a;-><init>(Lcom/peel/d/d;Lcom/peel/d/b;Lcom/peel/d/c;Ljava/lang/String;Ljava/util/List;)V

    iput-object v0, p0, Lcom/peel/h/a/am;->d:Lcom/peel/d/a;

    .line 312
    :cond_0
    iget-object v0, p0, Lcom/peel/h/a/am;->b:Lcom/peel/d/i;

    iget-object v1, p0, Lcom/peel/h/a/am;->d:Lcom/peel/d/a;

    invoke-virtual {v0, v1}, Lcom/peel/d/i;->a(Lcom/peel/d/a;)V

    .line 313
    return-void
.end method

.method public a(ILandroid/os/Bundle;)Landroid/support/v4/a/n;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/support/v4/a/n",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 259
    new-instance v0, Landroid/support/v4/a/g;

    invoke-virtual {p0}, Lcom/peel/h/a/am;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    sget-object v2, Lcom/peel/provider/a;->a:Landroid/net/Uri;

    const/4 v3, 0x5

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string/jumbo v6, "_id"

    aput-object v6, v3, v4

    const/4 v4, 0x1

    const-string/jumbo v6, "type"

    aput-object v6, v3, v4

    const/4 v4, 0x2

    const-string/jumbo v6, "name"

    aput-object v6, v3, v4

    const/4 v4, 0x3

    const-string/jumbo v6, "wifi_name"

    aput-object v6, v3, v4

    const/4 v4, 0x4

    const-string/jumbo v6, "wifi_bssid"

    aput-object v6, v3, v4

    const-string/jumbo v4, "type = 0"

    move-object v6, v5

    invoke-direct/range {v0 .. v6}, Landroid/support/v4/a/g;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    .line 103
    sget v0, Lcom/peel/ui/fq;->autolocklist:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 104
    sget v0, Lcom/peel/ui/fp;->label_lockscreen:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/peel/h/a/am;->al:Landroid/widget/TextView;

    .line 105
    sget v0, Lcom/peel/ui/fp;->loc_list:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/peel/h/a/am;->g:Landroid/widget/ListView;

    .line 106
    sget v0, Lcom/peel/ui/fp;->circle_location:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/h/a/am;->ao:Landroid/view/View;

    .line 107
    sget v0, Lcom/peel/ui/fp;->no_net:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/peel/h/a/am;->ar:Landroid/widget/RelativeLayout;

    .line 108
    new-instance v0, Lcom/peel/h/a/an;

    invoke-direct {v0, p0}, Lcom/peel/h/a/an;-><init>(Lcom/peel/h/a/am;)V

    invoke-virtual {v1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 113
    iput-object p1, p0, Lcom/peel/h/a/am;->h:Landroid/view/LayoutInflater;

    .line 114
    return-object v1
.end method

.method public a(Landroid/support/v4/a/n;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/a/n",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 247
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/peel/h/a/am;->an:Ljava/util/List;

    .line 249
    const-class v0, Lcom/peel/h/a/am;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "reset location data"

    new-instance v2, Lcom/peel/h/a/ap;

    invoke-direct {v2, p0}, Lcom/peel/h/a/ap;-><init>(Lcom/peel/h/a/am;)V

    invoke-static {v0, v1, v2}, Lcom/peel/util/i;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/lang/Runnable;

    .line 255
    return-void
.end method

.method public a(Landroid/support/v4/a/n;Landroid/database/Cursor;)V
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/a/n",
            "<",
            "Landroid/database/Cursor;",
            ">;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v8, 0x0

    const/4 v12, 0x3

    const/4 v7, 0x1

    const/4 v10, 0x0

    .line 215
    .line 216
    iget-object v0, p0, Lcom/peel/h/a/am;->an:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/peel/h/a/am;->an:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 218
    :cond_0
    if-eqz p2, :cond_1

    invoke-interface {p2}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_1

    move v9, v10

    .line 219
    :goto_0
    invoke-interface {p2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 220
    invoke-interface {p2, v12}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    .line 222
    new-instance v0, Lcom/peel/h/a/au;

    invoke-interface {p2, v10}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    invoke-interface {p2, v7}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    const/4 v1, 0x2

    invoke-interface {p2, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-interface {p2, v12}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    const/4 v1, 0x4

    invoke-interface {p2, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    move-object v1, p0

    invoke-direct/range {v0 .. v8}, Lcom/peel/h/a/au;-><init>(Lcom/peel/h/a/am;IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLcom/peel/h/a/an;)V

    .line 224
    iget-object v1, p0, Lcom/peel/h/a/am;->e:Ljava/lang/String;

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/peel/h/a/am;->e:Ljava/lang/String;

    invoke-virtual {v11, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    if-nez v9, :cond_4

    move v1, v7

    .line 228
    :goto_1
    iget-object v2, p0, Lcom/peel/h/a/am;->an:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move v9, v1

    .line 229
    goto :goto_0

    :cond_1
    move v9, v10

    .line 232
    :cond_2
    iget-object v0, p0, Lcom/peel/h/a/am;->e:Ljava/lang/String;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/peel/h/a/am;->f:Ljava/lang/String;

    if-eqz v0, :cond_3

    if-nez v9, :cond_3

    .line 233
    new-instance v0, Lcom/peel/h/a/au;

    const/4 v2, -0x1

    iget-object v4, p0, Lcom/peel/h/a/am;->e:Ljava/lang/String;

    iget-object v5, p0, Lcom/peel/h/a/am;->e:Ljava/lang/String;

    iget-object v6, p0, Lcom/peel/h/a/am;->f:Ljava/lang/String;

    move-object v1, p0

    move v3, v10

    move v7, v10

    invoke-direct/range {v0 .. v8}, Lcom/peel/h/a/au;-><init>(Lcom/peel/h/a/am;IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLcom/peel/h/a/an;)V

    .line 234
    iget-object v1, p0, Lcom/peel/h/a/am;->an:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 236
    :cond_3
    const-class v0, Lcom/peel/h/a/am;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "load finished"

    new-instance v2, Lcom/peel/h/a/ao;

    invoke-direct {v2, p0}, Lcom/peel/h/a/ao;-><init>(Lcom/peel/h/a/am;)V

    invoke-static {v0, v1, v2}, Lcom/peel/util/i;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/lang/Runnable;

    .line 243
    return-void

    :cond_4
    move v1, v9

    goto :goto_1
.end method

.method public bridge synthetic a(Landroid/support/v4/a/n;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 40
    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p1, p2}, Lcom/peel/h/a/am;->a(Landroid/support/v4/a/n;Landroid/database/Cursor;)V

    return-void
.end method

.method public d(Landroid/os/Bundle;)V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/16 v6, 0x8

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 57
    invoke-super {p0, p1}, Lcom/peel/d/u;->d(Landroid/os/Bundle;)V

    .line 58
    new-instance v0, Lcom/peel/h/a/aq;

    invoke-virtual {p0}, Lcom/peel/h/a/am;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/peel/h/a/aq;-><init>(Lcom/peel/h/a/am;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/peel/h/a/am;->i:Lcom/peel/h/a/aq;

    .line 59
    iget-object v0, p0, Lcom/peel/h/a/am;->g:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/peel/h/a/am;->i:Lcom/peel/h/a/aq;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 61
    invoke-virtual {p0}, Lcom/peel/h/a/am;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    invoke-static {v0}, Lcom/peel/util/bx;->d(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 63
    const-string/jumbo v1, "china"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 64
    iget-object v0, p0, Lcom/peel/h/a/am;->al:Landroid/widget/TextView;

    sget v1, Lcom/peel/ui/ft;->choose_wifi:I

    invoke-virtual {p0, v1}, Lcom/peel/h/a/am;->a(I)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "Wi-Fi"

    const-string/jumbo v3, "WLAN"

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 65
    iput-boolean v5, p0, Lcom/peel/h/a/am;->ap:Z

    .line 67
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/peel/h/a/am;->an:Ljava/util/List;

    .line 68
    invoke-virtual {p0}, Lcom/peel/h/a/am;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    const-string/jumbo v1, "connectivity"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/ae;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 69
    iput-object v7, p0, Lcom/peel/h/a/am;->aj:Landroid/net/wifi/WifiInfo;

    .line 71
    if-eqz v0, :cond_1

    .line 72
    invoke-virtual {v0, v5}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v0

    .line 74
    if-eqz v0, :cond_1

    .line 75
    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isConnectedOrConnecting()Z

    move-result v0

    iput-boolean v0, p0, Lcom/peel/h/a/am;->am:Z

    .line 77
    iget-boolean v0, p0, Lcom/peel/h/a/am;->am:Z

    if-eqz v0, :cond_2

    .line 78
    iget-object v0, p0, Lcom/peel/h/a/am;->ar:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v6}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 79
    iget-object v0, p0, Lcom/peel/h/a/am;->g:Landroid/widget/ListView;

    invoke-virtual {v0, v4}, Landroid/widget/ListView;->setVisibility(I)V

    .line 80
    invoke-virtual {p0}, Lcom/peel/h/a/am;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    const-string/jumbo v1, "wifi"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/ae;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiManager;

    iput-object v0, p0, Lcom/peel/h/a/am;->ak:Landroid/net/wifi/WifiManager;

    .line 81
    iget-object v0, p0, Lcom/peel/h/a/am;->ak:Landroid/net/wifi/WifiManager;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->getConnectionInfo()Landroid/net/wifi/WifiInfo;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/h/a/am;->aj:Landroid/net/wifi/WifiInfo;

    .line 83
    iget-object v0, p0, Lcom/peel/h/a/am;->aj:Landroid/net/wifi/WifiInfo;

    if-eqz v0, :cond_1

    .line 84
    iget-object v0, p0, Lcom/peel/h/a/am;->aj:Landroid/net/wifi/WifiInfo;

    invoke-virtual {v0}, Landroid/net/wifi/WifiInfo;->getSSID()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "\""

    const-string/jumbo v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/h/a/am;->e:Ljava/lang/String;

    .line 85
    iget-object v0, p0, Lcom/peel/h/a/am;->aj:Landroid/net/wifi/WifiInfo;

    invoke-virtual {v0}, Landroid/net/wifi/WifiInfo;->getBSSID()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/h/a/am;->f:Ljava/lang/String;

    .line 93
    :cond_1
    :goto_0
    invoke-virtual {p0}, Lcom/peel/h/a/am;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    sget v1, Lcom/peel/ui/fj;->splash_spinner_rotate:I

    invoke-static {v0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    .line 94
    const-wide/16 v2, 0x2710

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 95
    iget-object v1, p0, Lcom/peel/h/a/am;->ao:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 96
    invoke-virtual {p0}, Lcom/peel/h/a/am;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/h/a/am;->aq:Landroid/content/SharedPreferences;

    .line 98
    invoke-virtual {p0}, Lcom/peel/h/a/am;->u()Landroid/support/v4/app/bf;

    move-result-object v0

    invoke-virtual {v0, v4, v7, p0}, Landroid/support/v4/app/bf;->a(ILandroid/os/Bundle;Landroid/support/v4/app/bg;)Landroid/support/v4/a/n;

    .line 99
    return-void

    .line 88
    :cond_2
    iget-object v0, p0, Lcom/peel/h/a/am;->ar:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v4}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 89
    iget-object v0, p0, Lcom/peel/h/a/am;->g:Landroid/widget/ListView;

    invoke-virtual {v0, v6}, Landroid/widget/ListView;->setVisibility(I)V

    goto :goto_0
.end method

.method public w()V
    .locals 0

    .prologue
    .line 317
    invoke-super {p0}, Lcom/peel/d/u;->w()V

    .line 318
    invoke-virtual {p0}, Lcom/peel/h/a/am;->Z()V

    .line 319
    return-void
.end method
