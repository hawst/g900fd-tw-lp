.class final Lcom/peel/h/a/ah;
.super Landroid/support/v4/view/av;


# instance fields
.field final synthetic a:Lcom/peel/h/a/a;

.field private b:I

.field private c:I


# direct methods
.method public constructor <init>(Lcom/peel/h/a/a;II)V
    .locals 0

    .prologue
    .line 1311
    iput-object p1, p0, Lcom/peel/h/a/ah;->a:Lcom/peel/h/a/a;

    invoke-direct {p0}, Landroid/support/v4/view/av;-><init>()V

    .line 1312
    iput p3, p0, Lcom/peel/h/a/ah;->b:I

    .line 1313
    iput p2, p0, Lcom/peel/h/a/ah;->c:I

    .line 1314
    return-void
.end method


# virtual methods
.method public a(Landroid/view/ViewGroup;I)Ljava/lang/Object;
    .locals 10

    .prologue
    const/4 v4, 0x2

    const/4 v9, 0x1

    const v8, 0x106000d

    const/4 v7, 0x0

    const/4 v6, -0x2

    .line 1317
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string/jumbo v1, "layout_inflater"

    .line 1318
    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 1320
    iget v1, p0, Lcom/peel/h/a/ah;->c:I

    if-ne v1, v4, :cond_0

    sget v1, Lcom/peel/ui/fq;->test_other_btn_pagers_view:I

    .line 1322
    :goto_0
    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .line 1323
    sget v0, Lcom/peel/ui/fp;->button_num_text:I

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 1324
    sget v1, Lcom/peel/ui/fp;->button_num_text_small:I

    invoke-virtual {v3, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 1326
    iget v2, p0, Lcom/peel/h/a/ah;->c:I

    if-ne v2, v4, :cond_1

    .line 1327
    sget v2, Lcom/peel/ui/fp;->test_other_btn_large_view:I

    invoke-virtual {v3, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    sget v4, Lcom/peel/ui/fo;->setup_test_stb_ch_up_btn_states:I

    invoke-virtual {v2, v4}, Landroid/view/View;->setBackgroundResource(I)V

    .line 1328
    sget v2, Lcom/peel/ui/fp;->test_other_btn_small_view:I

    invoke-virtual {v3, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    sget v4, Lcom/peel/ui/fo;->setup_test_stb_ch_up_btn_states:I

    invoke-virtual {v2, v4}, Landroid/view/View;->setBackgroundResource(I)V

    .line 1340
    new-instance v4, Landroid/widget/Button;

    iget-object v2, p0, Lcom/peel/h/a/ah;->a:Lcom/peel/h/a/a;

    invoke-virtual {v2}, Lcom/peel/h/a/a;->m()Landroid/support/v4/app/ae;

    move-result-object v2

    invoke-direct {v4, v2}, Landroid/widget/Button;-><init>(Landroid/content/Context;)V

    .line 1341
    new-instance v2, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v2, v6, v6}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1342
    const/16 v5, 0x66

    invoke-static {v5}, Lcom/peel/util/dw;->a(I)I

    move-result v5

    iput v5, v2, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    .line 1343
    const/16 v5, 0x69

    invoke-static {v5}, Lcom/peel/util/dw;->a(I)I

    move-result v5

    iput v5, v2, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    .line 1344
    invoke-virtual {v4, v2}, Landroid/widget/Button;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1346
    iget-object v2, p0, Lcom/peel/h/a/ah;->a:Lcom/peel/h/a/a;

    invoke-virtual {v2}, Lcom/peel/h/a/a;->n()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v8}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v4, v2}, Landroid/widget/Button;->setBackgroundColor(I)V

    .line 1347
    iget-object v2, p0, Lcom/peel/h/a/ah;->a:Lcom/peel/h/a/a;

    sget v5, Lcom/peel/ui/ft;->channel:I

    invoke-virtual {v2, v5}, Lcom/peel/h/a/a;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Landroid/widget/Button;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1349
    new-instance v2, Lcom/peel/h/a/ai;

    invoke-direct {v2, p0}, Lcom/peel/h/a/ai;-><init>(Lcom/peel/h/a/ah;)V

    invoke-virtual {v4, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1356
    sget v2, Lcom/peel/ui/fp;->test_other_btn_large_view:I

    invoke-virtual {v3, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v4, v7}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;I)V

    .line 1368
    new-instance v4, Landroid/widget/Button;

    iget-object v2, p0, Lcom/peel/h/a/ah;->a:Lcom/peel/h/a/a;

    invoke-virtual {v2}, Lcom/peel/h/a/a;->m()Landroid/support/v4/app/ae;

    move-result-object v2

    invoke-direct {v4, v2}, Landroid/widget/Button;-><init>(Landroid/content/Context;)V

    .line 1369
    new-instance v2, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v2, v6, v6}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1370
    const/16 v5, 0x47

    invoke-static {v5}, Lcom/peel/util/dw;->a(I)I

    move-result v5

    iput v5, v2, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    .line 1371
    const/16 v5, 0x48

    invoke-static {v5}, Lcom/peel/util/dw;->a(I)I

    move-result v5

    iput v5, v2, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    .line 1372
    invoke-virtual {v4, v2}, Landroid/widget/Button;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1374
    iget-object v2, p0, Lcom/peel/h/a/ah;->a:Lcom/peel/h/a/a;

    invoke-virtual {v2}, Lcom/peel/h/a/a;->n()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v8}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v4, v2}, Landroid/widget/Button;->setBackgroundColor(I)V

    .line 1375
    iget-object v2, p0, Lcom/peel/h/a/ah;->a:Lcom/peel/h/a/a;

    sget v5, Lcom/peel/ui/ft;->channel:I

    invoke-virtual {v2, v5}, Lcom/peel/h/a/a;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Landroid/widget/Button;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1377
    new-instance v2, Lcom/peel/h/a/aj;

    invoke-direct {v2, p0}, Lcom/peel/h/a/aj;-><init>(Lcom/peel/h/a/ah;)V

    invoke-virtual {v4, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1384
    sget v2, Lcom/peel/ui/fp;->test_other_btn_small_view:I

    invoke-virtual {v3, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v4, v7}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;I)V

    .line 1446
    :goto_1
    iget-object v2, p0, Lcom/peel/h/a/ah;->a:Lcom/peel/h/a/a;

    invoke-virtual {v2}, Lcom/peel/h/a/a;->n()Landroid/content/res/Resources;

    move-result-object v2

    sget v4, Lcom/peel/ui/ft;->testing_btn_number:I

    new-array v5, v9, [Ljava/lang/Object;

    add-int/lit8 v6, p2, 0x1

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-virtual {v2, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1447
    iget-object v0, p0, Lcom/peel/h/a/ah;->a:Lcom/peel/h/a/a;

    invoke-virtual {v0}, Lcom/peel/h/a/a;->n()Landroid/content/res/Resources;

    move-result-object v0

    sget v2, Lcom/peel/ui/ft;->testing_btn_number:I

    new-array v4, v9, [Ljava/lang/Object;

    add-int/lit8 v5, p2, 0x1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v7

    invoke-virtual {v0, v2, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1449
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "btnView"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 1450
    invoke-virtual {p1, v3}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 1452
    return-object v3

    .line 1320
    :cond_0
    sget v1, Lcom/peel/ui/fq;->test_pw_btn_pagers_view:I

    goto/16 :goto_0

    .line 1386
    :cond_1
    sget v2, Lcom/peel/ui/fp;->test_pw_btn_large_view:I

    invoke-virtual {v3, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    sget v4, Lcom/peel/ui/fo;->initial_tv_power_onoff_stateful:I

    invoke-virtual {v2, v4}, Landroid/view/View;->setBackgroundResource(I)V

    .line 1387
    sget v2, Lcom/peel/ui/fp;->test_pw_btn_small_view:I

    invoke-virtual {v3, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    sget v4, Lcom/peel/ui/fo;->initial_tv_power_onoff_stateful:I

    invoke-virtual {v2, v4}, Landroid/view/View;->setBackgroundResource(I)V

    .line 1399
    new-instance v4, Landroid/widget/Button;

    iget-object v2, p0, Lcom/peel/h/a/ah;->a:Lcom/peel/h/a/a;

    invoke-virtual {v2}, Lcom/peel/h/a/a;->m()Landroid/support/v4/app/ae;

    move-result-object v2

    invoke-direct {v4, v2}, Landroid/widget/Button;-><init>(Landroid/content/Context;)V

    .line 1400
    new-instance v2, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v2, v6, v6}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1401
    const/16 v5, 0x66

    invoke-static {v5}, Lcom/peel/util/dw;->a(I)I

    move-result v5

    iput v5, v2, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    .line 1402
    const/16 v5, 0x69

    invoke-static {v5}, Lcom/peel/util/dw;->a(I)I

    move-result v5

    iput v5, v2, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    .line 1403
    invoke-virtual {v4, v2}, Landroid/widget/Button;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1405
    iget-object v2, p0, Lcom/peel/h/a/ah;->a:Lcom/peel/h/a/a;

    invoke-virtual {v2}, Lcom/peel/h/a/a;->n()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v8}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v4, v2}, Landroid/widget/Button;->setBackgroundColor(I)V

    .line 1406
    iget-object v2, p0, Lcom/peel/h/a/ah;->a:Lcom/peel/h/a/a;

    sget v5, Lcom/peel/ui/ft;->power:I

    invoke-virtual {v2, v5}, Lcom/peel/h/a/a;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Landroid/widget/Button;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1408
    new-instance v2, Lcom/peel/h/a/ak;

    invoke-direct {v2, p0}, Lcom/peel/h/a/ak;-><init>(Lcom/peel/h/a/ah;)V

    invoke-virtual {v4, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1415
    sget v2, Lcom/peel/ui/fp;->test_pw_btn_large_view:I

    invoke-virtual {v3, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v4, v7}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;I)V

    .line 1427
    new-instance v4, Landroid/widget/Button;

    iget-object v2, p0, Lcom/peel/h/a/ah;->a:Lcom/peel/h/a/a;

    invoke-virtual {v2}, Lcom/peel/h/a/a;->m()Landroid/support/v4/app/ae;

    move-result-object v2

    invoke-direct {v4, v2}, Landroid/widget/Button;-><init>(Landroid/content/Context;)V

    .line 1428
    new-instance v2, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v2, v6, v6}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1429
    const/16 v5, 0x47

    invoke-static {v5}, Lcom/peel/util/dw;->a(I)I

    move-result v5

    iput v5, v2, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    .line 1430
    const/16 v5, 0x48

    invoke-static {v5}, Lcom/peel/util/dw;->a(I)I

    move-result v5

    iput v5, v2, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    .line 1431
    invoke-virtual {v4, v2}, Landroid/widget/Button;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1433
    iget-object v2, p0, Lcom/peel/h/a/ah;->a:Lcom/peel/h/a/a;

    invoke-virtual {v2}, Lcom/peel/h/a/a;->n()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v8}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v4, v2}, Landroid/widget/Button;->setBackgroundColor(I)V

    .line 1434
    iget-object v2, p0, Lcom/peel/h/a/ah;->a:Lcom/peel/h/a/a;

    sget v5, Lcom/peel/ui/ft;->power:I

    invoke-virtual {v2, v5}, Lcom/peel/h/a/a;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Landroid/widget/Button;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1436
    new-instance v2, Lcom/peel/h/a/al;

    invoke-direct {v2, p0}, Lcom/peel/h/a/al;-><init>(Lcom/peel/h/a/ah;)V

    invoke-virtual {v4, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1443
    sget v2, Lcom/peel/ui/fp;->test_pw_btn_small_view:I

    invoke-virtual {v3, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v4, v7}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;I)V

    goto/16 :goto_1
.end method

.method public a(Landroid/view/ViewGroup;ILjava/lang/Object;)V
    .locals 0

    .prologue
    .line 1457
    check-cast p3, Landroid/view/View;

    invoke-virtual {p1, p3}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 1458
    return-void
.end method

.method public a(Landroid/view/View;Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 1462
    check-cast p2, Landroid/view/View;

    if-ne p1, p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()I
    .locals 1

    .prologue
    .line 1467
    iget v0, p0, Lcom/peel/h/a/ah;->b:I

    return v0
.end method
