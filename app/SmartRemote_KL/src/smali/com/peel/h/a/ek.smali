.class Lcom/peel/h/a/ek;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/widget/TextView$OnEditorActionListener;


# instance fields
.field final synthetic a:Lcom/peel/widget/ag;

.field final synthetic b:Landroid/widget/EditText;

.field final synthetic c:Lcom/peel/h/a/ej;


# direct methods
.method constructor <init>(Lcom/peel/h/a/ej;Lcom/peel/widget/ag;Landroid/widget/EditText;)V
    .locals 0

    .prologue
    .line 139
    iput-object p1, p0, Lcom/peel/h/a/ek;->c:Lcom/peel/h/a/ej;

    iput-object p2, p0, Lcom/peel/h/a/ek;->a:Lcom/peel/widget/ag;

    iput-object p3, p0, Lcom/peel/h/a/ek;->b:Landroid/widget/EditText;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onEditorAction(Landroid/widget/TextView;ILandroid/view/KeyEvent;)Z
    .locals 3

    .prologue
    .line 142
    const/4 v0, 0x6

    if-ne p2, v0, :cond_0

    .line 143
    iget-object v0, p0, Lcom/peel/h/a/ek;->a:Lcom/peel/widget/ag;

    invoke-virtual {v0}, Lcom/peel/widget/ag;->dismiss()V

    .line 144
    iget-object v0, p0, Lcom/peel/h/a/ek;->b:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/peel/h/a/ek;->b:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "Delay"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 145
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 146
    const-string/jumbo v1, "id"

    iget-object v2, p0, Lcom/peel/h/a/ek;->c:Lcom/peel/h/a/ej;

    iget-object v2, v2, Lcom/peel/h/a/ej;->c:Lcom/peel/h/a/ei;

    invoke-static {v2}, Lcom/peel/h/a/ei;->a(Lcom/peel/h/a/ei;)Lcom/peel/control/h;

    move-result-object v2

    invoke-virtual {v2}, Lcom/peel/control/h;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 147
    const-string/jumbo v1, "cmd"

    iget-object v2, p0, Lcom/peel/h/a/ek;->b:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 149
    iget-object v1, p0, Lcom/peel/h/a/ek;->c:Lcom/peel/h/a/ej;

    iget-object v1, v1, Lcom/peel/h/a/ej;->c:Lcom/peel/h/a/ei;

    iget-object v1, v1, Lcom/peel/h/a/ei;->b:Lcom/peel/d/i;

    invoke-virtual {v1}, Lcom/peel/d/i;->c()Landroid/support/v4/app/ae;

    move-result-object v1

    const-class v2, Lcom/peel/h/a/em;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lcom/peel/d/e;->c(Landroid/support/v4/app/ae;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 152
    :cond_0
    const/4 v0, 0x0

    return v0
.end method
