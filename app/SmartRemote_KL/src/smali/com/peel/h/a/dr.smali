.class Lcom/peel/h/a/dr;
.super Lcom/peel/util/t;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/peel/util/t",
        "<",
        "Ljava/util/List",
        "<",
        "Lcom/peel/content/listing/Listing;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/peel/h/a/dq;


# direct methods
.method constructor <init>(Lcom/peel/h/a/dq;)V
    .locals 0

    .prologue
    .line 87
    iput-object p1, p0, Lcom/peel/h/a/dr;->a:Lcom/peel/h/a/dq;

    invoke-direct {p0}, Lcom/peel/util/t;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(ZLjava/lang/Object;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 87
    check-cast p2, Ljava/util/List;

    invoke-virtual {p0, p1, p2, p3}, Lcom/peel/h/a/dr;->a(ZLjava/util/List;Ljava/lang/String;)V

    return-void
.end method

.method public a(ZLjava/util/List;Ljava/lang/String;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Ljava/util/List",
            "<",
            "Lcom/peel/content/listing/Listing;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 91
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 92
    :cond_0
    invoke-static {}, Lcom/peel/h/a/do;->c()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "error getting fav/cut nodes: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 93
    iget-object v0, p0, Lcom/peel/h/a/dr;->a:Lcom/peel/h/a/dq;

    iget-object v0, v0, Lcom/peel/h/a/dq;->b:Lcom/peel/h/a/do;

    invoke-static {v0}, Lcom/peel/h/a/do;->b(Lcom/peel/h/a/do;)V

    .line 118
    :goto_0
    return-void

    .line 96
    :cond_1
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 98
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/content/listing/Listing;

    move-object v1, v0

    .line 99
    check-cast v1, Lcom/peel/content/listing/LiveListing;

    invoke-virtual {v1}, Lcom/peel/content/listing/LiveListing;->b()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v2, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 102
    :cond_2
    invoke-static {}, Lcom/peel/h/a/do;->c()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "populate adapter"

    new-instance v3, Lcom/peel/h/a/ds;

    invoke-direct {v3, p0, v2}, Lcom/peel/h/a/ds;-><init>(Lcom/peel/h/a/dr;Ljava/util/Map;)V

    invoke-static {v0, v1, v3}, Lcom/peel/util/i;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/lang/Runnable;

    goto :goto_0
.end method
