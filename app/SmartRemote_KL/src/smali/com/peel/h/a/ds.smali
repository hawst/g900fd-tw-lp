.class Lcom/peel/h/a/ds;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Ljava/util/Map;

.field final synthetic b:Lcom/peel/h/a/dr;


# direct methods
.method constructor <init>(Lcom/peel/h/a/dr;Ljava/util/Map;)V
    .locals 0

    .prologue
    .line 102
    iput-object p1, p0, Lcom/peel/h/a/ds;->b:Lcom/peel/h/a/dr;

    iput-object p2, p0, Lcom/peel/h/a/ds;->a:Ljava/util/Map;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 106
    iget-object v0, p0, Lcom/peel/h/a/ds;->b:Lcom/peel/h/a/dr;

    iget-object v0, v0, Lcom/peel/h/a/dr;->a:Lcom/peel/h/a/dq;

    iget-object v0, v0, Lcom/peel/h/a/dq;->b:Lcom/peel/h/a/do;

    new-instance v1, Lcom/peel/h/a/dl;

    iget-object v2, p0, Lcom/peel/h/a/ds;->b:Lcom/peel/h/a/dr;

    iget-object v2, v2, Lcom/peel/h/a/dr;->a:Lcom/peel/h/a/dq;

    iget-object v2, v2, Lcom/peel/h/a/dq;->b:Lcom/peel/h/a/do;

    invoke-virtual {v2}, Lcom/peel/h/a/do;->m()Landroid/support/v4/app/ae;

    move-result-object v2

    iget-object v3, p0, Lcom/peel/h/a/ds;->b:Lcom/peel/h/a/dr;

    iget-object v3, v3, Lcom/peel/h/a/dr;->a:Lcom/peel/h/a/dq;

    iget-object v3, v3, Lcom/peel/h/a/dq;->a:Ljava/util/Map;

    iget-object v4, p0, Lcom/peel/h/a/ds;->b:Lcom/peel/h/a/dr;

    iget-object v4, v4, Lcom/peel/h/a/dr;->a:Lcom/peel/h/a/dq;

    iget-object v4, v4, Lcom/peel/h/a/dq;->b:Lcom/peel/h/a/do;

    invoke-static {v4}, Lcom/peel/h/a/do;->c(Lcom/peel/h/a/do;)I

    move-result v4

    iget-object v5, p0, Lcom/peel/h/a/ds;->a:Ljava/util/Map;

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/peel/h/a/dl;-><init>(Landroid/content/Context;Ljava/util/Map;ILjava/util/Map;)V

    invoke-static {v0, v1}, Lcom/peel/h/a/do;->a(Lcom/peel/h/a/do;Lcom/peel/h/a/dl;)Lcom/peel/h/a/dl;

    .line 107
    iget-object v0, p0, Lcom/peel/h/a/ds;->b:Lcom/peel/h/a/dr;

    iget-object v0, v0, Lcom/peel/h/a/dr;->a:Lcom/peel/h/a/dq;

    iget-object v0, v0, Lcom/peel/h/a/dq;->b:Lcom/peel/h/a/do;

    invoke-static {v0}, Lcom/peel/h/a/do;->d(Lcom/peel/h/a/do;)Landroid/widget/ListView;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/h/a/ds;->b:Lcom/peel/h/a/dr;

    iget-object v1, v1, Lcom/peel/h/a/dr;->a:Lcom/peel/h/a/dq;

    iget-object v1, v1, Lcom/peel/h/a/dq;->b:Lcom/peel/h/a/do;

    invoke-static {v1}, Lcom/peel/h/a/do;->a(Lcom/peel/h/a/do;)Lcom/peel/h/a/dl;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 108
    iget-object v0, p0, Lcom/peel/h/a/ds;->b:Lcom/peel/h/a/dr;

    iget-object v0, v0, Lcom/peel/h/a/dr;->a:Lcom/peel/h/a/dq;

    iget-object v0, v0, Lcom/peel/h/a/dq;->b:Lcom/peel/h/a/do;

    invoke-static {v0}, Lcom/peel/h/a/do;->e(Lcom/peel/h/a/do;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 109
    iget-object v0, p0, Lcom/peel/h/a/ds;->b:Lcom/peel/h/a/dr;

    iget-object v0, v0, Lcom/peel/h/a/dr;->a:Lcom/peel/h/a/dq;

    iget-object v0, v0, Lcom/peel/h/a/dq;->b:Lcom/peel/h/a/do;

    invoke-static {v0}, Lcom/peel/h/a/do;->f(Lcom/peel/h/a/do;)Landroid/widget/TextView;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 110
    iget-object v0, p0, Lcom/peel/h/a/ds;->b:Lcom/peel/h/a/dr;

    iget-object v0, v0, Lcom/peel/h/a/dr;->a:Lcom/peel/h/a/dq;

    iget-object v0, v0, Lcom/peel/h/a/dq;->b:Lcom/peel/h/a/do;

    invoke-static {v0}, Lcom/peel/h/a/do;->d(Lcom/peel/h/a/do;)Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/widget/ListView;->setVisibility(I)V

    .line 114
    iget-object v0, p0, Lcom/peel/h/a/ds;->b:Lcom/peel/h/a/dr;

    iget-object v0, v0, Lcom/peel/h/a/dr;->a:Lcom/peel/h/a/dq;

    iget-object v0, v0, Lcom/peel/h/a/dq;->b:Lcom/peel/h/a/do;

    invoke-static {v0}, Lcom/peel/h/a/do;->a(Lcom/peel/h/a/do;)Lcom/peel/h/a/dl;

    move-result-object v0

    invoke-virtual {v0, v6}, Lcom/peel/h/a/dl;->a(Z)V

    .line 115
    iget-object v0, p0, Lcom/peel/h/a/ds;->b:Lcom/peel/h/a/dr;

    iget-object v0, v0, Lcom/peel/h/a/dr;->a:Lcom/peel/h/a/dq;

    iget-object v0, v0, Lcom/peel/h/a/dq;->b:Lcom/peel/h/a/do;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/peel/h/a/do;->a(Lcom/peel/h/a/do;I)V

    .line 116
    return-void
.end method
