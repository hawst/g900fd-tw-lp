.class public Lcom/peel/h/a/ff;
.super Lcom/peel/d/u;


# instance fields
.field private aj:Z

.field private ak:[Lcom/peel/data/Channel;

.field private al:Lcom/peel/content/library/LiveLibrary;

.field private final am:Lcom/jess/ui/z;

.field private e:Landroid/widget/LinearLayout;

.field private f:Landroid/view/LayoutInflater;

.field private g:Lcom/jess/ui/TwoWayGridView;

.field private h:Landroid/content/Context;

.field private i:Lcom/peel/ui/d;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 43
    invoke-direct {p0}, Lcom/peel/d/u;-><init>()V

    .line 53
    new-instance v0, Lcom/peel/h/a/fg;

    invoke-direct {v0, p0}, Lcom/peel/h/a/fg;-><init>(Lcom/peel/h/a/ff;)V

    iput-object v0, p0, Lcom/peel/h/a/ff;->am:Lcom/jess/ui/z;

    return-void
.end method

.method static synthetic a(Lcom/peel/h/a/ff;Lcom/jess/ui/TwoWayGridView;)Lcom/jess/ui/TwoWayGridView;
    .locals 0

    .prologue
    .line 43
    iput-object p1, p0, Lcom/peel/h/a/ff;->g:Lcom/jess/ui/TwoWayGridView;

    return-object p1
.end method

.method static synthetic a(Lcom/peel/h/a/ff;Lcom/peel/ui/d;)Lcom/peel/ui/d;
    .locals 0

    .prologue
    .line 43
    iput-object p1, p0, Lcom/peel/h/a/ff;->i:Lcom/peel/ui/d;

    return-object p1
.end method

.method static synthetic a(Lcom/peel/h/a/ff;)[Lcom/peel/data/Channel;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/peel/h/a/ff;->ak:[Lcom/peel/data/Channel;

    return-object v0
.end method

.method static synthetic b(Lcom/peel/h/a/ff;)Z
    .locals 1

    .prologue
    .line 43
    iget-boolean v0, p0, Lcom/peel/h/a/ff;->aj:Z

    return v0
.end method

.method static synthetic c(Lcom/peel/h/a/ff;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/peel/h/a/ff;->a:Ljava/lang/String;

    return-object v0
.end method

.method private c()V
    .locals 5

    .prologue
    .line 196
    iget-object v0, p0, Lcom/peel/h/a/ff;->al:Lcom/peel/content/library/LiveLibrary;

    invoke-virtual {v0}, Lcom/peel/content/library/LiveLibrary;->i()[Lcom/peel/data/Channel;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/h/a/ff;->ak:[Lcom/peel/data/Channel;

    .line 197
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 198
    const-string/jumbo v1, "path"

    const-string/jumbo v2, "personalize"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 199
    const-string/jumbo v1, "country"

    invoke-virtual {p0}, Lcom/peel/h/a/ff;->m()Landroid/support/v4/app/ae;

    move-result-object v2

    invoke-static {v2}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v2

    const-string/jumbo v3, "country_ISO"

    const-string/jumbo v4, "US"

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 201
    invoke-virtual {p0}, Lcom/peel/h/a/ff;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string/jumbo v1, "country_ISO"

    const-string/jumbo v2, "US"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/h/a/ff;->al:Lcom/peel/content/library/LiveLibrary;

    .line 202
    invoke-virtual {v1}, Lcom/peel/content/library/LiveLibrary;->e()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/peel/h/a/fh;

    const/4 v3, 0x1

    invoke-direct {v2, p0, v3}, Lcom/peel/h/a/fh;-><init>(Lcom/peel/h/a/ff;I)V

    .line 200
    invoke-static {v0, v1, v2}, Lcom/peel/content/a/j;->c(Ljava/lang/String;Ljava/lang/String;Lcom/peel/util/t;)V

    .line 262
    return-void
.end method

.method static synthetic d(Lcom/peel/h/a/ff;)Lcom/peel/content/library/LiveLibrary;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/peel/h/a/ff;->al:Lcom/peel/content/library/LiveLibrary;

    return-object v0
.end method

.method static synthetic e(Lcom/peel/h/a/ff;)Landroid/view/LayoutInflater;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/peel/h/a/ff;->f:Landroid/view/LayoutInflater;

    return-object v0
.end method

.method static synthetic f(Lcom/peel/h/a/ff;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/peel/h/a/ff;->a:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic g(Lcom/peel/h/a/ff;)Lcom/peel/ui/d;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/peel/h/a/ff;->i:Lcom/peel/ui/d;

    return-object v0
.end method

.method static synthetic h(Lcom/peel/h/a/ff;)Lcom/jess/ui/TwoWayGridView;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/peel/h/a/ff;->g:Lcom/jess/ui/TwoWayGridView;

    return-object v0
.end method

.method static synthetic i(Lcom/peel/h/a/ff;)Lcom/jess/ui/z;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/peel/h/a/ff;->am:Lcom/jess/ui/z;

    return-object v0
.end method

.method static synthetic j(Lcom/peel/h/a/ff;)Landroid/widget/LinearLayout;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/peel/h/a/ff;->e:Landroid/widget/LinearLayout;

    return-object v0
.end method


# virtual methods
.method public Z()V
    .locals 7

    .prologue
    .line 170
    iget-object v0, p0, Lcom/peel/h/a/ff;->d:Lcom/peel/d/a;

    if-nez v0, :cond_1

    .line 171
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 172
    iget-boolean v0, p0, Lcom/peel/h/a/ff;->aj:Z

    if-eqz v0, :cond_0

    .line 173
    sget v0, Lcom/peel/ui/fp;->menu_next:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 176
    :cond_0
    new-instance v0, Lcom/peel/d/a;

    sget-object v1, Lcom/peel/d/d;->b:Lcom/peel/d/d;

    iget-boolean v2, p0, Lcom/peel/h/a/ff;->aj:Z

    if-eqz v2, :cond_2

    sget-object v2, Lcom/peel/d/b;->b:Lcom/peel/d/b;

    :goto_0
    sget-object v3, Lcom/peel/d/c;->b:Lcom/peel/d/c;

    invoke-virtual {p0}, Lcom/peel/h/a/ff;->n()Landroid/content/res/Resources;

    move-result-object v4

    sget v6, Lcom/peel/ui/ft;->personalization:I

    invoke-virtual {v4, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct/range {v0 .. v5}, Lcom/peel/d/a;-><init>(Lcom/peel/d/d;Lcom/peel/d/b;Lcom/peel/d/c;Ljava/lang/String;Ljava/util/List;)V

    iput-object v0, p0, Lcom/peel/h/a/ff;->d:Lcom/peel/d/a;

    .line 178
    :cond_1
    iget-object v0, p0, Lcom/peel/h/a/ff;->b:Lcom/peel/d/i;

    iget-object v1, p0, Lcom/peel/h/a/ff;->d:Lcom/peel/d/a;

    invoke-virtual {v0, v1}, Lcom/peel/d/i;->a(Lcom/peel/d/a;)V

    .line 179
    return-void

    .line 176
    :cond_2
    sget-object v2, Lcom/peel/d/b;->a:Lcom/peel/d/b;

    goto :goto_0
.end method

.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    .line 145
    iput-object p1, p0, Lcom/peel/h/a/ff;->f:Landroid/view/LayoutInflater;

    .line 146
    sget v0, Lcom/peel/ui/fq;->personalization:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 147
    sget v0, Lcom/peel/ui/fp;->container:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/peel/h/a/ff;->e:Landroid/widget/LinearLayout;

    .line 148
    invoke-virtual {p0}, Lcom/peel/h/a/ff;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/ae;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/h/a/ff;->h:Landroid/content/Context;

    .line 150
    return-object v1
.end method

.method public a(Landroid/view/MenuItem;)Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 183
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    .line 184
    sget v1, Lcom/peel/ui/fp;->menu_next:I

    if-ne v0, v1, :cond_0

    .line 185
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 186
    const-string/jumbo v1, "has_done"

    invoke-virtual {v0, v1, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 187
    const-string/jumbo v1, "from_setup"

    iget-boolean v2, p0, Lcom/peel/h/a/ff;->aj:Z

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 188
    invoke-virtual {p0}, Lcom/peel/h/a/ff;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    const-class v2, Lcom/peel/h/a/av;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lcom/peel/d/e;->c(Landroid/support/v4/app/ae;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 189
    iget-object v0, p0, Lcom/peel/h/a/ff;->h:Landroid/content/Context;

    invoke-static {v0, v3}, Lcom/peel/util/dq;->a(Landroid/content/Context;Z)V

    .line 190
    iget-object v0, p0, Lcom/peel/h/a/ff;->h:Landroid/content/Context;

    const-string/jumbo v1, "setup_type"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/peel/util/dq;->a(Landroid/content/Context;Ljava/lang/String;I)V

    .line 192
    :cond_0
    invoke-super {p0, p1}, Lcom/peel/d/u;->a(Landroid/view/MenuItem;)Z

    move-result v0

    return v0
.end method

.method public a_(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 126
    invoke-super {p0, p1}, Lcom/peel/d/u;->a_(Landroid/os/Bundle;)V

    .line 127
    iget-object v0, p0, Lcom/peel/h/a/ff;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "fromSetup"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/peel/h/a/ff;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "fromSetup"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/peel/h/a/ff;->aj:Z

    .line 129
    :cond_0
    const-string/jumbo v0, "live"

    invoke-static {v0}, Lcom/peel/content/a;->c(Ljava/lang/String;)Lcom/peel/content/library/Library;

    move-result-object v0

    check-cast v0, Lcom/peel/content/library/LiveLibrary;

    iput-object v0, p0, Lcom/peel/h/a/ff;->al:Lcom/peel/content/library/LiveLibrary;

    .line 131
    invoke-static {}, Lcom/peel/util/a/f;->a()Lcom/peel/util/a/f;

    move-result-object v2

    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v0}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    const/16 v3, 0xbf9

    iget-boolean v1, p0, Lcom/peel/h/a/ff;->aj:Z

    if-eqz v1, :cond_2

    const/16 v1, 0x7d5

    :goto_1
    invoke-virtual {v2, v0, v3, v1}, Lcom/peel/util/a/f;->a(III)V

    .line 134
    return-void

    .line 131
    :cond_1
    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v0}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/data/at;->f()I

    move-result v0

    goto :goto_0

    :cond_2
    const/16 v1, 0x7d8

    goto :goto_1
.end method

.method public e()V
    .locals 3

    .prologue
    .line 155
    invoke-super {p0}, Lcom/peel/d/u;->e()V

    .line 156
    invoke-virtual {p0}, Lcom/peel/h/a/ff;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    const-string/jumbo v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/ae;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 157
    invoke-virtual {p0}, Lcom/peel/h/a/ff;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/ae;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 158
    return-void
.end method

.method public h(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 162
    invoke-super {p0, p1}, Lcom/peel/d/u;->h(Landroid/os/Bundle;)V

    .line 163
    invoke-virtual {p0}, Lcom/peel/h/a/ff;->Z()V

    .line 164
    invoke-direct {p0}, Lcom/peel/h/a/ff;->c()V

    .line 165
    return-void
.end method

.method public w()V
    .locals 3

    .prologue
    .line 138
    invoke-super {p0}, Lcom/peel/d/u;->w()V

    .line 139
    invoke-virtual {p0}, Lcom/peel/h/a/ff;->Z()V

    .line 140
    invoke-virtual {p0}, Lcom/peel/h/a/ff;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    const-string/jumbo v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/ae;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 141
    invoke-virtual {p0}, Lcom/peel/h/a/ff;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/ae;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 142
    return-void
.end method
