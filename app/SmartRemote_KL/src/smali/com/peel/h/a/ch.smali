.class Lcom/peel/h/a/ch;
.super Landroid/widget/ArrayAdapter;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Landroid/view/LayoutInflater;

.field final synthetic b:[Ljava/lang/String;

.field final synthetic c:Lcom/peel/h/a/bz;


# direct methods
.method constructor <init>(Lcom/peel/h/a/bz;Landroid/content/Context;I[Ljava/lang/String;Landroid/view/LayoutInflater;[Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 184
    iput-object p1, p0, Lcom/peel/h/a/ch;->c:Lcom/peel/h/a/bz;

    iput-object p5, p0, Lcom/peel/h/a/ch;->a:Landroid/view/LayoutInflater;

    iput-object p6, p0, Lcom/peel/h/a/ch;->b:[Ljava/lang/String;

    invoke-direct {p0, p2, p3, p4}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 187
    if-eqz p2, :cond_0

    .line 188
    :goto_0
    const v0, 0x1020014

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 189
    iget-object v1, p0, Lcom/peel/h/a/ch;->b:[Ljava/lang/String;

    aget-object v1, v1, p1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 190
    return-object p2

    .line 187
    :cond_0
    iget-object v0, p0, Lcom/peel/h/a/ch;->a:Landroid/view/LayoutInflater;

    sget v1, Lcom/peel/ui/fq;->settings_single_selection_row_wospace:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    goto :goto_0
.end method
