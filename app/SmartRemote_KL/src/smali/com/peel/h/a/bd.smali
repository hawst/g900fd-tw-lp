.class Lcom/peel/h/a/bd;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:I

.field final synthetic b:Lcom/peel/h/a/bb;


# direct methods
.method constructor <init>(Lcom/peel/h/a/bb;I)V
    .locals 0

    .prologue
    .line 231
    iput-object p1, p0, Lcom/peel/h/a/bd;->b:Lcom/peel/h/a/bb;

    iput p2, p0, Lcom/peel/h/a/bd;->a:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 9

    .prologue
    const/high16 v4, 0x3f800000    # 1.0f

    const/16 v6, 0x3f

    const/4 v5, 0x0

    const/4 v1, 0x0

    const/4 v8, 0x1

    .line 235
    sget-object v0, Lcom/peel/content/a;->e:Ljava/lang/String;

    const-string/jumbo v2, "US"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 236
    iget v0, p0, Lcom/peel/h/a/bd;->a:I

    packed-switch v0, :pswitch_data_0

    .line 469
    :goto_0
    return-void

    .line 238
    :pswitch_0
    iget-object v0, p0, Lcom/peel/h/a/bd;->b:Lcom/peel/h/a/bb;

    invoke-static {v0}, Lcom/peel/h/a/bb;->b(Lcom/peel/h/a/bb;)Lcom/peel/widget/ag;

    move-result-object v0

    if-nez v0, :cond_0

    .line 239
    new-instance v2, Landroid/widget/RelativeLayout;

    iget-object v0, p0, Lcom/peel/h/a/bd;->b:Lcom/peel/h/a/bb;

    invoke-virtual {v0}, Lcom/peel/h/a/bb;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    invoke-direct {v2, v0}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 240
    iget-object v0, p0, Lcom/peel/h/a/bd;->b:Lcom/peel/h/a/bb;

    new-instance v3, Lcom/peel/widget/ag;

    iget-object v4, p0, Lcom/peel/h/a/bd;->b:Lcom/peel/h/a/bb;

    invoke-virtual {v4}, Lcom/peel/h/a/bb;->m()Landroid/support/v4/app/ae;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/peel/widget/ag;-><init>(Landroid/content/Context;)V

    invoke-static {v0, v3}, Lcom/peel/h/a/bb;->a(Lcom/peel/h/a/bb;Lcom/peel/widget/ag;)Lcom/peel/widget/ag;

    .line 241
    iget-object v0, p0, Lcom/peel/h/a/bd;->b:Lcom/peel/h/a/bb;

    invoke-static {v0}, Lcom/peel/h/a/bb;->b(Lcom/peel/h/a/bb;)Lcom/peel/widget/ag;

    move-result-object v0

    sget v3, Lcom/peel/ui/ft;->dialog_title_premium_channels:I

    invoke-virtual {v0, v3}, Lcom/peel/widget/ag;->a(I)Lcom/peel/widget/ag;

    .line 242
    iget-object v0, p0, Lcom/peel/h/a/bd;->b:Lcom/peel/h/a/bb;

    invoke-static {v0}, Lcom/peel/h/a/bb;->b(Lcom/peel/h/a/bb;)Lcom/peel/widget/ag;

    move-result-object v0

    iget-object v3, p0, Lcom/peel/h/a/bd;->b:Lcom/peel/h/a/bb;

    invoke-static {v3}, Lcom/peel/h/a/bb;->c(Lcom/peel/h/a/bb;)Landroid/content/DialogInterface$OnDismissListener;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/peel/widget/ag;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 243
    iget-object v0, p0, Lcom/peel/h/a/bd;->b:Lcom/peel/h/a/bb;

    invoke-static {v0}, Lcom/peel/h/a/bb;->b(Lcom/peel/h/a/bb;)Lcom/peel/widget/ag;

    move-result-object v0

    sget v3, Lcom/peel/ui/ft;->done:I

    new-instance v4, Lcom/peel/h/a/be;

    invoke-direct {v4, p0}, Lcom/peel/h/a/be;-><init>(Lcom/peel/h/a/bd;)V

    invoke-virtual {v0, v3, v4}, Lcom/peel/widget/ag;->c(ILandroid/content/DialogInterface$OnClickListener;)Lcom/peel/widget/ag;

    .line 249
    iget-object v0, p0, Lcom/peel/h/a/bd;->b:Lcom/peel/h/a/bb;

    invoke-static {v0}, Lcom/peel/h/a/bb;->d(Lcom/peel/h/a/bb;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v3, Lcom/peel/ui/fq;->settings_adddevice_activities:I

    invoke-virtual {v0, v3, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    .line 250
    new-instance v3, Landroid/widget/TextView;

    iget-object v4, p0, Lcom/peel/h/a/bd;->b:Lcom/peel/h/a/bb;

    invoke-virtual {v4}, Lcom/peel/h/a/bb;->m()Landroid/support/v4/app/ae;

    move-result-object v4

    invoke-direct {v3, v4}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 251
    const/16 v4, 0x11

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setGravity(I)V

    .line 252
    const/high16 v4, 0x41800000    # 16.0f

    invoke-virtual {v3, v8, v4}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 253
    sget v4, Lcom/peel/ui/ft;->loading:I

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    .line 254
    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 255
    const/4 v4, -0x1

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setTextColor(I)V

    .line 256
    const/16 v4, 0xa

    invoke-virtual {v3, v6, v6, v4, v6}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 257
    invoke-virtual {v2, v0}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 258
    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 259
    invoke-virtual {v0, v3}, Landroid/widget/ListView;->setEmptyView(Landroid/view/View;)V

    .line 260
    iget-object v4, p0, Lcom/peel/h/a/bd;->b:Lcom/peel/h/a/bb;

    invoke-virtual {v4}, Lcom/peel/h/a/bb;->m()Landroid/support/v4/app/ae;

    move-result-object v4

    invoke-static {v4}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v4

    const-string/jumbo v5, "country_ISO"

    const-string/jumbo v6, "US"

    invoke-interface {v4, v5, v6}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string/jumbo v5, "live"

    .line 261
    invoke-static {v5}, Lcom/peel/content/a;->c(Ljava/lang/String;)Lcom/peel/content/library/Library;

    move-result-object v5

    invoke-virtual {v5}, Lcom/peel/content/library/Library;->e()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/peel/h/a/bd;->b:Lcom/peel/h/a/bb;

    .line 262
    invoke-static {v6}, Lcom/peel/h/a/bb;->e(Lcom/peel/h/a/bb;)Lcom/peel/content/library/LiveLibrary;

    move-result-object v6

    invoke-virtual {v6}, Lcom/peel/content/library/LiveLibrary;->e()Ljava/lang/String;

    move-result-object v6

    new-instance v7, Lcom/peel/h/a/bf;

    invoke-direct {v7, p0, v3, v0}, Lcom/peel/h/a/bf;-><init>(Lcom/peel/h/a/bd;Landroid/widget/TextView;Landroid/widget/ListView;)V

    .line 260
    invoke-static {v4, v5, v6, v7}, Lcom/peel/content/a/j;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/peel/util/t;)V

    .line 283
    new-instance v3, Lcom/peel/ui/ay;

    iget-object v4, p0, Lcom/peel/h/a/bd;->b:Lcom/peel/h/a/bb;

    invoke-virtual {v4}, Lcom/peel/h/a/bb;->m()Landroid/support/v4/app/ae;

    move-result-object v4

    iget-object v5, p0, Lcom/peel/h/a/bd;->b:Lcom/peel/h/a/bb;

    invoke-static {v5}, Lcom/peel/h/a/bb;->f(Lcom/peel/h/a/bb;)Ljava/util/ArrayList;

    move-result-object v5

    iget-object v6, p0, Lcom/peel/h/a/bd;->b:Lcom/peel/h/a/bb;

    invoke-static {v6}, Lcom/peel/h/a/bb;->g(Lcom/peel/h/a/bb;)Ljava/util/ArrayList;

    move-result-object v6

    invoke-direct {v3, v4, v5, v6, v1}, Lcom/peel/ui/ay;-><init>(Landroid/content/Context;Ljava/util/ArrayList;Ljava/util/ArrayList;Z)V

    invoke-virtual {v0, v3}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 284
    new-instance v1, Lcom/peel/h/a/bh;

    invoke-direct {v1, p0, v0}, Lcom/peel/h/a/bh;-><init>(Lcom/peel/h/a/bd;Landroid/widget/ListView;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 304
    iget-object v0, p0, Lcom/peel/h/a/bd;->b:Lcom/peel/h/a/bb;

    invoke-static {v0}, Lcom/peel/h/a/bb;->b(Lcom/peel/h/a/bb;)Lcom/peel/widget/ag;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/peel/widget/ag;->a(Landroid/view/View;)Lcom/peel/widget/ag;

    .line 306
    :cond_0
    iget-object v0, p0, Lcom/peel/h/a/bd;->b:Lcom/peel/h/a/bb;

    invoke-static {v0}, Lcom/peel/h/a/bb;->b(Lcom/peel/h/a/bb;)Lcom/peel/widget/ag;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/widget/ag;->show()V

    goto/16 :goto_0

    .line 309
    :pswitch_1
    iget-object v0, p0, Lcom/peel/h/a/bd;->b:Lcom/peel/h/a/bb;

    invoke-static {v0}, Lcom/peel/h/a/bb;->i(Lcom/peel/h/a/bb;)Lcom/peel/widget/ag;

    move-result-object v0

    if-nez v0, :cond_1

    .line 310
    iget-object v0, p0, Lcom/peel/h/a/bd;->b:Lcom/peel/h/a/bb;

    new-instance v2, Lcom/peel/widget/ag;

    iget-object v3, p0, Lcom/peel/h/a/bd;->b:Lcom/peel/h/a/bb;

    invoke-virtual {v3}, Lcom/peel/h/a/bb;->m()Landroid/support/v4/app/ae;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/peel/widget/ag;-><init>(Landroid/content/Context;)V

    invoke-static {v0, v2}, Lcom/peel/h/a/bb;->b(Lcom/peel/h/a/bb;Lcom/peel/widget/ag;)Lcom/peel/widget/ag;

    .line 311
    iget-object v0, p0, Lcom/peel/h/a/bd;->b:Lcom/peel/h/a/bb;

    invoke-static {v0}, Lcom/peel/h/a/bb;->i(Lcom/peel/h/a/bb;)Lcom/peel/widget/ag;

    move-result-object v0

    sget v2, Lcom/peel/ui/ft;->dialog_title_hdsd:I

    invoke-virtual {v0, v2}, Lcom/peel/widget/ag;->a(I)Lcom/peel/widget/ag;

    .line 312
    iget-object v0, p0, Lcom/peel/h/a/bd;->b:Lcom/peel/h/a/bb;

    invoke-static {v0}, Lcom/peel/h/a/bb;->d(Lcom/peel/h/a/bb;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v2, Lcom/peel/ui/fq;->settings_adddevice_activities:I

    invoke-virtual {v0, v2, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    .line 313
    iget-object v2, p0, Lcom/peel/h/a/bd;->b:Lcom/peel/h/a/bb;

    invoke-virtual {v2}, Lcom/peel/h/a/bb;->n()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    invoke-static {v8, v4, v2}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v2

    float-to-int v2, v2

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setDividerHeight(I)V

    .line 314
    iget-object v2, p0, Lcom/peel/h/a/bd;->b:Lcom/peel/h/a/bb;

    invoke-static {v2}, Lcom/peel/h/a/bb;->i(Lcom/peel/h/a/bb;)Lcom/peel/widget/ag;

    move-result-object v2

    invoke-virtual {v2, v8}, Lcom/peel/widget/ag;->a(Z)Lcom/peel/widget/ag;

    .line 315
    iget-object v2, p0, Lcom/peel/h/a/bd;->b:Lcom/peel/h/a/bb;

    invoke-static {v2}, Lcom/peel/h/a/bb;->i(Lcom/peel/h/a/bb;)Lcom/peel/widget/ag;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/peel/widget/ag;->a(Landroid/view/View;)Lcom/peel/widget/ag;

    .line 316
    iget-object v2, p0, Lcom/peel/h/a/bd;->b:Lcom/peel/h/a/bb;

    invoke-static {v2}, Lcom/peel/h/a/bb;->i(Lcom/peel/h/a/bb;)Lcom/peel/widget/ag;

    move-result-object v2

    iget-object v3, p0, Lcom/peel/h/a/bd;->b:Lcom/peel/h/a/bb;

    invoke-static {v3}, Lcom/peel/h/a/bb;->c(Lcom/peel/h/a/bb;)Landroid/content/DialogInterface$OnDismissListener;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/peel/widget/ag;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 317
    iget-object v2, p0, Lcom/peel/h/a/bd;->b:Lcom/peel/h/a/bb;

    invoke-static {v2}, Lcom/peel/h/a/bb;->i(Lcom/peel/h/a/bb;)Lcom/peel/widget/ag;

    move-result-object v2

    sget v3, Lcom/peel/ui/ft;->done:I

    new-instance v4, Lcom/peel/h/a/bi;

    invoke-direct {v4, p0}, Lcom/peel/h/a/bi;-><init>(Lcom/peel/h/a/bd;)V

    invoke-virtual {v2, v3, v4}, Lcom/peel/widget/ag;->c(ILandroid/content/DialogInterface$OnClickListener;)Lcom/peel/widget/ag;

    .line 323
    new-instance v2, Lcom/peel/h/a/bj;

    invoke-direct {v2, p0, v0}, Lcom/peel/h/a/bj;-><init>(Lcom/peel/h/a/bd;Landroid/widget/ListView;)V

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 353
    new-instance v2, Lcom/peel/ui/ay;

    iget-object v3, p0, Lcom/peel/h/a/bd;->b:Lcom/peel/h/a/bb;

    invoke-virtual {v3}, Lcom/peel/h/a/bb;->m()Landroid/support/v4/app/ae;

    move-result-object v3

    iget-object v4, p0, Lcom/peel/h/a/bd;->b:Lcom/peel/h/a/bb;

    invoke-static {v4}, Lcom/peel/h/a/bb;->j(Lcom/peel/h/a/bb;)Ljava/util/ArrayList;

    move-result-object v4

    iget-object v5, p0, Lcom/peel/h/a/bd;->b:Lcom/peel/h/a/bb;

    invoke-static {v5}, Lcom/peel/h/a/bb;->k(Lcom/peel/h/a/bb;)Ljava/util/ArrayList;

    move-result-object v5

    invoke-direct {v2, v3, v4, v5, v1}, Lcom/peel/ui/ay;-><init>(Landroid/content/Context;Ljava/util/ArrayList;Ljava/util/ArrayList;Z)V

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 356
    :cond_1
    iget-object v0, p0, Lcom/peel/h/a/bd;->b:Lcom/peel/h/a/bb;

    invoke-static {v0}, Lcom/peel/h/a/bb;->i(Lcom/peel/h/a/bb;)Lcom/peel/widget/ag;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/widget/ag;->show()V

    goto/16 :goto_0

    .line 360
    :pswitch_2
    iget-object v0, p0, Lcom/peel/h/a/bd;->b:Lcom/peel/h/a/bb;

    invoke-static {v0}, Lcom/peel/h/a/bb;->m(Lcom/peel/h/a/bb;)Lcom/peel/widget/ag;

    move-result-object v0

    if-nez v0, :cond_4

    .line 361
    iget-object v0, p0, Lcom/peel/h/a/bd;->b:Lcom/peel/h/a/bb;

    new-instance v2, Lcom/peel/widget/ag;

    iget-object v3, p0, Lcom/peel/h/a/bd;->b:Lcom/peel/h/a/bb;

    invoke-virtual {v3}, Lcom/peel/h/a/bb;->m()Landroid/support/v4/app/ae;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/peel/widget/ag;-><init>(Landroid/content/Context;)V

    invoke-static {v0, v2}, Lcom/peel/h/a/bb;->c(Lcom/peel/h/a/bb;Lcom/peel/widget/ag;)Lcom/peel/widget/ag;

    .line 362
    iget-object v0, p0, Lcom/peel/h/a/bd;->b:Lcom/peel/h/a/bb;

    invoke-static {v0}, Lcom/peel/h/a/bb;->m(Lcom/peel/h/a/bb;)Lcom/peel/widget/ag;

    move-result-object v0

    sget v2, Lcom/peel/ui/ft;->dialog_title_language:I

    invoke-virtual {v0, v2}, Lcom/peel/widget/ag;->a(I)Lcom/peel/widget/ag;

    .line 363
    iget-object v0, p0, Lcom/peel/h/a/bd;->b:Lcom/peel/h/a/bb;

    invoke-static {v0}, Lcom/peel/h/a/bb;->m(Lcom/peel/h/a/bb;)Lcom/peel/widget/ag;

    move-result-object v0

    invoke-virtual {v0, v8}, Lcom/peel/widget/ag;->a(Z)Lcom/peel/widget/ag;

    .line 364
    iget-object v0, p0, Lcom/peel/h/a/bd;->b:Lcom/peel/h/a/bb;

    invoke-static {v0}, Lcom/peel/h/a/bb;->m(Lcom/peel/h/a/bb;)Lcom/peel/widget/ag;

    move-result-object v0

    iget-object v2, p0, Lcom/peel/h/a/bd;->b:Lcom/peel/h/a/bb;

    invoke-static {v2}, Lcom/peel/h/a/bb;->c(Lcom/peel/h/a/bb;)Landroid/content/DialogInterface$OnDismissListener;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/peel/widget/ag;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 365
    iget-object v0, p0, Lcom/peel/h/a/bd;->b:Lcom/peel/h/a/bb;

    invoke-static {v0}, Lcom/peel/h/a/bb;->m(Lcom/peel/h/a/bb;)Lcom/peel/widget/ag;

    move-result-object v0

    sget v2, Lcom/peel/ui/ft;->done:I

    new-instance v3, Lcom/peel/h/a/bk;

    invoke-direct {v3, p0}, Lcom/peel/h/a/bk;-><init>(Lcom/peel/h/a/bd;)V

    invoke-virtual {v0, v2, v3}, Lcom/peel/widget/ag;->c(ILandroid/content/DialogInterface$OnClickListener;)Lcom/peel/widget/ag;

    .line 371
    iget-object v0, p0, Lcom/peel/h/a/bd;->b:Lcom/peel/h/a/bb;

    invoke-static {v0}, Lcom/peel/h/a/bb;->d(Lcom/peel/h/a/bb;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v2, Lcom/peel/ui/fq;->settings_add_foreign_language_channel:I

    invoke-virtual {v0, v2, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    .line 373
    iget-object v2, p0, Lcom/peel/h/a/bd;->b:Lcom/peel/h/a/bb;

    invoke-static {v2}, Lcom/peel/h/a/bb;->e(Lcom/peel/h/a/bb;)Lcom/peel/content/library/LiveLibrary;

    move-result-object v2

    invoke-virtual {v2}, Lcom/peel/content/library/LiveLibrary;->i()[Lcom/peel/data/Channel;

    move-result-object v2

    .line 374
    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_3

    aget-object v4, v2, v1

    .line 375
    invoke-virtual {v4}, Lcom/peel/data/Channel;->g()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_2

    invoke-virtual {v4}, Lcom/peel/data/Channel;->g()Ljava/lang/String;

    move-result-object v5

    const-string/jumbo v6, "0"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_2

    iget-object v5, p0, Lcom/peel/h/a/bd;->b:Lcom/peel/h/a/bb;

    invoke-static {v5}, Lcom/peel/h/a/bb;->n(Lcom/peel/h/a/bb;)Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v4}, Lcom/peel/data/Channel;->g()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 376
    invoke-virtual {v4}, Lcom/peel/data/Channel;->g()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_2

    invoke-virtual {v4}, Lcom/peel/data/Channel;->g()Ljava/lang/String;

    move-result-object v5

    const-string/jumbo v6, "und"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 377
    iget-object v5, p0, Lcom/peel/h/a/bd;->b:Lcom/peel/h/a/bb;

    invoke-static {v5}, Lcom/peel/h/a/bb;->n(Lcom/peel/h/a/bb;)Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v4}, Lcom/peel/data/Channel;->g()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 379
    invoke-static {}, Lcom/peel/h/a/bb;->c()Ljava/lang/String;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "zzz, channel.getLanguage()="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v4}, Lcom/peel/data/Channel;->g()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v6, ", availableLanguages.size()="

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v6, p0, Lcom/peel/h/a/bd;->b:Lcom/peel/h/a/bb;

    invoke-static {v6}, Lcom/peel/h/a/bb;->n(Lcom/peel/h/a/bb;)Ljava/util/ArrayList;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v5, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 374
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_1

    .line 385
    :cond_3
    new-instance v1, Lcom/peel/ui/ay;

    iget-object v2, p0, Lcom/peel/h/a/bd;->b:Lcom/peel/h/a/bb;

    invoke-virtual {v2}, Lcom/peel/h/a/bb;->m()Landroid/support/v4/app/ae;

    move-result-object v2

    iget-object v3, p0, Lcom/peel/h/a/bd;->b:Lcom/peel/h/a/bb;

    invoke-static {v3}, Lcom/peel/h/a/bb;->n(Lcom/peel/h/a/bb;)Ljava/util/ArrayList;

    move-result-object v3

    iget-object v4, p0, Lcom/peel/h/a/bd;->b:Lcom/peel/h/a/bb;

    invoke-static {v4}, Lcom/peel/h/a/bb;->o(Lcom/peel/h/a/bb;)Ljava/util/ArrayList;

    move-result-object v4

    invoke-direct {v1, v2, v3, v4, v8}, Lcom/peel/ui/ay;-><init>(Landroid/content/Context;Ljava/util/ArrayList;Ljava/util/ArrayList;Z)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 386
    new-instance v1, Lcom/peel/h/a/bl;

    invoke-direct {v1, p0, v0}, Lcom/peel/h/a/bl;-><init>(Lcom/peel/h/a/bd;Landroid/widget/ListView;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 413
    iget-object v1, p0, Lcom/peel/h/a/bd;->b:Lcom/peel/h/a/bb;

    invoke-static {v1}, Lcom/peel/h/a/bb;->m(Lcom/peel/h/a/bb;)Lcom/peel/widget/ag;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/peel/widget/ag;->a(Landroid/view/View;)Lcom/peel/widget/ag;

    .line 415
    :cond_4
    iget-object v0, p0, Lcom/peel/h/a/bd;->b:Lcom/peel/h/a/bb;

    invoke-static {v0}, Lcom/peel/h/a/bb;->m(Lcom/peel/h/a/bb;)Lcom/peel/widget/ag;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/widget/ag;->show()V

    goto/16 :goto_0

    .line 419
    :cond_5
    iget-object v0, p0, Lcom/peel/h/a/bd;->b:Lcom/peel/h/a/bb;

    invoke-static {v0}, Lcom/peel/h/a/bb;->i(Lcom/peel/h/a/bb;)Lcom/peel/widget/ag;

    move-result-object v0

    if-nez v0, :cond_6

    .line 420
    iget-object v0, p0, Lcom/peel/h/a/bd;->b:Lcom/peel/h/a/bb;

    new-instance v2, Lcom/peel/widget/ag;

    iget-object v3, p0, Lcom/peel/h/a/bd;->b:Lcom/peel/h/a/bb;

    invoke-virtual {v3}, Lcom/peel/h/a/bb;->m()Landroid/support/v4/app/ae;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/peel/widget/ag;-><init>(Landroid/content/Context;)V

    invoke-static {v0, v2}, Lcom/peel/h/a/bb;->b(Lcom/peel/h/a/bb;Lcom/peel/widget/ag;)Lcom/peel/widget/ag;

    .line 421
    iget-object v0, p0, Lcom/peel/h/a/bd;->b:Lcom/peel/h/a/bb;

    invoke-static {v0}, Lcom/peel/h/a/bb;->i(Lcom/peel/h/a/bb;)Lcom/peel/widget/ag;

    move-result-object v0

    sget v2, Lcom/peel/ui/ft;->dialog_title_hdsd:I

    invoke-virtual {v0, v2}, Lcom/peel/widget/ag;->a(I)Lcom/peel/widget/ag;

    .line 422
    iget-object v0, p0, Lcom/peel/h/a/bd;->b:Lcom/peel/h/a/bb;

    invoke-static {v0}, Lcom/peel/h/a/bb;->d(Lcom/peel/h/a/bb;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v2, Lcom/peel/ui/fq;->settings_adddevice_activities:I

    invoke-virtual {v0, v2, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    .line 423
    iget-object v2, p0, Lcom/peel/h/a/bd;->b:Lcom/peel/h/a/bb;

    invoke-virtual {v2}, Lcom/peel/h/a/bb;->n()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    invoke-static {v8, v4, v2}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v2

    float-to-int v2, v2

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setDividerHeight(I)V

    .line 424
    iget-object v2, p0, Lcom/peel/h/a/bd;->b:Lcom/peel/h/a/bb;

    invoke-static {v2}, Lcom/peel/h/a/bb;->i(Lcom/peel/h/a/bb;)Lcom/peel/widget/ag;

    move-result-object v2

    invoke-virtual {v2, v8}, Lcom/peel/widget/ag;->a(Z)Lcom/peel/widget/ag;

    .line 425
    iget-object v2, p0, Lcom/peel/h/a/bd;->b:Lcom/peel/h/a/bb;

    invoke-static {v2}, Lcom/peel/h/a/bb;->i(Lcom/peel/h/a/bb;)Lcom/peel/widget/ag;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/peel/widget/ag;->a(Landroid/view/View;)Lcom/peel/widget/ag;

    .line 426
    iget-object v2, p0, Lcom/peel/h/a/bd;->b:Lcom/peel/h/a/bb;

    invoke-static {v2}, Lcom/peel/h/a/bb;->i(Lcom/peel/h/a/bb;)Lcom/peel/widget/ag;

    move-result-object v2

    iget-object v3, p0, Lcom/peel/h/a/bd;->b:Lcom/peel/h/a/bb;

    invoke-static {v3}, Lcom/peel/h/a/bb;->c(Lcom/peel/h/a/bb;)Landroid/content/DialogInterface$OnDismissListener;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/peel/widget/ag;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 427
    iget-object v2, p0, Lcom/peel/h/a/bd;->b:Lcom/peel/h/a/bb;

    invoke-static {v2}, Lcom/peel/h/a/bb;->i(Lcom/peel/h/a/bb;)Lcom/peel/widget/ag;

    move-result-object v2

    sget v3, Lcom/peel/ui/ft;->done:I

    new-instance v4, Lcom/peel/h/a/bm;

    invoke-direct {v4, p0}, Lcom/peel/h/a/bm;-><init>(Lcom/peel/h/a/bd;)V

    invoke-virtual {v2, v3, v4}, Lcom/peel/widget/ag;->c(ILandroid/content/DialogInterface$OnClickListener;)Lcom/peel/widget/ag;

    .line 434
    new-instance v2, Lcom/peel/h/a/bn;

    invoke-direct {v2, p0, v0}, Lcom/peel/h/a/bn;-><init>(Lcom/peel/h/a/bd;Landroid/widget/ListView;)V

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 464
    new-instance v2, Lcom/peel/ui/ay;

    iget-object v3, p0, Lcom/peel/h/a/bd;->b:Lcom/peel/h/a/bb;

    invoke-virtual {v3}, Lcom/peel/h/a/bb;->m()Landroid/support/v4/app/ae;

    move-result-object v3

    iget-object v4, p0, Lcom/peel/h/a/bd;->b:Lcom/peel/h/a/bb;

    invoke-static {v4}, Lcom/peel/h/a/bb;->j(Lcom/peel/h/a/bb;)Ljava/util/ArrayList;

    move-result-object v4

    iget-object v5, p0, Lcom/peel/h/a/bd;->b:Lcom/peel/h/a/bb;

    invoke-static {v5}, Lcom/peel/h/a/bb;->k(Lcom/peel/h/a/bb;)Ljava/util/ArrayList;

    move-result-object v5

    invoke-direct {v2, v3, v4, v5, v1}, Lcom/peel/ui/ay;-><init>(Landroid/content/Context;Ljava/util/ArrayList;Ljava/util/ArrayList;Z)V

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 467
    :cond_6
    iget-object v0, p0, Lcom/peel/h/a/bd;->b:Lcom/peel/h/a/bb;

    invoke-static {v0}, Lcom/peel/h/a/bb;->i(Lcom/peel/h/a/bb;)Lcom/peel/widget/ag;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/widget/ag;->show()V

    goto/16 :goto_0

    .line 236
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
