.class Lcom/peel/h/a/c;
.super Lcom/peel/util/t;


# instance fields
.field final synthetic a:Lcom/peel/control/RoomControl;

.field final synthetic b:Lcom/peel/h/a/a;


# direct methods
.method constructor <init>(Lcom/peel/h/a/a;Lcom/peel/control/RoomControl;)V
    .locals 0

    .prologue
    .line 486
    iput-object p1, p0, Lcom/peel/h/a/c;->b:Lcom/peel/h/a/a;

    iput-object p2, p0, Lcom/peel/h/a/c;->a:Lcom/peel/control/RoomControl;

    invoke-direct {p0}, Lcom/peel/util/t;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 490
    iget-boolean v0, p0, Lcom/peel/h/a/c;->i:Z

    if-eqz v0, :cond_0

    .line 492
    iget-object v0, p0, Lcom/peel/h/a/c;->j:Ljava/lang/Object;

    check-cast v0, Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/f/c;

    invoke-virtual {v0}, Lcom/peel/f/c;->a()I

    move-result v0

    .line 493
    const-string/jumbo v1, "https://partners-ir.peel.com"

    new-instance v2, Lcom/peel/h/a/d;

    invoke-direct {v2, p0, v0}, Lcom/peel/h/a/d;-><init>(Lcom/peel/h/a/c;I)V

    invoke-static {v1, v0, v2}, Lcom/peel/control/aa;->a(Ljava/lang/String;ILcom/peel/util/t;)V

    .line 551
    :goto_0
    return-void

    .line 546
    :cond_0
    iget-object v0, p0, Lcom/peel/h/a/c;->b:Lcom/peel/h/a/a;

    invoke-virtual {v0}, Lcom/peel/h/a/a;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/support/v4/app/ae;->setProgressBarIndeterminateVisibility(Z)V

    .line 547
    invoke-static {}, Lcom/peel/h/a/a;->c()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "getCodesets: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/peel/h/a/c;->b:Lcom/peel/h/a/a;

    invoke-static {v2}, Lcom/peel/h/a/a;->n(Lcom/peel/h/a/a;)Lcom/peel/f/a;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " type: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/peel/h/a/c;->b:Lcom/peel/h/a/a;

    invoke-static {v2}, Lcom/peel/h/a/a;->d(Lcom/peel/h/a/a;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " failed!\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/peel/h/a/c;->k:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/peel/h/a/c;->j:Ljava/lang/Object;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
