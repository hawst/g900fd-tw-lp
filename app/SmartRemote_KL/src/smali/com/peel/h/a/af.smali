.class final Lcom/peel/h/a/af;
.super Landroid/text/style/ClickableSpan;


# instance fields
.field final synthetic a:Lcom/peel/h/a/a;


# direct methods
.method private constructor <init>(Lcom/peel/h/a/a;)V
    .locals 0

    .prologue
    .line 1472
    iput-object p1, p0, Lcom/peel/h/a/af;->a:Lcom/peel/h/a/a;

    invoke-direct {p0}, Landroid/text/style/ClickableSpan;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/peel/h/a/a;Lcom/peel/h/a/b;)V
    .locals 0

    .prologue
    .line 1472
    invoke-direct {p0, p1}, Lcom/peel/h/a/af;-><init>(Lcom/peel/h/a/a;)V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 1475
    iget-object v0, p0, Lcom/peel/h/a/af;->a:Lcom/peel/h/a/a;

    invoke-virtual {v0}, Lcom/peel/h/a/a;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/h/a/af;->a:Lcom/peel/h/a/a;

    iget-object v1, v1, Lcom/peel/h/a/a;->b:Lcom/peel/d/i;

    iget-object v2, p0, Lcom/peel/h/a/af;->a:Lcom/peel/h/a/a;

    invoke-static {v2}, Lcom/peel/h/a/a;->n(Lcom/peel/h/a/a;)Lcom/peel/f/a;

    move-result-object v2

    invoke-virtual {v2}, Lcom/peel/f/a;->b()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/peel/h/a/af;->a:Lcom/peel/h/a/a;

    invoke-static {v3}, Lcom/peel/h/a/a;->l(Lcom/peel/h/a/a;)Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v4, "Power"

    iget-object v5, p0, Lcom/peel/h/a/af;->a:Lcom/peel/h/a/a;

    iget-object v5, v5, Lcom/peel/h/a/a;->c:Landroid/os/Bundle;

    const-class v6, Lcom/peel/h/a/a;

    invoke-virtual {v6}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-static/range {v0 .. v6}, Lcom/peel/util/bx;->a(Landroid/content/Context;Lcom/peel/d/i;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Ljava/lang/String;)V

    .line 1476
    iget-object v0, p0, Lcom/peel/h/a/af;->a:Lcom/peel/h/a/a;

    invoke-static {v0}, Lcom/peel/h/a/a;->u(Lcom/peel/h/a/a;)Lcom/peel/widget/TestBtnViewPager;

    move-result-object v0

    invoke-virtual {v0, v7}, Lcom/peel/widget/TestBtnViewPager;->setEnabledSwipe(Z)V

    .line 1477
    iget-object v0, p0, Lcom/peel/h/a/af;->a:Lcom/peel/h/a/a;

    invoke-static {v0}, Lcom/peel/h/a/a;->D(Lcom/peel/h/a/a;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v7}, Landroid/widget/Button;->setEnabled(Z)V

    .line 1478
    iget-object v0, p0, Lcom/peel/h/a/af;->a:Lcom/peel/h/a/a;

    invoke-static {v0}, Lcom/peel/h/a/a;->E(Lcom/peel/h/a/a;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v7}, Landroid/widget/Button;->setEnabled(Z)V

    .line 1479
    return-void
.end method

.method public updateDrawState(Landroid/text/TextPaint;)V
    .locals 1

    .prologue
    .line 1483
    const-string/jumbo v0, "#21dcda"

    invoke-static {v0}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/text/TextPaint;->setColor(I)V

    .line 1484
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/text/TextPaint;->setUnderlineText(Z)V

    .line 1485
    return-void
.end method
