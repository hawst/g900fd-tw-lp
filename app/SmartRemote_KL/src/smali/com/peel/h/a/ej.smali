.class Lcom/peel/h/a/ej;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Landroid/view/LayoutInflater;

.field final synthetic b:Landroid/widget/LinearLayout;

.field final synthetic c:Lcom/peel/h/a/ei;


# direct methods
.method constructor <init>(Lcom/peel/h/a/ei;Landroid/view/LayoutInflater;Landroid/widget/LinearLayout;)V
    .locals 0

    .prologue
    .line 133
    iput-object p1, p0, Lcom/peel/h/a/ej;->c:Lcom/peel/h/a/ei;

    iput-object p2, p0, Lcom/peel/h/a/ej;->a:Landroid/view/LayoutInflater;

    iput-object p3, p0, Lcom/peel/h/a/ej;->b:Landroid/widget/LinearLayout;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    .line 136
    new-instance v1, Lcom/peel/widget/ag;

    iget-object v0, p0, Lcom/peel/h/a/ej;->c:Lcom/peel/h/a/ei;

    invoke-virtual {v0}, Lcom/peel/h/a/ei;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/peel/widget/ag;-><init>(Landroid/content/Context;)V

    .line 137
    iget-object v0, p0, Lcom/peel/h/a/ej;->a:Landroid/view/LayoutInflater;

    sget v2, Lcom/peel/ui/fq;->new_code_view:I

    iget-object v3, p0, Lcom/peel/h/a/ej;->b:Landroid/widget/LinearLayout;

    const/4 v4, 0x0

    invoke-virtual {v0, v2, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 138
    sget v0, Lcom/peel/ui/fp;->edittext:I

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    .line 139
    new-instance v3, Lcom/peel/h/a/ek;

    invoke-direct {v3, p0, v1, v0}, Lcom/peel/h/a/ek;-><init>(Lcom/peel/h/a/ej;Lcom/peel/widget/ag;Landroid/widget/EditText;)V

    invoke-virtual {v0, v3}, Landroid/widget/EditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 156
    invoke-virtual {v1, v2}, Lcom/peel/widget/ag;->a(Landroid/view/View;)Lcom/peel/widget/ag;

    .line 157
    sget v2, Lcom/peel/ui/ft;->new_code:I

    invoke-virtual {v1, v2}, Lcom/peel/widget/ag;->a(I)Lcom/peel/widget/ag;

    .line 158
    sget v2, Lcom/peel/ui/ft;->cancel:I

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Lcom/peel/widget/ag;->c(ILandroid/content/DialogInterface$OnClickListener;)Lcom/peel/widget/ag;

    .line 159
    sget v2, Lcom/peel/ui/ft;->done:I

    new-instance v3, Lcom/peel/h/a/el;

    invoke-direct {v3, p0, v0}, Lcom/peel/h/a/el;-><init>(Lcom/peel/h/a/ej;Landroid/widget/EditText;)V

    invoke-virtual {v1, v2, v3}, Lcom/peel/widget/ag;->a(ILandroid/content/DialogInterface$OnClickListener;)Lcom/peel/widget/ag;

    .line 171
    invoke-virtual {v1}, Lcom/peel/widget/ag;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/4 v2, 0x5

    invoke-virtual {v0, v2}, Landroid/view/Window;->setSoftInputMode(I)V

    .line 172
    invoke-virtual {v1}, Lcom/peel/widget/ag;->d()Lcom/peel/widget/ag;

    .line 174
    return-void
.end method
