.class public Lcom/peel/h/a/dl;
.super Landroid/widget/BaseAdapter;


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private b:Landroid/view/LayoutInflater;

.field private c:I

.field private d:Z

.field private e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private f:Landroid/content/Context;

.field private g:Landroid/content/res/Resources;

.field private h:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private i:I

.field private j:I

.field private k:[Ljava/lang/String;

.field private l:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/peel/content/listing/Listing;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 37
    const-class v0, Lcom/peel/h/a/dl;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/peel/h/a/dl;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/util/Map;ILjava/util/Map;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;I",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/peel/content/listing/Listing;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 53
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 54
    iput-object p1, p0, Lcom/peel/h/a/dl;->f:Landroid/content/Context;

    .line 55
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/h/a/dl;->b:Landroid/view/LayoutInflater;

    .line 56
    iput p3, p0, Lcom/peel/h/a/dl;->c:I

    .line 57
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/h/a/dl;->g:Landroid/content/res/Resources;

    .line 58
    iput-object p4, p0, Lcom/peel/h/a/dl;->l:Ljava/util/Map;

    .line 59
    iput-boolean v1, p0, Lcom/peel/h/a/dl;->d:Z

    .line 60
    iput-object p2, p0, Lcom/peel/h/a/dl;->h:Ljava/util/Map;

    .line 61
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/peel/h/a/dl;->e:Ljava/util/List;

    move v0, v1

    .line 62
    :goto_0
    invoke-interface {p2}, Ljava/util/Map;->size()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 63
    iget-object v2, p0, Lcom/peel/h/a/dl;->e:Ljava/util/List;

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 62
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 66
    :cond_0
    invoke-interface {p2}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {p2}, Ljava/util/Map;->size()I

    move-result v1

    new-array v1, v1, [Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    iput-object v0, p0, Lcom/peel/h/a/dl;->k:[Ljava/lang/String;

    .line 67
    iget-object v0, p0, Lcom/peel/h/a/dl;->g:Landroid/content/res/Resources;

    sget v1, Lcom/peel/ui/fn;->settings_tile_width:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/peel/h/a/dl;->i:I

    .line 68
    iget-object v0, p0, Lcom/peel/h/a/dl;->g:Landroid/content/res/Resources;

    sget v1, Lcom/peel/ui/fn;->settings_tile_height:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/peel/h/a/dl;->j:I

    .line 69
    return-void
.end method

.method static synthetic a(Lcom/peel/h/a/dl;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/peel/h/a/dl;->f:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic b(Lcom/peel/h/a/dl;)I
    .locals 1

    .prologue
    .line 36
    iget v0, p0, Lcom/peel/h/a/dl;->i:I

    return v0
.end method

.method static synthetic c(Lcom/peel/h/a/dl;)I
    .locals 1

    .prologue
    .line 36
    iget v0, p0, Lcom/peel/h/a/dl;->j:I

    return v0
.end method


# virtual methods
.method public a()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .prologue
    .line 179
    iget-object v0, p0, Lcom/peel/h/a/dl;->e:Ljava/util/List;

    return-object v0
.end method

.method public a(I)V
    .locals 2

    .prologue
    .line 173
    iget-object v1, p0, Lcom/peel/h/a/dl;->e:Ljava/util/List;

    iget-object v0, p0, Lcom/peel/h/a/dl;->e:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-interface {v1, p1, v0}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 174
    invoke-virtual {p0}, Lcom/peel/h/a/dl;->notifyDataSetChanged()V

    .line 175
    return-void

    .line 173
    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public a(Z)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 163
    iput-boolean p1, p0, Lcom/peel/h/a/dl;->d:Z

    .line 164
    if-eqz p1, :cond_0

    move v0, v1

    .line 165
    :goto_0
    iget-object v2, p0, Lcom/peel/h/a/dl;->e:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 166
    iget-object v2, p0, Lcom/peel/h/a/dl;->e:Ljava/util/List;

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-interface {v2, v0, v3}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 165
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 169
    :cond_0
    invoke-virtual {p0}, Lcom/peel/h/a/dl;->notifyDataSetChanged()V

    .line 170
    return-void
.end method

.method public b()V
    .locals 7

    .prologue
    .line 183
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 185
    iget-object v0, p0, Lcom/peel/h/a/dl;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_1

    .line 186
    iget-object v0, p0, Lcom/peel/h/a/dl;->e:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 187
    invoke-virtual {p0, v1}, Lcom/peel/h/a/dl;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 189
    iget-object v0, p0, Lcom/peel/h/a/dl;->h:Ljava/util/Map;

    invoke-virtual {p0, v1}, Lcom/peel/h/a/dl;->getItem(I)Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 190
    iget-object v0, p0, Lcom/peel/h/a/dl;->e:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 191
    iget-object v0, p0, Lcom/peel/h/a/dl;->h:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    iget-object v3, p0, Lcom/peel/h/a/dl;->h:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->size()I

    move-result v3

    new-array v3, v3, [Ljava/lang/String;

    invoke-interface {v0, v3}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    iput-object v0, p0, Lcom/peel/h/a/dl;->k:[Ljava/lang/String;

    .line 185
    :cond_0
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    .line 195
    :cond_1
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_4

    .line 196
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 197
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 198
    const/4 v0, 0x0

    .line 200
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move v1, v0

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 201
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 203
    add-int/lit8 v0, v1, 0x1

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v6

    add-int/lit8 v6, v6, -0x1

    if-ge v1, v6, :cond_2

    .line 204
    const-string/jumbo v1, ","

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_2
    move v1, v0

    .line 206
    goto :goto_1

    .line 208
    :cond_3
    const-string/jumbo v0, "show"

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 209
    iget v0, p0, Lcom/peel/h/a/dl;->c:I

    if-nez v0, :cond_5

    .line 210
    const-string/jumbo v0, "path"

    const-string/jumbo v1, "show/unfav"

    invoke-virtual {v3, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 215
    :goto_2
    sget-object v0, Lcom/peel/content/a;->f:Lcom/peel/content/user/User;

    const/4 v1, 0x0

    invoke-virtual {v0, v3, v1}, Lcom/peel/content/user/User;->a(Landroid/os/Bundle;Lcom/peel/util/t;)V

    .line 218
    :cond_4
    invoke-virtual {p0}, Lcom/peel/h/a/dl;->notifyDataSetChanged()V

    .line 219
    return-void

    .line 212
    :cond_5
    const-string/jumbo v0, "path"

    const-string/jumbo v1, "show/uncut"

    invoke-virtual {v3, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 223
    iget-object v0, p0, Lcom/peel/h/a/dl;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 228
    iget-object v0, p0, Lcom/peel/h/a/dl;->k:[Ljava/lang/String;

    aget-object v0, v0, p1

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2

    .prologue
    .line 233
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x0

    .line 74
    if-nez p2, :cond_1

    .line 75
    new-instance v1, Lcom/peel/h/a/dn;

    invoke-direct {v1, v6}, Lcom/peel/h/a/dn;-><init>(Lcom/peel/h/a/dm;)V

    .line 76
    iget-object v0, p0, Lcom/peel/h/a/dl;->b:Landroid/view/LayoutInflater;

    sget v2, Lcom/peel/ui/fq;->fav_cut_row:I

    invoke-virtual {v0, v2, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 77
    sget v0, Lcom/peel/ui/fp;->text:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lcom/peel/h/a/dn;->a:Landroid/widget/TextView;

    .line 78
    sget v0, Lcom/peel/ui/fp;->checkbox:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, v1, Lcom/peel/h/a/dn;->b:Landroid/widget/ImageView;

    .line 80
    invoke-virtual {p2, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    move-object v2, v1

    .line 84
    :goto_0
    invoke-virtual {p0, p1}, Lcom/peel/h/a/dl;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 85
    iget-object v3, v2, Lcom/peel/h/a/dn;->a:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/peel/h/a/dl;->h:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v3, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 86
    iget-object v1, p0, Lcom/peel/h/a/dl;->l:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/content/listing/LiveListing;

    .line 88
    if-nez v0, :cond_2

    .line 89
    sget-object v0, Lcom/peel/h/a/dl;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "null node at position: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 90
    iget-object v0, p0, Lcom/peel/h/a/dl;->g:Landroid/content/res/Resources;

    sget v1, Lcom/peel/ui/fo;->genre_placeholder:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 91
    iget v1, p0, Lcom/peel/h/a/dl;->i:I

    iget v3, p0, Lcom/peel/h/a/dl;->j:I

    invoke-virtual {v0, v7, v7, v1, v3}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 92
    iget-object v1, v2, Lcom/peel/h/a/dn;->a:Landroid/widget/TextView;

    invoke-virtual {v1, v0, v6, v6, v6}, Landroid/widget/TextView;->setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 136
    :cond_0
    :goto_1
    iget-object v1, v2, Lcom/peel/h/a/dn;->b:Landroid/widget/ImageView;

    .line 137
    iget-boolean v0, p0, Lcom/peel/h/a/dl;->d:Z

    if-eqz v0, :cond_6

    .line 138
    invoke-virtual {v1, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 139
    iget-object v0, p0, Lcom/peel/h/a/dl;->e:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 140
    sget v0, Lcom/peel/ui/fo;->btn_checkbox_on_states:I

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 148
    :goto_2
    return-object p2

    .line 82
    :cond_1
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/h/a/dn;

    move-object v2, v0

    goto :goto_0

    .line 94
    :cond_2
    const-string/jumbo v1, "movie"

    .line 95
    invoke-virtual {v0}, Lcom/peel/content/listing/LiveListing;->k()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    const/16 v1, 0xa9

    .line 94
    :goto_3
    invoke-static {v0, v1}, Lcom/peel/util/bx;->a(Lcom/peel/content/listing/Listing;I)I

    move-result v3

    .line 97
    iget-object v1, p0, Lcom/peel/h/a/dl;->g:Landroid/content/res/Resources;

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 98
    iget v4, p0, Lcom/peel/h/a/dl;->i:I

    iget v5, p0, Lcom/peel/h/a/dl;->j:I

    invoke-virtual {v1, v7, v7, v4, v5}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 99
    iget-object v4, v2, Lcom/peel/h/a/dn;->a:Landroid/widget/TextView;

    invoke-virtual {v4, v1, v6, v6, v6}, Landroid/widget/TextView;->setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 101
    iget-object v1, v2, Lcom/peel/h/a/dn;->c:Lcom/squareup/picasso/Target;

    if-nez v1, :cond_4

    .line 102
    new-instance v1, Lcom/peel/h/a/dm;

    invoke-direct {v1, p0, v2}, Lcom/peel/h/a/dm;-><init>(Lcom/peel/h/a/dl;Lcom/peel/h/a/dn;)V

    iput-object v1, v2, Lcom/peel/h/a/dn;->c:Lcom/squareup/picasso/Target;

    .line 122
    :goto_4
    invoke-virtual {v0}, Lcom/peel/content/listing/LiveListing;->y()Ljava/lang/String;

    move-result-object v1

    .line 124
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 125
    const/4 v1, 0x3

    const/4 v4, 0x4

    const/16 v5, 0x10e

    invoke-virtual {v0}, Lcom/peel/content/listing/LiveListing;->B()Ljava/util/Map;

    move-result-object v0

    invoke-static {v1, v4, v5, v0}, Lcom/peel/util/bx;->a(IIILjava/util/Map;)Ljava/lang/String;

    move-result-object v0

    .line 128
    :goto_5
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 129
    iget-object v1, p0, Lcom/peel/h/a/dl;->f:Landroid/content/Context;

    invoke-static {v1}, Lcom/peel/util/b/e;->a(Landroid/content/Context;)Lcom/squareup/picasso/Picasso;

    move-result-object v1

    .line 130
    invoke-virtual {v1, v0}, Lcom/squareup/picasso/Picasso;->load(Ljava/lang/String;)Lcom/squareup/picasso/RequestCreator;

    move-result-object v0

    .line 131
    invoke-virtual {v0, v3}, Lcom/squareup/picasso/RequestCreator;->placeholder(I)Lcom/squareup/picasso/RequestCreator;

    move-result-object v0

    iget-object v1, v2, Lcom/peel/h/a/dn;->c:Lcom/squareup/picasso/Target;

    .line 132
    invoke-virtual {v0, v1}, Lcom/squareup/picasso/RequestCreator;->into(Lcom/squareup/picasso/Target;)V

    goto/16 :goto_1

    .line 95
    :cond_3
    const/16 v1, 0x2b

    goto :goto_3

    .line 119
    :cond_4
    iget-object v1, p0, Lcom/peel/h/a/dl;->f:Landroid/content/Context;

    invoke-static {v1}, Lcom/peel/util/b/e;->a(Landroid/content/Context;)Lcom/squareup/picasso/Picasso;

    move-result-object v1

    iget-object v4, v2, Lcom/peel/h/a/dn;->c:Lcom/squareup/picasso/Target;

    invoke-virtual {v1, v4}, Lcom/squareup/picasso/Picasso;->cancelRequest(Lcom/squareup/picasso/Target;)V

    goto :goto_4

    .line 142
    :cond_5
    sget v0, Lcom/peel/ui/fo;->btn_checkbox_off_states:I

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_2

    .line 145
    :cond_6
    const/16 v0, 0x8

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_2

    :cond_7
    move-object v0, v1

    goto :goto_5
.end method

.method public isEnabled(I)Z
    .locals 1

    .prologue
    .line 154
    iget-boolean v0, p0, Lcom/peel/h/a/dl;->d:Z

    if-eqz v0, :cond_0

    .line 155
    const/4 v0, 0x1

    .line 158
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
