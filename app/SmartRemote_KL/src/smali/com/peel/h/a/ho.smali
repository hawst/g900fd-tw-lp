.class Lcom/peel/h/a/ho;
.super Landroid/widget/BaseAdapter;


# instance fields
.field a:Landroid/view/LayoutInflater;

.field final synthetic b:Lcom/peel/h/a/hh;

.field private c:Z


# direct methods
.method public constructor <init>(Lcom/peel/h/a/hh;)V
    .locals 1

    .prologue
    .line 193
    iput-object p1, p0, Lcom/peel/h/a/ho;->b:Lcom/peel/h/a/hh;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 194
    invoke-virtual {p1}, Lcom/peel/h/a/hh;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/ae;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/h/a/ho;->a:Landroid/view/LayoutInflater;

    .line 195
    return-void
.end method


# virtual methods
.method public a(I)Ljava/lang/String;
    .locals 2

    .prologue
    .line 204
    iget-object v0, p0, Lcom/peel/h/a/ho;->b:Lcom/peel/h/a/hh;

    invoke-static {v0}, Lcom/peel/h/a/hh;->g(Lcom/peel/h/a/hh;)Ljava/util/Map;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/h/a/ho;->b:Lcom/peel/h/a/hh;

    invoke-static {v1}, Lcom/peel/h/a/hh;->f(Lcom/peel/h/a/hh;)[Ljava/lang/String;

    move-result-object v1

    aget-object v1, v1, p1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public a(Z)V
    .locals 0

    .prologue
    .line 241
    iput-boolean p1, p0, Lcom/peel/h/a/ho;->c:Z

    .line 242
    return-void
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 199
    iget-object v0, p0, Lcom/peel/h/a/ho;->b:Lcom/peel/h/a/hh;

    invoke-static {v0}, Lcom/peel/h/a/hh;->f(Lcom/peel/h/a/hh;)[Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/peel/h/a/ho;->b:Lcom/peel/h/a/hh;

    invoke-static {v0}, Lcom/peel/h/a/hh;->f(Lcom/peel/h/a/hh;)[Ljava/lang/String;

    move-result-object v0

    array-length v0, v0

    goto :goto_0
.end method

.method public synthetic getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 189
    invoke-virtual {p0, p1}, Lcom/peel/h/a/ho;->a(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2

    .prologue
    .line 209
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 214
    if-nez p2, :cond_1

    new-instance v0, Lcom/peel/h/a/hp;

    invoke-direct {v0, p0, v3}, Lcom/peel/h/a/hp;-><init>(Lcom/peel/h/a/ho;Lcom/peel/h/a/hi;)V

    move-object v1, v0

    .line 216
    :goto_0
    if-nez p2, :cond_0

    .line 217
    iget-object v0, p0, Lcom/peel/h/a/ho;->a:Landroid/view/LayoutInflater;

    sget v2, Lcom/peel/ui/fq;->fav_cut_row:I

    invoke-virtual {v0, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 218
    sget v0, Lcom/peel/ui/fp;->text:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-static {v1, v0}, Lcom/peel/h/a/hp;->a(Lcom/peel/h/a/hp;Landroid/widget/TextView;)Landroid/widget/TextView;

    .line 219
    sget v0, Lcom/peel/ui/fp;->checkbox:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-static {v1, v0}, Lcom/peel/h/a/hp;->a(Lcom/peel/h/a/hp;Landroid/widget/ImageView;)Landroid/widget/ImageView;

    .line 220
    invoke-virtual {p2, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 223
    :cond_0
    invoke-static {v1}, Lcom/peel/h/a/hp;->a(Lcom/peel/h/a/hp;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {p0, p1}, Lcom/peel/h/a/ho;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 225
    iget-boolean v0, p0, Lcom/peel/h/a/ho;->c:Z

    if-eqz v0, :cond_3

    .line 226
    invoke-static {v1}, Lcom/peel/h/a/hp;->b(Lcom/peel/h/a/hp;)Landroid/widget/ImageView;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 228
    iget-object v0, p0, Lcom/peel/h/a/ho;->b:Lcom/peel/h/a/hh;

    invoke-static {v0}, Lcom/peel/h/a/hh;->a(Lcom/peel/h/a/hh;)[Z

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/peel/h/a/ho;->b:Lcom/peel/h/a/hh;

    invoke-static {v0}, Lcom/peel/h/a/hh;->a(Lcom/peel/h/a/hh;)[Z

    move-result-object v0

    aget-boolean v0, v0, p1

    if-eqz v0, :cond_2

    .line 229
    invoke-static {v1}, Lcom/peel/h/a/hp;->b(Lcom/peel/h/a/hp;)Landroid/widget/ImageView;

    move-result-object v0

    sget v1, Lcom/peel/ui/fo;->btn_checkbox_on_states:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 237
    :goto_1
    return-object p2

    .line 214
    :cond_1
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/h/a/hp;

    move-object v1, v0

    goto :goto_0

    .line 231
    :cond_2
    invoke-static {v1}, Lcom/peel/h/a/hp;->b(Lcom/peel/h/a/hp;)Landroid/widget/ImageView;

    move-result-object v0

    sget v1, Lcom/peel/ui/fo;->btn_checkbox_off_states:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_1

    .line 234
    :cond_3
    invoke-static {v1}, Lcom/peel/h/a/hp;->b(Lcom/peel/h/a/hp;)Landroid/widget/ImageView;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_1
.end method
