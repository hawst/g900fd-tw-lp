.class Lcom/peel/h/a/bo;
.super Lcom/peel/util/t;


# instance fields
.field final synthetic a:Lcom/peel/h/a/bb;


# direct methods
.method constructor <init>(Lcom/peel/h/a/bb;I)V
    .locals 0

    .prologue
    .line 508
    iput-object p1, p0, Lcom/peel/h/a/bo;->a:Lcom/peel/h/a/bb;

    invoke-direct {p0, p2}, Lcom/peel/util/t;-><init>(I)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .prologue
    .line 511
    iget-boolean v0, p0, Lcom/peel/h/a/bo;->i:Z

    if-nez v0, :cond_1

    .line 543
    :cond_0
    :goto_0
    return-void

    .line 513
    :cond_1
    const/4 v2, 0x0

    .line 514
    iget-object v0, p0, Lcom/peel/h/a/bo;->j:Ljava/lang/Object;

    check-cast v0, [Lcom/peel/content/library/Library;

    check-cast v0, [Lcom/peel/content/library/Library;

    array-length v4, v0

    const/4 v1, 0x0

    move v3, v1

    :goto_1
    if-ge v3, v4, :cond_3

    aget-object v1, v0, v3

    .line 515
    invoke-virtual {v1}, Lcom/peel/content/library/Library;->f()Ljava/lang/String;

    move-result-object v5

    const-string/jumbo v6, "live"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    move-object v0, v1

    .line 521
    :goto_2
    if-eqz v0, :cond_0

    .line 522
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 523
    const-string/jumbo v2, "content_user"

    sget-object v3, Lcom/peel/content/a;->f:Lcom/peel/content/user/User;

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 524
    const-string/jumbo v2, "path"

    const-string/jumbo v3, "lineup"

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 525
    iget-object v2, p0, Lcom/peel/h/a/bo;->a:Lcom/peel/h/a/bb;

    sget-object v3, Lcom/peel/content/a;->f:Lcom/peel/content/user/User;

    invoke-virtual {v3}, Lcom/peel/content/user/User;->c()Landroid/os/Bundle;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/peel/h/a/bb;->a(Lcom/peel/h/a/bb;Landroid/os/Bundle;)Landroid/os/Bundle;

    .line 528
    const-string/jumbo v2, "user"

    sget-object v3, Lcom/peel/content/a;->f:Lcom/peel/content/user/User;

    invoke-virtual {v3}, Lcom/peel/content/user/User;->x()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 529
    const-string/jumbo v2, "hdprefs"

    iget-object v3, p0, Lcom/peel/h/a/bo;->a:Lcom/peel/h/a/bb;

    invoke-static {v3}, Lcom/peel/h/a/bb;->l(Lcom/peel/h/a/bb;)Landroid/os/Bundle;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lcom/peel/h/a/bo;->a:Lcom/peel/h/a/bb;

    invoke-static {v5}, Lcom/peel/h/a/bb;->h(Lcom/peel/h/a/bb;)Lcom/peel/data/ContentRoom;

    move-result-object v5

    invoke-virtual {v5}, Lcom/peel/data/ContentRoom;->a()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Lcom/peel/content/library/Library;->e()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 530
    const-string/jumbo v2, "country"

    iget-object v3, p0, Lcom/peel/h/a/bo;->a:Lcom/peel/h/a/bb;

    invoke-virtual {v3}, Lcom/peel/h/a/bb;->m()Landroid/support/v4/app/ae;

    move-result-object v3

    invoke-static {v3}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v3

    const-string/jumbo v4, "country_ISO"

    const-string/jumbo v5, "US"

    invoke-interface {v3, v4, v5}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 531
    new-instance v2, Lcom/peel/h/a/bp;

    const/4 v3, 0x1

    invoke-direct {v2, p0, v3}, Lcom/peel/h/a/bp;-><init>(Lcom/peel/h/a/bo;I)V

    invoke-virtual {v0, v1, v2}, Lcom/peel/content/library/Library;->a(Landroid/os/Bundle;Lcom/peel/util/t;)V

    .line 540
    iget-object v0, p0, Lcom/peel/h/a/bo;->a:Lcom/peel/h/a/bb;

    invoke-static {v0}, Lcom/peel/h/a/bb;->p(Lcom/peel/h/a/bb;)V

    goto/16 :goto_0

    .line 514
    :cond_2
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto/16 :goto_1

    :cond_3
    move-object v0, v2

    goto/16 :goto_2
.end method
