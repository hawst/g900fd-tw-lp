.class Lcom/peel/h/a/fp;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field final synthetic a:Lcom/peel/h/a/fo;


# direct methods
.method constructor <init>(Lcom/peel/h/a/fo;)V
    .locals 0

    .prologue
    .line 183
    iput-object p1, p0, Lcom/peel/h/a/fp;->a:Lcom/peel/h/a/fo;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 186
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-eqz v0, :cond_0

    .line 187
    iget-object v0, p0, Lcom/peel/h/a/fp;->a:Lcom/peel/h/a/fo;

    iget-object v0, v0, Lcom/peel/h/a/fo;->a:Lcom/peel/h/a/fn;

    invoke-static {v0}, Lcom/peel/h/a/fn;->d(Lcom/peel/h/a/fn;)Lcom/peel/widget/ag;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/widget/ag;->f()Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setEnabled(Z)V

    .line 188
    iget-object v0, p0, Lcom/peel/h/a/fp;->a:Lcom/peel/h/a/fo;

    iget-object v0, v0, Lcom/peel/h/a/fo;->a:Lcom/peel/h/a/fn;

    invoke-static {v0}, Lcom/peel/h/a/fn;->d(Lcom/peel/h/a/fn;)Lcom/peel/widget/ag;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/widget/ag;->f()Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setClickable(Z)V

    .line 189
    iget-object v0, p0, Lcom/peel/h/a/fp;->a:Lcom/peel/h/a/fo;

    iget-object v0, v0, Lcom/peel/h/a/fo;->a:Lcom/peel/h/a/fn;

    iget-object v1, p0, Lcom/peel/h/a/fp;->a:Lcom/peel/h/a/fo;

    iget-object v1, v1, Lcom/peel/h/a/fo;->a:Lcom/peel/h/a/fn;

    invoke-static {v1}, Lcom/peel/h/a/fn;->d(Lcom/peel/h/a/fn;)Lcom/peel/widget/ag;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/peel/h/a/fn;->a(Lcom/peel/h/a/fn;Lcom/peel/widget/ag;Ljava/lang/String;)V

    .line 194
    :goto_0
    return-void

    .line 191
    :cond_0
    iget-object v0, p0, Lcom/peel/h/a/fp;->a:Lcom/peel/h/a/fo;

    iget-object v0, v0, Lcom/peel/h/a/fo;->a:Lcom/peel/h/a/fn;

    invoke-static {v0}, Lcom/peel/h/a/fn;->d(Lcom/peel/h/a/fn;)Lcom/peel/widget/ag;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/widget/ag;->f()Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 192
    iget-object v0, p0, Lcom/peel/h/a/fp;->a:Lcom/peel/h/a/fo;

    iget-object v0, v0, Lcom/peel/h/a/fo;->a:Lcom/peel/h/a/fn;

    invoke-static {v0}, Lcom/peel/h/a/fn;->d(Lcom/peel/h/a/fn;)Lcom/peel/widget/ag;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/widget/ag;->f()Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setClickable(Z)V

    goto :goto_0
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 198
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 203
    return-void
.end method
