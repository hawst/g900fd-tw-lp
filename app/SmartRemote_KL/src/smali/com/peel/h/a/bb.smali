.class public Lcom/peel/h/a/bb;
.super Lcom/peel/d/u;


# static fields
.field private static final e:Ljava/lang/String;


# instance fields
.field private aA:Landroid/widget/LinearLayout;

.field private aB:Landroid/content/DialogInterface$OnDismissListener;

.field private aj:Z

.field private ak:Lcom/peel/widget/ag;

.field private al:[Ljava/lang/String;

.field private am:Lcom/peel/widget/ag;

.field private an:Lcom/peel/widget/ag;

.field private ao:Lcom/peel/widget/ag;

.field private ap:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private aq:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private ar:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private as:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private at:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private au:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private av:Ljava/util/LinkedHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashMap",
            "<",
            "Ljava/lang/String;",
            "[",
            "Lcom/peel/data/Channel;",
            ">;"
        }
    .end annotation
.end field

.field private aw:Landroid/os/Bundle;

.field private ax:Ljava/lang/String;

.field private ay:I

.field private az:Z

.field private f:Lcom/peel/content/library/LiveLibrary;

.field private g:Landroid/widget/ListView;

.field private h:Lcom/peel/data/ContentRoom;

.field private i:Landroid/view/LayoutInflater;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 55
    const-class v0, Lcom/peel/h/a/bb;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/peel/h/a/bb;->e:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 54
    invoke-direct {p0}, Lcom/peel/d/u;-><init>()V

    .line 62
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/peel/h/a/bb;->aj:Z

    .line 63
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/peel/h/a/bb;->ak:Lcom/peel/widget/ag;

    .line 798
    new-instance v0, Lcom/peel/h/a/bv;

    invoke-direct {v0, p0}, Lcom/peel/h/a/bv;-><init>(Lcom/peel/h/a/bb;)V

    iput-object v0, p0, Lcom/peel/h/a/bb;->aB:Landroid/content/DialogInterface$OnDismissListener;

    return-void
.end method

.method private S()V
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 119
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 120
    sget-object v1, Lcom/peel/content/a;->f:Lcom/peel/content/user/User;

    invoke-virtual {v1}, Lcom/peel/content/user/User;->g()Landroid/os/Bundle;

    move-result-object v1

    .line 122
    sget-object v2, Lcom/peel/content/a;->f:Lcom/peel/content/user/User;

    invoke-virtual {v2}, Lcom/peel/content/user/User;->d()Landroid/os/Bundle;

    move-result-object v2

    .line 123
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lcom/peel/h/a/bb;->h:Lcom/peel/data/ContentRoom;

    invoke-virtual {v5}, Lcom/peel/data/ContentRoom;->a()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/peel/h/a/bb;->f:Lcom/peel/content/library/LiveLibrary;

    invoke-virtual {v5}, Lcom/peel/content/library/LiveLibrary;->e()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 124
    new-instance v4, Ljava/util/ArrayList;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v6, p0, Lcom/peel/h/a/bb;->h:Lcom/peel/data/ContentRoom;

    invoke-virtual {v6}, Lcom/peel/data/ContentRoom;->a()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, "/"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/peel/h/a/bb;->f:Lcom/peel/content/library/LiveLibrary;

    invoke-virtual {v6}, Lcom/peel/content/library/LiveLibrary;->e()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-direct {v4, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v4, p0, Lcom/peel/h/a/bb;->aq:Ljava/util/ArrayList;

    .line 129
    :goto_0
    iget-object v2, p0, Lcom/peel/h/a/bb;->at:Ljava/util/ArrayList;

    if-nez v2, :cond_1

    .line 130
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/peel/h/a/bb;->at:Ljava/util/ArrayList;

    .line 131
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/peel/h/a/bb;->as:Ljava/util/ArrayList;

    .line 132
    invoke-virtual {p0}, Lcom/peel/h/a/bb;->q()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 133
    iget-object v2, p0, Lcom/peel/h/a/bb;->as:Ljava/util/ArrayList;

    sget v4, Lcom/peel/ui/ft;->shortlabel_hd:I

    invoke-virtual {p0, v4}, Lcom/peel/h/a/bb;->a(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 134
    iget-object v2, p0, Lcom/peel/h/a/bb;->as:Ljava/util/ArrayList;

    sget v4, Lcom/peel/ui/ft;->shortlabel_sd:I

    invoke-virtual {p0, v4}, Lcom/peel/h/a/bb;->a(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 136
    :cond_0
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/peel/h/a/bb;->au:Ljava/util/ArrayList;

    .line 137
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/peel/h/a/bb;->ar:Ljava/util/ArrayList;

    .line 140
    :cond_1
    sget-object v2, Lcom/peel/content/a;->f:Lcom/peel/content/user/User;

    invoke-virtual {v2}, Lcom/peel/content/user/User;->c()Landroid/os/Bundle;

    move-result-object v2

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lcom/peel/h/a/bb;->h:Lcom/peel/data/ContentRoom;

    invoke-virtual {v5}, Lcom/peel/data/ContentRoom;->a()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/peel/h/a/bb;->f:Lcom/peel/content/library/LiveLibrary;

    invoke-virtual {v5}, Lcom/peel/content/library/LiveLibrary;->e()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/peel/h/a/bb;->ax:Ljava/lang/String;

    .line 143
    sget-object v2, Lcom/peel/content/a;->e:Ljava/lang/String;

    const-string/jumbo v4, "US"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_f

    .line 144
    if-nez v1, :cond_8

    .line 145
    invoke-virtual {p0}, Lcom/peel/h/a/bb;->q()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 146
    sget v1, Lcom/peel/ui/ft;->edit_channels_subtitle_premium_default:I

    invoke-virtual {p0, v1}, Lcom/peel/h/a/bb;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 160
    :cond_2
    :goto_1
    iget-object v1, p0, Lcom/peel/h/a/bb;->ap:Ljava/util/ArrayList;

    if-nez v1, :cond_3

    .line 161
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/peel/h/a/bb;->ap:Ljava/util/ArrayList;

    .line 162
    const-string/jumbo v1, "B"

    iget-object v2, p0, Lcom/peel/h/a/bb;->ax:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_b

    .line 163
    invoke-virtual {p0}, Lcom/peel/h/a/bb;->q()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 164
    iget-object v1, p0, Lcom/peel/h/a/bb;->ap:Ljava/util/ArrayList;

    sget v2, Lcom/peel/ui/ft;->shortlabel_hd:I

    invoke-virtual {p0, v2}, Lcom/peel/h/a/bb;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 165
    iget-object v1, p0, Lcom/peel/h/a/bb;->ap:Ljava/util/ArrayList;

    sget v2, Lcom/peel/ui/ft;->shortlabel_sd:I

    invoke-virtual {p0, v2}, Lcom/peel/h/a/bb;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 176
    :cond_3
    :goto_2
    const-string/jumbo v1, "B"

    iget-object v2, p0, Lcom/peel/h/a/bb;->ax:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_d

    .line 177
    invoke-virtual {p0}, Lcom/peel/h/a/bb;->q()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 178
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget v2, Lcom/peel/ui/ft;->shortlabel_hd:I

    invoke-virtual {p0, v2}, Lcom/peel/h/a/bb;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget v2, Lcom/peel/ui/ft;->shortlabel_sd:I

    invoke-virtual {p0, v2}, Lcom/peel/h/a/bb;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 187
    :cond_4
    :goto_3
    iget-object v1, p0, Lcom/peel/h/a/bb;->aq:Ljava/util/ArrayList;

    const/4 v2, 0x1

    invoke-static {v1, v2}, Lcom/peel/util/bu;->a(Ljava/util/ArrayList;Z)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 217
    :cond_5
    :goto_4
    iget-object v1, p0, Lcom/peel/h/a/bb;->aA:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->removeAllViews()V

    move v2, v0

    .line 219
    :goto_5
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v2, v0, :cond_15

    .line 220
    iget-object v0, p0, Lcom/peel/h/a/bb;->i:Landroid/view/LayoutInflater;

    sget v1, Lcom/peel/ui/fq;->edit_channel_option_row:I

    const/4 v4, 0x0

    invoke-virtual {v0, v1, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v4

    .line 222
    sget v0, Lcom/peel/ui/fp;->tv_heading:I

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v1, p0, Lcom/peel/h/a/bb;->al:[Ljava/lang/String;

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 223
    sget v0, Lcom/peel/ui/fp;->tv_subheading:I

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 227
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-ne v2, v0, :cond_6

    .line 228
    sget v0, Lcom/peel/ui/fp;->divider_view:I

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 231
    :cond_6
    new-instance v0, Lcom/peel/h/a/bd;

    invoke-direct {v0, p0, v2}, Lcom/peel/h/a/bd;-><init>(Lcom/peel/h/a/bb;I)V

    invoke-virtual {v4, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 471
    iget-object v0, p0, Lcom/peel/h/a/bb;->aA:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 219
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_5

    .line 126
    :cond_7
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/peel/h/a/bb;->aq:Ljava/util/ArrayList;

    goto/16 :goto_0

    .line 148
    :cond_8
    sget-object v1, Lcom/peel/content/a;->f:Lcom/peel/content/user/User;

    invoke-virtual {v1}, Lcom/peel/content/user/User;->g()Landroid/os/Bundle;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/peel/h/a/bb;->h:Lcom/peel/data/ContentRoom;

    invoke-virtual {v4}, Lcom/peel/data/ContentRoom;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v4, "/"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v4, p0, Lcom/peel/h/a/bb;->f:Lcom/peel/content/library/LiveLibrary;

    invoke-virtual {v4}, Lcom/peel/content/library/LiveLibrary;->e()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    iput-object v1, p0, Lcom/peel/h/a/bb;->ar:Ljava/util/ArrayList;

    .line 149
    iget-object v1, p0, Lcom/peel/h/a/bb;->ar:Ljava/util/ArrayList;

    if-eqz v1, :cond_9

    iget-object v1, p0, Lcom/peel/h/a/bb;->ar:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-nez v1, :cond_a

    .line 150
    :cond_9
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/peel/h/a/bb;->ar:Ljava/util/ArrayList;

    .line 151
    invoke-virtual {p0}, Lcom/peel/h/a/bb;->q()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 152
    sget v1, Lcom/peel/ui/ft;->edit_channels_subtitle_premium_default:I

    invoke-virtual {p0, v1}, Lcom/peel/h/a/bb;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 154
    :cond_a
    invoke-virtual {p0}, Lcom/peel/h/a/bb;->q()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 155
    iget-object v1, p0, Lcom/peel/h/a/bb;->ar:Ljava/util/ArrayList;

    invoke-static {v1, v0}, Lcom/peel/util/bu;->a(Ljava/util/ArrayList;Z)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 167
    :cond_b
    const-string/jumbo v1, "HD"

    iget-object v2, p0, Lcom/peel/h/a/bb;->ax:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_c

    .line 168
    invoke-virtual {p0}, Lcom/peel/h/a/bb;->q()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 169
    iget-object v1, p0, Lcom/peel/h/a/bb;->ap:Ljava/util/ArrayList;

    sget v2, Lcom/peel/ui/ft;->shortlabel_hd:I

    invoke-virtual {p0, v2}, Lcom/peel/h/a/bb;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_2

    .line 171
    :cond_c
    invoke-virtual {p0}, Lcom/peel/h/a/bb;->q()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 172
    iget-object v1, p0, Lcom/peel/h/a/bb;->ap:Ljava/util/ArrayList;

    sget v2, Lcom/peel/ui/ft;->shortlabel_sd:I

    invoke-virtual {p0, v2}, Lcom/peel/h/a/bb;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_2

    .line 179
    :cond_d
    const-string/jumbo v1, "HD"

    iget-object v2, p0, Lcom/peel/h/a/bb;->ax:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_e

    .line 180
    invoke-virtual {p0}, Lcom/peel/h/a/bb;->q()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 181
    sget v1, Lcom/peel/ui/ft;->shortlabel_hd:I

    invoke-virtual {p0, v1}, Lcom/peel/h/a/bb;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_3

    .line 183
    :cond_e
    invoke-virtual {p0}, Lcom/peel/h/a/bb;->q()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 184
    sget v1, Lcom/peel/ui/ft;->shortlabel_sd:I

    invoke-virtual {p0, v1}, Lcom/peel/h/a/bb;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_3

    .line 189
    :cond_f
    iget-object v1, p0, Lcom/peel/h/a/bb;->ap:Ljava/util/ArrayList;

    if-nez v1, :cond_10

    .line 190
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/peel/h/a/bb;->ap:Ljava/util/ArrayList;

    .line 191
    const-string/jumbo v1, "B"

    iget-object v2, p0, Lcom/peel/h/a/bb;->ax:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_11

    .line 192
    invoke-virtual {p0}, Lcom/peel/h/a/bb;->q()Z

    move-result v1

    if-eqz v1, :cond_10

    .line 193
    iget-object v1, p0, Lcom/peel/h/a/bb;->ap:Ljava/util/ArrayList;

    sget v2, Lcom/peel/ui/ft;->shortlabel_hd:I

    invoke-virtual {p0, v2}, Lcom/peel/h/a/bb;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 194
    iget-object v1, p0, Lcom/peel/h/a/bb;->ap:Ljava/util/ArrayList;

    sget v2, Lcom/peel/ui/ft;->shortlabel_sd:I

    invoke-virtual {p0, v2}, Lcom/peel/h/a/bb;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 205
    :cond_10
    :goto_6
    const-string/jumbo v1, "B"

    iget-object v2, p0, Lcom/peel/h/a/bb;->ax:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_13

    .line 206
    invoke-virtual {p0}, Lcom/peel/h/a/bb;->q()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 207
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget v2, Lcom/peel/ui/ft;->shortlabel_hd:I

    invoke-virtual {p0, v2}, Lcom/peel/h/a/bb;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget v2, Lcom/peel/ui/ft;->shortlabel_sd:I

    invoke-virtual {p0, v2}, Lcom/peel/h/a/bb;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_4

    .line 196
    :cond_11
    const-string/jumbo v1, "HD"

    iget-object v2, p0, Lcom/peel/h/a/bb;->ax:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_12

    .line 197
    invoke-virtual {p0}, Lcom/peel/h/a/bb;->q()Z

    move-result v1

    if-eqz v1, :cond_10

    .line 198
    iget-object v1, p0, Lcom/peel/h/a/bb;->ap:Ljava/util/ArrayList;

    sget v2, Lcom/peel/ui/ft;->shortlabel_hd:I

    invoke-virtual {p0, v2}, Lcom/peel/h/a/bb;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_6

    .line 200
    :cond_12
    invoke-virtual {p0}, Lcom/peel/h/a/bb;->q()Z

    move-result v1

    if-eqz v1, :cond_10

    .line 201
    iget-object v1, p0, Lcom/peel/h/a/bb;->ap:Ljava/util/ArrayList;

    sget v2, Lcom/peel/ui/ft;->shortlabel_sd:I

    invoke-virtual {p0, v2}, Lcom/peel/h/a/bb;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_6

    .line 208
    :cond_13
    const-string/jumbo v1, "HD"

    iget-object v2, p0, Lcom/peel/h/a/bb;->ax:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_14

    .line 209
    invoke-virtual {p0}, Lcom/peel/h/a/bb;->q()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 210
    sget v1, Lcom/peel/ui/ft;->shortlabel_hd:I

    invoke-virtual {p0, v1}, Lcom/peel/h/a/bb;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_4

    .line 212
    :cond_14
    invoke-virtual {p0}, Lcom/peel/h/a/bb;->q()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 213
    sget v1, Lcom/peel/ui/ft;->shortlabel_sd:I

    invoke-virtual {p0, v1}, Lcom/peel/h/a/bb;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_4

    .line 473
    :cond_15
    return-void
.end method

.method static synthetic a(Lcom/peel/h/a/bb;I)I
    .locals 0

    .prologue
    .line 54
    iput p1, p0, Lcom/peel/h/a/bb;->ay:I

    return p1
.end method

.method static synthetic a(Lcom/peel/h/a/bb;Landroid/os/Bundle;)Landroid/os/Bundle;
    .locals 0

    .prologue
    .line 54
    iput-object p1, p0, Lcom/peel/h/a/bb;->aw:Landroid/os/Bundle;

    return-object p1
.end method

.method static synthetic a(Lcom/peel/h/a/bb;)Landroid/widget/ListView;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/peel/h/a/bb;->g:Landroid/widget/ListView;

    return-object v0
.end method

.method static synthetic a(Lcom/peel/h/a/bb;Lcom/peel/widget/ag;)Lcom/peel/widget/ag;
    .locals 0

    .prologue
    .line 54
    iput-object p1, p0, Lcom/peel/h/a/bb;->ao:Lcom/peel/widget/ag;

    return-object p1
.end method

.method static synthetic a(Lcom/peel/h/a/bb;[Lcom/peel/data/Channel;)V
    .locals 0

    .prologue
    .line 54
    invoke-direct {p0, p1}, Lcom/peel/h/a/bb;->a([Lcom/peel/data/Channel;)V

    return-void
.end method

.method private a([Lcom/peel/data/Channel;)V
    .locals 7

    .prologue
    .line 548
    iget-object v0, p0, Lcom/peel/h/a/bb;->f:Lcom/peel/content/library/LiveLibrary;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/peel/h/a/bb;->g:Landroid/widget/ListView;

    if-eqz v0, :cond_1

    .line 549
    iget-object v6, p0, Lcom/peel/h/a/bb;->g:Landroid/widget/ListView;

    new-instance v0, Lcom/peel/h/a/bw;

    invoke-virtual {p0}, Lcom/peel/h/a/bb;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    const/4 v2, -0x1

    iget-object v4, p0, Lcom/peel/h/a/bb;->f:Lcom/peel/content/library/LiveLibrary;

    iget-object v5, p0, Lcom/peel/h/a/bb;->h:Lcom/peel/data/ContentRoom;

    move-object v3, p1

    invoke-direct/range {v0 .. v5}, Lcom/peel/h/a/bw;-><init>(Landroid/content/Context;I[Lcom/peel/data/Channel;Lcom/peel/content/library/Library;Lcom/peel/data/ContentRoom;)V

    invoke-virtual {v6, v0}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 550
    iget-object v0, p0, Lcom/peel/h/a/bb;->g:Landroid/widget/ListView;

    new-instance v1, Lcom/peel/h/a/bq;

    invoke-direct {v1, p0}, Lcom/peel/h/a/bq;-><init>(Lcom/peel/h/a/bb;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 558
    iget-object v0, p0, Lcom/peel/h/a/bb;->g:Landroid/widget/ListView;

    new-instance v1, Lcom/peel/h/a/br;

    invoke-direct {v1, p0}, Lcom/peel/h/a/br;-><init>(Lcom/peel/h/a/bb;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 573
    iget-object v0, p0, Lcom/peel/h/a/bb;->g:Landroid/widget/ListView;

    new-instance v1, Lcom/peel/h/a/bs;

    invoke-direct {v1, p0}, Lcom/peel/h/a/bs;-><init>(Lcom/peel/h/a/bb;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemLongClickListener(Landroid/widget/AdapterView$OnItemLongClickListener;)V

    .line 620
    iget-boolean v0, p0, Lcom/peel/h/a/bb;->az:Z

    if-eqz v0, :cond_1

    .line 621
    iget v0, p0, Lcom/peel/h/a/bb;->ay:I

    if-eqz v0, :cond_0

    .line 622
    iget-object v0, p0, Lcom/peel/h/a/bb;->g:Landroid/widget/ListView;

    iget v1, p0, Lcom/peel/h/a/bb;->ay:I

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setSelection(I)V

    .line 623
    iget-object v0, p0, Lcom/peel/h/a/bb;->g:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->invalidate()V

    .line 625
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/peel/h/a/bb;->az:Z

    .line 628
    :cond_1
    return-void
.end method

.method static synthetic a(Lcom/peel/h/a/bb;Z)Z
    .locals 0

    .prologue
    .line 54
    iput-boolean p1, p0, Lcom/peel/h/a/bb;->aj:Z

    return p1
.end method

.method static synthetic b(Lcom/peel/h/a/bb;)Lcom/peel/widget/ag;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/peel/h/a/bb;->ao:Lcom/peel/widget/ag;

    return-object v0
.end method

.method static synthetic b(Lcom/peel/h/a/bb;Lcom/peel/widget/ag;)Lcom/peel/widget/ag;
    .locals 0

    .prologue
    .line 54
    iput-object p1, p0, Lcom/peel/h/a/bb;->am:Lcom/peel/widget/ag;

    return-object p1
.end method

.method static synthetic b(Lcom/peel/h/a/bb;Z)Z
    .locals 0

    .prologue
    .line 54
    iput-boolean p1, p0, Lcom/peel/h/a/bb;->az:Z

    return p1
.end method

.method static synthetic c(Lcom/peel/h/a/bb;)Landroid/content/DialogInterface$OnDismissListener;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/peel/h/a/bb;->aB:Landroid/content/DialogInterface$OnDismissListener;

    return-object v0
.end method

.method static synthetic c(Lcom/peel/h/a/bb;Lcom/peel/widget/ag;)Lcom/peel/widget/ag;
    .locals 0

    .prologue
    .line 54
    iput-object p1, p0, Lcom/peel/h/a/bb;->an:Lcom/peel/widget/ag;

    return-object p1
.end method

.method static synthetic c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 54
    sget-object v0, Lcom/peel/h/a/bb;->e:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic d(Lcom/peel/h/a/bb;)Landroid/view/LayoutInflater;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/peel/h/a/bb;->i:Landroid/view/LayoutInflater;

    return-object v0
.end method

.method static synthetic d(Lcom/peel/h/a/bb;Lcom/peel/widget/ag;)Lcom/peel/widget/ag;
    .locals 0

    .prologue
    .line 54
    iput-object p1, p0, Lcom/peel/h/a/bb;->ak:Lcom/peel/widget/ag;

    return-object p1
.end method

.method static synthetic e(Lcom/peel/h/a/bb;)Lcom/peel/content/library/LiveLibrary;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/peel/h/a/bb;->f:Lcom/peel/content/library/LiveLibrary;

    return-object v0
.end method

.method static synthetic f(Lcom/peel/h/a/bb;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/peel/h/a/bb;->au:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic g(Lcom/peel/h/a/bb;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/peel/h/a/bb;->ar:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic h(Lcom/peel/h/a/bb;)Lcom/peel/data/ContentRoom;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/peel/h/a/bb;->h:Lcom/peel/data/ContentRoom;

    return-object v0
.end method

.method static synthetic i(Lcom/peel/h/a/bb;)Lcom/peel/widget/ag;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/peel/h/a/bb;->am:Lcom/peel/widget/ag;

    return-object v0
.end method

.method static synthetic j(Lcom/peel/h/a/bb;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/peel/h/a/bb;->as:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic k(Lcom/peel/h/a/bb;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/peel/h/a/bb;->ap:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic l(Lcom/peel/h/a/bb;)Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/peel/h/a/bb;->aw:Landroid/os/Bundle;

    return-object v0
.end method

.method static synthetic m(Lcom/peel/h/a/bb;)Lcom/peel/widget/ag;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/peel/h/a/bb;->an:Lcom/peel/widget/ag;

    return-object v0
.end method

.method static synthetic n(Lcom/peel/h/a/bb;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/peel/h/a/bb;->at:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic o(Lcom/peel/h/a/bb;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/peel/h/a/bb;->aq:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic p(Lcom/peel/h/a/bb;)V
    .locals 0

    .prologue
    .line 54
    invoke-direct {p0}, Lcom/peel/h/a/bb;->S()V

    return-void
.end method

.method static synthetic q(Lcom/peel/h/a/bb;)Z
    .locals 1

    .prologue
    .line 54
    iget-boolean v0, p0, Lcom/peel/h/a/bb;->az:Z

    return v0
.end method

.method static synthetic r(Lcom/peel/h/a/bb;)Lcom/peel/widget/ag;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/peel/h/a/bb;->ak:Lcom/peel/widget/ag;

    return-object v0
.end method

.method static synthetic s(Lcom/peel/h/a/bb;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/peel/h/a/bb;->ax:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic t(Lcom/peel/h/a/bb;)Ljava/util/LinkedHashMap;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/peel/h/a/bb;->av:Ljava/util/LinkedHashMap;

    return-object v0
.end method

.method static synthetic u(Lcom/peel/h/a/bb;)Z
    .locals 1

    .prologue
    .line 54
    iget-boolean v0, p0, Lcom/peel/h/a/bb;->aj:Z

    return v0
.end method


# virtual methods
.method public Z()V
    .locals 6

    .prologue
    .line 792
    iget-object v0, p0, Lcom/peel/h/a/bb;->d:Lcom/peel/d/a;

    if-nez v0, :cond_0

    .line 793
    new-instance v0, Lcom/peel/d/a;

    sget-object v1, Lcom/peel/d/d;->b:Lcom/peel/d/d;

    sget-object v2, Lcom/peel/d/b;->a:Lcom/peel/d/b;

    sget-object v3, Lcom/peel/d/c;->b:Lcom/peel/d/c;

    sget v4, Lcom/peel/ui/ft;->header_edit_channel_lineup:I

    invoke-virtual {p0, v4}, Lcom/peel/h/a/bb;->a(I)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, Lcom/peel/d/a;-><init>(Lcom/peel/d/d;Lcom/peel/d/b;Lcom/peel/d/c;Ljava/lang/String;Ljava/util/List;)V

    iput-object v0, p0, Lcom/peel/h/a/bb;->d:Lcom/peel/d/a;

    .line 795
    :cond_0
    iget-object v0, p0, Lcom/peel/h/a/bb;->b:Lcom/peel/d/i;

    iget-object v1, p0, Lcom/peel/h/a/bb;->d:Lcom/peel/d/a;

    invoke-virtual {v0, v1}, Lcom/peel/d/i;->a(Lcom/peel/d/a;)V

    .line 796
    return-void
.end method

.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x0

    .line 77
    iput-object p1, p0, Lcom/peel/h/a/bb;->i:Landroid/view/LayoutInflater;

    .line 79
    sget v0, Lcom/peel/ui/fq;->settings_channels:I

    invoke-virtual {p1, v0, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 80
    sget v0, Lcom/peel/ui/fq;->settings_channels_edit_channels:I

    invoke-virtual {p1, v0, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/peel/h/a/bb;->aA:Landroid/widget/LinearLayout;

    .line 81
    sget v0, Lcom/peel/ui/fq;->settings_channels_text:I

    invoke-virtual {p1, v0, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 82
    sget v0, Lcom/peel/ui/fp;->channel_list:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/peel/h/a/bb;->g:Landroid/widget/ListView;

    .line 83
    sget v0, Lcom/peel/ui/fp;->text_overlay:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 86
    sget-object v3, Lcom/peel/content/a;->e:Ljava/lang/String;

    const-string/jumbo v4, "US"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 87
    invoke-virtual {p0}, Lcom/peel/h/a/bb;->n()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/peel/ui/fk;->edit_channels_header:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/peel/h/a/bb;->al:[Ljava/lang/String;

    .line 92
    :goto_0
    iget-object v3, p0, Lcom/peel/h/a/bb;->g:Landroid/widget/ListView;

    iget-object v4, p0, Lcom/peel/h/a/bb;->aA:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v4, v6, v7}, Landroid/widget/ListView;->addHeaderView(Landroid/view/View;Ljava/lang/Object;Z)V

    .line 93
    iget-object v3, p0, Lcom/peel/h/a/bb;->g:Landroid/widget/ListView;

    invoke-virtual {v3, v2, v6, v7}, Landroid/widget/ListView;->addHeaderView(Landroid/view/View;Ljava/lang/Object;Z)V

    .line 95
    iget-object v2, p0, Lcom/peel/h/a/bb;->g:Landroid/widget/ListView;

    invoke-virtual {v2}, Landroid/widget/ListView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v2

    new-instance v3, Lcom/peel/h/a/bc;

    invoke-direct {v3, p0, v0}, Lcom/peel/h/a/bc;-><init>(Lcom/peel/h/a/bb;Landroid/widget/TextView;)V

    invoke-virtual {v2, v3}, Landroid/view/ViewTreeObserver;->addOnScrollChangedListener(Landroid/view/ViewTreeObserver$OnScrollChangedListener;)V

    .line 104
    return-object v1

    .line 89
    :cond_0
    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    invoke-virtual {p0}, Lcom/peel/h/a/bb;->n()Landroid/content/res/Resources;

    move-result-object v4

    sget v5, Lcom/peel/ui/ft;->header_edit_hdsd:I

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v7

    iput-object v3, p0, Lcom/peel/h/a/bb;->al:[Ljava/lang/String;

    goto :goto_0
.end method

.method public a()Ljava/lang/String;
    .locals 2

    .prologue
    .line 632
    iget-object v0, p0, Lcom/peel/h/a/bb;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "goback_clazz"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a(Z)V
    .locals 7

    .prologue
    .line 636
    iget-boolean v0, p0, Lcom/peel/h/a/bb;->aj:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/peel/h/a/bb;->f:Lcom/peel/content/library/LiveLibrary;

    if-nez v0, :cond_1

    .line 781
    :cond_0
    :goto_0
    return-void

    .line 640
    :cond_1
    iget-object v0, p0, Lcom/peel/h/a/bb;->f:Lcom/peel/content/library/LiveLibrary;

    invoke-virtual {v0}, Lcom/peel/content/library/LiveLibrary;->i()[Lcom/peel/data/Channel;

    move-result-object v1

    .line 644
    sget-object v0, Lcom/peel/content/a;->f:Lcom/peel/content/user/User;

    invoke-virtual {v0}, Lcom/peel/content/user/User;->u()V

    .line 647
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 648
    const-string/jumbo v0, "content_user"

    sget-object v3, Lcom/peel/content/a;->f:Lcom/peel/content/user/User;

    invoke-virtual {v2, v0, v3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 649
    const-string/jumbo v0, "path"

    const-string/jumbo v3, "lineup"

    invoke-virtual {v2, v0, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 650
    const-string/jumbo v0, "force_refresh"

    const/4 v3, 0x1

    invoke-virtual {v2, v0, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 651
    const-string/jumbo v0, "room"

    iget-object v3, p0, Lcom/peel/h/a/bb;->h:Lcom/peel/data/ContentRoom;

    invoke-virtual {v2, v0, v3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 652
    const-string/jumbo v0, "user"

    sget-object v3, Lcom/peel/content/a;->f:Lcom/peel/content/user/User;

    invoke-virtual {v3}, Lcom/peel/content/user/User;->x()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 653
    const-string/jumbo v0, "country"

    invoke-virtual {p0}, Lcom/peel/h/a/bb;->m()Landroid/support/v4/app/ae;

    move-result-object v3

    invoke-static {v3}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v3

    const-string/jumbo v4, "country_ISO"

    const-string/jumbo v5, "US"

    invoke-interface {v3, v4, v5}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 655
    sget-object v0, Lcom/peel/content/a;->f:Lcom/peel/content/user/User;

    invoke-virtual {v0}, Lcom/peel/content/user/User;->c()Landroid/os/Bundle;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/h/a/bb;->aw:Landroid/os/Bundle;

    .line 656
    const-string/jumbo v0, "hdprefs"

    iget-object v3, p0, Lcom/peel/h/a/bb;->aw:Landroid/os/Bundle;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lcom/peel/h/a/bb;->h:Lcom/peel/data/ContentRoom;

    invoke-virtual {v5}, Lcom/peel/data/ContentRoom;->a()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/peel/h/a/bb;->f:Lcom/peel/content/library/LiveLibrary;

    invoke-virtual {v5}, Lcom/peel/content/library/LiveLibrary;->e()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 658
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 659
    array-length v4, v1

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v4, :cond_3

    aget-object v5, v1, v0

    .line 663
    invoke-virtual {v5}, Lcom/peel/data/Channel;->l()Z

    move-result v6

    if-nez v6, :cond_2

    .line 665
    invoke-virtual {v5}, Lcom/peel/data/Channel;->c()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, ","

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 659
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 669
    :cond_3
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    if-lez v0, :cond_4

    .line 670
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->deleteCharAt(I)Ljava/lang/StringBuilder;

    .line 673
    :cond_4
    iget-object v0, p0, Lcom/peel/h/a/bb;->f:Lcom/peel/content/library/LiveLibrary;

    new-instance v1, Lcom/peel/h/a/bu;

    invoke-direct {v1, p0, v3, p1}, Lcom/peel/h/a/bu;-><init>(Lcom/peel/h/a/bb;Ljava/lang/StringBuilder;Z)V

    invoke-virtual {v0, v2, v1}, Lcom/peel/content/library/LiveLibrary;->a(Landroid/os/Bundle;Lcom/peel/util/t;)V

    goto/16 :goto_0
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 785
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/peel/h/a/bb;->a(Z)V

    .line 786
    invoke-super {p0}, Lcom/peel/d/u;->b()Z

    move-result v0

    return v0
.end method

.method public c(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 502
    invoke-super {p0, p1}, Lcom/peel/d/u;->c(Landroid/os/Bundle;)V

    .line 504
    sget-object v0, Lcom/peel/content/a;->h:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-nez v0, :cond_0

    .line 545
    :goto_0
    return-void

    .line 508
    :cond_0
    iget-object v0, p0, Lcom/peel/h/a/bb;->h:Lcom/peel/data/ContentRoom;

    invoke-virtual {v0}, Lcom/peel/data/ContentRoom;->a()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lcom/peel/h/a/bo;

    const/4 v2, 0x1

    invoke-direct {v1, p0, v2}, Lcom/peel/h/a/bo;-><init>(Lcom/peel/h/a/bb;I)V

    invoke-static {v0, v1}, Lcom/peel/content/a;->a(Ljava/lang/String;Lcom/peel/util/t;)V

    goto :goto_0
.end method

.method public d(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 477
    invoke-super {p0, p1}, Lcom/peel/d/u;->d(Landroid/os/Bundle;)V

    .line 479
    invoke-virtual {p0}, Lcom/peel/h/a/bb;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    const-string/jumbo v1, "edit_channel_popup_trigger"

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lcom/peel/util/dq;->a(Landroid/content/Context;Ljava/lang/String;Z)V

    .line 480
    return-void
.end method

.method public f()V
    .locals 3

    .prologue
    .line 109
    invoke-super {p0}, Lcom/peel/d/u;->f()V

    .line 110
    invoke-virtual {p0}, Lcom/peel/h/a/bb;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    const-string/jumbo v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/ae;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 111
    invoke-virtual {p0}, Lcom/peel/h/a/bb;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/ae;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 116
    return-void
.end method

.method public h(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 490
    invoke-super {p0, p1}, Lcom/peel/d/u;->h(Landroid/os/Bundle;)V

    .line 492
    invoke-virtual {p0}, Lcom/peel/h/a/bb;->Z()V

    .line 494
    iget-object v0, p0, Lcom/peel/h/a/bb;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "room"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/peel/data/ContentRoom;

    iput-object v0, p0, Lcom/peel/h/a/bb;->h:Lcom/peel/data/ContentRoom;

    .line 495
    iget-object v0, p0, Lcom/peel/h/a/bb;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "library"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/peel/content/library/LiveLibrary;

    iput-object v0, p0, Lcom/peel/h/a/bb;->f:Lcom/peel/content/library/LiveLibrary;

    .line 497
    sget-object v0, Lcom/peel/content/a;->h:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/peel/h/a/bb;->c:Landroid/os/Bundle;

    invoke-virtual {p0, v0}, Lcom/peel/h/a/bb;->c(Landroid/os/Bundle;)V

    .line 498
    :cond_0
    return-void
.end method

.method public w()V
    .locals 0

    .prologue
    .line 484
    invoke-super {p0}, Lcom/peel/d/u;->w()V

    .line 485
    invoke-virtual {p0}, Lcom/peel/h/a/bb;->Z()V

    .line 486
    return-void
.end method
