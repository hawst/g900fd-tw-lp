.class Lcom/peel/h/a/fo;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/peel/h/a/fn;


# direct methods
.method constructor <init>(Lcom/peel/h/a/fn;)V
    .locals 0

    .prologue
    .line 172
    iput-object p1, p0, Lcom/peel/h/a/fo;->a:Lcom/peel/h/a/fn;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 175
    iget-object v0, p0, Lcom/peel/h/a/fo;->a:Lcom/peel/h/a/fn;

    new-instance v1, Lcom/peel/widget/ag;

    iget-object v2, p0, Lcom/peel/h/a/fo;->a:Lcom/peel/h/a/fn;

    invoke-virtual {v2}, Lcom/peel/h/a/fn;->m()Landroid/support/v4/app/ae;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/peel/widget/ag;-><init>(Landroid/content/Context;)V

    invoke-static {v0, v1}, Lcom/peel/h/a/fn;->a(Lcom/peel/h/a/fn;Lcom/peel/widget/ag;)Lcom/peel/widget/ag;

    .line 176
    iget-object v0, p0, Lcom/peel/h/a/fo;->a:Lcom/peel/h/a/fn;

    invoke-static {v0}, Lcom/peel/h/a/fn;->b(Lcom/peel/h/a/fn;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lcom/peel/ui/fq;->rename_room:I

    iget-object v2, p0, Lcom/peel/h/a/fo;->a:Lcom/peel/h/a/fn;

    invoke-static {v2}, Lcom/peel/h/a/fn;->a(Lcom/peel/h/a/fn;)Landroid/widget/LinearLayout;

    move-result-object v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 177
    sget v0, Lcom/peel/ui/fp;->edittext:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iget-object v2, p0, Lcom/peel/h/a/fo;->a:Lcom/peel/h/a/fn;

    invoke-static {v2}, Lcom/peel/h/a/fn;->c(Lcom/peel/h/a/fn;)Landroid/widget/CheckedTextView;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/CheckedTextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 178
    iget-object v0, p0, Lcom/peel/h/a/fo;->a:Lcom/peel/h/a/fn;

    invoke-static {v0}, Lcom/peel/h/a/fn;->d(Lcom/peel/h/a/fn;)Lcom/peel/widget/ag;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/peel/widget/ag;->setCanceledOnTouchOutside(Z)V

    .line 179
    iget-object v0, p0, Lcom/peel/h/a/fo;->a:Lcom/peel/h/a/fn;

    invoke-static {v0}, Lcom/peel/h/a/fn;->d(Lcom/peel/h/a/fn;)Lcom/peel/widget/ag;

    move-result-object v0

    sget v2, Lcom/peel/ui/ft;->pleaseenterroomname:I

    invoke-virtual {v0, v2}, Lcom/peel/widget/ag;->a(I)Lcom/peel/widget/ag;

    .line 180
    iget-object v0, p0, Lcom/peel/h/a/fo;->a:Lcom/peel/h/a/fn;

    invoke-static {v0}, Lcom/peel/h/a/fn;->d(Lcom/peel/h/a/fn;)Lcom/peel/widget/ag;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/peel/widget/ag;->a(Landroid/view/View;)Lcom/peel/widget/ag;

    .line 181
    iget-object v0, p0, Lcom/peel/h/a/fo;->a:Lcom/peel/h/a/fn;

    invoke-static {v0}, Lcom/peel/h/a/fn;->d(Lcom/peel/h/a/fn;)Lcom/peel/widget/ag;

    move-result-object v0

    sget v2, Lcom/peel/ui/ft;->cancel:I

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Lcom/peel/widget/ag;->c(ILandroid/content/DialogInterface$OnClickListener;)Lcom/peel/widget/ag;

    .line 183
    sget v0, Lcom/peel/ui/fp;->edittext:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    new-instance v2, Lcom/peel/h/a/fp;

    invoke-direct {v2, p0}, Lcom/peel/h/a/fp;-><init>(Lcom/peel/h/a/fo;)V

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 206
    iget-object v2, p0, Lcom/peel/h/a/fo;->a:Lcom/peel/h/a/fn;

    iget-object v0, p0, Lcom/peel/h/a/fo;->a:Lcom/peel/h/a/fn;

    invoke-static {v0}, Lcom/peel/h/a/fn;->d(Lcom/peel/h/a/fn;)Lcom/peel/widget/ag;

    move-result-object v3

    sget v0, Lcom/peel/ui/fp;->edittext:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v3, v0}, Lcom/peel/h/a/fn;->a(Lcom/peel/h/a/fn;Lcom/peel/widget/ag;Ljava/lang/String;)V

    .line 208
    iget-object v0, p0, Lcom/peel/h/a/fo;->a:Lcom/peel/h/a/fn;

    invoke-static {v0}, Lcom/peel/h/a/fn;->d(Lcom/peel/h/a/fn;)Lcom/peel/widget/ag;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/widget/ag;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Landroid/view/Window;->setSoftInputMode(I)V

    .line 209
    iget-object v0, p0, Lcom/peel/h/a/fo;->a:Lcom/peel/h/a/fn;

    invoke-static {v0}, Lcom/peel/h/a/fn;->d(Lcom/peel/h/a/fn;)Lcom/peel/widget/ag;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/widget/ag;->show()V

    .line 210
    return-void
.end method
