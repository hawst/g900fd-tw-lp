.class Lcom/peel/h/a/ci;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic a:Landroid/widget/ListView;

.field final synthetic b:[Ljava/lang/String;

.field final synthetic c:Lcom/peel/h/a/bz;


# direct methods
.method constructor <init>(Lcom/peel/h/a/bz;Landroid/widget/ListView;[Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 244
    iput-object p1, p0, Lcom/peel/h/a/ci;->c:Lcom/peel/h/a/bz;

    iput-object p2, p0, Lcom/peel/h/a/ci;->a:Landroid/widget/ListView;

    iput-object p3, p0, Lcom/peel/h/a/ci;->b:[Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 248
    iget-object v0, p0, Lcom/peel/h/a/ci;->c:Lcom/peel/h/a/bz;

    iget-object v1, p0, Lcom/peel/h/a/ci;->a:Landroid/widget/ListView;

    invoke-virtual {v1}, Landroid/widget/ListView;->getCheckedItemPosition()I

    move-result v1

    invoke-static {v0, v1}, Lcom/peel/h/a/bz;->a(Lcom/peel/h/a/bz;I)I

    .line 250
    invoke-static {}, Lcom/peel/h/a/bz;->c()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "\n##### volume idx: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/peel/h/a/ci;->c:Lcom/peel/h/a/bz;

    invoke-static {v2}, Lcom/peel/h/a/bz;->d(Lcom/peel/h/a/bz;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 251
    iget-object v0, p0, Lcom/peel/h/a/ci;->c:Lcom/peel/h/a/bz;

    invoke-static {v0}, Lcom/peel/h/a/bz;->d(Lcom/peel/h/a/bz;)I

    move-result v0

    if-ne v0, v3, :cond_0

    iget-object v0, p0, Lcom/peel/h/a/ci;->c:Lcom/peel/h/a/bz;

    invoke-static {v0}, Lcom/peel/h/a/bz;->e(Lcom/peel/h/a/bz;)Lcom/peel/control/h;

    move-result-object v0

    if-nez v0, :cond_0

    .line 252
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 253
    const-string/jumbo v1, "device_type"

    const/4 v2, 0x5

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 254
    const-string/jumbo v1, "room"

    iget-object v2, p0, Lcom/peel/h/a/ci;->c:Lcom/peel/h/a/bz;

    invoke-static {v2}, Lcom/peel/h/a/bz;->f(Lcom/peel/h/a/bz;)Lcom/peel/control/RoomControl;

    move-result-object v2

    invoke-virtual {v2}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v2

    invoke-virtual {v2}, Lcom/peel/data/at;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 255
    const-string/jumbo v1, "back_to_clazz"

    const-class v2, Lcom/peel/h/a/bz;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 256
    const-string/jumbo v1, "from_audio_setup"

    invoke-virtual {v0, v1, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 257
    const-string/jumbo v1, "activity_id"

    iget-object v2, p0, Lcom/peel/h/a/ci;->c:Lcom/peel/h/a/bz;

    invoke-static {v2}, Lcom/peel/h/a/bz;->g(Lcom/peel/h/a/bz;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 258
    const-string/jumbo v1, "providername"

    iget-object v2, p0, Lcom/peel/h/a/ci;->c:Lcom/peel/h/a/bz;

    iget-object v2, v2, Lcom/peel/h/a/bz;->c:Landroid/os/Bundle;

    const-string/jumbo v3, "providername"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 259
    iget-object v1, p0, Lcom/peel/h/a/ci;->c:Lcom/peel/h/a/bz;

    invoke-virtual {v1}, Lcom/peel/h/a/bz;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    const-class v2, Lcom/peel/h/a/a;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lcom/peel/d/e;->b(Landroid/support/v4/app/ae;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 263
    :goto_0
    return-void

    .line 261
    :cond_0
    iget-object v0, p0, Lcom/peel/h/a/ci;->c:Lcom/peel/h/a/bz;

    invoke-static {v0}, Lcom/peel/h/a/bz;->h(Lcom/peel/h/a/bz;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/h/a/ci;->b:[Ljava/lang/String;

    iget-object v2, p0, Lcom/peel/h/a/ci;->c:Lcom/peel/h/a/bz;

    invoke-static {v2}, Lcom/peel/h/a/bz;->d(Lcom/peel/h/a/bz;)I

    move-result v2

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method
