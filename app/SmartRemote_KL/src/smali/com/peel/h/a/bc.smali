.class Lcom/peel/h/a/bc;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/view/ViewTreeObserver$OnScrollChangedListener;


# instance fields
.field final synthetic a:Landroid/widget/TextView;

.field final synthetic b:Lcom/peel/h/a/bb;


# direct methods
.method constructor <init>(Lcom/peel/h/a/bb;Landroid/widget/TextView;)V
    .locals 0

    .prologue
    .line 95
    iput-object p1, p0, Lcom/peel/h/a/bc;->b:Lcom/peel/h/a/bb;

    iput-object p2, p0, Lcom/peel/h/a/bc;->a:Landroid/widget/TextView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onScrollChanged()V
    .locals 2

    .prologue
    .line 98
    iget-object v0, p0, Lcom/peel/h/a/bc;->b:Lcom/peel/h/a/bb;

    invoke-static {v0}, Lcom/peel/h/a/bb;->a(Lcom/peel/h/a/bb;)Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ListView;->getFirstVisiblePosition()I

    move-result v0

    const/4 v1, 0x1

    if-ge v0, v1, :cond_0

    .line 99
    iget-object v0, p0, Lcom/peel/h/a/bc;->a:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 102
    :goto_0
    return-void

    .line 101
    :cond_0
    iget-object v0, p0, Lcom/peel/h/a/bc;->a:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method
