.class Lcom/peel/util/cd;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic a:Lcom/peel/control/h;

.field final synthetic b:Ljava/lang/String;

.field final synthetic c:Z

.field final synthetic d:Ljava/lang/String;

.field final synthetic e:Lcom/peel/data/ContentRoom;

.field final synthetic f:Lcom/peel/content/library/Library;

.field final synthetic g:Lcom/peel/util/cb;


# direct methods
.method constructor <init>(Lcom/peel/util/cb;Lcom/peel/control/h;Ljava/lang/String;ZLjava/lang/String;Lcom/peel/data/ContentRoom;Lcom/peel/content/library/Library;)V
    .locals 0

    .prologue
    .line 1524
    iput-object p1, p0, Lcom/peel/util/cd;->g:Lcom/peel/util/cb;

    iput-object p2, p0, Lcom/peel/util/cd;->a:Lcom/peel/control/h;

    iput-object p3, p0, Lcom/peel/util/cd;->b:Ljava/lang/String;

    iput-boolean p4, p0, Lcom/peel/util/cd;->c:Z

    iput-object p5, p0, Lcom/peel/util/cd;->d:Ljava/lang/String;

    iput-object p6, p0, Lcom/peel/util/cd;->e:Lcom/peel/data/ContentRoom;

    iput-object p7, p0, Lcom/peel/util/cd;->f:Lcom/peel/content/library/Library;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 8

    .prologue
    const/16 v6, 0xa

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1527
    .line 1528
    iget-object v0, p0, Lcom/peel/util/cd;->a:Lcom/peel/control/h;

    if-eqz v0, :cond_11

    iget-object v0, p0, Lcom/peel/util/cd;->a:Lcom/peel/control/h;

    invoke-virtual {v0}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v0

    const-string/jumbo v3, "---"

    invoke-virtual {v0, v3}, Lcom/peel/data/g;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_11

    move v0, v1

    .line 1532
    :goto_0
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 1536
    iget-object v3, p0, Lcom/peel/util/cd;->b:Ljava/lang/String;

    const-string/jumbo v4, "T"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    iget-boolean v3, p0, Lcom/peel/util/cd;->c:Z

    if-eqz v3, :cond_8

    .line 1538
    :cond_0
    iget-object v0, p0, Lcom/peel/util/cd;->d:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 1539
    if-ge v0, v6, :cond_3

    .line 1540
    iget-object v0, p0, Lcom/peel/util/cd;->d:Ljava/lang/String;

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1590
    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/peel/util/cd;->a:Lcom/peel/control/h;

    if-eqz v0, :cond_10

    iget-object v0, p0, Lcom/peel/util/cd;->a:Lcom/peel/control/h;

    invoke-virtual {v0}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v0

    const-string/jumbo v3, "BS1"

    invoke-virtual {v0, v3}, Lcom/peel/data/g;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_10

    move v3, v1

    .line 1595
    :goto_2
    iget-object v0, p0, Lcom/peel/util/cd;->a:Lcom/peel/control/h;

    if-eqz v0, :cond_f

    iget-object v0, p0, Lcom/peel/util/cd;->a:Lcom/peel/control/h;

    invoke-virtual {v0}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v0

    const-string/jumbo v4, "CS1"

    invoke-virtual {v0, v4}, Lcom/peel/data/g;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_f

    move v4, v1

    .line 1599
    :goto_3
    iget-object v0, p0, Lcom/peel/util/cd;->g:Lcom/peel/util/cb;

    iget-object v0, v0, Lcom/peel/util/cb;->d:Landroid/content/Context;

    iget-object v6, p0, Lcom/peel/util/cd;->g:Lcom/peel/util/cb;

    iget-object v6, v6, Lcom/peel/util/cb;->c:Lcom/peel/control/a;

    invoke-static {v0, v6, v2}, Lcom/peel/util/bx;->b(Landroid/content/Context;Lcom/peel/control/a;I)I

    move-result v2

    .line 1601
    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_2
    :goto_4
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_e

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1602
    if-eqz v3, :cond_c

    if-ne v2, v1, :cond_c

    .line 1603
    iget-object v6, p0, Lcom/peel/util/cd;->g:Lcom/peel/util/cb;

    iget-object v6, v6, Lcom/peel/util/cb;->c:Lcom/peel/control/a;

    sget-object v7, Lcom/peel/util/bx;->n:Ljava/util/HashMap;

    invoke-virtual {v7, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v6, v0}, Lcom/peel/control/a;->b(Ljava/lang/String;)Z

    .line 1610
    :goto_5
    iget-object v0, p0, Lcom/peel/util/cd;->a:Lcom/peel/control/h;

    if-eqz v0, :cond_2

    .line 1611
    iget-object v0, p0, Lcom/peel/util/cd;->a:Lcom/peel/control/h;

    const-string/jumbo v6, "Delay"

    invoke-virtual {v0, v6}, Lcom/peel/control/h;->c(Ljava/lang/String;)Z

    goto :goto_4

    .line 1541
    :cond_3
    if-ne v0, v6, :cond_4

    .line 1542
    const-string/jumbo v0, "LLD_TEN"

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 1543
    :cond_4
    const/16 v3, 0xb

    if-ne v0, v3, :cond_5

    .line 1544
    const-string/jumbo v0, "LLD_ELEVEN"

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 1545
    :cond_5
    const/16 v3, 0xc

    if-ne v0, v3, :cond_6

    .line 1546
    const-string/jumbo v0, "LLD_TWELVE"

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 1548
    :cond_6
    sget-object v1, Lcom/peel/util/bx;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "For landline use channels 1-12: unsupported channel = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1624
    :cond_7
    :goto_6
    return-void

    .line 1554
    :cond_8
    invoke-static {}, Lcom/peel/d/i;->a()Lcom/peel/d/i;

    move-result-object v3

    .line 1555
    if-eqz v0, :cond_b

    .line 1557
    iget-object v0, p0, Lcom/peel/util/cd;->d:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    :goto_7
    const/4 v3, 0x3

    if-ge v0, v3, :cond_9

    .line 1558
    const-string/jumbo v3, "LLD_TEN"

    invoke-interface {v5, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1557
    add-int/lit8 v0, v0, 0x1

    goto :goto_7

    :cond_9
    move v0, v2

    .line 1561
    :goto_8
    iget-object v3, p0, Lcom/peel/util/cd;->d:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    if-ge v0, v3, :cond_1

    .line 1562
    iget-object v3, p0, Lcom/peel/util/cd;->d:Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljava/lang/String;->charAt(I)C

    move-result v3

    const/16 v4, 0x30

    if-ne v3, v4, :cond_a

    .line 1563
    const-string/jumbo v3, "LLD_TEN"

    invoke-interface {v5, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1561
    :goto_9
    add-int/lit8 v0, v0, 0x1

    goto :goto_8

    .line 1565
    :cond_a
    iget-object v3, p0, Lcom/peel/util/cd;->g:Lcom/peel/util/cb;

    iget-object v3, v3, Lcom/peel/util/cb;->b:Ljava/lang/String;

    add-int/lit8 v4, v0, 0x1

    invoke-virtual {v3, v0, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v5, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_9

    .line 1570
    :cond_b
    if-eqz v3, :cond_1

    .line 1571
    new-instance v0, Lcom/peel/widget/ag;

    iget-object v4, p0, Lcom/peel/util/cd;->g:Lcom/peel/util/cb;

    iget-object v4, v4, Lcom/peel/util/cb;->d:Landroid/content/Context;

    invoke-direct {v0, v4}, Lcom/peel/widget/ag;-><init>(Landroid/content/Context;)V

    sget v4, Lcom/peel/ui/ft;->preset_keys_question:I

    invoke-virtual {v0, v4}, Lcom/peel/widget/ag;->a(I)Lcom/peel/widget/ag;

    move-result-object v0

    .line 1572
    iget-object v4, p0, Lcom/peel/util/cd;->g:Lcom/peel/util/cb;

    iget-object v4, v4, Lcom/peel/util/cb;->d:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    sget v5, Lcom/peel/ui/ft;->should_preset_desc:I

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v6, p0, Lcom/peel/util/cd;->b:Ljava/lang/String;

    aput-object v6, v1, v2

    invoke-virtual {v4, v5, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/peel/widget/ag;->b(Ljava/lang/CharSequence;)Lcom/peel/widget/ag;

    .line 1573
    sget v1, Lcom/peel/ui/ft;->ok:I

    new-instance v2, Lcom/peel/util/ce;

    invoke-direct {v2, p0, v3}, Lcom/peel/util/ce;-><init>(Lcom/peel/util/cd;Lcom/peel/d/i;)V

    invoke-virtual {v0, v1, v2}, Lcom/peel/widget/ag;->a(ILandroid/content/DialogInterface$OnClickListener;)Lcom/peel/widget/ag;

    .line 1583
    sget v1, Lcom/peel/ui/ft;->cancel:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/peel/widget/ag;->c(ILandroid/content/DialogInterface$OnClickListener;)Lcom/peel/widget/ag;

    .line 1584
    invoke-virtual {v0}, Lcom/peel/widget/ag;->d()Lcom/peel/widget/ag;

    goto :goto_6

    .line 1604
    :cond_c
    if-eqz v4, :cond_d

    const/4 v6, 0x2

    if-ne v2, v6, :cond_d

    .line 1605
    iget-object v6, p0, Lcom/peel/util/cd;->g:Lcom/peel/util/cb;

    iget-object v6, v6, Lcom/peel/util/cb;->c:Lcom/peel/control/a;

    sget-object v7, Lcom/peel/util/bx;->o:Ljava/util/HashMap;

    invoke-virtual {v7, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v6, v0}, Lcom/peel/control/a;->b(Ljava/lang/String;)Z

    goto/16 :goto_5

    .line 1607
    :cond_d
    iget-object v6, p0, Lcom/peel/util/cd;->g:Lcom/peel/util/cb;

    iget-object v6, v6, Lcom/peel/util/cb;->c:Lcom/peel/control/a;

    invoke-virtual {v6, v0}, Lcom/peel/control/a;->b(Ljava/lang/String;)Z

    goto/16 :goto_5

    .line 1615
    :cond_e
    iget-object v0, p0, Lcom/peel/util/cd;->g:Lcom/peel/util/cb;

    iget-object v0, v0, Lcom/peel/util/cb;->a:Lcom/peel/util/t;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/peel/util/cd;->g:Lcom/peel/util/cb;

    iget-object v0, v0, Lcom/peel/util/cb;->a:Lcom/peel/util/t;

    iget-object v1, p0, Lcom/peel/util/cd;->g:Lcom/peel/util/cb;

    iget-boolean v1, v1, Lcom/peel/util/cb;->i:Z

    iget-object v2, p0, Lcom/peel/util/cd;->g:Lcom/peel/util/cb;

    iget-object v2, v2, Lcom/peel/util/cb;->j:Ljava/lang/Object;

    iget-object v3, p0, Lcom/peel/util/cd;->g:Lcom/peel/util/cb;

    iget-object v3, v3, Lcom/peel/util/cb;->k:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3}, Lcom/peel/util/t;->a(ZLjava/lang/Object;Ljava/lang/String;)V

    goto/16 :goto_6

    :cond_f
    move v4, v2

    goto/16 :goto_3

    :cond_10
    move v3, v2

    goto/16 :goto_2

    :cond_11
    move v0, v2

    goto/16 :goto_0
.end method
