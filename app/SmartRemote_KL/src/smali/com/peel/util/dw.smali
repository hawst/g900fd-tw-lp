.class public Lcom/peel/util/dw;
.super Ljava/lang/Object;


# static fields
.field public static a:F

.field public static b:Z

.field private static final c:Ljava/lang/String;

.field private static final d:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "[I>;"
        }
    .end annotation
.end field

.field private static final e:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "[I>;"
        }
    .end annotation
.end field

.field private static final f:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 29
    const-class v0, Lcom/peel/util/dw;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/peel/util/dw;->c:Ljava/lang/String;

    .line 30
    const/high16 v0, -0x40800000    # -1.0f

    sput v0, Lcom/peel/util/dw;->a:F

    .line 31
    sput-boolean v4, Lcom/peel/util/dw;->b:Z

    .line 33
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/peel/util/dw;->d:Ljava/util/Map;

    .line 36
    sget-object v0, Lcom/peel/util/dw;->d:Ljava/util/Map;

    const-string/jumbo v1, "Home"

    new-array v2, v8, [I

    sget v3, Lcom/peel/ui/fo;->btn_home:I

    aput v3, v2, v4

    sget v3, Lcom/peel/ui/fo;->btn_home_press:I

    aput v3, v2, v5

    sget v3, Lcom/peel/ui/ft;->button_home:I

    aput v3, v2, v6

    sget v3, Lcom/peel/ui/fo;->remote_icon_home:I

    aput v3, v2, v7

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 41
    sget-object v0, Lcom/peel/util/dw;->d:Ljava/util/Map;

    const-string/jumbo v1, "Back"

    new-array v2, v8, [I

    sget v3, Lcom/peel/ui/fo;->btn_back:I

    aput v3, v2, v4

    sget v3, Lcom/peel/ui/fo;->btn_back_press:I

    aput v3, v2, v5

    sget v3, Lcom/peel/ui/ft;->tv_remotecontrol_caption_back:I

    aput v3, v2, v6

    sget v3, Lcom/peel/ui/fo;->remote_icon_back:I

    aput v3, v2, v7

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 46
    sget-object v0, Lcom/peel/util/dw;->d:Ljava/util/Map;

    const-string/jumbo v1, "Play"

    new-array v2, v8, [I

    sget v3, Lcom/peel/ui/fo;->btn_play_pause:I

    aput v3, v2, v4

    sget v3, Lcom/peel/ui/fo;->btn_play_pause_press:I

    aput v3, v2, v5

    sget v3, Lcom/peel/ui/ft;->btn_playpause:I

    aput v3, v2, v6

    sget v3, Lcom/peel/ui/fo;->remote_icon_play_pause:I

    aput v3, v2, v7

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 51
    sget-object v0, Lcom/peel/util/dw;->d:Ljava/util/Map;

    const-string/jumbo v1, "Menu"

    new-array v2, v8, [I

    sget v3, Lcom/peel/ui/fo;->btn6_menu:I

    aput v3, v2, v4

    sget v3, Lcom/peel/ui/fo;->btn6_menu_press:I

    aput v3, v2, v5

    sget v3, Lcom/peel/ui/ft;->tv_remotecontrol_caption_menu:I

    aput v3, v2, v6

    sget v3, Lcom/peel/ui/ft;->menu_cap:I

    aput v3, v2, v7

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 56
    sget-object v0, Lcom/peel/util/dw;->d:Ljava/util/Map;

    const-string/jumbo v1, "Mute"

    new-array v2, v8, [I

    sget v3, Lcom/peel/ui/fo;->btn_mute:I

    aput v3, v2, v4

    sget v3, Lcom/peel/ui/fo;->btn_mute_press:I

    aput v3, v2, v5

    sget v3, Lcom/peel/ui/ft;->button_mute:I

    aput v3, v2, v6

    sget v3, Lcom/peel/ui/fo;->remote_icon_mute:I

    aput v3, v2, v7

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 61
    sget-object v0, Lcom/peel/util/dw;->d:Ljava/util/Map;

    const-string/jumbo v1, "keypad"

    new-array v2, v8, [I

    sget v3, Lcom/peel/ui/fo;->btn_keypad:I

    aput v3, v2, v4

    sget v3, Lcom/peel/ui/fo;->btn_keypad_press:I

    aput v3, v2, v5

    sget v3, Lcom/peel/ui/ft;->remotecontrol_other_keypad:I

    aput v3, v2, v6

    sget v3, Lcom/peel/ui/fo;->remote_icon_keypad:I

    aput v3, v2, v7

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 66
    sget-object v0, Lcom/peel/util/dw;->d:Ljava/util/Map;

    const-string/jumbo v1, "Input"

    new-array v2, v8, [I

    sget v3, Lcom/peel/ui/fo;->btn6_source:I

    aput v3, v2, v4

    sget v3, Lcom/peel/ui/fo;->btn6_source_press:I

    aput v3, v2, v5

    sget v3, Lcom/peel/ui/ft;->input:I

    aput v3, v2, v6

    sget v3, Lcom/peel/ui/ft;->input_cap:I

    aput v3, v2, v7

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 72
    sget-object v0, Lcom/peel/util/dw;->d:Ljava/util/Map;

    const-string/jumbo v1, "InstantReplay"

    new-array v2, v8, [I

    sget v3, Lcom/peel/ui/fo;->btn6_skip_back:I

    aput v3, v2, v4

    sget v3, Lcom/peel/ui/fo;->btn6_skip_back_press:I

    aput v3, v2, v5

    sget v3, Lcom/peel/ui/ft;->btn_instant_replay:I

    aput v3, v2, v6

    sget v3, Lcom/peel/ui/fo;->remote_icon_roku_replay:I

    aput v3, v2, v7

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 78
    sget-object v0, Lcom/peel/util/dw;->d:Ljava/util/Map;

    const-string/jumbo v1, "Options"

    new-array v2, v8, [I

    sget v3, Lcom/peel/ui/fo;->btn6_roku_star:I

    aput v3, v2, v4

    sget v3, Lcom/peel/ui/fo;->btn6_roku_star_press:I

    aput v3, v2, v5

    sget v3, Lcom/peel/ui/ft;->btn_options:I

    aput v3, v2, v6

    sget v3, Lcom/peel/ui/fo;->remote_icon_roku_option:I

    aput v3, v2, v7

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 83
    sget-object v0, Lcom/peel/util/dw;->d:Ljava/util/Map;

    const-string/jumbo v1, "Info"

    new-array v2, v8, [I

    sget v3, Lcom/peel/ui/fo;->btn6_info:I

    aput v3, v2, v4

    sget v3, Lcom/peel/ui/fo;->btn6_info_press:I

    aput v3, v2, v5

    sget v3, Lcom/peel/ui/ft;->tv_remotecontrol_caption_info:I

    aput v3, v2, v6

    sget v3, Lcom/peel/ui/ft;->info_cap:I

    aput v3, v2, v7

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 88
    sget-object v0, Lcom/peel/util/dw;->d:Ljava/util/Map;

    const-string/jumbo v1, "Exit"

    new-array v2, v8, [I

    sget v3, Lcom/peel/ui/fo;->btn6_exit:I

    aput v3, v2, v4

    sget v3, Lcom/peel/ui/fo;->btn6_exit_press:I

    aput v3, v2, v5

    sget v3, Lcom/peel/ui/ft;->tv_remotecontrol_caption_exit:I

    aput v3, v2, v6

    sget v3, Lcom/peel/ui/ft;->exit_cap:I

    aput v3, v2, v7

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 93
    sget-object v0, Lcom/peel/util/dw;->d:Ljava/util/Map;

    const-string/jumbo v1, "PopMenu"

    new-array v2, v8, [I

    sget v3, Lcom/peel/ui/fo;->btn6_popup_menu:I

    aput v3, v2, v4

    sget v3, Lcom/peel/ui/fo;->btn6_popup_menu_press:I

    aput v3, v2, v5

    sget v3, Lcom/peel/ui/ft;->bluray_popup_menu:I

    aput v3, v2, v6

    sget v3, Lcom/peel/ui/ft;->popmenu_cap:I

    aput v3, v2, v7

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 98
    sget-object v0, Lcom/peel/util/dw;->d:Ljava/util/Map;

    const-string/jumbo v1, "Return"

    new-array v2, v8, [I

    sget v3, Lcom/peel/ui/fo;->btn6_return:I

    aput v3, v2, v4

    sget v3, Lcom/peel/ui/fo;->btn6_return_press:I

    aput v3, v2, v5

    sget v3, Lcom/peel/ui/ft;->btn_return:I

    aput v3, v2, v6

    sget v3, Lcom/peel/ui/ft;->return_cap:I

    aput v3, v2, v7

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 103
    sget-object v0, Lcom/peel/util/dw;->d:Ljava/util/Map;

    const-string/jumbo v1, "Fav"

    new-array v2, v8, [I

    sget v3, Lcom/peel/ui/fo;->btn6_fav:I

    aput v3, v2, v4

    sget v3, Lcom/peel/ui/fo;->btn6_fav_press:I

    aput v3, v2, v5

    sget v3, Lcom/peel/ui/ft;->btn_fav:I

    aput v3, v2, v6

    sget v3, Lcom/peel/ui/ft;->fav_cap:I

    aput v3, v2, v7

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 108
    sget-object v0, Lcom/peel/util/dw;->d:Ljava/util/Map;

    const-string/jumbo v1, "AvMode"

    new-array v2, v8, [I

    sget v3, Lcom/peel/ui/fo;->btn6_av_mode:I

    aput v3, v2, v4

    sget v3, Lcom/peel/ui/fo;->btn6_av_mode_press:I

    aput v3, v2, v5

    sget v3, Lcom/peel/ui/ft;->btn_av_mode:I

    aput v3, v2, v6

    sget v3, Lcom/peel/ui/ft;->av_mode_cap:I

    aput v3, v2, v7

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 113
    sget-object v0, Lcom/peel/util/dw;->d:Ljava/util/Map;

    const-string/jumbo v1, "FLASH BACK"

    new-array v2, v8, [I

    sget v3, Lcom/peel/ui/fo;->btn6_flash_back:I

    aput v3, v2, v4

    sget v3, Lcom/peel/ui/fo;->btn6_flash_back_press:I

    aput v3, v2, v5

    sget v3, Lcom/peel/ui/ft;->btn_flash_back:I

    aput v3, v2, v6

    sget v3, Lcom/peel/ui/ft;->flash_back_cap:I

    aput v3, v2, v7

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 118
    sget-object v0, Lcom/peel/util/dw;->d:Ljava/util/Map;

    const-string/jumbo v1, "Smart_Hub"

    new-array v2, v8, [I

    sget v3, Lcom/peel/ui/fo;->btn6_samsung_link:I

    aput v3, v2, v4

    sget v3, Lcom/peel/ui/fo;->btn6_samsung_link_press:I

    aput v3, v2, v5

    sget v3, Lcom/peel/ui/ft;->smart_hub_1:I

    aput v3, v2, v6

    sget v3, Lcom/peel/ui/fo;->remote_icon_samsung_smart:I

    aput v3, v2, v7

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 123
    sget-object v0, Lcom/peel/util/dw;->d:Ljava/util/Map;

    const-string/jumbo v1, "Guide"

    new-array v2, v8, [I

    sget v3, Lcom/peel/ui/fo;->btn6_guide:I

    aput v3, v2, v4

    sget v3, Lcom/peel/ui/fo;->btn6_guide_press:I

    aput v3, v2, v5

    sget v3, Lcom/peel/ui/ft;->tv_remotecontrol_caption_guide:I

    aput v3, v2, v6

    sget v3, Lcom/peel/ui/ft;->guide_cap:I

    aput v3, v2, v7

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 128
    sget-object v0, Lcom/peel/util/dw;->d:Ljava/util/Map;

    const-string/jumbo v1, "DVR"

    new-array v2, v8, [I

    sget v3, Lcom/peel/ui/fo;->btn6_dvr:I

    aput v3, v2, v4

    sget v3, Lcom/peel/ui/fo;->btn6_dvr_press:I

    aput v3, v2, v5

    sget v3, Lcom/peel/ui/ft;->dvr:I

    aput v3, v2, v6

    sget v3, Lcom/peel/ui/ft;->dvr_cap:I

    aput v3, v2, v7

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 133
    sget-object v0, Lcom/peel/util/dw;->d:Ljava/util/Map;

    const-string/jumbo v1, "Skip_Back"

    new-array v2, v8, [I

    sget v3, Lcom/peel/ui/fo;->btn6_skip_back:I

    aput v3, v2, v4

    sget v3, Lcom/peel/ui/fo;->btn6_skip_back_press:I

    aput v3, v2, v5

    sget v3, Lcom/peel/ui/ft;->btn_skip_back:I

    aput v3, v2, v6

    sget v3, Lcom/peel/ui/fo;->remote_icon_skip_back:I

    aput v3, v2, v7

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 138
    sget-object v0, Lcom/peel/util/dw;->d:Ljava/util/Map;

    const-string/jumbo v1, "Skip_Forward"

    new-array v2, v8, [I

    sget v3, Lcom/peel/ui/fo;->btn6_skip_forward:I

    aput v3, v2, v4

    sget v3, Lcom/peel/ui/fo;->btn6_skip_forward_press:I

    aput v3, v2, v5

    sget v3, Lcom/peel/ui/ft;->btn_skip_forward:I

    aput v3, v2, v6

    sget v3, Lcom/peel/ui/fo;->remote_icon_skip_ff:I

    aput v3, v2, v7

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 143
    sget-object v0, Lcom/peel/util/dw;->d:Ljava/util/Map;

    const-string/jumbo v1, "OnDemand"

    new-array v2, v8, [I

    sget v3, Lcom/peel/ui/fo;->btn6_ondemand:I

    aput v3, v2, v4

    sget v3, Lcom/peel/ui/fo;->btn6_ondemand_press:I

    aput v3, v2, v5

    sget v3, Lcom/peel/ui/ft;->btn_ondemand:I

    aput v3, v2, v6

    sget v3, Lcom/peel/ui/ft;->ondemand_cap:I

    aput v3, v2, v7

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 148
    sget-object v0, Lcom/peel/util/dw;->d:Ljava/util/Map;

    const-string/jumbo v1, "LiveTV"

    new-array v2, v8, [I

    sget v3, Lcom/peel/ui/fo;->btn6_livetv:I

    aput v3, v2, v4

    sget v3, Lcom/peel/ui/fo;->btn6_livetv_press:I

    aput v3, v2, v5

    sget v3, Lcom/peel/ui/ft;->btn_livetv:I

    aput v3, v2, v6

    sget v3, Lcom/peel/ui/ft;->livetv_cap:I

    aput v3, v2, v7

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 153
    sget-object v0, Lcom/peel/util/dw;->d:Ljava/util/Map;

    const-string/jumbo v1, "Last"

    new-array v2, v8, [I

    sget v3, Lcom/peel/ui/fo;->btn6_last:I

    aput v3, v2, v4

    sget v3, Lcom/peel/ui/fo;->btn6_last_press:I

    aput v3, v2, v5

    sget v3, Lcom/peel/ui/ft;->btn_last:I

    aput v3, v2, v6

    sget v3, Lcom/peel/ui/ft;->last_cap:I

    aput v3, v2, v7

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 158
    sget-object v0, Lcom/peel/util/dw;->d:Ljava/util/Map;

    const-string/jumbo v1, "list"

    new-array v2, v8, [I

    sget v3, Lcom/peel/ui/fo;->btn6_list:I

    aput v3, v2, v4

    sget v3, Lcom/peel/ui/fo;->btn6_list_press:I

    aput v3, v2, v5

    sget v3, Lcom/peel/ui/ft;->button_list:I

    aput v3, v2, v6

    sget v3, Lcom/peel/ui/ft;->list_cap:I

    aput v3, v2, v7

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 163
    sget-object v0, Lcom/peel/util/dw;->d:Ljava/util/Map;

    const-string/jumbo v1, "TIVO"

    new-array v2, v8, [I

    sget v3, Lcom/peel/ui/fo;->btn6_tivo:I

    aput v3, v2, v4

    sget v3, Lcom/peel/ui/fo;->btn6_tivo_press:I

    aput v3, v2, v5

    sget v3, Lcom/peel/ui/ft;->tv_remotecontrol_caption_tivo:I

    aput v3, v2, v6

    sget v3, Lcom/peel/ui/fo;->remote_icon_tivo_logo:I

    aput v3, v2, v7

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 168
    sget-object v0, Lcom/peel/util/dw;->d:Ljava/util/Map;

    const-string/jumbo v1, "slow"

    new-array v2, v8, [I

    sget v3, Lcom/peel/ui/fo;->btn6_slow:I

    aput v3, v2, v4

    sget v3, Lcom/peel/ui/fo;->btn6_slow_press:I

    aput v3, v2, v5

    sget v3, Lcom/peel/ui/ft;->btn_slow:I

    aput v3, v2, v6

    sget v3, Lcom/peel/ui/ft;->slow_cap:I

    aput v3, v2, v7

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 173
    sget-object v0, Lcom/peel/util/dw;->d:Ljava/util/Map;

    const-string/jumbo v1, "ThumbsUp"

    new-array v2, v8, [I

    sget v3, Lcom/peel/ui/fo;->btn6_thumbs_up:I

    aput v3, v2, v4

    sget v3, Lcom/peel/ui/fo;->btn6_thumbs_up_press:I

    aput v3, v2, v5

    sget v3, Lcom/peel/ui/ft;->button_thumbs_up:I

    aput v3, v2, v6

    sget v3, Lcom/peel/ui/fo;->remote_icon_tivo_like:I

    aput v3, v2, v7

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 178
    sget-object v0, Lcom/peel/util/dw;->d:Ljava/util/Map;

    const-string/jumbo v1, "ThumbsDown"

    new-array v2, v8, [I

    sget v3, Lcom/peel/ui/fo;->btn6_thumbs_down:I

    aput v3, v2, v4

    sget v3, Lcom/peel/ui/fo;->btn6_thumbs_down_press:I

    aput v3, v2, v5

    sget v3, Lcom/peel/ui/ft;->button_thumbs_down:I

    aput v3, v2, v6

    sget v3, Lcom/peel/ui/fo;->remote_icon_tivo_dislike:I

    aput v3, v2, v7

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 183
    sget-object v0, Lcom/peel/util/dw;->d:Ljava/util/Map;

    const-string/jumbo v1, "VANE"

    new-array v2, v8, [I

    sget v3, Lcom/peel/ui/fo;->btn_direction:I

    aput v3, v2, v4

    sget v3, Lcom/peel/ui/fo;->btn_direction_press:I

    aput v3, v2, v5

    sget v3, Lcom/peel/ui/ft;->direction:I

    aput v3, v2, v6

    sget v3, Lcom/peel/ui/ft;->direction:I

    aput v3, v2, v7

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 188
    sget-object v0, Lcom/peel/util/dw;->d:Ljava/util/Map;

    const-string/jumbo v1, "MODE"

    new-array v2, v8, [I

    sget v3, Lcom/peel/ui/fo;->btn_mode_small:I

    aput v3, v2, v4

    sget v3, Lcom/peel/ui/fo;->btn_mode_small_press:I

    aput v3, v2, v5

    sget v3, Lcom/peel/ui/ft;->button_mode:I

    aput v3, v2, v6

    sget v3, Lcom/peel/ui/ft;->button_mode:I

    aput v3, v2, v7

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 193
    sget-object v0, Lcom/peel/util/dw;->d:Ljava/util/Map;

    const-string/jumbo v1, "CHList"

    new-array v2, v8, [I

    sget v3, Lcom/peel/ui/fo;->btn6_bg:I

    aput v3, v2, v4

    sget v3, Lcom/peel/ui/fo;->btn6_bg_press:I

    aput v3, v2, v5

    sget v3, Lcom/peel/ui/ft;->ch_list:I

    aput v3, v2, v6

    sget v3, Lcom/peel/ui/ft;->chlist_cap:I

    aput v3, v2, v7

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 198
    sget-object v0, Lcom/peel/util/dw;->d:Ljava/util/Map;

    const-string/jumbo v1, "Tools"

    new-array v2, v8, [I

    sget v3, Lcom/peel/ui/fo;->btn6_bg:I

    aput v3, v2, v4

    sget v3, Lcom/peel/ui/fo;->btn6_bg_press:I

    aput v3, v2, v5

    sget v3, Lcom/peel/ui/ft;->tools:I

    aput v3, v2, v6

    sget v3, Lcom/peel/ui/ft;->tools_cap:I

    aput v3, v2, v7

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 203
    sget-object v0, Lcom/peel/util/dw;->d:Ljava/util/Map;

    const-string/jumbo v1, "default"

    new-array v2, v8, [I

    sget v3, Lcom/peel/ui/fo;->icon:I

    aput v3, v2, v4

    sget v3, Lcom/peel/ui/fo;->icon:I

    aput v3, v2, v5

    sget v3, Lcom/peel/ui/ft;->app_name:I

    aput v3, v2, v6

    sget v3, Lcom/peel/ui/fo;->icon:I

    aput v3, v2, v7

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 208
    sget-object v0, Lcom/peel/util/dw;->d:Ljava/util/Map;

    const-string/jumbo v1, "Red"

    const/4 v2, 0x5

    new-array v2, v2, [I

    const v3, 0x30d74

    aput v3, v2, v4

    sget v3, Lcom/peel/ui/fo;->btn_02_box_a:I

    aput v3, v2, v5

    sget v3, Lcom/peel/ui/fo;->btn_02_box_a_press:I

    aput v3, v2, v6

    sget v3, Lcom/peel/ui/fo;->btn_box_a:I

    aput v3, v2, v7

    sget v3, Lcom/peel/ui/fo;->btn_box_a_press:I

    aput v3, v2, v8

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 214
    sget-object v0, Lcom/peel/util/dw;->d:Ljava/util/Map;

    const-string/jumbo v1, "Green"

    const/4 v2, 0x5

    new-array v2, v2, [I

    const v3, 0x30d75

    aput v3, v2, v4

    sget v3, Lcom/peel/ui/fo;->btn_02_box_b:I

    aput v3, v2, v5

    sget v3, Lcom/peel/ui/fo;->btn_02_box_b_press:I

    aput v3, v2, v6

    sget v3, Lcom/peel/ui/fo;->btn_box_b:I

    aput v3, v2, v7

    sget v3, Lcom/peel/ui/fo;->btn_box_b_press:I

    aput v3, v2, v8

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 220
    sget-object v0, Lcom/peel/util/dw;->d:Ljava/util/Map;

    const-string/jumbo v1, "Yellow"

    const/4 v2, 0x5

    new-array v2, v2, [I

    const v3, 0x30d76

    aput v3, v2, v4

    sget v3, Lcom/peel/ui/fo;->btn_02_box_c:I

    aput v3, v2, v5

    sget v3, Lcom/peel/ui/fo;->btn_02_box_c_press:I

    aput v3, v2, v6

    sget v3, Lcom/peel/ui/fo;->btn_box_c:I

    aput v3, v2, v7

    sget v3, Lcom/peel/ui/fo;->btn_box_c_press:I

    aput v3, v2, v8

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 226
    sget-object v0, Lcom/peel/util/dw;->d:Ljava/util/Map;

    const-string/jumbo v1, "Blue"

    const/4 v2, 0x5

    new-array v2, v2, [I

    const v3, 0x30d77

    aput v3, v2, v4

    sget v3, Lcom/peel/ui/fo;->btn_02_box_d:I

    aput v3, v2, v5

    sget v3, Lcom/peel/ui/fo;->btn_02_box_d_press:I

    aput v3, v2, v6

    sget v3, Lcom/peel/ui/fo;->btn_box_d:I

    aput v3, v2, v7

    sget v3, Lcom/peel/ui/fo;->btn_box_d_press:I

    aput v3, v2, v8

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1262
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/peel/util/dw;->e:Ljava/util/Map;

    .line 1264
    sget-object v0, Lcom/peel/util/dw;->e:Ljava/util/Map;

    const-string/jumbo v1, "List"

    new-array v2, v7, [I

    sget v3, Lcom/peel/ui/fo;->btn6_list:I

    aput v3, v2, v4

    sget v3, Lcom/peel/ui/fo;->btn6_list_press:I

    aput v3, v2, v5

    sget v3, Lcom/peel/ui/ft;->button_list:I

    aput v3, v2, v6

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1265
    sget-object v0, Lcom/peel/util/dw;->e:Ljava/util/Map;

    const-string/jumbo v1, "Input"

    new-array v2, v7, [I

    sget v3, Lcom/peel/ui/fo;->btn6_source:I

    aput v3, v2, v4

    sget v3, Lcom/peel/ui/fo;->btn6_source_press:I

    aput v3, v2, v5

    sget v3, Lcom/peel/ui/ft;->button_source:I

    aput v3, v2, v6

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1348
    new-array v0, v8, [Ljava/lang/String;

    const-string/jumbo v1, "A"

    aput-object v1, v0, v4

    const-string/jumbo v1, "B"

    aput-object v1, v0, v5

    const-string/jumbo v1, "C"

    aput-object v1, v0, v6

    const-string/jumbo v1, "D"

    aput-object v1, v0, v7

    sput-object v0, Lcom/peel/util/dw;->f:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(I)I
    .locals 2

    .prologue
    .line 1413
    int-to-float v0, p0

    sget v1, Lcom/peel/util/dw;->a:F

    mul-float/2addr v0, v1

    const/high16 v1, 0x3f000000    # 0.5f

    add-float/2addr v0, v1

    float-to-int v0, v0

    return v0
.end method

.method public static a(Landroid/content/Context;IIIIIIIIIIIILjava/util/List;IILjava/lang/String;Ljava/lang/String;Landroid/view/View$OnClickListener;)Landroid/view/View;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "IIIIIIIIIIII",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;II",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Landroid/view/View$OnClickListener;",
            ")",
            "Landroid/view/View;"
        }
    .end annotation

    .prologue
    .line 417
    new-instance v2, Landroid/widget/RelativeLayout;

    invoke-direct {v2, p0}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 418
    invoke-virtual {v2, p1}, Landroid/widget/RelativeLayout;->setId(I)V

    .line 419
    new-instance v3, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v1, -0x2

    const/4 v4, -0x2

    invoke-direct {v3, v1, v4}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 420
    const/16 v1, 0x53

    invoke-static {v1}, Lcom/peel/util/dw;->a(I)I

    move-result v1

    iput v1, v3, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    .line 421
    const/16 v1, 0xea

    invoke-static {v1}, Lcom/peel/util/dw;->a(I)I

    move-result v1

    iput v1, v3, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    .line 422
    if-lez p5, :cond_0

    invoke-static {p5}, Lcom/peel/util/dw;->a(I)I

    move-result v1

    iput v1, v3, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 423
    :cond_0
    if-lez p6, :cond_1

    invoke-static {p6}, Lcom/peel/util/dw;->a(I)I

    move-result v1

    iput v1, v3, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    .line 424
    :cond_1
    if-lez p7, :cond_2

    invoke-static {p7}, Lcom/peel/util/dw;->a(I)I

    move-result v1

    iput v1, v3, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 425
    :cond_2
    if-lez p8, :cond_3

    invoke-static/range {p8 .. p8}, Lcom/peel/util/dw;->a(I)I

    move-result v1

    iput v1, v3, Landroid/widget/RelativeLayout$LayoutParams;->bottomMargin:I

    .line 427
    :cond_3
    if-eqz p13, :cond_6

    invoke-interface/range {p13 .. p13}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_6

    .line 428
    invoke-interface/range {p13 .. p13}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_4
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 429
    const-string/jumbo v5, ","

    invoke-virtual {v1, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 430
    array-length v5, v1

    const/4 v6, 0x1

    if-ne v5, v6, :cond_5

    .line 431
    const/4 v5, 0x0

    aget-object v1, v1, v5

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v3, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    goto :goto_0

    .line 432
    :cond_5
    array-length v5, v1

    const/4 v6, 0x2

    if-ne v5, v6, :cond_4

    .line 433
    const/4 v5, 0x0

    aget-object v5, v1, v5

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    const/4 v6, 0x1

    aget-object v1, v1, v6

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v3, v5, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    goto :goto_0

    .line 438
    :cond_6
    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 440
    new-instance v1, Landroid/widget/ImageView;

    invoke-direct {v1, p0}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 441
    invoke-virtual {v1, p2}, Landroid/widget/ImageView;->setId(I)V

    .line 442
    const/4 v3, 0x1

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setClickable(Z)V

    .line 443
    move-object/from16 v0, p18

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 444
    invoke-virtual {p0, p4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 445
    invoke-virtual {v1, p3}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 446
    new-instance v3, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v4, -0x1

    const/4 v5, -0x1

    invoke-direct {v3, v4, v5}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 447
    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 449
    invoke-virtual {v2, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 451
    new-instance v3, Landroid/view/View;

    invoke-direct {v3, p0}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 452
    move-object/from16 v0, p16

    invoke-virtual {v3, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 453
    move/from16 v0, p9

    invoke-virtual {v3, v0}, Landroid/view/View;->setId(I)V

    .line 454
    new-instance v4, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v5, -0x1

    const/4 v6, -0x2

    invoke-direct {v4, v5, v6}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 455
    const/16 v5, 0x75

    invoke-static {v5}, Lcom/peel/util/dw;->a(I)I

    move-result v5

    iput v5, v4, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    .line 456
    const/16 v5, 0xa

    invoke-virtual {v4, v5}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 457
    const/16 v5, 0xe

    invoke-virtual {v4, v5}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 458
    move/from16 v0, p10

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 459
    new-instance v5, Lcom/peel/ui/jw;

    move/from16 v0, p14

    invoke-direct {v5, v1, v0}, Lcom/peel/ui/jw;-><init>(Landroid/widget/ImageView;I)V

    invoke-virtual {v3, v5}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 461
    invoke-virtual {v3, v4}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 463
    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 465
    new-instance v4, Landroid/view/View;

    invoke-direct {v4, p0}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 466
    move-object/from16 v0, p17

    invoke-virtual {v4, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 467
    move/from16 v0, p11

    invoke-virtual {v4, v0}, Landroid/view/View;->setId(I)V

    .line 468
    new-instance v5, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v6, -0x1

    const/4 v7, -0x2

    invoke-direct {v5, v6, v7}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 469
    const/16 v6, 0x75

    invoke-static {v6}, Lcom/peel/util/dw;->a(I)I

    move-result v6

    iput v6, v5, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    .line 470
    const/16 v6, 0xe

    invoke-virtual {v5, v6}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 471
    const/4 v6, 0x3

    invoke-virtual {v3}, Landroid/view/View;->getId()I

    move-result v3

    invoke-virtual {v5, v6, v3}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 472
    move/from16 v0, p12

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 473
    new-instance v3, Lcom/peel/ui/jw;

    move/from16 v0, p15

    invoke-direct {v3, v1, v0}, Lcom/peel/ui/jw;-><init>(Landroid/widget/ImageView;I)V

    invoke-virtual {v4, v3}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 475
    invoke-virtual {v4, v5}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 477
    invoke-virtual {v2, v4}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 479
    return-object v2
.end method

.method public static a(Landroid/content/Context;IIIIILandroid/view/View$OnClickListener;)Landroid/view/View;
    .locals 8

    .prologue
    const/16 v4, 0x44

    const/4 v3, 0x6

    const/4 v7, 0x1

    const/4 v6, 0x0

    const/4 v5, -0x2

    .line 636
    new-instance v0, Landroid/widget/LinearLayout;

    invoke-direct {v0, p0}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 637
    invoke-virtual {v0, v7}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 638
    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v1, v5, v5}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 641
    if-lez p2, :cond_0

    invoke-static {p2}, Lcom/peel/util/dw;->a(I)I

    move-result v2

    iput v2, v1, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    .line 642
    :cond_0
    if-lez p3, :cond_1

    invoke-static {p3}, Lcom/peel/util/dw;->a(I)I

    move-result v2

    iput v2, v1, Landroid/widget/LinearLayout$LayoutParams;->rightMargin:I

    .line 643
    :cond_1
    if-lez p4, :cond_2

    invoke-static {p4}, Lcom/peel/util/dw;->a(I)I

    move-result v2

    iput v2, v1, Landroid/widget/LinearLayout$LayoutParams;->topMargin:I

    .line 644
    :cond_2
    if-lez p5, :cond_3

    invoke-static {p5}, Lcom/peel/util/dw;->a(I)I

    move-result v2

    iput v2, v1, Landroid/widget/LinearLayout$LayoutParams;->bottomMargin:I

    .line 645
    :cond_3
    iput v7, v1, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    .line 646
    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 648
    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v1, v5, v5}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 649
    sget-boolean v2, Lcom/peel/util/dw;->b:Z

    if-eqz v2, :cond_4

    .line 650
    invoke-virtual {v1, v3, v6, v3, v6}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    .line 651
    invoke-static {v4}, Lcom/peel/util/dw;->a(I)I

    move-result v2

    iput v2, v1, Landroid/widget/LinearLayout$LayoutParams;->width:I

    .line 652
    invoke-static {v4}, Lcom/peel/util/dw;->a(I)I

    move-result v2

    iput v2, v1, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 658
    :goto_0
    new-instance v2, Landroid/widget/LinearLayout;

    invoke-direct {v2, p0}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 659
    new-instance v3, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v3, v5, v5}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 660
    invoke-virtual {v2, v6}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 661
    invoke-virtual {v2, v7}, Landroid/widget/LinearLayout;->setGravity(I)V

    .line 662
    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 665
    new-instance v3, Landroid/widget/ImageButton;

    invoke-direct {v3, p0}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    .line 666
    invoke-virtual {v3, p6}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 667
    const-string/jumbo v4, "Rewind"

    invoke-virtual {v3, v4}, Landroid/widget/ImageButton;->setTag(Ljava/lang/Object;)V

    .line 668
    sget-boolean v4, Lcom/peel/util/dw;->b:Z

    if-eqz v4, :cond_5

    .line 669
    sget v4, Lcom/peel/ui/fo;->remote_icon_rewind:I

    invoke-virtual {v3, v4}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 670
    sget v4, Lcom/peel/ui/fo;->common_btn_stateful:I

    invoke-virtual {v3, v4}, Landroid/widget/ImageButton;->setBackgroundResource(I)V

    .line 674
    :goto_1
    sget v4, Lcom/peel/ui/ft;->button_rewind:I

    invoke-virtual {p0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 676
    invoke-virtual {v3, v1}, Landroid/widget/ImageButton;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 678
    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 680
    new-instance v3, Landroid/widget/ImageButton;

    invoke-direct {v3, p0}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    .line 681
    invoke-virtual {v3, p6}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 682
    const-string/jumbo v4, "Play"

    invoke-virtual {v3, v4}, Landroid/widget/ImageButton;->setTag(Ljava/lang/Object;)V

    .line 683
    sget-boolean v4, Lcom/peel/util/dw;->b:Z

    if-eqz v4, :cond_6

    .line 684
    sget v4, Lcom/peel/ui/fo;->remote_icon_play:I

    invoke-virtual {v3, v4}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 685
    sget v4, Lcom/peel/ui/fo;->common_btn_stateful:I

    invoke-virtual {v3, v4}, Landroid/widget/ImageButton;->setBackgroundResource(I)V

    .line 689
    :goto_2
    sget v4, Lcom/peel/ui/ft;->button_play:I

    invoke-virtual {p0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 691
    invoke-virtual {v3, v1}, Landroid/widget/ImageButton;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 693
    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 695
    new-instance v3, Landroid/widget/ImageButton;

    invoke-direct {v3, p0}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    .line 696
    invoke-virtual {v3, p6}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 697
    const-string/jumbo v4, "Pause"

    invoke-virtual {v3, v4}, Landroid/widget/ImageButton;->setTag(Ljava/lang/Object;)V

    .line 698
    sget-boolean v4, Lcom/peel/util/dw;->b:Z

    if-eqz v4, :cond_7

    .line 699
    sget v4, Lcom/peel/ui/fo;->remote_icon_pause:I

    invoke-virtual {v3, v4}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 700
    sget v4, Lcom/peel/ui/fo;->common_btn_stateful:I

    invoke-virtual {v3, v4}, Landroid/widget/ImageButton;->setBackgroundResource(I)V

    .line 704
    :goto_3
    sget v4, Lcom/peel/ui/ft;->button_pause:I

    invoke-virtual {p0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 706
    invoke-virtual {v3, v1}, Landroid/widget/ImageButton;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 708
    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 710
    new-instance v3, Landroid/widget/ImageButton;

    invoke-direct {v3, p0}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    .line 711
    invoke-virtual {v3, p6}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 712
    const-string/jumbo v4, "Fast_Forward"

    invoke-virtual {v3, v4}, Landroid/widget/ImageButton;->setTag(Ljava/lang/Object;)V

    .line 713
    sget-boolean v4, Lcom/peel/util/dw;->b:Z

    if-eqz v4, :cond_8

    .line 714
    sget v4, Lcom/peel/ui/fo;->remote_icon_forward:I

    invoke-virtual {v3, v4}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 715
    sget v4, Lcom/peel/ui/fo;->common_btn_stateful:I

    invoke-virtual {v3, v4}, Landroid/widget/ImageButton;->setBackgroundResource(I)V

    .line 719
    :goto_4
    sget v4, Lcom/peel/ui/ft;->button_fast_forward:I

    invoke-virtual {p0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 721
    invoke-virtual {v3, v1}, Landroid/widget/ImageButton;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 723
    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 725
    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 727
    new-instance v2, Landroid/widget/LinearLayout;

    invoke-direct {v2, p0}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 728
    new-instance v3, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v3, v5, v5}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 729
    invoke-virtual {v2, v6}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 730
    invoke-virtual {v2, v7}, Landroid/widget/LinearLayout;->setGravity(I)V

    .line 731
    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 734
    new-instance v3, Landroid/widget/ImageButton;

    invoke-direct {v3, p0}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    .line 735
    invoke-virtual {v3, p6}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 736
    const-string/jumbo v4, "Previous"

    invoke-virtual {v3, v4}, Landroid/widget/ImageButton;->setTag(Ljava/lang/Object;)V

    .line 737
    sget-boolean v4, Lcom/peel/util/dw;->b:Z

    if-eqz v4, :cond_9

    .line 738
    sget v4, Lcom/peel/ui/fo;->remote_icon_pre:I

    invoke-virtual {v3, v4}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 739
    sget v4, Lcom/peel/ui/fo;->common_btn_stateful:I

    invoke-virtual {v3, v4}, Landroid/widget/ImageButton;->setBackgroundResource(I)V

    .line 743
    :goto_5
    sget v4, Lcom/peel/ui/ft;->button_previous:I

    invoke-virtual {p0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 745
    invoke-virtual {v3, v1}, Landroid/widget/ImageButton;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 747
    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 749
    new-instance v3, Landroid/widget/ImageButton;

    invoke-direct {v3, p0}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    .line 750
    invoke-virtual {v3, p6}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 751
    const-string/jumbo v4, "Record"

    invoke-virtual {v3, v4}, Landroid/widget/ImageButton;->setTag(Ljava/lang/Object;)V

    .line 752
    sget-boolean v4, Lcom/peel/util/dw;->b:Z

    if-eqz v4, :cond_a

    .line 753
    sget v4, Lcom/peel/ui/fo;->remote_icon_record:I

    invoke-virtual {v3, v4}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 754
    sget v4, Lcom/peel/ui/fo;->common_btn_stateful:I

    invoke-virtual {v3, v4}, Landroid/widget/ImageButton;->setBackgroundResource(I)V

    .line 758
    :goto_6
    sget v4, Lcom/peel/ui/ft;->button_record:I

    invoke-virtual {p0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 759
    invoke-virtual {v3, v1}, Landroid/widget/ImageButton;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 761
    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 763
    new-instance v3, Landroid/widget/ImageButton;

    invoke-direct {v3, p0}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    .line 764
    invoke-virtual {v3, p6}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 765
    const-string/jumbo v4, "Stop"

    invoke-virtual {v3, v4}, Landroid/widget/ImageButton;->setTag(Ljava/lang/Object;)V

    .line 766
    sget-boolean v4, Lcom/peel/util/dw;->b:Z

    if-eqz v4, :cond_b

    .line 767
    sget v4, Lcom/peel/ui/fo;->remote_icon_stop:I

    invoke-virtual {v3, v4}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 768
    sget v4, Lcom/peel/ui/fo;->common_btn_stateful:I

    invoke-virtual {v3, v4}, Landroid/widget/ImageButton;->setBackgroundResource(I)V

    .line 772
    :goto_7
    sget v4, Lcom/peel/ui/ft;->button_stop:I

    invoke-virtual {p0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 773
    invoke-virtual {v3, v1}, Landroid/widget/ImageButton;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 775
    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 777
    new-instance v3, Landroid/widget/ImageButton;

    invoke-direct {v3, p0}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    .line 778
    invoke-virtual {v3, p6}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 779
    const-string/jumbo v4, "Next"

    invoke-virtual {v3, v4}, Landroid/widget/ImageButton;->setTag(Ljava/lang/Object;)V

    .line 780
    sget-boolean v4, Lcom/peel/util/dw;->b:Z

    if-eqz v4, :cond_c

    .line 781
    sget v4, Lcom/peel/ui/fo;->remote_icon_next:I

    invoke-virtual {v3, v4}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 782
    sget v4, Lcom/peel/ui/fo;->common_btn_stateful:I

    invoke-virtual {v3, v4}, Landroid/widget/ImageButton;->setBackgroundResource(I)V

    .line 786
    :goto_8
    sget v4, Lcom/peel/ui/ft;->button_next:I

    invoke-virtual {p0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 788
    invoke-virtual {v3, v1}, Landroid/widget/ImageButton;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 790
    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 792
    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 794
    return-object v0

    .line 654
    :cond_4
    const/16 v2, 0x52

    invoke-static {v2}, Lcom/peel/util/dw;->a(I)I

    move-result v2

    iput v2, v1, Landroid/widget/LinearLayout$LayoutParams;->width:I

    .line 655
    const/16 v2, 0x2e

    invoke-static {v2}, Lcom/peel/util/dw;->a(I)I

    move-result v2

    iput v2, v1, Landroid/widget/LinearLayout$LayoutParams;->height:I

    goto/16 :goto_0

    .line 672
    :cond_5
    sget v4, Lcom/peel/ui/fo;->button_keypad_2_rewind_stateful:I

    invoke-virtual {v3, v4}, Landroid/widget/ImageButton;->setBackgroundResource(I)V

    goto/16 :goto_1

    .line 687
    :cond_6
    sget v4, Lcom/peel/ui/fo;->button_keypad_2_play_stateful:I

    invoke-virtual {v3, v4}, Landroid/widget/ImageButton;->setBackgroundResource(I)V

    goto/16 :goto_2

    .line 702
    :cond_7
    sget v4, Lcom/peel/ui/fo;->button_keypad_2_pause_stateful:I

    invoke-virtual {v3, v4}, Landroid/widget/ImageButton;->setBackgroundResource(I)V

    goto/16 :goto_3

    .line 717
    :cond_8
    sget v4, Lcom/peel/ui/fo;->button_keypad_2_ff_stateful:I

    invoke-virtual {v3, v4}, Landroid/widget/ImageButton;->setBackgroundResource(I)V

    goto/16 :goto_4

    .line 741
    :cond_9
    sget v4, Lcom/peel/ui/fo;->button_keypad_4_prev_stateful:I

    invoke-virtual {v3, v4}, Landroid/widget/ImageButton;->setBackgroundResource(I)V

    goto/16 :goto_5

    .line 756
    :cond_a
    sget v4, Lcom/peel/ui/fo;->button_keypad_4_rec_stateful:I

    invoke-virtual {v3, v4}, Landroid/widget/ImageButton;->setBackgroundResource(I)V

    goto/16 :goto_6

    .line 770
    :cond_b
    sget v4, Lcom/peel/ui/fo;->button_keypad_4_stop_stateful:I

    invoke-virtual {v3, v4}, Landroid/widget/ImageButton;->setBackgroundResource(I)V

    goto :goto_7

    .line 784
    :cond_c
    sget v4, Lcom/peel/ui/fo;->button_keypad_4_next_stateful:I

    invoke-virtual {v3, v4}, Landroid/widget/ImageButton;->setBackgroundResource(I)V

    goto :goto_8
.end method

.method public static a(Landroid/content/Context;IIIILjava/lang/String;Landroid/view/View$OnClickListener;)Landroid/view/View;
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, -0x2

    .line 359
    new-instance v0, Landroid/widget/Button;

    invoke-direct {v0, p0}, Landroid/widget/Button;-><init>(Landroid/content/Context;)V

    .line 360
    invoke-virtual {v0, p5}, Landroid/widget/Button;->setTag(Ljava/lang/Object;)V

    .line 361
    invoke-virtual {v0, p1}, Landroid/widget/Button;->setId(I)V

    .line 362
    invoke-virtual {v0, p6}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 364
    new-instance v1, Landroid/graphics/drawable/StateListDrawable;

    invoke-direct {v1}, Landroid/graphics/drawable/StateListDrawable;-><init>()V

    .line 365
    const/4 v2, 0x1

    new-array v2, v2, [I

    const v3, 0x10100a7

    aput v3, v2, v5

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, p3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 366
    new-array v2, v5, [I

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, p2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 367
    invoke-virtual {v0, v1}, Landroid/widget/Button;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 369
    invoke-virtual {p0, p4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 370
    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v1, v4, v4}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 371
    const/16 v2, 0x4a

    invoke-static {v2}, Lcom/peel/util/dw;->a(I)I

    move-result v2

    iput v2, v1, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    .line 372
    const/16 v2, 0x4c

    invoke-static {v2}, Lcom/peel/util/dw;->a(I)I

    move-result v2

    iput v2, v1, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    .line 373
    const/16 v2, 0x50

    invoke-static {v2}, Lcom/peel/util/dw;->a(I)I

    move-result v2

    iput v2, v1, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 375
    const/16 v2, 0xe

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 377
    invoke-virtual {v0, v1}, Landroid/widget/Button;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 379
    return-object v0
.end method

.method public static a(Landroid/content/Context;ILandroid/view/View$OnClickListener;Landroid/view/ViewGroup$LayoutParams;)Landroid/view/View;
    .locals 11

    .prologue
    const/16 v10, 0x50

    const/16 v9, 0x4f

    const/16 v8, 0x37

    const/16 v7, 0x35

    .line 799
    new-instance v0, Landroid/widget/AbsoluteLayout;

    invoke-direct {v0, p0}, Landroid/widget/AbsoluteLayout;-><init>(Landroid/content/Context;)V

    .line 801
    invoke-virtual {v0, p3}, Landroid/widget/AbsoluteLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 802
    invoke-virtual {v0, p1}, Landroid/widget/AbsoluteLayout;->setId(I)V

    .line 803
    sget v1, Lcom/peel/ui/fo;->btn_ok_bg:I

    invoke-virtual {v0, v1}, Landroid/widget/AbsoluteLayout;->setBackgroundResource(I)V

    .line 806
    new-instance v1, Landroid/widget/ImageButton;

    invoke-direct {v1, p0}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    .line 807
    const-string/jumbo v2, "Select"

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setTag(Ljava/lang/Object;)V

    .line 808
    invoke-virtual {v1, p2}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 809
    sget v2, Lcom/peel/ui/fo;->button_ok_stateful:I

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setBackgroundResource(I)V

    .line 810
    sget v2, Lcom/peel/ui/ft;->ok:I

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 812
    new-instance v2, Landroid/widget/AbsoluteLayout$LayoutParams;

    const/16 v3, 0x46

    invoke-static {v3}, Lcom/peel/util/dw;->a(I)I

    move-result v3

    const/16 v4, 0x49

    invoke-static {v4}, Lcom/peel/util/dw;->a(I)I

    move-result v4

    const/16 v5, 0x47

    invoke-static {v5}, Lcom/peel/util/dw;->a(I)I

    move-result v5

    const/16 v6, 0x45

    invoke-static {v6}, Lcom/peel/util/dw;->a(I)I

    move-result v6

    invoke-direct {v2, v3, v4, v5, v6}, Landroid/widget/AbsoluteLayout$LayoutParams;-><init>(IIII)V

    .line 813
    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 814
    invoke-virtual {v0, v1}, Landroid/widget/AbsoluteLayout;->addView(Landroid/view/View;)V

    .line 816
    new-instance v1, Landroid/widget/ImageButton;

    invoke-direct {v1, p0}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    .line 817
    const-string/jumbo v2, "Navigate_Left"

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setTag(Ljava/lang/Object;)V

    .line 818
    invoke-virtual {v1, p2}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 819
    sget v2, Lcom/peel/ui/fo;->button_left_stateful:I

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setBackgroundResource(I)V

    .line 820
    sget v2, Lcom/peel/ui/ft;->button_navigate_left:I

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 822
    new-instance v2, Landroid/widget/AbsoluteLayout$LayoutParams;

    invoke-static {v7}, Lcom/peel/util/dw;->a(I)I

    move-result v3

    invoke-static {v8}, Lcom/peel/util/dw;->a(I)I

    move-result v4

    const/16 v5, 0x9

    invoke-static {v5}, Lcom/peel/util/dw;->a(I)I

    move-result v5

    invoke-static {v10}, Lcom/peel/util/dw;->a(I)I

    move-result v6

    invoke-direct {v2, v3, v4, v5, v6}, Landroid/widget/AbsoluteLayout$LayoutParams;-><init>(IIII)V

    .line 823
    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 824
    invoke-virtual {v0, v1}, Landroid/widget/AbsoluteLayout;->addView(Landroid/view/View;)V

    .line 826
    new-instance v1, Landroid/widget/ImageButton;

    invoke-direct {v1, p0}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    .line 827
    const-string/jumbo v2, "Navigate_Up"

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setTag(Ljava/lang/Object;)V

    .line 828
    invoke-virtual {v1, p2}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 830
    sget v2, Lcom/peel/ui/fo;->button_up_stateful:I

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setBackgroundResource(I)V

    .line 831
    sget v2, Lcom/peel/ui/ft;->button_navigate_up:I

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 833
    new-instance v2, Landroid/widget/AbsoluteLayout$LayoutParams;

    invoke-static {v7}, Lcom/peel/util/dw;->a(I)I

    move-result v3

    invoke-static {v8}, Lcom/peel/util/dw;->a(I)I

    move-result v4

    invoke-static {v9}, Lcom/peel/util/dw;->a(I)I

    move-result v5

    const/16 v6, 0xa

    invoke-static {v6}, Lcom/peel/util/dw;->a(I)I

    move-result v6

    invoke-direct {v2, v3, v4, v5, v6}, Landroid/widget/AbsoluteLayout$LayoutParams;-><init>(IIII)V

    .line 834
    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 835
    invoke-virtual {v0, v1}, Landroid/widget/AbsoluteLayout;->addView(Landroid/view/View;)V

    .line 837
    new-instance v1, Landroid/widget/ImageButton;

    invoke-direct {v1, p0}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    .line 838
    const-string/jumbo v2, "Navigate_Right"

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setTag(Ljava/lang/Object;)V

    .line 839
    invoke-virtual {v1, p2}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 841
    sget v2, Lcom/peel/ui/fo;->button_right_stateful:I

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setBackgroundResource(I)V

    .line 842
    sget v2, Lcom/peel/ui/ft;->button_navigate_right:I

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 844
    new-instance v2, Landroid/widget/AbsoluteLayout$LayoutParams;

    invoke-static {v7}, Lcom/peel/util/dw;->a(I)I

    move-result v3

    invoke-static {v8}, Lcom/peel/util/dw;->a(I)I

    move-result v4

    const/16 v5, 0x97

    invoke-static {v5}, Lcom/peel/util/dw;->a(I)I

    move-result v5

    invoke-static {v10}, Lcom/peel/util/dw;->a(I)I

    move-result v6

    invoke-direct {v2, v3, v4, v5, v6}, Landroid/widget/AbsoluteLayout$LayoutParams;-><init>(IIII)V

    .line 845
    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 846
    invoke-virtual {v0, v1}, Landroid/widget/AbsoluteLayout;->addView(Landroid/view/View;)V

    .line 848
    new-instance v1, Landroid/widget/ImageButton;

    invoke-direct {v1, p0}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    .line 849
    const-string/jumbo v2, "Navigate_Down"

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setTag(Ljava/lang/Object;)V

    .line 850
    invoke-virtual {v1, p2}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 852
    sget v2, Lcom/peel/ui/fo;->button_down_stateful:I

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setBackgroundResource(I)V

    .line 853
    sget v2, Lcom/peel/ui/ft;->button_navigate_down:I

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 855
    new-instance v2, Landroid/widget/AbsoluteLayout$LayoutParams;

    invoke-static {v7}, Lcom/peel/util/dw;->a(I)I

    move-result v3

    invoke-static {v8}, Lcom/peel/util/dw;->a(I)I

    move-result v4

    invoke-static {v9}, Lcom/peel/util/dw;->a(I)I

    move-result v5

    const/16 v6, 0x94

    invoke-static {v6}, Lcom/peel/util/dw;->a(I)I

    move-result v6

    invoke-direct {v2, v3, v4, v5, v6}, Landroid/widget/AbsoluteLayout$LayoutParams;-><init>(IIII)V

    .line 856
    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 857
    invoke-virtual {v0, v1}, Landroid/widget/AbsoluteLayout;->addView(Landroid/view/View;)V

    .line 859
    return-object v0
.end method

.method public static a(Landroid/content/Context;Landroid/view/View$OnClickListener;)Landroid/view/View;
    .locals 11

    .prologue
    const/4 v1, -0x2

    const/4 v10, 0x6

    const/4 v9, 0x0

    .line 1368
    new-instance v7, Landroid/widget/LinearLayout;

    invoke-direct {v7, p0}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 1369
    invoke-virtual {v7, v9}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 1370
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v0, v1, v1}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 1371
    const/16 v1, 0x8

    invoke-static {v1}, Lcom/peel/util/dw;->a(I)I

    move-result v1

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->topMargin:I

    .line 1372
    const/4 v1, 0x1

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    .line 1373
    invoke-virtual {v7, v0}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1375
    const v1, 0x30d4c

    sget v2, Lcom/peel/ui/fo;->btn_a_rewind:I

    sget v3, Lcom/peel/ui/fo;->btn_a_rewind_press:I

    sget v4, Lcom/peel/ui/ft;->button_rewind:I

    const-string/jumbo v5, "Rewind"

    move-object v0, p0

    move-object v6, p1

    invoke-static/range {v0 .. v6}, Lcom/peel/util/dw;->b(Landroid/content/Context;IIIILjava/lang/String;Landroid/view/View$OnClickListener;)Landroid/view/View;

    move-result-object v8

    .line 1378
    invoke-virtual {v8}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 1379
    invoke-virtual {v0, v10, v9, v10, v9}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    .line 1380
    invoke-virtual {v8, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1381
    invoke-virtual {v7, v8}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1383
    const v1, 0x30d4a

    sget v2, Lcom/peel/ui/fo;->btn_b_play_pause:I

    sget v3, Lcom/peel/ui/fo;->btn_b_play_pause_press:I

    sget v4, Lcom/peel/ui/ft;->button_play_pause:I

    const-string/jumbo v5, "Play|Pause"

    move-object v0, p0

    move-object v6, p1

    invoke-static/range {v0 .. v6}, Lcom/peel/util/dw;->b(Landroid/content/Context;IIIILjava/lang/String;Landroid/view/View$OnClickListener;)Landroid/view/View;

    move-result-object v1

    .line 1386
    invoke-virtual {v8}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 1387
    invoke-virtual {v0, v10, v9, v10, v9}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    .line 1388
    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1389
    invoke-virtual {v7, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1391
    const v1, 0x30d79

    sget v2, Lcom/peel/ui/fo;->btn_a_ff:I

    sget v3, Lcom/peel/ui/fo;->btn_a_ff_press:I

    sget v4, Lcom/peel/ui/ft;->button_fast_forward:I

    const-string/jumbo v5, "Fast_Forward"

    move-object v0, p0

    move-object v6, p1

    invoke-static/range {v0 .. v6}, Lcom/peel/util/dw;->b(Landroid/content/Context;IIIILjava/lang/String;Landroid/view/View$OnClickListener;)Landroid/view/View;

    move-result-object v1

    .line 1394
    invoke-virtual {v8}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 1395
    invoke-virtual {v0, v10, v9, v10, v9}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    .line 1396
    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1397
    invoke-virtual {v7, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1399
    return-object v7
.end method

.method private static a(Landroid/content/Context;Ljava/lang/String;IIILandroid/view/View$OnClickListener;)Landroid/view/View;
    .locals 9

    .prologue
    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v4, 0x0

    const/4 v3, -0x1

    .line 1294
    sget-object v0, Lcom/peel/util/dw;->d:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [I

    .line 1295
    if-eqz v0, :cond_3

    .line 1296
    const-string/jumbo v1, "Play"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    const-string/jumbo v5, "Play|Pause"

    .line 1298
    :goto_0
    if-le p2, v3, :cond_0

    move v1, p2

    :goto_1
    if-le p3, v3, :cond_1

    move v2, p3

    :goto_2
    if-le p4, v3, :cond_2

    move v3, p4

    :goto_3
    aget v4, v0, v8

    move-object v0, p0

    move-object v6, p5

    invoke-static/range {v0 .. v6}, Lcom/peel/util/dw;->c(Landroid/content/Context;IIIILjava/lang/String;Landroid/view/View$OnClickListener;)Landroid/view/View;

    move-result-object v0

    .line 1307
    :goto_4
    return-object v0

    .line 1298
    :cond_0
    aget v1, v0, v4

    goto :goto_1

    :cond_1
    aget v2, v0, v6

    goto :goto_2

    :cond_2
    aget v3, v0, v7

    goto :goto_3

    .line 1305
    :cond_3
    sget-object v0, Lcom/peel/util/dw;->c:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "missing build button information for command: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1306
    sget-object v0, Lcom/peel/util/dw;->d:Ljava/util/Map;

    const-string/jumbo v1, "default"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [I

    .line 1307
    aget v1, v0, v4

    aget v2, v0, v6

    aget v3, v0, v7

    aget v4, v0, v8

    move-object v0, p0

    move-object v5, p1

    move-object v6, p5

    invoke-static/range {v0 .. v6}, Lcom/peel/util/dw;->c(Landroid/content/Context;IIIILjava/lang/String;Landroid/view/View$OnClickListener;)Landroid/view/View;

    move-result-object v0

    goto :goto_4

    :cond_4
    move-object v5, p1

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Landroid/view/View$OnClickListener;)Landroid/view/View;
    .locals 11

    .prologue
    const/16 v10, 0x44

    const/4 v9, 0x2

    const/4 v8, -0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 322
    new-instance v1, Landroid/widget/Button;

    invoke-direct {v1, p0}, Landroid/widget/Button;-><init>(Landroid/content/Context;)V

    .line 323
    invoke-virtual {v1, p1}, Landroid/widget/Button;->setTag(Ljava/lang/Object;)V

    .line 324
    invoke-virtual {v1, p2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 326
    sget-object v0, Lcom/peel/util/dw;->d:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [I

    .line 328
    sget-boolean v2, Lcom/peel/util/dw;->b:Z

    if-eqz v2, :cond_0

    .line 329
    const/4 v2, 0x3

    aget v2, v0, v2

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 330
    sget v2, Lcom/peel/ui/fo;->common_btn_stateful:I

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setBackgroundResource(I)V

    .line 338
    :goto_0
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/peel/ui/fm;->remote_ctrl_pad_text:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setTextColor(I)V

    .line 339
    const/high16 v2, 0x41700000    # 15.0f

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setTextSize(F)V

    .line 341
    aget v0, v0, v9

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 342
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v0, v8, v8}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 343
    sget-boolean v2, Lcom/peel/util/dw;->b:Z

    if-nez v2, :cond_1

    .line 344
    const/16 v2, 0x6a

    invoke-static {v2}, Lcom/peel/util/dw;->a(I)I

    move-result v2

    iput v2, v0, Landroid/widget/LinearLayout$LayoutParams;->width:I

    .line 345
    const/16 v2, 0x38

    invoke-static {v2}, Lcom/peel/util/dw;->a(I)I

    move-result v2

    iput v2, v0, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 350
    :goto_1
    iput v7, v0, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    .line 351
    invoke-virtual {v1, v0}, Landroid/widget/Button;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 353
    return-object v1

    .line 332
    :cond_0
    aget v2, v0, v9

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 333
    new-instance v2, Landroid/graphics/drawable/StateListDrawable;

    invoke-direct {v2}, Landroid/graphics/drawable/StateListDrawable;-><init>()V

    .line 334
    new-array v3, v7, [I

    const v4, 0x10100a7

    aput v4, v3, v6

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    aget v5, v0, v7

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 335
    new-array v3, v6, [I

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    aget v5, v0, v6

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 336
    invoke-virtual {v1, v2}, Landroid/widget/Button;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 347
    :cond_1
    invoke-static {v10}, Lcom/peel/util/dw;->a(I)I

    move-result v2

    iput v2, v0, Landroid/widget/LinearLayout$LayoutParams;->width:I

    .line 348
    invoke-static {v10}, Lcom/peel/util/dw;->a(I)I

    move-result v2

    iput v2, v0, Landroid/widget/LinearLayout$LayoutParams;->height:I

    goto :goto_1
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Landroid/view/View$OnClickListener;)Landroid/view/View;
    .locals 11

    .prologue
    const/16 v10, 0x44

    const/4 v9, 0x6

    const/4 v8, 0x1

    const/4 v7, -0x2

    const/4 v6, 0x0

    .line 1314
    sget-object v0, Lcom/peel/util/dw;->d:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [I

    .line 1316
    new-instance v1, Landroid/widget/Button;

    invoke-direct {v1, p0}, Landroid/widget/Button;-><init>(Landroid/content/Context;)V

    .line 1317
    invoke-virtual {v1, p1}, Landroid/widget/Button;->setTag(Ljava/lang/Object;)V

    .line 1318
    aget v2, v0, v6

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setId(I)V

    .line 1319
    invoke-virtual {v1, p3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1321
    new-instance v2, Landroid/graphics/drawable/StateListDrawable;

    invoke-direct {v2}, Landroid/graphics/drawable/StateListDrawable;-><init>()V

    .line 1322
    new-array v3, v8, [I

    const v4, 0x10100a7

    aput v4, v3, v6

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const/4 v5, 0x2

    aget v5, v0, v5

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 1323
    new-array v3, v6, [I

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    aget v0, v0, v8

    invoke-virtual {v4, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v2, v3, v0}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 1324
    invoke-virtual {v1, v2}, Landroid/widget/Button;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 1326
    invoke-virtual {v1, p1}, Landroid/widget/Button;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1327
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v0, v7, v7}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 1328
    sget-boolean v2, Lcom/peel/util/dw;->b:Z

    if-eqz v2, :cond_1

    .line 1329
    invoke-virtual {v0, v9, v6, v9, v6}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    .line 1330
    invoke-static {v10}, Lcom/peel/util/dw;->a(I)I

    move-result v2

    iput v2, v0, Landroid/widget/LinearLayout$LayoutParams;->width:I

    .line 1331
    invoke-static {v10}, Lcom/peel/util/dw;->a(I)I

    move-result v2

    iput v2, v0, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 1336
    :goto_0
    const/16 v2, 0x11

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setGravity(I)V

    .line 1338
    if-eqz p2, :cond_0

    .line 1339
    invoke-virtual {v1, p2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 1340
    const/high16 v2, -0x1000000

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setTextColor(I)V

    .line 1343
    :cond_0
    invoke-virtual {v1, v0}, Landroid/widget/Button;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1345
    return-object v1

    .line 1333
    :cond_1
    const/16 v2, 0x51

    invoke-static {v2}, Lcom/peel/util/dw;->a(I)I

    move-result v2

    iput v2, v0, Landroid/widget/LinearLayout$LayoutParams;->width:I

    .line 1334
    const/16 v2, 0x30

    invoke-static {v2}, Lcom/peel/util/dw;->a(I)I

    move-result v2

    iput v2, v0, Landroid/widget/LinearLayout$LayoutParams;->height:I

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Ljava/util/List;IIIILandroid/view/View$OnClickListener;)Landroid/view/View;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;IIII",
            "Landroid/view/View$OnClickListener;",
            ")",
            "Landroid/view/View;"
        }
    .end annotation

    .prologue
    .line 485
    new-instance v2, Landroid/widget/RelativeLayout;

    invoke-direct {v2, p0}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 486
    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v0, -0x2

    const/4 v3, -0x2

    invoke-direct {v1, v0, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 488
    if-lez p2, :cond_0

    invoke-static {p2}, Lcom/peel/util/dw;->a(I)I

    move-result v0

    iput v0, v1, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 489
    :cond_0
    if-lez p3, :cond_1

    invoke-static {p3}, Lcom/peel/util/dw;->a(I)I

    move-result v0

    iput v0, v1, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    .line 490
    :cond_1
    if-lez p4, :cond_2

    invoke-static {p4}, Lcom/peel/util/dw;->a(I)I

    move-result v0

    iput v0, v1, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 491
    :cond_2
    if-lez p5, :cond_3

    invoke-static {p5}, Lcom/peel/util/dw;->a(I)I

    move-result v0

    iput v0, v1, Landroid/widget/RelativeLayout$LayoutParams;->bottomMargin:I

    .line 493
    :cond_3
    if-eqz p1, :cond_6

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_6

    .line 494
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_4
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 495
    const-string/jumbo v4, ","

    invoke-virtual {v0, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 496
    array-length v4, v0

    const/4 v5, 0x1

    if-ne v4, v5, :cond_5

    .line 497
    const/4 v4, 0x0

    aget-object v0, v0, v4

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    goto :goto_0

    .line 498
    :cond_5
    array-length v4, v0

    const/4 v5, 0x2

    if-ne v4, v5, :cond_4

    .line 499
    const/4 v4, 0x0

    aget-object v4, v0, v4

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    const/4 v5, 0x1

    aget-object v0, v0, v5

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {v1, v4, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    goto :goto_0

    .line 504
    :cond_6
    invoke-virtual {v2, v1}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 506
    const/16 v1, 0x36

    const/16 v0, 0x35

    .line 507
    sget-boolean v3, Lcom/peel/util/dw;->b:Z

    if-eqz v3, :cond_7

    .line 508
    const/16 v1, 0x44

    .line 509
    const/16 v0, 0x44

    .line 512
    :cond_7
    new-instance v3, Landroid/widget/ImageButton;

    invoke-direct {v3, p0}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    .line 513
    invoke-virtual {v3, p6}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 514
    const-string/jumbo v4, "Play|Pause"

    invoke-virtual {v3, v4}, Landroid/widget/ImageButton;->setTag(Ljava/lang/Object;)V

    .line 515
    const v4, 0x30d4a

    invoke-virtual {v3, v4}, Landroid/widget/ImageButton;->setId(I)V

    .line 516
    sget-boolean v4, Lcom/peel/util/dw;->b:Z

    if-eqz v4, :cond_8

    .line 517
    sget v4, Lcom/peel/ui/fo;->common_btn_stateful:I

    invoke-virtual {v3, v4}, Landroid/widget/ImageButton;->setBackgroundResource(I)V

    .line 518
    sget v4, Lcom/peel/ui/fo;->remote_icon_play_pause:I

    invoke-virtual {v3, v4}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 522
    :goto_1
    sget v4, Lcom/peel/ui/ft;->button_play_pause:I

    invoke-virtual {p0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 523
    new-instance v4, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v5, -0x2

    const/4 v6, -0x2

    invoke-direct {v4, v5, v6}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 524
    invoke-static {v1}, Lcom/peel/util/dw;->a(I)I

    move-result v5

    iput v5, v4, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    .line 525
    invoke-static {v0}, Lcom/peel/util/dw;->a(I)I

    move-result v5

    iput v5, v4, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    .line 526
    const/16 v5, 0x13

    invoke-static {v5}, Lcom/peel/util/dw;->a(I)I

    move-result v5

    iput v5, v4, Landroid/widget/RelativeLayout$LayoutParams;->bottomMargin:I

    .line 527
    const/16 v5, 0x9

    invoke-virtual {v4, v5}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 528
    const/16 v5, 0xa

    invoke-virtual {v4, v5}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 529
    invoke-virtual {v3, v4}, Landroid/widget/ImageButton;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 530
    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 532
    new-instance v4, Landroid/widget/ImageButton;

    invoke-direct {v4, p0}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    .line 533
    invoke-virtual {v4, p6}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 534
    const-string/jumbo v5, "Stop"

    invoke-virtual {v4, v5}, Landroid/widget/ImageButton;->setTag(Ljava/lang/Object;)V

    .line 535
    const v5, 0x30d4b

    invoke-virtual {v4, v5}, Landroid/widget/ImageButton;->setId(I)V

    .line 536
    sget-boolean v5, Lcom/peel/util/dw;->b:Z

    if-eqz v5, :cond_9

    .line 537
    sget v5, Lcom/peel/ui/fo;->common_btn_stateful:I

    invoke-virtual {v4, v5}, Landroid/widget/ImageButton;->setBackgroundResource(I)V

    .line 538
    sget v5, Lcom/peel/ui/fo;->remote_icon_stop:I

    invoke-virtual {v4, v5}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 542
    :goto_2
    sget v5, Lcom/peel/ui/ft;->button_stop:I

    invoke-virtual {p0, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 543
    new-instance v5, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v6, -0x2

    const/4 v7, -0x2

    invoke-direct {v5, v6, v7}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 544
    invoke-static {v1}, Lcom/peel/util/dw;->a(I)I

    move-result v6

    iput v6, v5, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    .line 545
    invoke-static {v0}, Lcom/peel/util/dw;->a(I)I

    move-result v6

    iput v6, v5, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    .line 546
    const/16 v6, 0x13

    invoke-static {v6}, Lcom/peel/util/dw;->a(I)I

    move-result v6

    iput v6, v5, Landroid/widget/RelativeLayout$LayoutParams;->bottomMargin:I

    .line 547
    const/4 v6, 0x1

    invoke-virtual {v3}, Landroid/widget/ImageButton;->getId()I

    move-result v7

    invoke-virtual {v5, v6, v7}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 548
    const/4 v6, 0x6

    invoke-virtual {v3}, Landroid/widget/ImageButton;->getId()I

    move-result v7

    invoke-virtual {v5, v6, v7}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 549
    invoke-virtual {v4, v5}, Landroid/widget/ImageButton;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 550
    invoke-virtual {v2, v4}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 552
    new-instance v4, Landroid/widget/ImageButton;

    invoke-direct {v4, p0}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    .line 553
    invoke-virtual {v4, p6}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 554
    const-string/jumbo v5, "Rewind"

    invoke-virtual {v4, v5}, Landroid/widget/ImageButton;->setTag(Ljava/lang/Object;)V

    .line 555
    const v5, 0x30d4c

    invoke-virtual {v4, v5}, Landroid/widget/ImageButton;->setId(I)V

    .line 556
    sget-boolean v5, Lcom/peel/util/dw;->b:Z

    if-eqz v5, :cond_a

    .line 557
    sget v5, Lcom/peel/ui/fo;->common_btn_stateful:I

    invoke-virtual {v4, v5}, Landroid/widget/ImageButton;->setBackgroundResource(I)V

    .line 558
    sget v5, Lcom/peel/ui/fo;->remote_icon_rewind:I

    invoke-virtual {v4, v5}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 562
    :goto_3
    sget v5, Lcom/peel/ui/ft;->button_rewind:I

    invoke-virtual {p0, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 563
    new-instance v5, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v6, -0x2

    const/4 v7, -0x2

    invoke-direct {v5, v6, v7}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 564
    invoke-static {v1}, Lcom/peel/util/dw;->a(I)I

    move-result v6

    iput v6, v5, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    .line 565
    invoke-static {v0}, Lcom/peel/util/dw;->a(I)I

    move-result v6

    iput v6, v5, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    .line 566
    const/16 v6, 0x13

    invoke-static {v6}, Lcom/peel/util/dw;->a(I)I

    move-result v6

    iput v6, v5, Landroid/widget/RelativeLayout$LayoutParams;->bottomMargin:I

    .line 567
    const/16 v6, 0x9

    invoke-virtual {v5, v6}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 568
    const/4 v6, 0x3

    invoke-virtual {v3}, Landroid/widget/ImageButton;->getId()I

    move-result v3

    invoke-virtual {v5, v6, v3}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 569
    invoke-virtual {v4, v5}, Landroid/widget/ImageButton;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 570
    invoke-virtual {v2, v4}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 572
    new-instance v3, Landroid/widget/ImageButton;

    invoke-direct {v3, p0}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    .line 573
    invoke-virtual {v3, p6}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 574
    const-string/jumbo v5, "Fast_Forward"

    invoke-virtual {v3, v5}, Landroid/widget/ImageButton;->setTag(Ljava/lang/Object;)V

    .line 575
    const v5, 0x30d4d

    invoke-virtual {v3, v5}, Landroid/widget/ImageButton;->setId(I)V

    .line 576
    sget-boolean v5, Lcom/peel/util/dw;->b:Z

    if-eqz v5, :cond_b

    .line 577
    sget v5, Lcom/peel/ui/fo;->common_btn_stateful:I

    invoke-virtual {v3, v5}, Landroid/widget/ImageButton;->setBackgroundResource(I)V

    .line 578
    sget v5, Lcom/peel/ui/fo;->remote_icon_forward:I

    invoke-virtual {v3, v5}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 582
    :goto_4
    sget v5, Lcom/peel/ui/ft;->button_fast_forward:I

    invoke-virtual {p0, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 583
    new-instance v5, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v6, -0x2

    const/4 v7, -0x2

    invoke-direct {v5, v6, v7}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 584
    invoke-static {v1}, Lcom/peel/util/dw;->a(I)I

    move-result v6

    iput v6, v5, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    .line 585
    invoke-static {v0}, Lcom/peel/util/dw;->a(I)I

    move-result v6

    iput v6, v5, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    .line 586
    const/4 v6, 0x6

    invoke-virtual {v4}, Landroid/widget/ImageButton;->getId()I

    move-result v7

    invoke-virtual {v5, v6, v7}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 587
    const/4 v6, 0x1

    invoke-virtual {v4}, Landroid/widget/ImageButton;->getId()I

    move-result v7

    invoke-virtual {v5, v6, v7}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 588
    invoke-virtual {v3, v5}, Landroid/widget/ImageButton;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 589
    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 591
    new-instance v3, Landroid/widget/ImageButton;

    invoke-direct {v3, p0}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    .line 592
    invoke-virtual {v3, p6}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 593
    const-string/jumbo v5, "Previous"

    invoke-virtual {v3, v5}, Landroid/widget/ImageButton;->setTag(Ljava/lang/Object;)V

    .line 594
    const v5, 0x30d4e

    invoke-virtual {v3, v5}, Landroid/widget/ImageButton;->setId(I)V

    .line 595
    sget-boolean v5, Lcom/peel/util/dw;->b:Z

    if-eqz v5, :cond_c

    .line 596
    sget v5, Lcom/peel/ui/fo;->common_btn_stateful:I

    invoke-virtual {v3, v5}, Landroid/widget/ImageButton;->setBackgroundResource(I)V

    .line 597
    sget v5, Lcom/peel/ui/fo;->remote_icon_pre:I

    invoke-virtual {v3, v5}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 601
    :goto_5
    sget v5, Lcom/peel/ui/ft;->button_previous:I

    invoke-virtual {p0, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 602
    new-instance v5, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v6, -0x2

    const/4 v7, -0x2

    invoke-direct {v5, v6, v7}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 603
    invoke-static {v1}, Lcom/peel/util/dw;->a(I)I

    move-result v6

    iput v6, v5, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    .line 604
    invoke-static {v0}, Lcom/peel/util/dw;->a(I)I

    move-result v6

    iput v6, v5, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    .line 605
    const/16 v6, 0x9

    invoke-virtual {v5, v6}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 606
    const/4 v6, 0x3

    invoke-virtual {v4}, Landroid/widget/ImageButton;->getId()I

    move-result v4

    invoke-virtual {v5, v6, v4}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 607
    invoke-virtual {v3, v5}, Landroid/widget/ImageButton;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 608
    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 610
    new-instance v4, Landroid/widget/ImageButton;

    invoke-direct {v4, p0}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    .line 611
    invoke-virtual {v4, p6}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 612
    const-string/jumbo v5, "Next"

    invoke-virtual {v4, v5}, Landroid/widget/ImageButton;->setTag(Ljava/lang/Object;)V

    .line 613
    const v5, 0x30d4f

    invoke-virtual {v4, v5}, Landroid/widget/ImageButton;->setId(I)V

    .line 614
    sget-boolean v5, Lcom/peel/util/dw;->b:Z

    if-eqz v5, :cond_d

    .line 615
    sget v5, Lcom/peel/ui/fo;->common_btn_stateful:I

    invoke-virtual {v4, v5}, Landroid/widget/ImageButton;->setBackgroundResource(I)V

    .line 616
    sget v5, Lcom/peel/ui/fo;->remote_icon_next:I

    invoke-virtual {v4, v5}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 620
    :goto_6
    sget v5, Lcom/peel/ui/ft;->button_next:I

    invoke-virtual {p0, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 621
    new-instance v5, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v6, -0x2

    const/4 v7, -0x2

    invoke-direct {v5, v6, v7}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 622
    invoke-static {v1}, Lcom/peel/util/dw;->a(I)I

    move-result v1

    iput v1, v5, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    .line 623
    invoke-static {v0}, Lcom/peel/util/dw;->a(I)I

    move-result v0

    iput v0, v5, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    .line 624
    const/4 v0, 0x6

    invoke-virtual {v3}, Landroid/widget/ImageButton;->getId()I

    move-result v1

    invoke-virtual {v5, v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 625
    const/4 v0, 0x1

    invoke-virtual {v3}, Landroid/widget/ImageButton;->getId()I

    move-result v1

    invoke-virtual {v5, v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 626
    invoke-virtual {v4, v5}, Landroid/widget/ImageButton;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 627
    invoke-virtual {v2, v4}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 629
    return-object v2

    .line 520
    :cond_8
    sget v4, Lcom/peel/ui/fo;->btn_m_play_pause_stateful:I

    invoke-virtual {v3, v4}, Landroid/widget/ImageButton;->setBackgroundResource(I)V

    goto/16 :goto_1

    .line 540
    :cond_9
    sget v5, Lcom/peel/ui/fo;->btn_m_stop_stateful:I

    invoke-virtual {v4, v5}, Landroid/widget/ImageButton;->setBackgroundResource(I)V

    goto/16 :goto_2

    .line 560
    :cond_a
    sget v5, Lcom/peel/ui/fo;->btn_m_rewind_stateful:I

    invoke-virtual {v4, v5}, Landroid/widget/ImageButton;->setBackgroundResource(I)V

    goto/16 :goto_3

    .line 580
    :cond_b
    sget v5, Lcom/peel/ui/fo;->btn_m_ff_stateful:I

    invoke-virtual {v3, v5}, Landroid/widget/ImageButton;->setBackgroundResource(I)V

    goto/16 :goto_4

    .line 599
    :cond_c
    sget v5, Lcom/peel/ui/fo;->btn_m_prev_stateful:I

    invoke-virtual {v3, v5}, Landroid/widget/ImageButton;->setBackgroundResource(I)V

    goto/16 :goto_5

    .line 618
    :cond_d
    sget v5, Lcom/peel/ui/fo;->btn_m_next_stateful:I

    invoke-virtual {v4, v5}, Landroid/widget/ImageButton;->setBackgroundResource(I)V

    goto :goto_6
.end method

.method public static a(Landroid/content/Context;Ljava/util/List;ZLandroid/view/View$OnClickListener;)Landroid/view/View;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;Z",
            "Landroid/view/View$OnClickListener;",
            ")",
            "Landroid/view/View;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    const/4 v2, -0x2

    .line 1351
    new-instance v3, Landroid/widget/LinearLayout;

    invoke-direct {v3, p0}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 1352
    invoke-virtual {v3, v0}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 1353
    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v1, v2, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 1356
    const/16 v2, 0x8

    invoke-static {v2}, Lcom/peel/util/dw;->a(I)I

    move-result v2

    iput v2, v1, Landroid/widget/LinearLayout$LayoutParams;->topMargin:I

    .line 1357
    const/4 v2, 0x1

    iput v2, v1, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    .line 1358
    invoke-virtual {v3, v1}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    move v1, v0

    .line 1360
    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 1361
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    if-eqz p2, :cond_0

    sget-object v2, Lcom/peel/util/dw;->f:[Ljava/lang/String;

    aget-object v2, v2, v1

    :goto_1
    invoke-static {p0, v0, v2, p3}, Lcom/peel/util/dw;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Landroid/view/View$OnClickListener;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v3, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1360
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1361
    :cond_0
    const/4 v2, 0x0

    goto :goto_1

    .line 1364
    :cond_1
    return-object v3
.end method

.method private static a(Landroid/content/Context;ILjava/lang/String;ILandroid/view/View$OnClickListener;)Landroid/widget/Button;
    .locals 8

    .prologue
    const/16 v7, 0x44

    const/16 v6, 0x10

    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v3, -0x2

    .line 1237
    new-instance v0, Lcom/peel/ui/b/a;

    invoke-direct {v0, p0}, Lcom/peel/ui/b/a;-><init>(Landroid/content/Context;)V

    .line 1238
    invoke-virtual {v0, v5}, Lcom/peel/ui/b/a;->setSingleLine(Z)V

    .line 1239
    invoke-virtual {v0, p2}, Lcom/peel/ui/b/a;->setTag(Ljava/lang/Object;)V

    .line 1240
    invoke-virtual {v0, p2}, Lcom/peel/ui/b/a;->setText(Ljava/lang/CharSequence;)V

    .line 1241
    invoke-virtual {v0, p4}, Lcom/peel/ui/b/a;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1242
    invoke-virtual {v0, p1}, Lcom/peel/ui/b/a;->setId(I)V

    .line 1243
    invoke-virtual {v0, p3}, Lcom/peel/ui/b/a;->setBackgroundResource(I)V

    .line 1244
    sget-boolean v1, Lcom/peel/util/dw;->b:Z

    if-eqz v1, :cond_0

    .line 1245
    invoke-virtual {v0, p2}, Lcom/peel/ui/b/a;->setText(Ljava/lang/CharSequence;)V

    .line 1246
    const/high16 v1, 0x41d00000    # 26.0f

    invoke-virtual {v0, v1}, Lcom/peel/ui/b/a;->setTextSize(F)V

    .line 1247
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/peel/ui/fm;->remote_ctrl_pad_text:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/peel/ui/b/a;->setTextColor(I)V

    .line 1249
    :cond_0
    invoke-virtual {v0, p2}, Lcom/peel/ui/b/a;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1250
    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v1, v3, v3}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 1251
    sget-boolean v2, Lcom/peel/util/dw;->b:Z

    if-eqz v2, :cond_1

    .line 1252
    invoke-static {v7}, Lcom/peel/util/dw;->a(I)I

    move-result v2

    iput v2, v1, Landroid/widget/LinearLayout$LayoutParams;->width:I

    .line 1253
    invoke-static {v7}, Lcom/peel/util/dw;->a(I)I

    move-result v2

    iput v2, v1, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 1254
    invoke-virtual {v1, v6, v4, v6, v4}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    .line 1256
    :cond_1
    invoke-static {v5}, Lcom/peel/util/dw;->a(I)I

    move-result v2

    iput v2, v1, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    .line 1257
    invoke-virtual {v0, v1}, Lcom/peel/ui/b/a;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1259
    return-object v0
.end method

.method private static a(Landroid/content/Context;ILjava/lang/String;IILandroid/view/View$OnClickListener;)Landroid/widget/ImageButton;
    .locals 6

    .prologue
    const/16 v5, 0x44

    const/4 v4, 0x6

    const/4 v3, 0x0

    const/4 v2, -0x2

    .line 1214
    new-instance v0, Landroid/widget/ImageButton;

    invoke-direct {v0, p0}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    .line 1215
    invoke-virtual {v0, p2}, Landroid/widget/ImageButton;->setTag(Ljava/lang/Object;)V

    .line 1216
    invoke-virtual {v0, p5}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1217
    invoke-virtual {v0, p1}, Landroid/widget/ImageButton;->setId(I)V

    .line 1218
    invoke-virtual {v0, p3}, Landroid/widget/ImageButton;->setBackgroundResource(I)V

    .line 1219
    const/4 v1, -0x1

    if-le p4, v1, :cond_0

    .line 1220
    invoke-virtual {v0, p4}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 1222
    :cond_0
    invoke-virtual {v0, p2}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1223
    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v1, v2, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 1224
    sget-boolean v2, Lcom/peel/util/dw;->b:Z

    if-eqz v2, :cond_1

    .line 1225
    invoke-static {v5}, Lcom/peel/util/dw;->a(I)I

    move-result v2

    iput v2, v1, Landroid/widget/LinearLayout$LayoutParams;->width:I

    .line 1226
    invoke-static {v5}, Lcom/peel/util/dw;->a(I)I

    move-result v2

    iput v2, v1, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 1227
    invoke-virtual {v1, v4, v3, v4, v3}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    .line 1229
    :cond_1
    const/4 v2, 0x1

    invoke-static {v2}, Lcom/peel/util/dw;->a(I)I

    move-result v2

    iput v2, v1, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    .line 1230
    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1232
    return-object v0
.end method

.method private static a(Landroid/content/Context;I)Landroid/widget/LinearLayout;
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 1199
    new-instance v0, Landroid/widget/LinearLayout;

    invoke-direct {v0, p0}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 1200
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 1201
    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v2, -0x1

    const/4 v3, -0x2

    invoke-direct {v1, v2, v3}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 1202
    invoke-static {p1}, Lcom/peel/util/dw;->a(I)I

    move-result v2

    iput v2, v1, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    .line 1203
    invoke-static {v4}, Lcom/peel/util/dw;->a(I)I

    move-result v2

    iput v2, v1, Landroid/widget/LinearLayout$LayoutParams;->topMargin:I

    .line 1204
    const/16 v2, 0x49

    invoke-static {v2}, Lcom/peel/util/dw;->a(I)I

    move-result v2

    iput v2, v1, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 1205
    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setGravity(I)V

    .line 1206
    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1208
    return-object v0
.end method

.method public static a(Landroid/content/Context;ZZZZILandroid/view/View$OnClickListener;)Ljava/util/List;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "ZZZZI",
            "Landroid/view/View$OnClickListener;",
            ")",
            "Ljava/util/List",
            "<",
            "Landroid/widget/LinearLayout;",
            ">;"
        }
    .end annotation

    .prologue
    .line 952
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 955
    const v1, 0x30d55

    const-string/jumbo v2, "1"

    sget v3, Lcom/peel/ui/fo;->samsung_num_button_01_stateful:I

    const/4 v4, -0x1

    move-object v0, p0

    move-object v5, p6

    invoke-static/range {v0 .. v5}, Lcom/peel/util/dw;->a(Landroid/content/Context;ILjava/lang/String;IILandroid/view/View$OnClickListener;)Landroid/widget/ImageButton;

    move-result-object v7

    .line 957
    const v1, 0x30d56

    const-string/jumbo v2, "2"

    sget v3, Lcom/peel/ui/fo;->samsung_num_button_02_stateful:I

    const/4 v4, -0x1

    move-object v0, p0

    move-object v5, p6

    invoke-static/range {v0 .. v5}, Lcom/peel/util/dw;->a(Landroid/content/Context;ILjava/lang/String;IILandroid/view/View$OnClickListener;)Landroid/widget/ImageButton;

    move-result-object v8

    .line 959
    const v1, 0x30d57

    const-string/jumbo v2, "3"

    sget v3, Lcom/peel/ui/fo;->samsung_num_button_03_stateful:I

    const/4 v4, -0x1

    move-object v0, p0

    move-object v5, p6

    invoke-static/range {v0 .. v5}, Lcom/peel/util/dw;->a(Landroid/content/Context;ILjava/lang/String;IILandroid/view/View$OnClickListener;)Landroid/widget/ImageButton;

    move-result-object v0

    .line 961
    invoke-static {p0, p5}, Lcom/peel/util/dw;->a(Landroid/content/Context;I)Landroid/widget/LinearLayout;

    move-result-object v1

    .line 962
    invoke-virtual {v1, v7}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 963
    invoke-virtual {v1, v8}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 964
    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 965
    invoke-interface {v6, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 968
    const v1, 0x30d58

    const-string/jumbo v2, "4"

    sget v3, Lcom/peel/ui/fo;->samsung_num_button_04_stateful:I

    const/4 v4, -0x1

    move-object v0, p0

    move-object v5, p6

    invoke-static/range {v0 .. v5}, Lcom/peel/util/dw;->a(Landroid/content/Context;ILjava/lang/String;IILandroid/view/View$OnClickListener;)Landroid/widget/ImageButton;

    move-result-object v7

    .line 970
    const v1, 0x30d59

    const-string/jumbo v2, "5"

    sget v3, Lcom/peel/ui/fo;->samsung_num_button_05_stateful:I

    const/4 v4, -0x1

    move-object v0, p0

    move-object v5, p6

    invoke-static/range {v0 .. v5}, Lcom/peel/util/dw;->a(Landroid/content/Context;ILjava/lang/String;IILandroid/view/View$OnClickListener;)Landroid/widget/ImageButton;

    move-result-object v8

    .line 972
    const v1, 0x30d5a

    const-string/jumbo v2, "6"

    sget v3, Lcom/peel/ui/fo;->samsung_num_button_06_stateful:I

    const/4 v4, -0x1

    move-object v0, p0

    move-object v5, p6

    invoke-static/range {v0 .. v5}, Lcom/peel/util/dw;->a(Landroid/content/Context;ILjava/lang/String;IILandroid/view/View$OnClickListener;)Landroid/widget/ImageButton;

    move-result-object v0

    .line 974
    invoke-static {p0, p5}, Lcom/peel/util/dw;->a(Landroid/content/Context;I)Landroid/widget/LinearLayout;

    move-result-object v1

    .line 975
    invoke-virtual {v1, v7}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 976
    invoke-virtual {v1, v8}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 977
    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 978
    invoke-interface {v6, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 981
    const v1, 0x30d5b

    const-string/jumbo v2, "7"

    sget v3, Lcom/peel/ui/fo;->samsung_num_button_07_stateful:I

    const/4 v4, -0x1

    move-object v0, p0

    move-object v5, p6

    invoke-static/range {v0 .. v5}, Lcom/peel/util/dw;->a(Landroid/content/Context;ILjava/lang/String;IILandroid/view/View$OnClickListener;)Landroid/widget/ImageButton;

    move-result-object v7

    .line 983
    const v1, 0x30d5c

    const-string/jumbo v2, "8"

    sget v3, Lcom/peel/ui/fo;->samsung_num_button_08_stateful:I

    const/4 v4, -0x1

    move-object v0, p0

    move-object v5, p6

    invoke-static/range {v0 .. v5}, Lcom/peel/util/dw;->a(Landroid/content/Context;ILjava/lang/String;IILandroid/view/View$OnClickListener;)Landroid/widget/ImageButton;

    move-result-object v8

    .line 985
    const v1, 0x30d5d

    const-string/jumbo v2, "9"

    sget v3, Lcom/peel/ui/fo;->samsung_num_button_09_stateful:I

    const/4 v4, -0x1

    move-object v0, p0

    move-object v5, p6

    invoke-static/range {v0 .. v5}, Lcom/peel/util/dw;->a(Landroid/content/Context;ILjava/lang/String;IILandroid/view/View$OnClickListener;)Landroid/widget/ImageButton;

    move-result-object v0

    .line 987
    invoke-static {p0, p5}, Lcom/peel/util/dw;->a(Landroid/content/Context;I)Landroid/widget/LinearLayout;

    move-result-object v1

    .line 988
    invoke-virtual {v1, v7}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 989
    invoke-virtual {v1, v8}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 990
    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 991
    invoke-interface {v6, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 994
    if-eqz p1, :cond_1

    .line 996
    const v1, 0x30d5e

    const-string/jumbo v2, "10"

    sget v3, Lcom/peel/ui/fo;->samsung_num_button_10_stateful:I

    const/4 v4, -0x1

    move-object v0, p0

    move-object v5, p6

    invoke-static/range {v0 .. v5}, Lcom/peel/util/dw;->a(Landroid/content/Context;ILjava/lang/String;IILandroid/view/View$OnClickListener;)Landroid/widget/ImageButton;

    move-result-object v7

    .line 998
    const v1, 0x30d5f

    const-string/jumbo v2, "11"

    sget v3, Lcom/peel/ui/fo;->samsung_num_button_11_stateful:I

    const/4 v4, -0x1

    move-object v0, p0

    move-object v5, p6

    invoke-static/range {v0 .. v5}, Lcom/peel/util/dw;->a(Landroid/content/Context;ILjava/lang/String;IILandroid/view/View$OnClickListener;)Landroid/widget/ImageButton;

    move-result-object v8

    .line 1000
    const v1, 0x30d60

    const-string/jumbo v2, "12"

    sget v3, Lcom/peel/ui/fo;->samsung_num_button_12_stateful:I

    const/4 v4, -0x1

    move-object v0, p0

    move-object v5, p6

    invoke-static/range {v0 .. v5}, Lcom/peel/util/dw;->a(Landroid/content/Context;ILjava/lang/String;IILandroid/view/View$OnClickListener;)Landroid/widget/ImageButton;

    move-result-object v0

    .line 1002
    invoke-static {p0, p5}, Lcom/peel/util/dw;->a(Landroid/content/Context;I)Landroid/widget/LinearLayout;

    move-result-object v1

    .line 1003
    invoke-virtual {v1, v7}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1004
    invoke-virtual {v1, v8}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1005
    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1006
    invoke-interface {v6, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1008
    if-eqz p2, :cond_0

    .line 1009
    const v0, 0x30d61

    const-string/jumbo v1, "LLD"

    sget v2, Lcom/peel/ui/fo;->keypad_modes4_btn_left:I

    invoke-static {p0, v0, v1, v2, p6}, Lcom/peel/util/dw;->a(Landroid/content/Context;ILjava/lang/String;ILandroid/view/View$OnClickListener;)Landroid/widget/Button;

    move-result-object v0

    .line 1011
    const v1, 0x30d62

    const-string/jumbo v2, "BS"

    sget v3, Lcom/peel/ui/fo;->keypad_modes4_btn_center:I

    invoke-static {p0, v1, v2, v3, p6}, Lcom/peel/util/dw;->a(Landroid/content/Context;ILjava/lang/String;ILandroid/view/View$OnClickListener;)Landroid/widget/Button;

    move-result-object v1

    .line 1013
    const v2, 0x30d63

    const-string/jumbo v3, "CS"

    sget v4, Lcom/peel/ui/fo;->keypad_modes4_btn_center:I

    invoke-static {p0, v2, v3, v4, p6}, Lcom/peel/util/dw;->a(Landroid/content/Context;ILjava/lang/String;ILandroid/view/View$OnClickListener;)Landroid/widget/Button;

    move-result-object v2

    .line 1015
    const v3, 0x30d64

    const-string/jumbo v4, "---"

    sget v5, Lcom/peel/ui/fo;->keypad_modes4_btn_right:I

    invoke-static {p0, v3, v4, v5, p6}, Lcom/peel/util/dw;->a(Landroid/content/Context;ILjava/lang/String;ILandroid/view/View$OnClickListener;)Landroid/widget/Button;

    move-result-object v3

    .line 1017
    invoke-static {p0, p5}, Lcom/peel/util/dw;->a(Landroid/content/Context;I)Landroid/widget/LinearLayout;

    move-result-object v4

    .line 1018
    invoke-virtual {v4, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1019
    invoke-virtual {v4, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1020
    invoke-virtual {v4, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1021
    invoke-virtual {v4, v3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1022
    invoke-interface {v6, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1069
    :goto_0
    return-object v6

    .line 1024
    :cond_0
    const v0, 0x30d61

    const-string/jumbo v1, "LLD"

    sget v2, Lcom/peel/ui/fo;->keypad_modes3_btn_left:I

    invoke-static {p0, v0, v1, v2, p6}, Lcom/peel/util/dw;->a(Landroid/content/Context;ILjava/lang/String;ILandroid/view/View$OnClickListener;)Landroid/widget/Button;

    move-result-object v0

    .line 1026
    const v1, 0x30d62

    const-string/jumbo v2, "BS"

    sget v3, Lcom/peel/ui/fo;->keypad_modes3_btn_center:I

    invoke-static {p0, v1, v2, v3, p6}, Lcom/peel/util/dw;->a(Landroid/content/Context;ILjava/lang/String;ILandroid/view/View$OnClickListener;)Landroid/widget/Button;

    move-result-object v1

    .line 1028
    const v2, 0x30d63

    const-string/jumbo v3, "CS"

    sget v4, Lcom/peel/ui/fo;->keypad_modes3_btn_right:I

    invoke-static {p0, v2, v3, v4, p6}, Lcom/peel/util/dw;->a(Landroid/content/Context;ILjava/lang/String;ILandroid/view/View$OnClickListener;)Landroid/widget/Button;

    move-result-object v2

    .line 1030
    invoke-static {p0, p5}, Lcom/peel/util/dw;->a(Landroid/content/Context;I)Landroid/widget/LinearLayout;

    move-result-object v3

    .line 1031
    invoke-virtual {v3, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1032
    invoke-virtual {v3, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1033
    invoke-virtual {v3, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1034
    invoke-interface {v6, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1037
    :cond_1
    const v1, 0x30d65

    const-string/jumbo v2, "."

    sget v3, Lcom/peel/ui/fo;->samsung_num_button_dash_stateful:I

    const/4 v4, -0x1

    move-object v0, p0

    move-object v5, p6

    invoke-static/range {v0 .. v5}, Lcom/peel/util/dw;->a(Landroid/content/Context;ILjava/lang/String;IILandroid/view/View$OnClickListener;)Landroid/widget/ImageButton;

    move-result-object v7

    .line 1039
    const v1, 0x30d54

    const-string/jumbo v2, "0"

    sget v3, Lcom/peel/ui/fo;->samsung_num_button_00_stateful:I

    const/4 v4, -0x1

    move-object v0, p0

    move-object v5, p6

    invoke-static/range {v0 .. v5}, Lcom/peel/util/dw;->a(Landroid/content/Context;ILjava/lang/String;IILandroid/view/View$OnClickListener;)Landroid/widget/ImageButton;

    move-result-object v8

    .line 1041
    const v1, 0x30d66

    const-string/jumbo v2, "Enter"

    sget v3, Lcom/peel/ui/fo;->samsung_num_button_enter_stateful:I

    const/4 v4, -0x1

    move-object v0, p0

    move-object v5, p6

    invoke-static/range {v0 .. v5}, Lcom/peel/util/dw;->a(Landroid/content/Context;ILjava/lang/String;IILandroid/view/View$OnClickListener;)Landroid/widget/ImageButton;

    move-result-object v0

    .line 1043
    invoke-static {p0, p5}, Lcom/peel/util/dw;->a(Landroid/content/Context;I)Landroid/widget/LinearLayout;

    move-result-object v1

    .line 1045
    if-nez p3, :cond_2

    .line 1046
    sget-boolean v2, Lcom/peel/util/dw;->b:Z

    if-eqz v2, :cond_4

    .line 1047
    const/4 v2, 0x0

    invoke-virtual {v7, v2}, Landroid/widget/ImageButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1051
    :goto_1
    const/4 v2, 0x0

    invoke-virtual {v7, v2}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 1054
    :cond_2
    if-nez p4, :cond_3

    .line 1055
    sget-boolean v2, Lcom/peel/util/dw;->b:Z

    if-eqz v2, :cond_5

    .line 1056
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1060
    :goto_2
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 1063
    :cond_3
    invoke-virtual {v1, v7}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1064
    invoke-virtual {v1, v8}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1065
    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1066
    invoke-interface {v6, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 1049
    :cond_4
    sget v2, Lcom/peel/ui/fo;->keypad_blank:I

    invoke-virtual {v7, v2}, Landroid/widget/ImageButton;->setBackgroundResource(I)V

    goto :goto_1

    .line 1058
    :cond_5
    sget v2, Lcom/peel/ui/fo;->keypad_blank_reverse:I

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setBackgroundResource(I)V

    goto :goto_2
.end method

.method public static a(Landroid/content/Context;Z)V
    .locals 2

    .prologue
    .line 234
    sget v0, Lcom/peel/util/dw;->a:F

    const/high16 v1, -0x40800000    # -1.0f

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    sput v0, Lcom/peel/util/dw;->a:F

    .line 235
    :cond_0
    sput-boolean p1, Lcom/peel/util/dw;->b:Z

    .line 236
    return-void
.end method

.method public static b(Landroid/content/Context;IIIILjava/lang/String;Landroid/view/View$OnClickListener;)Landroid/view/View;
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, -0x2

    .line 385
    new-instance v0, Landroid/widget/Button;

    invoke-direct {v0, p0}, Landroid/widget/Button;-><init>(Landroid/content/Context;)V

    .line 386
    invoke-virtual {v0, p5}, Landroid/widget/Button;->setTag(Ljava/lang/Object;)V

    .line 387
    invoke-virtual {v0, p1}, Landroid/widget/Button;->setId(I)V

    .line 388
    invoke-virtual {v0, p6}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 390
    new-instance v1, Landroid/graphics/drawable/StateListDrawable;

    invoke-direct {v1}, Landroid/graphics/drawable/StateListDrawable;-><init>()V

    .line 391
    const/4 v2, 0x1

    new-array v2, v2, [I

    const v3, 0x10100a7

    aput v3, v2, v5

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, p3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 392
    new-array v2, v5, [I

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, p2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 393
    invoke-virtual {v0, v1}, Landroid/widget/Button;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 395
    invoke-virtual {p0, p4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 396
    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-direct {v1, v4, v4, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(IIF)V

    .line 397
    const/16 v2, 0x51

    invoke-static {v2}, Lcom/peel/util/dw;->a(I)I

    move-result v2

    iput v2, v1, Landroid/widget/LinearLayout$LayoutParams;->width:I

    .line 398
    const/16 v2, 0x30

    invoke-static {v2}, Lcom/peel/util/dw;->a(I)I

    move-result v2

    iput v2, v1, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 400
    const/16 v2, 0x11

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setGravity(I)V

    .line 402
    invoke-virtual {v0, v1}, Landroid/widget/Button;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 404
    return-object v0
.end method

.method public static b(Landroid/content/Context;ILandroid/view/View$OnClickListener;Landroid/view/ViewGroup$LayoutParams;)Landroid/view/View;
    .locals 11

    .prologue
    const/16 v10, 0xf

    const/16 v9, 0xe

    const/16 v8, 0x45

    const/16 v7, 0x49

    const/4 v6, -0x2

    .line 864
    new-instance v0, Landroid/widget/RelativeLayout;

    invoke-direct {v0, p0}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 866
    invoke-virtual {v0, p3}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 867
    invoke-virtual {v0, p1}, Landroid/widget/RelativeLayout;->setId(I)V

    .line 870
    new-instance v1, Landroid/widget/ImageButton;

    invoke-direct {v1, p0}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    .line 871
    const v2, 0x30d7a

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setId(I)V

    .line 872
    const-string/jumbo v2, "Select"

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setTag(Ljava/lang/Object;)V

    .line 873
    invoke-virtual {v1, p2}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 874
    sget v2, Lcom/peel/ui/fo;->button_ok_stateful:I

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setBackgroundResource(I)V

    .line 875
    sget v2, Lcom/peel/ui/ft;->ok:I

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 876
    new-instance v2, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v2, v6, v6}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 877
    invoke-static {v7}, Lcom/peel/util/dw;->a(I)I

    move-result v3

    iput v3, v2, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    .line 878
    invoke-static {v7}, Lcom/peel/util/dw;->a(I)I

    move-result v3

    iput v3, v2, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    .line 879
    const/16 v3, 0xd

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 880
    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 881
    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 883
    new-instance v2, Landroid/widget/ImageButton;

    invoke-direct {v2, p0}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    .line 884
    const-string/jumbo v3, "Navigate_Up"

    invoke-virtual {v2, v3}, Landroid/widget/ImageButton;->setTag(Ljava/lang/Object;)V

    .line 885
    invoke-virtual {v2, p2}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 886
    sget v3, Lcom/peel/ui/fo;->button_up_stateful:I

    invoke-virtual {v2, v3}, Landroid/widget/ImageButton;->setBackgroundResource(I)V

    .line 887
    sget v3, Lcom/peel/ui/ft;->button_navigate_up:I

    invoke-virtual {p0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 888
    new-instance v3, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v3, v6, v6}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 889
    invoke-static {v7}, Lcom/peel/util/dw;->a(I)I

    move-result v4

    iput v4, v3, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    .line 890
    invoke-static {v8}, Lcom/peel/util/dw;->a(I)I

    move-result v4

    iput v4, v3, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    .line 891
    invoke-virtual {v3, v9}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 892
    const/4 v4, 0x2

    invoke-virtual {v1}, Landroid/widget/ImageButton;->getId()I

    move-result v5

    invoke-virtual {v3, v4, v5}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 893
    invoke-virtual {v2, v3}, Landroid/widget/ImageButton;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 894
    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 896
    new-instance v2, Landroid/widget/ImageButton;

    invoke-direct {v2, p0}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    .line 897
    const-string/jumbo v3, "Navigate_Down"

    invoke-virtual {v2, v3}, Landroid/widget/ImageButton;->setTag(Ljava/lang/Object;)V

    .line 898
    invoke-virtual {v2, p2}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 900
    sget v3, Lcom/peel/ui/fo;->button_down_stateful:I

    invoke-virtual {v2, v3}, Landroid/widget/ImageButton;->setBackgroundResource(I)V

    .line 901
    sget v3, Lcom/peel/ui/ft;->button_navigate_down:I

    invoke-virtual {p0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 902
    new-instance v3, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v3, v6, v6}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 903
    invoke-static {v7}, Lcom/peel/util/dw;->a(I)I

    move-result v4

    iput v4, v3, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    .line 904
    invoke-static {v8}, Lcom/peel/util/dw;->a(I)I

    move-result v4

    iput v4, v3, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    .line 905
    invoke-virtual {v3, v9}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 906
    const/4 v4, 0x3

    invoke-virtual {v1}, Landroid/widget/ImageButton;->getId()I

    move-result v5

    invoke-virtual {v3, v4, v5}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 907
    invoke-virtual {v2, v3}, Landroid/widget/ImageButton;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 908
    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 910
    new-instance v2, Landroid/widget/ImageButton;

    invoke-direct {v2, p0}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    .line 911
    const-string/jumbo v3, "Navigate_Left"

    invoke-virtual {v2, v3}, Landroid/widget/ImageButton;->setTag(Ljava/lang/Object;)V

    .line 912
    invoke-virtual {v2, p2}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 913
    sget v3, Lcom/peel/ui/fo;->button_left_stateful:I

    invoke-virtual {v2, v3}, Landroid/widget/ImageButton;->setBackgroundResource(I)V

    .line 914
    sget v3, Lcom/peel/ui/ft;->button_navigate_left:I

    invoke-virtual {p0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 915
    new-instance v3, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v3, v6, v6}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 916
    invoke-static {v8}, Lcom/peel/util/dw;->a(I)I

    move-result v4

    iput v4, v3, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    .line 917
    invoke-static {v7}, Lcom/peel/util/dw;->a(I)I

    move-result v4

    iput v4, v3, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    .line 918
    invoke-virtual {v3, v10}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 919
    const/4 v4, 0x0

    invoke-virtual {v1}, Landroid/widget/ImageButton;->getId()I

    move-result v5

    invoke-virtual {v3, v4, v5}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 920
    invoke-virtual {v2, v3}, Landroid/widget/ImageButton;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 921
    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 923
    new-instance v2, Landroid/widget/ImageButton;

    invoke-direct {v2, p0}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    .line 924
    const-string/jumbo v3, "Navigate_Right"

    invoke-virtual {v2, v3}, Landroid/widget/ImageButton;->setTag(Ljava/lang/Object;)V

    .line 925
    invoke-virtual {v2, p2}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 927
    sget v3, Lcom/peel/ui/fo;->button_right_stateful:I

    invoke-virtual {v2, v3}, Landroid/widget/ImageButton;->setBackgroundResource(I)V

    .line 928
    sget v3, Lcom/peel/ui/ft;->button_navigate_right:I

    invoke-virtual {p0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 929
    new-instance v3, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v3, v6, v6}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 930
    invoke-static {v8}, Lcom/peel/util/dw;->a(I)I

    move-result v4

    iput v4, v3, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    .line 931
    invoke-static {v7}, Lcom/peel/util/dw;->a(I)I

    move-result v4

    iput v4, v3, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    .line 932
    invoke-virtual {v3, v10}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 933
    const/4 v4, 0x1

    invoke-virtual {v1}, Landroid/widget/ImageButton;->getId()I

    move-result v1

    invoke-virtual {v3, v4, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 934
    invoke-virtual {v2, v3}, Landroid/widget/ImageButton;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 935
    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 937
    return-object v0
.end method

.method public static b(Landroid/content/Context;Ljava/lang/String;Landroid/view/View$OnClickListener;)Landroid/view/View;
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v2, -0x1

    .line 1277
    const/16 v0, 0x5e

    invoke-virtual {p1, v0}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    if-le v0, v2, :cond_1

    .line 1278
    const-string/jumbo v0, "\\^"

    invoke-virtual {p1, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 1280
    sget-object v0, Lcom/peel/util/dw;->e:Ljava/util/Map;

    aget-object v3, v1, v5

    invoke-interface {v0, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1281
    sget-object v0, Lcom/peel/util/dw;->e:Ljava/util/Map;

    aget-object v2, v1, v5

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [I

    .line 1282
    aget-object v1, v1, v4

    aget v2, v0, v4

    aget v3, v0, v5

    const/4 v4, 0x2

    aget v4, v0, v4

    move-object v0, p0

    move-object v5, p2

    invoke-static/range {v0 .. v5}, Lcom/peel/util/dw;->a(Landroid/content/Context;Ljava/lang/String;IIILandroid/view/View$OnClickListener;)Landroid/view/View;

    move-result-object v0

    .line 1287
    :goto_0
    return-object v0

    .line 1284
    :cond_0
    aget-object v1, v1, v4

    move-object v0, p0

    move v3, v2

    move v4, v2

    move-object v5, p2

    invoke-static/range {v0 .. v5}, Lcom/peel/util/dw;->a(Landroid/content/Context;Ljava/lang/String;IIILandroid/view/View$OnClickListener;)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    :cond_1
    move-object v0, p0

    move-object v1, p1

    move v3, v2

    move v4, v2

    move-object v5, p2

    .line 1287
    invoke-static/range {v0 .. v5}, Lcom/peel/util/dw;->a(Landroid/content/Context;Ljava/lang/String;IIILandroid/view/View$OnClickListener;)Landroid/view/View;

    move-result-object v0

    goto :goto_0
.end method

.method public static b(Landroid/content/Context;ZZZZILandroid/view/View$OnClickListener;)Ljava/util/List;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "ZZZZI",
            "Landroid/view/View$OnClickListener;",
            ")",
            "Ljava/util/List",
            "<",
            "Landroid/widget/LinearLayout;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1076
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 1079
    const v0, 0x30d55

    const-string/jumbo v1, "1"

    sget v2, Lcom/peel/ui/fo;->common_btn_stateful:I

    invoke-static {p0, v0, v1, v2, p6}, Lcom/peel/util/dw;->a(Landroid/content/Context;ILjava/lang/String;ILandroid/view/View$OnClickListener;)Landroid/widget/Button;

    move-result-object v0

    .line 1081
    const v1, 0x30d56

    const-string/jumbo v2, "2"

    sget v3, Lcom/peel/ui/fo;->common_btn_stateful:I

    invoke-static {p0, v1, v2, v3, p6}, Lcom/peel/util/dw;->a(Landroid/content/Context;ILjava/lang/String;ILandroid/view/View$OnClickListener;)Landroid/widget/Button;

    move-result-object v1

    .line 1083
    const v2, 0x30d57

    const-string/jumbo v3, "3"

    sget v4, Lcom/peel/ui/fo;->common_btn_stateful:I

    invoke-static {p0, v2, v3, v4, p6}, Lcom/peel/util/dw;->a(Landroid/content/Context;ILjava/lang/String;ILandroid/view/View$OnClickListener;)Landroid/widget/Button;

    move-result-object v2

    .line 1085
    invoke-static {p0, p5}, Lcom/peel/util/dw;->a(Landroid/content/Context;I)Landroid/widget/LinearLayout;

    move-result-object v3

    .line 1087
    invoke-virtual {v3, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1088
    invoke-virtual {v3, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1089
    invoke-virtual {v3, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1091
    invoke-interface {v6, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1094
    const v0, 0x30d58

    const-string/jumbo v1, "4"

    sget v2, Lcom/peel/ui/fo;->common_btn_stateful:I

    invoke-static {p0, v0, v1, v2, p6}, Lcom/peel/util/dw;->a(Landroid/content/Context;ILjava/lang/String;ILandroid/view/View$OnClickListener;)Landroid/widget/Button;

    move-result-object v0

    .line 1096
    const v1, 0x30d59

    const-string/jumbo v2, "5"

    sget v3, Lcom/peel/ui/fo;->common_btn_stateful:I

    invoke-static {p0, v1, v2, v3, p6}, Lcom/peel/util/dw;->a(Landroid/content/Context;ILjava/lang/String;ILandroid/view/View$OnClickListener;)Landroid/widget/Button;

    move-result-object v1

    .line 1098
    const v2, 0x30d5a

    const-string/jumbo v3, "6"

    sget v4, Lcom/peel/ui/fo;->common_btn_stateful:I

    invoke-static {p0, v2, v3, v4, p6}, Lcom/peel/util/dw;->a(Landroid/content/Context;ILjava/lang/String;ILandroid/view/View$OnClickListener;)Landroid/widget/Button;

    move-result-object v2

    .line 1100
    invoke-static {p0, p5}, Lcom/peel/util/dw;->a(Landroid/content/Context;I)Landroid/widget/LinearLayout;

    move-result-object v3

    .line 1101
    invoke-virtual {v3, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1102
    invoke-virtual {v3, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1103
    invoke-virtual {v3, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1104
    invoke-interface {v6, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1107
    const v0, 0x30d5b

    const-string/jumbo v1, "7"

    sget v2, Lcom/peel/ui/fo;->common_btn_stateful:I

    invoke-static {p0, v0, v1, v2, p6}, Lcom/peel/util/dw;->a(Landroid/content/Context;ILjava/lang/String;ILandroid/view/View$OnClickListener;)Landroid/widget/Button;

    move-result-object v0

    .line 1109
    const v1, 0x30d5c

    const-string/jumbo v2, "8"

    sget v3, Lcom/peel/ui/fo;->common_btn_stateful:I

    invoke-static {p0, v1, v2, v3, p6}, Lcom/peel/util/dw;->a(Landroid/content/Context;ILjava/lang/String;ILandroid/view/View$OnClickListener;)Landroid/widget/Button;

    move-result-object v1

    .line 1111
    const v2, 0x30d5d

    const-string/jumbo v3, "9"

    sget v4, Lcom/peel/ui/fo;->common_btn_stateful:I

    invoke-static {p0, v2, v3, v4, p6}, Lcom/peel/util/dw;->a(Landroid/content/Context;ILjava/lang/String;ILandroid/view/View$OnClickListener;)Landroid/widget/Button;

    move-result-object v2

    .line 1113
    invoke-static {p0, p5}, Lcom/peel/util/dw;->a(Landroid/content/Context;I)Landroid/widget/LinearLayout;

    move-result-object v3

    .line 1114
    invoke-virtual {v3, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1115
    invoke-virtual {v3, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1116
    invoke-virtual {v3, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1117
    invoke-interface {v6, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1120
    if-eqz p1, :cond_1

    .line 1122
    const v0, 0x30d5e

    const-string/jumbo v1, "10"

    sget v2, Lcom/peel/ui/fo;->common_btn_stateful:I

    invoke-static {p0, v0, v1, v2, p6}, Lcom/peel/util/dw;->a(Landroid/content/Context;ILjava/lang/String;ILandroid/view/View$OnClickListener;)Landroid/widget/Button;

    move-result-object v0

    .line 1124
    const v1, 0x30d5f

    const-string/jumbo v2, "11"

    sget v3, Lcom/peel/ui/fo;->common_btn_stateful:I

    invoke-static {p0, v1, v2, v3, p6}, Lcom/peel/util/dw;->a(Landroid/content/Context;ILjava/lang/String;ILandroid/view/View$OnClickListener;)Landroid/widget/Button;

    move-result-object v1

    .line 1126
    const v2, 0x30d60

    const-string/jumbo v3, "12"

    sget v4, Lcom/peel/ui/fo;->common_btn_stateful:I

    invoke-static {p0, v2, v3, v4, p6}, Lcom/peel/util/dw;->a(Landroid/content/Context;ILjava/lang/String;ILandroid/view/View$OnClickListener;)Landroid/widget/Button;

    move-result-object v2

    .line 1128
    invoke-static {p0, p5}, Lcom/peel/util/dw;->a(Landroid/content/Context;I)Landroid/widget/LinearLayout;

    move-result-object v3

    .line 1129
    invoke-virtual {v3, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1130
    invoke-virtual {v3, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1131
    invoke-virtual {v3, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1132
    invoke-interface {v6, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1134
    if-eqz p2, :cond_0

    .line 1135
    const v0, 0x30d61

    const-string/jumbo v1, "LLD"

    sget v2, Lcom/peel/ui/fo;->common_btn_stateful:I

    invoke-static {p0, v0, v1, v2, p6}, Lcom/peel/util/dw;->a(Landroid/content/Context;ILjava/lang/String;ILandroid/view/View$OnClickListener;)Landroid/widget/Button;

    move-result-object v0

    .line 1137
    const v1, 0x30d62

    const-string/jumbo v2, "BS"

    sget v3, Lcom/peel/ui/fo;->common_btn_stateful:I

    invoke-static {p0, v1, v2, v3, p6}, Lcom/peel/util/dw;->a(Landroid/content/Context;ILjava/lang/String;ILandroid/view/View$OnClickListener;)Landroid/widget/Button;

    move-result-object v1

    .line 1139
    const v2, 0x30d63

    const-string/jumbo v3, "CS"

    sget v4, Lcom/peel/ui/fo;->common_btn_stateful:I

    invoke-static {p0, v2, v3, v4, p6}, Lcom/peel/util/dw;->a(Landroid/content/Context;ILjava/lang/String;ILandroid/view/View$OnClickListener;)Landroid/widget/Button;

    move-result-object v2

    .line 1141
    const v3, 0x30d64

    const-string/jumbo v4, "---"

    sget v5, Lcom/peel/ui/fo;->common_btn_stateful:I

    invoke-static {p0, v3, v4, v5, p6}, Lcom/peel/util/dw;->a(Landroid/content/Context;ILjava/lang/String;ILandroid/view/View$OnClickListener;)Landroid/widget/Button;

    move-result-object v3

    .line 1143
    invoke-static {p0, p5}, Lcom/peel/util/dw;->a(Landroid/content/Context;I)Landroid/widget/LinearLayout;

    move-result-object v4

    .line 1144
    invoke-virtual {v4, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1145
    invoke-virtual {v4, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1146
    invoke-virtual {v4, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1147
    invoke-virtual {v4, v3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1148
    invoke-interface {v6, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1195
    :goto_0
    return-object v6

    .line 1150
    :cond_0
    const v0, 0x30d61

    const-string/jumbo v1, "LLD"

    sget v2, Lcom/peel/ui/fo;->common_btn_stateful:I

    invoke-static {p0, v0, v1, v2, p6}, Lcom/peel/util/dw;->a(Landroid/content/Context;ILjava/lang/String;ILandroid/view/View$OnClickListener;)Landroid/widget/Button;

    move-result-object v0

    .line 1152
    const v1, 0x30d62

    const-string/jumbo v2, "BS"

    sget v3, Lcom/peel/ui/fo;->common_btn_stateful:I

    invoke-static {p0, v1, v2, v3, p6}, Lcom/peel/util/dw;->a(Landroid/content/Context;ILjava/lang/String;ILandroid/view/View$OnClickListener;)Landroid/widget/Button;

    move-result-object v1

    .line 1154
    const v2, 0x30d63

    const-string/jumbo v3, "CS"

    sget v4, Lcom/peel/ui/fo;->common_btn_stateful:I

    invoke-static {p0, v2, v3, v4, p6}, Lcom/peel/util/dw;->a(Landroid/content/Context;ILjava/lang/String;ILandroid/view/View$OnClickListener;)Landroid/widget/Button;

    move-result-object v2

    .line 1156
    invoke-static {p0, p5}, Lcom/peel/util/dw;->a(Landroid/content/Context;I)Landroid/widget/LinearLayout;

    move-result-object v3

    .line 1157
    invoke-virtual {v3, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1158
    invoke-virtual {v3, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1159
    invoke-virtual {v3, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1160
    invoke-interface {v6, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1163
    :cond_1
    const v1, 0x30d65

    const-string/jumbo v2, "."

    sget v3, Lcom/peel/ui/fo;->common_btn_stateful:I

    sget v4, Lcom/peel/ui/fo;->remote_icon_minus:I

    move-object v0, p0

    move-object v5, p6

    invoke-static/range {v0 .. v5}, Lcom/peel/util/dw;->a(Landroid/content/Context;ILjava/lang/String;IILandroid/view/View$OnClickListener;)Landroid/widget/ImageButton;

    move-result-object v7

    .line 1165
    const v0, 0x30d54

    const-string/jumbo v1, "0"

    sget v2, Lcom/peel/ui/fo;->common_btn_stateful:I

    invoke-static {p0, v0, v1, v2, p6}, Lcom/peel/util/dw;->a(Landroid/content/Context;ILjava/lang/String;ILandroid/view/View$OnClickListener;)Landroid/widget/Button;

    move-result-object v8

    .line 1167
    const v1, 0x30d66

    const-string/jumbo v2, "Enter"

    sget v3, Lcom/peel/ui/fo;->common_btn_stateful:I

    sget v4, Lcom/peel/ui/fo;->remote_icon_enter:I

    move-object v0, p0

    move-object v5, p6

    invoke-static/range {v0 .. v5}, Lcom/peel/util/dw;->a(Landroid/content/Context;ILjava/lang/String;IILandroid/view/View$OnClickListener;)Landroid/widget/ImageButton;

    move-result-object v0

    .line 1169
    invoke-static {p0, p5}, Lcom/peel/util/dw;->a(Landroid/content/Context;I)Landroid/widget/LinearLayout;

    move-result-object v1

    .line 1171
    if-nez p3, :cond_2

    .line 1172
    sget-boolean v2, Lcom/peel/util/dw;->b:Z

    if-eqz v2, :cond_4

    .line 1173
    const/4 v2, 0x0

    invoke-virtual {v7, v2}, Landroid/widget/ImageButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1177
    :goto_1
    const/4 v2, 0x0

    invoke-virtual {v7, v2}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 1180
    :cond_2
    if-nez p4, :cond_3

    .line 1181
    sget-boolean v2, Lcom/peel/util/dw;->b:Z

    if-eqz v2, :cond_5

    .line 1182
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1186
    :goto_2
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 1189
    :cond_3
    invoke-virtual {v1, v7}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1190
    invoke-virtual {v1, v8}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1191
    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1192
    invoke-interface {v6, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 1175
    :cond_4
    sget v2, Lcom/peel/ui/fo;->keypad_blank:I

    invoke-virtual {v7, v2}, Landroid/widget/ImageButton;->setBackgroundResource(I)V

    goto :goto_1

    .line 1184
    :cond_5
    sget v2, Lcom/peel/ui/fo;->keypad_blank_reverse:I

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setBackgroundResource(I)V

    goto :goto_2
.end method

.method private static c(Landroid/content/Context;IIIILjava/lang/String;Landroid/view/View$OnClickListener;)Landroid/view/View;
    .locals 6

    .prologue
    .line 250
    new-instance v2, Landroid/widget/LinearLayout;

    invoke-direct {v2, p0}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 251
    const/4 v0, 0x1

    invoke-virtual {v2, v0}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 254
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v1, -0x2

    const/4 v3, -0x2

    const/high16 v4, 0x3f800000    # 1.0f

    invoke-direct {v0, v1, v3, v4}, Landroid/widget/LinearLayout$LayoutParams;-><init>(IIF)V

    .line 255
    const/4 v1, 0x1

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    .line 256
    invoke-virtual {v2, v0}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 258
    const/4 v0, 0x1

    .line 260
    sget-boolean v1, Lcom/peel/util/dw;->b:Z

    if-eqz v1, :cond_0

    .line 261
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, p4}, Landroid/content/res/Resources;->getResourceTypeName(I)Ljava/lang/String;

    move-result-object v1

    .line 262
    const-string/jumbo v3, "string"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 263
    const/4 v0, 0x0

    .line 268
    :cond_0
    if-eqz v0, :cond_1

    .line 269
    new-instance v1, Landroid/widget/ImageButton;

    invoke-direct {v1, p0}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    .line 274
    :goto_0
    invoke-virtual {v1, p5}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 275
    invoke-virtual {v1, p6}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 277
    sget-boolean v3, Lcom/peel/util/dw;->b:Z

    if-eqz v3, :cond_3

    .line 278
    sget v3, Lcom/peel/ui/fo;->common_btn_stateful:I

    invoke-virtual {v1, v3}, Landroid/view/View;->setBackgroundResource(I)V

    .line 279
    if-eqz v0, :cond_2

    move-object v0, v1

    .line 280
    check-cast v0, Landroid/widget/ImageButton;

    invoke-virtual {v0, p4}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 293
    :goto_1
    invoke-virtual {p0, p3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 294
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v3, -0x2

    const/4 v4, -0x2

    invoke-direct {v0, v3, v4}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 295
    const/16 v3, 0x45

    invoke-static {v3}, Lcom/peel/util/dw;->a(I)I

    move-result v3

    iput v3, v0, Landroid/widget/LinearLayout$LayoutParams;->width:I

    .line 296
    const/16 v3, 0x48

    invoke-static {v3}, Lcom/peel/util/dw;->a(I)I

    move-result v3

    iput v3, v0, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 297
    const/4 v3, 0x1

    iput v3, v0, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    .line 298
    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 299
    invoke-virtual {v2, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 301
    sget-boolean v0, Lcom/peel/util/dw;->b:Z

    if-eqz v0, :cond_4

    .line 317
    :goto_2
    return-object v2

    .line 271
    :cond_1
    new-instance v1, Landroid/widget/Button;

    invoke-direct {v1, p0}, Landroid/widget/Button;-><init>(Landroid/content/Context;)V

    goto :goto_0

    :cond_2
    move-object v0, v1

    .line 282
    check-cast v0, Landroid/widget/Button;

    invoke-virtual {p0, p4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    move-object v0, v1

    .line 283
    check-cast v0, Landroid/widget/Button;

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/peel/ui/fm;->remote_ctrl_pad_text:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setTextColor(I)V

    move-object v0, v1

    .line 284
    check-cast v0, Landroid/widget/Button;

    const/high16 v3, 0x41700000    # 15.0f

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setTextSize(F)V

    goto :goto_1

    .line 287
    :cond_3
    new-instance v0, Landroid/graphics/drawable/StateListDrawable;

    invoke-direct {v0}, Landroid/graphics/drawable/StateListDrawable;-><init>()V

    .line 288
    const/4 v3, 0x1

    new-array v3, v3, [I

    const/4 v4, 0x0

    const v5, 0x10100a7

    aput v5, v3, v4

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, p2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 289
    const/4 v3, 0x0

    new-array v3, v3, [I

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, p1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 290
    invoke-virtual {v1, v0}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto :goto_1

    .line 304
    :cond_4
    new-instance v0, Landroid/widget/TextView;

    invoke-direct {v0, p0}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 305
    sget v1, Lcom/peel/ui/fp;->btn_txt_id:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setId(I)V

    .line 306
    invoke-virtual {p0, p3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 307
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v3, Lcom/peel/ui/fm;->remote_ctrl_pad_text:I

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 308
    const/high16 v1, 0x41700000    # 15.0f

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextSize(F)V

    .line 309
    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMaxLines(I)V

    .line 310
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setGravity(I)V

    .line 311
    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v3, -0x1

    const/4 v4, -0x2

    invoke-direct {v1, v3, v4}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 312
    const/4 v3, 0x2

    invoke-static {v3}, Lcom/peel/util/dw;->a(I)I

    move-result v3

    iput v3, v1, Landroid/widget/LinearLayout$LayoutParams;->topMargin:I

    .line 313
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 314
    invoke-virtual {v2, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    goto/16 :goto_2
.end method
