.class Lcom/peel/util/ec;
.super Lcom/peel/util/t;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/peel/util/t",
        "<",
        "Ljava/util/List",
        "<",
        "Lcom/peel/content/listing/Listing;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/peel/util/eb;


# direct methods
.method constructor <init>(Lcom/peel/util/eb;I)V
    .locals 0

    .prologue
    .line 152
    iput-object p1, p0, Lcom/peel/util/ec;->a:Lcom/peel/util/eb;

    invoke-direct {p0, p2}, Lcom/peel/util/t;-><init>(I)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .prologue
    .line 155
    iget-boolean v0, p0, Lcom/peel/util/ec;->i:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/peel/util/ec;->a:Lcom/peel/util/eb;

    iget-object v0, v0, Lcom/peel/util/eb;->b:Lcom/peel/content/library/Library;

    invoke-virtual {v0}, Lcom/peel/content/library/Library;->f()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "live"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 180
    :cond_0
    iget-boolean v0, p0, Lcom/peel/util/ec;->i:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/peel/util/ec;->j:Ljava/lang/Object;

    if-nez v0, :cond_3

    .line 181
    :cond_1
    iget-object v0, p0, Lcom/peel/util/ec;->a:Lcom/peel/util/eb;

    iget-object v0, v0, Lcom/peel/util/eb;->i:Lcom/peel/util/dz;

    sget-object v1, Lcom/peel/util/ed;->d:Lcom/peel/util/ed;

    invoke-static {v0, v1}, Lcom/peel/util/dz;->a(Lcom/peel/util/dz;Lcom/peel/util/ed;)V

    .line 183
    iget-object v0, p0, Lcom/peel/util/ec;->a:Lcom/peel/util/eb;

    iget-object v0, v0, Lcom/peel/util/eb;->c:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    move-result v0

    iget-object v1, p0, Lcom/peel/util/ec;->a:Lcom/peel/util/eb;

    iget-object v1, v1, Lcom/peel/util/eb;->d:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-eq v0, v1, :cond_3

    .line 184
    invoke-static {}, Lcom/peel/util/dz;->b()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/peel/util/ec;->a:Lcom/peel/util/eb;

    iget-object v2, v2, Lcom/peel/util/eb;->b:Lcom/peel/content/library/Library;

    invoke-virtual {v2}, Lcom/peel/content/library/Library;->f()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "no data returned from library"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 199
    :cond_2
    :goto_0
    return-void

    .line 190
    :cond_3
    iget-object v0, p0, Lcom/peel/util/ec;->j:Ljava/lang/Object;

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/peel/util/ec;->i:Z

    if-eqz v0, :cond_2

    .line 191
    iget-object v3, p0, Lcom/peel/util/ec;->j:Ljava/lang/Object;

    check-cast v3, Ljava/util/List;

    .line 192
    iget-object v0, p0, Lcom/peel/util/ec;->a:Lcom/peel/util/eb;

    iget-object v0, v0, Lcom/peel/util/eb;->i:Lcom/peel/util/dz;

    iget-object v1, p0, Lcom/peel/util/ec;->a:Lcom/peel/util/eb;

    iget-object v1, v1, Lcom/peel/util/eb;->e:Ljava/util/concurrent/atomic/AtomicInteger;

    iget-object v2, p0, Lcom/peel/util/ec;->a:Lcom/peel/util/eb;

    iget-object v2, v2, Lcom/peel/util/eb;->b:Lcom/peel/content/library/Library;

    iget-object v4, p0, Lcom/peel/util/ec;->a:Lcom/peel/util/eb;

    iget-object v4, v4, Lcom/peel/util/eb;->f:Ljava/util/Map;

    iget-object v5, p0, Lcom/peel/util/ec;->a:Lcom/peel/util/eb;

    iget-object v5, v5, Lcom/peel/util/eb;->g:Ljava/util/Map;

    iget-object v6, p0, Lcom/peel/util/ec;->a:Lcom/peel/util/eb;

    iget-object v6, v6, Lcom/peel/util/eb;->h:Ljava/util/Map;

    invoke-static/range {v0 .. v6}, Lcom/peel/util/dz;->a(Lcom/peel/util/dz;Ljava/util/concurrent/atomic/AtomicInteger;Lcom/peel/content/library/Library;Ljava/util/List;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;)V

    .line 194
    iget-object v0, p0, Lcom/peel/util/ec;->a:Lcom/peel/util/eb;

    iget-object v0, v0, Lcom/peel/util/eb;->c:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    move-result v0

    iget-object v1, p0, Lcom/peel/util/ec;->a:Lcom/peel/util/eb;

    iget-object v1, v1, Lcom/peel/util/eb;->d:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ne v0, v1, :cond_2

    .line 195
    iget-object v0, p0, Lcom/peel/util/ec;->a:Lcom/peel/util/eb;

    iget-object v0, v0, Lcom/peel/util/eb;->i:Lcom/peel/util/dz;

    iget-object v1, p0, Lcom/peel/util/ec;->a:Lcom/peel/util/eb;

    iget-object v1, v1, Lcom/peel/util/eb;->f:Ljava/util/Map;

    iget-object v2, p0, Lcom/peel/util/ec;->a:Lcom/peel/util/eb;

    iget-object v2, v2, Lcom/peel/util/eb;->g:Ljava/util/Map;

    iget-object v3, p0, Lcom/peel/util/ec;->a:Lcom/peel/util/eb;

    iget-object v3, v3, Lcom/peel/util/eb;->h:Ljava/util/Map;

    iget-object v4, p0, Lcom/peel/util/ec;->a:Lcom/peel/util/eb;

    iget-object v4, v4, Lcom/peel/util/eb;->e:Ljava/util/concurrent/atomic/AtomicInteger;

    iget-object v5, p0, Lcom/peel/util/ec;->a:Lcom/peel/util/eb;

    iget-object v5, v5, Lcom/peel/util/eb;->a:Ljava/lang/String;

    invoke-static/range {v0 .. v5}, Lcom/peel/util/dz;->a(Lcom/peel/util/dz;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;Ljava/util/concurrent/atomic/AtomicInteger;Ljava/lang/String;)V

    .line 196
    iget-object v0, p0, Lcom/peel/util/ec;->a:Lcom/peel/util/eb;

    iget-object v0, v0, Lcom/peel/util/eb;->i:Lcom/peel/util/dz;

    invoke-static {v0}, Lcom/peel/util/dz;->a(Lcom/peel/util/dz;)V

    goto :goto_0
.end method
