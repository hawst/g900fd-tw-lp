.class Lcom/peel/util/eb;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic b:Lcom/peel/content/library/Library;

.field final synthetic c:Ljava/util/concurrent/atomic/AtomicInteger;

.field final synthetic d:Ljava/util/List;

.field final synthetic e:Ljava/util/concurrent/atomic/AtomicInteger;

.field final synthetic f:Ljava/util/Map;

.field final synthetic g:Ljava/util/Map;

.field final synthetic h:Ljava/util/Map;

.field final synthetic i:Lcom/peel/util/dz;


# direct methods
.method constructor <init>(Lcom/peel/util/dz;Ljava/lang/String;Lcom/peel/content/library/Library;Ljava/util/concurrent/atomic/AtomicInteger;Ljava/util/List;Ljava/util/concurrent/atomic/AtomicInteger;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;)V
    .locals 0

    .prologue
    .line 137
    iput-object p1, p0, Lcom/peel/util/eb;->i:Lcom/peel/util/dz;

    iput-object p2, p0, Lcom/peel/util/eb;->a:Ljava/lang/String;

    iput-object p3, p0, Lcom/peel/util/eb;->b:Lcom/peel/content/library/Library;

    iput-object p4, p0, Lcom/peel/util/eb;->c:Ljava/util/concurrent/atomic/AtomicInteger;

    iput-object p5, p0, Lcom/peel/util/eb;->d:Ljava/util/List;

    iput-object p6, p0, Lcom/peel/util/eb;->e:Ljava/util/concurrent/atomic/AtomicInteger;

    iput-object p7, p0, Lcom/peel/util/eb;->f:Ljava/util/Map;

    iput-object p8, p0, Lcom/peel/util/eb;->g:Ljava/util/Map;

    iput-object p9, p0, Lcom/peel/util/eb;->h:Ljava/util/Map;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 140
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 141
    const-string/jumbo v0, "path"

    const-string/jumbo v2, "listing/genre"

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 142
    const-string/jumbo v0, "window"

    invoke-virtual {v1, v0, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 143
    const-string/jumbo v0, "cacheWindow"

    invoke-virtual {v1, v0, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 145
    :try_start_0
    sget-object v0, Lcom/peel/util/x;->c:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/text/SimpleDateFormat;

    iget-object v2, p0, Lcom/peel/util/eb;->a:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    .line 146
    const-string/jumbo v0, "start"

    invoke-virtual {v1, v0, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 150
    :goto_0
    const-string/jumbo v0, "room"

    invoke-static {}, Lcom/peel/content/a;->a()Lcom/peel/data/ContentRoom;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 152
    iget-object v0, p0, Lcom/peel/util/eb;->b:Lcom/peel/content/library/Library;

    new-instance v2, Lcom/peel/util/ec;

    const/4 v3, 0x0

    invoke-direct {v2, p0, v3}, Lcom/peel/util/ec;-><init>(Lcom/peel/util/eb;I)V

    invoke-virtual {v0, v1, v2}, Lcom/peel/content/library/Library;->a(Landroid/os/Bundle;Lcom/peel/util/t;)V

    .line 201
    return-void

    .line 147
    :catch_0
    move-exception v0

    .line 148
    invoke-static {}, Lcom/peel/util/dz;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {}, Lcom/peel/util/dz;->b()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method
