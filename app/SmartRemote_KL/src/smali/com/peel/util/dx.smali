.class public Lcom/peel/util/dx;
.super Ljava/lang/Object;


# static fields
.field private static final a:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 20
    const-class v0, Lcom/peel/util/dx;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/peel/util/dx;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(IILandroid/content/Intent;Landroid/app/Activity;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v3, -0x1

    const/16 v4, 0x1f43

    const/16 v0, 0x1f42

    const/4 v5, 0x0

    .line 23
    sget-object v1, Lcom/peel/c/a;->b:Lcom/peel/c/e;

    invoke-static {v1}, Lcom/peel/c/a;->a(Lcom/peel/c/e;)Ljava/lang/Object;

    move-result-object v1

    sget-object v2, Lcom/peel/c/b;->a:Lcom/peel/c/b;

    if-ne v1, v2, :cond_2

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x15

    if-ge v1, v2, :cond_2

    .line 25
    if-ne p1, v3, :cond_1

    if-eq p0, v4, :cond_0

    const/16 v0, 0x1f44

    if-ne p0, v0, :cond_1

    .line 28
    :cond_0
    if-eqz p2, :cond_1

    const-string/jumbo v0, "is_agree_to_disclaimer"

    invoke-virtual {p2, v0, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 30
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const-string/jumbo v1, "api_server_url"

    aput-object v1, v0, v5

    const-string/jumbo v1, "auth_server_url"

    aput-object v1, v0, v6

    .line 32
    new-instance v1, Landroid/content/Intent;

    const-string/jumbo v2, "com.msc.action.ACCESSTOKEN_V02_REQUEST"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 34
    const-string/jumbo v2, "client_id"

    const-string/jumbo v3, "8z1ac1q7yr"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 35
    const-string/jumbo v2, "client_secret"

    const-string/jumbo v3, "FFE99F19500083A06FE04BFC1786CBE0"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 36
    const-string/jumbo v2, "mypackage"

    invoke-virtual {p3}, Landroid/app/Activity;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 37
    const-string/jumbo v2, "OSP_VER"

    const-string/jumbo v3, "OSP_02"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 39
    const-string/jumbo v2, "MODE"

    const-string/jumbo v3, "BACKGROUND"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 41
    const-string/jumbo v2, "additional"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 43
    invoke-virtual {p3, v1}, Landroid/app/Activity;->sendBroadcast(Landroid/content/Intent;)V

    .line 94
    :cond_1
    :goto_0
    return-void

    .line 47
    :cond_2
    if-ne p1, v3, :cond_1

    .line 48
    if-eq p0, v4, :cond_3

    const/16 v1, 0x1f44

    if-ne p0, v1, :cond_5

    .line 50
    :cond_3
    new-instance v1, Landroid/content/Intent;

    const-string/jumbo v2, "com.msc.action.samsungaccount.REQUEST_CHECKLIST_VALIDATION"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 51
    const-string/jumbo v2, "client_id"

    const-string/jumbo v3, "8z1ac1q7yr"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 52
    const-string/jumbo v2, "client_secret"

    const-string/jumbo v3, "FFE99F19500083A06FE04BFC1786CBE0"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 53
    const-string/jumbo v2, "validation_result_only"

    invoke-virtual {v1, v2, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 54
    const-string/jumbo v2, "progress_theme"

    const-string/jumbo v3, "dark"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 56
    if-eqz p3, :cond_1

    .line 57
    if-ne p0, v4, :cond_4

    :goto_1
    invoke-virtual {p3, v1, v0}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    :cond_4
    const/16 v0, 0x1f46

    goto :goto_1

    .line 62
    :cond_5
    if-eq p0, v0, :cond_6

    const/16 v1, 0x1f46

    if-ne p0, v1, :cond_8

    .line 65
    :cond_6
    sget-object v1, Lcom/peel/util/dx;->a:Ljava/lang/String;

    const-string/jumbo v2, "Agreement complete"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 67
    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/String;

    const-string/jumbo v2, "user_id"

    aput-object v2, v1, v5

    const-string/jumbo v2, "login_id"

    aput-object v2, v1, v6

    const/4 v2, 0x2

    const-string/jumbo v3, "api_server_url"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string/jumbo v3, "auth_server_url"

    aput-object v3, v1, v2

    .line 69
    new-instance v2, Landroid/content/Intent;

    const-string/jumbo v3, "com.msc.action.samsungaccount.REQUEST_ACCESSTOKEN"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 71
    const-string/jumbo v3, "client_id"

    const-string/jumbo v4, "8z1ac1q7yr"

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 72
    const-string/jumbo v3, "client_secret"

    const-string/jumbo v4, "FFE99F19500083A06FE04BFC1786CBE0"

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 73
    const-string/jumbo v3, "validation_result_only"

    invoke-virtual {v2, v3, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 75
    const-string/jumbo v3, "additional"

    invoke-virtual {v2, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 76
    const-string/jumbo v1, "progress_theme"

    const-string/jumbo v3, "dark"

    invoke-virtual {v2, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 78
    if-ne p0, v0, :cond_7

    const/16 v0, 0x1f41

    :goto_2
    invoke-virtual {p3, v2, v0}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_0

    :cond_7
    const/16 v0, 0x1f45

    goto :goto_2

    .line 83
    :cond_8
    const/16 v0, 0x1f41

    if-eq p0, v0, :cond_9

    const/16 v0, 0x1f45

    if-ne p0, v0, :cond_1

    .line 86
    :cond_9
    const-string/jumbo v0, "social_accounts_setup"

    invoke-virtual {p3, v0, v5}, Landroid/app/Activity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 87
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string/jumbo v2, "samsung_access_token"

    const-string/jumbo v3, "access_token"

    invoke-virtual {p2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 88
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string/jumbo v2, "samsungid"

    const-string/jumbo v3, "user_id"

    invoke-virtual {p2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 89
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string/jumbo v2, "samsung_account_name"

    const-string/jumbo v3, "login_id"

    invoke-virtual {p2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 90
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v1, "samsung_account_email"

    const-string/jumbo v2, "login_id"

    invoke-virtual {p2, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    goto/16 :goto_0
.end method
