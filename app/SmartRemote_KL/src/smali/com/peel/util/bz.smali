.class final Lcom/peel/util/bz;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic a:Landroid/content/SharedPreferences;

.field final synthetic b:Lcom/peel/d/i;

.field final synthetic c:Landroid/content/Context;


# direct methods
.method constructor <init>(Landroid/content/SharedPreferences;Lcom/peel/d/i;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1074
    iput-object p1, p0, Lcom/peel/util/bz;->a:Landroid/content/SharedPreferences;

    iput-object p2, p0, Lcom/peel/util/bz;->b:Lcom/peel/d/i;

    iput-object p3, p0, Lcom/peel/util/bz;->c:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4

    .prologue
    .line 1076
    const/4 v0, 0x1

    sput-boolean v0, Lcom/peel/util/b/a;->a:Z

    .line 1077
    iget-object v0, p0, Lcom/peel/util/bz;->a:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v1, "wlan"

    invoke-static {}, Lcom/peel/util/bx;->e()Z

    move-result v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 1079
    iget-object v0, p0, Lcom/peel/util/bz;->b:Lcom/peel/d/i;

    if-eqz v0, :cond_0

    .line 1080
    iget-object v0, p0, Lcom/peel/util/bz;->b:Lcom/peel/d/i;

    invoke-virtual {v0}, Lcom/peel/d/i;->c()Landroid/support/v4/app/ae;

    move-result-object v0

    invoke-static {v0}, Lcom/peel/d/e;->a(Landroid/support/v4/app/ae;)Lcom/peel/d/u;

    move-result-object v0

    .line 1081
    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/peel/util/bz;->c:Landroid/content/Context;

    invoke-static {v1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    const-string/jumbo v2, "is_setup_complete"

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Lcom/peel/d/u;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1082
    invoke-virtual {v0}, Lcom/peel/d/u;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/ae;->finish()V

    .line 1085
    :cond_0
    return-void
.end method
