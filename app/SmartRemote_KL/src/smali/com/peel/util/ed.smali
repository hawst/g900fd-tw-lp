.class public final enum Lcom/peel/util/ed;
.super Ljava/lang/Enum;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/peel/util/ed;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/peel/util/ed;

.field public static final enum b:Lcom/peel/util/ed;

.field public static final enum c:Lcom/peel/util/ed;

.field public static final enum d:Lcom/peel/util/ed;

.field private static final synthetic e:[Lcom/peel/util/ed;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 22
    new-instance v0, Lcom/peel/util/ed;

    const-string/jumbo v1, "IdleGet"

    invoke-direct {v0, v1, v2}, Lcom/peel/util/ed;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/peel/util/ed;->a:Lcom/peel/util/ed;

    new-instance v0, Lcom/peel/util/ed;

    const-string/jumbo v1, "LoadingGet"

    invoke-direct {v0, v1, v3}, Lcom/peel/util/ed;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/peel/util/ed;->b:Lcom/peel/util/ed;

    new-instance v0, Lcom/peel/util/ed;

    const-string/jumbo v1, "LoadedGet"

    invoke-direct {v0, v1, v4}, Lcom/peel/util/ed;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/peel/util/ed;->c:Lcom/peel/util/ed;

    new-instance v0, Lcom/peel/util/ed;

    const-string/jumbo v1, "NoDataGet"

    invoke-direct {v0, v1, v5}, Lcom/peel/util/ed;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/peel/util/ed;->d:Lcom/peel/util/ed;

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/peel/util/ed;

    sget-object v1, Lcom/peel/util/ed;->a:Lcom/peel/util/ed;

    aput-object v1, v0, v2

    sget-object v1, Lcom/peel/util/ed;->b:Lcom/peel/util/ed;

    aput-object v1, v0, v3

    sget-object v1, Lcom/peel/util/ed;->c:Lcom/peel/util/ed;

    aput-object v1, v0, v4

    sget-object v1, Lcom/peel/util/ed;->d:Lcom/peel/util/ed;

    aput-object v1, v0, v5

    sput-object v0, Lcom/peel/util/ed;->e:[Lcom/peel/util/ed;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 22
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/peel/util/ed;
    .locals 1

    .prologue
    .line 22
    const-class v0, Lcom/peel/util/ed;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/peel/util/ed;

    return-object v0
.end method

.method public static values()[Lcom/peel/util/ed;
    .locals 1

    .prologue
    .line 22
    sget-object v0, Lcom/peel/util/ed;->e:[Lcom/peel/util/ed;

    invoke-virtual {v0}, [Lcom/peel/util/ed;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/peel/util/ed;

    return-object v0
.end method
