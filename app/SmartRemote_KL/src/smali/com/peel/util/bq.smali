.class public Lcom/peel/util/bq;
.super Ljava/lang/Object;


# static fields
.field private static final a:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 37
    const-class v0, Lcom/peel/util/bq;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/peel/util/bq;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/peel/content/listing/Listing;Z)Landroid/widget/RemoteViews;
    .locals 10

    .prologue
    const/16 v9, 0x8

    const/high16 v7, 0x10000000

    const/16 v6, 0x15

    .line 143
    check-cast p1, Lcom/peel/content/listing/LiveListing;

    .line 144
    new-instance v8, Landroid/widget/RemoteViews;

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    if-eqz p2, :cond_3

    sget v0, Lcom/peel/ui/fq;->notification_playing_collapsed:I

    :goto_0
    invoke-direct {v8, v1, v0}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    .line 146
    new-instance v2, Landroid/content/Intent;

    const-class v0, Lcom/peel/receiver/ChannelUpdateReceiver;

    invoke-direct {v2, p0, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 147
    const-string/jumbo v0, "com.peel.receiver.action.TUNE_IN"

    invoke-virtual {v2, v0}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 148
    const-string/jumbo v0, "com.peel.receiver.extra.TUNE_IN"

    invoke-virtual {p1}, Lcom/peel/content/listing/LiveListing;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 149
    const-string/jumbo v0, "episode_id"

    invoke-virtual {p1}, Lcom/peel/content/listing/LiveListing;->g()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 150
    const-string/jumbo v0, "show_id"

    invoke-virtual {p1}, Lcom/peel/content/listing/LiveListing;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 151
    if-eqz p2, :cond_5

    sget v0, Lcom/peel/ui/fp;->noti_btn_collapsed_watchon:I

    move v1, v0

    :goto_1
    if-eqz p2, :cond_6

    sget v0, Lcom/peel/ui/fp;->noti_btn_collapsed_watchon:I

    .line 152
    :goto_2
    invoke-static {p0, v0, v2, v7}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    .line 151
    invoke-virtual {v8, v1, v0}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 155
    invoke-virtual {p1}, Lcom/peel/content/listing/LiveListing;->y()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_b

    .line 158
    if-eqz p2, :cond_9

    .line 159
    :try_start_0
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v0, v6, :cond_8

    .line 160
    sget v1, Lcom/peel/ui/fp;->noti_show_image_collapsed:I

    .line 161
    invoke-static {p0}, Lcom/squareup/picasso/Picasso;->with(Landroid/content/Context;)Lcom/squareup/picasso/Picasso;

    move-result-object v0

    invoke-virtual {p1}, Lcom/peel/content/listing/LiveListing;->y()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/squareup/picasso/Picasso;->load(Ljava/lang/String;)Lcom/squareup/picasso/RequestCreator;

    move-result-object v2

    sget-object v0, Lcom/peel/c/a;->b:Lcom/peel/c/e;

    invoke-static {v0}, Lcom/peel/c/a;->a(Lcom/peel/c/e;)Ljava/lang/Object;

    move-result-object v0

    sget-object v3, Lcom/peel/c/b;->c:Lcom/peel/c/b;

    if-ne v0, v3, :cond_7

    sget v0, Lcom/peel/ui/fo;->l_widget_place_holder_psr:I

    :goto_3
    invoke-virtual {v2, v0}, Lcom/squareup/picasso/RequestCreator;->placeholder(I)Lcom/squareup/picasso/RequestCreator;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/picasso/RequestCreator;->get()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 160
    invoke-virtual {v8, v1, v0}, Landroid/widget/RemoteViews;->setImageViewBitmap(ILandroid/graphics/Bitmap;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 193
    :goto_4
    new-instance v2, Landroid/content/Intent;

    const-class v0, Lcom/peel/main/Home;

    invoke-direct {v2, p0, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 194
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "peel://tunein/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/peel/content/listing/LiveListing;->g()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 195
    const-string/jumbo v0, "action"

    const-string/jumbo v1, "default"

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 196
    const-string/jumbo v0, "channel_id"

    invoke-virtual {p1}, Lcom/peel/content/listing/LiveListing;->q()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 197
    const-string/jumbo v0, "starttime"

    invoke-virtual {p1}, Lcom/peel/content/listing/LiveListing;->o()J

    move-result-wide v4

    invoke-virtual {v2, v0, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 198
    const-string/jumbo v0, "episode_id"

    invoke-virtual {p1}, Lcom/peel/content/listing/LiveListing;->g()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 199
    const-string/jumbo v0, "show_id"

    invoke-virtual {p1}, Lcom/peel/content/listing/LiveListing;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 200
    const-string/jumbo v0, "noti_event_id"

    const/16 v1, 0x2eea

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 201
    if-eqz p2, :cond_10

    sget v0, Lcom/peel/ui/fp;->noti_show_image_collapsed:I

    move v1, v0

    :goto_5
    if-eqz p2, :cond_11

    sget v0, Lcom/peel/ui/fp;->noti_show_image_collapsed:I

    .line 202
    :goto_6
    invoke-static {p0, v0, v2, v7}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    .line 201
    invoke-virtual {v8, v1, v0}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 203
    if-eqz p2, :cond_12

    sget v0, Lcom/peel/ui/fp;->noti_show_title_collapsed:I

    move v1, v0

    :goto_7
    if-eqz p2, :cond_13

    sget v0, Lcom/peel/ui/fp;->noti_show_title_collapsed:I

    .line 204
    :goto_8
    invoke-static {p0, v0, v2, v7}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    .line 203
    invoke-virtual {v8, v1, v0}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 206
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 207
    const-string/jumbo v1, "ro.csc.country_code"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 209
    const-string/jumbo v1, "CN"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string/jumbo v1, "china"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 210
    :cond_0
    sget v0, Lcom/peel/ui/fp;->noti_btn_twitter:I

    invoke-virtual {v8, v0, v9}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 211
    sget v0, Lcom/peel/ui/fp;->divider3:I

    invoke-virtual {v8, v0, v9}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 214
    :cond_1
    if-nez p2, :cond_14

    .line 217
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/peel/receiver/ChannelUpdateReceiver;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 218
    const-string/jumbo v1, "com.peel.receiver.action.CLOSE_NOTIFICATION"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 219
    sget v1, Lcom/peel/ui/fp;->noti_btn_close:I

    sget v2, Lcom/peel/ui/fp;->noti_btn_close:I

    invoke-static {p0, v2, v0, v7}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    invoke-virtual {v8, v1, v0}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 221
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/peel/main/Home;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 222
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "peel://tunein/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/peel/content/listing/LiveListing;->g()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 223
    const-string/jumbo v1, "action"

    const-string/jumbo v2, "tweet"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 224
    const-string/jumbo v1, "episode_id"

    invoke-virtual {p1}, Lcom/peel/content/listing/LiveListing;->g()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 225
    const-string/jumbo v1, "show_id"

    invoke-virtual {p1}, Lcom/peel/content/listing/LiveListing;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 226
    const-string/jumbo v1, "noti_event_id"

    const/16 v2, 0x2eec

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 228
    const-string/jumbo v1, "com.peel.util.NotificationUtil.action.TWEET"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 229
    sget v1, Lcom/peel/ui/fp;->noti_btn_twitter:I

    sget v2, Lcom/peel/ui/fp;->noti_btn_twitter:I

    invoke-static {p0, v2, v0, v7}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    invoke-virtual {v8, v1, v0}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 231
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/peel/receiver/ChannelUpdateReceiver;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 232
    const-string/jumbo v1, "com.peel.receiver.action.CHANNEL_UPDATE"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 234
    const-string/jumbo v1, "previous_channel"

    const-string/jumbo v2, "true"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 235
    sget v1, Lcom/peel/ui/fp;->noti_btn_prev:I

    sget v2, Lcom/peel/ui/fp;->noti_btn_prev:I

    invoke-static {p0, v2, v0, v7}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    invoke-virtual {v8, v1, v0}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 249
    :cond_2
    :goto_9
    sget v0, Lcom/peel/ui/fp;->noti_show_channel:I

    new-instance v1, Ljava/lang/StringBuilder;

    sget v2, Lcom/peel/ui/ft;->noti_playing_channel:I

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v2, " "

    .line 250
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/peel/content/listing/LiveListing;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 249
    invoke-virtual {v8, v0, v1}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 251
    if-eqz p2, :cond_15

    sget v0, Lcom/peel/ui/fp;->noti_show_title_collapsed:I

    :goto_a
    invoke-virtual {p1}, Lcom/peel/content/listing/LiveListing;->v()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_16

    invoke-virtual {p1}, Lcom/peel/content/listing/LiveListing;->l()Ljava/lang/String;

    move-result-object v1

    :goto_b
    invoke-virtual {v8, v0, v1}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 253
    sget v0, Lcom/peel/ui/fp;->noti_show_status:I

    new-instance v1, Ljava/lang/StringBuilder;

    sget v2, Lcom/peel/ui/ft;->noti_playing_endtime:I

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v2, " "

    .line 254
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    .line 255
    invoke-virtual {p1}, Lcom/peel/content/listing/LiveListing;->o()J

    move-result-wide v2

    invoke-virtual {p1}, Lcom/peel/content/listing/LiveListing;->j()J

    move-result-wide v4

    .line 256
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    move-object v1, p0

    .line 255
    invoke-static/range {v1 .. v7}, Lcom/peel/util/bx;->a(Landroid/content/Context;JJJ)Ljava/lang/String;

    move-result-object v1

    .line 257
    invoke-virtual {v1}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v1

    .line 255
    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 253
    invoke-virtual {v8, v0, v1}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 259
    return-object v8

    .line 144
    :cond_3
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v0, v6, :cond_4

    sget v0, Lcom/peel/ui/fq;->notification_playing_new:I

    goto/16 :goto_0

    :cond_4
    sget v0, Lcom/peel/ui/fq;->notification_playing:I

    goto/16 :goto_0

    .line 151
    :cond_5
    sget v0, Lcom/peel/ui/fp;->noti_btn_watchon:I

    move v1, v0

    goto/16 :goto_1

    :cond_6
    sget v0, Lcom/peel/ui/fp;->noti_btn_watchon:I

    goto/16 :goto_2

    .line 161
    :cond_7
    :try_start_1
    sget v0, Lcom/peel/ui/fo;->l_widget_place_holder:I

    goto/16 :goto_3

    .line 163
    :cond_8
    sget v0, Lcom/peel/ui/fp;->noti_show_image_collapsed:I

    invoke-static {p0}, Lcom/squareup/picasso/Picasso;->with(Landroid/content/Context;)Lcom/squareup/picasso/Picasso;

    move-result-object v1

    invoke-virtual {p1}, Lcom/peel/content/listing/LiveListing;->y()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/picasso/Picasso;->load(Ljava/lang/String;)Lcom/squareup/picasso/RequestCreator;

    move-result-object v1

    sget v2, Lcom/peel/ui/fo;->genre_placeholder:I

    invoke-virtual {v1, v2}, Lcom/squareup/picasso/RequestCreator;->placeholder(I)Lcom/squareup/picasso/RequestCreator;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/picasso/RequestCreator;->get()Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v8, v0, v1}, Landroid/widget/RemoteViews;->setImageViewBitmap(ILandroid/graphics/Bitmap;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_4

    .line 173
    :catch_0
    move-exception v0

    .line 174
    sget-object v0, Lcom/peel/util/bq;->a:Ljava/lang/String;

    const-string/jumbo v1, "Cannot load image"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_4

    .line 167
    :cond_9
    :try_start_2
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v0, v6, :cond_a

    sget-object v0, Lcom/peel/c/a;->b:Lcom/peel/c/e;

    invoke-static {v0}, Lcom/peel/c/a;->a(Lcom/peel/c/e;)Ljava/lang/Object;

    move-result-object v0

    sget-object v1, Lcom/peel/c/b;->c:Lcom/peel/c/b;

    if-ne v0, v1, :cond_a

    .line 168
    sget v0, Lcom/peel/ui/fp;->noti_show_image:I

    invoke-static {p0}, Lcom/squareup/picasso/Picasso;->with(Landroid/content/Context;)Lcom/squareup/picasso/Picasso;

    move-result-object v1

    invoke-virtual {p1}, Lcom/peel/content/listing/LiveListing;->y()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/picasso/Picasso;->load(Ljava/lang/String;)Lcom/squareup/picasso/RequestCreator;

    move-result-object v1

    sget v2, Lcom/peel/ui/fo;->genre_placeholder_big_psr_l:I

    invoke-virtual {v1, v2}, Lcom/squareup/picasso/RequestCreator;->placeholder(I)Lcom/squareup/picasso/RequestCreator;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/picasso/RequestCreator;->get()Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v8, v0, v1}, Landroid/widget/RemoteViews;->setImageViewBitmap(ILandroid/graphics/Bitmap;)V

    goto/16 :goto_4

    .line 170
    :cond_a
    sget v0, Lcom/peel/ui/fp;->noti_show_image:I

    invoke-static {p0}, Lcom/squareup/picasso/Picasso;->with(Landroid/content/Context;)Lcom/squareup/picasso/Picasso;

    move-result-object v1

    invoke-virtual {p1}, Lcom/peel/content/listing/LiveListing;->y()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/picasso/Picasso;->load(Ljava/lang/String;)Lcom/squareup/picasso/RequestCreator;

    move-result-object v1

    sget v2, Lcom/peel/ui/fo;->genre_placeholder:I

    invoke-virtual {v1, v2}, Lcom/squareup/picasso/RequestCreator;->placeholder(I)Lcom/squareup/picasso/RequestCreator;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/picasso/RequestCreator;->get()Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v8, v0, v1}, Landroid/widget/RemoteViews;->setImageViewBitmap(ILandroid/graphics/Bitmap;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    goto/16 :goto_4

    .line 177
    :cond_b
    if-eqz p2, :cond_e

    .line 178
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v0, v6, :cond_d

    .line 179
    sget v1, Lcom/peel/ui/fp;->noti_show_image_collapsed:I

    sget-object v0, Lcom/peel/c/a;->b:Lcom/peel/c/e;

    invoke-static {v0}, Lcom/peel/c/a;->a(Lcom/peel/c/e;)Ljava/lang/Object;

    move-result-object v0

    sget-object v2, Lcom/peel/c/b;->c:Lcom/peel/c/b;

    if-ne v0, v2, :cond_c

    sget v0, Lcom/peel/ui/fo;->l_widget_place_holder_psr:I

    :goto_c
    invoke-virtual {v8, v1, v0}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    goto/16 :goto_4

    :cond_c
    sget v0, Lcom/peel/ui/fo;->l_widget_place_holder:I

    goto :goto_c

    .line 181
    :cond_d
    sget v0, Lcom/peel/ui/fp;->noti_show_image_collapsed:I

    sget v1, Lcom/peel/ui/fo;->genre_placeholder:I

    invoke-virtual {v8, v0, v1}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    goto/16 :goto_4

    .line 185
    :cond_e
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v0, v6, :cond_f

    sget-object v0, Lcom/peel/c/a;->b:Lcom/peel/c/e;

    invoke-static {v0}, Lcom/peel/c/a;->a(Lcom/peel/c/e;)Ljava/lang/Object;

    move-result-object v0

    sget-object v1, Lcom/peel/c/b;->c:Lcom/peel/c/b;

    if-ne v0, v1, :cond_f

    .line 186
    sget v0, Lcom/peel/ui/fp;->noti_show_image:I

    sget v1, Lcom/peel/ui/fo;->genre_placeholder_big_psr_l:I

    invoke-virtual {v8, v0, v1}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    goto/16 :goto_4

    .line 188
    :cond_f
    sget v0, Lcom/peel/ui/fp;->noti_show_image:I

    sget v1, Lcom/peel/ui/fo;->genre_placeholder:I

    invoke-virtual {v8, v0, v1}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    goto/16 :goto_4

    .line 201
    :cond_10
    sget v0, Lcom/peel/ui/fp;->noti_show_image:I

    move v1, v0

    goto/16 :goto_5

    :cond_11
    sget v0, Lcom/peel/ui/fp;->noti_show_image:I

    goto/16 :goto_6

    .line 203
    :cond_12
    sget v0, Lcom/peel/ui/fp;->noti_show_title:I

    move v1, v0

    goto/16 :goto_7

    :cond_13
    sget v0, Lcom/peel/ui/fp;->noti_show_title:I

    goto/16 :goto_8

    .line 238
    :cond_14
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-ge v0, v6, :cond_2

    sget-object v0, Lcom/peel/c/a;->b:Lcom/peel/c/e;

    invoke-static {v0}, Lcom/peel/c/a;->a(Lcom/peel/c/e;)Ljava/lang/Object;

    move-result-object v0

    sget-object v1, Lcom/peel/c/b;->c:Lcom/peel/c/b;

    if-ne v0, v1, :cond_2

    .line 239
    sget v0, Lcom/peel/ui/fp;->noti_playing_widget_bg_img:I

    const/4 v1, 0x0

    invoke-virtual {v8, v0, v1}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 240
    sget v0, Lcom/peel/ui/fp;->noti_show_title_collapsed:I

    const/4 v1, -0x1

    invoke-virtual {v8, v0, v1}, Landroid/widget/RemoteViews;->setTextColor(II)V

    .line 241
    sget v0, Lcom/peel/ui/fp;->noti_show_status:I

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/peel/ui/fm;->now_playing_widget_text:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v8, v0, v1}, Landroid/widget/RemoteViews;->setTextColor(II)V

    .line 242
    sget v0, Lcom/peel/ui/fp;->noti_show_channel:I

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/peel/ui/fm;->now_playing_widget_text:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v8, v0, v1}, Landroid/widget/RemoteViews;->setTextColor(II)V

    .line 243
    sget v0, Lcom/peel/ui/fp;->noti_btn_watchon:I

    sget v1, Lcom/peel/ui/fo;->widget_wot_kitkat_psr_stateful:I

    invoke-virtual {v8, v0, v1}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    .line 244
    sget v0, Lcom/peel/ui/fp;->noti_btn_collapsed_watchon:I

    sget v1, Lcom/peel/ui/fo;->noti_watchon_icon_psr_kitkat:I

    invoke-virtual {v8, v0, v1}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    goto/16 :goto_9

    .line 251
    :cond_15
    sget v0, Lcom/peel/ui/fp;->noti_show_title:I

    goto/16 :goto_a

    .line 252
    :cond_16
    invoke-virtual {p1}, Lcom/peel/content/listing/LiveListing;->v()Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_b
.end method

.method static synthetic a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 36
    sget-object v0, Lcom/peel/util/bq;->a:Ljava/lang/String;

    return-object v0
.end method

.method public static a(Landroid/content/Context;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 72
    const-string/jumbo v0, "live"

    invoke-static {v0}, Lcom/peel/content/a;->c(Ljava/lang/String;)Lcom/peel/content/library/Library;

    move-result-object v0

    check-cast v0, Lcom/peel/content/library/LiveLibrary;

    .line 73
    if-nez v0, :cond_0

    invoke-virtual {v0}, Lcom/peel/content/library/LiveLibrary;->n()Lcom/peel/content/listing/Listing;

    move-result-object v1

    if-nez v1, :cond_0

    .line 139
    :goto_0
    return-void

    .line 78
    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-string/jumbo v2, "widget_pref"

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    const-string/jumbo v2, "notification"

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 79
    sget-object v0, Lcom/peel/util/v;->a:Lcom/peel/util/v;

    invoke-virtual {v0}, Lcom/peel/util/v;->a()V

    goto :goto_0

    .line 83
    :cond_1
    sget-object v1, Lcom/peel/util/bq;->a:Ljava/lang/String;

    const-string/jumbo v2, "Display playing status notification"

    new-instance v3, Lcom/peel/util/bt;

    invoke-direct {v3, p0, v0}, Lcom/peel/util/bt;-><init>(Landroid/content/Context;Lcom/peel/content/library/LiveLibrary;)V

    invoke-static {v1, v2, v3}, Lcom/peel/util/i;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 46
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 47
    const-string/jumbo v0, "#"

    invoke-virtual {p0, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 48
    sget-object v1, Lcom/peel/util/bq;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "send notification tracking request in background:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/peel/util/br;

    invoke-direct {v3, v0}, Lcom/peel/util/br;-><init>([Ljava/lang/String;)V

    invoke-static {v1, v2, v3}, Lcom/peel/util/i;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    .line 68
    :cond_0
    return-void
.end method
