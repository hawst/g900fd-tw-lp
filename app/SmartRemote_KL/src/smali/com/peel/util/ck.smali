.class Lcom/peel/util/ck;
.super Lcom/peel/util/t;


# instance fields
.field final synthetic a:Lcom/peel/util/cj;


# direct methods
.method constructor <init>(Lcom/peel/util/cj;)V
    .locals 0

    .prologue
    .line 1924
    iput-object p1, p0, Lcom/peel/util/ck;->a:Lcom/peel/util/cj;

    invoke-direct {p0}, Lcom/peel/util/t;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 12

    .prologue
    const/4 v4, 0x0

    const/4 v11, 0x0

    .line 1926
    iget-boolean v0, p0, Lcom/peel/util/ck;->i:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/peel/util/ck;->j:Ljava/lang/Object;

    if-nez v0, :cond_1

    .line 1928
    :cond_0
    iget-object v0, p0, Lcom/peel/util/ck;->a:Lcom/peel/util/cj;

    iget-object v0, v0, Lcom/peel/util/cj;->b:Lcom/peel/util/t;

    invoke-virtual {v0, v4, v11, v11}, Lcom/peel/util/t;->a(ZLjava/lang/Object;Ljava/lang/String;)V

    .line 1976
    :goto_0
    return-void

    .line 1933
    :cond_1
    :try_start_0
    invoke-static {}, Lcom/peel/util/bx;->f()Lorg/codehaus/jackson/map/ObjectMapper;

    move-result-object v1

    iget-object v0, p0, Lcom/peel/util/ck;->j:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    const-class v2, Ljava/util/ArrayList;

    invoke-virtual {v1, v0, v2}, Lorg/codehaus/jackson/map/ObjectMapper;->readValue(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 1934
    if-eqz v0, :cond_2

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-nez v1, :cond_3

    .line 1936
    :cond_2
    iget-object v0, p0, Lcom/peel/util/ck;->a:Lcom/peel/util/cj;

    iget-object v0, v0, Lcom/peel/util/cj;->b:Lcom/peel/util/t;

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lcom/peel/util/t;->a(ZLjava/lang/Object;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1970
    :catch_0
    move-exception v0

    .line 1971
    sget-object v1, Lcom/peel/util/bx;->a:Ljava/lang/String;

    sget-object v2, Lcom/peel/util/bx;->a:Ljava/lang/String;

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1974
    iget-object v0, p0, Lcom/peel/util/ck;->a:Lcom/peel/util/cj;

    iget-object v0, v0, Lcom/peel/util/cj;->b:Lcom/peel/util/t;

    invoke-virtual {v0, v4, v11, v11}, Lcom/peel/util/t;->a(ZLjava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 1940
    :cond_3
    :try_start_1
    iget-object v1, p0, Lcom/peel/util/ck;->a:Lcom/peel/util/cj;

    iget-object v1, v1, Lcom/peel/util/cj;->c:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v5

    .line 1941
    iget-object v1, p0, Lcom/peel/util/ck;->a:Lcom/peel/util/cj;

    iget-object v1, v1, Lcom/peel/util/cj;->c:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    .line 1943
    new-instance v7, Ljava/util/ArrayList;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    invoke-direct {v7, v1}, Ljava/util/ArrayList;-><init>(I)V

    move v3, v4

    .line 1944
    :goto_1
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-ge v3, v1, :cond_5

    .line 1945
    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map;

    .line 1946
    new-instance v8, Landroid/os/Bundle;

    invoke-direct {v8}, Landroid/os/Bundle;-><init>()V

    .line 1950
    const-string/jumbo v9, "name"

    const-string/jumbo v2, "countryName"

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v8, v9, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1951
    const-string/jumbo v9, "endpoint"

    const-string/jumbo v2, "endpoint"

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v8, v9, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1952
    const-string/jumbo v9, "iso"

    const-string/jumbo v2, "countryCode"

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v8, v9, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1953
    const-string/jumbo v2, "type"

    const-string/jumbo v9, "providers_support_type"

    invoke-interface {v1, v9}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v8, v2, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1954
    const-string/jumbo v1, "flag"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "flag_"

    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v9, "iso"

    invoke-virtual {v8, v9}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    sget-object v10, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v9, v10}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v9, "drawable"

    invoke-static {v2, v9, v5, v6}, Lcom/peel/util/bx;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/res/Resources;)I

    move-result v2

    invoke-virtual {v8, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1956
    const-string/jumbo v1, "iso"

    invoke-virtual {v8, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "cn"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 1957
    const-string/jumbo v1, "urloverride"

    const-string/jumbo v2, "http://www.peelchina.com"

    invoke-virtual {v8, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1959
    :cond_4
    invoke-interface {v7, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1944
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto/16 :goto_1

    .line 1963
    :cond_5
    new-instance v0, Lcom/peel/util/cl;

    invoke-direct {v0, p0}, Lcom/peel/util/cl;-><init>(Lcom/peel/util/ck;)V

    invoke-static {v7, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 1969
    iget-object v0, p0, Lcom/peel/util/ck;->a:Lcom/peel/util/cj;

    iget-object v0, v0, Lcom/peel/util/cj;->b:Lcom/peel/util/t;

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v7, v2}, Lcom/peel/util/t;->a(ZLjava/lang/Object;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0
.end method
