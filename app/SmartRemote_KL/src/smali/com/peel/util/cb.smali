.class final Lcom/peel/util/cb;
.super Lcom/peel/util/t;


# instance fields
.field final synthetic a:Lcom/peel/util/t;

.field final synthetic b:Ljava/lang/String;

.field final synthetic c:Lcom/peel/control/a;

.field final synthetic d:Landroid/content/Context;


# direct methods
.method constructor <init>(ILcom/peel/util/t;Ljava/lang/String;Lcom/peel/control/a;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1436
    iput-object p2, p0, Lcom/peel/util/cb;->a:Lcom/peel/util/t;

    iput-object p3, p0, Lcom/peel/util/cb;->b:Ljava/lang/String;

    iput-object p4, p0, Lcom/peel/util/cb;->c:Lcom/peel/control/a;

    iput-object p5, p0, Lcom/peel/util/cb;->d:Landroid/content/Context;

    invoke-direct {p0, p1}, Lcom/peel/util/t;-><init>(I)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 14

    .prologue
    .line 1439
    iget-boolean v0, p0, Lcom/peel/util/cb;->i:Z

    if-nez v0, :cond_1

    .line 1440
    iget-object v0, p0, Lcom/peel/util/cb;->a:Lcom/peel/util/t;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/peel/util/cb;->a:Lcom/peel/util/t;

    iget-boolean v1, p0, Lcom/peel/util/cb;->i:Z

    iget-object v2, p0, Lcom/peel/util/cb;->j:Ljava/lang/Object;

    iget-object v3, p0, Lcom/peel/util/cb;->k:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3}, Lcom/peel/util/t;->a(ZLjava/lang/Object;Ljava/lang/String;)V

    .line 1627
    :cond_0
    :goto_0
    return-void

    .line 1444
    :cond_1
    const/4 v7, 0x0

    .line 1445
    const/4 v2, 0x0

    .line 1446
    iget-object v0, p0, Lcom/peel/util/cb;->j:Ljava/lang/Object;

    check-cast v0, [Lcom/peel/content/library/Library;

    check-cast v0, [Lcom/peel/content/library/Library;

    array-length v4, v0

    const/4 v1, 0x0

    move v3, v1

    :goto_1
    if-ge v3, v4, :cond_c

    aget-object v1, v0, v3

    .line 1447
    invoke-virtual {v1}, Lcom/peel/content/library/Library;->f()Ljava/lang/String;

    move-result-object v5

    const-string/jumbo v6, "live"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_7

    move-object v0, v1

    .line 1448
    check-cast v0, Lcom/peel/content/library/LiveLibrary;

    .line 1450
    invoke-virtual {v0}, Lcom/peel/content/library/LiveLibrary;->i()[Lcom/peel/data/Channel;

    move-result-object v4

    .line 1451
    array-length v5, v4

    const/4 v1, 0x0

    move v3, v1

    :goto_2
    if-ge v3, v5, :cond_b

    aget-object v1, v4, v3

    .line 1453
    invoke-virtual {v1}, Lcom/peel/data/Channel;->m()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1}, Lcom/peel/data/Channel;->e()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_6

    invoke-virtual {v1}, Lcom/peel/data/Channel;->m()Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lcom/peel/util/cb;->b:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_6

    :cond_2
    :goto_3
    move-object v7, v0

    .line 1466
    :goto_4
    iget-object v0, p0, Lcom/peel/util/cb;->c:Lcom/peel/control/a;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lcom/peel/control/a;->a(I)Lcom/peel/control/h;

    move-result-object v2

    .line 1468
    const-string/jumbo v3, "T"

    .line 1469
    if-eqz v1, :cond_3

    .line 1470
    invoke-virtual {v1}, Lcom/peel/data/Channel;->j()Ljava/lang/String;

    move-result-object v3

    .line 1474
    :cond_3
    const/4 v5, 0x0

    .line 1475
    const/4 v4, 0x0

    .line 1477
    sget-object v0, Lcom/peel/content/a;->f:Lcom/peel/content/user/User;

    invoke-virtual {v0}, Lcom/peel/content/user/User;->e()Landroid/os/Bundle;

    move-result-object v8

    .line 1478
    invoke-static {}, Lcom/peel/content/a;->a()Lcom/peel/data/ContentRoom;

    move-result-object v6

    .line 1479
    if-eqz v6, :cond_8

    const/4 v0, 0x1

    move v1, v0

    :goto_5
    if-eqz v7, :cond_9

    const/4 v0, 0x1

    :goto_6
    and-int/2addr v0, v1

    if-eqz v0, :cond_5

    .line 1480
    invoke-virtual {v7}, Lcom/peel/content/library/Library;->e()Ljava/lang/String;

    move-result-object v1

    .line 1481
    invoke-virtual {v6}, Lcom/peel/data/ContentRoom;->a()Ljava/lang/String;

    move-result-object v9

    .line 1482
    invoke-virtual {v8}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :cond_4
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1483
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v11, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string/jumbo v12, "/"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string/jumbo v12, "/"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v0, v11}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v11

    if-eqz v11, :cond_4

    .line 1486
    invoke-virtual {v8, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v12, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string/jumbo v13, "_"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    iget-object v13, p0, Lcom/peel/util/cb;->b:Ljava/lang/String;

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_4

    .line 1487
    const-string/jumbo v1, "_"

    invoke-virtual {v0, v1}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v5

    .line 1488
    const/4 v4, 0x1

    .line 1493
    :cond_5
    if-eqz v5, :cond_a

    .line 1496
    :goto_7
    new-instance v0, Lcom/peel/widget/ag;

    iget-object v1, p0, Lcom/peel/util/cb;->d:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/peel/widget/ag;-><init>(Landroid/content/Context;)V

    sget v1, Lcom/peel/ui/ft;->switch_tv_source:I

    invoke-virtual {v0, v1}, Lcom/peel/widget/ag;->a(I)Lcom/peel/widget/ag;

    move-result-object v8

    .line 1498
    sget v0, Lcom/peel/ui/fp;->customPanel:I

    invoke-virtual {v8, v0}, Lcom/peel/widget/ag;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 1499
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 1500
    iget-object v1, p0, Lcom/peel/util/cb;->d:Landroid/content/Context;

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    sget v9, Lcom/peel/ui/fq;->switch_tv_source_view:I

    invoke-virtual {v1, v9, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 1502
    sget v0, Lcom/peel/ui/fp;->source_btn:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 1503
    iget-object v9, p0, Lcom/peel/util/cb;->d:Landroid/content/Context;

    invoke-virtual {v9}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    sget v10, Lcom/peel/ui/ft;->switch_to:I

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    iget-object v13, p0, Lcom/peel/util/cb;->d:Landroid/content/Context;

    invoke-static {v13, v3}, Lcom/peel/util/bx;->d(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    aput-object v13, v11, v12

    invoke-virtual {v9, v10, v11}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v0, v9}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 1504
    new-instance v9, Lcom/peel/util/cc;

    invoke-direct {v9, p0, v3}, Lcom/peel/util/cc;-><init>(Lcom/peel/util/cb;Ljava/lang/String;)V

    invoke-virtual {v0, v9}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1519
    sget v0, Lcom/peel/ui/fp;->after_switch_textview:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 1520
    iget-object v1, p0, Lcom/peel/util/cb;->d:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v9, Lcom/peel/ui/ft;->after_switch_source:I

    const/4 v10, 0x2

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    iget-object v12, p0, Lcom/peel/util/cb;->d:Landroid/content/Context;

    invoke-static {v12, v3}, Lcom/peel/util/bx;->d(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    aput-object v12, v10, v11

    const/4 v11, 0x1

    iget-object v12, p0, Lcom/peel/util/cb;->b:Ljava/lang/String;

    aput-object v12, v10, v11

    invoke-virtual {v1, v9, v10}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1522
    iget-object v0, p0, Lcom/peel/util/cb;->d:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/peel/ui/ft;->switch_tv_source_msg:I

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    iget-object v11, p0, Lcom/peel/util/cb;->d:Landroid/content/Context;

    invoke-static {v11, v3}, Lcom/peel/util/bx;->d(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    aput-object v11, v9, v10

    invoke-virtual {v0, v1, v9}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v8, v0}, Lcom/peel/widget/ag;->b(Ljava/lang/CharSequence;)Lcom/peel/widget/ag;

    .line 1523
    sget v0, Lcom/peel/ui/ft;->cancel:I

    const/4 v1, 0x0

    invoke-virtual {v8, v0, v1}, Lcom/peel/widget/ag;->c(ILandroid/content/DialogInterface$OnClickListener;)Lcom/peel/widget/ag;

    .line 1524
    iget-object v9, p0, Lcom/peel/util/cb;->b:Ljava/lang/String;

    new-instance v0, Lcom/peel/util/cd;

    move-object v1, p0

    invoke-direct/range {v0 .. v7}, Lcom/peel/util/cd;-><init>(Lcom/peel/util/cb;Lcom/peel/control/h;Ljava/lang/String;ZLjava/lang/String;Lcom/peel/data/ContentRoom;Lcom/peel/content/library/Library;)V

    invoke-virtual {v8, v9, v0}, Lcom/peel/widget/ag;->a(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lcom/peel/widget/ag;

    .line 1626
    invoke-virtual {v8}, Lcom/peel/widget/ag;->d()Lcom/peel/widget/ag;

    goto/16 :goto_0

    .line 1456
    :cond_6
    invoke-virtual {v1}, Lcom/peel/data/Channel;->e()Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lcom/peel/util/cb;->b:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_2

    .line 1451
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto/16 :goto_2

    .line 1446
    :cond_7
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto/16 :goto_1

    .line 1479
    :cond_8
    const/4 v0, 0x0

    move v1, v0

    goto/16 :goto_5

    :cond_9
    const/4 v0, 0x0

    goto/16 :goto_6

    .line 1493
    :cond_a
    iget-object v5, p0, Lcom/peel/util/cb;->b:Ljava/lang/String;

    goto/16 :goto_7

    :cond_b
    move-object v1, v2

    goto/16 :goto_3

    :cond_c
    move-object v1, v2

    goto/16 :goto_4
.end method
