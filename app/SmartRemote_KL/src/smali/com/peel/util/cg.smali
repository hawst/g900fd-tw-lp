.class final Lcom/peel/util/cg;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Landroid/content/Context;

.field final synthetic b:Ljava/net/URI;

.field final synthetic c:Lcom/peel/control/a;


# direct methods
.method constructor <init>(Landroid/content/Context;Ljava/net/URI;Lcom/peel/control/a;)V
    .locals 0

    .prologue
    .line 1756
    iput-object p1, p0, Lcom/peel/util/cg;->a:Landroid/content/Context;

    iput-object p2, p0, Lcom/peel/util/cg;->b:Ljava/net/URI;

    iput-object p3, p0, Lcom/peel/util/cg;->c:Lcom/peel/control/a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 1759
    iget-object v0, p0, Lcom/peel/util/cg;->a:Landroid/content/Context;

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string/jumbo v1, "country"

    const-string/jumbo v2, ""

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "Japan"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1761
    iget-object v0, p0, Lcom/peel/util/cg;->b:Ljava/net/URI;

    invoke-virtual {v0}, Ljava/net/URI;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1762
    const/16 v1, 0x2f

    invoke-virtual {v0, v1}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 1763
    iget-object v1, p0, Lcom/peel/util/cg;->a:Landroid/content/Context;

    iget-object v2, p0, Lcom/peel/util/cg;->c:Lcom/peel/control/a;

    const/4 v3, 0x0

    invoke-static {v1, v2, v0, v3}, Lcom/peel/util/bx;->a(Landroid/content/Context;Lcom/peel/control/a;Ljava/lang/String;Lcom/peel/util/t;)V

    .line 1785
    :goto_0
    return-void

    .line 1765
    :cond_0
    iget-object v0, p0, Lcom/peel/util/cg;->a:Landroid/content/Context;

    const-string/jumbo v1, "custom_delay"

    invoke-static {v0, v1}, Lcom/peel/util/dq;->a(Landroid/content/Context;Ljava/lang/String;)I

    move-result v0

    .line 1766
    if-lez v0, :cond_1

    .line 1767
    iget-object v1, p0, Lcom/peel/util/cg;->c:Lcom/peel/control/a;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/peel/util/cg;->b:Ljava/net/URI;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "?delayms="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/net/URI;->create(Ljava/lang/String;)Ljava/net/URI;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/peel/control/a;->a(Ljava/net/URI;)Z

    goto :goto_0

    .line 1769
    :cond_1
    iget-object v0, p0, Lcom/peel/util/cg;->c:Lcom/peel/control/a;

    iget-object v1, p0, Lcom/peel/util/cg;->b:Ljava/net/URI;

    invoke-virtual {v0, v1}, Lcom/peel/control/a;->a(Ljava/net/URI;)Z

    goto :goto_0
.end method
