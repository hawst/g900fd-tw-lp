.class Lcom/peel/util/ce;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic a:Lcom/peel/d/i;

.field final synthetic b:Lcom/peel/util/cd;


# direct methods
.method constructor <init>(Lcom/peel/util/cd;Lcom/peel/d/i;)V
    .locals 0

    .prologue
    .line 1573
    iput-object p1, p0, Lcom/peel/util/ce;->b:Lcom/peel/util/cd;

    iput-object p2, p0, Lcom/peel/util/ce;->a:Lcom/peel/d/i;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3

    .prologue
    .line 1576
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 1577
    const-string/jumbo v1, "room"

    iget-object v2, p0, Lcom/peel/util/ce;->b:Lcom/peel/util/cd;

    iget-object v2, v2, Lcom/peel/util/cd;->e:Lcom/peel/data/ContentRoom;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1578
    const-string/jumbo v1, "library"

    iget-object v2, p0, Lcom/peel/util/ce;->b:Lcom/peel/util/cd;

    iget-object v2, v2, Lcom/peel/util/cd;->f:Lcom/peel/content/library/Library;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1579
    const-string/jumbo v1, "source"

    iget-object v2, p0, Lcom/peel/util/ce;->b:Lcom/peel/util/cd;

    iget-object v2, v2, Lcom/peel/util/cd;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1580
    iget-object v1, p0, Lcom/peel/util/ce;->a:Lcom/peel/d/i;

    invoke-virtual {v1}, Lcom/peel/d/i;->c()Landroid/support/v4/app/ae;

    move-result-object v1

    const-class v2, Lcom/peel/h/a/fj;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lcom/peel/d/e;->c(Landroid/support/v4/app/ae;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 1581
    return-void
.end method
