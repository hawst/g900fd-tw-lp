.class public final enum Lcom/peel/util/v;
.super Ljava/lang/Enum;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/peel/util/v;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/peel/util/v;

.field private static final synthetic d:[Lcom/peel/util/v;


# instance fields
.field private b:Landroid/app/AlarmManager;

.field private c:Landroid/app/PendingIntent;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 23
    new-instance v0, Lcom/peel/util/v;

    const-string/jumbo v1, "INSTANCE"

    invoke-direct {v0, v1, v2}, Lcom/peel/util/v;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/peel/util/v;->a:Lcom/peel/util/v;

    .line 22
    const/4 v0, 0x1

    new-array v0, v0, [Lcom/peel/util/v;

    sget-object v1, Lcom/peel/util/v;->a:Lcom/peel/util/v;

    aput-object v1, v0, v2

    sput-object v0, Lcom/peel/util/v;->d:[Lcom/peel/util/v;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 28
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 29
    sget-object v0, Lcom/peel/c/a;->a:Lcom/peel/c/e;

    invoke-static {v0}, Lcom/peel/c/a;->a(Lcom/peel/c/e;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    const-string/jumbo v1, "alarm"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    iput-object v0, p0, Lcom/peel/util/v;->b:Landroid/app/AlarmManager;

    .line 30
    return-void
.end method

.method private a(JLjava/lang/String;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 80
    new-instance v1, Landroid/content/Intent;

    sget-object v0, Lcom/peel/c/a;->a:Lcom/peel/c/e;

    invoke-static {v0}, Lcom/peel/c/a;->a(Lcom/peel/c/e;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    const-class v2, Lcom/peel/receiver/ChannelUpdateReceiver;

    invoke-direct {v1, v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 81
    const-string/jumbo v0, "com.peel.receiver.action.CHANNEL_UPDATE"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 82
    const-string/jumbo v0, "channel"

    invoke-virtual {v1, v0, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 83
    sget-object v0, Lcom/peel/c/a;->a:Lcom/peel/c/e;

    invoke-static {v0}, Lcom/peel/c/a;->a(Lcom/peel/c/e;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    const/high16 v2, 0x10000000

    invoke-static {v0, v4, v1, v2}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/util/v;->c:Landroid/app/PendingIntent;

    .line 86
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 87
    invoke-virtual {v0, p1, p2}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 88
    iget-object v1, p0, Lcom/peel/util/v;->b:Landroid/app/AlarmManager;

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    iget-object v0, p0, Lcom/peel/util/v;->c:Landroid/app/PendingIntent;

    invoke-virtual {v1, v4, v2, v3, v0}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    .line 89
    return-void
.end method

.method static synthetic a(Lcom/peel/util/v;JLjava/lang/String;)V
    .locals 1

    .prologue
    .line 22
    invoke-direct {p0, p1, p2, p3}, Lcom/peel/util/v;->a(JLjava/lang/String;)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/peel/util/v;
    .locals 1

    .prologue
    .line 22
    const-class v0, Lcom/peel/util/v;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/peel/util/v;

    return-object v0
.end method

.method public static values()[Lcom/peel/util/v;
    .locals 1

    .prologue
    .line 22
    sget-object v0, Lcom/peel/util/v;->d:[Lcom/peel/util/v;

    invoke-virtual {v0}, [Lcom/peel/util/v;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/peel/util/v;

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 92
    sget-object v0, Lcom/peel/c/a;->a:Lcom/peel/c/e;

    invoke-static {v0}, Lcom/peel/c/a;->a(Lcom/peel/c/e;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    const-string/jumbo v1, "notification"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    .line 93
    const/16 v1, 0x7c2

    invoke-virtual {v0, v1}, Landroid/app/NotificationManager;->cancel(I)V

    .line 95
    sget-object v0, Lcom/peel/c/a;->a:Lcom/peel/c/e;

    invoke-static {v0}, Lcom/peel/c/a;->a(Lcom/peel/c/e;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    const-string/jumbo v1, "widget_pref"

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 96
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v1, "playing_status_notification"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 98
    iget-object v0, p0, Lcom/peel/util/v;->c:Landroid/app/PendingIntent;

    if-eqz v0, :cond_0

    .line 99
    iget-object v0, p0, Lcom/peel/util/v;->b:Landroid/app/AlarmManager;

    iget-object v1, p0, Lcom/peel/util/v;->c:Landroid/app/PendingIntent;

    invoke-virtual {v0, v1}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    .line 101
    :cond_0
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 33
    sget-object v0, Lcom/peel/c/a;->a:Lcom/peel/c/e;

    .line 34
    invoke-static {v0}, Lcom/peel/c/a;->a(Lcom/peel/c/e;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string/jumbo v3, "setup_type"

    invoke-interface {v0, v3, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    if-ne v1, v0, :cond_1

    move v0, v1

    .line 36
    :goto_0
    if-eqz v0, :cond_2

    .line 77
    :cond_0
    :goto_1
    return-void

    :cond_1
    move v0, v2

    .line 34
    goto :goto_0

    .line 39
    :cond_2
    const-string/jumbo v0, "live"

    invoke-static {v0}, Lcom/peel/content/a;->c(Ljava/lang/String;)Lcom/peel/content/library/Library;

    move-result-object v0

    check-cast v0, Lcom/peel/content/library/LiveLibrary;

    .line 41
    const-string/jumbo v2, "previous"

    invoke-virtual {p1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 42
    const-string/jumbo v2, "/"

    invoke-virtual {p1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 43
    array-length v3, v2

    const/4 v4, 0x4

    if-lt v3, v4, :cond_0

    .line 46
    aget-object p1, v2, v5

    .line 47
    aget-object v1, v2, v1

    aget-object v3, v2, v5

    const/4 v4, 0x3

    aget-object v2, v2, v4

    invoke-static {v1, v3, v2}, Lcom/peel/util/bx;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 50
    :cond_3
    if-eqz v0, :cond_0

    .line 56
    new-instance v1, Lcom/peel/util/w;

    invoke-direct {v1, p0, v0}, Lcom/peel/util/w;-><init>(Lcom/peel/util/v;Lcom/peel/content/library/LiveLibrary;)V

    invoke-virtual {v0, p1, v1}, Lcom/peel/content/library/LiveLibrary;->a(Ljava/lang/String;Lcom/peel/util/t;)V

    goto :goto_1
.end method
