.class final Lcom/peel/util/du;
.super Ljava/util/concurrent/FutureTask;

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/util/concurrent/FutureTask",
        "<TT;>;",
        "Ljava/lang/Comparable",
        "<",
        "Lcom/peel/util/du",
        "<TT;>;>;"
    }
.end annotation


# instance fields
.field private final a:I


# direct methods
.method public constructor <init>(ILjava/lang/Runnable;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/Runnable;",
            "TT;)V"
        }
    .end annotation

    .prologue
    .line 38
    invoke-direct {p0, p2, p3}, Ljava/util/concurrent/FutureTask;-><init>(Ljava/lang/Runnable;Ljava/lang/Object;)V

    .line 39
    iput p1, p0, Lcom/peel/util/du;->a:I

    .line 40
    return-void
.end method


# virtual methods
.method public a(Lcom/peel/util/du;)I
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/peel/util/du",
            "<TT;>;)I"
        }
    .end annotation

    .prologue
    const-wide/16 v4, 0x0

    .line 43
    iget v0, p1, Lcom/peel/util/du;->a:I

    iget v1, p0, Lcom/peel/util/du;->a:I

    sub-int/2addr v0, v1

    int-to-long v0, v0

    .line 44
    cmp-long v2, v4, v0

    if-nez v2, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    cmp-long v0, v4, v0

    if-lez v0, :cond_1

    const/4 v0, -0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public synthetic compareTo(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 29
    check-cast p1, Lcom/peel/util/du;

    invoke-virtual {p0, p1}, Lcom/peel/util/du;->a(Lcom/peel/util/du;)I

    move-result v0

    return v0
.end method
