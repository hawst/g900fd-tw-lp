.class final Lcom/peel/util/b/g;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/squareup/picasso/Picasso$RequestTransformer;


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public transformRequest(Lcom/squareup/picasso/Request;)Lcom/squareup/picasso/Request;
    .locals 2

    .prologue
    .line 35
    sget-object v0, Lcom/peel/util/b/a;->b:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 40
    :goto_0
    return-object p1

    :cond_0
    invoke-virtual {p1}, Lcom/squareup/picasso/Request;->buildUpon()Lcom/squareup/picasso/Request$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/picasso/Request;->uri:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Lcom/squareup/picasso/Request$Builder;->setUri(Landroid/net/Uri;)Lcom/squareup/picasso/Request$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/picasso/Request$Builder;->build()Lcom/squareup/picasso/Request;

    move-result-object p1

    goto :goto_0
.end method
