.class Lcom/peel/util/bm;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# instance fields
.field final synthetic a:Landroid/content/SharedPreferences;

.field final synthetic b:Landroid/content/Context;

.field final synthetic c:Lcom/peel/util/be;


# direct methods
.method constructor <init>(Lcom/peel/util/be;Landroid/content/SharedPreferences;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 459
    iput-object p1, p0, Lcom/peel/util/bm;->c:Lcom/peel/util/be;

    iput-object p2, p0, Lcom/peel/util/bm;->a:Landroid/content/SharedPreferences;

    iput-object p3, p0, Lcom/peel/util/bm;->b:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 4

    .prologue
    .line 462
    iget-object v0, p0, Lcom/peel/util/bm;->a:Landroid/content/SharedPreferences;

    const-string/jumbo v1, "lockscreen_enabled"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 463
    if-eqz p2, :cond_0

    if-eqz v0, :cond_1

    :cond_0
    if-nez p2, :cond_2

    if-eqz v0, :cond_2

    .line 464
    :cond_1
    iget-object v0, p0, Lcom/peel/util/bm;->a:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v1, "lockscreen_enabled"

    invoke-interface {v0, v1, p2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 466
    if-eqz p2, :cond_4

    .line 467
    iget-object v0, p0, Lcom/peel/util/bm;->c:Lcom/peel/util/be;

    iget-object v1, p0, Lcom/peel/util/bm;->b:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/peel/util/be;->d(Landroid/content/Context;)V

    .line 468
    invoke-static {}, Lcom/peel/util/a/f;->a()Lcom/peel/util/a/f;

    move-result-object v1

    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v0}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v0

    if-nez v0, :cond_3

    const/4 v0, 0x1

    :goto_0
    const/16 v2, 0x1524

    const/16 v3, 0x7d8

    invoke-virtual {v1, v0, v2, v3}, Lcom/peel/util/a/f;->a(III)V

    .line 473
    :cond_2
    :goto_1
    return-void

    .line 468
    :cond_3
    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v0}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/data/at;->f()I

    move-result v0

    goto :goto_0

    .line 470
    :cond_4
    iget-object v0, p0, Lcom/peel/util/bm;->c:Lcom/peel/util/be;

    iget-object v1, p0, Lcom/peel/util/bm;->b:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/peel/util/be;->b(Landroid/content/Context;)V

    goto :goto_1
.end method
