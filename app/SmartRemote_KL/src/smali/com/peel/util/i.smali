.class public final Lcom/peel/util/i;
.super Ljava/lang/Object;


# static fields
.field private static final a:Ljava/lang/String;

.field private static b:Lcom/peel/util/dr;

.field private static c:Landroid/os/Handler;

.field private static d:Landroid/os/Handler;

.field private static e:Landroid/os/Handler;

.field private static f:Landroid/os/Handler;

.field private static g:Landroid/os/Handler;

.field private static h:Z

.field private static i:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 11
    const-class v0, Lcom/peel/util/i;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/peel/util/i;->a:Ljava/lang/String;

    .line 27
    sput-boolean v1, Lcom/peel/util/i;->h:Z

    .line 28
    sput-boolean v1, Lcom/peel/util/i;->i:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 353
    return-void
.end method

.method public static a(Lcom/peel/util/p;J)Lcom/peel/util/p;
    .locals 1

    .prologue
    .line 134
    sget-object v0, Lcom/peel/util/i;->c:Landroid/os/Handler;

    invoke-virtual {v0, p0, p1, p2}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 135
    return-object p0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;J)Ljava/lang/Runnable;
    .locals 3

    .prologue
    .line 112
    new-instance v0, Lcom/peel/util/k;

    invoke-direct {v0, p2, p1}, Lcom/peel/util/k;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    .line 125
    sget-boolean v1, Lcom/peel/util/i;->i:Z

    if-eqz v1, :cond_0

    .line 126
    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 130
    :goto_0
    return-object v0

    .line 128
    :cond_0
    sget-object v1, Lcom/peel/util/i;->c:Landroid/os/Handler;

    invoke-virtual {v1, v0, p3, p4}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method

.method public static final a(Ljava/lang/String;Ljava/lang/String;ILjava/lang/Runnable;)Ljava/util/concurrent/Future;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "I",
            "Ljava/lang/Runnable;",
            ")",
            "Ljava/util/concurrent/Future",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 92
    sget-object v0, Lcom/peel/util/i;->b:Lcom/peel/util/dr;

    new-instance v1, Lcom/peel/util/j;

    invoke-direct {v1, p2, p3, p1}, Lcom/peel/util/j;-><init>(ILjava/lang/Runnable;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/peel/util/dr;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/util/concurrent/Future;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/Runnable;",
            ")",
            "Ljava/util/concurrent/Future",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 109
    const/16 v0, 0xa

    invoke-static {p0, p1, v0, p2}, Lcom/peel/util/i;->a(Ljava/lang/String;Ljava/lang/String;ILjava/lang/Runnable;)Ljava/util/concurrent/Future;

    move-result-object v0

    return-object v0
.end method

.method public static a()V
    .locals 10

    .prologue
    const/16 v2, 0xa

    .line 31
    sget-boolean v0, Lcom/peel/util/i;->h:Z

    if-eqz v0, :cond_0

    .line 53
    :goto_0
    return-void

    .line 32
    :cond_0
    const/4 v0, 0x1

    sput-boolean v0, Lcom/peel/util/i;->h:Z

    .line 34
    new-instance v0, Landroid/os/HandlerThread;

    const-string/jumbo v1, "non-ui"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    .line 35
    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 37
    new-instance v7, Landroid/os/HandlerThread;

    const-string/jumbo v1, "db"

    invoke-direct {v7, v1, v2}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;I)V

    .line 38
    invoke-virtual {v7}, Landroid/os/HandlerThread;->start()V

    .line 40
    new-instance v8, Landroid/os/HandlerThread;

    const-string/jumbo v1, "tr"

    invoke-direct {v8, v1, v2}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;I)V

    .line 41
    invoke-virtual {v8}, Landroid/os/HandlerThread;->start()V

    .line 43
    new-instance v9, Landroid/os/HandlerThread;

    const-string/jumbo v1, "ir"

    invoke-direct {v9, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    .line 44
    invoke-virtual {v9}, Landroid/os/HandlerThread;->start()V

    .line 46
    new-instance v1, Lcom/peel/util/dr;

    const/4 v2, 0x5

    const/16 v3, 0x80

    const-wide/16 v4, 0x1

    sget-object v6, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-direct/range {v1 .. v6}, Lcom/peel/util/dr;-><init>(IIJLjava/util/concurrent/TimeUnit;)V

    sput-object v1, Lcom/peel/util/i;->b:Lcom/peel/util/dr;

    .line 48
    new-instance v1, Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-direct {v1, v0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    sput-object v1, Lcom/peel/util/i;->c:Landroid/os/Handler;

    .line 49
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    sput-object v0, Lcom/peel/util/i;->d:Landroid/os/Handler;

    .line 50
    new-instance v0, Landroid/os/Handler;

    invoke-virtual {v7}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    sput-object v0, Lcom/peel/util/i;->e:Landroid/os/Handler;

    .line 51
    new-instance v0, Landroid/os/Handler;

    invoke-virtual {v8}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    sput-object v0, Lcom/peel/util/i;->g:Landroid/os/Handler;

    .line 52
    new-instance v0, Landroid/os/Handler;

    invoke-virtual {v9}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    sput-object v0, Lcom/peel/util/i;->f:Landroid/os/Handler;

    goto :goto_0
.end method

.method public static a(Ljava/lang/Runnable;)V
    .locals 1

    .prologue
    .line 82
    sget-object v0, Lcom/peel/util/i;->c:Landroid/os/Handler;

    invoke-virtual {v0, p0}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    return-void
.end method

.method public static b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/lang/Runnable;
    .locals 2

    .prologue
    .line 216
    const-wide/16 v0, 0x0

    invoke-static {p0, p1, p2, v0, v1}, Lcom/peel/util/i;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;J)Ljava/lang/Runnable;

    move-result-object v0

    return-object v0
.end method

.method public static b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;J)Ljava/lang/Runnable;
    .locals 3

    .prologue
    .line 139
    new-instance v0, Lcom/peel/util/l;

    invoke-direct {v0, p2, p1}, Lcom/peel/util/l;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    .line 152
    sget-object v1, Lcom/peel/util/i;->e:Landroid/os/Handler;

    invoke-virtual {v1, v0, p3, p4}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 153
    return-object v0
.end method

.method public static b()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 56
    sget-object v0, Lcom/peel/util/i;->c:Landroid/os/Handler;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/peel/util/i;->c:Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Looper;->quit()V

    .line 57
    :cond_0
    sget-object v0, Lcom/peel/util/i;->e:Landroid/os/Handler;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/peel/util/i;->e:Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Looper;->quit()V

    .line 58
    :cond_1
    sget-object v0, Lcom/peel/util/i;->b:Lcom/peel/util/dr;

    if-eqz v0, :cond_2

    sget-object v0, Lcom/peel/util/i;->b:Lcom/peel/util/dr;

    invoke-virtual {v0}, Lcom/peel/util/dr;->shutdown()V

    .line 59
    :cond_2
    sget-object v0, Lcom/peel/util/i;->f:Landroid/os/Handler;

    if-eqz v0, :cond_3

    sget-object v0, Lcom/peel/util/i;->f:Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Looper;->quit()V

    .line 60
    :cond_3
    sget-object v0, Lcom/peel/util/i;->g:Landroid/os/Handler;

    if-eqz v0, :cond_4

    sget-object v0, Lcom/peel/util/i;->g:Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Looper;->quit()V

    .line 62
    :cond_4
    sput-object v1, Lcom/peel/util/i;->b:Lcom/peel/util/dr;

    .line 63
    sput-object v1, Lcom/peel/util/i;->c:Landroid/os/Handler;

    .line 64
    sput-object v1, Lcom/peel/util/i;->d:Landroid/os/Handler;

    .line 65
    sput-object v1, Lcom/peel/util/i;->e:Landroid/os/Handler;

    .line 66
    sput-object v1, Lcom/peel/util/i;->f:Landroid/os/Handler;

    .line 67
    sput-object v1, Lcom/peel/util/i;->g:Landroid/os/Handler;

    .line 69
    const/4 v0, 0x0

    sput-boolean v0, Lcom/peel/util/i;->h:Z

    .line 70
    return-void
.end method

.method public static c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/lang/Runnable;
    .locals 2

    .prologue
    .line 218
    const-wide/16 v0, 0x0

    invoke-static {p0, p1, p2, v0, v1}, Lcom/peel/util/i;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;J)Ljava/lang/Runnable;

    move-result-object v0

    return-object v0
.end method

.method public static c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;J)Ljava/lang/Runnable;
    .locals 3

    .prologue
    .line 157
    new-instance v0, Lcom/peel/util/m;

    invoke-direct {v0, p2, p1}, Lcom/peel/util/m;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    .line 170
    sget-object v1, Lcom/peel/util/i;->d:Landroid/os/Handler;

    invoke-virtual {v1, v0, p3, p4}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 171
    return-object v0
.end method

.method public static c()Z
    .locals 2

    .prologue
    .line 72
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Looper;->getThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/lang/Runnable;
    .locals 2

    .prologue
    .line 220
    const-wide/16 v0, 0x0

    invoke-static {p0, p1, p2, v0, v1}, Lcom/peel/util/i;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;J)Ljava/lang/Runnable;

    move-result-object v0

    return-object v0
.end method

.method public static d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;J)V
    .locals 3

    .prologue
    .line 176
    sget-object v0, Lcom/peel/util/i;->f:Landroid/os/Handler;

    new-instance v1, Lcom/peel/util/n;

    invoke-direct {v1, p2, p1}, Lcom/peel/util/n;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    invoke-virtual {v0, v1, p3, p4}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 189
    return-void
.end method

.method public static d()Z
    .locals 2

    .prologue
    .line 76
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    sget-object v1, Lcom/peel/util/i;->e:Landroid/os/Handler;

    invoke-virtual {v1}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 10
    sget-object v0, Lcom/peel/util/i;->a:Ljava/lang/String;

    return-object v0
.end method

.method public static e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)V
    .locals 2

    .prologue
    .line 222
    const-wide/16 v0, 0x0

    invoke-static {p0, p1, p2, v0, v1}, Lcom/peel/util/i;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;J)V

    return-void
.end method

.method public static e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;J)V
    .locals 3

    .prologue
    .line 193
    const-class v1, Lcom/peel/util/i;

    monitor-enter v1

    .line 194
    :try_start_0
    sget-object v0, Lcom/peel/util/i;->g:Landroid/os/Handler;

    if-nez v0, :cond_0

    .line 195
    new-instance v0, Landroid/os/HandlerThread;

    const-string/jumbo v2, "tr"

    invoke-direct {v0, v2}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    .line 196
    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 197
    new-instance v2, Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-direct {v2, v0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    sput-object v2, Lcom/peel/util/i;->g:Landroid/os/Handler;

    .line 199
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 201
    sget-object v0, Lcom/peel/util/i;->g:Landroid/os/Handler;

    new-instance v1, Lcom/peel/util/o;

    invoke-direct {v1, p2, p1}, Lcom/peel/util/o;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    invoke-virtual {v0, v1, p3, p4}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 214
    return-void

    .line 199
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public static f(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)V
    .locals 2

    .prologue
    .line 224
    const-wide/16 v0, 0x0

    invoke-static {p0, p1, p2, v0, v1}, Lcom/peel/util/i;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;J)V

    return-void
.end method
