.class final Lcom/peel/util/bt;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Landroid/content/Context;

.field final synthetic b:Lcom/peel/content/library/LiveLibrary;


# direct methods
.method constructor <init>(Landroid/content/Context;Lcom/peel/content/library/LiveLibrary;)V
    .locals 0

    .prologue
    .line 83
    iput-object p1, p0, Lcom/peel/util/bt;->a:Landroid/content/Context;

    iput-object p2, p0, Lcom/peel/util/bt;->b:Lcom/peel/content/library/LiveLibrary;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    const/4 v4, 0x1

    const/16 v5, 0x15

    .line 87
    iget-object v0, p0, Lcom/peel/util/bt;->a:Landroid/content/Context;

    iget-object v1, p0, Lcom/peel/util/bt;->b:Lcom/peel/content/library/LiveLibrary;

    invoke-virtual {v1}, Lcom/peel/content/library/LiveLibrary;->n()Lcom/peel/content/listing/Listing;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/peel/util/bq;->a(Landroid/content/Context;Lcom/peel/content/listing/Listing;Z)Landroid/widget/RemoteViews;

    move-result-object v2

    .line 88
    if-eqz v2, :cond_4

    .line 90
    new-instance v1, Landroid/app/Notification$Builder;

    iget-object v0, p0, Lcom/peel/util/bt;->a:Landroid/content/Context;

    invoke-direct {v1, v0}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;)V

    .line 91
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v0, v5, :cond_5

    sget-object v0, Lcom/peel/c/a;->b:Lcom/peel/c/e;

    invoke-static {v0}, Lcom/peel/c/a;->a(Lcom/peel/c/e;)Ljava/lang/Object;

    move-result-object v0

    sget-object v3, Lcom/peel/c/b;->c:Lcom/peel/c/b;

    if-ne v0, v3, :cond_5

    sget v0, Lcom/peel/ui/fo;->l_widget_cover_icon:I

    :goto_0
    invoke-virtual {v1, v0}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    move-result-object v0

    .line 93
    invoke-virtual {v0, v4}, Landroid/app/Notification$Builder;->setAutoCancel(Z)Landroid/app/Notification$Builder;

    move-result-object v0

    const/4 v3, 0x2

    .line 94
    invoke-virtual {v0, v3}, Landroid/app/Notification$Builder;->setPriority(I)Landroid/app/Notification$Builder;

    .line 96
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v0, v5, :cond_0

    .line 97
    invoke-virtual {v1, v4}, Landroid/app/Notification$Builder;->setVisibility(I)Landroid/app/Notification$Builder;

    .line 100
    :cond_0
    invoke-virtual {v1}, Landroid/app/Notification$Builder;->build()Landroid/app/Notification;

    move-result-object v3

    .line 101
    iget v0, v3, Landroid/app/Notification;->flags:I

    or-int/lit8 v0, v0, 0x2

    iput v0, v3, Landroid/app/Notification;->flags:I

    .line 104
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-ge v0, v5, :cond_1

    sget-object v0, Lcom/peel/c/a;->b:Lcom/peel/c/e;

    invoke-static {v0}, Lcom/peel/c/a;->a(Lcom/peel/c/e;)Ljava/lang/Object;

    move-result-object v0

    sget-object v1, Lcom/peel/c/b;->c:Lcom/peel/c/b;

    if-ne v0, v1, :cond_2

    .line 105
    :cond_1
    iget-object v0, p0, Lcom/peel/util/bt;->a:Landroid/content/Context;

    iget-object v1, p0, Lcom/peel/util/bt;->b:Lcom/peel/content/library/LiveLibrary;

    invoke-virtual {v1}, Lcom/peel/content/library/LiveLibrary;->n()Lcom/peel/content/listing/Listing;

    move-result-object v1

    invoke-static {v0, v1, v4}, Lcom/peel/util/bq;->a(Landroid/content/Context;Lcom/peel/content/listing/Listing;Z)Landroid/widget/RemoteViews;

    move-result-object v0

    .line 106
    iput-object v0, v3, Landroid/app/Notification;->contentView:Landroid/widget/RemoteViews;

    .line 107
    iput-object v2, v3, Landroid/app/Notification;->bigContentView:Landroid/widget/RemoteViews;

    .line 110
    :cond_2
    const/4 v1, 0x0

    .line 112
    :try_start_0
    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-string/jumbo v4, "twQuickPanelEvent"

    invoke-virtual {v0, v4}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;
    :try_end_0
    .catch Ljava/lang/NoSuchFieldException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 116
    :goto_1
    if-eqz v0, :cond_6

    .line 118
    const/16 v1, 0x101

    :try_start_1
    invoke-virtual {v0, v3, v1}, Ljava/lang/reflect/Field;->setInt(Ljava/lang/Object;I)V

    .line 119
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-ge v0, v5, :cond_3

    .line 120
    iput-object v2, v3, Landroid/app/Notification;->contentView:Landroid/widget/RemoteViews;
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_1 .. :try_end_1} :catch_2

    .line 134
    :cond_3
    :goto_2
    iget-object v0, p0, Lcom/peel/util/bt;->a:Landroid/content/Context;

    const-string/jumbo v1, "notification"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    .line 135
    const/16 v1, 0x7c2

    invoke-virtual {v0, v1, v3}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 137
    :cond_4
    return-void

    .line 91
    :cond_5
    sget v0, Lcom/peel/ui/fo;->noti_main_icon:I

    goto :goto_0

    .line 113
    :catch_0
    move-exception v0

    .line 114
    invoke-static {}, Lcom/peel/util/bq;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0}, Ljava/lang/NoSuchFieldException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v1

    goto :goto_1

    .line 122
    :catch_1
    move-exception v0

    .line 123
    invoke-static {}, Lcom/peel/util/bq;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 124
    :catch_2
    move-exception v0

    .line 125
    invoke-static {}, Lcom/peel/util/bq;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/IllegalAccessException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 128
    :cond_6
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-ge v0, v5, :cond_3

    .line 129
    iput-object v2, v3, Landroid/app/Notification;->bigContentView:Landroid/widget/RemoteViews;

    goto :goto_2
.end method
