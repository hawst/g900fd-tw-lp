.class final Lcom/peel/util/dg;
.super Lcom/peel/util/t;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/peel/util/t",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:[I

.field final synthetic b:Landroid/content/res/Resources;

.field final synthetic c:Lcom/peel/util/t;


# direct methods
.method constructor <init>([ILandroid/content/res/Resources;Lcom/peel/util/t;)V
    .locals 0

    .prologue
    .line 2810
    iput-object p1, p0, Lcom/peel/util/dg;->a:[I

    iput-object p2, p0, Lcom/peel/util/dg;->b:Landroid/content/res/Resources;

    iput-object p3, p0, Lcom/peel/util/dg;->c:Lcom/peel/util/t;

    invoke-direct {p0}, Lcom/peel/util/t;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .prologue
    const/4 v1, 0x0

    const/4 v6, 0x0

    .line 2813
    iget-boolean v0, p0, Lcom/peel/util/dg;->i:Z

    if-eqz v0, :cond_2

    .line 2814
    iget-object v0, p0, Lcom/peel/util/dg;->j:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    .line 2816
    const-string/jumbo v2, ","

    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 2818
    if-eqz v0, :cond_0

    .line 2819
    sget-object v3, Lcom/peel/util/bx;->a:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "get tile view spacing:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    move v0, v1

    .line 2821
    :goto_0
    array-length v1, v2

    if-ge v0, v1, :cond_1

    .line 2822
    iget-object v1, p0, Lcom/peel/util/dg;->a:[I

    const/4 v3, 0x1

    aget-object v4, v2, v0

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    int-to-float v4, v4

    iget-object v5, p0, Lcom/peel/util/dg;->b:Landroid/content/res/Resources;

    invoke-virtual {v5}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v5

    invoke-static {v3, v4, v5}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v3

    float-to-int v3, v3

    aput v3, v1, v0

    .line 2821
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2824
    :cond_1
    iget-object v0, p0, Lcom/peel/util/dg;->a:[I

    sput-object v0, Lcom/peel/util/a;->e:[I

    .line 2825
    iget-object v0, p0, Lcom/peel/util/dg;->c:Lcom/peel/util/t;

    iget-boolean v1, p0, Lcom/peel/util/dg;->i:Z

    iget-object v2, p0, Lcom/peel/util/dg;->a:[I

    invoke-virtual {v0, v1, v2, v6}, Lcom/peel/util/t;->a(ZLjava/lang/Object;Ljava/lang/String;)V

    .line 2829
    :goto_1
    return-void

    .line 2827
    :cond_2
    iget-object v0, p0, Lcom/peel/util/dg;->c:Lcom/peel/util/t;

    invoke-virtual {v0, v1, v6, v6}, Lcom/peel/util/t;->a(ZLjava/lang/Object;Ljava/lang/String;)V

    goto :goto_1
.end method
