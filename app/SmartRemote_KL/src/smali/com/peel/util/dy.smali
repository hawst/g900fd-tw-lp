.class public Lcom/peel/util/dy;
.super Ljava/lang/Object;


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private b:J

.field private c:J

.field private d:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 32
    const-class v0, Lcom/peel/util/dy;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/peel/util/dy;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a([BI)J
    .locals 7

    .prologue
    const/16 v5, 0x80

    .line 153
    aget-byte v3, p1, p2

    .line 154
    add-int/lit8 v0, p2, 0x1

    aget-byte v2, p1, v0

    .line 155
    add-int/lit8 v0, p2, 0x2

    aget-byte v1, p1, v0

    .line 156
    add-int/lit8 v0, p2, 0x3

    aget-byte v0, p1, v0

    .line 159
    and-int/lit16 v4, v3, 0x80

    if-ne v4, v5, :cond_0

    and-int/lit8 v3, v3, 0x7f

    add-int/lit16 v3, v3, 0x80

    .line 160
    :cond_0
    and-int/lit16 v4, v2, 0x80

    if-ne v4, v5, :cond_1

    and-int/lit8 v2, v2, 0x7f

    add-int/lit16 v2, v2, 0x80

    .line 161
    :cond_1
    and-int/lit16 v4, v1, 0x80

    if-ne v4, v5, :cond_2

    and-int/lit8 v1, v1, 0x7f

    add-int/lit16 v1, v1, 0x80

    .line 162
    :cond_2
    and-int/lit16 v4, v0, 0x80

    if-ne v4, v5, :cond_3

    and-int/lit8 v0, v0, 0x7f

    add-int/lit16 v0, v0, 0x80

    .line 164
    :cond_3
    int-to-long v4, v3

    const/16 v3, 0x18

    shl-long/2addr v4, v3

    int-to-long v2, v2

    const/16 v6, 0x10

    shl-long/2addr v2, v6

    add-long/2addr v2, v4

    int-to-long v4, v1

    const/16 v1, 0x8

    shl-long/2addr v4, v1

    add-long/2addr v2, v4

    int-to-long v0, v0

    add-long/2addr v0, v2

    return-wide v0
.end method

.method private a([BIJ)V
    .locals 9

    .prologue
    .line 182
    const-wide/16 v0, 0x3e8

    div-long v0, p3, v0

    .line 183
    const-wide/16 v2, 0x3e8

    mul-long/2addr v2, v0

    sub-long v2, p3, v2

    .line 184
    const-wide v4, 0x83aa7e80L

    add-long/2addr v0, v4

    .line 187
    add-int/lit8 v4, p2, 0x1

    const/16 v5, 0x18

    shr-long v6, v0, v5

    long-to-int v5, v6

    int-to-byte v5, v5

    aput-byte v5, p1, p2

    .line 188
    add-int/lit8 v5, v4, 0x1

    const/16 v6, 0x10

    shr-long v6, v0, v6

    long-to-int v6, v6

    int-to-byte v6, v6

    aput-byte v6, p1, v4

    .line 189
    add-int/lit8 v4, v5, 0x1

    const/16 v6, 0x8

    shr-long v6, v0, v6

    long-to-int v6, v6

    int-to-byte v6, v6

    aput-byte v6, p1, v5

    .line 190
    add-int/lit8 v5, v4, 0x1

    const/4 v6, 0x0

    shr-long/2addr v0, v6

    long-to-int v0, v0

    int-to-byte v0, v0

    aput-byte v0, p1, v4

    .line 192
    const-wide v0, 0x100000000L

    mul-long/2addr v0, v2

    const-wide/16 v2, 0x3e8

    div-long/2addr v0, v2

    .line 194
    add-int/lit8 v2, v5, 0x1

    const/16 v3, 0x18

    shr-long v6, v0, v3

    long-to-int v3, v6

    int-to-byte v3, v3

    aput-byte v3, p1, v5

    .line 195
    add-int/lit8 v3, v2, 0x1

    const/16 v4, 0x10

    shr-long v4, v0, v4

    long-to-int v4, v4

    int-to-byte v4, v4

    aput-byte v4, p1, v2

    .line 196
    add-int/lit8 v2, v3, 0x1

    const/16 v4, 0x8

    shr-long/2addr v0, v4

    long-to-int v0, v0

    int-to-byte v0, v0

    aput-byte v0, p1, v3

    .line 198
    add-int/lit8 v0, v2, 0x1

    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v0

    const-wide v4, 0x406fe00000000000L    # 255.0

    mul-double/2addr v0, v4

    double-to-int v0, v0

    int-to-byte v0, v0

    aput-byte v0, p1, v2

    .line 199
    return-void
.end method

.method private b([BI)J
    .locals 8

    .prologue
    const-wide/16 v6, 0x3e8

    .line 172
    invoke-direct {p0, p1, p2}, Lcom/peel/util/dy;->a([BI)J

    move-result-wide v0

    .line 173
    add-int/lit8 v2, p2, 0x4

    invoke-direct {p0, p1, v2}, Lcom/peel/util/dy;->a([BI)J

    move-result-wide v2

    .line 174
    const-wide v4, 0x83aa7e80L

    sub-long/2addr v0, v4

    mul-long/2addr v0, v6

    mul-long/2addr v2, v6

    const-wide v4, 0x100000000L

    div-long/2addr v2, v4

    add-long/2addr v0, v2

    return-wide v0
.end method


# virtual methods
.method public a()J
    .locals 2

    .prologue
    .line 127
    iget-wide v0, p0, Lcom/peel/util/dy;->b:J

    return-wide v0
.end method

.method public a(Ljava/lang/String;I)Z
    .locals 16

    .prologue
    .line 66
    :try_start_0
    new-instance v2, Ljava/net/DatagramSocket;

    invoke-direct {v2}, Ljava/net/DatagramSocket;-><init>()V

    .line 67
    move/from16 v0, p2

    invoke-virtual {v2, v0}, Ljava/net/DatagramSocket;->setSoTimeout(I)V

    .line 68
    invoke-static/range {p1 .. p1}, Ljava/net/InetAddress;->getByName(Ljava/lang/String;)Ljava/net/InetAddress;

    move-result-object v3

    .line 69
    const/16 v4, 0x30

    new-array v4, v4, [B

    .line 70
    new-instance v5, Ljava/net/DatagramPacket;

    array-length v6, v4

    const/16 v7, 0x7b

    invoke-direct {v5, v4, v6, v3, v7}, Ljava/net/DatagramPacket;-><init>([BILjava/net/InetAddress;I)V

    .line 75
    const/4 v3, 0x0

    const/16 v6, 0x1b

    aput-byte v6, v4, v3

    .line 78
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    .line 79
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v8

    .line 80
    const/16 v3, 0x28

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v3, v6, v7}, Lcom/peel/util/dy;->a([BIJ)V

    .line 82
    invoke-virtual {v2, v5}, Ljava/net/DatagramSocket;->send(Ljava/net/DatagramPacket;)V

    .line 85
    new-instance v3, Ljava/net/DatagramPacket;

    array-length v5, v4

    invoke-direct {v3, v4, v5}, Ljava/net/DatagramPacket;-><init>([BI)V

    .line 86
    invoke-virtual {v2, v3}, Ljava/net/DatagramSocket;->receive(Ljava/net/DatagramPacket;)V

    .line 87
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v10

    .line 88
    sub-long v12, v10, v8

    add-long/2addr v6, v12

    .line 89
    invoke-virtual {v2}, Ljava/net/DatagramSocket;->close()V

    .line 92
    const/16 v2, 0x18

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v2}, Lcom/peel/util/dy;->b([BI)J

    move-result-wide v2

    .line 93
    const/16 v5, 0x20

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v5}, Lcom/peel/util/dy;->b([BI)J

    move-result-wide v12

    .line 94
    const/16 v5, 0x28

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v5}, Lcom/peel/util/dy;->b([BI)J

    move-result-wide v4

    .line 95
    sub-long v8, v10, v8

    sub-long v14, v4, v12

    sub-long/2addr v8, v14

    .line 104
    sub-long v2, v12, v2

    sub-long/2addr v4, v6

    add-long/2addr v2, v4

    const-wide/16 v4, 0x2

    div-long/2addr v2, v4

    .line 110
    add-long/2addr v2, v6

    move-object/from16 v0, p0

    iput-wide v2, v0, Lcom/peel/util/dy;->b:J

    .line 111
    move-object/from16 v0, p0

    iput-wide v10, v0, Lcom/peel/util/dy;->c:J

    .line 112
    move-object/from16 v0, p0

    iput-wide v8, v0, Lcom/peel/util/dy;->d:J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 118
    const/4 v2, 0x1

    :goto_0
    return v2

    .line 113
    :catch_0
    move-exception v2

    .line 114
    sget-object v3, Lcom/peel/util/dy;->a:Ljava/lang/String;

    const-string/jumbo v4, "request time failed:"

    invoke-static {v3, v4, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 115
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public b()J
    .locals 2

    .prologue
    .line 137
    iget-wide v0, p0, Lcom/peel/util/dy;->c:J

    return-wide v0
.end method
