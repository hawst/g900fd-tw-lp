.class Lcom/peel/util/bi;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Ljava/util/concurrent/atomic/AtomicBoolean;

.field final synthetic b:Landroid/content/SharedPreferences$Editor;

.field final synthetic c:Landroid/content/Context;

.field final synthetic d:Ljava/lang/StringBuilder;

.field final synthetic e:Ljava/util/concurrent/atomic/AtomicBoolean;

.field final synthetic f:Ljava/lang/StringBuilder;

.field final synthetic g:Ljava/util/concurrent/atomic/AtomicBoolean;

.field final synthetic h:Lcom/peel/util/be;


# direct methods
.method constructor <init>(Lcom/peel/util/be;Ljava/util/concurrent/atomic/AtomicBoolean;Landroid/content/SharedPreferences$Editor;Landroid/content/Context;Ljava/lang/StringBuilder;Ljava/util/concurrent/atomic/AtomicBoolean;Ljava/lang/StringBuilder;Ljava/util/concurrent/atomic/AtomicBoolean;)V
    .locals 0

    .prologue
    .line 308
    iput-object p1, p0, Lcom/peel/util/bi;->h:Lcom/peel/util/be;

    iput-object p2, p0, Lcom/peel/util/bi;->a:Ljava/util/concurrent/atomic/AtomicBoolean;

    iput-object p3, p0, Lcom/peel/util/bi;->b:Landroid/content/SharedPreferences$Editor;

    iput-object p4, p0, Lcom/peel/util/bi;->c:Landroid/content/Context;

    iput-object p5, p0, Lcom/peel/util/bi;->d:Ljava/lang/StringBuilder;

    iput-object p6, p0, Lcom/peel/util/bi;->e:Ljava/util/concurrent/atomic/AtomicBoolean;

    iput-object p7, p0, Lcom/peel/util/bi;->f:Ljava/lang/StringBuilder;

    iput-object p8, p0, Lcom/peel/util/bi;->g:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 11

    .prologue
    const/16 v10, 0x7e7

    const/4 v7, -0x1

    const/4 v5, 0x0

    const/4 v6, 0x1

    const/4 v9, 0x0

    .line 311
    iget-object v0, p0, Lcom/peel/util/bi;->a:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 312
    iget-object v0, p0, Lcom/peel/util/bi;->b:Landroid/content/SharedPreferences$Editor;

    const-string/jumbo v1, "notification"

    invoke-interface {v0, v1, v6}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 313
    iget-object v0, p0, Lcom/peel/util/bi;->c:Landroid/content/Context;

    new-instance v1, Landroid/content/Intent;

    const-string/jumbo v2, "tv.peel.samsung.notification.EXPANDED"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 315
    invoke-static {}, Lcom/peel/util/a/f;->a()Lcom/peel/util/a/f;

    move-result-object v1

    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v0}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v0

    if-nez v0, :cond_4

    move v0, v6

    :goto_0
    const/16 v2, 0x1523

    invoke-virtual {v1, v0, v2, v10}, Lcom/peel/util/a/f;->a(III)V

    .line 321
    :goto_1
    sget-object v0, Lcom/peel/c/a;->b:Lcom/peel/c/e;

    invoke-static {v0}, Lcom/peel/c/a;->a(Lcom/peel/c/e;)Ljava/lang/Object;

    move-result-object v0

    sget-object v1, Lcom/peel/c/b;->a:Lcom/peel/c/b;

    if-ne v0, v1, :cond_8

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-ge v0, v1, :cond_8

    .line 323
    iget-object v0, p0, Lcom/peel/util/bi;->c:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/peel/provider/a;->a:Landroid/net/Uri;

    new-array v2, v6, [Ljava/lang/String;

    const-string/jumbo v3, "_id"

    aput-object v3, v2, v9

    const-string/jumbo v3, "wifi_name = ?"

    new-array v4, v6, [Ljava/lang/String;

    iget-object v8, p0, Lcom/peel/util/bi;->d:Ljava/lang/StringBuilder;

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v4, v9

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 325
    if-eqz v1, :cond_b

    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_b

    .line 326
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    .line 327
    invoke-interface {v1, v9}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    .line 330
    :goto_2
    if-eqz v1, :cond_0

    .line 331
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 334
    :cond_0
    iget-object v1, p0, Lcom/peel/util/bi;->e:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 335
    iget-object v1, p0, Lcom/peel/util/bi;->d:Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    if-lez v1, :cond_2

    .line 336
    if-ne v0, v7, :cond_1

    .line 337
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 338
    const-string/jumbo v1, "type"

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 339
    const-string/jumbo v1, "wifi_name"

    iget-object v2, p0, Lcom/peel/util/bi;->d:Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 340
    const-string/jumbo v1, "name"

    iget-object v2, p0, Lcom/peel/util/bi;->d:Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 341
    const-string/jumbo v1, "timestamp"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 342
    const-string/jumbo v1, "wifi_bssid"

    iget-object v2, p0, Lcom/peel/util/bi;->f:Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 344
    iget-object v1, p0, Lcom/peel/util/bi;->c:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Lcom/peel/provider/a;->a:Landroid/net/Uri;

    invoke-virtual {v1, v2, v0}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    .line 347
    :cond_1
    iget-object v0, p0, Lcom/peel/util/bi;->h:Lcom/peel/util/be;

    iget-object v1, p0, Lcom/peel/util/bi;->c:Landroid/content/Context;

    invoke-static {v0, v1}, Lcom/peel/util/be;->a(Lcom/peel/util/be;Landroid/content/Context;)V

    .line 365
    :cond_2
    :goto_3
    iget-object v0, p0, Lcom/peel/util/bi;->g:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 366
    invoke-static {}, Lcom/peel/util/be;->a()Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v1, "pin_remote_show"

    invoke-interface {v0, v1, v6}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 367
    invoke-static {}, Lcom/peel/util/a/f;->a()Lcom/peel/util/a/f;

    move-result-object v0

    sget-object v1, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v1}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v1

    if-nez v1, :cond_a

    :goto_4
    const/16 v1, 0x1525

    invoke-virtual {v0, v6, v1, v10}, Lcom/peel/util/a/f;->a(III)V

    .line 370
    :cond_3
    invoke-static {}, Lcom/peel/util/be;->b()Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->cancel()V

    .line 371
    return-void

    .line 315
    :cond_4
    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v0}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/data/at;->f()I

    move-result v0

    goto/16 :goto_0

    .line 317
    :cond_5
    iget-object v0, p0, Lcom/peel/util/bi;->b:Landroid/content/SharedPreferences$Editor;

    const-string/jumbo v1, "notification"

    invoke-interface {v0, v1, v9}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 318
    iget-object v0, p0, Lcom/peel/util/bi;->c:Landroid/content/Context;

    new-instance v1, Landroid/content/Intent;

    const-string/jumbo v2, "tv.peel.samsung.notification.COLLAPSED"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto/16 :goto_1

    .line 350
    :cond_6
    if-ltz v0, :cond_7

    .line 351
    iget-object v1, p0, Lcom/peel/util/bi;->c:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "content://com.peel.provider.MyLocation/locations/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v1, v0, v5, v5}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 354
    :cond_7
    iget-object v0, p0, Lcom/peel/util/bi;->c:Landroid/content/Context;

    new-instance v1, Landroid/content/Intent;

    const-string/jumbo v2, "com.android.internal.policy.impl.keyguard.sec.REMOTE_REMOVED"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto/16 :goto_3

    .line 357
    :cond_8
    iget-object v0, p0, Lcom/peel/util/bi;->e:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 358
    iget-object v0, p0, Lcom/peel/util/bi;->h:Lcom/peel/util/be;

    iget-object v1, p0, Lcom/peel/util/bi;->c:Landroid/content/Context;

    invoke-static {v0, v1}, Lcom/peel/util/be;->a(Lcom/peel/util/be;Landroid/content/Context;)V

    goto/16 :goto_3

    .line 360
    :cond_9
    iget-object v0, p0, Lcom/peel/util/bi;->h:Lcom/peel/util/be;

    iget-object v1, p0, Lcom/peel/util/bi;->c:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/peel/util/be;->b(Landroid/content/Context;)V

    .line 361
    invoke-static {}, Lcom/peel/util/be;->a()Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v1, "lockscreen_enabled"

    invoke-interface {v0, v1, v9}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    goto/16 :goto_3

    .line 367
    :cond_a
    sget-object v1, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v1}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/data/at;->f()I

    move-result v6

    goto/16 :goto_4

    :cond_b
    move v0, v7

    goto/16 :goto_2
.end method
