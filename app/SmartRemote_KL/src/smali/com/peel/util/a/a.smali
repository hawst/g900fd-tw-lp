.class public Lcom/peel/util/a/a;
.super Ljava/lang/Object;


# static fields
.field private static a:Lcom/amazon/insights/AmazonInsights;

.field private static b:Lcom/amazon/insights/ABTestClient;

.field private static c:Lcom/peel/util/a/a;

.field private static d:Lcom/amazon/insights/EventClient;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    sget-object v0, Lcom/peel/util/a/a;->a:Lcom/amazon/insights/AmazonInsights;

    if-nez v0, :cond_0

    .line 24
    const-string/jumbo v0, "33f01b7294b34261803b9eea8630ae4c"

    const-string/jumbo v1, "eZhE06OLUDOM/W/tNllXGxCKYksuAhWTcSrh7GK/C2g="

    invoke-static {v0, v1}, Lcom/amazon/insights/AmazonInsights;->newCredentials(Ljava/lang/String;Ljava/lang/String;)Lcom/amazon/insights/InsightsCredentials;

    move-result-object v0

    .line 25
    invoke-static {v0, p1}, Lcom/amazon/insights/AmazonInsights;->newInstance(Lcom/amazon/insights/InsightsCredentials;Landroid/content/Context;)Lcom/amazon/insights/AmazonInsights;

    move-result-object v0

    sput-object v0, Lcom/peel/util/a/a;->a:Lcom/amazon/insights/AmazonInsights;

    .line 26
    sget-object v0, Lcom/peel/util/a/a;->a:Lcom/amazon/insights/AmazonInsights;

    invoke-virtual {v0}, Lcom/amazon/insights/AmazonInsights;->getABTestClient()Lcom/amazon/insights/ABTestClient;

    move-result-object v0

    sput-object v0, Lcom/peel/util/a/a;->b:Lcom/amazon/insights/ABTestClient;

    .line 27
    sget-object v0, Lcom/peel/util/a/a;->a:Lcom/amazon/insights/AmazonInsights;

    invoke-virtual {v0}, Lcom/amazon/insights/AmazonInsights;->getEventClient()Lcom/amazon/insights/EventClient;

    move-result-object v0

    sput-object v0, Lcom/peel/util/a/a;->d:Lcom/amazon/insights/EventClient;

    .line 29
    :cond_0
    return-void
.end method

.method public static a(Landroid/content/Context;)Lcom/peel/util/a/a;
    .locals 1

    .prologue
    .line 32
    sget-object v0, Lcom/peel/util/a/a;->c:Lcom/peel/util/a/a;

    if-nez v0, :cond_0

    new-instance v0, Lcom/peel/util/a/a;

    invoke-direct {v0, p0}, Lcom/peel/util/a/a;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/peel/util/a/a;->c:Lcom/peel/util/a/a;

    .line 33
    :cond_0
    sget-object v0, Lcom/peel/util/a/a;->c:Lcom/peel/util/a/a;

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 71
    sget-object v0, Lcom/peel/util/a/a;->d:Lcom/amazon/insights/EventClient;

    invoke-interface {v0}, Lcom/amazon/insights/EventClient;->submitEvents()V

    .line 72
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 65
    sget-object v0, Lcom/peel/util/a/a;->d:Lcom/amazon/insights/EventClient;

    invoke-interface {v0, p1}, Lcom/amazon/insights/EventClient;->createEvent(Ljava/lang/String;)Lcom/amazon/insights/Event;

    move-result-object v0

    .line 66
    sget-object v1, Lcom/peel/util/a/a;->d:Lcom/amazon/insights/EventClient;

    invoke-interface {v1, v0}, Lcom/amazon/insights/EventClient;->recordEvent(Lcom/amazon/insights/Event;)V

    .line 67
    invoke-virtual {p0}, Lcom/peel/util/a/a;->a()V

    .line 68
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILcom/peel/util/t;Ljava/lang/String;)V
    .locals 10

    .prologue
    .line 45
    sget-object v0, Lcom/peel/util/a/a;->b:Lcom/amazon/insights/ABTestClient;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-interface {v0, v1}, Lcom/amazon/insights/ABTestClient;->getVariations([Ljava/lang/String;)Lcom/amazon/insights/InsightsHandler;

    move-result-object v9

    new-instance v0, Lcom/peel/util/a/b;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p3

    move-object v4, p2

    move v5, p5

    move-object v6, p4

    move-object/from16 v7, p7

    move-object/from16 v8, p6

    invoke-direct/range {v0 .. v8}, Lcom/peel/util/a/b;-><init>(Lcom/peel/util/a/a;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Lcom/peel/util/t;)V

    invoke-interface {v9, v0}, Lcom/amazon/insights/InsightsHandler;->setCallback(Lcom/amazon/insights/InsightsCallback;)V

    .line 62
    return-void
.end method

.method public a(Ljava/util/Map;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 37
    sget-object v0, Lcom/peel/util/a/a;->a:Lcom/amazon/insights/AmazonInsights;

    invoke-virtual {v0}, Lcom/amazon/insights/AmazonInsights;->getUserProfile()Lcom/amazon/insights/UserProfile;

    move-result-object v2

    .line 39
    invoke-interface {p1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 40
    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v2, v0, v1}, Lcom/amazon/insights/UserProfile;->addDimensionAsString(Ljava/lang/String;Ljava/lang/String;)Lcom/amazon/insights/UserProfile;

    goto :goto_0

    .line 42
    :cond_0
    return-void
.end method
