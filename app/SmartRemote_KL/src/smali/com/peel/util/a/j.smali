.class Lcom/peel/util/a/j;
.super Lcom/peel/util/t;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/peel/util/t",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/peel/util/a/i;


# direct methods
.method constructor <init>(Lcom/peel/util/a/i;I)V
    .locals 0

    .prologue
    .line 175
    iput-object p1, p0, Lcom/peel/util/a/j;->a:Lcom/peel/util/a/i;

    invoke-direct {p0, p2}, Lcom/peel/util/t;-><init>(I)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    .line 177
    iget-boolean v0, p0, Lcom/peel/util/a/j;->i:Z

    if-eqz v0, :cond_2

    .line 178
    invoke-static {}, Lcom/peel/util/a/f;->e()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "log events"

    new-instance v2, Lcom/peel/util/a/k;

    invoke-direct {v2, p0}, Lcom/peel/util/a/k;-><init>(Lcom/peel/util/a/j;)V

    invoke-static {v0, v1, v2}, Lcom/peel/util/i;->f(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)V

    .line 181
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "logged "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v0, p0, Lcom/peel/util/a/j;->a:Lcom/peel/util/a/i;

    iget-object v0, v0, Lcom/peel/util/a/i;->b:[Ljava/lang/String;

    if-nez v0, :cond_1

    const-string/jumbo v0, "0"

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " events to the server"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 182
    invoke-static {}, Lcom/peel/util/a/f;->e()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 191
    :cond_0
    :goto_1
    return-void

    .line 181
    :cond_1
    iget-object v0, p0, Lcom/peel/util/a/j;->a:Lcom/peel/util/a/i;

    iget-object v0, v0, Lcom/peel/util/a/i;->b:[Ljava/lang/String;

    array-length v0, v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    .line 185
    :cond_2
    invoke-static {}, Lcom/peel/util/a/f;->e()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "unable to clear the events database, error returned from the server: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/peel/util/a/j;->k:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 188
    sget-boolean v0, Lcom/peel/util/b/a;->a:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/peel/util/a/j;->a:Lcom/peel/util/a/i;

    iget-wide v0, v0, Lcom/peel/util/a/i;->c:J

    const-wide/32 v2, 0x2bf20

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    .line 189
    iget-object v0, p0, Lcom/peel/util/a/j;->a:Lcom/peel/util/a/i;

    iget-object v2, v0, Lcom/peel/util/a/i;->d:Lcom/peel/util/a/f;

    const-wide/16 v0, 0x0

    iget-object v3, p0, Lcom/peel/util/a/j;->a:Lcom/peel/util/a/i;

    iget-wide v4, v3, Lcom/peel/util/a/i;->c:J

    cmp-long v0, v0, v4

    if-nez v0, :cond_3

    invoke-static {}, Lcom/peel/util/a/f;->f()Lcom/peel/util/a/f;

    move-result-object v0

    invoke-static {v0}, Lcom/peel/util/a/f;->c(Lcom/peel/util/a/f;)J

    move-result-wide v0

    :goto_2
    invoke-static {v2, v0, v1}, Lcom/peel/util/a/f;->b(Lcom/peel/util/a/f;J)V

    goto :goto_1

    :cond_3
    iget-object v0, p0, Lcom/peel/util/a/j;->a:Lcom/peel/util/a/i;

    iget-wide v0, v0, Lcom/peel/util/a/i;->c:J

    const-wide/16 v4, 0x2

    mul-long/2addr v0, v4

    goto :goto_2
.end method
