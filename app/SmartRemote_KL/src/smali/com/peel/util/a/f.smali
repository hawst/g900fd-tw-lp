.class public Lcom/peel/util/a/f;
.super Ljava/lang/Object;


# static fields
.field private static final a:Ljava/lang/String;

.field private static b:Lcom/peel/util/a/f;


# instance fields
.field private c:Lcom/peel/util/a/e;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:J

.field private g:Z

.field private h:Z

.field private i:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 20
    const-class v0, Lcom/peel/util/a/f;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/peel/util/a/f;->a:Ljava/lang/String;

    .line 22
    const/4 v0, 0x0

    sput-object v0, Lcom/peel/util/a/f;->b:Lcom/peel/util/a/f;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v0, 0x0

    const/4 v2, 0x0

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-object v0, p0, Lcom/peel/util/a/f;->d:Ljava/lang/String;

    .line 25
    iput-object v0, p0, Lcom/peel/util/a/f;->e:Ljava/lang/String;

    .line 26
    const-wide/16 v0, 0x1388

    iput-wide v0, p0, Lcom/peel/util/a/f;->f:J

    .line 27
    iput-boolean v2, p0, Lcom/peel/util/a/f;->g:Z

    .line 28
    iput-boolean v2, p0, Lcom/peel/util/a/f;->h:Z

    .line 30
    iput-boolean v2, p0, Lcom/peel/util/a/f;->i:Z

    return-void
.end method

.method public static a()Lcom/peel/util/a/f;
    .locals 1

    .prologue
    .line 33
    sget-object v0, Lcom/peel/util/a/f;->b:Lcom/peel/util/a/f;

    if-nez v0, :cond_0

    new-instance v0, Lcom/peel/util/a/f;

    invoke-direct {v0}, Lcom/peel/util/a/f;-><init>()V

    sput-object v0, Lcom/peel/util/a/f;->b:Lcom/peel/util/a/f;

    .line 34
    :cond_0
    sget-object v0, Lcom/peel/util/a/f;->b:Lcom/peel/util/a/f;

    return-object v0
.end method

.method static synthetic a(Lcom/peel/util/a/f;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 19
    iget-object v0, p0, Lcom/peel/util/a/f;->d:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic a(Lcom/peel/util/a/f;J)V
    .locals 1

    .prologue
    .line 19
    invoke-direct {p0, p1, p2}, Lcom/peel/util/a/f;->d(J)V

    return-void
.end method

.method static synthetic b(Lcom/peel/util/a/f;)Lcom/peel/util/a/e;
    .locals 1

    .prologue
    .line 19
    iget-object v0, p0, Lcom/peel/util/a/f;->c:Lcom/peel/util/a/e;

    return-object v0
.end method

.method static synthetic b(Lcom/peel/util/a/f;J)V
    .locals 1

    .prologue
    .line 19
    invoke-direct {p0, p1, p2}, Lcom/peel/util/a/f;->c(J)V

    return-void
.end method

.method static synthetic c(Lcom/peel/util/a/f;)J
    .locals 2

    .prologue
    .line 19
    iget-wide v0, p0, Lcom/peel/util/a/f;->f:J

    return-wide v0
.end method

.method private c(J)V
    .locals 3

    .prologue
    .line 133
    const-wide/16 v0, 0x0

    cmp-long v0, v0, p1

    if-gez v0, :cond_1

    .line 134
    monitor-enter p0

    .line 135
    :try_start_0
    iget-boolean v0, p0, Lcom/peel/util/a/f;->i:Z

    if-eqz v0, :cond_0

    monitor-exit p0

    .line 143
    :goto_0
    return-void

    .line 136
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/peel/util/a/f;->i:Z

    .line 137
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 140
    :cond_1
    sget-object v0, Lcom/peel/util/a/f;->a:Ljava/lang/String;

    const-string/jumbo v1, "schedule next send"

    new-instance v2, Lcom/peel/util/a/h;

    invoke-direct {v2, p0, p1, p2}, Lcom/peel/util/a/h;-><init>(Lcom/peel/util/a/f;J)V

    invoke-static {v0, v1, v2, p1, p2}, Lcom/peel/util/i;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;J)V

    goto :goto_0

    .line 137
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method private c(Ljava/lang/String;)V
    .locals 13

    .prologue
    const/4 v1, 0x0

    .line 206
    :try_start_0
    const-string/jumbo v0, "\n"

    const-string/jumbo v2, ""

    invoke-virtual {p1, v0, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v2, "\r"

    const-string/jumbo v3, ""

    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 207
    const-string/jumbo v2, "^^"

    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 208
    array-length v5, v4

    move v3, v1

    :goto_0
    if-ge v3, v5, :cond_3

    aget-object v0, v4, v3

    .line 209
    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_2

    .line 210
    const-string/jumbo v2, "\""

    const-string/jumbo v6, ""

    invoke-virtual {v0, v2, v6}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v2, "^^"

    const-string/jumbo v6, ""

    invoke-virtual {v0, v2, v6}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 211
    const-string/jumbo v2, "\\|\\|"

    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    .line 212
    new-instance v7, Ljava/text/SimpleDateFormat;

    const-string/jumbo v0, "yyyy-MM-dd HH:mm:ss"

    invoke-direct {v7, v0}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 213
    const-string/jumbo v0, "UTC"

    invoke-static {v0}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v0

    invoke-virtual {v7, v0}, Ljava/text/SimpleDateFormat;->setTimeZone(Ljava/util/TimeZone;)V

    .line 214
    const-string/jumbo v0, ""

    move v2, v1

    .line 217
    :goto_1
    array-length v8, v6

    if-ge v2, v8, :cond_0

    .line 218
    aget-object v8, v6, v2

    const-string/jumbo v9, "|"

    const-string/jumbo v10, "\\|"

    invoke-virtual {v8, v9, v10}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v2

    .line 217
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 221
    :cond_0
    const/4 v2, 0x7

    aget-object v2, v6, v2

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v8

    .line 222
    const/4 v2, 0x7

    new-instance v10, Ljava/util/Date;

    invoke-direct {v10, v8, v9}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v7, v10}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v10

    aput-object v10, v6, v2

    .line 223
    array-length v10, v6

    move-object v2, v0

    move v0, v1

    :goto_2
    if-ge v0, v10, :cond_1

    aget-object v11, v6, v0

    .line 224
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v12, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v11, "|"

    invoke-virtual {v2, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 223
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 227
    :cond_1
    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v0

    invoke-virtual {v7, v0}, Ljava/text/SimpleDateFormat;->setTimeZone(Ljava/util/TimeZone;)V

    .line 228
    invoke-virtual {p0, v8, v9}, Lcom/peel/util/a/f;->b(J)Ljava/lang/String;

    move-result-object v0

    .line 229
    const/4 v6, 0x0

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v7

    add-int/lit8 v7, v7, -0x1

    invoke-virtual {v2, v6, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    .line 230
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v6, "|"

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 232
    invoke-static {}, Lcom/peel/a/a/a/g;->b()Lcom/peel/a/a/a/g;

    move-result-object v2

    new-instance v6, Lcom/peel/a/a/a/b/b;

    invoke-direct {v6, v0}, Lcom/peel/a/a/a/b/b;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v6}, Lcom/peel/a/a/a/g;->a(Lcom/peel/a/a/a/b/b;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 208
    :cond_2
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto/16 :goto_0

    .line 235
    :catch_0
    move-exception v0

    .line 236
    sget-object v1, Lcom/peel/util/a/f;->a:Ljava/lang/String;

    sget-object v2, Lcom/peel/util/a/f;->a:Ljava/lang/String;

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 238
    :cond_3
    return-void
.end method

.method private d(J)V
    .locals 9

    .prologue
    const/16 v8, 0x7c

    const/4 v2, 0x0

    .line 147
    iget-object v0, p0, Lcom/peel/util/a/f;->c:Lcom/peel/util/a/e;

    if-nez v0, :cond_1

    const/4 v0, 0x0

    .line 149
    :goto_0
    monitor-enter p0

    .line 150
    const/4 v1, 0x0

    :try_start_0
    iput-boolean v1, p0, Lcom/peel/util/a/f;->i:Z

    .line 151
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 152
    if-nez v0, :cond_2

    .line 200
    :cond_0
    :goto_1
    return-void

    .line 147
    :cond_1
    iget-object v0, p0, Lcom/peel/util/a/f;->c:Lcom/peel/util/a/e;

    invoke-virtual {v0}, Lcom/peel/util/a/e;->a()[Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 151
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 154
    :cond_2
    array-length v1, v0

    if-eqz v1, :cond_0

    .line 156
    array-length v1, v0

    new-array v3, v1, [Ljava/lang/String;

    .line 157
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    move v1, v2

    .line 158
    :goto_2
    array-length v5, v0

    if-ge v1, v5, :cond_4

    .line 159
    if-lez v1, :cond_3

    const-string/jumbo v5, "^^"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 161
    :cond_3
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v6, p0, Lcom/peel/util/a/f;->e:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, "||"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    aget-object v6, v0, v1

    aget-object v7, v0, v1

    invoke-virtual {v7, v8}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v7

    add-int/lit8 v7, v7, -0x1

    invoke-virtual {v6, v2, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 162
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 163
    invoke-direct {p0, v5}, Lcom/peel/util/a/f;->c(Ljava/lang/String;)V

    .line 164
    aget-object v6, v0, v1

    aget-object v7, v0, v1

    invoke-virtual {v7, v8}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v7

    add-int/lit8 v7, v7, 0x1

    invoke-virtual {v6, v7}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v3, v1

    .line 165
    sget-object v6, Lcom/peel/util/a/f;->a:Ljava/lang/String;

    invoke-static {v6, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 158
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 169
    :cond_4
    iget-boolean v0, p0, Lcom/peel/util/a/f;->g:Z

    if-nez v0, :cond_5

    .line 170
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 171
    const-string/jumbo v0, "event"

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 172
    sget-object v6, Lcom/peel/util/a/f;->a:Ljava/lang/String;

    const-string/jumbo v7, "insights upload"

    const/4 v8, 0x1

    new-instance v0, Lcom/peel/util/a/i;

    move-object v1, p0

    move-wide v4, p1

    invoke-direct/range {v0 .. v5}, Lcom/peel/util/a/i;-><init>(Lcom/peel/util/a/f;Ljava/util/HashMap;[Ljava/lang/String;J)V

    invoke-static {v6, v7, v8, v0}, Lcom/peel/util/i;->a(Ljava/lang/String;Ljava/lang/String;ILjava/lang/Runnable;)Ljava/util/concurrent/Future;

    goto/16 :goto_1

    .line 196
    :cond_5
    sget-object v0, Lcom/peel/util/a/f;->a:Ljava/lang/String;

    const-string/jumbo v1, "log events"

    new-instance v2, Lcom/peel/util/a/l;

    invoke-direct {v2, p0, v3}, Lcom/peel/util/a/l;-><init>(Lcom/peel/util/a/f;[Ljava/lang/String;)V

    invoke-static {v0, v1, v2}, Lcom/peel/util/i;->f(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)V

    goto/16 :goto_1
.end method

.method static synthetic e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lcom/peel/util/a/f;->a:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic f()Lcom/peel/util/a/f;
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lcom/peel/util/a/f;->b:Lcom/peel/util/a/f;

    return-object v0
.end method


# virtual methods
.method public a(III)V
    .locals 12

    .prologue
    const/4 v4, 0x0

    const/4 v5, -0x1

    .line 90
    move-object v0, p0

    move v1, p1

    move v2, p2

    move v3, p3

    move-object v6, v4

    move v7, v5

    move-object v8, v4

    move v9, v5

    move-object v10, v4

    move v11, v5

    invoke-virtual/range {v0 .. v11}, Lcom/peel/util/a/f;->a(IIILjava/lang/String;ILjava/lang/String;ILjava/lang/String;ILjava/lang/String;I)V

    return-void
.end method

.method public a(IIILjava/lang/String;)V
    .locals 12

    .prologue
    .line 92
    const/4 v5, -0x1

    const/4 v6, 0x0

    const/4 v7, -0x1

    const/4 v8, 0x0

    const/4 v9, -0x1

    const/4 v10, 0x0

    const/4 v11, -0x1

    move-object v0, p0

    move v1, p1

    move v2, p2

    move v3, p3

    move-object/from16 v4, p4

    invoke-virtual/range {v0 .. v11}, Lcom/peel/util/a/f;->a(IIILjava/lang/String;ILjava/lang/String;ILjava/lang/String;ILjava/lang/String;I)V

    return-void
.end method

.method public a(IIILjava/lang/String;I)V
    .locals 12

    .prologue
    .line 96
    const/4 v6, 0x0

    const/4 v7, -0x1

    const/4 v8, 0x0

    const/4 v9, -0x1

    const/4 v10, 0x0

    const/4 v11, -0x1

    move-object v0, p0

    move v1, p1

    move v2, p2

    move v3, p3

    move-object/from16 v4, p4

    move/from16 v5, p5

    invoke-virtual/range {v0 .. v11}, Lcom/peel/util/a/f;->a(IIILjava/lang/String;ILjava/lang/String;ILjava/lang/String;ILjava/lang/String;I)V

    return-void
.end method

.method public a(IIILjava/lang/String;ILjava/lang/String;I)V
    .locals 12

    .prologue
    .line 94
    const/4 v8, 0x0

    const/4 v9, -0x1

    const/4 v10, 0x0

    const/4 v11, -0x1

    move-object v0, p0

    move v1, p1

    move v2, p2

    move v3, p3

    move-object/from16 v4, p4

    move/from16 v5, p5

    move-object/from16 v6, p6

    move/from16 v7, p7

    invoke-virtual/range {v0 .. v11}, Lcom/peel/util/a/f;->a(IIILjava/lang/String;ILjava/lang/String;ILjava/lang/String;ILjava/lang/String;I)V

    return-void
.end method

.method public a(IIILjava/lang/String;ILjava/lang/String;ILjava/lang/String;I)V
    .locals 12

    .prologue
    .line 98
    const/4 v10, 0x0

    const/4 v11, -0x1

    move-object v0, p0

    move v1, p1

    move v2, p2

    move v3, p3

    move-object/from16 v4, p4

    move/from16 v5, p5

    move-object/from16 v6, p6

    move/from16 v7, p7

    move-object/from16 v8, p8

    move/from16 v9, p9

    invoke-virtual/range {v0 .. v11}, Lcom/peel/util/a/f;->a(IIILjava/lang/String;ILjava/lang/String;ILjava/lang/String;ILjava/lang/String;I)V

    return-void
.end method

.method public a(IIILjava/lang/String;ILjava/lang/String;ILjava/lang/String;ILjava/lang/String;I)V
    .locals 19

    .prologue
    .line 108
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v2

    .line 109
    invoke-virtual {v2}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v4

    const/16 v3, 0xf

    invoke-virtual {v2, v3}, Ljava/util/Calendar;->get(I)I

    move-result v3

    int-to-long v6, v3

    add-long/2addr v4, v6

    const/16 v3, 0x10

    invoke-virtual {v2, v3}, Ljava/util/Calendar;->get(I)I

    move-result v2

    int-to-long v2, v2

    add-long/2addr v4, v2

    .line 111
    sget-object v17, Lcom/peel/util/a/f;->a:Ljava/lang/String;

    const-string/jumbo v18, "add event"

    new-instance v2, Lcom/peel/util/a/g;

    move-object/from16 v3, p0

    move/from16 v6, p1

    move/from16 v7, p2

    move/from16 v8, p3

    move-object/from16 v9, p4

    move/from16 v10, p5

    move-object/from16 v11, p6

    move/from16 v12, p7

    move-object/from16 v13, p8

    move/from16 v14, p9

    move-object/from16 v15, p10

    move/from16 v16, p11

    invoke-direct/range {v2 .. v16}, Lcom/peel/util/a/g;-><init>(Lcom/peel/util/a/f;JIIILjava/lang/String;ILjava/lang/String;ILjava/lang/String;ILjava/lang/String;I)V

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-static {v0, v1, v2}, Lcom/peel/util/i;->f(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)V

    .line 117
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 118
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/peel/util/a/f;->e:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "||"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/peel/util/a/f;->d:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "||"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, p1

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "||"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, p2

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "||"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 119
    move/from16 v0, p3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "||"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p4

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "||"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, p5

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "||"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 120
    move-object/from16 v0, p6

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "||"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, p7

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "||"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 121
    move-object/from16 v0, p8

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "||"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, p9

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "||"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 122
    move-object/from16 v0, p10

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "||"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, p11

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 124
    sget-object v3, Lcom/peel/util/a/f;->a:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "LoggingEvents: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 126
    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/peel/util/a/f;->f:J

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v3}, Lcom/peel/util/a/f;->c(J)V

    .line 127
    return-void
.end method

.method public a(J)V
    .locals 3

    .prologue
    .line 42
    iput-wide p1, p0, Lcom/peel/util/a/f;->f:J

    .line 45
    const-wide/16 v0, 0x0

    invoke-direct {p0, v0, v1}, Lcom/peel/util/a/f;->c(J)V

    .line 46
    return-void
.end method

.method public a(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 49
    new-instance v0, Lcom/peel/util/a/e;

    invoke-direct {v0, p1}, Lcom/peel/util/a/e;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/peel/util/a/f;->c:Lcom/peel/util/a/e;

    .line 52
    iget-boolean v0, p0, Lcom/peel/util/a/f;->h:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/peel/util/a/f;->d:Ljava/lang/String;

    if-nez v0, :cond_1

    :cond_0
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/util/a/f;->d:Ljava/lang/String;

    .line 53
    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/peel/util/a/f;->h:Z

    .line 54
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 61
    iput-object p1, p0, Lcom/peel/util/a/f;->e:Ljava/lang/String;

    .line 62
    invoke-static {}, Lcom/peel/a/a/a/g;->b()Lcom/peel/a/a/a/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/a/a/a/g;->a()Lcom/peel/a/a/a/a/a;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/peel/a/a/a/a/a;->c(Ljava/lang/String;)V

    .line 63
    invoke-static {}, Lcom/peel/a/a/a/g;->b()Lcom/peel/a/a/a/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/a/a/a/g;->a()Lcom/peel/a/a/a/a/a;

    move-result-object v0

    const-string/jumbo v1, "user_id"

    invoke-virtual {v0, v1, p1}, Lcom/peel/a/a/a/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 64
    invoke-static {}, Lcom/peel/a/a/a/g;->c()V

    .line 65
    invoke-virtual {p0}, Lcom/peel/util/a/f;->d()V

    .line 66
    return-void
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/peel/util/a/f;->d:Ljava/lang/String;

    return-object v0
.end method

.method public b(J)Ljava/lang/String;
    .locals 5

    .prologue
    .line 242
    :try_start_0
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 243
    const/16 v1, 0xf

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->get(I)I

    move-result v1

    const/16 v2, 0x10

    invoke-virtual {v0, v2}, Ljava/util/Calendar;->get(I)I

    move-result v0

    add-int/2addr v0, v1

    .line 244
    int-to-long v0, v0

    sub-long v0, p1, v0

    .line 245
    new-instance v2, Ljava/text/SimpleDateFormat;

    const-string/jumbo v3, "yyyy-MM-dd HH:mm:ss"

    invoke-direct {v2, v3}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 246
    const-string/jumbo v3, "GMT"

    invoke-static {v3}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/text/SimpleDateFormat;->setTimeZone(Ljava/util/TimeZone;)V

    .line 247
    new-instance v3, Ljava/util/Date;

    invoke-direct {v3, v0, v1}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v2, v3}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 251
    :goto_0
    return-object v0

    .line 248
    :catch_0
    move-exception v0

    .line 249
    sget-object v1, Lcom/peel/util/a/f;->a:Ljava/lang/String;

    sget-object v2, Lcom/peel/util/a/f;->a:Ljava/lang/String;

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 251
    const-string/jumbo v0, ""

    goto :goto_0
.end method

.method public b(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 76
    iget-object v0, p0, Lcom/peel/util/a/f;->e:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 77
    iput-object p1, p0, Lcom/peel/util/a/f;->e:Ljava/lang/String;

    .line 78
    invoke-static {}, Lcom/peel/a/a/a/g;->b()Lcom/peel/a/a/a/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/a/a/a/g;->a()Lcom/peel/a/a/a/a/a;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/util/a/f;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/peel/a/a/a/a/a;->c(Ljava/lang/String;)V

    .line 79
    invoke-static {}, Lcom/peel/a/a/a/g;->b()Lcom/peel/a/a/a/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/a/a/a/g;->a()Lcom/peel/a/a/a/a/a;

    move-result-object v0

    const-string/jumbo v1, "user_id"

    iget-object v2, p0, Lcom/peel/util/a/f;->e:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/peel/a/a/a/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 80
    invoke-static {}, Lcom/peel/a/a/a/g;->c()V

    .line 81
    invoke-virtual {p0}, Lcom/peel/util/a/f;->d()V

    .line 83
    :cond_0
    return-void
.end method

.method public c()V
    .locals 1

    .prologue
    .line 58
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/peel/util/a/f;->h:Z

    return-void
.end method

.method public d()V
    .locals 2

    .prologue
    .line 87
    const-wide/16 v0, 0x0

    invoke-direct {p0, v0, v1}, Lcom/peel/util/a/f;->c(J)V

    .line 88
    return-void
.end method
