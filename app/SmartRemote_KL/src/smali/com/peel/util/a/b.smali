.class Lcom/peel/util/a/b;
.super Lcom/amazon/insights/InsightsCallback;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/amazon/insights/InsightsCallback",
        "<",
        "Lcom/amazon/insights/VariationSet;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic b:Ljava/lang/String;

.field final synthetic c:Ljava/lang/String;

.field final synthetic d:I

.field final synthetic e:Ljava/lang/String;

.field final synthetic f:Ljava/lang/String;

.field final synthetic g:Lcom/peel/util/t;

.field final synthetic h:Lcom/peel/util/a/a;


# direct methods
.method constructor <init>(Lcom/peel/util/a/a;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Lcom/peel/util/t;)V
    .locals 0

    .prologue
    .line 45
    iput-object p1, p0, Lcom/peel/util/a/b;->h:Lcom/peel/util/a/a;

    iput-object p2, p0, Lcom/peel/util/a/b;->a:Ljava/lang/String;

    iput-object p3, p0, Lcom/peel/util/a/b;->b:Ljava/lang/String;

    iput-object p4, p0, Lcom/peel/util/a/b;->c:Ljava/lang/String;

    iput p5, p0, Lcom/peel/util/a/b;->d:I

    iput-object p6, p0, Lcom/peel/util/a/b;->e:Ljava/lang/String;

    iput-object p7, p0, Lcom/peel/util/a/b;->f:Ljava/lang/String;

    iput-object p8, p0, Lcom/peel/util/a/b;->g:Lcom/peel/util/t;

    invoke-direct {p0}, Lcom/amazon/insights/InsightsCallback;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/amazon/insights/VariationSet;)V
    .locals 12

    .prologue
    const/4 v1, -0x1

    .line 49
    iget-object v0, p0, Lcom/peel/util/a/b;->a:Ljava/lang/String;

    invoke-interface {p1, v0}, Lcom/amazon/insights/VariationSet;->getVariation(Ljava/lang/String;)Lcom/amazon/insights/Variation;

    move-result-object v0

    .line 50
    invoke-interface {v0}, Lcom/amazon/insights/Variation;->getName()Ljava/lang/String;

    move-result-object v10

    .line 51
    const-string/jumbo v2, "EXCLUSIVE CONTROL"

    invoke-virtual {v10, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v6, p0, Lcom/peel/util/a/b;->b:Ljava/lang/String;

    .line 53
    :goto_0
    const-string/jumbo v0, "EXCLUSIVE CONTROL"

    invoke-virtual {v10, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 54
    invoke-static {}, Lcom/peel/util/a/f;->a()Lcom/peel/util/a/f;

    move-result-object v0

    const/16 v2, 0x9c5

    iget v3, p0, Lcom/peel/util/a/b;->d:I

    iget-object v4, p0, Lcom/peel/util/a/b;->e:Ljava/lang/String;

    iget-object v8, p0, Lcom/peel/util/a/b;->a:Ljava/lang/String;

    move v5, v1

    move v7, v1

    move v9, v1

    move v11, v1

    invoke-virtual/range {v0 .. v11}, Lcom/peel/util/a/f;->a(IIILjava/lang/String;ILjava/lang/String;ILjava/lang/String;ILjava/lang/String;I)V

    .line 56
    iget-object v0, p0, Lcom/peel/util/a/b;->f:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 57
    iget-object v0, p0, Lcom/peel/util/a/b;->h:Lcom/peel/util/a/a;

    iget-object v1, p0, Lcom/peel/util/a/b;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/peel/util/a/a;->a(Ljava/lang/String;)V

    .line 59
    :cond_0
    iget-object v0, p0, Lcom/peel/util/a/b;->g:Lcom/peel/util/t;

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v6, v2}, Lcom/peel/util/t;->a(ZLjava/lang/Object;Ljava/lang/String;)V

    .line 60
    return-void

    .line 51
    :cond_1
    iget-object v2, p0, Lcom/peel/util/a/b;->c:Ljava/lang/String;

    iget-object v3, p0, Lcom/peel/util/a/b;->b:Ljava/lang/String;

    invoke-interface {v0, v2, v3}, Lcom/amazon/insights/Variation;->getVariableAsString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    goto :goto_0
.end method

.method public synthetic onComplete(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 45
    check-cast p1, Lcom/amazon/insights/VariationSet;

    invoke-virtual {p0, p1}, Lcom/peel/util/a/b;->a(Lcom/amazon/insights/VariationSet;)V

    return-void
.end method
