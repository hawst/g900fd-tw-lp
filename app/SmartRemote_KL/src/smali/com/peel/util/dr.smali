.class public Lcom/peel/util/dr;
.super Ljava/util/concurrent/ThreadPoolExecutor;


# direct methods
.method public constructor <init>(IIJLjava/util/concurrent/TimeUnit;)V
    .locals 9

    .prologue
    .line 14
    new-instance v7, Ljava/util/concurrent/PriorityBlockingQueue;

    const/16 v0, 0xb

    new-instance v1, Lcom/peel/util/dv;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/peel/util/dv;-><init>(Lcom/peel/util/ds;)V

    invoke-direct {v7, v0, v1}, Ljava/util/concurrent/PriorityBlockingQueue;-><init>(ILjava/util/Comparator;)V

    move-object v1, p0

    move v2, p1

    move v3, p2

    move-wide v4, p3

    move-object v6, p5

    invoke-direct/range {v1 .. v7}, Ljava/util/concurrent/ThreadPoolExecutor;-><init>(IIJLjava/util/concurrent/TimeUnit;Ljava/util/concurrent/BlockingQueue;)V

    .line 15
    return-void
.end method


# virtual methods
.method protected newTaskFor(Ljava/lang/Runnable;Ljava/lang/Object;)Ljava/util/concurrent/RunnableFuture;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Runnable;",
            "TT;)",
            "Ljava/util/concurrent/RunnableFuture",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 18
    instance-of v0, p1, Lcom/peel/util/dt;

    if-eqz v0, :cond_0

    .line 19
    new-instance v1, Lcom/peel/util/du;

    move-object v0, p1

    check-cast v0, Lcom/peel/util/dt;

    invoke-virtual {v0}, Lcom/peel/util/dt;->a()I

    move-result v0

    invoke-direct {v1, v0, p1, p2}, Lcom/peel/util/du;-><init>(ILjava/lang/Runnable;Ljava/lang/Object;)V

    move-object v0, v1

    .line 20
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/peel/util/du;

    const/4 v1, 0x0

    invoke-direct {v0, v1, p1, p2}, Lcom/peel/util/du;-><init>(ILjava/lang/Runnable;Ljava/lang/Object;)V

    goto :goto_0
.end method
