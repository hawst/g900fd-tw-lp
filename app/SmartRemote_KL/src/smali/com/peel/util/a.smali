.class public Lcom/peel/util/a;
.super Ljava/lang/Object;


# static fields
.field public static final a:[Ljava/lang/String;

.field public static final b:[Ljava/lang/String;

.field public static final c:[Ljava/lang/String;

.field public static final d:[Ljava/lang/String;

.field public static e:[I

.field public static f:Z

.field public static g:Z

.field public static h:Z

.field public static i:Z

.field public static j:Ljava/lang/String;

.field public static k:Ljava/lang/String;

.field public static l:Z

.field public static m:Z

.field public static n:Ljava/lang/String;

.field public static final o:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "[",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 32
    new-array v0, v8, [Ljava/lang/String;

    const-string/jumbo v1, "control"

    aput-object v1, v0, v5

    const-string/jumbo v1, "toppicks"

    aput-object v1, v0, v4

    const-string/jumbo v1, "favorites"

    aput-object v1, v0, v6

    const-string/jumbo v1, "guide"

    aput-object v1, v0, v7

    sput-object v0, Lcom/peel/util/a;->a:[Ljava/lang/String;

    .line 33
    new-array v0, v6, [Ljava/lang/String;

    const-string/jumbo v1, "control"

    aput-object v1, v0, v5

    const-string/jumbo v1, "opencard"

    aput-object v1, v0, v4

    sput-object v0, Lcom/peel/util/a;->b:[Ljava/lang/String;

    .line 34
    new-array v0, v7, [Ljava/lang/String;

    const-string/jumbo v1, "FORCE_TUTORIAL"

    aput-object v1, v0, v5

    const-string/jumbo v1, "TUTORIAL_ALLOW_EXIT"

    aput-object v1, v0, v4

    const-string/jumbo v1, "TUTORIAL_TRIGGER"

    aput-object v1, v0, v6

    sput-object v0, Lcom/peel/util/a;->c:[Ljava/lang/String;

    .line 35
    new-array v0, v7, [Ljava/lang/String;

    const-string/jumbo v1, "NO_LOAD"

    aput-object v1, v0, v5

    const-string/jumbo v1, "IR_AND_GUIDE"

    aput-object v1, v0, v4

    const-string/jumbo v1, "GUIDE_ONLY"

    aput-object v1, v0, v6

    sput-object v0, Lcom/peel/util/a;->d:[Ljava/lang/String;

    .line 49
    sput-boolean v4, Lcom/peel/util/a;->g:Z

    .line 50
    sput-boolean v4, Lcom/peel/util/a;->h:Z

    .line 51
    sput-boolean v4, Lcom/peel/util/a;->i:Z

    .line 53
    const/4 v0, 0x0

    sput-object v0, Lcom/peel/util/a;->j:Ljava/lang/String;

    .line 54
    const/4 v0, 0x0

    sput-object v0, Lcom/peel/util/a;->k:Ljava/lang/String;

    .line 55
    sput-boolean v5, Lcom/peel/util/a;->l:Z

    .line 56
    sput-boolean v4, Lcom/peel/util/a;->m:Z

    .line 57
    const/4 v0, 0x0

    sput-object v0, Lcom/peel/util/a;->n:Ljava/lang/String;

    .line 59
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/peel/util/a;->o:Ljava/util/Map;

    .line 61
    sget-object v0, Lcom/peel/util/a;->o:Ljava/util/Map;

    const-string/jumbo v1, "landingpage"

    new-array v2, v8, [Ljava/lang/String;

    const-string/jumbo v3, "screenname"

    aput-object v3, v2, v5

    const-string/jumbo v3, "toppicks"

    aput-object v3, v2, v4

    const-string/jumbo v3, "LANDING_PAGE"

    aput-object v3, v2, v6

    const-string/jumbo v3, "landingpageinit"

    aput-object v3, v2, v7

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 62
    sget-object v0, Lcom/peel/util/a;->o:Ljava/util/Map;

    const-string/jumbo v1, "tuneinoptions"

    new-array v2, v8, [Ljava/lang/String;

    const-string/jumbo v3, "option"

    aput-object v3, v2, v5

    const-string/jumbo v3, "control"

    aput-object v3, v2, v4

    const-string/jumbo v3, "TUNEIN_OPTIONS"

    aput-object v3, v2, v6

    const-string/jumbo v3, "tuninoptinit"

    aput-object v3, v2, v7

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 63
    sget-object v0, Lcom/peel/util/a;->o:Ljava/util/Map;

    const-string/jumbo v1, "tiletitle"

    new-array v2, v8, [Ljava/lang/String;

    const-string/jumbo v3, "showtitle"

    aput-object v3, v2, v5

    const-string/jumbo v3, "YES"

    aput-object v3, v2, v4

    const-string/jumbo v3, "TILE_TITLE"

    aput-object v3, v2, v6

    const-string/jumbo v3, "tiletitleinit"

    aput-object v3, v2, v7

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 64
    sget-object v0, Lcom/peel/util/a;->o:Ljava/util/Map;

    const-string/jumbo v1, "RemoteSetupTrigger"

    new-array v2, v8, [Ljava/lang/String;

    const-string/jumbo v3, "showRemoteTutorial"

    aput-object v3, v2, v5

    const-string/jumbo v3, "TUTORIAL_ALLOW_EXIT"

    aput-object v3, v2, v4

    const-string/jumbo v3, "FORCE_REMOTE_TUTORIAL"

    aput-object v3, v2, v6

    const-string/jumbo v3, "remoteTutorial"

    aput-object v3, v2, v7

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 65
    sget-object v0, Lcom/peel/util/a;->o:Ljava/util/Map;

    const-string/jumbo v1, "Setup"

    new-array v2, v8, [Ljava/lang/String;

    const-string/jumbo v3, "showPersonalize"

    aput-object v3, v2, v5

    const-string/jumbo v3, "No"

    aput-object v3, v2, v4

    const-string/jumbo v3, "SHOW_PERSONALIZE"

    aput-object v3, v2, v6

    const-string/jumbo v3, "showpersonalize"

    aput-object v3, v2, v7

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 66
    sget-object v0, Lcom/peel/util/a;->o:Ljava/util/Map;

    const-string/jumbo v1, "remotechannelguide"

    new-array v2, v8, [Ljava/lang/String;

    const-string/jumbo v3, "guide_load"

    aput-object v3, v2, v5

    const-string/jumbo v3, "NO_LOAD"

    aput-object v3, v2, v4

    const-string/jumbo v3, "REMOTE_CHANNEL_GUIDE"

    aput-object v3, v2, v6

    const-string/jumbo v3, "showguideinit"

    aput-object v3, v2, v7

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 67
    sget-object v0, Lcom/peel/util/a;->o:Ljava/util/Map;

    const-string/jumbo v1, "Remote Setup"

    new-array v2, v8, [Ljava/lang/String;

    const-string/jumbo v3, "showhelp"

    aput-object v3, v2, v5

    const-string/jumbo v3, "YES"

    aput-object v3, v2, v4

    const-string/jumbo v3, "SHOW_HELP_SCREENS"

    aput-object v3, v2, v6

    const-string/jumbo v3, "showhelpscreens"

    aput-object v3, v2, v7

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 68
    sget-object v0, Lcom/peel/util/a;->o:Ljava/util/Map;

    const-string/jumbo v1, "Next Button"

    new-array v2, v8, [Ljava/lang/String;

    const-string/jumbo v3, "show"

    aput-object v3, v2, v5

    const-string/jumbo v3, "YES"

    aput-object v3, v2, v4

    const-string/jumbo v3, "SHOW_NEXT_BUTTON"

    aput-object v3, v2, v6

    const-string/jumbo v3, "shownextbutton"

    aput-object v3, v2, v7

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 69
    return-void
.end method

.method public static a()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 123
    sput-object v0, Lcom/peel/util/a;->j:Ljava/lang/String;

    .line 124
    sput-object v0, Lcom/peel/util/a;->k:Ljava/lang/String;

    .line 125
    sput-object v0, Lcom/peel/util/a;->e:[I

    .line 126
    const/4 v0, 0x0

    sput-boolean v0, Lcom/peel/util/a;->f:Z

    .line 127
    return-void
.end method

.method public static a(Ljava/lang/String;Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 118
    invoke-static {p1}, Lcom/peel/util/a/a;->a(Landroid/content/Context;)Lcom/peel/util/a/a;

    move-result-object v0

    .line 119
    invoke-virtual {v0, p0}, Lcom/peel/util/a/a;->a(Ljava/lang/String;)V

    .line 120
    return-void
.end method

.method public static a(Ljava/lang/String;Landroid/content/Context;Lcom/peel/util/t;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Landroid/content/Context;",
            "Lcom/peel/util/t",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 73
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 74
    const-string/jumbo v0, "appname"

    const-string/jumbo v2, "WATCHON"

    invoke-virtual {v1, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 75
    const-string/jumbo v0, "country"

    sget-object v2, Lcom/peel/content/a;->e:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 76
    const-string/jumbo v0, "device"

    const-string/jumbo v2, "HANDSET"

    invoke-virtual {v1, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 78
    invoke-static {p1}, Lcom/peel/util/a/a;->a(Landroid/content/Context;)Lcom/peel/util/a/a;

    move-result-object v0

    .line 79
    invoke-virtual {v0, v1}, Lcom/peel/util/a/a;->a(Ljava/util/Map;)V

    .line 81
    sget-object v1, Lcom/peel/util/a;->o:Ljava/util/Map;

    invoke-interface {v1, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Ljava/lang/String;

    .line 82
    const/4 v2, 0x0

    aget-object v2, v1, v2

    const/4 v3, 0x1

    aget-object v3, v1, v3

    const/4 v4, 0x2

    aget-object v4, v1, v4

    const/16 v5, 0x7db

    new-instance v6, Lcom/peel/util/b;

    invoke-direct {v6, v1, p2}, Lcom/peel/util/b;-><init>([Ljava/lang/String;Lcom/peel/util/t;)V

    const/4 v7, 0x3

    aget-object v7, v1, v7

    move-object v1, p0

    invoke-virtual/range {v0 .. v7}, Lcom/peel/util/a/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILcom/peel/util/t;Ljava/lang/String;)V

    .line 92
    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;Lcom/peel/util/t;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Landroid/content/Context;",
            "Lcom/peel/util/t",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 97
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 98
    const-string/jumbo v0, "appname"

    const-string/jumbo v2, "WATCHON"

    invoke-virtual {v1, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 99
    const-string/jumbo v0, "country"

    sget-object v2, Lcom/peel/content/a;->e:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 100
    const-string/jumbo v0, "device"

    const-string/jumbo v2, "HANDSET"

    invoke-virtual {v1, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 102
    invoke-static {p5}, Lcom/peel/util/a/a;->a(Landroid/content/Context;)Lcom/peel/util/a/a;

    move-result-object v0

    .line 103
    invoke-virtual {v0, v1}, Lcom/peel/util/a/a;->a(Ljava/util/Map;)V

    .line 105
    const/16 v5, 0x7db

    new-instance v6, Lcom/peel/util/c;

    invoke-direct {v6, p2, p6}, Lcom/peel/util/c;-><init>(Ljava/lang/String;Lcom/peel/util/t;)V

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v7, p4

    invoke-virtual/range {v0 .. v7}, Lcom/peel/util/a/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILcom/peel/util/t;Ljava/lang/String;)V

    .line 115
    return-void
.end method
