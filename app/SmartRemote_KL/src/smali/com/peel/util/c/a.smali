.class public Lcom/peel/util/c/a;
.super Ljava/lang/Object;


# static fields
.field private static final a:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 31
    const-class v0, Lcom/peel/util/c/a;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/peel/util/c/a;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Ljava/lang/String;)Landroid/os/Bundle;
    .locals 5

    .prologue
    .line 34
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 35
    const-string/jumbo v0, "\\|"

    invoke-static {p0, v0}, Landroid/text/TextUtils;->split(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 36
    const/4 v0, 0x0

    :goto_0
    array-length v3, v2

    if-ge v0, v3, :cond_0

    .line 37
    aget-object v3, v2, v0

    add-int/lit8 v4, v0, 0x1

    aget-object v4, v2, v4

    invoke-virtual {v1, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 36
    add-int/lit8 v0, v0, 0x2

    goto :goto_0

    .line 39
    :cond_0
    return-object v1
.end method

.method static synthetic a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 29
    sget-object v0, Lcom/peel/util/c/a;->a:Ljava/lang/String;

    return-object v0
.end method

.method public static varargs a(Landroid/content/Context;Ljava/util/HashMap;Ljava/util/Map;[Ljava/io/File;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;[",
            "Ljava/io/File;",
            ")V"
        }
    .end annotation

    .prologue
    .line 84
    new-instance v0, Lcom/peel/util/c/c;

    const/4 v1, 0x2

    invoke-direct {v0, v1, p0, p1, p2}, Lcom/peel/util/c/c;-><init>(ILandroid/content/Context;Ljava/util/HashMap;Ljava/util/Map;)V

    invoke-static {v0, p3}, Lcom/peel/util/c/a;->a(Lcom/peel/util/t;[Ljava/io/File;)V

    .line 204
    return-void
.end method

.method private static varargs a(Lcom/peel/util/t;[Ljava/io/File;)V
    .locals 9

    .prologue
    .line 43
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 44
    array-length v0, p1

    if-nez v0, :cond_1

    .line 45
    const/4 v0, 0x1

    const/4 v2, 0x0

    invoke-virtual {p0, v0, v1, v2}, Lcom/peel/util/t;->a(ZLjava/lang/Object;Ljava/lang/String;)V

    .line 81
    :cond_0
    return-void

    .line 49
    :cond_1
    new-instance v2, Ljava/util/concurrent/atomic/AtomicInteger;

    array-length v0, p1

    invoke-direct {v2, v0}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    .line 50
    array-length v3, p1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_0

    aget-object v4, p1, v0

    .line 51
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "https://zelfy.zendesk.com/api/v2/uploads.json?filename="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v4}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const-string/jumbo v6, "courier@zelfy.com"

    const-string/jumbo v7, "zelfy123"

    new-instance v8, Lcom/peel/util/c/b;

    invoke-direct {v8, v4, v1, v2, p0}, Lcom/peel/util/c/b;-><init>(Ljava/io/File;Ljava/util/List;Ljava/util/concurrent/atomic/AtomicInteger;Lcom/peel/util/t;)V

    invoke-static {v5, v6, v7, v4, v8}, Lcom/peel/util/b/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/io/File;Lcom/peel/util/t;)V

    .line 50
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method
