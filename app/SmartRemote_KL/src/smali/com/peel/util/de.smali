.class Lcom/peel/util/de;
.super Lcom/peel/util/t;


# instance fields
.field final synthetic a:Lcom/peel/util/dd;


# direct methods
.method constructor <init>(Lcom/peel/util/dd;)V
    .locals 0

    .prologue
    .line 2753
    iput-object p1, p0, Lcom/peel/util/de;->a:Lcom/peel/util/dd;

    invoke-direct {p0}, Lcom/peel/util/t;-><init>()V

    return-void
.end method


# virtual methods
.method public a(ZLjava/lang/Object;Ljava/lang/String;)V
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 2756
    if-eqz p1, :cond_1

    .line 2757
    check-cast p2, Ljava/lang/String;

    .line 2759
    if-eqz p2, :cond_3

    const-string/jumbo v0, "null"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 2761
    :try_start_0
    invoke-static {}, Lcom/peel/util/bx;->f()Lorg/codehaus/jackson/map/ObjectMapper;

    move-result-object v0

    const-class v1, Ljava/util/HashMap;

    invoke-virtual {v0, p2, v1}, Lorg/codehaus/jackson/map/ObjectMapper;->readValue(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    .line 2762
    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    .line 2764
    const-string/jumbo v1, "bgimage"

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2765
    const-string/jumbo v1, "bgimage"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2768
    :goto_0
    :try_start_1
    const-string/jumbo v3, "tracking_url"

    invoke-interface {v0, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    if-eqz v1, :cond_0

    .line 2769
    const-string/jumbo v3, "tracking_url"

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2770
    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_0

    .line 2771
    new-instance v3, Lcom/peel/util/df;

    invoke-direct {v3, p0, v0}, Lcom/peel/util/df;-><init>(Lcom/peel/util/de;Ljava/lang/String;)V

    invoke-static {v0, v3}, Lcom/peel/util/b/a;->b(Ljava/lang/String;Lcom/peel/util/t;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    :cond_0
    move-object v0, v1

    .line 2789
    :goto_1
    iget-object v1, p0, Lcom/peel/util/de;->a:Lcom/peel/util/dd;

    iget-object v1, v1, Lcom/peel/util/dd;->c:Lcom/peel/util/t;

    if-eqz v1, :cond_1

    .line 2790
    iget-object v1, p0, Lcom/peel/util/de;->a:Lcom/peel/util/dd;

    iget-object v1, v1, Lcom/peel/util/dd;->c:Lcom/peel/util/t;

    invoke-virtual {v1, p1, v0, v2}, Lcom/peel/util/t;->a(ZLjava/lang/Object;Ljava/lang/String;)V

    .line 2793
    :cond_1
    return-void

    .line 2784
    :catch_0
    move-exception v0

    move-object v1, v0

    move-object v0, v2

    .line 2785
    :goto_2
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 2784
    :catch_1
    move-exception v0

    move-object v4, v0

    move-object v0, v1

    move-object v1, v4

    goto :goto_2

    :cond_2
    move-object v1, v2

    goto :goto_0

    :cond_3
    move-object v0, v2

    goto :goto_1
.end method
