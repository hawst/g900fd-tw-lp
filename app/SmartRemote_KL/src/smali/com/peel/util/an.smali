.class Lcom/peel/util/an;
.super Lcom/peel/util/t;


# instance fields
.field final synthetic a:Lcom/peel/util/am;


# direct methods
.method constructor <init>(Lcom/peel/util/am;)V
    .locals 0

    .prologue
    .line 42
    iput-object p1, p0, Lcom/peel/util/an;->a:Lcom/peel/util/am;

    invoke-direct {p0}, Lcom/peel/util/t;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 45
    iget-boolean v0, p0, Lcom/peel/util/an;->i:Z

    if-nez v0, :cond_1

    .line 46
    invoke-static {}, Lcom/peel/util/al;->a()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "DVR capability check failed: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/peel/util/an;->a:Lcom/peel/util/am;

    iget-object v2, v2, Lcom/peel/util/am;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "\nmsg: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/peel/util/an;->k:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "\nresults: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/peel/util/an;->j:Ljava/lang/Object;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 47
    iget-object v0, p0, Lcom/peel/util/an;->a:Lcom/peel/util/am;

    iget-object v0, v0, Lcom/peel/util/am;->b:Lcom/peel/util/t;

    if-eqz v0, :cond_0

    .line 48
    iget-object v0, p0, Lcom/peel/util/an;->a:Lcom/peel/util/am;

    iget-object v0, v0, Lcom/peel/util/am;->b:Lcom/peel/util/t;

    iget-object v1, p0, Lcom/peel/util/an;->j:Ljava/lang/Object;

    iget-object v2, p0, Lcom/peel/util/an;->k:Ljava/lang/String;

    invoke-virtual {v0, v4, v1, v2}, Lcom/peel/util/t;->a(ZLjava/lang/Object;Ljava/lang/String;)V

    .line 76
    :cond_0
    :goto_0
    return-void

    .line 55
    :cond_1
    :try_start_0
    new-instance v1, Lorg/json/JSONObject;

    iget-object v0, p0, Lcom/peel/util/an;->j:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    invoke-direct {v1, v0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    const-string/jumbo v0, "Schedule"

    invoke-virtual {v1, v0}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    .line 56
    new-instance v1, Lcom/peel/util/ak;

    invoke-direct {v1}, Lcom/peel/util/ak;-><init>()V

    .line 57
    const-string/jumbo v2, "Yes"

    const-string/jumbo v3, "Single"

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    iput-boolean v2, v1, Lcom/peel/util/ak;->c:Z

    .line 58
    const-string/jumbo v2, "Yes"

    const-string/jumbo v3, "Multiple"

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, v1, Lcom/peel/util/ak;->a:Z

    .line 59
    const-string/jumbo v0, "Yes"

    const-string/jumbo v2, "EndOffset"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, v1, Lcom/peel/util/ak;->b:Z

    .line 60
    const-string/jumbo v0, "Yes"

    const-string/jumbo v2, "StartOffset"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, v1, Lcom/peel/util/ak;->d:Z

    .line 61
    const-string/jumbo v0, "Yes"

    const-string/jumbo v2, "Override"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, v1, Lcom/peel/util/ak;->e:Z

    .line 62
    const-string/jumbo v0, "Yes"

    const-string/jumbo v2, "Delete"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, v1, Lcom/peel/util/ak;->f:Z

    .line 64
    invoke-static {}, Lcom/peel/util/al;->b()Ljava/util/HashMap;

    move-result-object v0

    iget-object v2, p0, Lcom/peel/util/an;->a:Lcom/peel/util/am;

    iget-object v2, v2, Lcom/peel/util/am;->c:Ljava/lang/String;

    invoke-virtual {v0, v2, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 66
    iget-object v0, p0, Lcom/peel/util/an;->a:Lcom/peel/util/am;

    iget-object v0, v0, Lcom/peel/util/am;->b:Lcom/peel/util/t;

    if-eqz v0, :cond_0

    .line 67
    iget-object v0, p0, Lcom/peel/util/an;->a:Lcom/peel/util/am;

    iget-object v0, v0, Lcom/peel/util/am;->b:Lcom/peel/util/t;

    const/4 v2, 0x1

    const-string/jumbo v3, "found"

    invoke-virtual {v0, v2, v1, v3}, Lcom/peel/util/t;->a(ZLjava/lang/Object;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 69
    :catch_0
    move-exception v0

    .line 70
    invoke-static {}, Lcom/peel/util/al;->a()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "DVR capability parsing exception: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 71
    iget-object v1, p0, Lcom/peel/util/an;->a:Lcom/peel/util/am;

    iget-object v1, v1, Lcom/peel/util/am;->b:Lcom/peel/util/t;

    if-eqz v1, :cond_0

    .line 72
    iget-object v1, p0, Lcom/peel/util/an;->a:Lcom/peel/util/am;

    iget-object v1, v1, Lcom/peel/util/am;->b:Lcom/peel/util/t;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v4, v0, v2}, Lcom/peel/util/t;->a(ZLjava/lang/Object;Ljava/lang/String;)V

    goto/16 :goto_0
.end method
