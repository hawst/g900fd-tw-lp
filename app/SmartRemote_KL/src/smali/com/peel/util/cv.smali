.class final Lcom/peel/util/cv;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Landroid/widget/PopupWindow;

.field final synthetic b:I

.field final synthetic c:Ljava/lang/String;

.field final synthetic d:Ljava/lang/String;

.field final synthetic e:Landroid/content/Context;


# direct methods
.method constructor <init>(Landroid/widget/PopupWindow;ILjava/lang/String;Ljava/lang/String;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2461
    iput-object p1, p0, Lcom/peel/util/cv;->a:Landroid/widget/PopupWindow;

    iput p2, p0, Lcom/peel/util/cv;->b:I

    iput-object p3, p0, Lcom/peel/util/cv;->c:Ljava/lang/String;

    iput-object p4, p0, Lcom/peel/util/cv;->d:Ljava/lang/String;

    iput-object p5, p0, Lcom/peel/util/cv;->e:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    .line 2464
    iget-object v0, p0, Lcom/peel/util/cv;->a:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->dismiss()V

    .line 2465
    invoke-static {}, Lcom/peel/util/a/f;->a()Lcom/peel/util/a/f;

    move-result-object v1

    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v0}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const/16 v2, 0x4e4

    iget v3, p0, Lcom/peel/util/cv;->b:I

    invoke-virtual {v1, v0, v2, v3}, Lcom/peel/util/a/f;->a(III)V

    .line 2467
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 2468
    const-string/jumbo v0, "context_id"

    iget v2, p0, Lcom/peel/util/cv;->b:I

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 2469
    const-string/jumbo v0, "channel_number"

    iget-object v2, p0, Lcom/peel/util/cv;->c:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2470
    const-string/jumbo v0, "channel_name"

    iget-object v2, p0, Lcom/peel/util/cv;->d:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2471
    iget-object v0, p0, Lcom/peel/util/cv;->e:Landroid/content/Context;

    check-cast v0, Landroid/support/v4/app/ae;

    const-class v2, Lcom/peel/h/a/cp;

    .line 2472
    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    .line 2471
    invoke-static {v0, v2, v1}, Lcom/peel/d/e;->c(Landroid/support/v4/app/ae;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 2473
    return-void

    .line 2465
    :cond_0
    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v0}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/data/at;->f()I

    move-result v0

    goto :goto_0
.end method
