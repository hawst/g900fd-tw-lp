.class final Lcom/peel/util/ch;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Z

.field final synthetic b:Landroid/app/Activity;

.field final synthetic c:Ljava/lang/String;


# direct methods
.method constructor <init>(ZLandroid/app/Activity;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1870
    iput-boolean p1, p0, Lcom/peel/util/ch;->a:Z

    iput-object p2, p0, Lcom/peel/util/ch;->b:Landroid/app/Activity;

    iput-object p3, p0, Lcom/peel/util/ch;->c:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4
    .annotation build Landroid/annotation/TargetApi;
        value = 0x11
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 1874
    iget-boolean v0, p0, Lcom/peel/util/ch;->a:Z

    if-eqz v0, :cond_3

    .line 1875
    sget-object v0, Lcom/peel/util/bx;->j:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_2

    .line 1876
    sget-object v0, Lcom/peel/util/bx;->j:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/peel/util/ch;->b:Landroid/app/Activity;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/peel/util/ch;->b:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/peel/util/ch;->b:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->isDestroyed()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1899
    :cond_0
    :goto_0
    return-void

    .line 1880
    :cond_1
    :try_start_0
    sget-object v0, Lcom/peel/util/bx;->j:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1881
    :catch_0
    move-exception v0

    .line 1882
    iget-object v1, p0, Lcom/peel/util/ch;->c:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "error : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1886
    :cond_2
    new-instance v0, Landroid/app/ProgressDialog;

    iget-object v1, p0, Lcom/peel/util/ch;->b:Landroid/app/Activity;

    sget v2, Lcom/peel/ui/fu;->DialogTheme:I

    invoke-direct {v0, v1, v2}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;I)V

    sput-object v0, Lcom/peel/util/bx;->j:Landroid/app/ProgressDialog;

    .line 1887
    sget-object v0, Lcom/peel/util/bx;->j:Landroid/app/ProgressDialog;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    .line 1888
    sget-object v0, Lcom/peel/util/bx;->j:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v3}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 1889
    sget-object v0, Lcom/peel/util/bx;->j:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v3}, Landroid/app/ProgressDialog;->setCanceledOnTouchOutside(Z)V

    .line 1890
    sget-object v0, Lcom/peel/util/bx;->j:Landroid/app/ProgressDialog;

    iget-object v1, p0, Lcom/peel/util/ch;->b:Landroid/app/Activity;

    sget v2, Lcom/peel/ui/ft;->please_wait:I

    invoke-virtual {v1, v2}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 1891
    sget-object v0, Lcom/peel/util/bx;->j:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    goto :goto_0

    .line 1894
    :cond_3
    sget-object v0, Lcom/peel/util/bx;->j:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/peel/util/bx;->j:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1895
    sget-object v0, Lcom/peel/util/bx;->j:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    goto :goto_0
.end method
