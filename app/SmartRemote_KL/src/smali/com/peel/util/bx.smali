.class public Lcom/peel/util/bx;
.super Ljava/lang/Object;


# static fields
.field public static final a:Ljava/lang/String;

.field public static final b:Ljava/util/regex/Pattern;

.field public static final c:Ljava/util/regex/Pattern;

.field public static final d:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field public static e:Lcom/peel/widget/ag;

.field public static f:Lcom/peel/widget/ag;

.field public static g:Lcom/peel/widget/ag;

.field public static h:Z

.field public static i:I

.field public static j:Landroid/app/ProgressDialog;

.field public static k:I

.field public static final l:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "[I>;"
        }
    .end annotation
.end field

.field public static m:Ljava/util/LinkedHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static n:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static o:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final p:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static q:Z

.field private static r:Z

.field private static final s:Lorg/codehaus/jackson/map/ObjectMapper;

.field private static t:Ljava/lang/String;

.field private static u:Ljava/lang/String;

.field private static v:Ljava/lang/String;

.field private static w:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x0

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 125
    const-class v0, Lcom/peel/util/bx;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/peel/util/bx;->a:Ljava/lang/String;

    .line 126
    const-string/jumbo v0, "[a-zA-Z0-9\\+\\.\\_\\%\\-\\+]{1,256}\\@[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}(\\.[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25})+"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/peel/util/bx;->b:Ljava/util/regex/Pattern;

    .line 128
    const-string/jumbo v0, "[a-zA-Z0-9\\-\\s]+"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/peel/util/bx;->c:Ljava/util/regex/Pattern;

    .line 138
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/peel/util/bx;->d:Ljava/util/Map;

    .line 143
    sput-object v8, Lcom/peel/util/bx;->f:Lcom/peel/widget/ag;

    .line 144
    sput-object v8, Lcom/peel/util/bx;->g:Lcom/peel/widget/ag;

    .line 145
    sput-boolean v4, Lcom/peel/util/bx;->h:Z

    .line 148
    sput v5, Lcom/peel/util/bx;->i:I

    .line 150
    sput-object v8, Lcom/peel/util/bx;->j:Landroid/app/ProgressDialog;

    .line 152
    sput v4, Lcom/peel/util/bx;->k:I

    .line 153
    sput-boolean v4, Lcom/peel/util/bx;->q:Z

    sput-boolean v4, Lcom/peel/util/bx;->r:Z

    .line 159
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/peel/util/bx;->l:Ljava/util/HashMap;

    .line 162
    sget-object v0, Lcom/peel/util/bx;->l:Ljava/util/HashMap;

    const-string/jumbo v1, "missing"

    new-array v2, v7, [I

    sget v3, Lcom/peel/ui/fo;->sport_other_movie:I

    aput v3, v2, v4

    sget v3, Lcom/peel/ui/fo;->sidebar_sport_other_stateful:I

    aput v3, v2, v5

    sget v3, Lcom/peel/ui/fo;->sport_other_movie_big:I

    aput v3, v2, v6

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 163
    sget-object v0, Lcom/peel/util/bx;->l:Ljava/util/HashMap;

    const-string/jumbo v1, "Football"

    new-array v2, v7, [I

    sget v3, Lcom/peel/ui/fo;->sport_football_movie:I

    aput v3, v2, v4

    sget v3, Lcom/peel/ui/fo;->sidebar_sport_football_stateful:I

    aput v3, v2, v5

    sget v3, Lcom/peel/ui/fo;->sport_football_movie_big:I

    aput v3, v2, v6

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 164
    sget-object v0, Lcom/peel/util/bx;->l:Ljava/util/HashMap;

    const-string/jumbo v1, "Baseball"

    new-array v2, v7, [I

    sget v3, Lcom/peel/ui/fo;->sport_baseball_movie:I

    aput v3, v2, v4

    sget v3, Lcom/peel/ui/fo;->sidebar_sport_baseball_stateful:I

    aput v3, v2, v5

    sget v3, Lcom/peel/ui/fo;->sport_baseball_movie_big:I

    aput v3, v2, v6

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 165
    sget-object v0, Lcom/peel/util/bx;->l:Ljava/util/HashMap;

    const-string/jumbo v1, "Basketball"

    new-array v2, v7, [I

    sget v3, Lcom/peel/ui/fo;->sport_basketball_movie:I

    aput v3, v2, v4

    sget v3, Lcom/peel/ui/fo;->sidebar_sport_basketball_stateful:I

    aput v3, v2, v5

    sget v3, Lcom/peel/ui/fo;->sport_basketball_movie_big:I

    aput v3, v2, v6

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 166
    sget-object v0, Lcom/peel/util/bx;->l:Ljava/util/HashMap;

    const-string/jumbo v1, "Hockey"

    new-array v2, v7, [I

    sget v3, Lcom/peel/ui/fo;->sport_hockey_movie:I

    aput v3, v2, v4

    sget v3, Lcom/peel/ui/fo;->sidebar_sport_hockey_stateful:I

    aput v3, v2, v5

    sget v3, Lcom/peel/ui/fo;->sport_hockey_movie_big:I

    aput v3, v2, v6

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 167
    sget-object v0, Lcom/peel/util/bx;->l:Ljava/util/HashMap;

    const-string/jumbo v1, "Soccer"

    new-array v2, v7, [I

    sget v3, Lcom/peel/ui/fo;->sport_soccer_movie:I

    aput v3, v2, v4

    sget v3, Lcom/peel/ui/fo;->sidebar_sport_soccer_stateful:I

    aput v3, v2, v5

    sget v3, Lcom/peel/ui/fo;->sport_soccer_movie_big:I

    aput v3, v2, v6

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 168
    sget-object v0, Lcom/peel/util/bx;->l:Ljava/util/HashMap;

    const-string/jumbo v1, "News & Talk"

    new-array v2, v7, [I

    sget v3, Lcom/peel/ui/fo;->sport_newstalk_movie:I

    aput v3, v2, v4

    sget v3, Lcom/peel/ui/fo;->sidebar_sport_newstalk_stateful:I

    aput v3, v2, v5

    sget v3, Lcom/peel/ui/fo;->sport_newstalk_movie_big:I

    aput v3, v2, v6

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 169
    sget-object v0, Lcom/peel/util/bx;->l:Ljava/util/HashMap;

    const-string/jumbo v1, "Other"

    new-array v2, v7, [I

    sget v3, Lcom/peel/ui/fo;->sport_other_movie:I

    aput v3, v2, v4

    sget v3, Lcom/peel/ui/fo;->sidebar_sport_other_stateful:I

    aput v3, v2, v5

    sget v3, Lcom/peel/ui/fo;->sport_other_movie_big:I

    aput v3, v2, v6

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 172
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    sput-object v0, Lcom/peel/util/bx;->m:Ljava/util/LinkedHashMap;

    .line 175
    sget-object v0, Lcom/peel/util/bx;->m:Ljava/util/LinkedHashMap;

    const-string/jumbo v1, "en"

    const-string/jumbo v2, "English"

    invoke-virtual {v0, v1, v2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 176
    sget-object v0, Lcom/peel/util/bx;->m:Ljava/util/LinkedHashMap;

    const-string/jumbo v1, "es"

    const-string/jumbo v2, "Espanol"

    invoke-virtual {v0, v1, v2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 177
    sget-object v0, Lcom/peel/util/bx;->m:Ljava/util/LinkedHashMap;

    const-string/jumbo v1, "fr"

    const-string/jumbo v2, "French"

    invoke-virtual {v0, v1, v2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 178
    sget-object v0, Lcom/peel/util/bx;->m:Ljava/util/LinkedHashMap;

    const-string/jumbo v1, "fr-CA"

    const-string/jumbo v2, "French(Canada)"

    invoke-virtual {v0, v1, v2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 179
    sget-object v0, Lcom/peel/util/bx;->m:Ljava/util/LinkedHashMap;

    const-string/jumbo v1, "hi"

    const-string/jumbo v2, "Hindi"

    invoke-virtual {v0, v1, v2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 180
    sget-object v0, Lcom/peel/util/bx;->m:Ljava/util/LinkedHashMap;

    const-string/jumbo v1, "it"

    const-string/jumbo v2, "Italian"

    invoke-virtual {v0, v1, v2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 181
    sget-object v0, Lcom/peel/util/bx;->m:Ljava/util/LinkedHashMap;

    const-string/jumbo v1, "ru"

    const-string/jumbo v2, "Russian"

    invoke-virtual {v0, v1, v2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 182
    sget-object v0, Lcom/peel/util/bx;->m:Ljava/util/LinkedHashMap;

    const-string/jumbo v1, "pl"

    const-string/jumbo v2, "Polish"

    invoke-virtual {v0, v1, v2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 183
    sget-object v0, Lcom/peel/util/bx;->m:Ljava/util/LinkedHashMap;

    const-string/jumbo v1, "pt"

    const-string/jumbo v2, "Portuguese"

    invoke-virtual {v0, v1, v2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 184
    sget-object v0, Lcom/peel/util/bx;->m:Ljava/util/LinkedHashMap;

    const-string/jumbo v1, "de"

    const-string/jumbo v2, "German"

    invoke-virtual {v0, v1, v2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 185
    sget-object v0, Lcom/peel/util/bx;->m:Ljava/util/LinkedHashMap;

    const-string/jumbo v1, "en-GB"

    const-string/jumbo v2, "English(UK)"

    invoke-virtual {v0, v1, v2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 186
    sget-object v0, Lcom/peel/util/bx;->m:Ljava/util/LinkedHashMap;

    const-string/jumbo v1, "ja"

    const-string/jumbo v2, "Japanese"

    invoke-virtual {v0, v1, v2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 187
    sget-object v0, Lcom/peel/util/bx;->m:Ljava/util/LinkedHashMap;

    const-string/jumbo v1, "el"

    const-string/jumbo v2, "Greek"

    invoke-virtual {v0, v1, v2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 188
    sget-object v0, Lcom/peel/util/bx;->m:Ljava/util/LinkedHashMap;

    const-string/jumbo v1, "fa"

    const-string/jumbo v2, "Farsi"

    invoke-virtual {v0, v1, v2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 189
    sget-object v0, Lcom/peel/util/bx;->m:Ljava/util/LinkedHashMap;

    const-string/jumbo v1, "ps"

    const-string/jumbo v2, "Pashto"

    invoke-virtual {v0, v1, v2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 190
    sget-object v0, Lcom/peel/util/bx;->m:Ljava/util/LinkedHashMap;

    const-string/jumbo v1, "vi"

    const-string/jumbo v2, "Vietnamese"

    invoke-virtual {v0, v1, v2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 191
    sget-object v0, Lcom/peel/util/bx;->m:Ljava/util/LinkedHashMap;

    const-string/jumbo v1, "tl"

    const-string/jumbo v2, "Tagalog"

    invoke-virtual {v0, v1, v2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 192
    sget-object v0, Lcom/peel/util/bx;->m:Ljava/util/LinkedHashMap;

    const-string/jumbo v1, "ko"

    const-string/jumbo v2, "Korean"

    invoke-virtual {v0, v1, v2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 193
    sget-object v0, Lcom/peel/util/bx;->m:Ljava/util/LinkedHashMap;

    const-string/jumbo v1, "pa"

    const-string/jumbo v2, "Punjabi"

    invoke-virtual {v0, v1, v2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 194
    sget-object v0, Lcom/peel/util/bx;->m:Ljava/util/LinkedHashMap;

    const-string/jumbo v1, "ar"

    const-string/jumbo v2, "Arabic"

    invoke-virtual {v0, v1, v2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 195
    sget-object v0, Lcom/peel/util/bx;->m:Ljava/util/LinkedHashMap;

    const-string/jumbo v1, "und"

    const-string/jumbo v2, "Other Languages"

    invoke-virtual {v0, v1, v2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 201
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/peel/util/bx;->n:Ljava/util/HashMap;

    .line 204
    sget-object v0, Lcom/peel/util/bx;->n:Ljava/util/HashMap;

    const-string/jumbo v1, "1"

    const-string/jumbo v2, "BS_ONE"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 205
    sget-object v0, Lcom/peel/util/bx;->n:Ljava/util/HashMap;

    const-string/jumbo v1, "2"

    const-string/jumbo v2, "BS_TWO"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 206
    sget-object v0, Lcom/peel/util/bx;->n:Ljava/util/HashMap;

    const-string/jumbo v1, "3"

    const-string/jumbo v2, "BS_THREE"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 207
    sget-object v0, Lcom/peel/util/bx;->n:Ljava/util/HashMap;

    const-string/jumbo v1, "4"

    const-string/jumbo v2, "BS_FOUR"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 208
    sget-object v0, Lcom/peel/util/bx;->n:Ljava/util/HashMap;

    const-string/jumbo v1, "5"

    const-string/jumbo v2, "BS_FIVE"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 209
    sget-object v0, Lcom/peel/util/bx;->n:Ljava/util/HashMap;

    const-string/jumbo v1, "6"

    const-string/jumbo v2, "BS_SIX"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 210
    sget-object v0, Lcom/peel/util/bx;->n:Ljava/util/HashMap;

    const-string/jumbo v1, "7"

    const-string/jumbo v2, "BS_SEVEN"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 211
    sget-object v0, Lcom/peel/util/bx;->n:Ljava/util/HashMap;

    const-string/jumbo v1, "8"

    const-string/jumbo v2, "BS_EIGHT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 212
    sget-object v0, Lcom/peel/util/bx;->n:Ljava/util/HashMap;

    const-string/jumbo v1, "9"

    const-string/jumbo v2, "BS_NINE"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 213
    sget-object v0, Lcom/peel/util/bx;->n:Ljava/util/HashMap;

    const-string/jumbo v1, "LLD_TEN"

    const-string/jumbo v2, "BS_TEN"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 214
    sget-object v0, Lcom/peel/util/bx;->n:Ljava/util/HashMap;

    const-string/jumbo v1, "LLD_ELEVEN"

    const-string/jumbo v2, "BS_ELEVEN"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 215
    sget-object v0, Lcom/peel/util/bx;->n:Ljava/util/HashMap;

    const-string/jumbo v1, "LLD_TWELVE"

    const-string/jumbo v2, "BS_TWELVE"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 218
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/peel/util/bx;->o:Ljava/util/HashMap;

    .line 221
    sget-object v0, Lcom/peel/util/bx;->o:Ljava/util/HashMap;

    const-string/jumbo v1, "1"

    const-string/jumbo v2, "CS_ONE"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 222
    sget-object v0, Lcom/peel/util/bx;->o:Ljava/util/HashMap;

    const-string/jumbo v1, "2"

    const-string/jumbo v2, "CS_TWO"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 223
    sget-object v0, Lcom/peel/util/bx;->o:Ljava/util/HashMap;

    const-string/jumbo v1, "3"

    const-string/jumbo v2, "CS_THREE"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 224
    sget-object v0, Lcom/peel/util/bx;->o:Ljava/util/HashMap;

    const-string/jumbo v1, "4"

    const-string/jumbo v2, "CS_FOUR"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 225
    sget-object v0, Lcom/peel/util/bx;->o:Ljava/util/HashMap;

    const-string/jumbo v1, "5"

    const-string/jumbo v2, "CS_FIVE"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 226
    sget-object v0, Lcom/peel/util/bx;->o:Ljava/util/HashMap;

    const-string/jumbo v1, "6"

    const-string/jumbo v2, "CS_SIX"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 227
    sget-object v0, Lcom/peel/util/bx;->o:Ljava/util/HashMap;

    const-string/jumbo v1, "7"

    const-string/jumbo v2, "CS_SEVEN"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 228
    sget-object v0, Lcom/peel/util/bx;->o:Ljava/util/HashMap;

    const-string/jumbo v1, "8"

    const-string/jumbo v2, "CS_EIGHT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 229
    sget-object v0, Lcom/peel/util/bx;->o:Ljava/util/HashMap;

    const-string/jumbo v1, "9"

    const-string/jumbo v2, "CS_NINE"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 230
    sget-object v0, Lcom/peel/util/bx;->o:Ljava/util/HashMap;

    const-string/jumbo v1, "LLD_TEN"

    const-string/jumbo v2, "CS_TEN"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 231
    sget-object v0, Lcom/peel/util/bx;->o:Ljava/util/HashMap;

    const-string/jumbo v1, "LLD_ELEVEN"

    const-string/jumbo v2, "CS_ELEVEN"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 232
    sget-object v0, Lcom/peel/util/bx;->o:Ljava/util/HashMap;

    const-string/jumbo v1, "LLD_TWELVE"

    const-string/jumbo v2, "CS_TWELVE"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1913
    new-instance v0, Lorg/codehaus/jackson/map/ObjectMapper;

    invoke-direct {v0}, Lorg/codehaus/jackson/map/ObjectMapper;-><init>()V

    sput-object v0, Lcom/peel/util/bx;->s:Lorg/codehaus/jackson/map/ObjectMapper;

    .line 1982
    sput-object v8, Lcom/peel/util/bx;->t:Ljava/lang/String;

    .line 2021
    sput-object v8, Lcom/peel/util/bx;->u:Ljava/lang/String;

    .line 2094
    sput-object v8, Lcom/peel/util/bx;->v:Ljava/lang/String;

    .line 2430
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/peel/util/bx;->p:Ljava/util/Map;

    .line 2433
    sget-object v0, Lcom/peel/util/bx;->p:Ljava/util/Map;

    const-string/jumbo v1, "Sunday"

    sget v2, Lcom/peel/ui/ft;->sunday:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2434
    sget-object v0, Lcom/peel/util/bx;->p:Ljava/util/Map;

    const-string/jumbo v1, "Monday"

    sget v2, Lcom/peel/ui/ft;->monday:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2435
    sget-object v0, Lcom/peel/util/bx;->p:Ljava/util/Map;

    const-string/jumbo v1, "Tuesday"

    sget v2, Lcom/peel/ui/ft;->tuesday:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2436
    sget-object v0, Lcom/peel/util/bx;->p:Ljava/util/Map;

    const-string/jumbo v1, "Wednesday"

    sget v2, Lcom/peel/ui/ft;->wednesday:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2437
    sget-object v0, Lcom/peel/util/bx;->p:Ljava/util/Map;

    const-string/jumbo v1, "Thursday"

    sget v2, Lcom/peel/ui/ft;->thursday:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2438
    sget-object v0, Lcom/peel/util/bx;->p:Ljava/util/Map;

    const-string/jumbo v1, "Friday"

    sget v2, Lcom/peel/ui/ft;->friday:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2439
    sget-object v0, Lcom/peel/util/bx;->p:Ljava/util/Map;

    const-string/jumbo v1, "Saturday"

    sget v2, Lcom/peel/ui/ft;->saturday:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2531
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/peel/util/bx;->w:Ljava/util/HashMap;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 121
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/res/Resources;F)F
    .locals 2

    .prologue
    .line 756
    invoke-virtual {p0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    .line 757
    iget v0, v0, Landroid/util/DisplayMetrics;->densityDpi:I

    int-to-float v0, v0

    const/high16 v1, 0x43200000    # 160.0f

    div-float/2addr v0, v1

    mul-float/2addr v0, p1

    .line 758
    return v0
.end method

.method public static a(Lcom/peel/content/listing/Listing;)I
    .locals 3

    .prologue
    .line 267
    invoke-virtual {p0}, Lcom/peel/content/listing/Listing;->m()[Ljava/lang/String;

    move-result-object v0

    .line 268
    if-eqz v0, :cond_0

    array-length v1, v0

    const/4 v2, 0x1

    if-ge v1, v2, :cond_1

    :cond_0
    const-string/jumbo v0, "missing"

    .line 270
    :goto_0
    const-string/jumbo v1, "sports"

    invoke-virtual {p0}, Lcom/peel/content/listing/Listing;->k()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 271
    sget-object v1, Lcom/peel/util/bx;->l:Ljava/util/HashMap;

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [I

    const/4 v1, 0x2

    aget v0, v0, v1

    .line 273
    :goto_1
    return v0

    .line 268
    :cond_1
    const/4 v1, 0x0

    aget-object v0, v0, v1

    goto :goto_0

    .line 273
    :cond_2
    sget v0, Lcom/peel/ui/fo;->genre_placeholder_big:I

    goto :goto_1
.end method

.method public static a(Lcom/peel/content/listing/Listing;I)I
    .locals 3

    .prologue
    .line 295
    invoke-virtual {p0}, Lcom/peel/content/listing/Listing;->m()[Ljava/lang/String;

    move-result-object v0

    .line 296
    if-eqz v0, :cond_0

    array-length v1, v0

    const/4 v2, 0x1

    if-ge v1, v2, :cond_1

    :cond_0
    const-string/jumbo v0, "missing"

    .line 298
    :goto_0
    const-string/jumbo v1, "sports"

    invoke-virtual {p0}, Lcom/peel/content/listing/Listing;->k()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 299
    const-string/jumbo v1, "sports"

    invoke-static {v1, v0, p1}, Lcom/peel/util/bx;->a(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v0

    .line 301
    :goto_1
    return v0

    .line 296
    :cond_1
    const/4 v1, 0x0

    aget-object v0, v0, v1

    goto :goto_0

    .line 301
    :cond_2
    sget v0, Lcom/peel/ui/fo;->genre_placeholder:I

    goto :goto_1
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;I)I
    .locals 3

    .prologue
    const/4 v1, -0x1

    .line 239
    .line 242
    const-string/jumbo v0, "sports"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 243
    sget-object v0, Lcom/peel/util/bx;->l:Ljava/util/HashMap;

    .line 250
    :goto_0
    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [I

    .line 251
    if-eqz v0, :cond_4

    .line 252
    const/4 v2, 0x0

    aget v0, v0, v2

    .line 255
    :goto_1
    if-ne v1, v0, :cond_0

    .line 256
    const-string/jumbo v0, "sports"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 257
    sget v0, Lcom/peel/ui/fo;->sport_other_movie:I

    .line 263
    :cond_0
    :goto_2
    return v0

    .line 247
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 259
    :cond_2
    const/16 v0, 0xa9

    if-ne p2, v0, :cond_3

    sget v0, Lcom/peel/ui/fo;->missing_movie:I

    goto :goto_2

    :cond_3
    sget v0, Lcom/peel/ui/fo;->missing_tv:I

    goto :goto_2

    :cond_4
    move v0, v1

    goto :goto_1
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/res/Resources;)I
    .locals 1

    .prologue
    .line 729
    invoke-virtual {p3, p0, p1, p2}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public static a(Ljava/lang/String;[Lcom/peel/data/Channel;)I
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 2901
    if-eqz p1, :cond_0

    array-length v1, p1

    if-nez v1, :cond_2

    .line 2909
    :cond_0
    :goto_0
    return v0

    .line 2905
    :cond_1
    add-int/lit8 v0, v0, 0x1

    :cond_2
    array-length v1, p1

    if-ge v0, v1, :cond_3

    .line 2906
    aget-object v1, p1, v0

    invoke-virtual {v1}, Lcom/peel/data/Channel;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    goto :goto_0

    .line 2909
    :cond_3
    array-length v0, p1

    goto :goto_0
.end method

.method public static a([Ljava/lang/String;Ljava/lang/String;)I
    .locals 2

    .prologue
    .line 277
    if-eqz p0, :cond_0

    array-length v0, p0

    const/4 v1, 0x1

    if-ge v0, v1, :cond_1

    :cond_0
    const-string/jumbo v0, "missing"

    .line 279
    :goto_0
    const-string/jumbo v1, "sports"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 280
    sget-object v1, Lcom/peel/util/bx;->l:Ljava/util/HashMap;

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [I

    const/4 v1, 0x2

    aget v0, v0, v1

    .line 282
    :goto_1
    return v0

    .line 277
    :cond_1
    const/4 v0, 0x0

    aget-object v0, p0, v0

    goto :goto_0

    .line 282
    :cond_2
    sget v0, Lcom/peel/ui/fo;->genre_placeholder_big:I

    goto :goto_1
.end method

.method public static a([Ljava/lang/String;Ljava/lang/String;I)I
    .locals 2

    .prologue
    .line 286
    if-eqz p0, :cond_0

    array-length v0, p0

    const/4 v1, 0x1

    if-ge v0, v1, :cond_1

    :cond_0
    const-string/jumbo v0, "missing"

    .line 288
    :goto_0
    const-string/jumbo v1, "sports"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 289
    const-string/jumbo v1, "sports"

    invoke-static {v1, v0, p2}, Lcom/peel/util/bx;->a(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v0

    .line 291
    :goto_1
    return v0

    .line 286
    :cond_1
    const/4 v0, 0x0

    aget-object v0, p0, v0

    goto :goto_0

    .line 291
    :cond_2
    sget v0, Lcom/peel/ui/fo;->genre_placeholder:I

    goto :goto_1
.end method

.method public static a(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 598
    packed-switch p0, :pswitch_data_0

    .line 620
    :pswitch_0
    const-string/jumbo v0, "Device"

    :goto_0
    return-object v0

    .line 600
    :pswitch_1
    const-string/jumbo v0, "TV"

    goto :goto_0

    .line 602
    :pswitch_2
    const-string/jumbo v0, "Set-top box"

    goto :goto_0

    .line 604
    :pswitch_3
    const-string/jumbo v0, "DVD Player"

    goto :goto_0

    .line 606
    :pswitch_4
    const-string/jumbo v0, "Blu-ray Player"

    goto :goto_0

    .line 610
    :pswitch_5
    const-string/jumbo v0, "AV Receiver/Stereo"

    goto :goto_0

    .line 612
    :pswitch_6
    const-string/jumbo v0, "Projector"

    goto :goto_0

    .line 614
    :pswitch_7
    const-string/jumbo v0, "DVR"

    goto :goto_0

    .line 616
    :pswitch_8
    const-string/jumbo v0, "Streaming Media Player"

    goto :goto_0

    .line 618
    :pswitch_9
    const-string/jumbo v0, "Air Conditioner"

    goto :goto_0

    .line 598
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_8
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_6
        :pswitch_0
        :pswitch_0
        :pswitch_5
        :pswitch_5
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_9
        :pswitch_0
        :pswitch_7
    .end packed-switch
.end method

.method public static a(IIILjava/util/Map;)Ljava/lang/String;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(III",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    const/4 v6, 0x1

    const/4 v0, 0x0

    .line 2318
    if-eqz p3, :cond_4

    if-eqz p0, :cond_4

    .line 2319
    :try_start_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p3, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2320
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p3, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2370
    :goto_0
    return-object v0

    .line 2326
    :cond_0
    const/16 v3, 0x3e7

    .line 2330
    invoke-interface {p3}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v7

    move v2, v0

    move-object v1, v4

    :cond_1
    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2332
    const-string/jumbo v5, "object_type"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 2333
    const-string/jumbo v5, ":"

    invoke-virtual {v0, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 2335
    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v5

    const-string/jumbo v8, "x"

    invoke-virtual {v5, v8}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    .line 2336
    const/4 v8, 0x0

    aget-object v8, v5, v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v8

    .line 2337
    const/4 v9, 0x1

    aget-object v5, v5, v9

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    .line 2339
    mul-int/2addr v5, p0

    div-int/2addr v5, p1

    if-ne v5, v8, :cond_2

    .line 2343
    sub-int v2, v8, p2

    invoke-static {v2}, Ljava/lang/Math;->abs(I)I

    move-result v2

    .line 2344
    if-ge v2, v3, :cond_5

    .line 2346
    invoke-interface {p3, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    :goto_2
    move v5, v6

    move v3, v2

    .line 2354
    :goto_3
    sub-int v2, v8, p2

    invoke-static {v2}, Ljava/lang/Math;->abs(I)I

    move-result v2

    .line 2355
    if-ge v2, v3, :cond_6

    .line 2357
    invoke-interface {p3, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    move v1, v2

    :goto_4
    move v2, v5

    move v3, v1

    move-object v1, v0

    .line 2359
    goto :goto_1

    .line 2349
    :cond_2
    if-nez v2, :cond_1

    move v5, v2

    goto :goto_3

    .line 2361
    :cond_3
    if-eqz v1, :cond_4

    .line 2362
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v2, ":"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v2, ":"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p3, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-object v0, v1

    .line 2363
    goto/16 :goto_0

    .line 2366
    :catch_0
    move-exception v0

    .line 2367
    sget-object v1, Lcom/peel/util/bx;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_4
    move-object v0, v4

    .line 2370
    goto/16 :goto_0

    :cond_5
    move v2, v3

    goto :goto_2

    :cond_6
    move-object v0, v1

    move v1, v3

    goto :goto_4
.end method

.method public static a(Landroid/content/Context;I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 625
    packed-switch p1, :pswitch_data_0

    .line 647
    :pswitch_0
    sget v0, Lcom/peel/ui/ft;->error:I

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    .line 627
    :pswitch_1
    sget v0, Lcom/peel/ui/ft;->DeviceType1:I

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 629
    :pswitch_2
    sget v0, Lcom/peel/ui/ft;->DeviceType2:I

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 631
    :pswitch_3
    sget v0, Lcom/peel/ui/ft;->DeviceType3:I

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 633
    :pswitch_4
    sget v0, Lcom/peel/ui/ft;->DeviceType4:I

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 637
    :pswitch_5
    sget v0, Lcom/peel/ui/ft;->DeviceType5:I

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 639
    :pswitch_6
    sget v0, Lcom/peel/ui/ft;->DeviceType10:I

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 641
    :pswitch_7
    sget v0, Lcom/peel/ui/ft;->DeviceType20:I

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 643
    :pswitch_8
    sget v0, Lcom/peel/ui/ft;->DeviceType6:I

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 645
    :pswitch_9
    sget v0, Lcom/peel/ui/ft;->DeviceType18:I

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 625
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_8
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_6
        :pswitch_0
        :pswitch_0
        :pswitch_5
        :pswitch_5
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_9
        :pswitch_0
        :pswitch_7
    .end packed-switch
.end method

.method public static a(Landroid/content/Context;JJJ)Ljava/lang/String;
    .locals 9

    .prologue
    .line 2884
    sub-long v0, p5, p1

    sub-long v0, p3, v0

    long-to-double v0, v0

    .line 2885
    const-wide/16 v2, 0x0

    cmpg-double v2, v0, v2

    if-gtz v2, :cond_0

    .line 2886
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "0 "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget v1, Lcom/peel/ui/ft;->time_unit_min:I

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2896
    :goto_0
    return-object v0

    .line 2887
    :cond_0
    const-wide v2, 0x414b774000000000L    # 3600000.0

    cmpg-double v2, v0, v2

    if-gez v2, :cond_1

    .line 2888
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "%d "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget v3, Lcom/peel/ui/ft;->time_unit_min:I

    invoke-virtual {p0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    const-wide v6, 0x40ed4c0000000000L    # 60000.0

    div-double/2addr v0, v6

    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v0

    double-to-int v0, v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2889
    :cond_1
    const-wide v2, 0x414b774000000000L    # 3600000.0

    cmpl-double v2, v0, v2

    if-nez v2, :cond_2

    .line 2890
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "%d "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget v3, Lcom/peel/ui/ft;->time_unit_hour:I

    invoke-virtual {p0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    double-to-int v0, v0

    const v1, 0x36ee80

    div-int/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2892
    :cond_2
    double-to-int v2, v0

    const v3, 0x36ee80

    div-int/2addr v2, v3

    .line 2893
    const-wide v4, 0x414b774000000000L    # 3600000.0

    rem-double v4, v0, v4

    const-wide/16 v6, 0x0

    cmpl-double v3, v4, v6

    if-nez v3, :cond_3

    .line 2894
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "%d "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget v1, Lcom/peel/ui/ft;->time_unit_hour:I

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 2896
    :cond_3
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "%d "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget v4, Lcom/peel/ui/ft;->time_unit_hour:I

    invoke-virtual {p0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, " %d "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget v4, Lcom/peel/ui/ft;->time_unit_min:I

    invoke-virtual {p0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v4, v5

    const/4 v2, 0x1

    const-wide v6, 0x414b774000000000L    # 3600000.0

    rem-double/2addr v0, v6

    const-wide v6, 0x40ed4c0000000000L    # 60000.0

    div-double/2addr v0, v6

    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v0

    double-to-int v0, v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v4, v2

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0
.end method

.method public static a(Landroid/content/Context;Lcom/peel/control/a;Lcom/peel/control/h;Ljava/lang/String;)Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 1650
    if-nez p1, :cond_1

    .line 1651
    sget-object v0, Lcom/peel/util/bx;->a:Ljava/lang/String;

    const-string/jumbo v1, "Cannot convert to JP command if activity is null"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1682
    :cond_0
    :goto_0
    return-object p3

    .line 1656
    :cond_1
    if-eqz p2, :cond_8

    invoke-virtual {p2}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v0

    const-string/jumbo v1, "BS1"

    invoke-virtual {v0, v1}, Lcom/peel/data/g;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    move v4, v2

    .line 1661
    :goto_1
    if-eqz p2, :cond_7

    invoke-virtual {p2}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v0

    const-string/jumbo v1, "CS1"

    invoke-virtual {v0, v1}, Lcom/peel/data/g;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    move v1, v2

    .line 1665
    :goto_2
    const/4 v0, 0x0

    .line 1666
    if-eqz v4, :cond_3

    invoke-static {p0, p1, v3}, Lcom/peel/util/bx;->b(Landroid/content/Context;Lcom/peel/control/a;I)I

    move-result v4

    if-ne v2, v4, :cond_3

    .line 1667
    sget-object v0, Lcom/peel/util/bx;->n:Ljava/util/HashMap;

    invoke-virtual {v0, p3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1672
    :cond_2
    :goto_3
    if-eqz v0, :cond_4

    move-object p3, v0

    .line 1673
    goto :goto_0

    .line 1668
    :cond_3
    if-eqz v1, :cond_2

    invoke-static {p0, p1, v3}, Lcom/peel/util/bx;->b(Landroid/content/Context;Lcom/peel/control/a;I)I

    move-result v1

    if-ne v5, v1, :cond_2

    .line 1669
    sget-object v0, Lcom/peel/util/bx;->o:Ljava/util/HashMap;

    invoke-virtual {v0, p3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    goto :goto_3

    .line 1675
    :cond_4
    const-string/jumbo v0, "LLD"

    invoke-virtual {p3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1676
    invoke-static {p0, p1, v3}, Lcom/peel/util/bx;->a(Landroid/content/Context;Lcom/peel/control/a;I)V

    goto :goto_0

    .line 1677
    :cond_5
    const-string/jumbo v0, "BS"

    invoke-virtual {p3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1678
    invoke-static {p0, p1, v2}, Lcom/peel/util/bx;->a(Landroid/content/Context;Lcom/peel/control/a;I)V

    goto :goto_0

    .line 1679
    :cond_6
    const-string/jumbo v0, "CS"

    invoke-virtual {p3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1680
    invoke-static {p0, p1, v5}, Lcom/peel/util/bx;->a(Landroid/content/Context;Lcom/peel/control/a;I)V

    goto :goto_0

    :cond_7
    move v1, v3

    goto :goto_2

    :cond_8
    move v4, v3

    goto :goto_1
.end method

.method public static a(Landroid/content/Context;Lcom/peel/control/a;Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 1639
    if-nez p1, :cond_0

    .line 1640
    sget-object v0, Lcom/peel/util/bx;->a:Ljava/lang/String;

    const-string/jumbo v1, "Cannot convert to JP command if activity is null"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1645
    :goto_0
    return-object p2

    .line 1644
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/peel/control/a;->a(I)Lcom/peel/control/h;

    move-result-object v0

    .line 1645
    invoke-static {p0, p1, v0, p2}, Lcom/peel/util/bx;->a(Landroid/content/Context;Lcom/peel/control/a;Lcom/peel/control/h;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    const/4 v2, 0x2

    .line 488
    if-eqz p0, :cond_1

    .line 490
    const/4 v0, 0x7

    :try_start_0
    invoke-virtual {p0, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 491
    array-length v1, v0

    if-le v1, v2, :cond_0

    .line 492
    const/4 v1, 0x2

    aget-object v0, v0, v1

    .line 500
    :goto_0
    return-object v0

    .line 494
    :cond_0
    array-length v1, v0

    add-int/lit8 v1, v1, -0x1

    aget-object v0, v0, v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 496
    :catch_0
    move-exception v0

    .line 497
    sget-object v1, Lcom/peel/util/bx;->a:Ljava/lang/String;

    sget-object v2, Lcom/peel/util/bx;->a:Ljava/lang/String;

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 500
    :cond_1
    const-string/jumbo v0, ""

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;Landroid/content/Context;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 706
    const-string/jumbo v0, "[^A-Za-z0-9]"

    const-string/jumbo v1, ""

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    .line 709
    :try_start_0
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const-string/jumbo v2, "string"

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v0, v2, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 710
    if-nez v0, :cond_0

    .line 713
    :goto_0
    return-object p0

    .line 711
    :cond_0
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object p0

    goto :goto_0

    .line 712
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;Landroid/content/res/Resources;)Ljava/lang/String;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 720
    :try_start_0
    const-string/jumbo v0, "string"

    invoke-virtual {p2, p0, v0, p1}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 721
    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 724
    :cond_0
    :goto_0
    return-object p0

    .line 722
    :cond_1
    invoke-virtual {p2, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object p0

    goto :goto_0

    .line 723
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 3121
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "#"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "#"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "#"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "#"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "#"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(ZLcom/peel/control/a;Lcom/peel/control/h;Landroid/content/Context;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 1834
    invoke-virtual {p2}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/data/g;->d()I

    move-result v0

    .line 1835
    const/4 v1, 0x6

    if-ne v0, v1, :cond_0

    .line 1836
    invoke-virtual {p2}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/data/g;->f()Ljava/lang/String;

    move-result-object v0

    .line 1866
    :goto_0
    return-object v0

    .line 1839
    :cond_0
    if-eqz p0, :cond_1

    .line 1840
    packed-switch v0, :pswitch_data_0

    .line 1866
    :pswitch_0
    const/4 v0, 0x0

    goto :goto_0

    .line 1842
    :pswitch_1
    sget v0, Lcom/peel/ui/ft;->DeviceType1:I

    invoke-virtual {p3, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1844
    :pswitch_2
    sget v0, Lcom/peel/ui/ft;->DeviceType2:I

    invoke-virtual {p3, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1846
    :pswitch_3
    sget v0, Lcom/peel/ui/ft;->DeviceType10:I

    invoke-virtual {p3, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1848
    :pswitch_4
    sget v0, Lcom/peel/ui/ft;->DeviceType3:I

    invoke-virtual {p3, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1850
    :pswitch_5
    sget v0, Lcom/peel/ui/ft;->DeviceType4:I

    invoke-virtual {p3, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1852
    :pswitch_6
    sget v0, Lcom/peel/ui/ft;->DeviceType20:I

    invoke-virtual {p3, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1854
    :pswitch_7
    sget v0, Lcom/peel/ui/ft;->DeviceType18:I

    invoke-virtual {p3, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1856
    :pswitch_8
    sget v0, Lcom/peel/ui/ft;->DeviceType5:I

    invoke-virtual {p3, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1858
    :pswitch_9
    sget v0, Lcom/peel/ui/ft;->DeviceType13:I

    invoke-virtual {p3, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1860
    :pswitch_a
    sget v0, Lcom/peel/ui/ft;->DeviceType14:I

    invoke-virtual {p3, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1863
    :cond_1
    invoke-virtual {p1}, Lcom/peel/control/a;->a()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1840
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_4
        :pswitch_5
        :pswitch_8
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_9
        :pswitch_a
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_7
        :pswitch_0
        :pswitch_6
    .end packed-switch
.end method

.method public static a([Lcom/peel/content/listing/Listing;)Ljava/lang/String;
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v1, 0x0

    .line 504
    array-length v3, p0

    move v2, v1

    :goto_0
    if-ge v2, v3, :cond_5

    aget-object v0, p0, v2

    .line 505
    invoke-virtual {v0}, Lcom/peel/content/listing/Listing;->n()Ljava/lang/String;

    move-result-object v4

    const-string/jumbo v5, "live"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 507
    invoke-virtual {v0}, Lcom/peel/content/listing/Listing;->f()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/peel/util/bx;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 535
    :goto_1
    return-object v0

    .line 508
    :cond_0
    invoke-virtual {v0}, Lcom/peel/content/listing/Listing;->n()Ljava/lang/String;

    move-result-object v4

    const-string/jumbo v5, "dtv"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 511
    invoke-virtual {v0}, Lcom/peel/content/listing/Listing;->f()Ljava/lang/String;

    move-result-object v0

    const/4 v4, 0x6

    invoke-virtual {v0, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v4, "[/?]"

    invoke-virtual {v0, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 512
    array-length v4, v0

    const/4 v5, 0x3

    if-lt v4, v5, :cond_4

    aget-object v0, v0, v8

    goto :goto_1

    .line 513
    :cond_1
    invoke-virtual {v0}, Lcom/peel/content/listing/Listing;->n()Ljava/lang/String;

    move-result-object v4

    const-string/jumbo v5, "nflx"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 517
    invoke-virtual {v0}, Lcom/peel/content/listing/Listing;->e()Ljava/net/URI;

    move-result-object v0

    .line 518
    if-eqz v0, :cond_4

    .line 519
    invoke-virtual {v0}, Ljava/net/URI;->getQuery()Ljava/lang/String;

    move-result-object v0

    .line 520
    const-string/jumbo v4, "epiId"

    invoke-virtual {v0, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 521
    const-string/jumbo v4, "&"

    invoke-virtual {v0, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 522
    array-length v5, v4

    move v0, v1

    :goto_2
    if-ge v0, v5, :cond_4

    aget-object v6, v4, v0

    .line 523
    const-string/jumbo v7, "epiId"

    invoke-virtual {v6, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 524
    const-string/jumbo v0, "="

    invoke-virtual {v6, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 525
    aget-object v0, v0, v8

    goto :goto_1

    .line 522
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 529
    :cond_3
    const-string/jumbo v1, "="

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 530
    aget-object v0, v0, v8

    goto :goto_1

    .line 504
    :cond_4
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto/16 :goto_0

    .line 535
    :cond_5
    aget-object v0, p0, v1

    invoke-virtual {v0}, Lcom/peel/content/listing/Listing;->g()Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method public static a(Lcom/peel/control/a;)Ljava/util/ArrayList;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/peel/control/a;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/peel/control/h;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1823
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1824
    invoke-virtual {p0}, Lcom/peel/control/a;->e()[Lcom/peel/control/h;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_1

    aget-object v4, v2, v0

    .line 1826
    invoke-virtual {v4}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v5

    invoke-virtual {v5}, Lcom/peel/data/g;->d()I

    move-result v5

    const/4 v6, 0x6

    if-ne v5, v6, :cond_0

    .line 1824
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1828
    :cond_0
    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 1830
    :cond_1
    return-object v1
.end method

.method public static a(ILjava/lang/String;Ljava/lang/String;)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2705
    sget-object v0, Lcom/peel/content/a;->f:Lcom/peel/content/user/User;

    invoke-virtual {v0}, Lcom/peel/content/user/User;->i()Landroid/os/Bundle;

    move-result-object v0

    .line 2706
    if-eqz v0, :cond_1

    .line 2707
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    .line 2708
    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-lt v1, p0, :cond_0

    .line 2709
    const/4 v1, 0x0

    invoke-interface {v0, v1, p0}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v0

    .line 2713
    :cond_0
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Ljava/util/List;JJLjava/lang/String;Lcom/peel/data/ContentRoom;)Ljava/util/List;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/peel/content/listing/Listing;",
            ">;JJ",
            "Ljava/lang/String;",
            "Lcom/peel/data/ContentRoom;",
            ")",
            "Ljava/util/List",
            "<[",
            "Lcom/peel/content/listing/Listing;",
            ">;"
        }
    .end annotation

    .prologue
    .line 416
    const/4 v8, 0x0

    move-object v1, p0

    move-wide v2, p1

    move-wide v4, p3

    move-object v6, p5

    move-object v7, p6

    invoke-static/range {v1 .. v8}, Lcom/peel/util/bx;->a(Ljava/util/List;JJLjava/lang/String;Lcom/peel/data/ContentRoom;Z)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/util/List;JJLjava/lang/String;Lcom/peel/data/ContentRoom;Z)Ljava/util/List;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/peel/content/listing/Listing;",
            ">;JJ",
            "Ljava/lang/String;",
            "Lcom/peel/data/ContentRoom;",
            "Z)",
            "Ljava/util/List",
            "<[",
            "Lcom/peel/content/listing/Listing;",
            ">;"
        }
    .end annotation

    .prologue
    .line 422
    sget-object v0, Lcom/peel/content/a;->h:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-nez v0, :cond_0

    .line 423
    sget-object v0, Lcom/peel/util/bx;->a:Ljava/lang/String;

    const-string/jumbo v1, "groupListings called when PeelContent is not loaded."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 424
    const/4 v0, 0x0

    .line 484
    :goto_0
    return-object v0

    .line 428
    :cond_0
    if-eqz p5, :cond_1

    .line 430
    :try_start_0
    invoke-static/range {p5 .. p5}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Comparator;

    invoke-static {p0, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 435
    :cond_1
    :goto_1
    sget-object v0, Lcom/peel/content/a;->f:Lcom/peel/content/user/User;

    invoke-virtual {v0}, Lcom/peel/content/user/User;->f()Landroid/os/Bundle;

    move-result-object v0

    .line 436
    if-nez v0, :cond_9

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    move-object v1, v0

    .line 438
    :goto_2
    const-string/jumbo v0, "live"

    invoke-static {v0}, Lcom/peel/content/a;->c(Ljava/lang/String;)Lcom/peel/content/library/Library;

    move-result-object v0

    check-cast v0, Lcom/peel/content/library/LiveLibrary;

    .line 439
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    .line 440
    if-eqz v0, :cond_2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {p6 .. p6}, Lcom/peel/data/ContentRoom;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Lcom/peel/content/library/LiveLibrary;->e()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 441
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {p6 .. p6}, Lcom/peel/data/ContentRoom;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Lcom/peel/content/library/LiveLibrary;->e()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 444
    :cond_2
    new-instance v3, Ljava/util/LinkedHashMap;

    invoke-direct {v3}, Ljava/util/LinkedHashMap;-><init>()V

    .line 445
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_3
    :goto_3
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/content/listing/Listing;

    .line 446
    if-eqz v0, :cond_3

    .line 447
    invoke-virtual {v0}, Lcom/peel/content/listing/Listing;->h()Ljava/lang/String;

    move-result-object v5

    .line 449
    const-string/jumbo v1, "live"

    invoke-virtual {v0}, Lcom/peel/content/listing/Listing;->n()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    instance-of v1, v0, Lcom/peel/content/listing/LiveListing;

    if-eqz v1, :cond_5

    move-object v1, v0

    .line 450
    check-cast v1, Lcom/peel/content/listing/LiveListing;

    .line 452
    invoke-virtual {v1}, Lcom/peel/content/listing/LiveListing;->q()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v2, v6}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_3

    .line 454
    const-wide/16 v6, -0x1

    cmp-long v6, p1, v6

    if-lez v6, :cond_4

    invoke-virtual {v1}, Lcom/peel/content/listing/LiveListing;->o()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-virtual {v0}, Lcom/peel/content/listing/Listing;->j()J

    move-result-wide v8

    add-long/2addr v6, v8

    cmp-long v6, p1, v6

    if-ltz v6, :cond_4

    .line 455
    sget-object v1, Lcom/peel/util/bx;->a:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Lcom/peel/content/listing/Listing;->e()Ljava/net/URI;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v5, "(ended) dropped"

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    .line 459
    :cond_4
    if-eqz p7, :cond_5

    .line 461
    const-wide/16 v6, -0x1

    cmp-long v6, p3, v6

    if-lez v6, :cond_5

    invoke-virtual {v1}, Lcom/peel/content/listing/LiveListing;->o()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    cmp-long v1, p3, v6

    if-gez v1, :cond_5

    .line 462
    sget-object v1, Lcom/peel/util/bx;->a:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Lcom/peel/content/listing/Listing;->e()Ljava/net/URI;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v5, "(not started) dropped"

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_3

    .line 468
    :cond_5
    invoke-interface {v3, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    .line 469
    if-nez v1, :cond_6

    .line 470
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 471
    invoke-interface {v3, v5, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 474
    :cond_6
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_3

    .line 477
    :cond_7
    new-instance v1, Ljava/util/ArrayList;

    invoke-interface {v3}, Ljava/util/Map;->size()I

    move-result v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 479
    invoke-interface {v3}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_4
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 480
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 481
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    new-array v3, v3, [Lcom/peel/content/listing/Listing;

    invoke-interface {v0, v3}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_4

    :cond_8
    move-object v0, v1

    .line 484
    goto/16 :goto_0

    .line 431
    :catch_0
    move-exception v0

    goto/16 :goto_1

    :cond_9
    move-object v1, v0

    goto/16 :goto_2
.end method

.method public static a([Ljava/lang/String;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Landroid/os/Bundle;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1141
    new-instance v1, Ljava/util/ArrayList;

    array-length v0, p0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 1142
    array-length v2, p0

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, p0, v0

    .line 1143
    invoke-static {v3}, Lcom/peel/util/bx;->b(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1142
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1147
    :cond_0
    new-instance v0, Lcom/peel/util/ca;

    invoke-direct {v0}, Lcom/peel/util/ca;-><init>()V

    invoke-static {v1, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 1154
    return-object v1
.end method

.method public static a(Landroid/content/Context;Landroid/content/SharedPreferences;)V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v1, 0x0

    .line 1313
    const-string/jumbo v0, "PREFS_VERSION"

    invoke-interface {p1, v0, v1}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 1314
    if-gt v6, v0, :cond_0

    .line 1350
    :goto_0
    return-void

    .line 1318
    :cond_0
    :try_start_0
    const-string/jumbo v0, "PREFS_VERSION"

    const/4 v1, 0x0

    invoke-interface {p1, v0, v1}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 1320
    packed-switch v0, :pswitch_data_0

    .line 1349
    :cond_1
    :goto_1
    invoke-interface {p1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v1, "PREFS_VERSION"

    invoke-interface {v0, v1, v6}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    goto :goto_0

    .line 1324
    :pswitch_0
    :try_start_1
    const-string/jumbo v0, "config_legacy"

    const-string/jumbo v1, "none"

    invoke-interface {p1, v0, v1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1325
    const-string/jumbo v0, "none"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1326
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 1327
    sget v2, Lcom/peel/ui/fk;->countries:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/peel/util/bx;->a([Ljava/lang/String;)Ljava/util/List;

    move-result-object v2

    .line 1329
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 1330
    const-string/jumbo v4, "name"

    sget v5, Lcom/peel/ui/ft;->other_countries:I

    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v4, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1331
    const-string/jumbo v0, "endpoint"

    const-string/jumbo v4, "usa"

    invoke-virtual {v3, v0, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1332
    const-string/jumbo v0, "iso"

    const-string/jumbo v4, "XX"

    invoke-virtual {v3, v0, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1333
    const-string/jumbo v0, "type"

    const-string/jumbo v4, "none"

    invoke-virtual {v3, v0, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1334
    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1336
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    .line 1337
    const-string/jumbo v3, "iso"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 1338
    invoke-interface {p1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string/jumbo v2, "config_legacy"

    invoke-static {v0}, Lcom/peel/util/bx;->b(Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_1

    .line 1344
    :catch_0
    move-exception v0

    .line 1345
    sget-object v1, Lcom/peel/util/bx;->a:Ljava/lang/String;

    sget-object v2, Lcom/peel/util/bx;->a:Ljava/lang/String;

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_0

    .line 1320
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public static a(Landroid/content/Context;Landroid/os/Bundle;ILcom/peel/util/t;)V
    .locals 11

    .prologue
    .line 2247
    const-string/jumbo v0, "live"

    invoke-static {v0}, Lcom/peel/content/a;->c(Ljava/lang/String;)Lcom/peel/content/library/Library;

    move-result-object v10

    .line 2248
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string/jumbo v1, "country_ISO"

    const-string/jumbo v2, "US"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2249
    const-string/jumbo v0, "listing"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/peel/content/listing/LiveListing;

    .line 2250
    const-string/jumbo v2, "id"

    invoke-virtual {v0}, Lcom/peel/content/listing/LiveListing;->f()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2251
    const-string/jumbo v2, "path"

    const-string/jumbo v3, "add/setreminder"

    invoke-virtual {p1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2252
    const-string/jumbo v2, "episodeid"

    invoke-static {v0}, Lcom/peel/util/bx;->b(Lcom/peel/content/listing/Listing;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2253
    invoke-virtual {v0}, Lcom/peel/content/listing/LiveListing;->b()Ljava/lang/String;

    move-result-object v6

    .line 2254
    const-string/jumbo v2, "providerid"

    invoke-virtual {v10}, Lcom/peel/content/library/Library;->e()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2255
    const-string/jumbo v2, "country"

    invoke-virtual {p1, v2, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2256
    const-string/jumbo v1, "language"

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2257
    const-string/jumbo v1, "scheduletime"

    invoke-virtual {v0}, Lcom/peel/content/listing/LiveListing;->r()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2259
    if-eqz v6, :cond_0

    .line 2260
    const-string/jumbo v0, "showid"

    invoke-virtual {p1, v0, v6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2263
    :cond_0
    invoke-static {}, Lcom/peel/util/a/f;->a()Lcom/peel/util/a/f;

    move-result-object v0

    sget-object v1, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v1}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v1

    if-nez v1, :cond_2

    const/4 v1, 0x1

    :goto_0
    const/16 v2, 0x4d2

    const-string/jumbo v3, "episodeid"

    .line 2265
    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v7, 0x0

    move v3, p2

    .line 2263
    invoke-virtual/range {v0 .. v7}, Lcom/peel/util/a/f;->a(IIILjava/lang/String;ILjava/lang/String;I)V

    .line 2267
    const-string/jumbo v0, "remindertype"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2268
    const-string/jumbo v1, "show"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 2269
    invoke-static {}, Lcom/peel/util/a/f;->a()Lcom/peel/util/a/f;

    move-result-object v2

    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v0}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v0

    if-nez v0, :cond_3

    const/4 v3, 0x1

    :goto_1
    const/16 v4, 0x4d3

    const/4 v7, 0x0

    const-string/jumbo v0, "extra"

    .line 2273
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x0

    move v5, p2

    .line 2269
    invoke-virtual/range {v2 .. v9}, Lcom/peel/util/a/f;->a(IIILjava/lang/String;ILjava/lang/String;I)V

    .line 2281
    :cond_1
    :goto_2
    new-instance v0, Lcom/peel/util/cp;

    invoke-direct {v0, p0, p3}, Lcom/peel/util/cp;-><init>(Landroid/content/Context;Lcom/peel/util/t;)V

    invoke-virtual {v10, p1, v0}, Lcom/peel/content/library/Library;->b(Landroid/os/Bundle;Lcom/peel/util/t;)V

    .line 2294
    return-void

    .line 2263
    :cond_2
    sget-object v1, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v1}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/data/at;->f()I

    move-result v1

    goto :goto_0

    .line 2269
    :cond_3
    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v0}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/data/at;->f()I

    move-result v3

    goto :goto_1

    .line 2274
    :cond_4
    const-string/jumbo v1, "team"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2275
    invoke-static {}, Lcom/peel/util/a/f;->a()Lcom/peel/util/a/f;

    move-result-object v0

    sget-object v1, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v1}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v1

    if-nez v1, :cond_5

    const/4 v1, 0x1

    :goto_3
    const/16 v2, 0x4d4

    const/16 v3, 0x7d3

    const-string/jumbo v4, "teamids"

    .line 2278
    invoke-virtual {p1, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    .line 2275
    invoke-virtual/range {v0 .. v5}, Lcom/peel/util/a/f;->a(IIILjava/lang/String;I)V

    goto :goto_2

    :cond_5
    sget-object v1, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v1}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/data/at;->f()I

    move-result v1

    goto :goto_3
.end method

.method public static a(Landroid/content/Context;Landroid/os/Bundle;Lcom/peel/content/listing/LiveListing;ILcom/peel/util/t;)V
    .locals 10

    .prologue
    .line 3037
    const-string/jumbo v0, "title"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 3038
    const-string/jumbo v1, "desc"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 3039
    invoke-virtual {p2}, Lcom/peel/content/listing/LiveListing;->o()J

    move-result-wide v2

    invoke-virtual {p2}, Lcom/peel/content/listing/LiveListing;->j()J

    move-result-wide v4

    add-long/2addr v2, v4

    .line 3041
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    .line 3042
    new-instance v5, Landroid/content/ContentValues;

    invoke-direct {v5}, Landroid/content/ContentValues;-><init>()V

    .line 3043
    const-string/jumbo v6, "dtstart"

    invoke-virtual {p2}, Lcom/peel/content/listing/LiveListing;->o()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 3044
    const-string/jumbo v6, "dtend"

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v5, v6, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 3045
    const-string/jumbo v2, "title"

    invoke-virtual {v5, v2, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 3046
    const-string/jumbo v0, "description"

    invoke-virtual {v5, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 3047
    const-string/jumbo v0, "calendar_id"

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v5, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 3048
    const-string/jumbo v0, "eventTimezone"

    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/TimeZone;->getID()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 3049
    const-string/jumbo v0, "hasAlarm"

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v5, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 3050
    sget-object v0, Landroid/provider/CalendarContract$Events;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v4, v0, v5}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v1

    .line 3052
    invoke-virtual {v1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 3053
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 3054
    const-string/jumbo v5, "event_id"

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v5, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 3055
    const-string/jumbo v2, "method"

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 3056
    const-string/jumbo v2, "minutes"

    const/16 v3, 0xa

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 3059
    sget-object v2, Landroid/provider/CalendarContract$Reminders;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v4, v2, v0}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v0

    .line 3060
    if-eqz v0, :cond_1

    sget-object v0, Lcom/peel/content/a;->f:Lcom/peel/content/user/User;

    if-eqz v0, :cond_1

    .line 3061
    sget-object v0, Lcom/peel/content/a;->f:Lcom/peel/content/user/User;

    invoke-virtual {v0}, Lcom/peel/content/user/User;->r()Ljava/util/Map;

    move-result-object v0

    .line 3063
    if-nez v0, :cond_0

    .line 3064
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 3067
    :cond_0
    invoke-static {p2}, Lcom/peel/util/bx;->b(Lcom/peel/content/listing/Listing;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p2}, Lcom/peel/content/listing/LiveListing;->r()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "!"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 3068
    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 3067
    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 3070
    sget-object v1, Lcom/peel/content/a;->f:Lcom/peel/content/user/User;

    invoke-virtual {v1, v0}, Lcom/peel/content/user/User;->b(Ljava/util/Map;)V

    .line 3071
    sget-object v0, Lcom/peel/content/a;->f:Lcom/peel/content/user/User;

    invoke-virtual {v0}, Lcom/peel/content/user/User;->u()V

    .line 3072
    invoke-static {}, Lcom/peel/util/a/f;->a()Lcom/peel/util/a/f;

    move-result-object v0

    sget-object v1, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v1}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v1

    if-nez v1, :cond_2

    const/4 v1, 0x1

    :goto_0
    const/16 v2, 0x4d2

    const-string/jumbo v3, "episodeid"

    .line 3074
    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {p2}, Lcom/peel/content/listing/LiveListing;->b()Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    move v3, p3

    .line 3072
    invoke-virtual/range {v0 .. v7}, Lcom/peel/util/a/f;->a(IIILjava/lang/String;ILjava/lang/String;I)V

    .line 3076
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "reminder_updated"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 3077
    invoke-static {p0}, Landroid/support/v4/a/q;->a(Landroid/content/Context;)Landroid/support/v4/a/q;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/support/v4/a/q;->a(Landroid/content/Intent;)Z

    .line 3079
    if-eqz p4, :cond_1

    .line 3080
    const/4 v0, 0x1

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {p4, v0, v1, v2}, Lcom/peel/util/t;->a(ZLjava/lang/Object;Ljava/lang/String;)V

    .line 3086
    :cond_1
    return-void

    .line 3072
    :cond_2
    sget-object v1, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v1}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/data/at;->f()I

    move-result v1

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Landroid/support/v4/app/ae;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1005
    invoke-static {v3}, Lcom/peel/util/bx;->c(I)V

    .line 1006
    sget-object v0, Lcom/peel/util/bx;->e:Lcom/peel/widget/ag;

    if-nez v0, :cond_0

    .line 1007
    new-instance v0, Lcom/peel/widget/ag;

    invoke-direct {v0, p0}, Lcom/peel/widget/ag;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/peel/util/bx;->e:Lcom/peel/widget/ag;

    .line 1008
    sget-object v0, Lcom/peel/util/bx;->e:Lcom/peel/widget/ag;

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/peel/ui/ft;->no_internet:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/peel/widget/ag;->a(Ljava/lang/CharSequence;)Lcom/peel/widget/ag;

    move-result-object v0

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/peel/ui/ft;->no_internet_alert:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/peel/widget/ag;->b(Ljava/lang/CharSequence;)Lcom/peel/widget/ag;

    move-result-object v0

    sget v1, Lcom/peel/ui/ft;->label_settings:I

    new-instance v2, Lcom/peel/util/dm;

    invoke-direct {v2, p0}, Lcom/peel/util/dm;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1, v2}, Lcom/peel/widget/ag;->c(ILandroid/content/DialogInterface$OnClickListener;)Lcom/peel/widget/ag;

    move-result-object v0

    sget v1, Lcom/peel/ui/ft;->ok:I

    new-instance v2, Lcom/peel/util/dl;

    invoke-direct {v2, p1}, Lcom/peel/util/dl;-><init>(Landroid/support/v4/app/ae;)V

    .line 1012
    invoke-virtual {v0, v1, v2}, Lcom/peel/widget/ag;->a(ILandroid/content/DialogInterface$OnClickListener;)Lcom/peel/widget/ag;

    move-result-object v0

    .line 1022
    invoke-virtual {v0, v3}, Lcom/peel/widget/ag;->a(Z)Lcom/peel/widget/ag;

    .line 1024
    :cond_0
    sget-object v0, Lcom/peel/util/bx;->e:Lcom/peel/widget/ag;

    invoke-virtual {v0, v3}, Lcom/peel/widget/ag;->setCanceledOnTouchOutside(Z)V

    .line 1025
    sget-boolean v0, Lcom/peel/util/bx;->h:Z

    if-eqz v0, :cond_1

    sget-object v0, Lcom/peel/util/bx;->e:Lcom/peel/widget/ag;

    invoke-virtual {v0}, Lcom/peel/widget/ag;->isShowing()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1026
    const-string/jumbo v0, "input_method"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 1027
    invoke-virtual {p1}, Landroid/support/v4/app/ae;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    invoke-virtual {v0, v1, v3}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 1028
    sget-object v0, Lcom/peel/util/bx;->e:Lcom/peel/widget/ag;

    sget-object v1, Lcom/peel/util/bx;->e:Lcom/peel/widget/ag;

    invoke-virtual {v0, v1}, Lcom/peel/widget/ag;->a(Lcom/peel/widget/ag;)Lcom/peel/widget/ag;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/widget/ag;->show()V

    .line 1030
    :cond_1
    return-void
.end method

.method public static a(Landroid/content/Context;Landroid/view/View;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 9

    .prologue
    const/4 v6, 0x1

    const/4 v8, 0x0

    .line 2443
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v0

    if-le v0, v6, :cond_0

    .line 2444
    const-string/jumbo v0, "has_shown_tunein_check"

    invoke-static {p0, v0}, Lcom/peel/util/dq;->b(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2446
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lcom/peel/ui/fq;->popup_tunein_check:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 2447
    invoke-virtual {v2, v8, v8}, Landroid/view/View;->measure(II)V

    .line 2448
    new-instance v1, Landroid/widget/PopupWindow;

    invoke-direct {v1, v2}, Landroid/widget/PopupWindow;-><init>(Landroid/view/View;)V

    .line 2449
    const/4 v0, -0x1

    const/4 v3, -0x2

    invoke-virtual {v1, v0, v3}, Landroid/widget/PopupWindow;->setWindowLayoutMode(II)V

    .line 2450
    invoke-virtual {v1, v6}, Landroid/widget/PopupWindow;->setOutsideTouchable(Z)V

    .line 2451
    sget v0, Lcom/peel/ui/fp;->tunein_check_msg:I

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    sget v3, Lcom/peel/ui/ft;->description_tunein_check:I

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    aput-object p3, v4, v8

    aput-object p2, v4, v6

    invoke-virtual {p0, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2452
    sget v0, Lcom/peel/ui/fp;->btn_yes:I

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v3, Lcom/peel/util/cu;

    invoke-direct {v3, p0, v1, p4}, Lcom/peel/util/cu;-><init>(Landroid/content/Context;Landroid/widget/PopupWindow;I)V

    invoke-virtual {v0, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2461
    sget v0, Lcom/peel/ui/fp;->btn_no:I

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    new-instance v0, Lcom/peel/util/cv;

    move v2, p4

    move-object v3, p2

    move-object v4, p3

    move-object v5, p0

    invoke-direct/range {v0 .. v5}, Lcom/peel/util/cv;-><init>(Landroid/widget/PopupWindow;ILjava/lang/String;Ljava/lang/String;Landroid/content/Context;)V

    invoke-virtual {v7, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2476
    const/16 v0, 0x50

    invoke-virtual {v1, p1, v0, v8, v8}, Landroid/widget/PopupWindow;->showAtLocation(Landroid/view/View;III)V

    .line 2478
    invoke-static {}, Lcom/peel/util/a/f;->a()Lcom/peel/util/a/f;

    move-result-object v2

    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v0}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v0

    if-nez v0, :cond_1

    move v0, v6

    :goto_0
    const/16 v3, 0x4e2

    invoke-virtual {v2, v0, v3, p4}, Lcom/peel/util/a/f;->a(III)V

    .line 2482
    sget-object v0, Lcom/peel/util/bx;->a:Ljava/lang/String;

    const-string/jumbo v2, "dismiss pop up"

    new-instance v3, Lcom/peel/util/cw;

    invoke-direct {v3, v1}, Lcom/peel/util/cw;-><init>(Landroid/widget/PopupWindow;)V

    const-wide/16 v4, 0x4e20

    invoke-static {v0, v2, v3, v4, v5}, Lcom/peel/util/i;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;J)Ljava/lang/Runnable;

    .line 2490
    :cond_0
    return-void

    .line 2478
    :cond_1
    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v0}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/data/at;->f()I

    move-result v0

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Lcom/peel/content/listing/Listing;Ljava/lang/String;)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 3012
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1, v2, v2}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 3014
    if-lez v0, :cond_0

    .line 3015
    sget-object v0, Lcom/peel/content/a;->f:Lcom/peel/content/user/User;

    invoke-virtual {v0}, Lcom/peel/content/user/User;->r()Ljava/util/Map;

    move-result-object v1

    .line 3017
    if-eqz v1, :cond_0

    invoke-interface {v1}, Ljava/util/Map;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 3018
    invoke-static {p1}, Lcom/peel/util/bx;->b(Lcom/peel/content/listing/Listing;)Ljava/lang/String;

    move-result-object v2

    .line 3020
    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 3022
    if-eqz v0, :cond_0

    .line 3023
    const-string/jumbo v3, "!"

    invoke-virtual {v0, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 3025
    const/4 v3, 0x0

    aget-object v0, v0, v3

    check-cast p1, Lcom/peel/content/listing/LiveListing;

    invoke-virtual {p1}, Lcom/peel/content/listing/LiveListing;->r()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3026
    invoke-interface {v1, v2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 3027
    sget-object v0, Lcom/peel/content/a;->f:Lcom/peel/content/user/User;

    invoke-virtual {v0, v1}, Lcom/peel/content/user/User;->b(Ljava/util/Map;)V

    .line 3028
    sget-object v0, Lcom/peel/content/a;->f:Lcom/peel/content/user/User;

    invoke-virtual {v0}, Lcom/peel/content/user/User;->u()V

    .line 3033
    :cond_0
    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/peel/control/RoomControl;)V
    .locals 0

    .prologue
    .line 1424
    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/peel/control/a;I)V
    .locals 3

    .prologue
    .line 1427
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Lcom/peel/control/a;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "japan_last_mode_idx"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 1428
    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/peel/control/a;Ljava/lang/String;Lcom/peel/util/t;)V
    .locals 7

    .prologue
    .line 1436
    invoke-static {}, Lcom/peel/content/a;->a()Lcom/peel/data/ContentRoom;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/data/ContentRoom;->a()Ljava/lang/String;

    move-result-object v6

    new-instance v0, Lcom/peel/util/cb;

    const/4 v1, 0x1

    move-object v2, p3

    move-object v3, p2

    move-object v4, p1

    move-object v5, p0

    invoke-direct/range {v0 .. v5}, Lcom/peel/util/cb;-><init>(ILcom/peel/util/t;Ljava/lang/String;Lcom/peel/control/a;Landroid/content/Context;)V

    invoke-static {v6, v0}, Lcom/peel/content/a;->a(Ljava/lang/String;Lcom/peel/util/t;)V

    .line 1629
    return-void
.end method

.method static synthetic a(Landroid/content/Context;Lcom/peel/control/a;Ljava/net/URI;Lcom/peel/util/s;)V
    .locals 0

    .prologue
    .line 121
    invoke-static {p0, p1, p2, p3}, Lcom/peel/util/bx;->b(Landroid/content/Context;Lcom/peel/control/a;Ljava/net/URI;Lcom/peel/util/s;)V

    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/peel/d/i;)V
    .locals 7

    .prologue
    const/4 v6, -0x1

    const/4 v2, 0x1

    const/4 v0, 0x0

    .line 1183
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v3

    .line 1186
    const-string/jumbo v1, "ro.csc.country_code"

    const/4 v4, 0x0

    invoke-interface {v3, v1, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1187
    if-nez v1, :cond_0

    .line 1188
    invoke-static {p0}, Lcom/peel/util/bx;->d(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 1189
    if-eqz v1, :cond_0

    .line 1190
    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v4

    const-string/jumbo v5, "ro.csc.country_code"

    invoke-interface {v4, v5, v1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 1194
    :cond_0
    sget-object v1, Lcom/peel/c/a;->b:Lcom/peel/c/e;

    invoke-static {v1}, Lcom/peel/c/a;->a(Lcom/peel/c/e;)Ljava/lang/Object;

    move-result-object v1

    sget-object v4, Lcom/peel/c/b;->b:Lcom/peel/c/b;

    if-ne v1, v4, :cond_2

    .line 1195
    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v1, "yosemite_enabled"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v1, "yosemite_available"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 1230
    :cond_1
    :goto_0
    :try_start_0
    const-string/jumbo v0, "versioncode"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v2, v4, v5}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v2

    iget v2, v2, Landroid/content/pm/PackageInfo;->versionCode:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/peel/d/i;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1235
    :goto_1
    invoke-static {p0, v3}, Lcom/peel/util/bx;->a(Landroid/content/Context;Landroid/content/SharedPreferences;)V

    .line 1236
    return-void

    .line 1196
    :cond_2
    sget-object v1, Lcom/peel/c/a;->b:Lcom/peel/c/e;

    invoke-static {v1}, Lcom/peel/c/a;->a(Lcom/peel/c/e;)Ljava/lang/Object;

    move-result-object v1

    sget-object v4, Lcom/peel/c/b;->b:Lcom/peel/c/b;

    if-eq v1, v4, :cond_1

    const-string/jumbo v1, "yosemite_enabled"

    invoke-virtual {p1, v1, v6}, Lcom/peel/d/i;->a(Ljava/lang/String;I)I

    move-result v1

    if-ne v6, v1, :cond_1

    .line 1197
    invoke-static {p0}, Lcom/peel/util/bx;->f(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    .line 1199
    const-string/jumbo v1, "yosemite_enabled"

    invoke-interface {v3, v1, v0}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    .line 1202
    if-eqz v1, :cond_4

    .line 1203
    sget-object v5, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-static {p0, v4, v5}, Lcom/peel/util/bx;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v5

    .line 1204
    if-nez v5, :cond_4

    .line 1205
    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string/jumbo v5, "yosemite_enabled"

    .line 1206
    invoke-interface {v1, v5, v0}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string/jumbo v5, "yosemite_available"

    .line 1207
    invoke-interface {v1, v5, v0}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 1208
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 1210
    const-string/jumbo v1, "yosemite_enabled"

    invoke-virtual {p1, v1, v0}, Lcom/peel/d/i;->b(Ljava/lang/String;I)V

    .line 1214
    :goto_2
    const-string/jumbo v1, "com.sec.yosemite.phone"

    invoke-static {p0, v1}, Lcom/peel/util/bx;->c(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1216
    const-string/jumbo v0, "yosemite_enabled"

    invoke-virtual {p1, v0, v2}, Lcom/peel/d/i;->b(Ljava/lang/String;I)V

    .line 1217
    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v1, "yosemite_enabled"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    move v0, v2

    .line 1220
    :cond_3
    if-nez v0, :cond_1

    .line 1221
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1222
    const-string/jumbo v0, "ro.csc.countryiso_code"

    invoke-virtual {p1, v0, v4}, Lcom/peel/d/i;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1224
    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-static {p0, v4, v0}, Lcom/peel/util/bx;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Z

    goto/16 :goto_0

    .line 1231
    :catch_0
    move-exception v0

    .line 1232
    sget-object v1, Lcom/peel/util/bx;->a:Ljava/lang/String;

    sget-object v2, Lcom/peel/util/bx;->a:Ljava/lang/String;

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_1

    :cond_4
    move v0, v1

    goto :goto_2
.end method

.method public static a(Landroid/content/Context;Lcom/peel/d/i;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 7

    .prologue
    const/4 v5, 0x0

    .line 733
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v6, v5

    invoke-static/range {v0 .. v6}, Lcom/peel/util/bx;->a(Landroid/content/Context;Lcom/peel/d/i;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Ljava/lang/String;)V

    .line 734
    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/peel/d/i;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Ljava/lang/String;)V
    .locals 9

    .prologue
    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 737
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 738
    const-string/jumbo v1, "feedback_subject"

    sget v2, Lcom/peel/ui/ft;->missing_ir_input_subject:I

    new-array v3, v8, [Ljava/lang/Object;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v6

    aput-object p4, v3, v7

    invoke-virtual {p0, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 739
    const-string/jumbo v1, "feedback_desc"

    const-string/jumbo v2, ""

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 740
    const-string/jumbo v1, "feedback_model"

    sget v2, Lcom/peel/ui/ft;->missing_ir_input_model:I

    new-array v3, v8, [Ljava/lang/Object;

    aput-object p2, v3, v6

    aput-object p3, v3, v7

    invoke-virtual {p0, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 741
    const-string/jumbo v1, "feedback_tags"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "-"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 742
    if-eqz p5, :cond_0

    if-eqz p6, :cond_0

    .line 743
    invoke-virtual {p1, p6, p5}, Lcom/peel/d/i;->a(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 745
    :cond_0
    invoke-virtual {p1}, Lcom/peel/d/i;->c()Landroid/support/v4/app/ae;

    move-result-object v1

    const-class v2, Lcom/peel/h/a/du;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lcom/peel/d/e;->c(Landroid/support/v4/app/ae;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 746
    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/peel/util/t;)V
    .locals 8

    .prologue
    const/4 v3, -0x1

    .line 2800
    const/4 v0, 0x2

    new-array v5, v0, [I

    .line 2801
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    .line 2802
    const/16 v0, 0x14

    .line 2803
    const/16 v1, 0xe

    .line 2804
    const/4 v2, 0x0

    aput v3, v5, v2

    .line 2805
    const/4 v2, 0x1

    aput v3, v5, v2

    .line 2806
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 2808
    const-string/jumbo v0, "tilelayout"

    const-string/jumbo v1, "space"

    const-string/jumbo v3, "TILE_LAYOUT"

    const-string/jumbo v4, "tilelayoutinit"

    new-instance v6, Lcom/peel/util/dg;

    invoke-direct {v6, v5, v7, p1}, Lcom/peel/util/dg;-><init>([ILandroid/content/res/Resources;Lcom/peel/util/t;)V

    move-object v5, p0

    invoke-static/range {v0 .. v6}, Lcom/peel/util/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;Lcom/peel/util/t;)V

    .line 2831
    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 1353
    sget-object v0, Lcom/peel/util/bx;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "update prefs current room: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1354
    if-eqz p1, :cond_0

    const-string/jumbo v0, ""

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1355
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v1, "current_room"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 1357
    :cond_0
    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Landroid/widget/EditText;J)V
    .locals 3

    .prologue
    .line 1904
    const-string/jumbo v0, "show keyboard after a delay"

    new-instance v1, Lcom/peel/util/ci;

    invoke-direct {v1, p0, p2}, Lcom/peel/util/ci;-><init>(Landroid/content/Context;Landroid/widget/EditText;)V

    invoke-static {p1, v0, v1, p3, p4}, Lcom/peel/util/i;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;J)Ljava/lang/Runnable;

    .line 1911
    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Lcom/peel/util/t;)V
    .locals 3

    .prologue
    .line 1916
    sget-object v0, Lcom/peel/util/bx;->a:Ljava/lang/String;

    const-string/jumbo v1, "grabbing turned on countries"

    new-instance v2, Lcom/peel/util/cj;

    invoke-direct {v2, p1, p2, p0}, Lcom/peel/util/cj;-><init>(Ljava/lang/String;Lcom/peel/util/t;Landroid/content/Context;)V

    invoke-static {v0, v1, v2}, Lcom/peel/util/i;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    .line 1980
    return-void
.end method

.method public static a(Landroid/content/Context;Z)V
    .locals 2

    .prologue
    .line 1131
    const-string/jumbo v0, "is_setup_complete"

    const-string/jumbo v1, "widget_pref"

    invoke-static {p0, v0, p1, v1}, Lcom/peel/util/dq;->a(Landroid/content/Context;Ljava/lang/String;ZLjava/lang/String;)V

    .line 1136
    return-void
.end method

.method public static a(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 776
    if-eqz p0, :cond_1

    .line 777
    invoke-virtual {p0}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 778
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "key: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " -- value: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 779
    sget-object v2, Lcom/peel/util/bx;->a:Ljava/lang/String;

    invoke-static {v2, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 778
    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    .line 782
    :cond_1
    return-void
.end method

.method public static a(Landroid/view/View;Ljava/lang/String;)V
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 799
    invoke-virtual {p0, v0}, Landroid/view/View;->setEnabled(Z)V

    .line 800
    invoke-virtual {p0, v0}, Landroid/view/View;->setClickable(Z)V

    .line 801
    const-string/jumbo v0, "re-enable list after some delay"

    new-instance v1, Lcom/peel/util/co;

    invoke-direct {v1, p0}, Lcom/peel/util/co;-><init>(Landroid/view/View;)V

    const-wide/16 v2, 0x1f4

    invoke-static {p1, v0, v1, v2, v3}, Lcom/peel/util/i;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;J)Ljava/lang/Runnable;

    .line 808
    return-void
.end method

.method public static a(Landroid/view/View;Ljava/lang/String;I)V
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 785
    if-eqz p0, :cond_0

    .line 786
    invoke-virtual {p0, v0}, Landroid/view/View;->setEnabled(Z)V

    .line 787
    invoke-virtual {p0, v0}, Landroid/view/View;->setClickable(Z)V

    .line 788
    const-string/jumbo v0, "re-enable view after some delay"

    new-instance v1, Lcom/peel/util/by;

    invoke-direct {v1, p0}, Lcom/peel/util/by;-><init>(Landroid/view/View;)V

    int-to-long v2, p2

    invoke-static {p1, v0, v1, v2, v3}, Lcom/peel/util/i;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;J)Ljava/lang/Runnable;

    .line 796
    :cond_0
    return-void
.end method

.method public static a(Landroid/widget/ListView;)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 393
    invoke-virtual {p0}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v3

    .line 394
    if-nez v3, :cond_0

    .line 410
    :goto_0
    return-void

    :cond_0
    move v0, v1

    move v2, v1

    .line 400
    :goto_1
    invoke-interface {v3}, Landroid/widget/ListAdapter;->getCount()I

    move-result v4

    if-ge v0, v4, :cond_1

    .line 401
    const/4 v4, 0x0

    invoke-interface {v3, v0, v4, p0}, Landroid/widget/ListAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v4

    .line 402
    invoke-virtual {v4, v1, v1}, Landroid/view/View;->measure(II)V

    .line 403
    invoke-virtual {v4}, Landroid/view/View;->getMeasuredHeight()I

    move-result v4

    add-int/2addr v2, v4

    .line 400
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 406
    :cond_1
    invoke-virtual {p0}, Landroid/widget/ListView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 407
    invoke-virtual {p0}, Landroid/widget/ListView;->getDividerHeight()I

    move-result v1

    invoke-interface {v3}, Landroid/widget/ListAdapter;->getCount()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    mul-int/2addr v1, v3

    add-int/2addr v1, v2

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 408
    invoke-virtual {p0, v0}, Landroid/widget/ListView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 409
    invoke-virtual {p0}, Landroid/widget/ListView;->requestLayout()V

    goto :goto_0
.end method

.method public static a(Landroid/widget/TextView;Lcom/peel/content/listing/Listing;)V
    .locals 1

    .prologue
    .line 305
    if-eqz p0, :cond_0

    .line 307
    invoke-virtual {p1}, Lcom/peel/content/listing/Listing;->l()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 308
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 310
    :cond_0
    return-void
.end method

.method public static a(Lcom/peel/content/listing/Listing;ILjava/lang/String;)V
    .locals 13

    .prologue
    .line 2581
    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v0}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v12

    .line 2582
    invoke-virtual {p0}, Lcom/peel/content/listing/Listing;->n()Ljava/lang/String;

    move-result-object v0

    .line 2584
    const-string/jumbo v1, "live"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 2585
    invoke-static {p0}, Lcom/peel/util/bx;->b(Lcom/peel/content/listing/Listing;)Ljava/lang/String;

    move-result-object v6

    .line 2586
    check-cast p0, Lcom/peel/content/listing/LiveListing;

    .line 2587
    invoke-static {}, Lcom/peel/util/a/f;->a()Lcom/peel/util/a/f;

    move-result-object v0

    if-nez v12, :cond_2

    const/4 v1, 0x1

    .line 2588
    :goto_0
    const/16 v2, 0x3f3

    .line 2590
    invoke-virtual {p0}, Lcom/peel/content/listing/LiveListing;->d()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v7, 0x0

    invoke-virtual {p0}, Lcom/peel/content/listing/LiveListing;->c()Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x0

    if-nez p2, :cond_3

    const-string/jumbo v10, "live"

    :goto_1
    const/4 v11, 0x0

    move v3, p1

    .line 2587
    invoke-virtual/range {v0 .. v11}, Lcom/peel/util/a/f;->a(IIILjava/lang/String;ILjava/lang/String;ILjava/lang/String;ILjava/lang/String;I)V

    .line 2592
    const/16 v0, 0x7dd

    if-ne p1, v0, :cond_0

    .line 2593
    invoke-static {}, Lcom/peel/util/a/f;->a()Lcom/peel/util/a/f;

    move-result-object v0

    if-nez v12, :cond_4

    const/4 v1, 0x1

    .line 2594
    :goto_2
    const/16 v2, 0x4b2

    .line 2596
    invoke-virtual {p0}, Lcom/peel/content/listing/LiveListing;->d()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v7, 0x0

    invoke-virtual {p0}, Lcom/peel/content/listing/LiveListing;->c()Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x0

    move v3, p1

    .line 2593
    invoke-virtual/range {v0 .. v9}, Lcom/peel/util/a/f;->a(IIILjava/lang/String;ILjava/lang/String;ILjava/lang/String;I)V

    .line 2599
    :cond_0
    invoke-static {v6}, Lcom/peel/util/bx;->d(Ljava/lang/String;)V

    .line 2614
    :cond_1
    :goto_3
    return-void

    .line 2588
    :cond_2
    invoke-virtual {v12}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/data/at;->f()I

    move-result v1

    goto :goto_0

    :cond_3
    move-object v10, p2

    .line 2590
    goto :goto_1

    .line 2594
    :cond_4
    invoke-virtual {v12}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/data/at;->f()I

    move-result v1

    goto :goto_2

    .line 2600
    :cond_5
    const-string/jumbo v1, "dtv"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    move-object v3, p0

    .line 2601
    check-cast v3, Lcom/peel/content/listing/DirecTVListing;

    .line 2602
    invoke-static {}, Lcom/peel/util/a/f;->a()Lcom/peel/util/a/f;

    move-result-object v0

    if-nez v12, :cond_6

    const/4 v1, 0x1

    .line 2603
    :goto_4
    const/16 v2, 0x3f3

    .line 2604
    invoke-virtual {v3}, Lcom/peel/content/listing/DirecTVListing;->d()I

    move-result v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    .line 2605
    invoke-static {p0}, Lcom/peel/util/bx;->b(Lcom/peel/content/listing/Listing;)Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    invoke-virtual {v3}, Lcom/peel/content/listing/DirecTVListing;->c()Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x0

    if-nez p2, :cond_7

    const-string/jumbo v10, "directv"

    :goto_5
    const/4 v11, 0x0

    move v3, p1

    .line 2602
    invoke-virtual/range {v0 .. v11}, Lcom/peel/util/a/f;->a(IIILjava/lang/String;ILjava/lang/String;ILjava/lang/String;ILjava/lang/String;I)V

    goto :goto_3

    .line 2603
    :cond_6
    invoke-virtual {v12}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/data/at;->f()I

    move-result v1

    goto :goto_4

    :cond_7
    move-object v10, p2

    .line 2605
    goto :goto_5

    .line 2608
    :cond_8
    const-string/jumbo v1, "nflx"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2609
    invoke-static {}, Lcom/peel/util/a/f;->a()Lcom/peel/util/a/f;

    move-result-object v0

    if-nez v12, :cond_9

    const/4 v1, 0x1

    .line 2610
    :goto_6
    const/16 v2, 0x3f3

    const-string/jumbo v4, ""

    const/4 v5, 0x0

    .line 2612
    invoke-static {p0}, Lcom/peel/util/bx;->b(Lcom/peel/content/listing/Listing;)Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    const-string/jumbo v8, ""

    const/4 v9, 0x0

    if-nez p2, :cond_a

    const-string/jumbo v10, "netflix"

    :goto_7
    const/4 v11, 0x0

    move v3, p1

    .line 2609
    invoke-virtual/range {v0 .. v11}, Lcom/peel/util/a/f;->a(IIILjava/lang/String;ILjava/lang/String;ILjava/lang/String;ILjava/lang/String;I)V

    goto :goto_3

    .line 2610
    :cond_9
    invoke-virtual {v12}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/data/at;->f()I

    move-result v1

    goto :goto_6

    :cond_a
    move-object v10, p2

    .line 2612
    goto :goto_7
.end method

.method public static a(Lcom/peel/content/listing/Listing;Landroid/content/Context;Lcom/peel/util/t;)V
    .locals 4

    .prologue
    .line 2971
    check-cast p0, Lcom/peel/content/listing/LiveListing;

    .line 2973
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 2974
    invoke-static {p1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    const-string/jumbo v2, "country_ISO"

    const-string/jumbo v3, "US"

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2975
    const-string/jumbo v2, "listing"

    invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2976
    invoke-virtual {p0}, Lcom/peel/content/listing/LiveListing;->k()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "sports"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    invoke-virtual {p0}, Lcom/peel/content/listing/LiveListing;->p()Z

    move-result v2

    if-eqz v2, :cond_1

    const-string/jumbo v2, "us"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string/jumbo v2, "CA"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2977
    :cond_0
    invoke-virtual {p0}, Lcom/peel/content/listing/LiveListing;->b()Ljava/lang/String;

    move-result-object v1

    .line 2978
    const-string/jumbo v2, "remindertype"

    const-string/jumbo v3, "show"

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2979
    const-string/jumbo v2, "showid"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2985
    :goto_0
    const-string/jumbo v1, "live"

    invoke-static {v1}, Lcom/peel/content/a;->c(Ljava/lang/String;)Lcom/peel/content/library/Library;

    move-result-object v1

    .line 2986
    const-string/jumbo v2, "path"

    const-string/jumbo v3, "delete/setreminder"

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2987
    const-string/jumbo v2, "id"

    invoke-virtual {p0}, Lcom/peel/content/listing/LiveListing;->f()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2988
    const-string/jumbo v2, "episodeid"

    invoke-static {p0}, Lcom/peel/util/bx;->b(Lcom/peel/content/listing/Listing;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2990
    new-instance v2, Lcom/peel/util/di;

    invoke-direct {v2, p2}, Lcom/peel/util/di;-><init>(Lcom/peel/util/t;)V

    invoke-virtual {v1, v0, v2}, Lcom/peel/content/library/Library;->b(Landroid/os/Bundle;Lcom/peel/util/t;)V

    .line 2998
    return-void

    .line 2981
    :cond_1
    const-string/jumbo v1, "remindertype"

    const-string/jumbo v2, "schedule"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2982
    const-string/jumbo v1, "scheduletime"

    invoke-virtual {p0}, Lcom/peel/content/listing/LiveListing;->r()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static a(Lcom/peel/content/listing/Listing;Landroid/support/v4/app/ae;ILjava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 2188
    check-cast p0, Lcom/peel/content/listing/LiveListing;

    .line 2189
    invoke-virtual {p0}, Lcom/peel/content/listing/LiveListing;->p()Z

    move-result v0

    .line 2190
    invoke-static {p1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    const-string/jumbo v2, "country_ISO"

    const-string/jumbo v3, "US"

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2192
    if-eqz v0, :cond_1

    const-string/jumbo v2, "us"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string/jumbo v2, "CA"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2193
    :cond_0
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 2194
    const-string/jumbo v2, "listing"

    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2195
    const-string/jumbo v2, "showoption"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2196
    const-string/jumbo v0, "context_id"

    invoke-virtual {v1, v0, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 2197
    const-class v0, Lcom/peel/ui/fx;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0, v1}, Lcom/peel/d/e;->c(Landroid/support/v4/app/ae;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 2244
    :goto_0
    return-void

    .line 2198
    :cond_1
    const-string/jumbo v0, "CN"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2199
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 2200
    const-string/jumbo v1, "listing"

    invoke-virtual {v0, v1, p0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2201
    const-string/jumbo v1, "context_id"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 2202
    const-string/jumbo v1, "episodeid"

    invoke-static {p0}, Lcom/peel/util/bx;->b(Lcom/peel/content/listing/Listing;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2203
    const-string/jumbo v1, "title"

    invoke-virtual {p0}, Lcom/peel/content/listing/LiveListing;->l()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2204
    const-string/jumbo v1, "desc"

    invoke-virtual {p0}, Lcom/peel/content/listing/LiveListing;->x()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2206
    invoke-virtual {p1}, Landroid/support/v4/app/ae;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const/16 v2, 0x7d0

    new-instance v3, Lcom/peel/util/cm;

    invoke-direct {v3, p0, p1}, Lcom/peel/util/cm;-><init>(Lcom/peel/content/listing/LiveListing;Landroid/support/v4/app/ae;)V

    invoke-static {v1, v0, p0, v2, v3}, Lcom/peel/util/bx;->a(Landroid/content/Context;Landroid/os/Bundle;Lcom/peel/content/listing/LiveListing;ILcom/peel/util/t;)V

    goto :goto_0

    .line 2218
    :cond_2
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 2219
    const-string/jumbo v1, "listing"

    invoke-virtual {v0, v1, p0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2220
    const-string/jumbo v1, "remindertype"

    const-string/jumbo v2, "schedule"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2221
    const-string/jumbo v1, "listing"

    invoke-virtual {v0, v1, p0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2222
    const-string/jumbo v1, "image_url"

    invoke-virtual {v0, v1, p4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2223
    const-string/jumbo v1, "context_id"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 2225
    if-eqz p3, :cond_3

    .line 2226
    const-string/jumbo v1, "showoption"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2227
    const-string/jumbo v1, "action"

    invoke-virtual {v0, v1, p3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2228
    const-class v1, Lcom/peel/ui/fx;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v1, v0}, Lcom/peel/d/e;->c(Landroid/support/v4/app/ae;Ljava/lang/String;Landroid/os/Bundle;)V

    goto/16 :goto_0

    .line 2231
    :cond_3
    new-instance v1, Lcom/peel/util/cn;

    invoke-direct {v1, v0, p0, p1}, Lcom/peel/util/cn;-><init>(Landroid/os/Bundle;Lcom/peel/content/listing/LiveListing;Landroid/support/v4/app/ae;)V

    invoke-static {p1, v0, p2, v1}, Lcom/peel/util/bx;->a(Landroid/content/Context;Landroid/os/Bundle;ILcom/peel/util/t;)V

    goto/16 :goto_0
.end method

.method public static a(Lcom/peel/content/listing/LiveListing;)V
    .locals 6

    .prologue
    .line 2617
    const-string/jumbo v0, "live"

    invoke-static {v0}, Lcom/peel/content/a;->c(Ljava/lang/String;)Lcom/peel/content/library/Library;

    move-result-object v4

    .line 2618
    if-nez v4, :cond_0

    .line 2624
    :goto_0
    return-void

    .line 2622
    :cond_0
    invoke-virtual {p0}, Lcom/peel/content/listing/LiveListing;->q()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/peel/content/listing/LiveListing;->c()Ljava/lang/String;

    move-result-object v1

    .line 2623
    invoke-virtual {p0}, Lcom/peel/content/listing/LiveListing;->d()Ljava/lang/String;

    move-result-object v2

    invoke-static {}, Lcom/peel/content/a;->a()Lcom/peel/data/ContentRoom;

    move-result-object v3

    invoke-virtual {v3}, Lcom/peel/data/ContentRoom;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4}, Lcom/peel/content/library/Library;->e()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    .line 2622
    invoke-static/range {v0 .. v5}, Lcom/peel/util/bx;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    goto :goto_0
.end method

.method static synthetic a(Lcom/peel/d/i;)V
    .locals 0

    .prologue
    .line 121
    invoke-static {p0}, Lcom/peel/util/bx;->b(Lcom/peel/d/i;)V

    return-void
.end method

.method public static a(Lcom/peel/d/i;Landroid/content/Context;)V
    .locals 7

    .prologue
    const/4 v4, 0x1

    const/4 v6, 0x0

    .line 1033
    const/4 v0, 0x2

    invoke-static {v0}, Lcom/peel/util/bx;->c(I)V

    .line 1034
    sget-object v0, Lcom/peel/util/bx;->g:Lcom/peel/widget/ag;

    if-nez v0, :cond_0

    .line 1035
    const-string/jumbo v0, "network_setup"

    invoke-virtual {p1, v0, v6}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 1036
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 1037
    sget v2, Lcom/peel/ui/fq;->network_dialog:I

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 1039
    sget v0, Lcom/peel/ui/fp;->do_not_show_btn:I

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    .line 1041
    new-instance v3, Lcom/peel/util/dn;

    invoke-direct {v3}, Lcom/peel/util/dn;-><init>()V

    invoke-virtual {v0, v3}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 1057
    invoke-virtual {v0, v4}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 1058
    sput-boolean v4, Lcom/peel/util/bx;->r:Z

    .line 1072
    new-instance v3, Lcom/peel/widget/ag;

    invoke-direct {v3, p1}, Lcom/peel/widget/ag;-><init>(Landroid/content/Context;)V

    sput-object v3, Lcom/peel/util/bx;->g:Lcom/peel/widget/ag;

    .line 1073
    sget-object v3, Lcom/peel/util/bx;->g:Lcom/peel/widget/ag;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    sget v5, Lcom/peel/ui/ft;->network_connect_to_wlan:I

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/peel/widget/ag;->a(Ljava/lang/CharSequence;)Lcom/peel/widget/ag;

    move-result-object v3

    sget v4, Lcom/peel/ui/ft;->cancel:I

    new-instance v5, Lcom/peel/util/bz;

    invoke-direct {v5, v1, p0, p1}, Lcom/peel/util/bz;-><init>(Landroid/content/SharedPreferences;Lcom/peel/d/i;Landroid/content/Context;)V

    .line 1074
    invoke-virtual {v3, v4, v5}, Lcom/peel/widget/ag;->c(ILandroid/content/DialogInterface$OnClickListener;)Lcom/peel/widget/ag;

    move-result-object v3

    sget v4, Lcom/peel/ui/ft;->network_connect:I

    new-instance v5, Lcom/peel/util/do;

    invoke-direct {v5, v0, v1}, Lcom/peel/util/do;-><init>(Landroid/widget/CheckBox;Landroid/content/SharedPreferences;)V

    .line 1086
    invoke-virtual {v3, v4, v5}, Lcom/peel/widget/ag;->a(ILandroid/content/DialogInterface$OnClickListener;)Lcom/peel/widget/ag;

    move-result-object v0

    .line 1094
    invoke-virtual {v0, v6}, Lcom/peel/widget/ag;->a(Z)Lcom/peel/widget/ag;

    .line 1096
    sget v0, Lcom/peel/ui/fp;->msg:I

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    sget v1, Lcom/peel/ui/ft;->network_will_connect_to_wlan:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 1098
    sget-object v0, Lcom/peel/util/bx;->g:Lcom/peel/widget/ag;

    invoke-virtual {v0, v2}, Lcom/peel/widget/ag;->a(Landroid/view/View;)Lcom/peel/widget/ag;

    .line 1099
    sget-object v0, Lcom/peel/util/bx;->g:Lcom/peel/widget/ag;

    invoke-virtual {v0, v6}, Lcom/peel/widget/ag;->setCanceledOnTouchOutside(Z)V

    .line 1102
    :cond_0
    sget-boolean v0, Lcom/peel/util/bx;->h:Z

    if-eqz v0, :cond_1

    sget-object v0, Lcom/peel/util/bx;->g:Lcom/peel/widget/ag;

    invoke-virtual {v0}, Lcom/peel/widget/ag;->isShowing()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1103
    sget-object v0, Lcom/peel/util/bx;->g:Lcom/peel/widget/ag;

    sget-object v1, Lcom/peel/util/bx;->g:Lcom/peel/widget/ag;

    invoke-virtual {v0, v1}, Lcom/peel/widget/ag;->a(Lcom/peel/widget/ag;)Lcom/peel/widget/ag;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/widget/ag;->show()V

    .line 1105
    :cond_1
    return-void
.end method

.method public static a(Lcom/peel/d/i;Landroid/content/Context;I)V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 913
    invoke-static {v6}, Lcom/peel/util/bx;->c(I)V

    .line 914
    sget-object v0, Lcom/peel/util/bx;->f:Lcom/peel/widget/ag;

    if-nez v0, :cond_1

    .line 915
    const-string/jumbo v0, "network_setup"

    invoke-virtual {p1, v0, v7}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 916
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 917
    sget v2, Lcom/peel/ui/fq;->network_dialog:I

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 919
    sget v0, Lcom/peel/ui/fp;->do_not_show_btn:I

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    .line 921
    new-instance v3, Lcom/peel/util/dc;

    invoke-direct {v3}, Lcom/peel/util/dc;-><init>()V

    invoke-virtual {v0, v3}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 948
    new-instance v3, Lcom/peel/widget/ag;

    invoke-direct {v3, p1}, Lcom/peel/widget/ag;-><init>(Landroid/content/Context;)V

    sput-object v3, Lcom/peel/util/bx;->f:Lcom/peel/widget/ag;

    .line 949
    sget-object v3, Lcom/peel/util/bx;->f:Lcom/peel/widget/ag;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    sget v5, Lcom/peel/ui/ft;->network_mobile_title:I

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/peel/widget/ag;->a(Ljava/lang/CharSequence;)Lcom/peel/widget/ag;

    move-result-object v3

    sget v4, Lcom/peel/ui/ft;->cancel:I

    new-instance v5, Lcom/peel/util/dk;

    invoke-direct {v5, v1, p2, p0, p1}, Lcom/peel/util/dk;-><init>(Landroid/content/SharedPreferences;ILcom/peel/d/i;Landroid/content/Context;)V

    invoke-virtual {v3, v4, v5}, Lcom/peel/widget/ag;->c(ILandroid/content/DialogInterface$OnClickListener;)Lcom/peel/widget/ag;

    move-result-object v3

    sget v4, Lcom/peel/ui/ft;->network_connect:I

    new-instance v5, Lcom/peel/util/dj;

    invoke-direct {v5, v0, v1, p2}, Lcom/peel/util/dj;-><init>(Landroid/widget/CheckBox;Landroid/content/SharedPreferences;I)V

    .line 965
    invoke-virtual {v3, v4, v5}, Lcom/peel/widget/ag;->a(ILandroid/content/DialogInterface$OnClickListener;)Lcom/peel/widget/ag;

    move-result-object v1

    .line 976
    invoke-virtual {v1, v7}, Lcom/peel/widget/ag;->a(Z)Lcom/peel/widget/ag;

    .line 978
    if-ne v6, p2, :cond_0

    .line 979
    sget v1, Lcom/peel/ui/fp;->msg:I

    invoke-virtual {v2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    sget v3, Lcom/peel/ui/ft;->network_extra_charge:I

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(I)V

    .line 981
    :cond_0
    const-string/jumbo v1, "china"

    invoke-static {p1}, Lcom/peel/util/bx;->d(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 982
    sget v1, Lcom/peel/ui/fp;->msg:I

    invoke-virtual {v2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    .line 983
    sget v1, Lcom/peel/ui/fp;->msg:I

    invoke-virtual {v2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    const-string/jumbo v4, "Wi-Fi"

    const-string/jumbo v5, "WLAN"

    invoke-virtual {v3, v4, v5}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 985
    invoke-virtual {v0, v6}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 986
    sput-boolean v6, Lcom/peel/util/bx;->q:Z

    .line 992
    :goto_0
    sget-object v0, Lcom/peel/util/bx;->f:Lcom/peel/widget/ag;

    invoke-virtual {v0, v2}, Lcom/peel/widget/ag;->a(Landroid/view/View;)Lcom/peel/widget/ag;

    .line 993
    sget-object v0, Lcom/peel/util/bx;->f:Lcom/peel/widget/ag;

    invoke-virtual {v0, v7}, Lcom/peel/widget/ag;->setCanceledOnTouchOutside(Z)V

    .line 996
    :cond_1
    sget-boolean v0, Lcom/peel/util/bx;->h:Z

    if-eqz v0, :cond_2

    sget-object v0, Lcom/peel/util/bx;->f:Lcom/peel/widget/ag;

    invoke-virtual {v0}, Lcom/peel/widget/ag;->isShowing()Z

    move-result v0

    if-nez v0, :cond_2

    .line 997
    sget-object v0, Lcom/peel/util/bx;->f:Lcom/peel/widget/ag;

    sget-object v1, Lcom/peel/util/bx;->f:Lcom/peel/widget/ag;

    invoke-virtual {v0, v1}, Lcom/peel/widget/ag;->a(Lcom/peel/widget/ag;)Lcom/peel/widget/ag;

    move-result-object v0

    sget-object v1, Lcom/peel/util/bx;->f:Lcom/peel/widget/ag;

    invoke-virtual {v0, v1}, Lcom/peel/widget/ag;->a(Lcom/peel/widget/ag;)Lcom/peel/widget/ag;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/widget/ag;->show()V

    .line 1002
    :cond_2
    return-void

    .line 988
    :cond_3
    sget v0, Lcom/peel/ui/fp;->msg:I

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    .line 989
    sget v0, Lcom/peel/ui/fp;->msg:I

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const-string/jumbo v3, "WLAN"

    const-string/jumbo v4, "Wi-Fi"

    invoke-virtual {v1, v3, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public static a(Lcom/peel/d/i;Landroid/content/Context;Z)V
    .locals 9

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    const/4 v8, 0x0

    .line 827
    if-nez p0, :cond_1

    .line 910
    :cond_0
    :goto_0
    return-void

    .line 829
    :cond_1
    const-string/jumbo v0, "connectivity"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 830
    const-string/jumbo v3, "network_setup"

    invoke-virtual {p1, v3, v8}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v3

    .line 832
    invoke-static {p1}, Lcom/peel/util/bx;->d(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    .line 833
    sget-object v5, Lcom/peel/util/bx;->a:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "\n\ncountry_code: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, "\n\n"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 836
    if-eqz v0, :cond_11

    .line 837
    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v0

    .line 839
    :goto_1
    const-string/jumbo v5, "wlan_network"

    invoke-interface {v3, v5, v8}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v5

    .line 840
    const-string/jumbo v6, "wlan_dialog"

    invoke-interface {v3, v6, v8}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v6

    .line 842
    if-eqz v0, :cond_a

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getType()I

    move-result v7

    if-nez v7, :cond_a

    .line 844
    sget-object v1, Lcom/peel/util/bx;->a:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "\n\n network type MOBILE path... network offline: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget-boolean v6, Lcom/peel/util/b/a;->a:Z

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, "\n\n"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v1, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 846
    const-string/jumbo v1, "mobile_network"

    invoke-interface {v3, v1, v8}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    .line 847
    const-string/jumbo v5, "roaming_network"

    invoke-interface {v3, v5, v8}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v5

    .line 848
    const-string/jumbo v6, "network_dialog"

    invoke-interface {v3, v6, v8}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v6

    .line 849
    const-string/jumbo v7, "roaming_dialog"

    invoke-interface {v3, v7, v8}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    .line 852
    if-eqz v0, :cond_6

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isRoaming()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 853
    if-nez v3, :cond_5

    if-eqz v5, :cond_2

    if-eqz v5, :cond_5

    if-eqz p2, :cond_5

    .line 854
    :cond_2
    const-string/jumbo v0, "Hong Kong"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_4

    const-string/jumbo v0, "Taiwan"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 855
    invoke-static {p0, p1, v2}, Lcom/peel/util/bx;->a(Lcom/peel/d/i;Landroid/content/Context;I)V

    .line 906
    :cond_3
    :goto_2
    sget-boolean v0, Lcom/peel/util/b/a;->a:Z

    if-nez v0, :cond_0

    .line 907
    const/4 v0, -0x1

    invoke-static {v0}, Lcom/peel/util/bx;->c(I)V

    goto/16 :goto_0

    .line 857
    :cond_4
    sput-boolean v8, Lcom/peel/util/b/a;->a:Z

    goto :goto_2

    .line 859
    :cond_5
    if-nez v5, :cond_3

    .line 860
    sput-boolean v2, Lcom/peel/util/b/a;->a:Z

    goto :goto_2

    .line 864
    :cond_6
    if-nez v6, :cond_9

    if-eqz v1, :cond_7

    if-eqz v1, :cond_9

    if-eqz p2, :cond_9

    .line 865
    :cond_7
    const-string/jumbo v0, "Hong Kong"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_8

    const-string/jumbo v0, "Taiwan"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_8

    .line 866
    invoke-static {p0, p1, v8}, Lcom/peel/util/bx;->a(Lcom/peel/d/i;Landroid/content/Context;I)V

    goto :goto_2

    .line 868
    :cond_8
    sput-boolean v8, Lcom/peel/util/b/a;->a:Z

    goto :goto_2

    .line 870
    :cond_9
    if-nez v1, :cond_3

    .line 871
    sput-boolean v2, Lcom/peel/util/b/a;->a:Z

    goto :goto_2

    .line 874
    :cond_a
    if-eqz v0, :cond_10

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getType()I

    move-result v3

    if-ne v3, v2, :cond_10

    .line 875
    sget-object v0, Lcom/peel/util/bx;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "\n\n network type WIFI path... network offline: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-boolean v3, Lcom/peel/util/b/a;->a:Z

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v3, "\n\n"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 876
    const-string/jumbo v0, "china"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 877
    if-nez v6, :cond_d

    if-eqz v5, :cond_b

    if-eqz v5, :cond_d

    if-eqz p2, :cond_d

    .line 878
    :cond_b
    invoke-static {p0, p1}, Lcom/peel/util/bx;->a(Lcom/peel/d/i;Landroid/content/Context;)V

    .line 886
    :goto_3
    invoke-static {p1}, Lcom/peel/util/bx;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 887
    const-string/jumbo v0, "wifi_ssid"

    invoke-static {p1, v0}, Lcom/peel/util/dq;->c(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 888
    if-eqz v1, :cond_3

    .line 889
    if-eqz v0, :cond_c

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 890
    :cond_c
    const-string/jumbo v0, "wifi_ssid"

    invoke-static {p1, v0, v1}, Lcom/peel/util/dq;->d(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 892
    invoke-static {}, Lcom/peel/util/a/f;->a()Lcom/peel/util/a/f;

    move-result-object v3

    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    .line 893
    invoke-virtual {v0}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v0

    if-nez v0, :cond_f

    move v0, v2

    .line 894
    :goto_4
    const/16 v2, 0x51c

    const/16 v4, 0x7db

    .line 892
    invoke-virtual {v3, v0, v2, v4, v1}, Lcom/peel/util/a/f;->a(IIILjava/lang/String;)V

    goto/16 :goto_2

    .line 880
    :cond_d
    sput-boolean v8, Lcom/peel/util/b/a;->a:Z

    goto :goto_3

    .line 883
    :cond_e
    sput-boolean v8, Lcom/peel/util/b/a;->a:Z

    goto :goto_3

    .line 893
    :cond_f
    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v0}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v0

    .line 894
    invoke-virtual {v0}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/data/at;->f()I

    move-result v0

    goto :goto_4

    .line 900
    :cond_10
    if-nez v0, :cond_3

    .line 901
    sput-boolean v2, Lcom/peel/util/b/a;->a:Z

    .line 902
    invoke-static {p1, v1}, Lcom/peel/util/bx;->a(Landroid/content/Context;Landroid/support/v4/app/ae;)V

    .line 903
    sget-object v0, Lcom/peel/util/bx;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "\n\n else if networkInfo == null path... network offline: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-boolean v2, Lcom/peel/util/b/a;->a:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "\n\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2

    :cond_11
    move-object v0, v1

    goto/16 :goto_1
.end method

.method public static a(Lcom/peel/data/Channel;)V
    .locals 6

    .prologue
    .line 2628
    const-string/jumbo v0, "live"

    invoke-static {v0}, Lcom/peel/content/a;->c(Ljava/lang/String;)Lcom/peel/content/library/Library;

    move-result-object v4

    .line 2629
    if-nez v4, :cond_0

    .line 2633
    :goto_0
    return-void

    .line 2632
    :cond_0
    invoke-virtual {p0}, Lcom/peel/data/Channel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/peel/data/Channel;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/peel/data/Channel;->e()Ljava/lang/String;

    move-result-object v2

    invoke-static {}, Lcom/peel/content/a;->a()Lcom/peel/data/ContentRoom;

    move-result-object v3

    invoke-virtual {v3}, Lcom/peel/data/ContentRoom;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4}, Lcom/peel/content/library/Library;->e()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-static/range {v0 .. v5}, Lcom/peel/util/bx;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    goto :goto_0
.end method

.method public static a(Lcom/peel/util/t;)V
    .locals 4

    .prologue
    .line 2297
    sget-object v0, Lcom/peel/content/a;->f:Lcom/peel/content/user/User;

    invoke-virtual {v0}, Lcom/peel/content/user/User;->x()Ljava/lang/String;

    move-result-object v0

    .line 2298
    sget-object v1, Lcom/peel/util/bx;->a:Ljava/lang/String;

    const-string/jumbo v2, "refresh reminder"

    new-instance v3, Lcom/peel/util/cq;

    invoke-direct {v3, v0, p0}, Lcom/peel/util/cq;-><init>(Ljava/lang/String;Lcom/peel/util/t;)V

    invoke-static {v1, v2, v3}, Lcom/peel/util/i;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    .line 2314
    return-void
.end method

.method public static a(Ljava/io/File;Ljava/io/File;)V
    .locals 5

    .prologue
    .line 1393
    new-instance v0, Ljava/io/FileInputStream;

    invoke-direct {v0, p0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    .line 1394
    new-instance v1, Ljava/io/FileOutputStream;

    invoke-direct {v1, p1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 1397
    const/16 v2, 0x400

    new-array v2, v2, [B

    .line 1399
    :goto_0
    invoke-virtual {v0, v2}, Ljava/io/InputStream;->read([B)I

    move-result v3

    if-lez v3, :cond_0

    .line 1400
    const/4 v4, 0x0

    invoke-virtual {v1, v2, v4, v3}, Ljava/io/OutputStream;->write([BII)V

    goto :goto_0

    .line 1402
    :cond_0
    invoke-virtual {v0}, Ljava/io/InputStream;->close()V

    .line 1403
    invoke-virtual {v1}, Ljava/io/OutputStream;->close()V

    .line 1404
    return-void
.end method

.method public static a(Ljava/lang/String;Landroid/app/Activity;Z)V
    .locals 2

    .prologue
    .line 1870
    const-string/jumbo v0, "setProgressBarIndeterminateVisibility"

    new-instance v1, Lcom/peel/util/ch;

    invoke-direct {v1, p2, p1, p0}, Lcom/peel/util/ch;-><init>(ZLandroid/app/Activity;Ljava/lang/String;)V

    invoke-static {p0, v0, v1}, Lcom/peel/util/i;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/lang/Runnable;

    .line 1901
    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 2641
    const-string/jumbo v0, "live"

    invoke-static {v0}, Lcom/peel/content/a;->c(Ljava/lang/String;)Lcom/peel/content/library/Library;

    move-result-object v0

    .line 2642
    if-nez v0, :cond_0

    .line 2646
    :goto_0
    return-void

    .line 2645
    :cond_0
    invoke-static {}, Lcom/peel/content/a;->a()Lcom/peel/data/ContentRoom;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/data/ContentRoom;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, Lcom/peel/content/library/Library;->e()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-static/range {v0 .. v5}, Lcom/peel/util/bx;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 3

    .prologue
    .line 2650
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 2651
    const-string/jumbo v1, "channel"

    invoke-virtual {v0, v1, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2652
    const-string/jumbo v1, "callsign"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2653
    const-string/jumbo v1, "library"

    invoke-virtual {v0, v1, p4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2654
    const-string/jumbo v1, "channelNumber"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2655
    const-string/jumbo v1, "roomId"

    invoke-virtual {v0, v1, p3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2656
    const-string/jumbo v1, "path"

    const-string/jumbo v2, "channel/lastchannel"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2657
    if-eqz p5, :cond_0

    .line 2658
    const-string/jumbo v1, "test"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2660
    :cond_0
    sget-object v1, Lcom/peel/content/a;->f:Lcom/peel/content/user/User;

    new-instance v2, Lcom/peel/util/db;

    invoke-direct {v2, p5, p1}, Lcom/peel/util/db;-><init>(ZLjava/lang/String;)V

    invoke-virtual {v1, v0, v2}, Lcom/peel/content/user/User;->a(Landroid/os/Bundle;Lcom/peel/util/t;)V

    .line 2669
    return-void
.end method

.method public static a(Ljava/util/List;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/peel/content/listing/LiveListing;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2732
    sget-object v0, Lcom/peel/content/a;->f:Lcom/peel/content/user/User;

    invoke-virtual {v0}, Lcom/peel/content/user/User;->f()Landroid/os/Bundle;

    move-result-object v0

    .line 2733
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    .line 2735
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_1

    .line 2736
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 2738
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2739
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/content/listing/LiveListing;

    .line 2740
    invoke-virtual {v0}, Lcom/peel/content/listing/LiveListing;->q()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2741
    invoke-interface {v2}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    .line 2745
    :cond_1
    return-void
.end method

.method public static a()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2523
    sget-object v1, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v1}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2524
    sget-object v1, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v1}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/control/RoomControl;->g()[Ljava/lang/String;

    move-result-object v1

    .line 2525
    if-eqz v1, :cond_0

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    const-string/jumbo v2, "live"

    invoke-interface {v1, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    .line 2528
    :cond_0
    return v0
.end method

.method public static a(Landroid/content/Context;)Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 314
    const-string/jumbo v0, "connectivity"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 315
    invoke-virtual {v0, v1}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getState()Landroid/net/NetworkInfo$State;

    move-result-object v0

    sget-object v2, Landroid/net/NetworkInfo$State;->CONNECTED:Landroid/net/NetworkInfo$State;

    if-ne v0, v2, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 1281
    const-string/jumbo v2, "US"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string/jumbo v2, "KR"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1282
    :cond_0
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v2, "yosemite_available"

    invoke-interface {v0, v2, v1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    move v0, v1

    .line 1286
    :goto_0
    return v0

    .line 1285
    :cond_1
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string/jumbo v2, "yosemite_available"

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->apply()V

    goto :goto_0
.end method

.method public static a(Lcom/peel/control/h;Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 1813
    invoke-virtual {p0}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/data/g;->a()Ljava/util/Map;

    move-result-object v0

    .line 1815
    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v1

    if-lez v1, :cond_0

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1816
    const/4 v0, 0x1

    .line 1818
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Ljava/lang/CharSequence;)Z
    .locals 1

    .prologue
    .line 131
    sget-object v0, Lcom/peel/util/bx;->b:Ljava/util/regex/Pattern;

    invoke-virtual {v0, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->matches()Z

    move-result v0

    return v0
.end method

.method public static a(Ljava/util/ArrayList;Ljava/lang/String;)Z
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ")Z"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    .line 3089
    invoke-virtual {p0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 3090
    if-eqz v0, :cond_0

    const-string/jumbo v3, "#"

    invoke-virtual {v0, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    array-length v3, v3

    if-le v3, v1, :cond_0

    const-string/jumbo v3, "#"

    .line 3091
    invoke-virtual {v0, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    aget-object v0, v0, v1

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    .line 3095
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic a(Z)Z
    .locals 0

    .prologue
    .line 121
    sput-boolean p0, Lcom/peel/util/bx;->q:Z

    return p0
.end method

.method public static a(Ljava/lang/String;ZLandroid/content/Context;Ljava/lang/String;)[Ljava/lang/String;
    .locals 10

    .prologue
    const/4 v9, 0x0

    const/4 v8, 0x2

    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 321
    if-eqz p0, :cond_1

    .line 323
    :try_start_0
    new-instance v2, Ljava/util/GregorianCalendar;

    invoke-direct {v2}, Ljava/util/GregorianCalendar;-><init>()V

    .line 324
    invoke-virtual {v2}, Ljava/util/GregorianCalendar;->getTime()Ljava/util/Date;

    move-result-object v0

    .line 325
    new-instance v1, Ljava/text/SimpleDateFormat;

    const-string/jumbo v3, "yyyy-MM-dd HH:mm:ss"

    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v1, v3, v4}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 327
    invoke-virtual {v1, p0}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v3

    .line 329
    invoke-virtual {v3}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v0

    sub-long v0, v4, v0

    const-wide/32 v4, 0x36ee80

    div-long/2addr v0, v4

    long-to-int v0, v0

    .line 330
    const/16 v1, -0x18

    if-ge v0, v1, :cond_0

    .line 332
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    sget v2, Lcom/peel/ui/ft;->withinthelast24hoursavailable:I

    invoke-virtual {p2, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const/4 v2, 0x0

    aput-object v2, v0, v1

    .line 389
    :goto_0
    return-object v0

    .line 334
    :cond_0
    const/16 v1, 0xa8

    if-le v0, v1, :cond_2

    .line 336
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    sget v2, Lcom/peel/ui/ft;->onlyoneweekscheduleavailable:I

    invoke-virtual {p2, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const/4 v2, 0x0

    aput-object v2, v0, v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 385
    :catch_0
    move-exception v0

    .line 386
    sget-object v1, Lcom/peel/util/bx;->a:Ljava/lang/String;

    sget-object v2, Lcom/peel/util/bx;->a:Ljava/lang/String;

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 389
    :cond_1
    new-array v0, v8, [Ljava/lang/String;

    const-string/jumbo v1, "Unknown"

    aput-object v1, v0, v7

    aput-object v9, v0, v6

    goto :goto_0

    .line 342
    :cond_2
    :try_start_1
    new-instance v0, Ljava/text/DecimalFormat;

    const-string/jumbo v1, "#00"

    invoke-direct {v0, v1}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    .line 343
    if-eqz p1, :cond_4

    .line 344
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3}, Ljava/util/Date;->getHours()I

    move-result v4

    int-to-long v4, v4

    invoke-virtual {v0, v4, v5}, Ljava/text/NumberFormat;->format(J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v3}, Ljava/util/Date;->getMinutes()I

    move-result v0

    const/16 v4, 0x1e

    if-ge v0, v4, :cond_3

    const-string/jumbo v0, "00"

    :goto_1
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    .line 353
    :goto_2
    const/4 v0, 0x6

    invoke-virtual {v2, v0}, Ljava/util/GregorianCalendar;->get(I)I

    move-result v0

    .line 354
    invoke-virtual {v2, v3}, Ljava/util/GregorianCalendar;->setTime(Ljava/util/Date;)V

    .line 355
    const/4 v3, 0x6

    invoke-virtual {v2, v3}, Ljava/util/GregorianCalendar;->get(I)I

    move-result v3

    .line 357
    if-ne v0, v3, :cond_5

    .line 358
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v2, 0x0

    sget v3, Lcom/peel/ui/ft;->today:I

    invoke-virtual {p2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v0, v2

    const/4 v2, 0x1

    aput-object v1, v0, v2

    goto :goto_0

    .line 344
    :cond_3
    const-string/jumbo v0, "30"

    goto :goto_1

    .line 346
    :cond_4
    invoke-static {p0, p3}, Lcom/peel/util/x;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    goto :goto_2

    .line 359
    :cond_5
    sub-int v4, v3, v0

    if-ne v4, v6, :cond_6

    .line 360
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v2, 0x0

    sget v3, Lcom/peel/ui/ft;->tomorrow:I

    invoke-virtual {p2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v0, v2

    const/4 v2, 0x1

    aput-object v1, v0, v2

    goto/16 :goto_0

    .line 361
    :cond_6
    sub-int/2addr v0, v3

    if-ne v0, v6, :cond_7

    .line 362
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v2, 0x0

    sget v3, Lcom/peel/ui/ft;->yesterday:I

    invoke-virtual {p2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v0, v2

    const/4 v2, 0x1

    aput-object v1, v0, v2

    goto/16 :goto_0

    .line 364
    :cond_7
    const/4 v0, 0x7

    invoke-virtual {v2, v0}, Ljava/util/GregorianCalendar;->get(I)I

    move-result v0

    .line 365
    packed-switch v0, :pswitch_data_0

    .line 381
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string/jumbo v2, "Unknown"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const/4 v2, 0x0

    aput-object v2, v0, v1

    goto/16 :goto_0

    .line 367
    :pswitch_0
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v2, 0x0

    sget v3, Lcom/peel/ui/ft;->sunday:I

    invoke-virtual {p2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v0, v2

    const/4 v2, 0x1

    aput-object v1, v0, v2

    goto/16 :goto_0

    .line 369
    :pswitch_1
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v2, 0x0

    sget v3, Lcom/peel/ui/ft;->monday:I

    invoke-virtual {p2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v0, v2

    const/4 v2, 0x1

    aput-object v1, v0, v2

    goto/16 :goto_0

    .line 371
    :pswitch_2
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v2, 0x0

    sget v3, Lcom/peel/ui/ft;->tuesday:I

    invoke-virtual {p2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v0, v2

    const/4 v2, 0x1

    aput-object v1, v0, v2

    goto/16 :goto_0

    .line 373
    :pswitch_3
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v2, 0x0

    sget v3, Lcom/peel/ui/ft;->wednesday:I

    invoke-virtual {p2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v0, v2

    const/4 v2, 0x1

    aput-object v1, v0, v2

    goto/16 :goto_0

    .line 375
    :pswitch_4
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v2, 0x0

    sget v3, Lcom/peel/ui/ft;->thursday:I

    invoke-virtual {p2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v0, v2

    const/4 v2, 0x1

    aput-object v1, v0, v2

    goto/16 :goto_0

    .line 377
    :pswitch_5
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v2, 0x0

    sget v3, Lcom/peel/ui/ft;->friday:I

    invoke-virtual {p2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v0, v2

    const/4 v2, 0x1

    aput-object v1, v0, v2

    goto/16 :goto_0

    .line 379
    :pswitch_6
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v2, 0x0

    sget v3, Lcom/peel/ui/ft;->saturday:I

    invoke-virtual {p2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v0, v2

    const/4 v2, 0x1

    aput-object v1, v0, v2
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0

    .line 365
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method public static b(Landroid/content/Context;Lcom/peel/control/a;I)I
    .locals 3

    .prologue
    .line 1431
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Lcom/peel/control/a;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "japan_last_mode_idx"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public static b(Ljava/util/ArrayList;Ljava/lang/String;)I
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ")I"
        }
    .end annotation

    .prologue
    const/4 v5, 0x1

    const/4 v2, -0x1

    .line 3099
    .line 3100
    invoke-virtual {p0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v2

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 3101
    add-int/lit8 v1, v1, 0x1

    .line 3102
    if-eqz v0, :cond_0

    const-string/jumbo v4, "#"

    invoke-virtual {v0, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    array-length v4, v4

    if-le v4, v5, :cond_0

    const-string/jumbo v4, "#"

    .line 3103
    invoke-virtual {v0, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    aget-object v0, v0, v5

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3107
    :goto_0
    return v1

    :cond_1
    move v1, v2

    goto :goto_0
.end method

.method public static b(Ljava/lang/String;)Landroid/os/Bundle;
    .locals 5

    .prologue
    .line 1158
    if-nez p0, :cond_0

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 1165
    :goto_0
    return-object v0

    .line 1160
    :cond_0
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 1161
    const-string/jumbo v0, "\\|"

    invoke-static {p0, v0}, Landroid/text/TextUtils;->split(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 1162
    const/4 v0, 0x0

    :goto_1
    array-length v3, v2

    if-ge v0, v3, :cond_1

    .line 1163
    aget-object v3, v2, v0

    add-int/lit8 v4, v0, 0x1

    aget-object v4, v2, v4

    invoke-virtual {v1, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1162
    add-int/lit8 v0, v0, 0x2

    goto :goto_1

    :cond_1
    move-object v0, v1

    .line 1165
    goto :goto_0
.end method

.method public static b()Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/4 v4, 0x2

    const/4 v3, 0x1

    .line 2673
    invoke-static {v4}, Lcom/peel/util/bx;->d(I)Ljava/util/List;

    move-result-object v0

    .line 2674
    if-eqz v0, :cond_1

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    if-le v2, v3, :cond_1

    .line 2675
    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2676
    if-eqz v0, :cond_0

    const-string/jumbo v2, "/"

    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    array-length v2, v2

    if-le v2, v4, :cond_0

    .line 2681
    :goto_0
    return-object v0

    :cond_0
    move-object v0, v1

    .line 2679
    goto :goto_0

    :cond_1
    move-object v0, v1

    .line 2681
    goto :goto_0
.end method

.method public static b(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 679
    packed-switch p0, :pswitch_data_0

    .line 701
    :pswitch_0
    const-string/jumbo v0, "Error"

    :goto_0
    return-object v0

    .line 681
    :pswitch_1
    const-string/jumbo v0, "TV"

    goto :goto_0

    .line 683
    :pswitch_2
    const-string/jumbo v0, "SetTopBox"

    goto :goto_0

    .line 685
    :pswitch_3
    const-string/jumbo v0, "DVDPlayer"

    goto :goto_0

    .line 687
    :pswitch_4
    const-string/jumbo v0, "BluRayPlayer"

    goto :goto_0

    .line 691
    :pswitch_5
    const-string/jumbo v0, "AVReceiver"

    goto :goto_0

    .line 693
    :pswitch_6
    const-string/jumbo v0, "HomeProjector"

    goto :goto_0

    .line 695
    :pswitch_7
    const-string/jumbo v0, "DVR"

    goto :goto_0

    .line 697
    :pswitch_8
    const-string/jumbo v0, "StreamingMediaPlayer"

    goto :goto_0

    .line 699
    :pswitch_9
    const-string/jumbo v0, "AirConditioner"

    goto :goto_0

    .line 679
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_8
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_6
        :pswitch_0
        :pswitch_0
        :pswitch_5
        :pswitch_5
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_9
        :pswitch_0
        :pswitch_7
    .end packed-switch
.end method

.method public static b(Landroid/content/Context;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 812
    const-string/jumbo v0, "wifi"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiManager;

    .line 814
    if-eqz v0, :cond_0

    .line 815
    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->getConnectionInfo()Landroid/net/wifi/WifiInfo;

    move-result-object v0

    .line 816
    if-eqz v0, :cond_0

    .line 817
    invoke-virtual {v0}, Landroid/net/wifi/WifiInfo;->getSSID()Ljava/lang/String;

    move-result-object v0

    .line 820
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(Landroid/content/Context;I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 652
    packed-switch p1, :pswitch_data_0

    .line 674
    :pswitch_0
    sget v0, Lcom/peel/ui/ft;->error:I

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    .line 654
    :pswitch_1
    sget v0, Lcom/peel/ui/ft;->DeviceType1:I

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 656
    :pswitch_2
    sget v0, Lcom/peel/ui/ft;->DeviceType2_short:I

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 658
    :pswitch_3
    sget v0, Lcom/peel/ui/ft;->DeviceType3_short:I

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 660
    :pswitch_4
    sget v0, Lcom/peel/ui/ft;->DeviceType4_short:I

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 664
    :pswitch_5
    sget v0, Lcom/peel/ui/ft;->DeviceType5_short:I

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 666
    :pswitch_6
    sget v0, Lcom/peel/ui/ft;->DeviceType10_short:I

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 668
    :pswitch_7
    sget v0, Lcom/peel/ui/ft;->DeviceType20:I

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 670
    :pswitch_8
    sget v0, Lcom/peel/ui/ft;->DeviceType6_short:I

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 672
    :pswitch_9
    sget v0, Lcom/peel/ui/ft;->DeviceType18_short:I

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 652
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_8
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_6
        :pswitch_0
        :pswitch_0
        :pswitch_5
        :pswitch_5
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_9
        :pswitch_0
        :pswitch_7
    .end packed-switch
.end method

.method public static b(Landroid/os/Bundle;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 1169
    new-instance v1, Ljava/util/ArrayList;

    invoke-virtual {p0}, Landroid/os/Bundle;->size()I

    move-result v0

    mul-int/lit8 v0, v0, 0x2

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 1170
    invoke-virtual {p0}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1171
    invoke-virtual {p0, v0}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    instance-of v3, v3, Ljava/lang/String;

    if-eqz v3, :cond_0

    .line 1172
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1173
    invoke-virtual {p0, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1176
    :cond_1
    const-string/jumbo v0, "|"

    invoke-static {v0, v1}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static b(Lcom/peel/content/listing/Listing;)Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 539
    invoke-virtual {p0}, Lcom/peel/content/listing/Listing;->n()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "live"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 541
    invoke-virtual {p0}, Lcom/peel/content/listing/Listing;->f()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/peel/util/bx;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 568
    :goto_0
    return-object v0

    .line 542
    :cond_0
    invoke-virtual {p0}, Lcom/peel/content/listing/Listing;->n()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "dtv"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 545
    invoke-virtual {p0}, Lcom/peel/content/listing/Listing;->f()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "[/?]"

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 546
    array-length v1, v0

    const/4 v2, 0x3

    if-lt v1, v2, :cond_4

    aget-object v0, v0, v5

    goto :goto_0

    .line 547
    :cond_1
    invoke-virtual {p0}, Lcom/peel/content/listing/Listing;->n()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "nflx"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 551
    invoke-virtual {p0}, Lcom/peel/content/listing/Listing;->e()Ljava/net/URI;

    move-result-object v0

    .line 552
    if-eqz v0, :cond_4

    .line 553
    invoke-virtual {v0}, Ljava/net/URI;->getQuery()Ljava/lang/String;

    move-result-object v0

    .line 554
    const-string/jumbo v1, "epiId"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 555
    const-string/jumbo v1, "&"

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 556
    array-length v2, v1

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v2, :cond_4

    aget-object v3, v1, v0

    .line 557
    const-string/jumbo v4, "epiId"

    invoke-virtual {v3, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 558
    const-string/jumbo v0, "="

    invoke-virtual {v3, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 559
    aget-object v0, v0, v5

    goto :goto_0

    .line 556
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 563
    :cond_3
    const-string/jumbo v1, "="

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 564
    aget-object v0, v0, v5

    goto :goto_0

    .line 568
    :cond_4
    invoke-virtual {p0}, Lcom/peel/content/listing/Listing;->g()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static b(Lcom/peel/data/Channel;)Ljava/lang/String;
    .locals 6

    .prologue
    .line 3111
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/peel/data/Channel;->c()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 3112
    invoke-virtual {p0}, Lcom/peel/data/Channel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/peel/data/Channel;->c()Ljava/lang/String;

    move-result-object v1

    .line 3113
    invoke-virtual {p0}, Lcom/peel/data/Channel;->f()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lcom/peel/data/Channel;->i()I

    move-result v3

    invoke-virtual {p0}, Lcom/peel/data/Channel;->e()Ljava/lang/String;

    move-result-object v4

    .line 3114
    invoke-virtual {p0}, Lcom/peel/data/Channel;->d()Ljava/lang/String;

    move-result-object v5

    .line 3112
    invoke-static/range {v0 .. v5}, Lcom/peel/util/bx;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 3116
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static b(Landroid/content/Context;Lcom/peel/control/a;Ljava/net/URI;Lcom/peel/util/s;)V
    .locals 4

    .prologue
    .line 1754
    if-eqz p3, :cond_0

    :try_start_0
    sget-object v0, Lcom/peel/control/am;->a:Lcom/peel/control/aq;

    invoke-virtual {v0, p3}, Lcom/peel/control/aq;->b(Ljava/lang/Object;)V

    .line 1756
    :cond_0
    sget-object v0, Lcom/peel/util/bx;->a:Ljava/lang/String;

    const-string/jumbo v1, "sendCommand"

    new-instance v2, Lcom/peel/util/cg;

    invoke-direct {v2, p0, p2, p1}, Lcom/peel/util/cg;-><init>(Landroid/content/Context;Ljava/net/URI;Lcom/peel/control/a;)V

    invoke-static {v0, v1, v2}, Lcom/peel/util/i;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/lang/Runnable;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1790
    :goto_0
    return-void

    .line 1787
    :catch_0
    move-exception v0

    .line 1788
    sget-object v1, Lcom/peel/util/bx;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Exception: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static b(Landroid/content/Context;Lcom/peel/util/t;)V
    .locals 2

    .prologue
    .line 2834
    const-string/jumbo v0, "tiletitle"

    new-instance v1, Lcom/peel/util/dh;

    invoke-direct {v1, p1}, Lcom/peel/util/dh;-><init>(Lcom/peel/util/t;)V

    invoke-static {v0, p0, v1}, Lcom/peel/util/a;->a(Ljava/lang/String;Landroid/content/Context;Lcom/peel/util/t;)V

    .line 2845
    return-void
.end method

.method public static b(Landroid/content/Context;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 1360
    sget-object v0, Lcom/peel/util/bx;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "update prefs last room: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1361
    if-eqz p1, :cond_0

    const-string/jumbo v0, ""

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1362
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v1, "last_room"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 1364
    :cond_0
    return-void
.end method

.method public static b(Lcom/peel/content/listing/Listing;I)V
    .locals 1

    .prologue
    .line 2577
    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Lcom/peel/util/bx;->a(Lcom/peel/content/listing/Listing;ILjava/lang/String;)V

    .line 2578
    return-void
.end method

.method private static b(Lcom/peel/d/i;)V
    .locals 5

    .prologue
    .line 2374
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 2375
    new-instance v1, Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v2, -0x1

    invoke-direct {v1, v2}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    .line 2376
    const-string/jumbo v2, "landingpage"

    invoke-virtual {p0, v2}, Lcom/peel/d/i;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2377
    sget-object v3, Lcom/peel/util/a;->a:[Ljava/lang/String;

    const/4 v4, 0x2

    aget-object v3, v3, v4

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 2378
    sget v2, Lcom/peel/ui/fp;->menu_favorites:I

    invoke-virtual {v1, v2}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V

    .line 2379
    const-class v2, Lcom/peel/ui/bq;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2388
    :goto_0
    sget-object v2, Lcom/peel/util/bx;->a:Ljava/lang/String;

    const-string/jumbo v3, "load top"

    new-instance v4, Lcom/peel/util/cs;

    invoke-direct {v4, p0, v0, v1}, Lcom/peel/util/cs;-><init>(Lcom/peel/d/i;Ljava/lang/StringBuilder;Ljava/util/concurrent/atomic/AtomicInteger;)V

    invoke-static {v2, v3, v4}, Lcom/peel/util/i;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/lang/Runnable;

    .line 2397
    return-void

    .line 2380
    :cond_0
    sget-object v3, Lcom/peel/util/a;->a:[Ljava/lang/String;

    const/4 v4, 0x3

    aget-object v3, v3, v4

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2381
    sget v2, Lcom/peel/ui/fp;->menu_program_guide:I

    invoke-virtual {v1, v2}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V

    .line 2382
    const-class v2, Lcom/peel/ui/du;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 2384
    :cond_1
    sget v2, Lcom/peel/ui/fp;->menu_browse:I

    invoke-virtual {v1, v2}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V

    .line 2385
    const-class v2, Lcom/peel/ui/lq;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0
.end method

.method public static b(Lcom/peel/d/i;Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 2400
    if-eqz p0, :cond_0

    .line 2401
    const-string/jumbo v0, "landingpage"

    invoke-virtual {p0, v0}, Lcom/peel/d/i;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    .line 2402
    const-string/jumbo v0, "landingpage"

    new-instance v1, Lcom/peel/util/ct;

    invoke-direct {v1, p0}, Lcom/peel/util/ct;-><init>(Lcom/peel/d/i;)V

    invoke-static {v0, p1, v1}, Lcom/peel/util/a;->a(Ljava/lang/String;Landroid/content/Context;Lcom/peel/util/t;)V

    .line 2413
    :cond_0
    :goto_0
    return-void

    .line 2410
    :cond_1
    invoke-static {p0}, Lcom/peel/util/bx;->b(Lcom/peel/d/i;)V

    goto :goto_0
.end method

.method public static b(Lcom/peel/util/t;)V
    .locals 5

    .prologue
    .line 2748
    sget-object v0, Lcom/peel/content/a;->f:Lcom/peel/content/user/User;

    invoke-virtual {v0}, Lcom/peel/content/user/User;->x()Ljava/lang/String;

    move-result-object v0

    .line 2749
    sget-object v1, Lcom/peel/content/a;->e:Ljava/lang/String;

    .line 2750
    sget-object v2, Lcom/peel/util/bx;->a:Ljava/lang/String;

    const-string/jumbo v3, "get sponsored theme"

    new-instance v4, Lcom/peel/util/dd;

    invoke-direct {v4, v0, v1, p0}, Lcom/peel/util/dd;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/peel/util/t;)V

    invoke-static {v2, v3, v4}, Lcom/peel/util/i;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    .line 2797
    return-void
.end method

.method public static b(Ljava/lang/CharSequence;)Z
    .locals 1

    .prologue
    .line 135
    sget-object v0, Lcom/peel/util/bx;->c:Ljava/util/regex/Pattern;

    invoke-virtual {v0, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->matches()Z

    move-result v0

    return v0
.end method

.method static synthetic b(Z)Z
    .locals 0

    .prologue
    .line 121
    sput-boolean p0, Lcom/peel/util/bx;->r:Z

    return p0
.end method

.method public static c()Ljava/lang/String;
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 2717
    invoke-static {}, Lcom/peel/util/bx;->b()Ljava/lang/String;

    move-result-object v1

    .line 2719
    if-eqz v1, :cond_0

    .line 2720
    const-string/jumbo v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 2721
    array-length v2, v1

    const/4 v3, 0x3

    if-ge v2, v3, :cond_1

    .line 2728
    :cond_0
    :goto_0
    return-object v0

    .line 2725
    :cond_1
    const/4 v0, 0x1

    aget-object v0, v1, v0

    goto :goto_0
.end method

.method public static c(Landroid/content/Context;)Ljava/lang/String;
    .locals 7

    .prologue
    .line 1239
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 1242
    const-string/jumbo v1, "ro.csc.country_code"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1243
    if-nez v0, :cond_3

    .line 1244
    invoke-static {p0}, Lcom/peel/util/bx;->d(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    .line 1247
    :goto_0
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1249
    const-string/jumbo v0, "phone"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    .line 1250
    if-eqz v0, :cond_0

    .line 1251
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getSimCountryIso()Ljava/lang/String;

    move-result-object v1

    .line 1253
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1254
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getNetworkCountryIso()Ljava/lang/String;

    move-result-object v1

    .line 1257
    :cond_0
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1260
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    const-string/jumbo v2, "android.os.SystemProperties"

    invoke-virtual {v0, v2}, Ljava/lang/ClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    .line 1261
    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Class;

    .line 1262
    const/4 v3, 0x0

    const-class v4, Ljava/lang/String;

    aput-object v4, v2, v3

    .line 1263
    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    .line 1264
    const/4 v4, 0x0

    new-instance v5, Ljava/lang/String;

    const-string/jumbo v6, "ro.csc.countryiso_code"

    invoke-direct {v5, v6}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    aput-object v5, v3, v4

    .line 1265
    const-string/jumbo v4, "get"

    invoke-virtual {v0, v4, v2}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v2

    .line 1266
    invoke-virtual {v2, v0, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-object v1, v0

    .line 1273
    :cond_1
    :goto_1
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1274
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget-object v0, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v0}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v1

    .line 1277
    :cond_2
    return-object v1

    .line 1267
    :catch_0
    move-exception v0

    goto :goto_1

    :cond_3
    move-object v1, v0

    goto :goto_0
.end method

.method public static c(Landroid/content/Context;I)Ljava/lang/String;
    .locals 5

    .prologue
    .line 1687
    const/4 v0, 0x2

    if-ne p1, v0, :cond_0

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "ja"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1688
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/peel/ui/ft;->choose_stb_brand:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1691
    :goto_0
    return-object v0

    .line 1690
    :cond_0
    invoke-static {p0, p1}, Lcom/peel/util/bx;->a(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v0

    .line 1691
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/peel/ui/ft;->choose_device_brand:I

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static c(Lcom/peel/content/listing/Listing;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 572
    check-cast p0, Lcom/peel/content/listing/LiveListing;

    invoke-virtual {p0}, Lcom/peel/content/listing/LiveListing;->u()Ljava/lang/String;

    move-result-object v0

    .line 573
    if-nez v0, :cond_1

    const/4 v0, 0x0

    .line 587
    :cond_0
    :goto_0
    return-object v0

    .line 575
    :cond_1
    const-string/jumbo v1, "live"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 576
    const-string/jumbo v0, "live"

    goto :goto_0

    .line 579
    :cond_2
    const-string/jumbo v1, "premiere"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 580
    const-string/jumbo v0, "premiere"

    goto :goto_0

    .line 583
    :cond_3
    const-string/jumbo v1, "new"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 584
    const-string/jumbo v0, "new"

    goto :goto_0
.end method

.method public static c(I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1108
    sget-object v0, Lcom/peel/util/bx;->e:Lcom/peel/widget/ag;

    if-eqz v0, :cond_1

    if-eqz p0, :cond_1

    .line 1109
    sget-object v0, Lcom/peel/util/bx;->e:Lcom/peel/widget/ag;

    invoke-virtual {v0}, Lcom/peel/widget/ag;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1110
    sget-object v0, Lcom/peel/util/bx;->e:Lcom/peel/widget/ag;

    invoke-virtual {v0}, Lcom/peel/widget/ag;->dismiss()V

    .line 1112
    :cond_0
    sput-object v1, Lcom/peel/util/bx;->e:Lcom/peel/widget/ag;

    .line 1115
    :cond_1
    sget-object v0, Lcom/peel/util/bx;->f:Lcom/peel/widget/ag;

    if-eqz v0, :cond_3

    const/4 v0, 0x1

    if-eq p0, v0, :cond_3

    .line 1116
    sget-object v0, Lcom/peel/util/bx;->f:Lcom/peel/widget/ag;

    invoke-virtual {v0}, Lcom/peel/widget/ag;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1117
    sget-object v0, Lcom/peel/util/bx;->f:Lcom/peel/widget/ag;

    invoke-virtual {v0}, Lcom/peel/widget/ag;->dismiss()V

    .line 1119
    :cond_2
    sput-object v1, Lcom/peel/util/bx;->f:Lcom/peel/widget/ag;

    .line 1122
    :cond_3
    sget-object v0, Lcom/peel/util/bx;->g:Lcom/peel/widget/ag;

    if-eqz v0, :cond_5

    const/4 v0, 0x2

    if-eq p0, v0, :cond_5

    .line 1123
    sget-object v0, Lcom/peel/util/bx;->g:Lcom/peel/widget/ag;

    invoke-virtual {v0}, Lcom/peel/widget/ag;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1124
    sget-object v0, Lcom/peel/util/bx;->g:Lcom/peel/widget/ag;

    invoke-virtual {v0}, Lcom/peel/widget/ag;->dismiss()V

    .line 1126
    :cond_4
    sput-object v1, Lcom/peel/util/bx;->g:Lcom/peel/widget/ag;

    .line 1128
    :cond_5
    return-void
.end method

.method public static c(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 2535
    sget-object v0, Lcom/peel/util/bx;->a:Ljava/lang/String;

    const-string/jumbo v1, "getTrackingUrls"

    new-instance v2, Lcom/peel/util/cx;

    invoke-direct {v2, p0}, Lcom/peel/util/cx;-><init>(Ljava/lang/String;)V

    invoke-static {v0, v1, v2}, Lcom/peel/util/i;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    .line 2554
    return-void
.end method

.method public static c(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 1407
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 1410
    const/4 v2, 0x1

    :try_start_0
    invoke-virtual {v1, p1, v2}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1415
    :goto_0
    return v0

    .line 1412
    :catch_0
    move-exception v0

    .line 1413
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static d(Landroid/content/Context;)Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v1, 0x0

    const/4 v4, 0x1

    const/4 v6, 0x0

    .line 1985
    sget-object v0, Lcom/peel/util/bx;->t:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 1988
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    const-string/jumbo v2, "android.os.SystemProperties"

    invoke-virtual {v0, v2}, Ljava/lang/ClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 1993
    :goto_0
    new-array v2, v4, [Ljava/lang/Class;

    .line 1994
    const-class v3, Ljava/lang/String;

    aput-object v3, v2, v6

    .line 1995
    new-array v3, v4, [Ljava/lang/Object;

    .line 1996
    new-instance v4, Ljava/lang/String;

    const-string/jumbo v5, "ro.csc.country_code"

    invoke-direct {v4, v5}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    aput-object v4, v3, v6

    .line 1999
    :try_start_1
    const-string/jumbo v4, "get"

    invoke-virtual {v0, v4, v2}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
    :try_end_1
    .catch Ljava/lang/NoSuchMethodException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v1

    .line 2005
    :goto_1
    :try_start_2
    invoke-virtual {v1, v0, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    sput-object v0, Lcom/peel/util/bx;->t:Ljava/lang/String;
    :try_end_2
    .catch Ljava/lang/IllegalAccessException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_3
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_2 .. :try_end_2} :catch_4

    .line 2018
    :cond_0
    :goto_2
    sget-object v0, Lcom/peel/util/bx;->t:Ljava/lang/String;

    return-object v0

    .line 1989
    :catch_0
    move-exception v0

    .line 1991
    invoke-virtual {v0}, Ljava/lang/ClassNotFoundException;->printStackTrace()V

    move-object v0, v1

    goto :goto_0

    .line 2000
    :catch_1
    move-exception v2

    .line 2002
    invoke-virtual {v2}, Ljava/lang/NoSuchMethodException;->printStackTrace()V

    goto :goto_1

    .line 2006
    :catch_2
    move-exception v0

    .line 2008
    invoke-virtual {v0}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    goto :goto_2

    .line 2009
    :catch_3
    move-exception v0

    .line 2011
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_2

    .line 2012
    :catch_4
    move-exception v0

    .line 2014
    invoke-virtual {v0}, Ljava/lang/reflect/InvocationTargetException;->printStackTrace()V

    goto :goto_2
.end method

.method public static d(Landroid/content/Context;I)Ljava/lang/String;
    .locals 2

    .prologue
    .line 2493
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget-object v0, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v0}, Ljava/util/Locale;->getISO3Language()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "jpn"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2494
    packed-switch p1, :pswitch_data_0

    .line 2516
    :pswitch_0
    sget v0, Lcom/peel/ui/ft;->error:I

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 2519
    :goto_0
    return-object v0

    .line 2496
    :pswitch_1
    sget v0, Lcom/peel/ui/ft;->DeviceType1_half:I

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2498
    :pswitch_2
    sget v0, Lcom/peel/ui/ft;->DeviceType2_half:I

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2500
    :pswitch_3
    sget v0, Lcom/peel/ui/ft;->DeviceType3_half:I

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2502
    :pswitch_4
    sget v0, Lcom/peel/ui/ft;->DeviceType4_half:I

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2506
    :pswitch_5
    sget v0, Lcom/peel/ui/ft;->DeviceType5_half:I

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2508
    :pswitch_6
    sget v0, Lcom/peel/ui/ft;->DeviceType10_half:I

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2510
    :pswitch_7
    sget v0, Lcom/peel/ui/ft;->DeviceType2_half:I

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2512
    :pswitch_8
    sget v0, Lcom/peel/ui/ft;->DeviceType6_half:I

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2514
    :pswitch_9
    sget v0, Lcom/peel/ui/ft;->DeviceType18_half:I

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2519
    :cond_0
    invoke-static {p0, p1}, Lcom/peel/util/bx;->a(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2494
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_8
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_6
        :pswitch_0
        :pswitch_0
        :pswitch_5
        :pswitch_5
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_9
        :pswitch_0
        :pswitch_7
    .end packed-switch
.end method

.method public static d(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 1632
    const-string/jumbo v0, "T"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1633
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/peel/ui/ft;->terrestrial:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    .line 1635
    :cond_0
    return-object p1
.end method

.method public static d(Lcom/peel/content/listing/Listing;)Ljava/lang/String;
    .locals 8

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    const/4 v1, 0x0

    .line 2061
    if-eqz p0, :cond_4

    move-object v0, p0

    .line 2062
    check-cast v0, Lcom/peel/content/listing/LiveListing;

    .line 2063
    invoke-virtual {v0}, Lcom/peel/content/listing/LiveListing;->o()J

    move-result-wide v4

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    cmp-long v0, v4, v6

    if-gez v0, :cond_0

    move v0, v2

    .line 2065
    :goto_0
    if-eqz v0, :cond_1

    move-object v0, v1

    .line 2091
    :goto_1
    return-object v0

    :cond_0
    move v0, v3

    .line 2063
    goto :goto_0

    .line 2067
    :cond_1
    sget-object v0, Lcom/peel/content/a;->f:Lcom/peel/content/user/User;

    invoke-virtual {v0}, Lcom/peel/content/user/User;->r()Ljava/util/Map;

    move-result-object v0

    .line 2069
    if-nez v0, :cond_2

    move-object v0, v1

    .line 2070
    goto :goto_1

    .line 2073
    :cond_2
    invoke-static {p0}, Lcom/peel/util/bx;->b(Lcom/peel/content/listing/Listing;)Ljava/lang/String;

    move-result-object v4

    .line 2075
    if-eqz v4, :cond_4

    .line 2076
    invoke-interface {v0, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2078
    if-eqz v0, :cond_4

    .line 2079
    const-string/jumbo v4, "!"

    invoke-virtual {v0, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 2081
    array-length v4, v0

    const/4 v5, 0x2

    if-ge v4, v5, :cond_3

    move-object v0, v1

    .line 2082
    goto :goto_1

    .line 2083
    :cond_3
    check-cast p0, Lcom/peel/content/listing/LiveListing;

    invoke-virtual {p0}, Lcom/peel/content/listing/LiveListing;->r()Ljava/lang/String;

    move-result-object v4

    aget-object v3, v0, v3

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 2084
    aget-object v0, v0, v2

    goto :goto_1

    :cond_4
    move-object v0, v1

    .line 2091
    goto :goto_1
.end method

.method public static d(I)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2697
    const-string/jumbo v0, "live"

    invoke-static {v0}, Lcom/peel/content/a;->c(Ljava/lang/String;)Lcom/peel/content/library/Library;

    move-result-object v0

    .line 2698
    if-nez v0, :cond_0

    .line 2699
    const/4 v0, 0x0

    .line 2701
    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/peel/content/a;->a()Lcom/peel/data/ContentRoom;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/data/ContentRoom;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lcom/peel/content/library/Library;->e()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v1, v0}, Lcom/peel/util/bx;->a(ILjava/lang/String;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method

.method public static d(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 2561
    sget-object v0, Lcom/peel/util/bx;->w:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->size()I

    move-result v0

    if-nez v0, :cond_1

    .line 2574
    :cond_0
    :goto_0
    return-void

    .line 2562
    :cond_1
    sget-object v0, Lcom/peel/util/bx;->w:Ljava/util/HashMap;

    invoke-virtual {v0, p0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2563
    sget-object v1, Lcom/peel/util/bx;->a:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "ping tracking url: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v0, Lcom/peel/util/bx;->w:Ljava/util/HashMap;

    invoke-virtual {v0, p0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v2, Lcom/peel/util/cz;

    invoke-direct {v2, p0}, Lcom/peel/util/cz;-><init>(Ljava/lang/String;)V

    invoke-static {v1, v0, v2}, Lcom/peel/util/i;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    goto :goto_0
.end method

.method static synthetic d()Z
    .locals 1

    .prologue
    .line 121
    sget-boolean v0, Lcom/peel/util/bx;->q:Z

    return v0
.end method

.method public static e(I)Ljava/lang/String;
    .locals 9

    .prologue
    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 2913
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 2914
    new-instance v1, Ljava/util/Formatter;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-direct {v1, v0, v2}, Ljava/util/Formatter;-><init>(Ljava/lang/Appendable;Ljava/util/Locale;)V

    .line 2915
    div-int/lit16 v2, p0, 0x3e8

    .line 2917
    rem-int/lit8 v3, v2, 0x3c

    .line 2918
    div-int/lit8 v4, v2, 0x3c

    rem-int/lit8 v4, v4, 0x3c

    .line 2919
    div-int/lit16 v2, v2, 0xe10

    .line 2921
    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 2922
    if-lez v2, :cond_0

    .line 2923
    const-string/jumbo v0, "%d:%02d:%02d"

    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v5, v6

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v5, v7

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v5, v8

    invoke-virtual {v1, v0, v5}, Ljava/util/Formatter;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/util/Formatter;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Formatter;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2925
    :goto_0
    return-object v0

    :cond_0
    const-string/jumbo v0, "%02d:%02d"

    new-array v2, v8, [Ljava/lang/Object;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v6

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v7

    invoke-virtual {v1, v0, v2}, Ljava/util/Formatter;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/util/Formatter;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Formatter;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static e(Landroid/content/Context;)Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v1, 0x0

    const/4 v4, 0x1

    const/4 v6, 0x0

    .line 2024
    sget-object v0, Lcom/peel/util/bx;->u:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 2027
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    const-string/jumbo v2, "android.os.SystemProperties"

    invoke-virtual {v0, v2}, Ljava/lang/ClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 2032
    :goto_0
    new-array v2, v4, [Ljava/lang/Class;

    .line 2033
    const-class v3, Ljava/lang/String;

    aput-object v3, v2, v6

    .line 2034
    new-array v3, v4, [Ljava/lang/Object;

    .line 2035
    new-instance v4, Ljava/lang/String;

    const-string/jumbo v5, "ro.product.locale.region"

    invoke-direct {v4, v5}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    aput-object v4, v3, v6

    .line 2038
    :try_start_1
    const-string/jumbo v4, "get"

    invoke-virtual {v0, v4, v2}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
    :try_end_1
    .catch Ljava/lang/NoSuchMethodException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v1

    .line 2044
    :goto_1
    :try_start_2
    invoke-virtual {v1, v0, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    sput-object v0, Lcom/peel/util/bx;->u:Ljava/lang/String;
    :try_end_2
    .catch Ljava/lang/IllegalAccessException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_3
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_2 .. :try_end_2} :catch_4

    .line 2057
    :cond_0
    :goto_2
    sget-object v0, Lcom/peel/util/bx;->u:Ljava/lang/String;

    return-object v0

    .line 2028
    :catch_0
    move-exception v0

    .line 2030
    invoke-virtual {v0}, Ljava/lang/ClassNotFoundException;->printStackTrace()V

    move-object v0, v1

    goto :goto_0

    .line 2039
    :catch_1
    move-exception v2

    .line 2041
    invoke-virtual {v2}, Ljava/lang/NoSuchMethodException;->printStackTrace()V

    goto :goto_1

    .line 2045
    :catch_2
    move-exception v0

    .line 2047
    invoke-virtual {v0}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    goto :goto_2

    .line 2048
    :catch_3
    move-exception v0

    .line 2050
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_2

    .line 2051
    :catch_4
    move-exception v0

    .line 2053
    invoke-virtual {v0}, Ljava/lang/reflect/InvocationTargetException;->printStackTrace()V

    goto :goto_2
.end method

.method public static e(Ljava/lang/String;)Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v3, 0x1

    .line 2850
    :try_start_0
    new-instance v0, Ljava/net/URI;

    invoke-direct {v0, p0}, Ljava/net/URI;-><init>(Ljava/lang/String;)V

    .line 2853
    invoke-virtual {v0}, Ljava/net/URI;->getQuery()Ljava/lang/String;

    move-result-object v1

    .line 2854
    const/4 v0, 0x0

    .line 2855
    if-eqz v1, :cond_0

    .line 2856
    const-string/jumbo v2, "&"

    invoke-virtual {v1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 2857
    if-eqz v1, :cond_0

    .line 2858
    array-length v2, v1

    if-ne v3, v2, :cond_1

    .line 2859
    const/4 v0, 0x0

    aget-object v0, v1, v0

    const/4 v2, 0x0

    aget-object v1, v1, v2

    const-string/jumbo v2, "="

    invoke-virtual {v1, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 2868
    :cond_0
    :goto_0
    if-eqz v0, :cond_2

    .line 2870
    const-string/jumbo v1, "http://api.netflix.com/catalog/titles/movies/"

    const-string/jumbo v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "/action=play"

    const-string/jumbo v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2871
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "nflx://www.netflix.com/Browse?q="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "action=play&source=myapp&movieid=http://api.netflix.com/catalog/titles/movies/"

    invoke-static {v2}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    .line 2880
    :goto_1
    return-object p0

    .line 2860
    :cond_1
    const/4 v2, 0x2

    array-length v3, v1

    if-ne v2, v3, :cond_0

    .line 2861
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v2, 0x1

    aget-object v2, v1, v2

    const/4 v3, 0x1

    aget-object v3, v1, v3

    const-string/jumbo v4, "="

    invoke-virtual {v3, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    invoke-virtual {v2, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v2, "/"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/4 v2, 0x0

    aget-object v2, v1, v2

    const/4 v3, 0x0

    aget-object v1, v1, v3

    const-string/jumbo v3, "="

    invoke-virtual {v1, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v2, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2874
    :cond_2
    sget-object v0, Lcom/peel/util/bx;->a:Ljava/lang/String;

    const-string/jumbo v1, "movieid == null"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/net/URISyntaxException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 2876
    :catch_0
    move-exception v0

    .line 2877
    invoke-virtual {v0}, Ljava/net/URISyntaxException;->printStackTrace()V

    goto :goto_1
.end method

.method public static e(Landroid/content/Context;Ljava/lang/String;)V
    .locals 9

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 1700
    :try_start_0
    sget-object v2, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v2}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v3

    .line 1701
    if-eqz v3, :cond_2

    .line 1703
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "live://channel/"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1704
    invoke-static {v2}, Ljava/net/URI;->create(Ljava/lang/String;)Ljava/net/URI;

    move-result-object v4

    .line 1705
    invoke-virtual {v4}, Ljava/net/URI;->getScheme()Ljava/lang/String;

    move-result-object v5

    .line 1708
    invoke-virtual {v3}, Lcom/peel/control/RoomControl;->c()[Lcom/peel/control/a;

    move-result-object v6

    array-length v7, v6

    move v2, v0

    :goto_0
    if-ge v2, v7, :cond_5

    aget-object v0, v6, v2

    .line 1709
    invoke-virtual {v0}, Lcom/peel/control/a;->d()[Ljava/lang/String;

    move-result-object v8

    if-nez v8, :cond_1

    .line 1708
    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 1710
    :cond_1
    invoke-virtual {v0}, Lcom/peel/control/a;->d()[Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v8

    invoke-interface {v8, v5}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 1716
    :goto_1
    if-nez v0, :cond_3

    .line 1717
    sget-object v0, Lcom/peel/util/bx;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "no activity in room for scheme: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1748
    :cond_2
    :goto_2
    return-void

    .line 1721
    :cond_3
    const/4 v1, 0x2

    invoke-virtual {v0}, Lcom/peel/control/a;->f()I

    move-result v2

    if-ne v1, v2, :cond_4

    .line 1722
    const/4 v1, 0x0

    invoke-static {p0, v0, v4, v1}, Lcom/peel/util/bx;->b(Landroid/content/Context;Lcom/peel/control/a;Ljava/net/URI;Lcom/peel/util/s;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    .line 1744
    :catch_0
    move-exception v0

    .line 1745
    sget-object v1, Lcom/peel/util/bx;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Exception: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 1729
    :cond_4
    :try_start_1
    sget-object v1, Lcom/peel/control/am;->a:Lcom/peel/control/aq;

    new-instance v2, Lcom/peel/util/cf;

    invoke-direct {v2, p0, v4}, Lcom/peel/util/cf;-><init>(Landroid/content/Context;Ljava/net/URI;)V

    invoke-virtual {v1, v2}, Lcom/peel/control/aq;->a(Ljava/lang/Object;)V

    .line 1740
    const/4 v1, 0x0

    invoke-virtual {v3, v1}, Lcom/peel/control/RoomControl;->a(I)Z

    .line 1741
    const/4 v1, 0x1

    invoke-virtual {v3, v0, v1}, Lcom/peel/control/RoomControl;->a(Lcom/peel/control/a;I)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2

    :cond_5
    move-object v0, v1

    goto :goto_1
.end method

.method static synthetic e()Z
    .locals 1

    .prologue
    .line 121
    sget-boolean v0, Lcom/peel/util/bx;->r:Z

    return v0
.end method

.method public static e(Lcom/peel/content/listing/Listing;)Z
    .locals 10

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2125
    if-eqz p0, :cond_8

    move-object v0, p0

    .line 2126
    check-cast v0, Lcom/peel/content/listing/LiveListing;

    .line 2127
    invoke-virtual {v0}, Lcom/peel/content/listing/LiveListing;->b()Ljava/lang/String;

    move-result-object v1

    .line 2128
    invoke-virtual {p0}, Lcom/peel/content/listing/Listing;->k()Ljava/lang/String;

    move-result-object v4

    const-string/jumbo v5, "sports"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 2129
    sget-object v1, Lcom/peel/content/a;->f:Lcom/peel/content/user/User;

    invoke-virtual {v1}, Lcom/peel/content/user/User;->p()Ljava/util/Map;

    move-result-object v1

    .line 2130
    invoke-virtual {v0}, Lcom/peel/content/listing/LiveListing;->s()[Lcom/peel/data/SportsTeam;

    move-result-object v4

    .line 2132
    if-eqz v1, :cond_5

    if-eqz v4, :cond_5

    array-length v5, v4

    const/4 v6, 0x2

    if-ne v5, v6, :cond_5

    .line 2133
    invoke-virtual {v0}, Lcom/peel/content/listing/LiveListing;->t()Z

    move-result v5

    if-eqz v5, :cond_0

    move v0, v2

    .line 2166
    :goto_0
    return v0

    .line 2136
    :cond_0
    aget-object v5, v4, v2

    invoke-virtual {v5}, Lcom/peel/data/SportsTeam;->a()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v1, v5}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_1

    aget-object v4, v4, v3

    invoke-virtual {v4}, Lcom/peel/data/SportsTeam;->a()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v1, v4}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    :cond_1
    move v0, v3

    .line 2137
    goto :goto_0

    .line 2142
    :cond_2
    sget-object v4, Lcom/peel/content/a;->f:Lcom/peel/content/user/User;

    invoke-virtual {v4}, Lcom/peel/content/user/User;->q()Ljava/util/Map;

    move-result-object v4

    .line 2144
    if-eqz v4, :cond_5

    if-eqz v1, :cond_5

    invoke-interface {v4, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 2145
    invoke-interface {v4, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    const-string/jumbo v4, "new"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 2146
    invoke-virtual {v0}, Lcom/peel/content/listing/LiveListing;->u()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_3

    invoke-virtual {v0}, Lcom/peel/content/listing/LiveListing;->u()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "new"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    move v0, v3

    .line 2147
    goto :goto_0

    :cond_3
    move v0, v2

    .line 2149
    goto :goto_0

    :cond_4
    move v0, v3

    .line 2152
    goto :goto_0

    .line 2157
    :cond_5
    sget-object v1, Lcom/peel/content/a;->f:Lcom/peel/content/user/User;

    invoke-virtual {v1}, Lcom/peel/content/user/User;->r()Ljava/util/Map;

    move-result-object v4

    .line 2158
    invoke-virtual {v0}, Lcom/peel/content/listing/LiveListing;->o()J

    move-result-wide v6

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    cmp-long v1, v6, v8

    if-gez v1, :cond_6

    move v1, v3

    .line 2160
    :goto_1
    if-eqz v1, :cond_7

    move v0, v2

    goto :goto_0

    :cond_6
    move v1, v2

    .line 2158
    goto :goto_1

    .line 2161
    :cond_7
    if-eqz v4, :cond_8

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {p0}, Lcom/peel/util/bx;->b(Lcom/peel/content/listing/Listing;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v5, "/"

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Lcom/peel/content/listing/LiveListing;->r()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v4, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    move v0, v3

    .line 2162
    goto/16 :goto_0

    :cond_8
    move v0, v2

    .line 2166
    goto/16 :goto_0
.end method

.method public static f(Landroid/content/Context;)Ljava/lang/String;
    .locals 6

    .prologue
    .line 2097
    sget-object v0, Lcom/peel/util/bx;->v:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 2098
    const-string/jumbo v0, "phone"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    .line 2099
    if-eqz v0, :cond_0

    .line 2100
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getSimCountryIso()Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/peel/util/bx;->v:Ljava/lang/String;

    .line 2102
    sget-object v1, Lcom/peel/util/bx;->v:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2103
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getNetworkCountryIso()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/peel/util/bx;->v:Ljava/lang/String;

    .line 2106
    :cond_0
    sget-object v0, Lcom/peel/util/bx;->v:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2109
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    const-string/jumbo v1, "android.os.SystemProperties"

    invoke-virtual {v0, v1}, Ljava/lang/ClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    .line 2110
    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Class;

    .line 2111
    const/4 v2, 0x0

    const-class v3, Ljava/lang/String;

    aput-object v3, v1, v2

    .line 2112
    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    .line 2113
    const/4 v3, 0x0

    new-instance v4, Ljava/lang/String;

    const-string/jumbo v5, "ro.csc.countryiso_code"

    invoke-direct {v4, v5}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    aput-object v4, v2, v3

    .line 2114
    const-string/jumbo v3, "get"

    invoke-virtual {v0, v3, v1}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    .line 2115
    invoke-virtual {v1, v0, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    sput-object v0, Lcom/peel/util/bx;->v:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2121
    :cond_1
    :goto_0
    sget-object v0, Lcom/peel/util/bx;->v:Ljava/lang/String;

    return-object v0

    .line 2116
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method static synthetic f()Lorg/codehaus/jackson/map/ObjectMapper;
    .locals 1

    .prologue
    .line 121
    sget-object v0, Lcom/peel/util/bx;->s:Lorg/codehaus/jackson/map/ObjectMapper;

    return-object v0
.end method

.method public static g(Landroid/content/Context;)Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 2171
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 2173
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v1

    .line 2174
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 2175
    const-string/jumbo v3, "Version "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v1, v1, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "\nCopyright "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "&copy;"

    .line 2177
    invoke-static {v4}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "2010-"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const/4 v4, 0x1

    invoke-virtual {v0, v4}, Ljava/util/Calendar;->get(I)I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, ",\n Peel Technologies Inc.\nAll Rights Reserved."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2178
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 2182
    :goto_0
    return-object v0

    .line 2179
    :catch_0
    move-exception v1

    .line 2180
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "&copy;"

    invoke-static {v2}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "2010"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0, v5}, Ljava/util/Calendar;->get(I)I

    move-result v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "\nPeel Technologies Inc.\nAll Rights Reserved."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method static synthetic g()Ljava/util/HashMap;
    .locals 1

    .prologue
    .line 121
    sget-object v0, Lcom/peel/util/bx;->w:Ljava/util/HashMap;

    return-object v0
.end method

.method public static h(Landroid/content/Context;)Z
    .locals 3

    .prologue
    .line 2935
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 2936
    const-string/jumbo v1, "com.osp.app.signin"

    const-string/jumbo v2, "com.osp.app.signin.AccountView"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2937
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    const/high16 v2, 0x10000

    invoke-virtual {v1, v0, v2}, Landroid/content/pm/PackageManager;->resolveActivity(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static i(Landroid/content/Context;)V
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 2945
    .line 2946
    const-string/jumbo v4, ""

    const-string/jumbo v6, ""

    .line 2950
    :try_start_0
    invoke-static {p0}, Lcom/google/android/gms/ads/identifier/AdvertisingIdClient;->a(Landroid/content/Context;)Lcom/google/android/gms/ads/identifier/AdvertisingIdClient$Info;

    move-result-object v0

    .line 2951
    invoke-virtual {v0}, Lcom/google/android/gms/ads/identifier/AdvertisingIdClient$Info;->a()Ljava/lang/String;

    move-result-object v4

    .line 2952
    invoke-virtual {v0}, Lcom/google/android/gms/ads/identifier/AdvertisingIdClient$Info;->b()Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/google/android/gms/common/GooglePlayServicesNotAvailableException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/google/android/gms/common/GooglePlayServicesRepairableException; {:try_start_0 .. :try_end_0} :catch_2

    move-result v0

    move v5, v0

    .line 2964
    :goto_0
    invoke-static {}, Lcom/peel/util/a/f;->a()Lcom/peel/util/a/f;

    move-result-object v0

    sget-object v1, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    .line 2965
    invoke-virtual {v1}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v1

    if-nez v1, :cond_0

    move v1, v8

    :goto_1
    const/16 v2, 0x803

    const/16 v3, 0x3e8

    if-eqz v5, :cond_1

    move v5, v8

    :goto_2
    const/4 v7, -0x1

    .line 2964
    invoke-virtual/range {v0 .. v7}, Lcom/peel/util/a/f;->a(IIILjava/lang/String;ILjava/lang/String;I)V

    .line 2967
    return-void

    .line 2953
    :catch_0
    move-exception v0

    .line 2956
    const-string/jumbo v6, "cannot connect to google play services"

    move v5, v7

    .line 2962
    goto :goto_0

    .line 2957
    :catch_1
    move-exception v0

    .line 2959
    const-string/jumbo v6, "google play services not available"

    move v5, v7

    .line 2962
    goto :goto_0

    .line 2960
    :catch_2
    move-exception v0

    .line 2961
    const-string/jumbo v6, "google play services error"

    move v5, v7

    goto :goto_0

    .line 2965
    :cond_0
    sget-object v1, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v1}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/data/at;->f()I

    move-result v1

    goto :goto_1

    :cond_1
    move v5, v7

    goto :goto_2
.end method

.method public static j(Landroid/content/Context;)I
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 3001
    .line 3003
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v1

    iget v0, v1, Landroid/content/pm/PackageInfo;->versionCode:I
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 3007
    :goto_0
    return v0

    .line 3004
    :catch_0
    move-exception v1

    goto :goto_0
.end method
