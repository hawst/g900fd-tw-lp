.class Lcom/peel/util/cy;
.super Lcom/peel/util/t;


# instance fields
.field final synthetic a:Lcom/peel/util/cx;


# direct methods
.method constructor <init>(Lcom/peel/util/cx;)V
    .locals 0

    .prologue
    .line 2537
    iput-object p1, p0, Lcom/peel/util/cy;->a:Lcom/peel/util/cx;

    invoke-direct {p0}, Lcom/peel/util/t;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    .line 2539
    iget-boolean v0, p0, Lcom/peel/util/cy;->i:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/peel/util/cy;->j:Ljava/lang/Object;

    if-nez v0, :cond_1

    .line 2550
    :cond_0
    :goto_0
    return-void

    .line 2543
    :cond_1
    :try_start_0
    new-instance v1, Lorg/json/JSONObject;

    iget-object v0, p0, Lcom/peel/util/cy;->j:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    invoke-direct {v1, v0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    const-string/jumbo v0, "tracking_urls"

    invoke-virtual {v1, v0}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v1

    .line 2544
    const/4 v0, 0x0

    :goto_1
    invoke-virtual {v1}, Lorg/json/JSONArray;->length()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 2545
    invoke-static {}, Lcom/peel/util/bx;->g()Ljava/util/HashMap;

    move-result-object v2

    invoke-virtual {v1, v0}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v3

    const-string/jumbo v4, "showid"

    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v0}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v4

    const-string/jumbo v5, "url"

    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2544
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 2547
    :catch_0
    move-exception v0

    .line 2548
    sget-object v1, Lcom/peel/util/bx;->a:Ljava/lang/String;

    sget-object v2, Lcom/peel/util/bx;->a:Ljava/lang/String;

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method
