.class final Lcom/peel/util/dj;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic a:Landroid/widget/CheckBox;

.field final synthetic b:Landroid/content/SharedPreferences;

.field final synthetic c:I


# direct methods
.method constructor <init>(Landroid/widget/CheckBox;Landroid/content/SharedPreferences;I)V
    .locals 0

    .prologue
    .line 965
    iput-object p1, p0, Lcom/peel/util/dj;->a:Landroid/widget/CheckBox;

    iput-object p2, p0, Lcom/peel/util/dj;->b:Landroid/content/SharedPreferences;

    iput p3, p0, Lcom/peel/util/dj;->c:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    .line 967
    invoke-static {}, Lcom/peel/util/a/f;->a()Lcom/peel/util/a/f;

    move-result-object v2

    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v0}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v0

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    const/16 v3, 0x517

    const/16 v4, 0x7db

    invoke-virtual {v2, v0, v3, v4}, Lcom/peel/util/a/f;->a(III)V

    .line 969
    const/4 v0, 0x0

    sput-boolean v0, Lcom/peel/util/b/a;->a:Z

    .line 970
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 971
    iget-object v0, p0, Lcom/peel/util/dj;->a:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 972
    iget-object v0, p0, Lcom/peel/util/dj;->b:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    iget v0, p0, Lcom/peel/util/dj;->c:I

    if-nez v0, :cond_2

    const-string/jumbo v0, "network_dialog"

    :goto_1
    invoke-static {}, Lcom/peel/util/bx;->d()Z

    move-result v3

    invoke-interface {v2, v0, v3}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 973
    iget-object v0, p0, Lcom/peel/util/dj;->b:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    iget v0, p0, Lcom/peel/util/dj;->c:I

    if-nez v0, :cond_3

    const-string/jumbo v0, "mobile_network"

    :goto_2
    invoke-interface {v2, v0, v1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 975
    :cond_0
    return-void

    .line 967
    :cond_1
    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v0}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/data/at;->f()I

    move-result v0

    goto :goto_0

    .line 972
    :cond_2
    const-string/jumbo v0, "roaming_dialog"

    goto :goto_1

    .line 973
    :cond_3
    const-string/jumbo v0, "roaming_network"

    goto :goto_2
.end method
