.class Lcom/peel/util/aw;
.super Lcom/peel/util/t;


# instance fields
.field final synthetic a:Lcom/peel/util/av;


# direct methods
.method constructor <init>(Lcom/peel/util/av;)V
    .locals 0

    .prologue
    .line 181
    iput-object p1, p0, Lcom/peel/util/aw;->a:Lcom/peel/util/av;

    invoke-direct {p0}, Lcom/peel/util/t;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    const/4 v3, 0x1

    .line 183
    iget-boolean v0, p0, Lcom/peel/util/aw;->i:Z

    if-ne v3, v0, :cond_1

    .line 185
    invoke-static {}, Lcom/peel/util/au;->a()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "userid "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/peel/util/aw;->a:Lcom/peel/util/av;

    iget-object v2, v2, Lcom/peel/util/av;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " regid "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/peel/util/aw;->a:Lcom/peel/util/av;

    iget-object v2, v2, Lcom/peel/util/av;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 186
    iget-object v0, p0, Lcom/peel/util/aw;->a:Lcom/peel/util/av;

    iget-object v0, v0, Lcom/peel/util/av;->a:Ljava/lang/String;

    iget-object v1, p0, Lcom/peel/util/aw;->a:Lcom/peel/util/av;

    iget-object v1, v1, Lcom/peel/util/av;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 187
    iget-object v0, p0, Lcom/peel/util/aw;->a:Lcom/peel/util/av;

    iget-object v0, v0, Lcom/peel/util/av;->e:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v1, "registration_to_peel"

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 198
    :cond_0
    :goto_0
    return-void

    .line 190
    :cond_1
    invoke-static {}, Lcom/peel/util/au;->a()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "failed to register"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 192
    :try_start_0
    invoke-static {}, Lcom/peel/util/a/f;->a()Lcom/peel/util/a/f;

    move-result-object v0

    sget-object v1, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v1}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/data/at;->f()I

    move-result v1

    const/16 v2, 0x1518

    const/16 v3, 0x7e8

    const-string/jumbo v4, "GCM register failed"

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/peel/util/a/f;->a(IIILjava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 193
    :catch_0
    move-exception v0

    goto :goto_0
.end method
