.class public abstract Lcom/peel/util/t;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/lang/Runnable;"
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field public h:I

.field public i:Z

.field public j:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field public k:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 237
    const-class v0, Lcom/peel/util/t;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/peel/util/t;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 244
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/peel/util/t;->h:I

    return-void
.end method

.method public constructor <init>(I)V
    .locals 0

    .prologue
    .line 246
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/peel/util/t;->h:I

    return-void
.end method

.method static synthetic a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 232
    sget-object v0, Lcom/peel/util/t;->a:Ljava/lang/String;

    return-object v0
.end method

.method private b()V
    .locals 6

    .prologue
    .line 253
    .line 254
    new-instance v0, Lcom/peel/util/u;

    invoke-direct {v0, p0, p0}, Lcom/peel/util/u;-><init>(Lcom/peel/util/t;Lcom/peel/util/t;)V

    .line 264
    iget v1, p0, Lcom/peel/util/t;->h:I

    packed-switch v1, :pswitch_data_0

    .line 267
    sget-object v1, Lcom/peel/util/t;->a:Ljava/lang/String;

    sget-object v2, Lcom/peel/util/t;->a:Ljava/lang/String;

    new-instance v3, Ljava/lang/RuntimeException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "UNRECOGNIZED OnComplete mode "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/peel/util/t;->h:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, " posting to NUI"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    invoke-static {v1, v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 270
    :pswitch_0
    sget-object v1, Lcom/peel/util/t;->a:Ljava/lang/String;

    const-string/jumbo v2, "OnComplete"

    invoke-static {v1, v2, v0}, Lcom/peel/util/i;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/lang/Runnable;

    .line 281
    :goto_0
    return-void

    .line 274
    :pswitch_1
    sget-object v1, Lcom/peel/util/t;->a:Ljava/lang/String;

    const-string/jumbo v2, "OnComplete"

    invoke-static {v1, v2, v0}, Lcom/peel/util/i;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/lang/Runnable;

    goto :goto_0

    .line 278
    :pswitch_2
    sget-object v1, Lcom/peel/util/t;->a:Ljava/lang/String;

    const-string/jumbo v2, "OnComplete"

    invoke-static {v1, v2, v0}, Lcom/peel/util/i;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    goto :goto_0

    .line 264
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public a(ZLjava/lang/Object;Ljava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ZTT;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 284
    iput-boolean p1, p0, Lcom/peel/util/t;->i:Z

    .line 285
    iput-object p2, p0, Lcom/peel/util/t;->j:Ljava/lang/Object;

    .line 286
    iput-object p3, p0, Lcom/peel/util/t;->k:Ljava/lang/String;

    .line 287
    invoke-direct {p0}, Lcom/peel/util/t;->b()V

    .line 288
    return-void
.end method

.method public run()V
    .locals 2

    .prologue
    .line 249
    sget-object v0, Lcom/peel/util/t;->a:Ljava/lang/String;

    const-string/jumbo v1, "run not implemented!"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 250
    return-void
.end method
