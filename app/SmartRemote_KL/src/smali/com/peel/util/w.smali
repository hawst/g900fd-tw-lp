.class Lcom/peel/util/w;
.super Lcom/peel/util/t;


# instance fields
.field final synthetic a:Lcom/peel/content/library/LiveLibrary;

.field final synthetic b:Lcom/peel/util/v;


# direct methods
.method constructor <init>(Lcom/peel/util/v;Lcom/peel/content/library/LiveLibrary;)V
    .locals 0

    .prologue
    .line 56
    iput-object p1, p0, Lcom/peel/util/w;->b:Lcom/peel/util/v;

    iput-object p2, p0, Lcom/peel/util/w;->a:Lcom/peel/content/library/LiveLibrary;

    invoke-direct {p0}, Lcom/peel/util/t;-><init>()V

    return-void
.end method


# virtual methods
.method public a(ZLjava/lang/Object;Ljava/lang/String;)V
    .locals 8

    .prologue
    .line 60
    if-eqz p1, :cond_1

    .line 61
    sget-object v0, Lcom/peel/c/a;->a:Lcom/peel/c/e;

    invoke-static {v0}, Lcom/peel/c/a;->a(Lcom/peel/c/e;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    const-string/jumbo v1, "widget_pref"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 62
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v1, "playing_status_notification"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 63
    sget-object v0, Lcom/peel/c/a;->a:Lcom/peel/c/e;

    invoke-static {v0}, Lcom/peel/c/a;->a(Lcom/peel/c/e;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-static {v0}, Lcom/peel/util/bq;->a(Landroid/content/Context;)V

    .line 64
    iget-object v0, p0, Lcom/peel/util/w;->a:Lcom/peel/content/library/LiveLibrary;

    invoke-virtual {v0}, Lcom/peel/content/library/LiveLibrary;->o()Lcom/peel/content/listing/Listing;

    move-result-object v0

    check-cast v0, Lcom/peel/content/listing/LiveListing;

    .line 65
    if-eqz v0, :cond_1

    .line 66
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 67
    const-wide/32 v4, 0x493e0

    add-long/2addr v4, v2

    const-wide/32 v6, 0xea60

    rem-long/2addr v2, v6

    sub-long v2, v4, v2

    .line 68
    invoke-virtual {v0}, Lcom/peel/content/listing/LiveListing;->o()J

    move-result-wide v4

    cmp-long v1, v2, v4

    if-ltz v1, :cond_0

    .line 69
    invoke-virtual {v0}, Lcom/peel/content/listing/LiveListing;->o()J

    move-result-wide v2

    .line 71
    :cond_0
    iget-object v1, p0, Lcom/peel/util/w;->b:Lcom/peel/util/v;

    invoke-virtual {v0}, Lcom/peel/content/listing/LiveListing;->c()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v2, v3, v0}, Lcom/peel/util/v;->a(Lcom/peel/util/v;JLjava/lang/String;)V

    .line 74
    :cond_1
    return-void
.end method
