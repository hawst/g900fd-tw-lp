.class Lcom/peel/i/fk;
.super Landroid/widget/BaseAdapter;


# instance fields
.field final synthetic a:[Lcom/peel/control/h;

.field final synthetic b:Lcom/peel/i/fc;


# direct methods
.method constructor <init>(Lcom/peel/i/fc;[Lcom/peel/control/h;)V
    .locals 0

    .prologue
    .line 221
    iput-object p1, p0, Lcom/peel/i/fk;->b:Lcom/peel/i/fc;

    iput-object p2, p0, Lcom/peel/i/fk;->a:[Lcom/peel/control/h;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 263
    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 224
    iget-object v0, p0, Lcom/peel/i/fk;->a:[Lcom/peel/control/h;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/peel/i/fk;->a:[Lcom/peel/control/h;

    array-length v0, v0

    goto :goto_0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 230
    const/4 v0, 0x0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2

    .prologue
    .line 236
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4

    .prologue
    .line 241
    .line 242
    if-nez p2, :cond_0

    .line 243
    new-instance v1, Lcom/peel/i/fm;

    invoke-direct {v1, p0}, Lcom/peel/i/fm;-><init>(Lcom/peel/i/fk;)V

    .line 244
    iget-object v0, p0, Lcom/peel/i/fk;->b:Lcom/peel/i/fc;

    invoke-static {v0}, Lcom/peel/i/fc;->d(Lcom/peel/i/fc;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v2, Lcom/peel/ui/fq;->roku_list_item:I

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 245
    sget v0, Lcom/peel/ui/fp;->ip_address:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-static {v1, v0}, Lcom/peel/i/fm;->a(Lcom/peel/i/fm;Landroid/widget/TextView;)Landroid/widget/TextView;

    .line 246
    sget v0, Lcom/peel/ui/fp;->add_btn:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    invoke-static {v1, v0}, Lcom/peel/i/fm;->a(Lcom/peel/i/fm;Landroid/widget/Button;)Landroid/widget/Button;

    .line 247
    invoke-virtual {p2, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    move-object v0, v1

    .line 252
    :goto_0
    invoke-static {v0}, Lcom/peel/i/fm;->a(Lcom/peel/i/fm;)Landroid/widget/TextView;

    move-result-object v1

    iget-object v2, p0, Lcom/peel/i/fk;->a:[Lcom/peel/control/h;

    aget-object v2, v2, p1

    invoke-virtual {v2}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v2

    invoke-virtual {v2}, Lcom/peel/data/g;->i()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 253
    invoke-static {v0}, Lcom/peel/i/fm;->b(Lcom/peel/i/fm;)Landroid/widget/Button;

    move-result-object v0

    new-instance v1, Lcom/peel/i/fl;

    invoke-direct {v1, p0, p1}, Lcom/peel/i/fl;-><init>(Lcom/peel/i/fk;I)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 260
    return-object p2

    .line 249
    :cond_0
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/i/fm;

    goto :goto_0
.end method
