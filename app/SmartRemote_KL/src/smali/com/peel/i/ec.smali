.class Lcom/peel/i/ec;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/widget/AdapterView$OnItemSelectedListener;


# instance fields
.field final synthetic a:Lcom/peel/i/dz;


# direct methods
.method constructor <init>(Lcom/peel/i/dz;)V
    .locals 0

    .prologue
    .line 179
    iput-object p1, p0, Lcom/peel/i/ec;->a:Lcom/peel/i/dz;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    const/4 v5, -0x1

    .line 182
    iget-object v0, p0, Lcom/peel/i/ec;->a:Lcom/peel/i/dz;

    iget-object v0, v0, Lcom/peel/i/dz;->b:Lcom/peel/i/dx;

    invoke-virtual {v0}, Lcom/peel/i/dx;->c()V

    .line 184
    invoke-static {}, Lcom/peel/util/a/f;->a()Lcom/peel/util/a/f;

    move-result-object v0

    sget-object v1, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v1}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v1

    if-nez v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    const/16 v2, 0x43f

    const/16 v3, 0x7d5

    iget-object v4, p0, Lcom/peel/i/ec;->a:Lcom/peel/i/dz;

    iget-object v4, v4, Lcom/peel/i/dz;->a:Landroid/os/Bundle;

    const-string/jumbo v6, "iso"

    invoke-virtual {v4, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1}, Landroid/widget/AdapterView;->getSelectedItem()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/util/Pair;

    iget-object v6, v6, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v6, Ljava/lang/String;

    move v7, v5

    invoke-virtual/range {v0 .. v7}, Lcom/peel/util/a/f;->a(IIILjava/lang/String;ILjava/lang/String;I)V

    .line 186
    iget-object v0, p0, Lcom/peel/i/ec;->a:Lcom/peel/i/dz;

    iget-object v1, v0, Lcom/peel/i/dz;->b:Lcom/peel/i/dx;

    invoke-virtual {p1, p3}, Landroid/widget/AdapterView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    invoke-static {v1, v0}, Lcom/peel/i/dx;->a(Lcom/peel/i/dx;Ljava/lang/String;)Ljava/lang/String;

    .line 188
    iget-object v0, p0, Lcom/peel/i/ec;->a:Lcom/peel/i/dz;

    iget-object v1, v0, Lcom/peel/i/dz;->b:Lcom/peel/i/dx;

    iget-object v0, p0, Lcom/peel/i/ec;->a:Lcom/peel/i/dz;

    iget-object v2, v0, Lcom/peel/i/dz;->a:Landroid/os/Bundle;

    invoke-virtual {p1}, Landroid/widget/AdapterView;->getSelectedItem()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    iget-object v0, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Lcom/peel/i/dx;->a(Landroid/os/Bundle;Ljava/lang/String;)V

    .line 189
    return-void

    .line 184
    :cond_0
    sget-object v1, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v1}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/data/at;->f()I

    move-result v1

    goto :goto_0
.end method

.method public onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 192
    return-void
.end method
