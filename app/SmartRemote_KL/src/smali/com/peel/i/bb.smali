.class Lcom/peel/i/bb;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/peel/i/al;


# direct methods
.method constructor <init>(Lcom/peel/i/al;)V
    .locals 0

    .prologue
    .line 246
    iput-object p1, p0, Lcom/peel/i/bb;->a:Lcom/peel/i/al;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 249
    iget-object v0, p0, Lcom/peel/i/bb;->a:Lcom/peel/i/al;

    invoke-static {v0}, Lcom/peel/i/al;->j(Lcom/peel/i/al;)V

    .line 250
    iget-object v0, p0, Lcom/peel/i/bb;->a:Lcom/peel/i/al;

    invoke-virtual {v0}, Lcom/peel/i/al;->v()Landroid/view/View;

    move-result-object v0

    sget v1, Lcom/peel/ui/fp;->ip_device_list_panel:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 251
    iget-object v1, p0, Lcom/peel/i/bb;->a:Lcom/peel/i/al;

    iget-object v0, p0, Lcom/peel/i/bb;->a:Lcom/peel/i/al;

    invoke-virtual {v0}, Lcom/peel/i/al;->v()Landroid/view/View;

    move-result-object v0

    sget v2, Lcom/peel/ui/fp;->device_list:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    invoke-static {v1, v0}, Lcom/peel/i/al;->a(Lcom/peel/i/al;Landroid/widget/ListView;)Landroid/widget/ListView;

    .line 252
    iget-object v0, p0, Lcom/peel/i/bb;->a:Lcom/peel/i/al;

    new-instance v1, Lcom/peel/i/a/e;

    iget-object v2, p0, Lcom/peel/i/bb;->a:Lcom/peel/i/al;

    invoke-virtual {v2}, Lcom/peel/i/al;->m()Landroid/support/v4/app/ae;

    move-result-object v2

    sget v3, Lcom/peel/ui/fq;->dtv_device_row:I

    iget-object v4, p0, Lcom/peel/i/bb;->a:Lcom/peel/i/al;

    invoke-static {v4}, Lcom/peel/i/al;->c(Lcom/peel/i/al;)[Lcom/peel/control/h;

    move-result-object v4

    invoke-direct {v1, v2, v3, v4}, Lcom/peel/i/a/e;-><init>(Landroid/content/Context;I[Lcom/peel/control/h;)V

    invoke-static {v0, v1}, Lcom/peel/i/al;->a(Lcom/peel/i/al;Lcom/peel/i/a/e;)Lcom/peel/i/a/e;

    .line 253
    iget-object v0, p0, Lcom/peel/i/bb;->a:Lcom/peel/i/al;

    invoke-static {v0}, Lcom/peel/i/al;->p(Lcom/peel/i/al;)Landroid/widget/ListView;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/i/bb;->a:Lcom/peel/i/al;

    invoke-static {v1}, Lcom/peel/i/al;->o(Lcom/peel/i/al;)Lcom/peel/i/a/e;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 254
    iget-object v0, p0, Lcom/peel/i/bb;->a:Lcom/peel/i/al;

    invoke-static {v0}, Lcom/peel/i/al;->p(Lcom/peel/i/al;)Landroid/widget/ListView;

    move-result-object v0

    new-instance v1, Lcom/peel/i/bc;

    invoke-direct {v1, p0}, Lcom/peel/i/bc;-><init>(Lcom/peel/i/bb;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 270
    iget-object v0, p0, Lcom/peel/i/bb;->a:Lcom/peel/i/al;

    invoke-virtual {v0}, Lcom/peel/i/al;->v()Landroid/view/View;

    move-result-object v0

    sget v1, Lcom/peel/ui/fp;->no_thanks:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/peel/i/be;

    invoke-direct {v1, p0}, Lcom/peel/i/be;-><init>(Lcom/peel/i/bb;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 281
    iget-object v0, p0, Lcom/peel/i/bb;->a:Lcom/peel/i/al;

    invoke-virtual {v0}, Lcom/peel/i/al;->v()Landroid/view/View;

    move-result-object v0

    sget v1, Lcom/peel/ui/fp;->done:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/peel/i/bf;

    invoke-direct {v1, p0}, Lcom/peel/i/bf;-><init>(Lcom/peel/i/bb;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 369
    return-void
.end method
