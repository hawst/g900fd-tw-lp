.class Lcom/peel/i/dm;
.super Lcom/peel/util/t;


# instance fields
.field final synthetic a:Landroid/os/Bundle;

.field final synthetic b:Ljava/lang/String;

.field final synthetic c:Lcom/peel/i/cr;


# direct methods
.method constructor <init>(Lcom/peel/i/cr;ILandroid/os/Bundle;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 517
    iput-object p1, p0, Lcom/peel/i/dm;->c:Lcom/peel/i/cr;

    iput-object p3, p0, Lcom/peel/i/dm;->a:Landroid/os/Bundle;

    iput-object p4, p0, Lcom/peel/i/dm;->b:Ljava/lang/String;

    invoke-direct {p0, p2}, Lcom/peel/util/t;-><init>(I)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 521
    iget-boolean v0, p0, Lcom/peel/i/dm;->i:Z

    if-nez v0, :cond_0

    .line 523
    iget-object v0, p0, Lcom/peel/i/dm;->c:Lcom/peel/i/cr;

    iget-object v1, p0, Lcom/peel/i/dm;->a:Landroid/os/Bundle;

    iget-object v2, p0, Lcom/peel/i/dm;->b:Ljava/lang/String;

    const/4 v3, 0x0

    invoke-static {v0, v1, v2, v3}, Lcom/peel/i/cr;->a(Lcom/peel/i/cr;Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;)V

    .line 586
    :goto_0
    return-void

    .line 526
    :cond_0
    iget-object v0, p0, Lcom/peel/i/dm;->c:Lcom/peel/i/cr;

    invoke-static {v0}, Lcom/peel/i/cr;->m(Lcom/peel/i/cr;)Lcom/peel/util/CustomSpinner;

    move-result-object v0

    new-instance v1, Lcom/peel/i/dn;

    invoke-direct {v1, p0}, Lcom/peel/i/dn;-><init>(Lcom/peel/i/dm;)V

    invoke-virtual {v0, v1}, Lcom/peel/util/CustomSpinner;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 547
    iget-object v0, p0, Lcom/peel/i/dm;->c:Lcom/peel/i/cr;

    invoke-static {v0}, Lcom/peel/i/cr;->m(Lcom/peel/i/cr;)Lcom/peel/util/CustomSpinner;

    move-result-object v0

    invoke-virtual {v0, v5}, Lcom/peel/util/CustomSpinner;->setVisibility(I)V

    .line 548
    iget-object v0, p0, Lcom/peel/i/dm;->c:Lcom/peel/i/cr;

    invoke-static {v0}, Lcom/peel/i/cr;->m(Lcom/peel/i/cr;)Lcom/peel/util/CustomSpinner;

    move-result-object v1

    new-instance v2, Lcom/peel/i/do;

    iget-object v0, p0, Lcom/peel/i/dm;->c:Lcom/peel/i/cr;

    invoke-virtual {v0}, Lcom/peel/i/cr;->m()Landroid/support/v4/app/ae;

    move-result-object v3

    sget v4, Lcom/peel/ui/fq;->region_spinner_list_item:I

    iget-object v0, p0, Lcom/peel/i/dm;->j:Ljava/lang/Object;

    check-cast v0, Ljava/util/List;

    invoke-direct {v2, p0, v3, v4, v0}, Lcom/peel/i/do;-><init>(Lcom/peel/i/dm;Landroid/content/Context;ILjava/util/List;)V

    invoke-virtual {v1, v2}, Lcom/peel/util/CustomSpinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 567
    iget-object v0, p0, Lcom/peel/i/dm;->c:Lcom/peel/i/cr;

    invoke-static {v0}, Lcom/peel/i/cr;->m(Lcom/peel/i/cr;)Lcom/peel/util/CustomSpinner;

    move-result-object v0

    new-instance v1, Lcom/peel/i/dp;

    invoke-direct {v1, p0}, Lcom/peel/i/dp;-><init>(Lcom/peel/i/dm;)V

    invoke-virtual {v0, v1}, Lcom/peel/util/CustomSpinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 584
    iget-object v0, p0, Lcom/peel/i/dm;->c:Lcom/peel/i/cr;

    invoke-static {v0}, Lcom/peel/i/cr;->m(Lcom/peel/i/cr;)Lcom/peel/util/CustomSpinner;

    move-result-object v0

    invoke-virtual {v0, v5}, Lcom/peel/util/CustomSpinner;->setSelection(I)V

    goto :goto_0
.end method
