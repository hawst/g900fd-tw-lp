.class Lcom/peel/i/de;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/peel/i/dc;


# direct methods
.method constructor <init>(Lcom/peel/i/dc;)V
    .locals 0

    .prologue
    .line 342
    iput-object p1, p0, Lcom/peel/i/de;->a:Lcom/peel/i/dc;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 345
    iget-object v0, p0, Lcom/peel/i/de;->a:Lcom/peel/i/dc;

    iget-object v0, v0, Lcom/peel/i/dc;->d:Lcom/peel/i/cr;

    new-instance v1, Lcom/peel/widget/ag;

    iget-object v2, p0, Lcom/peel/i/de;->a:Lcom/peel/i/dc;

    iget-object v2, v2, Lcom/peel/i/dc;->d:Lcom/peel/i/cr;

    invoke-virtual {v2}, Lcom/peel/i/cr;->m()Landroid/support/v4/app/ae;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/peel/widget/ag;-><init>(Landroid/content/Context;)V

    invoke-static {v0, v1}, Lcom/peel/i/cr;->a(Lcom/peel/i/cr;Lcom/peel/widget/ag;)Lcom/peel/widget/ag;

    .line 346
    iget-object v0, p0, Lcom/peel/i/de;->a:Lcom/peel/i/dc;

    iget-object v0, v0, Lcom/peel/i/dc;->a:Landroid/os/Bundle;

    const-string/jumbo v1, "type"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "zip"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 347
    iget-object v0, p0, Lcom/peel/i/de;->a:Lcom/peel/i/dc;

    iget-object v0, v0, Lcom/peel/i/dc;->d:Lcom/peel/i/cr;

    invoke-static {v0}, Lcom/peel/i/cr;->i(Lcom/peel/i/cr;)Lcom/peel/widget/ag;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/i/de;->a:Lcom/peel/i/dc;

    iget-object v1, v1, Lcom/peel/i/dc;->d:Lcom/peel/i/cr;

    invoke-virtual {v1}, Lcom/peel/i/cr;->n()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/peel/ui/ft;->zipcode_validation_msg_without_zipcode:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/peel/widget/ag;->b(Ljava/lang/CharSequence;)Lcom/peel/widget/ag;

    .line 351
    :goto_0
    iget-object v0, p0, Lcom/peel/i/de;->a:Lcom/peel/i/dc;

    iget-object v0, v0, Lcom/peel/i/dc;->d:Lcom/peel/i/cr;

    invoke-static {v0}, Lcom/peel/i/cr;->i(Lcom/peel/i/cr;)Lcom/peel/widget/ag;

    move-result-object v0

    sget v1, Lcom/peel/ui/ft;->mytvservicemissing:I

    invoke-virtual {v0, v1}, Lcom/peel/widget/ag;->a(I)Lcom/peel/widget/ag;

    move-result-object v0

    sget v1, Lcom/peel/ui/ft;->label_report:I

    new-instance v2, Lcom/peel/i/df;

    invoke-direct {v2, p0}, Lcom/peel/i/df;-><init>(Lcom/peel/i/de;)V

    invoke-virtual {v0, v1, v2}, Lcom/peel/widget/ag;->a(ILandroid/content/DialogInterface$OnClickListener;)Lcom/peel/widget/ag;

    move-result-object v0

    sget v1, Lcom/peel/ui/ft;->cancel:I

    const/4 v2, 0x0

    .line 368
    invoke-virtual {v0, v1, v2}, Lcom/peel/widget/ag;->c(ILandroid/content/DialogInterface$OnClickListener;)Lcom/peel/widget/ag;

    .line 369
    iget-object v0, p0, Lcom/peel/i/de;->a:Lcom/peel/i/dc;

    iget-object v0, v0, Lcom/peel/i/dc;->d:Lcom/peel/i/cr;

    invoke-static {v0}, Lcom/peel/i/cr;->i(Lcom/peel/i/cr;)Lcom/peel/widget/ag;

    move-result-object v0

    invoke-virtual {v0, v6}, Lcom/peel/widget/ag;->setCanceledOnTouchOutside(Z)V

    .line 370
    iget-object v0, p0, Lcom/peel/i/de;->a:Lcom/peel/i/dc;

    iget-object v0, v0, Lcom/peel/i/dc;->d:Lcom/peel/i/cr;

    invoke-static {v0}, Lcom/peel/i/cr;->i(Lcom/peel/i/cr;)Lcom/peel/widget/ag;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/i/de;->a:Lcom/peel/i/dc;

    iget-object v1, v1, Lcom/peel/i/dc;->d:Lcom/peel/i/cr;

    invoke-static {v1}, Lcom/peel/i/cr;->i(Lcom/peel/i/cr;)Lcom/peel/widget/ag;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/peel/widget/ag;->a(Lcom/peel/widget/ag;)Lcom/peel/widget/ag;

    .line 371
    iget-object v0, p0, Lcom/peel/i/de;->a:Lcom/peel/i/dc;

    iget-object v0, v0, Lcom/peel/i/dc;->d:Lcom/peel/i/cr;

    invoke-static {v0}, Lcom/peel/i/cr;->i(Lcom/peel/i/cr;)Lcom/peel/widget/ag;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/i/de;->a:Lcom/peel/i/dc;

    iget-object v1, v1, Lcom/peel/i/dc;->d:Lcom/peel/i/cr;

    invoke-static {v1}, Lcom/peel/i/cr;->i(Lcom/peel/i/cr;)Lcom/peel/widget/ag;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/peel/widget/ag;->a(Lcom/peel/widget/ag;)Lcom/peel/widget/ag;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/widget/ag;->d()Lcom/peel/widget/ag;

    .line 372
    return-void

    .line 349
    :cond_0
    iget-object v0, p0, Lcom/peel/i/de;->a:Lcom/peel/i/dc;

    iget-object v0, v0, Lcom/peel/i/dc;->d:Lcom/peel/i/cr;

    invoke-static {v0}, Lcom/peel/i/cr;->i(Lcom/peel/i/cr;)Lcom/peel/widget/ag;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/i/de;->a:Lcom/peel/i/dc;

    iget-object v1, v1, Lcom/peel/i/dc;->d:Lcom/peel/i/cr;

    invoke-virtual {v1}, Lcom/peel/i/cr;->n()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/peel/ui/ft;->zipcode_validation_msg:I

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lcom/peel/i/de;->a:Lcom/peel/i/dc;

    iget-object v5, v5, Lcom/peel/i/dc;->b:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/peel/i/de;->a:Lcom/peel/i/dc;

    iget-object v5, v5, Lcom/peel/i/dc;->c:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/peel/widget/ag;->b(Ljava/lang/CharSequence;)Lcom/peel/widget/ag;

    goto/16 :goto_0
.end method
