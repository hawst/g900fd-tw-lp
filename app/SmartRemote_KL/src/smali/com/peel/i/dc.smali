.class Lcom/peel/i/dc;
.super Lcom/peel/util/t;


# instance fields
.field final synthetic a:Landroid/os/Bundle;

.field final synthetic b:Ljava/lang/String;

.field final synthetic c:Ljava/lang/String;

.field final synthetic d:Lcom/peel/i/cr;


# direct methods
.method constructor <init>(Lcom/peel/i/cr;ILandroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 311
    iput-object p1, p0, Lcom/peel/i/dc;->d:Lcom/peel/i/cr;

    iput-object p3, p0, Lcom/peel/i/dc;->a:Landroid/os/Bundle;

    iput-object p4, p0, Lcom/peel/i/dc;->b:Ljava/lang/String;

    iput-object p5, p0, Lcom/peel/i/dc;->c:Ljava/lang/String;

    invoke-direct {p0, p2}, Lcom/peel/util/t;-><init>(I)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v5, 0x0

    const/4 v3, 0x1

    .line 314
    iget-object v0, p0, Lcom/peel/i/dc;->d:Lcom/peel/i/cr;

    invoke-virtual {v0}, Lcom/peel/i/cr;->S()V

    .line 316
    iget-boolean v0, p0, Lcom/peel/i/dc;->i:Z

    if-nez v0, :cond_1

    .line 317
    invoke-static {}, Lcom/peel/i/cr;->T()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Downloader.offline :: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-boolean v2, Lcom/peel/util/b/a;->a:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 318
    sget-boolean v0, Lcom/peel/util/b/a;->a:Z

    if-eqz v0, :cond_0

    .line 415
    :goto_0
    return-void

    .line 321
    :cond_0
    iget-object v0, p0, Lcom/peel/i/dc;->d:Lcom/peel/i/cr;

    invoke-virtual {v0}, Lcom/peel/i/cr;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    sget v1, Lcom/peel/ui/ft;->unable_get_lineups:I

    invoke-static {v0, v1, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 326
    :cond_1
    iget-object v0, p0, Lcom/peel/i/dc;->j:Ljava/lang/Object;

    check-cast v0, [Landroid/os/Bundle;

    move-object v4, v0

    check-cast v4, [Landroid/os/Bundle;

    .line 327
    const-string/jumbo v0, "US"

    iget-object v1, p0, Lcom/peel/i/dc;->a:Landroid/os/Bundle;

    const-string/jumbo v2, "iso"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 328
    invoke-static {}, Ljava/text/Collator;->getInstance()Ljava/text/Collator;

    move-result-object v0

    .line 329
    invoke-virtual {v0, v3}, Ljava/text/Collator;->setDecomposition(I)V

    .line 332
    new-instance v1, Lcom/peel/i/dd;

    invoke-direct {v1, p0, v0}, Lcom/peel/i/dd;-><init>(Lcom/peel/i/dc;Ljava/text/Collator;)V

    invoke-static {v4, v1}, Ljava/util/Arrays;->sort([Ljava/lang/Object;Ljava/util/Comparator;)V

    .line 340
    :cond_2
    iget-object v0, p0, Lcom/peel/i/dc;->d:Lcom/peel/i/cr;

    invoke-static {v0}, Lcom/peel/i/cr;->g(Lcom/peel/i/cr;)Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ListView;->getFooterViewsCount()I

    move-result v0

    if-nez v0, :cond_3

    .line 341
    iget-object v0, p0, Lcom/peel/i/dc;->d:Lcom/peel/i/cr;

    invoke-static {v0}, Lcom/peel/i/cr;->h(Lcom/peel/i/cr;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lcom/peel/ui/fq;->report_missing_service_provider_footer:I

    invoke-virtual {v0, v1, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 342
    sget v1, Lcom/peel/ui/fp;->report:I

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    new-instance v2, Lcom/peel/i/de;

    invoke-direct {v2, p0}, Lcom/peel/i/de;-><init>(Lcom/peel/i/dc;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 374
    iget-object v1, p0, Lcom/peel/i/dc;->d:Lcom/peel/i/cr;

    invoke-static {v1}, Lcom/peel/i/cr;->g(Lcom/peel/i/cr;)Landroid/widget/ListView;

    move-result-object v1

    invoke-virtual {v1, v0, v5, v7}, Landroid/widget/ListView;->addFooterView(Landroid/view/View;Ljava/lang/Object;Z)V

    .line 377
    :cond_3
    iget-object v0, p0, Lcom/peel/i/dc;->d:Lcom/peel/i/cr;

    invoke-virtual {v0}, Lcom/peel/i/cr;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/peel/i/dc;->d:Lcom/peel/i/cr;

    invoke-virtual {v0}, Lcom/peel/i/cr;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/ae;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v5

    .line 378
    :cond_4
    iget-object v0, p0, Lcom/peel/i/dc;->d:Lcom/peel/i/cr;

    invoke-static {v0}, Lcom/peel/i/cr;->g(Lcom/peel/i/cr;)Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/ListView;->setItemsCanFocus(Z)V

    .line 379
    iget-object v0, p0, Lcom/peel/i/dc;->d:Lcom/peel/i/cr;

    invoke-static {v0}, Lcom/peel/i/cr;->g(Lcom/peel/i/cr;)Landroid/widget/ListView;

    move-result-object v6

    new-instance v0, Lcom/peel/i/dg;

    iget-object v1, p0, Lcom/peel/i/dc;->d:Lcom/peel/i/cr;

    invoke-virtual {v1}, Lcom/peel/i/cr;->m()Landroid/support/v4/app/ae;

    move-result-object v2

    sget v3, Lcom/peel/ui/fq;->provider_row:I

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/peel/i/dg;-><init>(Lcom/peel/i/dc;Landroid/content/Context;I[Landroid/os/Bundle;Ljava/lang/String;)V

    invoke-virtual {v6, v0}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 393
    iget-object v0, p0, Lcom/peel/i/dc;->d:Lcom/peel/i/cr;

    invoke-static {v0}, Lcom/peel/i/cr;->g(Lcom/peel/i/cr;)Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0, v7}, Landroid/widget/ListView;->setVisibility(I)V

    .line 395
    iget-object v0, p0, Lcom/peel/i/dc;->d:Lcom/peel/i/cr;

    invoke-static {v0}, Lcom/peel/i/cr;->g(Lcom/peel/i/cr;)Landroid/widget/ListView;

    move-result-object v0

    new-instance v1, Lcom/peel/i/dh;

    invoke-direct {v1, p0}, Lcom/peel/i/dh;-><init>(Lcom/peel/i/dc;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 414
    iget-object v0, p0, Lcom/peel/i/dc;->d:Lcom/peel/i/cr;

    invoke-virtual {v0}, Lcom/peel/i/cr;->S()V

    goto/16 :goto_0
.end method
