.class Lcom/peel/i/ce;
.super Lcom/peel/util/t;


# instance fields
.field final synthetic a:Lcom/peel/i/cd;


# direct methods
.method constructor <init>(Lcom/peel/i/cd;)V
    .locals 0

    .prologue
    .line 223
    iput-object p1, p0, Lcom/peel/i/ce;->a:Lcom/peel/i/cd;

    invoke-direct {p0}, Lcom/peel/util/t;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 225
    iget-boolean v0, p0, Lcom/peel/i/ce;->i:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/peel/i/ce;->j:Ljava/lang/Object;

    if-eqz v0, :cond_0

    .line 227
    iget-object v0, p0, Lcom/peel/i/ce;->a:Lcom/peel/i/cd;

    iget-object v0, v0, Lcom/peel/i/cd;->a:Lcom/peel/i/cc;

    iget-object v1, v0, Lcom/peel/i/cc;->b:Landroid/os/Bundle;

    const-string/jumbo v2, "languages"

    new-instance v3, Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/peel/i/ce;->j:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    const-string/jumbo v4, ","

    invoke-static {v0, v4}, Landroid/text/TextUtils;->split(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-direct {v3, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 230
    :cond_0
    iget-object v0, p0, Lcom/peel/i/ce;->a:Lcom/peel/i/cd;

    iget-object v0, v0, Lcom/peel/i/cd;->a:Lcom/peel/i/cc;

    iget-object v0, v0, Lcom/peel/i/cc;->c:Lcom/peel/i/cb;

    iget-object v0, v0, Lcom/peel/i/cb;->b:Lcom/peel/i/by;

    iget-object v0, v0, Lcom/peel/i/by;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "country"

    iget-object v2, p0, Lcom/peel/i/ce;->a:Lcom/peel/i/cd;

    iget-object v2, v2, Lcom/peel/i/cd;->a:Lcom/peel/i/cc;

    iget-object v2, v2, Lcom/peel/i/cc;->b:Landroid/os/Bundle;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 232
    iget-object v0, p0, Lcom/peel/i/ce;->a:Lcom/peel/i/cd;

    iget-object v0, v0, Lcom/peel/i/cd;->a:Lcom/peel/i/cc;

    iget-object v0, v0, Lcom/peel/i/cc;->c:Lcom/peel/i/cb;

    iget-object v0, v0, Lcom/peel/i/cb;->b:Lcom/peel/i/by;

    iget-object v0, v0, Lcom/peel/i/by;->e:Lcom/peel/i/cf;

    if-eqz v0, :cond_1

    .line 233
    iget-object v0, p0, Lcom/peel/i/ce;->a:Lcom/peel/i/cd;

    iget-object v0, v0, Lcom/peel/i/cd;->a:Lcom/peel/i/cc;

    iget-object v0, v0, Lcom/peel/i/cc;->c:Lcom/peel/i/cb;

    iget-object v0, v0, Lcom/peel/i/cb;->b:Lcom/peel/i/by;

    iget-object v0, v0, Lcom/peel/i/by;->e:Lcom/peel/i/cf;

    iget-object v1, p0, Lcom/peel/i/ce;->a:Lcom/peel/i/cd;

    iget-object v1, v1, Lcom/peel/i/cd;->a:Lcom/peel/i/cc;

    iget-object v1, v1, Lcom/peel/i/cc;->c:Lcom/peel/i/cb;

    iget-object v1, v1, Lcom/peel/i/cb;->b:Lcom/peel/i/by;

    iget-object v1, v1, Lcom/peel/i/by;->c:Landroid/os/Bundle;

    invoke-interface {v0, v1}, Lcom/peel/i/cf;->a(Landroid/os/Bundle;)V

    .line 234
    :cond_1
    invoke-static {}, Lcom/peel/i/by;->c()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/i/ce;->a:Lcom/peel/i/cd;

    iget-object v1, v1, Lcom/peel/i/cd;->a:Lcom/peel/i/cc;

    iget-object v1, v1, Lcom/peel/i/cc;->c:Lcom/peel/i/cb;

    iget-object v1, v1, Lcom/peel/i/cb;->b:Lcom/peel/i/by;

    invoke-virtual {v1}, Lcom/peel/i/by;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/peel/util/bx;->a(Ljava/lang/String;Landroid/app/Activity;Z)V

    .line 235
    invoke-static {}, Lcom/peel/i/by;->c()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/i/ce;->a:Lcom/peel/i/cd;

    iget-object v1, v1, Lcom/peel/i/cd;->a:Lcom/peel/i/cc;

    iget-object v1, v1, Lcom/peel/i/cc;->c:Lcom/peel/i/cb;

    iget-object v1, v1, Lcom/peel/i/cb;->b:Lcom/peel/i/by;

    invoke-virtual {v1}, Lcom/peel/i/by;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/peel/d/e;->a(Ljava/lang/String;Landroid/support/v4/app/ae;)V

    .line 236
    return-void
.end method
