.class Lcom/peel/i/ef;
.super Landroid/widget/ArrayAdapter;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Landroid/util/Pair",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/String;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/peel/i/ed;


# direct methods
.method constructor <init>(Lcom/peel/i/ed;Landroid/content/Context;ILjava/util/List;)V
    .locals 0

    .prologue
    .line 243
    iput-object p1, p0, Lcom/peel/i/ef;->a:Lcom/peel/i/ed;

    invoke-direct {p0, p2, p3, p4}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    return-void
.end method


# virtual methods
.method public getDropDownView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    .prologue
    .line 262
    invoke-super {p0, p1, p2, p3}, Landroid/widget/ArrayAdapter;->getDropDownView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 263
    const v1, 0x1020014

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, p1, v0, p3}, Lcom/peel/i/ef;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 5

    .prologue
    .line 246
    iget-object v0, p0, Lcom/peel/i/ef;->a:Lcom/peel/i/ed;

    iget-object v1, v0, Lcom/peel/i/ed;->c:Lcom/peel/i/dx;

    if-nez p2, :cond_0

    iget-object v0, p0, Lcom/peel/i/ef;->a:Lcom/peel/i/ed;

    iget-object v0, v0, Lcom/peel/i/ed;->c:Lcom/peel/i/dx;

    invoke-static {v0}, Lcom/peel/i/dx;->e(Lcom/peel/i/dx;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v2, Lcom/peel/ui/fq;->region_spinner_list_item:I

    iget-object v3, p0, Lcom/peel/i/ef;->a:Lcom/peel/i/ed;

    iget-object v3, v3, Lcom/peel/i/ed;->c:Lcom/peel/i/dx;

    invoke-static {v3}, Lcom/peel/i/dx;->d(Lcom/peel/i/dx;)Landroid/widget/LinearLayout;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v0, v2, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    :goto_0
    check-cast v0, Landroid/widget/TextView;

    check-cast v0, Landroid/widget/TextView;

    invoke-static {v1, v0}, Lcom/peel/i/dx;->b(Lcom/peel/i/dx;Landroid/widget/TextView;)Landroid/widget/TextView;

    .line 247
    invoke-virtual {p0, p1}, Lcom/peel/i/ef;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    .line 248
    iget-object v1, p0, Lcom/peel/i/ef;->a:Lcom/peel/i/ed;

    iget-object v1, v1, Lcom/peel/i/ed;->c:Lcom/peel/i/dx;

    invoke-static {v1}, Lcom/peel/i/dx;->f(Lcom/peel/i/dx;)Landroid/widget/TextView;

    move-result-object v1

    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 249
    iget-object v0, p0, Lcom/peel/i/ef;->a:Lcom/peel/i/ed;

    iget-object v0, v0, Lcom/peel/i/ed;->c:Lcom/peel/i/dx;

    invoke-static {v0}, Lcom/peel/i/dx;->g(Lcom/peel/i/dx;)Lcom/peel/util/CustomSpinner;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/util/CustomSpinner;->getSelectedItemPosition()I

    move-result v0

    if-ne p1, v0, :cond_1

    .line 250
    iget-object v0, p0, Lcom/peel/i/ef;->a:Lcom/peel/i/ed;

    iget-object v0, v0, Lcom/peel/i/ed;->c:Lcom/peel/i/dx;

    invoke-static {v0}, Lcom/peel/i/dx;->f(Lcom/peel/i/dx;)Landroid/widget/TextView;

    move-result-object v0

    const-string/jumbo v1, "#2396c5"

    invoke-static {v1}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 257
    :goto_1
    iget-object v0, p0, Lcom/peel/i/ef;->a:Lcom/peel/i/ed;

    iget-object v0, v0, Lcom/peel/i/ed;->c:Lcom/peel/i/dx;

    invoke-static {v0}, Lcom/peel/i/dx;->f(Lcom/peel/i/dx;)Landroid/widget/TextView;

    move-result-object v0

    return-object v0

    :cond_0
    move-object v0, p2

    .line 246
    goto :goto_0

    .line 252
    :cond_1
    sget-object v0, Lcom/peel/c/a;->b:Lcom/peel/c/e;

    invoke-static {v0}, Lcom/peel/c/a;->a(Lcom/peel/c/e;)Ljava/lang/Object;

    move-result-object v0

    sget-object v1, Lcom/peel/c/b;->c:Lcom/peel/c/b;

    if-ne v0, v1, :cond_2

    .line 253
    iget-object v0, p0, Lcom/peel/i/ef;->a:Lcom/peel/i/ed;

    iget-object v0, v0, Lcom/peel/i/ed;->c:Lcom/peel/i/dx;

    invoke-static {v0}, Lcom/peel/i/dx;->f(Lcom/peel/i/dx;)Landroid/widget/TextView;

    move-result-object v0

    const-string/jumbo v1, "#252525"

    invoke-static {v1}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_1

    .line 255
    :cond_2
    iget-object v0, p0, Lcom/peel/i/ef;->a:Lcom/peel/i/ed;

    iget-object v0, v0, Lcom/peel/i/ed;->c:Lcom/peel/i/dx;

    invoke-static {v0}, Lcom/peel/i/dx;->f(Lcom/peel/i/dx;)Landroid/widget/TextView;

    move-result-object v0

    const-string/jumbo v1, "#FFFFFF"

    invoke-static {v1}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_1
.end method
