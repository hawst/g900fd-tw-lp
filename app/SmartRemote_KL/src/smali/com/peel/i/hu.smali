.class Lcom/peel/i/hu;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/support/v4/view/cs;


# instance fields
.field final synthetic a:Lcom/peel/i/ht;


# direct methods
.method constructor <init>(Lcom/peel/i/ht;)V
    .locals 0

    .prologue
    .line 1696
    iput-object p1, p0, Lcom/peel/i/hu;->a:Lcom/peel/i/ht;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(I)V
    .locals 6

    .prologue
    const/16 v5, 0x8

    const/4 v4, 0x0

    .line 1718
    iget-object v0, p0, Lcom/peel/i/hu;->a:Lcom/peel/i/ht;

    iget-object v0, v0, Lcom/peel/i/ht;->a:Lcom/peel/i/gm;

    invoke-static {v0, p1}, Lcom/peel/i/gm;->c(Lcom/peel/i/gm;I)I

    .line 1719
    iget-object v0, p0, Lcom/peel/i/hu;->a:Lcom/peel/i/ht;

    iget-object v1, v0, Lcom/peel/i/ht;->a:Lcom/peel/i/gm;

    iget-object v0, p0, Lcom/peel/i/hu;->a:Lcom/peel/i/ht;

    iget-object v0, v0, Lcom/peel/i/ht;->a:Lcom/peel/i/gm;

    invoke-static {v0}, Lcom/peel/i/gm;->S(Lcom/peel/i/gm;)Ljava/util/ArrayList;

    move-result-object v0

    iget-object v2, p0, Lcom/peel/i/hu;->a:Lcom/peel/i/ht;

    iget-object v2, v2, Lcom/peel/i/ht;->a:Lcom/peel/i/gm;

    invoke-static {v2}, Lcom/peel/i/gm;->ag(Lcom/peel/i/gm;)I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    const-string/jumbo v2, "codesetid"

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v1, v0}, Lcom/peel/i/gm;->d(Lcom/peel/i/gm;I)I

    .line 1720
    iget-object v0, p0, Lcom/peel/i/hu;->a:Lcom/peel/i/ht;

    iget-object v0, v0, Lcom/peel/i/ht;->a:Lcom/peel/i/gm;

    invoke-static {v0}, Lcom/peel/i/gm;->w(Lcom/peel/i/gm;)Lcom/peel/control/h;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v1

    const-string/jumbo v2, "Channel_Up"

    iget-object v0, p0, Lcom/peel/i/hu;->a:Lcom/peel/i/ht;

    iget-object v0, v0, Lcom/peel/i/ht;->a:Lcom/peel/i/gm;

    invoke-static {v0}, Lcom/peel/i/gm;->S(Lcom/peel/i/gm;)Ljava/util/ArrayList;

    move-result-object v0

    iget-object v3, p0, Lcom/peel/i/hu;->a:Lcom/peel/i/ht;

    iget-object v3, v3, Lcom/peel/i/ht;->a:Lcom/peel/i/gm;

    invoke-static {v3}, Lcom/peel/i/gm;->ag(Lcom/peel/i/gm;)I

    move-result v3

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    invoke-virtual {v1, v2, v0}, Lcom/peel/data/g;->a(Ljava/lang/String;Ljava/util/Map;)V

    .line 1721
    invoke-static {}, Lcom/peel/i/gm;->c()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "STB codeIdx:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/peel/i/hu;->a:Lcom/peel/i/ht;

    iget-object v2, v2, Lcom/peel/i/ht;->a:Lcom/peel/i/gm;

    invoke-static {v2}, Lcom/peel/i/gm;->ag(Lcom/peel/i/gm;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "/ codesetId:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/peel/i/hu;->a:Lcom/peel/i/ht;

    iget-object v2, v2, Lcom/peel/i/ht;->a:Lcom/peel/i/gm;

    invoke-static {v2}, Lcom/peel/i/gm;->V(Lcom/peel/i/gm;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1723
    iget-object v0, p0, Lcom/peel/i/hu;->a:Lcom/peel/i/ht;

    iget-object v0, v0, Lcom/peel/i/ht;->a:Lcom/peel/i/gm;

    invoke-static {v0}, Lcom/peel/i/gm;->U(Lcom/peel/i/gm;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1724
    iget-object v0, p0, Lcom/peel/i/hu;->a:Lcom/peel/i/ht;

    iget-object v0, v0, Lcom/peel/i/ht;->a:Lcom/peel/i/gm;

    invoke-static {v0}, Lcom/peel/i/gm;->Y(Lcom/peel/i/gm;)Landroid/widget/RelativeLayout;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1725
    iget-object v0, p0, Lcom/peel/i/hu;->a:Lcom/peel/i/ht;

    iget-object v0, v0, Lcom/peel/i/ht;->a:Lcom/peel/i/gm;

    invoke-static {v0}, Lcom/peel/i/gm;->Z(Lcom/peel/i/gm;)Landroid/widget/RelativeLayout;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1726
    iget-object v0, p0, Lcom/peel/i/hu;->a:Lcom/peel/i/ht;

    iget-object v0, v0, Lcom/peel/i/ht;->a:Lcom/peel/i/gm;

    invoke-static {v0}, Lcom/peel/i/gm;->aa(Lcom/peel/i/gm;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/i/hu;->a:Lcom/peel/i/ht;

    iget-object v1, v1, Lcom/peel/i/ht;->a:Lcom/peel/i/gm;

    sget v2, Lcom/peel/ui/ft;->testing_key_stb:I

    invoke-virtual {v1, v2}, Lcom/peel/i/gm;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1727
    iget-object v0, p0, Lcom/peel/i/hu;->a:Lcom/peel/i/ht;

    iget-object v0, v0, Lcom/peel/i/ht;->a:Lcom/peel/i/gm;

    invoke-static {v0}, Lcom/peel/i/gm;->ad(Lcom/peel/i/gm;)V

    .line 1729
    iget-object v0, p0, Lcom/peel/i/hu;->a:Lcom/peel/i/ht;

    iget-object v0, v0, Lcom/peel/i/ht;->a:Lcom/peel/i/gm;

    invoke-static {v0}, Lcom/peel/i/gm;->T(Lcom/peel/i/gm;)Lcom/peel/widget/TestBtnViewPager;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "btnView"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/peel/i/hu;->a:Lcom/peel/i/ht;

    iget-object v2, v2, Lcom/peel/i/ht;->a:Lcom/peel/i/gm;

    invoke-static {v2}, Lcom/peel/i/gm;->T(Lcom/peel/i/gm;)Lcom/peel/widget/TestBtnViewPager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/peel/widget/TestBtnViewPager;->getCurrentItem()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/peel/widget/TestBtnViewPager;->findViewWithTag(Ljava/lang/Object;)Landroid/view/View;

    move-result-object v0

    .line 1730
    sget v1, Lcom/peel/ui/fp;->test_other_btn_large_view:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    .line 1731
    iget-object v1, p0, Lcom/peel/i/hu;->a:Lcom/peel/i/ht;

    iget-object v1, v1, Lcom/peel/i/ht;->a:Lcom/peel/i/gm;

    invoke-static {v1}, Lcom/peel/i/gm;->T(Lcom/peel/i/gm;)Lcom/peel/widget/TestBtnViewPager;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "btnView"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/peel/i/hu;->a:Lcom/peel/i/ht;

    iget-object v3, v3, Lcom/peel/i/ht;->a:Lcom/peel/i/gm;

    invoke-static {v3}, Lcom/peel/i/gm;->aw(Lcom/peel/i/gm;)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/peel/widget/TestBtnViewPager;->findViewWithTag(Ljava/lang/Object;)Landroid/view/View;

    move-result-object v1

    .line 1733
    sget v2, Lcom/peel/ui/fp;->test_other_btn_large_view:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    .line 1734
    sget v2, Lcom/peel/ui/fp;->test_other_btn_small_view:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    .line 1735
    sget v0, Lcom/peel/ui/fp;->test_other_btn_large_view:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    .line 1736
    sget v0, Lcom/peel/ui/fp;->test_other_btn_small_view:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 1737
    iget-object v0, p0, Lcom/peel/i/hu;->a:Lcom/peel/i/ht;

    iget-object v0, v0, Lcom/peel/i/ht;->a:Lcom/peel/i/gm;

    invoke-static {v0, p1}, Lcom/peel/i/gm;->f(Lcom/peel/i/gm;I)I

    .line 1738
    return-void
.end method

.method public a(IFI)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x4

    const/4 v2, 0x0

    .line 1699
    iget-object v0, p0, Lcom/peel/i/hu;->a:Lcom/peel/i/ht;

    iget-object v0, v0, Lcom/peel/i/ht;->a:Lcom/peel/i/gm;

    invoke-static {v0}, Lcom/peel/i/gm;->T(Lcom/peel/i/gm;)Lcom/peel/widget/TestBtnViewPager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/widget/TestBtnViewPager;->getCurrentItem()I

    move-result v0

    .line 1700
    if-nez v0, :cond_0

    iget-object v1, p0, Lcom/peel/i/hu;->a:Lcom/peel/i/ht;

    iget-object v1, v1, Lcom/peel/i/ht;->a:Lcom/peel/i/gm;

    invoke-static {v1}, Lcom/peel/i/gm;->S(Lcom/peel/i/gm;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-le v1, v4, :cond_0

    .line 1701
    iget-object v0, p0, Lcom/peel/i/hu;->a:Lcom/peel/i/ht;

    iget-object v0, v0, Lcom/peel/i/ht;->a:Lcom/peel/i/gm;

    invoke-static {v0}, Lcom/peel/i/gm;->ab(Lcom/peel/i/gm;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setVisibility(I)V

    .line 1702
    iget-object v0, p0, Lcom/peel/i/hu;->a:Lcom/peel/i/ht;

    iget-object v0, v0, Lcom/peel/i/ht;->a:Lcom/peel/i/gm;

    invoke-static {v0}, Lcom/peel/i/gm;->ac(Lcom/peel/i/gm;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 1713
    :goto_0
    return-void

    .line 1703
    :cond_0
    iget-object v1, p0, Lcom/peel/i/hu;->a:Lcom/peel/i/ht;

    iget-object v1, v1, Lcom/peel/i/ht;->a:Lcom/peel/i/gm;

    invoke-static {v1}, Lcom/peel/i/gm;->S(Lcom/peel/i/gm;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-ne v0, v1, :cond_1

    iget-object v1, p0, Lcom/peel/i/hu;->a:Lcom/peel/i/ht;

    iget-object v1, v1, Lcom/peel/i/ht;->a:Lcom/peel/i/gm;

    invoke-static {v1}, Lcom/peel/i/gm;->S(Lcom/peel/i/gm;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-le v1, v4, :cond_1

    .line 1704
    iget-object v0, p0, Lcom/peel/i/hu;->a:Lcom/peel/i/ht;

    iget-object v0, v0, Lcom/peel/i/ht;->a:Lcom/peel/i/gm;

    invoke-static {v0}, Lcom/peel/i/gm;->ab(Lcom/peel/i/gm;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 1705
    iget-object v0, p0, Lcom/peel/i/hu;->a:Lcom/peel/i/ht;

    iget-object v0, v0, Lcom/peel/i/ht;->a:Lcom/peel/i/gm;

    invoke-static {v0}, Lcom/peel/i/gm;->ac(Lcom/peel/i/gm;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_0

    .line 1706
    :cond_1
    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/peel/i/hu;->a:Lcom/peel/i/ht;

    iget-object v0, v0, Lcom/peel/i/ht;->a:Lcom/peel/i/gm;

    invoke-static {v0}, Lcom/peel/i/gm;->S(Lcom/peel/i/gm;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ne v0, v4, :cond_2

    .line 1707
    iget-object v0, p0, Lcom/peel/i/hu;->a:Lcom/peel/i/ht;

    iget-object v0, v0, Lcom/peel/i/ht;->a:Lcom/peel/i/gm;

    invoke-static {v0}, Lcom/peel/i/gm;->ab(Lcom/peel/i/gm;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setVisibility(I)V

    .line 1708
    iget-object v0, p0, Lcom/peel/i/hu;->a:Lcom/peel/i/ht;

    iget-object v0, v0, Lcom/peel/i/ht;->a:Lcom/peel/i/gm;

    invoke-static {v0}, Lcom/peel/i/gm;->ac(Lcom/peel/i/gm;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_0

    .line 1710
    :cond_2
    iget-object v0, p0, Lcom/peel/i/hu;->a:Lcom/peel/i/ht;

    iget-object v0, v0, Lcom/peel/i/ht;->a:Lcom/peel/i/gm;

    invoke-static {v0}, Lcom/peel/i/gm;->ab(Lcom/peel/i/gm;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 1711
    iget-object v0, p0, Lcom/peel/i/hu;->a:Lcom/peel/i/ht;

    iget-object v0, v0, Lcom/peel/i/ht;->a:Lcom/peel/i/gm;

    invoke-static {v0}, Lcom/peel/i/gm;->ac(Lcom/peel/i/gm;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_0
.end method

.method public b(I)V
    .locals 0

    .prologue
    .line 1743
    return-void
.end method
