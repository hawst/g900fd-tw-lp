.class Lcom/peel/i/bt;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/peel/i/bs;


# direct methods
.method constructor <init>(Lcom/peel/i/bs;)V
    .locals 0

    .prologue
    .line 211
    iput-object p1, p0, Lcom/peel/i/bt;->a:Lcom/peel/i/bs;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 214
    iget-object v0, p0, Lcom/peel/i/bt;->a:Lcom/peel/i/bs;

    iget-object v0, v0, Lcom/peel/i/bs;->a:Lcom/peel/i/bm;

    new-instance v1, Lcom/peel/widget/ag;

    iget-object v2, p0, Lcom/peel/i/bt;->a:Lcom/peel/i/bs;

    iget-object v2, v2, Lcom/peel/i/bs;->a:Lcom/peel/i/bm;

    invoke-virtual {v2}, Lcom/peel/i/bm;->m()Landroid/support/v4/app/ae;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/peel/widget/ag;-><init>(Landroid/content/Context;)V

    invoke-static {v0, v1}, Lcom/peel/i/bm;->a(Lcom/peel/i/bm;Lcom/peel/widget/ag;)Lcom/peel/widget/ag;

    .line 215
    iget-object v0, p0, Lcom/peel/i/bt;->a:Lcom/peel/i/bs;

    iget-object v0, v0, Lcom/peel/i/bs;->a:Lcom/peel/i/bm;

    invoke-static {v0}, Lcom/peel/i/bm;->d(Lcom/peel/i/bm;)Lcom/peel/widget/ag;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/peel/widget/ag;->setCanceledOnTouchOutside(Z)V

    .line 216
    iget-object v0, p0, Lcom/peel/i/bt;->a:Lcom/peel/i/bs;

    iget-object v0, v0, Lcom/peel/i/bs;->a:Lcom/peel/i/bm;

    invoke-static {v0}, Lcom/peel/i/bm;->d(Lcom/peel/i/bm;)Lcom/peel/widget/ag;

    move-result-object v0

    sget v1, Lcom/peel/ui/ft;->title_confirmation:I

    invoke-virtual {v0, v1}, Lcom/peel/widget/ag;->a(I)Lcom/peel/widget/ag;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/i/bt;->a:Lcom/peel/i/bs;

    iget-object v1, v1, Lcom/peel/i/bs;->a:Lcom/peel/i/bm;

    invoke-virtual {v1}, Lcom/peel/i/bm;->n()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/peel/ui/ft;->missing_tv_service_confirmation:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/peel/widget/ag;->b(Ljava/lang/CharSequence;)Lcom/peel/widget/ag;

    move-result-object v0

    sget v1, Lcom/peel/ui/ft;->okay:I

    new-instance v2, Lcom/peel/i/bu;

    invoke-direct {v2, p0}, Lcom/peel/i/bu;-><init>(Lcom/peel/i/bt;)V

    .line 217
    invoke-virtual {v0, v1, v2}, Lcom/peel/widget/ag;->a(ILandroid/content/DialogInterface$OnClickListener;)Lcom/peel/widget/ag;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/i/bt;->a:Lcom/peel/i/bs;

    iget-object v1, v1, Lcom/peel/i/bs;->a:Lcom/peel/i/bm;

    .line 223
    invoke-static {v1}, Lcom/peel/i/bm;->d(Lcom/peel/i/bm;)Lcom/peel/widget/ag;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/peel/widget/ag;->a(Lcom/peel/widget/ag;)Lcom/peel/widget/ag;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/widget/ag;->show()V

    .line 224
    return-void
.end method
