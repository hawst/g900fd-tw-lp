.class Lcom/peel/i/ij;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field final synthetic a:Lcom/peel/i/gm;


# direct methods
.method constructor <init>(Lcom/peel/i/gm;)V
    .locals 0

    .prologue
    .line 821
    iput-object p1, p0, Lcom/peel/i/ij;->a:Lcom/peel/i/gm;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    .line 824
    iget-object v0, p0, Lcom/peel/i/ij;->a:Lcom/peel/i/gm;

    invoke-static {v0, v3}, Lcom/peel/i/gm;->c(Lcom/peel/i/gm;Z)Z

    .line 825
    iget-object v0, p0, Lcom/peel/i/ij;->a:Lcom/peel/i/gm;

    invoke-static {v0}, Lcom/peel/i/gm;->H(Lcom/peel/i/gm;)Landroid/widget/ListView;

    move-result-object v0

    invoke-static {}, Lcom/peel/i/gm;->c()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/peel/util/bx;->a(Landroid/view/View;Ljava/lang/String;)V

    .line 826
    iget-object v1, p0, Lcom/peel/i/ij;->a:Lcom/peel/i/gm;

    invoke-virtual {p1}, Landroid/widget/AdapterView;->getAdapter()Landroid/widget/Adapter;

    move-result-object v0

    invoke-interface {v0, p3}, Landroid/widget/Adapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/f/a;

    invoke-static {v1, v0}, Lcom/peel/i/gm;->a(Lcom/peel/i/gm;Lcom/peel/f/a;)Lcom/peel/f/a;

    .line 827
    iget-object v0, p0, Lcom/peel/i/ij;->a:Lcom/peel/i/gm;

    invoke-static {v0}, Lcom/peel/i/gm;->J(Lcom/peel/i/gm;)Lcom/peel/i/a/a;

    move-result-object v0

    invoke-virtual {v0, p3}, Lcom/peel/i/a/a;->b(I)V

    .line 828
    iget-object v0, p0, Lcom/peel/i/ij;->a:Lcom/peel/i/gm;

    invoke-static {v0}, Lcom/peel/i/gm;->I(Lcom/peel/i/gm;)Landroid/widget/AutoCompleteTextView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/AutoCompleteTextView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 829
    iget-object v0, p0, Lcom/peel/i/ij;->a:Lcom/peel/i/gm;

    invoke-virtual {v0}, Lcom/peel/i/gm;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    const-string/jumbo v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/ae;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 830
    iget-object v1, p0, Lcom/peel/i/ij;->a:Lcom/peel/i/gm;

    invoke-virtual {v1}, Lcom/peel/i/gm;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/ae;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 832
    :cond_0
    iget-object v0, p0, Lcom/peel/i/ij;->a:Lcom/peel/i/gm;

    iget-object v1, p0, Lcom/peel/i/ij;->a:Lcom/peel/i/gm;

    invoke-virtual {v1}, Lcom/peel/i/gm;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    iget-object v2, p0, Lcom/peel/i/ij;->a:Lcom/peel/i/gm;

    invoke-static {v2}, Lcom/peel/i/gm;->D(Lcom/peel/i/gm;)I

    move-result v2

    invoke-static {v1, v2}, Lcom/peel/util/bx;->c(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/peel/i/ij;->a:Lcom/peel/i/gm;

    invoke-static {v2}, Lcom/peel/i/gm;->K(Lcom/peel/i/gm;)Z

    move-result v2

    invoke-static {v0, v1, v3, v2}, Lcom/peel/i/gm;->a(Lcom/peel/i/gm;Ljava/lang/String;ZZ)V

    .line 833
    return-void
.end method
