.class public Lcom/peel/i/ep;
.super Lcom/peel/d/u;


# static fields
.field private static final e:Ljava/lang/String;


# instance fields
.field private aj:Landroid/widget/ListView;

.field private ak:Landroid/widget/RelativeLayout;

.field private al:Lcom/peel/widget/ag;

.field private am:Landroid/app/ProgressDialog;

.field private an:Landroid/view/View;

.field private ao:Landroid/os/Bundle;

.field private ap:Ljava/lang/String;

.field private aq:I

.field private ar:Landroid/widget/LinearLayout;

.field private f:Landroid/view/LayoutInflater;

.field private g:Landroid/widget/LinearLayout;

.field private h:Landroid/widget/EditText;

.field private i:Landroid/widget/TextView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 60
    const-class v0, Lcom/peel/i/ep;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/peel/i/ep;->e:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 59
    invoke-direct {p0}, Lcom/peel/d/u;-><init>()V

    .line 70
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/peel/i/ep;->al:Lcom/peel/widget/ag;

    .line 76
    const/4 v0, -0x1

    iput v0, p0, Lcom/peel/i/ep;->aq:I

    return-void
.end method

.method static synthetic T()Ljava/lang/String;
    .locals 1

    .prologue
    .line 59
    sget-object v0, Lcom/peel/i/ep;->e:Ljava/lang/String;

    return-object v0
.end method

.method private U()V
    .locals 8

    .prologue
    const/4 v5, 0x0

    .line 469
    iget-object v0, p0, Lcom/peel/i/ep;->ao:Landroid/os/Bundle;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/peel/i/ep;->ap:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/peel/i/ep;->aq:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    sget-boolean v0, Lcom/peel/util/b/a;->a:Z

    if-nez v0, :cond_0

    .line 470
    invoke-static {}, Lcom/peel/util/a/f;->a()Lcom/peel/util/a/f;

    move-result-object v0

    sget-object v1, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v1}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v1

    if-nez v1, :cond_1

    const/4 v1, 0x1

    :goto_0
    const/16 v2, 0xbc6

    const/16 v3, 0x7d5

    iget-object v4, p0, Lcom/peel/i/ep;->ao:Landroid/os/Bundle;

    const-string/jumbo v6, "mso"

    invoke-virtual {v4, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iget-object v6, p0, Lcom/peel/i/ep;->ap:Ljava/lang/String;

    move v7, v5

    invoke-virtual/range {v0 .. v7}, Lcom/peel/util/a/f;->a(IIILjava/lang/String;ILjava/lang/String;I)V

    .line 471
    iget-object v0, p0, Lcom/peel/i/ep;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "provider"

    iget-object v2, p0, Lcom/peel/i/ep;->ao:Landroid/os/Bundle;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 472
    iget-object v0, p0, Lcom/peel/i/ep;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "provider"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    const-string/jumbo v1, "location"

    iget-object v2, p0, Lcom/peel/i/ep;->ap:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 473
    invoke-virtual {p0}, Lcom/peel/i/ep;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    const-class v1, Lcom/peel/i/cg;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/peel/i/ep;->c:Landroid/os/Bundle;

    invoke-static {v0, v1, v2}, Lcom/peel/d/e;->c(Landroid/support/v4/app/ae;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 475
    :cond_0
    return-void

    .line 470
    :cond_1
    sget-object v1, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v1}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/data/at;->f()I

    move-result v1

    goto :goto_0
.end method

.method static synthetic a(Lcom/peel/i/ep;I)I
    .locals 0

    .prologue
    .line 59
    iput p1, p0, Lcom/peel/i/ep;->aq:I

    return p1
.end method

.method static synthetic a(Lcom/peel/i/ep;Landroid/app/ProgressDialog;)Landroid/app/ProgressDialog;
    .locals 0

    .prologue
    .line 59
    iput-object p1, p0, Lcom/peel/i/ep;->am:Landroid/app/ProgressDialog;

    return-object p1
.end method

.method static synthetic a(Lcom/peel/i/ep;Landroid/os/Bundle;)Landroid/os/Bundle;
    .locals 0

    .prologue
    .line 59
    iput-object p1, p0, Lcom/peel/i/ep;->ao:Landroid/os/Bundle;

    return-object p1
.end method

.method static synthetic a(Lcom/peel/i/ep;)Landroid/widget/EditText;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/peel/i/ep;->h:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic a(Lcom/peel/i/ep;Landroid/widget/LinearLayout;)Landroid/widget/LinearLayout;
    .locals 0

    .prologue
    .line 59
    iput-object p1, p0, Lcom/peel/i/ep;->ar:Landroid/widget/LinearLayout;

    return-object p1
.end method

.method static synthetic a(Lcom/peel/i/ep;Lcom/peel/widget/ag;)Lcom/peel/widget/ag;
    .locals 0

    .prologue
    .line 59
    iput-object p1, p0, Lcom/peel/i/ep;->al:Lcom/peel/widget/ag;

    return-object p1
.end method

.method static synthetic a(Lcom/peel/i/ep;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 59
    invoke-direct {p0, p1}, Lcom/peel/i/ep;->a(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic a(Lcom/peel/i/ep;Z)V
    .locals 0

    .prologue
    .line 59
    invoke-direct {p0, p1}, Lcom/peel/i/ep;->a(Z)V

    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    .line 235
    sget-object v0, Lcom/peel/content/a;->h:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/peel/i/ep;->v()Landroid/view/View;

    move-result-object v0

    if-nez v0, :cond_1

    .line 363
    :cond_0
    :goto_0
    return-void

    .line 236
    :cond_1
    sget-boolean v0, Lcom/peel/util/b/a;->a:Z

    if-nez v0, :cond_0

    .line 238
    invoke-virtual {p0}, Lcom/peel/i/ep;->c()V

    .line 240
    invoke-static {}, Lcom/peel/util/a/f;->a()Lcom/peel/util/a/f;

    move-result-object v2

    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v0}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v0

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    const/16 v3, 0xbc3

    const/16 v4, 0x7d5

    invoke-virtual {v2, v0, v3, v4, p1}, Lcom/peel/util/a/f;->a(IIILjava/lang/String;)V

    .line 243
    iget-object v0, p0, Lcom/peel/i/ep;->c:Landroid/os/Bundle;

    const-string/jumbo v2, "country"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    .line 244
    const-string/jumbo v2, "iso"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/peel/i/et;

    invoke-direct {v3, p0, v1, v0, p1}, Lcom/peel/i/et;-><init>(Lcom/peel/i/ep;ILandroid/os/Bundle;Ljava/lang/String;)V

    invoke-static {p1, v2, v3}, Lcom/peel/content/a/j;->a(Ljava/lang/String;Ljava/lang/String;Lcom/peel/util/t;)V

    goto :goto_0

    .line 240
    :cond_2
    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v0}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/data/at;->f()I

    move-result v0

    goto :goto_1
.end method

.method private a(Z)V
    .locals 7

    .prologue
    .line 394
    .line 395
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 396
    sget v0, Lcom/peel/ui/fp;->menu_next:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 397
    new-instance v0, Lcom/peel/d/a;

    sget-object v1, Lcom/peel/d/d;->b:Lcom/peel/d/d;

    sget-object v2, Lcom/peel/d/b;->a:Lcom/peel/d/b;

    sget-object v3, Lcom/peel/d/c;->b:Lcom/peel/d/c;

    sget v4, Lcom/peel/ui/ft;->find_your_tv_service:I

    invoke-virtual {p0, v4}, Lcom/peel/i/ep;->a(I)Ljava/lang/String;

    move-result-object v4

    move v5, p1

    invoke-direct/range {v0 .. v6}, Lcom/peel/d/a;-><init>(Lcom/peel/d/d;Lcom/peel/d/b;Lcom/peel/d/c;Ljava/lang/String;ZLjava/util/List;)V

    iput-object v0, p0, Lcom/peel/i/ep;->d:Lcom/peel/d/a;

    .line 398
    invoke-virtual {p0}, Lcom/peel/i/ep;->Z()V

    .line 400
    return-void
.end method

.method static synthetic b(Lcom/peel/i/ep;)Landroid/widget/ListView;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/peel/i/ep;->aj:Landroid/widget/ListView;

    return-object v0
.end method

.method static synthetic b(Lcom/peel/i/ep;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 59
    iput-object p1, p0, Lcom/peel/i/ep;->ap:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic c(Lcom/peel/i/ep;)Landroid/view/View;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/peel/i/ep;->an:Landroid/view/View;

    return-object v0
.end method

.method static synthetic d(Lcom/peel/i/ep;)Landroid/widget/LinearLayout;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/peel/i/ep;->ar:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic e(Lcom/peel/i/ep;)Landroid/view/LayoutInflater;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/peel/i/ep;->f:Landroid/view/LayoutInflater;

    return-object v0
.end method

.method static synthetic f(Lcom/peel/i/ep;)Lcom/peel/widget/ag;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/peel/i/ep;->al:Lcom/peel/widget/ag;

    return-object v0
.end method

.method static synthetic g(Lcom/peel/i/ep;)I
    .locals 1

    .prologue
    .line 59
    iget v0, p0, Lcom/peel/i/ep;->aq:I

    return v0
.end method

.method static synthetic h(Lcom/peel/i/ep;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/peel/i/ep;->i:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic i(Lcom/peel/i/ep;)V
    .locals 0

    .prologue
    .line 59
    invoke-direct {p0}, Lcom/peel/i/ep;->U()V

    return-void
.end method

.method static synthetic j(Lcom/peel/i/ep;)Landroid/app/ProgressDialog;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/peel/i/ep;->am:Landroid/app/ProgressDialog;

    return-object v0
.end method


# virtual methods
.method public S()V
    .locals 3

    .prologue
    .line 443
    new-instance v0, Lcom/peel/i/fb;

    invoke-direct {v0, p0}, Lcom/peel/i/fb;-><init>(Lcom/peel/i/ep;)V

    .line 451
    invoke-static {}, Lcom/peel/util/i;->c()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 452
    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 456
    :goto_0
    return-void

    .line 454
    :cond_0
    const-class v1, Lcom/peel/i/ep;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "dismiss loading"

    invoke-static {v1, v2, v0}, Lcom/peel/util/i;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/lang/Runnable;

    goto :goto_0
.end method

.method public Z()V
    .locals 2

    .prologue
    .line 405
    iget-object v0, p0, Lcom/peel/i/ep;->d:Lcom/peel/d/a;

    if-nez v0, :cond_0

    .line 410
    :goto_0
    return-void

    .line 408
    :cond_0
    iget-object v0, p0, Lcom/peel/i/ep;->b:Lcom/peel/d/i;

    iget-object v1, p0, Lcom/peel/i/ep;->d:Lcom/peel/d/a;

    invoke-virtual {v0, v1}, Lcom/peel/d/i;->a(Lcom/peel/d/a;)V

    goto :goto_0
.end method

.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    .line 80
    iput-object p1, p0, Lcom/peel/i/ep;->f:Landroid/view/LayoutInflater;

    .line 81
    sget v0, Lcom/peel/ui/fq;->setup_zip:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/peel/i/ep;->g:Landroid/widget/LinearLayout;

    .line 82
    iget-object v0, p0, Lcom/peel/i/ep;->g:Landroid/widget/LinearLayout;

    sget v1, Lcom/peel/ui/fp;->zipcode_txt:I

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/peel/i/ep;->h:Landroid/widget/EditText;

    .line 83
    iget-object v0, p0, Lcom/peel/i/ep;->g:Landroid/widget/LinearLayout;

    sget v1, Lcom/peel/ui/fp;->help_txt:I

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/peel/i/ep;->i:Landroid/widget/TextView;

    .line 84
    iget-object v0, p0, Lcom/peel/i/ep;->g:Landroid/widget/LinearLayout;

    sget v1, Lcom/peel/ui/fp;->tv_service_list:I

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/peel/i/ep;->aj:Landroid/widget/ListView;

    .line 85
    iget-object v0, p0, Lcom/peel/i/ep;->g:Landroid/widget/LinearLayout;

    sget v1, Lcom/peel/ui/fp;->zip_separator:I

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/i/ep;->an:Landroid/view/View;

    .line 86
    iget-object v0, p0, Lcom/peel/i/ep;->g:Landroid/widget/LinearLayout;

    sget v1, Lcom/peel/ui/fp;->search_row_layout:I

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/peel/i/ep;->ak:Landroid/widget/RelativeLayout;

    .line 88
    iget-object v0, p0, Lcom/peel/i/ep;->h:Landroid/widget/EditText;

    new-instance v1, Lcom/peel/i/eq;

    invoke-direct {v1, p0}, Lcom/peel/i/eq;-><init>(Lcom/peel/i/ep;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 109
    iget-object v0, p0, Lcom/peel/i/ep;->g:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method public a(Landroid/view/MenuItem;)Z
    .locals 2

    .prologue
    .line 461
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    .line 462
    sget v1, Lcom/peel/ui/fp;->menu_next:I

    if-ne v0, v1, :cond_0

    .line 463
    invoke-direct {p0}, Lcom/peel/i/ep;->U()V

    .line 465
    :cond_0
    invoke-super {p0, p1}, Lcom/peel/d/u;->a(Landroid/view/MenuItem;)Z

    move-result v0

    return v0
.end method

.method public c()V
    .locals 3

    .prologue
    .line 413
    invoke-virtual {p0}, Lcom/peel/i/ep;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/peel/i/ep;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/ae;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 440
    :cond_0
    :goto_0
    return-void

    .line 415
    :cond_1
    new-instance v0, Lcom/peel/i/fa;

    invoke-direct {v0, p0}, Lcom/peel/i/fa;-><init>(Lcom/peel/i/ep;)V

    .line 435
    invoke-static {}, Lcom/peel/util/i;->c()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 436
    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    goto :goto_0

    .line 438
    :cond_2
    sget-object v1, Lcom/peel/i/ep;->e:Ljava/lang/String;

    const-string/jumbo v2, "show loading"

    invoke-static {v1, v2, v0}, Lcom/peel/util/i;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/lang/Runnable;

    goto :goto_0
.end method

.method public c(Landroid/os/Bundle;)V
    .locals 9

    .prologue
    const/4 v8, 0x2

    const/16 v7, 0x8

    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 163
    invoke-super {p0, p1}, Lcom/peel/d/u;->c(Landroid/os/Bundle;)V

    .line 165
    sget-object v0, Lcom/peel/content/a;->h:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/peel/i/ep;->v()Landroid/view/View;

    move-result-object v0

    if-nez v0, :cond_1

    .line 230
    :cond_0
    :goto_0
    return-void

    .line 167
    :cond_1
    const-string/jumbo v0, "country"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    .line 169
    if-eqz v0, :cond_2

    const-string/jumbo v1, "iso"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_2

    const-string/jumbo v1, "iso"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "US"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 170
    invoke-virtual {p0}, Lcom/peel/i/ep;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    invoke-static {v1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    const-string/jumbo v2, "is_setup_complete"

    invoke-interface {v1, v2, v4}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-nez v1, :cond_2

    .line 171
    const-string/jumbo v1, "Next Button"

    invoke-virtual {p0}, Lcom/peel/i/ep;->m()Landroid/support/v4/app/ae;

    move-result-object v2

    new-instance v3, Lcom/peel/i/er;

    invoke-direct {v3, p0}, Lcom/peel/i/er;-><init>(Lcom/peel/i/ep;)V

    invoke-static {v1, v2, v3}, Lcom/peel/util/a;->a(Ljava/lang/String;Landroid/content/Context;Lcom/peel/util/t;)V

    .line 182
    const-string/jumbo v1, "selectprovider"

    invoke-virtual {p0}, Lcom/peel/i/ep;->m()Landroid/support/v4/app/ae;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/peel/util/a;->a(Ljava/lang/String;Landroid/content/Context;)V

    .line 185
    :cond_2
    const-string/jumbo v1, "none"

    const-string/jumbo v2, "type"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 186
    iget-object v0, p0, Lcom/peel/i/ep;->h:Landroid/widget/EditText;

    invoke-virtual {v0, v7}, Landroid/widget/EditText;->setVisibility(I)V

    .line 187
    iget-object v0, p0, Lcom/peel/i/ep;->i:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 188
    iget-object v0, p0, Lcom/peel/i/ep;->ak:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v7}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 189
    const-string/jumbo v0, "1234"

    invoke-direct {p0, v0}, Lcom/peel/i/ep;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 191
    :cond_3
    iget-object v1, p0, Lcom/peel/i/ep;->ak:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v4}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 192
    iget-object v1, p0, Lcom/peel/i/ep;->i:Landroid/widget/TextView;

    invoke-virtual {v1, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 193
    iget-object v1, p0, Lcom/peel/i/ep;->h:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->requestFocus()Z

    .line 197
    const-string/jumbo v1, "5digitzip"

    const-string/jumbo v2, "type"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 198
    iget-object v0, p0, Lcom/peel/i/ep;->h:Landroid/widget/EditText;

    sget v1, Lcom/peel/ui/ft;->enter_us_zip:I

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setHint(I)V

    .line 199
    iget-object v0, p0, Lcom/peel/i/ep;->h:Landroid/widget/EditText;

    invoke-virtual {v0, v8}, Landroid/widget/EditText;->setInputType(I)V

    .line 200
    iget-object v0, p0, Lcom/peel/i/ep;->h:Landroid/widget/EditText;

    new-array v1, v6, [Landroid/text/InputFilter;

    new-instance v2, Landroid/text/InputFilter$LengthFilter;

    const/4 v3, 0x5

    invoke-direct {v2, v3}, Landroid/text/InputFilter$LengthFilter;-><init>(I)V

    aput-object v2, v1, v4

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    .line 213
    :goto_1
    const-string/jumbo v0, "zipCode"

    invoke-virtual {p1, v0, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 214
    const-string/jumbo v0, "zipCode"

    invoke-virtual {p1, v0, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_6

    .line 215
    iget-object v0, p0, Lcom/peel/i/ep;->h:Landroid/widget/EditText;

    const-string/jumbo v1, "zipCode"

    invoke-virtual {p1, v1, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 216
    iget-object v0, p0, Lcom/peel/i/ep;->h:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/peel/i/ep;->h:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setSelection(I)V

    .line 227
    :cond_4
    :goto_2
    const-string/jumbo v0, "zipCode"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "zipCode"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 228
    const-string/jumbo v0, "zipCode"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/peel/i/ep;->a(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 202
    :cond_5
    iget-object v0, p0, Lcom/peel/i/ep;->h:Landroid/widget/EditText;

    sget v1, Lcom/peel/ui/ft;->enter_postal_code:I

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setHint(I)V

    .line 203
    iget-object v0, p0, Lcom/peel/i/ep;->h:Landroid/widget/EditText;

    invoke-virtual {v0, v6}, Landroid/widget/EditText;->setInputType(I)V

    .line 204
    iget-object v0, p0, Lcom/peel/i/ep;->h:Landroid/widget/EditText;

    new-array v1, v8, [Landroid/text/InputFilter;

    new-instance v2, Landroid/text/InputFilter$LengthFilter;

    const/16 v3, 0xc

    invoke-direct {v2, v3}, Landroid/text/InputFilter$LengthFilter;-><init>(I)V

    aput-object v2, v1, v4

    new-instance v2, Lcom/peel/i/es;

    invoke-direct {v2, p0}, Lcom/peel/i/es;-><init>(Lcom/peel/i/ep;)V

    aput-object v2, v1, v6

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    goto :goto_1

    .line 223
    :cond_6
    iget-object v0, p0, Lcom/peel/i/ep;->h:Landroid/widget/EditText;

    const-string/jumbo v1, "zipCode"

    invoke-virtual {p1, v1, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 224
    iget-object v0, p0, Lcom/peel/i/ep;->h:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/peel/i/ep;->h:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setSelection(I)V

    goto :goto_2
.end method

.method public d(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 114
    invoke-super {p0, p1}, Lcom/peel/d/u;->d(Landroid/os/Bundle;)V

    .line 115
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/peel/i/ep;->a(Z)V

    .line 116
    return-void
.end method

.method public e()V
    .locals 0

    .prologue
    .line 120
    invoke-super {p0}, Lcom/peel/d/u;->e()V

    .line 132
    return-void
.end method

.method public e(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 389
    iget-object v0, p0, Lcom/peel/i/ep;->c:Landroid/os/Bundle;

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->putAll(Landroid/os/Bundle;)V

    .line 390
    invoke-super {p0, p1}, Lcom/peel/d/u;->e(Landroid/os/Bundle;)V

    .line 391
    return-void
.end method

.method public f()V
    .locals 3

    .prologue
    .line 142
    invoke-super {p0}, Lcom/peel/d/u;->f()V

    .line 143
    invoke-virtual {p0}, Lcom/peel/i/ep;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    const-string/jumbo v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/ae;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 144
    iget-object v1, p0, Lcom/peel/i/ep;->h:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 145
    iget-object v0, p0, Lcom/peel/i/ep;->al:Lcom/peel/widget/ag;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/peel/i/ep;->al:Lcom/peel/widget/ag;

    invoke-virtual {v0}, Lcom/peel/widget/ag;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 146
    iget-object v0, p0, Lcom/peel/i/ep;->al:Lcom/peel/widget/ag;

    invoke-virtual {v0}, Lcom/peel/widget/ag;->dismiss()V

    .line 148
    :cond_0
    return-void
.end method

.method public h(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 151
    invoke-super {p0, p1}, Lcom/peel/d/u;->h(Landroid/os/Bundle;)V

    .line 152
    if-eqz p1, :cond_0

    .line 153
    iget-object v0, p0, Lcom/peel/i/ep;->c:Landroid/os/Bundle;

    invoke-virtual {v0, p1}, Landroid/os/Bundle;->putAll(Landroid/os/Bundle;)V

    .line 156
    :cond_0
    invoke-static {}, Lcom/peel/util/a/f;->a()Lcom/peel/util/a/f;

    move-result-object v1

    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v0}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v0

    if-nez v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    const/16 v2, 0xbc2

    const/16 v3, 0x7d5

    invoke-virtual {v1, v0, v2, v3}, Lcom/peel/util/a/f;->a(III)V

    .line 158
    sget-object v0, Lcom/peel/content/a;->h:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/peel/i/ep;->c:Landroid/os/Bundle;

    invoke-virtual {p0, v0}, Lcom/peel/i/ep;->c(Landroid/os/Bundle;)V

    .line 159
    :cond_1
    return-void

    .line 156
    :cond_2
    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v0}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/data/at;->f()I

    move-result v0

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 367
    sget-object v0, Lcom/peel/content/a;->h:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/peel/i/ep;->b:Lcom/peel/d/i;

    if-nez v0, :cond_1

    .line 385
    :cond_0
    :goto_0
    return-void

    .line 369
    :cond_1
    sget v0, Lcom/peel/ui/fp;->search_btn:I

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    if-ne v0, v1, :cond_0

    .line 371
    :try_start_0
    invoke-virtual {p0}, Lcom/peel/i/ep;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    const-string/jumbo v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/ae;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 372
    iget-object v1, p0, Lcom/peel/i/ep;->h:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 378
    :goto_1
    iget-object v0, p0, Lcom/peel/i/ep;->h:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_2

    .line 379
    iget-object v0, p0, Lcom/peel/i/ep;->h:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, " "

    const-string/jumbo v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/peel/i/ep;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 382
    :cond_2
    invoke-virtual {p0}, Lcom/peel/i/ep;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    sget v1, Lcom/peel/ui/ft;->empty_zip_code:I

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 373
    :catch_0
    move-exception v0

    goto :goto_1
.end method

.method public w()V
    .locals 0

    .prologue
    .line 136
    invoke-super {p0}, Lcom/peel/d/u;->w()V

    .line 137
    invoke-virtual {p0}, Lcom/peel/i/ep;->Z()V

    .line 138
    return-void
.end method
