.class final Lcom/peel/i/ad;
.super Landroid/support/v4/view/av;


# instance fields
.field final synthetic a:Lcom/peel/i/a;

.field private b:I

.field private c:I


# direct methods
.method public constructor <init>(Lcom/peel/i/a;II)V
    .locals 0

    .prologue
    .line 1082
    iput-object p1, p0, Lcom/peel/i/ad;->a:Lcom/peel/i/a;

    invoke-direct {p0}, Landroid/support/v4/view/av;-><init>()V

    .line 1083
    iput p3, p0, Lcom/peel/i/ad;->b:I

    .line 1084
    iput p2, p0, Lcom/peel/i/ad;->c:I

    .line 1085
    return-void
.end method


# virtual methods
.method public a(Landroid/view/ViewGroup;I)Ljava/lang/Object;
    .locals 10

    .prologue
    const/4 v4, 0x2

    const/4 v9, 0x1

    const v8, 0x106000d

    const/4 v7, 0x0

    const/4 v6, -0x2

    .line 1088
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string/jumbo v1, "layout_inflater"

    .line 1089
    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 1091
    iget v1, p0, Lcom/peel/i/ad;->c:I

    if-ne v1, v4, :cond_0

    sget v1, Lcom/peel/ui/fq;->test_other_btn_pagers_view:I

    .line 1093
    :goto_0
    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .line 1094
    sget v0, Lcom/peel/ui/fp;->button_num_text:I

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 1095
    sget v1, Lcom/peel/ui/fp;->button_num_text_small:I

    invoke-virtual {v3, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 1097
    iget v2, p0, Lcom/peel/i/ad;->c:I

    if-ne v2, v4, :cond_1

    .line 1098
    sget v2, Lcom/peel/ui/fp;->test_other_btn_large_view:I

    invoke-virtual {v3, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    sget v4, Lcom/peel/ui/fo;->setup_test_stb_ch_up_btn_states:I

    invoke-virtual {v2, v4}, Landroid/view/View;->setBackgroundResource(I)V

    .line 1099
    sget v2, Lcom/peel/ui/fp;->test_other_btn_small_view:I

    invoke-virtual {v3, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    sget v4, Lcom/peel/ui/fo;->setup_test_stb_ch_up_btn_states:I

    invoke-virtual {v2, v4}, Landroid/view/View;->setBackgroundResource(I)V

    .line 1111
    new-instance v4, Landroid/widget/Button;

    iget-object v2, p0, Lcom/peel/i/ad;->a:Lcom/peel/i/a;

    invoke-virtual {v2}, Lcom/peel/i/a;->m()Landroid/support/v4/app/ae;

    move-result-object v2

    invoke-direct {v4, v2}, Landroid/widget/Button;-><init>(Landroid/content/Context;)V

    .line 1112
    new-instance v2, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v2, v6, v6}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1113
    const/16 v5, 0x66

    invoke-static {v5}, Lcom/peel/util/dw;->a(I)I

    move-result v5

    iput v5, v2, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    .line 1114
    const/16 v5, 0x69

    invoke-static {v5}, Lcom/peel/util/dw;->a(I)I

    move-result v5

    iput v5, v2, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    .line 1115
    invoke-virtual {v4, v2}, Landroid/widget/Button;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1117
    iget-object v2, p0, Lcom/peel/i/ad;->a:Lcom/peel/i/a;

    invoke-virtual {v2}, Lcom/peel/i/a;->n()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v8}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v4, v2}, Landroid/widget/Button;->setBackgroundColor(I)V

    .line 1118
    iget-object v2, p0, Lcom/peel/i/ad;->a:Lcom/peel/i/a;

    sget v5, Lcom/peel/ui/ft;->channel:I

    invoke-virtual {v2, v5}, Lcom/peel/i/a;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Landroid/widget/Button;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1120
    new-instance v2, Lcom/peel/i/ae;

    invoke-direct {v2, p0}, Lcom/peel/i/ae;-><init>(Lcom/peel/i/ad;)V

    invoke-virtual {v4, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1127
    sget v2, Lcom/peel/ui/fp;->test_other_btn_large_view:I

    invoke-virtual {v3, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v4, v7}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;I)V

    .line 1139
    new-instance v4, Landroid/widget/Button;

    iget-object v2, p0, Lcom/peel/i/ad;->a:Lcom/peel/i/a;

    invoke-virtual {v2}, Lcom/peel/i/a;->m()Landroid/support/v4/app/ae;

    move-result-object v2

    invoke-direct {v4, v2}, Landroid/widget/Button;-><init>(Landroid/content/Context;)V

    .line 1140
    new-instance v2, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v2, v6, v6}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1141
    const/16 v5, 0x47

    invoke-static {v5}, Lcom/peel/util/dw;->a(I)I

    move-result v5

    iput v5, v2, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    .line 1142
    const/16 v5, 0x48

    invoke-static {v5}, Lcom/peel/util/dw;->a(I)I

    move-result v5

    iput v5, v2, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    .line 1143
    invoke-virtual {v4, v2}, Landroid/widget/Button;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1145
    iget-object v2, p0, Lcom/peel/i/ad;->a:Lcom/peel/i/a;

    invoke-virtual {v2}, Lcom/peel/i/a;->n()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v8}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v4, v2}, Landroid/widget/Button;->setBackgroundColor(I)V

    .line 1146
    iget-object v2, p0, Lcom/peel/i/ad;->a:Lcom/peel/i/a;

    sget v5, Lcom/peel/ui/ft;->channel:I

    invoke-virtual {v2, v5}, Lcom/peel/i/a;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Landroid/widget/Button;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1148
    new-instance v2, Lcom/peel/i/af;

    invoke-direct {v2, p0}, Lcom/peel/i/af;-><init>(Lcom/peel/i/ad;)V

    invoke-virtual {v4, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1155
    sget v2, Lcom/peel/ui/fp;->test_other_btn_small_view:I

    invoke-virtual {v3, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v4, v7}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;I)V

    .line 1217
    :goto_1
    iget-object v2, p0, Lcom/peel/i/ad;->a:Lcom/peel/i/a;

    invoke-virtual {v2}, Lcom/peel/i/a;->n()Landroid/content/res/Resources;

    move-result-object v2

    sget v4, Lcom/peel/ui/ft;->testing_btn_number:I

    new-array v5, v9, [Ljava/lang/Object;

    add-int/lit8 v6, p2, 0x1

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-virtual {v2, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1218
    iget-object v0, p0, Lcom/peel/i/ad;->a:Lcom/peel/i/a;

    invoke-virtual {v0}, Lcom/peel/i/a;->n()Landroid/content/res/Resources;

    move-result-object v0

    sget v2, Lcom/peel/ui/ft;->testing_btn_number:I

    new-array v4, v9, [Ljava/lang/Object;

    add-int/lit8 v5, p2, 0x1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v7

    invoke-virtual {v0, v2, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1220
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "btnView"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 1221
    invoke-virtual {p1, v3}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 1223
    return-object v3

    .line 1091
    :cond_0
    sget v1, Lcom/peel/ui/fq;->test_pw_btn_pagers_view:I

    goto/16 :goto_0

    .line 1157
    :cond_1
    sget v2, Lcom/peel/ui/fp;->test_pw_btn_large_view:I

    invoke-virtual {v3, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    sget v4, Lcom/peel/ui/fo;->initial_tv_power_onoff_stateful:I

    invoke-virtual {v2, v4}, Landroid/view/View;->setBackgroundResource(I)V

    .line 1158
    sget v2, Lcom/peel/ui/fp;->test_pw_btn_small_view:I

    invoke-virtual {v3, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    sget v4, Lcom/peel/ui/fo;->initial_tv_power_onoff_stateful:I

    invoke-virtual {v2, v4}, Landroid/view/View;->setBackgroundResource(I)V

    .line 1170
    new-instance v4, Landroid/widget/Button;

    iget-object v2, p0, Lcom/peel/i/ad;->a:Lcom/peel/i/a;

    invoke-virtual {v2}, Lcom/peel/i/a;->m()Landroid/support/v4/app/ae;

    move-result-object v2

    invoke-direct {v4, v2}, Landroid/widget/Button;-><init>(Landroid/content/Context;)V

    .line 1171
    new-instance v2, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v2, v6, v6}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1172
    const/16 v5, 0x66

    invoke-static {v5}, Lcom/peel/util/dw;->a(I)I

    move-result v5

    iput v5, v2, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    .line 1173
    const/16 v5, 0x69

    invoke-static {v5}, Lcom/peel/util/dw;->a(I)I

    move-result v5

    iput v5, v2, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    .line 1174
    invoke-virtual {v4, v2}, Landroid/widget/Button;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1176
    iget-object v2, p0, Lcom/peel/i/ad;->a:Lcom/peel/i/a;

    invoke-virtual {v2}, Lcom/peel/i/a;->n()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v8}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v4, v2}, Landroid/widget/Button;->setBackgroundColor(I)V

    .line 1177
    iget-object v2, p0, Lcom/peel/i/ad;->a:Lcom/peel/i/a;

    sget v5, Lcom/peel/ui/ft;->power:I

    invoke-virtual {v2, v5}, Lcom/peel/i/a;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Landroid/widget/Button;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1179
    new-instance v2, Lcom/peel/i/ag;

    invoke-direct {v2, p0}, Lcom/peel/i/ag;-><init>(Lcom/peel/i/ad;)V

    invoke-virtual {v4, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1186
    sget v2, Lcom/peel/ui/fp;->test_pw_btn_large_view:I

    invoke-virtual {v3, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v4, v7}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;I)V

    .line 1198
    new-instance v4, Landroid/widget/Button;

    iget-object v2, p0, Lcom/peel/i/ad;->a:Lcom/peel/i/a;

    invoke-virtual {v2}, Lcom/peel/i/a;->m()Landroid/support/v4/app/ae;

    move-result-object v2

    invoke-direct {v4, v2}, Landroid/widget/Button;-><init>(Landroid/content/Context;)V

    .line 1199
    new-instance v2, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v2, v6, v6}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1200
    const/16 v5, 0x47

    invoke-static {v5}, Lcom/peel/util/dw;->a(I)I

    move-result v5

    iput v5, v2, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    .line 1201
    const/16 v5, 0x48

    invoke-static {v5}, Lcom/peel/util/dw;->a(I)I

    move-result v5

    iput v5, v2, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    .line 1202
    invoke-virtual {v4, v2}, Landroid/widget/Button;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1204
    iget-object v2, p0, Lcom/peel/i/ad;->a:Lcom/peel/i/a;

    invoke-virtual {v2}, Lcom/peel/i/a;->n()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v8}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v4, v2}, Landroid/widget/Button;->setBackgroundColor(I)V

    .line 1205
    iget-object v2, p0, Lcom/peel/i/ad;->a:Lcom/peel/i/a;

    sget v5, Lcom/peel/ui/ft;->power:I

    invoke-virtual {v2, v5}, Lcom/peel/i/a;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Landroid/widget/Button;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1207
    new-instance v2, Lcom/peel/i/ah;

    invoke-direct {v2, p0}, Lcom/peel/i/ah;-><init>(Lcom/peel/i/ad;)V

    invoke-virtual {v4, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1214
    sget v2, Lcom/peel/ui/fp;->test_pw_btn_small_view:I

    invoke-virtual {v3, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v4, v7}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;I)V

    goto/16 :goto_1
.end method

.method public a(Landroid/view/ViewGroup;ILjava/lang/Object;)V
    .locals 0

    .prologue
    .line 1228
    check-cast p3, Landroid/view/View;

    invoke-virtual {p1, p3}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 1229
    return-void
.end method

.method public a(Landroid/view/View;Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 1233
    check-cast p2, Landroid/view/View;

    if-ne p1, p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()I
    .locals 1

    .prologue
    .line 1238
    iget v0, p0, Lcom/peel/i/ad;->b:I

    return v0
.end method
