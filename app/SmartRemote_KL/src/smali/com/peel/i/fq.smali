.class public Lcom/peel/i/fq;
.super Lcom/peel/d/u;


# instance fields
.field private e:Landroid/view/View;

.field private f:Landroid/widget/EditText;

.field private g:Lcom/peel/widget/ag;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0}, Lcom/peel/d/u;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/peel/i/fq;)Landroid/widget/EditText;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/peel/i/fq;->f:Landroid/widget/EditText;

    return-object v0
.end method


# virtual methods
.method public Z()V
    .locals 6

    .prologue
    .line 203
    iget-object v0, p0, Lcom/peel/i/fq;->d:Lcom/peel/d/a;

    if-nez v0, :cond_0

    .line 204
    new-instance v0, Lcom/peel/d/a;

    sget-object v1, Lcom/peel/d/d;->b:Lcom/peel/d/d;

    sget-object v2, Lcom/peel/d/b;->a:Lcom/peel/d/b;

    sget-object v3, Lcom/peel/d/c;->b:Lcom/peel/d/c;

    sget v4, Lcom/peel/ui/ft;->label_add_room:I

    invoke-virtual {p0, v4}, Lcom/peel/i/fq;->a(I)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, Lcom/peel/d/a;-><init>(Lcom/peel/d/d;Lcom/peel/d/b;Lcom/peel/d/c;Ljava/lang/String;Ljava/util/List;)V

    iput-object v0, p0, Lcom/peel/i/fq;->d:Lcom/peel/d/a;

    .line 206
    :cond_0
    iget-object v0, p0, Lcom/peel/i/fq;->b:Lcom/peel/d/i;

    iget-object v1, p0, Lcom/peel/i/fq;->d:Lcom/peel/d/a;

    invoke-virtual {v0, v1}, Lcom/peel/d/i;->a(Lcom/peel/d/a;)V

    .line 207
    return-void
.end method

.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 48
    sget v0, Lcom/peel/ui/fq;->setup_name:I

    invoke-virtual {p1, v0, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/i/fq;->e:Landroid/view/View;

    .line 49
    iget-object v0, p0, Lcom/peel/i/fq;->e:Landroid/view/View;

    sget v1, Lcom/peel/ui/fp;->room_name:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/peel/i/fq;->f:Landroid/widget/EditText;

    .line 50
    iget-object v0, p0, Lcom/peel/i/fq;->e:Landroid/view/View;

    sget v1, Lcom/peel/ui/fp;->room_next:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 51
    invoke-virtual {v0, v2}, Landroid/view/View;->setEnabled(Z)V

    .line 52
    invoke-virtual {p0}, Lcom/peel/i/fq;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    invoke-static {v1}, Lcom/peel/util/g;->a(Landroid/app/Activity;)V

    .line 53
    iget-object v1, p0, Lcom/peel/i/fq;->f:Landroid/widget/EditText;

    new-instance v2, Lcom/peel/i/fr;

    invoke-direct {v2, p0, v0}, Lcom/peel/i/fr;-><init>(Lcom/peel/i/fq;Landroid/view/View;)V

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 70
    iget-object v1, p0, Lcom/peel/i/fq;->f:Landroid/widget/EditText;

    new-instance v2, Lcom/peel/i/fs;

    invoke-direct {v2, p0, v0}, Lcom/peel/i/fs;-><init>(Lcom/peel/i/fq;Landroid/view/View;)V

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 86
    iget-object v0, p0, Lcom/peel/i/fq;->e:Landroid/view/View;

    return-object v0
.end method

.method public d(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 99
    invoke-super {p0, p1}, Lcom/peel/d/u;->d(Landroid/os/Bundle;)V

    .line 100
    invoke-virtual {p0}, Lcom/peel/i/fq;->Z()V

    .line 101
    return-void
.end method

.method public e()V
    .locals 6

    .prologue
    .line 122
    invoke-super {p0}, Lcom/peel/d/u;->e()V

    .line 123
    const-class v0, Lcom/peel/i/fq;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "show keyboard after delay"

    new-instance v2, Lcom/peel/i/ft;

    invoke-direct {v2, p0}, Lcom/peel/i/ft;-><init>(Lcom/peel/i/fq;)V

    const-wide/16 v4, 0x64

    invoke-static {v0, v1, v2, v4, v5}, Lcom/peel/util/i;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;J)Ljava/lang/Runnable;

    .line 131
    return-void
.end method

.method public e(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 188
    iget-object v0, p0, Lcom/peel/i/fq;->c:Landroid/os/Bundle;

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->putAll(Landroid/os/Bundle;)V

    .line 189
    invoke-super {p0, p1}, Lcom/peel/d/u;->e(Landroid/os/Bundle;)V

    .line 190
    return-void
.end method

.method public f()V
    .locals 3

    .prologue
    .line 90
    invoke-super {p0}, Lcom/peel/d/u;->f()V

    .line 91
    invoke-virtual {p0}, Lcom/peel/i/fq;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    const-string/jumbo v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/ae;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 92
    invoke-virtual {p0}, Lcom/peel/i/fq;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/ae;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 93
    iget-object v0, p0, Lcom/peel/i/fq;->g:Lcom/peel/widget/ag;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/peel/i/fq;->g:Lcom/peel/widget/ag;

    invoke-virtual {v0}, Lcom/peel/widget/ag;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 94
    iget-object v0, p0, Lcom/peel/i/fq;->g:Lcom/peel/widget/ag;

    invoke-virtual {v0}, Lcom/peel/widget/ag;->dismiss()V

    .line 96
    :cond_0
    return-void
.end method

.method public h(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 105
    invoke-super {p0, p1}, Lcom/peel/d/u;->h(Landroid/os/Bundle;)V

    .line 106
    if-eqz p1, :cond_0

    .line 107
    iget-object v0, p0, Lcom/peel/i/fq;->c:Landroid/os/Bundle;

    invoke-virtual {v0, p1}, Landroid/os/Bundle;->putAll(Landroid/os/Bundle;)V

    .line 110
    :cond_0
    iget-object v0, p0, Lcom/peel/i/fq;->f:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    .line 111
    iget-object v0, p0, Lcom/peel/i/fq;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "room_name"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->remove(Ljava/lang/String;)V

    .line 112
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v1, 0x0

    .line 135
    sget-object v0, Lcom/peel/content/a;->h:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/peel/i/fq;->b:Lcom/peel/d/i;

    if-nez v0, :cond_1

    .line 184
    :cond_0
    :goto_0
    return-void

    .line 137
    :cond_1
    sget v0, Lcom/peel/ui/fp;->room_next:I

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    if-ne v0, v2, :cond_0

    .line 138
    iget-object v0, p0, Lcom/peel/i/fq;->f:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    .line 139
    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v0}, Lcom/peel/control/am;->c()[Lcom/peel/control/RoomControl;

    move-result-object v3

    .line 140
    array-length v4, v3

    move v0, v1

    :goto_1
    if-ge v0, v4, :cond_3

    aget-object v5, v3, v0

    .line 141
    invoke-virtual {v5}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v5

    invoke-virtual {v5}, Lcom/peel/data/at;->a()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 142
    new-instance v0, Lcom/peel/widget/ag;

    invoke-virtual {p0}, Lcom/peel/i/fq;->m()Landroid/support/v4/app/ae;

    move-result-object v3

    invoke-direct {v0, v3}, Lcom/peel/widget/ag;-><init>(Landroid/content/Context;)V

    sget v3, Lcom/peel/ui/ft;->room_name_exists_title:I

    invoke-virtual {v0, v3}, Lcom/peel/widget/ag;->a(I)Lcom/peel/widget/ag;

    move-result-object v0

    invoke-virtual {p0}, Lcom/peel/i/fq;->n()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/peel/ui/ft;->room_name_exists_msg:I

    new-array v5, v6, [Ljava/lang/Object;

    aput-object v2, v5, v1

    invoke-virtual {v3, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/peel/widget/ag;->b(Ljava/lang/CharSequence;)Lcom/peel/widget/ag;

    move-result-object v0

    sget v1, Lcom/peel/ui/ft;->ok:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/peel/widget/ag;->a(ILandroid/content/DialogInterface$OnClickListener;)Lcom/peel/widget/ag;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/i/fq;->g:Lcom/peel/widget/ag;

    .line 143
    iget-object v0, p0, Lcom/peel/i/fq;->g:Lcom/peel/widget/ag;

    invoke-virtual {v0}, Lcom/peel/widget/ag;->d()Lcom/peel/widget/ag;

    goto :goto_0

    .line 140
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 148
    :cond_3
    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    .line 149
    iget-object v0, p0, Lcom/peel/i/fq;->c:Landroid/os/Bundle;

    const-string/jumbo v3, "room_name"

    invoke-virtual {v0, v3, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 151
    :cond_4
    invoke-virtual {p0}, Lcom/peel/i/fq;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    const-string/jumbo v2, "input_method"

    invoke-virtual {v0, v2}, Landroid/support/v4/app/ae;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 152
    invoke-virtual {p0}, Lcom/peel/i/fq;->m()Landroid/support/v4/app/ae;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/ae;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v2

    invoke-virtual {v0, v2, v1}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 154
    invoke-virtual {p0}, Lcom/peel/i/fq;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string/jumbo v1, "config_legacy"

    const-string/jumbo v2, "US"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 155
    const-string/jumbo v1, "US"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 156
    const-string/jumbo v0, "name|USA|endpoint|usa|iso|US|type|5digitzip"

    invoke-static {v0}, Lcom/peel/util/bx;->b(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    .line 161
    :goto_2
    const-string/jumbo v1, "iso"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "na"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_5

    const-string/jumbo v1, "iso"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "XX"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 162
    :cond_5
    invoke-virtual {p0}, Lcom/peel/i/fq;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    const-class v1, Lcom/peel/i/bv;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/peel/i/fq;->c:Landroid/os/Bundle;

    invoke-static {v0, v1, v2}, Lcom/peel/d/e;->c(Landroid/support/v4/app/ae;Ljava/lang/String;Landroid/os/Bundle;)V

    goto/16 :goto_0

    .line 158
    :cond_6
    invoke-static {v0}, Lcom/peel/util/bx;->b(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    goto :goto_2

    .line 164
    :cond_7
    const-class v1, Lcom/peel/i/fq;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/peel/i/fq;->m()Landroid/support/v4/app/ae;

    move-result-object v2

    invoke-static {v1, v2, v6}, Lcom/peel/util/bx;->a(Ljava/lang/String;Landroid/app/Activity;Z)V

    .line 165
    const-string/jumbo v1, "iso"

    const-string/jumbo v2, "US"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v1, v2}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/peel/i/fu;

    invoke-direct {v2, p0, v0}, Lcom/peel/i/fu;-><init>(Lcom/peel/i/fq;Landroid/os/Bundle;)V

    invoke-static {v1, v2}, Lcom/peel/content/a/j;->b(Ljava/lang/String;Lcom/peel/util/t;)V

    goto/16 :goto_0
.end method

.method public w()V
    .locals 0

    .prologue
    .line 116
    invoke-super {p0}, Lcom/peel/d/u;->w()V

    .line 117
    invoke-virtual {p0}, Lcom/peel/i/fq;->Z()V

    .line 118
    return-void
.end method
