.class public Lcom/peel/i/fv;
.super Lcom/peel/d/u;


# instance fields
.field private aj:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/peel/backup/MobileDeviceProfile;",
            ">;"
        }
    .end annotation
.end field

.field private ak:Landroid/content/BroadcastReceiver;

.field private e:Landroid/view/View;

.field private f:Landroid/view/View;

.field private g:Landroid/widget/Button;

.field private h:Landroid/app/ProgressDialog;

.field private i:Landroid/content/SharedPreferences;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 54
    invoke-direct {p0}, Lcom/peel/d/u;-><init>()V

    .line 386
    new-instance v0, Lcom/peel/i/gj;

    invoke-direct {v0, p0}, Lcom/peel/i/gj;-><init>(Lcom/peel/i/fv;)V

    iput-object v0, p0, Lcom/peel/i/fv;->ak:Landroid/content/BroadcastReceiver;

    return-void
.end method

.method private S()V
    .locals 2

    .prologue
    .line 403
    invoke-virtual {p0}, Lcom/peel/i/fv;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    invoke-static {v0}, Lcom/peel/social/w;->f(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 404
    iget-object v0, p0, Lcom/peel/i/fv;->f:Landroid/view/View;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 405
    invoke-direct {p0}, Lcom/peel/i/fv;->T()V

    .line 410
    :goto_0
    invoke-virtual {p0}, Lcom/peel/i/fv;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/ae;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    new-instance v1, Lcom/peel/i/gk;

    invoke-direct {v1, p0}, Lcom/peel/i/gk;-><init>(Lcom/peel/i/fv;)V

    invoke-static {v0, v1}, Lcom/peel/social/w;->a(Landroid/content/Context;Lcom/peel/util/t;)V

    .line 422
    return-void

    .line 407
    :cond_0
    iget-object v0, p0, Lcom/peel/i/fv;->f:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method private T()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 435
    invoke-virtual {p0}, Lcom/peel/i/fv;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    const-string/jumbo v1, "social_accounts_setup"

    invoke-virtual {v0, v1, v3}, Landroid/support/v4/app/ae;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/i/fv;->i:Landroid/content/SharedPreferences;

    .line 437
    iget-object v0, p0, Lcom/peel/i/fv;->a:Ljava/lang/String;

    const-string/jumbo v1, "hide btn"

    new-instance v2, Lcom/peel/i/gl;

    invoke-direct {v2, p0}, Lcom/peel/i/gl;-><init>(Lcom/peel/i/fv;)V

    invoke-static {v0, v1, v2}, Lcom/peel/util/i;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/lang/Runnable;

    .line 444
    invoke-virtual {p0}, Lcom/peel/i/fv;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/ae;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/peel/social/w;->f(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/peel/i/fv;->i:Landroid/content/SharedPreferences;

    const-string/jumbo v1, "import_option"

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_0

    .line 445
    sget v0, Lcom/peel/ui/ft;->login_in:I

    invoke-direct {p0, v0}, Lcom/peel/i/fv;->b(I)V

    .line 447
    new-instance v0, Lcom/peel/backup/c;

    invoke-virtual {p0}, Lcom/peel/i/fv;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/ae;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/peel/backup/c;-><init>(Landroid/content/Context;)V

    .line 448
    new-instance v1, Lcom/peel/i/fz;

    invoke-direct {v1, p0}, Lcom/peel/i/fz;-><init>(Lcom/peel/i/fv;)V

    invoke-virtual {v0, v1}, Lcom/peel/backup/c;->a(Lcom/peel/util/t;)V

    .line 469
    :cond_0
    return-void
.end method

.method static synthetic a(Lcom/peel/i/fv;Landroid/app/ProgressDialog;)Landroid/app/ProgressDialog;
    .locals 0

    .prologue
    .line 54
    iput-object p1, p0, Lcom/peel/i/fv;->h:Landroid/app/ProgressDialog;

    return-object p1
.end method

.method static synthetic a(Lcom/peel/i/fv;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/peel/i/fv;->a:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic a(Lcom/peel/i/fv;Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 0

    .prologue
    .line 54
    iput-object p1, p0, Lcom/peel/i/fv;->aj:Ljava/util/ArrayList;

    return-object p1
.end method

.method private b(I)V
    .locals 3

    .prologue
    .line 148
    iget-object v0, p0, Lcom/peel/i/fv;->a:Ljava/lang/String;

    const-string/jumbo v1, "show progress"

    new-instance v2, Lcom/peel/i/gc;

    invoke-direct {v2, p0, p1}, Lcom/peel/i/gc;-><init>(Lcom/peel/i/fv;I)V

    invoke-static {v0, v1, v2}, Lcom/peel/util/i;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/lang/Runnable;

    .line 167
    return-void
.end method

.method static synthetic b(Lcom/peel/i/fv;)V
    .locals 0

    .prologue
    .line 54
    invoke-direct {p0}, Lcom/peel/i/fv;->c()V

    return-void
.end method

.method private c()V
    .locals 3

    .prologue
    .line 137
    iget-object v0, p0, Lcom/peel/i/fv;->a:Ljava/lang/String;

    const-string/jumbo v1, "show progress"

    new-instance v2, Lcom/peel/i/gb;

    invoke-direct {v2, p0}, Lcom/peel/i/gb;-><init>(Lcom/peel/i/fv;)V

    invoke-static {v0, v1, v2}, Lcom/peel/util/i;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/lang/Runnable;

    .line 145
    return-void
.end method

.method static synthetic c(Lcom/peel/i/fv;)V
    .locals 0

    .prologue
    .line 54
    invoke-direct {p0}, Lcom/peel/i/fv;->T()V

    return-void
.end method

.method static synthetic d(Lcom/peel/i/fv;)Landroid/app/ProgressDialog;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/peel/i/fv;->h:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method static synthetic e(Lcom/peel/i/fv;)Landroid/widget/Button;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/peel/i/fv;->g:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic f(Lcom/peel/i/fv;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/peel/i/fv;->a:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic g(Lcom/peel/i/fv;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/peel/i/fv;->a:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic h(Lcom/peel/i/fv;)V
    .locals 0

    .prologue
    .line 54
    invoke-direct {p0}, Lcom/peel/i/fv;->S()V

    return-void
.end method

.method static synthetic i(Lcom/peel/i/fv;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/peel/i/fv;->a:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic j(Lcom/peel/i/fv;)Landroid/view/View;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/peel/i/fv;->f:Landroid/view/View;

    return-object v0
.end method

.method static synthetic k(Lcom/peel/i/fv;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/peel/i/fv;->a:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic l(Lcom/peel/i/fv;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/peel/i/fv;->aj:Ljava/util/ArrayList;

    return-object v0
.end method


# virtual methods
.method public Z()V
    .locals 6

    .prologue
    .line 427
    iget-object v0, p0, Lcom/peel/i/fv;->d:Lcom/peel/d/a;

    if-nez v0, :cond_0

    .line 428
    new-instance v0, Lcom/peel/d/a;

    sget-object v1, Lcom/peel/d/d;->a:Lcom/peel/d/d;

    sget-object v2, Lcom/peel/d/b;->b:Lcom/peel/d/b;

    sget-object v3, Lcom/peel/d/c;->b:Lcom/peel/d/c;

    sget v4, Lcom/peel/ui/ft;->app_desc:I

    invoke-virtual {p0, v4}, Lcom/peel/i/fv;->a(I)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, Lcom/peel/d/a;-><init>(Lcom/peel/d/d;Lcom/peel/d/b;Lcom/peel/d/c;Ljava/lang/String;Ljava/util/List;)V

    iput-object v0, p0, Lcom/peel/i/fv;->d:Lcom/peel/d/a;

    .line 430
    :cond_0
    iget-object v0, p0, Lcom/peel/i/fv;->b:Lcom/peel/d/i;

    iget-object v1, p0, Lcom/peel/i/fv;->d:Lcom/peel/d/a;

    invoke-virtual {v0, v1}, Lcom/peel/d/i;->a(Lcom/peel/d/a;)V

    .line 432
    return-void
.end method

.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 6

    .prologue
    .line 63
    sget v0, Lcom/peel/ui/fq;->setup_splash:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/i/fv;->e:Landroid/view/View;

    .line 64
    iget-object v0, p0, Lcom/peel/i/fv;->e:Landroid/view/View;

    sget v1, Lcom/peel/ui/fp;->setup_btn:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/peel/i/fv;->g:Landroid/widget/Button;

    .line 65
    iget-object v0, p0, Lcom/peel/i/fv;->e:Landroid/view/View;

    sget v1, Lcom/peel/ui/fp;->taphere:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/i/fv;->f:Landroid/view/View;

    .line 66
    iget-object v0, p0, Lcom/peel/i/fv;->e:Landroid/view/View;

    sget v1, Lcom/peel/ui/fp;->textView:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 72
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget v2, Lcom/peel/ui/ft;->existing_user:I

    invoke-virtual {p0, v2}, Lcom/peel/i/fv;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget v2, Lcom/peel/ui/ft;->tap_here:I

    invoke-virtual {p0, v2}, Lcom/peel/i/fv;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 73
    new-instance v2, Landroid/text/SpannableString;

    invoke-direct {v2, v1}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 74
    new-instance v3, Lcom/peel/i/fw;

    invoke-direct {v3, p0}, Lcom/peel/i/fw;-><init>(Lcom/peel/i/fv;)V

    .line 101
    sget v4, Lcom/peel/ui/ft;->existing_user:I

    invoke-virtual {p0, v4}, Lcom/peel/i/fv;->a(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    const/16 v5, 0x21

    invoke-virtual {v2, v3, v4, v1, v5}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 102
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 103
    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 108
    iget-object v0, p0, Lcom/peel/i/fv;->e:Landroid/view/View;

    return-object v0
.end method

.method public a(IILandroid/content/Intent;)V
    .locals 6

    .prologue
    .line 328
    invoke-super {p0, p1, p2, p3}, Lcom/peel/d/u;->a(IILandroid/content/Intent;)V

    .line 330
    sget v0, Lcom/peel/social/w;->d:I

    if-eq p1, v0, :cond_0

    const/16 v0, 0x1f45

    if-ne p1, v0, :cond_2

    .line 332
    :cond_0
    iget-object v0, p0, Lcom/peel/i/fv;->a:Ljava/lang/String;

    const-string/jumbo v1, "next step"

    new-instance v2, Lcom/peel/i/ge;

    invoke-direct {v2, p0}, Lcom/peel/i/ge;-><init>(Lcom/peel/i/fv;)V

    invoke-static {v0, v1, v2}, Lcom/peel/util/i;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/lang/Runnable;

    .line 378
    :cond_1
    :goto_0
    return-void

    .line 339
    :cond_2
    const/16 v0, 0x3e8

    if-ne p1, v0, :cond_1

    .line 340
    invoke-direct {p0}, Lcom/peel/i/fv;->c()V

    .line 341
    iget-object v0, p0, Lcom/peel/i/fv;->i:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v1, "import_option"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 343
    const/4 v0, -0x1

    if-ne v0, p2, :cond_1

    .line 344
    if-eqz p3, :cond_1

    .line 346
    sget v0, Lcom/peel/ui/ft;->importing:I

    invoke-direct {p0, v0}, Lcom/peel/i/fv;->b(I)V

    .line 347
    const-string/jumbo v0, "mobile_info"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/peel/backup/MobileDeviceProfile;

    .line 348
    new-instance v0, Lcom/peel/backup/c;

    invoke-virtual {p0}, Lcom/peel/i/fv;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/ae;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/peel/backup/c;-><init>(Landroid/content/Context;)V

    .line 349
    invoke-virtual {v4}, Lcom/peel/backup/MobileDeviceProfile;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4}, Lcom/peel/backup/MobileDeviceProfile;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4}, Lcom/peel/backup/MobileDeviceProfile;->d()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4}, Lcom/peel/backup/MobileDeviceProfile;->e()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Lcom/peel/i/gf;

    invoke-direct {v5, p0}, Lcom/peel/i/gf;-><init>(Lcom/peel/i/fv;)V

    invoke-virtual/range {v0 .. v5}, Lcom/peel/backup/c;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/peel/util/t;)V

    goto :goto_0
.end method

.method public c(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 171
    invoke-super {p0, p1}, Lcom/peel/d/u;->c(Landroid/os/Bundle;)V

    .line 172
    sget-object v0, Lcom/peel/content/a;->h:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/peel/i/fv;->b:Lcom/peel/d/i;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/peel/i/fv;->r()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 284
    :cond_0
    :goto_0
    return-void

    .line 174
    :cond_1
    const-string/jumbo v0, "countries"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 175
    sget v0, Lcom/peel/ui/ft;->please_wait:I

    invoke-direct {p0, v0}, Lcom/peel/i/fv;->b(I)V

    .line 178
    invoke-virtual {p0}, Lcom/peel/i/fv;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    invoke-static {v0}, Lcom/peel/util/bx;->c(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 179
    new-instance v1, Lcom/peel/i/gd;

    invoke-direct {v1, p0, v2, p1}, Lcom/peel/i/gd;-><init>(Lcom/peel/i/fv;ILandroid/os/Bundle;)V

    invoke-static {v0, v1}, Lcom/peel/content/a/j;->c(Ljava/lang/String;Lcom/peel/util/t;)V

    .line 278
    :goto_1
    invoke-virtual {p0}, Lcom/peel/i/fv;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    invoke-static {v0}, Lcom/peel/social/w;->f(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 279
    iget-object v0, p0, Lcom/peel/i/fv;->f:Landroid/view/View;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 280
    invoke-direct {p0}, Lcom/peel/i/fv;->T()V

    goto :goto_0

    .line 275
    :cond_2
    iget-object v0, p0, Lcom/peel/i/fv;->g:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_1

    .line 282
    :cond_3
    iget-object v0, p0, Lcom/peel/i/fv;->f:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method public d(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 123
    invoke-super {p0, p1}, Lcom/peel/d/u;->d(Landroid/os/Bundle;)V

    .line 125
    sget-object v0, Lcom/peel/content/a;->h:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 126
    iget-object v0, p0, Lcom/peel/i/fv;->c:Landroid/os/Bundle;

    invoke-virtual {p0, v0}, Lcom/peel/i/fv;->c(Landroid/os/Bundle;)V

    .line 128
    :cond_0
    return-void
.end method

.method public e()V
    .locals 4

    .prologue
    .line 132
    invoke-super {p0}, Lcom/peel/d/u;->e()V

    .line 133
    invoke-virtual {p0}, Lcom/peel/i/fv;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    invoke-static {v0}, Landroid/support/v4/a/q;->a(Landroid/content/Context;)Landroid/support/v4/a/q;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/i/fv;->ak:Landroid/content/BroadcastReceiver;

    new-instance v2, Landroid/content/IntentFilter;

    const-string/jumbo v3, "samsung_account"

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/a/q;->a(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)V

    .line 134
    return-void
.end method

.method public e(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 321
    iget-object v0, p0, Lcom/peel/i/fv;->c:Landroid/os/Bundle;

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->putAll(Landroid/os/Bundle;)V

    .line 322
    invoke-super {p0, p1}, Lcom/peel/d/u;->e(Landroid/os/Bundle;)V

    .line 323
    return-void
.end method

.method public f()V
    .locals 2

    .prologue
    .line 382
    invoke-super {p0}, Lcom/peel/d/u;->f()V

    .line 383
    invoke-virtual {p0}, Lcom/peel/i/fv;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    invoke-static {v0}, Landroid/support/v4/a/q;->a(Landroid/content/Context;)Landroid/support/v4/a/q;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/i/fv;->ak:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/support/v4/a/q;->a(Landroid/content/BroadcastReceiver;)V

    .line 384
    return-void
.end method

.method public h(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 113
    invoke-super {p0, p1}, Lcom/peel/d/u;->h(Landroid/os/Bundle;)V

    .line 114
    invoke-virtual {p0}, Lcom/peel/i/fv;->Z()V

    .line 116
    if-eqz p1, :cond_0

    .line 117
    iget-object v0, p0, Lcom/peel/i/fv;->c:Landroid/os/Bundle;

    invoke-virtual {v0, p1}, Landroid/os/Bundle;->putAll(Landroid/os/Bundle;)V

    .line 119
    :cond_0
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 288
    sget-object v0, Lcom/peel/content/a;->h:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/peel/i/fv;->b:Lcom/peel/d/i;

    if-nez v0, :cond_1

    .line 316
    :cond_0
    :goto_0
    return-void

    .line 290
    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    .line 291
    sget v1, Lcom/peel/ui/fp;->setup_btn:I

    if-ne v0, v1, :cond_0

    .line 292
    iget-object v0, p0, Lcom/peel/i/fv;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "countries"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 293
    iget-object v0, p0, Lcom/peel/i/fv;->c:Landroid/os/Bundle;

    invoke-virtual {p0, v0}, Lcom/peel/i/fv;->c(Landroid/os/Bundle;)V

    goto :goto_0

    .line 295
    :cond_2
    invoke-virtual {p0}, Lcom/peel/i/fv;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    const-class v1, Lcom/peel/i/cr;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/peel/i/fv;->c:Landroid/os/Bundle;

    invoke-static {v0, v1, v2}, Lcom/peel/d/e;->c(Landroid/support/v4/app/ae;Ljava/lang/String;Landroid/os/Bundle;)V

    goto :goto_0
.end method
