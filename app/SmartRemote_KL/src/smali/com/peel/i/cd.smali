.class Lcom/peel/i/cd;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/peel/i/cc;


# direct methods
.method constructor <init>(Lcom/peel/i/cc;)V
    .locals 0

    .prologue
    .line 211
    iput-object p1, p0, Lcom/peel/i/cd;->a:Lcom/peel/i/cc;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    const/16 v5, 0x7d5

    const/4 v4, 0x1

    .line 214
    iget-object v0, p0, Lcom/peel/i/cd;->a:Lcom/peel/i/cc;

    iget-object v0, v0, Lcom/peel/i/cc;->b:Landroid/os/Bundle;

    const-string/jumbo v1, "iso"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "XX"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 215
    iget-object v0, p0, Lcom/peel/i/cd;->a:Lcom/peel/i/cc;

    iget-object v0, v0, Lcom/peel/i/cc;->c:Lcom/peel/i/cb;

    iget-object v0, v0, Lcom/peel/i/cb;->b:Lcom/peel/i/by;

    invoke-virtual {v0}, Lcom/peel/i/by;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    const-class v1, Lcom/peel/i/bv;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/peel/i/cd;->a:Lcom/peel/i/cc;

    iget-object v2, v2, Lcom/peel/i/cc;->c:Lcom/peel/i/cb;

    iget-object v2, v2, Lcom/peel/i/cb;->b:Lcom/peel/i/by;

    iget-object v2, v2, Lcom/peel/i/by;->c:Landroid/os/Bundle;

    invoke-static {v0, v1, v2}, Lcom/peel/d/e;->c(Landroid/support/v4/app/ae;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 217
    invoke-static {}, Lcom/peel/util/a/f;->a()Lcom/peel/util/a/f;

    move-result-object v0

    const/16 v1, 0x42f

    invoke-virtual {v0, v4, v1, v5}, Lcom/peel/util/a/f;->a(III)V

    .line 218
    invoke-static {}, Lcom/peel/i/by;->c()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/i/cd;->a:Lcom/peel/i/cc;

    iget-object v1, v1, Lcom/peel/i/cc;->c:Lcom/peel/i/cb;

    iget-object v1, v1, Lcom/peel/i/cb;->b:Lcom/peel/i/by;

    invoke-virtual {v1}, Lcom/peel/i/by;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/peel/util/bx;->a(Ljava/lang/String;Landroid/app/Activity;Z)V

    .line 239
    :goto_0
    return-void

    .line 221
    :cond_0
    invoke-static {}, Lcom/peel/util/a/f;->a()Lcom/peel/util/a/f;

    move-result-object v0

    const/16 v1, 0x437

    iget-object v2, p0, Lcom/peel/i/cd;->a:Lcom/peel/i/cc;

    iget-object v2, v2, Lcom/peel/i/cc;->b:Landroid/os/Bundle;

    const-string/jumbo v3, "iso"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v4, v1, v5, v2}, Lcom/peel/util/a/f;->a(IIILjava/lang/String;)V

    .line 223
    iget-object v0, p0, Lcom/peel/i/cd;->a:Lcom/peel/i/cc;

    iget-object v0, v0, Lcom/peel/i/cc;->b:Landroid/os/Bundle;

    const-string/jumbo v1, "iso"

    const-string/jumbo v2, "US"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v0, v1}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lcom/peel/i/ce;

    invoke-direct {v1, p0}, Lcom/peel/i/ce;-><init>(Lcom/peel/i/cd;)V

    invoke-static {v0, v1}, Lcom/peel/content/a/j;->b(Ljava/lang/String;Lcom/peel/util/t;)V

    goto :goto_0
.end method
