.class Lcom/peel/i/fs;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/widget/TextView$OnEditorActionListener;


# instance fields
.field final synthetic a:Landroid/view/View;

.field final synthetic b:Lcom/peel/i/fq;


# direct methods
.method constructor <init>(Lcom/peel/i/fq;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 70
    iput-object p1, p0, Lcom/peel/i/fs;->b:Lcom/peel/i/fq;

    iput-object p2, p0, Lcom/peel/i/fs;->a:Landroid/view/View;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onEditorAction(Landroid/widget/TextView;ILandroid/view/KeyEvent;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 73
    iget-object v2, p0, Lcom/peel/i/fs;->b:Lcom/peel/i/fq;

    invoke-static {v2}, Lcom/peel/i/fq;->a(Lcom/peel/i/fq;)Landroid/widget/EditText;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_0

    .line 74
    iget-object v2, p0, Lcom/peel/i/fs;->a:Landroid/view/View;

    invoke-virtual {v2, v1}, Landroid/view/View;->setEnabled(Z)V

    .line 78
    :goto_0
    const/4 v2, 0x6

    if-ne p2, v2, :cond_1

    iget-object v2, p0, Lcom/peel/i/fs;->a:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->isEnabled()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 79
    iget-object v1, p0, Lcom/peel/i/fs;->a:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->performClick()Z

    .line 82
    :goto_1
    return v0

    .line 76
    :cond_0
    iget-object v2, p0, Lcom/peel/i/fs;->a:Landroid/view/View;

    invoke-virtual {v2, v0}, Landroid/view/View;->setEnabled(Z)V

    goto :goto_0

    :cond_1
    move v0, v1

    .line 82
    goto :goto_1
.end method
