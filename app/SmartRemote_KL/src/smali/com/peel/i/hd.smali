.class Lcom/peel/i/hd;
.super Lcom/peel/util/t;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/peel/util/t",
        "<",
        "Ljava/util/Map",
        "<",
        "Ljava/lang/String;",
        "Ljava/util/Map",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Object;",
        ">;>;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/peel/i/hc;


# direct methods
.method constructor <init>(Lcom/peel/i/hc;I)V
    .locals 0

    .prologue
    .line 1132
    iput-object p1, p0, Lcom/peel/i/hd;->a:Lcom/peel/i/hc;

    invoke-direct {p0, p2}, Lcom/peel/util/t;-><init>(I)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    const/4 v5, 0x7

    const/4 v4, 0x2

    const/4 v3, 0x0

    .line 1136
    invoke-static {}, Lcom/peel/i/gm;->c()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/i/hd;->a:Lcom/peel/i/hc;

    iget-object v1, v1, Lcom/peel/i/hc;->a:Lcom/peel/i/gm;

    invoke-virtual {v1}, Lcom/peel/i/gm;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    invoke-static {v0, v1, v3}, Lcom/peel/util/bx;->a(Ljava/lang/String;Landroid/app/Activity;Z)V

    .line 1137
    sget-boolean v0, Lcom/peel/util/b/a;->a:Z

    if-eqz v0, :cond_1

    .line 1163
    :cond_0
    :goto_0
    return-void

    .line 1141
    :cond_1
    iget-boolean v0, p0, Lcom/peel/i/hd;->i:Z

    if-nez v0, :cond_2

    .line 1142
    invoke-static {}, Lcom/peel/i/gm;->c()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "unable to getAllIrCodesByCodesetid"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1143
    iget-object v0, p0, Lcom/peel/i/hd;->a:Lcom/peel/i/hc;

    iget-object v0, v0, Lcom/peel/i/hc;->a:Lcom/peel/i/gm;

    invoke-virtual {v0}, Lcom/peel/i/gm;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/i/hd;->a:Lcom/peel/i/hc;

    iget-object v1, v1, Lcom/peel/i/hc;->a:Lcom/peel/i/gm;

    iget-object v1, v1, Lcom/peel/i/gm;->b:Lcom/peel/d/i;

    iget-object v2, p0, Lcom/peel/i/hd;->a:Lcom/peel/i/hc;

    iget-object v2, v2, Lcom/peel/i/hc;->a:Lcom/peel/i/gm;

    invoke-static {v2}, Lcom/peel/i/gm;->s(Lcom/peel/i/gm;)Lcom/peel/f/a;

    move-result-object v2

    invoke-virtual {v2}, Lcom/peel/f/a;->b()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/peel/i/hd;->a:Lcom/peel/i/hc;

    iget-object v3, v3, Lcom/peel/i/hc;->a:Lcom/peel/i/gm;

    invoke-virtual {v3}, Lcom/peel/i/gm;->m()Landroid/support/v4/app/ae;

    move-result-object v3

    iget-object v4, p0, Lcom/peel/i/hd;->a:Lcom/peel/i/hc;

    iget-object v4, v4, Lcom/peel/i/hc;->a:Lcom/peel/i/gm;

    invoke-static {v4}, Lcom/peel/i/gm;->D(Lcom/peel/i/gm;)I

    move-result v4

    invoke-static {v3, v4}, Lcom/peel/util/bx;->a(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/peel/i/hd;->a:Lcom/peel/i/hc;

    iget-object v4, v4, Lcom/peel/i/hc;->a:Lcom/peel/i/gm;

    invoke-static {v4}, Lcom/peel/i/gm;->o(Lcom/peel/i/gm;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v1, v2, v3, v4}, Lcom/peel/util/bx;->a(Landroid/content/Context;Lcom/peel/d/i;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1147
    :cond_2
    iget-object v0, p0, Lcom/peel/i/hd;->a:Lcom/peel/i/hc;

    iget-object v0, v0, Lcom/peel/i/hc;->a:Lcom/peel/i/gm;

    invoke-static {v0}, Lcom/peel/i/gm;->c(Lcom/peel/i/gm;)Lcom/peel/control/h;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v1

    iget-object v0, p0, Lcom/peel/i/hd;->a:Lcom/peel/i/hc;

    iget-object v0, v0, Lcom/peel/i/hc;->a:Lcom/peel/i/gm;

    invoke-static {v0}, Lcom/peel/i/gm;->V(Lcom/peel/i/gm;)I

    move-result v2

    iget-object v0, p0, Lcom/peel/i/hd;->j:Ljava/lang/Object;

    check-cast v0, Ljava/util/Map;

    invoke-virtual {v1, v2, v0}, Lcom/peel/data/g;->a(ILjava/util/Map;)V

    .line 1148
    iget-object v0, p0, Lcom/peel/i/hd;->a:Lcom/peel/i/hc;

    iget-object v0, v0, Lcom/peel/i/hc;->a:Lcom/peel/i/gm;

    invoke-static {v0}, Lcom/peel/i/gm;->c(Lcom/peel/i/gm;)Lcom/peel/control/h;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/peel/control/h;->a(I)V

    .line 1151
    iget-object v0, p0, Lcom/peel/i/hd;->a:Lcom/peel/i/hc;

    iget-object v0, v0, Lcom/peel/i/hc;->a:Lcom/peel/i/gm;

    iget-object v0, v0, Lcom/peel/i/gm;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "setup_google_tv"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 1152
    iget-object v0, p0, Lcom/peel/i/hd;->a:Lcom/peel/i/hc;

    iget-object v0, v0, Lcom/peel/i/hc;->a:Lcom/peel/i/gm;

    invoke-static {v0}, Lcom/peel/i/gm;->e(Lcom/peel/i/gm;)Lcom/peel/control/a;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1156
    iget-object v0, p0, Lcom/peel/i/hd;->a:Lcom/peel/i/hc;

    iget-object v0, v0, Lcom/peel/i/hc;->a:Lcom/peel/i/gm;

    invoke-static {v0, v4}, Lcom/peel/i/gm;->b(Lcom/peel/i/gm;I)I

    .line 1158
    iget-object v0, p0, Lcom/peel/i/hd;->a:Lcom/peel/i/hc;

    iget-object v0, v0, Lcom/peel/i/hc;->a:Lcom/peel/i/gm;

    invoke-static {v0}, Lcom/peel/i/gm;->R(Lcom/peel/i/gm;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/i/hd;->a:Lcom/peel/i/hc;

    iget-object v1, v1, Lcom/peel/i/hc;->a:Lcom/peel/i/gm;

    invoke-static {v1}, Lcom/peel/i/gm;->R(Lcom/peel/i/gm;)Landroid/os/Handler;

    move-result-object v1

    invoke-static {v1, v3, v4, v5}, Landroid/os/Message;->obtain(Landroid/os/Handler;III)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto/16 :goto_0

    .line 1161
    :cond_3
    iget-object v0, p0, Lcom/peel/i/hd;->a:Lcom/peel/i/hc;

    iget-object v0, v0, Lcom/peel/i/hc;->a:Lcom/peel/i/gm;

    invoke-static {v0}, Lcom/peel/i/gm;->R(Lcom/peel/i/gm;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/i/hd;->a:Lcom/peel/i/hc;

    iget-object v1, v1, Lcom/peel/i/hc;->a:Lcom/peel/i/gm;

    invoke-static {v1}, Lcom/peel/i/gm;->R(Lcom/peel/i/gm;)Landroid/os/Handler;

    move-result-object v1

    invoke-static {v1, v3, v4, v5}, Landroid/os/Message;->obtain(Landroid/os/Handler;III)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto/16 :goto_0
.end method
