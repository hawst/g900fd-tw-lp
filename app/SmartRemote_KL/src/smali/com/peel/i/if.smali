.class Lcom/peel/i/if;
.super Lcom/peel/util/t;


# instance fields
.field final synthetic a:Lcom/peel/i/gm;


# direct methods
.method constructor <init>(Lcom/peel/i/gm;I)V
    .locals 0

    .prologue
    .line 689
    iput-object p1, p0, Lcom/peel/i/if;->a:Lcom/peel/i/gm;

    invoke-direct {p0, p2}, Lcom/peel/util/t;-><init>(I)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v2, 0x0

    .line 693
    invoke-static {}, Lcom/peel/i/gm;->c()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/i/if;->a:Lcom/peel/i/gm;

    invoke-virtual {v1}, Lcom/peel/i/gm;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    invoke-static {v0, v1, v2}, Lcom/peel/util/bx;->a(Ljava/lang/String;Landroid/app/Activity;Z)V

    .line 694
    sget-boolean v0, Lcom/peel/util/b/a;->a:Z

    if-eqz v0, :cond_0

    .line 717
    :goto_0
    return-void

    .line 697
    :cond_0
    iget-boolean v0, p0, Lcom/peel/i/if;->i:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/peel/i/if;->j:Ljava/lang/Object;

    if-nez v0, :cond_2

    .line 698
    :cond_1
    invoke-static {}, Lcom/peel/i/gm;->c()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "getBrandsByDeviceType"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 699
    iget-object v0, p0, Lcom/peel/i/if;->a:Lcom/peel/i/gm;

    invoke-virtual {v0}, Lcom/peel/i/gm;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/i/if;->a:Lcom/peel/i/gm;

    iget-object v1, v1, Lcom/peel/i/gm;->b:Lcom/peel/d/i;

    iget-object v2, p0, Lcom/peel/i/if;->a:Lcom/peel/i/gm;

    invoke-virtual {v2}, Lcom/peel/i/gm;->m()Landroid/support/v4/app/ae;

    move-result-object v2

    invoke-static {v2, v5}, Lcom/peel/util/bx;->a(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v3, v2, v3}, Lcom/peel/util/bx;->a(Landroid/content/Context;Lcom/peel/d/i;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 703
    :cond_2
    iget-object v1, p0, Lcom/peel/i/if;->a:Lcom/peel/i/gm;

    iget-object v0, p0, Lcom/peel/i/if;->j:Ljava/lang/Object;

    check-cast v0, Ljava/util/List;

    invoke-static {v1, v0}, Lcom/peel/i/gm;->a(Lcom/peel/i/gm;Ljava/util/List;)Ljava/util/List;

    .line 704
    iget-object v0, p0, Lcom/peel/i/if;->a:Lcom/peel/i/gm;

    invoke-static {v0}, Lcom/peel/i/gm;->A(Lcom/peel/i/gm;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    move v1, v2

    .line 706
    :goto_1
    iget-object v0, p0, Lcom/peel/i/if;->a:Lcom/peel/i/gm;

    invoke-static {v0}, Lcom/peel/i/gm;->A(Lcom/peel/i/gm;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 707
    iget-object v0, p0, Lcom/peel/i/if;->a:Lcom/peel/i/gm;

    invoke-static {v0}, Lcom/peel/i/gm;->A(Lcom/peel/i/gm;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/f/a;

    invoke-virtual {v0}, Lcom/peel/f/a;->c()I

    move-result v0

    const/16 v3, 0x3e7

    if-ne v0, v3, :cond_4

    .line 708
    iget-object v0, p0, Lcom/peel/i/if;->a:Lcom/peel/i/gm;

    iget-object v3, p0, Lcom/peel/i/if;->a:Lcom/peel/i/gm;

    invoke-static {v3}, Lcom/peel/i/gm;->A(Lcom/peel/i/gm;)Ljava/util/List;

    move-result-object v3

    iget-object v4, p0, Lcom/peel/i/if;->a:Lcom/peel/i/gm;

    invoke-static {v4}, Lcom/peel/i/gm;->A(Lcom/peel/i/gm;)Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    invoke-interface {v3, v2, v4}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/peel/i/gm;->b(Lcom/peel/i/gm;Ljava/util/List;)Ljava/util/List;

    .line 709
    iget-object v0, p0, Lcom/peel/i/if;->a:Lcom/peel/i/gm;

    iget-object v3, p0, Lcom/peel/i/if;->a:Lcom/peel/i/gm;

    invoke-static {v3}, Lcom/peel/i/gm;->A(Lcom/peel/i/gm;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3, v2, v1}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/peel/i/gm;->a(Lcom/peel/i/gm;Ljava/util/List;)Ljava/util/List;

    .line 714
    :cond_3
    iget-object v0, p0, Lcom/peel/i/if;->a:Lcom/peel/i/gm;

    new-instance v1, Lcom/peel/i/a/a;

    iget-object v2, p0, Lcom/peel/i/if;->a:Lcom/peel/i/gm;

    invoke-virtual {v2}, Lcom/peel/i/gm;->m()Landroid/support/v4/app/ae;

    move-result-object v2

    sget v3, Lcom/peel/ui/fq;->brand_row:I

    iget-object v4, p0, Lcom/peel/i/if;->a:Lcom/peel/i/gm;

    invoke-static {v4}, Lcom/peel/i/gm;->A(Lcom/peel/i/gm;)Ljava/util/List;

    move-result-object v4

    invoke-direct {v1, v2, v3, v4}, Lcom/peel/i/a/a;-><init>(Landroid/content/Context;ILjava/util/List;)V

    invoke-static {v0, v1}, Lcom/peel/i/gm;->a(Lcom/peel/i/gm;Lcom/peel/i/a/a;)Lcom/peel/i/a/a;

    .line 715
    iget-object v0, p0, Lcom/peel/i/if;->a:Lcom/peel/i/gm;

    invoke-static {v0}, Lcom/peel/i/gm;->C(Lcom/peel/i/gm;)Landroid/widget/ListView;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/i/if;->a:Lcom/peel/i/gm;

    invoke-static {v1}, Lcom/peel/i/gm;->B(Lcom/peel/i/gm;)Lcom/peel/i/a/a;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 716
    iget-object v0, p0, Lcom/peel/i/if;->a:Lcom/peel/i/gm;

    invoke-static {v0, v5}, Lcom/peel/i/gm;->a(Lcom/peel/i/gm;I)I

    goto/16 :goto_0

    .line 706
    :cond_4
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1
.end method
