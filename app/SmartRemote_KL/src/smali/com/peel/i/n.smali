.class Lcom/peel/i/n;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/peel/i/a;


# direct methods
.method constructor <init>(Lcom/peel/i/a;)V
    .locals 0

    .prologue
    .line 659
    iput-object p1, p0, Lcom/peel/i/n;->a:Lcom/peel/i/a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 9

    .prologue
    const/4 v8, 0x1

    .line 665
    iget-object v0, p0, Lcom/peel/i/n;->a:Lcom/peel/i/a;

    invoke-static {v0}, Lcom/peel/i/a;->B(Lcom/peel/i/a;)Landroid/widget/Button;

    move-result-object v0

    invoke-static {}, Lcom/peel/i/a;->c()Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0x3e8

    invoke-static {v0, v1, v2}, Lcom/peel/util/bx;->a(Landroid/view/View;Ljava/lang/String;I)V

    .line 667
    invoke-static {}, Lcom/peel/i/a;->c()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/i/n;->a:Lcom/peel/i/a;

    invoke-virtual {v1}, Lcom/peel/i/a;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    invoke-static {v0, v1, v8}, Lcom/peel/util/bx;->a(Ljava/lang/String;Landroid/app/Activity;Z)V

    .line 668
    iget-object v0, p0, Lcom/peel/i/n;->a:Lcom/peel/i/a;

    invoke-virtual {v0}, Lcom/peel/i/a;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    const-string/jumbo v1, "social_accounts_setup"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/ae;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v1, "show social login"

    invoke-interface {v0, v1, v8}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 669
    invoke-static {}, Lcom/peel/util/a/f;->a()Lcom/peel/util/a/f;

    move-result-object v0

    sget-object v1, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v1}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v1

    if-nez v1, :cond_0

    move v1, v8

    :goto_0
    const/16 v2, 0x431

    const/16 v3, 0x7d8

    iget-object v4, p0, Lcom/peel/i/n;->a:Lcom/peel/i/a;

    invoke-static {v4}, Lcom/peel/i/a;->a(Lcom/peel/i/a;)Lcom/peel/f/a;

    move-result-object v4

    invoke-virtual {v4}, Lcom/peel/f/a;->b()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/peel/i/n;->a:Lcom/peel/i/a;

    invoke-static {v5}, Lcom/peel/i/a;->h(Lcom/peel/i/a;)I

    move-result v5

    const-string/jumbo v6, ""

    iget-object v7, p0, Lcom/peel/i/n;->a:Lcom/peel/i/a;

    .line 670
    invoke-static {v7}, Lcom/peel/i/a;->A(Lcom/peel/i/a;)I

    move-result v7

    .line 669
    invoke-virtual/range {v0 .. v7}, Lcom/peel/util/a/f;->a(IIILjava/lang/String;ILjava/lang/String;I)V

    .line 672
    iget-object v0, p0, Lcom/peel/i/n;->a:Lcom/peel/i/a;

    invoke-static {v0}, Lcom/peel/i/a;->z(Lcom/peel/i/a;)Landroid/widget/TextView;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 674
    const-string/jumbo v0, "https://partners-ir.peel.com"

    iget-object v1, p0, Lcom/peel/i/n;->a:Lcom/peel/i/a;

    invoke-static {v1}, Lcom/peel/i/a;->A(Lcom/peel/i/a;)I

    move-result v1

    new-instance v2, Lcom/peel/i/o;

    invoke-direct {v2, p0, v8}, Lcom/peel/i/o;-><init>(Lcom/peel/i/n;I)V

    invoke-static {v0, v1, v2}, Lcom/peel/control/aa;->a(Ljava/lang/String;ILcom/peel/util/t;)V

    .line 805
    return-void

    .line 669
    :cond_0
    sget-object v1, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v1}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/data/at;->f()I

    move-result v1

    goto :goto_0
.end method
