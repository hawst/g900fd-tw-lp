.class Lcom/peel/i/ey;
.super Landroid/widget/ArrayAdapter;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Landroid/os/Bundle;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic b:Lcom/peel/i/et;


# direct methods
.method constructor <init>(Lcom/peel/i/et;Landroid/content/Context;I[Landroid/os/Bundle;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 319
    iput-object p1, p0, Lcom/peel/i/ey;->b:Lcom/peel/i/et;

    iput-object p5, p0, Lcom/peel/i/ey;->a:Ljava/lang/String;

    invoke-direct {p0, p2, p3, p4}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 322
    if-eqz p2, :cond_1

    .line 323
    :goto_0
    invoke-virtual {p0, p1}, Lcom/peel/i/ey;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    .line 325
    sget v1, Lcom/peel/ui/fp;->checked_icon:I

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    iget-object v1, p0, Lcom/peel/i/ey;->b:Lcom/peel/i/et;

    iget-object v1, v1, Lcom/peel/i/et;->c:Lcom/peel/i/ep;

    invoke-static {v1}, Lcom/peel/i/ep;->g(Lcom/peel/i/ep;)I

    move-result v1

    if-ne v1, p1, :cond_2

    move v1, v2

    :goto_1
    invoke-virtual {v3, v1}, Landroid/view/View;->setVisibility(I)V

    .line 326
    sget v1, Lcom/peel/ui/fp;->name:I

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    const-string/jumbo v3, "mso"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/peel/i/ey;->a:Ljava/lang/String;

    iget-object v5, p0, Lcom/peel/i/ey;->b:Lcom/peel/i/et;

    iget-object v5, v5, Lcom/peel/i/et;->c:Lcom/peel/i/ep;

    invoke-virtual {v5}, Lcom/peel/i/ep;->n()Landroid/content/res/Resources;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/peel/util/bx;->a(Ljava/lang/String;Ljava/lang/String;Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 327
    sget v1, Lcom/peel/ui/fp;->alt_name:I

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const-string/jumbo v3, "name"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v4, "mso"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/16 v2, 0x8

    :cond_0
    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 328
    sget v1, Lcom/peel/ui/fp;->alt_name:I

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    const-string/jumbo v2, "name"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 330
    return-object p2

    .line 322
    :cond_1
    iget-object v0, p0, Lcom/peel/i/ey;->b:Lcom/peel/i/et;

    iget-object v0, v0, Lcom/peel/i/et;->c:Lcom/peel/i/ep;

    invoke-static {v0}, Lcom/peel/i/ep;->e(Lcom/peel/i/ep;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lcom/peel/ui/fq;->provider_row:I

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    goto :goto_0

    .line 325
    :cond_2
    const/4 v1, 0x4

    goto :goto_1
.end method
