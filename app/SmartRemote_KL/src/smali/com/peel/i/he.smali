.class Lcom/peel/i/he;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/peel/i/gm;


# direct methods
.method constructor <init>(Lcom/peel/i/gm;)V
    .locals 0

    .prologue
    .line 1169
    iput-object p1, p0, Lcom/peel/i/he;->a:Lcom/peel/i/gm;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 11

    .prologue
    const/4 v6, 0x0

    const/16 v10, 0x8

    const/4 v8, 0x1

    const/4 v9, 0x0

    .line 1172
    invoke-static {}, Lcom/peel/util/a/f;->a()Lcom/peel/util/a/f;

    move-result-object v0

    sget-object v1, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v1}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v1

    if-nez v1, :cond_0

    move v1, v8

    :goto_0
    const/16 v2, 0x432

    const/16 v3, 0x7d5

    iget-object v4, p0, Lcom/peel/i/he;->a:Lcom/peel/i/gm;

    invoke-static {v4}, Lcom/peel/i/gm;->m(Lcom/peel/i/gm;)Lcom/peel/f/a;

    move-result-object v4

    invoke-virtual {v4}, Lcom/peel/f/a;->b()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/peel/i/he;->a:Lcom/peel/i/gm;

    invoke-static {v5}, Lcom/peel/i/gm;->V(Lcom/peel/i/gm;)I

    move-result v5

    iget-object v7, p0, Lcom/peel/i/he;->a:Lcom/peel/i/gm;

    invoke-static {v7}, Lcom/peel/i/gm;->X(Lcom/peel/i/gm;)I

    move-result v7

    invoke-virtual/range {v0 .. v7}, Lcom/peel/util/a/f;->a(IIILjava/lang/String;ILjava/lang/String;I)V

    .line 1173
    iget-object v0, p0, Lcom/peel/i/he;->a:Lcom/peel/i/gm;

    invoke-static {v0}, Lcom/peel/i/gm;->T(Lcom/peel/i/gm;)Lcom/peel/widget/TestBtnViewPager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/widget/TestBtnViewPager;->getCurrentItem()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    iget-object v1, p0, Lcom/peel/i/he;->a:Lcom/peel/i/gm;

    invoke-static {v1}, Lcom/peel/i/gm;->S(Lcom/peel/i/gm;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 1174
    iget-object v0, p0, Lcom/peel/i/he;->a:Lcom/peel/i/gm;

    invoke-static {v0}, Lcom/peel/i/gm;->T(Lcom/peel/i/gm;)Lcom/peel/widget/TestBtnViewPager;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/i/he;->a:Lcom/peel/i/gm;

    invoke-static {v1}, Lcom/peel/i/gm;->T(Lcom/peel/i/gm;)Lcom/peel/widget/TestBtnViewPager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/widget/TestBtnViewPager;->getCurrentItem()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v1}, Lcom/peel/widget/TestBtnViewPager;->setCurrentItem(I)V

    .line 1197
    :goto_1
    return-void

    .line 1172
    :cond_0
    sget-object v1, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v1}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/data/at;->f()I

    move-result v1

    goto :goto_0

    .line 1176
    :cond_1
    invoke-static {}, Lcom/peel/i/gm;->c()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "show missing IR code screen"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1179
    iget-object v0, p0, Lcom/peel/i/he;->a:Lcom/peel/i/gm;

    invoke-static {v0}, Lcom/peel/i/gm;->Y(Lcom/peel/i/gm;)Landroid/widget/RelativeLayout;

    move-result-object v0

    invoke-virtual {v0, v10}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1180
    iget-object v0, p0, Lcom/peel/i/he;->a:Lcom/peel/i/gm;

    invoke-static {v0}, Lcom/peel/i/gm;->Z(Lcom/peel/i/gm;)Landroid/widget/RelativeLayout;

    move-result-object v0

    invoke-virtual {v0, v9}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1182
    iget-object v0, p0, Lcom/peel/i/he;->a:Lcom/peel/i/gm;

    invoke-static {v0}, Lcom/peel/i/gm;->aa(Lcom/peel/i/gm;)Landroid/widget/TextView;

    move-result-object v0

    new-instance v1, Lcom/peel/i/il;

    iget-object v2, p0, Lcom/peel/i/he;->a:Lcom/peel/i/gm;

    invoke-direct {v1, v2, v8}, Lcom/peel/i/il;-><init>(Lcom/peel/i/gm;I)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 1184
    iget-object v0, p0, Lcom/peel/i/he;->a:Lcom/peel/i/gm;

    sget v1, Lcom/peel/ui/ft;->ir_report_missing_code:I

    invoke-virtual {v0, v1}, Lcom/peel/i/gm;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    .line 1185
    new-instance v2, Landroid/text/SpannableStringBuilder;

    invoke-direct {v2, v0}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 1187
    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    const-class v1, Landroid/text/style/URLSpan;

    invoke-virtual {v2, v9, v0, v1}, Landroid/text/SpannableStringBuilder;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/text/style/URLSpan;

    .line 1188
    array-length v3, v0

    move v1, v9

    :goto_2
    if-ge v1, v3, :cond_2

    aget-object v4, v0, v1

    .line 1189
    new-instance v5, Lcom/peel/i/ik;

    iget-object v7, p0, Lcom/peel/i/he;->a:Lcom/peel/i/gm;

    invoke-direct {v5, v7, v6}, Lcom/peel/i/ik;-><init>(Lcom/peel/i/gm;Lcom/peel/i/gn;)V

    .line 1190
    invoke-virtual {v2, v4}, Landroid/text/SpannableStringBuilder;->getSpanStart(Ljava/lang/Object;)I

    move-result v7

    .line 1191
    invoke-virtual {v2, v4}, Landroid/text/SpannableStringBuilder;->getSpanEnd(Ljava/lang/Object;)I

    move-result v4

    const/16 v8, 0x22

    .line 1190
    invoke-virtual {v2, v5, v7, v4, v8}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 1188
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 1193
    :cond_2
    iget-object v0, p0, Lcom/peel/i/he;->a:Lcom/peel/i/gm;

    invoke-static {v0}, Lcom/peel/i/gm;->aa(Lcom/peel/i/gm;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1194
    iget-object v0, p0, Lcom/peel/i/he;->a:Lcom/peel/i/gm;

    invoke-static {v0}, Lcom/peel/i/gm;->U(Lcom/peel/i/gm;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v10}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_1
.end method
