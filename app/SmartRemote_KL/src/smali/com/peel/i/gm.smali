.class public Lcom/peel/i/gm;
.super Lcom/peel/d/u;


# static fields
.field private static final e:Ljava/lang/String;

.field private static f:I


# instance fields
.field private aA:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/peel/f/a;",
            ">;"
        }
    .end annotation
.end field

.field private aB:Z

.field private aC:I

.field private aD:Ljava/lang/String;

.field private aE:Ljava/lang/String;

.field private aF:Ljava/lang/String;

.field private aG:Lcom/peel/i/a/a;

.field private aH:Lcom/peel/i/a/a;

.field private aI:Lcom/peel/widget/ag;

.field private aJ:Lcom/peel/control/h;

.field private aK:Lcom/peel/control/h;

.field private aL:Lcom/peel/control/RoomControl;

.field private aM:Lcom/peel/control/a;

.field private aN:Lcom/peel/content/library/LiveLibrary;

.field private aO:Ljava/util/Stack;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Stack",
            "<",
            "Lcom/peel/i/im;",
            ">;"
        }
    .end annotation
.end field

.field private aP:Landroid/os/Bundle;

.field private aQ:Lcom/peel/ui/dg;

.field private aR:Z

.field private aS:Landroid/widget/ImageView;

.field private aT:Landroid/widget/ImageView;

.field private aU:Landroid/widget/RelativeLayout;

.field private aV:Landroid/widget/RelativeLayout;

.field private aW:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;>;"
        }
    .end annotation
.end field

.field private aX:I

.field private aY:I

.field private aZ:Ljava/lang/String;

.field private aj:Lcom/peel/i/a/a;

.field private ak:Lcom/peel/i/a/a;

.field private al:Lcom/peel/i/a/a;

.field private am:I

.field private an:I

.field private ao:Lcom/peel/f/a;

.field private ap:Lcom/peel/f/a;

.field private aq:Landroid/widget/ListView;

.field private ar:Landroid/widget/ListView;

.field private as:Landroid/widget/ListView;

.field private at:Landroid/widget/ListView;

.field private au:Landroid/widget/AutoCompleteTextView;

.field private av:Landroid/widget/AutoCompleteTextView;

.field private aw:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/peel/f/a;",
            ">;"
        }
    .end annotation
.end field

.field private ax:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/peel/f/a;",
            ">;"
        }
    .end annotation
.end field

.field private ay:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/peel/f/a;",
            ">;"
        }
    .end annotation
.end field

.field private az:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/peel/f/a;",
            ">;"
        }
    .end annotation
.end field

.field private bA:I

.field private bB:Lcom/peel/ui/dr;

.field private ba:Ljava/lang/String;

.field private bb:Landroid/widget/Button;

.field private bc:Landroid/widget/Button;

.field private bd:Landroid/widget/TextView;

.field private be:Landroid/widget/TextView;

.field private bf:Landroid/widget/TextView;

.field private bg:Landroid/widget/TextView;

.field private bh:Landroid/widget/RelativeLayout;

.field private bi:Landroid/widget/RelativeLayout;

.field private bj:Landroid/support/v4/view/ViewPager;

.field private bk:Landroid/widget/ImageView;

.field private bl:Landroid/widget/ImageView;

.field private bm:Landroid/graphics/drawable/AnimationDrawable;

.field private bn:Lcom/peel/widget/TestBtnViewPager;

.field private bo:Landroid/widget/Button;

.field private bp:Landroid/widget/Button;

.field private bq:Lcom/peel/i/in;

.field private br:I

.field private bs:I

.field private bt:Z

.field private bu:Z

.field private bv:Z

.field private bw:Z

.field private bx:Z

.field private by:Z

.field private final bz:Landroid/os/Handler;

.field private g:Landroid/view/LayoutInflater;

.field private h:Landroid/widget/ImageView;

.field private i:Landroid/widget/ViewFlipper;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 97
    const-class v0, Lcom/peel/i/gm;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/peel/i/gm;->e:Ljava/lang/String;

    .line 112
    const/4 v0, -0x1

    sput v0, Lcom/peel/i/gm;->f:I

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 95
    invoke-direct {p0}, Lcom/peel/d/u;-><init>()V

    .line 126
    iput-boolean v1, p0, Lcom/peel/i/gm;->aB:Z

    .line 130
    const/4 v0, 0x1

    iput v0, p0, Lcom/peel/i/gm;->aC:I

    .line 131
    iput-object v2, p0, Lcom/peel/i/gm;->aD:Ljava/lang/String;

    .line 133
    iput-object v2, p0, Lcom/peel/i/gm;->aI:Lcom/peel/widget/ag;

    .line 138
    new-instance v0, Ljava/util/Stack;

    invoke-direct {v0}, Ljava/util/Stack;-><init>()V

    iput-object v0, p0, Lcom/peel/i/gm;->aO:Ljava/util/Stack;

    .line 140
    iput-object v2, p0, Lcom/peel/i/gm;->aQ:Lcom/peel/ui/dg;

    .line 141
    iput-boolean v1, p0, Lcom/peel/i/gm;->aR:Z

    .line 163
    iput v1, p0, Lcom/peel/i/gm;->br:I

    .line 164
    iput v1, p0, Lcom/peel/i/gm;->bs:I

    .line 169
    iput-boolean v1, p0, Lcom/peel/i/gm;->bv:Z

    .line 170
    iput-boolean v1, p0, Lcom/peel/i/gm;->bw:Z

    .line 171
    iput-boolean v1, p0, Lcom/peel/i/gm;->bx:Z

    .line 172
    iput-boolean v1, p0, Lcom/peel/i/gm;->by:Z

    .line 174
    new-instance v0, Lcom/peel/i/gn;

    invoke-direct {v0, p0}, Lcom/peel/i/gn;-><init>(Lcom/peel/i/gm;)V

    iput-object v0, p0, Lcom/peel/i/gm;->bz:Landroid/os/Handler;

    .line 443
    const/4 v0, -0x1

    iput v0, p0, Lcom/peel/i/gm;->bA:I

    .line 994
    new-instance v0, Lcom/peel/i/gw;

    invoke-direct {v0, p0}, Lcom/peel/i/gw;-><init>(Lcom/peel/i/gm;)V

    iput-object v0, p0, Lcom/peel/i/gm;->bB:Lcom/peel/ui/dr;

    .line 2412
    return-void
.end method

.method static synthetic A(Lcom/peel/i/gm;)Ljava/util/List;
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lcom/peel/i/gm;->aw:Ljava/util/List;

    return-object v0
.end method

.method static synthetic B(Lcom/peel/i/gm;)Lcom/peel/i/a/a;
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lcom/peel/i/gm;->aj:Lcom/peel/i/a/a;

    return-object v0
.end method

.method static synthetic C(Lcom/peel/i/gm;)Landroid/widget/ListView;
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lcom/peel/i/gm;->aq:Landroid/widget/ListView;

    return-object v0
.end method

.method static synthetic D(Lcom/peel/i/gm;)I
    .locals 1

    .prologue
    .line 95
    iget v0, p0, Lcom/peel/i/gm;->am:I

    return v0
.end method

.method static synthetic E(Lcom/peel/i/gm;)Z
    .locals 1

    .prologue
    .line 95
    iget-boolean v0, p0, Lcom/peel/i/gm;->bv:Z

    return v0
.end method

.method static synthetic F(Lcom/peel/i/gm;)Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lcom/peel/i/gm;->h:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic G(Lcom/peel/i/gm;)Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lcom/peel/i/gm;->aS:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic H(Lcom/peel/i/gm;)Landroid/widget/ListView;
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lcom/peel/i/gm;->ar:Landroid/widget/ListView;

    return-object v0
.end method

.method static synthetic I(Lcom/peel/i/gm;)Landroid/widget/AutoCompleteTextView;
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lcom/peel/i/gm;->av:Landroid/widget/AutoCompleteTextView;

    return-object v0
.end method

.method static synthetic J(Lcom/peel/i/gm;)Lcom/peel/i/a/a;
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lcom/peel/i/gm;->ak:Lcom/peel/i/a/a;

    return-object v0
.end method

.method static synthetic K(Lcom/peel/i/gm;)Z
    .locals 1

    .prologue
    .line 95
    iget-boolean v0, p0, Lcom/peel/i/gm;->bw:Z

    return v0
.end method

.method static synthetic L(Lcom/peel/i/gm;)Ljava/util/List;
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lcom/peel/i/gm;->ax:Ljava/util/List;

    return-object v0
.end method

.method static synthetic M(Lcom/peel/i/gm;)Lcom/peel/ui/dr;
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lcom/peel/i/gm;->bB:Lcom/peel/ui/dr;

    return-object v0
.end method

.method static synthetic N(Lcom/peel/i/gm;)Lcom/peel/ui/dg;
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lcom/peel/i/gm;->aQ:Lcom/peel/ui/dg;

    return-object v0
.end method

.method static synthetic O(Lcom/peel/i/gm;)Ljava/util/List;
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lcom/peel/i/gm;->ay:Ljava/util/List;

    return-object v0
.end method

.method static synthetic P(Lcom/peel/i/gm;)Lcom/peel/i/a/a;
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lcom/peel/i/gm;->al:Lcom/peel/i/a/a;

    return-object v0
.end method

.method static synthetic Q(Lcom/peel/i/gm;)Landroid/widget/RelativeLayout;
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lcom/peel/i/gm;->aV:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method static synthetic R(Lcom/peel/i/gm;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lcom/peel/i/gm;->bz:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic S(Lcom/peel/i/gm;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lcom/peel/i/gm;->aW:Ljava/util/ArrayList;

    return-object v0
.end method

.method private S()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 593
    invoke-static {}, Lcom/peel/util/a/f;->a()Lcom/peel/util/a/f;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/i/gm;->aL:Lcom/peel/control/RoomControl;

    invoke-virtual {v1}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/data/at;->f()I

    move-result v1

    const/16 v2, 0xbeb

    const/16 v3, 0x7d5

    const-string/jumbo v4, "SCREEN_LINE_OF_SIGHT_DIAGRAM"

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/peel/util/a/f;->a(IIILjava/lang/String;)V

    .line 594
    iget-object v0, p0, Lcom/peel/i/gm;->i:Landroid/widget/ViewFlipper;

    invoke-virtual {v0, v5}, Landroid/widget/ViewFlipper;->setDisplayedChild(I)V

    .line 595
    sput v5, Lcom/peel/i/gm;->f:I

    .line 596
    sget v0, Lcom/peel/ui/ft;->using_peel:I

    invoke-virtual {p0, v0}, Lcom/peel/i/gm;->a(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    invoke-direct {p0, v0, v5, v1}, Lcom/peel/i/gm;->a(Ljava/lang/String;ZZ)V

    .line 597
    return-void
.end method

.method static synthetic T(Lcom/peel/i/gm;)Lcom/peel/widget/TestBtnViewPager;
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lcom/peel/i/gm;->bn:Lcom/peel/widget/TestBtnViewPager;

    return-object v0
.end method

.method private T()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 601
    iget-object v0, p0, Lcom/peel/i/gm;->i:Landroid/widget/ViewFlipper;

    invoke-virtual {v0, v1}, Landroid/widget/ViewFlipper;->setDisplayedChild(I)V

    .line 602
    sput v1, Lcom/peel/i/gm;->f:I

    .line 603
    sget v0, Lcom/peel/ui/ft;->before_you_start:I

    invoke-virtual {p0, v0}, Lcom/peel/i/gm;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, v1, v1}, Lcom/peel/i/gm;->a(Ljava/lang/String;ZZ)V

    .line 605
    invoke-virtual {p0}, Lcom/peel/i/gm;->v()Landroid/view/View;

    move-result-object v0

    sget v1, Lcom/peel/ui/fp;->before_setup_viewpager:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ViewPager;

    iput-object v0, p0, Lcom/peel/i/gm;->bj:Landroid/support/v4/view/ViewPager;

    .line 606
    iget-object v0, p0, Lcom/peel/i/gm;->bj:Landroid/support/v4/view/ViewPager;

    new-instance v1, Lcom/peel/i/ho;

    invoke-direct {v1, p0}, Lcom/peel/i/ho;-><init>(Lcom/peel/i/gm;)V

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setAdapter(Landroid/support/v4/view/av;)V

    .line 649
    iget-object v0, p0, Lcom/peel/i/gm;->bj:Landroid/support/v4/view/ViewPager;

    new-instance v1, Lcom/peel/i/ib;

    invoke-direct {v1, p0}, Lcom/peel/i/ib;-><init>(Lcom/peel/i/gm;)V

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setOnPageChangeListener(Landroid/support/v4/view/cs;)V

    .line 673
    return-void
.end method

.method static synthetic U(Lcom/peel/i/gm;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lcom/peel/i/gm;->bf:Landroid/widget/TextView;

    return-object v0
.end method

.method private U()V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    .line 676
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/peel/i/gm;->ao:Lcom/peel/f/a;

    .line 677
    iput v5, p0, Lcom/peel/i/gm;->am:I

    .line 678
    invoke-static {}, Lcom/peel/util/a/f;->a()Lcom/peel/util/a/f;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/i/gm;->aL:Lcom/peel/control/RoomControl;

    invoke-virtual {v1}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/data/at;->f()I

    move-result v1

    const/16 v2, 0xbeb

    const/16 v3, 0x7d5

    const-string/jumbo v4, "SCREEN_TV_BRAND_LIST"

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/peel/util/a/f;->a(IIILjava/lang/String;)V

    .line 680
    iget-object v0, p0, Lcom/peel/i/gm;->i:Landroid/widget/ViewFlipper;

    invoke-virtual {v0, v6}, Landroid/widget/ViewFlipper;->setDisplayedChild(I)V

    .line 681
    sput v6, Lcom/peel/i/gm;->f:I

    .line 682
    invoke-virtual {p0}, Lcom/peel/i/gm;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    iget v1, p0, Lcom/peel/i/gm;->am:I

    invoke-static {v0, v1}, Lcom/peel/util/bx;->c(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v0

    iget-boolean v1, p0, Lcom/peel/i/gm;->bv:Z

    invoke-direct {p0, v0, v5, v1}, Lcom/peel/i/gm;->a(Ljava/lang/String;ZZ)V

    .line 684
    invoke-virtual {p0}, Lcom/peel/i/gm;->v()Landroid/view/View;

    move-result-object v0

    sget v1, Lcom/peel/ui/fp;->tv_list:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/peel/i/gm;->aq:Landroid/widget/ListView;

    .line 686
    iget-object v0, p0, Lcom/peel/i/gm;->aw:Ljava/util/List;

    if-nez v0, :cond_0

    .line 688
    sget-object v0, Lcom/peel/i/gm;->e:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/peel/i/gm;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    invoke-static {v0, v1, v5}, Lcom/peel/util/bx;->a(Ljava/lang/String;Landroid/app/Activity;Z)V

    .line 689
    const-string/jumbo v0, "https://partners-ir.peel.com"

    iget-object v1, p0, Lcom/peel/i/gm;->aE:Ljava/lang/String;

    new-instance v2, Lcom/peel/i/if;

    invoke-direct {v2, p0, v5}, Lcom/peel/i/if;-><init>(Lcom/peel/i/gm;I)V

    invoke-static {v0, v5, v1, v2}, Lcom/peel/control/aa;->a(Ljava/lang/String;ILjava/lang/String;Lcom/peel/util/t;)V

    .line 725
    :goto_0
    iget-object v0, p0, Lcom/peel/i/gm;->aq:Landroid/widget/ListView;

    new-instance v1, Lcom/peel/i/ig;

    invoke-direct {v1, p0}, Lcom/peel/i/ig;-><init>(Lcom/peel/i/gm;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 735
    return-void

    .line 720
    :cond_0
    new-instance v0, Lcom/peel/i/a/a;

    invoke-virtual {p0}, Lcom/peel/i/gm;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    sget v2, Lcom/peel/ui/fq;->brand_row:I

    iget-object v3, p0, Lcom/peel/i/gm;->aw:Ljava/util/List;

    invoke-direct {v0, v1, v2, v3}, Lcom/peel/i/a/a;-><init>(Landroid/content/Context;ILjava/util/List;)V

    iput-object v0, p0, Lcom/peel/i/gm;->aj:Lcom/peel/i/a/a;

    .line 721
    iget-object v0, p0, Lcom/peel/i/gm;->aq:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/peel/i/gm;->aj:Lcom/peel/i/a/a;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 722
    iput v5, p0, Lcom/peel/i/gm;->am:I

    goto :goto_0
.end method

.method static synthetic V(Lcom/peel/i/gm;)I
    .locals 1

    .prologue
    .line 95
    iget v0, p0, Lcom/peel/i/gm;->aX:I

    return v0
.end method

.method private V()V
    .locals 9

    .prologue
    const/4 v8, 0x3

    const/16 v2, 0x8

    const/4 v7, 0x1

    const/4 v1, 0x0

    .line 739
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/peel/i/gm;->ao:Lcom/peel/f/a;

    .line 740
    iget-object v0, p0, Lcom/peel/i/gm;->ax:Ljava/util/List;

    if-nez v0, :cond_0

    .line 852
    :goto_0
    return-void

    .line 742
    :cond_0
    iput-boolean v1, p0, Lcom/peel/i/gm;->aR:Z

    .line 743
    iput v7, p0, Lcom/peel/i/gm;->am:I

    .line 744
    invoke-static {}, Lcom/peel/util/a/f;->a()Lcom/peel/util/a/f;

    move-result-object v0

    iget-object v3, p0, Lcom/peel/i/gm;->aL:Lcom/peel/control/RoomControl;

    invoke-virtual {v3}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v3

    invoke-virtual {v3}, Lcom/peel/data/at;->f()I

    move-result v3

    const/16 v4, 0xbeb

    const/16 v5, 0x7d5

    const-string/jumbo v6, "SCREEN_OTHER_TV_PROJECTOR_BRAND_LIST"

    invoke-virtual {v0, v3, v4, v5, v6}, Lcom/peel/util/a/f;->a(IIILjava/lang/String;)V

    .line 746
    iget-object v0, p0, Lcom/peel/i/gm;->i:Landroid/widget/ViewFlipper;

    invoke-virtual {v0, v8}, Landroid/widget/ViewFlipper;->setDisplayedChild(I)V

    .line 747
    sput v8, Lcom/peel/i/gm;->f:I

    .line 748
    invoke-virtual {p0}, Lcom/peel/i/gm;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    iget v3, p0, Lcom/peel/i/gm;->am:I

    invoke-static {v0, v3}, Lcom/peel/util/bx;->c(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v0

    iget-boolean v3, p0, Lcom/peel/i/gm;->bw:Z

    invoke-direct {p0, v0, v7, v3}, Lcom/peel/i/gm;->a(Ljava/lang/String;ZZ)V

    .line 750
    invoke-virtual {p0}, Lcom/peel/i/gm;->v()Landroid/view/View;

    move-result-object v0

    sget v3, Lcom/peel/ui/fp;->other_tv_list_filter:I

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/AutoCompleteTextView;

    iput-object v0, p0, Lcom/peel/i/gm;->av:Landroid/widget/AutoCompleteTextView;

    .line 752
    invoke-virtual {p0}, Lcom/peel/i/gm;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    const-string/jumbo v3, "input_method"

    invoke-virtual {v0, v3}, Landroid/support/v4/app/ae;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 753
    iget-object v3, p0, Lcom/peel/i/gm;->av:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v0, v3, v1}, Landroid/view/inputmethod/InputMethodManager;->showSoftInput(Landroid/view/View;I)Z

    .line 754
    invoke-virtual {p0}, Lcom/peel/i/gm;->v()Landroid/view/View;

    move-result-object v0

    sget v3, Lcom/peel/ui/fp;->search_cancel_btn:I

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/peel/i/gm;->h:Landroid/widget/ImageView;

    .line 755
    iget-object v0, p0, Lcom/peel/i/gm;->ar:Landroid/widget/ListView;

    if-nez v0, :cond_1

    .line 756
    invoke-virtual {p0}, Lcom/peel/i/gm;->v()Landroid/view/View;

    move-result-object v0

    sget v3, Lcom/peel/ui/fp;->other_list:I

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/peel/i/gm;->ar:Landroid/widget/ListView;

    .line 758
    :cond_1
    iget-object v0, p0, Lcom/peel/i/gm;->ba:Ljava/lang/String;

    const-string/jumbo v3, "iw"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/peel/i/gm;->ba:Ljava/lang/String;

    const-string/jumbo v3, "ar"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 759
    :cond_2
    iget-object v0, p0, Lcom/peel/i/gm;->av:Landroid/widget/AutoCompleteTextView;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget v4, Lcom/peel/ui/ft;->hint_search_box:I

    invoke-virtual {p0, v4}, Lcom/peel/i/gm;->a(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/AutoCompleteTextView;->setHint(Ljava/lang/CharSequence;)V

    .line 764
    :goto_1
    invoke-virtual {p0}, Lcom/peel/i/gm;->v()Landroid/view/View;

    move-result-object v0

    sget v3, Lcom/peel/ui/fp;->search_icon_othertv:I

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/peel/i/gm;->aS:Landroid/widget/ImageView;

    .line 765
    iget-object v0, p0, Lcom/peel/i/gm;->ax:Ljava/util/List;

    if-nez v0, :cond_4

    .line 766
    iget-object v0, p0, Lcom/peel/i/gm;->av:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v0, v2}, Landroid/widget/AutoCompleteTextView;->setVisibility(I)V

    .line 767
    iget-object v0, p0, Lcom/peel/i/gm;->aS:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_0

    .line 761
    :cond_3
    iget-object v0, p0, Lcom/peel/i/gm;->av:Landroid/widget/AutoCompleteTextView;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "       "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget v4, Lcom/peel/ui/ft;->hint_search_box:I

    invoke-virtual {p0, v4}, Lcom/peel/i/gm;->a(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/AutoCompleteTextView;->setHint(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 770
    :cond_4
    iget-object v0, p0, Lcom/peel/i/gm;->av:Landroid/widget/AutoCompleteTextView;

    if-nez v0, :cond_5

    .line 771
    invoke-virtual {p0}, Lcom/peel/i/gm;->v()Landroid/view/View;

    move-result-object v0

    sget v3, Lcom/peel/ui/fp;->other_tv_list_filter:I

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/AutoCompleteTextView;

    iput-object v0, p0, Lcom/peel/i/gm;->av:Landroid/widget/AutoCompleteTextView;

    .line 775
    :goto_2
    iget-object v0, p0, Lcom/peel/i/gm;->av:Landroid/widget/AutoCompleteTextView;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "       "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget v4, Lcom/peel/ui/ft;->hint_search_box:I

    invoke-virtual {p0, v4}, Lcom/peel/i/gm;->a(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/AutoCompleteTextView;->setHint(Ljava/lang/CharSequence;)V

    .line 776
    invoke-virtual {p0}, Lcom/peel/i/gm;->v()Landroid/view/View;

    move-result-object v0

    sget v3, Lcom/peel/ui/fp;->search_icon_othertv:I

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/peel/i/gm;->aS:Landroid/widget/ImageView;

    .line 778
    iget-object v0, p0, Lcom/peel/i/gm;->av:Landroid/widget/AutoCompleteTextView;

    new-instance v3, Lcom/peel/i/ih;

    invoke-direct {v3, p0}, Lcom/peel/i/ih;-><init>(Lcom/peel/i/gm;)V

    invoke-virtual {v0, v3}, Landroid/widget/AutoCompleteTextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 801
    iget-object v0, p0, Lcom/peel/i/gm;->h:Landroid/widget/ImageView;

    new-instance v3, Lcom/peel/i/ii;

    invoke-direct {v3, p0}, Lcom/peel/i/ii;-><init>(Lcom/peel/i/gm;)V

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 814
    invoke-virtual {p0}, Lcom/peel/i/gm;->v()Landroid/view/View;

    move-result-object v0

    sget v3, Lcom/peel/ui/fp;->search_layout:I

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    iget-object v0, p0, Lcom/peel/i/gm;->ax:Ljava/util/List;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/peel/i/gm;->ax:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/16 v4, 0x9

    if-le v0, v4, :cond_6

    move v0, v1

    :goto_3
    invoke-virtual {v3, v0}, Landroid/view/View;->setVisibility(I)V

    .line 817
    iget-object v0, p0, Lcom/peel/i/gm;->ax:Ljava/util/List;

    new-instance v1, Lcom/peel/f/b;

    invoke-direct {v1}, Lcom/peel/f/b;-><init>()V

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 818
    new-instance v0, Lcom/peel/i/a/a;

    invoke-virtual {p0}, Lcom/peel/i/gm;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    sget v2, Lcom/peel/ui/fq;->brand_row:I

    iget-object v3, p0, Lcom/peel/i/gm;->ax:Ljava/util/List;

    invoke-direct {v0, v1, v2, v3}, Lcom/peel/i/a/a;-><init>(Landroid/content/Context;ILjava/util/List;)V

    iput-object v0, p0, Lcom/peel/i/gm;->ak:Lcom/peel/i/a/a;

    .line 819
    iget-object v0, p0, Lcom/peel/i/gm;->ar:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/peel/i/gm;->ak:Lcom/peel/i/a/a;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 820
    iput v7, p0, Lcom/peel/i/gm;->am:I

    .line 821
    iget-object v0, p0, Lcom/peel/i/gm;->ar:Landroid/widget/ListView;

    new-instance v1, Lcom/peel/i/ij;

    invoke-direct {v1, p0}, Lcom/peel/i/ij;-><init>(Lcom/peel/i/gm;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 836
    invoke-virtual {p0}, Lcom/peel/i/gm;->v()Landroid/view/View;

    move-result-object v0

    sget v1, Lcom/peel/ui/fp;->missing_tv_projector_brand_btn:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/peel/i/go;

    invoke-direct {v1, p0}, Lcom/peel/i/go;-><init>(Lcom/peel/i/gm;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_0

    .line 773
    :cond_5
    iget-object v0, p0, Lcom/peel/i/gm;->av:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v0}, Landroid/widget/AutoCompleteTextView;->getEditableText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->clear()V

    goto/16 :goto_2

    :cond_6
    move v0, v2

    .line 814
    goto :goto_3
.end method

.method static synthetic W(Lcom/peel/i/gm;)Landroid/widget/Button;
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lcom/peel/i/gm;->bb:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic X(Lcom/peel/i/gm;)I
    .locals 1

    .prologue
    .line 95
    iget v0, p0, Lcom/peel/i/gm;->an:I

    return v0
.end method

.method static synthetic Y(Lcom/peel/i/gm;)Landroid/widget/RelativeLayout;
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lcom/peel/i/gm;->bi:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method static synthetic Z(Lcom/peel/i/gm;)Landroid/widget/RelativeLayout;
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lcom/peel/i/gm;->bh:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method static synthetic a(Lcom/peel/i/gm;I)I
    .locals 0

    .prologue
    .line 95
    iput p1, p0, Lcom/peel/i/gm;->am:I

    return p1
.end method

.method static synthetic a(Lcom/peel/i/gm;Landroid/graphics/drawable/AnimationDrawable;)Landroid/graphics/drawable/AnimationDrawable;
    .locals 0

    .prologue
    .line 95
    iput-object p1, p0, Lcom/peel/i/gm;->bm:Landroid/graphics/drawable/AnimationDrawable;

    return-object p1
.end method

.method static synthetic a(Lcom/peel/i/gm;Landroid/widget/ImageView;)Landroid/widget/ImageView;
    .locals 0

    .prologue
    .line 95
    iput-object p1, p0, Lcom/peel/i/gm;->bk:Landroid/widget/ImageView;

    return-object p1
.end method

.method static synthetic a(Lcom/peel/i/gm;Lcom/peel/control/a;)Lcom/peel/control/a;
    .locals 0

    .prologue
    .line 95
    iput-object p1, p0, Lcom/peel/i/gm;->aM:Lcom/peel/control/a;

    return-object p1
.end method

.method static synthetic a(Lcom/peel/i/gm;Lcom/peel/control/h;)Lcom/peel/control/h;
    .locals 0

    .prologue
    .line 95
    iput-object p1, p0, Lcom/peel/i/gm;->aJ:Lcom/peel/control/h;

    return-object p1
.end method

.method static synthetic a(Lcom/peel/i/gm;Lcom/peel/f/a;)Lcom/peel/f/a;
    .locals 0

    .prologue
    .line 95
    iput-object p1, p0, Lcom/peel/i/gm;->ao:Lcom/peel/f/a;

    return-object p1
.end method

.method static synthetic a(Lcom/peel/i/gm;Lcom/peel/i/a/a;)Lcom/peel/i/a/a;
    .locals 0

    .prologue
    .line 95
    iput-object p1, p0, Lcom/peel/i/gm;->aj:Lcom/peel/i/a/a;

    return-object p1
.end method

.method static synthetic a(Lcom/peel/i/gm;Lcom/peel/i/in;)Lcom/peel/i/in;
    .locals 0

    .prologue
    .line 95
    iput-object p1, p0, Lcom/peel/i/gm;->bq:Lcom/peel/i/in;

    return-object p1
.end method

.method static synthetic a(Lcom/peel/i/gm;Lcom/peel/ui/dg;)Lcom/peel/ui/dg;
    .locals 0

    .prologue
    .line 95
    iput-object p1, p0, Lcom/peel/i/gm;->aQ:Lcom/peel/ui/dg;

    return-object p1
.end method

.method static synthetic a(Lcom/peel/i/gm;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 95
    iput-object p1, p0, Lcom/peel/i/gm;->aD:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic a(Lcom/peel/i/gm;Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 0

    .prologue
    .line 95
    iput-object p1, p0, Lcom/peel/i/gm;->aW:Ljava/util/ArrayList;

    return-object p1
.end method

.method static synthetic a(Lcom/peel/i/gm;Ljava/util/List;)Ljava/util/List;
    .locals 0

    .prologue
    .line 95
    iput-object p1, p0, Lcom/peel/i/gm;->aw:Ljava/util/List;

    return-object p1
.end method

.method static synthetic a(Lcom/peel/i/gm;)Ljava/util/Stack;
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lcom/peel/i/gm;->aO:Ljava/util/Stack;

    return-object v0
.end method

.method static synthetic a(Lcom/peel/i/gm;Ljava/lang/String;ZZ)V
    .locals 0

    .prologue
    .line 95
    invoke-direct {p0, p1, p2, p3}, Lcom/peel/i/gm;->a(Ljava/lang/String;ZZ)V

    return-void
.end method

.method private a(Ljava/lang/String;ZZ)V
    .locals 7

    .prologue
    .line 1911
    const/4 v6, 0x0

    .line 1912
    if-eqz p2, :cond_0

    .line 1913
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 1914
    sget v0, Lcom/peel/ui/fp;->menu_next:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1917
    :cond_0
    new-instance v0, Lcom/peel/d/a;

    sget-object v1, Lcom/peel/d/d;->b:Lcom/peel/d/d;

    sget-object v2, Lcom/peel/d/b;->a:Lcom/peel/d/b;

    sget-object v3, Lcom/peel/d/c;->b:Lcom/peel/d/c;

    move-object v4, p1

    move v5, p3

    invoke-direct/range {v0 .. v6}, Lcom/peel/d/a;-><init>(Lcom/peel/d/d;Lcom/peel/d/b;Lcom/peel/d/c;Ljava/lang/String;ZLjava/util/List;)V

    iput-object v0, p0, Lcom/peel/i/gm;->d:Lcom/peel/d/a;

    .line 1918
    invoke-virtual {p0}, Lcom/peel/i/gm;->Z()V

    .line 1919
    iget-object v0, p0, Lcom/peel/i/gm;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "category"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1920
    return-void
.end method

.method static synthetic a(Lcom/peel/i/gm;Z)Z
    .locals 0

    .prologue
    .line 95
    iput-boolean p1, p0, Lcom/peel/i/gm;->aB:Z

    return p1
.end method

.method static synthetic aa(Lcom/peel/i/gm;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lcom/peel/i/gm;->be:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic ab(Lcom/peel/i/gm;)Landroid/widget/Button;
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lcom/peel/i/gm;->bo:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic ac(Lcom/peel/i/gm;)Landroid/widget/Button;
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lcom/peel/i/gm;->bp:Landroid/widget/Button;

    return-object v0
.end method

.method private ac()V
    .locals 9

    .prologue
    const/16 v8, 0xa

    const/4 v7, 0x3

    const/4 v6, 0x0

    const/4 v1, 0x1

    .line 855
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/peel/i/gm;->ao:Lcom/peel/f/a;

    .line 856
    iput-boolean v1, p0, Lcom/peel/i/gm;->aR:Z

    .line 857
    iput v8, p0, Lcom/peel/i/gm;->am:I

    .line 858
    iput-boolean v6, p0, Lcom/peel/i/gm;->bw:Z

    .line 859
    invoke-virtual {p0}, Lcom/peel/i/gm;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    iget v2, p0, Lcom/peel/i/gm;->am:I

    invoke-static {v0, v2}, Lcom/peel/util/bx;->c(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v0

    iget-boolean v2, p0, Lcom/peel/i/gm;->bw:Z

    invoke-direct {p0, v0, v1, v2}, Lcom/peel/i/gm;->a(Ljava/lang/String;ZZ)V

    .line 860
    invoke-static {}, Lcom/peel/util/a/f;->a()Lcom/peel/util/a/f;

    move-result-object v2

    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v0}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v0

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    const/16 v3, 0xbeb

    const/16 v4, 0x7d5

    const-string/jumbo v5, "SCREEN_OTHER_TV_PROJECTOR_BRAND_LIST"

    invoke-virtual {v2, v0, v3, v4, v5}, Lcom/peel/util/a/f;->a(IIILjava/lang/String;)V

    .line 862
    iget-object v0, p0, Lcom/peel/i/gm;->i:Landroid/widget/ViewFlipper;

    invoke-virtual {v0, v7}, Landroid/widget/ViewFlipper;->setDisplayedChild(I)V

    .line 863
    sput v7, Lcom/peel/i/gm;->f:I

    .line 865
    invoke-virtual {p0}, Lcom/peel/i/gm;->v()Landroid/view/View;

    move-result-object v0

    sget v2, Lcom/peel/ui/fp;->other_tv_list_filter:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/AutoCompleteTextView;

    iput-object v0, p0, Lcom/peel/i/gm;->av:Landroid/widget/AutoCompleteTextView;

    .line 867
    invoke-virtual {p0}, Lcom/peel/i/gm;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    const-string/jumbo v2, "input_method"

    invoke-virtual {v0, v2}, Landroid/support/v4/app/ae;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 868
    iget-object v2, p0, Lcom/peel/i/gm;->av:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v0, v2, v6}, Landroid/view/inputmethod/InputMethodManager;->showSoftInput(Landroid/view/View;I)Z

    .line 869
    invoke-virtual {p0}, Lcom/peel/i/gm;->v()Landroid/view/View;

    move-result-object v0

    sget v2, Lcom/peel/ui/fp;->other_list:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/peel/i/gm;->ar:Landroid/widget/ListView;

    .line 870
    invoke-virtual {p0}, Lcom/peel/i/gm;->v()Landroid/view/View;

    move-result-object v0

    sget v2, Lcom/peel/ui/fp;->search_layout:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/peel/i/gm;->aV:Landroid/widget/RelativeLayout;

    .line 871
    invoke-virtual {p0}, Lcom/peel/i/gm;->v()Landroid/view/View;

    move-result-object v0

    sget v2, Lcom/peel/ui/fp;->search_cancel_btn:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/peel/i/gm;->h:Landroid/widget/ImageView;

    .line 872
    iget-object v0, p0, Lcom/peel/i/gm;->av:Landroid/widget/AutoCompleteTextView;

    if-nez v0, :cond_2

    .line 873
    invoke-virtual {p0}, Lcom/peel/i/gm;->v()Landroid/view/View;

    move-result-object v0

    sget v2, Lcom/peel/ui/fp;->other_tv_list_filter:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/AutoCompleteTextView;

    iput-object v0, p0, Lcom/peel/i/gm;->av:Landroid/widget/AutoCompleteTextView;

    .line 878
    :goto_1
    iget-object v0, p0, Lcom/peel/i/gm;->ba:Ljava/lang/String;

    const-string/jumbo v2, "iw"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/peel/i/gm;->ba:Ljava/lang/String;

    const-string/jumbo v2, "ar"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 879
    :cond_0
    iget-object v0, p0, Lcom/peel/i/gm;->av:Landroid/widget/AutoCompleteTextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget v3, Lcom/peel/ui/ft;->hint_search_box:I

    invoke-virtual {p0, v3}, Lcom/peel/i/gm;->a(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/AutoCompleteTextView;->setHint(Ljava/lang/CharSequence;)V

    .line 883
    :goto_2
    invoke-virtual {p0}, Lcom/peel/i/gm;->v()Landroid/view/View;

    move-result-object v0

    sget v2, Lcom/peel/ui/fp;->search_icon_othertv:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/peel/i/gm;->aS:Landroid/widget/ImageView;

    .line 884
    iget-object v0, p0, Lcom/peel/i/gm;->av:Landroid/widget/AutoCompleteTextView;

    new-instance v2, Lcom/peel/i/gq;

    invoke-direct {v2, p0}, Lcom/peel/i/gq;-><init>(Lcom/peel/i/gm;)V

    invoke-virtual {v0, v2}, Landroid/widget/AutoCompleteTextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 906
    iget-object v0, p0, Lcom/peel/i/gm;->h:Landroid/widget/ImageView;

    new-instance v2, Lcom/peel/i/gr;

    invoke-direct {v2, p0}, Lcom/peel/i/gr;-><init>(Lcom/peel/i/gm;)V

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 920
    iget-object v0, p0, Lcom/peel/i/gm;->ay:Ljava/util/List;

    if-nez v0, :cond_4

    .line 922
    sget-object v0, Lcom/peel/i/gm;->e:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/peel/i/gm;->m()Landroid/support/v4/app/ae;

    move-result-object v2

    invoke-static {v0, v2, v1}, Lcom/peel/util/bx;->a(Ljava/lang/String;Landroid/app/Activity;Z)V

    .line 923
    const-string/jumbo v0, "https://partners-ir.peel.com"

    iget-object v2, p0, Lcom/peel/i/gm;->aE:Ljava/lang/String;

    new-instance v3, Lcom/peel/i/gs;

    invoke-direct {v3, p0, v1}, Lcom/peel/i/gs;-><init>(Lcom/peel/i/gm;I)V

    invoke-static {v0, v8, v2, v3}, Lcom/peel/control/aa;->a(Ljava/lang/String;ILjava/lang/String;Lcom/peel/util/t;)V

    .line 951
    :goto_3
    iget-object v0, p0, Lcom/peel/i/gm;->ar:Landroid/widget/ListView;

    new-instance v1, Lcom/peel/i/gt;

    invoke-direct {v1, p0}, Lcom/peel/i/gt;-><init>(Lcom/peel/i/gm;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 973
    invoke-virtual {p0}, Lcom/peel/i/gm;->v()Landroid/view/View;

    move-result-object v0

    sget v1, Lcom/peel/ui/fp;->missing_tv_projector_brand_btn:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/peel/i/gu;

    invoke-direct {v1, p0}, Lcom/peel/i/gu;-><init>(Lcom/peel/i/gm;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 988
    return-void

    .line 860
    :cond_1
    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v0}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/data/at;->f()I

    move-result v0

    goto/16 :goto_0

    .line 875
    :cond_2
    iget-object v0, p0, Lcom/peel/i/gm;->av:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v0}, Landroid/widget/AutoCompleteTextView;->getEditableText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->clear()V

    goto/16 :goto_1

    .line 881
    :cond_3
    iget-object v0, p0, Lcom/peel/i/gm;->av:Landroid/widget/AutoCompleteTextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "       "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget v3, Lcom/peel/ui/ft;->hint_search_box:I

    invoke-virtual {p0, v3}, Lcom/peel/i/gm;->a(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/AutoCompleteTextView;->setHint(Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    .line 946
    :cond_4
    new-instance v0, Lcom/peel/i/a/a;

    invoke-virtual {p0}, Lcom/peel/i/gm;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    sget v2, Lcom/peel/ui/fq;->brand_row:I

    iget-object v3, p0, Lcom/peel/i/gm;->ay:Ljava/util/List;

    invoke-direct {v0, v1, v2, v3}, Lcom/peel/i/a/a;-><init>(Landroid/content/Context;ILjava/util/List;)V

    iput-object v0, p0, Lcom/peel/i/gm;->al:Lcom/peel/i/a/a;

    .line 947
    iget-object v0, p0, Lcom/peel/i/gm;->ar:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/peel/i/gm;->al:Lcom/peel/i/a/a;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    goto :goto_3
.end method

.method private ad()Ljava/lang/String;
    .locals 1

    .prologue
    .line 991
    iget-boolean v0, p0, Lcom/peel/i/gm;->aB:Z

    if-eqz v0, :cond_0

    sget v0, Lcom/peel/ui/ft;->DeviceType10:I

    invoke-virtual {p0, v0}, Lcom/peel/i/gm;->a(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    sget v0, Lcom/peel/ui/ft;->tv:I

    invoke-virtual {p0, v0}, Lcom/peel/i/gm;->a(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method static synthetic ad(Lcom/peel/i/gm;)V
    .locals 0

    .prologue
    .line 95
    invoke-direct {p0}, Lcom/peel/i/gm;->ae()V

    return-void
.end method

.method static synthetic ae(Lcom/peel/i/gm;)Lcom/peel/i/in;
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lcom/peel/i/gm;->bq:Lcom/peel/i/in;

    return-object v0
.end method

.method private ae()V
    .locals 5

    .prologue
    .line 1023
    iget-object v1, p0, Lcom/peel/i/gm;->bf:Landroid/widget/TextView;

    iget v0, p0, Lcom/peel/i/gm;->aY:I

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1024
    return-void

    .line 1023
    :cond_0
    sget v0, Lcom/peel/ui/ft;->ir_send_code:I

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget v4, p0, Lcom/peel/i/gm;->aY:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {p0, v0, v2}, Lcom/peel/i/gm;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method static synthetic af(Lcom/peel/i/gm;)I
    .locals 1

    .prologue
    .line 95
    iget v0, p0, Lcom/peel/i/gm;->br:I

    return v0
.end method

.method private af()V
    .locals 11

    .prologue
    const/4 v10, 0x4

    const/4 v4, 0x0

    const/4 v9, 0x1

    const/4 v0, 0x0

    .line 1027
    const-string/jumbo v1, "Power"

    iput-object v1, p0, Lcom/peel/i/gm;->aZ:Ljava/lang/String;

    .line 1028
    iput v0, p0, Lcom/peel/i/gm;->br:I

    .line 1029
    invoke-virtual {p0}, Lcom/peel/i/gm;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    const-string/jumbo v2, "input_method"

    invoke-virtual {v1, v2}, Landroid/support/v4/app/ae;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/inputmethod/InputMethodManager;

    .line 1030
    invoke-virtual {p0}, Lcom/peel/i/gm;->m()Landroid/support/v4/app/ae;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/ae;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 1034
    iget-object v1, p0, Lcom/peel/i/gm;->aM:Lcom/peel/control/a;

    if-eqz v1, :cond_0

    .line 1035
    iget-object v1, p0, Lcom/peel/i/gm;->aL:Lcom/peel/control/RoomControl;

    iget-object v2, p0, Lcom/peel/i/gm;->aM:Lcom/peel/control/a;

    invoke-virtual {v1, v2}, Lcom/peel/control/RoomControl;->b(Lcom/peel/control/a;)V

    .line 1037
    sget-object v1, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    iget-object v2, p0, Lcom/peel/i/gm;->aM:Lcom/peel/control/a;

    invoke-virtual {v1, v2}, Lcom/peel/control/am;->a(Lcom/peel/control/a;)V

    .line 1038
    iput-object v4, p0, Lcom/peel/i/gm;->aM:Lcom/peel/control/a;

    .line 1041
    :cond_0
    iget-object v1, p0, Lcom/peel/i/gm;->aN:Lcom/peel/content/library/LiveLibrary;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/peel/i/gm;->aN:Lcom/peel/content/library/LiveLibrary;

    invoke-virtual {v1}, Lcom/peel/content/library/LiveLibrary;->j()Ljava/lang/String;

    move-result-object v1

    .line 1042
    :goto_0
    iget-object v2, p0, Lcom/peel/i/gm;->c:Landroid/os/Bundle;

    const-string/jumbo v3, "setup_google_tv"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    const-string/jumbo v1, "Netflix"

    :cond_1
    invoke-static {v1}, Lcom/peel/control/a;->a(Ljava/lang/String;)Lcom/peel/control/a;

    move-result-object v1

    iput-object v1, p0, Lcom/peel/i/gm;->aM:Lcom/peel/control/a;

    .line 1043
    iget-object v1, p0, Lcom/peel/i/gm;->aL:Lcom/peel/control/RoomControl;

    iget-object v2, p0, Lcom/peel/i/gm;->aM:Lcom/peel/control/a;

    invoke-virtual {v1, v2}, Lcom/peel/control/RoomControl;->a(Lcom/peel/control/a;)V

    .line 1047
    iget v1, p0, Lcom/peel/i/gm;->am:I

    iget-object v2, p0, Lcom/peel/i/gm;->ao:Lcom/peel/f/a;

    invoke-virtual {v2}, Lcom/peel/f/a;->b()Ljava/lang/String;

    move-result-object v2

    const/4 v5, -0x1

    move v3, v0

    move-object v6, v4

    move-object v7, v4

    move-object v8, v4

    invoke-static/range {v0 .. v8}, Lcom/peel/control/h;->a(IILjava/lang/String;ZLjava/lang/String;ILandroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;)Lcom/peel/control/h;

    move-result-object v1

    iput-object v1, p0, Lcom/peel/i/gm;->aJ:Lcom/peel/control/h;

    .line 1049
    invoke-static {}, Lcom/peel/util/a/f;->a()Lcom/peel/util/a/f;

    move-result-object v2

    sget-object v1, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v1}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v1

    if-nez v1, :cond_3

    move v1, v9

    :goto_1
    const/16 v3, 0xbeb

    const/16 v4, 0x7d5

    const-string/jumbo v5, "SCREEN_TURN_ON_TV"

    invoke-virtual {v2, v1, v3, v4, v5}, Lcom/peel/util/a/f;->a(IIILjava/lang/String;)V

    .line 1051
    iget-object v1, p0, Lcom/peel/i/gm;->i:Landroid/widget/ViewFlipper;

    invoke-virtual {v1, v10}, Landroid/widget/ViewFlipper;->setDisplayedChild(I)V

    .line 1052
    sput v10, Lcom/peel/i/gm;->f:I

    .line 1055
    sget-object v1, Lcom/peel/i/gm;->e:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/peel/i/gm;->m()Landroid/support/v4/app/ae;

    move-result-object v2

    invoke-static {v1, v2, v9}, Lcom/peel/util/bx;->a(Ljava/lang/String;Landroid/app/Activity;Z)V

    .line 1057
    sget v1, Lcom/peel/ui/ft;->testing_device:I

    new-array v2, v9, [Ljava/lang/Object;

    invoke-direct {p0}, Lcom/peel/i/gm;->am()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    invoke-virtual {p0, v1, v2}, Lcom/peel/i/gm;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1, v0, v9}, Lcom/peel/i/gm;->a(Ljava/lang/String;ZZ)V

    .line 1058
    invoke-virtual {p0}, Lcom/peel/i/gm;->v()Landroid/view/View;

    move-result-object v1

    sget v2, Lcom/peel/ui/fp;->coverView:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    new-instance v2, Lcom/peel/i/gx;

    invoke-direct {v2, p0}, Lcom/peel/i/gx;-><init>(Lcom/peel/i/gm;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1065
    invoke-virtual {p0}, Lcom/peel/i/gm;->v()Landroid/view/View;

    move-result-object v1

    sget v2, Lcom/peel/ui/fp;->tv_visual:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/peel/i/gm;->bl:Landroid/widget/ImageView;

    .line 1066
    iget-object v2, p0, Lcom/peel/i/gm;->bl:Landroid/widget/ImageView;

    iget-boolean v1, p0, Lcom/peel/i/gm;->aB:Z

    if-eqz v1, :cond_4

    sget v1, Lcom/peel/ui/fo;->test_projector_drawing:I

    :goto_2
    invoke-virtual {v2, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1068
    invoke-virtual {p0}, Lcom/peel/i/gm;->v()Landroid/view/View;

    move-result-object v1

    sget v2, Lcom/peel/ui/fp;->testing_turn_on_msg:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/peel/i/gm;->bd:Landroid/widget/TextView;

    .line 1069
    iget-object v1, p0, Lcom/peel/i/gm;->bd:Landroid/widget/TextView;

    sget v2, Lcom/peel/ui/ft;->testing_tv_msg:I

    new-array v3, v9, [Ljava/lang/Object;

    invoke-direct {p0}, Lcom/peel/i/gm;->ad()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v0

    invoke-virtual {p0, v2, v3}, Lcom/peel/i/gm;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1071
    invoke-virtual {p0}, Lcom/peel/i/gm;->v()Landroid/view/View;

    move-result-object v1

    sget v2, Lcom/peel/ui/fp;->test_btn_viewpager:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/peel/widget/TestBtnViewPager;

    iput-object v1, p0, Lcom/peel/i/gm;->bn:Lcom/peel/widget/TestBtnViewPager;

    .line 1072
    iget-object v1, p0, Lcom/peel/i/gm;->bn:Lcom/peel/widget/TestBtnViewPager;

    invoke-virtual {v1, v9}, Lcom/peel/widget/TestBtnViewPager;->setEnabledSwipe(Z)V

    .line 1073
    iget-object v1, p0, Lcom/peel/i/gm;->bn:Lcom/peel/widget/TestBtnViewPager;

    invoke-virtual {v1, v10}, Lcom/peel/widget/TestBtnViewPager;->setVisibility(I)V

    .line 1075
    invoke-virtual {p0}, Lcom/peel/i/gm;->v()Landroid/view/View;

    move-result-object v1

    sget v2, Lcom/peel/ui/fp;->test_pager_left_btn:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/peel/i/gm;->bo:Landroid/widget/Button;

    .line 1076
    iget-object v1, p0, Lcom/peel/i/gm;->bo:Landroid/widget/Button;

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setEnabled(Z)V

    .line 1077
    iget-object v1, p0, Lcom/peel/i/gm;->bo:Landroid/widget/Button;

    new-instance v2, Lcom/peel/i/gy;

    invoke-direct {v2, p0}, Lcom/peel/i/gy;-><init>(Lcom/peel/i/gm;)V

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1086
    invoke-virtual {p0}, Lcom/peel/i/gm;->v()Landroid/view/View;

    move-result-object v1

    sget v2, Lcom/peel/ui/fp;->test_pager_right_btn:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/peel/i/gm;->bp:Landroid/widget/Button;

    .line 1087
    iget-object v1, p0, Lcom/peel/i/gm;->bp:Landroid/widget/Button;

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setEnabled(Z)V

    .line 1088
    iget-object v1, p0, Lcom/peel/i/gm;->bp:Landroid/widget/Button;

    new-instance v2, Lcom/peel/i/gz;

    invoke-direct {v2, p0}, Lcom/peel/i/gz;-><init>(Lcom/peel/i/gm;)V

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1096
    iget-object v1, p0, Lcom/peel/i/gm;->bo:Landroid/widget/Button;

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setVisibility(I)V

    .line 1097
    iget-object v1, p0, Lcom/peel/i/gm;->bp:Landroid/widget/Button;

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setVisibility(I)V

    .line 1098
    invoke-virtual {p0}, Lcom/peel/i/gm;->v()Landroid/view/View;

    move-result-object v1

    sget v2, Lcom/peel/ui/fp;->test_question_msg:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/peel/i/gm;->bg:Landroid/widget/TextView;

    .line 1099
    iget-object v1, p0, Lcom/peel/i/gm;->bg:Landroid/widget/TextView;

    sget v2, Lcom/peel/ui/ft;->device_test_turn_on_question_msg:I

    new-array v3, v9, [Ljava/lang/Object;

    invoke-direct {p0}, Lcom/peel/i/gm;->ad()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v0

    invoke-virtual {p0, v2, v3}, Lcom/peel/i/gm;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1101
    invoke-virtual {p0}, Lcom/peel/i/gm;->v()Landroid/view/View;

    move-result-object v1

    sget v2, Lcom/peel/ui/fp;->layout_test_btn:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    iput-object v1, p0, Lcom/peel/i/gm;->bi:Landroid/widget/RelativeLayout;

    .line 1102
    invoke-virtual {p0}, Lcom/peel/i/gm;->v()Landroid/view/View;

    move-result-object v1

    sget v2, Lcom/peel/ui/fp;->layout_test_msg:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    iput-object v1, p0, Lcom/peel/i/gm;->bh:Landroid/widget/RelativeLayout;

    .line 1104
    iget-object v1, p0, Lcom/peel/i/gm;->bi:Landroid/widget/RelativeLayout;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1105
    iget-object v1, p0, Lcom/peel/i/gm;->bh:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v0}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1107
    invoke-virtual {p0}, Lcom/peel/i/gm;->v()Landroid/view/View;

    move-result-object v1

    sget v2, Lcom/peel/ui/fp;->turn_on_msg:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/peel/i/gm;->be:Landroid/widget/TextView;

    .line 1108
    iget-object v1, p0, Lcom/peel/i/gm;->be:Landroid/widget/TextView;

    sget v2, Lcom/peel/ui/ft;->testing_key_power:I

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/peel/i/gm;->ao:Lcom/peel/f/a;

    invoke-virtual {v4}, Lcom/peel/f/a;->b()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v0

    invoke-direct {p0}, Lcom/peel/i/gm;->ad()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v9

    invoke-virtual {p0, v2, v3}, Lcom/peel/i/gm;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1110
    invoke-virtual {p0}, Lcom/peel/i/gm;->v()Landroid/view/View;

    move-result-object v0

    sget v1, Lcom/peel/ui/fp;->test_status_msg:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/peel/i/gm;->bf:Landroid/widget/TextView;

    .line 1111
    iget-object v0, p0, Lcom/peel/i/gm;->bf:Landroid/widget/TextView;

    new-instance v1, Lcom/peel/i/hb;

    invoke-direct {v1, p0}, Lcom/peel/i/hb;-><init>(Lcom/peel/i/gm;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 1120
    invoke-virtual {p0}, Lcom/peel/i/gm;->v()Landroid/view/View;

    move-result-object v0

    sget v1, Lcom/peel/ui/fp;->yes_btn:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/peel/i/gm;->bb:Landroid/widget/Button;

    .line 1121
    invoke-virtual {p0}, Lcom/peel/i/gm;->v()Landroid/view/View;

    move-result-object v0

    sget v1, Lcom/peel/ui/fp;->no_btn:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/peel/i/gm;->bc:Landroid/widget/Button;

    .line 1123
    iget-object v0, p0, Lcom/peel/i/gm;->bb:Landroid/widget/Button;

    new-instance v1, Lcom/peel/i/hc;

    invoke-direct {v1, p0}, Lcom/peel/i/hc;-><init>(Lcom/peel/i/gm;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1169
    iget-object v0, p0, Lcom/peel/i/gm;->bc:Landroid/widget/Button;

    new-instance v1, Lcom/peel/i/he;

    invoke-direct {v1, p0}, Lcom/peel/i/he;-><init>(Lcom/peel/i/gm;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1202
    invoke-virtual {p0}, Lcom/peel/i/gm;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string/jumbo v1, "country_ISO"

    const-string/jumbo v2, "US"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 1203
    const-string/jumbo v0, "https://partners-ir.peel.com"

    iget-object v1, p0, Lcom/peel/i/gm;->aZ:Ljava/lang/String;

    iget v2, p0, Lcom/peel/i/gm;->am:I

    iget-object v3, p0, Lcom/peel/i/gm;->ao:Lcom/peel/f/a;

    invoke-virtual {v3}, Lcom/peel/f/a;->a()I

    move-result v3

    new-instance v5, Lcom/peel/i/hf;

    invoke-direct {v5, p0, v9, v4}, Lcom/peel/i/hf;-><init>(Lcom/peel/i/gm;ILjava/lang/String;)V

    invoke-static/range {v0 .. v5}, Lcom/peel/control/aa;->a(Ljava/lang/String;Ljava/lang/String;IILjava/lang/String;Lcom/peel/util/t;)V

    .line 1329
    return-void

    :cond_2
    move-object v1, v4

    .line 1041
    goto/16 :goto_0

    .line 1049
    :cond_3
    sget-object v1, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v1}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/data/at;->f()I

    move-result v1

    goto/16 :goto_1

    .line 1066
    :cond_4
    sget v1, Lcom/peel/ui/fo;->test_tv_drawing:I

    goto/16 :goto_2
.end method

.method static synthetic ag(Lcom/peel/i/gm;)I
    .locals 1

    .prologue
    .line 95
    iget v0, p0, Lcom/peel/i/gm;->aY:I

    return v0
.end method

.method private ag()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v1, 0x1

    .line 1332
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "renderChannelChangerScreen: watch tv activity: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v0, p0, Lcom/peel/i/gm;->aM:Lcom/peel/control/a;

    if-eqz v0, :cond_0

    const-string/jumbo v0, "NOT NULL"

    :goto_0
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1333
    sget-object v2, Lcom/peel/i/gm;->e:Ljava/lang/String;

    invoke-static {v2, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1334
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "renderChannelChangerScreen tv device: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v0, p0, Lcom/peel/i/gm;->aJ:Lcom/peel/control/h;

    if-eqz v0, :cond_1

    const-string/jumbo v0, "NOT NULL"

    :goto_1
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1335
    sget-object v2, Lcom/peel/i/gm;->e:Ljava/lang/String;

    invoke-static {v2, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1336
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "renderChannelChangerScreen room.getActivities(): "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v0, p0, Lcom/peel/i/gm;->aL:Lcom/peel/control/RoomControl;

    invoke-virtual {v0}, Lcom/peel/control/RoomControl;->c()[Lcom/peel/control/a;

    move-result-object v0

    if-nez v0, :cond_2

    const-string/jumbo v0, "NO ACTIVITIES"

    :goto_2
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1337
    sget-object v2, Lcom/peel/i/gm;->e:Ljava/lang/String;

    invoke-static {v2, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1339
    invoke-static {}, Lcom/peel/util/a/f;->a()Lcom/peel/util/a/f;

    move-result-object v2

    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v0}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v0

    if-nez v0, :cond_3

    move v0, v1

    :goto_3
    const/16 v3, 0xbeb

    const/16 v4, 0x7d5

    const-string/jumbo v5, "SCREEN_CHANGE_CHANNEL"

    invoke-virtual {v2, v0, v3, v4, v5}, Lcom/peel/util/a/f;->a(IIILjava/lang/String;)V

    .line 1341
    iget-object v0, p0, Lcom/peel/i/gm;->i:Landroid/widget/ViewFlipper;

    const/4 v2, 0x6

    invoke-virtual {v0, v2}, Landroid/widget/ViewFlipper;->setDisplayedChild(I)V

    .line 1342
    const/16 v0, 0x64

    sput v0, Lcom/peel/i/gm;->f:I

    .line 1343
    sget v0, Lcom/peel/ui/ft;->choose_channel_changer:I

    invoke-virtual {p0, v0}, Lcom/peel/i/gm;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, v6, v1}, Lcom/peel/i/gm;->a(Ljava/lang/String;ZZ)V

    .line 1352
    invoke-virtual {p0}, Lcom/peel/i/gm;->v()Landroid/view/View;

    move-result-object v0

    sget v2, Lcom/peel/ui/fp;->tv_with_dvr_textview:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    sget v2, Lcom/peel/ui/ft;->DeviceType2:I

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    .line 1353
    invoke-virtual {p0}, Lcom/peel/i/gm;->v()Landroid/view/View;

    move-result-object v0

    sget v2, Lcom/peel/ui/fp;->tv_only_textview:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    sget v2, Lcom/peel/ui/ft;->my_brand_tv:I

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/peel/i/gm;->ao:Lcom/peel/f/a;

    invoke-virtual {v4}, Lcom/peel/f/a;->b()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-direct {p0}, Lcom/peel/i/gm;->ad()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v1

    invoke-virtual {p0, v2, v3}, Lcom/peel/i/gm;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1354
    return-void

    .line 1332
    :cond_0
    const-string/jumbo v0, "NULL"

    goto/16 :goto_0

    .line 1334
    :cond_1
    const-string/jumbo v0, "NULL"

    goto/16 :goto_1

    .line 1336
    :cond_2
    iget-object v0, p0, Lcom/peel/i/gm;->aL:Lcom/peel/control/RoomControl;

    invoke-virtual {v0}, Lcom/peel/control/RoomControl;->c()[Lcom/peel/control/a;

    move-result-object v0

    array-length v0, v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto/16 :goto_2

    .line 1339
    :cond_3
    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v0}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/data/at;->f()I

    move-result v0

    goto :goto_3
.end method

.method static synthetic ah(Lcom/peel/i/gm;)Ljava/util/List;
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lcom/peel/i/gm;->az:Ljava/util/List;

    return-object v0
.end method

.method private ah()V
    .locals 8

    .prologue
    const/4 v7, 0x7

    const/4 v6, 0x0

    const/4 v1, 0x1

    .line 1357
    invoke-static {}, Lcom/peel/util/a/f;->a()Lcom/peel/util/a/f;

    move-result-object v2

    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v0}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    const/16 v3, 0xbeb

    const/16 v4, 0x7d5

    const-string/jumbo v5, "SCREEN_STB_BRAND_LIST"

    invoke-virtual {v2, v0, v3, v4, v5}, Lcom/peel/util/a/f;->a(IIILjava/lang/String;)V

    .line 1358
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/peel/i/gm;->ap:Lcom/peel/f/a;

    .line 1359
    iput-boolean v6, p0, Lcom/peel/i/gm;->bt:Z

    .line 1360
    iput-boolean v6, p0, Lcom/peel/i/gm;->bu:Z

    .line 1361
    iget-object v0, p0, Lcom/peel/i/gm;->i:Landroid/widget/ViewFlipper;

    invoke-virtual {v0, v7}, Landroid/widget/ViewFlipper;->setDisplayedChild(I)V

    .line 1362
    sput v7, Lcom/peel/i/gm;->f:I

    .line 1363
    sget v0, Lcom/peel/ui/ft;->action_bar_title_stb:I

    invoke-virtual {p0, v0}, Lcom/peel/i/gm;->a(I)Ljava/lang/String;

    move-result-object v0

    iget-boolean v2, p0, Lcom/peel/i/gm;->bx:Z

    invoke-direct {p0, v0, v1, v2}, Lcom/peel/i/gm;->a(Ljava/lang/String;ZZ)V

    .line 1364
    invoke-virtual {p0}, Lcom/peel/i/gm;->v()Landroid/view/View;

    move-result-object v0

    sget v2, Lcom/peel/ui/fp;->stb_brand_list:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/peel/i/gm;->as:Landroid/widget/ListView;

    .line 1366
    iget-object v0, p0, Lcom/peel/i/gm;->az:Ljava/util/List;

    if-nez v0, :cond_1

    .line 1367
    sget-object v0, Lcom/peel/i/gm;->e:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/peel/i/gm;->m()Landroid/support/v4/app/ae;

    move-result-object v2

    invoke-static {v0, v2, v1}, Lcom/peel/util/bx;->a(Ljava/lang/String;Landroid/app/Activity;Z)V

    .line 1368
    sget-object v0, Lcom/peel/content/a/j;->b:Ljava/lang/String;

    iget-object v2, p0, Lcom/peel/i/gm;->aN:Lcom/peel/content/library/LiveLibrary;

    invoke-virtual {v2}, Lcom/peel/content/library/LiveLibrary;->e()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/peel/i/hi;

    invoke-direct {v3, p0, v1}, Lcom/peel/i/hi;-><init>(Lcom/peel/i/gm;I)V

    invoke-static {v0, v2, v3}, Lcom/peel/control/aa;->a(Ljava/lang/String;Ljava/lang/String;Lcom/peel/util/t;)V

    .line 1441
    :goto_1
    iget-object v0, p0, Lcom/peel/i/gm;->as:Landroid/widget/ListView;

    new-instance v1, Lcom/peel/i/hj;

    invoke-direct {v1, p0}, Lcom/peel/i/hj;-><init>(Lcom/peel/i/gm;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 1455
    return-void

    .line 1357
    :cond_0
    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v0}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/data/at;->f()I

    move-result v0

    goto :goto_0

    .line 1437
    :cond_1
    new-instance v0, Lcom/peel/i/a/a;

    invoke-virtual {p0}, Lcom/peel/i/gm;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    sget v2, Lcom/peel/ui/fq;->brand_row:I

    iget-object v3, p0, Lcom/peel/i/gm;->az:Ljava/util/List;

    invoke-direct {v0, v1, v2, v3}, Lcom/peel/i/a/a;-><init>(Landroid/content/Context;ILjava/util/List;)V

    iput-object v0, p0, Lcom/peel/i/gm;->aG:Lcom/peel/i/a/a;

    .line 1438
    iget-object v0, p0, Lcom/peel/i/gm;->as:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/peel/i/gm;->aG:Lcom/peel/i/a/a;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    goto :goto_1
.end method

.method static synthetic ai(Lcom/peel/i/gm;)Lcom/peel/i/a/a;
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lcom/peel/i/gm;->aG:Lcom/peel/i/a/a;

    return-object v0
.end method

.method private ai()V
    .locals 7

    .prologue
    const/16 v6, 0x8

    const/4 v1, 0x1

    .line 1458
    invoke-static {}, Lcom/peel/util/a/f;->a()Lcom/peel/util/a/f;

    move-result-object v2

    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v0}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v0

    if-nez v0, :cond_2

    move v0, v1

    :goto_0
    const/16 v3, 0xbeb

    const/16 v4, 0x7d5

    const-string/jumbo v5, "SCREEN_OTHER_STB_BRAND_LIST"

    invoke-virtual {v2, v0, v3, v4, v5}, Lcom/peel/util/a/f;->a(IIILjava/lang/String;)V

    .line 1459
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/peel/i/gm;->ap:Lcom/peel/f/a;

    .line 1460
    sget v0, Lcom/peel/ui/ft;->action_bar_title_stb:I

    invoke-virtual {p0, v0}, Lcom/peel/i/gm;->a(I)Ljava/lang/String;

    move-result-object v0

    iget-boolean v2, p0, Lcom/peel/i/gm;->by:Z

    invoke-direct {p0, v0, v1, v2}, Lcom/peel/i/gm;->a(Ljava/lang/String;ZZ)V

    .line 1461
    iget-object v0, p0, Lcom/peel/i/gm;->i:Landroid/widget/ViewFlipper;

    invoke-virtual {v0, v6}, Landroid/widget/ViewFlipper;->setDisplayedChild(I)V

    .line 1462
    sput v6, Lcom/peel/i/gm;->f:I

    .line 1464
    invoke-virtual {p0}, Lcom/peel/i/gm;->v()Landroid/view/View;

    move-result-object v0

    sget v2, Lcom/peel/ui/fp;->search_settop_cancel_btn:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/peel/i/gm;->h:Landroid/widget/ImageView;

    .line 1465
    invoke-virtual {p0}, Lcom/peel/i/gm;->v()Landroid/view/View;

    move-result-object v0

    sget v2, Lcom/peel/ui/fp;->search_setup_layout:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/peel/i/gm;->aU:Landroid/widget/RelativeLayout;

    .line 1467
    iget-object v0, p0, Lcom/peel/i/gm;->at:Landroid/widget/ListView;

    if-nez v0, :cond_0

    .line 1468
    invoke-virtual {p0}, Lcom/peel/i/gm;->v()Landroid/view/View;

    move-result-object v0

    sget v2, Lcom/peel/ui/fp;->other_settop_stb_brand_list:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/peel/i/gm;->at:Landroid/widget/ListView;

    .line 1471
    :cond_0
    iget-object v0, p0, Lcom/peel/i/gm;->au:Landroid/widget/AutoCompleteTextView;

    if-nez v0, :cond_3

    .line 1472
    invoke-virtual {p0}, Lcom/peel/i/gm;->v()Landroid/view/View;

    move-result-object v0

    sget v2, Lcom/peel/ui/fp;->other_settopbox_list_filter:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/AutoCompleteTextView;

    iput-object v0, p0, Lcom/peel/i/gm;->au:Landroid/widget/AutoCompleteTextView;

    .line 1476
    :goto_1
    iget-object v0, p0, Lcom/peel/i/gm;->ba:Ljava/lang/String;

    const-string/jumbo v2, "iw"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/peel/i/gm;->ba:Ljava/lang/String;

    const-string/jumbo v2, "ar"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1477
    :cond_1
    iget-object v0, p0, Lcom/peel/i/gm;->au:Landroid/widget/AutoCompleteTextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget v3, Lcom/peel/ui/ft;->hint_search_box:I

    invoke-virtual {p0, v3}, Lcom/peel/i/gm;->a(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/AutoCompleteTextView;->setHint(Ljava/lang/CharSequence;)V

    .line 1482
    :goto_2
    invoke-virtual {p0}, Lcom/peel/i/gm;->v()Landroid/view/View;

    move-result-object v0

    sget v2, Lcom/peel/ui/fp;->search_icon_othersetupbox:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/peel/i/gm;->aT:Landroid/widget/ImageView;

    .line 1483
    iget-object v0, p0, Lcom/peel/i/gm;->au:Landroid/widget/AutoCompleteTextView;

    new-instance v2, Lcom/peel/i/hk;

    invoke-direct {v2, p0}, Lcom/peel/i/hk;-><init>(Lcom/peel/i/gm;)V

    invoke-virtual {v0, v2}, Landroid/widget/AutoCompleteTextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 1505
    iget-object v0, p0, Lcom/peel/i/gm;->h:Landroid/widget/ImageView;

    new-instance v2, Lcom/peel/i/hl;

    invoke-direct {v2, p0}, Lcom/peel/i/hl;-><init>(Lcom/peel/i/gm;)V

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1518
    iget-object v0, p0, Lcom/peel/i/gm;->at:Landroid/widget/ListView;

    new-instance v2, Lcom/peel/i/hm;

    invoke-direct {v2, p0}, Lcom/peel/i/hm;-><init>(Lcom/peel/i/gm;)V

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 1532
    sget-object v0, Lcom/peel/i/gm;->e:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/peel/i/gm;->m()Landroid/support/v4/app/ae;

    move-result-object v2

    invoke-static {v0, v2, v1}, Lcom/peel/util/bx;->a(Ljava/lang/String;Landroid/app/Activity;Z)V

    .line 1533
    const-string/jumbo v0, "https://partners-ir.peel.com"

    const/4 v2, 0x2

    iget-object v3, p0, Lcom/peel/i/gm;->aE:Ljava/lang/String;

    new-instance v4, Lcom/peel/i/hn;

    invoke-direct {v4, p0, v1}, Lcom/peel/i/hn;-><init>(Lcom/peel/i/gm;I)V

    invoke-static {v0, v2, v3, v4}, Lcom/peel/control/aa;->a(Ljava/lang/String;ILjava/lang/String;Lcom/peel/util/t;)V

    .line 1558
    invoke-virtual {p0}, Lcom/peel/i/gm;->v()Landroid/view/View;

    move-result-object v0

    sget v1, Lcom/peel/ui/fp;->missing_other_stb_brand_btn:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/peel/i/hp;

    invoke-direct {v1, p0}, Lcom/peel/i/hp;-><init>(Lcom/peel/i/gm;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1573
    return-void

    .line 1458
    :cond_2
    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v0}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/data/at;->f()I

    move-result v0

    goto/16 :goto_0

    .line 1474
    :cond_3
    iget-object v0, p0, Lcom/peel/i/gm;->au:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v0}, Landroid/widget/AutoCompleteTextView;->getEditableText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->clear()V

    goto/16 :goto_1

    .line 1479
    :cond_4
    iget-object v0, p0, Lcom/peel/i/gm;->au:Landroid/widget/AutoCompleteTextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "       "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget v3, Lcom/peel/ui/ft;->hint_search_box:I

    invoke-virtual {p0, v3}, Lcom/peel/i/gm;->a(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/AutoCompleteTextView;->setHint(Ljava/lang/CharSequence;)V

    goto/16 :goto_2
.end method

.method static synthetic aj(Lcom/peel/i/gm;)Landroid/widget/ListView;
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lcom/peel/i/gm;->as:Landroid/widget/ListView;

    return-object v0
.end method

.method private aj()V
    .locals 9

    .prologue
    const/4 v8, 0x1

    .line 1576
    invoke-static {}, Lcom/peel/util/a/f;->a()Lcom/peel/util/a/f;

    move-result-object v0

    sget-object v1, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v1}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v1

    if-nez v1, :cond_0

    move v1, v8

    :goto_0
    const/16 v2, 0x439

    const/16 v3, 0x7d5

    iget-object v4, p0, Lcom/peel/i/gm;->ap:Lcom/peel/f/a;

    invoke-virtual {v4}, Lcom/peel/f/a;->b()Ljava/lang/String;

    move-result-object v4

    iget v5, p0, Lcom/peel/i/gm;->aX:I

    const/4 v6, 0x0

    iget v7, p0, Lcom/peel/i/gm;->an:I

    invoke-virtual/range {v0 .. v7}, Lcom/peel/util/a/f;->a(IIILjava/lang/String;ILjava/lang/String;I)V

    .line 1578
    const-string/jumbo v0, "https://partners-ir.peel.com"

    iget v1, p0, Lcom/peel/i/gm;->aX:I

    new-instance v2, Lcom/peel/i/hr;

    invoke-direct {v2, p0, v8}, Lcom/peel/i/hr;-><init>(Lcom/peel/i/gm;I)V

    invoke-static {v0, v1, v2}, Lcom/peel/control/aa;->a(Ljava/lang/String;ILcom/peel/util/t;)V

    .line 1622
    return-void

    .line 1576
    :cond_0
    sget-object v1, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v1}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/data/at;->f()I

    move-result v1

    goto :goto_0
.end method

.method private ak()V
    .locals 8

    .prologue
    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 1625
    iget-object v0, p0, Lcom/peel/i/gm;->ap:Lcom/peel/f/a;

    invoke-virtual {v0}, Lcom/peel/f/a;->b()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "Dish"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/peel/i/gm;->ap:Lcom/peel/f/a;

    invoke-virtual {v0}, Lcom/peel/f/a;->b()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "DIRECTV"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1626
    :cond_0
    new-instance v0, Lcom/peel/widget/ag;

    invoke-virtual {p0}, Lcom/peel/i/gm;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/peel/widget/ag;-><init>(Landroid/content/Context;)V

    sget v1, Lcom/peel/ui/ft;->rf_stb_pairing_title:I

    invoke-virtual {p0, v1}, Lcom/peel/i/gm;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/peel/widget/ag;->a(Ljava/lang/CharSequence;)Lcom/peel/widget/ag;

    move-result-object v0

    sget v1, Lcom/peel/ui/ft;->rf_stb_pairing_message:I

    .line 1627
    invoke-virtual {p0, v1}, Lcom/peel/i/gm;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/peel/widget/ag;->b(Ljava/lang/CharSequence;)Lcom/peel/widget/ag;

    move-result-object v0

    sget v1, Lcom/peel/ui/ft;->okay:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/peel/widget/ag;->a(ILandroid/content/DialogInterface$OnClickListener;)Lcom/peel/widget/ag;

    move-result-object v0

    sget v1, Lcom/peel/ui/ft;->more_info:I

    new-instance v2, Lcom/peel/i/hs;

    invoke-direct {v2, p0}, Lcom/peel/i/hs;-><init>(Lcom/peel/i/gm;)V

    .line 1628
    invoke-virtual {v0, v1, v2}, Lcom/peel/widget/ag;->c(ILandroid/content/DialogInterface$OnClickListener;)Lcom/peel/widget/ag;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/i/gm;->aI:Lcom/peel/widget/ag;

    .line 1635
    iget-object v0, p0, Lcom/peel/i/gm;->aI:Lcom/peel/widget/ag;

    invoke-virtual {v0, v7}, Lcom/peel/widget/ag;->setCanceledOnTouchOutside(Z)V

    .line 1636
    iget-object v0, p0, Lcom/peel/i/gm;->aI:Lcom/peel/widget/ag;

    iget-object v1, p0, Lcom/peel/i/gm;->aI:Lcom/peel/widget/ag;

    invoke-virtual {v0, v1}, Lcom/peel/widget/ag;->a(Lcom/peel/widget/ag;)Lcom/peel/widget/ag;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/widget/ag;->show()V

    .line 1638
    :cond_1
    invoke-static {}, Lcom/peel/util/a/f;->a()Lcom/peel/util/a/f;

    move-result-object v1

    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v0}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v0

    if-nez v0, :cond_2

    move v0, v6

    :goto_0
    const/16 v2, 0xbeb

    const/16 v3, 0x7d5

    const-string/jumbo v4, "SCREEN_CHANGE_NEXT_CHANNEL"

    invoke-virtual {v1, v0, v2, v3, v4}, Lcom/peel/util/a/f;->a(IIILjava/lang/String;)V

    .line 1640
    iput v7, p0, Lcom/peel/i/gm;->bs:I

    .line 1643
    sget-object v0, Lcom/peel/i/gm;->e:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/peel/i/gm;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    invoke-static {v0, v1, v6}, Lcom/peel/util/bx;->a(Ljava/lang/String;Landroid/app/Activity;Z)V

    .line 1646
    invoke-virtual {p0}, Lcom/peel/i/gm;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string/jumbo v1, "country_ISO"

    const-string/jumbo v2, "US"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 1647
    const-string/jumbo v0, "https://partners-ir.peel.com"

    const-string/jumbo v1, "Channel_Up"

    const/4 v2, 0x2

    iget-object v3, p0, Lcom/peel/i/gm;->ap:Lcom/peel/f/a;

    invoke-virtual {v3}, Lcom/peel/f/a;->a()I

    move-result v3

    new-instance v5, Lcom/peel/i/ht;

    invoke-direct {v5, p0, v6}, Lcom/peel/i/ht;-><init>(Lcom/peel/i/gm;I)V

    invoke-static/range {v0 .. v5}, Lcom/peel/control/aa;->a(Ljava/lang/String;Ljava/lang/String;IILjava/lang/String;Lcom/peel/util/t;)V

    .line 1749
    invoke-virtual {p0}, Lcom/peel/i/gm;->v()Landroid/view/View;

    move-result-object v0

    sget v1, Lcom/peel/ui/fp;->stb_signal_container:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    .line 1750
    invoke-virtual {p0}, Lcom/peel/i/gm;->v()Landroid/view/View;

    move-result-object v1

    sget v2, Lcom/peel/ui/fp;->coverView:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    new-instance v2, Lcom/peel/i/hv;

    invoke-direct {v2, p0}, Lcom/peel/i/hv;-><init>(Lcom/peel/i/gm;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1756
    sget v1, Lcom/peel/ui/fp;->test_btn_viewpager:I

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/peel/widget/TestBtnViewPager;

    iput-object v1, p0, Lcom/peel/i/gm;->bn:Lcom/peel/widget/TestBtnViewPager;

    .line 1757
    iget-object v1, p0, Lcom/peel/i/gm;->bn:Lcom/peel/widget/TestBtnViewPager;

    invoke-virtual {v1, v6}, Lcom/peel/widget/TestBtnViewPager;->setEnabledSwipe(Z)V

    .line 1758
    iget-object v1, p0, Lcom/peel/i/gm;->bn:Lcom/peel/widget/TestBtnViewPager;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Lcom/peel/widget/TestBtnViewPager;->setVisibility(I)V

    .line 1760
    sget v1, Lcom/peel/ui/fp;->test_pager_left_btn:I

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/peel/i/gm;->bo:Landroid/widget/Button;

    .line 1761
    iget-object v1, p0, Lcom/peel/i/gm;->bo:Landroid/widget/Button;

    invoke-virtual {v1, v7}, Landroid/widget/Button;->setEnabled(Z)V

    .line 1762
    iget-object v1, p0, Lcom/peel/i/gm;->bo:Landroid/widget/Button;

    new-instance v2, Lcom/peel/i/hw;

    invoke-direct {v2, p0}, Lcom/peel/i/hw;-><init>(Lcom/peel/i/gm;)V

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1771
    sget v1, Lcom/peel/ui/fp;->test_pager_right_btn:I

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/peel/i/gm;->bp:Landroid/widget/Button;

    .line 1772
    iget-object v1, p0, Lcom/peel/i/gm;->bp:Landroid/widget/Button;

    invoke-virtual {v1, v7}, Landroid/widget/Button;->setEnabled(Z)V

    .line 1773
    iget-object v1, p0, Lcom/peel/i/gm;->bp:Landroid/widget/Button;

    new-instance v2, Lcom/peel/i/hx;

    invoke-direct {v2, p0}, Lcom/peel/i/hx;-><init>(Lcom/peel/i/gm;)V

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1781
    iget-object v1, p0, Lcom/peel/i/gm;->bo:Landroid/widget/Button;

    invoke-virtual {v1, v7}, Landroid/widget/Button;->setVisibility(I)V

    .line 1782
    iget-object v1, p0, Lcom/peel/i/gm;->bp:Landroid/widget/Button;

    invoke-virtual {v1, v7}, Landroid/widget/Button;->setVisibility(I)V

    .line 1783
    sget v1, Lcom/peel/ui/fp;->test_question_msg:I

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/peel/i/gm;->bg:Landroid/widget/TextView;

    .line 1784
    iget-object v1, p0, Lcom/peel/i/gm;->bg:Landroid/widget/TextView;

    sget v2, Lcom/peel/ui/ft;->device_test_channel_change_question_msg:I

    invoke-virtual {p0, v2}, Lcom/peel/i/gm;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1786
    sget v1, Lcom/peel/ui/fp;->layout_test_btn:I

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    iput-object v1, p0, Lcom/peel/i/gm;->bi:Landroid/widget/RelativeLayout;

    .line 1787
    sget v1, Lcom/peel/ui/fp;->layout_test_msg:I

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    iput-object v1, p0, Lcom/peel/i/gm;->bh:Landroid/widget/RelativeLayout;

    .line 1789
    iget-object v1, p0, Lcom/peel/i/gm;->bi:Landroid/widget/RelativeLayout;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1790
    iget-object v1, p0, Lcom/peel/i/gm;->bh:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v7}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1792
    sget v1, Lcom/peel/ui/fp;->turn_on_msg:I

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/peel/i/gm;->be:Landroid/widget/TextView;

    .line 1793
    iget-object v1, p0, Lcom/peel/i/gm;->be:Landroid/widget/TextView;

    sget v2, Lcom/peel/ui/ft;->testing_key_stb:I

    invoke-virtual {p0, v2}, Lcom/peel/i/gm;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1795
    sget v1, Lcom/peel/ui/fp;->test_status_msg:I

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/peel/i/gm;->bf:Landroid/widget/TextView;

    .line 1796
    iget-object v1, p0, Lcom/peel/i/gm;->bf:Landroid/widget/TextView;

    new-instance v2, Lcom/peel/i/hy;

    invoke-direct {v2, p0}, Lcom/peel/i/hy;-><init>(Lcom/peel/i/gm;)V

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 1806
    sget v1, Lcom/peel/ui/fp;->yes_btn:I

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/peel/i/gm;->bb:Landroid/widget/Button;

    .line 1807
    sget v1, Lcom/peel/ui/fp;->no_btn:I

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/peel/i/gm;->bc:Landroid/widget/Button;

    .line 1809
    sget v1, Lcom/peel/ui/fp;->yes_btn:I

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/peel/i/gm;->bb:Landroid/widget/Button;

    .line 1810
    sget v1, Lcom/peel/ui/fp;->no_btn:I

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/peel/i/gm;->bc:Landroid/widget/Button;

    .line 1813
    iget-object v0, p0, Lcom/peel/i/gm;->bb:Landroid/widget/Button;

    new-instance v1, Lcom/peel/i/hz;

    invoke-direct {v1, p0}, Lcom/peel/i/hz;-><init>(Lcom/peel/i/gm;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1828
    iget-object v0, p0, Lcom/peel/i/gm;->bc:Landroid/widget/Button;

    new-instance v1, Lcom/peel/i/ia;

    invoke-direct {v1, p0}, Lcom/peel/i/ia;-><init>(Lcom/peel/i/gm;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1854
    return-void

    .line 1638
    :cond_2
    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v0}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/data/at;->f()I

    move-result v0

    goto/16 :goto_0
.end method

.method static synthetic ak(Lcom/peel/i/gm;)Z
    .locals 1

    .prologue
    .line 95
    iget-boolean v0, p0, Lcom/peel/i/gm;->bt:Z

    return v0
.end method

.method private al()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 1857
    iget-object v0, p0, Lcom/peel/i/gm;->aL:Lcom/peel/control/RoomControl;

    invoke-virtual {v0}, Lcom/peel/control/RoomControl;->h()V

    .line 1859
    invoke-virtual {p0}, Lcom/peel/i/gm;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    invoke-static {v0, v2}, Lcom/peel/util/dq;->a(Landroid/content/Context;Z)V

    .line 1860
    invoke-virtual {p0}, Lcom/peel/i/gm;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    const-string/jumbo v1, "is_device_setup_complete"

    invoke-static {v0, v1, v2}, Lcom/peel/util/dq;->a(Landroid/content/Context;Ljava/lang/String;Z)V

    .line 1862
    invoke-virtual {p0}, Lcom/peel/i/gm;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    invoke-static {v0, v2}, Lcom/peel/util/bx;->a(Landroid/content/Context;Z)V

    .line 1863
    invoke-virtual {p0}, Lcom/peel/i/gm;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/i/gm;->aL:Lcom/peel/control/RoomControl;

    invoke-static {v0, v1}, Lcom/peel/util/bx;->a(Landroid/content/Context;Lcom/peel/control/RoomControl;)V

    .line 1864
    iget-object v0, p0, Lcom/peel/i/gm;->aF:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 1865
    iget-object v0, p0, Lcom/peel/i/gm;->aP:Landroid/os/Bundle;

    if-nez v0, :cond_0

    .line 1866
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, Lcom/peel/i/gm;->aP:Landroid/os/Bundle;

    .line 1867
    :cond_0
    iget-object v0, p0, Lcom/peel/i/gm;->aP:Landroid/os/Bundle;

    const-string/jumbo v1, "goback_clazz"

    iget-object v2, p0, Lcom/peel/i/gm;->aF:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1868
    invoke-virtual {p0}, Lcom/peel/i/gm;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    const-class v1, Lcom/peel/ui/gt;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/peel/i/gm;->aP:Landroid/os/Bundle;

    invoke-static {v0, v1, v2}, Lcom/peel/d/e;->c(Landroid/support/v4/app/ae;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 1877
    :goto_0
    return-void

    .line 1869
    :cond_1
    iget-object v0, p0, Lcom/peel/i/gm;->aP:Landroid/os/Bundle;

    if-eqz v0, :cond_2

    .line 1870
    invoke-virtual {p0}, Lcom/peel/i/gm;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    const-class v1, Lcom/peel/ui/gt;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/peel/i/gm;->aP:Landroid/os/Bundle;

    invoke-static {v0, v1, v2}, Lcom/peel/d/e;->a(Landroid/support/v4/app/ae;Ljava/lang/String;Landroid/os/Bundle;)V

    goto :goto_0

    .line 1874
    :cond_2
    iget-object v0, p0, Lcom/peel/i/gm;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "popstack"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1875
    sget-object v0, Lcom/peel/i/gm;->e:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/peel/i/gm;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/peel/d/e;->a(Ljava/lang/String;Landroid/support/v4/app/ae;)V

    goto :goto_0
.end method

.method static synthetic al(Lcom/peel/i/gm;)Z
    .locals 1

    .prologue
    .line 95
    iget-boolean v0, p0, Lcom/peel/i/gm;->bu:Z

    return v0
.end method

.method private am()Ljava/lang/String;
    .locals 4

    .prologue
    const/4 v3, 0x4

    const/4 v1, 0x1

    .line 1882
    iget v0, p0, Lcom/peel/i/gm;->aC:I

    if-ne v0, v1, :cond_2

    .line 1883
    iget v0, p0, Lcom/peel/i/gm;->am:I

    if-ne v0, v1, :cond_1

    .line 1884
    sget v0, Lcom/peel/ui/ft;->DeviceType1:I

    invoke-virtual {p0, v0}, Lcom/peel/i/gm;->a(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/i/gm;->aD:Ljava/lang/String;

    .line 1907
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/peel/i/gm;->aD:Ljava/lang/String;

    return-object v0

    .line 1885
    :cond_1
    iget v0, p0, Lcom/peel/i/gm;->am:I

    const/16 v1, 0xa

    if-ne v0, v1, :cond_0

    .line 1886
    sget v0, Lcom/peel/ui/ft;->DeviceType10:I

    invoke-virtual {p0, v0}, Lcom/peel/i/gm;->a(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/i/gm;->aD:Ljava/lang/String;

    goto :goto_0

    .line 1888
    :cond_2
    sget v0, Lcom/peel/ui/ft;->DeviceType2:I

    invoke-virtual {p0, v0}, Lcom/peel/i/gm;->a(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/i/gm;->aD:Ljava/lang/String;

    .line 1889
    iget-object v0, p0, Lcom/peel/i/gm;->aO:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/i/im;

    .line 1890
    if-eqz v0, :cond_3

    iget v1, v0, Lcom/peel/i/im;->a:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_3

    iget v1, v0, Lcom/peel/i/im;->b:I

    if-ne v1, v3, :cond_3

    .line 1891
    sget v0, Lcom/peel/ui/ft;->DeviceType1:I

    invoke-virtual {p0, v0}, Lcom/peel/i/gm;->a(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/i/gm;->aD:Ljava/lang/String;

    goto :goto_0

    .line 1892
    :cond_3
    if-eqz v0, :cond_0

    iget v1, v0, Lcom/peel/i/im;->a:I

    const/4 v2, 0x3

    if-ne v1, v2, :cond_0

    iget v0, v0, Lcom/peel/i/im;->b:I

    if-ne v0, v3, :cond_0

    .line 1893
    iget-boolean v0, p0, Lcom/peel/i/gm;->aR:Z

    if-nez v0, :cond_4

    .line 1894
    sget v0, Lcom/peel/ui/ft;->DeviceType1:I

    invoke-virtual {p0, v0}, Lcom/peel/i/gm;->a(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/i/gm;->aD:Ljava/lang/String;

    goto :goto_0

    .line 1896
    :cond_4
    sget v0, Lcom/peel/ui/ft;->DeviceType10:I

    invoke-virtual {p0, v0}, Lcom/peel/i/gm;->a(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/i/gm;->aD:Ljava/lang/String;

    goto :goto_0
.end method

.method static synthetic am(Lcom/peel/i/gm;)Z
    .locals 1

    .prologue
    .line 95
    iget-boolean v0, p0, Lcom/peel/i/gm;->bx:Z

    return v0
.end method

.method static synthetic an(Lcom/peel/i/gm;)Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lcom/peel/i/gm;->aT:Landroid/widget/ImageView;

    return-object v0
.end method

.method private an()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 2198
    iget-object v0, p0, Lcom/peel/i/gm;->aK:Lcom/peel/control/h;

    if-eqz v0, :cond_0

    .line 2199
    iget-object v0, p0, Lcom/peel/i/gm;->aK:Lcom/peel/control/h;

    const-string/jumbo v1, "Channel_Up"

    invoke-virtual {v0, v1}, Lcom/peel/control/h;->c(Ljava/lang/String;)Z

    .line 2200
    iget-object v0, p0, Lcom/peel/i/gm;->bf:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2201
    iget-object v0, p0, Lcom/peel/i/gm;->bf:Landroid/widget/TextView;

    sget v1, Lcom/peel/ui/ft;->ir_send_code:I

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    iget v3, p0, Lcom/peel/i/gm;->aY:I

    add-int/lit8 v3, v3, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {p0, v1, v2}, Lcom/peel/i/gm;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2202
    iget-object v0, p0, Lcom/peel/i/gm;->bi:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v4}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 2203
    iget-object v0, p0, Lcom/peel/i/gm;->bh:Landroid/widget/RelativeLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 2206
    sget-object v0, Lcom/peel/i/gm;->e:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "STB cmd pressed codeIdx:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/peel/i/gm;->aY:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "/codesetId:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/peel/i/gm;->aX:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2208
    :cond_0
    return-void
.end method

.method static synthetic ao(Lcom/peel/i/gm;)Landroid/widget/ListView;
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lcom/peel/i/gm;->at:Landroid/widget/ListView;

    return-object v0
.end method

.method private ao()V
    .locals 10

    .prologue
    const/4 v8, 0x1

    const/4 v9, 0x0

    .line 2211
    iget-object v0, p0, Lcom/peel/i/gm;->aJ:Lcom/peel/control/h;

    if-eqz v0, :cond_0

    .line 2212
    iget-object v0, p0, Lcom/peel/i/gm;->aJ:Lcom/peel/control/h;

    iget-object v1, p0, Lcom/peel/i/gm;->aZ:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/peel/control/h;->c(Ljava/lang/String;)Z

    .line 2213
    invoke-static {}, Lcom/peel/util/a/f;->a()Lcom/peel/util/a/f;

    move-result-object v0

    sget-object v1, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v1}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v1

    if-nez v1, :cond_1

    move v1, v8

    :goto_0
    const/16 v2, 0x430

    const/16 v3, 0x7d5

    iget-object v4, p0, Lcom/peel/i/gm;->ao:Lcom/peel/f/a;

    invoke-virtual {v4}, Lcom/peel/f/a;->b()Ljava/lang/String;

    move-result-object v4

    iget v5, p0, Lcom/peel/i/gm;->aX:I

    const/4 v6, 0x0

    iget v7, p0, Lcom/peel/i/gm;->an:I

    invoke-virtual/range {v0 .. v7}, Lcom/peel/util/a/f;->a(IIILjava/lang/String;ILjava/lang/String;I)V

    .line 2214
    iget-object v0, p0, Lcom/peel/i/gm;->bf:Landroid/widget/TextView;

    invoke-virtual {v0, v9}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2217
    sget-object v0, Lcom/peel/i/gm;->e:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "TV cmd pressed codeIdx:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/peel/i/gm;->aY:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "/codesetId:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/peel/i/gm;->aX:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2218
    iget-object v0, p0, Lcom/peel/i/gm;->bf:Landroid/widget/TextView;

    sget v1, Lcom/peel/ui/ft;->ir_send_code:I

    new-array v2, v8, [Ljava/lang/Object;

    iget v3, p0, Lcom/peel/i/gm;->aY:I

    add-int/lit8 v3, v3, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v9

    invoke-virtual {p0, v1, v2}, Lcom/peel/i/gm;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2219
    iget-object v0, p0, Lcom/peel/i/gm;->bi:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v9}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 2220
    iget-object v0, p0, Lcom/peel/i/gm;->bh:Landroid/widget/RelativeLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 2222
    :cond_0
    return-void

    .line 2213
    :cond_1
    sget-object v1, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v1}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/data/at;->f()I

    move-result v1

    goto :goto_0
.end method

.method static synthetic ap(Lcom/peel/i/gm;)Landroid/widget/AutoCompleteTextView;
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lcom/peel/i/gm;->au:Landroid/widget/AutoCompleteTextView;

    return-object v0
.end method

.method static synthetic aq(Lcom/peel/i/gm;)Lcom/peel/i/a/a;
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lcom/peel/i/gm;->aH:Lcom/peel/i/a/a;

    return-object v0
.end method

.method static synthetic ar(Lcom/peel/i/gm;)Z
    .locals 1

    .prologue
    .line 95
    iget-boolean v0, p0, Lcom/peel/i/gm;->by:Z

    return v0
.end method

.method static synthetic as(Lcom/peel/i/gm;)Ljava/util/List;
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lcom/peel/i/gm;->aA:Ljava/util/List;

    return-object v0
.end method

.method static synthetic at(Lcom/peel/i/gm;)Landroid/widget/RelativeLayout;
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lcom/peel/i/gm;->aU:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method static synthetic au(Lcom/peel/i/gm;)V
    .locals 0

    .prologue
    .line 95
    invoke-direct {p0}, Lcom/peel/i/gm;->aj()V

    return-void
.end method

.method static synthetic av(Lcom/peel/i/gm;)Landroid/widget/ViewFlipper;
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lcom/peel/i/gm;->i:Landroid/widget/ViewFlipper;

    return-object v0
.end method

.method static synthetic aw(Lcom/peel/i/gm;)I
    .locals 1

    .prologue
    .line 95
    iget v0, p0, Lcom/peel/i/gm;->bs:I

    return v0
.end method

.method static synthetic ax(Lcom/peel/i/gm;)V
    .locals 0

    .prologue
    .line 95
    invoke-direct {p0}, Lcom/peel/i/gm;->an()V

    return-void
.end method

.method static synthetic ay(Lcom/peel/i/gm;)V
    .locals 0

    .prologue
    .line 95
    invoke-direct {p0}, Lcom/peel/i/gm;->ao()V

    return-void
.end method

.method static synthetic b(I)I
    .locals 0

    .prologue
    .line 95
    sput p0, Lcom/peel/i/gm;->f:I

    return p0
.end method

.method static synthetic b(Lcom/peel/i/gm;I)I
    .locals 0

    .prologue
    .line 95
    iput p1, p0, Lcom/peel/i/gm;->aC:I

    return p1
.end method

.method static synthetic b(Lcom/peel/i/gm;)Lcom/peel/control/RoomControl;
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lcom/peel/i/gm;->aL:Lcom/peel/control/RoomControl;

    return-object v0
.end method

.method static synthetic b(Lcom/peel/i/gm;Lcom/peel/control/h;)Lcom/peel/control/h;
    .locals 0

    .prologue
    .line 95
    iput-object p1, p0, Lcom/peel/i/gm;->aK:Lcom/peel/control/h;

    return-object p1
.end method

.method static synthetic b(Lcom/peel/i/gm;Lcom/peel/f/a;)Lcom/peel/f/a;
    .locals 0

    .prologue
    .line 95
    iput-object p1, p0, Lcom/peel/i/gm;->ap:Lcom/peel/f/a;

    return-object p1
.end method

.method static synthetic b(Lcom/peel/i/gm;Lcom/peel/i/a/a;)Lcom/peel/i/a/a;
    .locals 0

    .prologue
    .line 95
    iput-object p1, p0, Lcom/peel/i/gm;->al:Lcom/peel/i/a/a;

    return-object p1
.end method

.method static synthetic b(Lcom/peel/i/gm;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 95
    iput-object p1, p0, Lcom/peel/i/gm;->aZ:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic b(Lcom/peel/i/gm;Ljava/util/List;)Ljava/util/List;
    .locals 0

    .prologue
    .line 95
    iput-object p1, p0, Lcom/peel/i/gm;->ax:Ljava/util/List;

    return-object p1
.end method

.method static synthetic b(Lcom/peel/i/gm;Z)Z
    .locals 0

    .prologue
    .line 95
    iput-boolean p1, p0, Lcom/peel/i/gm;->bv:Z

    return p1
.end method

.method static synthetic c(Lcom/peel/i/gm;I)I
    .locals 0

    .prologue
    .line 95
    iput p1, p0, Lcom/peel/i/gm;->aY:I

    return p1
.end method

.method static synthetic c(Lcom/peel/i/gm;)Lcom/peel/control/h;
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lcom/peel/i/gm;->aJ:Lcom/peel/control/h;

    return-object v0
.end method

.method static synthetic c(Lcom/peel/i/gm;Lcom/peel/i/a/a;)Lcom/peel/i/a/a;
    .locals 0

    .prologue
    .line 95
    iput-object p1, p0, Lcom/peel/i/gm;->aG:Lcom/peel/i/a/a;

    return-object p1
.end method

.method static synthetic c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 95
    sget-object v0, Lcom/peel/i/gm;->e:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic c(Lcom/peel/i/gm;Ljava/util/List;)Ljava/util/List;
    .locals 0

    .prologue
    .line 95
    iput-object p1, p0, Lcom/peel/i/gm;->ay:Ljava/util/List;

    return-object p1
.end method

.method static synthetic c(Lcom/peel/i/gm;Z)Z
    .locals 0

    .prologue
    .line 95
    iput-boolean p1, p0, Lcom/peel/i/gm;->bw:Z

    return p1
.end method

.method static synthetic d(Lcom/peel/i/gm;I)I
    .locals 0

    .prologue
    .line 95
    iput p1, p0, Lcom/peel/i/gm;->aX:I

    return p1
.end method

.method static synthetic d(Lcom/peel/i/gm;)Lcom/peel/content/library/LiveLibrary;
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lcom/peel/i/gm;->aN:Lcom/peel/content/library/LiveLibrary;

    return-object v0
.end method

.method static synthetic d(Lcom/peel/i/gm;Lcom/peel/i/a/a;)Lcom/peel/i/a/a;
    .locals 0

    .prologue
    .line 95
    iput-object p1, p0, Lcom/peel/i/gm;->aH:Lcom/peel/i/a/a;

    return-object p1
.end method

.method static synthetic d(Lcom/peel/i/gm;Ljava/util/List;)Ljava/util/List;
    .locals 0

    .prologue
    .line 95
    iput-object p1, p0, Lcom/peel/i/gm;->az:Ljava/util/List;

    return-object p1
.end method

.method static synthetic d(Lcom/peel/i/gm;Z)Z
    .locals 0

    .prologue
    .line 95
    iput-boolean p1, p0, Lcom/peel/i/gm;->bx:Z

    return p1
.end method

.method static synthetic e(Lcom/peel/i/gm;I)I
    .locals 0

    .prologue
    .line 95
    iput p1, p0, Lcom/peel/i/gm;->br:I

    return p1
.end method

.method static synthetic e(Lcom/peel/i/gm;)Lcom/peel/control/a;
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lcom/peel/i/gm;->aM:Lcom/peel/control/a;

    return-object v0
.end method

.method static synthetic e(Lcom/peel/i/gm;Ljava/util/List;)Ljava/util/List;
    .locals 0

    .prologue
    .line 95
    iput-object p1, p0, Lcom/peel/i/gm;->aA:Ljava/util/List;

    return-object p1
.end method

.method static synthetic e(Lcom/peel/i/gm;Z)Z
    .locals 0

    .prologue
    .line 95
    iput-boolean p1, p0, Lcom/peel/i/gm;->bt:Z

    return p1
.end method

.method static synthetic f(Lcom/peel/i/gm;I)I
    .locals 0

    .prologue
    .line 95
    iput p1, p0, Lcom/peel/i/gm;->bs:I

    return p1
.end method

.method static synthetic f(Lcom/peel/i/gm;)V
    .locals 0

    .prologue
    .line 95
    invoke-direct {p0}, Lcom/peel/i/gm;->ag()V

    return-void
.end method

.method static synthetic f(Lcom/peel/i/gm;Z)Z
    .locals 0

    .prologue
    .line 95
    iput-boolean p1, p0, Lcom/peel/i/gm;->bu:Z

    return p1
.end method

.method static synthetic g(Lcom/peel/i/gm;)V
    .locals 0

    .prologue
    .line 95
    invoke-direct {p0}, Lcom/peel/i/gm;->U()V

    return-void
.end method

.method static synthetic g(Lcom/peel/i/gm;Z)Z
    .locals 0

    .prologue
    .line 95
    iput-boolean p1, p0, Lcom/peel/i/gm;->by:Z

    return p1
.end method

.method static synthetic h(Lcom/peel/i/gm;)V
    .locals 0

    .prologue
    .line 95
    invoke-direct {p0}, Lcom/peel/i/gm;->T()V

    return-void
.end method

.method static synthetic i(Lcom/peel/i/gm;)V
    .locals 0

    .prologue
    .line 95
    invoke-direct {p0}, Lcom/peel/i/gm;->af()V

    return-void
.end method

.method static synthetic j(Lcom/peel/i/gm;)V
    .locals 0

    .prologue
    .line 95
    invoke-direct {p0}, Lcom/peel/i/gm;->ah()V

    return-void
.end method

.method static synthetic k(Lcom/peel/i/gm;)V
    .locals 0

    .prologue
    .line 95
    invoke-direct {p0}, Lcom/peel/i/gm;->V()V

    return-void
.end method

.method static synthetic l(Lcom/peel/i/gm;)V
    .locals 0

    .prologue
    .line 95
    invoke-direct {p0}, Lcom/peel/i/gm;->ac()V

    return-void
.end method

.method static synthetic m(Lcom/peel/i/gm;)Lcom/peel/f/a;
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lcom/peel/i/gm;->ao:Lcom/peel/f/a;

    return-object v0
.end method

.method static synthetic n(Lcom/peel/i/gm;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 95
    invoke-direct {p0}, Lcom/peel/i/gm;->ad()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic o(Lcom/peel/i/gm;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lcom/peel/i/gm;->aZ:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic p(Lcom/peel/i/gm;)V
    .locals 0

    .prologue
    .line 95
    invoke-direct {p0}, Lcom/peel/i/gm;->al()V

    return-void
.end method

.method static synthetic q(Lcom/peel/i/gm;)V
    .locals 0

    .prologue
    .line 95
    invoke-direct {p0}, Lcom/peel/i/gm;->ak()V

    return-void
.end method

.method static synthetic r(Lcom/peel/i/gm;)V
    .locals 0

    .prologue
    .line 95
    invoke-direct {p0}, Lcom/peel/i/gm;->ai()V

    return-void
.end method

.method static synthetic s(Lcom/peel/i/gm;)Lcom/peel/f/a;
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lcom/peel/i/gm;->ap:Lcom/peel/f/a;

    return-object v0
.end method

.method static synthetic t(Lcom/peel/i/gm;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 95
    invoke-direct {p0}, Lcom/peel/i/gm;->am()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic u(Lcom/peel/i/gm;)V
    .locals 0

    .prologue
    .line 95
    invoke-direct {p0}, Lcom/peel/i/gm;->S()V

    return-void
.end method

.method static synthetic v(Lcom/peel/i/gm;)Z
    .locals 1

    .prologue
    .line 95
    iget-boolean v0, p0, Lcom/peel/i/gm;->aB:Z

    return v0
.end method

.method static synthetic w(Lcom/peel/i/gm;)Lcom/peel/control/h;
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lcom/peel/i/gm;->aK:Lcom/peel/control/h;

    return-object v0
.end method

.method static synthetic x(Lcom/peel/i/gm;)Landroid/view/LayoutInflater;
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lcom/peel/i/gm;->g:Landroid/view/LayoutInflater;

    return-object v0
.end method

.method static synthetic y(Lcom/peel/i/gm;)Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lcom/peel/i/gm;->bk:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic z(Lcom/peel/i/gm;)Landroid/graphics/drawable/AnimationDrawable;
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lcom/peel/i/gm;->bm:Landroid/graphics/drawable/AnimationDrawable;

    return-object v0
.end method


# virtual methods
.method public Z()V
    .locals 2

    .prologue
    .line 2184
    iget-object v0, p0, Lcom/peel/i/gm;->d:Lcom/peel/d/a;

    if-nez v0, :cond_1

    .line 2195
    :cond_0
    :goto_0
    return-void

    .line 2187
    :cond_1
    iget-object v0, p0, Lcom/peel/i/gm;->b:Lcom/peel/d/i;

    iget-object v1, p0, Lcom/peel/i/gm;->d:Lcom/peel/d/a;

    invoke-virtual {v0, v1}, Lcom/peel/d/i;->a(Lcom/peel/d/a;)V

    .line 2188
    iget-object v0, p0, Lcom/peel/i/gm;->b:Lcom/peel/d/i;

    invoke-virtual {v0}, Lcom/peel/d/i;->h()Landroid/support/v4/app/a;

    move-result-object v0

    .line 2189
    if-eqz v0, :cond_0

    .line 2190
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v4/app/a;->a(Z)V

    .line 2191
    invoke-virtual {p0}, Lcom/peel/i/gm;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    check-cast v0, Landroid/support/v7/app/ActionBarActivity;

    .line 2192
    invoke-virtual {v0}, Landroid/support/v7/app/ActionBarActivity;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/support/v7/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    goto :goto_0
.end method

.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    .line 447
    iput-object p1, p0, Lcom/peel/i/gm;->g:Landroid/view/LayoutInflater;

    .line 448
    sget v0, Lcom/peel/ui/fq;->simplified_device_setup:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ViewFlipper;

    iput-object v0, p0, Lcom/peel/i/gm;->i:Landroid/widget/ViewFlipper;

    .line 449
    iget-object v0, p0, Lcom/peel/i/gm;->i:Landroid/widget/ViewFlipper;

    new-instance v1, Lcom/peel/i/ha;

    invoke-direct {v1, p0}, Lcom/peel/i/ha;-><init>(Lcom/peel/i/gm;)V

    invoke-virtual {v0, v1}, Landroid/widget/ViewFlipper;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 455
    invoke-virtual {p0}, Lcom/peel/i/gm;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    invoke-static {v0}, Lcom/peel/util/g;->a(Landroid/app/Activity;)V

    .line 456
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/peel/i/gm;->d(Z)V

    .line 457
    iget-object v0, p0, Lcom/peel/i/gm;->i:Landroid/widget/ViewFlipper;

    return-object v0
.end method

.method public a(Landroid/view/MenuItem;)Z
    .locals 10

    .prologue
    const/4 v7, -0x1

    const/16 v3, 0x7d5

    const/4 v9, 0x3

    const/4 v5, 0x1

    const/4 v8, 0x0

    .line 1925
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    .line 1926
    const v1, 0x102002c

    if-ne v0, v1, :cond_1

    .line 1927
    sget-object v0, Lcom/peel/i/gm;->e:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/peel/i/gm;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/peel/d/e;->a(Ljava/lang/String;Landroid/support/v4/app/ae;)V

    .line 2026
    :cond_0
    :goto_0
    invoke-super {p0, p1}, Lcom/peel/d/u;->a(Landroid/view/MenuItem;)Z

    move-result v0

    return v0

    .line 1928
    :cond_1
    sget v1, Lcom/peel/ui/fp;->menu_next:I

    if-ne v0, v1, :cond_0

    .line 1929
    sget v0, Lcom/peel/i/gm;->f:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_2

    .line 1930
    iget-object v0, p0, Lcom/peel/i/gm;->ao:Lcom/peel/f/a;

    if-eqz v0, :cond_0

    .line 1931
    invoke-static {}, Lcom/peel/util/a/f;->a()Lcom/peel/util/a/f;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/i/gm;->aL:Lcom/peel/control/RoomControl;

    invoke-virtual {v1}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/data/at;->f()I

    move-result v1

    const/16 v2, 0xc29

    iget-object v4, p0, Lcom/peel/i/gm;->ao:Lcom/peel/f/a;

    invoke-virtual {v4}, Lcom/peel/f/a;->b()Ljava/lang/String;

    move-result-object v4

    const-string/jumbo v6, "TV"

    invoke-virtual/range {v0 .. v7}, Lcom/peel/util/a/f;->a(IIILjava/lang/String;ILjava/lang/String;I)V

    .line 1932
    iget-object v0, p0, Lcom/peel/i/gm;->bz:Landroid/os/Handler;

    iget-object v1, p0, Lcom/peel/i/gm;->bz:Landroid/os/Handler;

    const/4 v2, 0x2

    const/4 v3, 0x4

    invoke-static {v1, v8, v2, v3}, Landroid/os/Message;->obtain(Landroid/os/Handler;III)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0

    .line 1934
    :cond_2
    sget v0, Lcom/peel/i/gm;->f:I

    if-ne v0, v9, :cond_3

    iget-boolean v0, p0, Lcom/peel/i/gm;->aB:Z

    if-nez v0, :cond_3

    .line 1935
    iget-object v0, p0, Lcom/peel/i/gm;->ao:Lcom/peel/f/a;

    if-eqz v0, :cond_0

    .line 1936
    invoke-static {}, Lcom/peel/util/a/f;->a()Lcom/peel/util/a/f;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/i/gm;->aL:Lcom/peel/control/RoomControl;

    invoke-virtual {v1}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/data/at;->f()I

    move-result v1

    const/16 v2, 0xc29

    iget-object v4, p0, Lcom/peel/i/gm;->ao:Lcom/peel/f/a;

    invoke-virtual {v4}, Lcom/peel/f/a;->b()Ljava/lang/String;

    move-result-object v4

    const-string/jumbo v6, "TV"

    invoke-virtual/range {v0 .. v7}, Lcom/peel/util/a/f;->a(IIILjava/lang/String;ILjava/lang/String;I)V

    .line 1937
    iget-object v0, p0, Lcom/peel/i/gm;->bz:Landroid/os/Handler;

    iget-object v1, p0, Lcom/peel/i/gm;->bz:Landroid/os/Handler;

    const/4 v2, 0x4

    invoke-static {v1, v8, v9, v2}, Landroid/os/Message;->obtain(Landroid/os/Handler;III)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0

    .line 1939
    :cond_3
    sget v0, Lcom/peel/i/gm;->f:I

    if-ne v0, v9, :cond_4

    iget-boolean v0, p0, Lcom/peel/i/gm;->aB:Z

    if-eqz v0, :cond_4

    .line 1940
    iget-object v0, p0, Lcom/peel/i/gm;->ao:Lcom/peel/f/a;

    if-eqz v0, :cond_0

    .line 1941
    invoke-static {}, Lcom/peel/util/a/f;->a()Lcom/peel/util/a/f;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/i/gm;->aL:Lcom/peel/control/RoomControl;

    invoke-virtual {v1}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/data/at;->f()I

    move-result v1

    const/16 v2, 0xc29

    iget-object v4, p0, Lcom/peel/i/gm;->ao:Lcom/peel/f/a;

    invoke-virtual {v4}, Lcom/peel/f/a;->b()Ljava/lang/String;

    move-result-object v4

    const/16 v5, 0xa

    const-string/jumbo v6, "Projector"

    invoke-virtual/range {v0 .. v7}, Lcom/peel/util/a/f;->a(IIILjava/lang/String;ILjava/lang/String;I)V

    .line 1942
    iget-object v0, p0, Lcom/peel/i/gm;->bz:Landroid/os/Handler;

    iget-object v1, p0, Lcom/peel/i/gm;->bz:Landroid/os/Handler;

    const/4 v2, 0x4

    invoke-static {v1, v8, v9, v2}, Landroid/os/Message;->obtain(Landroid/os/Handler;III)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto/16 :goto_0

    .line 1944
    :cond_4
    sget v0, Lcom/peel/i/gm;->f:I

    const/4 v1, 0x7

    if-ne v0, v1, :cond_f

    .line 1945
    iget-object v0, p0, Lcom/peel/i/gm;->ap:Lcom/peel/f/a;

    if-eqz v0, :cond_5

    .line 1946
    new-instance v0, Lcom/peel/widget/ag;

    invoke-virtual {p0}, Lcom/peel/i/gm;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/peel/widget/ag;-><init>(Landroid/content/Context;)V

    sget v1, Lcom/peel/ui/ft;->setup_stb_dialog_title:I

    invoke-virtual {p0, v1}, Lcom/peel/i/gm;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/peel/widget/ag;->a(Ljava/lang/CharSequence;)Lcom/peel/widget/ag;

    move-result-object v0

    sget v1, Lcom/peel/ui/ft;->setup_stb_dialog_msg:I

    .line 1947
    invoke-virtual {p0, v1}, Lcom/peel/i/gm;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/peel/widget/ag;->b(Ljava/lang/CharSequence;)Lcom/peel/widget/ag;

    move-result-object v0

    sget v1, Lcom/peel/ui/ft;->done:I

    new-instance v2, Lcom/peel/i/id;

    invoke-direct {v2, p0}, Lcom/peel/i/id;-><init>(Lcom/peel/i/gm;)V

    .line 1948
    invoke-virtual {v0, v1, v2}, Lcom/peel/widget/ag;->a(ILandroid/content/DialogInterface$OnClickListener;)Lcom/peel/widget/ag;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/i/gm;->aI:Lcom/peel/widget/ag;

    .line 1965
    iget-object v0, p0, Lcom/peel/i/gm;->aI:Lcom/peel/widget/ag;

    invoke-virtual {v0, v8}, Lcom/peel/widget/ag;->setCanceledOnTouchOutside(Z)V

    .line 1966
    iget-object v0, p0, Lcom/peel/i/gm;->aI:Lcom/peel/widget/ag;

    iget-object v1, p0, Lcom/peel/i/gm;->aI:Lcom/peel/widget/ag;

    invoke-virtual {v0, v1}, Lcom/peel/widget/ag;->a(Lcom/peel/widget/ag;)Lcom/peel/widget/ag;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/widget/ag;->show()V

    goto/16 :goto_0

    .line 1967
    :cond_5
    iget-boolean v0, p0, Lcom/peel/i/gm;->bu:Z

    if-eqz v0, :cond_6

    .line 1968
    iget-object v0, p0, Lcom/peel/i/gm;->bz:Landroid/os/Handler;

    iget-object v1, p0, Lcom/peel/i/gm;->bz:Landroid/os/Handler;

    const/4 v2, 0x7

    const/16 v3, 0x8

    invoke-static {v1, v8, v2, v3}, Landroid/os/Message;->obtain(Landroid/os/Handler;III)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto/16 :goto_0

    .line 1969
    :cond_6
    iget-boolean v0, p0, Lcom/peel/i/gm;->bt:Z

    if-eqz v0, :cond_0

    .line 1970
    iget-object v0, p0, Lcom/peel/i/gm;->aL:Lcom/peel/control/RoomControl;

    invoke-virtual {v0}, Lcom/peel/control/RoomControl;->c()[Lcom/peel/control/a;

    move-result-object v2

    array-length v4, v2

    move v1, v8

    :goto_1
    if-ge v1, v4, :cond_8

    aget-object v0, v2, v1

    .line 1971
    if-nez v0, :cond_7

    const-string/jumbo v0, "NULL Activity"

    .line 1972
    :goto_2
    sget-object v6, Lcom/peel/i/gm;->e:Ljava/lang/String;

    invoke-static {v6, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1970
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1971
    :cond_7
    invoke-virtual {v0}, Lcom/peel/control/a;->a()Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    .line 1975
    :cond_8
    invoke-static {}, Lcom/peel/util/a/f;->a()Lcom/peel/util/a/f;

    move-result-object v1

    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v0}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v0

    if-nez v0, :cond_b

    move v0, v5

    :goto_3
    const/16 v2, 0xbeb

    const-string/jumbo v4, "SCREEN_CHANGE_CHANNEL"

    invoke-virtual {v1, v0, v2, v3, v4}, Lcom/peel/util/a/f;->a(IIILjava/lang/String;)V

    .line 1977
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "renderChannelChangerScreen: watch tv activity: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v0, p0, Lcom/peel/i/gm;->aM:Lcom/peel/control/a;

    if-eqz v0, :cond_c

    const-string/jumbo v0, "NOT NULL"

    :goto_4
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1978
    sget-object v1, Lcom/peel/i/gm;->e:Ljava/lang/String;

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1979
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "renderChannelChangerScreen tv device: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v0, p0, Lcom/peel/i/gm;->aJ:Lcom/peel/control/h;

    if-eqz v0, :cond_d

    const-string/jumbo v0, "NOT NULL"

    :goto_5
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1980
    sget-object v1, Lcom/peel/i/gm;->e:Ljava/lang/String;

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1981
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "renderChannelChangerScreen room.getActivities(): "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v0, p0, Lcom/peel/i/gm;->aL:Lcom/peel/control/RoomControl;

    invoke-virtual {v0}, Lcom/peel/control/RoomControl;->c()[Lcom/peel/control/a;

    move-result-object v0

    if-nez v0, :cond_e

    const-string/jumbo v0, "NO ACTIVITIES"

    :goto_6
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1982
    sget-object v1, Lcom/peel/i/gm;->e:Ljava/lang/String;

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1984
    iget-object v0, p0, Lcom/peel/i/gm;->aM:Lcom/peel/control/a;

    if-eqz v0, :cond_0

    .line 1985
    iput v5, p0, Lcom/peel/i/gm;->aC:I

    .line 1987
    iget-object v0, p0, Lcom/peel/i/gm;->aM:Lcom/peel/control/a;

    iget-object v1, p0, Lcom/peel/i/gm;->aJ:Lcom/peel/control/h;

    const/4 v2, 0x0

    new-array v3, v5, [Ljava/lang/Integer;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v8

    invoke-virtual {v0, v1, v2, v3}, Lcom/peel/control/a;->a(Lcom/peel/control/h;Ljava/lang/String;[Ljava/lang/Integer;)Z

    .line 1988
    invoke-virtual {p0}, Lcom/peel/i/gm;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/ae;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/peel/social/w;->f(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 1989
    new-instance v0, Lcom/peel/backup/c;

    invoke-virtual {p0}, Lcom/peel/i/gm;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/ae;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/peel/backup/c;-><init>(Landroid/content/Context;)V

    .line 1990
    iget-object v1, p0, Lcom/peel/i/gm;->aL:Lcom/peel/control/RoomControl;

    iget-object v2, p0, Lcom/peel/i/gm;->aN:Lcom/peel/content/library/LiveLibrary;

    invoke-virtual {v2}, Lcom/peel/content/library/LiveLibrary;->d()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/peel/i/gm;->aM:Lcom/peel/control/a;

    iget-object v4, p0, Lcom/peel/i/gm;->aJ:Lcom/peel/control/h;

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/peel/backup/c;->a(Lcom/peel/control/RoomControl;Ljava/lang/String;Lcom/peel/control/a;Lcom/peel/control/h;)V

    .line 1993
    :cond_9
    iget-object v0, p0, Lcom/peel/i/gm;->aL:Lcom/peel/control/RoomControl;

    iget-object v1, p0, Lcom/peel/i/gm;->aM:Lcom/peel/control/a;

    invoke-virtual {v0, v1, v8}, Lcom/peel/control/RoomControl;->a(Lcom/peel/control/a;I)Z

    .line 1995
    iget-object v0, p0, Lcom/peel/i/gm;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "from_tutorial"

    invoke-virtual {v0, v1, v8}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 1996
    const-string/jumbo v0, "remoteSetupCompletion"

    invoke-virtual {p0}, Lcom/peel/i/gm;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/peel/util/a;->a(Ljava/lang/String;Landroid/content/Context;)V

    .line 1998
    :cond_a
    iget-object v0, p0, Lcom/peel/i/gm;->bz:Landroid/os/Handler;

    iget-object v1, p0, Lcom/peel/i/gm;->bz:Landroid/os/Handler;

    const/4 v2, 0x6

    const/16 v3, 0x63

    invoke-static {v1, v8, v2, v3}, Landroid/os/Message;->obtain(Landroid/os/Handler;III)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto/16 :goto_0

    .line 1975
    :cond_b
    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v0}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/data/at;->f()I

    move-result v0

    goto/16 :goto_3

    .line 1977
    :cond_c
    const-string/jumbo v0, "NULL"

    goto/16 :goto_4

    .line 1979
    :cond_d
    const-string/jumbo v0, "NULL"

    goto/16 :goto_5

    .line 1981
    :cond_e
    iget-object v0, p0, Lcom/peel/i/gm;->aL:Lcom/peel/control/RoomControl;

    invoke-virtual {v0}, Lcom/peel/control/RoomControl;->c()[Lcom/peel/control/a;

    move-result-object v0

    array-length v0, v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto/16 :goto_6

    .line 2001
    :cond_f
    sget v0, Lcom/peel/i/gm;->f:I

    const/16 v1, 0x8

    if-ne v0, v1, :cond_10

    .line 2002
    iget-object v0, p0, Lcom/peel/i/gm;->ap:Lcom/peel/f/a;

    if-eqz v0, :cond_0

    .line 2003
    new-instance v0, Lcom/peel/widget/ag;

    invoke-virtual {p0}, Lcom/peel/i/gm;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/peel/widget/ag;-><init>(Landroid/content/Context;)V

    sget v1, Lcom/peel/ui/ft;->setup_stb_dialog_title:I

    invoke-virtual {p0, v1}, Lcom/peel/i/gm;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/peel/widget/ag;->a(Ljava/lang/CharSequence;)Lcom/peel/widget/ag;

    move-result-object v0

    sget v1, Lcom/peel/ui/ft;->setup_stb_dialog_msg:I

    .line 2004
    invoke-virtual {p0, v1}, Lcom/peel/i/gm;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/peel/widget/ag;->b(Ljava/lang/CharSequence;)Lcom/peel/widget/ag;

    move-result-object v0

    sget v1, Lcom/peel/ui/ft;->done:I

    new-instance v2, Lcom/peel/i/ie;

    invoke-direct {v2, p0}, Lcom/peel/i/ie;-><init>(Lcom/peel/i/gm;)V

    .line 2005
    invoke-virtual {v0, v1, v2}, Lcom/peel/widget/ag;->a(ILandroid/content/DialogInterface$OnClickListener;)Lcom/peel/widget/ag;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/i/gm;->aI:Lcom/peel/widget/ag;

    .line 2015
    iget-object v0, p0, Lcom/peel/i/gm;->aI:Lcom/peel/widget/ag;

    invoke-virtual {v0, v8}, Lcom/peel/widget/ag;->setCanceledOnTouchOutside(Z)V

    .line 2016
    iget-object v0, p0, Lcom/peel/i/gm;->aI:Lcom/peel/widget/ag;

    iget-object v1, p0, Lcom/peel/i/gm;->aI:Lcom/peel/widget/ag;

    invoke-virtual {v0, v1}, Lcom/peel/widget/ag;->a(Lcom/peel/widget/ag;)Lcom/peel/widget/ag;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/widget/ag;->show()V

    goto/16 :goto_0

    .line 2018
    :cond_10
    sget v0, Lcom/peel/i/gm;->f:I

    if-ne v0, v5, :cond_0

    .line 2019
    iget-object v0, p0, Lcom/peel/i/gm;->bj:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v0

    if-nez v0, :cond_11

    .line 2020
    iget-object v0, p0, Lcom/peel/i/gm;->bj:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, v5}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    goto/16 :goto_0

    .line 2022
    :cond_11
    iget-object v0, p0, Lcom/peel/i/gm;->bz:Landroid/os/Handler;

    iget-object v1, p0, Lcom/peel/i/gm;->bz:Landroid/os/Handler;

    const/4 v2, 0x2

    invoke-static {v1, v8, v5, v2}, Landroid/os/Message;->obtain(Landroid/os/Handler;III)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto/16 :goto_0
.end method

.method public b()Z
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 2127
    iput-boolean v1, p0, Lcom/peel/i/gm;->bv:Z

    .line 2128
    iput-boolean v1, p0, Lcom/peel/i/gm;->bw:Z

    .line 2129
    iput-boolean v1, p0, Lcom/peel/i/gm;->bx:Z

    .line 2130
    iput-boolean v1, p0, Lcom/peel/i/gm;->by:Z

    .line 2132
    invoke-virtual {p0}, Lcom/peel/i/gm;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    const-string/jumbo v3, "input_method"

    invoke-virtual {v0, v3}, Landroid/support/v4/app/ae;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 2133
    invoke-virtual {p0}, Lcom/peel/i/gm;->m()Landroid/support/v4/app/ae;

    move-result-object v3

    invoke-virtual {v3}, Landroid/support/v4/app/ae;->getWindow()Landroid/view/Window;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v3

    invoke-virtual {v0, v3, v1}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 2135
    iget-object v0, p0, Lcom/peel/i/gm;->c:Landroid/os/Bundle;

    const-string/jumbo v3, "popstack"

    invoke-virtual {v0, v3, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2136
    invoke-super {p0}, Lcom/peel/d/u;->b()Z

    move-result v0

    .line 2166
    :goto_0
    return v0

    .line 2139
    :cond_0
    iget-object v0, p0, Lcom/peel/i/gm;->aO:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->empty()Z

    move-result v0

    if-nez v0, :cond_4

    .line 2140
    iget-object v0, p0, Lcom/peel/i/gm;->aO:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/i/im;

    .line 2142
    iget v3, v0, Lcom/peel/i/im;->a:I

    const/4 v4, -0x1

    if-eq v3, v4, :cond_1

    iget v0, v0, Lcom/peel/i/im;->a:I

    if-nez v0, :cond_4

    .line 2144
    :cond_1
    iget-object v0, p0, Lcom/peel/i/gm;->aM:Lcom/peel/control/a;

    if-eqz v0, :cond_2

    .line 2146
    iget-object v0, p0, Lcom/peel/i/gm;->aL:Lcom/peel/control/RoomControl;

    iget-object v3, p0, Lcom/peel/i/gm;->aM:Lcom/peel/control/a;

    invoke-virtual {v0, v3}, Lcom/peel/control/RoomControl;->b(Lcom/peel/control/a;)V

    .line 2148
    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    iget-object v3, p0, Lcom/peel/i/gm;->aM:Lcom/peel/control/a;

    invoke-virtual {v0, v3}, Lcom/peel/control/am;->a(Lcom/peel/control/a;)V

    .line 2149
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/peel/i/gm;->aM:Lcom/peel/control/a;

    .line 2152
    :cond_2
    invoke-virtual {p0}, Lcom/peel/i/gm;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    invoke-static {v0}, Lcom/peel/d/e;->b(Landroid/support/v4/app/ae;)I

    move-result v0

    if-le v0, v2, :cond_3

    .line 2153
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 2154
    const-string/jumbo v3, "from_tv_setup"

    invoke-virtual {v0, v3, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    move v0, v1

    .line 2159
    goto :goto_0

    :cond_3
    move v0, v1

    .line 2161
    goto :goto_0

    .line 2165
    :cond_4
    iget-object v0, p0, Lcom/peel/i/gm;->bz:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    move v0, v2

    .line 2166
    goto :goto_0
.end method

.method public c(Landroid/os/Bundle;)V
    .locals 7

    .prologue
    const/4 v6, 0x7

    const/4 v5, -0x1

    const/4 v4, 0x1

    const/4 v3, 0x2

    const/4 v2, 0x0

    .line 559
    invoke-super {p0, p1}, Lcom/peel/d/u;->c(Landroid/os/Bundle;)V

    .line 561
    sget-object v0, Lcom/peel/content/a;->h:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-nez v0, :cond_1

    .line 590
    :cond_0
    :goto_0
    return-void

    .line 562
    :cond_1
    sget-object v0, Lcom/peel/content/a;->f:Lcom/peel/content/user/User;

    invoke-virtual {v0}, Lcom/peel/content/user/User;->x()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/peel/i/gm;->an:I

    .line 564
    const-string/jumbo v0, "live"

    invoke-static {v0}, Lcom/peel/content/a;->c(Ljava/lang/String;)Lcom/peel/content/library/Library;

    move-result-object v0

    check-cast v0, Lcom/peel/content/library/LiveLibrary;

    iput-object v0, p0, Lcom/peel/i/gm;->aN:Lcom/peel/content/library/LiveLibrary;

    .line 567
    const-string/jumbo v0, "room"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 568
    if-eqz v0, :cond_3

    sget-object v1, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v1, v0}, Lcom/peel/control/am;->a(Ljava/lang/String;)Lcom/peel/control/RoomControl;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/i/gm;->aL:Lcom/peel/control/RoomControl;

    .line 571
    :goto_1
    iput-boolean v2, p0, Lcom/peel/i/gm;->aB:Z

    .line 572
    iget v0, p0, Lcom/peel/i/gm;->bA:I

    if-le v0, v5, :cond_0

    .line 577
    iget v0, p0, Lcom/peel/i/gm;->bA:I

    if-ne v0, v4, :cond_4

    .line 578
    iget-object v0, p0, Lcom/peel/i/gm;->bz:Landroid/os/Handler;

    iget-object v1, p0, Lcom/peel/i/gm;->bz:Landroid/os/Handler;

    invoke-static {v1, v2, v2, v4}, Landroid/os/Message;->obtain(Landroid/os/Handler;III)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 588
    :cond_2
    :goto_2
    iput v5, p0, Lcom/peel/i/gm;->bA:I

    goto :goto_0

    .line 569
    :cond_3
    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v0}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/i/gm;->aL:Lcom/peel/control/RoomControl;

    goto :goto_1

    .line 579
    :cond_4
    iget v0, p0, Lcom/peel/i/gm;->bA:I

    if-ne v0, v3, :cond_6

    .line 580
    sget-boolean v0, Lcom/peel/util/a;->l:Z

    if-eqz v0, :cond_5

    .line 581
    iget-object v0, p0, Lcom/peel/i/gm;->bz:Landroid/os/Handler;

    iget-object v1, p0, Lcom/peel/i/gm;->bz:Landroid/os/Handler;

    invoke-static {v1, v2, v2, v3}, Landroid/os/Message;->obtain(Landroid/os/Handler;III)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_2

    .line 583
    :cond_5
    iget-object v0, p0, Lcom/peel/i/gm;->bz:Landroid/os/Handler;

    iget-object v1, p0, Lcom/peel/i/gm;->bz:Landroid/os/Handler;

    invoke-static {v1, v2, v4, v3}, Landroid/os/Message;->obtain(Landroid/os/Handler;III)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_2

    .line 585
    :cond_6
    iget v0, p0, Lcom/peel/i/gm;->bA:I

    if-ne v0, v6, :cond_2

    .line 586
    iget-object v0, p0, Lcom/peel/i/gm;->bz:Landroid/os/Handler;

    iget-object v1, p0, Lcom/peel/i/gm;->bz:Landroid/os/Handler;

    invoke-static {v1, v2, v3, v6}, Landroid/os/Message;->obtain(Landroid/os/Handler;III)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_2
.end method

.method public d(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 528
    invoke-super {p0, p1}, Lcom/peel/d/u;->d(Landroid/os/Bundle;)V

    .line 529
    iget-object v0, p0, Lcom/peel/i/gm;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "category"

    sget v2, Lcom/peel/ui/ft;->label_setup_now:I

    invoke-virtual {p0, v2}, Lcom/peel/i/gm;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 531
    iget-object v0, p0, Lcom/peel/i/gm;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "passback_clazz"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 532
    iget-object v0, p0, Lcom/peel/i/gm;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "passback_clazz"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/i/gm;->aF:Ljava/lang/String;

    .line 534
    :cond_0
    iget-object v0, p0, Lcom/peel/i/gm;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "passback_bundle"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 535
    iget-object v0, p0, Lcom/peel/i/gm;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "passback_bundle"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/i/gm;->aP:Landroid/os/Bundle;

    .line 538
    :cond_1
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/i/gm;->ba:Ljava/lang/String;

    .line 540
    iget-object v0, p0, Lcom/peel/i/gm;->b:Lcom/peel/d/i;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/peel/i/gm;->b:Lcom/peel/d/i;

    sget-object v1, Lcom/peel/i/gm;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/peel/d/i;->c(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 541
    new-instance v0, Landroid/os/Bundle;

    iget-object v1, p0, Lcom/peel/i/gm;->b:Lcom/peel/d/i;

    sget-object v2, Lcom/peel/i/gm;->e:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/peel/d/i;->c(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Bundle;-><init>(Landroid/os/Bundle;)V

    .line 543
    const-string/jumbo v1, "child_idx"

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/peel/i/gm;->bA:I

    .line 545
    iget-object v0, p0, Lcom/peel/i/gm;->b:Lcom/peel/d/i;

    sget-object v1, Lcom/peel/i/gm;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/peel/d/i;->c(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Bundle;->clear()V

    .line 550
    :goto_0
    invoke-virtual {p0}, Lcom/peel/i/gm;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string/jumbo v1, "country_ISO"

    const-string/jumbo v2, "US"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/i/gm;->aE:Ljava/lang/String;

    .line 552
    sget-object v0, Lcom/peel/content/a;->h:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 553
    iget-object v0, p0, Lcom/peel/i/gm;->c:Landroid/os/Bundle;

    invoke-virtual {p0, v0}, Lcom/peel/i/gm;->c(Landroid/os/Bundle;)V

    .line 555
    :cond_2
    return-void

    .line 547
    :cond_3
    sget-boolean v0, Lcom/peel/util/a;->l:Z

    if-eqz v0, :cond_4

    const/4 v0, 0x2

    :goto_1
    iput v0, p0, Lcom/peel/i/gm;->bA:I

    goto :goto_0

    :cond_4
    const/4 v0, 0x1

    goto :goto_1
.end method

.method public e()V
    .locals 6

    .prologue
    const-wide/16 v4, 0xfa

    const/4 v3, 0x1

    .line 512
    invoke-super {p0}, Lcom/peel/d/u;->e()V

    .line 513
    invoke-virtual {p0}, Lcom/peel/i/gm;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    const-string/jumbo v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/ae;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 514
    invoke-virtual {p0}, Lcom/peel/i/gm;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/ae;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 515
    iget-object v0, p0, Lcom/peel/i/gm;->au:Landroid/widget/AutoCompleteTextView;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/peel/i/gm;->au:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v0}, Landroid/widget/AutoCompleteTextView;->isFocused()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 516
    invoke-virtual {p0}, Lcom/peel/i/gm;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    const-class v1, Lcom/peel/i/gm;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/peel/i/gm;->au:Landroid/widget/AutoCompleteTextView;

    invoke-static {v0, v1, v2, v4, v5}, Lcom/peel/util/bx;->a(Landroid/content/Context;Ljava/lang/String;Landroid/widget/EditText;J)V

    .line 521
    :cond_0
    :goto_0
    sget v0, Lcom/peel/i/gm;->f:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_3

    .line 522
    invoke-virtual {p0}, Lcom/peel/i/gm;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    iget v1, p0, Lcom/peel/i/gm;->am:I

    invoke-static {v0, v1}, Lcom/peel/util/bx;->c(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v0

    iget-boolean v1, p0, Lcom/peel/i/gm;->bv:Z

    invoke-direct {p0, v0, v3, v1}, Lcom/peel/i/gm;->a(Ljava/lang/String;ZZ)V

    .line 525
    :cond_1
    :goto_1
    return-void

    .line 517
    :cond_2
    iget-object v0, p0, Lcom/peel/i/gm;->av:Landroid/widget/AutoCompleteTextView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/peel/i/gm;->av:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v0}, Landroid/widget/AutoCompleteTextView;->isFocused()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 518
    invoke-virtual {p0}, Lcom/peel/i/gm;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    const-class v1, Lcom/peel/i/gm;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/peel/i/gm;->av:Landroid/widget/AutoCompleteTextView;

    invoke-static {v0, v1, v2, v4, v5}, Lcom/peel/util/bx;->a(Landroid/content/Context;Ljava/lang/String;Landroid/widget/EditText;J)V

    goto :goto_0

    .line 523
    :cond_3
    sget v0, Lcom/peel/i/gm;->f:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_1

    .line 524
    invoke-virtual {p0}, Lcom/peel/i/gm;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    iget v1, p0, Lcom/peel/i/gm;->am:I

    invoke-static {v0, v1}, Lcom/peel/util/bx;->c(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v0

    iget-boolean v1, p0, Lcom/peel/i/gm;->bw:Z

    invoke-direct {p0, v0, v3, v1}, Lcom/peel/i/gm;->a(Ljava/lang/String;ZZ)V

    goto :goto_1
.end method

.method public f()V
    .locals 3

    .prologue
    .line 502
    invoke-super {p0}, Lcom/peel/d/u;->f()V

    .line 503
    invoke-virtual {p0}, Lcom/peel/i/gm;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    const-string/jumbo v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/ae;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 504
    invoke-virtual {p0}, Lcom/peel/i/gm;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/ae;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 506
    iget-object v0, p0, Lcom/peel/i/gm;->aI:Lcom/peel/widget/ag;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/peel/i/gm;->aI:Lcom/peel/widget/ag;

    invoke-virtual {v0}, Lcom/peel/widget/ag;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 507
    iget-object v0, p0, Lcom/peel/i/gm;->aI:Lcom/peel/widget/ag;

    invoke-virtual {v0}, Lcom/peel/widget/ag;->dismiss()V

    .line 509
    :cond_0
    return-void
.end method

.method public g()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2114
    iget-object v0, p0, Lcom/peel/i/gm;->ak:Lcom/peel/i/a/a;

    if-eqz v0, :cond_0

    .line 2115
    iput-object v1, p0, Lcom/peel/i/gm;->ak:Lcom/peel/i/a/a;

    .line 2118
    :cond_0
    iget-object v0, p0, Lcom/peel/i/gm;->ar:Landroid/widget/ListView;

    if-eqz v0, :cond_1

    .line 2119
    iput-object v1, p0, Lcom/peel/i/gm;->ar:Landroid/widget/ListView;

    .line 2121
    :cond_1
    invoke-super {p0}, Lcom/peel/d/u;->g()V

    .line 2122
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 7

    .prologue
    const/4 v4, 0x3

    const/4 v2, 0x0

    const/4 v6, 0x2

    const/4 v0, 0x1

    const/4 v5, 0x0

    .line 2031
    iget-object v1, p0, Lcom/peel/i/gm;->b:Lcom/peel/d/i;

    if-nez v1, :cond_1

    .line 2110
    :cond_0
    :goto_0
    return-void

    .line 2034
    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    .line 2035
    sget v3, Lcom/peel/ui/fp;->next_btn:I

    if-ne v1, v3, :cond_3

    .line 2036
    invoke-static {}, Lcom/peel/util/a/f;->a()Lcom/peel/util/a/f;

    move-result-object v1

    sget-object v3, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v3}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v3

    if-nez v3, :cond_2

    :goto_1
    const/16 v3, 0xbec

    const/16 v4, 0x7d5

    invoke-virtual {v1, v0, v3, v4, v2}, Lcom/peel/util/a/f;->a(IIILjava/lang/String;)V

    .line 2037
    iget-object v0, p0, Lcom/peel/i/gm;->bz:Landroid/os/Handler;

    iget-object v1, p0, Lcom/peel/i/gm;->bz:Landroid/os/Handler;

    invoke-static {v1, v5, v5, v6}, Landroid/os/Message;->obtain(Landroid/os/Handler;III)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0

    .line 2036
    :cond_2
    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v0}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/data/at;->f()I

    move-result v0

    goto :goto_1

    .line 2039
    :cond_3
    sget v3, Lcom/peel/ui/fp;->other_tv_brand_btn:I

    if-ne v1, v3, :cond_4

    .line 2040
    iget-object v0, p0, Lcom/peel/i/gm;->bz:Landroid/os/Handler;

    iget-object v1, p0, Lcom/peel/i/gm;->bz:Landroid/os/Handler;

    sget v2, Lcom/peel/ui/fp;->other_tv_brand_btn:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v1, v5, v6, v4, v2}, Landroid/os/Message;->obtain(Landroid/os/Handler;IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0

    .line 2042
    :cond_4
    sget v3, Lcom/peel/ui/fp;->projector_btn:I

    if-ne v1, v3, :cond_5

    .line 2043
    iget-object v0, p0, Lcom/peel/i/gm;->bz:Landroid/os/Handler;

    iget-object v1, p0, Lcom/peel/i/gm;->bz:Landroid/os/Handler;

    sget v2, Lcom/peel/ui/fp;->projector_btn:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v1, v5, v6, v4, v2}, Landroid/os/Message;->obtain(Landroid/os/Handler;IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0

    .line 2074
    :cond_5
    sget v3, Lcom/peel/ui/fp;->tv_only_btn:I

    if-ne v1, v3, :cond_c

    .line 2075
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "watch tv activity: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v1, p0, Lcom/peel/i/gm;->aM:Lcom/peel/control/a;

    if-eqz v1, :cond_8

    const-string/jumbo v1, "NOT NULL"

    :goto_2
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 2076
    sget-object v3, Lcom/peel/i/gm;->e:Ljava/lang/String;

    invoke-static {v3, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2077
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "tv device: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v1, p0, Lcom/peel/i/gm;->aJ:Lcom/peel/control/h;

    if-eqz v1, :cond_9

    const-string/jumbo v1, "NOT NULL"

    :goto_3
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 2078
    sget-object v3, Lcom/peel/i/gm;->e:Ljava/lang/String;

    invoke-static {v3, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2079
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "room.getActivities(): "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v1, p0, Lcom/peel/i/gm;->aL:Lcom/peel/control/RoomControl;

    invoke-virtual {v1}, Lcom/peel/control/RoomControl;->c()[Lcom/peel/control/a;

    move-result-object v1

    if-nez v1, :cond_a

    const-string/jumbo v1, "NO ACTIVITIES"

    :goto_4
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 2080
    sget-object v3, Lcom/peel/i/gm;->e:Ljava/lang/String;

    invoke-static {v3, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2081
    iget-object v1, p0, Lcom/peel/i/gm;->aM:Lcom/peel/control/a;

    if-eqz v1, :cond_0

    .line 2082
    iput v0, p0, Lcom/peel/i/gm;->aC:I

    .line 2084
    iget-object v1, p0, Lcom/peel/i/gm;->aM:Lcom/peel/control/a;

    iget-object v3, p0, Lcom/peel/i/gm;->aJ:Lcom/peel/control/h;

    new-array v4, v0, [Ljava/lang/Integer;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v4, v5

    invoke-virtual {v1, v3, v2, v4}, Lcom/peel/control/a;->a(Lcom/peel/control/h;Ljava/lang/String;[Ljava/lang/Integer;)Z

    .line 2085
    invoke-virtual {p0}, Lcom/peel/i/gm;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/ae;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/peel/social/w;->f(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 2086
    new-instance v1, Lcom/peel/backup/c;

    invoke-virtual {p0}, Lcom/peel/i/gm;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/ae;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/peel/backup/c;-><init>(Landroid/content/Context;)V

    .line 2087
    iget-object v0, p0, Lcom/peel/i/gm;->aN:Lcom/peel/content/library/LiveLibrary;

    if-eqz v0, :cond_b

    iget-object v0, p0, Lcom/peel/i/gm;->aN:Lcom/peel/content/library/LiveLibrary;

    invoke-virtual {v0}, Lcom/peel/content/library/LiveLibrary;->d()Ljava/lang/String;

    move-result-object v0

    .line 2088
    :goto_5
    iget-object v2, p0, Lcom/peel/i/gm;->aL:Lcom/peel/control/RoomControl;

    iget-object v3, p0, Lcom/peel/i/gm;->aM:Lcom/peel/control/a;

    iget-object v4, p0, Lcom/peel/i/gm;->aJ:Lcom/peel/control/h;

    invoke-virtual {v1, v2, v0, v3, v4}, Lcom/peel/backup/c;->a(Lcom/peel/control/RoomControl;Ljava/lang/String;Lcom/peel/control/a;Lcom/peel/control/h;)V

    .line 2091
    :cond_6
    iget-object v0, p0, Lcom/peel/i/gm;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "from_tutorial"

    invoke-virtual {v0, v1, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 2092
    const-string/jumbo v0, "remoteSetupCompletion"

    invoke-virtual {p0}, Lcom/peel/i/gm;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/peel/util/a;->a(Ljava/lang/String;Landroid/content/Context;)V

    .line 2095
    :cond_7
    iget-object v0, p0, Lcom/peel/i/gm;->bz:Landroid/os/Handler;

    iget-object v1, p0, Lcom/peel/i/gm;->bz:Landroid/os/Handler;

    const/4 v2, 0x6

    const/16 v3, 0x63

    invoke-static {v1, v5, v2, v3}, Landroid/os/Message;->obtain(Landroid/os/Handler;III)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto/16 :goto_0

    .line 2075
    :cond_8
    const-string/jumbo v1, "NULL"

    goto/16 :goto_2

    .line 2077
    :cond_9
    const-string/jumbo v1, "NULL"

    goto/16 :goto_3

    .line 2079
    :cond_a
    iget-object v1, p0, Lcom/peel/i/gm;->aL:Lcom/peel/control/RoomControl;

    invoke-virtual {v1}, Lcom/peel/control/RoomControl;->c()[Lcom/peel/control/a;

    move-result-object v1

    array-length v1, v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    goto/16 :goto_4

    :cond_b
    move-object v0, v2

    .line 2087
    goto :goto_5

    .line 2098
    :cond_c
    sget v0, Lcom/peel/ui/fp;->tv_with_dvr_btn:I

    if-ne v1, v0, :cond_0

    .line 2099
    iget-object v0, p0, Lcom/peel/i/gm;->aM:Lcom/peel/control/a;

    if-eqz v0, :cond_0

    .line 2100
    check-cast p1, Landroid/widget/Button;

    invoke-virtual {p1}, Landroid/widget/Button;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    sget v1, Lcom/peel/ui/ft;->DeviceType20:I

    invoke-virtual {p0, v1}, Lcom/peel/i/gm;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 2101
    const/4 v0, 0x4

    iput v0, p0, Lcom/peel/i/gm;->aC:I

    .line 2106
    :goto_6
    iget-object v0, p0, Lcom/peel/i/gm;->bz:Landroid/os/Handler;

    iget-object v1, p0, Lcom/peel/i/gm;->bz:Landroid/os/Handler;

    const/4 v2, 0x6

    const/4 v3, 0x7

    invoke-static {v1, v5, v2, v3}, Landroid/os/Message;->obtain(Landroid/os/Handler;III)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto/16 :goto_0

    .line 2103
    :cond_d
    iput v6, p0, Lcom/peel/i/gm;->aC:I

    goto :goto_6
.end method

.method public w()V
    .locals 6

    .prologue
    const-wide/16 v4, 0xfa

    const/4 v1, 0x1

    .line 462
    invoke-super {p0}, Lcom/peel/d/u;->w()V

    .line 464
    :try_start_0
    sget v0, Lcom/peel/i/gm;->f:I

    if-nez v0, :cond_2

    .line 465
    sget v0, Lcom/peel/ui/ft;->using_peel:I

    invoke-virtual {p0, v0}, Lcom/peel/i/gm;->a(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-direct {p0, v0, v1, v2}, Lcom/peel/i/gm;->a(Ljava/lang/String;ZZ)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 487
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/peel/i/gm;->aQ:Lcom/peel/ui/dg;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/peel/i/gm;->aQ:Lcom/peel/ui/dg;

    invoke-virtual {v0}, Lcom/peel/ui/dg;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 488
    iget-object v0, p0, Lcom/peel/i/gm;->aQ:Lcom/peel/ui/dg;

    iget-object v0, v0, Lcom/peel/ui/dg;->b:Landroid/widget/EditText;

    if-eqz v0, :cond_b

    iget-object v0, p0, Lcom/peel/i/gm;->aQ:Lcom/peel/ui/dg;

    iget-object v0, v0, Lcom/peel/ui/dg;->b:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->isFocused()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 489
    invoke-virtual {p0}, Lcom/peel/i/gm;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    const-class v1, Lcom/peel/i/gm;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/peel/i/gm;->aQ:Lcom/peel/ui/dg;

    iget-object v2, v2, Lcom/peel/ui/dg;->b:Landroid/widget/EditText;

    invoke-static {v0, v1, v2, v4, v5}, Lcom/peel/util/bx;->a(Landroid/content/Context;Ljava/lang/String;Landroid/widget/EditText;J)V

    .line 497
    :cond_1
    :goto_1
    return-void

    .line 466
    :cond_2
    :try_start_1
    sget v0, Lcom/peel/i/gm;->f:I

    if-ne v0, v1, :cond_3

    .line 467
    sget v0, Lcom/peel/ui/ft;->before_you_start:I

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-direct {p0}, Lcom/peel/i/gm;->am()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {p0, v0, v1}, Lcom/peel/i/gm;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    const/4 v2, 0x1

    invoke-direct {p0, v0, v1, v2}, Lcom/peel/i/gm;->a(Ljava/lang/String;ZZ)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 484
    :catch_0
    move-exception v0

    .line 485
    sget-object v1, Lcom/peel/i/gm;->e:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "failed in setup title"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 468
    :cond_3
    :try_start_2
    sget v0, Lcom/peel/i/gm;->f:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_4

    .line 469
    invoke-virtual {p0}, Lcom/peel/i/gm;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    iget v1, p0, Lcom/peel/i/gm;->am:I

    invoke-static {v0, v1}, Lcom/peel/util/bx;->c(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    iget-boolean v2, p0, Lcom/peel/i/gm;->bv:Z

    invoke-direct {p0, v0, v1, v2}, Lcom/peel/i/gm;->a(Ljava/lang/String;ZZ)V

    goto :goto_0

    .line 470
    :cond_4
    sget v0, Lcom/peel/i/gm;->f:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_5

    .line 471
    invoke-virtual {p0}, Lcom/peel/i/gm;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    iget v1, p0, Lcom/peel/i/gm;->am:I

    invoke-static {v0, v1}, Lcom/peel/util/bx;->c(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    iget-boolean v2, p0, Lcom/peel/i/gm;->bw:Z

    invoke-direct {p0, v0, v1, v2}, Lcom/peel/i/gm;->a(Ljava/lang/String;ZZ)V

    goto/16 :goto_0

    .line 472
    :cond_5
    sget v0, Lcom/peel/i/gm;->f:I

    const/4 v1, 0x4

    if-ne v0, v1, :cond_6

    .line 473
    sget v0, Lcom/peel/ui/ft;->testing_device:I

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-direct {p0}, Lcom/peel/i/gm;->am()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {p0, v0, v1}, Lcom/peel/i/gm;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-direct {p0, v0, v1, v2}, Lcom/peel/i/gm;->a(Ljava/lang/String;ZZ)V

    goto/16 :goto_0

    .line 474
    :cond_6
    sget v0, Lcom/peel/i/gm;->f:I

    const/4 v1, 0x6

    if-ne v0, v1, :cond_7

    .line 475
    sget v0, Lcom/peel/ui/ft;->google_tv_setup:I

    invoke-virtual {p0, v0}, Lcom/peel/i/gm;->a(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-direct {p0, v0, v1, v2}, Lcom/peel/i/gm;->a(Ljava/lang/String;ZZ)V

    goto/16 :goto_0

    .line 476
    :cond_7
    sget v0, Lcom/peel/i/gm;->f:I

    const/16 v1, 0x64

    if-ne v0, v1, :cond_8

    .line 477
    sget v0, Lcom/peel/ui/ft;->choose_channel_changer:I

    invoke-virtual {p0, v0}, Lcom/peel/i/gm;->a(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-direct {p0, v0, v1, v2}, Lcom/peel/i/gm;->a(Ljava/lang/String;ZZ)V

    goto/16 :goto_0

    .line 478
    :cond_8
    sget v0, Lcom/peel/i/gm;->f:I

    const/4 v1, 0x7

    if-ne v0, v1, :cond_9

    .line 479
    sget v0, Lcom/peel/ui/ft;->action_bar_title_stb:I

    invoke-virtual {p0, v0}, Lcom/peel/i/gm;->a(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    iget-boolean v2, p0, Lcom/peel/i/gm;->bx:Z

    invoke-direct {p0, v0, v1, v2}, Lcom/peel/i/gm;->a(Ljava/lang/String;ZZ)V

    goto/16 :goto_0

    .line 480
    :cond_9
    sget v0, Lcom/peel/i/gm;->f:I

    const/16 v1, 0x8

    if-ne v0, v1, :cond_a

    .line 481
    sget v0, Lcom/peel/ui/ft;->action_bar_title_stb:I

    invoke-virtual {p0, v0}, Lcom/peel/i/gm;->a(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    iget-boolean v2, p0, Lcom/peel/i/gm;->by:Z

    invoke-direct {p0, v0, v1, v2}, Lcom/peel/i/gm;->a(Ljava/lang/String;ZZ)V

    goto/16 :goto_0

    .line 482
    :cond_a
    sget v0, Lcom/peel/i/gm;->f:I

    const/16 v1, 0x9

    if-ne v0, v1, :cond_0

    .line 483
    sget v0, Lcom/peel/ui/ft;->setup_stb_dialog_title:I

    invoke-virtual {p0, v0}, Lcom/peel/i/gm;->a(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-direct {p0, v0, v1, v2}, Lcom/peel/i/gm;->a(Ljava/lang/String;ZZ)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    goto/16 :goto_0

    .line 490
    :cond_b
    iget-object v0, p0, Lcom/peel/i/gm;->aQ:Lcom/peel/ui/dg;

    iget-object v0, v0, Lcom/peel/ui/dg;->c:Landroid/widget/EditText;

    if-eqz v0, :cond_c

    iget-object v0, p0, Lcom/peel/i/gm;->aQ:Lcom/peel/ui/dg;

    iget-object v0, v0, Lcom/peel/ui/dg;->c:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->isFocused()Z

    move-result v0

    if-eqz v0, :cond_c

    .line 491
    invoke-virtual {p0}, Lcom/peel/i/gm;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    const-class v1, Lcom/peel/i/gm;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/peel/i/gm;->aQ:Lcom/peel/ui/dg;

    iget-object v2, v2, Lcom/peel/ui/dg;->c:Landroid/widget/EditText;

    invoke-static {v0, v1, v2, v4, v5}, Lcom/peel/util/bx;->a(Landroid/content/Context;Ljava/lang/String;Landroid/widget/EditText;J)V

    goto/16 :goto_1

    .line 492
    :cond_c
    iget-object v0, p0, Lcom/peel/i/gm;->aQ:Lcom/peel/ui/dg;

    iget-object v0, v0, Lcom/peel/ui/dg;->d:Landroid/widget/EditText;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/peel/i/gm;->aQ:Lcom/peel/ui/dg;

    iget-object v0, v0, Lcom/peel/ui/dg;->d:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->isFocused()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 493
    invoke-virtual {p0}, Lcom/peel/i/gm;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    const-class v1, Lcom/peel/i/gm;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/peel/i/gm;->aQ:Lcom/peel/ui/dg;

    iget-object v2, v2, Lcom/peel/ui/dg;->d:Landroid/widget/EditText;

    invoke-static {v0, v1, v2, v4, v5}, Lcom/peel/util/bx;->a(Landroid/content/Context;Ljava/lang/String;Landroid/widget/EditText;J)V

    goto/16 :goto_1
.end method
