.class Lcom/peel/i/cc;
.super Lcom/peel/util/t;


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic b:Landroid/os/Bundle;

.field final synthetic c:Lcom/peel/i/cb;


# direct methods
.method constructor <init>(Lcom/peel/i/cb;ILjava/lang/String;Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 187
    iput-object p1, p0, Lcom/peel/i/cc;->c:Lcom/peel/i/cb;

    iput-object p3, p0, Lcom/peel/i/cc;->a:Ljava/lang/String;

    iput-object p4, p0, Lcom/peel/i/cc;->b:Landroid/os/Bundle;

    invoke-direct {p0, p2}, Lcom/peel/util/t;-><init>(I)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 12

    .prologue
    const/4 v1, -0x1

    .line 189
    iget-boolean v0, p0, Lcom/peel/i/cc;->i:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/peel/i/cc;->j:Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 190
    :cond_0
    invoke-static {}, Lcom/peel/i/by;->c()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/peel/i/cc;->j:Ljava/lang/Object;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/peel/i/cc;->k:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 242
    :goto_0
    return-void

    .line 194
    :cond_1
    iget-object v0, p0, Lcom/peel/i/cc;->j:Ljava/lang/Object;

    check-cast v0, Lcom/peel/content/user/User;

    .line 196
    invoke-static {}, Lcom/peel/data/m;->a()Lcom/peel/data/m;

    move-result-object v1

    invoke-virtual {v0}, Lcom/peel/content/user/User;->x()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "legacy"

    invoke-virtual {v0}, Lcom/peel/content/user/User;->t()Landroid/os/Bundle;

    move-result-object v4

    invoke-virtual {v1, v2, v3, v4}, Lcom/peel/data/m;->a(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 198
    sput-object v0, Lcom/peel/content/a;->f:Lcom/peel/content/user/User;

    .line 200
    sget-object v0, Lcom/peel/content/a;->f:Lcom/peel/content/user/User;

    invoke-virtual {v0}, Lcom/peel/content/user/User;->u()V

    .line 203
    :try_start_0
    iget-object v0, p0, Lcom/peel/i/cc;->c:Lcom/peel/i/cb;

    iget-object v0, v0, Lcom/peel/i/cb;->b:Lcom/peel/i/by;

    invoke-virtual {v0}, Lcom/peel/i/by;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/ae;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/i/cc;->c:Lcom/peel/i/cb;

    iget-object v1, v1, Lcom/peel/i/cb;->b:Lcom/peel/i/by;

    invoke-virtual {v1}, Lcom/peel/i/by;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/ae;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v8

    .line 204
    invoke-static {}, Lcom/peel/util/a/f;->a()Lcom/peel/util/a/f;

    move-result-object v0

    const/4 v1, 0x1

    const/16 v2, 0xbb8

    const/16 v3, 0x7d5

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "Handset: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-object v5, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "-"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-object v5, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, -0x1

    sget-object v6, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    iget v7, v8, Landroid/content/pm/PackageInfo;->versionCode:I

    iget-object v8, v8, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    const/4 v9, -0x1

    iget-object v10, p0, Lcom/peel/i/cc;->a:Ljava/lang/String;

    const/4 v11, -0x1

    invoke-virtual/range {v0 .. v11}, Lcom/peel/util/a/f;->a(IIILjava/lang/String;ILjava/lang/String;ILjava/lang/String;ILjava/lang/String;I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 209
    :goto_1
    invoke-static {}, Lcom/peel/util/a/f;->a()Lcom/peel/util/a/f;

    move-result-object v1

    iget-object v0, p0, Lcom/peel/i/cc;->j:Ljava/lang/Object;

    check-cast v0, Lcom/peel/content/user/User;

    invoke-virtual {v0}, Lcom/peel/content/user/User;->x()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/peel/util/a/f;->a(Ljava/lang/String;)V

    .line 211
    invoke-static {}, Lcom/peel/i/by;->c()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, ""

    new-instance v2, Lcom/peel/i/cd;

    invoke-direct {v2, p0}, Lcom/peel/i/cd;-><init>(Lcom/peel/i/cc;)V

    invoke-static {v0, v1, v2}, Lcom/peel/util/i;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/lang/Runnable;

    goto/16 :goto_0

    .line 206
    :catch_0
    move-exception v0

    goto :goto_1
.end method
