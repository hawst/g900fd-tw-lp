.class Lcom/peel/i/ej;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/peel/i/eh;


# direct methods
.method constructor <init>(Lcom/peel/i/eh;)V
    .locals 0

    .prologue
    .line 320
    iput-object p1, p0, Lcom/peel/i/ej;->a:Lcom/peel/i/eh;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 323
    iget-object v0, p0, Lcom/peel/i/ej;->a:Lcom/peel/i/eh;

    iget-object v0, v0, Lcom/peel/i/eh;->d:Lcom/peel/i/dx;

    new-instance v1, Lcom/peel/widget/ag;

    iget-object v2, p0, Lcom/peel/i/ej;->a:Lcom/peel/i/eh;

    iget-object v2, v2, Lcom/peel/i/eh;->d:Lcom/peel/i/dx;

    invoke-virtual {v2}, Lcom/peel/i/dx;->m()Landroid/support/v4/app/ae;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/peel/widget/ag;-><init>(Landroid/content/Context;)V

    invoke-static {v0, v1}, Lcom/peel/i/dx;->a(Lcom/peel/i/dx;Lcom/peel/widget/ag;)Lcom/peel/widget/ag;

    .line 324
    iget-object v0, p0, Lcom/peel/i/ej;->a:Lcom/peel/i/eh;

    iget-object v0, v0, Lcom/peel/i/eh;->a:Landroid/os/Bundle;

    const-string/jumbo v1, "type"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "zip"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 325
    iget-object v0, p0, Lcom/peel/i/ej;->a:Lcom/peel/i/eh;

    iget-object v0, v0, Lcom/peel/i/eh;->d:Lcom/peel/i/dx;

    invoke-static {v0}, Lcom/peel/i/dx;->i(Lcom/peel/i/dx;)Lcom/peel/widget/ag;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/i/ej;->a:Lcom/peel/i/eh;

    iget-object v1, v1, Lcom/peel/i/eh;->d:Lcom/peel/i/dx;

    invoke-virtual {v1}, Lcom/peel/i/dx;->n()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/peel/ui/ft;->zipcode_validation_msg_without_zipcode:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/peel/widget/ag;->b(Ljava/lang/CharSequence;)Lcom/peel/widget/ag;

    .line 329
    :goto_0
    iget-object v0, p0, Lcom/peel/i/ej;->a:Lcom/peel/i/eh;

    iget-object v0, v0, Lcom/peel/i/eh;->d:Lcom/peel/i/dx;

    invoke-static {v0}, Lcom/peel/i/dx;->i(Lcom/peel/i/dx;)Lcom/peel/widget/ag;

    move-result-object v0

    sget v1, Lcom/peel/ui/ft;->mytvservicemissing:I

    invoke-virtual {v0, v1}, Lcom/peel/widget/ag;->a(I)Lcom/peel/widget/ag;

    move-result-object v0

    sget v1, Lcom/peel/ui/ft;->label_report:I

    new-instance v2, Lcom/peel/i/ek;

    invoke-direct {v2, p0}, Lcom/peel/i/ek;-><init>(Lcom/peel/i/ej;)V

    invoke-virtual {v0, v1, v2}, Lcom/peel/widget/ag;->a(ILandroid/content/DialogInterface$OnClickListener;)Lcom/peel/widget/ag;

    move-result-object v0

    sget v1, Lcom/peel/ui/ft;->cancel:I

    const/4 v2, 0x0

    .line 346
    invoke-virtual {v0, v1, v2}, Lcom/peel/widget/ag;->c(ILandroid/content/DialogInterface$OnClickListener;)Lcom/peel/widget/ag;

    .line 347
    iget-object v0, p0, Lcom/peel/i/ej;->a:Lcom/peel/i/eh;

    iget-object v0, v0, Lcom/peel/i/eh;->d:Lcom/peel/i/dx;

    invoke-static {v0}, Lcom/peel/i/dx;->i(Lcom/peel/i/dx;)Lcom/peel/widget/ag;

    move-result-object v0

    invoke-virtual {v0, v6}, Lcom/peel/widget/ag;->setCanceledOnTouchOutside(Z)V

    .line 348
    iget-object v0, p0, Lcom/peel/i/ej;->a:Lcom/peel/i/eh;

    iget-object v0, v0, Lcom/peel/i/eh;->d:Lcom/peel/i/dx;

    invoke-static {v0}, Lcom/peel/i/dx;->i(Lcom/peel/i/dx;)Lcom/peel/widget/ag;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/i/ej;->a:Lcom/peel/i/eh;

    iget-object v1, v1, Lcom/peel/i/eh;->d:Lcom/peel/i/dx;

    invoke-static {v1}, Lcom/peel/i/dx;->i(Lcom/peel/i/dx;)Lcom/peel/widget/ag;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/peel/widget/ag;->a(Lcom/peel/widget/ag;)Lcom/peel/widget/ag;

    .line 349
    iget-object v0, p0, Lcom/peel/i/ej;->a:Lcom/peel/i/eh;

    iget-object v0, v0, Lcom/peel/i/eh;->d:Lcom/peel/i/dx;

    invoke-static {v0}, Lcom/peel/i/dx;->i(Lcom/peel/i/dx;)Lcom/peel/widget/ag;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/i/ej;->a:Lcom/peel/i/eh;

    iget-object v1, v1, Lcom/peel/i/eh;->d:Lcom/peel/i/dx;

    invoke-static {v1}, Lcom/peel/i/dx;->i(Lcom/peel/i/dx;)Lcom/peel/widget/ag;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/peel/widget/ag;->a(Lcom/peel/widget/ag;)Lcom/peel/widget/ag;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/widget/ag;->d()Lcom/peel/widget/ag;

    .line 350
    return-void

    .line 327
    :cond_0
    iget-object v0, p0, Lcom/peel/i/ej;->a:Lcom/peel/i/eh;

    iget-object v0, v0, Lcom/peel/i/eh;->d:Lcom/peel/i/dx;

    invoke-static {v0}, Lcom/peel/i/dx;->i(Lcom/peel/i/dx;)Lcom/peel/widget/ag;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/i/ej;->a:Lcom/peel/i/eh;

    iget-object v1, v1, Lcom/peel/i/eh;->d:Lcom/peel/i/dx;

    invoke-virtual {v1}, Lcom/peel/i/dx;->n()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/peel/ui/ft;->zipcode_validation_msg:I

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lcom/peel/i/ej;->a:Lcom/peel/i/eh;

    iget-object v5, v5, Lcom/peel/i/eh;->b:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/peel/i/ej;->a:Lcom/peel/i/eh;

    iget-object v5, v5, Lcom/peel/i/eh;->c:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/peel/widget/ag;->b(Ljava/lang/CharSequence;)Lcom/peel/widget/ag;

    goto/16 :goto_0
.end method
