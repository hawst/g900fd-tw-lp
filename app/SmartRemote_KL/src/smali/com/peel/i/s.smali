.class Lcom/peel/i/s;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/support/v4/view/cs;


# instance fields
.field final synthetic a:Lcom/peel/i/r;


# direct methods
.method constructor <init>(Lcom/peel/i/r;)V
    .locals 0

    .prologue
    .line 887
    iput-object p1, p0, Lcom/peel/i/s;->a:Lcom/peel/i/r;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(I)V
    .locals 9

    .prologue
    const/4 v8, 0x2

    const/16 v7, 0x8

    const/4 v6, 0x0

    .line 909
    iget-object v0, p0, Lcom/peel/i/s;->a:Lcom/peel/i/r;

    iget-object v0, v0, Lcom/peel/i/r;->a:Lcom/peel/i/a;

    invoke-static {v0, p1}, Lcom/peel/i/a;->c(Lcom/peel/i/a;I)I

    .line 910
    iget-object v0, p0, Lcom/peel/i/s;->a:Lcom/peel/i/r;

    iget-object v1, v0, Lcom/peel/i/r;->a:Lcom/peel/i/a;

    iget-object v0, p0, Lcom/peel/i/s;->a:Lcom/peel/i/r;

    iget-object v0, v0, Lcom/peel/i/r;->a:Lcom/peel/i/a;

    invoke-static {v0}, Lcom/peel/i/a;->x(Lcom/peel/i/a;)Ljava/util/ArrayList;

    move-result-object v0

    iget-object v2, p0, Lcom/peel/i/s;->a:Lcom/peel/i/r;

    iget-object v2, v2, Lcom/peel/i/r;->a:Lcom/peel/i/a;

    invoke-static {v2}, Lcom/peel/i/a;->M(Lcom/peel/i/a;)I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    const-string/jumbo v2, "funName"

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v1, v0}, Lcom/peel/i/a;->b(Lcom/peel/i/a;Ljava/lang/String;)Ljava/lang/String;

    .line 911
    iget-object v0, p0, Lcom/peel/i/s;->a:Lcom/peel/i/r;

    iget-object v1, v0, Lcom/peel/i/r;->a:Lcom/peel/i/a;

    iget-object v0, p0, Lcom/peel/i/s;->a:Lcom/peel/i/r;

    iget-object v0, v0, Lcom/peel/i/r;->a:Lcom/peel/i/a;

    invoke-static {v0}, Lcom/peel/i/a;->x(Lcom/peel/i/a;)Ljava/util/ArrayList;

    move-result-object v0

    iget-object v2, p0, Lcom/peel/i/s;->a:Lcom/peel/i/r;

    iget-object v2, v2, Lcom/peel/i/r;->a:Lcom/peel/i/a;

    invoke-static {v2}, Lcom/peel/i/a;->M(Lcom/peel/i/a;)I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    const-string/jumbo v2, "codesetid"

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v1, v0}, Lcom/peel/i/a;->d(Lcom/peel/i/a;I)I

    .line 912
    iget-object v0, p0, Lcom/peel/i/s;->a:Lcom/peel/i/r;

    iget-object v0, v0, Lcom/peel/i/r;->a:Lcom/peel/i/a;

    invoke-static {v0}, Lcom/peel/i/a;->v(Lcom/peel/i/a;)Lcom/peel/control/h;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v1

    iget-object v0, p0, Lcom/peel/i/s;->a:Lcom/peel/i/r;

    iget-object v0, v0, Lcom/peel/i/r;->a:Lcom/peel/i/a;

    invoke-static {v0}, Lcom/peel/i/a;->c(Lcom/peel/i/a;)Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Lcom/peel/i/s;->a:Lcom/peel/i/r;

    iget-object v0, v0, Lcom/peel/i/r;->a:Lcom/peel/i/a;

    invoke-static {v0}, Lcom/peel/i/a;->x(Lcom/peel/i/a;)Ljava/util/ArrayList;

    move-result-object v0

    iget-object v3, p0, Lcom/peel/i/s;->a:Lcom/peel/i/r;

    iget-object v3, v3, Lcom/peel/i/r;->a:Lcom/peel/i/a;

    invoke-static {v3}, Lcom/peel/i/a;->M(Lcom/peel/i/a;)I

    move-result v3

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    invoke-virtual {v1, v2, v0}, Lcom/peel/data/g;->a(Ljava/lang/String;Ljava/util/Map;)V

    .line 914
    invoke-static {}, Lcom/peel/i/a;->c()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/peel/i/s;->a:Lcom/peel/i/r;

    iget-object v2, v2, Lcom/peel/i/r;->a:Lcom/peel/i/a;

    invoke-static {v2}, Lcom/peel/i/a;->b(Lcom/peel/i/a;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " codeIdx:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/peel/i/s;->a:Lcom/peel/i/r;

    iget-object v2, v2, Lcom/peel/i/r;->a:Lcom/peel/i/a;

    invoke-static {v2}, Lcom/peel/i/a;->M(Lcom/peel/i/a;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "/codesetId:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/peel/i/s;->a:Lcom/peel/i/r;

    iget-object v2, v2, Lcom/peel/i/r;->a:Lcom/peel/i/a;

    invoke-static {v2}, Lcom/peel/i/a;->A(Lcom/peel/i/a;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 916
    iget-object v0, p0, Lcom/peel/i/s;->a:Lcom/peel/i/r;

    iget-object v0, v0, Lcom/peel/i/r;->a:Lcom/peel/i/a;

    invoke-static {v0}, Lcom/peel/i/a;->z(Lcom/peel/i/a;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 917
    iget-object v0, p0, Lcom/peel/i/s;->a:Lcom/peel/i/r;

    iget-object v0, v0, Lcom/peel/i/r;->a:Lcom/peel/i/a;

    invoke-static {v0}, Lcom/peel/i/a;->F(Lcom/peel/i/a;)Landroid/widget/RelativeLayout;

    move-result-object v0

    invoke-virtual {v0, v7}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 918
    iget-object v0, p0, Lcom/peel/i/s;->a:Lcom/peel/i/r;

    iget-object v0, v0, Lcom/peel/i/r;->a:Lcom/peel/i/a;

    invoke-static {v0}, Lcom/peel/i/a;->G(Lcom/peel/i/a;)Landroid/widget/RelativeLayout;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 919
    iget-object v0, p0, Lcom/peel/i/s;->a:Lcom/peel/i/r;

    iget-object v0, v0, Lcom/peel/i/r;->a:Lcom/peel/i/a;

    invoke-static {v0}, Lcom/peel/i/a;->H(Lcom/peel/i/a;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/i/s;->a:Lcom/peel/i/r;

    iget-object v1, v1, Lcom/peel/i/r;->a:Lcom/peel/i/a;

    sget v2, Lcom/peel/ui/ft;->testing_key_power:I

    new-array v3, v8, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/peel/i/s;->a:Lcom/peel/i/r;

    iget-object v4, v4, Lcom/peel/i/r;->a:Lcom/peel/i/a;

    invoke-static {v4}, Lcom/peel/i/a;->a(Lcom/peel/i/a;)Lcom/peel/f/a;

    move-result-object v4

    invoke-virtual {v4}, Lcom/peel/f/a;->b()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v6

    const/4 v4, 0x1

    iget-object v5, p0, Lcom/peel/i/s;->a:Lcom/peel/i/r;

    iget-object v5, v5, Lcom/peel/i/r;->a:Lcom/peel/i/a;

    invoke-static {v5}, Lcom/peel/i/a;->b(Lcom/peel/i/a;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3}, Lcom/peel/i/a;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 920
    iget-object v0, p0, Lcom/peel/i/s;->a:Lcom/peel/i/r;

    iget-object v0, v0, Lcom/peel/i/r;->a:Lcom/peel/i/a;

    invoke-static {v0}, Lcom/peel/i/a;->N(Lcom/peel/i/a;)V

    .line 922
    iget-object v0, p0, Lcom/peel/i/s;->a:Lcom/peel/i/r;

    iget-object v0, v0, Lcom/peel/i/r;->a:Lcom/peel/i/a;

    invoke-static {v0}, Lcom/peel/i/a;->y(Lcom/peel/i/a;)Lcom/peel/widget/TestBtnViewPager;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "btnView"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/peel/i/s;->a:Lcom/peel/i/r;

    iget-object v2, v2, Lcom/peel/i/r;->a:Lcom/peel/i/a;

    invoke-static {v2}, Lcom/peel/i/a;->y(Lcom/peel/i/a;)Lcom/peel/widget/TestBtnViewPager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/peel/widget/TestBtnViewPager;->getCurrentItem()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/peel/widget/TestBtnViewPager;->findViewWithTag(Ljava/lang/Object;)Landroid/view/View;

    move-result-object v0

    .line 924
    iget-object v1, p0, Lcom/peel/i/s;->a:Lcom/peel/i/r;

    iget-object v1, v1, Lcom/peel/i/r;->a:Lcom/peel/i/a;

    invoke-static {v1}, Lcom/peel/i/a;->h(Lcom/peel/i/a;)I

    move-result v1

    if-ne v1, v8, :cond_0

    .line 925
    sget v1, Lcom/peel/ui/fp;->test_other_btn_large_view:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/view/View;->setVisibility(I)V

    .line 930
    :goto_0
    iget-object v1, p0, Lcom/peel/i/s;->a:Lcom/peel/i/r;

    iget-object v1, v1, Lcom/peel/i/r;->a:Lcom/peel/i/a;

    invoke-static {v1}, Lcom/peel/i/a;->y(Lcom/peel/i/a;)Lcom/peel/widget/TestBtnViewPager;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "btnView"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/peel/i/s;->a:Lcom/peel/i/r;

    iget-object v3, v3, Lcom/peel/i/r;->a:Lcom/peel/i/a;

    invoke-static {v3}, Lcom/peel/i/a;->L(Lcom/peel/i/a;)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/peel/widget/TestBtnViewPager;->findViewWithTag(Ljava/lang/Object;)Landroid/view/View;

    move-result-object v1

    .line 932
    iget-object v2, p0, Lcom/peel/i/s;->a:Lcom/peel/i/r;

    iget-object v2, v2, Lcom/peel/i/r;->a:Lcom/peel/i/a;

    invoke-static {v2}, Lcom/peel/i/a;->h(Lcom/peel/i/a;)I

    move-result v2

    if-ne v2, v8, :cond_1

    .line 933
    sget v2, Lcom/peel/ui/fp;->test_other_btn_large_view:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v6}, Landroid/view/View;->setVisibility(I)V

    .line 934
    sget v2, Lcom/peel/ui/fp;->test_other_btn_small_view:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v7}, Landroid/view/View;->setVisibility(I)V

    .line 935
    sget v0, Lcom/peel/ui/fp;->test_other_btn_large_view:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v7}, Landroid/view/View;->setVisibility(I)V

    .line 936
    sget v0, Lcom/peel/ui/fp;->test_other_btn_small_view:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    .line 943
    :goto_1
    iget-object v0, p0, Lcom/peel/i/s;->a:Lcom/peel/i/r;

    iget-object v0, v0, Lcom/peel/i/r;->a:Lcom/peel/i/a;

    invoke-static {v0, p1}, Lcom/peel/i/a;->e(Lcom/peel/i/a;I)I

    .line 944
    return-void

    .line 927
    :cond_0
    sget v1, Lcom/peel/ui/fp;->test_pw_btn_large_view:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 938
    :cond_1
    sget v2, Lcom/peel/ui/fp;->test_pw_btn_large_view:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v6}, Landroid/view/View;->setVisibility(I)V

    .line 939
    sget v2, Lcom/peel/ui/fp;->test_pw_btn_small_view:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v7}, Landroid/view/View;->setVisibility(I)V

    .line 940
    sget v0, Lcom/peel/ui/fp;->test_pw_btn_large_view:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v7}, Landroid/view/View;->setVisibility(I)V

    .line 941
    sget v0, Lcom/peel/ui/fp;->test_pw_btn_small_view:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1
.end method

.method public a(IFI)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x4

    const/4 v2, 0x0

    .line 890
    iget-object v0, p0, Lcom/peel/i/s;->a:Lcom/peel/i/r;

    iget-object v0, v0, Lcom/peel/i/r;->a:Lcom/peel/i/a;

    invoke-static {v0}, Lcom/peel/i/a;->y(Lcom/peel/i/a;)Lcom/peel/widget/TestBtnViewPager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/widget/TestBtnViewPager;->getCurrentItem()I

    move-result v0

    .line 891
    if-nez v0, :cond_0

    iget-object v1, p0, Lcom/peel/i/s;->a:Lcom/peel/i/r;

    iget-object v1, v1, Lcom/peel/i/r;->a:Lcom/peel/i/a;

    invoke-static {v1}, Lcom/peel/i/a;->x(Lcom/peel/i/a;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-le v1, v4, :cond_0

    .line 892
    iget-object v0, p0, Lcom/peel/i/s;->a:Lcom/peel/i/r;

    iget-object v0, v0, Lcom/peel/i/r;->a:Lcom/peel/i/a;

    invoke-static {v0}, Lcom/peel/i/a;->I(Lcom/peel/i/a;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setVisibility(I)V

    .line 893
    iget-object v0, p0, Lcom/peel/i/s;->a:Lcom/peel/i/r;

    iget-object v0, v0, Lcom/peel/i/r;->a:Lcom/peel/i/a;

    invoke-static {v0}, Lcom/peel/i/a;->J(Lcom/peel/i/a;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 904
    :goto_0
    return-void

    .line 894
    :cond_0
    iget-object v1, p0, Lcom/peel/i/s;->a:Lcom/peel/i/r;

    iget-object v1, v1, Lcom/peel/i/r;->a:Lcom/peel/i/a;

    invoke-static {v1}, Lcom/peel/i/a;->x(Lcom/peel/i/a;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-ne v0, v1, :cond_1

    iget-object v1, p0, Lcom/peel/i/s;->a:Lcom/peel/i/r;

    iget-object v1, v1, Lcom/peel/i/r;->a:Lcom/peel/i/a;

    invoke-static {v1}, Lcom/peel/i/a;->x(Lcom/peel/i/a;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-le v1, v4, :cond_1

    .line 895
    iget-object v0, p0, Lcom/peel/i/s;->a:Lcom/peel/i/r;

    iget-object v0, v0, Lcom/peel/i/r;->a:Lcom/peel/i/a;

    invoke-static {v0}, Lcom/peel/i/a;->I(Lcom/peel/i/a;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 896
    iget-object v0, p0, Lcom/peel/i/s;->a:Lcom/peel/i/r;

    iget-object v0, v0, Lcom/peel/i/r;->a:Lcom/peel/i/a;

    invoke-static {v0}, Lcom/peel/i/a;->J(Lcom/peel/i/a;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_0

    .line 897
    :cond_1
    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/peel/i/s;->a:Lcom/peel/i/r;

    iget-object v0, v0, Lcom/peel/i/r;->a:Lcom/peel/i/a;

    invoke-static {v0}, Lcom/peel/i/a;->x(Lcom/peel/i/a;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ne v0, v4, :cond_2

    .line 898
    iget-object v0, p0, Lcom/peel/i/s;->a:Lcom/peel/i/r;

    iget-object v0, v0, Lcom/peel/i/r;->a:Lcom/peel/i/a;

    invoke-static {v0}, Lcom/peel/i/a;->I(Lcom/peel/i/a;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setVisibility(I)V

    .line 899
    iget-object v0, p0, Lcom/peel/i/s;->a:Lcom/peel/i/r;

    iget-object v0, v0, Lcom/peel/i/r;->a:Lcom/peel/i/a;

    invoke-static {v0}, Lcom/peel/i/a;->J(Lcom/peel/i/a;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_0

    .line 901
    :cond_2
    iget-object v0, p0, Lcom/peel/i/s;->a:Lcom/peel/i/r;

    iget-object v0, v0, Lcom/peel/i/r;->a:Lcom/peel/i/a;

    invoke-static {v0}, Lcom/peel/i/a;->I(Lcom/peel/i/a;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 902
    iget-object v0, p0, Lcom/peel/i/s;->a:Lcom/peel/i/r;

    iget-object v0, v0, Lcom/peel/i/r;->a:Lcom/peel/i/a;

    invoke-static {v0}, Lcom/peel/i/a;->J(Lcom/peel/i/a;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_0
.end method

.method public b(I)V
    .locals 0

    .prologue
    .line 949
    return-void
.end method
