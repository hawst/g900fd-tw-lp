.class public Lcom/peel/i/al;
.super Lcom/peel/d/u;


# instance fields
.field private aj:Lcom/peel/widget/ag;

.field private ak:Landroid/widget/ListView;

.field private al:Z

.field private am:Landroid/app/ProgressDialog;

.field private an:Landroid/widget/TextView;

.field private ao:Landroid/widget/TextView;

.field private ap:Landroid/widget/ProgressBar;

.field private aq:I

.field private ar:I

.field private e:Lcom/peel/control/RoomControl;

.field private f:Lcom/peel/i/a/e;

.field private g:Lcom/peel/control/h;

.field private h:[Lcom/peel/control/h;

.field private i:Lcom/peel/widget/ag;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 56
    invoke-direct {p0}, Lcom/peel/d/u;-><init>()V

    return-void
.end method

.method static synthetic A(Lcom/peel/i/al;)I
    .locals 1

    .prologue
    .line 56
    iget v0, p0, Lcom/peel/i/al;->ar:I

    return v0
.end method

.method static synthetic B(Lcom/peel/i/al;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/peel/i/al;->ao:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic C(Lcom/peel/i/al;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/peel/i/al;->a:Ljava/lang/String;

    return-object v0
.end method

.method private S()V
    .locals 3

    .prologue
    .line 149
    const-string/jumbo v0, "Looking up DIRECTV boxes\u2026"

    invoke-direct {p0, v0}, Lcom/peel/i/al;->a(Ljava/lang/String;)V

    .line 150
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "lookupDtv"

    new-instance v2, Lcom/peel/i/ar;

    invoke-direct {v2, p0}, Lcom/peel/i/ar;-><init>(Lcom/peel/i/al;)V

    invoke-static {v0, v1, v2}, Lcom/peel/util/i;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    .line 186
    return-void
.end method

.method private T()V
    .locals 3

    .prologue
    .line 189
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "showDtvNotFoundDialog"

    new-instance v2, Lcom/peel/i/au;

    invoke-direct {v2, p0}, Lcom/peel/i/au;-><init>(Lcom/peel/i/al;)V

    invoke-static {v0, v1, v2}, Lcom/peel/util/i;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/lang/Runnable;

    .line 243
    return-void
.end method

.method private U()V
    .locals 3

    .prologue
    .line 246
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "showDtvSelectionList"

    new-instance v2, Lcom/peel/i/bb;

    invoke-direct {v2, p0}, Lcom/peel/i/bb;-><init>(Lcom/peel/i/al;)V

    invoke-static {v0, v1, v2}, Lcom/peel/util/i;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/lang/Runnable;

    .line 371
    return-void
.end method

.method private V()V
    .locals 1

    .prologue
    .line 480
    iget-object v0, p0, Lcom/peel/i/al;->am:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/peel/i/al;->am:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/peel/i/al;->am:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 481
    :cond_0
    return-void
.end method

.method static synthetic a(Lcom/peel/i/al;I)I
    .locals 0

    .prologue
    .line 56
    iput p1, p0, Lcom/peel/i/al;->aq:I

    return p1
.end method

.method static synthetic a(Lcom/peel/i/al;Landroid/widget/ListView;)Landroid/widget/ListView;
    .locals 0

    .prologue
    .line 56
    iput-object p1, p0, Lcom/peel/i/al;->ak:Landroid/widget/ListView;

    return-object p1
.end method

.method static synthetic a(Lcom/peel/i/al;Lcom/peel/control/h;)Lcom/peel/control/h;
    .locals 0

    .prologue
    .line 56
    iput-object p1, p0, Lcom/peel/i/al;->g:Lcom/peel/control/h;

    return-object p1
.end method

.method static synthetic a(Lcom/peel/i/al;Lcom/peel/i/a/e;)Lcom/peel/i/a/e;
    .locals 0

    .prologue
    .line 56
    iput-object p1, p0, Lcom/peel/i/al;->f:Lcom/peel/i/a/e;

    return-object p1
.end method

.method static synthetic a(Lcom/peel/i/al;Lcom/peel/widget/ag;)Lcom/peel/widget/ag;
    .locals 0

    .prologue
    .line 56
    iput-object p1, p0, Lcom/peel/i/al;->i:Lcom/peel/widget/ag;

    return-object p1
.end method

.method static synthetic a(Lcom/peel/i/al;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/peel/i/al;->a:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic a(Lcom/peel/i/al;Z)V
    .locals 0

    .prologue
    .line 56
    invoke-direct {p0, p1}, Lcom/peel/i/al;->a(Z)V

    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 471
    iget-object v0, p0, Lcom/peel/i/al;->am:Landroid/app/ProgressDialog;

    if-nez v0, :cond_0

    .line 472
    new-instance v0, Landroid/app/ProgressDialog;

    invoke-virtual {p0}, Lcom/peel/i/al;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    const/4 v2, 0x4

    invoke-direct {v0, v1, v2}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/peel/i/al;->am:Landroid/app/ProgressDialog;

    .line 473
    iget-object v0, p0, Lcom/peel/i/al;->am:Landroid/app/ProgressDialog;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 475
    :cond_0
    iget-object v0, p0, Lcom/peel/i/al;->am:Landroid/app/ProgressDialog;

    invoke-virtual {v0, p1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 476
    iget-object v0, p0, Lcom/peel/i/al;->am:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    .line 477
    return-void
.end method

.method private a(Z)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 379
    iget-boolean v0, p0, Lcom/peel/i/al;->al:Z

    if-nez v0, :cond_0

    .line 380
    const-string/jumbo v0, "dtv"

    invoke-static {v0}, Lcom/peel/content/a;->c(Ljava/lang/String;)Lcom/peel/content/library/Library;

    move-result-object v0

    .line 381
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/peel/content/library/Library;->e()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/i/al;->e:Lcom/peel/control/RoomControl;

    invoke-virtual {v1}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/data/at;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/peel/content/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 385
    :cond_0
    if-eqz p1, :cond_1

    .line 386
    new-instance v1, Lcom/peel/content/library/DirecTVLibrary;

    iget-object v0, p0, Lcom/peel/i/al;->g:Lcom/peel/control/h;

    invoke-virtual {v0}, Lcom/peel/control/h;->d()Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/peel/i/al;->g:Lcom/peel/control/h;

    invoke-virtual {v2}, Lcom/peel/control/h;->d()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v0, v2}, Lcom/peel/content/library/DirecTVLibrary;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 387
    iget-object v0, p0, Lcom/peel/i/al;->e:Lcom/peel/control/RoomControl;

    invoke-virtual {v0}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/data/at;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/peel/content/a;->a(Lcom/peel/content/library/Library;Ljava/lang/String;)V

    .line 388
    iget-object v0, p0, Lcom/peel/i/al;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "\n\n########### adding dtv lib w/ dtv device id: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/peel/i/al;->g:Lcom/peel/control/h;

    invoke-virtual {v3}, Lcom/peel/control/h;->d()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 390
    const-string/jumbo v0, "Loading DIRECTV recordings from your DIRECTV box\u2026 This might take a few minutes depending on how many recordings you have on the box. Please wait until this loading finishes."

    invoke-direct {p0, v0}, Lcom/peel/i/al;->a(Ljava/lang/String;)V

    .line 392
    invoke-virtual {p0}, Lcom/peel/i/al;->v()Landroid/view/View;

    move-result-object v0

    sget v2, Lcom/peel/ui/fp;->progress_bar:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/peel/i/al;->ap:Landroid/widget/ProgressBar;

    .line 393
    invoke-virtual {p0}, Lcom/peel/i/al;->v()Landroid/view/View;

    move-result-object v0

    sget v2, Lcom/peel/ui/fp;->progress_desc:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/peel/i/al;->an:Landroid/widget/TextView;

    .line 394
    invoke-virtual {p0}, Lcom/peel/i/al;->v()Landroid/view/View;

    move-result-object v0

    sget v2, Lcom/peel/ui/fp;->progress_msg:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/peel/i/al;->ao:Landroid/widget/TextView;

    .line 397
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 398
    const-string/jumbo v2, "path"

    const-string/jumbo v3, "listing"

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 399
    new-instance v2, Lcom/peel/i/bj;

    invoke-direct {v2, p0, v4}, Lcom/peel/i/bj;-><init>(Lcom/peel/i/al;I)V

    new-instance v3, Lcom/peel/i/bl;

    invoke-direct {v3, p0, v4}, Lcom/peel/i/bl;-><init>(Lcom/peel/i/al;I)V

    invoke-virtual {v1, v0, v2, v3}, Lcom/peel/content/library/DirecTVLibrary;->a(Landroid/os/Bundle;Lcom/peel/util/t;Lcom/peel/util/t;)V

    .line 462
    :goto_0
    return-void

    .line 446
    :cond_1
    iget-boolean v0, p0, Lcom/peel/i/al;->al:Z

    if-eqz v0, :cond_2

    .line 447
    invoke-virtual {p0}, Lcom/peel/i/al;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    const-class v1, Lcom/peel/i/cg;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/peel/d/e;->a(Landroid/support/v4/app/ae;Ljava/lang/String;Landroid/os/Bundle;)V

    goto :goto_0

    .line 449
    :cond_2
    invoke-direct {p0}, Lcom/peel/i/al;->V()V

    .line 450
    new-instance v0, Lcom/peel/widget/ag;

    invoke-virtual {p0}, Lcom/peel/i/al;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/peel/widget/ag;-><init>(Landroid/content/Context;)V

    sget v1, Lcom/peel/ui/ft;->label_success:I

    .line 451
    invoke-virtual {v0, v1}, Lcom/peel/widget/ag;->a(I)Lcom/peel/widget/ag;

    move-result-object v0

    sget v1, Lcom/peel/ui/ft;->desc_dtv_setup_successful:I

    .line 452
    invoke-virtual {v0, v1}, Lcom/peel/widget/ag;->b(I)Lcom/peel/widget/ag;

    move-result-object v0

    sget v1, Lcom/peel/ui/ft;->done:I

    new-instance v2, Lcom/peel/i/an;

    invoke-direct {v2, p0}, Lcom/peel/i/an;-><init>(Lcom/peel/i/al;)V

    .line 453
    invoke-virtual {v0, v1, v2}, Lcom/peel/widget/ag;->a(ILandroid/content/DialogInterface$OnClickListener;)Lcom/peel/widget/ag;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/i/al;->aj:Lcom/peel/widget/ag;

    .line 459
    iget-object v0, p0, Lcom/peel/i/al;->aj:Lcom/peel/widget/ag;

    invoke-virtual {v0}, Lcom/peel/widget/ag;->show()V

    goto :goto_0
.end method

.method static synthetic a(Lcom/peel/i/al;[Lcom/peel/control/h;)[Lcom/peel/control/h;
    .locals 0

    .prologue
    .line 56
    iput-object p1, p0, Lcom/peel/i/al;->h:[Lcom/peel/control/h;

    return-object p1
.end method

.method static synthetic b(Lcom/peel/i/al;I)I
    .locals 0

    .prologue
    .line 56
    iput p1, p0, Lcom/peel/i/al;->ar:I

    return p1
.end method

.method static synthetic b(Lcom/peel/i/al;Lcom/peel/widget/ag;)Lcom/peel/widget/ag;
    .locals 0

    .prologue
    .line 56
    iput-object p1, p0, Lcom/peel/i/al;->aj:Lcom/peel/widget/ag;

    return-object p1
.end method

.method static synthetic b(Lcom/peel/i/al;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/peel/i/al;->a:Ljava/lang/String;

    return-object v0
.end method

.method private c()V
    .locals 3

    .prologue
    .line 106
    invoke-virtual {p0}, Lcom/peel/i/al;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    invoke-static {v0}, Lcom/peel/util/bx;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 108
    invoke-direct {p0}, Lcom/peel/i/al;->S()V

    .line 145
    :goto_0
    return-void

    .line 110
    :cond_0
    iget-boolean v0, p0, Lcom/peel/i/al;->al:Z

    if-eqz v0, :cond_1

    .line 111
    new-instance v0, Lcom/peel/widget/ag;

    invoke-virtual {p0}, Lcom/peel/i/al;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/peel/widget/ag;-><init>(Landroid/content/Context;)V

    sget v1, Lcom/peel/ui/ft;->title_wifi_unavailable:I

    .line 112
    invoke-virtual {v0, v1}, Lcom/peel/widget/ag;->a(I)Lcom/peel/widget/ag;

    move-result-object v0

    sget v1, Lcom/peel/ui/ft;->msg_wifi_unavailable:I

    .line 113
    invoke-virtual {v0, v1}, Lcom/peel/widget/ag;->b(I)Lcom/peel/widget/ag;

    move-result-object v0

    sget v1, Lcom/peel/ui/ft;->label_settings:I

    new-instance v2, Lcom/peel/i/ao;

    invoke-direct {v2, p0}, Lcom/peel/i/ao;-><init>(Lcom/peel/i/al;)V

    .line 114
    invoke-virtual {v0, v1, v2}, Lcom/peel/widget/ag;->a(ILandroid/content/DialogInterface$OnClickListener;)Lcom/peel/widget/ag;

    move-result-object v0

    sget v1, Lcom/peel/ui/ft;->nothanks:I

    new-instance v2, Lcom/peel/i/am;

    invoke-direct {v2, p0}, Lcom/peel/i/am;-><init>(Lcom/peel/i/al;)V

    .line 120
    invoke-virtual {v0, v1, v2}, Lcom/peel/widget/ag;->c(ILandroid/content/DialogInterface$OnClickListener;)Lcom/peel/widget/ag;

    move-result-object v0

    .line 126
    invoke-virtual {v0}, Lcom/peel/widget/ag;->show()V

    goto :goto_0

    .line 128
    :cond_1
    new-instance v0, Lcom/peel/widget/ag;

    invoke-virtual {p0}, Lcom/peel/i/al;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/peel/widget/ag;-><init>(Landroid/content/Context;)V

    sget v1, Lcom/peel/ui/ft;->title_wifi_unavailable:I

    .line 129
    invoke-virtual {v0, v1}, Lcom/peel/widget/ag;->a(I)Lcom/peel/widget/ag;

    move-result-object v0

    sget v1, Lcom/peel/ui/ft;->msg_wifi_unavailable:I

    .line 130
    invoke-virtual {v0, v1}, Lcom/peel/widget/ag;->b(I)Lcom/peel/widget/ag;

    move-result-object v0

    sget v1, Lcom/peel/ui/ft;->label_settings:I

    new-instance v2, Lcom/peel/i/aq;

    invoke-direct {v2, p0}, Lcom/peel/i/aq;-><init>(Lcom/peel/i/al;)V

    .line 131
    invoke-virtual {v0, v1, v2}, Lcom/peel/widget/ag;->a(ILandroid/content/DialogInterface$OnClickListener;)Lcom/peel/widget/ag;

    move-result-object v0

    sget v1, Lcom/peel/ui/ft;->label_try_later:I

    new-instance v2, Lcom/peel/i/ap;

    invoke-direct {v2, p0}, Lcom/peel/i/ap;-><init>(Lcom/peel/i/al;)V

    .line 137
    invoke-virtual {v0, v1, v2}, Lcom/peel/widget/ag;->c(ILandroid/content/DialogInterface$OnClickListener;)Lcom/peel/widget/ag;

    move-result-object v0

    .line 142
    invoke-virtual {v0}, Lcom/peel/widget/ag;->show()V

    goto :goto_0
.end method

.method static synthetic c(Lcom/peel/i/al;)[Lcom/peel/control/h;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/peel/i/al;->h:[Lcom/peel/control/h;

    return-object v0
.end method

.method static synthetic d(Lcom/peel/i/al;)V
    .locals 0

    .prologue
    .line 56
    invoke-direct {p0}, Lcom/peel/i/al;->U()V

    return-void
.end method

.method static synthetic e(Lcom/peel/i/al;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/peel/i/al;->a:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic f(Lcom/peel/i/al;)V
    .locals 0

    .prologue
    .line 56
    invoke-direct {p0}, Lcom/peel/i/al;->T()V

    return-void
.end method

.method static synthetic g(Lcom/peel/i/al;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/peel/i/al;->a:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic h(Lcom/peel/i/al;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/peel/i/al;->a:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic i(Lcom/peel/i/al;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/peel/i/al;->a:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic j(Lcom/peel/i/al;)V
    .locals 0

    .prologue
    .line 56
    invoke-direct {p0}, Lcom/peel/i/al;->V()V

    return-void
.end method

.method static synthetic k(Lcom/peel/i/al;)Lcom/peel/widget/ag;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/peel/i/al;->i:Lcom/peel/widget/ag;

    return-object v0
.end method

.method static synthetic l(Lcom/peel/i/al;)Z
    .locals 1

    .prologue
    .line 56
    iget-boolean v0, p0, Lcom/peel/i/al;->al:Z

    return v0
.end method

.method static synthetic m(Lcom/peel/i/al;)V
    .locals 0

    .prologue
    .line 56
    invoke-direct {p0}, Lcom/peel/i/al;->S()V

    return-void
.end method

.method static synthetic n(Lcom/peel/i/al;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/peel/i/al;->a:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic o(Lcom/peel/i/al;)Lcom/peel/i/a/e;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/peel/i/al;->f:Lcom/peel/i/a/e;

    return-object v0
.end method

.method static synthetic p(Lcom/peel/i/al;)Landroid/widget/ListView;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/peel/i/al;->ak:Landroid/widget/ListView;

    return-object v0
.end method

.method static synthetic q(Lcom/peel/i/al;)Lcom/peel/control/h;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/peel/i/al;->g:Lcom/peel/control/h;

    return-object v0
.end method

.method static synthetic r(Lcom/peel/i/al;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/peel/i/al;->a:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic s(Lcom/peel/i/al;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/peel/i/al;->a:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic t(Lcom/peel/i/al;)Lcom/peel/widget/ag;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/peel/i/al;->aj:Lcom/peel/widget/ag;

    return-object v0
.end method

.method static synthetic u(Lcom/peel/i/al;)Lcom/peel/control/RoomControl;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/peel/i/al;->e:Lcom/peel/control/RoomControl;

    return-object v0
.end method

.method static synthetic v(Lcom/peel/i/al;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/peel/i/al;->a:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic w(Lcom/peel/i/al;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/peel/i/al;->a:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic x(Lcom/peel/i/al;)I
    .locals 1

    .prologue
    .line 56
    iget v0, p0, Lcom/peel/i/al;->aq:I

    return v0
.end method

.method static synthetic y(Lcom/peel/i/al;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/peel/i/al;->an:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic z(Lcom/peel/i/al;)Landroid/widget/ProgressBar;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/peel/i/al;->ap:Landroid/widget/ProgressBar;

    return-object v0
.end method


# virtual methods
.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3

    .prologue
    .line 81
    sget v0, Lcom/peel/ui/fq;->dtv_device_list_view:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 82
    sget v0, Lcom/peel/ui/fp;->device_list:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/peel/i/al;->ak:Landroid/widget/ListView;

    .line 83
    sget v0, Lcom/peel/ui/fp;->ip_device_list_panel:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 84
    return-object v1
.end method

.method public a_(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 71
    invoke-super {p0, p1}, Lcom/peel/d/u;->a_(Landroid/os/Bundle;)V

    .line 72
    sget-object v1, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v1}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v1

    iput-object v1, p0, Lcom/peel/i/al;->e:Lcom/peel/control/RoomControl;

    .line 73
    iget-object v1, p0, Lcom/peel/i/al;->c:Landroid/os/Bundle;

    const-string/jumbo v2, "from_setup"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lcom/peel/i/al;->al:Z

    .line 75
    invoke-static {}, Lcom/peel/util/a/f;->a()Lcom/peel/util/a/f;

    move-result-object v1

    sget-object v2, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v2}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v2

    if-nez v2, :cond_0

    :goto_0
    const/16 v2, 0x14b4

    const/16 v3, 0x7d5

    invoke-virtual {v1, v0, v2, v3}, Lcom/peel/util/a/f;->a(III)V

    .line 77
    return-void

    .line 75
    :cond_0
    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v0}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/data/at;->f()I

    move-result v0

    goto :goto_0
.end method

.method public d(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 89
    invoke-super {p0, p1}, Lcom/peel/d/u;->d(Landroid/os/Bundle;)V

    .line 90
    iget-object v0, p0, Lcom/peel/i/al;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "category"

    sget v2, Lcom/peel/ui/ft;->title_dtv_setup:I

    invoke-virtual {p0, v2}, Lcom/peel/i/al;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 91
    return-void
.end method

.method public f()V
    .locals 3

    .prologue
    .line 94
    invoke-super {p0}, Lcom/peel/d/u;->f()V

    .line 95
    invoke-virtual {p0}, Lcom/peel/i/al;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    const-string/jumbo v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/ae;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 96
    invoke-virtual {p0}, Lcom/peel/i/al;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/ae;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 97
    iget-object v0, p0, Lcom/peel/i/al;->aj:Lcom/peel/widget/ag;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/peel/i/al;->aj:Lcom/peel/widget/ag;

    invoke-virtual {v0}, Lcom/peel/widget/ag;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 99
    iget-object v0, p0, Lcom/peel/i/al;->aj:Lcom/peel/widget/ag;

    invoke-virtual {v0}, Lcom/peel/widget/ag;->dismiss()V

    .line 101
    :cond_0
    iget-object v0, p0, Lcom/peel/i/al;->i:Lcom/peel/widget/ag;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/peel/i/al;->i:Lcom/peel/widget/ag;

    invoke-virtual {v0}, Lcom/peel/widget/ag;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 102
    iget-object v0, p0, Lcom/peel/i/al;->i:Lcom/peel/widget/ag;

    invoke-virtual {v0}, Lcom/peel/widget/ag;->dismiss()V

    .line 104
    :cond_1
    return-void
.end method

.method public w()V
    .locals 0

    .prologue
    .line 466
    invoke-super {p0}, Lcom/peel/d/u;->x()V

    .line 467
    invoke-direct {p0}, Lcom/peel/i/al;->c()V

    .line 468
    return-void
.end method
