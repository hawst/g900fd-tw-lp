.class Lcom/peel/i/dk;
.super Landroid/widget/ArrayAdapter;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Landroid/util/Pair",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/String;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/peel/i/di;


# direct methods
.method constructor <init>(Lcom/peel/i/di;Landroid/content/Context;ILjava/util/List;)V
    .locals 0

    .prologue
    .line 463
    iput-object p1, p0, Lcom/peel/i/dk;->a:Lcom/peel/i/di;

    invoke-direct {p0, p2, p3, p4}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    return-void
.end method


# virtual methods
.method public getDropDownView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    .prologue
    .line 479
    invoke-super {p0, p1, p2, p3}, Landroid/widget/ArrayAdapter;->getDropDownView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 480
    const v1, 0x1020014

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, p1, v0, p3}, Lcom/peel/i/dk;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 5

    .prologue
    .line 466
    iget-object v0, p0, Lcom/peel/i/dk;->a:Lcom/peel/i/di;

    iget-object v1, v0, Lcom/peel/i/di;->b:Lcom/peel/i/cr;

    if-nez p2, :cond_0

    iget-object v0, p0, Lcom/peel/i/dk;->a:Lcom/peel/i/di;

    iget-object v0, v0, Lcom/peel/i/di;->b:Lcom/peel/i/cr;

    invoke-static {v0}, Lcom/peel/i/cr;->h(Lcom/peel/i/cr;)Landroid/view/LayoutInflater;

    move-result-object v2

    sget v3, Lcom/peel/ui/fq;->region_spinner_list_item:I

    iget-object v0, p0, Lcom/peel/i/dk;->a:Lcom/peel/i/di;

    iget-object v0, v0, Lcom/peel/i/di;->b:Lcom/peel/i/cr;

    invoke-static {v0}, Lcom/peel/i/cr;->o(Lcom/peel/i/cr;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v0, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    :goto_0
    check-cast v0, Landroid/widget/TextView;

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lcom/peel/i/cr;->e:Landroid/widget/TextView;

    .line 467
    invoke-virtual {p0, p1}, Lcom/peel/i/dk;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    .line 468
    iget-object v1, p0, Lcom/peel/i/dk;->a:Lcom/peel/i/di;

    iget-object v1, v1, Lcom/peel/i/di;->b:Lcom/peel/i/cr;

    iget-object v1, v1, Lcom/peel/i/cr;->e:Landroid/widget/TextView;

    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 469
    iget-object v0, p0, Lcom/peel/i/dk;->a:Lcom/peel/i/di;

    iget-object v0, v0, Lcom/peel/i/di;->b:Lcom/peel/i/cr;

    invoke-static {v0}, Lcom/peel/i/cr;->l(Lcom/peel/i/cr;)Lcom/peel/util/CustomSpinner;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/util/CustomSpinner;->getSelectedItemPosition()I

    move-result v0

    if-ne p1, v0, :cond_1

    .line 470
    iget-object v0, p0, Lcom/peel/i/dk;->a:Lcom/peel/i/di;

    iget-object v0, v0, Lcom/peel/i/di;->b:Lcom/peel/i/cr;

    iget-object v0, v0, Lcom/peel/i/cr;->e:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/peel/i/dk;->a:Lcom/peel/i/di;

    iget-object v1, v1, Lcom/peel/i/di;->b:Lcom/peel/i/cr;

    invoke-virtual {v1}, Lcom/peel/i/cr;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/ae;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/peel/ui/fm;->countrytextcolor_selector:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 474
    :goto_1
    iget-object v0, p0, Lcom/peel/i/dk;->a:Lcom/peel/i/di;

    iget-object v0, v0, Lcom/peel/i/di;->b:Lcom/peel/i/cr;

    iget-object v0, v0, Lcom/peel/i/cr;->e:Landroid/widget/TextView;

    return-object v0

    :cond_0
    move-object v0, p2

    .line 466
    goto :goto_0

    .line 472
    :cond_1
    iget-object v0, p0, Lcom/peel/i/dk;->a:Lcom/peel/i/di;

    iget-object v0, v0, Lcom/peel/i/di;->b:Lcom/peel/i/cr;

    iget-object v0, v0, Lcom/peel/i/cr;->e:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/peel/i/dk;->a:Lcom/peel/i/di;

    iget-object v1, v1, Lcom/peel/i/di;->b:Lcom/peel/i/cr;

    invoke-virtual {v1}, Lcom/peel/i/cr;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/ae;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/peel/ui/fm;->country_region_text_normal:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_1
.end method
