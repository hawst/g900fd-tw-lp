.class public Lcom/peel/i/a;
.super Lcom/peel/d/u;


# static fields
.field private static final e:Ljava/lang/String;


# instance fields
.field private aA:Z

.field private aB:Landroid/widget/ImageView;

.field private aC:Landroid/widget/EditText;

.field private aD:Landroid/widget/ImageView;

.field private aE:Landroid/widget/RelativeLayout;

.field private aF:Lcom/peel/ui/dg;

.field private aG:Lcom/peel/widget/ag;

.field private aH:Lcom/peel/widget/ag;

.field private aI:Landroid/widget/TextView;

.field private aJ:Landroid/widget/TextView;

.field private aK:Landroid/widget/TextView;

.field private aL:Landroid/widget/TextView;

.field private aM:Landroid/widget/RelativeLayout;

.field private aN:Landroid/widget/RelativeLayout;

.field private aO:Landroid/widget/ImageView;

.field private aP:Lcom/peel/widget/TestBtnViewPager;

.field private aQ:Landroid/widget/Button;

.field private aR:Landroid/widget/Button;

.field private aS:Lcom/peel/i/ad;

.field private aT:I

.field private aU:Landroid/widget/Button;

.field private aV:Landroid/widget/Button;

.field private aj:I

.field private ak:I

.field private al:Lcom/peel/control/a;

.field private am:Lcom/peel/f/a;

.field private an:Landroid/widget/ListView;

.field private ao:Landroid/widget/ListView;

.field private ap:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/peel/f/a;",
            ">;"
        }
    .end annotation
.end field

.field private aq:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/peel/f/a;",
            ">;"
        }
    .end annotation
.end field

.field private ar:Lcom/peel/control/h;

.field private as:Lcom/peel/control/h;

.field private at:Lcom/peel/control/h;

.field private au:Lcom/peel/widget/ag;

.field private av:Lcom/peel/widget/ag;

.field private aw:Lcom/peel/i/a/a;

.field private ax:Lcom/peel/i/a/a;

.field private ay:Ljava/lang/String;

.field private az:Ljava/lang/String;

.field private f:Landroid/widget/ViewFlipper;

.field private g:I

.field private h:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;>;"
        }
    .end annotation
.end field

.field private i:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 83
    const-class v0, Lcom/peel/i/a;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/peel/i/a;->e:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 82
    invoke-direct {p0}, Lcom/peel/d/u;-><init>()V

    .line 89
    const/4 v0, -0x1

    iput v0, p0, Lcom/peel/i/a;->g:I

    .line 100
    iput-boolean v1, p0, Lcom/peel/i/a;->aA:Z

    .line 105
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/peel/i/a;->aF:Lcom/peel/ui/dg;

    .line 118
    iput v1, p0, Lcom/peel/i/a;->aT:I

    .line 1260
    return-void
.end method

.method static synthetic A(Lcom/peel/i/a;)I
    .locals 1

    .prologue
    .line 82
    iget v0, p0, Lcom/peel/i/a;->i:I

    return v0
.end method

.method static synthetic B(Lcom/peel/i/a;)Landroid/widget/Button;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/peel/i/a;->aU:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic C(Lcom/peel/i/a;)Lcom/peel/control/a;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/peel/i/a;->al:Lcom/peel/control/a;

    return-object v0
.end method

.method static synthetic D(Lcom/peel/i/a;)Lcom/peel/control/h;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/peel/i/a;->at:Lcom/peel/control/h;

    return-object v0
.end method

.method static synthetic E(Lcom/peel/i/a;)Lcom/peel/control/h;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/peel/i/a;->as:Lcom/peel/control/h;

    return-object v0
.end method

.method static synthetic F(Lcom/peel/i/a;)Landroid/widget/RelativeLayout;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/peel/i/a;->aN:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method static synthetic G(Lcom/peel/i/a;)Landroid/widget/RelativeLayout;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/peel/i/a;->aM:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method static synthetic H(Lcom/peel/i/a;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/peel/i/a;->aI:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic I(Lcom/peel/i/a;)Landroid/widget/Button;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/peel/i/a;->aQ:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic J(Lcom/peel/i/a;)Landroid/widget/Button;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/peel/i/a;->aR:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic K(Lcom/peel/i/a;)Lcom/peel/i/ad;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/peel/i/a;->aS:Lcom/peel/i/ad;

    return-object v0
.end method

.method static synthetic L(Lcom/peel/i/a;)I
    .locals 1

    .prologue
    .line 82
    iget v0, p0, Lcom/peel/i/a;->aT:I

    return v0
.end method

.method static synthetic M(Lcom/peel/i/a;)I
    .locals 1

    .prologue
    .line 82
    iget v0, p0, Lcom/peel/i/a;->aj:I

    return v0
.end method

.method static synthetic N(Lcom/peel/i/a;)V
    .locals 0

    .prologue
    .line 82
    invoke-direct {p0}, Lcom/peel/i/a;->ad()V

    return-void
.end method

.method static synthetic O(Lcom/peel/i/a;)V
    .locals 0

    .prologue
    .line 82
    invoke-direct {p0}, Lcom/peel/i/a;->ae()V

    return-void
.end method

.method private S()V
    .locals 3

    .prologue
    .line 121
    iget-object v0, p0, Lcom/peel/i/a;->av:Lcom/peel/widget/ag;

    if-nez v0, :cond_0

    .line 122
    new-instance v0, Lcom/peel/widget/ag;

    invoke-virtual {p0}, Lcom/peel/i/a;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/peel/widget/ag;-><init>(Landroid/content/Context;)V

    sget v1, Lcom/peel/ui/ft;->warning:I

    invoke-virtual {v0, v1}, Lcom/peel/widget/ag;->a(I)Lcom/peel/widget/ag;

    move-result-object v0

    sget v1, Lcom/peel/ui/ft;->label_no_codes_found:I

    invoke-virtual {v0, v1}, Lcom/peel/widget/ag;->b(I)Lcom/peel/widget/ag;

    move-result-object v0

    sget v1, Lcom/peel/ui/ft;->label_report:I

    new-instance v2, Lcom/peel/i/b;

    invoke-direct {v2, p0}, Lcom/peel/i/b;-><init>(Lcom/peel/i/a;)V

    invoke-virtual {v0, v1, v2}, Lcom/peel/widget/ag;->a(ILandroid/content/DialogInterface$OnClickListener;)Lcom/peel/widget/ag;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/i/a;->av:Lcom/peel/widget/ag;

    .line 130
    invoke-virtual {v0, v1}, Lcom/peel/widget/ag;->a(Lcom/peel/widget/ag;)Lcom/peel/widget/ag;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/widget/ag;->d()Lcom/peel/widget/ag;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/i/a;->av:Lcom/peel/widget/ag;

    .line 131
    iget-object v0, p0, Lcom/peel/i/a;->av:Lcom/peel/widget/ag;

    new-instance v1, Lcom/peel/i/q;

    invoke-direct {v1, p0}, Lcom/peel/i/q;-><init>(Lcom/peel/i/a;)V

    invoke-virtual {v0, v1}, Lcom/peel/widget/ag;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 138
    :cond_0
    return-void
.end method

.method private T()V
    .locals 3

    .prologue
    .line 141
    iget-object v0, p0, Lcom/peel/i/a;->au:Lcom/peel/widget/ag;

    if-nez v0, :cond_1

    .line 142
    new-instance v0, Lcom/peel/widget/ag;

    invoke-virtual {p0}, Lcom/peel/i/a;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/peel/widget/ag;-><init>(Landroid/content/Context;)V

    sget v1, Lcom/peel/ui/ft;->no_internet:I

    invoke-virtual {p0, v1}, Lcom/peel/i/a;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/peel/widget/ag;->a(Ljava/lang/CharSequence;)Lcom/peel/widget/ag;

    move-result-object v0

    sget v1, Lcom/peel/ui/ft;->no_internet_alert:I

    invoke-virtual {p0, v1}, Lcom/peel/i/a;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/peel/widget/ag;->b(Ljava/lang/CharSequence;)Lcom/peel/widget/ag;

    move-result-object v0

    sget v1, Lcom/peel/ui/ft;->label_settings:I

    new-instance v2, Lcom/peel/i/v;

    invoke-direct {v2, p0}, Lcom/peel/i/v;-><init>(Lcom/peel/i/a;)V

    invoke-virtual {v0, v1, v2}, Lcom/peel/widget/ag;->c(ILandroid/content/DialogInterface$OnClickListener;)Lcom/peel/widget/ag;

    move-result-object v0

    sget v1, Lcom/peel/ui/ft;->ok:I

    new-instance v2, Lcom/peel/i/u;

    invoke-direct {v2, p0}, Lcom/peel/i/u;-><init>(Lcom/peel/i/a;)V

    .line 147
    invoke-virtual {v0, v1, v2}, Lcom/peel/widget/ag;->a(ILandroid/content/DialogInterface$OnClickListener;)Lcom/peel/widget/ag;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/i/a;->au:Lcom/peel/widget/ag;

    .line 156
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/peel/i/a;->au:Lcom/peel/widget/ag;

    iget-object v1, p0, Lcom/peel/i/a;->au:Lcom/peel/widget/ag;

    invoke-virtual {v0, v1}, Lcom/peel/widget/ag;->a(Lcom/peel/widget/ag;)Lcom/peel/widget/ag;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/widget/ag;->show()V

    .line 157
    return-void

    .line 153
    :cond_1
    iget-object v0, p0, Lcom/peel/i/a;->au:Lcom/peel/widget/ag;

    invoke-virtual {v0}, Lcom/peel/widget/ag;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 154
    iget-object v0, p0, Lcom/peel/i/a;->au:Lcom/peel/widget/ag;

    invoke-virtual {v0}, Lcom/peel/widget/ag;->dismiss()V

    goto :goto_0
.end method

.method private U()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v2, 0x0

    .line 252
    iget-object v0, p0, Lcom/peel/i/a;->f:Landroid/widget/ViewFlipper;

    invoke-virtual {v0, v2}, Landroid/widget/ViewFlipper;->setDisplayedChild(I)V

    .line 253
    iput v2, p0, Lcom/peel/i/a;->g:I

    .line 254
    invoke-virtual {p0}, Lcom/peel/i/a;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    iget v1, p0, Lcom/peel/i/a;->ak:I

    invoke-static {v0, v1}, Lcom/peel/util/bx;->c(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, v4, v2}, Lcom/peel/i/a;->a(Ljava/lang/String;ZZ)V

    .line 256
    invoke-virtual {p0}, Lcom/peel/i/a;->v()Landroid/view/View;

    move-result-object v0

    sget v1, Lcom/peel/ui/fp;->brand_list:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/peel/i/a;->an:Landroid/widget/ListView;

    .line 258
    iget-object v0, p0, Lcom/peel/i/a;->an:Landroid/widget/ListView;

    new-instance v1, Lcom/peel/i/w;

    invoke-direct {v1, p0}, Lcom/peel/i/w;-><init>(Lcom/peel/i/a;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 268
    iget-object v0, p0, Lcom/peel/i/a;->f:Landroid/widget/ViewFlipper;

    sget v1, Lcom/peel/ui/fp;->other_brand_btn:I

    invoke-virtual {v0, v1}, Landroid/widget/ViewFlipper;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    sget v1, Lcom/peel/ui/ft;->other_device_brand:I

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    .line 269
    iget-object v0, p0, Lcom/peel/i/a;->f:Landroid/widget/ViewFlipper;

    sget v1, Lcom/peel/ui/fp;->other_brand_btn:I

    invoke-virtual {v0, v1}, Landroid/widget/ViewFlipper;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/peel/i/x;

    invoke-direct {v1, p0}, Lcom/peel/i/x;-><init>(Lcom/peel/i/a;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 276
    iget-object v0, p0, Lcom/peel/i/a;->ap:Ljava/util/List;

    if-nez v0, :cond_1

    .line 279
    sget-object v0, Lcom/peel/i/a;->e:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/peel/i/a;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    invoke-static {v0, v1, v4}, Lcom/peel/util/bx;->a(Ljava/lang/String;Landroid/app/Activity;Z)V

    .line 280
    invoke-virtual {p0}, Lcom/peel/i/a;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string/jumbo v1, "country_ISO"

    const-string/jumbo v2, "US"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 281
    const-string/jumbo v1, "https://partners-ir.peel.com"

    iget v2, p0, Lcom/peel/i/a;->ak:I

    new-instance v3, Lcom/peel/i/y;

    invoke-direct {v3, p0, v4}, Lcom/peel/i/y;-><init>(Lcom/peel/i/a;I)V

    invoke-static {v1, v2, v0, v3}, Lcom/peel/control/aa;->a(Ljava/lang/String;ILjava/lang/String;Lcom/peel/util/t;)V

    .line 321
    :cond_0
    :goto_0
    return-void

    .line 316
    :cond_1
    new-instance v0, Lcom/peel/i/a/a;

    invoke-virtual {p0}, Lcom/peel/i/a;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    sget v2, Lcom/peel/ui/fq;->brand_row:I

    iget-object v3, p0, Lcom/peel/i/a;->ap:Ljava/util/List;

    invoke-direct {v0, v1, v2, v3}, Lcom/peel/i/a/a;-><init>(Landroid/content/Context;ILjava/util/List;)V

    iput-object v0, p0, Lcom/peel/i/a;->aw:Lcom/peel/i/a/a;

    .line 317
    iget-object v0, p0, Lcom/peel/i/a;->an:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/peel/i/a;->aw:Lcom/peel/i/a/a;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 318
    iget-object v0, p0, Lcom/peel/i/a;->ap:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_0

    .line 319
    invoke-direct {p0}, Lcom/peel/i/a;->V()V

    goto :goto_0
.end method

.method private V()V
    .locals 5

    .prologue
    const/16 v2, 0x8

    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 324
    iget-object v0, p0, Lcom/peel/i/a;->f:Landroid/widget/ViewFlipper;

    invoke-virtual {v0, v4}, Landroid/widget/ViewFlipper;->setDisplayedChild(I)V

    .line 325
    iput v4, p0, Lcom/peel/i/a;->g:I

    .line 326
    invoke-virtual {p0}, Lcom/peel/i/a;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    iget v3, p0, Lcom/peel/i/a;->ak:I

    invoke-static {v0, v3}, Lcom/peel/util/bx;->c(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, v4, v1}, Lcom/peel/i/a;->a(Ljava/lang/String;ZZ)V

    .line 328
    invoke-virtual {p0}, Lcom/peel/i/a;->v()Landroid/view/View;

    move-result-object v0

    sget v3, Lcom/peel/ui/fp;->search_other_list_filter:I

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/AutoCompleteTextView;

    iput-object v0, p0, Lcom/peel/i/a;->aC:Landroid/widget/EditText;

    .line 329
    invoke-virtual {p0}, Lcom/peel/i/a;->v()Landroid/view/View;

    move-result-object v0

    sget v3, Lcom/peel/ui/fp;->search_icon:I

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/peel/i/a;->aD:Landroid/widget/ImageView;

    .line 332
    invoke-virtual {p0}, Lcom/peel/i/a;->v()Landroid/view/View;

    move-result-object v0

    sget v3, Lcom/peel/ui/fp;->search_other_list_filter:I

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/AutoCompleteTextView;

    iput-object v0, p0, Lcom/peel/i/a;->aC:Landroid/widget/EditText;

    .line 333
    iget-object v0, p0, Lcom/peel/i/a;->aC:Landroid/widget/EditText;

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setVisibility(I)V

    .line 334
    invoke-virtual {p0}, Lcom/peel/i/a;->v()Landroid/view/View;

    move-result-object v0

    sget v3, Lcom/peel/ui/fp;->search_icon:I

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/peel/i/a;->aD:Landroid/widget/ImageView;

    .line 335
    iget-object v0, p0, Lcom/peel/i/a;->aD:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 336
    iget-object v0, p0, Lcom/peel/i/a;->aq:Ljava/util/List;

    if-eqz v0, :cond_1

    .line 337
    iget-object v0, p0, Lcom/peel/i/a;->aD:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 338
    iget-object v0, p0, Lcom/peel/i/a;->aC:Landroid/widget/EditText;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setVisibility(I)V

    .line 339
    iget-object v0, p0, Lcom/peel/i/a;->aC:Landroid/widget/EditText;

    invoke-virtual {v0, v4}, Landroid/widget/EditText;->setFocusable(Z)V

    .line 340
    iget-object v0, p0, Lcom/peel/i/a;->aC:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    .line 341
    iget-object v0, p0, Lcom/peel/i/a;->aC:Landroid/widget/EditText;

    if-nez v0, :cond_2

    .line 342
    invoke-virtual {p0}, Lcom/peel/i/a;->v()Landroid/view/View;

    move-result-object v0

    sget v3, Lcom/peel/ui/fp;->search_other_list_filter:I

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/AutoCompleteTextView;

    iput-object v0, p0, Lcom/peel/i/a;->aC:Landroid/widget/EditText;

    .line 346
    :goto_0
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v0

    .line 347
    const-string/jumbo v3, "iw"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    const-string/jumbo v3, "ar"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 348
    :cond_0
    iget-object v0, p0, Lcom/peel/i/a;->aC:Landroid/widget/EditText;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget v4, Lcom/peel/ui/ft;->hint_search_box:I

    invoke-virtual {p0, v4}, Lcom/peel/i/a;->a(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/EditText;->setHint(Ljava/lang/CharSequence;)V

    .line 352
    :goto_1
    invoke-virtual {p0}, Lcom/peel/i/a;->v()Landroid/view/View;

    move-result-object v0

    sget v3, Lcom/peel/ui/fp;->other_list:I

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/peel/i/a;->ao:Landroid/widget/ListView;

    .line 354
    invoke-virtual {p0}, Lcom/peel/i/a;->v()Landroid/view/View;

    move-result-object v0

    sget v3, Lcom/peel/ui/fp;->search_cancel_btn_other:I

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/peel/i/a;->aB:Landroid/widget/ImageView;

    .line 355
    invoke-virtual {p0}, Lcom/peel/i/a;->v()Landroid/view/View;

    move-result-object v0

    sget v3, Lcom/peel/ui/fp;->search_layout_other:I

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/peel/i/a;->aE:Landroid/widget/RelativeLayout;

    .line 357
    iget-object v0, p0, Lcom/peel/i/a;->aC:Landroid/widget/EditText;

    new-instance v3, Lcom/peel/i/z;

    invoke-direct {v3, p0}, Lcom/peel/i/z;-><init>(Lcom/peel/i/a;)V

    invoke-virtual {v0, v3}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 381
    iget-object v0, p0, Lcom/peel/i/a;->aB:Landroid/widget/ImageView;

    new-instance v3, Lcom/peel/i/aa;

    invoke-direct {v3, p0}, Lcom/peel/i/aa;-><init>(Lcom/peel/i/a;)V

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 394
    iget-object v3, p0, Lcom/peel/i/a;->aE:Landroid/widget/RelativeLayout;

    iget-object v0, p0, Lcom/peel/i/a;->aq:Ljava/util/List;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/peel/i/a;->aq:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/16 v4, 0x9

    if-le v0, v4, :cond_4

    move v0, v1

    :goto_2
    invoke-virtual {v3, v0}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 396
    iget-object v0, p0, Lcom/peel/i/a;->aq:Ljava/util/List;

    new-instance v1, Lcom/peel/f/b;

    invoke-direct {v1}, Lcom/peel/f/b;-><init>()V

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 397
    new-instance v0, Lcom/peel/i/a/a;

    invoke-virtual {p0}, Lcom/peel/i/a;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    sget v2, Lcom/peel/ui/fq;->brand_row:I

    iget-object v3, p0, Lcom/peel/i/a;->aq:Ljava/util/List;

    invoke-direct {v0, v1, v2, v3}, Lcom/peel/i/a/a;-><init>(Landroid/content/Context;ILjava/util/List;)V

    iput-object v0, p0, Lcom/peel/i/a;->ax:Lcom/peel/i/a/a;

    .line 398
    iget-object v0, p0, Lcom/peel/i/a;->ao:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/peel/i/a;->ax:Lcom/peel/i/a/a;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 399
    iget-object v0, p0, Lcom/peel/i/a;->ao:Landroid/widget/ListView;

    new-instance v1, Lcom/peel/i/c;

    invoke-direct {v1, p0}, Lcom/peel/i/c;-><init>(Lcom/peel/i/a;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 412
    invoke-virtual {p0}, Lcom/peel/i/a;->v()Landroid/view/View;

    move-result-object v0

    sget v1, Lcom/peel/ui/fp;->missing_device_brand_btn:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    new-instance v1, Lcom/peel/i/d;

    invoke-direct {v1, p0}, Lcom/peel/i/d;-><init>(Lcom/peel/i/a;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 439
    :cond_1
    return-void

    .line 344
    :cond_2
    iget-object v0, p0, Lcom/peel/i/a;->aC:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getEditableText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->clear()V

    goto/16 :goto_0

    .line 350
    :cond_3
    iget-object v0, p0, Lcom/peel/i/a;->aC:Landroid/widget/EditText;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "       "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget v4, Lcom/peel/ui/ft;->hint_search_box:I

    invoke-virtual {p0, v4}, Lcom/peel/i/a;->a(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/EditText;->setHint(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    :cond_4
    move v0, v2

    .line 394
    goto :goto_2
.end method

.method static synthetic a(Lcom/peel/i/a;I)I
    .locals 0

    .prologue
    .line 82
    iput p1, p0, Lcom/peel/i/a;->g:I

    return p1
.end method

.method static synthetic a(Lcom/peel/i/a;Lcom/peel/control/a;)Lcom/peel/control/a;
    .locals 0

    .prologue
    .line 82
    iput-object p1, p0, Lcom/peel/i/a;->al:Lcom/peel/control/a;

    return-object p1
.end method

.method static synthetic a(Lcom/peel/i/a;)Lcom/peel/f/a;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/peel/i/a;->am:Lcom/peel/f/a;

    return-object v0
.end method

.method static synthetic a(Lcom/peel/i/a;Lcom/peel/f/a;)Lcom/peel/f/a;
    .locals 0

    .prologue
    .line 82
    iput-object p1, p0, Lcom/peel/i/a;->am:Lcom/peel/f/a;

    return-object p1
.end method

.method static synthetic a(Lcom/peel/i/a;Lcom/peel/i/a/a;)Lcom/peel/i/a/a;
    .locals 0

    .prologue
    .line 82
    iput-object p1, p0, Lcom/peel/i/a;->aw:Lcom/peel/i/a/a;

    return-object p1
.end method

.method static synthetic a(Lcom/peel/i/a;Lcom/peel/i/ad;)Lcom/peel/i/ad;
    .locals 0

    .prologue
    .line 82
    iput-object p1, p0, Lcom/peel/i/a;->aS:Lcom/peel/i/ad;

    return-object p1
.end method

.method static synthetic a(Lcom/peel/i/a;Lcom/peel/ui/dg;)Lcom/peel/ui/dg;
    .locals 0

    .prologue
    .line 82
    iput-object p1, p0, Lcom/peel/i/a;->aF:Lcom/peel/ui/dg;

    return-object p1
.end method

.method static synthetic a(Lcom/peel/i/a;Lcom/peel/widget/ag;)Lcom/peel/widget/ag;
    .locals 0

    .prologue
    .line 82
    iput-object p1, p0, Lcom/peel/i/a;->av:Lcom/peel/widget/ag;

    return-object p1
.end method

.method static synthetic a(Lcom/peel/i/a;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 82
    iput-object p1, p0, Lcom/peel/i/a;->az:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic a(Lcom/peel/i/a;Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 0

    .prologue
    .line 82
    iput-object p1, p0, Lcom/peel/i/a;->h:Ljava/util/ArrayList;

    return-object p1
.end method

.method static synthetic a(Lcom/peel/i/a;Ljava/util/List;)Ljava/util/List;
    .locals 0

    .prologue
    .line 82
    iput-object p1, p0, Lcom/peel/i/a;->ap:Ljava/util/List;

    return-object p1
.end method

.method private a(Lcom/peel/control/h;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 444
    iget-object v0, p0, Lcom/peel/i/a;->al:Lcom/peel/control/a;

    if-nez v0, :cond_0

    .line 446
    iget-object v0, p0, Lcom/peel/i/a;->az:Ljava/lang/String;

    invoke-static {v0}, Lcom/peel/control/a;->a(Ljava/lang/String;)Lcom/peel/control/a;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/i/a;->al:Lcom/peel/control/a;

    .line 449
    :cond_0
    iget-object v0, p0, Lcom/peel/i/a;->al:Lcom/peel/control/a;

    new-array v1, v6, [Ljava/lang/Integer;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-virtual {v0, p1, v5, v1}, Lcom/peel/control/a;->a(Lcom/peel/control/h;Ljava/lang/String;[Ljava/lang/Integer;)Z

    .line 451
    iget-object v0, p0, Lcom/peel/i/a;->at:Lcom/peel/control/h;

    if-eqz v0, :cond_3

    .line 452
    iget-object v0, p0, Lcom/peel/i/a;->al:Lcom/peel/control/a;

    iget-object v1, p0, Lcom/peel/i/a;->at:Lcom/peel/control/h;

    new-array v2, v6, [Ljava/lang/Integer;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v5, v2}, Lcom/peel/control/a;->a(Lcom/peel/control/h;Ljava/lang/String;[Ljava/lang/Integer;)Z

    .line 454
    iget-object v0, p0, Lcom/peel/i/a;->as:Lcom/peel/control/h;

    if-eqz v0, :cond_1

    .line 455
    iget-object v0, p0, Lcom/peel/i/a;->al:Lcom/peel/control/a;

    iget-object v1, p0, Lcom/peel/i/a;->as:Lcom/peel/control/h;

    invoke-virtual {v0, v1, v5, v5}, Lcom/peel/control/a;->a(Lcom/peel/control/h;Ljava/lang/String;[Ljava/lang/Integer;)Z

    .line 460
    :cond_1
    :goto_0
    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v0}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/i/a;->al:Lcom/peel/control/a;

    invoke-virtual {v0, v1}, Lcom/peel/control/RoomControl;->a(Lcom/peel/control/a;)V

    .line 461
    invoke-virtual {p0}, Lcom/peel/i/a;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    const-string/jumbo v1, "social_accounts_setup"

    invoke-virtual {v0, v1, v4}, Landroid/support/v4/app/ae;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v1, "show social login"

    invoke-interface {v0, v1, v6}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 462
    invoke-virtual {p0}, Lcom/peel/i/a;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/ae;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/peel/social/w;->f(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 463
    new-instance v0, Lcom/peel/backup/c;

    invoke-virtual {p0}, Lcom/peel/i/a;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/ae;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/peel/backup/c;-><init>(Landroid/content/Context;)V

    .line 464
    sget-object v1, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v1}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v1

    iget-object v2, p0, Lcom/peel/i/a;->al:Lcom/peel/control/a;

    iget-object v3, p0, Lcom/peel/i/a;->ar:Lcom/peel/control/h;

    invoke-virtual {v0, v1, v5, v2, v3}, Lcom/peel/backup/c;->a(Lcom/peel/control/RoomControl;Ljava/lang/String;Lcom/peel/control/a;Lcom/peel/control/h;)V

    .line 475
    :cond_2
    sget-object v0, Lcom/peel/i/a;->e:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/peel/i/a;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    invoke-static {v0, v1, v4}, Lcom/peel/util/bx;->a(Ljava/lang/String;Landroid/app/Activity;Z)V

    .line 476
    invoke-virtual {p0}, Lcom/peel/i/a;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    sget-object v1, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v1}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/peel/util/bx;->a(Landroid/content/Context;Lcom/peel/control/RoomControl;)V

    .line 477
    return-void

    .line 456
    :cond_3
    iget-object v0, p0, Lcom/peel/i/a;->as:Lcom/peel/control/h;

    if-eqz v0, :cond_1

    .line 457
    iget-object v0, p0, Lcom/peel/i/a;->al:Lcom/peel/control/a;

    iget-object v1, p0, Lcom/peel/i/a;->as:Lcom/peel/control/h;

    new-array v2, v6, [Ljava/lang/Integer;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v5, v2}, Lcom/peel/control/a;->a(Lcom/peel/control/h;Ljava/lang/String;[Ljava/lang/Integer;)Z

    goto :goto_0
.end method

.method static synthetic a(Lcom/peel/i/a;Lcom/peel/control/h;)V
    .locals 0

    .prologue
    .line 82
    invoke-direct {p0, p1}, Lcom/peel/i/a;->a(Lcom/peel/control/h;)V

    return-void
.end method

.method static synthetic a(Lcom/peel/i/a;Ljava/lang/String;ZZ)V
    .locals 0

    .prologue
    .line 82
    invoke-direct {p0, p1, p2, p3}, Lcom/peel/i/a;->a(Ljava/lang/String;ZZ)V

    return-void
.end method

.method private a(Ljava/lang/String;ZZ)V
    .locals 7

    .prologue
    .line 991
    const/4 v6, 0x0

    .line 992
    if-eqz p2, :cond_0

    .line 993
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 994
    sget v0, Lcom/peel/ui/fp;->menu_next:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 997
    :cond_0
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 998
    const-string/jumbo v1, "text"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 999
    iget-object v0, p0, Lcom/peel/i/a;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "category"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1000
    new-instance v0, Lcom/peel/d/a;

    sget-object v1, Lcom/peel/d/d;->b:Lcom/peel/d/d;

    sget-object v2, Lcom/peel/d/b;->a:Lcom/peel/d/b;

    sget-object v3, Lcom/peel/d/c;->b:Lcom/peel/d/c;

    move-object v4, p1

    move v5, p3

    invoke-direct/range {v0 .. v6}, Lcom/peel/d/a;-><init>(Lcom/peel/d/d;Lcom/peel/d/b;Lcom/peel/d/c;Ljava/lang/String;ZLjava/util/List;)V

    iput-object v0, p0, Lcom/peel/i/a;->d:Lcom/peel/d/a;

    .line 1001
    invoke-virtual {p0}, Lcom/peel/i/a;->Z()V

    .line 1002
    return-void
.end method

.method static synthetic a(Lcom/peel/i/a;Z)Z
    .locals 0

    .prologue
    .line 82
    iput-boolean p1, p0, Lcom/peel/i/a;->aA:Z

    return p1
.end method

.method private ac()V
    .locals 9

    .prologue
    .line 482
    iget v0, p0, Lcom/peel/i/a;->ak:I

    packed-switch v0, :pswitch_data_0

    .line 549
    const/4 v0, 0x0

    iget v1, p0, Lcom/peel/i/a;->ak:I

    iget-object v2, p0, Lcom/peel/i/a;->am:Lcom/peel/f/a;

    invoke-virtual {v2}, Lcom/peel/f/a;->b()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, -0x1

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-static/range {v0 .. v8}, Lcom/peel/control/h;->a(IILjava/lang/String;ZLjava/lang/String;ILandroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;)Lcom/peel/control/h;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/i/a;->ar:Lcom/peel/control/h;

    .line 552
    iget-object v0, p0, Lcom/peel/i/a;->f:Landroid/widget/ViewFlipper;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/widget/ViewFlipper;->setDisplayedChild(I)V

    .line 553
    invoke-virtual {p0}, Lcom/peel/i/a;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    const-string/jumbo v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/ae;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 554
    invoke-virtual {p0}, Lcom/peel/i/a;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/ae;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 556
    const/4 v0, 0x0

    iput v0, p0, Lcom/peel/i/a;->aT:I

    .line 557
    invoke-virtual {p0}, Lcom/peel/i/a;->v()Landroid/view/View;

    move-result-object v0

    sget v1, Lcom/peel/ui/fp;->layout_device_setup_test:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    .line 558
    sget v1, Lcom/peel/ui/fp;->device_visual:I

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/peel/i/a;->aO:Landroid/widget/ImageView;

    .line 559
    invoke-virtual {p0}, Lcom/peel/i/a;->v()Landroid/view/View;

    move-result-object v1

    sget v2, Lcom/peel/ui/fp;->coverView:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    new-instance v2, Lcom/peel/i/j;

    invoke-direct {v2, p0}, Lcom/peel/i/j;-><init>(Lcom/peel/i/a;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 565
    iget v1, p0, Lcom/peel/i/a;->ak:I

    sparse-switch v1, :sswitch_data_0

    .line 584
    iget-object v1, p0, Lcom/peel/i/a;->aO:Landroid/widget/ImageView;

    sget v2, Lcom/peel/ui/fo;->test_tv_drawing:I

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 588
    :goto_0
    sget v1, Lcom/peel/ui/fp;->testing_turn_on_msg:I

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/peel/i/a;->aL:Landroid/widget/TextView;

    .line 589
    iget-object v1, p0, Lcom/peel/i/a;->aL:Landroid/widget/TextView;

    sget v2, Lcom/peel/ui/ft;->testing_tv_msg:I

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/peel/i/a;->az:Ljava/lang/String;

    aput-object v5, v3, v4

    invoke-virtual {p0, v2, v3}, Lcom/peel/i/a;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 591
    sget v1, Lcom/peel/ui/fp;->test_btn_viewpager:I

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/peel/widget/TestBtnViewPager;

    iput-object v1, p0, Lcom/peel/i/a;->aP:Lcom/peel/widget/TestBtnViewPager;

    .line 592
    iget-object v1, p0, Lcom/peel/i/a;->aP:Lcom/peel/widget/TestBtnViewPager;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/peel/widget/TestBtnViewPager;->setEnabledSwipe(Z)V

    .line 593
    iget-object v1, p0, Lcom/peel/i/a;->aP:Lcom/peel/widget/TestBtnViewPager;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Lcom/peel/widget/TestBtnViewPager;->setVisibility(I)V

    .line 596
    sget v1, Lcom/peel/ui/fp;->test_pager_left_btn:I

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/peel/i/a;->aQ:Landroid/widget/Button;

    .line 597
    iget-object v1, p0, Lcom/peel/i/a;->aQ:Landroid/widget/Button;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setEnabled(Z)V

    .line 598
    iget-object v1, p0, Lcom/peel/i/a;->aQ:Landroid/widget/Button;

    new-instance v2, Lcom/peel/i/k;

    invoke-direct {v2, p0}, Lcom/peel/i/k;-><init>(Lcom/peel/i/a;)V

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 607
    sget v1, Lcom/peel/ui/fp;->test_pager_right_btn:I

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/peel/i/a;->aR:Landroid/widget/Button;

    .line 608
    iget-object v1, p0, Lcom/peel/i/a;->aR:Landroid/widget/Button;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setEnabled(Z)V

    .line 609
    iget-object v1, p0, Lcom/peel/i/a;->aR:Landroid/widget/Button;

    new-instance v2, Lcom/peel/i/l;

    invoke-direct {v2, p0}, Lcom/peel/i/l;-><init>(Lcom/peel/i/a;)V

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 617
    iget-object v1, p0, Lcom/peel/i/a;->aQ:Landroid/widget/Button;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 618
    iget-object v1, p0, Lcom/peel/i/a;->aR:Landroid/widget/Button;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 620
    sget v1, Lcom/peel/ui/fp;->test_question_msg:I

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/peel/i/a;->aK:Landroid/widget/TextView;

    .line 621
    const/4 v1, 0x2

    iget v2, p0, Lcom/peel/i/a;->ak:I

    if-eq v1, v2, :cond_0

    const/16 v1, 0x14

    iget v2, p0, Lcom/peel/i/a;->ak:I

    if-ne v1, v2, :cond_8

    .line 622
    :cond_0
    iget-object v1, p0, Lcom/peel/i/a;->aK:Landroid/widget/TextView;

    sget v2, Lcom/peel/ui/ft;->device_test_channel_change_question_msg:I

    invoke-virtual {p0, v2}, Lcom/peel/i/a;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 627
    :goto_1
    sget v1, Lcom/peel/ui/fp;->layout_test_btn:I

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    iput-object v1, p0, Lcom/peel/i/a;->aN:Landroid/widget/RelativeLayout;

    .line 628
    sget v1, Lcom/peel/ui/fp;->layout_test_msg:I

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    iput-object v1, p0, Lcom/peel/i/a;->aM:Landroid/widget/RelativeLayout;

    .line 630
    iget-object v1, p0, Lcom/peel/i/a;->aN:Landroid/widget/RelativeLayout;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 631
    iget-object v1, p0, Lcom/peel/i/a;->aM:Landroid/widget/RelativeLayout;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 633
    sget v1, Lcom/peel/ui/fp;->turn_on_msg:I

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/peel/i/a;->aI:Landroid/widget/TextView;

    .line 634
    iget-object v1, p0, Lcom/peel/i/a;->aI:Landroid/widget/TextView;

    sget v2, Lcom/peel/ui/ft;->testing_key_power:I

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/peel/i/a;->am:Lcom/peel/f/a;

    invoke-virtual {v5}, Lcom/peel/f/a;->b()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    iget-object v5, p0, Lcom/peel/i/a;->az:Ljava/lang/String;

    aput-object v5, v3, v4

    invoke-virtual {p0, v2, v3}, Lcom/peel/i/a;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 636
    sget v1, Lcom/peel/ui/fp;->test_status_msg:I

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/peel/i/a;->aJ:Landroid/widget/TextView;

    .line 637
    iget-object v1, p0, Lcom/peel/i/a;->aJ:Landroid/widget/TextView;

    new-instance v2, Lcom/peel/i/m;

    invoke-direct {v2, p0}, Lcom/peel/i/m;-><init>(Lcom/peel/i/a;)V

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 647
    sget v1, Lcom/peel/ui/fp;->yes_btn:I

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/peel/i/a;->aU:Landroid/widget/Button;

    .line 648
    sget v1, Lcom/peel/ui/fp;->no_btn:I

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/peel/i/a;->aV:Landroid/widget/Button;

    .line 651
    sget-object v0, Lcom/peel/i/a;->e:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/peel/i/a;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lcom/peel/util/bx;->a(Ljava/lang/String;Landroid/app/Activity;Z)V

    .line 653
    iget-object v0, p0, Lcom/peel/i/a;->ay:Ljava/lang/String;

    const-string/jumbo v1, "Power"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/peel/i/a;->ay:Ljava/lang/String;

    const-string/jumbo v1, "PowerOn"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 654
    :cond_1
    sget v0, Lcom/peel/ui/ft;->turn_on_device:I

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/peel/i/a;->am:Lcom/peel/f/a;

    invoke-virtual {v3}, Lcom/peel/f/a;->b()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/peel/i/a;->az:Ljava/lang/String;

    aput-object v3, v1, v2

    invoke-virtual {p0, v0, v1}, Lcom/peel/i/a;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-direct {p0, v0, v1, v2}, Lcom/peel/i/a;->a(Ljava/lang/String;ZZ)V

    .line 659
    :cond_2
    :goto_2
    iget-object v0, p0, Lcom/peel/i/a;->aU:Landroid/widget/Button;

    new-instance v1, Lcom/peel/i/n;

    invoke-direct {v1, p0}, Lcom/peel/i/n;-><init>(Lcom/peel/i/a;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 807
    iget-object v0, p0, Lcom/peel/i/a;->aV:Landroid/widget/Button;

    new-instance v1, Lcom/peel/i/p;

    invoke-direct {v1, p0}, Lcom/peel/i/p;-><init>(Lcom/peel/i/a;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 839
    const/4 v0, 0x1

    new-array v1, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    iget-object v2, p0, Lcom/peel/i/a;->ay:Ljava/lang/String;

    aput-object v2, v1, v0

    .line 841
    invoke-virtual {p0}, Lcom/peel/i/a;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string/jumbo v2, "country_ISO"

    const-string/jumbo v3, "US"

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 842
    const-string/jumbo v0, "https://partners-ir.peel.com"

    iget v2, p0, Lcom/peel/i/a;->ak:I

    iget-object v3, p0, Lcom/peel/i/a;->am:Lcom/peel/f/a;

    invoke-virtual {v3}, Lcom/peel/f/a;->a()I

    move-result v3

    new-instance v5, Lcom/peel/i/r;

    const/4 v6, 0x1

    invoke-direct {v5, p0, v6}, Lcom/peel/i/r;-><init>(Lcom/peel/i/a;I)V

    invoke-static/range {v0 .. v5}, Lcom/peel/control/aa;->a(Ljava/lang/String;[Ljava/lang/String;IILjava/lang/String;Lcom/peel/util/t;)V

    .line 955
    :cond_3
    :goto_3
    return-void

    .line 484
    :pswitch_0
    const/4 v0, 0x0

    iget v1, p0, Lcom/peel/i/a;->ak:I

    iget-object v2, p0, Lcom/peel/i/a;->am:Lcom/peel/f/a;

    invoke-virtual {v2}, Lcom/peel/f/a;->b()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    const/4 v4, 0x0

    const/4 v5, -0x1

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-static/range {v0 .. v8}, Lcom/peel/control/h;->a(IILjava/lang/String;ZLjava/lang/String;ILandroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;)Lcom/peel/control/h;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/i/a;->ar:Lcom/peel/control/h;

    .line 490
    iget-object v0, p0, Lcom/peel/i/a;->am:Lcom/peel/f/a;

    invoke-virtual {v0}, Lcom/peel/f/a;->b()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v0, v1}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "apple"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 491
    const-class v0, Lcom/peel/i/a;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "start scanning for google tv"

    new-instance v2, Lcom/peel/i/g;

    invoke-direct {v2, p0}, Lcom/peel/i/g;-><init>(Lcom/peel/i/a;)V

    invoke-static {v0, v1, v2}, Lcom/peel/util/i;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/lang/Runnable;

    .line 511
    :cond_4
    :goto_4
    iget-object v0, p0, Lcom/peel/i/a;->am:Lcom/peel/f/a;

    invoke-virtual {v0}, Lcom/peel/f/a;->b()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v0, v1}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "roku"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 512
    sget-object v0, Lcom/peel/i/a;->e:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/peel/i/a;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lcom/peel/util/bx;->a(Ljava/lang/String;Landroid/app/Activity;Z)V

    .line 514
    invoke-virtual {p0}, Lcom/peel/i/a;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string/jumbo v1, "country_ISO"

    const-string/jumbo v2, "US"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 515
    const-string/jumbo v1, "https://partners-ir.peel.com"

    iget-object v2, p0, Lcom/peel/i/a;->am:Lcom/peel/f/a;

    invoke-virtual {v2}, Lcom/peel/f/a;->a()I

    move-result v2

    iget v3, p0, Lcom/peel/i/a;->ak:I

    new-instance v4, Lcom/peel/i/h;

    invoke-direct {v4, p0}, Lcom/peel/i/h;-><init>(Lcom/peel/i/a;)V

    invoke-static {v1, v2, v3, v0, v4}, Lcom/peel/control/aa;->a(Ljava/lang/String;IILjava/lang/String;Lcom/peel/util/t;)V

    goto :goto_3

    .line 498
    :cond_5
    iget-object v0, p0, Lcom/peel/i/a;->am:Lcom/peel/f/a;

    invoke-virtual {v0}, Lcom/peel/f/a;->b()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v0, v1}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "roku"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 499
    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/peel/i/a;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    invoke-static {v1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    const-string/jumbo v2, "setup_type"

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    if-ne v0, v1, :cond_6

    const/4 v0, 0x1

    .line 500
    :goto_5
    sget-object v1, Lcom/peel/i/a;->e:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/peel/i/a;->m()Landroid/support/v4/app/ae;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Lcom/peel/util/bx;->a(Ljava/lang/String;Landroid/app/Activity;Z)V

    .line 501
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 502
    const-string/jumbo v2, "brandName"

    iget-object v3, p0, Lcom/peel/i/a;->am:Lcom/peel/f/a;

    invoke-virtual {v3}, Lcom/peel/f/a;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 503
    const-string/jumbo v2, "device_type"

    iget v3, p0, Lcom/peel/i/a;->ak:I

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 504
    const-string/jumbo v2, "brandId"

    iget-object v3, p0, Lcom/peel/i/a;->am:Lcom/peel/f/a;

    invoke-virtual {v3}, Lcom/peel/f/a;->a()I

    move-result v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 505
    const-string/jumbo v2, "back_to_clazz"

    if-eqz v0, :cond_7

    const-class v0, Lcom/peel/i/ai;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    :goto_6
    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 506
    invoke-virtual {p0}, Lcom/peel/i/a;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    const-class v2, Lcom/peel/i/fc;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2, v1}, Lcom/peel/d/e;->c(Landroid/support/v4/app/ae;Ljava/lang/String;Landroid/os/Bundle;)V

    goto/16 :goto_4

    .line 499
    :cond_6
    const/4 v0, 0x0

    goto :goto_5

    .line 505
    :cond_7
    const-class v0, Lcom/peel/i/a;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    goto :goto_6

    .line 568
    :sswitch_0
    iget-object v1, p0, Lcom/peel/i/a;->aO:Landroid/widget/ImageView;

    sget v2, Lcom/peel/ui/fo;->test_stb_drawing:I

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_0

    .line 571
    :sswitch_1
    iget-object v1, p0, Lcom/peel/i/a;->aO:Landroid/widget/ImageView;

    sget v2, Lcom/peel/ui/fo;->test_av_drawing:I

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_0

    .line 574
    :sswitch_2
    iget-object v1, p0, Lcom/peel/i/a;->aO:Landroid/widget/ImageView;

    sget v2, Lcom/peel/ui/fo;->test_dvd_drawing:I

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_0

    .line 577
    :sswitch_3
    iget-object v1, p0, Lcom/peel/i/a;->aO:Landroid/widget/ImageView;

    sget v2, Lcom/peel/ui/fo;->test_bluray_drawing:I

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_0

    .line 580
    :sswitch_4
    iget-object v1, p0, Lcom/peel/i/a;->aO:Landroid/widget/ImageView;

    sget v2, Lcom/peel/ui/fo;->test_projector_drawing:I

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 581
    const-string/jumbo v1, "Power"

    iput-object v1, p0, Lcom/peel/i/a;->ay:Ljava/lang/String;

    goto/16 :goto_0

    .line 624
    :cond_8
    iget-object v1, p0, Lcom/peel/i/a;->aK:Landroid/widget/TextView;

    sget v2, Lcom/peel/ui/ft;->device_test_turn_on_question_msg:I

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/peel/i/a;->az:Ljava/lang/String;

    aput-object v5, v3, v4

    invoke-virtual {p0, v2, v3}, Lcom/peel/i/a;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 655
    :cond_9
    iget-object v0, p0, Lcom/peel/i/a;->ay:Ljava/lang/String;

    const-string/jumbo v1, "Channel_Up"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 656
    sget v0, Lcom/peel/ui/ft;->testing_device:I

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/peel/i/a;->az:Ljava/lang/String;

    aput-object v3, v1, v2

    invoke-virtual {p0, v0, v1}, Lcom/peel/i/a;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-direct {p0, v0, v1, v2}, Lcom/peel/i/a;->a(Ljava/lang/String;ZZ)V

    goto/16 :goto_2

    .line 482
    :pswitch_data_0
    .packed-switch 0x6
        :pswitch_0
    .end packed-switch

    .line 565
    :sswitch_data_0
    .sparse-switch
        0x2 -> :sswitch_0
        0x3 -> :sswitch_2
        0x4 -> :sswitch_3
        0x5 -> :sswitch_1
        0xa -> :sswitch_4
        0x14 -> :sswitch_0
    .end sparse-switch
.end method

.method private ad()V
    .locals 5

    .prologue
    .line 958
    iget-object v1, p0, Lcom/peel/i/a;->aJ:Landroid/widget/TextView;

    iget v0, p0, Lcom/peel/i/a;->aj:I

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 959
    return-void

    .line 958
    :cond_0
    sget v0, Lcom/peel/ui/ft;->ir_send_code:I

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget v4, p0, Lcom/peel/i/a;->aj:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {p0, v0, v2}, Lcom/peel/i/a;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private ae()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 962
    iget-object v0, p0, Lcom/peel/i/a;->ar:Lcom/peel/control/h;

    if-eqz v0, :cond_0

    .line 963
    iget-object v0, p0, Lcom/peel/i/a;->ar:Lcom/peel/control/h;

    iget-object v1, p0, Lcom/peel/i/a;->ay:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/peel/control/h;->c(Ljava/lang/String;)Z

    .line 964
    sget-object v0, Lcom/peel/i/a;->e:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/peel/i/a;->az:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/peel/i/a;->ay:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " cmd pressed codeIdx:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/peel/i/a;->aj:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "/codesetId:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/peel/i/a;->i:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 967
    iget-object v0, p0, Lcom/peel/i/a;->aJ:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 968
    iget-object v0, p0, Lcom/peel/i/a;->aJ:Landroid/widget/TextView;

    sget v1, Lcom/peel/ui/ft;->ir_send_code:I

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    iget v3, p0, Lcom/peel/i/a;->aj:I

    add-int/lit8 v3, v3, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {p0, v1, v2}, Lcom/peel/i/a;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 969
    iget-object v0, p0, Lcom/peel/i/a;->aN:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v4}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 970
    iget-object v0, p0, Lcom/peel/i/a;->aM:Landroid/widget/RelativeLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 972
    :cond_0
    return-void
.end method

.method private af()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 976
    new-instance v0, Lcom/peel/widget/ag;

    invoke-virtual {p0}, Lcom/peel/i/a;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/peel/widget/ag;-><init>(Landroid/content/Context;)V

    sget v1, Lcom/peel/ui/ft;->label_success:I

    invoke-virtual {v0, v1}, Lcom/peel/widget/ag;->a(I)Lcom/peel/widget/ag;

    move-result-object v0

    sget v1, Lcom/peel/ui/ft;->label_device_setup_success:I

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/peel/i/a;->am:Lcom/peel/f/a;

    invoke-virtual {v4}, Lcom/peel/f/a;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/peel/i/a;->az:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-virtual {p0, v1, v2}, Lcom/peel/i/a;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/peel/widget/ag;->b(Ljava/lang/CharSequence;)Lcom/peel/widget/ag;

    move-result-object v0

    sget v1, Lcom/peel/ui/ft;->okay:I

    new-instance v2, Lcom/peel/i/t;

    invoke-direct {v2, p0}, Lcom/peel/i/t;-><init>(Lcom/peel/i/a;)V

    .line 977
    invoke-virtual {v0, v1, v2}, Lcom/peel/widget/ag;->a(ILandroid/content/DialogInterface$OnClickListener;)Lcom/peel/widget/ag;

    move-result-object v0

    .line 983
    invoke-virtual {v0, v5}, Lcom/peel/widget/ag;->a(Z)Lcom/peel/widget/ag;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/i/a;->aH:Lcom/peel/widget/ag;

    .line 984
    iget-object v0, p0, Lcom/peel/i/a;->aH:Lcom/peel/widget/ag;

    invoke-virtual {v0, v5}, Lcom/peel/widget/ag;->setCanceledOnTouchOutside(Z)V

    .line 985
    iget-object v0, p0, Lcom/peel/i/a;->aH:Lcom/peel/widget/ag;

    invoke-virtual {v0}, Lcom/peel/widget/ag;->isShowing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 986
    iget-object v0, p0, Lcom/peel/i/a;->aH:Lcom/peel/widget/ag;

    iget-object v1, p0, Lcom/peel/i/a;->aH:Lcom/peel/widget/ag;

    invoke-virtual {v0, v1}, Lcom/peel/widget/ag;->a(Lcom/peel/widget/ag;)Lcom/peel/widget/ag;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/widget/ag;->show()V

    .line 987
    :cond_0
    return-void
.end method

.method static synthetic b(Lcom/peel/i/a;I)I
    .locals 0

    .prologue
    .line 82
    iput p1, p0, Lcom/peel/i/a;->ak:I

    return p1
.end method

.method static synthetic b(Lcom/peel/i/a;Lcom/peel/widget/ag;)Lcom/peel/widget/ag;
    .locals 0

    .prologue
    .line 82
    iput-object p1, p0, Lcom/peel/i/a;->aG:Lcom/peel/widget/ag;

    return-object p1
.end method

.method static synthetic b(Lcom/peel/i/a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/peel/i/a;->az:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic b(Lcom/peel/i/a;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 82
    iput-object p1, p0, Lcom/peel/i/a;->ay:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic b(Lcom/peel/i/a;Ljava/util/List;)Ljava/util/List;
    .locals 0

    .prologue
    .line 82
    iput-object p1, p0, Lcom/peel/i/a;->aq:Ljava/util/List;

    return-object p1
.end method

.method static synthetic c(Lcom/peel/i/a;I)I
    .locals 0

    .prologue
    .line 82
    iput p1, p0, Lcom/peel/i/a;->aj:I

    return p1
.end method

.method static synthetic c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 82
    sget-object v0, Lcom/peel/i/a;->e:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic c(Lcom/peel/i/a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/peel/i/a;->ay:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic d(Lcom/peel/i/a;I)I
    .locals 0

    .prologue
    .line 82
    iput p1, p0, Lcom/peel/i/a;->i:I

    return p1
.end method

.method static synthetic d(Lcom/peel/i/a;)Lcom/peel/widget/ag;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/peel/i/a;->au:Lcom/peel/widget/ag;

    return-object v0
.end method

.method static synthetic e(Lcom/peel/i/a;I)I
    .locals 0

    .prologue
    .line 82
    iput p1, p0, Lcom/peel/i/a;->aT:I

    return p1
.end method

.method static synthetic e(Lcom/peel/i/a;)Landroid/widget/ListView;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/peel/i/a;->an:Landroid/widget/ListView;

    return-object v0
.end method

.method static synthetic f(Lcom/peel/i/a;)Ljava/util/List;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/peel/i/a;->ap:Ljava/util/List;

    return-object v0
.end method

.method static synthetic g(Lcom/peel/i/a;)Lcom/peel/i/a/a;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/peel/i/a;->aw:Lcom/peel/i/a/a;

    return-object v0
.end method

.method static synthetic h(Lcom/peel/i/a;)I
    .locals 1

    .prologue
    .line 82
    iget v0, p0, Lcom/peel/i/a;->ak:I

    return v0
.end method

.method static synthetic i(Lcom/peel/i/a;)V
    .locals 0

    .prologue
    .line 82
    invoke-direct {p0}, Lcom/peel/i/a;->V()V

    return-void
.end method

.method static synthetic j(Lcom/peel/i/a;)V
    .locals 0

    .prologue
    .line 82
    invoke-direct {p0}, Lcom/peel/i/a;->T()V

    return-void
.end method

.method static synthetic k(Lcom/peel/i/a;)V
    .locals 0

    .prologue
    .line 82
    invoke-direct {p0}, Lcom/peel/i/a;->S()V

    return-void
.end method

.method static synthetic l(Lcom/peel/i/a;)Landroid/widget/ViewFlipper;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/peel/i/a;->f:Landroid/widget/ViewFlipper;

    return-object v0
.end method

.method static synthetic m(Lcom/peel/i/a;)Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/peel/i/a;->aD:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic n(Lcom/peel/i/a;)Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/peel/i/a;->aB:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic o(Lcom/peel/i/a;)Landroid/widget/ListView;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/peel/i/a;->ao:Landroid/widget/ListView;

    return-object v0
.end method

.method static synthetic p(Lcom/peel/i/a;)Landroid/widget/EditText;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/peel/i/a;->aC:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic q(Lcom/peel/i/a;)Lcom/peel/i/a/a;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/peel/i/a;->ax:Lcom/peel/i/a/a;

    return-object v0
.end method

.method static synthetic r(Lcom/peel/i/a;)Ljava/util/List;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/peel/i/a;->aq:Ljava/util/List;

    return-object v0
.end method

.method static synthetic s(Lcom/peel/i/a;)V
    .locals 0

    .prologue
    .line 82
    invoke-direct {p0}, Lcom/peel/i/a;->ac()V

    return-void
.end method

.method static synthetic t(Lcom/peel/i/a;)Lcom/peel/ui/dg;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/peel/i/a;->aF:Lcom/peel/ui/dg;

    return-object v0
.end method

.method static synthetic u(Lcom/peel/i/a;)Lcom/peel/widget/ag;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/peel/i/a;->aG:Lcom/peel/widget/ag;

    return-object v0
.end method

.method static synthetic v(Lcom/peel/i/a;)Lcom/peel/control/h;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/peel/i/a;->ar:Lcom/peel/control/h;

    return-object v0
.end method

.method static synthetic w(Lcom/peel/i/a;)V
    .locals 0

    .prologue
    .line 82
    invoke-direct {p0}, Lcom/peel/i/a;->af()V

    return-void
.end method

.method static synthetic x(Lcom/peel/i/a;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/peel/i/a;->h:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic y(Lcom/peel/i/a;)Lcom/peel/widget/TestBtnViewPager;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/peel/i/a;->aP:Lcom/peel/widget/TestBtnViewPager;

    return-object v0
.end method

.method static synthetic z(Lcom/peel/i/a;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/peel/i/a;->aJ:Landroid/widget/TextView;

    return-object v0
.end method


# virtual methods
.method public Z()V
    .locals 2

    .prologue
    .line 1070
    iget-object v0, p0, Lcom/peel/i/a;->d:Lcom/peel/d/a;

    if-nez v0, :cond_0

    .line 1075
    :goto_0
    return-void

    .line 1073
    :cond_0
    iget-object v0, p0, Lcom/peel/i/a;->b:Lcom/peel/d/i;

    iget-object v1, p0, Lcom/peel/i/a;->d:Lcom/peel/d/a;

    invoke-virtual {v0, v1}, Lcom/peel/d/i;->a(Lcom/peel/d/a;)V

    goto :goto_0
.end method

.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    .line 161
    iget-object v0, p0, Lcom/peel/i/a;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "test_command"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/i/a;->ay:Ljava/lang/String;

    .line 163
    sget v0, Lcom/peel/ui/fq;->device_setup:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ViewFlipper;

    iput-object v0, p0, Lcom/peel/i/a;->f:Landroid/widget/ViewFlipper;

    .line 165
    invoke-virtual {p0}, Lcom/peel/i/a;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    invoke-static {v0}, Lcom/peel/util/g;->a(Landroid/app/Activity;)V

    .line 166
    iget-object v0, p0, Lcom/peel/i/a;->f:Landroid/widget/ViewFlipper;

    return-object v0
.end method

.method public a(Landroid/view/MenuItem;)Z
    .locals 8

    .prologue
    const/4 v1, 0x1

    .line 1007
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    .line 1008
    sget v2, Lcom/peel/ui/fp;->menu_next:I

    if-ne v0, v2, :cond_1

    .line 1009
    iget-object v0, p0, Lcom/peel/i/a;->f:Landroid/widget/ViewFlipper;

    invoke-virtual {v0}, Landroid/widget/ViewFlipper;->getDisplayedChild()I

    move-result v0

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/peel/i/a;->f:Landroid/widget/ViewFlipper;

    invoke-virtual {v0}, Landroid/widget/ViewFlipper;->getDisplayedChild()I

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/peel/i/a;->am:Lcom/peel/f/a;

    if-eqz v0, :cond_1

    .line 1010
    invoke-static {}, Lcom/peel/util/a/f;->a()Lcom/peel/util/a/f;

    move-result-object v0

    sget-object v2, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v2}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v2

    if-nez v2, :cond_2

    .line 1011
    :goto_0
    const/16 v2, 0xc29

    const/16 v3, 0x7d8

    iget-object v4, p0, Lcom/peel/i/a;->am:Lcom/peel/f/a;

    .line 1012
    invoke-virtual {v4}, Lcom/peel/f/a;->b()Ljava/lang/String;

    move-result-object v4

    iget v5, p0, Lcom/peel/i/a;->ak:I

    iget-object v6, p0, Lcom/peel/i/a;->az:Ljava/lang/String;

    const/4 v7, -0x1

    .line 1010
    invoke-virtual/range {v0 .. v7}, Lcom/peel/util/a/f;->a(IIILjava/lang/String;ILjava/lang/String;I)V

    .line 1013
    invoke-direct {p0}, Lcom/peel/i/a;->ac()V

    .line 1017
    :cond_1
    invoke-super {p0, p1}, Lcom/peel/d/u;->a(Landroid/view/MenuItem;)Z

    move-result v0

    return v0

    .line 1010
    :cond_2
    sget-object v1, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    .line 1011
    invoke-virtual {v1}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/data/at;->f()I

    move-result v1

    goto :goto_0
.end method

.method public b()Z
    .locals 5

    .prologue
    const/4 v4, -0x1

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1022
    iget-boolean v0, p0, Lcom/peel/i/a;->aA:Z

    if-ne v2, v0, :cond_0

    .line 1023
    iput-boolean v1, p0, Lcom/peel/i/a;->aA:Z

    move v0, v1

    .line 1052
    :goto_0
    return v0

    .line 1029
    :cond_0
    iget-object v0, p0, Lcom/peel/i/a;->aC:Landroid/widget/EditText;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/peel/i/a;->aC:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->hasFocus()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1030
    invoke-virtual {p0}, Lcom/peel/i/a;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    const-string/jumbo v3, "input_method"

    invoke-virtual {v0, v3}, Landroid/support/v4/app/ae;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 1031
    iget-object v3, p0, Lcom/peel/i/a;->aC:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v3

    invoke-virtual {v0, v3, v1}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 1033
    :cond_1
    iget-object v0, p0, Lcom/peel/i/a;->f:Landroid/widget/ViewFlipper;

    invoke-virtual {v0}, Landroid/widget/ViewFlipper;->getDisplayedChild()I

    move-result v0

    const/4 v3, 0x2

    if-ne v0, v3, :cond_3

    .line 1035
    iget v0, p0, Lcom/peel/i/a;->g:I

    if-ltz v0, :cond_2

    .line 1036
    iget-object v0, p0, Lcom/peel/i/a;->f:Landroid/widget/ViewFlipper;

    iget v1, p0, Lcom/peel/i/a;->g:I

    invoke-virtual {v0, v1}, Landroid/widget/ViewFlipper;->setDisplayedChild(I)V

    .line 1037
    iput v4, p0, Lcom/peel/i/a;->g:I

    .line 1041
    :goto_1
    invoke-direct {p0}, Lcom/peel/i/a;->U()V

    move v0, v2

    .line 1042
    goto :goto_0

    .line 1039
    :cond_2
    iget-object v0, p0, Lcom/peel/i/a;->f:Landroid/widget/ViewFlipper;

    invoke-virtual {v0, v1}, Landroid/widget/ViewFlipper;->setDisplayedChild(I)V

    goto :goto_1

    .line 1043
    :cond_3
    iget-object v0, p0, Lcom/peel/i/a;->f:Landroid/widget/ViewFlipper;

    invoke-virtual {v0}, Landroid/widget/ViewFlipper;->getDisplayedChild()I

    move-result v0

    if-ne v0, v2, :cond_5

    iget-object v0, p0, Lcom/peel/i/a;->ap:Ljava/util/List;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/peel/i/a;->ap:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_5

    .line 1044
    iget v0, p0, Lcom/peel/i/a;->g:I

    if-ltz v0, :cond_4

    .line 1045
    iput v4, p0, Lcom/peel/i/a;->g:I

    .line 1047
    :cond_4
    iget-object v0, p0, Lcom/peel/i/a;->f:Landroid/widget/ViewFlipper;

    invoke-virtual {v0, v1}, Landroid/widget/ViewFlipper;->setDisplayedChild(I)V

    .line 1048
    invoke-direct {p0}, Lcom/peel/i/a;->U()V

    move v0, v2

    .line 1049
    goto :goto_0

    .line 1052
    :cond_5
    invoke-super {p0}, Lcom/peel/d/u;->b()Z

    move-result v0

    goto :goto_0
.end method

.method public c(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    .line 233
    invoke-super {p0, p1}, Lcom/peel/d/u;->c(Landroid/os/Bundle;)V

    .line 234
    sget-object v0, Lcom/peel/content/a;->h:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-nez v0, :cond_0

    .line 249
    :goto_0
    return-void

    .line 237
    :cond_0
    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v0}, Lcom/peel/control/am;->e()[Lcom/peel/control/h;

    move-result-object v1

    .line 238
    if-eqz v1, :cond_4

    array-length v0, v1

    if-lez v0, :cond_4

    .line 239
    array-length v2, v1

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v2, :cond_4

    aget-object v3, v1, v0

    .line 240
    invoke-virtual {v3}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v4

    invoke-virtual {v4}, Lcom/peel/data/g;->d()I

    move-result v4

    const/4 v5, 0x5

    if-ne v4, v5, :cond_2

    .line 241
    iput-object v3, p0, Lcom/peel/i/a;->at:Lcom/peel/control/h;

    .line 239
    :cond_1
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 242
    :cond_2
    invoke-virtual {v3}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v4

    invoke-virtual {v4}, Lcom/peel/data/g;->d()I

    move-result v4

    const/4 v5, 0x1

    if-eq v4, v5, :cond_3

    invoke-virtual {v3}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v4

    invoke-virtual {v4}, Lcom/peel/data/g;->d()I

    move-result v4

    const/16 v5, 0xa

    if-ne v4, v5, :cond_1

    .line 243
    :cond_3
    iput-object v3, p0, Lcom/peel/i/a;->as:Lcom/peel/control/h;

    goto :goto_2

    .line 248
    :cond_4
    invoke-direct {p0}, Lcom/peel/i/a;->U()V

    goto :goto_0
.end method

.method public d(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v3, -0x1

    .line 202
    invoke-super {p0, p1}, Lcom/peel/d/u;->d(Landroid/os/Bundle;)V

    .line 206
    iget-object v0, p0, Lcom/peel/i/a;->b:Lcom/peel/d/i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/peel/i/a;->b:Lcom/peel/d/i;

    const-class v1, Lcom/peel/i/a;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/peel/d/i;->c(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 207
    new-instance v0, Landroid/os/Bundle;

    iget-object v1, p0, Lcom/peel/i/a;->b:Lcom/peel/d/i;

    const-class v2, Lcom/peel/i/a;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/peel/d/i;->c(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Bundle;-><init>(Landroid/os/Bundle;)V

    invoke-super {p0, v0}, Lcom/peel/d/u;->c(Landroid/os/Bundle;)V

    .line 210
    iget-object v0, p0, Lcom/peel/i/a;->b:Lcom/peel/d/i;

    const-class v1, Lcom/peel/i/a;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/peel/d/i;->c(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    const-string/jumbo v1, "from_feedback"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 212
    iget-object v0, p0, Lcom/peel/i/a;->b:Lcom/peel/d/i;

    const-class v1, Lcom/peel/i/a;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/peel/d/i;->c(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Bundle;->clear()V

    .line 216
    :cond_0
    iget-object v0, p0, Lcom/peel/i/a;->c:Landroid/os/Bundle;

    invoke-static {v0}, Lcom/peel/util/bx;->a(Landroid/os/Bundle;)V

    .line 219
    iget-object v0, p0, Lcom/peel/i/a;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "device_type"

    invoke-virtual {v0, v1, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/peel/i/a;->ak:I

    .line 220
    iget v0, p0, Lcom/peel/i/a;->ak:I

    if-ne v0, v3, :cond_1

    .line 221
    sget-object v0, Lcom/peel/i/a;->e:Ljava/lang/String;

    const-string/jumbo v1, "NO device_type specified in bundle()!"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 222
    sget-object v0, Lcom/peel/i/a;->e:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/peel/i/a;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/peel/d/e;->a(Ljava/lang/String;Landroid/support/v4/app/ae;)V

    .line 225
    :cond_1
    invoke-virtual {p0}, Lcom/peel/i/a;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    iget v1, p0, Lcom/peel/i/a;->ak:I

    invoke-static {v0, v1}, Lcom/peel/util/bx;->a(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/i/a;->az:Ljava/lang/String;

    .line 227
    sget-object v0, Lcom/peel/content/a;->h:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 228
    iget-object v0, p0, Lcom/peel/i/a;->c:Landroid/os/Bundle;

    invoke-virtual {p0, v0}, Lcom/peel/i/a;->c(Landroid/os/Bundle;)V

    .line 229
    :cond_2
    return-void
.end method

.method public f()V
    .locals 3

    .prologue
    .line 170
    invoke-super {p0}, Lcom/peel/d/u;->f()V

    .line 171
    invoke-virtual {p0}, Lcom/peel/i/a;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    const-string/jumbo v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/ae;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 172
    invoke-virtual {p0}, Lcom/peel/i/a;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/ae;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 173
    iget-object v0, p0, Lcom/peel/i/a;->av:Lcom/peel/widget/ag;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/peel/i/a;->av:Lcom/peel/widget/ag;

    invoke-virtual {v0}, Lcom/peel/widget/ag;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 174
    iget-object v0, p0, Lcom/peel/i/a;->av:Lcom/peel/widget/ag;

    invoke-virtual {v0}, Lcom/peel/widget/ag;->dismiss()V

    .line 179
    :cond_0
    iget-object v0, p0, Lcom/peel/i/a;->aG:Lcom/peel/widget/ag;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/peel/i/a;->aG:Lcom/peel/widget/ag;

    invoke-virtual {v0}, Lcom/peel/widget/ag;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 180
    iget-object v0, p0, Lcom/peel/i/a;->aG:Lcom/peel/widget/ag;

    invoke-virtual {v0}, Lcom/peel/widget/ag;->dismiss()V

    .line 182
    :cond_1
    return-void
.end method

.method public g()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1057
    iget-object v0, p0, Lcom/peel/i/a;->ax:Lcom/peel/i/a/a;

    if-eqz v0, :cond_0

    .line 1058
    iput-object v1, p0, Lcom/peel/i/a;->ax:Lcom/peel/i/a/a;

    .line 1061
    :cond_0
    iget-object v0, p0, Lcom/peel/i/a;->ao:Landroid/widget/ListView;

    if-eqz v0, :cond_1

    .line 1062
    iput-object v1, p0, Lcom/peel/i/a;->ao:Landroid/widget/ListView;

    .line 1064
    :cond_1
    invoke-super {p0}, Lcom/peel/d/u;->g()V

    .line 1065
    return-void
.end method

.method public w()V
    .locals 6

    .prologue
    const-wide/16 v4, 0xfa

    .line 185
    invoke-super {p0}, Lcom/peel/d/u;->w()V

    .line 186
    iget-object v0, p0, Lcom/peel/i/a;->aC:Landroid/widget/EditText;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/peel/i/a;->aC:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->isFocused()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 187
    invoke-virtual {p0}, Lcom/peel/i/a;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    const-class v1, Lcom/peel/i/a;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/peel/i/a;->aC:Landroid/widget/EditText;

    invoke-static {v0, v1, v2, v4, v5}, Lcom/peel/util/bx;->a(Landroid/content/Context;Ljava/lang/String;Landroid/widget/EditText;J)V

    .line 190
    :cond_0
    iget-object v0, p0, Lcom/peel/i/a;->aF:Lcom/peel/ui/dg;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/peel/i/a;->aF:Lcom/peel/ui/dg;

    invoke-virtual {v0}, Lcom/peel/ui/dg;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 191
    iget-object v0, p0, Lcom/peel/i/a;->aF:Lcom/peel/ui/dg;

    iget-object v0, v0, Lcom/peel/ui/dg;->b:Landroid/widget/EditText;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/peel/i/a;->aF:Lcom/peel/ui/dg;

    iget-object v0, v0, Lcom/peel/ui/dg;->b:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->isFocused()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 192
    invoke-virtual {p0}, Lcom/peel/i/a;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    const-class v1, Lcom/peel/i/a;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/peel/i/a;->aF:Lcom/peel/ui/dg;

    iget-object v2, v2, Lcom/peel/ui/dg;->b:Landroid/widget/EditText;

    invoke-static {v0, v1, v2, v4, v5}, Lcom/peel/util/bx;->a(Landroid/content/Context;Ljava/lang/String;Landroid/widget/EditText;J)V

    .line 199
    :cond_1
    :goto_0
    return-void

    .line 193
    :cond_2
    iget-object v0, p0, Lcom/peel/i/a;->aF:Lcom/peel/ui/dg;

    iget-object v0, v0, Lcom/peel/ui/dg;->c:Landroid/widget/EditText;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/peel/i/a;->aF:Lcom/peel/ui/dg;

    iget-object v0, v0, Lcom/peel/ui/dg;->c:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->isFocused()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 194
    invoke-virtual {p0}, Lcom/peel/i/a;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    const-class v1, Lcom/peel/i/a;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/peel/i/a;->aF:Lcom/peel/ui/dg;

    iget-object v2, v2, Lcom/peel/ui/dg;->c:Landroid/widget/EditText;

    invoke-static {v0, v1, v2, v4, v5}, Lcom/peel/util/bx;->a(Landroid/content/Context;Ljava/lang/String;Landroid/widget/EditText;J)V

    goto :goto_0

    .line 195
    :cond_3
    iget-object v0, p0, Lcom/peel/i/a;->aF:Lcom/peel/ui/dg;

    iget-object v0, v0, Lcom/peel/ui/dg;->d:Landroid/widget/EditText;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/peel/i/a;->aF:Lcom/peel/ui/dg;

    iget-object v0, v0, Lcom/peel/ui/dg;->d:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->isFocused()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 196
    invoke-virtual {p0}, Lcom/peel/i/a;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    const-class v1, Lcom/peel/i/a;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/peel/i/a;->aF:Lcom/peel/ui/dg;

    iget-object v2, v2, Lcom/peel/ui/dg;->d:Landroid/widget/EditText;

    invoke-static {v0, v1, v2, v4, v5}, Lcom/peel/util/bx;->a(Landroid/content/Context;Ljava/lang/String;Landroid/widget/EditText;J)V

    goto :goto_0
.end method
