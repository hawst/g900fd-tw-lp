.class final Lcom/peel/i/ab;
.super Landroid/text/style/ClickableSpan;


# instance fields
.field final synthetic a:Lcom/peel/i/a;


# direct methods
.method private constructor <init>(Lcom/peel/i/a;)V
    .locals 0

    .prologue
    .line 1243
    iput-object p1, p0, Lcom/peel/i/ab;->a:Lcom/peel/i/a;

    invoke-direct {p0}, Landroid/text/style/ClickableSpan;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/peel/i/a;Lcom/peel/i/b;)V
    .locals 0

    .prologue
    .line 1243
    invoke-direct {p0, p1}, Lcom/peel/i/ab;-><init>(Lcom/peel/i/a;)V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 1246
    iget-object v0, p0, Lcom/peel/i/ab;->a:Lcom/peel/i/a;

    iget-object v0, v0, Lcom/peel/i/a;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "from_feedback"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1247
    iget-object v0, p0, Lcom/peel/i/ab;->a:Lcom/peel/i/a;

    invoke-virtual {v0}, Lcom/peel/i/a;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/i/ab;->a:Lcom/peel/i/a;

    iget-object v1, v1, Lcom/peel/i/a;->b:Lcom/peel/d/i;

    iget-object v2, p0, Lcom/peel/i/ab;->a:Lcom/peel/i/a;

    invoke-static {v2}, Lcom/peel/i/a;->a(Lcom/peel/i/a;)Lcom/peel/f/a;

    move-result-object v2

    invoke-virtual {v2}, Lcom/peel/f/a;->b()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/peel/i/ab;->a:Lcom/peel/i/a;

    invoke-static {v3}, Lcom/peel/i/a;->b(Lcom/peel/i/a;)Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v4, "Power"

    iget-object v5, p0, Lcom/peel/i/ab;->a:Lcom/peel/i/a;

    iget-object v5, v5, Lcom/peel/i/a;->c:Landroid/os/Bundle;

    const-class v6, Lcom/peel/i/a;

    invoke-virtual {v6}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-static/range {v0 .. v6}, Lcom/peel/util/bx;->a(Landroid/content/Context;Lcom/peel/d/i;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Ljava/lang/String;)V

    .line 1248
    iget-object v0, p0, Lcom/peel/i/ab;->a:Lcom/peel/i/a;

    invoke-static {v0}, Lcom/peel/i/a;->y(Lcom/peel/i/a;)Lcom/peel/widget/TestBtnViewPager;

    move-result-object v0

    invoke-virtual {v0, v7}, Lcom/peel/widget/TestBtnViewPager;->setEnabledSwipe(Z)V

    .line 1249
    iget-object v0, p0, Lcom/peel/i/ab;->a:Lcom/peel/i/a;

    invoke-static {v0}, Lcom/peel/i/a;->I(Lcom/peel/i/a;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v7}, Landroid/widget/Button;->setEnabled(Z)V

    .line 1250
    iget-object v0, p0, Lcom/peel/i/ab;->a:Lcom/peel/i/a;

    invoke-static {v0}, Lcom/peel/i/a;->J(Lcom/peel/i/a;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v7}, Landroid/widget/Button;->setEnabled(Z)V

    .line 1251
    return-void
.end method

.method public updateDrawState(Landroid/text/TextPaint;)V
    .locals 1

    .prologue
    .line 1255
    const-string/jumbo v0, "#21dcda"

    invoke-static {v0}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/text/TextPaint;->setColor(I)V

    .line 1256
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/text/TextPaint;->setUnderlineText(Z)V

    .line 1257
    return-void
.end method
