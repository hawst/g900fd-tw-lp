.class Lcom/peel/i/da;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Landroid/os/Bundle;

.field final synthetic b:Lcom/peel/i/cr;


# direct methods
.method constructor <init>(Lcom/peel/i/cr;Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 236
    iput-object p1, p0, Lcom/peel/i/da;->b:Lcom/peel/i/cr;

    iput-object p2, p0, Lcom/peel/i/da;->a:Landroid/os/Bundle;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 9

    .prologue
    const/16 v8, 0x8

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 239
    iget-object v0, p0, Lcom/peel/i/da;->b:Lcom/peel/i/cr;

    invoke-static {v0}, Lcom/peel/i/cr;->a(Lcom/peel/i/cr;)Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->clearFocus()V

    .line 241
    iget-object v0, p0, Lcom/peel/i/da;->b:Lcom/peel/i/cr;

    invoke-static {v0}, Lcom/peel/i/cr;->c(Lcom/peel/i/cr;)Landroid/widget/Button;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/i/da;->a:Landroid/os/Bundle;

    const-string/jumbo v2, "display_name"

    iget-object v3, p0, Lcom/peel/i/da;->a:Landroid/os/Bundle;

    const-string/jumbo v4, "name"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 243
    const-string/jumbo v0, "region"

    iget-object v1, p0, Lcom/peel/i/da;->a:Landroid/os/Bundle;

    const-string/jumbo v2, "type"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string/jumbo v0, "subregion"

    iget-object v1, p0, Lcom/peel/i/da;->a:Landroid/os/Bundle;

    const-string/jumbo v2, "type"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 245
    :cond_0
    iget-object v0, p0, Lcom/peel/i/da;->b:Lcom/peel/i/cr;

    invoke-static {v0}, Lcom/peel/i/cr;->a(Lcom/peel/i/cr;)Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->clearFocus()V

    .line 246
    iget-object v0, p0, Lcom/peel/i/da;->b:Lcom/peel/i/cr;

    invoke-static {v0}, Lcom/peel/i/cr;->d(Lcom/peel/i/cr;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v8}, Landroid/view/View;->setVisibility(I)V

    .line 247
    iget-object v0, p0, Lcom/peel/i/da;->b:Lcom/peel/i/cr;

    invoke-static {v0}, Lcom/peel/i/cr;->e(Lcom/peel/i/cr;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    .line 277
    :goto_0
    return-void

    .line 251
    :cond_1
    iget-object v0, p0, Lcom/peel/i/da;->b:Lcom/peel/i/cr;

    invoke-static {v0}, Lcom/peel/i/cr;->a(Lcom/peel/i/cr;)Landroid/widget/EditText;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 252
    iget-object v0, p0, Lcom/peel/i/da;->b:Lcom/peel/i/cr;

    invoke-static {v0}, Lcom/peel/i/cr;->e(Lcom/peel/i/cr;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v8}, Landroid/view/View;->setVisibility(I)V

    .line 253
    iget-object v0, p0, Lcom/peel/i/da;->b:Lcom/peel/i/cr;

    invoke-static {v0}, Lcom/peel/i/cr;->d(Lcom/peel/i/cr;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    .line 255
    const-string/jumbo v0, "5digitzip"

    iget-object v1, p0, Lcom/peel/i/da;->a:Landroid/os/Bundle;

    const-string/jumbo v2, "type"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 256
    iget-object v0, p0, Lcom/peel/i/da;->b:Lcom/peel/i/cr;

    invoke-static {v0}, Lcom/peel/i/cr;->f(Lcom/peel/i/cr;)Landroid/widget/TextView;

    move-result-object v0

    sget v1, Lcom/peel/ui/ft;->enter_us_zip_for_providers:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 257
    iget-object v0, p0, Lcom/peel/i/da;->b:Lcom/peel/i/cr;

    invoke-static {v0}, Lcom/peel/i/cr;->a(Lcom/peel/i/cr;)Landroid/widget/EditText;

    move-result-object v0

    sget v1, Lcom/peel/ui/ft;->enter_us_zip:I

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setHint(I)V

    .line 258
    iget-object v0, p0, Lcom/peel/i/da;->b:Lcom/peel/i/cr;

    invoke-static {v0}, Lcom/peel/i/cr;->a(Lcom/peel/i/cr;)Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0, v7}, Landroid/widget/EditText;->setInputType(I)V

    .line 259
    iget-object v0, p0, Lcom/peel/i/da;->b:Lcom/peel/i/cr;

    invoke-static {v0}, Lcom/peel/i/cr;->a(Lcom/peel/i/cr;)Landroid/widget/EditText;

    move-result-object v0

    new-array v1, v6, [Landroid/text/InputFilter;

    new-instance v2, Landroid/text/InputFilter$LengthFilter;

    const/4 v3, 0x5

    invoke-direct {v2, v3}, Landroid/text/InputFilter$LengthFilter;-><init>(I)V

    aput-object v2, v1, v5

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    .line 273
    :goto_1
    iget-object v0, p0, Lcom/peel/i/da;->b:Lcom/peel/i/cr;

    invoke-static {v0}, Lcom/peel/i/cr;->a(Lcom/peel/i/cr;)Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    .line 274
    iget-object v0, p0, Lcom/peel/i/da;->b:Lcom/peel/i/cr;

    invoke-virtual {v0}, Lcom/peel/i/cr;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    const-string/jumbo v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/ae;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 275
    iget-object v1, p0, Lcom/peel/i/da;->b:Lcom/peel/i/cr;

    invoke-static {v1}, Lcom/peel/i/cr;->a(Lcom/peel/i/cr;)Landroid/widget/EditText;

    move-result-object v1

    invoke-virtual {v0, v1, v6}, Landroid/view/inputmethod/InputMethodManager;->showSoftInput(Landroid/view/View;I)Z

    goto/16 :goto_0

    .line 261
    :cond_2
    iget-object v0, p0, Lcom/peel/i/da;->b:Lcom/peel/i/cr;

    invoke-static {v0}, Lcom/peel/i/cr;->f(Lcom/peel/i/cr;)Landroid/widget/TextView;

    move-result-object v0

    sget v1, Lcom/peel/ui/ft;->enter_postal_code_for_providers:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 262
    iget-object v0, p0, Lcom/peel/i/da;->b:Lcom/peel/i/cr;

    invoke-static {v0}, Lcom/peel/i/cr;->a(Lcom/peel/i/cr;)Landroid/widget/EditText;

    move-result-object v0

    sget v1, Lcom/peel/ui/ft;->enter_postal_code:I

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setHint(I)V

    .line 263
    iget-object v0, p0, Lcom/peel/i/da;->b:Lcom/peel/i/cr;

    invoke-static {v0}, Lcom/peel/i/cr;->a(Lcom/peel/i/cr;)Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/widget/EditText;->setInputType(I)V

    .line 264
    iget-object v0, p0, Lcom/peel/i/da;->b:Lcom/peel/i/cr;

    invoke-static {v0}, Lcom/peel/i/cr;->a(Lcom/peel/i/cr;)Landroid/widget/EditText;

    move-result-object v0

    new-array v1, v7, [Landroid/text/InputFilter;

    new-instance v2, Landroid/text/InputFilter$LengthFilter;

    const/16 v3, 0xc

    invoke-direct {v2, v3}, Landroid/text/InputFilter$LengthFilter;-><init>(I)V

    aput-object v2, v1, v5

    new-instance v2, Lcom/peel/i/db;

    invoke-direct {v2, p0}, Lcom/peel/i/db;-><init>(Lcom/peel/i/da;)V

    aput-object v2, v1, v6

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    goto :goto_1
.end method
