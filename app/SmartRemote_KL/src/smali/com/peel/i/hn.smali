.class Lcom/peel/i/hn;
.super Lcom/peel/util/t;


# instance fields
.field final synthetic a:Lcom/peel/i/gm;


# direct methods
.method constructor <init>(Lcom/peel/i/gm;I)V
    .locals 0

    .prologue
    .line 1533
    iput-object p1, p0, Lcom/peel/i/hn;->a:Lcom/peel/i/gm;

    invoke-direct {p0, p2}, Lcom/peel/util/t;-><init>(I)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    const/4 v4, 0x0

    const/4 v1, 0x0

    .line 1537
    invoke-static {}, Lcom/peel/i/gm;->c()Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/peel/i/hn;->a:Lcom/peel/i/gm;

    invoke-virtual {v2}, Lcom/peel/i/gm;->m()Landroid/support/v4/app/ae;

    move-result-object v2

    invoke-static {v0, v2, v1}, Lcom/peel/util/bx;->a(Ljava/lang/String;Landroid/app/Activity;Z)V

    .line 1538
    sget-boolean v0, Lcom/peel/util/b/a;->a:Z

    if-eqz v0, :cond_0

    .line 1555
    :goto_0
    return-void

    .line 1541
    :cond_0
    iget-boolean v0, p0, Lcom/peel/i/hn;->i:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/peel/i/hn;->j:Ljava/lang/Object;

    if-nez v0, :cond_2

    .line 1542
    :cond_1
    invoke-static {}, Lcom/peel/i/gm;->c()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "unable to get stb brands from cloud"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1543
    iget-object v0, p0, Lcom/peel/i/hn;->a:Lcom/peel/i/gm;

    invoke-virtual {v0}, Lcom/peel/i/gm;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/i/hn;->a:Lcom/peel/i/gm;

    iget-object v1, v1, Lcom/peel/i/gm;->b:Lcom/peel/d/i;

    iget-object v2, p0, Lcom/peel/i/hn;->a:Lcom/peel/i/gm;

    invoke-virtual {v2}, Lcom/peel/i/gm;->m()Landroid/support/v4/app/ae;

    move-result-object v2

    const/4 v3, 0x2

    invoke-static {v2, v3}, Lcom/peel/util/bx;->a(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v4, v2, v4}, Lcom/peel/util/bx;->a(Landroid/content/Context;Lcom/peel/d/i;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1548
    :cond_2
    iget-object v2, p0, Lcom/peel/i/hn;->a:Lcom/peel/i/gm;

    iget-object v0, p0, Lcom/peel/i/hn;->j:Ljava/lang/Object;

    check-cast v0, Ljava/util/List;

    invoke-static {v2, v0}, Lcom/peel/i/gm;->e(Lcom/peel/i/gm;Ljava/util/List;)Ljava/util/List;

    .line 1551
    iget-object v0, p0, Lcom/peel/i/hn;->a:Lcom/peel/i/gm;

    invoke-static {v0}, Lcom/peel/i/gm;->as(Lcom/peel/i/gm;)Ljava/util/List;

    move-result-object v0

    new-instance v2, Lcom/peel/f/b;

    invoke-direct {v2}, Lcom/peel/f/b;-><init>()V

    invoke-static {v0, v2}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 1552
    iget-object v0, p0, Lcom/peel/i/hn;->a:Lcom/peel/i/gm;

    new-instance v2, Lcom/peel/i/a/a;

    iget-object v3, p0, Lcom/peel/i/hn;->a:Lcom/peel/i/gm;

    invoke-virtual {v3}, Lcom/peel/i/gm;->m()Landroid/support/v4/app/ae;

    move-result-object v3

    sget v4, Lcom/peel/ui/fq;->brand_row:I

    iget-object v5, p0, Lcom/peel/i/hn;->a:Lcom/peel/i/gm;

    invoke-static {v5}, Lcom/peel/i/gm;->as(Lcom/peel/i/gm;)Ljava/util/List;

    move-result-object v5

    invoke-direct {v2, v3, v4, v5}, Lcom/peel/i/a/a;-><init>(Landroid/content/Context;ILjava/util/List;)V

    invoke-static {v0, v2}, Lcom/peel/i/gm;->d(Lcom/peel/i/gm;Lcom/peel/i/a/a;)Lcom/peel/i/a/a;

    .line 1553
    iget-object v0, p0, Lcom/peel/i/hn;->a:Lcom/peel/i/gm;

    invoke-static {v0}, Lcom/peel/i/gm;->ao(Lcom/peel/i/gm;)Landroid/widget/ListView;

    move-result-object v0

    iget-object v2, p0, Lcom/peel/i/hn;->a:Lcom/peel/i/gm;

    invoke-static {v2}, Lcom/peel/i/gm;->aq(Lcom/peel/i/gm;)Lcom/peel/i/a/a;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 1554
    iget-object v0, p0, Lcom/peel/i/hn;->a:Lcom/peel/i/gm;

    invoke-static {v0}, Lcom/peel/i/gm;->at(Lcom/peel/i/gm;)Landroid/widget/RelativeLayout;

    move-result-object v2

    iget-object v0, p0, Lcom/peel/i/hn;->a:Lcom/peel/i/gm;

    invoke-static {v0}, Lcom/peel/i/gm;->as(Lcom/peel/i/gm;)Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/peel/i/hn;->a:Lcom/peel/i/gm;

    invoke-static {v0}, Lcom/peel/i/gm;->as(Lcom/peel/i/gm;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/16 v3, 0x9

    if-le v0, v3, :cond_3

    move v0, v1

    :goto_1
    invoke-virtual {v2, v0}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    goto/16 :goto_0

    :cond_3
    const/16 v0, 0x8

    goto :goto_1
.end method
