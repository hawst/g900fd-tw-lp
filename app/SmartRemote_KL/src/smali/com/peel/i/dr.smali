.class public Lcom/peel/i/dr;
.super Lcom/peel/d/t;


# static fields
.field private static final al:Ljava/lang/String;


# instance fields
.field private am:I

.field private an:Ljava/util/Timer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 28
    const-class v0, Lcom/peel/i/dr;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/peel/i/dr;->al:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/peel/d/t;-><init>()V

    .line 30
    const/4 v0, 0x0

    iput v0, p0, Lcom/peel/i/dr;->am:I

    return-void
.end method

.method static synthetic S()Ljava/lang/String;
    .locals 1

    .prologue
    .line 27
    sget-object v0, Lcom/peel/i/dr;->al:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic a(Lcom/peel/i/dr;)I
    .locals 1

    .prologue
    .line 27
    iget v0, p0, Lcom/peel/i/dr;->am:I

    return v0
.end method

.method static synthetic a(Lcom/peel/i/dr;I)I
    .locals 0

    .prologue
    .line 27
    iput p1, p0, Lcom/peel/i/dr;->am:I

    return p1
.end method

.method static synthetic b(Lcom/peel/i/dr;)I
    .locals 2

    .prologue
    .line 27
    iget v0, p0, Lcom/peel/i/dr;->am:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/peel/i/dr;->am:I

    return v0
.end method


# virtual methods
.method public a_(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 35
    invoke-super {p0, p1}, Lcom/peel/d/t;->a_(Landroid/os/Bundle;)V

    .line 36
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/peel/i/dr;->b(Z)V

    .line 37
    return-void
.end method

.method public c(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 7

    .prologue
    const/4 v4, 0x0

    .line 41
    new-instance v6, Lcom/peel/widget/ag;

    invoke-virtual {p0}, Lcom/peel/i/dr;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    invoke-direct {v6, v0}, Lcom/peel/widget/ag;-><init>(Landroid/content/Context;)V

    .line 43
    invoke-virtual {p0}, Lcom/peel/i/dr;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 44
    sget v1, Lcom/peel/ui/fq;->setup_dialog:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 45
    const/4 v1, 0x4

    new-array v2, v1, [I

    sget v1, Lcom/peel/ui/fo;->remote_setup_remote_01:I

    aput v1, v2, v4

    const/4 v1, 0x1

    sget v3, Lcom/peel/ui/fo;->remote_setup_remote_02:I

    aput v3, v2, v1

    const/4 v1, 0x2

    sget v3, Lcom/peel/ui/fo;->remote_setup_remote_03:I

    aput v3, v2, v1

    const/4 v1, 0x3

    sget v3, Lcom/peel/ui/fo;->remote_setup_remote_04:I

    aput v3, v2, v1

    .line 47
    sget v1, Lcom/peel/ui/fp;->continue_btn:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    new-instance v3, Lcom/peel/i/ds;

    invoke-direct {v3, p0, v6}, Lcom/peel/i/ds;-><init>(Lcom/peel/i/dr;Lcom/peel/widget/ag;)V

    invoke-virtual {v1, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 61
    invoke-virtual {v6, v0}, Lcom/peel/widget/ag;->a(Landroid/view/View;)Lcom/peel/widget/ag;

    move-result-object v1

    invoke-virtual {v1, v4}, Lcom/peel/widget/ag;->a(Z)Lcom/peel/widget/ag;

    .line 62
    invoke-virtual {v6, v6}, Lcom/peel/widget/ag;->a(Lcom/peel/widget/ag;)Lcom/peel/widget/ag;

    .line 63
    invoke-virtual {v6}, Lcom/peel/widget/ag;->b()Lcom/peel/widget/ag;

    .line 64
    invoke-virtual {v6}, Lcom/peel/widget/ag;->e()V

    .line 81
    sget v1, Lcom/peel/ui/fp;->img:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 83
    new-instance v1, Lcom/peel/i/dt;

    invoke-direct {v1, p0, v2, v0}, Lcom/peel/i/dt;-><init>(Lcom/peel/i/dr;[ILandroid/widget/ImageView;)V

    .line 99
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, Lcom/peel/i/dr;->an:Ljava/util/Timer;

    .line 100
    iget-object v0, p0, Lcom/peel/i/dr;->an:Ljava/util/Timer;

    const-wide/16 v2, 0x0

    const-wide/16 v4, 0x1f4

    invoke-virtual/range {v0 .. v5}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;JJ)V

    .line 103
    new-instance v0, Lcom/peel/i/dv;

    invoke-direct {v0, p0}, Lcom/peel/i/dv;-><init>(Lcom/peel/i/dr;)V

    invoke-virtual {v6, v0}, Lcom/peel/widget/ag;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V

    .line 113
    return-object v6
.end method

.method public y()V
    .locals 6

    .prologue
    .line 118
    iget-object v0, p0, Lcom/peel/i/dr;->an:Ljava/util/Timer;

    if-eqz v0, :cond_0

    .line 119
    iget-object v0, p0, Lcom/peel/i/dr;->an:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 122
    :cond_0
    const-class v0, Lcom/peel/i/dr;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "enable remote icon"

    new-instance v2, Lcom/peel/i/dw;

    invoke-direct {v2, p0}, Lcom/peel/i/dw;-><init>(Lcom/peel/i/dr;)V

    const-wide/16 v4, 0x12c

    invoke-static {v0, v1, v2, v4, v5}, Lcom/peel/util/i;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;J)Ljava/lang/Runnable;

    .line 129
    invoke-super {p0}, Lcom/peel/d/t;->y()V

    .line 130
    return-void
.end method
