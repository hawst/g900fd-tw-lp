.class Lcom/peel/i/gd;
.super Lcom/peel/util/t;


# instance fields
.field final synthetic a:Landroid/os/Bundle;

.field final synthetic b:Lcom/peel/i/fv;


# direct methods
.method constructor <init>(Lcom/peel/i/fv;ILandroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 179
    iput-object p1, p0, Lcom/peel/i/gd;->b:Lcom/peel/i/fv;

    iput-object p3, p0, Lcom/peel/i/gd;->a:Landroid/os/Bundle;

    invoke-direct {p0, p2}, Lcom/peel/util/t;-><init>(I)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 9

    .prologue
    const/4 v2, 0x0

    const/4 v4, 0x0

    .line 183
    :try_start_0
    iget-object v0, p0, Lcom/peel/i/gd;->b:Lcom/peel/i/fv;

    iget-object v0, v0, Lcom/peel/i/fv;->b:Lcom/peel/d/i;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    .line 270
    iget-object v0, p0, Lcom/peel/i/gd;->b:Lcom/peel/i/fv;

    invoke-static {v0}, Lcom/peel/i/fv;->b(Lcom/peel/i/fv;)V

    .line 272
    :goto_0
    return-void

    .line 185
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/peel/i/gd;->b:Lcom/peel/i/fv;

    invoke-static {v0}, Lcom/peel/i/fv;->e(Lcom/peel/i/fv;)Landroid/widget/Button;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 187
    iget-boolean v0, p0, Lcom/peel/i/gd;->i:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/peel/i/gd;->j:Ljava/lang/Object;

    if-nez v0, :cond_2

    .line 188
    :cond_1
    iget-object v0, p0, Lcom/peel/i/gd;->a:Landroid/os/Bundle;

    const-string/jumbo v1, "countries"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->remove(Ljava/lang/String;)V

    .line 189
    iget-object v0, p0, Lcom/peel/i/gd;->b:Lcom/peel/i/fv;

    invoke-virtual {v0}, Lcom/peel/i/fv;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    sget v1, Lcom/peel/ui/ft;->nointernetconnectionalert:I

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 192
    iget-object v0, p0, Lcom/peel/i/gd;->b:Lcom/peel/i/fv;

    invoke-static {v0}, Lcom/peel/i/fv;->e(Lcom/peel/i/fv;)Landroid/widget/Button;

    move-result-object v0

    sget v1, Lcom/peel/ui/ft;->retry:I

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 270
    iget-object v0, p0, Lcom/peel/i/gd;->b:Lcom/peel/i/fv;

    invoke-static {v0}, Lcom/peel/i/fv;->b(Lcom/peel/i/fv;)V

    goto :goto_0

    .line 196
    :cond_2
    :try_start_2
    iget-object v0, p0, Lcom/peel/i/gd;->b:Lcom/peel/i/fv;

    invoke-static {v0}, Lcom/peel/i/fv;->e(Lcom/peel/i/fv;)Landroid/widget/Button;

    move-result-object v0

    sget v1, Lcom/peel/ui/ft;->start_txt:I

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    .line 198
    iget-object v0, p0, Lcom/peel/i/gd;->j:Ljava/lang/Object;

    check-cast v0, Ljava/util/ArrayList;

    .line 205
    new-instance v5, Landroid/os/Bundle;

    invoke-direct {v5}, Landroid/os/Bundle;-><init>()V

    .line 207
    iget-object v1, p0, Lcom/peel/i/gd;->b:Lcom/peel/i/fv;

    invoke-virtual {v1}, Lcom/peel/i/fv;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    invoke-static {v1}, Lcom/peel/util/bx;->c(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 209
    iget-object v3, p0, Lcom/peel/i/gd;->b:Lcom/peel/i/fv;

    invoke-static {v3}, Lcom/peel/i/fv;->f(Lcom/peel/i/fv;)Ljava/lang/String;

    move-result-object v3

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "\n\n value: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v3, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 211
    const-string/jumbo v3, "china"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_3

    const-string/jumbo v3, "cn"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_8

    .line 212
    :cond_3
    const-string/jumbo v1, "CN"

    .line 213
    const-string/jumbo v3, "Taiwan"

    const-string/jumbo v6, "Taiwan"

    invoke-virtual {v5, v3, v6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 214
    const-string/jumbo v3, "Hong Kong"

    const-string/jumbo v6, "Hong Kong / China"

    invoke-virtual {v5, v3, v6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 229
    :goto_1
    if-nez v1, :cond_4

    .line 231
    iget-object v1, p0, Lcom/peel/i/gd;->b:Lcom/peel/i/fv;

    invoke-virtual {v1}, Lcom/peel/i/fv;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    const-string/jumbo v3, "phone"

    invoke-virtual {v1, v3}, Landroid/support/v4/app/ae;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/telephony/TelephonyManager;

    check-cast v1, Landroid/telephony/TelephonyManager;

    .line 232
    if-nez v1, :cond_d

    move-object v1, v2

    .line 236
    :cond_4
    :goto_2
    if-nez v1, :cond_5

    .line 237
    iget-object v1, p0, Lcom/peel/i/gd;->b:Lcom/peel/i/fv;

    invoke-virtual {v1}, Lcom/peel/i/fv;->n()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget-object v1, v1, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v1}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v1

    .line 241
    :cond_5
    if-eqz v1, :cond_e

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_e

    invoke-virtual {v1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v1

    move-object v3, v1

    .line 242
    :goto_3
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-ge v4, v1, :cond_f

    .line 243
    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/Bundle;

    const-string/jumbo v2, "iso"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 244
    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v6, v1}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 245
    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/Bundle;

    const-string/jumbo v7, "display_name"

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/os/Bundle;

    const-string/jumbo v8, "name"

    invoke-virtual {v2, v8}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v5, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v7, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 249
    :cond_6
    invoke-virtual {v6, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 250
    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/Bundle;

    .line 251
    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 252
    const/4 v2, 0x0

    invoke-virtual {v0, v2, v1}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 242
    :cond_7
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    goto :goto_3

    .line 215
    :cond_8
    const-string/jumbo v3, "hong kong"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_9

    const-string/jumbo v3, "hk"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_a

    .line 216
    :cond_9
    const-string/jumbo v1, "HK"

    .line 217
    const-string/jumbo v3, "tw"

    const/4 v6, -0x1

    invoke-virtual {v5, v3, v6}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 218
    const-string/jumbo v3, "Taiwan"

    const-string/jumbo v6, "Taiwan"

    invoke-virtual {v5, v3, v6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 219
    const-string/jumbo v3, "Hong Kong"

    const-string/jumbo v6, "Hong Kong / China"

    invoke-virtual {v5, v3, v6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_1

    .line 270
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/peel/i/gd;->b:Lcom/peel/i/fv;

    invoke-static {v1}, Lcom/peel/i/fv;->b(Lcom/peel/i/fv;)V

    throw v0

    .line 220
    :cond_a
    :try_start_3
    const-string/jumbo v3, "taiwan"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_b

    const-string/jumbo v3, "tw"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_c

    .line 221
    :cond_b
    const-string/jumbo v1, "TW"

    .line 222
    const-string/jumbo v3, "Hong Kong"

    const-string/jumbo v6, "Hong Kong / China"

    invoke-virtual {v5, v3, v6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 224
    :cond_c
    const-string/jumbo v1, "Hong Kong"

    const-string/jumbo v3, "Hong Kong / China"

    invoke-virtual {v5, v1, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    move-object v1, v2

    goto/16 :goto_1

    .line 232
    :cond_d
    invoke-virtual {v1}, Landroid/telephony/TelephonyManager;->getNetworkCountryIso()Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_2

    .line 241
    :cond_e
    iget-object v1, p0, Lcom/peel/i/gd;->b:Lcom/peel/i/fv;

    invoke-virtual {v1}, Lcom/peel/i/fv;->n()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget-object v1, v1, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v1}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v1

    move-object v3, v1

    goto/16 :goto_3

    .line 256
    :cond_f
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 257
    const-string/jumbo v2, "name"

    iget-object v3, p0, Lcom/peel/i/gd;->b:Lcom/peel/i/fv;

    sget v4, Lcom/peel/ui/ft;->other_countries:I

    invoke-virtual {v3, v4}, Lcom/peel/i/fv;->a(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 258
    const-string/jumbo v2, "endpoint"

    const-string/jumbo v3, "usa"

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 259
    const-string/jumbo v2, "iso"

    const-string/jumbo v3, "XX"

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 260
    const-string/jumbo v2, "type"

    const-string/jumbo v3, "none"

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 261
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 263
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_10

    .line 264
    iget-object v1, p0, Lcom/peel/i/gd;->a:Landroid/os/Bundle;

    const-string/jumbo v2, "countries"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 270
    :goto_4
    iget-object v0, p0, Lcom/peel/i/gd;->b:Lcom/peel/i/fv;

    invoke-static {v0}, Lcom/peel/i/fv;->b(Lcom/peel/i/fv;)V

    goto/16 :goto_0

    .line 266
    :cond_10
    :try_start_4
    iget-object v0, p0, Lcom/peel/i/gd;->a:Landroid/os/Bundle;

    const-string/jumbo v1, "countries"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->remove(Ljava/lang/String;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_4
.end method
