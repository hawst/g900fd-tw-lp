.class final Lcom/peel/i/in;
.super Landroid/support/v4/view/av;


# instance fields
.field final synthetic a:Lcom/peel/i/gm;

.field private b:I

.field private c:I


# direct methods
.method public constructor <init>(Lcom/peel/i/gm;II)V
    .locals 0

    .prologue
    .line 2229
    iput-object p1, p0, Lcom/peel/i/in;->a:Lcom/peel/i/gm;

    invoke-direct {p0}, Landroid/support/v4/view/av;-><init>()V

    .line 2230
    iput p2, p0, Lcom/peel/i/in;->b:I

    .line 2231
    iput p3, p0, Lcom/peel/i/in;->c:I

    .line 2232
    return-void
.end method


# virtual methods
.method public a(Landroid/view/ViewGroup;I)Ljava/lang/Object;
    .locals 10

    .prologue
    const/4 v4, 0x2

    const/4 v9, 0x1

    const v8, 0x106000d

    const/4 v7, 0x0

    const/4 v6, -0x2

    .line 2235
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string/jumbo v1, "layout_inflater"

    .line 2236
    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 2238
    iget v1, p0, Lcom/peel/i/in;->b:I

    if-ne v1, v4, :cond_0

    sget v1, Lcom/peel/ui/fq;->test_other_btn_pagers_view:I

    .line 2240
    :goto_0
    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .line 2241
    sget v0, Lcom/peel/ui/fp;->button_num_text:I

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 2242
    sget v1, Lcom/peel/ui/fp;->button_num_text_small:I

    invoke-virtual {v3, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 2244
    iget v2, p0, Lcom/peel/i/in;->b:I

    if-ne v2, v4, :cond_1

    .line 2245
    sget v2, Lcom/peel/ui/fp;->test_other_btn_large_view:I

    invoke-virtual {v3, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    sget v4, Lcom/peel/ui/fo;->setup_test_stb_ch_up_btn_states:I

    invoke-virtual {v2, v4}, Landroid/view/View;->setBackgroundResource(I)V

    .line 2246
    sget v2, Lcom/peel/ui/fp;->test_other_btn_small_view:I

    invoke-virtual {v3, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    sget v4, Lcom/peel/ui/fo;->setup_test_stb_ch_up_btn_states:I

    invoke-virtual {v2, v4}, Landroid/view/View;->setBackgroundResource(I)V

    .line 2258
    new-instance v4, Landroid/widget/Button;

    iget-object v2, p0, Lcom/peel/i/in;->a:Lcom/peel/i/gm;

    invoke-virtual {v2}, Lcom/peel/i/gm;->m()Landroid/support/v4/app/ae;

    move-result-object v2

    invoke-direct {v4, v2}, Landroid/widget/Button;-><init>(Landroid/content/Context;)V

    .line 2259
    new-instance v2, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v2, v6, v6}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 2260
    const/16 v5, 0x66

    invoke-static {v5}, Lcom/peel/util/dw;->a(I)I

    move-result v5

    iput v5, v2, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    .line 2261
    const/16 v5, 0x69

    invoke-static {v5}, Lcom/peel/util/dw;->a(I)I

    move-result v5

    iput v5, v2, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    .line 2262
    invoke-virtual {v4, v2}, Landroid/widget/Button;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2264
    iget-object v2, p0, Lcom/peel/i/in;->a:Lcom/peel/i/gm;

    invoke-virtual {v2}, Lcom/peel/i/gm;->n()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v8}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v4, v2}, Landroid/widget/Button;->setBackgroundColor(I)V

    .line 2265
    iget-object v2, p0, Lcom/peel/i/in;->a:Lcom/peel/i/gm;

    sget v5, Lcom/peel/ui/ft;->channel:I

    invoke-virtual {v2, v5}, Lcom/peel/i/gm;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Landroid/widget/Button;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2267
    new-instance v2, Lcom/peel/i/io;

    invoke-direct {v2, p0}, Lcom/peel/i/io;-><init>(Lcom/peel/i/in;)V

    invoke-virtual {v4, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2274
    sget v2, Lcom/peel/ui/fp;->test_other_btn_large_view:I

    invoke-virtual {v3, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v4, v7}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;I)V

    .line 2286
    new-instance v4, Landroid/widget/Button;

    iget-object v2, p0, Lcom/peel/i/in;->a:Lcom/peel/i/gm;

    invoke-virtual {v2}, Lcom/peel/i/gm;->m()Landroid/support/v4/app/ae;

    move-result-object v2

    invoke-direct {v4, v2}, Landroid/widget/Button;-><init>(Landroid/content/Context;)V

    .line 2287
    new-instance v2, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v2, v6, v6}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 2288
    const/16 v5, 0x47

    invoke-static {v5}, Lcom/peel/util/dw;->a(I)I

    move-result v5

    iput v5, v2, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    .line 2289
    const/16 v5, 0x48

    invoke-static {v5}, Lcom/peel/util/dw;->a(I)I

    move-result v5

    iput v5, v2, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    .line 2290
    invoke-virtual {v4, v2}, Landroid/widget/Button;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2292
    iget-object v2, p0, Lcom/peel/i/in;->a:Lcom/peel/i/gm;

    invoke-virtual {v2}, Lcom/peel/i/gm;->n()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v8}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v4, v2}, Landroid/widget/Button;->setBackgroundColor(I)V

    .line 2293
    iget-object v2, p0, Lcom/peel/i/in;->a:Lcom/peel/i/gm;

    sget v5, Lcom/peel/ui/ft;->channel:I

    invoke-virtual {v2, v5}, Lcom/peel/i/gm;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Landroid/widget/Button;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2295
    new-instance v2, Lcom/peel/i/ip;

    invoke-direct {v2, p0}, Lcom/peel/i/ip;-><init>(Lcom/peel/i/in;)V

    invoke-virtual {v4, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2302
    sget v2, Lcom/peel/ui/fp;->test_other_btn_small_view:I

    invoke-virtual {v3, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v4, v7}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;I)V

    .line 2364
    :goto_1
    iget-object v2, p0, Lcom/peel/i/in;->a:Lcom/peel/i/gm;

    invoke-virtual {v2}, Lcom/peel/i/gm;->n()Landroid/content/res/Resources;

    move-result-object v2

    sget v4, Lcom/peel/ui/ft;->testing_btn_number:I

    new-array v5, v9, [Ljava/lang/Object;

    add-int/lit8 v6, p2, 0x1

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-virtual {v2, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2365
    iget-object v0, p0, Lcom/peel/i/in;->a:Lcom/peel/i/gm;

    invoke-virtual {v0}, Lcom/peel/i/gm;->n()Landroid/content/res/Resources;

    move-result-object v0

    sget v2, Lcom/peel/ui/ft;->testing_btn_number:I

    new-array v4, v9, [Ljava/lang/Object;

    add-int/lit8 v5, p2, 0x1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v7

    invoke-virtual {v0, v2, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2367
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "btnView"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 2368
    invoke-virtual {p1, v3}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 2370
    return-object v3

    .line 2238
    :cond_0
    sget v1, Lcom/peel/ui/fq;->test_pw_btn_pagers_view:I

    goto/16 :goto_0

    .line 2304
    :cond_1
    sget v2, Lcom/peel/ui/fp;->test_pw_btn_large_view:I

    invoke-virtual {v3, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    sget v4, Lcom/peel/ui/fo;->initial_tv_power_onoff_stateful:I

    invoke-virtual {v2, v4}, Landroid/view/View;->setBackgroundResource(I)V

    .line 2305
    sget v2, Lcom/peel/ui/fp;->test_pw_btn_small_view:I

    invoke-virtual {v3, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    sget v4, Lcom/peel/ui/fo;->initial_tv_power_onoff_stateful:I

    invoke-virtual {v2, v4}, Landroid/view/View;->setBackgroundResource(I)V

    .line 2317
    new-instance v4, Landroid/widget/Button;

    iget-object v2, p0, Lcom/peel/i/in;->a:Lcom/peel/i/gm;

    invoke-virtual {v2}, Lcom/peel/i/gm;->m()Landroid/support/v4/app/ae;

    move-result-object v2

    invoke-direct {v4, v2}, Landroid/widget/Button;-><init>(Landroid/content/Context;)V

    .line 2318
    new-instance v2, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v2, v6, v6}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 2319
    const/16 v5, 0x66

    invoke-static {v5}, Lcom/peel/util/dw;->a(I)I

    move-result v5

    iput v5, v2, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    .line 2320
    const/16 v5, 0x69

    invoke-static {v5}, Lcom/peel/util/dw;->a(I)I

    move-result v5

    iput v5, v2, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    .line 2321
    invoke-virtual {v4, v2}, Landroid/widget/Button;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2323
    iget-object v2, p0, Lcom/peel/i/in;->a:Lcom/peel/i/gm;

    invoke-virtual {v2}, Lcom/peel/i/gm;->n()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v8}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v4, v2}, Landroid/widget/Button;->setBackgroundColor(I)V

    .line 2324
    iget-object v2, p0, Lcom/peel/i/in;->a:Lcom/peel/i/gm;

    sget v5, Lcom/peel/ui/ft;->power:I

    invoke-virtual {v2, v5}, Lcom/peel/i/gm;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Landroid/widget/Button;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2326
    new-instance v2, Lcom/peel/i/iq;

    invoke-direct {v2, p0}, Lcom/peel/i/iq;-><init>(Lcom/peel/i/in;)V

    invoke-virtual {v4, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2333
    sget v2, Lcom/peel/ui/fp;->test_pw_btn_large_view:I

    invoke-virtual {v3, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v4, v7}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;I)V

    .line 2345
    new-instance v4, Landroid/widget/Button;

    iget-object v2, p0, Lcom/peel/i/in;->a:Lcom/peel/i/gm;

    invoke-virtual {v2}, Lcom/peel/i/gm;->m()Landroid/support/v4/app/ae;

    move-result-object v2

    invoke-direct {v4, v2}, Landroid/widget/Button;-><init>(Landroid/content/Context;)V

    .line 2346
    new-instance v2, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v2, v6, v6}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 2347
    const/16 v5, 0x47

    invoke-static {v5}, Lcom/peel/util/dw;->a(I)I

    move-result v5

    iput v5, v2, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    .line 2348
    const/16 v5, 0x48

    invoke-static {v5}, Lcom/peel/util/dw;->a(I)I

    move-result v5

    iput v5, v2, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    .line 2349
    invoke-virtual {v4, v2}, Landroid/widget/Button;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2351
    iget-object v2, p0, Lcom/peel/i/in;->a:Lcom/peel/i/gm;

    invoke-virtual {v2}, Lcom/peel/i/gm;->n()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v8}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v4, v2}, Landroid/widget/Button;->setBackgroundColor(I)V

    .line 2352
    iget-object v2, p0, Lcom/peel/i/in;->a:Lcom/peel/i/gm;

    sget v5, Lcom/peel/ui/ft;->power:I

    invoke-virtual {v2, v5}, Lcom/peel/i/gm;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Landroid/widget/Button;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2354
    new-instance v2, Lcom/peel/i/ir;

    invoke-direct {v2, p0}, Lcom/peel/i/ir;-><init>(Lcom/peel/i/in;)V

    invoke-virtual {v4, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2361
    sget v2, Lcom/peel/ui/fp;->test_pw_btn_small_view:I

    invoke-virtual {v3, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v4, v7}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;I)V

    goto/16 :goto_1
.end method

.method public a(Landroid/view/ViewGroup;ILjava/lang/Object;)V
    .locals 0

    .prologue
    .line 2375
    check-cast p3, Landroid/view/View;

    invoke-virtual {p1, p3}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 2376
    return-void
.end method

.method public a(Landroid/view/View;Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2380
    check-cast p2, Landroid/view/View;

    if-ne p1, p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()I
    .locals 1

    .prologue
    .line 2385
    iget v0, p0, Lcom/peel/i/in;->c:I

    return v0
.end method
