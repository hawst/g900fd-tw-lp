.class Lcom/peel/i/o;
.super Lcom/peel/util/t;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/peel/util/t",
        "<",
        "Ljava/util/Map",
        "<",
        "Ljava/lang/String;",
        "Ljava/util/Map",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Object;",
        ">;>;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/peel/i/n;


# direct methods
.method constructor <init>(Lcom/peel/i/n;I)V
    .locals 0

    .prologue
    .line 674
    iput-object p1, p0, Lcom/peel/i/o;->a:Lcom/peel/i/n;

    invoke-direct {p0, p2}, Lcom/peel/util/t;-><init>(I)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 13

    .prologue
    const/16 v12, 0x12

    const/4 v11, 0x5

    const/4 v10, 0x0

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 678
    invoke-static {}, Lcom/peel/i/a;->c()Ljava/lang/String;

    move-result-object v0

    iget-object v3, p0, Lcom/peel/i/o;->a:Lcom/peel/i/n;

    iget-object v3, v3, Lcom/peel/i/n;->a:Lcom/peel/i/a;

    invoke-virtual {v3}, Lcom/peel/i/a;->m()Landroid/support/v4/app/ae;

    move-result-object v3

    invoke-static {v0, v3, v1}, Lcom/peel/util/bx;->a(Ljava/lang/String;Landroid/app/Activity;Z)V

    .line 680
    iget-boolean v0, p0, Lcom/peel/i/o;->i:Z

    if-nez v0, :cond_0

    .line 681
    invoke-static {}, Lcom/peel/i/a;->c()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "unable to getAllIrCodesByCodesetid"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 682
    iget-object v0, p0, Lcom/peel/i/o;->a:Lcom/peel/i/n;

    iget-object v0, v0, Lcom/peel/i/n;->a:Lcom/peel/i/a;

    invoke-static {v0}, Lcom/peel/i/a;->j(Lcom/peel/i/a;)V

    .line 802
    :goto_0
    return-void

    .line 686
    :cond_0
    iget-object v0, p0, Lcom/peel/i/o;->a:Lcom/peel/i/n;

    iget-object v0, v0, Lcom/peel/i/n;->a:Lcom/peel/i/a;

    invoke-static {v0}, Lcom/peel/i/a;->v(Lcom/peel/i/a;)Lcom/peel/control/h;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v3

    iget-object v0, p0, Lcom/peel/i/o;->a:Lcom/peel/i/n;

    iget-object v0, v0, Lcom/peel/i/n;->a:Lcom/peel/i/a;

    invoke-static {v0}, Lcom/peel/i/a;->A(Lcom/peel/i/a;)I

    move-result v4

    iget-object v0, p0, Lcom/peel/i/o;->j:Ljava/lang/Object;

    check-cast v0, Ljava/util/Map;

    invoke-virtual {v3, v4, v0}, Lcom/peel/data/g;->a(ILjava/util/Map;)V

    .line 687
    iget-object v0, p0, Lcom/peel/i/o;->a:Lcom/peel/i/n;

    iget-object v0, v0, Lcom/peel/i/n;->a:Lcom/peel/i/a;

    invoke-static {v0}, Lcom/peel/i/a;->v(Lcom/peel/i/a;)Lcom/peel/control/h;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/peel/control/h;->a(I)V

    .line 688
    iget-object v0, p0, Lcom/peel/i/o;->a:Lcom/peel/i/n;

    iget-object v0, v0, Lcom/peel/i/n;->a:Lcom/peel/i/a;

    iget-object v3, p0, Lcom/peel/i/o;->a:Lcom/peel/i/n;

    iget-object v3, v3, Lcom/peel/i/n;->a:Lcom/peel/i/a;

    invoke-static {v3}, Lcom/peel/i/a;->b(Lcom/peel/i/a;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/peel/control/a;->a(Ljava/lang/String;)Lcom/peel/control/a;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/peel/i/a;->a(Lcom/peel/i/a;Lcom/peel/control/a;)Lcom/peel/control/a;

    .line 689
    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v0}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v0

    iget-object v3, p0, Lcom/peel/i/o;->a:Lcom/peel/i/n;

    iget-object v3, v3, Lcom/peel/i/n;->a:Lcom/peel/i/a;

    invoke-static {v3}, Lcom/peel/i/a;->C(Lcom/peel/i/a;)Lcom/peel/control/a;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/peel/control/RoomControl;->a(Lcom/peel/control/a;)V

    .line 691
    iget-object v0, p0, Lcom/peel/i/o;->a:Lcom/peel/i/n;

    iget-object v0, v0, Lcom/peel/i/n;->a:Lcom/peel/i/a;

    invoke-static {v0}, Lcom/peel/i/a;->h(Lcom/peel/i/a;)I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 759
    iget-object v0, p0, Lcom/peel/i/o;->a:Lcom/peel/i/n;

    iget-object v0, v0, Lcom/peel/i/n;->a:Lcom/peel/i/a;

    invoke-static {v0}, Lcom/peel/i/a;->C(Lcom/peel/i/a;)Lcom/peel/control/a;

    move-result-object v0

    iget-object v3, p0, Lcom/peel/i/o;->a:Lcom/peel/i/n;

    iget-object v3, v3, Lcom/peel/i/n;->a:Lcom/peel/i/a;

    invoke-static {v3}, Lcom/peel/i/a;->v(Lcom/peel/i/a;)Lcom/peel/control/h;

    move-result-object v3

    new-array v4, v2, [Ljava/lang/Integer;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v1

    invoke-virtual {v0, v3, v10, v4}, Lcom/peel/control/a;->a(Lcom/peel/control/h;Ljava/lang/String;[Ljava/lang/Integer;)Z

    .line 762
    :goto_1
    iget-object v0, p0, Lcom/peel/i/o;->a:Lcom/peel/i/n;

    iget-object v0, v0, Lcom/peel/i/n;->a:Lcom/peel/i/a;

    invoke-static {v0}, Lcom/peel/i/a;->v(Lcom/peel/i/a;)Lcom/peel/control/h;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/data/g;->d()I

    move-result v0

    if-eq v0, v11, :cond_1

    iget-object v0, p0, Lcom/peel/i/o;->a:Lcom/peel/i/n;

    iget-object v0, v0, Lcom/peel/i/n;->a:Lcom/peel/i/a;

    invoke-static {v0}, Lcom/peel/i/a;->v(Lcom/peel/i/a;)Lcom/peel/control/h;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/data/g;->d()I

    move-result v0

    if-eq v0, v12, :cond_1

    iget-object v0, p0, Lcom/peel/i/o;->a:Lcom/peel/i/n;

    iget-object v0, v0, Lcom/peel/i/n;->a:Lcom/peel/i/a;

    invoke-static {v0}, Lcom/peel/i/a;->v(Lcom/peel/i/a;)Lcom/peel/control/h;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/data/g;->d()I

    move-result v0

    if-eq v0, v2, :cond_1

    iget-object v0, p0, Lcom/peel/i/o;->a:Lcom/peel/i/n;

    iget-object v0, v0, Lcom/peel/i/n;->a:Lcom/peel/i/a;

    .line 763
    invoke-static {v0}, Lcom/peel/i/a;->v(Lcom/peel/i/a;)Lcom/peel/control/h;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/data/g;->d()I

    move-result v0

    const/16 v3, 0xa

    if-ne v0, v3, :cond_e

    .line 777
    :cond_1
    :goto_2
    iget-object v0, p0, Lcom/peel/i/o;->a:Lcom/peel/i/n;

    iget-object v0, v0, Lcom/peel/i/n;->a:Lcom/peel/i/a;

    invoke-static {v0}, Lcom/peel/i/a;->h(Lcom/peel/i/a;)I

    move-result v0

    if-eq v0, v2, :cond_2

    iget-object v0, p0, Lcom/peel/i/o;->a:Lcom/peel/i/n;

    iget-object v0, v0, Lcom/peel/i/n;->a:Lcom/peel/i/a;

    invoke-static {v0}, Lcom/peel/i/a;->h(Lcom/peel/i/a;)I

    move-result v0

    const/16 v3, 0xa

    if-ne v0, v3, :cond_11

    .line 778
    :cond_2
    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v0}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/control/RoomControl;->c()[Lcom/peel/control/a;

    move-result-object v3

    array-length v4, v3

    move v0, v1

    :goto_3
    if-ge v0, v4, :cond_11

    aget-object v1, v3, v0

    .line 779
    if-nez v1, :cond_10

    .line 778
    :cond_3
    :goto_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 693
    :sswitch_0
    iget-object v0, p0, Lcom/peel/i/o;->a:Lcom/peel/i/n;

    iget-object v0, v0, Lcom/peel/i/n;->a:Lcom/peel/i/a;

    invoke-static {v0}, Lcom/peel/i/a;->C(Lcom/peel/i/a;)Lcom/peel/control/a;

    move-result-object v0

    iget-object v3, p0, Lcom/peel/i/o;->a:Lcom/peel/i/n;

    iget-object v3, v3, Lcom/peel/i/n;->a:Lcom/peel/i/a;

    invoke-static {v3}, Lcom/peel/i/a;->v(Lcom/peel/i/a;)Lcom/peel/control/h;

    move-result-object v3

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Integer;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v2

    invoke-virtual {v0, v3, v10, v4}, Lcom/peel/control/a;->a(Lcom/peel/control/h;Ljava/lang/String;[Ljava/lang/Integer;)Z

    goto/16 :goto_1

    .line 699
    :sswitch_1
    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v0}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/control/RoomControl;->c()[Lcom/peel/control/a;

    move-result-object v5

    array-length v6, v5

    move v4, v1

    move v0, v1

    :goto_5
    if-ge v4, v6, :cond_5

    aget-object v3, v5, v4

    .line 700
    invoke-virtual {v3}, Lcom/peel/control/a;->e()[Lcom/peel/control/h;

    move-result-object v7

    array-length v8, v7

    move v3, v1

    :goto_6
    if-ge v3, v8, :cond_4

    aget-object v9, v7, v3

    .line 701
    invoke-virtual {v9}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v9

    invoke-virtual {v9}, Lcom/peel/data/g;->d()I

    move-result v9

    if-ne v9, v2, :cond_7

    move v0, v2

    .line 706
    :cond_4
    if-eqz v0, :cond_8

    .line 710
    :cond_5
    if-eqz v0, :cond_b

    .line 714
    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v0}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/control/RoomControl;->c()[Lcom/peel/control/a;

    move-result-object v3

    array-length v4, v3

    move v0, v1

    :goto_7
    if-ge v0, v4, :cond_d

    aget-object v5, v3, v0

    .line 715
    invoke-virtual {v5}, Lcom/peel/control/a;->b()Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lcom/peel/i/o;->a:Lcom/peel/i/n;

    iget-object v7, v7, Lcom/peel/i/n;->a:Lcom/peel/i/a;

    invoke-static {v7}, Lcom/peel/i/a;->C(Lcom/peel/i/a;)Lcom/peel/control/a;

    move-result-object v7

    invoke-virtual {v7}, Lcom/peel/control/a;->b()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_9

    .line 714
    :cond_6
    :goto_8
    add-int/lit8 v0, v0, 0x1

    goto :goto_7

    .line 700
    :cond_7
    add-int/lit8 v3, v3, 0x1

    goto :goto_6

    .line 699
    :cond_8
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    goto :goto_5

    .line 717
    :cond_9
    invoke-virtual {v5, v1}, Lcom/peel/control/a;->a(I)Lcom/peel/control/h;

    move-result-object v6

    .line 718
    if-eqz v6, :cond_6

    .line 722
    invoke-virtual {v6}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v7

    invoke-virtual {v7}, Lcom/peel/data/g;->d()I

    move-result v7

    if-eq v7, v11, :cond_6

    invoke-virtual {v6}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v7

    invoke-virtual {v7}, Lcom/peel/data/g;->d()I

    move-result v7

    if-eq v7, v12, :cond_6

    .line 726
    invoke-virtual {v5, v6}, Lcom/peel/control/a;->b(Lcom/peel/control/h;)[Ljava/lang/Integer;

    move-result-object v7

    .line 727
    array-length v7, v7

    if-ne v7, v2, :cond_a

    .line 730
    invoke-virtual {v5, v6, v10, v10}, Lcom/peel/control/a;->b(Lcom/peel/control/h;Ljava/lang/String;[Ljava/lang/Integer;)V

    goto :goto_8

    .line 734
    :cond_a
    new-array v7, v2, [Ljava/lang/Integer;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v1

    invoke-virtual {v5, v6, v10, v7}, Lcom/peel/control/a;->b(Lcom/peel/control/h;Ljava/lang/String;[Ljava/lang/Integer;)V

    goto :goto_8

    .line 739
    :cond_b
    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v0}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/control/RoomControl;->c()[Lcom/peel/control/a;

    move-result-object v3

    array-length v4, v3

    move v0, v1

    :goto_9
    if-ge v0, v4, :cond_d

    aget-object v5, v3, v0

    .line 740
    iget-object v6, p0, Lcom/peel/i/o;->a:Lcom/peel/i/n;

    iget-object v6, v6, Lcom/peel/i/n;->a:Lcom/peel/i/a;

    invoke-static {v6}, Lcom/peel/i/a;->C(Lcom/peel/i/a;)Lcom/peel/control/a;

    move-result-object v6

    invoke-virtual {v6}, Lcom/peel/control/a;->b()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5}, Lcom/peel/control/a;->b()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_c

    .line 741
    invoke-static {}, Lcom/peel/i/a;->c()Ljava/lang/String;

    move-result-object v5

    const-string/jumbo v6, "\nsame activity, continue..."

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 739
    :goto_a
    add-int/lit8 v0, v0, 0x1

    goto :goto_9

    .line 744
    :cond_c
    iget-object v6, p0, Lcom/peel/i/o;->a:Lcom/peel/i/n;

    iget-object v6, v6, Lcom/peel/i/n;->a:Lcom/peel/i/a;

    invoke-static {v6}, Lcom/peel/i/a;->v(Lcom/peel/i/a;)Lcom/peel/control/h;

    move-result-object v6

    new-array v7, v2, [Ljava/lang/Integer;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v1

    invoke-virtual {v5, v6, v10, v7}, Lcom/peel/control/a;->a(Lcom/peel/control/h;Ljava/lang/String;[Ljava/lang/Integer;)Z

    goto :goto_a

    .line 753
    :cond_d
    iget-object v0, p0, Lcom/peel/i/o;->a:Lcom/peel/i/n;

    iget-object v0, v0, Lcom/peel/i/n;->a:Lcom/peel/i/a;

    invoke-static {v0}, Lcom/peel/i/a;->C(Lcom/peel/i/a;)Lcom/peel/control/a;

    move-result-object v0

    iget-object v3, p0, Lcom/peel/i/o;->a:Lcom/peel/i/n;

    iget-object v3, v3, Lcom/peel/i/n;->a:Lcom/peel/i/a;

    invoke-static {v3}, Lcom/peel/i/a;->v(Lcom/peel/i/a;)Lcom/peel/control/h;

    move-result-object v3

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Integer;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v2

    invoke-virtual {v0, v3, v10, v4}, Lcom/peel/control/a;->a(Lcom/peel/control/h;Ljava/lang/String;[Ljava/lang/Integer;)Z

    goto/16 :goto_1

    .line 756
    :sswitch_2
    iget-object v0, p0, Lcom/peel/i/o;->a:Lcom/peel/i/n;

    iget-object v0, v0, Lcom/peel/i/n;->a:Lcom/peel/i/a;

    invoke-static {v0}, Lcom/peel/i/a;->C(Lcom/peel/i/a;)Lcom/peel/control/a;

    move-result-object v0

    iget-object v3, p0, Lcom/peel/i/o;->a:Lcom/peel/i/n;

    iget-object v3, v3, Lcom/peel/i/n;->a:Lcom/peel/i/a;

    invoke-static {v3}, Lcom/peel/i/a;->v(Lcom/peel/i/a;)Lcom/peel/control/h;

    move-result-object v3

    new-array v4, v2, [Ljava/lang/Integer;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v1

    invoke-virtual {v0, v3, v10, v4}, Lcom/peel/control/a;->a(Lcom/peel/control/h;Ljava/lang/String;[Ljava/lang/Integer;)Z

    goto/16 :goto_1

    .line 767
    :cond_e
    iget-object v0, p0, Lcom/peel/i/o;->a:Lcom/peel/i/n;

    iget-object v0, v0, Lcom/peel/i/n;->a:Lcom/peel/i/a;

    invoke-static {v0}, Lcom/peel/i/a;->D(Lcom/peel/i/a;)Lcom/peel/control/h;

    move-result-object v0

    if-eqz v0, :cond_f

    .line 768
    iget-object v0, p0, Lcom/peel/i/o;->a:Lcom/peel/i/n;

    iget-object v0, v0, Lcom/peel/i/n;->a:Lcom/peel/i/a;

    invoke-static {v0}, Lcom/peel/i/a;->C(Lcom/peel/i/a;)Lcom/peel/control/a;

    move-result-object v0

    iget-object v3, p0, Lcom/peel/i/o;->a:Lcom/peel/i/n;

    iget-object v3, v3, Lcom/peel/i/n;->a:Lcom/peel/i/a;

    invoke-static {v3}, Lcom/peel/i/a;->D(Lcom/peel/i/a;)Lcom/peel/control/h;

    move-result-object v3

    new-array v4, v2, [Ljava/lang/Integer;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v1

    invoke-virtual {v0, v3, v10, v4}, Lcom/peel/control/a;->a(Lcom/peel/control/h;Ljava/lang/String;[Ljava/lang/Integer;)Z

    .line 770
    iget-object v0, p0, Lcom/peel/i/o;->a:Lcom/peel/i/n;

    iget-object v0, v0, Lcom/peel/i/n;->a:Lcom/peel/i/a;

    invoke-static {v0}, Lcom/peel/i/a;->E(Lcom/peel/i/a;)Lcom/peel/control/h;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 771
    iget-object v0, p0, Lcom/peel/i/o;->a:Lcom/peel/i/n;

    iget-object v0, v0, Lcom/peel/i/n;->a:Lcom/peel/i/a;

    invoke-static {v0}, Lcom/peel/i/a;->C(Lcom/peel/i/a;)Lcom/peel/control/a;

    move-result-object v0

    iget-object v3, p0, Lcom/peel/i/o;->a:Lcom/peel/i/n;

    iget-object v3, v3, Lcom/peel/i/n;->a:Lcom/peel/i/a;

    invoke-static {v3}, Lcom/peel/i/a;->E(Lcom/peel/i/a;)Lcom/peel/control/h;

    move-result-object v3

    invoke-virtual {v0, v3, v10, v10}, Lcom/peel/control/a;->a(Lcom/peel/control/h;Ljava/lang/String;[Ljava/lang/Integer;)Z

    goto/16 :goto_2

    .line 772
    :cond_f
    iget-object v0, p0, Lcom/peel/i/o;->a:Lcom/peel/i/n;

    iget-object v0, v0, Lcom/peel/i/n;->a:Lcom/peel/i/a;

    invoke-static {v0}, Lcom/peel/i/a;->E(Lcom/peel/i/a;)Lcom/peel/control/h;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 773
    iget-object v0, p0, Lcom/peel/i/o;->a:Lcom/peel/i/n;

    iget-object v0, v0, Lcom/peel/i/n;->a:Lcom/peel/i/a;

    invoke-static {v0}, Lcom/peel/i/a;->C(Lcom/peel/i/a;)Lcom/peel/control/a;

    move-result-object v0

    iget-object v3, p0, Lcom/peel/i/o;->a:Lcom/peel/i/n;

    iget-object v3, v3, Lcom/peel/i/n;->a:Lcom/peel/i/a;

    invoke-static {v3}, Lcom/peel/i/a;->E(Lcom/peel/i/a;)Lcom/peel/control/h;

    move-result-object v3

    new-array v4, v2, [Ljava/lang/Integer;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v1

    invoke-virtual {v0, v3, v10, v4}, Lcom/peel/control/a;->a(Lcom/peel/control/h;Ljava/lang/String;[Ljava/lang/Integer;)Z

    goto/16 :goto_2

    .line 782
    :cond_10
    invoke-virtual {v1}, Lcom/peel/control/a;->b()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/peel/i/o;->a:Lcom/peel/i/n;

    iget-object v6, v6, Lcom/peel/i/n;->a:Lcom/peel/i/a;

    invoke-static {v6}, Lcom/peel/i/a;->C(Lcom/peel/i/a;)Lcom/peel/control/a;

    move-result-object v6

    invoke-virtual {v6}, Lcom/peel/control/a;->b()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_3

    .line 784
    invoke-virtual {v1, v2}, Lcom/peel/control/a;->a(I)Lcom/peel/control/h;

    move-result-object v5

    .line 785
    if-eqz v5, :cond_3

    .line 789
    invoke-virtual {v5}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v6

    invoke-virtual {v6}, Lcom/peel/data/g;->d()I

    move-result v6

    if-eq v6, v11, :cond_3

    invoke-virtual {v5}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v5

    invoke-virtual {v5}, Lcom/peel/data/g;->d()I

    move-result v5

    if-eq v5, v12, :cond_3

    .line 792
    iget-object v5, p0, Lcom/peel/i/o;->a:Lcom/peel/i/n;

    iget-object v5, v5, Lcom/peel/i/n;->a:Lcom/peel/i/a;

    invoke-static {v5}, Lcom/peel/i/a;->v(Lcom/peel/i/a;)Lcom/peel/control/h;

    move-result-object v5

    invoke-virtual {v1, v5, v10, v10}, Lcom/peel/control/a;->a(Lcom/peel/control/h;Ljava/lang/String;[Ljava/lang/Integer;)Z

    goto/16 :goto_4

    .line 796
    :cond_11
    iget-object v0, p0, Lcom/peel/i/o;->a:Lcom/peel/i/n;

    iget-object v0, v0, Lcom/peel/i/n;->a:Lcom/peel/i/a;

    invoke-virtual {v0}, Lcom/peel/i/a;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/ae;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/peel/social/w;->f(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_12

    .line 797
    new-instance v0, Lcom/peel/backup/c;

    iget-object v1, p0, Lcom/peel/i/o;->a:Lcom/peel/i/n;

    iget-object v1, v1, Lcom/peel/i/n;->a:Lcom/peel/i/a;

    invoke-virtual {v1}, Lcom/peel/i/a;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/ae;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/peel/backup/c;-><init>(Landroid/content/Context;)V

    .line 798
    sget-object v1, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v1}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v1

    iget-object v2, p0, Lcom/peel/i/o;->a:Lcom/peel/i/n;

    iget-object v2, v2, Lcom/peel/i/n;->a:Lcom/peel/i/a;

    invoke-static {v2}, Lcom/peel/i/a;->C(Lcom/peel/i/a;)Lcom/peel/control/a;

    move-result-object v2

    iget-object v3, p0, Lcom/peel/i/o;->a:Lcom/peel/i/n;

    iget-object v3, v3, Lcom/peel/i/n;->a:Lcom/peel/i/a;

    invoke-static {v3}, Lcom/peel/i/a;->v(Lcom/peel/i/a;)Lcom/peel/control/h;

    move-result-object v3

    invoke-virtual {v0, v1, v10, v2, v3}, Lcom/peel/backup/c;->a(Lcom/peel/control/RoomControl;Ljava/lang/String;Lcom/peel/control/a;Lcom/peel/control/h;)V

    .line 801
    :cond_12
    iget-object v0, p0, Lcom/peel/i/o;->a:Lcom/peel/i/n;

    iget-object v0, v0, Lcom/peel/i/n;->a:Lcom/peel/i/a;

    invoke-static {v0}, Lcom/peel/i/a;->w(Lcom/peel/i/a;)V

    goto/16 :goto_0

    .line 691
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x5 -> :sswitch_1
        0xa -> :sswitch_2
    .end sparse-switch
.end method
