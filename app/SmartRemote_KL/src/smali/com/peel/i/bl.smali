.class Lcom/peel/i/bl;
.super Lcom/peel/util/t;


# instance fields
.field final synthetic a:Lcom/peel/i/al;


# direct methods
.method constructor <init>(Lcom/peel/i/al;I)V
    .locals 0

    .prologue
    .line 424
    iput-object p1, p0, Lcom/peel/i/bl;->a:Lcom/peel/i/al;

    invoke-direct {p0, p2}, Lcom/peel/util/t;-><init>(I)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 430
    iget-object v0, p0, Lcom/peel/i/bl;->k:Ljava/lang/String;

    const-string/jumbo v1, "total"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 431
    iget-object v1, p0, Lcom/peel/i/bl;->a:Lcom/peel/i/al;

    iget-object v0, p0, Lcom/peel/i/bl;->j:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v1, v0}, Lcom/peel/i/al;->a(Lcom/peel/i/al;I)I

    .line 433
    iget-object v0, p0, Lcom/peel/i/bl;->a:Lcom/peel/i/al;

    invoke-static {v0}, Lcom/peel/i/al;->j(Lcom/peel/i/al;)V

    .line 434
    iget-object v0, p0, Lcom/peel/i/bl;->a:Lcom/peel/i/al;

    invoke-virtual {v0}, Lcom/peel/i/al;->v()Landroid/view/View;

    move-result-object v0

    sget v1, Lcom/peel/ui/fp;->progress_panel:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 435
    iget-object v0, p0, Lcom/peel/i/bl;->a:Lcom/peel/i/al;

    invoke-static {v0}, Lcom/peel/i/al;->y(Lcom/peel/i/al;)Landroid/widget/TextView;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "There are a total of "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/peel/i/bl;->a:Lcom/peel/i/al;

    invoke-static {v2}, Lcom/peel/i/al;->x(Lcom/peel/i/al;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " recordings on your DVR. It will take a few moments to fully load the data. Please be patient."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 436
    iget-object v0, p0, Lcom/peel/i/bl;->a:Lcom/peel/i/al;

    invoke-static {v0}, Lcom/peel/i/al;->z(Lcom/peel/i/al;)Landroid/widget/ProgressBar;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/i/bl;->a:Lcom/peel/i/al;

    invoke-static {v1}, Lcom/peel/i/al;->x(Lcom/peel/i/al;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setMax(I)V

    .line 443
    :goto_0
    return-void

    .line 438
    :cond_0
    iget-object v1, p0, Lcom/peel/i/bl;->a:Lcom/peel/i/al;

    iget-object v0, p0, Lcom/peel/i/bl;->j:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    mul-int/lit8 v0, v0, 0x19

    invoke-static {v1, v0}, Lcom/peel/i/al;->b(Lcom/peel/i/al;I)I

    .line 439
    iget-object v0, p0, Lcom/peel/i/bl;->a:Lcom/peel/i/al;

    invoke-static {v0}, Lcom/peel/i/al;->A(Lcom/peel/i/al;)I

    move-result v0

    iget-object v1, p0, Lcom/peel/i/bl;->a:Lcom/peel/i/al;

    invoke-static {v1}, Lcom/peel/i/al;->x(Lcom/peel/i/al;)I

    move-result v1

    if-le v0, v1, :cond_1

    iget-object v0, p0, Lcom/peel/i/bl;->a:Lcom/peel/i/al;

    iget-object v1, p0, Lcom/peel/i/bl;->a:Lcom/peel/i/al;

    invoke-static {v1}, Lcom/peel/i/al;->x(Lcom/peel/i/al;)I

    move-result v1

    invoke-static {v0, v1}, Lcom/peel/i/al;->b(Lcom/peel/i/al;I)I

    .line 440
    :cond_1
    iget-object v0, p0, Lcom/peel/i/bl;->a:Lcom/peel/i/al;

    invoke-static {v0}, Lcom/peel/i/al;->B(Lcom/peel/i/al;)Landroid/widget/TextView;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Loading "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/peel/i/bl;->a:Lcom/peel/i/al;

    invoke-static {v2}, Lcom/peel/i/al;->A(Lcom/peel/i/al;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " of "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/peel/i/bl;->a:Lcom/peel/i/al;

    invoke-static {v2}, Lcom/peel/i/al;->x(Lcom/peel/i/al;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " recordings..."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 441
    iget-object v0, p0, Lcom/peel/i/bl;->a:Lcom/peel/i/al;

    invoke-static {v0}, Lcom/peel/i/al;->z(Lcom/peel/i/al;)Landroid/widget/ProgressBar;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/i/bl;->a:Lcom/peel/i/al;

    invoke-static {v1}, Lcom/peel/i/al;->A(Lcom/peel/i/al;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setProgress(I)V

    goto :goto_0
.end method
