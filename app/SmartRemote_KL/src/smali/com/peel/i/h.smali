.class Lcom/peel/i/h;
.super Lcom/peel/util/t;


# instance fields
.field final synthetic a:Lcom/peel/i/a;


# direct methods
.method constructor <init>(Lcom/peel/i/a;)V
    .locals 0

    .prologue
    .line 515
    iput-object p1, p0, Lcom/peel/i/h;->a:Lcom/peel/i/a;

    invoke-direct {p0}, Lcom/peel/util/t;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 519
    iget-boolean v0, p0, Lcom/peel/i/h;->i:Z

    if-eqz v0, :cond_0

    .line 521
    iget-object v0, p0, Lcom/peel/i/h;->j:Ljava/lang/Object;

    check-cast v0, Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/f/c;

    invoke-virtual {v0}, Lcom/peel/f/c;->a()I

    move-result v0

    .line 522
    const-string/jumbo v1, "https://partners-ir.peel.com"

    new-instance v2, Lcom/peel/i/i;

    invoke-direct {v2, p0, v0}, Lcom/peel/i/i;-><init>(Lcom/peel/i/h;I)V

    invoke-static {v1, v0, v2}, Lcom/peel/control/aa;->a(Ljava/lang/String;ILcom/peel/util/t;)V

    .line 544
    :goto_0
    return-void

    .line 539
    :cond_0
    invoke-static {}, Lcom/peel/i/a;->c()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/i/h;->a:Lcom/peel/i/a;

    invoke-virtual {v1}, Lcom/peel/i/a;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    invoke-static {v0, v1, v2}, Lcom/peel/util/bx;->a(Ljava/lang/String;Landroid/app/Activity;Z)V

    .line 540
    invoke-static {}, Lcom/peel/i/a;->c()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "getCodesets: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/peel/i/h;->a:Lcom/peel/i/a;

    invoke-static {v2}, Lcom/peel/i/a;->a(Lcom/peel/i/a;)Lcom/peel/f/a;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " type: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/peel/i/h;->a:Lcom/peel/i/a;

    invoke-static {v2}, Lcom/peel/i/a;->h(Lcom/peel/i/a;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " failed!\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/peel/i/h;->k:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/peel/i/h;->j:Ljava/lang/Object;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
