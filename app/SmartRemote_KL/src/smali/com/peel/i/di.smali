.class Lcom/peel/i/di;
.super Lcom/peel/util/t;


# instance fields
.field final synthetic a:Landroid/os/Bundle;

.field final synthetic b:Lcom/peel/i/cr;


# direct methods
.method constructor <init>(Lcom/peel/i/cr;ILandroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 423
    iput-object p1, p0, Lcom/peel/i/di;->b:Lcom/peel/i/cr;

    iput-object p3, p0, Lcom/peel/i/di;->a:Landroid/os/Bundle;

    invoke-direct {p0, p2}, Lcom/peel/util/t;-><init>(I)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 427
    iget-object v0, p0, Lcom/peel/i/di;->b:Lcom/peel/i/cr;

    invoke-static {v0}, Lcom/peel/i/cr;->l(Lcom/peel/i/cr;)Lcom/peel/util/CustomSpinner;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/util/CustomSpinner;->getAdapter()Landroid/widget/SpinnerAdapter;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 428
    iget-object v0, p0, Lcom/peel/i/di;->b:Lcom/peel/i/cr;

    invoke-static {v0}, Lcom/peel/i/cr;->l(Lcom/peel/i/cr;)Lcom/peel/util/CustomSpinner;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/util/CustomSpinner;->getAdapter()Landroid/widget/SpinnerAdapter;

    move-result-object v0

    check-cast v0, Landroid/widget/ArrayAdapter;

    invoke-virtual {v0}, Landroid/widget/ArrayAdapter;->clear()V

    .line 429
    iget-object v0, p0, Lcom/peel/i/di;->b:Lcom/peel/i/cr;

    invoke-static {v0}, Lcom/peel/i/cr;->l(Lcom/peel/i/cr;)Lcom/peel/util/CustomSpinner;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/util/CustomSpinner;->getAdapter()Landroid/widget/SpinnerAdapter;

    move-result-object v0

    check-cast v0, Landroid/widget/ArrayAdapter;

    invoke-virtual {v0}, Landroid/widget/ArrayAdapter;->notifyDataSetChanged()V

    .line 431
    :cond_0
    iget-object v0, p0, Lcom/peel/i/di;->b:Lcom/peel/i/cr;

    invoke-static {v0}, Lcom/peel/i/cr;->m(Lcom/peel/i/cr;)Lcom/peel/util/CustomSpinner;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/util/CustomSpinner;->getAdapter()Landroid/widget/SpinnerAdapter;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 432
    iget-object v0, p0, Lcom/peel/i/di;->b:Lcom/peel/i/cr;

    invoke-static {v0}, Lcom/peel/i/cr;->m(Lcom/peel/i/cr;)Lcom/peel/util/CustomSpinner;

    move-result-object v0

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Lcom/peel/util/CustomSpinner;->setVisibility(I)V

    .line 433
    iget-object v0, p0, Lcom/peel/i/di;->b:Lcom/peel/i/cr;

    invoke-static {v0}, Lcom/peel/i/cr;->m(Lcom/peel/i/cr;)Lcom/peel/util/CustomSpinner;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/util/CustomSpinner;->getAdapter()Landroid/widget/SpinnerAdapter;

    move-result-object v0

    check-cast v0, Landroid/widget/ArrayAdapter;

    invoke-virtual {v0}, Landroid/widget/ArrayAdapter;->clear()V

    .line 434
    iget-object v0, p0, Lcom/peel/i/di;->b:Lcom/peel/i/cr;

    invoke-static {v0}, Lcom/peel/i/cr;->m(Lcom/peel/i/cr;)Lcom/peel/util/CustomSpinner;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/util/CustomSpinner;->getAdapter()Landroid/widget/SpinnerAdapter;

    move-result-object v0

    check-cast v0, Landroid/widget/ArrayAdapter;

    invoke-virtual {v0}, Landroid/widget/ArrayAdapter;->notifyDataSetChanged()V

    .line 436
    :cond_1
    iget-object v0, p0, Lcom/peel/i/di;->b:Lcom/peel/i/cr;

    invoke-virtual {v0}, Lcom/peel/i/cr;->S()V

    .line 437
    iget-boolean v0, p0, Lcom/peel/i/di;->i:Z

    if-nez v0, :cond_3

    .line 512
    :cond_2
    :goto_0
    return-void

    .line 440
    :cond_3
    iget-object v0, p0, Lcom/peel/i/di;->b:Lcom/peel/i/cr;

    invoke-static {v0}, Lcom/peel/i/cr;->l(Lcom/peel/i/cr;)Lcom/peel/util/CustomSpinner;

    move-result-object v0

    new-instance v2, Lcom/peel/i/dj;

    invoke-direct {v2, p0}, Lcom/peel/i/dj;-><init>(Lcom/peel/i/di;)V

    invoke-virtual {v0, v2}, Lcom/peel/util/CustomSpinner;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 463
    iget-object v0, p0, Lcom/peel/i/di;->b:Lcom/peel/i/cr;

    invoke-static {v0}, Lcom/peel/i/cr;->l(Lcom/peel/i/cr;)Lcom/peel/util/CustomSpinner;

    move-result-object v2

    new-instance v3, Lcom/peel/i/dk;

    iget-object v0, p0, Lcom/peel/i/di;->b:Lcom/peel/i/cr;

    invoke-virtual {v0}, Lcom/peel/i/cr;->m()Landroid/support/v4/app/ae;

    move-result-object v4

    sget v5, Lcom/peel/ui/fq;->region_spinner_list_item:I

    iget-object v0, p0, Lcom/peel/i/di;->j:Ljava/lang/Object;

    check-cast v0, Ljava/util/List;

    invoke-direct {v3, p0, v4, v5, v0}, Lcom/peel/i/dk;-><init>(Lcom/peel/i/di;Landroid/content/Context;ILjava/util/List;)V

    invoke-virtual {v2, v3}, Lcom/peel/util/CustomSpinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 484
    iget-object v0, p0, Lcom/peel/i/di;->b:Lcom/peel/i/cr;

    invoke-static {v0}, Lcom/peel/i/cr;->l(Lcom/peel/i/cr;)Lcom/peel/util/CustomSpinner;

    move-result-object v0

    new-instance v2, Lcom/peel/i/dl;

    invoke-direct {v2, p0}, Lcom/peel/i/dl;-><init>(Lcom/peel/i/di;)V

    invoke-virtual {v0, v2}, Lcom/peel/util/CustomSpinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 501
    const-string/jumbo v0, "KR"

    iget-object v2, p0, Lcom/peel/i/di;->a:Landroid/os/Bundle;

    const-string/jumbo v3, "iso"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 502
    :goto_1
    iget-object v0, p0, Lcom/peel/i/di;->b:Lcom/peel/i/cr;

    invoke-static {v0}, Lcom/peel/i/cr;->l(Lcom/peel/i/cr;)Lcom/peel/util/CustomSpinner;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/util/CustomSpinner;->getAdapter()Landroid/widget/SpinnerAdapter;

    move-result-object v0

    invoke-interface {v0}, Landroid/widget/SpinnerAdapter;->getCount()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 504
    const-string/jumbo v2, "\uc11c\uc6b8\ud2b9\ubcc4\uc2dc"

    iget-object v0, p0, Lcom/peel/i/di;->b:Lcom/peel/i/cr;

    invoke-static {v0}, Lcom/peel/i/cr;->l(Lcom/peel/i/cr;)Lcom/peel/util/CustomSpinner;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/util/CustomSpinner;->getAdapter()Landroid/widget/SpinnerAdapter;

    move-result-object v0

    invoke-interface {v0, v1}, Landroid/widget/SpinnerAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 505
    iget-object v0, p0, Lcom/peel/i/di;->b:Lcom/peel/i/cr;

    invoke-static {v0}, Lcom/peel/i/cr;->l(Lcom/peel/i/cr;)Lcom/peel/util/CustomSpinner;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/peel/util/CustomSpinner;->setSelection(I)V

    goto :goto_0

    .line 502
    :cond_4
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 510
    :cond_5
    iget-object v0, p0, Lcom/peel/i/di;->b:Lcom/peel/i/cr;

    invoke-static {v0}, Lcom/peel/i/cr;->l(Lcom/peel/i/cr;)Lcom/peel/util/CustomSpinner;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/peel/util/CustomSpinner;->setSelection(I)V

    goto/16 :goto_0
.end method
