.class public Lcom/peel/i/is;
.super Lcom/peel/d/u;


# static fields
.field private static final e:Ljava/lang/String;


# instance fields
.field private aj:I

.field private f:La/a/a/a;

.field private g:La/a/e;

.field private h:Ljava/lang/String;

.field private i:Landroid/webkit/WebView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 37
    const-class v0, Lcom/peel/i/is;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/peel/i/is;->e:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 36
    invoke-direct {p0}, Lcom/peel/d/u;-><init>()V

    .line 45
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/peel/i/is;->h:Ljava/lang/String;

    return-void
.end method

.method static synthetic a(Lcom/peel/i/is;)La/a/e;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/peel/i/is;->g:La/a/e;

    return-object v0
.end method

.method static synthetic a(Lcom/peel/i/is;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 36
    iput-object p1, p0, Lcom/peel/i/is;->h:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic b(Lcom/peel/i/is;)La/a/a/a;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/peel/i/is;->f:La/a/a/a;

    return-object v0
.end method

.method static synthetic c(Lcom/peel/i/is;)I
    .locals 1

    .prologue
    .line 36
    iget v0, p0, Lcom/peel/i/is;->aj:I

    return v0
.end method

.method static synthetic c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 36
    sget-object v0, Lcom/peel/i/is;->e:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic d(Lcom/peel/i/is;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/peel/i/is;->h:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic e(Lcom/peel/i/is;)Landroid/webkit/WebView;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/peel/i/is;->i:Landroid/webkit/WebView;

    return-object v0
.end method


# virtual methods
.method public Z()V
    .locals 6

    .prologue
    .line 167
    iget-object v0, p0, Lcom/peel/i/is;->d:Lcom/peel/d/a;

    if-nez v0, :cond_0

    .line 168
    new-instance v0, Lcom/peel/d/a;

    sget-object v1, Lcom/peel/d/d;->b:Lcom/peel/d/d;

    sget-object v2, Lcom/peel/d/b;->a:Lcom/peel/d/b;

    sget-object v3, Lcom/peel/d/c;->b:Lcom/peel/d/c;

    sget v4, Lcom/peel/ui/ft;->twitter_signup:I

    invoke-virtual {p0, v4}, Lcom/peel/i/is;->a(I)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, Lcom/peel/d/a;-><init>(Lcom/peel/d/d;Lcom/peel/d/b;Lcom/peel/d/c;Ljava/lang/String;Ljava/util/List;)V

    iput-object v0, p0, Lcom/peel/i/is;->d:Lcom/peel/d/a;

    .line 170
    :cond_0
    iget-object v0, p0, Lcom/peel/i/is;->b:Lcom/peel/d/i;

    iget-object v1, p0, Lcom/peel/i/is;->d:Lcom/peel/d/a;

    invoke-virtual {v0, v1}, Lcom/peel/d/i;->a(Lcom/peel/d/a;)V

    .line 171
    return-void
.end method

.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 61
    sget v0, Lcom/peel/ui/fq;->netflix_setup:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 62
    sget v0, Lcom/peel/ui/fp;->auth_webview_id:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/webkit/WebView;

    iput-object v0, p0, Lcom/peel/i/is;->i:Landroid/webkit/WebView;

    .line 63
    iget-object v0, p0, Lcom/peel/i/is;->i:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V

    .line 64
    iget-object v0, p0, Lcom/peel/i/is;->i:Landroid/webkit/WebView;

    const/16 v2, 0x82

    invoke-virtual {v0, v2}, Landroid/webkit/WebView;->requestFocus(I)Z

    .line 65
    iget-object v0, p0, Lcom/peel/i/is;->i:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/webkit/WebSettings;->setSaveFormData(Z)V

    .line 66
    iget-object v0, p0, Lcom/peel/i/is;->i:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/webkit/WebSettings;->setLoadWithOverviewMode(Z)V

    .line 67
    iget-object v0, p0, Lcom/peel/i/is;->i:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/webkit/WebSettings;->setUseWideViewPort(Z)V

    .line 69
    return-object v1
.end method

.method public a(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 74
    invoke-super {p0, p1, p2}, Lcom/peel/d/u;->a(Landroid/view/View;Landroid/os/Bundle;)V

    .line 76
    iget-object v0, p0, Lcom/peel/i/is;->i:Landroid/webkit/WebView;

    new-instance v1, Lcom/peel/i/it;

    invoke-direct {v1, p0}, Lcom/peel/i/it;-><init>(Lcom/peel/i/is;)V

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 135
    return-void
.end method

.method public a_(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 51
    invoke-super {p0, p1}, Lcom/peel/d/u;->a_(Landroid/os/Bundle;)V

    .line 52
    new-instance v0, La/a/a/a;

    const-string/jumbo v1, "PCuGTY7d0uwotdTUX8x9vw"

    const-string/jumbo v2, "tebwNZxqzjiNNiCrgV66QvZSNs3xbxlvyNOmElwfvg"

    invoke-direct {v0, v1, v2}, La/a/a/a;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/peel/i/is;->f:La/a/a/a;

    .line 53
    new-instance v0, La/a/a/b;

    const-string/jumbo v1, "https://api.twitter.com/oauth/request_token"

    const-string/jumbo v2, "https://api.twitter.com/oauth/access_token"

    const-string/jumbo v3, "https://api.twitter.com/oauth/authorize"

    invoke-direct {v0, v1, v2, v3}, La/a/a/b;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/peel/i/is;->g:La/a/e;

    .line 54
    iget-object v0, p0, Lcom/peel/i/is;->g:La/a/e;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, La/a/e;->a(Z)V

    .line 56
    iget-object v0, p0, Lcom/peel/i/is;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "context_id"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/peel/i/is;->aj:I

    .line 57
    return-void
.end method

.method public d(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 139
    invoke-super {p0, p1}, Lcom/peel/d/u;->d(Landroid/os/Bundle;)V

    .line 141
    const-class v0, Lcom/peel/i/is;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "request twitter setup"

    new-instance v2, Lcom/peel/i/ix;

    invoke-direct {v2, p0}, Lcom/peel/i/ix;-><init>(Lcom/peel/i/is;)V

    invoke-static {v0, v1, v2}, Lcom/peel/util/i;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    .line 163
    return-void
.end method

.method public w()V
    .locals 0

    .prologue
    .line 175
    invoke-super {p0}, Lcom/peel/d/u;->w()V

    .line 176
    invoke-virtual {p0}, Lcom/peel/i/is;->Z()V

    .line 177
    return-void
.end method
