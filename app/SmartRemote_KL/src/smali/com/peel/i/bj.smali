.class Lcom/peel/i/bj;
.super Lcom/peel/util/t;


# instance fields
.field final synthetic a:Lcom/peel/i/al;


# direct methods
.method constructor <init>(Lcom/peel/i/al;I)V
    .locals 0

    .prologue
    .line 402
    iput-object p1, p0, Lcom/peel/i/bj;->a:Lcom/peel/i/al;

    invoke-direct {p0, p2}, Lcom/peel/util/t;-><init>(I)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 405
    iget-object v0, p0, Lcom/peel/i/bj;->a:Lcom/peel/i/al;

    invoke-static {v0}, Lcom/peel/i/al;->v(Lcom/peel/i/al;)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "dtv lib loading finished"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 406
    iget-object v0, p0, Lcom/peel/i/bj;->a:Lcom/peel/i/al;

    invoke-static {v0}, Lcom/peel/i/al;->l(Lcom/peel/i/al;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 407
    iget-object v0, p0, Lcom/peel/i/bj;->a:Lcom/peel/i/al;

    invoke-virtual {v0}, Lcom/peel/i/al;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    const-class v1, Lcom/peel/i/cg;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/peel/d/e;->a(Landroid/support/v4/app/ae;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 421
    :goto_0
    return-void

    .line 409
    :cond_0
    iget-object v0, p0, Lcom/peel/i/bj;->a:Lcom/peel/i/al;

    invoke-static {v0}, Lcom/peel/i/al;->j(Lcom/peel/i/al;)V

    .line 410
    iget-object v0, p0, Lcom/peel/i/bj;->a:Lcom/peel/i/al;

    new-instance v1, Lcom/peel/widget/ag;

    iget-object v2, p0, Lcom/peel/i/bj;->a:Lcom/peel/i/al;

    invoke-virtual {v2}, Lcom/peel/i/al;->m()Landroid/support/v4/app/ae;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/peel/widget/ag;-><init>(Landroid/content/Context;)V

    sget v2, Lcom/peel/ui/ft;->label_success:I

    .line 411
    invoke-virtual {v1, v2}, Lcom/peel/widget/ag;->a(I)Lcom/peel/widget/ag;

    move-result-object v1

    sget v2, Lcom/peel/ui/ft;->desc_dtv_setup_successful:I

    .line 412
    invoke-virtual {v1, v2}, Lcom/peel/widget/ag;->b(I)Lcom/peel/widget/ag;

    move-result-object v1

    sget v2, Lcom/peel/ui/ft;->done:I

    new-instance v3, Lcom/peel/i/bk;

    invoke-direct {v3, p0}, Lcom/peel/i/bk;-><init>(Lcom/peel/i/bj;)V

    .line 413
    invoke-virtual {v1, v2, v3}, Lcom/peel/widget/ag;->a(ILandroid/content/DialogInterface$OnClickListener;)Lcom/peel/widget/ag;

    move-result-object v1

    .line 410
    invoke-static {v0, v1}, Lcom/peel/i/al;->b(Lcom/peel/i/al;Lcom/peel/widget/ag;)Lcom/peel/widget/ag;

    .line 419
    iget-object v0, p0, Lcom/peel/i/bj;->a:Lcom/peel/i/al;

    invoke-static {v0}, Lcom/peel/i/al;->t(Lcom/peel/i/al;)Lcom/peel/widget/ag;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/widget/ag;->show()V

    goto :goto_0
.end method
