.class Lcom/peel/i/it;
.super Landroid/webkit/WebViewClient;


# instance fields
.field final synthetic a:Lcom/peel/i/is;


# direct methods
.method constructor <init>(Lcom/peel/i/is;)V
    .locals 0

    .prologue
    .line 76
    iput-object p1, p0, Lcom/peel/i/it;->a:Lcom/peel/i/is;

    invoke-direct {p0}, Landroid/webkit/WebViewClient;-><init>()V

    return-void
.end method


# virtual methods
.method public shouldOverrideUrlLoading(Landroid/webkit/WebView;Ljava/lang/String;)Z
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 79
    if-eqz p2, :cond_0

    const-string/jumbo v1, "http://peel.tv/twittoauth"

    invoke-virtual {p2, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 80
    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    const-string/jumbo v2, "oauth_verifier"

    invoke-virtual {v1, v2}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 81
    iget-object v2, p0, Lcom/peel/i/it;->a:Lcom/peel/i/is;

    invoke-static {v2}, Lcom/peel/i/is;->a(Lcom/peel/i/is;)La/a/e;

    move-result-object v2

    invoke-interface {v2, v0}, La/a/e;->a(Z)V

    .line 83
    const-class v2, Lcom/peel/i/is;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "retrieving auth data"

    new-instance v4, Lcom/peel/i/iu;

    invoke-direct {v4, p0, v1}, Lcom/peel/i/iu;-><init>(Lcom/peel/i/it;Ljava/lang/String;)V

    invoke-static {v2, v3, v4}, Lcom/peel/util/i;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    .line 132
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Landroid/webkit/WebViewClient;->shouldOverrideUrlLoading(Landroid/webkit/WebView;Ljava/lang/String;)Z

    move-result v0

    goto :goto_0
.end method
