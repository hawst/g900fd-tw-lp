.class Lcom/peel/i/hf;
.super Lcom/peel/util/t;


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic b:Lcom/peel/i/gm;


# direct methods
.method constructor <init>(Lcom/peel/i/gm;ILjava/lang/String;)V
    .locals 0

    .prologue
    .line 1203
    iput-object p1, p0, Lcom/peel/i/hf;->b:Lcom/peel/i/gm;

    iput-object p3, p0, Lcom/peel/i/hf;->a:Ljava/lang/String;

    invoke-direct {p0, p2}, Lcom/peel/util/t;-><init>(I)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v4, 0x0

    .line 1207
    sget-boolean v0, Lcom/peel/util/b/a;->a:Z

    if-eqz v0, :cond_0

    .line 1208
    invoke-static {}, Lcom/peel/i/gm;->c()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/i/hf;->b:Lcom/peel/i/gm;

    invoke-virtual {v1}, Lcom/peel/i/gm;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    invoke-static {v0, v1, v4}, Lcom/peel/util/bx;->a(Ljava/lang/String;Landroid/app/Activity;Z)V

    .line 1326
    :goto_0
    return-void

    .line 1213
    :cond_0
    iget-boolean v0, p0, Lcom/peel/i/hf;->i:Z

    if-nez v0, :cond_2

    .line 1214
    invoke-static {}, Lcom/peel/i/gm;->c()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "unable to get all possible ir by func name: Power for device type: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/peel/i/hf;->b:Lcom/peel/i/gm;

    invoke-static {v2}, Lcom/peel/i/gm;->D(Lcom/peel/i/gm;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " -- brand: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/peel/i/hf;->b:Lcom/peel/i/gm;

    invoke-static {v2}, Lcom/peel/i/gm;->m(Lcom/peel/i/gm;)Lcom/peel/f/a;

    move-result-object v2

    invoke-virtual {v2}, Lcom/peel/f/a;->a()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1217
    iget-object v0, p0, Lcom/peel/i/hf;->b:Lcom/peel/i/gm;

    invoke-static {v0}, Lcom/peel/i/gm;->D(Lcom/peel/i/gm;)I

    move-result v0

    const/16 v1, 0xa

    if-ne v0, v1, :cond_1

    .line 1218
    iget-object v0, p0, Lcom/peel/i/hf;->b:Lcom/peel/i/gm;

    const-string/jumbo v1, "Power"

    invoke-static {v0, v1}, Lcom/peel/i/gm;->b(Lcom/peel/i/gm;Ljava/lang/String;)Ljava/lang/String;

    .line 1219
    const-string/jumbo v0, "https://partners-ir.peel.com"

    iget-object v1, p0, Lcom/peel/i/hf;->b:Lcom/peel/i/gm;

    invoke-static {v1}, Lcom/peel/i/gm;->o(Lcom/peel/i/gm;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/peel/i/hf;->b:Lcom/peel/i/gm;

    invoke-static {v2}, Lcom/peel/i/gm;->D(Lcom/peel/i/gm;)I

    move-result v2

    iget-object v3, p0, Lcom/peel/i/hf;->b:Lcom/peel/i/gm;

    invoke-static {v3}, Lcom/peel/i/gm;->m(Lcom/peel/i/gm;)Lcom/peel/f/a;

    move-result-object v3

    invoke-virtual {v3}, Lcom/peel/f/a;->a()I

    move-result v3

    iget-object v4, p0, Lcom/peel/i/hf;->a:Ljava/lang/String;

    new-instance v5, Lcom/peel/i/hg;

    invoke-direct {v5, p0, v6}, Lcom/peel/i/hg;-><init>(Lcom/peel/i/hf;I)V

    invoke-static/range {v0 .. v5}, Lcom/peel/control/aa;->a(Ljava/lang/String;Ljava/lang/String;IILjava/lang/String;Lcom/peel/util/t;)V

    goto :goto_0

    .line 1248
    :cond_1
    invoke-static {}, Lcom/peel/i/gm;->c()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/i/hf;->b:Lcom/peel/i/gm;

    invoke-virtual {v1}, Lcom/peel/i/gm;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    invoke-static {v0, v1, v4}, Lcom/peel/util/bx;->a(Ljava/lang/String;Landroid/app/Activity;Z)V

    .line 1249
    iget-object v0, p0, Lcom/peel/i/hf;->b:Lcom/peel/i/gm;

    invoke-virtual {v0}, Lcom/peel/i/gm;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/i/hf;->b:Lcom/peel/i/gm;

    iget-object v1, v1, Lcom/peel/i/gm;->b:Lcom/peel/d/i;

    iget-object v2, p0, Lcom/peel/i/hf;->b:Lcom/peel/i/gm;

    invoke-static {v2}, Lcom/peel/i/gm;->s(Lcom/peel/i/gm;)Lcom/peel/f/a;

    move-result-object v2

    invoke-virtual {v2}, Lcom/peel/f/a;->b()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/peel/i/hf;->b:Lcom/peel/i/gm;

    invoke-virtual {v3}, Lcom/peel/i/gm;->m()Landroid/support/v4/app/ae;

    move-result-object v3

    iget-object v4, p0, Lcom/peel/i/hf;->b:Lcom/peel/i/gm;

    invoke-static {v4}, Lcom/peel/i/gm;->D(Lcom/peel/i/gm;)I

    move-result v4

    invoke-static {v3, v4}, Lcom/peel/util/bx;->a(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/peel/i/hf;->b:Lcom/peel/i/gm;

    invoke-static {v4}, Lcom/peel/i/gm;->o(Lcom/peel/i/gm;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v1, v2, v3, v4}, Lcom/peel/util/bx;->a(Landroid/content/Context;Lcom/peel/d/i;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1253
    :cond_2
    invoke-static {}, Lcom/peel/i/gm;->c()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/i/hf;->b:Lcom/peel/i/gm;

    invoke-virtual {v1}, Lcom/peel/i/gm;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    invoke-static {v0, v1, v4}, Lcom/peel/util/bx;->a(Ljava/lang/String;Landroid/app/Activity;Z)V

    .line 1254
    iget-object v1, p0, Lcom/peel/i/hf;->b:Lcom/peel/i/gm;

    iget-object v0, p0, Lcom/peel/i/hf;->j:Ljava/lang/Object;

    check-cast v0, Ljava/util/ArrayList;

    invoke-static {v1, v0}, Lcom/peel/i/gm;->a(Lcom/peel/i/gm;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    .line 1255
    iget-object v0, p0, Lcom/peel/i/hf;->b:Lcom/peel/i/gm;

    invoke-static {v0, v4}, Lcom/peel/i/gm;->c(Lcom/peel/i/gm;I)I

    .line 1256
    iget-object v1, p0, Lcom/peel/i/hf;->b:Lcom/peel/i/gm;

    iget-object v0, p0, Lcom/peel/i/hf;->b:Lcom/peel/i/gm;

    invoke-static {v0}, Lcom/peel/i/gm;->S(Lcom/peel/i/gm;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    const-string/jumbo v2, "codesetid"

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v1, v0}, Lcom/peel/i/gm;->d(Lcom/peel/i/gm;I)I

    .line 1257
    iget-object v0, p0, Lcom/peel/i/hf;->b:Lcom/peel/i/gm;

    invoke-static {v0}, Lcom/peel/i/gm;->c(Lcom/peel/i/gm;)Lcom/peel/control/h;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v1

    iget-object v0, p0, Lcom/peel/i/hf;->b:Lcom/peel/i/gm;

    invoke-static {v0}, Lcom/peel/i/gm;->o(Lcom/peel/i/gm;)Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Lcom/peel/i/hf;->b:Lcom/peel/i/gm;

    invoke-static {v0}, Lcom/peel/i/gm;->S(Lcom/peel/i/gm;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    invoke-virtual {v1, v2, v0}, Lcom/peel/data/g;->a(Ljava/lang/String;Ljava/util/Map;)V

    .line 1258
    iget-object v0, p0, Lcom/peel/i/hf;->b:Lcom/peel/i/gm;

    invoke-static {v0}, Lcom/peel/i/gm;->S(Lcom/peel/i/gm;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    .line 1259
    iget-object v0, p0, Lcom/peel/i/hf;->b:Lcom/peel/i/gm;

    new-instance v1, Lcom/peel/i/in;

    iget-object v2, p0, Lcom/peel/i/hf;->b:Lcom/peel/i/gm;

    iget-object v3, p0, Lcom/peel/i/hf;->b:Lcom/peel/i/gm;

    invoke-static {v3}, Lcom/peel/i/gm;->S(Lcom/peel/i/gm;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    invoke-direct {v1, v2, v6, v3}, Lcom/peel/i/in;-><init>(Lcom/peel/i/gm;II)V

    invoke-static {v0, v1}, Lcom/peel/i/gm;->a(Lcom/peel/i/gm;Lcom/peel/i/in;)Lcom/peel/i/in;

    .line 1260
    iget-object v0, p0, Lcom/peel/i/hf;->b:Lcom/peel/i/gm;

    invoke-static {v0}, Lcom/peel/i/gm;->T(Lcom/peel/i/gm;)Lcom/peel/widget/TestBtnViewPager;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/i/hf;->b:Lcom/peel/i/gm;

    invoke-static {v1}, Lcom/peel/i/gm;->ae(Lcom/peel/i/gm;)Lcom/peel/i/in;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/peel/widget/TestBtnViewPager;->setAdapter(Landroid/support/v4/view/av;)V

    .line 1261
    iget-object v0, p0, Lcom/peel/i/hf;->b:Lcom/peel/i/gm;

    invoke-static {v0}, Lcom/peel/i/gm;->T(Lcom/peel/i/gm;)Lcom/peel/widget/TestBtnViewPager;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/i/hf;->b:Lcom/peel/i/gm;

    invoke-virtual {v1}, Lcom/peel/i/gm;->n()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/peel/ui/fn;->test_btn_pager_margin:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    mul-int/lit8 v1, v1, 0x2

    iget-object v2, p0, Lcom/peel/i/hf;->b:Lcom/peel/i/gm;

    invoke-virtual {v2}, Lcom/peel/i/gm;->n()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/peel/ui/fn;->test_btn_pager_margin:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v2

    mul-int/lit8 v2, v2, 0x2

    invoke-virtual {v0, v1, v4, v2, v4}, Lcom/peel/widget/TestBtnViewPager;->setPadding(IIII)V

    .line 1262
    iget-object v0, p0, Lcom/peel/i/hf;->b:Lcom/peel/i/gm;

    invoke-static {v0}, Lcom/peel/i/gm;->T(Lcom/peel/i/gm;)Lcom/peel/widget/TestBtnViewPager;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/peel/widget/TestBtnViewPager;->setClipToPadding(Z)V

    .line 1263
    iget-object v0, p0, Lcom/peel/i/hf;->b:Lcom/peel/i/gm;

    invoke-static {v0}, Lcom/peel/i/gm;->T(Lcom/peel/i/gm;)Lcom/peel/widget/TestBtnViewPager;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/i/hf;->b:Lcom/peel/i/gm;

    invoke-virtual {v1}, Lcom/peel/i/gm;->n()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/peel/ui/fn;->test_btn_pager_margin:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/peel/widget/TestBtnViewPager;->setPageMargin(I)V

    .line 1264
    iget-object v0, p0, Lcom/peel/i/hf;->b:Lcom/peel/i/gm;

    invoke-static {v0}, Lcom/peel/i/gm;->T(Lcom/peel/i/gm;)Lcom/peel/widget/TestBtnViewPager;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/peel/widget/TestBtnViewPager;->setClipChildren(Z)V

    .line 1265
    iget-object v0, p0, Lcom/peel/i/hf;->b:Lcom/peel/i/gm;

    invoke-static {v0}, Lcom/peel/i/gm;->T(Lcom/peel/i/gm;)Lcom/peel/widget/TestBtnViewPager;

    move-result-object v0

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/peel/widget/TestBtnViewPager;->setOffscreenPageLimit(I)V

    .line 1266
    iget-object v0, p0, Lcom/peel/i/hf;->b:Lcom/peel/i/gm;

    invoke-static {v0}, Lcom/peel/i/gm;->T(Lcom/peel/i/gm;)Lcom/peel/widget/TestBtnViewPager;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/peel/widget/TestBtnViewPager;->setVisibility(I)V

    .line 1267
    iget-object v0, p0, Lcom/peel/i/hf;->b:Lcom/peel/i/gm;

    invoke-static {v0}, Lcom/peel/i/gm;->ab(Lcom/peel/i/gm;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/widget/Button;->setEnabled(Z)V

    .line 1268
    iget-object v0, p0, Lcom/peel/i/hf;->b:Lcom/peel/i/gm;

    invoke-static {v0}, Lcom/peel/i/gm;->ac(Lcom/peel/i/gm;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/widget/Button;->setEnabled(Z)V

    .line 1271
    :cond_3
    iget-object v0, p0, Lcom/peel/i/hf;->b:Lcom/peel/i/gm;

    invoke-static {v0}, Lcom/peel/i/gm;->T(Lcom/peel/i/gm;)Lcom/peel/widget/TestBtnViewPager;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "btnView"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/peel/i/hf;->b:Lcom/peel/i/gm;

    invoke-static {v2}, Lcom/peel/i/gm;->af(Lcom/peel/i/gm;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/peel/widget/TestBtnViewPager;->findViewWithTag(Ljava/lang/Object;)Landroid/view/View;

    move-result-object v0

    sget v1, Lcom/peel/ui/fp;->test_pw_btn_large_view:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 1272
    iget-object v0, p0, Lcom/peel/i/hf;->b:Lcom/peel/i/gm;

    invoke-static {v0}, Lcom/peel/i/gm;->T(Lcom/peel/i/gm;)Lcom/peel/widget/TestBtnViewPager;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "btnView"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/peel/i/hf;->b:Lcom/peel/i/gm;

    invoke-static {v2}, Lcom/peel/i/gm;->af(Lcom/peel/i/gm;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/peel/widget/TestBtnViewPager;->findViewWithTag(Ljava/lang/Object;)Landroid/view/View;

    move-result-object v0

    sget v1, Lcom/peel/ui/fp;->test_pw_btn_small_view:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1273
    iget-object v0, p0, Lcom/peel/i/hf;->b:Lcom/peel/i/gm;

    invoke-static {v0}, Lcom/peel/i/gm;->T(Lcom/peel/i/gm;)Lcom/peel/widget/TestBtnViewPager;

    move-result-object v0

    new-instance v1, Lcom/peel/i/hh;

    invoke-direct {v1, p0}, Lcom/peel/i/hh;-><init>(Lcom/peel/i/hf;)V

    invoke-virtual {v0, v1}, Lcom/peel/widget/TestBtnViewPager;->setOnPageChangeListener(Landroid/support/v4/view/cs;)V

    .line 1324
    iget-object v0, p0, Lcom/peel/i/hf;->b:Lcom/peel/i/gm;

    invoke-static {v0}, Lcom/peel/i/gm;->ad(Lcom/peel/i/gm;)V

    goto/16 :goto_0
.end method
