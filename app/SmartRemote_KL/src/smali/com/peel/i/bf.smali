.class Lcom/peel/i/bf;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/peel/i/bb;


# direct methods
.method constructor <init>(Lcom/peel/i/bb;)V
    .locals 0

    .prologue
    .line 281
    iput-object p1, p0, Lcom/peel/i/bf;->a:Lcom/peel/i/bb;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 13

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x0

    .line 284
    iget-object v0, p0, Lcom/peel/i/bf;->a:Lcom/peel/i/bb;

    iget-object v0, v0, Lcom/peel/i/bb;->a:Lcom/peel/i/al;

    invoke-virtual {v0}, Lcom/peel/i/al;->v()Landroid/view/View;

    move-result-object v0

    sget v1, Lcom/peel/ui/fp;->ip_device_list_panel:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 288
    iget-object v0, p0, Lcom/peel/i/bf;->a:Lcom/peel/i/bb;

    iget-object v0, v0, Lcom/peel/i/bb;->a:Lcom/peel/i/al;

    invoke-static {v0}, Lcom/peel/i/al;->l(Lcom/peel/i/al;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 290
    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v0}, Lcom/peel/control/am;->e()[Lcom/peel/control/h;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 291
    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v0}, Lcom/peel/control/am;->e()[Lcom/peel/control/h;

    move-result-object v7

    array-length v8, v7

    move v6, v5

    move-object v1, v2

    :goto_0
    if-ge v6, v8, :cond_2

    aget-object v0, v7, v6

    .line 292
    invoke-virtual {v0}, Lcom/peel/control/h;->c()[Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_7

    .line 293
    invoke-virtual {v0}, Lcom/peel/control/h;->c()[Ljava/lang/String;

    move-result-object v9

    array-length v10, v9

    move v4, v5

    :goto_1
    if-ge v4, v10, :cond_7

    aget-object v11, v9, v4

    .line 294
    const-string/jumbo v12, "dtv"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_0

    .line 291
    :goto_2
    add-int/lit8 v1, v6, 0x1

    move v6, v1

    move-object v1, v0

    goto :goto_0

    .line 293
    :cond_0
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    :cond_1
    move-object v1, v2

    .line 304
    :cond_2
    iget-object v0, p0, Lcom/peel/i/bf;->a:Lcom/peel/i/bb;

    iget-object v0, v0, Lcom/peel/i/bb;->a:Lcom/peel/i/al;

    invoke-static {v0}, Lcom/peel/i/al;->q(Lcom/peel/i/al;)Lcom/peel/control/h;

    move-result-object v0

    if-nez v0, :cond_3

    .line 305
    iget-object v0, p0, Lcom/peel/i/bf;->a:Lcom/peel/i/bb;

    iget-object v0, v0, Lcom/peel/i/bb;->a:Lcom/peel/i/al;

    iget-object v4, p0, Lcom/peel/i/bf;->a:Lcom/peel/i/bb;

    iget-object v4, v4, Lcom/peel/i/bb;->a:Lcom/peel/i/al;

    invoke-static {v4}, Lcom/peel/i/al;->c(Lcom/peel/i/al;)[Lcom/peel/control/h;

    move-result-object v4

    aget-object v4, v4, v5

    invoke-static {v0, v4}, Lcom/peel/i/al;->a(Lcom/peel/i/al;Lcom/peel/control/h;)Lcom/peel/control/h;

    .line 306
    invoke-static {}, Lcom/peel/util/a/f;->a()Lcom/peel/util/a/f;

    move-result-object v4

    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v0}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v0

    if-nez v0, :cond_4

    move v0, v3

    :goto_3
    const/16 v6, 0x14b5

    const/16 v7, 0x7d5

    iget-object v8, p0, Lcom/peel/i/bf;->a:Lcom/peel/i/bb;

    iget-object v8, v8, Lcom/peel/i/bb;->a:Lcom/peel/i/al;

    invoke-static {v8}, Lcom/peel/i/al;->q(Lcom/peel/i/al;)Lcom/peel/control/h;

    move-result-object v8

    invoke-virtual {v8}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v8

    invoke-virtual {v8}, Lcom/peel/data/g;->i()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v4, v0, v6, v7, v8}, Lcom/peel/util/a/f;->a(IIILjava/lang/String;)V

    .line 311
    :cond_3
    if-eqz v1, :cond_6

    .line 312
    invoke-virtual {v1}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/data/g;->i()Ljava/lang/String;

    move-result-object v0

    iget-object v4, p0, Lcom/peel/i/bf;->a:Lcom/peel/i/bb;

    iget-object v4, v4, Lcom/peel/i/bb;->a:Lcom/peel/i/al;

    invoke-static {v4}, Lcom/peel/i/al;->q(Lcom/peel/i/al;)Lcom/peel/control/h;

    move-result-object v4

    invoke-virtual {v4}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v4

    invoke-virtual {v4}, Lcom/peel/data/g;->i()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 314
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "show confirmation dialog"

    new-instance v2, Lcom/peel/i/bg;

    invoke-direct {v2, p0}, Lcom/peel/i/bg;-><init>(Lcom/peel/i/bf;)V

    invoke-static {v0, v1, v2}, Lcom/peel/util/i;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/lang/Runnable;

    .line 367
    :goto_4
    return-void

    .line 306
    :cond_4
    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v0}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/data/at;->f()I

    move-result v0

    goto :goto_3

    .line 333
    :cond_5
    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v0, v1}, Lcom/peel/control/am;->b(Lcom/peel/control/h;)V

    .line 336
    :cond_6
    const-string/jumbo v0, "DIRECTV"

    invoke-static {v0}, Lcom/peel/control/a;->a(Ljava/lang/String;)Lcom/peel/control/a;

    move-result-object v0

    .line 338
    iget-object v1, p0, Lcom/peel/i/bf;->a:Lcom/peel/i/bb;

    iget-object v1, v1, Lcom/peel/i/bb;->a:Lcom/peel/i/al;

    invoke-static {v1}, Lcom/peel/i/al;->q(Lcom/peel/i/al;)Lcom/peel/control/h;

    move-result-object v1

    new-array v4, v3, [Ljava/lang/Integer;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v4, v5

    invoke-virtual {v0, v1, v2, v4}, Lcom/peel/control/a;->a(Lcom/peel/control/h;Ljava/lang/String;[Ljava/lang/Integer;)Z

    .line 340
    iget-object v1, p0, Lcom/peel/i/bf;->a:Lcom/peel/i/bb;

    iget-object v1, v1, Lcom/peel/i/bb;->a:Lcom/peel/i/al;

    invoke-static {v1}, Lcom/peel/i/al;->u(Lcom/peel/i/al;)Lcom/peel/control/RoomControl;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/peel/control/RoomControl;->a(Lcom/peel/control/a;)V

    .line 341
    iget-object v1, p0, Lcom/peel/i/bf;->a:Lcom/peel/i/bb;

    iget-object v1, v1, Lcom/peel/i/bb;->a:Lcom/peel/i/al;

    invoke-static {v1}, Lcom/peel/i/al;->u(Lcom/peel/i/al;)Lcom/peel/control/RoomControl;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v1

    invoke-virtual {v0}, Lcom/peel/control/a;->c()Lcom/peel/data/d;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/data/d;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/peel/data/at;->c(Ljava/lang/String;)V

    .line 344
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, ""

    new-instance v2, Lcom/peel/i/bi;

    invoke-direct {v2, p0}, Lcom/peel/i/bi;-><init>(Lcom/peel/i/bf;)V

    invoke-static {v0, v1, v2}, Lcom/peel/util/i;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/lang/Runnable;

    goto :goto_4

    :cond_7
    move-object v0, v1

    goto/16 :goto_2
.end method
