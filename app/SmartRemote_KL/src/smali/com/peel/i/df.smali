.class Lcom/peel/i/df;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic a:Lcom/peel/i/de;


# direct methods
.method constructor <init>(Lcom/peel/i/de;)V
    .locals 0

    .prologue
    .line 351
    iput-object p1, p0, Lcom/peel/i/df;->a:Lcom/peel/i/de;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 6

    .prologue
    .line 355
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 357
    iget-object v1, p0, Lcom/peel/i/df;->a:Lcom/peel/i/de;

    iget-object v1, v1, Lcom/peel/i/de;->a:Lcom/peel/i/dc;

    iget-object v1, v1, Lcom/peel/i/dc;->d:Lcom/peel/i/cr;

    invoke-static {v1}, Lcom/peel/i/cr;->j(Lcom/peel/i/cr;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 358
    const-string/jumbo v1, "zipcode"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/peel/i/df;->a:Lcom/peel/i/de;

    iget-object v3, v3, Lcom/peel/i/de;->a:Lcom/peel/i/dc;

    iget-object v3, v3, Lcom/peel/i/dc;->d:Lcom/peel/i/cr;

    invoke-static {v3}, Lcom/peel/i/cr;->k(Lcom/peel/i/cr;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " / "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/peel/i/df;->a:Lcom/peel/i/de;

    iget-object v3, v3, Lcom/peel/i/de;->a:Lcom/peel/i/dc;

    iget-object v3, v3, Lcom/peel/i/dc;->d:Lcom/peel/i/cr;

    invoke-static {v3}, Lcom/peel/i/cr;->j(Lcom/peel/i/cr;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 362
    :goto_0
    const-string/jumbo v1, "region"

    iget-object v2, p0, Lcom/peel/i/df;->a:Lcom/peel/i/de;

    iget-object v2, v2, Lcom/peel/i/de;->a:Lcom/peel/i/dc;

    iget-object v2, v2, Lcom/peel/i/dc;->a:Landroid/os/Bundle;

    const-string/jumbo v3, "type"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 363
    iget-object v1, p0, Lcom/peel/i/df;->a:Lcom/peel/i/de;

    iget-object v1, v1, Lcom/peel/i/de;->a:Lcom/peel/i/dc;

    iget-object v1, v1, Lcom/peel/i/dc;->d:Lcom/peel/i/cr;

    invoke-virtual {v1}, Lcom/peel/i/cr;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    const-class v2, Lcom/peel/i/bm;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lcom/peel/d/e;->c(Landroid/support/v4/app/ae;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 365
    invoke-static {}, Lcom/peel/util/a/f;->a()Lcom/peel/util/a/f;

    move-result-object v1

    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v0}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    const/16 v2, 0xbe2

    const/16 v3, 0x7d5

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lcom/peel/i/df;->a:Lcom/peel/i/de;

    iget-object v5, v5, Lcom/peel/i/de;->a:Lcom/peel/i/dc;

    iget-object v5, v5, Lcom/peel/i/dc;->b:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/peel/i/df;->a:Lcom/peel/i/de;

    iget-object v5, v5, Lcom/peel/i/de;->a:Lcom/peel/i/dc;

    iget-object v5, v5, Lcom/peel/i/dc;->c:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v0, v2, v3, v4}, Lcom/peel/util/a/f;->a(IIILjava/lang/String;)V

    .line 367
    return-void

    .line 360
    :cond_0
    const-string/jumbo v1, "zipcode"

    iget-object v2, p0, Lcom/peel/i/df;->a:Lcom/peel/i/de;

    iget-object v2, v2, Lcom/peel/i/de;->a:Lcom/peel/i/dc;

    iget-object v2, v2, Lcom/peel/i/dc;->d:Lcom/peel/i/cr;

    invoke-static {v2}, Lcom/peel/i/cr;->k(Lcom/peel/i/cr;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 365
    :cond_1
    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v0}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/data/at;->f()I

    move-result v0

    goto :goto_1
.end method
