.class final Lcom/peel/i/il;
.super Landroid/text/method/LinkMovementMethod;


# instance fields
.field final synthetic a:Lcom/peel/i/gm;

.field private b:I


# direct methods
.method public constructor <init>(Lcom/peel/i/gm;I)V
    .locals 0

    .prologue
    .line 2415
    iput-object p1, p0, Lcom/peel/i/il;->a:Lcom/peel/i/gm;

    invoke-direct {p0}, Landroid/text/method/LinkMovementMethod;-><init>()V

    .line 2416
    iput p2, p0, Lcom/peel/i/il;->b:I

    .line 2417
    return-void
.end method


# virtual methods
.method public onTouchEvent(Landroid/widget/TextView;Landroid/text/Spannable;Landroid/view/MotionEvent;)Z
    .locals 7

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2422
    invoke-virtual {p3}, Landroid/view/MotionEvent;->getAction()I

    move-result v4

    .line 2424
    if-eq v4, v3, :cond_0

    if-nez v4, :cond_5

    .line 2426
    :cond_0
    invoke-virtual {p3}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    float-to-int v0, v0

    .line 2427
    invoke-virtual {p3}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    float-to-int v1, v1

    .line 2429
    invoke-virtual {p1}, Landroid/widget/TextView;->getTotalPaddingLeft()I

    move-result v5

    sub-int/2addr v0, v5

    .line 2430
    invoke-virtual {p1}, Landroid/widget/TextView;->getTotalPaddingTop()I

    move-result v5

    sub-int/2addr v1, v5

    .line 2432
    invoke-virtual {p1}, Landroid/widget/TextView;->getScrollX()I

    move-result v5

    add-int/2addr v0, v5

    .line 2433
    invoke-virtual {p1}, Landroid/widget/TextView;->getScrollY()I

    move-result v5

    add-int/2addr v1, v5

    .line 2435
    invoke-virtual {p1}, Landroid/widget/TextView;->getLayout()Landroid/text/Layout;

    move-result-object v5

    .line 2436
    invoke-virtual {v5, v1}, Landroid/text/Layout;->getLineForVertical(I)I

    move-result v1

    .line 2437
    int-to-float v0, v0

    invoke-virtual {v5, v1, v0}, Landroid/text/Layout;->getOffsetForHorizontal(IF)I

    move-result v0

    .line 2439
    const-class v1, Lcom/peel/i/ik;

    invoke-interface {p2, v0, v0, v1}, Landroid/text/Spannable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/peel/i/ik;

    move v1, v2

    .line 2441
    :goto_0
    array-length v5, v0

    if-ge v1, v5, :cond_1

    .line 2442
    aget-object v5, v0, v1

    iget v6, p0, Lcom/peel/i/il;->b:I

    invoke-static {v5, v6}, Lcom/peel/i/ik;->a(Lcom/peel/i/ik;I)I

    .line 2441
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2444
    :cond_1
    array-length v1, v0

    if-eqz v1, :cond_4

    .line 2445
    if-ne v4, v3, :cond_3

    .line 2446
    aget-object v0, v0, v2

    invoke-virtual {v0, p1}, Lcom/peel/i/ik;->onClick(Landroid/view/View;)V

    :cond_2
    :goto_1
    move v0, v3

    .line 2458
    :goto_2
    return v0

    .line 2447
    :cond_3
    if-nez v4, :cond_2

    .line 2448
    aget-object v1, v0, v2

    .line 2449
    invoke-interface {p2, v1}, Landroid/text/Spannable;->getSpanStart(Ljava/lang/Object;)I

    move-result v1

    aget-object v0, v0, v2

    .line 2450
    invoke-interface {p2, v0}, Landroid/text/Spannable;->getSpanEnd(Ljava/lang/Object;)I

    move-result v0

    .line 2448
    invoke-static {p2, v1, v0}, Landroid/text/Selection;->setSelection(Landroid/text/Spannable;II)V

    goto :goto_1

    .line 2454
    :cond_4
    invoke-static {p2}, Landroid/text/Selection;->removeSelection(Landroid/text/Spannable;)V

    .line 2458
    :cond_5
    invoke-static {p1, p2, p3}, Landroid/text/method/Touch;->onTouchEvent(Landroid/widget/TextView;Landroid/text/Spannable;Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_2
.end method
