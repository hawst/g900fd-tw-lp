.class public Lcom/peel/i/bm;
.super Lcom/peel/d/u;


# instance fields
.field private aj:Lcom/peel/widget/ag;

.field private ak:Lcom/peel/widget/ag;

.field private al:Lcom/peel/widget/ag;

.field private am:Lcom/peel/widget/ag;

.field private an:Lcom/peel/widget/ag;

.field public e:Landroid/widget/EditText;

.field public f:Landroid/widget/EditText;

.field public g:Landroid/widget/EditText;

.field public h:Landroid/widget/EditText;

.field private i:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Lcom/peel/d/u;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/peel/i/bm;Lcom/peel/widget/ag;)Lcom/peel/widget/ag;
    .locals 0

    .prologue
    .line 32
    iput-object p1, p0, Lcom/peel/i/bm;->an:Lcom/peel/widget/ag;

    return-object p1
.end method

.method static synthetic a(Lcom/peel/i/bm;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/peel/i/bm;->i:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic b(Lcom/peel/i/bm;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/peel/i/bm;->a:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic c(Lcom/peel/i/bm;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/peel/i/bm;->a:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic d(Lcom/peel/i/bm;)Lcom/peel/widget/ag;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/peel/i/bm;->an:Lcom/peel/widget/ag;

    return-object v0
.end method

.method static synthetic e(Lcom/peel/i/bm;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/peel/i/bm;->a:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public Z()V
    .locals 7

    .prologue
    .line 234
    iget-object v0, p0, Lcom/peel/i/bm;->d:Lcom/peel/d/a;

    if-nez v0, :cond_0

    .line 235
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 236
    sget v0, Lcom/peel/ui/fp;->menu_send:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 237
    new-instance v0, Lcom/peel/d/a;

    sget-object v1, Lcom/peel/d/d;->b:Lcom/peel/d/d;

    sget-object v2, Lcom/peel/d/b;->a:Lcom/peel/d/b;

    sget-object v3, Lcom/peel/d/c;->b:Lcom/peel/d/c;

    iget-object v4, p0, Lcom/peel/i/bm;->c:Landroid/os/Bundle;

    const-string/jumbo v6, "category"

    invoke-virtual {v4, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-direct/range {v0 .. v5}, Lcom/peel/d/a;-><init>(Lcom/peel/d/d;Lcom/peel/d/b;Lcom/peel/d/c;Ljava/lang/String;Ljava/util/List;)V

    iput-object v0, p0, Lcom/peel/i/bm;->d:Lcom/peel/d/a;

    .line 239
    :cond_0
    iget-object v0, p0, Lcom/peel/i/bm;->b:Lcom/peel/d/i;

    iget-object v1, p0, Lcom/peel/i/bm;->d:Lcom/peel/d/a;

    invoke-virtual {v0, v1}, Lcom/peel/d/i;->a(Lcom/peel/d/a;)V

    .line 240
    return-void
.end method

.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    .line 48
    sget v0, Lcom/peel/ui/fq;->report_missing_service:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 49
    return-object v0
.end method

.method public a(Landroid/view/MenuItem;)Z
    .locals 2

    .prologue
    .line 117
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    .line 118
    sget v1, Lcom/peel/ui/fp;->menu_send:I

    if-ne v0, v1, :cond_0

    .line 119
    invoke-virtual {p0}, Lcom/peel/i/bm;->c()V

    .line 121
    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method public a_(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 41
    invoke-super {p0, p1}, Lcom/peel/d/u;->a_(Landroid/os/Bundle;)V

    .line 42
    iget-object v0, p0, Lcom/peel/i/bm;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "zipcode"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/i/bm;->i:Ljava/lang/String;

    .line 44
    return-void
.end method

.method public b()Z
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 244
    :try_start_0
    invoke-virtual {p0}, Lcom/peel/i/bm;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    const-string/jumbo v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/ae;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 245
    invoke-virtual {p0}, Lcom/peel/i/bm;->v()Landroid/view/View;

    move-result-object v1

    sget v2, Lcom/peel/ui/fp;->missing_tv_msg_email:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 248
    :goto_0
    return v3

    .line 246
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public c()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 125
    iget-object v0, p0, Lcom/peel/i/bm;->e:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/peel/util/bx;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 126
    new-instance v0, Lcom/peel/widget/ag;

    invoke-virtual {p0}, Lcom/peel/i/bm;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/peel/widget/ag;-><init>(Landroid/content/Context;)V

    sget v1, Lcom/peel/ui/ft;->invalid_email_address:I

    invoke-virtual {v0, v1}, Lcom/peel/widget/ag;->a(I)Lcom/peel/widget/ag;

    move-result-object v0

    invoke-virtual {p0}, Lcom/peel/i/bm;->n()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/peel/ui/ft;->enter_valid_email:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/peel/widget/ag;->b(Ljava/lang/CharSequence;)Lcom/peel/widget/ag;

    move-result-object v0

    sget v1, Lcom/peel/ui/ft;->okay:I

    new-instance v2, Lcom/peel/i/bo;

    invoke-direct {v2, p0}, Lcom/peel/i/bo;-><init>(Lcom/peel/i/bm;)V

    invoke-virtual {v0, v1, v2}, Lcom/peel/widget/ag;->a(ILandroid/content/DialogInterface$OnClickListener;)Lcom/peel/widget/ag;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/i/bm;->aj:Lcom/peel/widget/ag;

    .line 132
    iget-object v0, p0, Lcom/peel/i/bm;->aj:Lcom/peel/widget/ag;

    iget-object v1, p0, Lcom/peel/i/bm;->aj:Lcom/peel/widget/ag;

    invoke-virtual {v0, v1}, Lcom/peel/widget/ag;->a(Lcom/peel/widget/ag;)Lcom/peel/widget/ag;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/widget/ag;->show()V

    .line 228
    :goto_0
    return-void

    .line 136
    :cond_0
    iget-object v0, p0, Lcom/peel/i/bm;->f:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    if-nez v0, :cond_1

    .line 137
    new-instance v0, Lcom/peel/widget/ag;

    invoke-virtual {p0}, Lcom/peel/i/bm;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/peel/widget/ag;-><init>(Landroid/content/Context;)V

    invoke-virtual {p0}, Lcom/peel/i/bm;->n()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/peel/ui/ft;->enter_subject:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/peel/widget/ag;->b(Ljava/lang/CharSequence;)Lcom/peel/widget/ag;

    move-result-object v0

    sget v1, Lcom/peel/ui/ft;->invalid_subject_title:I

    invoke-virtual {v0, v1}, Lcom/peel/widget/ag;->a(I)Lcom/peel/widget/ag;

    move-result-object v0

    sget v1, Lcom/peel/ui/ft;->okay:I

    new-instance v2, Lcom/peel/i/bp;

    invoke-direct {v2, p0}, Lcom/peel/i/bp;-><init>(Lcom/peel/i/bm;)V

    .line 138
    invoke-virtual {v0, v1, v2}, Lcom/peel/widget/ag;->a(ILandroid/content/DialogInterface$OnClickListener;)Lcom/peel/widget/ag;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/i/bm;->ak:Lcom/peel/widget/ag;

    .line 144
    iget-object v0, p0, Lcom/peel/i/bm;->ak:Lcom/peel/widget/ag;

    iget-object v1, p0, Lcom/peel/i/bm;->ak:Lcom/peel/widget/ag;

    invoke-virtual {v0, v1}, Lcom/peel/widget/ag;->a(Lcom/peel/widget/ag;)Lcom/peel/widget/ag;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/widget/ag;->show()V

    goto :goto_0

    .line 147
    :cond_1
    iget-object v0, p0, Lcom/peel/i/bm;->g:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    if-nez v0, :cond_2

    .line 148
    new-instance v0, Lcom/peel/widget/ag;

    invoke-virtual {p0}, Lcom/peel/i/bm;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/peel/widget/ag;-><init>(Landroid/content/Context;)V

    invoke-virtual {p0}, Lcom/peel/i/bm;->n()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/peel/ui/ft;->enter_service_provider_name:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/peel/widget/ag;->b(Ljava/lang/CharSequence;)Lcom/peel/widget/ag;

    move-result-object v0

    sget v1, Lcom/peel/ui/ft;->invalid_tv_service_provider_title:I

    invoke-virtual {v0, v1}, Lcom/peel/widget/ag;->a(I)Lcom/peel/widget/ag;

    move-result-object v0

    sget v1, Lcom/peel/ui/ft;->okay:I

    new-instance v2, Lcom/peel/i/bq;

    invoke-direct {v2, p0}, Lcom/peel/i/bq;-><init>(Lcom/peel/i/bm;)V

    .line 149
    invoke-virtual {v0, v1, v2}, Lcom/peel/widget/ag;->a(ILandroid/content/DialogInterface$OnClickListener;)Lcom/peel/widget/ag;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/i/bm;->al:Lcom/peel/widget/ag;

    .line 155
    iget-object v0, p0, Lcom/peel/i/bm;->al:Lcom/peel/widget/ag;

    iget-object v1, p0, Lcom/peel/i/bm;->al:Lcom/peel/widget/ag;

    invoke-virtual {v0, v1}, Lcom/peel/widget/ag;->a(Lcom/peel/widget/ag;)Lcom/peel/widget/ag;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/widget/ag;->show()V

    goto/16 :goto_0

    .line 158
    :cond_2
    iget-object v0, p0, Lcom/peel/i/bm;->h:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    if-nez v0, :cond_3

    .line 159
    new-instance v0, Lcom/peel/widget/ag;

    invoke-virtual {p0}, Lcom/peel/i/bm;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/peel/widget/ag;-><init>(Landroid/content/Context;)V

    invoke-virtual {p0}, Lcom/peel/i/bm;->n()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/peel/ui/ft;->enter_desc:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/peel/widget/ag;->b(Ljava/lang/CharSequence;)Lcom/peel/widget/ag;

    move-result-object v0

    sget v1, Lcom/peel/ui/ft;->okay:I

    new-instance v2, Lcom/peel/i/br;

    invoke-direct {v2, p0}, Lcom/peel/i/br;-><init>(Lcom/peel/i/bm;)V

    invoke-virtual {v0, v1, v2}, Lcom/peel/widget/ag;->a(ILandroid/content/DialogInterface$OnClickListener;)Lcom/peel/widget/ag;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/i/bm;->am:Lcom/peel/widget/ag;

    .line 165
    iget-object v0, p0, Lcom/peel/i/bm;->am:Lcom/peel/widget/ag;

    iget-object v1, p0, Lcom/peel/i/bm;->am:Lcom/peel/widget/ag;

    invoke-virtual {v0, v1}, Lcom/peel/widget/ag;->a(Lcom/peel/widget/ag;)Lcom/peel/widget/ag;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/widget/ag;->show()V

    goto/16 :goto_0

    .line 168
    :cond_3
    invoke-virtual {p0}, Lcom/peel/i/bm;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    const-string/jumbo v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/ae;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 169
    iget-object v1, p0, Lcom/peel/i/bm;->e:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 170
    const-class v0, Lcom/peel/i/bm;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/peel/i/bm;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    invoke-static {v0, v1, v2}, Lcom/peel/util/bx;->a(Ljava/lang/String;Landroid/app/Activity;Z)V

    .line 173
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "report missing service provider"

    new-instance v2, Lcom/peel/i/bs;

    invoke-direct {v2, p0}, Lcom/peel/i/bs;-><init>(Lcom/peel/i/bm;)V

    invoke-static {v0, v1, v2}, Lcom/peel/util/i;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/lang/Runnable;

    goto/16 :goto_0
.end method

.method public d(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 83
    invoke-super {p0, p1}, Lcom/peel/d/u;->d(Landroid/os/Bundle;)V

    .line 84
    iget-object v0, p0, Lcom/peel/i/bm;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "category"

    sget v2, Lcom/peel/ui/ft;->label_report:I

    invoke-virtual {p0, v2}, Lcom/peel/i/bm;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 85
    return-void
.end method

.method public f()V
    .locals 3

    .prologue
    .line 60
    invoke-super {p0}, Lcom/peel/d/u;->f()V

    .line 61
    invoke-virtual {p0}, Lcom/peel/i/bm;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    const-string/jumbo v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/ae;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 62
    invoke-virtual {p0}, Lcom/peel/i/bm;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/ae;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 79
    return-void
.end method

.method public h(Landroid/os/Bundle;)V
    .locals 8

    .prologue
    const/4 v3, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 89
    invoke-super {p0, p1}, Lcom/peel/d/u;->h(Landroid/os/Bundle;)V

    .line 90
    invoke-virtual {p0}, Lcom/peel/i/bm;->v()Landroid/view/View;

    move-result-object v1

    .line 92
    sget v0, Lcom/peel/ui/fp;->missing_tv_msg_email:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/peel/i/bm;->e:Landroid/widget/EditText;

    .line 93
    sget v0, Lcom/peel/ui/fp;->missing_tv_msg_subject:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/peel/i/bm;->f:Landroid/widget/EditText;

    .line 94
    sget v0, Lcom/peel/ui/fp;->missing_tv_msg_service_provider_name:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/peel/i/bm;->g:Landroid/widget/EditText;

    .line 95
    sget v0, Lcom/peel/ui/fp;->missing_tv_msg_desc:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/peel/i/bm;->h:Landroid/widget/EditText;

    .line 96
    iget-object v0, p0, Lcom/peel/i/bm;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "region"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 97
    iget-object v0, p0, Lcom/peel/i/bm;->h:Landroid/widget/EditText;

    sget v1, Lcom/peel/ui/ft;->report_missing_regions:I

    new-array v2, v3, [Ljava/lang/Object;

    .line 98
    invoke-virtual {p0}, Lcom/peel/i/bm;->m()Landroid/support/v4/app/ae;

    move-result-object v3

    invoke-static {v3}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v3

    const-string/jumbo v4, "country"

    const-string/jumbo v5, "N/A"

    invoke-interface {v3, v4, v5}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v6

    iget-object v3, p0, Lcom/peel/i/bm;->i:Ljava/lang/String;

    aput-object v3, v2, v7

    .line 97
    invoke-virtual {p0, v1, v2}, Lcom/peel/i/bm;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 105
    :goto_0
    iget-object v0, p0, Lcom/peel/i/bm;->e:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    .line 106
    iget-object v0, p0, Lcom/peel/i/bm;->e:Landroid/widget/EditText;

    new-instance v1, Lcom/peel/i/bn;

    invoke-direct {v1, p0}, Lcom/peel/i/bn;-><init>(Lcom/peel/i/bm;)V

    const-wide/16 v2, 0x64

    invoke-virtual {v0, v1, v2, v3}, Landroid/widget/EditText;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 113
    return-void

    .line 99
    :cond_0
    iget-object v0, p0, Lcom/peel/i/bm;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "device_type"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    .line 100
    iget-object v0, p0, Lcom/peel/i/bm;->h:Landroid/widget/EditText;

    sget v1, Lcom/peel/ui/ft;->report_missing:I

    new-array v2, v3, [Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/peel/i/bm;->m()Landroid/support/v4/app/ae;

    move-result-object v3

    invoke-static {v3}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v3

    const-string/jumbo v4, "country"

    const-string/jumbo v5, "N/A"

    invoke-interface {v3, v4, v5}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v6

    iget-object v3, p0, Lcom/peel/i/bm;->i:Ljava/lang/String;

    aput-object v3, v2, v7

    invoke-virtual {p0, v1, v2}, Lcom/peel/i/bm;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 102
    :cond_1
    iget-object v0, p0, Lcom/peel/i/bm;->f:Landroid/widget/EditText;

    sget v1, Lcom/peel/ui/ft;->mydevicemissing:I

    new-array v2, v7, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/peel/i/bm;->c:Landroid/os/Bundle;

    const-string/jumbo v4, "device_type"

    const-string/jumbo v5, ""

    invoke-virtual {v3, v4, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v6

    invoke-virtual {p0, v1, v2}, Lcom/peel/i/bm;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public w()V
    .locals 0

    .prologue
    .line 54
    invoke-super {p0}, Lcom/peel/d/u;->w()V

    .line 55
    invoke-virtual {p0}, Lcom/peel/i/bm;->Z()V

    .line 56
    return-void
.end method
