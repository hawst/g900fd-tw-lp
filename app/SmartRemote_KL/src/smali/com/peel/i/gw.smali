.class Lcom/peel/i/gw;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/peel/ui/dr;


# instance fields
.field final synthetic a:Lcom/peel/i/gm;


# direct methods
.method constructor <init>(Lcom/peel/i/gm;)V
    .locals 0

    .prologue
    .line 994
    iput-object p1, p0, Lcom/peel/i/gw;->a:Lcom/peel/i/gm;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(ILcom/peel/f/a;)V
    .locals 10

    .prologue
    .line 997
    const/4 v0, 0x1

    if-ne p1, v0, :cond_1

    .line 998
    iget-object v0, p0, Lcom/peel/i/gw;->a:Lcom/peel/i/gm;

    invoke-static {v0, p2}, Lcom/peel/i/gm;->a(Lcom/peel/i/gm;Lcom/peel/f/a;)Lcom/peel/f/a;

    .line 999
    invoke-static {}, Lcom/peel/util/a/f;->a()Lcom/peel/util/a/f;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/i/gw;->a:Lcom/peel/i/gm;

    invoke-static {v1}, Lcom/peel/i/gm;->b(Lcom/peel/i/gm;)Lcom/peel/control/RoomControl;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/data/at;->f()I

    move-result v1

    const/16 v2, 0xc29

    const/16 v3, 0x7d5

    iget-object v4, p0, Lcom/peel/i/gw;->a:Lcom/peel/i/gm;

    invoke-static {v4}, Lcom/peel/i/gm;->m(Lcom/peel/i/gm;)Lcom/peel/f/a;

    move-result-object v4

    invoke-virtual {v4}, Lcom/peel/f/a;->b()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x1

    const-string/jumbo v6, "TV"

    const/4 v7, -0x1

    invoke-virtual/range {v0 .. v7}, Lcom/peel/util/a/f;->a(IIILjava/lang/String;ILjava/lang/String;I)V

    .line 1001
    iget-object v0, p0, Lcom/peel/i/gw;->a:Lcom/peel/i/gm;

    invoke-static {v0}, Lcom/peel/i/gm;->R(Lcom/peel/i/gm;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/i/gw;->a:Lcom/peel/i/gm;

    invoke-static {v1}, Lcom/peel/i/gm;->R(Lcom/peel/i/gm;)Landroid/os/Handler;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x2

    const/4 v4, 0x4

    invoke-virtual {v1, v2, v3, v4}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 1019
    :cond_0
    :goto_0
    return-void

    .line 1003
    :cond_1
    const/16 v0, 0xa

    if-ne p1, v0, :cond_2

    .line 1004
    iget-object v0, p0, Lcom/peel/i/gw;->a:Lcom/peel/i/gm;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/peel/i/gm;->a(Lcom/peel/i/gm;Z)Z

    .line 1005
    iget-object v0, p0, Lcom/peel/i/gw;->a:Lcom/peel/i/gm;

    invoke-static {v0, p2}, Lcom/peel/i/gm;->a(Lcom/peel/i/gm;Lcom/peel/f/a;)Lcom/peel/f/a;

    .line 1006
    invoke-static {}, Lcom/peel/util/a/f;->a()Lcom/peel/util/a/f;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/i/gw;->a:Lcom/peel/i/gm;

    invoke-static {v1}, Lcom/peel/i/gm;->b(Lcom/peel/i/gm;)Lcom/peel/control/RoomControl;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/data/at;->f()I

    move-result v1

    const/16 v2, 0xc29

    const/16 v3, 0x7d5

    iget-object v4, p0, Lcom/peel/i/gw;->a:Lcom/peel/i/gm;

    invoke-static {v4}, Lcom/peel/i/gm;->m(Lcom/peel/i/gm;)Lcom/peel/f/a;

    move-result-object v4

    invoke-virtual {v4}, Lcom/peel/f/a;->b()Ljava/lang/String;

    move-result-object v4

    const/16 v5, 0xa

    const-string/jumbo v6, "Projector"

    const/4 v7, -0x1

    invoke-virtual/range {v0 .. v7}, Lcom/peel/util/a/f;->a(IIILjava/lang/String;ILjava/lang/String;I)V

    .line 1008
    iget-object v0, p0, Lcom/peel/i/gw;->a:Lcom/peel/i/gm;

    invoke-static {v0}, Lcom/peel/i/gm;->R(Lcom/peel/i/gm;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/i/gw;->a:Lcom/peel/i/gm;

    invoke-static {v1}, Lcom/peel/i/gm;->R(Lcom/peel/i/gm;)Landroid/os/Handler;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x3

    const/4 v4, 0x4

    invoke-static {v1, v2, v3, v4}, Landroid/os/Message;->obtain(Landroid/os/Handler;III)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0

    .line 1010
    :cond_2
    const/4 v0, 0x2

    if-ne p1, v0, :cond_0

    .line 1011
    iget-object v0, p0, Lcom/peel/i/gw;->a:Lcom/peel/i/gm;

    invoke-static {v0, p2}, Lcom/peel/i/gm;->b(Lcom/peel/i/gm;Lcom/peel/f/a;)Lcom/peel/f/a;

    .line 1012
    invoke-static {}, Lcom/peel/util/a/f;->a()Lcom/peel/util/a/f;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/i/gw;->a:Lcom/peel/i/gm;

    invoke-static {v1}, Lcom/peel/i/gm;->b(Lcom/peel/i/gm;)Lcom/peel/control/RoomControl;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/data/at;->f()I

    move-result v1

    const/16 v2, 0xc29

    const/16 v3, 0x7d5

    iget-object v4, p0, Lcom/peel/i/gw;->a:Lcom/peel/i/gm;

    invoke-static {v4}, Lcom/peel/i/gm;->s(Lcom/peel/i/gm;)Lcom/peel/f/a;

    move-result-object v4

    invoke-virtual {v4}, Lcom/peel/f/a;->b()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x2

    const-string/jumbo v6, "STB"

    const/4 v7, -0x1

    invoke-virtual/range {v0 .. v7}, Lcom/peel/util/a/f;->a(IIILjava/lang/String;ILjava/lang/String;I)V

    .line 1014
    const-string/jumbo v0, "tivo"

    iget-object v1, p0, Lcom/peel/i/gw;->a:Lcom/peel/i/gm;

    invoke-static {v1}, Lcom/peel/i/gm;->s(Lcom/peel/i/gm;)Lcom/peel/f/a;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/f/a;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    .line 1015
    iget-object v9, p0, Lcom/peel/i/gw;->a:Lcom/peel/i/gm;

    const/4 v0, 0x0

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/peel/i/gw;->a:Lcom/peel/i/gm;

    invoke-static {v2}, Lcom/peel/i/gm;->s(Lcom/peel/i/gm;)Lcom/peel/f/a;

    move-result-object v2

    invoke-virtual {v2}, Lcom/peel/f/a;->b()Ljava/lang/String;

    move-result-object v2

    const/4 v4, 0x0

    const/4 v5, -0x1

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-static/range {v0 .. v8}, Lcom/peel/control/h;->a(IILjava/lang/String;ZLjava/lang/String;ILandroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;)Lcom/peel/control/h;

    move-result-object v0

    invoke-static {v9, v0}, Lcom/peel/i/gm;->b(Lcom/peel/i/gm;Lcom/peel/control/h;)Lcom/peel/control/h;

    .line 1016
    iget-object v0, p0, Lcom/peel/i/gw;->a:Lcom/peel/i/gm;

    invoke-static {v0}, Lcom/peel/i/gm;->w(Lcom/peel/i/gm;)Lcom/peel/control/h;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/peel/control/h;->a(I)V

    .line 1017
    iget-object v0, p0, Lcom/peel/i/gw;->a:Lcom/peel/i/gm;

    invoke-static {v0}, Lcom/peel/i/gm;->R(Lcom/peel/i/gm;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/i/gw;->a:Lcom/peel/i/gm;

    invoke-static {v1}, Lcom/peel/i/gm;->R(Lcom/peel/i/gm;)Landroid/os/Handler;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x7

    const/16 v4, 0x9

    invoke-static {v1, v2, v3, v4}, Landroid/os/Message;->obtain(Landroid/os/Handler;III)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto/16 :goto_0
.end method
