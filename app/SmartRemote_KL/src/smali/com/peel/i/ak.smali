.class Lcom/peel/i/ak;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/peel/i/ai;


# direct methods
.method constructor <init>(Lcom/peel/i/ai;)V
    .locals 0

    .prologue
    .line 174
    iput-object p1, p0, Lcom/peel/i/ak;->a:Lcom/peel/i/ai;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 178
    iget-object v0, p0, Lcom/peel/i/ak;->a:Lcom/peel/i/ai;

    invoke-virtual {v0}, Lcom/peel/i/ai;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    invoke-static {v0, v3}, Lcom/peel/util/dq;->a(Landroid/content/Context;Z)V

    .line 179
    iget-object v0, p0, Lcom/peel/i/ak;->a:Lcom/peel/i/ai;

    invoke-virtual {v0}, Lcom/peel/i/ai;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v1, "is_device_setup_complete"

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 180
    iget-object v0, p0, Lcom/peel/i/ak;->a:Lcom/peel/i/ai;

    invoke-virtual {v0}, Lcom/peel/i/ai;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    const-string/jumbo v1, "social_accounts_setup"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/ae;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v1, "show social login"

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 181
    iget-object v0, p0, Lcom/peel/i/ak;->a:Lcom/peel/i/ai;

    invoke-virtual {v0}, Lcom/peel/i/ai;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    invoke-static {v0, v3}, Lcom/peel/util/bx;->a(Landroid/content/Context;Z)V

    .line 184
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 185
    const-string/jumbo v1, "control_only_mode"

    invoke-virtual {v0, v1, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 186
    iget-object v1, p0, Lcom/peel/i/ak;->a:Lcom/peel/i/ai;

    invoke-virtual {v1}, Lcom/peel/i/ai;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    const-class v2, Lcom/peel/ui/gt;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lcom/peel/d/e;->a(Landroid/support/v4/app/ae;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 187
    return-void
.end method
