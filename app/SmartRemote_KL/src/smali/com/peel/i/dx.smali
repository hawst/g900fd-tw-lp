.class public Lcom/peel/i/dx;
.super Lcom/peel/d/u;


# static fields
.field private static final e:Ljava/lang/String;


# instance fields
.field private aj:Lcom/peel/util/CustomSpinner;

.field private ak:Lcom/peel/util/CustomSpinner;

.field private al:Landroid/graphics/Rect;

.field private am:Landroid/widget/TextView;

.field private an:Landroid/widget/TextView;

.field private ao:Lcom/peel/widget/ag;

.field private ap:Ljava/lang/String;

.field private aq:Ljava/lang/String;

.field private f:Landroid/view/LayoutInflater;

.field private g:Landroid/widget/LinearLayout;

.field private h:Landroid/widget/ListView;

.field private i:Landroid/app/ProgressDialog;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 51
    const-class v0, Lcom/peel/i/dx;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/peel/i/dx;->e:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 50
    invoke-direct {p0}, Lcom/peel/d/u;-><init>()V

    return-void
.end method

.method static synthetic T()Ljava/lang/String;
    .locals 1

    .prologue
    .line 50
    sget-object v0, Lcom/peel/i/dx;->e:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic a(Lcom/peel/i/dx;Landroid/app/ProgressDialog;)Landroid/app/ProgressDialog;
    .locals 0

    .prologue
    .line 50
    iput-object p1, p0, Lcom/peel/i/dx;->i:Landroid/app/ProgressDialog;

    return-object p1
.end method

.method static synthetic a(Lcom/peel/i/dx;Landroid/graphics/Rect;)Landroid/graphics/Rect;
    .locals 0

    .prologue
    .line 50
    iput-object p1, p0, Lcom/peel/i/dx;->al:Landroid/graphics/Rect;

    return-object p1
.end method

.method static synthetic a(Lcom/peel/i/dx;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/peel/i/dx;->am:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic a(Lcom/peel/i/dx;Landroid/widget/TextView;)Landroid/widget/TextView;
    .locals 0

    .prologue
    .line 50
    iput-object p1, p0, Lcom/peel/i/dx;->am:Landroid/widget/TextView;

    return-object p1
.end method

.method static synthetic a(Lcom/peel/i/dx;Lcom/peel/widget/ag;)Lcom/peel/widget/ag;
    .locals 0

    .prologue
    .line 50
    iput-object p1, p0, Lcom/peel/i/dx;->ao:Lcom/peel/widget/ag;

    return-object p1
.end method

.method static synthetic a(Lcom/peel/i/dx;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 50
    iput-object p1, p0, Lcom/peel/i/dx;->ap:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic b(Lcom/peel/i/dx;)Landroid/graphics/Rect;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/peel/i/dx;->al:Landroid/graphics/Rect;

    return-object v0
.end method

.method static synthetic b(Lcom/peel/i/dx;Landroid/widget/TextView;)Landroid/widget/TextView;
    .locals 0

    .prologue
    .line 50
    iput-object p1, p0, Lcom/peel/i/dx;->an:Landroid/widget/TextView;

    return-object p1
.end method

.method static synthetic b(Lcom/peel/i/dx;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 50
    iput-object p1, p0, Lcom/peel/i/dx;->aq:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic c(Lcom/peel/i/dx;)Lcom/peel/util/CustomSpinner;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/peel/i/dx;->aj:Lcom/peel/util/CustomSpinner;

    return-object v0
.end method

.method static synthetic d(Lcom/peel/i/dx;)Landroid/widget/LinearLayout;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/peel/i/dx;->g:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic e(Lcom/peel/i/dx;)Landroid/view/LayoutInflater;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/peel/i/dx;->f:Landroid/view/LayoutInflater;

    return-object v0
.end method

.method static synthetic f(Lcom/peel/i/dx;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/peel/i/dx;->an:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic g(Lcom/peel/i/dx;)Lcom/peel/util/CustomSpinner;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/peel/i/dx;->ak:Lcom/peel/util/CustomSpinner;

    return-object v0
.end method

.method static synthetic h(Lcom/peel/i/dx;)Landroid/widget/ListView;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/peel/i/dx;->h:Landroid/widget/ListView;

    return-object v0
.end method

.method static synthetic i(Lcom/peel/i/dx;)Lcom/peel/widget/ag;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/peel/i/dx;->ao:Lcom/peel/widget/ag;

    return-object v0
.end method

.method static synthetic j(Lcom/peel/i/dx;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/peel/i/dx;->aq:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic k(Lcom/peel/i/dx;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/peel/i/dx;->ap:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic l(Lcom/peel/i/dx;)Landroid/app/ProgressDialog;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/peel/i/dx;->i:Landroid/app/ProgressDialog;

    return-object v0
.end method


# virtual methods
.method public S()V
    .locals 3

    .prologue
    .line 423
    new-instance v0, Lcom/peel/i/eo;

    invoke-direct {v0, p0}, Lcom/peel/i/eo;-><init>(Lcom/peel/i/dx;)V

    .line 431
    invoke-static {}, Lcom/peel/util/i;->c()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 432
    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 436
    :goto_0
    return-void

    .line 434
    :cond_0
    const-class v1, Lcom/peel/i/cg;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "dismiss loading"

    invoke-static {v1, v2, v0}, Lcom/peel/util/i;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/lang/Runnable;

    goto :goto_0
.end method

.method public Z()V
    .locals 6

    .prologue
    .line 447
    iget-object v0, p0, Lcom/peel/i/dx;->d:Lcom/peel/d/a;

    if-nez v0, :cond_0

    .line 448
    new-instance v0, Lcom/peel/d/a;

    sget-object v1, Lcom/peel/d/d;->b:Lcom/peel/d/d;

    sget-object v2, Lcom/peel/d/b;->a:Lcom/peel/d/b;

    sget-object v3, Lcom/peel/d/c;->b:Lcom/peel/d/c;

    sget v4, Lcom/peel/ui/ft;->find_your_tv_service:I

    invoke-virtual {p0, v4}, Lcom/peel/i/dx;->a(I)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, Lcom/peel/d/a;-><init>(Lcom/peel/d/d;Lcom/peel/d/b;Lcom/peel/d/c;Ljava/lang/String;Ljava/util/List;)V

    iput-object v0, p0, Lcom/peel/i/dx;->d:Lcom/peel/d/a;

    .line 450
    :cond_0
    iget-object v0, p0, Lcom/peel/i/dx;->b:Lcom/peel/d/i;

    iget-object v1, p0, Lcom/peel/i/dx;->d:Lcom/peel/d/a;

    invoke-virtual {v0, v1}, Lcom/peel/d/i;->a(Lcom/peel/d/a;)V

    .line 451
    return-void
.end method

.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    .line 63
    iput-object p1, p0, Lcom/peel/i/dx;->f:Landroid/view/LayoutInflater;

    .line 64
    sget v0, Lcom/peel/ui/fq;->setup_region:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/peel/i/dx;->g:Landroid/widget/LinearLayout;

    .line 65
    iget-object v0, p0, Lcom/peel/i/dx;->g:Landroid/widget/LinearLayout;

    sget v1, Lcom/peel/ui/fp;->regions:I

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/peel/util/CustomSpinner;

    iput-object v0, p0, Lcom/peel/i/dx;->aj:Lcom/peel/util/CustomSpinner;

    .line 66
    iget-object v0, p0, Lcom/peel/i/dx;->g:Landroid/widget/LinearLayout;

    sget v1, Lcom/peel/ui/fp;->subregions:I

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/peel/util/CustomSpinner;

    iput-object v0, p0, Lcom/peel/i/dx;->ak:Lcom/peel/util/CustomSpinner;

    .line 67
    iget-object v0, p0, Lcom/peel/i/dx;->g:Landroid/widget/LinearLayout;

    sget v1, Lcom/peel/ui/fp;->tv_service_list:I

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/peel/i/dx;->h:Landroid/widget/ListView;

    .line 68
    iget-object v0, p0, Lcom/peel/i/dx;->g:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method public a(Landroid/os/Bundle;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 211
    const-string/jumbo v0, "iso"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lcom/peel/i/ed;

    const/4 v2, 0x1

    invoke-direct {v1, p0, v2, p2, p1}, Lcom/peel/i/ed;-><init>(Lcom/peel/i/dx;ILjava/lang/String;Landroid/os/Bundle;)V

    invoke-static {p2, v0, v1}, Lcom/peel/content/a/j;->b(Ljava/lang/String;Ljava/lang/String;Lcom/peel/util/t;)V

    .line 286
    return-void
.end method

.method public a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;)V
    .locals 7

    .prologue
    .line 289
    const-string/jumbo v0, "iso"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    new-instance v0, Lcom/peel/i/eh;

    const/4 v2, 0x1

    move-object v1, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/peel/i/eh;-><init>(Lcom/peel/i/dx;ILandroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {p3, p2, v6, v0}, Lcom/peel/content/a/j;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/peel/util/t;)V

    .line 395
    return-void
.end method

.method public c()V
    .locals 3

    .prologue
    .line 398
    new-instance v0, Lcom/peel/i/en;

    invoke-direct {v0, p0}, Lcom/peel/i/en;-><init>(Lcom/peel/i/dx;)V

    .line 415
    invoke-static {}, Lcom/peel/util/i;->c()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 416
    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 420
    :goto_0
    return-void

    .line 418
    :cond_0
    const-class v1, Lcom/peel/i/cg;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "show loading"

    invoke-static {v1, v2, v0}, Lcom/peel/util/i;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/lang/Runnable;

    goto :goto_0
.end method

.method public c(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 115
    invoke-super {p0, p1}, Lcom/peel/d/u;->c(Landroid/os/Bundle;)V

    .line 116
    sget-object v0, Lcom/peel/content/a;->h:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/peel/i/dx;->v()Landroid/view/View;

    move-result-object v0

    if-nez v0, :cond_1

    .line 121
    :cond_0
    :goto_0
    return-void

    .line 118
    :cond_1
    const-string/jumbo v0, "country"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    .line 119
    invoke-virtual {p0}, Lcom/peel/i/dx;->c()V

    .line 120
    invoke-virtual {p0, v0}, Lcom/peel/i/dx;->l(Landroid/os/Bundle;)V

    goto :goto_0
.end method

.method public d(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 96
    invoke-super {p0, p1}, Lcom/peel/d/u;->d(Landroid/os/Bundle;)V

    .line 97
    invoke-virtual {p0}, Lcom/peel/i/dx;->Z()V

    .line 98
    return-void
.end method

.method public e(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 440
    iget-object v0, p0, Lcom/peel/i/dx;->c:Landroid/os/Bundle;

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->putAll(Landroid/os/Bundle;)V

    .line 441
    invoke-super {p0, p1}, Lcom/peel/d/u;->e(Landroid/os/Bundle;)V

    .line 442
    return-void
.end method

.method public f()V
    .locals 3

    .prologue
    .line 87
    invoke-super {p0}, Lcom/peel/d/u;->f()V

    .line 88
    invoke-virtual {p0}, Lcom/peel/i/dx;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    const-string/jumbo v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/ae;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 89
    invoke-virtual {p0}, Lcom/peel/i/dx;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/ae;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 90
    iget-object v0, p0, Lcom/peel/i/dx;->ao:Lcom/peel/widget/ag;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/peel/i/dx;->ao:Lcom/peel/widget/ag;

    invoke-virtual {v0}, Lcom/peel/widget/ag;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 91
    iget-object v0, p0, Lcom/peel/i/dx;->ao:Lcom/peel/widget/ag;

    invoke-virtual {v0}, Lcom/peel/widget/ag;->dismiss()V

    .line 93
    :cond_0
    return-void
.end method

.method public h(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 102
    invoke-super {p0, p1}, Lcom/peel/d/u;->h(Landroid/os/Bundle;)V

    .line 103
    if-eqz p1, :cond_0

    .line 104
    iget-object v0, p0, Lcom/peel/i/dx;->c:Landroid/os/Bundle;

    invoke-virtual {v0, p1}, Landroid/os/Bundle;->putAll(Landroid/os/Bundle;)V

    .line 107
    :cond_0
    invoke-static {}, Lcom/peel/util/a/f;->a()Lcom/peel/util/a/f;

    move-result-object v1

    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v0}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v0

    if-nez v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    const/16 v2, 0xbc2

    const/16 v3, 0x7d5

    invoke-virtual {v1, v0, v2, v3}, Lcom/peel/util/a/f;->a(III)V

    .line 110
    sget-object v0, Lcom/peel/content/a;->h:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/peel/i/dx;->c:Landroid/os/Bundle;

    invoke-virtual {p0, v0}, Lcom/peel/i/dx;->c(Landroid/os/Bundle;)V

    .line 111
    :cond_1
    return-void

    .line 107
    :cond_2
    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v0}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/data/at;->f()I

    move-result v0

    goto :goto_0
.end method

.method public l(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 124
    const-string/jumbo v0, "iso"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lcom/peel/i/dz;

    const/4 v2, 0x1

    invoke-direct {v1, p0, v2, p1}, Lcom/peel/i/dz;-><init>(Lcom/peel/i/dx;ILandroid/os/Bundle;)V

    invoke-static {v0, v1}, Lcom/peel/content/a/j;->a(Ljava/lang/String;Lcom/peel/util/t;)V

    .line 208
    return-void
.end method

.method public w()V
    .locals 6

    .prologue
    .line 73
    invoke-super {p0}, Lcom/peel/d/u;->w()V

    .line 74
    invoke-virtual {p0}, Lcom/peel/i/dx;->Z()V

    .line 75
    const-class v0, Lcom/peel/i/fq;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "hide keyboard after delay"

    new-instance v2, Lcom/peel/i/dy;

    invoke-direct {v2, p0}, Lcom/peel/i/dy;-><init>(Lcom/peel/i/dx;)V

    const-wide/16 v4, 0x64

    invoke-static {v0, v1, v2, v4, v5}, Lcom/peel/util/i;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;J)Ljava/lang/Runnable;

    .line 83
    return-void
.end method
