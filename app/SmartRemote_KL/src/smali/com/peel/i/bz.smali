.class Lcom/peel/i/bz;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field final synthetic a:Lcom/peel/i/by;


# direct methods
.method constructor <init>(Lcom/peel/i/by;)V
    .locals 0

    .prologue
    .line 75
    iput-object p1, p0, Lcom/peel/i/bz;->a:Lcom/peel/i/by;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 0

    .prologue
    .line 91
    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 93
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 3

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 78
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-nez v0, :cond_1

    .line 79
    iget-object v0, p0, Lcom/peel/i/bz;->a:Lcom/peel/i/by;

    invoke-static {v0}, Lcom/peel/i/by;->a(Lcom/peel/i/by;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 80
    iget-object v0, p0, Lcom/peel/i/bz;->a:Lcom/peel/i/by;

    invoke-static {v0}, Lcom/peel/i/by;->b(Lcom/peel/i/by;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 87
    :goto_0
    iget-object v0, p0, Lcom/peel/i/bz;->a:Lcom/peel/i/by;

    invoke-static {v0}, Lcom/peel/i/by;->c(Lcom/peel/i/by;)Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lcom/peel/i/a/c;

    .line 88
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/peel/i/a/c;->getFilter()Landroid/widget/Filter;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/widget/Filter;->filter(Ljava/lang/CharSequence;)V

    .line 89
    :cond_0
    return-void

    .line 83
    :cond_1
    iget-object v0, p0, Lcom/peel/i/bz;->a:Lcom/peel/i/by;

    invoke-static {v0}, Lcom/peel/i/by;->b(Lcom/peel/i/by;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 84
    iget-object v0, p0, Lcom/peel/i/bz;->a:Lcom/peel/i/by;

    invoke-static {v0}, Lcom/peel/i/by;->a(Lcom/peel/i/by;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0
.end method
