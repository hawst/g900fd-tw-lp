.class Lcom/peel/i/c;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field final synthetic a:Lcom/peel/i/a;


# direct methods
.method constructor <init>(Lcom/peel/i/a;)V
    .locals 0

    .prologue
    .line 399
    iput-object p1, p0, Lcom/peel/i/c;->a:Lcom/peel/i/a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    .line 402
    iget-object v0, p0, Lcom/peel/i/c;->a:Lcom/peel/i/a;

    invoke-virtual {v0}, Lcom/peel/i/a;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    const-string/jumbo v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/ae;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 403
    iget-object v1, p0, Lcom/peel/i/c;->a:Lcom/peel/i/a;

    invoke-static {v1}, Lcom/peel/i/a;->p(Lcom/peel/i/a;)Landroid/widget/EditText;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/EditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 404
    iget-object v0, p0, Lcom/peel/i/c;->a:Lcom/peel/i/a;

    invoke-static {v0}, Lcom/peel/i/a;->o(Lcom/peel/i/a;)Landroid/widget/ListView;

    move-result-object v0

    invoke-static {}, Lcom/peel/i/a;->c()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/peel/util/bx;->a(Landroid/view/View;Ljava/lang/String;)V

    .line 405
    iget-object v1, p0, Lcom/peel/i/c;->a:Lcom/peel/i/a;

    invoke-virtual {p1}, Landroid/widget/AdapterView;->getAdapter()Landroid/widget/Adapter;

    move-result-object v0

    invoke-interface {v0, p3}, Landroid/widget/Adapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/f/a;

    invoke-static {v1, v0}, Lcom/peel/i/a;->a(Lcom/peel/i/a;Lcom/peel/f/a;)Lcom/peel/f/a;

    .line 407
    iget-object v0, p0, Lcom/peel/i/c;->a:Lcom/peel/i/a;

    invoke-static {v0}, Lcom/peel/i/a;->q(Lcom/peel/i/a;)Lcom/peel/i/a/a;

    move-result-object v0

    invoke-virtual {v0, p3}, Lcom/peel/i/a/a;->b(I)V

    .line 408
    iget-object v0, p0, Lcom/peel/i/c;->a:Lcom/peel/i/a;

    iget-object v1, p0, Lcom/peel/i/c;->a:Lcom/peel/i/a;

    invoke-virtual {v1}, Lcom/peel/i/a;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    iget-object v2, p0, Lcom/peel/i/c;->a:Lcom/peel/i/a;

    invoke-static {v2}, Lcom/peel/i/a;->h(Lcom/peel/i/a;)I

    move-result v2

    invoke-static {v1, v2}, Lcom/peel/util/bx;->c(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v3, v3}, Lcom/peel/i/a;->a(Lcom/peel/i/a;Ljava/lang/String;ZZ)V

    .line 409
    return-void
.end method
