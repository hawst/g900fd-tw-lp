.class public Lcom/peel/i/cr;
.super Lcom/peel/d/u;

# interfaces
.implements Lcom/peel/i/cf;


# static fields
.field private static final g:Ljava/lang/String;


# instance fields
.field private aj:Landroid/widget/ImageButton;

.field private ak:Landroid/widget/Button;

.field private al:Landroid/widget/Button;

.field private am:Landroid/widget/EditText;

.field private an:Landroid/widget/TextView;

.field private ao:Landroid/view/View;

.field private ap:Landroid/view/View;

.field private aq:Lcom/peel/util/CustomSpinner;

.field private ar:Lcom/peel/util/CustomSpinner;

.field private as:Landroid/widget/ListView;

.field private at:Lcom/peel/widget/ag;

.field private au:Landroid/app/ProgressDialog;

.field private av:Ljava/lang/String;

.field private aw:Ljava/lang/String;

.field private ax:Landroid/graphics/Rect;

.field public e:Landroid/widget/TextView;

.field public f:Landroid/widget/TextView;

.field private h:Landroid/view/LayoutInflater;

.field private i:Landroid/view/View;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 67
    const-class v0, Lcom/peel/i/cr;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/peel/i/cr;->g:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 66
    invoke-direct {p0}, Lcom/peel/d/u;-><init>()V

    return-void
.end method

.method static synthetic T()Ljava/lang/String;
    .locals 1

    .prologue
    .line 66
    sget-object v0, Lcom/peel/i/cr;->g:Ljava/lang/String;

    return-object v0
.end method

.method private U()V
    .locals 7

    .prologue
    .line 150
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 151
    iget-object v0, p0, Lcom/peel/i/cr;->c:Landroid/os/Bundle;

    const-string/jumbo v2, "countries"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Parcelable;

    .line 152
    check-cast v0, Landroid/os/Bundle;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 154
    :cond_0
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    .line 156
    sget-object v1, Lcom/peel/i/cr;->g:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "####### country[0]: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "iso"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 158
    sget-object v1, Lcom/peel/b/a;->a:Ljava/util/Map;

    const-string/jumbo v2, "endpoint"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    sput-object v1, Lcom/peel/b/a;->e:Ljava/lang/String;

    .line 159
    sget-object v1, Lcom/peel/b/a;->e:Ljava/lang/String;

    invoke-static {v1}, Lcom/peel/content/a/j;->a(Ljava/lang/String;)V

    .line 160
    const-string/jumbo v1, "iso"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/peel/content/a;->e:Ljava/lang/String;

    .line 161
    const-string/jumbo v1, "urloverride"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/peel/util/b/a;->b:Ljava/lang/String;

    .line 164
    invoke-virtual {p0}, Lcom/peel/i/cr;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    invoke-static {v1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string/jumbo v2, "config_legacy"

    .line 165
    invoke-static {v0}, Lcom/peel/util/bx;->b(Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string/jumbo v2, "country"

    const-string/jumbo v3, "name"

    .line 166
    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string/jumbo v2, "country_ISO"

    const-string/jumbo v3, "iso"

    .line 167
    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 169
    iget-object v1, p0, Lcom/peel/i/cr;->c:Landroid/os/Bundle;

    const-string/jumbo v2, "country"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 170
    const-string/jumbo v1, "XX"

    .line 171
    const-string/jumbo v2, "XX"

    const-string/jumbo v3, "iso"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 172
    const-string/jumbo v1, "XX"

    .line 182
    :cond_1
    :goto_1
    invoke-virtual {p0}, Lcom/peel/i/cr;->m()Landroid/support/v4/app/ae;

    move-result-object v2

    sget-object v3, Lcom/peel/b/a;->b:Ljava/lang/String;

    invoke-static {v2, v1, v3}, Lcom/peel/util/eg;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 183
    sget-object v2, Lcom/peel/content/a;->e:Ljava/lang/String;

    const-string/jumbo v3, "Handset"

    sget-object v4, Landroid/os/Build;->MODEL:Ljava/lang/String;

    new-instance v5, Lcom/peel/i/cx;

    const/4 v6, 0x2

    invoke-direct {v5, p0, v6, v1, v0}, Lcom/peel/i/cx;-><init>(Lcom/peel/i/cr;ILjava/lang/String;Landroid/os/Bundle;)V

    invoke-static {v1, v2, v3, v4, v5}, Lcom/peel/content/a/j;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/peel/util/t;)V

    .line 230
    iget-object v0, p0, Lcom/peel/i/cr;->c:Landroid/os/Bundle;

    invoke-virtual {p0, v0}, Lcom/peel/i/cr;->c(Landroid/os/Bundle;)V

    .line 233
    return-void

    .line 173
    :cond_2
    const-string/jumbo v2, "usa"

    const-string/jumbo v3, "endpoint"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    const-string/jumbo v1, "US"

    goto :goto_1

    .line 174
    :cond_3
    const-string/jumbo v2, "asia"

    const-string/jumbo v3, "endpoint"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    const-string/jumbo v1, "AS"

    goto :goto_1

    .line 175
    :cond_4
    const-string/jumbo v2, "europe"

    const-string/jumbo v3, "endpoint"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_5

    const-string/jumbo v1, "EU"

    goto :goto_1

    .line 176
    :cond_5
    const-string/jumbo v2, "australia"

    const-string/jumbo v3, "endpoint"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 177
    const-string/jumbo v1, "AU"

    goto :goto_1

    .line 178
    :cond_6
    const-string/jumbo v2, "latin"

    const-string/jumbo v3, "endpoint"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_7

    const-string/jumbo v1, "LA"

    goto/16 :goto_1

    .line 179
    :cond_7
    const-string/jumbo v2, "bramex"

    const-string/jumbo v3, "endpoint"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    const-string/jumbo v1, "BM"

    goto/16 :goto_1
.end method

.method private V()V
    .locals 3

    .prologue
    .line 644
    :try_start_0
    invoke-virtual {p0}, Lcom/peel/i/cr;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    const-string/jumbo v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/ae;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 645
    iget-object v1, p0, Lcom/peel/i/cr;->am:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 647
    :goto_0
    iget-object v0, p0, Lcom/peel/i/cr;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "zipCode"

    iget-object v2, p0, Lcom/peel/i/cr;->am:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 648
    invoke-virtual {p0}, Lcom/peel/i/cr;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    const-class v1, Lcom/peel/i/ep;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/peel/i/cr;->c:Landroid/os/Bundle;

    invoke-static {v0, v1, v2}, Lcom/peel/d/e;->c(Landroid/support/v4/app/ae;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 649
    return-void

    .line 646
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method static synthetic a(Lcom/peel/i/cr;Landroid/app/ProgressDialog;)Landroid/app/ProgressDialog;
    .locals 0

    .prologue
    .line 66
    iput-object p1, p0, Lcom/peel/i/cr;->au:Landroid/app/ProgressDialog;

    return-object p1
.end method

.method static synthetic a(Lcom/peel/i/cr;Landroid/graphics/Rect;)Landroid/graphics/Rect;
    .locals 0

    .prologue
    .line 66
    iput-object p1, p0, Lcom/peel/i/cr;->ax:Landroid/graphics/Rect;

    return-object p1
.end method

.method static synthetic a(Lcom/peel/i/cr;)Landroid/widget/EditText;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/peel/i/cr;->am:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic a(Lcom/peel/i/cr;Lcom/peel/widget/ag;)Lcom/peel/widget/ag;
    .locals 0

    .prologue
    .line 66
    iput-object p1, p0, Lcom/peel/i/cr;->at:Lcom/peel/widget/ag;

    return-object p1
.end method

.method static synthetic a(Lcom/peel/i/cr;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 66
    iput-object p1, p0, Lcom/peel/i/cr;->av:Ljava/lang/String;

    return-object p1
.end method

.method private a(Landroid/os/Bundle;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 517
    const-string/jumbo v0, "iso"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lcom/peel/i/dm;

    const/4 v2, 0x1

    invoke-direct {v1, p0, v2, p1, p2}, Lcom/peel/i/dm;-><init>(Lcom/peel/i/cr;ILandroid/os/Bundle;Ljava/lang/String;)V

    invoke-static {p2, v0, v1}, Lcom/peel/content/a/j;->b(Ljava/lang/String;Ljava/lang/String;Lcom/peel/util/t;)V

    .line 588
    return-void
.end method

.method private a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;)V
    .locals 7

    .prologue
    .line 311
    const-string/jumbo v0, "iso"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    new-instance v0, Lcom/peel/i/dc;

    const/4 v2, 0x1

    move-object v1, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/peel/i/dc;-><init>(Lcom/peel/i/cr;ILandroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {p3, p2, v6, v0}, Lcom/peel/content/a/j;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/peel/util/t;)V

    .line 417
    return-void
.end method

.method static synthetic a(Lcom/peel/i/cr;Landroid/os/Bundle;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 66
    invoke-direct {p0, p1, p2}, Lcom/peel/i/cr;->a(Landroid/os/Bundle;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic a(Lcom/peel/i/cr;Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 66
    invoke-direct {p0, p1, p2, p3}, Lcom/peel/i/cr;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic b(Lcom/peel/i/cr;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 66
    iput-object p1, p0, Lcom/peel/i/cr;->aw:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic b(Lcom/peel/i/cr;)V
    .locals 0

    .prologue
    .line 66
    invoke-direct {p0}, Lcom/peel/i/cr;->V()V

    return-void
.end method

.method static synthetic c(Lcom/peel/i/cr;)Landroid/widget/Button;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/peel/i/cr;->al:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic d(Lcom/peel/i/cr;)Landroid/view/View;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/peel/i/cr;->ao:Landroid/view/View;

    return-object v0
.end method

.method static synthetic e(Lcom/peel/i/cr;)Landroid/view/View;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/peel/i/cr;->ap:Landroid/view/View;

    return-object v0
.end method

.method static synthetic f(Lcom/peel/i/cr;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/peel/i/cr;->an:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic g(Lcom/peel/i/cr;)Landroid/widget/ListView;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/peel/i/cr;->as:Landroid/widget/ListView;

    return-object v0
.end method

.method static synthetic h(Lcom/peel/i/cr;)Landroid/view/LayoutInflater;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/peel/i/cr;->h:Landroid/view/LayoutInflater;

    return-object v0
.end method

.method static synthetic i(Lcom/peel/i/cr;)Lcom/peel/widget/ag;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/peel/i/cr;->at:Lcom/peel/widget/ag;

    return-object v0
.end method

.method static synthetic j(Lcom/peel/i/cr;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/peel/i/cr;->aw:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic k(Lcom/peel/i/cr;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/peel/i/cr;->av:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic l(Lcom/peel/i/cr;)Lcom/peel/util/CustomSpinner;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/peel/i/cr;->aq:Lcom/peel/util/CustomSpinner;

    return-object v0
.end method

.method private l(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 236
    sget-object v0, Lcom/peel/i/cr;->g:Ljava/lang/String;

    const-string/jumbo v1, "render zipcode or region selection"

    new-instance v2, Lcom/peel/i/da;

    invoke-direct {v2, p0, p1}, Lcom/peel/i/da;-><init>(Lcom/peel/i/cr;Landroid/os/Bundle;)V

    invoke-static {v0, v1, v2}, Lcom/peel/util/i;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/lang/Runnable;

    .line 279
    return-void
.end method

.method static synthetic m(Lcom/peel/i/cr;)Lcom/peel/util/CustomSpinner;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/peel/i/cr;->ar:Lcom/peel/util/CustomSpinner;

    return-object v0
.end method

.method private m(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 423
    const-string/jumbo v0, "iso"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lcom/peel/i/di;

    const/4 v2, 0x1

    invoke-direct {v1, p0, v2, p1}, Lcom/peel/i/di;-><init>(Lcom/peel/i/cr;ILandroid/os/Bundle;)V

    invoke-static {v0, v1}, Lcom/peel/content/a/j;->a(Ljava/lang/String;Lcom/peel/util/t;)V

    .line 514
    return-void
.end method

.method static synthetic n(Lcom/peel/i/cr;)Landroid/graphics/Rect;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/peel/i/cr;->ax:Landroid/graphics/Rect;

    return-object v0
.end method

.method static synthetic o(Lcom/peel/i/cr;)Landroid/view/View;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/peel/i/cr;->i:Landroid/view/View;

    return-object v0
.end method

.method static synthetic p(Lcom/peel/i/cr;)Landroid/app/ProgressDialog;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/peel/i/cr;->au:Landroid/app/ProgressDialog;

    return-object v0
.end method


# virtual methods
.method public S()V
    .locals 3

    .prologue
    .line 616
    new-instance v0, Lcom/peel/i/ct;

    invoke-direct {v0, p0}, Lcom/peel/i/ct;-><init>(Lcom/peel/i/cr;)V

    .line 624
    invoke-static {}, Lcom/peel/util/i;->c()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 625
    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 629
    :goto_0
    return-void

    .line 627
    :cond_0
    const-class v1, Lcom/peel/i/cg;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "dismiss loading"

    invoke-static {v1, v2, v0}, Lcom/peel/util/i;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/lang/Runnable;

    goto :goto_0
.end method

.method public Z()V
    .locals 6

    .prologue
    .line 304
    iget-object v0, p0, Lcom/peel/i/cr;->d:Lcom/peel/d/a;

    if-nez v0, :cond_0

    .line 305
    new-instance v0, Lcom/peel/d/a;

    sget-object v1, Lcom/peel/d/d;->a:Lcom/peel/d/d;

    sget-object v2, Lcom/peel/d/b;->b:Lcom/peel/d/b;

    sget-object v3, Lcom/peel/d/c;->b:Lcom/peel/d/c;

    sget v4, Lcom/peel/ui/ft;->find_your_tv_service:I

    invoke-virtual {p0, v4}, Lcom/peel/i/cr;->a(I)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, Lcom/peel/d/a;-><init>(Lcom/peel/d/d;Lcom/peel/d/b;Lcom/peel/d/c;Ljava/lang/String;Ljava/util/List;)V

    iput-object v0, p0, Lcom/peel/i/cr;->d:Lcom/peel/d/a;

    .line 307
    :cond_0
    iget-object v0, p0, Lcom/peel/i/cr;->b:Lcom/peel/d/i;

    iget-object v1, p0, Lcom/peel/i/cr;->d:Lcom/peel/d/a;

    invoke-virtual {v0, v1}, Lcom/peel/d/i;->a(Lcom/peel/d/a;)V

    .line 308
    return-void
.end method

.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    .line 90
    sget v0, Lcom/peel/ui/fq;->setup_main_selection:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/i/cr;->i:Landroid/view/View;

    .line 91
    iput-object p1, p0, Lcom/peel/i/cr;->h:Landroid/view/LayoutInflater;

    .line 93
    iget-object v0, p0, Lcom/peel/i/cr;->i:Landroid/view/View;

    sget v1, Lcom/peel/ui/fp;->search_btn:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/peel/i/cr;->aj:Landroid/widget/ImageButton;

    .line 94
    iget-object v0, p0, Lcom/peel/i/cr;->i:Landroid/view/View;

    sget v1, Lcom/peel/ui/fp;->country_btn:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/peel/i/cr;->ak:Landroid/widget/Button;

    .line 95
    iget-object v0, p0, Lcom/peel/i/cr;->i:Landroid/view/View;

    sget v1, Lcom/peel/ui/fp;->country_list_btn:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/peel/i/cr;->al:Landroid/widget/Button;

    .line 96
    iget-object v0, p0, Lcom/peel/i/cr;->i:Landroid/view/View;

    sget v1, Lcom/peel/ui/fp;->zipcode_txt:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/peel/i/cr;->am:Landroid/widget/EditText;

    .line 97
    iget-object v0, p0, Lcom/peel/i/cr;->i:Landroid/view/View;

    sget v1, Lcom/peel/ui/fp;->search_by_zipcode_layout:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/i/cr;->ao:Landroid/view/View;

    .line 98
    iget-object v0, p0, Lcom/peel/i/cr;->i:Landroid/view/View;

    sget v1, Lcom/peel/ui/fp;->search_by_region_layout:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/i/cr;->ap:Landroid/view/View;

    .line 99
    iget-object v0, p0, Lcom/peel/i/cr;->i:Landroid/view/View;

    sget v1, Lcom/peel/ui/fp;->title_setup_zipcode:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/peel/i/cr;->an:Landroid/widget/TextView;

    .line 100
    iget-object v0, p0, Lcom/peel/i/cr;->i:Landroid/view/View;

    sget v1, Lcom/peel/ui/fp;->regions:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/peel/util/CustomSpinner;

    iput-object v0, p0, Lcom/peel/i/cr;->aq:Lcom/peel/util/CustomSpinner;

    .line 101
    iget-object v0, p0, Lcom/peel/i/cr;->i:Landroid/view/View;

    sget v1, Lcom/peel/ui/fp;->subregions:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/peel/util/CustomSpinner;

    iput-object v0, p0, Lcom/peel/i/cr;->ar:Lcom/peel/util/CustomSpinner;

    .line 102
    iget-object v0, p0, Lcom/peel/i/cr;->i:Landroid/view/View;

    sget v1, Lcom/peel/ui/fp;->tv_service_list:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/peel/i/cr;->as:Landroid/widget/ListView;

    .line 104
    iget-object v0, p0, Lcom/peel/i/cr;->ak:Landroid/widget/Button;

    new-instance v1, Lcom/peel/i/cs;

    invoke-direct {v1, p0}, Lcom/peel/i/cs;-><init>(Lcom/peel/i/cr;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 113
    iget-object v0, p0, Lcom/peel/i/cr;->aj:Landroid/widget/ImageButton;

    new-instance v1, Lcom/peel/i/cv;

    invoke-direct {v1, p0}, Lcom/peel/i/cv;-><init>(Lcom/peel/i/cr;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 126
    iget-object v0, p0, Lcom/peel/i/cr;->am:Landroid/widget/EditText;

    new-instance v1, Lcom/peel/i/cw;

    invoke-direct {v1, p0}, Lcom/peel/i/cw;-><init>(Lcom/peel/i/cr;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 136
    iget-object v0, p0, Lcom/peel/i/cr;->i:Landroid/view/View;

    return-object v0
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 634
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "Render selections"

    new-instance v2, Lcom/peel/i/cu;

    invoke-direct {v2, p0, p1}, Lcom/peel/i/cu;-><init>(Lcom/peel/i/cr;Landroid/os/Bundle;)V

    invoke-static {v0, v1, v2}, Lcom/peel/util/i;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/lang/Runnable;

    .line 640
    return-void
.end method

.method public c()V
    .locals 3

    .prologue
    .line 591
    new-instance v0, Lcom/peel/i/dq;

    invoke-direct {v0, p0}, Lcom/peel/i/dq;-><init>(Lcom/peel/i/cr;)V

    .line 608
    invoke-static {}, Lcom/peel/util/i;->c()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 609
    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 613
    :goto_0
    return-void

    .line 611
    :cond_0
    const-class v1, Lcom/peel/i/cg;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "show loading"

    invoke-static {v1, v2, v0}, Lcom/peel/util/i;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/lang/Runnable;

    goto :goto_0
.end method

.method public c(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 289
    invoke-super {p0, p1}, Lcom/peel/d/u;->c(Landroid/os/Bundle;)V

    .line 290
    const-string/jumbo v0, "country"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    .line 292
    const-string/jumbo v1, "region"

    const-string/jumbo v2, "type"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string/jumbo v1, "subregion"

    const-string/jumbo v2, "type"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 293
    :cond_0
    sget-object v1, Lcom/peel/content/a;->h:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Lcom/peel/i/cr;->v()Landroid/view/View;

    move-result-object v1

    if-nez v1, :cond_2

    .line 299
    :cond_1
    :goto_0
    return-void

    .line 294
    :cond_2
    invoke-virtual {p0}, Lcom/peel/i/cr;->c()V

    .line 295
    invoke-direct {p0, v0}, Lcom/peel/i/cr;->m(Landroid/os/Bundle;)V

    .line 298
    :cond_3
    invoke-direct {p0, v0}, Lcom/peel/i/cr;->l(Landroid/os/Bundle;)V

    goto :goto_0
.end method

.method public e(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 283
    iget-object v0, p0, Lcom/peel/i/cr;->c:Landroid/os/Bundle;

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->putAll(Landroid/os/Bundle;)V

    .line 284
    invoke-super {p0, p1}, Lcom/peel/d/u;->e(Landroid/os/Bundle;)V

    .line 285
    return-void
.end method

.method public h(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 140
    invoke-super {p0, p1}, Lcom/peel/d/u;->h(Landroid/os/Bundle;)V

    .line 141
    invoke-virtual {p0}, Lcom/peel/i/cr;->Z()V

    .line 143
    if-eqz p1, :cond_0

    .line 144
    iget-object v0, p0, Lcom/peel/i/cr;->c:Landroid/os/Bundle;

    invoke-virtual {v0, p1}, Landroid/os/Bundle;->putAll(Landroid/os/Bundle;)V

    .line 146
    :cond_0
    invoke-direct {p0}, Lcom/peel/i/cr;->U()V

    .line 147
    return-void
.end method
