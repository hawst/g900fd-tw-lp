.class public Lcom/peel/i/by;
.super Lcom/peel/d/u;


# static fields
.field private static final f:Ljava/lang/String;


# instance fields
.field private aj:Landroid/widget/ImageView;

.field public e:Lcom/peel/i/cf;

.field private g:Landroid/widget/ListView;

.field private h:Landroid/widget/AutoCompleteTextView;

.field private i:Landroid/widget/ImageView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 49
    const-class v0, Lcom/peel/i/by;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/peel/i/by;->f:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 48
    invoke-direct {p0}, Lcom/peel/d/u;-><init>()V

    .line 56
    return-void
.end method

.method static synthetic a(Lcom/peel/i/by;)Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/peel/i/by;->aj:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic b(Lcom/peel/i/by;)Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/peel/i/by;->i:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic c(Lcom/peel/i/by;)Landroid/widget/ListView;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/peel/i/by;->g:Landroid/widget/ListView;

    return-object v0
.end method

.method static synthetic c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 48
    sget-object v0, Lcom/peel/i/by;->f:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic d(Lcom/peel/i/by;)Landroid/widget/AutoCompleteTextView;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/peel/i/by;->h:Landroid/widget/AutoCompleteTextView;

    return-object v0
.end method


# virtual methods
.method public Z()V
    .locals 6

    .prologue
    .line 259
    iget-object v0, p0, Lcom/peel/i/by;->d:Lcom/peel/d/a;

    if-nez v0, :cond_0

    .line 260
    new-instance v0, Lcom/peel/d/a;

    sget-object v1, Lcom/peel/d/d;->b:Lcom/peel/d/d;

    sget-object v2, Lcom/peel/d/b;->a:Lcom/peel/d/b;

    sget-object v3, Lcom/peel/d/c;->b:Lcom/peel/d/c;

    sget v4, Lcom/peel/ui/ft;->label_select_country_region:I

    .line 261
    invoke-virtual {p0, v4}, Lcom/peel/i/by;->a(I)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, Lcom/peel/d/a;-><init>(Lcom/peel/d/d;Lcom/peel/d/b;Lcom/peel/d/c;Ljava/lang/String;Ljava/util/List;)V

    iput-object v0, p0, Lcom/peel/i/by;->d:Lcom/peel/d/a;

    .line 263
    :cond_0
    iget-object v0, p0, Lcom/peel/i/by;->b:Lcom/peel/d/i;

    iget-object v1, p0, Lcom/peel/i/by;->d:Lcom/peel/d/a;

    invoke-virtual {v0, v1}, Lcom/peel/d/i;->a(Lcom/peel/d/a;)V

    .line 264
    return-void
.end method

.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4

    .prologue
    .line 61
    sget v0, Lcom/peel/ui/fq;->setup_country:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 62
    sget v0, Lcom/peel/ui/fp;->country_list:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/peel/i/by;->g:Landroid/widget/ListView;

    .line 63
    sget v0, Lcom/peel/ui/fp;->country_list_filter:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/AutoCompleteTextView;

    iput-object v0, p0, Lcom/peel/i/by;->h:Landroid/widget/AutoCompleteTextView;

    .line 65
    iget-object v0, p0, Lcom/peel/i/by;->h:Landroid/widget/AutoCompleteTextView;

    if-nez v0, :cond_0

    .line 66
    sget v0, Lcom/peel/ui/fp;->other_tv_list_filter:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/AutoCompleteTextView;

    iput-object v0, p0, Lcom/peel/i/by;->h:Landroid/widget/AutoCompleteTextView;

    .line 70
    :goto_0
    invoke-virtual {p0}, Lcom/peel/i/by;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    invoke-static {v0}, Lcom/peel/util/g;->a(Landroid/app/Activity;)V

    .line 71
    iget-object v0, p0, Lcom/peel/i/by;->h:Landroid/widget/AutoCompleteTextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "       "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget v3, Lcom/peel/ui/ft;->label_select_country_region:I

    invoke-virtual {p0, v3}, Lcom/peel/i/by;->a(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/AutoCompleteTextView;->setHint(Ljava/lang/CharSequence;)V

    .line 72
    sget v0, Lcom/peel/ui/fp;->search_icon:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/peel/i/by;->i:Landroid/widget/ImageView;

    .line 73
    sget v0, Lcom/peel/ui/fp;->search_cancel_btn:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/peel/i/by;->aj:Landroid/widget/ImageView;

    .line 75
    iget-object v0, p0, Lcom/peel/i/by;->h:Landroid/widget/AutoCompleteTextView;

    new-instance v2, Lcom/peel/i/bz;

    invoke-direct {v2, p0}, Lcom/peel/i/bz;-><init>(Lcom/peel/i/by;)V

    invoke-virtual {v0, v2}, Landroid/widget/AutoCompleteTextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 96
    iget-object v0, p0, Lcom/peel/i/by;->aj:Landroid/widget/ImageView;

    new-instance v2, Lcom/peel/i/ca;

    invoke-direct {v2, p0}, Lcom/peel/i/ca;-><init>(Lcom/peel/i/by;)V

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 106
    :try_start_0
    invoke-virtual {p0}, Lcom/peel/i/by;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/ae;->getSupportFragmentManager()Landroid/support/v4/app/aj;

    move-result-object v0

    const-class v2, Lcom/peel/i/cr;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/support/v4/app/aj;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/peel/i/cf;

    iput-object v0, p0, Lcom/peel/i/by;->e:Lcom/peel/i/cf;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    .line 111
    return-object v1

    .line 68
    :cond_0
    iget-object v0, p0, Lcom/peel/i/by;->h:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v0}, Landroid/widget/AutoCompleteTextView;->getEditableText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->clear()V

    goto :goto_0

    .line 107
    :catch_0
    move-exception v0

    .line 108
    new-instance v0, Ljava/lang/ClassCastException;

    const-string/jumbo v1, "Does not implement Listener"

    invoke-direct {v0, v1}, Ljava/lang/ClassCastException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public e(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 252
    iget-object v0, p0, Lcom/peel/i/by;->c:Landroid/os/Bundle;

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->putAll(Landroid/os/Bundle;)V

    .line 253
    invoke-super {p0, p1}, Lcom/peel/d/u;->e(Landroid/os/Bundle;)V

    .line 254
    return-void
.end method

.method public g()V
    .locals 1

    .prologue
    .line 268
    iget-object v0, p0, Lcom/peel/i/by;->e:Lcom/peel/i/cf;

    if-eqz v0, :cond_0

    .line 269
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/peel/i/by;->e:Lcom/peel/i/cf;

    .line 270
    :cond_0
    invoke-super {p0}, Lcom/peel/d/u;->g()V

    .line 271
    return-void
.end method

.method public h(Landroid/os/Bundle;)V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 123
    invoke-super {p0, p1}, Lcom/peel/d/u;->h(Landroid/os/Bundle;)V

    .line 124
    invoke-virtual {p0}, Lcom/peel/i/by;->Z()V

    .line 126
    if-eqz p1, :cond_0

    .line 127
    iget-object v0, p0, Lcom/peel/i/by;->c:Landroid/os/Bundle;

    invoke-virtual {v0, p1}, Landroid/os/Bundle;->putAll(Landroid/os/Bundle;)V

    .line 130
    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 131
    iget-object v0, p0, Lcom/peel/i/by;->c:Landroid/os/Bundle;

    const-string/jumbo v2, "countries"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Parcelable;

    .line 132
    check-cast v0, Landroid/os/Bundle;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 135
    :cond_1
    new-instance v0, Lcom/peel/i/a/c;

    invoke-virtual {p0}, Lcom/peel/i/by;->m()Landroid/support/v4/app/ae;

    move-result-object v2

    sget v3, Lcom/peel/ui/fq;->country_row:I

    iget-object v4, p0, Lcom/peel/i/by;->c:Landroid/os/Bundle;

    const-string/jumbo v5, "selected_country"

    invoke-virtual {v4, v5, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v4

    invoke-direct {v0, v2, v3, v1, v4}, Lcom/peel/i/a/c;-><init>(Landroid/content/Context;ILjava/util/List;I)V

    .line 136
    iget-object v1, p0, Lcom/peel/i/by;->g:Landroid/widget/ListView;

    invoke-virtual {v1, v7}, Landroid/widget/ListView;->setChoiceMode(I)V

    .line 137
    iget-object v1, p0, Lcom/peel/i/by;->g:Landroid/widget/ListView;

    invoke-virtual {v1, v0}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 138
    iget-object v1, p0, Lcom/peel/i/by;->g:Landroid/widget/ListView;

    new-instance v2, Lcom/peel/i/cb;

    invoke-direct {v2, p0, v0}, Lcom/peel/i/cb;-><init>(Lcom/peel/i/by;Lcom/peel/i/a/c;)V

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 247
    iget-object v0, p0, Lcom/peel/i/by;->g:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/peel/i/by;->c:Landroid/os/Bundle;

    const-string/jumbo v2, "selected_country"

    invoke-virtual {v1, v2, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    invoke-virtual {v0, v1, v7}, Landroid/widget/ListView;->setItemChecked(IZ)V

    .line 248
    iget-object v0, p0, Lcom/peel/i/by;->g:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/peel/i/by;->g:Landroid/widget/ListView;

    invoke-virtual {v1}, Landroid/widget/ListView;->getCheckedItemPosition()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setSelection(I)V

    .line 249
    return-void
.end method

.method public w()V
    .locals 6

    .prologue
    .line 116
    invoke-super {p0}, Lcom/peel/d/u;->w()V

    .line 117
    iget-object v0, p0, Lcom/peel/i/by;->h:Landroid/widget/AutoCompleteTextView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/peel/i/by;->h:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v0}, Landroid/widget/AutoCompleteTextView;->isFocused()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 118
    invoke-virtual {p0}, Lcom/peel/i/by;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    const-class v1, Lcom/peel/i/gm;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/peel/i/by;->h:Landroid/widget/AutoCompleteTextView;

    const-wide/16 v4, 0xfa

    invoke-static {v0, v1, v2, v4, v5}, Lcom/peel/util/bx;->a(Landroid/content/Context;Ljava/lang/String;Landroid/widget/EditText;J)V

    .line 120
    :cond_0
    return-void
.end method
