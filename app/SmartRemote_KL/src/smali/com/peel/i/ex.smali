.class Lcom/peel/i/ex;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic a:Lcom/peel/i/ev;


# direct methods
.method constructor <init>(Lcom/peel/i/ev;)V
    .locals 0

    .prologue
    .line 288
    iput-object p1, p0, Lcom/peel/i/ex;->a:Lcom/peel/i/ev;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 5

    .prologue
    .line 292
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 293
    iget-object v1, p0, Lcom/peel/i/ex;->a:Lcom/peel/i/ev;

    iget-object v1, v1, Lcom/peel/i/ev;->a:Lcom/peel/i/et;

    iget-object v1, v1, Lcom/peel/i/et;->a:Landroid/os/Bundle;

    const-string/jumbo v2, "type"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "zip"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 294
    const-string/jumbo v1, "zipcode"

    const-string/jumbo v2, "N/A"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 298
    :goto_0
    iget-object v1, p0, Lcom/peel/i/ex;->a:Lcom/peel/i/ev;

    iget-object v1, v1, Lcom/peel/i/ev;->a:Lcom/peel/i/et;

    iget-object v1, v1, Lcom/peel/i/et;->c:Lcom/peel/i/ep;

    invoke-virtual {v1}, Lcom/peel/i/ep;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    const-class v2, Lcom/peel/i/bm;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lcom/peel/d/e;->c(Landroid/support/v4/app/ae;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 300
    invoke-static {}, Lcom/peel/util/a/f;->a()Lcom/peel/util/a/f;

    move-result-object v1

    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v0}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    const/16 v2, 0xbe2

    const/16 v3, 0x7d5

    iget-object v4, p0, Lcom/peel/i/ex;->a:Lcom/peel/i/ev;

    iget-object v4, v4, Lcom/peel/i/ev;->a:Lcom/peel/i/et;

    iget-object v4, v4, Lcom/peel/i/et;->b:Ljava/lang/String;

    invoke-virtual {v1, v0, v2, v3, v4}, Lcom/peel/util/a/f;->a(IIILjava/lang/String;)V

    .line 302
    return-void

    .line 296
    :cond_0
    const-string/jumbo v1, "zipcode"

    iget-object v2, p0, Lcom/peel/i/ex;->a:Lcom/peel/i/ev;

    iget-object v2, v2, Lcom/peel/i/ev;->a:Lcom/peel/i/et;

    iget-object v2, v2, Lcom/peel/i/et;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 300
    :cond_1
    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v0}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/data/at;->f()I

    move-result v0

    goto :goto_1
.end method
