.class Lcom/peel/i/fa;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/peel/i/ep;


# direct methods
.method constructor <init>(Lcom/peel/i/ep;)V
    .locals 0

    .prologue
    .line 415
    iput-object p1, p0, Lcom/peel/i/fa;->a:Lcom/peel/i/ep;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 418
    iget-object v0, p0, Lcom/peel/i/fa;->a:Lcom/peel/i/ep;

    invoke-virtual {v0}, Lcom/peel/i/ep;->s()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 419
    iget-object v0, p0, Lcom/peel/i/fa;->a:Lcom/peel/i/ep;

    invoke-static {v0}, Lcom/peel/i/ep;->j(Lcom/peel/i/ep;)Landroid/app/ProgressDialog;

    move-result-object v0

    if-nez v0, :cond_0

    .line 420
    iget-object v0, p0, Lcom/peel/i/fa;->a:Lcom/peel/i/ep;

    new-instance v1, Landroid/app/ProgressDialog;

    iget-object v2, p0, Lcom/peel/i/fa;->a:Lcom/peel/i/ep;

    invoke-virtual {v2}, Lcom/peel/i/ep;->m()Landroid/support/v4/app/ae;

    move-result-object v2

    sget v3, Lcom/peel/ui/fu;->DialogTheme:I

    invoke-direct {v1, v2, v3}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;I)V

    invoke-static {v0, v1}, Lcom/peel/i/ep;->a(Lcom/peel/i/ep;Landroid/app/ProgressDialog;)Landroid/app/ProgressDialog;

    .line 421
    iget-object v0, p0, Lcom/peel/i/fa;->a:Lcom/peel/i/ep;

    invoke-static {v0}, Lcom/peel/i/ep;->j(Lcom/peel/i/ep;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    .line 422
    iget-object v0, p0, Lcom/peel/i/fa;->a:Lcom/peel/i/ep;

    invoke-static {v0}, Lcom/peel/i/ep;->j(Lcom/peel/i/ep;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 425
    :cond_0
    iget-object v0, p0, Lcom/peel/i/fa;->a:Lcom/peel/i/ep;

    invoke-static {v0}, Lcom/peel/i/ep;->j(Lcom/peel/i/ep;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-nez v0, :cond_1

    .line 426
    iget-object v0, p0, Lcom/peel/i/fa;->a:Lcom/peel/i/ep;

    invoke-static {v0}, Lcom/peel/i/ep;->j(Lcom/peel/i/ep;)Landroid/app/ProgressDialog;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/i/fa;->a:Lcom/peel/i/ep;

    sget v2, Lcom/peel/ui/ft;->please_wait:I

    invoke-virtual {v1, v2}, Lcom/peel/i/ep;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 427
    iget-object v0, p0, Lcom/peel/i/fa;->a:Lcom/peel/i/ep;

    invoke-static {v0}, Lcom/peel/i/ep;->j(Lcom/peel/i/ep;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    .line 428
    iget-object v0, p0, Lcom/peel/i/fa;->a:Lcom/peel/i/ep;

    invoke-static {v0}, Lcom/peel/i/ep;->j(Lcom/peel/i/ep;)Landroid/app/ProgressDialog;

    move-result-object v0

    const v1, 0x102000b

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 429
    iget-object v1, p0, Lcom/peel/i/fa;->a:Lcom/peel/i/ep;

    sget v2, Lcom/peel/ui/ft;->please_wait:I

    invoke-virtual {v1, v2}, Lcom/peel/i/ep;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 430
    iget-object v1, p0, Lcom/peel/i/fa;->a:Lcom/peel/i/ep;

    invoke-virtual {v1}, Lcom/peel/i/ep;->n()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/peel/ui/fn;->progress_dialog_text:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    invoke-virtual {v0, v4, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 433
    :cond_1
    return-void
.end method
