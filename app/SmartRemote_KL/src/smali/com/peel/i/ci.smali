.class Lcom/peel/i/ci;
.super Lcom/peel/util/t;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/peel/util/t",
        "<[[",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/peel/i/ch;


# direct methods
.method constructor <init>(Lcom/peel/i/ch;)V
    .locals 0

    .prologue
    .line 333
    iput-object p1, p0, Lcom/peel/i/ci;->a:Lcom/peel/i/ch;

    invoke-direct {p0}, Lcom/peel/util/t;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 9

    .prologue
    const/4 v3, 0x0

    .line 335
    iget-object v0, p0, Lcom/peel/i/ci;->j:Ljava/lang/Object;

    move-object v6, v0

    check-cast v6, [[Ljava/lang/String;

    .line 336
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    move v7, v3

    .line 337
    :goto_0
    array-length v0, v6

    if-ge v7, v0, :cond_1

    .line 339
    iget-object v0, p0, Lcom/peel/i/ci;->a:Lcom/peel/i/ch;

    iget-object v0, v0, Lcom/peel/i/ch;->a:Lcom/peel/i/cg;

    invoke-virtual {v0}, Lcom/peel/i/cg;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string/jumbo v1, "country_ISO"

    const-string/jumbo v2, ""

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "KR"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 340
    aget-object v0, v6, v7

    aget-object v0, v0, v3

    const-string/jumbo v1, "Broadcast Channels"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 337
    :goto_1
    add-int/lit8 v0, v7, 0x1

    move v7, v0

    goto :goto_0

    .line 345
    :cond_0
    new-instance v0, Lcom/peel/data/Genre;

    aget-object v1, v6, v7

    const/4 v2, 0x1

    aget-object v1, v1, v2

    aget-object v2, v6, v7

    aget-object v2, v2, v3

    aget-object v4, v6, v7

    const/4 v5, 0x2

    aget-object v4, v4, v5

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    move v5, v3

    invoke-direct/range {v0 .. v5}, Lcom/peel/data/Genre;-><init>(Ljava/lang/String;Ljava/lang/String;IIZ)V

    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 347
    :cond_1
    sget-object v0, Lcom/peel/content/a;->f:Lcom/peel/content/user/User;

    invoke-virtual {v0, v8}, Lcom/peel/content/user/User;->b(Ljava/util/List;)V

    .line 348
    return-void
.end method
