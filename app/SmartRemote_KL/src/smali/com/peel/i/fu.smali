.class Lcom/peel/i/fu;
.super Lcom/peel/util/t;


# instance fields
.field final synthetic a:Landroid/os/Bundle;

.field final synthetic b:Lcom/peel/i/fq;


# direct methods
.method constructor <init>(Lcom/peel/i/fq;Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 165
    iput-object p1, p0, Lcom/peel/i/fu;->b:Lcom/peel/i/fq;

    iput-object p2, p0, Lcom/peel/i/fu;->a:Landroid/os/Bundle;

    invoke-direct {p0}, Lcom/peel/util/t;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 168
    const-class v0, Lcom/peel/i/fq;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/i/fu;->b:Lcom/peel/i/fq;

    invoke-virtual {v1}, Lcom/peel/i/fq;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/peel/util/bx;->a(Ljava/lang/String;Landroid/app/Activity;Z)V

    .line 169
    iget-boolean v0, p0, Lcom/peel/i/fu;->i:Z

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/peel/i/fu;->j:Ljava/lang/Object;

    if-eqz v0, :cond_1

    .line 171
    iget-object v1, p0, Lcom/peel/i/fu;->a:Landroid/os/Bundle;

    const-string/jumbo v2, "languages"

    new-instance v3, Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/peel/i/fu;->j:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    const-string/jumbo v4, ","

    invoke-static {v0, v4}, Landroid/text/TextUtils;->split(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-direct {v3, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 173
    iget-object v0, p0, Lcom/peel/i/fu;->b:Lcom/peel/i/fq;

    iget-object v0, v0, Lcom/peel/i/fq;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "country"

    iget-object v2, p0, Lcom/peel/i/fu;->a:Landroid/os/Bundle;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 174
    const-string/jumbo v0, "region"

    iget-object v1, p0, Lcom/peel/i/fu;->a:Landroid/os/Bundle;

    const-string/jumbo v2, "type"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string/jumbo v0, "subregion"

    iget-object v1, p0, Lcom/peel/i/fu;->a:Landroid/os/Bundle;

    const-string/jumbo v2, "type"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 175
    :cond_0
    iget-object v0, p0, Lcom/peel/i/fu;->b:Lcom/peel/i/fq;

    invoke-virtual {v0}, Lcom/peel/i/fq;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    const-class v1, Lcom/peel/i/dx;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/peel/i/fu;->b:Lcom/peel/i/fq;

    iget-object v2, v2, Lcom/peel/i/fq;->c:Landroid/os/Bundle;

    invoke-static {v0, v1, v2}, Lcom/peel/d/e;->c(Landroid/support/v4/app/ae;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 180
    :cond_1
    :goto_0
    return-void

    .line 177
    :cond_2
    iget-object v0, p0, Lcom/peel/i/fu;->b:Lcom/peel/i/fq;

    invoke-virtual {v0}, Lcom/peel/i/fq;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    const-class v1, Lcom/peel/i/ep;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/peel/i/fu;->b:Lcom/peel/i/fq;

    iget-object v2, v2, Lcom/peel/i/fq;->c:Landroid/os/Bundle;

    invoke-static {v0, v1, v2}, Lcom/peel/d/e;->c(Landroid/support/v4/app/ae;Ljava/lang/String;Landroid/os/Bundle;)V

    goto :goto_0
.end method
