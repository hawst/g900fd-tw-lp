.class Lcom/peel/i/fg;
.super Lcom/peel/util/t;


# instance fields
.field final synthetic a:Lcom/peel/i/fd;


# direct methods
.method constructor <init>(Lcom/peel/i/fd;)V
    .locals 0

    .prologue
    .line 148
    iput-object p1, p0, Lcom/peel/i/fg;->a:Lcom/peel/i/fd;

    invoke-direct {p0}, Lcom/peel/util/t;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 12

    .prologue
    const/4 v0, 0x1

    .line 152
    iget-boolean v1, p0, Lcom/peel/i/fg;->i:Z

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/peel/i/fg;->j:Ljava/lang/Object;

    if-eqz v1, :cond_2

    .line 153
    iget-object v1, p0, Lcom/peel/i/fg;->j:Ljava/lang/Object;

    move-object v9, v1

    check-cast v9, Ljava/util/ArrayList;

    .line 154
    if-eqz v9, :cond_1

    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_1

    .line 155
    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v1

    new-array v11, v1, [Lcom/peel/control/h;

    .line 156
    const/4 v1, 0x0

    move v10, v1

    :goto_0
    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v1

    if-ge v10, v1, :cond_0

    .line 157
    const/4 v1, 0x6

    iget-object v2, p0, Lcom/peel/i/fg;->a:Lcom/peel/i/fd;

    iget-object v2, v2, Lcom/peel/i/fd;->a:Lcom/peel/i/fc;

    invoke-static {v2}, Lcom/peel/i/fc;->b(Lcom/peel/i/fc;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v9, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/peel/control/c/b;

    invoke-virtual {v3}, Lcom/peel/control/c/b;->b()Ljava/lang/String;

    move-result-object v4

    const/16 v5, 0x1f7c

    invoke-interface {v9, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/peel/control/c/b;

    invoke-virtual {v3}, Lcom/peel/control/c/b;->a()Landroid/os/Bundle;

    move-result-object v6

    const-string/jumbo v7, "Roku"

    const-string/jumbo v8, ""

    move v3, v0

    invoke-static/range {v0 .. v8}, Lcom/peel/control/h;->a(IILjava/lang/String;ZLjava/lang/String;ILandroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;)Lcom/peel/control/h;

    move-result-object v1

    aput-object v1, v11, v10

    .line 156
    add-int/lit8 v1, v10, 0x1

    move v10, v1

    goto :goto_0

    .line 159
    :cond_0
    invoke-static {}, Lcom/peel/i/fc;->c()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "getRokuLocations"

    new-instance v2, Lcom/peel/i/fh;

    invoke-direct {v2, p0, v11}, Lcom/peel/i/fh;-><init>(Lcom/peel/i/fg;[Lcom/peel/control/h;)V

    invoke-static {v0, v1, v2}, Lcom/peel/util/i;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/lang/Runnable;

    .line 181
    :goto_1
    return-void

    .line 166
    :cond_1
    invoke-static {}, Lcom/peel/i/fc;->c()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "fail to get roku"

    new-instance v2, Lcom/peel/i/fi;

    invoke-direct {v2, p0}, Lcom/peel/i/fi;-><init>(Lcom/peel/i/fg;)V

    invoke-static {v0, v1, v2}, Lcom/peel/util/i;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/lang/Runnable;

    goto :goto_1

    .line 174
    :cond_2
    invoke-static {}, Lcom/peel/i/fc;->c()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "fail to get roku"

    new-instance v2, Lcom/peel/i/fj;

    invoke-direct {v2, p0}, Lcom/peel/i/fj;-><init>(Lcom/peel/i/fg;)V

    invoke-static {v0, v1, v2}, Lcom/peel/util/i;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/lang/Runnable;

    goto :goto_1
.end method
