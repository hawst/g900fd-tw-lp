.class Lcom/peel/i/ck;
.super Lcom/peel/util/t;


# instance fields
.field final synthetic a:Landroid/os/Bundle;

.field final synthetic b:Lcom/peel/data/ContentRoom;

.field final synthetic c:Lcom/peel/content/library/LiveLibrary;

.field final synthetic d:Landroid/os/Bundle;

.field final synthetic e:Landroid/os/Bundle;

.field final synthetic f:Z

.field final synthetic g:Lcom/peel/i/cg;


# direct methods
.method constructor <init>(Lcom/peel/i/cg;Landroid/os/Bundle;Lcom/peel/data/ContentRoom;Lcom/peel/content/library/LiveLibrary;Landroid/os/Bundle;Landroid/os/Bundle;Z)V
    .locals 0

    .prologue
    .line 391
    iput-object p1, p0, Lcom/peel/i/ck;->g:Lcom/peel/i/cg;

    iput-object p2, p0, Lcom/peel/i/ck;->a:Landroid/os/Bundle;

    iput-object p3, p0, Lcom/peel/i/ck;->b:Lcom/peel/data/ContentRoom;

    iput-object p4, p0, Lcom/peel/i/ck;->c:Lcom/peel/content/library/LiveLibrary;

    iput-object p5, p0, Lcom/peel/i/ck;->d:Landroid/os/Bundle;

    iput-object p6, p0, Lcom/peel/i/ck;->e:Landroid/os/Bundle;

    iput-boolean p7, p0, Lcom/peel/i/ck;->f:Z

    invoke-direct {p0}, Lcom/peel/util/t;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v2, 0x0

    const/4 v7, 0x0

    .line 394
    iget-boolean v0, p0, Lcom/peel/i/ck;->i:Z

    if-nez v0, :cond_0

    .line 397
    iget-object v0, p0, Lcom/peel/i/ck;->g:Lcom/peel/i/cg;

    invoke-static {v0}, Lcom/peel/i/cg;->a(Lcom/peel/i/cg;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "error getting lineup/channels: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/peel/i/ck;->k:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 400
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "report channel lineup issues"

    new-instance v2, Lcom/peel/i/cl;

    invoke-direct {v2, p0}, Lcom/peel/i/cl;-><init>(Lcom/peel/i/ck;)V

    invoke-static {v0, v1, v2}, Lcom/peel/util/i;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/lang/Runnable;

    .line 539
    :goto_0
    return-void

    .line 430
    :cond_0
    iget-object v0, p0, Lcom/peel/i/ck;->g:Lcom/peel/i/cg;

    iget-object v0, v0, Lcom/peel/i/cg;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "content_library"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 431
    iget-object v0, p0, Lcom/peel/i/ck;->g:Lcom/peel/i/cg;

    iget-object v0, v0, Lcom/peel/i/cg;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "content_library"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/peel/content/library/Library;

    .line 434
    sget-object v1, Lcom/peel/content/a;->f:Lcom/peel/content/user/User;

    invoke-virtual {v1}, Lcom/peel/content/user/User;->d()Landroid/os/Bundle;

    move-result-object v1

    .line 435
    iget-object v3, p0, Lcom/peel/i/ck;->a:Landroid/os/Bundle;

    const-string/jumbo v4, "languages"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v6, p0, Lcom/peel/i/ck;->b:Lcom/peel/data/ContentRoom;

    invoke-virtual {v6}, Lcom/peel/data/ContentRoom;->a()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, "/"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Lcom/peel/content/library/Library;->e()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v3, v4, v0}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 442
    :cond_1
    iget-object v0, p0, Lcom/peel/i/ck;->g:Lcom/peel/i/cg;

    iget-object v0, v0, Lcom/peel/i/cg;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "content_room"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 443
    iget-object v0, p0, Lcom/peel/i/ck;->b:Lcom/peel/data/ContentRoom;

    invoke-virtual {v0}, Lcom/peel/data/ContentRoom;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v2, v8, v7}, Lcom/peel/content/a;->a(Ljava/lang/String;ZZLcom/peel/util/t;)V

    .line 445
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 446
    const-string/jumbo v1, "providerid"

    iget-object v3, p0, Lcom/peel/i/ck;->c:Lcom/peel/content/library/LiveLibrary;

    invoke-virtual {v3}, Lcom/peel/content/library/LiveLibrary;->e()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v1, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 447
    const-string/jumbo v1, "zipcode"

    iget-object v3, p0, Lcom/peel/i/ck;->c:Lcom/peel/content/library/LiveLibrary;

    invoke-virtual {v3}, Lcom/peel/content/library/LiveLibrary;->h()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v1, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 448
    const-string/jumbo v1, "roomid"

    iget-object v3, p0, Lcom/peel/i/ck;->b:Lcom/peel/data/ContentRoom;

    invoke-virtual {v3}, Lcom/peel/data/ContentRoom;->b()I

    move-result v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v1, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 449
    const-string/jumbo v1, "hdpreference"

    iget-object v3, p0, Lcom/peel/i/ck;->d:Landroid/os/Bundle;

    const-string/jumbo v4, "hdprefs"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v1, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 450
    const-string/jumbo v1, "langpreference"

    const-string/jumbo v3, ","

    iget-object v4, p0, Lcom/peel/i/ck;->a:Landroid/os/Bundle;

    const-string/jumbo v5, "languages"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v1, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 451
    const-string/jumbo v1, "tiers"

    const-string/jumbo v3, "0,1,2"

    invoke-interface {v0, v1, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 452
    sget-object v1, Lcom/peel/content/a;->f:Lcom/peel/content/user/User;

    invoke-virtual {v1}, Lcom/peel/content/user/User;->x()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v0, v7}, Lcom/peel/content/a/j;->a(Ljava/lang/String;Ljava/util/Map;Lcom/peel/util/t;)V

    .line 455
    :cond_2
    iget-object v0, p0, Lcom/peel/i/ck;->g:Lcom/peel/i/cg;

    iget-object v0, v0, Lcom/peel/i/cg;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "content_library"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 456
    iget-object v0, p0, Lcom/peel/i/ck;->g:Lcom/peel/i/cg;

    iget-object v0, v0, Lcom/peel/i/cg;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "content_library"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/peel/content/library/Library;

    .line 457
    invoke-virtual {v0}, Lcom/peel/content/library/Library;->e()Ljava/lang/String;

    move-result-object v1

    iget-object v3, p0, Lcom/peel/i/ck;->e:Landroid/os/Bundle;

    const-string/jumbo v4, "id"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 458
    invoke-virtual {v0}, Lcom/peel/content/library/Library;->e()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/i/ck;->b:Lcom/peel/data/ContentRoom;

    invoke-virtual {v1}, Lcom/peel/data/ContentRoom;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/peel/content/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 462
    :cond_3
    iget-object v0, p0, Lcom/peel/i/ck;->c:Lcom/peel/content/library/LiveLibrary;

    iget-object v1, p0, Lcom/peel/i/ck;->b:Lcom/peel/data/ContentRoom;

    invoke-virtual {v1}, Lcom/peel/data/ContentRoom;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/peel/content/a;->a(Lcom/peel/content/library/Library;Ljava/lang/String;)V

    .line 464
    sget-object v0, Lcom/peel/content/a;->f:Lcom/peel/content/user/User;

    invoke-virtual {v0}, Lcom/peel/content/user/User;->d()Landroid/os/Bundle;

    move-result-object v0

    .line 465
    iget-object v1, p0, Lcom/peel/i/ck;->g:Lcom/peel/i/cg;

    invoke-static {v1}, Lcom/peel/i/cg;->d(Lcom/peel/i/cg;)Ljava/lang/String;

    move-result-object v1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "\n\n********** languages.putStringArrayList : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/peel/i/ck;->b:Lcom/peel/data/ContentRoom;

    invoke-virtual {v4}, Lcom/peel/data/ContentRoom;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/peel/i/ck;->c:Lcom/peel/content/library/LiveLibrary;

    invoke-virtual {v4}, Lcom/peel/content/library/LiveLibrary;->e()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 467
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/peel/i/ck;->b:Lcom/peel/data/ContentRoom;

    invoke-virtual {v3}, Lcom/peel/data/ContentRoom;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v3, "/"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v3, p0, Lcom/peel/i/ck;->c:Lcom/peel/content/library/LiveLibrary;

    invoke-virtual {v3}, Lcom/peel/content/library/LiveLibrary;->e()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v3, p0, Lcom/peel/i/ck;->a:Landroid/os/Bundle;

    const-string/jumbo v4, "languages"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v0, v1, v3}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 469
    iget-object v0, p0, Lcom/peel/i/ck;->c:Lcom/peel/content/library/LiveLibrary;

    invoke-virtual {v0}, Lcom/peel/content/library/LiveLibrary;->i()[Lcom/peel/data/Channel;

    move-result-object v1

    .line 470
    if-eqz v1, :cond_7

    array-length v0, v1

    if-lez v0, :cond_7

    .line 471
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 472
    array-length v4, v1

    move v0, v2

    :goto_1
    if-ge v0, v4, :cond_5

    aget-object v5, v1, v0

    .line 473
    invoke-virtual {v5}, Lcom/peel/data/Channel;->l()Z

    move-result v6

    if-eqz v6, :cond_4

    invoke-virtual {v5}, Lcom/peel/data/Channel;->a()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v3, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 472
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 475
    :cond_5
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_7

    .line 476
    sget-object v0, Lcom/peel/content/a;->f:Lcom/peel/content/user/User;

    invoke-virtual {v0}, Lcom/peel/content/user/User;->f()Landroid/os/Bundle;

    move-result-object v4

    .line 477
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/peel/i/ck;->b:Lcom/peel/data/ContentRoom;

    invoke-virtual {v1}, Lcom/peel/data/ContentRoom;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/i/ck;->c:Lcom/peel/content/library/LiveLibrary;

    invoke-virtual {v1}, Lcom/peel/content/library/LiveLibrary;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    .line 478
    if-nez v1, :cond_6

    .line 479
    new-instance v1, Ljava/util/ArrayList;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 480
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lcom/peel/i/ck;->b:Lcom/peel/data/ContentRoom;

    invoke-virtual {v5}, Lcom/peel/data/ContentRoom;->a()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v5, "/"

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v5, p0, Lcom/peel/i/ck;->c:Lcom/peel/content/library/LiveLibrary;

    invoke-virtual {v5}, Lcom/peel/content/library/LiveLibrary;->e()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    move-object v0, v1

    check-cast v0, Ljava/util/ArrayList;

    invoke-virtual {v4, v5, v0}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 482
    :cond_6
    invoke-interface {v1, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 487
    :cond_7
    sget-object v0, Lcom/peel/content/a;->f:Lcom/peel/content/user/User;

    invoke-virtual {v0}, Lcom/peel/content/user/User;->u()V

    .line 489
    iget-object v0, p0, Lcom/peel/i/ck;->g:Lcom/peel/i/cg;

    invoke-virtual {v0}, Lcom/peel/i/cg;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string/jumbo v1, "country_ISO"

    const-string/jumbo v3, "US"

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 490
    const-string/jumbo v1, "US"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 492
    const-string/jumbo v0, "live"

    invoke-static {v0}, Lcom/peel/content/a;->c(Ljava/lang/String;)Lcom/peel/content/library/Library;

    move-result-object v0

    .line 493
    if-eqz v0, :cond_8

    .line 494
    invoke-virtual {v0}, Lcom/peel/content/library/Library;->e()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lcom/peel/i/cm;

    invoke-direct {v1, p0}, Lcom/peel/i/cm;-><init>(Lcom/peel/i/ck;)V

    invoke-static {v0, v1}, Lcom/peel/util/al;->a(Ljava/lang/String;Lcom/peel/util/t;)V

    .line 503
    :cond_8
    iget-object v0, p0, Lcom/peel/i/ck;->g:Lcom/peel/i/cg;

    invoke-virtual {v0}, Lcom/peel/i/cg;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/ae;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/peel/social/w;->f(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 504
    new-instance v0, Lcom/peel/backup/c;

    iget-object v1, p0, Lcom/peel/i/ck;->g:Lcom/peel/i/cg;

    invoke-virtual {v1}, Lcom/peel/i/cg;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/ae;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/peel/backup/c;-><init>(Landroid/content/Context;)V

    .line 506
    sget-object v1, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v1}, Lcom/peel/control/am;->c()[Lcom/peel/control/RoomControl;

    move-result-object v1

    array-length v1, v1

    if-ne v1, v8, :cond_b

    .line 507
    iget-object v1, p0, Lcom/peel/i/ck;->c:Lcom/peel/content/library/LiveLibrary;

    invoke-virtual {v1}, Lcom/peel/content/library/LiveLibrary;->e()Ljava/lang/String;

    move-result-object v1

    iget-object v3, p0, Lcom/peel/i/ck;->c:Lcom/peel/content/library/LiveLibrary;

    invoke-virtual {v3}, Lcom/peel/content/library/LiveLibrary;->d()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v8, v1, v3, v7}, Lcom/peel/backup/c;->a(ZLjava/lang/String;Ljava/lang/String;Lcom/peel/util/t;)V

    .line 515
    :cond_9
    :goto_2
    iget-boolean v0, p0, Lcom/peel/i/ck;->f:Z

    if-eqz v0, :cond_a

    .line 517
    iget-object v0, p0, Lcom/peel/i/ck;->g:Lcom/peel/i/cg;

    invoke-static {v0}, Lcom/peel/i/cg;->f(Lcom/peel/i/cg;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string/jumbo v1, "disambiguated_rooms"

    new-instance v3, Ljava/util/HashSet;

    invoke-direct {v3}, Ljava/util/HashSet;-><init>()V

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences;->getStringSet(Ljava/lang/String;Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    .line 518
    iget-object v1, p0, Lcom/peel/i/ck;->b:Lcom/peel/data/ContentRoom;

    invoke-virtual {v1}, Lcom/peel/data/ContentRoom;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 519
    iget-object v1, p0, Lcom/peel/i/ck;->g:Lcom/peel/i/cg;

    invoke-static {v1}, Lcom/peel/i/cg;->f(Lcom/peel/i/cg;)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string/jumbo v3, "disambiguated_rooms"

    invoke-interface {v1, v3, v0}, Landroid/content/SharedPreferences$Editor;->putStringSet(Ljava/lang/String;Ljava/util/Set;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 522
    :cond_a
    iget-object v0, p0, Lcom/peel/i/ck;->g:Lcom/peel/i/cg;

    invoke-virtual {v0}, Lcom/peel/i/cg;->S()V

    .line 534
    iget-object v0, p0, Lcom/peel/i/ck;->g:Lcom/peel/i/cg;

    iget-object v0, v0, Lcom/peel/i/cg;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "from_settings"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 535
    iget-object v0, p0, Lcom/peel/i/ck;->g:Lcom/peel/i/cg;

    iget-object v1, p0, Lcom/peel/i/ck;->b:Lcom/peel/data/ContentRoom;

    iget-object v2, p0, Lcom/peel/i/ck;->c:Lcom/peel/content/library/LiveLibrary;

    invoke-static {v0, v1, v2}, Lcom/peel/i/cg;->a(Lcom/peel/i/cg;Lcom/peel/data/ContentRoom;Lcom/peel/content/library/Library;)V

    goto/16 :goto_0

    .line 509
    :cond_b
    sget-object v1, Lcom/peel/content/a;->f:Lcom/peel/content/user/User;

    if-eqz v1, :cond_9

    .line 510
    sget-object v1, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v1}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v1

    iget-object v3, p0, Lcom/peel/i/ck;->c:Lcom/peel/content/library/LiveLibrary;

    invoke-virtual {v3}, Lcom/peel/content/library/LiveLibrary;->d()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v3, v7}, Lcom/peel/backup/c;->a(Lcom/peel/control/RoomControl;Ljava/lang/String;Lcom/peel/util/t;)V

    goto :goto_2

    .line 537
    :cond_c
    iget-object v0, p0, Lcom/peel/i/ck;->g:Lcom/peel/i/cg;

    invoke-static {v0}, Lcom/peel/i/cg;->g(Lcom/peel/i/cg;)V

    goto/16 :goto_0
.end method
