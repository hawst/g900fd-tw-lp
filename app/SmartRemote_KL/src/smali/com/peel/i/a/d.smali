.class Lcom/peel/i/a/d;
.super Landroid/widget/Filter;


# instance fields
.field final synthetic a:Lcom/peel/i/a/c;


# direct methods
.method constructor <init>(Lcom/peel/i/a/c;)V
    .locals 0

    .prologue
    .line 82
    iput-object p1, p0, Lcom/peel/i/a/d;->a:Lcom/peel/i/a/c;

    invoke-direct {p0}, Landroid/widget/Filter;-><init>()V

    return-void
.end method


# virtual methods
.method protected performFiltering(Ljava/lang/CharSequence;)Landroid/widget/Filter$FilterResults;
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 84
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 86
    sget-object v2, Ljava/text/Normalizer$Form;->NFD:Ljava/text/Normalizer$Form;

    invoke-static {p1, v2}, Ljava/text/Normalizer;->normalize(Ljava/lang/CharSequence;Ljava/text/Normalizer$Form;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    .line 87
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    if-lez v3, :cond_1

    .line 88
    :goto_0
    iget-object v3, p0, Lcom/peel/i/a/d;->a:Lcom/peel/i/a/c;

    invoke-static {v3}, Lcom/peel/i/a/c;->a(Lcom/peel/i/a/c;)I

    move-result v3

    if-ge v0, v3, :cond_2

    .line 89
    iget-object v3, p0, Lcom/peel/i/a/d;->a:Lcom/peel/i/a/c;

    invoke-static {v3, v0}, Lcom/peel/i/a/c;->a(Lcom/peel/i/a/c;I)Landroid/os/Bundle;

    move-result-object v3

    const-string/jumbo v4, "name"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    sget-object v4, Ljava/text/Normalizer$Form;->NFD:Ljava/text/Normalizer$Form;

    invoke-static {v3, v4}, Ljava/text/Normalizer;->normalize(Ljava/lang/CharSequence;Ljava/text/Normalizer$Form;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 90
    iget-object v3, p0, Lcom/peel/i/a/d;->a:Lcom/peel/i/a/c;

    invoke-static {v3, v0}, Lcom/peel/i/a/c;->a(Lcom/peel/i/a/c;I)Landroid/os/Bundle;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 88
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 94
    :cond_1
    :goto_1
    iget-object v2, p0, Lcom/peel/i/a/d;->a:Lcom/peel/i/a/c;

    invoke-static {v2}, Lcom/peel/i/a/c;->a(Lcom/peel/i/a/c;)I

    move-result v2

    if-ge v0, v2, :cond_2

    .line 95
    iget-object v2, p0, Lcom/peel/i/a/d;->a:Lcom/peel/i/a/c;

    invoke-static {v2, v0}, Lcom/peel/i/a/c;->a(Lcom/peel/i/a/c;I)Landroid/os/Bundle;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 94
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 99
    :cond_2
    new-instance v0, Landroid/widget/Filter$FilterResults;

    invoke-direct {v0}, Landroid/widget/Filter$FilterResults;-><init>()V

    .line 100
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    iput v2, v0, Landroid/widget/Filter$FilterResults;->count:I

    .line 101
    iput-object v1, v0, Landroid/widget/Filter$FilterResults;->values:Ljava/lang/Object;

    .line 103
    return-object v0
.end method

.method protected publishResults(Ljava/lang/CharSequence;Landroid/widget/Filter$FilterResults;)V
    .locals 2

    .prologue
    .line 107
    iget-object v1, p0, Lcom/peel/i/a/d;->a:Lcom/peel/i/a/c;

    iget-object v0, p2, Landroid/widget/Filter$FilterResults;->values:Ljava/lang/Object;

    check-cast v0, Ljava/util/List;

    invoke-static {v1, v0}, Lcom/peel/i/a/c;->a(Lcom/peel/i/a/c;Ljava/util/List;)Ljava/util/List;

    .line 108
    iget-object v0, p0, Lcom/peel/i/a/d;->a:Lcom/peel/i/a/c;

    invoke-virtual {v0}, Lcom/peel/i/a/c;->notifyDataSetChanged()V

    .line 109
    return-void
.end method
