.class Lcom/peel/i/a/b;
.super Landroid/widget/Filter;


# instance fields
.field final synthetic a:Lcom/peel/i/a/a;


# direct methods
.method constructor <init>(Lcom/peel/i/a/a;)V
    .locals 0

    .prologue
    .line 65
    iput-object p1, p0, Lcom/peel/i/a/b;->a:Lcom/peel/i/a/a;

    invoke-direct {p0}, Landroid/widget/Filter;-><init>()V

    return-void
.end method


# virtual methods
.method protected performFiltering(Ljava/lang/CharSequence;)Landroid/widget/Filter$FilterResults;
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 67
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 69
    sget-object v2, Ljava/text/Normalizer$Form;->NFD:Ljava/text/Normalizer$Form;

    invoke-static {p1, v2}, Ljava/text/Normalizer;->normalize(Ljava/lang/CharSequence;Ljava/text/Normalizer$Form;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    .line 70
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    if-lez v3, :cond_1

    .line 71
    :goto_0
    iget-object v3, p0, Lcom/peel/i/a/b;->a:Lcom/peel/i/a/a;

    invoke-static {v3}, Lcom/peel/i/a/a;->a(Lcom/peel/i/a/a;)I

    move-result v3

    if-ge v0, v3, :cond_2

    .line 72
    iget-object v3, p0, Lcom/peel/i/a/b;->a:Lcom/peel/i/a/a;

    invoke-static {v3, v0}, Lcom/peel/i/a/a;->a(Lcom/peel/i/a/a;I)Lcom/peel/f/a;

    move-result-object v3

    invoke-virtual {v3}, Lcom/peel/f/a;->b()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Ljava/text/Normalizer$Form;->NFD:Ljava/text/Normalizer$Form;

    invoke-static {v3, v4}, Ljava/text/Normalizer;->normalize(Ljava/lang/CharSequence;Ljava/text/Normalizer$Form;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 73
    iget-object v3, p0, Lcom/peel/i/a/b;->a:Lcom/peel/i/a/a;

    invoke-static {v3, v0}, Lcom/peel/i/a/a;->a(Lcom/peel/i/a/a;I)Lcom/peel/f/a;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 71
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 77
    :cond_1
    :goto_1
    iget-object v2, p0, Lcom/peel/i/a/b;->a:Lcom/peel/i/a/a;

    invoke-static {v2}, Lcom/peel/i/a/a;->a(Lcom/peel/i/a/a;)I

    move-result v2

    if-ge v0, v2, :cond_2

    .line 78
    iget-object v2, p0, Lcom/peel/i/a/b;->a:Lcom/peel/i/a/a;

    invoke-static {v2, v0}, Lcom/peel/i/a/a;->a(Lcom/peel/i/a/a;I)Lcom/peel/f/a;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 77
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 82
    :cond_2
    new-instance v0, Landroid/widget/Filter$FilterResults;

    invoke-direct {v0}, Landroid/widget/Filter$FilterResults;-><init>()V

    .line 83
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    iput v2, v0, Landroid/widget/Filter$FilterResults;->count:I

    .line 84
    iput-object v1, v0, Landroid/widget/Filter$FilterResults;->values:Ljava/lang/Object;

    .line 86
    return-object v0
.end method

.method protected publishResults(Ljava/lang/CharSequence;Landroid/widget/Filter$FilterResults;)V
    .locals 2

    .prologue
    .line 90
    iget-object v1, p0, Lcom/peel/i/a/b;->a:Lcom/peel/i/a/a;

    iget-object v0, p2, Landroid/widget/Filter$FilterResults;->values:Ljava/lang/Object;

    check-cast v0, Ljava/util/List;

    invoke-static {v1, v0}, Lcom/peel/i/a/a;->a(Lcom/peel/i/a/a;Ljava/util/List;)Ljava/util/List;

    .line 91
    iget-object v0, p0, Lcom/peel/i/a/b;->a:Lcom/peel/i/a/a;

    invoke-virtual {v0}, Lcom/peel/i/a/a;->notifyDataSetChanged()V

    .line 92
    return-void
.end method
