.class public Lcom/peel/i/a/c;
.super Landroid/widget/ArrayAdapter;

# interfaces
.implements Landroid/widget/SectionIndexer;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Landroid/os/Bundle;",
        ">;",
        "Landroid/widget/SectionIndexer;"
    }
.end annotation


# instance fields
.field final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field final b:Landroid/util/SparseIntArray;

.field final c:Landroid/util/SparseIntArray;

.field private d:Landroid/view/LayoutInflater;

.field private e:Landroid/content/Context;

.field private f:I

.field private g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/os/Bundle;",
            ">;"
        }
    .end annotation
.end field

.field private h:Landroid/widget/Filter;


# direct methods
.method public constructor <init>(Landroid/content/Context;ILjava/util/List;I)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Ljava/util/List",
            "<",
            "Landroid/os/Bundle;",
            ">;I)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 28
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 21
    iput v2, p0, Lcom/peel/i/a/c;->f:I

    .line 22
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/peel/i/a/c;->g:Ljava/util/List;

    .line 23
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/peel/i/a/c;->a:Ljava/util/List;

    .line 24
    new-instance v0, Landroid/util/SparseIntArray;

    invoke-direct {v0}, Landroid/util/SparseIntArray;-><init>()V

    iput-object v0, p0, Lcom/peel/i/a/c;->b:Landroid/util/SparseIntArray;

    .line 25
    new-instance v0, Landroid/util/SparseIntArray;

    invoke-direct {v0}, Landroid/util/SparseIntArray;-><init>()V

    iput-object v0, p0, Lcom/peel/i/a/c;->c:Landroid/util/SparseIntArray;

    .line 29
    iput-object p1, p0, Lcom/peel/i/a/c;->e:Landroid/content/Context;

    .line 30
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/i/a/c;->d:Landroid/view/LayoutInflater;

    .line 31
    iget-object v0, p0, Lcom/peel/i/a/c;->g:Ljava/util/List;

    invoke-interface {v0, p3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 33
    iput p4, p0, Lcom/peel/i/a/c;->f:I

    .line 37
    invoke-interface {p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v2

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    .line 38
    const-string/jumbo v4, "name"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const/4 v4, 0x1

    invoke-virtual {v0, v2, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    .line 40
    iget-object v0, p0, Lcom/peel/i/a/c;->c:Landroid/util/SparseIntArray;

    iget-object v5, p0, Lcom/peel/i/a/c;->a:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    invoke-virtual {v0, v1, v5}, Landroid/util/SparseIntArray;->put(II)V

    .line 42
    iget-object v0, p0, Lcom/peel/i/a/c;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-ne v1, v0, :cond_3

    .line 43
    :cond_0
    iget-object v0, p0, Lcom/peel/i/a/c;->b:Landroid/util/SparseIntArray;

    iget-object v4, p0, Lcom/peel/i/a/c;->a:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    invoke-virtual {v0, v4, v1}, Landroid/util/SparseIntArray;->put(II)V

    .line 44
    iget-object v4, p0, Lcom/peel/i/a/c;->a:Ljava/util/List;

    iget-object v0, p0, Lcom/peel/i/a/c;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    const-string/jumbo v0, "\u2764"

    :goto_1
    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 50
    :cond_1
    :goto_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    .line 51
    goto :goto_0

    .line 44
    :cond_2
    const-string/jumbo v0, "\u2708"

    goto :goto_1

    .line 45
    :cond_3
    iget-object v0, p0, Lcom/peel/i/a/c;->a:Ljava/util/List;

    iget-object v5, p0, Lcom/peel/i/a/c;->a:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v0, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 46
    iget-object v0, p0, Lcom/peel/i/a/c;->b:Landroid/util/SparseIntArray;

    iget-object v5, p0, Lcom/peel/i/a/c;->a:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    invoke-virtual {v0, v5, v1}, Landroid/util/SparseIntArray;->put(II)V

    .line 47
    iget-object v0, p0, Lcom/peel/i/a/c;->a:Ljava/util/List;

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 52
    :cond_4
    return-void
.end method

.method private a()I
    .locals 1

    .prologue
    .line 54
    invoke-super {p0}, Landroid/widget/ArrayAdapter;->getCount()I

    move-result v0

    return v0
.end method

.method static synthetic a(Lcom/peel/i/a/c;)I
    .locals 1

    .prologue
    .line 18
    invoke-direct {p0}, Lcom/peel/i/a/c;->a()I

    move-result v0

    return v0
.end method

.method static synthetic a(Lcom/peel/i/a/c;I)Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 18
    invoke-direct {p0, p1}, Lcom/peel/i/a/c;->b(I)Landroid/os/Bundle;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/peel/i/a/c;Ljava/util/List;)Ljava/util/List;
    .locals 0

    .prologue
    .line 18
    iput-object p1, p0, Lcom/peel/i/a/c;->g:Ljava/util/List;

    return-object p1
.end method

.method private b(I)Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 55
    invoke-super {p0, p1}, Landroid/widget/ArrayAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    return-object v0
.end method


# virtual methods
.method public a(I)Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/peel/i/a/c;->g:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    return-object v0
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/peel/i/a/c;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getFilter()Landroid/widget/Filter;
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lcom/peel/i/a/c;->h:Landroid/widget/Filter;

    if-nez v0, :cond_0

    .line 82
    new-instance v0, Lcom/peel/i/a/d;

    invoke-direct {v0, p0}, Lcom/peel/i/a/d;-><init>(Lcom/peel/i/a/c;)V

    iput-object v0, p0, Lcom/peel/i/a/c;->h:Landroid/widget/Filter;

    .line 113
    :cond_0
    iget-object v0, p0, Lcom/peel/i/a/c;->h:Landroid/widget/Filter;

    return-object v0
.end method

.method public synthetic getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 18
    invoke-virtual {p0, p1}, Lcom/peel/i/a/c;->a(I)Landroid/os/Bundle;

    move-result-object v0

    return-object v0
.end method

.method public getPositionForSection(I)I
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lcom/peel/i/a/c;->b:Landroid/util/SparseIntArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseIntArray;->get(I)I

    move-result v0

    return v0
.end method

.method public getSectionForPosition(I)I
    .locals 1

    .prologue
    .line 116
    iget-object v0, p0, Lcom/peel/i/a/c;->c:Landroid/util/SparseIntArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseIntArray;->get(I)I

    move-result v0

    return v0
.end method

.method public getSections()[Ljava/lang/Object;
    .locals 2

    .prologue
    .line 117
    iget-object v0, p0, Lcom/peel/i/a/c;->a:Ljava/util/List;

    iget-object v1, p0, Lcom/peel/i/a/c;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    new-array v1, v1, [Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 60
    .line 62
    if-nez v0, :cond_0

    .line 63
    if-eqz v0, :cond_1

    check-cast v0, Landroid/widget/TextView;

    .line 64
    :goto_0
    invoke-virtual {p0, p1}, Lcom/peel/i/a/c;->a(I)Landroid/os/Bundle;

    move-result-object v1

    .line 66
    const-string/jumbo v2, "display_name"

    const-string/jumbo v3, "name"

    invoke-virtual {v1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 69
    iget v1, p0, Lcom/peel/i/a/c;->f:I

    if-ne p1, v1, :cond_0

    .line 70
    iget-object v1, p0, Lcom/peel/i/a/c;->e:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/peel/ui/fm;->countrytextcolor_selector:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 73
    new-instance v1, Ljava/lang/Object;

    invoke-direct {v1}, Ljava/lang/Object;-><init>()V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTag(Ljava/lang/Object;)V

    .line 77
    :cond_0
    return-object v0

    .line 63
    :cond_1
    iget-object v1, p0, Lcom/peel/i/a/c;->d:Landroid/view/LayoutInflater;

    sget v2, Lcom/peel/ui/fq;->country_row:I

    invoke-virtual {v1, v2, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    goto :goto_0
.end method
