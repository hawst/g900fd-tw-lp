.class public Lcom/peel/i/a/f;
.super Ljava/lang/Object;


# instance fields
.field private a:Landroid/widget/TextView;

.field private b:Landroid/widget/TextView;

.field private c:Landroid/widget/ImageView;

.field private d:Landroid/view/View;


# direct methods
.method constructor <init>(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-object p1, p0, Lcom/peel/i/a/f;->d:Landroid/view/View;

    .line 22
    return-void
.end method


# virtual methods
.method public a()Landroid/widget/TextView;
    .locals 2

    .prologue
    .line 25
    iget-object v0, p0, Lcom/peel/i/a/f;->a:Landroid/widget/TextView;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/peel/i/a/f;->d:Landroid/view/View;

    sget v1, Lcom/peel/ui/fp;->ip:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/peel/i/a/f;->a:Landroid/widget/TextView;

    .line 26
    :cond_0
    iget-object v0, p0, Lcom/peel/i/a/f;->a:Landroid/widget/TextView;

    return-object v0
.end method

.method public b()Landroid/widget/TextView;
    .locals 2

    .prologue
    .line 30
    iget-object v0, p0, Lcom/peel/i/a/f;->b:Landroid/widget/TextView;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/peel/i/a/f;->d:Landroid/view/View;

    sget v1, Lcom/peel/ui/fp;->location:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/peel/i/a/f;->b:Landroid/widget/TextView;

    .line 31
    :cond_0
    iget-object v0, p0, Lcom/peel/i/a/f;->b:Landroid/widget/TextView;

    return-object v0
.end method

.method public c()Landroid/widget/ImageView;
    .locals 2

    .prologue
    .line 35
    iget-object v0, p0, Lcom/peel/i/a/f;->c:Landroid/widget/ImageView;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/peel/i/a/f;->d:Landroid/view/View;

    sget v1, Lcom/peel/ui/fp;->checkmark:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/peel/i/a/f;->c:Landroid/widget/ImageView;

    .line 36
    :cond_0
    iget-object v0, p0, Lcom/peel/i/a/f;->c:Landroid/widget/ImageView;

    return-object v0
.end method
