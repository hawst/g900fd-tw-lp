.class public Lcom/peel/i/a/e;
.super Landroid/widget/ArrayAdapter;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lcom/peel/control/h;",
        ">;"
    }
.end annotation


# instance fields
.field private a:[Lcom/peel/control/h;

.field private b:Landroid/widget/TextView;

.field private c:Landroid/widget/TextView;

.field private d:Landroid/widget/ImageView;

.field private e:Landroid/view/View;

.field private f:Landroid/view/LayoutInflater;

.field private g:I


# direct methods
.method public constructor <init>(Landroid/content/Context;I[Lcom/peel/control/h;)V
    .locals 1

    .prologue
    .line 24
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    .line 21
    const/4 v0, -0x1

    iput v0, p0, Lcom/peel/i/a/e;->g:I

    .line 25
    iput-object p3, p0, Lcom/peel/i/a/e;->a:[Lcom/peel/control/h;

    .line 26
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/i/a/e;->f:Landroid/view/LayoutInflater;

    .line 27
    return-void
.end method


# virtual methods
.method public a(I)V
    .locals 0

    .prologue
    .line 30
    iput p1, p0, Lcom/peel/i/a/e;->g:I

    .line 31
    invoke-virtual {p0}, Lcom/peel/i/a/e;->notifyDataSetChanged()V

    .line 32
    return-void
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 5

    .prologue
    const/16 v4, 0x8

    .line 36
    iget-object v0, p0, Lcom/peel/i/a/e;->f:Landroid/view/LayoutInflater;

    sget v1, Lcom/peel/ui/fq;->dtv_device_row:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/i/a/e;->e:Landroid/view/View;

    .line 37
    new-instance v0, Lcom/peel/i/a/f;

    iget-object v1, p0, Lcom/peel/i/a/e;->e:Landroid/view/View;

    invoke-direct {v0, v1}, Lcom/peel/i/a/f;-><init>(Landroid/view/View;)V

    .line 38
    invoke-virtual {v0}, Lcom/peel/i/a/f;->a()Landroid/widget/TextView;

    move-result-object v1

    iput-object v1, p0, Lcom/peel/i/a/e;->b:Landroid/widget/TextView;

    .line 39
    iget-object v1, p0, Lcom/peel/i/a/e;->b:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/peel/i/a/e;->a:[Lcom/peel/control/h;

    aget-object v2, v2, p1

    invoke-virtual {v2}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v2

    invoke-virtual {v2}, Lcom/peel/data/g;->i()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 40
    invoke-virtual {v0}, Lcom/peel/i/a/f;->b()Landroid/widget/TextView;

    move-result-object v1

    iput-object v1, p0, Lcom/peel/i/a/e;->c:Landroid/widget/TextView;

    .line 41
    iget-object v1, p0, Lcom/peel/i/a/e;->a:[Lcom/peel/control/h;

    aget-object v1, v1, p1

    invoke-virtual {v1}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/data/g;->k()Landroid/os/Bundle;

    move-result-object v1

    const-string/jumbo v2, "locationName"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/peel/i/a/e;->a:[Lcom/peel/control/h;

    aget-object v1, v1, p1

    invoke-virtual {v1}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/data/g;->k()Landroid/os/Bundle;

    move-result-object v1

    const-string/jumbo v2, "locationName"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    iget-object v1, p0, Lcom/peel/i/a/e;->c:Landroid/widget/TextView;

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 43
    :goto_0
    invoke-virtual {v0}, Lcom/peel/i/a/f;->c()Landroid/widget/ImageView;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/i/a/e;->d:Landroid/widget/ImageView;

    .line 44
    iget v0, p0, Lcom/peel/i/a/e;->g:I

    if-ne v0, p1, :cond_2

    iget-object v0, p0, Lcom/peel/i/a/e;->d:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 46
    :goto_1
    iget-object v0, p0, Lcom/peel/i/a/e;->e:Landroid/view/View;

    return-object v0

    .line 42
    :cond_1
    iget-object v1, p0, Lcom/peel/i/a/e;->c:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/peel/i/a/e;->a:[Lcom/peel/control/h;

    aget-object v2, v2, p1

    invoke-virtual {v2}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v2

    invoke-virtual {v2}, Lcom/peel/data/g;->k()Landroid/os/Bundle;

    move-result-object v2

    const-string/jumbo v3, "locationName"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 45
    :cond_2
    iget-object v0, p0, Lcom/peel/i/a/e;->d:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_1
.end method
