.class public Lcom/peel/i/a/a;
.super Landroid/widget/ArrayAdapter;

# interfaces
.implements Landroid/widget/SectionIndexer;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lcom/peel/f/a;",
        ">;",
        "Landroid/widget/SectionIndexer;"
    }
.end annotation


# instance fields
.field private a:Landroid/view/LayoutInflater;

.field private b:I

.field private c:I

.field private d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/peel/f/a;",
            ">;"
        }
    .end annotation
.end field

.field private e:Landroid/widget/Filter;


# direct methods
.method public constructor <init>(Landroid/content/Context;ILjava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Ljava/util/List",
            "<",
            "Lcom/peel/f/a;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 35
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 31
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/peel/i/a/a;->d:Ljava/util/List;

    .line 36
    iput p2, p0, Lcom/peel/i/a/a;->b:I

    .line 37
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/i/a/a;->a:Landroid/view/LayoutInflater;

    .line 38
    iget-object v0, p0, Lcom/peel/i/a/a;->d:Ljava/util/List;

    invoke-interface {v0, p3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 39
    const/4 v0, -0x1

    iput v0, p0, Lcom/peel/i/a/a;->c:I

    .line 40
    return-void
.end method

.method private a()I
    .locals 1

    .prologue
    .line 42
    invoke-super {p0}, Landroid/widget/ArrayAdapter;->getCount()I

    move-result v0

    return v0
.end method

.method static synthetic a(Lcom/peel/i/a/a;)I
    .locals 1

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/peel/i/a/a;->a()I

    move-result v0

    return v0
.end method

.method static synthetic a(Lcom/peel/i/a/a;I)Lcom/peel/f/a;
    .locals 1

    .prologue
    .line 26
    invoke-direct {p0, p1}, Lcom/peel/i/a/a;->c(I)Lcom/peel/f/a;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/peel/i/a/a;Ljava/util/List;)Ljava/util/List;
    .locals 0

    .prologue
    .line 26
    iput-object p1, p0, Lcom/peel/i/a/a;->d:Ljava/util/List;

    return-object p1
.end method

.method private c(I)Lcom/peel/f/a;
    .locals 1

    .prologue
    .line 43
    invoke-super {p0, p1}, Landroid/widget/ArrayAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/f/a;

    return-object v0
.end method


# virtual methods
.method public a(I)Lcom/peel/f/a;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/peel/i/a/a;->d:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/f/a;

    return-object v0
.end method

.method public b(I)V
    .locals 0

    .prologue
    .line 108
    iput p1, p0, Lcom/peel/i/a/a;->c:I

    .line 109
    invoke-virtual {p0}, Lcom/peel/i/a/a;->notifyDataSetChanged()V

    .line 111
    return-void
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/peel/i/a/a;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getFilter()Landroid/widget/Filter;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/peel/i/a/a;->e:Landroid/widget/Filter;

    if-nez v0, :cond_0

    .line 65
    new-instance v0, Lcom/peel/i/a/b;

    invoke-direct {v0, p0}, Lcom/peel/i/a/b;-><init>(Lcom/peel/i/a/a;)V

    iput-object v0, p0, Lcom/peel/i/a/a;->e:Landroid/widget/Filter;

    .line 96
    :cond_0
    iget-object v0, p0, Lcom/peel/i/a/a;->e:Landroid/widget/Filter;

    return-object v0
.end method

.method public synthetic getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 26
    invoke-virtual {p0, p1}, Lcom/peel/i/a/a;->a(I)Lcom/peel/f/a;

    move-result-object v0

    return-object v0
.end method

.method public getPositionForSection(I)I
    .locals 1

    .prologue
    .line 100
    mul-int/lit8 v0, p1, 0xa

    return v0
.end method

.method public getSectionForPosition(I)I
    .locals 1

    .prologue
    .line 104
    div-int/lit8 v0, p1, 0xa

    return v0
.end method

.method public getSections()[Ljava/lang/Object;
    .locals 6

    .prologue
    const/4 v3, 0x3

    const/4 v2, 0x0

    .line 114
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    move v1, v2

    .line 115
    :goto_0
    iget-object v0, p0, Lcom/peel/i/a/a;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 116
    iget-object v0, p0, Lcom/peel/i/a/a;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 117
    iget-object v0, p0, Lcom/peel/i/a/a;->d:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/f/a;

    invoke-virtual {v0}, Lcom/peel/f/a;->b()Ljava/lang/String;

    move-result-object v5

    .line 118
    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v0

    if-le v0, v3, :cond_1

    move v0, v3

    :goto_1
    invoke-virtual {v5, v2, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 115
    :cond_0
    add-int/lit8 v0, v1, 0xa

    move v1, v0

    goto :goto_0

    .line 118
    :cond_1
    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v0

    goto :goto_1

    .line 121
    :cond_2
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_3

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/String;

    invoke-interface {v4, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    :goto_2
    return-object v0

    :cond_3
    const/4 v0, 0x0

    goto :goto_2
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 53
    if-nez p2, :cond_0

    iget-object v0, p0, Lcom/peel/i/a/a;->a:Landroid/view/LayoutInflater;

    iget v1, p0, Lcom/peel/i/a/a;->b:I

    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 54
    :cond_0
    sget v0, Lcom/peel/ui/fp;->name:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 55
    iget-object v1, p0, Lcom/peel/i/a/a;->d:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/peel/f/a;

    invoke-virtual {v1}, Lcom/peel/f/a;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 57
    sget v0, Lcom/peel/ui/fp;->checked_icon:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 58
    iget v1, p0, Lcom/peel/i/a/a;->c:I

    if-ne p1, v1, :cond_1

    move v1, v2

    :goto_0
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 60
    return-object p2

    .line 58
    :cond_1
    const/16 v1, 0x8

    goto :goto_0
.end method
