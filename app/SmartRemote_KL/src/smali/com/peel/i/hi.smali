.class Lcom/peel/i/hi;
.super Lcom/peel/util/t;


# instance fields
.field final synthetic a:Lcom/peel/i/gm;


# direct methods
.method constructor <init>(Lcom/peel/i/gm;I)V
    .locals 0

    .prologue
    .line 1368
    iput-object p1, p0, Lcom/peel/i/hi;->a:Lcom/peel/i/gm;

    invoke-direct {p0, p2}, Lcom/peel/util/t;-><init>(I)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    const/4 v5, 0x4

    const/4 v4, 0x0

    .line 1371
    invoke-static {}, Lcom/peel/i/gm;->c()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/i/hi;->a:Lcom/peel/i/gm;

    invoke-virtual {v1}, Lcom/peel/i/gm;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    invoke-static {v0, v1, v4}, Lcom/peel/util/bx;->a(Ljava/lang/String;Landroid/app/Activity;Z)V

    .line 1373
    iget-boolean v0, p0, Lcom/peel/i/hi;->i:Z

    if-nez v0, :cond_0

    .line 1374
    invoke-static {}, Lcom/peel/i/gm;->c()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "unable to get brands by provider"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1412
    iget-object v0, p0, Lcom/peel/i/hi;->a:Lcom/peel/i/gm;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-static {v0, v1}, Lcom/peel/i/gm;->d(Lcom/peel/i/gm;Ljava/util/List;)Ljava/util/List;

    .line 1413
    iget-object v0, p0, Lcom/peel/i/hi;->a:Lcom/peel/i/gm;

    invoke-static {v0}, Lcom/peel/i/gm;->ah(Lcom/peel/i/gm;)Ljava/util/List;

    move-result-object v0

    new-instance v1, Lcom/peel/f/a;

    iget-object v2, p0, Lcom/peel/i/hi;->a:Lcom/peel/i/gm;

    sget v3, Lcom/peel/ui/ft;->yes_i_do:I

    invoke-virtual {v2, v3}, Lcom/peel/i/gm;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v4, v2, v4}, Lcom/peel/f/a;-><init>(ILjava/lang/String;I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1414
    iget-object v0, p0, Lcom/peel/i/hi;->a:Lcom/peel/i/gm;

    invoke-static {v0}, Lcom/peel/i/gm;->ah(Lcom/peel/i/gm;)Ljava/util/List;

    move-result-object v0

    new-instance v1, Lcom/peel/f/a;

    iget-object v2, p0, Lcom/peel/i/hi;->a:Lcom/peel/i/gm;

    sget v3, Lcom/peel/ui/ft;->testing_stb_list_no_stb:I

    invoke-virtual {v2, v3}, Lcom/peel/i/gm;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v4, v2, v4}, Lcom/peel/f/a;-><init>(ILjava/lang/String;I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1415
    iget-object v0, p0, Lcom/peel/i/hi;->a:Lcom/peel/i/gm;

    new-instance v1, Lcom/peel/i/a/a;

    iget-object v2, p0, Lcom/peel/i/hi;->a:Lcom/peel/i/gm;

    invoke-virtual {v2}, Lcom/peel/i/gm;->m()Landroid/support/v4/app/ae;

    move-result-object v2

    sget v3, Lcom/peel/ui/fq;->brand_row:I

    iget-object v4, p0, Lcom/peel/i/hi;->a:Lcom/peel/i/gm;

    invoke-static {v4}, Lcom/peel/i/gm;->ah(Lcom/peel/i/gm;)Ljava/util/List;

    move-result-object v4

    invoke-direct {v1, v2, v3, v4}, Lcom/peel/i/a/a;-><init>(Landroid/content/Context;ILjava/util/List;)V

    invoke-static {v0, v1}, Lcom/peel/i/gm;->c(Lcom/peel/i/gm;Lcom/peel/i/a/a;)Lcom/peel/i/a/a;

    .line 1416
    iget-object v0, p0, Lcom/peel/i/hi;->a:Lcom/peel/i/gm;

    invoke-static {v0}, Lcom/peel/i/gm;->aj(Lcom/peel/i/gm;)Landroid/widget/ListView;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/i/hi;->a:Lcom/peel/i/gm;

    invoke-static {v1}, Lcom/peel/i/gm;->ai(Lcom/peel/i/gm;)Lcom/peel/i/a/a;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 1418
    iget-object v0, p0, Lcom/peel/i/hi;->a:Lcom/peel/i/gm;

    invoke-static {v0}, Lcom/peel/i/gm;->aj(Lcom/peel/i/gm;)Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ListView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 1419
    iget-object v1, p0, Lcom/peel/i/hi;->a:Lcom/peel/i/gm;

    invoke-virtual {v1}, Lcom/peel/i/gm;->n()Landroid/content/res/Resources;

    move-result-object v1

    const/high16 v2, 0x42e60000    # 115.0f

    invoke-static {v1, v2}, Lcom/peel/util/bx;->a(Landroid/content/res/Resources;F)F

    move-result v1

    float-to-int v1, v1

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 1420
    iget-object v1, p0, Lcom/peel/i/hi;->a:Lcom/peel/i/gm;

    invoke-static {v1}, Lcom/peel/i/gm;->aj(Lcom/peel/i/gm;)Landroid/widget/ListView;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/widget/ListView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1434
    :goto_0
    return-void

    .line 1423
    :cond_0
    iget-object v1, p0, Lcom/peel/i/hi;->a:Lcom/peel/i/gm;

    iget-object v0, p0, Lcom/peel/i/hi;->j:Ljava/lang/Object;

    check-cast v0, Ljava/util/List;

    invoke-static {v1, v0}, Lcom/peel/i/gm;->d(Lcom/peel/i/gm;Ljava/util/List;)Ljava/util/List;

    .line 1424
    iget-object v0, p0, Lcom/peel/i/hi;->a:Lcom/peel/i/gm;

    invoke-static {v0}, Lcom/peel/i/gm;->ah(Lcom/peel/i/gm;)Ljava/util/List;

    move-result-object v0

    new-instance v1, Lcom/peel/f/a;

    iget-object v2, p0, Lcom/peel/i/hi;->a:Lcom/peel/i/gm;

    sget v3, Lcom/peel/ui/ft;->testing_stb_list_other_brand:I

    invoke-virtual {v2, v3}, Lcom/peel/i/gm;->a(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x3

    invoke-direct {v1, v4, v2, v3}, Lcom/peel/f/a;-><init>(ILjava/lang/String;I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1425
    iget-object v0, p0, Lcom/peel/i/hi;->a:Lcom/peel/i/gm;

    invoke-static {v0}, Lcom/peel/i/gm;->ah(Lcom/peel/i/gm;)Ljava/util/List;

    move-result-object v0

    new-instance v1, Lcom/peel/f/a;

    iget-object v2, p0, Lcom/peel/i/hi;->a:Lcom/peel/i/gm;

    sget v3, Lcom/peel/ui/ft;->testing_stb_list_no_stb:I

    invoke-virtual {v2, v3}, Lcom/peel/i/gm;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v4, v2, v5}, Lcom/peel/f/a;-><init>(ILjava/lang/String;I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1427
    iget-object v0, p0, Lcom/peel/i/hi;->a:Lcom/peel/i/gm;

    new-instance v1, Lcom/peel/i/a/a;

    iget-object v2, p0, Lcom/peel/i/hi;->a:Lcom/peel/i/gm;

    invoke-virtual {v2}, Lcom/peel/i/gm;->m()Landroid/support/v4/app/ae;

    move-result-object v2

    sget v3, Lcom/peel/ui/fq;->brand_row:I

    iget-object v4, p0, Lcom/peel/i/hi;->a:Lcom/peel/i/gm;

    invoke-static {v4}, Lcom/peel/i/gm;->ah(Lcom/peel/i/gm;)Ljava/util/List;

    move-result-object v4

    invoke-direct {v1, v2, v3, v4}, Lcom/peel/i/a/a;-><init>(Landroid/content/Context;ILjava/util/List;)V

    invoke-static {v0, v1}, Lcom/peel/i/gm;->c(Lcom/peel/i/gm;Lcom/peel/i/a/a;)Lcom/peel/i/a/a;

    .line 1428
    iget-object v0, p0, Lcom/peel/i/hi;->a:Lcom/peel/i/gm;

    invoke-static {v0}, Lcom/peel/i/gm;->aj(Lcom/peel/i/gm;)Landroid/widget/ListView;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/i/hi;->a:Lcom/peel/i/gm;

    invoke-static {v1}, Lcom/peel/i/gm;->ai(Lcom/peel/i/gm;)Lcom/peel/i/a/a;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 1430
    iget-object v0, p0, Lcom/peel/i/hi;->a:Lcom/peel/i/gm;

    invoke-static {v0}, Lcom/peel/i/gm;->aj(Lcom/peel/i/gm;)Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ListView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    .line 1431
    iget-object v0, p0, Lcom/peel/i/hi;->a:Lcom/peel/i/gm;

    invoke-virtual {v0}, Lcom/peel/i/gm;->n()Landroid/content/res/Resources;

    move-result-object v2

    iget-object v0, p0, Lcom/peel/i/hi;->a:Lcom/peel/i/gm;

    invoke-static {v0}, Lcom/peel/i/gm;->ah(Lcom/peel/i/gm;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-le v0, v5, :cond_1

    const/high16 v0, 0x43650000    # 229.0f

    :goto_1
    invoke-static {v2, v0}, Lcom/peel/util/bx;->a(Landroid/content/res/Resources;F)F

    move-result v0

    float-to-int v0, v0

    iput v0, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 1432
    iget-object v0, p0, Lcom/peel/i/hi;->a:Lcom/peel/i/gm;

    invoke-static {v0}, Lcom/peel/i/gm;->aj(Lcom/peel/i/gm;)Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto/16 :goto_0

    .line 1431
    :cond_1
    iget-object v0, p0, Lcom/peel/i/hi;->a:Lcom/peel/i/gm;

    invoke-static {v0}, Lcom/peel/i/gm;->ah(Lcom/peel/i/gm;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    mul-int/lit8 v0, v0, 0x38

    iget-object v3, p0, Lcom/peel/i/hi;->a:Lcom/peel/i/gm;

    invoke-static {v3}, Lcom/peel/i/gm;->ah(Lcom/peel/i/gm;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    add-int/2addr v0, v3

    add-int/lit8 v0, v0, 0x1

    int-to-float v0, v0

    goto :goto_1
.end method
