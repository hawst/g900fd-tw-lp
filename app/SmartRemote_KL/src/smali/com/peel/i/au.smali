.class Lcom/peel/i/au;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/peel/i/al;


# direct methods
.method constructor <init>(Lcom/peel/i/al;)V
    .locals 0

    .prologue
    .line 189
    iput-object p1, p0, Lcom/peel/i/au;->a:Lcom/peel/i/al;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 192
    iget-object v0, p0, Lcom/peel/i/au;->a:Lcom/peel/i/al;

    invoke-static {v0}, Lcom/peel/i/al;->k(Lcom/peel/i/al;)Lcom/peel/widget/ag;

    move-result-object v0

    if-nez v0, :cond_0

    .line 193
    iget-object v0, p0, Lcom/peel/i/au;->a:Lcom/peel/i/al;

    invoke-static {v0}, Lcom/peel/i/al;->l(Lcom/peel/i/al;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 194
    iget-object v0, p0, Lcom/peel/i/au;->a:Lcom/peel/i/al;

    new-instance v1, Lcom/peel/widget/ag;

    iget-object v2, p0, Lcom/peel/i/au;->a:Lcom/peel/i/al;

    invoke-virtual {v2}, Lcom/peel/i/al;->m()Landroid/support/v4/app/ae;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/peel/widget/ag;-><init>(Landroid/content/Context;)V

    sget v2, Lcom/peel/ui/ft;->title_dtv_not_found:I

    .line 195
    invoke-virtual {v1, v2}, Lcom/peel/widget/ag;->a(I)Lcom/peel/widget/ag;

    move-result-object v1

    sget v2, Lcom/peel/ui/ft;->msg_dtv_not_found:I

    .line 196
    invoke-virtual {v1, v2}, Lcom/peel/widget/ag;->b(I)Lcom/peel/widget/ag;

    move-result-object v1

    sget v2, Lcom/peel/ui/ft;->label_setup_now:I

    new-instance v3, Lcom/peel/i/ax;

    invoke-direct {v3, p0}, Lcom/peel/i/ax;-><init>(Lcom/peel/i/au;)V

    .line 197
    invoke-virtual {v1, v2, v3}, Lcom/peel/widget/ag;->a(ILandroid/content/DialogInterface$OnClickListener;)Lcom/peel/widget/ag;

    move-result-object v1

    sget v2, Lcom/peel/ui/ft;->label_help:I

    new-instance v3, Lcom/peel/i/aw;

    invoke-direct {v3, p0}, Lcom/peel/i/aw;-><init>(Lcom/peel/i/au;)V

    .line 203
    invoke-virtual {v1, v2, v3}, Lcom/peel/widget/ag;->c(ILandroid/content/DialogInterface$OnClickListener;)Lcom/peel/widget/ag;

    move-result-object v1

    sget v2, Lcom/peel/ui/ft;->nothanks:I

    new-instance v3, Lcom/peel/i/av;

    invoke-direct {v3, p0}, Lcom/peel/i/av;-><init>(Lcom/peel/i/au;)V

    .line 209
    invoke-virtual {v1, v2, v3}, Lcom/peel/widget/ag;->b(ILandroid/content/DialogInterface$OnClickListener;)Lcom/peel/widget/ag;

    move-result-object v1

    .line 194
    invoke-static {v0, v1}, Lcom/peel/i/al;->a(Lcom/peel/i/al;Lcom/peel/widget/ag;)Lcom/peel/widget/ag;

    .line 240
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/peel/i/au;->a:Lcom/peel/i/al;

    invoke-static {v0}, Lcom/peel/i/al;->k(Lcom/peel/i/al;)Lcom/peel/widget/ag;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/i/au;->a:Lcom/peel/i/al;

    invoke-static {v1}, Lcom/peel/i/al;->k(Lcom/peel/i/al;)Lcom/peel/widget/ag;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/peel/widget/ag;->a(Lcom/peel/widget/ag;)Lcom/peel/widget/ag;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/widget/ag;->show()V

    .line 241
    return-void

    .line 217
    :cond_1
    iget-object v0, p0, Lcom/peel/i/au;->a:Lcom/peel/i/al;

    new-instance v1, Lcom/peel/widget/ag;

    iget-object v2, p0, Lcom/peel/i/au;->a:Lcom/peel/i/al;

    invoke-virtual {v2}, Lcom/peel/i/al;->m()Landroid/support/v4/app/ae;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/peel/widget/ag;-><init>(Landroid/content/Context;)V

    sget v2, Lcom/peel/ui/ft;->title_dtv_not_found:I

    .line 218
    invoke-virtual {v1, v2}, Lcom/peel/widget/ag;->a(I)Lcom/peel/widget/ag;

    move-result-object v1

    sget v2, Lcom/peel/ui/ft;->msg_dtv_not_found:I

    .line 219
    invoke-virtual {v1, v2}, Lcom/peel/widget/ag;->b(I)Lcom/peel/widget/ag;

    move-result-object v1

    sget v2, Lcom/peel/ui/ft;->label_setup_now:I

    new-instance v3, Lcom/peel/i/ba;

    invoke-direct {v3, p0}, Lcom/peel/i/ba;-><init>(Lcom/peel/i/au;)V

    .line 220
    invoke-virtual {v1, v2, v3}, Lcom/peel/widget/ag;->a(ILandroid/content/DialogInterface$OnClickListener;)Lcom/peel/widget/ag;

    move-result-object v1

    sget v2, Lcom/peel/ui/ft;->label_help:I

    new-instance v3, Lcom/peel/i/az;

    invoke-direct {v3, p0}, Lcom/peel/i/az;-><init>(Lcom/peel/i/au;)V

    .line 226
    invoke-virtual {v1, v2, v3}, Lcom/peel/widget/ag;->c(ILandroid/content/DialogInterface$OnClickListener;)Lcom/peel/widget/ag;

    move-result-object v1

    sget v2, Lcom/peel/ui/ft;->nothanks:I

    new-instance v3, Lcom/peel/i/ay;

    invoke-direct {v3, p0}, Lcom/peel/i/ay;-><init>(Lcom/peel/i/au;)V

    .line 232
    invoke-virtual {v1, v2, v3}, Lcom/peel/widget/ag;->b(ILandroid/content/DialogInterface$OnClickListener;)Lcom/peel/widget/ag;

    move-result-object v1

    .line 217
    invoke-static {v0, v1}, Lcom/peel/i/al;->a(Lcom/peel/i/al;Lcom/peel/widget/ag;)Lcom/peel/widget/ag;

    goto :goto_0
.end method
