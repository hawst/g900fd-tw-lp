.class Lcom/peel/i/ez;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field final synthetic a:Landroid/widget/ArrayAdapter;

.field final synthetic b:Lcom/peel/i/et;


# direct methods
.method constructor <init>(Lcom/peel/i/et;Landroid/widget/ArrayAdapter;)V
    .locals 0

    .prologue
    .line 339
    iput-object p1, p0, Lcom/peel/i/ez;->b:Lcom/peel/i/et;

    iput-object p2, p0, Lcom/peel/i/ez;->a:Landroid/widget/ArrayAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 343
    :try_start_0
    iget-object v0, p0, Lcom/peel/i/ez;->b:Lcom/peel/i/et;

    iget-object v0, v0, Lcom/peel/i/et;->c:Lcom/peel/i/ep;

    invoke-virtual {v0}, Lcom/peel/i/ep;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    const-string/jumbo v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/ae;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 344
    iget-object v1, p0, Lcom/peel/i/ez;->b:Lcom/peel/i/et;

    iget-object v1, v1, Lcom/peel/i/et;->c:Lcom/peel/i/ep;

    invoke-static {v1}, Lcom/peel/i/ep;->a(Lcom/peel/i/ep;)Landroid/widget/EditText;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/EditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 347
    :goto_0
    iget-object v0, p0, Lcom/peel/i/ez;->b:Lcom/peel/i/et;

    iget-object v0, v0, Lcom/peel/i/et;->c:Lcom/peel/i/ep;

    invoke-static {v0}, Lcom/peel/i/ep;->b(Lcom/peel/i/ep;)Landroid/widget/ListView;

    move-result-object v0

    invoke-static {}, Lcom/peel/i/ep;->T()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/peel/util/bx;->a(Landroid/view/View;Ljava/lang/String;)V

    .line 348
    iget-object v0, p0, Lcom/peel/i/ez;->b:Lcom/peel/i/et;

    iget-object v1, v0, Lcom/peel/i/et;->c:Lcom/peel/i/ep;

    invoke-virtual {p1}, Landroid/widget/AdapterView;->getAdapter()Landroid/widget/Adapter;

    move-result-object v0

    invoke-interface {v0, p3}, Landroid/widget/Adapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    invoke-static {v1, v0}, Lcom/peel/i/ep;->a(Lcom/peel/i/ep;Landroid/os/Bundle;)Landroid/os/Bundle;

    .line 349
    iget-object v0, p0, Lcom/peel/i/ez;->b:Lcom/peel/i/et;

    iget-object v0, v0, Lcom/peel/i/et;->c:Lcom/peel/i/ep;

    iget-object v1, p0, Lcom/peel/i/ez;->b:Lcom/peel/i/et;

    iget-object v1, v1, Lcom/peel/i/et;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/peel/i/ep;->b(Lcom/peel/i/ep;Ljava/lang/String;)Ljava/lang/String;

    .line 350
    iget-object v0, p0, Lcom/peel/i/ez;->b:Lcom/peel/i/et;

    iget-object v0, v0, Lcom/peel/i/et;->c:Lcom/peel/i/ep;

    invoke-static {v0, p3}, Lcom/peel/i/ep;->a(Lcom/peel/i/ep;I)I

    .line 351
    iget-object v0, p0, Lcom/peel/i/ez;->a:Landroid/widget/ArrayAdapter;

    invoke-virtual {v0}, Landroid/widget/ArrayAdapter;->notifyDataSetChanged()V

    .line 353
    sget-boolean v0, Lcom/peel/util/a;->m:Z

    if-eqz v0, :cond_0

    .line 354
    iget-object v0, p0, Lcom/peel/i/ez;->b:Lcom/peel/i/et;

    iget-object v0, v0, Lcom/peel/i/et;->c:Lcom/peel/i/ep;

    invoke-static {v0}, Lcom/peel/i/ep;->i(Lcom/peel/i/ep;)V

    .line 358
    :goto_1
    return-void

    .line 356
    :cond_0
    iget-object v0, p0, Lcom/peel/i/ez;->b:Lcom/peel/i/et;

    iget-object v0, v0, Lcom/peel/i/et;->c:Lcom/peel/i/ep;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/peel/i/ep;->a(Lcom/peel/i/ep;Z)V

    goto :goto_1

    .line 345
    :catch_0
    move-exception v0

    goto :goto_0
.end method
