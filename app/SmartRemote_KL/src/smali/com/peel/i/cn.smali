.class Lcom/peel/i/cn;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/peel/i/cg;


# direct methods
.method constructor <init>(Lcom/peel/i/cg;)V
    .locals 0

    .prologue
    .line 556
    iput-object p1, p0, Lcom/peel/i/cn;->a:Lcom/peel/i/cg;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 559
    iget-object v0, p0, Lcom/peel/i/cn;->a:Lcom/peel/i/cg;

    invoke-static {v0}, Lcom/peel/i/cg;->h(Lcom/peel/i/cg;)Landroid/content/Context;

    move-result-object v0

    invoke-static {}, Lcom/peel/content/a;->a()Lcom/peel/data/ContentRoom;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/data/ContentRoom;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/peel/util/bx;->a(Landroid/content/Context;Ljava/lang/String;)V

    .line 560
    iget-object v0, p0, Lcom/peel/i/cn;->a:Lcom/peel/i/cg;

    invoke-static {v0}, Lcom/peel/i/cg;->h(Lcom/peel/i/cg;)Landroid/content/Context;

    move-result-object v0

    invoke-static {}, Lcom/peel/content/a;->a()Lcom/peel/data/ContentRoom;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/data/ContentRoom;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/peel/util/bx;->b(Landroid/content/Context;Ljava/lang/String;)V

    .line 562
    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v0}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/control/RoomControl;->g()[Ljava/lang/String;

    move-result-object v0

    .line 563
    if-eqz v0, :cond_1

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    const-string/jumbo v1, "live"

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 564
    iget-object v0, p0, Lcom/peel/i/cn;->a:Lcom/peel/i/cg;

    invoke-static {v0}, Lcom/peel/i/cg;->h(Lcom/peel/i/cg;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, v4}, Lcom/peel/util/bx;->a(Landroid/content/Context;Z)V

    .line 569
    :goto_0
    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v0}, Lcom/peel/control/am;->c()[Lcom/peel/control/RoomControl;

    move-result-object v0

    array-length v0, v0

    if-ne v0, v4, :cond_4

    .line 570
    iget-object v0, p0, Lcom/peel/i/cn;->a:Lcom/peel/i/cg;

    invoke-virtual {v0}, Lcom/peel/i/cg;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string/jumbo v1, "country_ISO"

    const-string/jumbo v2, "US"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "US"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object v0, Lcom/peel/util/a;->k:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/peel/util/a;->k:Ljava/lang/String;

    const-string/jumbo v1, "yes"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 572
    :cond_0
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 573
    const-string/jumbo v1, "fromSetup"

    invoke-virtual {v0, v1, v4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 574
    iget-object v1, p0, Lcom/peel/i/cn;->a:Lcom/peel/i/cg;

    invoke-virtual {v1}, Lcom/peel/i/cg;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    const-class v2, Lcom/peel/h/a/ff;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lcom/peel/d/e;->a(Landroid/support/v4/app/ae;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 585
    :goto_1
    iget-object v0, p0, Lcom/peel/i/cn;->a:Lcom/peel/i/cg;

    invoke-static {v0}, Lcom/peel/i/cg;->h(Lcom/peel/i/cg;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, v4}, Lcom/peel/util/dq;->a(Landroid/content/Context;Z)V

    .line 586
    iget-object v0, p0, Lcom/peel/i/cn;->a:Lcom/peel/i/cg;

    invoke-static {v0}, Lcom/peel/i/cg;->h(Lcom/peel/i/cg;)Landroid/content/Context;

    move-result-object v0

    const-string/jumbo v1, "setup_type"

    invoke-static {v0, v1, v3}, Lcom/peel/util/dq;->a(Landroid/content/Context;Ljava/lang/String;I)V

    .line 592
    iget-object v0, p0, Lcom/peel/i/cn;->a:Lcom/peel/i/cg;

    invoke-static {v0}, Lcom/peel/i/cg;->h(Lcom/peel/i/cg;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/peel/util/au;->b(Landroid/content/Context;)V

    .line 616
    :goto_2
    return-void

    .line 566
    :cond_1
    iget-object v0, p0, Lcom/peel/i/cn;->a:Lcom/peel/i/cg;

    invoke-static {v0}, Lcom/peel/i/cg;->h(Lcom/peel/i/cg;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, v3}, Lcom/peel/util/bx;->a(Landroid/content/Context;Z)V

    goto :goto_0

    .line 577
    :cond_2
    sget-boolean v0, Lcom/peel/util/a;->g:Z

    if-eqz v0, :cond_3

    .line 578
    iget-object v0, p0, Lcom/peel/i/cn;->a:Lcom/peel/i/cg;

    iget-object v0, v0, Lcom/peel/i/cg;->b:Lcom/peel/d/i;

    iget-object v1, p0, Lcom/peel/i/cn;->a:Lcom/peel/i/cg;

    invoke-virtual {v1}, Lcom/peel/i/cg;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/ae;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/peel/util/bx;->b(Lcom/peel/d/i;Landroid/content/Context;)V

    goto :goto_1

    .line 580
    :cond_3
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 581
    const-string/jumbo v1, "type"

    invoke-virtual {v0, v1, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 582
    iget-object v1, p0, Lcom/peel/i/cn;->a:Lcom/peel/i/cg;

    invoke-virtual {v1}, Lcom/peel/i/cg;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    const-class v2, Lcom/peel/ui/lq;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lcom/peel/d/e;->a(Landroid/support/v4/app/ae;Ljava/lang/String;Landroid/os/Bundle;)V

    goto :goto_1

    .line 594
    :cond_4
    iget-object v0, p0, Lcom/peel/i/cn;->a:Lcom/peel/i/cg;

    invoke-static {v0}, Lcom/peel/i/cg;->f(Lcom/peel/i/cg;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v1, "setup_type"

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 597
    sget-boolean v0, Lcom/peel/util/a;->h:Z

    if-eqz v0, :cond_5

    .line 598
    const-string/jumbo v0, "tuneinoptions"

    iget-object v1, p0, Lcom/peel/i/cn;->a:Lcom/peel/i/cg;

    .line 599
    invoke-virtual {v1}, Lcom/peel/i/cg;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/ae;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    new-instance v2, Lcom/peel/i/co;

    invoke-direct {v2, p0}, Lcom/peel/i/co;-><init>(Lcom/peel/i/cn;)V

    .line 598
    invoke-static {v0, v1, v2}, Lcom/peel/util/a;->a(Ljava/lang/String;Landroid/content/Context;Lcom/peel/util/t;)V

    .line 608
    :cond_5
    sget-boolean v0, Lcom/peel/util/a;->g:Z

    if-eqz v0, :cond_6

    .line 609
    iget-object v0, p0, Lcom/peel/i/cn;->a:Lcom/peel/i/cg;

    iget-object v0, v0, Lcom/peel/i/cg;->b:Lcom/peel/d/i;

    iget-object v1, p0, Lcom/peel/i/cn;->a:Lcom/peel/i/cg;

    invoke-virtual {v1}, Lcom/peel/i/cg;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/ae;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/peel/util/bx;->b(Lcom/peel/d/i;Landroid/content/Context;)V

    goto :goto_2

    .line 611
    :cond_6
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 612
    const-string/jumbo v1, "type"

    invoke-virtual {v0, v1, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 613
    iget-object v1, p0, Lcom/peel/i/cn;->a:Lcom/peel/i/cg;

    invoke-virtual {v1}, Lcom/peel/i/cg;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    const-class v2, Lcom/peel/ui/lq;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lcom/peel/d/e;->a(Landroid/support/v4/app/ae;Ljava/lang/String;Landroid/os/Bundle;)V

    goto/16 :goto_2
.end method
