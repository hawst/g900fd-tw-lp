.class Lcom/peel/i/ek;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic a:Lcom/peel/i/ej;


# direct methods
.method constructor <init>(Lcom/peel/i/ej;)V
    .locals 0

    .prologue
    .line 329
    iput-object p1, p0, Lcom/peel/i/ek;->a:Lcom/peel/i/ej;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 6

    .prologue
    .line 333
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 335
    iget-object v1, p0, Lcom/peel/i/ek;->a:Lcom/peel/i/ej;

    iget-object v1, v1, Lcom/peel/i/ej;->a:Lcom/peel/i/eh;

    iget-object v1, v1, Lcom/peel/i/eh;->d:Lcom/peel/i/dx;

    invoke-static {v1}, Lcom/peel/i/dx;->j(Lcom/peel/i/dx;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 336
    const-string/jumbo v1, "zipcode"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/peel/i/ek;->a:Lcom/peel/i/ej;

    iget-object v3, v3, Lcom/peel/i/ej;->a:Lcom/peel/i/eh;

    iget-object v3, v3, Lcom/peel/i/eh;->d:Lcom/peel/i/dx;

    invoke-static {v3}, Lcom/peel/i/dx;->k(Lcom/peel/i/dx;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " / "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/peel/i/ek;->a:Lcom/peel/i/ej;

    iget-object v3, v3, Lcom/peel/i/ej;->a:Lcom/peel/i/eh;

    iget-object v3, v3, Lcom/peel/i/eh;->d:Lcom/peel/i/dx;

    invoke-static {v3}, Lcom/peel/i/dx;->j(Lcom/peel/i/dx;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 340
    :goto_0
    const-string/jumbo v1, "region"

    iget-object v2, p0, Lcom/peel/i/ek;->a:Lcom/peel/i/ej;

    iget-object v2, v2, Lcom/peel/i/ej;->a:Lcom/peel/i/eh;

    iget-object v2, v2, Lcom/peel/i/eh;->a:Landroid/os/Bundle;

    const-string/jumbo v3, "type"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 341
    iget-object v1, p0, Lcom/peel/i/ek;->a:Lcom/peel/i/ej;

    iget-object v1, v1, Lcom/peel/i/ej;->a:Lcom/peel/i/eh;

    iget-object v1, v1, Lcom/peel/i/eh;->d:Lcom/peel/i/dx;

    invoke-virtual {v1}, Lcom/peel/i/dx;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    const-class v2, Lcom/peel/i/bm;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lcom/peel/d/e;->c(Landroid/support/v4/app/ae;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 343
    invoke-static {}, Lcom/peel/util/a/f;->a()Lcom/peel/util/a/f;

    move-result-object v1

    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v0}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    const/16 v2, 0xbe2

    const/16 v3, 0x7d5

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lcom/peel/i/ek;->a:Lcom/peel/i/ej;

    iget-object v5, v5, Lcom/peel/i/ej;->a:Lcom/peel/i/eh;

    iget-object v5, v5, Lcom/peel/i/eh;->b:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/peel/i/ek;->a:Lcom/peel/i/ej;

    iget-object v5, v5, Lcom/peel/i/ej;->a:Lcom/peel/i/eh;

    iget-object v5, v5, Lcom/peel/i/eh;->c:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v0, v2, v3, v4}, Lcom/peel/util/a/f;->a(IIILjava/lang/String;)V

    .line 345
    return-void

    .line 338
    :cond_0
    const-string/jumbo v1, "zipcode"

    iget-object v2, p0, Lcom/peel/i/ek;->a:Lcom/peel/i/ej;

    iget-object v2, v2, Lcom/peel/i/ej;->a:Lcom/peel/i/eh;

    iget-object v2, v2, Lcom/peel/i/eh;->d:Lcom/peel/i/dx;

    invoke-static {v2}, Lcom/peel/i/dx;->k(Lcom/peel/i/dx;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 343
    :cond_1
    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v0}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/data/at;->f()I

    move-result v0

    goto :goto_1
.end method
