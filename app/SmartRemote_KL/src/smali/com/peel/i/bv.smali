.class public Lcom/peel/i/bv;
.super Lcom/peel/d/t;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Lcom/peel/d/t;-><init>()V

    return-void
.end method


# virtual methods
.method public S()V
    .locals 10

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x1

    const/4 v3, 0x0

    .line 51
    sget-object v0, Lcom/peel/content/a;->f:Lcom/peel/content/user/User;

    invoke-virtual {v0}, Lcom/peel/content/user/User;->l()[Lcom/peel/data/ContentRoom;

    move-result-object v8

    .line 53
    if-eqz v8, :cond_6

    .line 54
    array-length v2, v8

    move v1, v7

    move v0, v6

    :goto_0
    if-ge v1, v2, :cond_1

    aget-object v4, v8, v1

    .line 55
    invoke-virtual {v4}, Lcom/peel/data/ContentRoom;->b()I

    move-result v5

    if-lt v5, v0, :cond_0

    .line 56
    invoke-virtual {v4}, Lcom/peel/data/ContentRoom;->b()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    .line 54
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    move v4, v0

    .line 61
    :goto_1
    new-instance v9, Lcom/peel/control/RoomControl;

    iget-object v0, p0, Lcom/peel/i/bv;->ak:Landroid/os/Bundle;

    const-string/jumbo v1, "room_name"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v9, v0}, Lcom/peel/control/RoomControl;-><init>(Ljava/lang/String;)V

    .line 62
    invoke-virtual {v9}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/peel/data/at;->a(I)V

    .line 63
    invoke-static {v7, v3}, Lcom/peel/control/o;->a(ILjava/lang/String;)Lcom/peel/control/o;

    move-result-object v0

    invoke-virtual {v9, v0}, Lcom/peel/control/RoomControl;->a(Lcom/peel/control/o;)V

    .line 72
    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v0}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v0

    if-nez v0, :cond_2

    .line 73
    invoke-virtual {v9}, Lcom/peel/control/RoomControl;->e()Z

    .line 76
    :cond_2
    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v0, v9}, Lcom/peel/control/am;->a(Lcom/peel/control/RoomControl;)V

    .line 77
    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v0, v9}, Lcom/peel/control/am;->b(Lcom/peel/control/RoomControl;)V

    .line 79
    new-instance v0, Lcom/peel/data/ContentRoom;

    invoke-virtual {v9}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/data/at;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v9}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v2

    invoke-virtual {v2}, Lcom/peel/data/at;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v9}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v5

    invoke-virtual {v5}, Lcom/peel/data/at;->b()Ljava/lang/String;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, Lcom/peel/data/ContentRoom;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;ILjava/lang/String;)V

    .line 81
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 82
    const-string/jumbo v2, "path"

    const-string/jumbo v4, "rooms/add"

    invoke-virtual {v1, v2, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 83
    const-string/jumbo v2, "room"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 84
    sget-object v2, Lcom/peel/content/a;->f:Lcom/peel/content/user/User;

    invoke-virtual {v2, v1, v3}, Lcom/peel/content/user/User;->a(Landroid/os/Bundle;Lcom/peel/util/t;)V

    .line 88
    invoke-virtual {v0}, Lcom/peel/data/ContentRoom;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v7, v6, v3}, Lcom/peel/content/a;->a(Ljava/lang/String;ZZLcom/peel/util/t;)V

    .line 90
    sget-object v0, Lcom/peel/content/a;->f:Lcom/peel/content/user/User;

    invoke-virtual {v0}, Lcom/peel/content/user/User;->u()V

    .line 92
    invoke-virtual {p0}, Lcom/peel/i/bv;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    invoke-static {v0, v6}, Lcom/peel/util/dq;->a(Landroid/content/Context;Z)V

    .line 93
    invoke-virtual {p0}, Lcom/peel/i/bv;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    const-string/jumbo v1, "setup_type"

    invoke-static {v0, v1, v6}, Lcom/peel/util/dq;->a(Landroid/content/Context;Ljava/lang/String;I)V

    .line 95
    invoke-virtual {p0}, Lcom/peel/i/bv;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/ae;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/peel/social/w;->f(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 96
    new-instance v0, Lcom/peel/backup/c;

    invoke-virtual {p0}, Lcom/peel/i/bv;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/ae;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/peel/backup/c;-><init>(Landroid/content/Context;)V

    .line 98
    if-eqz v8, :cond_3

    array-length v1, v8

    if-nez v1, :cond_5

    .line 99
    :cond_3
    invoke-virtual {v0, v6, v3, v3, v3}, Lcom/peel/backup/c;->a(ZLjava/lang/String;Ljava/lang/String;Lcom/peel/util/t;)V

    .line 105
    :cond_4
    :goto_2
    const-class v0, Lcom/peel/i/bv;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "finish device setup"

    new-instance v2, Lcom/peel/i/bx;

    invoke-direct {v2, p0}, Lcom/peel/i/bx;-><init>(Lcom/peel/i/bv;)V

    invoke-static {v0, v1, v2}, Lcom/peel/util/i;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/lang/Runnable;

    .line 112
    return-void

    .line 101
    :cond_5
    invoke-virtual {v0, v9, v3, v3}, Lcom/peel/backup/c;->a(Lcom/peel/control/RoomControl;Ljava/lang/String;Lcom/peel/util/t;)V

    goto :goto_2

    :cond_6
    move v4, v6

    goto/16 :goto_1
.end method

.method public a_(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 25
    invoke-super {p0, p1}, Lcom/peel/d/t;->a_(Landroid/os/Bundle;)V

    .line 26
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/peel/i/bv;->b(Z)V

    .line 27
    return-void
.end method

.method public c(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 4

    .prologue
    .line 31
    new-instance v0, Landroid/app/ProgressDialog;

    invoke-virtual {p0}, Lcom/peel/i/bv;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    sget v2, Lcom/peel/ui/fu;->DialogTheme:I

    invoke-direct {v0, v1, v2}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;I)V

    .line 32
    sget v1, Lcom/peel/ui/ft;->please_wait:I

    invoke-virtual {p0, v1}, Lcom/peel/i/bv;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 33
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    .line 34
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 36
    iget-object v1, p0, Lcom/peel/i/bv;->ak:Landroid/os/Bundle;

    const-string/jumbo v2, "room_name"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 37
    iget-object v1, p0, Lcom/peel/i/bv;->ak:Landroid/os/Bundle;

    const-string/jumbo v2, "room_name"

    sget v3, Lcom/peel/ui/ft;->my_room:I

    invoke-virtual {p0, v3}, Lcom/peel/i/bv;->a(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 40
    :cond_0
    const-class v1, Lcom/peel/i/bv;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "finishControlOnlySetup"

    new-instance v3, Lcom/peel/i/bw;

    invoke-direct {v3, p0}, Lcom/peel/i/bw;-><init>(Lcom/peel/i/bv;)V

    invoke-static {v1, v2, v3}, Lcom/peel/util/i;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/lang/Runnable;

    .line 47
    return-object v0
.end method
