.class public Lcom/peel/i/ai;
.super Lcom/peel/d/u;


# instance fields
.field private e:Landroid/view/View;

.field private f:Landroid/widget/ListView;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Lcom/peel/d/u;-><init>()V

    return-void
.end method


# virtual methods
.method public Z()V
    .locals 6

    .prologue
    .line 197
    iget-object v0, p0, Lcom/peel/i/ai;->d:Lcom/peel/d/a;

    if-nez v0, :cond_0

    .line 198
    new-instance v0, Lcom/peel/d/a;

    sget-object v1, Lcom/peel/d/d;->b:Lcom/peel/d/d;

    sget-object v2, Lcom/peel/d/b;->b:Lcom/peel/d/b;

    sget-object v3, Lcom/peel/d/c;->b:Lcom/peel/d/c;

    sget v4, Lcom/peel/ui/ft;->choose_device_type_msg:I

    invoke-virtual {p0, v4}, Lcom/peel/i/ai;->a(I)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, Lcom/peel/d/a;-><init>(Lcom/peel/d/d;Lcom/peel/d/b;Lcom/peel/d/c;Ljava/lang/String;Ljava/util/List;)V

    iput-object v0, p0, Lcom/peel/i/ai;->d:Lcom/peel/d/a;

    .line 200
    :cond_0
    iget-object v0, p0, Lcom/peel/i/ai;->b:Lcom/peel/d/i;

    iget-object v1, p0, Lcom/peel/i/ai;->d:Lcom/peel/d/a;

    invoke-virtual {v0, v1}, Lcom/peel/d/i;->a(Lcom/peel/d/a;)V

    .line 201
    return-void
.end method

.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    .line 38
    sget v0, Lcom/peel/ui/fq;->device_type_selection_fragment:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/i/ai;->e:Landroid/view/View;

    .line 39
    iget-object v0, p0, Lcom/peel/i/ai;->e:Landroid/view/View;

    sget v1, Lcom/peel/ui/fp;->single_list:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/peel/i/ai;->f:Landroid/widget/ListView;

    .line 40
    iget-object v0, p0, Lcom/peel/i/ai;->e:Landroid/view/View;

    return-object v0
.end method

.method public c(Landroid/os/Bundle;)V
    .locals 15

    .prologue
    .line 55
    invoke-super/range {p0 .. p1}, Lcom/peel/d/u;->c(Landroid/os/Bundle;)V

    .line 57
    sget-object v0, Lcom/peel/content/a;->h:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-nez v0, :cond_1

    .line 192
    :cond_0
    :goto_0
    return-void

    .line 59
    :cond_1
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 60
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 61
    const/4 v6, 0x0

    .line 62
    const/4 v5, 0x0

    .line 63
    const/4 v4, 0x0

    .line 64
    const/4 v3, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 67
    sget-object v7, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v7}, Lcom/peel/control/am;->e()[Lcom/peel/control/h;

    move-result-object v10

    .line 68
    if-eqz v10, :cond_a

    array-length v7, v10

    if-lez v7, :cond_a

    .line 69
    array-length v11, v10

    const/4 v7, 0x0

    :goto_1
    if-ge v7, v11, :cond_a

    aget-object v12, v10, v7

    .line 70
    invoke-virtual {v12}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v13

    invoke-virtual {v13}, Lcom/peel/data/g;->d()I

    move-result v13

    const/4 v14, 0x1

    if-ne v13, v14, :cond_3

    .line 71
    const/4 v6, 0x1

    .line 69
    :cond_2
    :goto_2
    add-int/lit8 v7, v7, 0x1

    goto :goto_1

    .line 72
    :cond_3
    invoke-virtual {v12}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v13

    invoke-virtual {v13}, Lcom/peel/data/g;->d()I

    move-result v13

    const/4 v14, 0x2

    if-eq v13, v14, :cond_4

    invoke-virtual {v12}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v13

    invoke-virtual {v13}, Lcom/peel/data/g;->d()I

    move-result v13

    const/16 v14, 0x14

    if-ne v13, v14, :cond_5

    .line 73
    :cond_4
    const/4 v5, 0x1

    goto :goto_2

    .line 74
    :cond_5
    invoke-virtual {v12}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v13

    invoke-virtual {v13}, Lcom/peel/data/g;->d()I

    move-result v13

    const/4 v14, 0x5

    if-ne v13, v14, :cond_6

    .line 75
    const/4 v4, 0x1

    goto :goto_2

    .line 76
    :cond_6
    invoke-virtual {v12}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v13

    invoke-virtual {v13}, Lcom/peel/data/g;->d()I

    move-result v13

    const/4 v14, 0x3

    if-ne v13, v14, :cond_7

    .line 77
    const/4 v3, 0x1

    goto :goto_2

    .line 78
    :cond_7
    invoke-virtual {v12}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v13

    invoke-virtual {v13}, Lcom/peel/data/g;->d()I

    move-result v13

    const/4 v14, 0x4

    if-ne v13, v14, :cond_8

    .line 79
    const/4 v2, 0x1

    goto :goto_2

    .line 80
    :cond_8
    invoke-virtual {v12}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v13

    invoke-virtual {v13}, Lcom/peel/data/g;->d()I

    move-result v13

    const/16 v14, 0xa

    if-ne v13, v14, :cond_9

    .line 81
    const/4 v1, 0x1

    goto :goto_2

    .line 82
    :cond_9
    invoke-virtual {v12}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v12

    invoke-virtual {v12}, Lcom/peel/data/g;->d()I

    move-result v12

    const/16 v13, 0x12

    if-ne v12, v13, :cond_2

    .line 83
    const/4 v0, 0x1

    goto :goto_2

    .line 88
    :cond_a
    if-nez v6, :cond_b

    if-nez v1, :cond_b

    .line 89
    const/4 v7, 0x1

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v8, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 90
    sget v7, Lcom/peel/ui/ft;->DeviceType1:I

    invoke-virtual {p0, v7}, Lcom/peel/i/ai;->a(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v9, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 92
    :cond_b
    if-nez v5, :cond_c

    .line 93
    const/4 v5, 0x2

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v8, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 94
    sget v5, Lcom/peel/ui/ft;->DeviceType2:I

    invoke-virtual {p0, v5}, Lcom/peel/i/ai;->a(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v9, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 96
    :cond_c
    if-nez v3, :cond_d

    .line 97
    const/4 v3, 0x3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v8, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 98
    sget v3, Lcom/peel/ui/ft;->DeviceType3:I

    invoke-virtual {p0, v3}, Lcom/peel/i/ai;->a(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v9, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 100
    :cond_d
    if-nez v2, :cond_e

    .line 101
    const/4 v2, 0x4

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v8, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 102
    sget v2, Lcom/peel/ui/ft;->DeviceType4:I

    invoke-virtual {p0, v2}, Lcom/peel/i/ai;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v9, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 104
    :cond_e
    if-nez v4, :cond_f

    .line 105
    const/4 v2, 0x5

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v8, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 106
    sget v2, Lcom/peel/ui/ft;->DeviceType5:I

    invoke-virtual {p0, v2}, Lcom/peel/i/ai;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v9, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 108
    :cond_f
    const/4 v2, 0x6

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v8, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 109
    sget v2, Lcom/peel/ui/ft;->DeviceType6:I

    invoke-virtual {p0, v2}, Lcom/peel/i/ai;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v9, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 110
    if-nez v6, :cond_10

    if-nez v1, :cond_10

    .line 111
    const/16 v1, 0xa

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v8, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 112
    sget v1, Lcom/peel/ui/ft;->DeviceType10:I

    invoke-virtual {p0, v1}, Lcom/peel/i/ai;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v9, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 115
    :cond_10
    if-nez v0, :cond_11

    .line 116
    const/16 v0, 0x12

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 117
    sget v0, Lcom/peel/ui/ft;->DeviceType18:I

    invoke-virtual {p0, v0}, Lcom/peel/i/ai;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v9, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 120
    :cond_11
    iget-object v0, p0, Lcom/peel/i/ai;->f:Landroid/widget/ListView;

    new-instance v1, Landroid/widget/ArrayAdapter;

    invoke-virtual {p0}, Lcom/peel/i/ai;->m()Landroid/support/v4/app/ae;

    move-result-object v2

    sget v3, Lcom/peel/ui/fq;->device_type_row:I

    .line 122
    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v4

    new-array v4, v4, [Ljava/lang/String;

    invoke-virtual {v9, v4}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v4

    invoke-direct {v1, v2, v3, v4}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    .line 120
    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 126
    iget-object v0, p0, Lcom/peel/i/ai;->f:Landroid/widget/ListView;

    new-instance v1, Lcom/peel/i/aj;

    invoke-direct {v1, p0, v8}, Lcom/peel/i/aj;-><init>(Lcom/peel/i/ai;Ljava/util/ArrayList;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 170
    iget-object v0, p0, Lcom/peel/i/ai;->e:Landroid/view/View;

    sget v1, Lcom/peel/ui/fp;->done_btn:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    .line 171
    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v0}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 172
    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v0}, Lcom/peel/control/am;->e()[Lcom/peel/control/h;

    move-result-object v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v0}, Lcom/peel/control/am;->e()[Lcom/peel/control/h;

    move-result-object v0

    array-length v0, v0

    if-lez v0, :cond_0

    .line 173
    iget-object v0, p0, Lcom/peel/i/ai;->e:Landroid/view/View;

    sget v1, Lcom/peel/ui/fp;->done_btn:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 174
    iget-object v0, p0, Lcom/peel/i/ai;->e:Landroid/view/View;

    sget v1, Lcom/peel/ui/fp;->done_btn:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/peel/i/ak;

    invoke-direct {v1, p0}, Lcom/peel/i/ak;-><init>(Lcom/peel/i/ai;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_0
.end method

.method public d(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 45
    invoke-super {p0, p1}, Lcom/peel/d/u;->d(Landroid/os/Bundle;)V

    .line 47
    iget-object v0, p0, Lcom/peel/i/ai;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "category"

    sget v2, Lcom/peel/ui/ft;->choose_device_type_msg:I

    invoke-virtual {p0, v2}, Lcom/peel/i/ai;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 48
    sget-object v0, Lcom/peel/content/a;->h:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/peel/i/ai;->c:Landroid/os/Bundle;

    invoke-virtual {p0, v0}, Lcom/peel/i/ai;->c(Landroid/os/Bundle;)V

    .line 50
    :cond_0
    invoke-virtual {p0}, Lcom/peel/i/ai;->Z()V

    .line 51
    return-void
.end method
