.class public Lcom/peel/i/cg;
.super Lcom/peel/d/u;

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private e:Landroid/app/ProgressDialog;

.field private f:Landroid/content/Context;

.field private g:Landroid/content/SharedPreferences;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 59
    invoke-direct {p0}, Lcom/peel/d/u;-><init>()V

    return-void
.end method

.method private T()V
    .locals 4

    .prologue
    .line 553
    invoke-static {}, Lcom/peel/util/a/f;->a()Lcom/peel/util/a/f;

    move-result-object v0

    invoke-static {}, Lcom/peel/content/a;->a()Lcom/peel/data/ContentRoom;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/data/ContentRoom;->b()I

    move-result v1

    const/16 v2, 0xbc5

    const/16 v3, 0x7d5

    invoke-virtual {v0, v1, v2, v3}, Lcom/peel/util/a/f;->a(III)V

    .line 556
    const-class v0, Lcom/peel/i/cg;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, ""

    new-instance v2, Lcom/peel/i/cn;

    invoke-direct {v2, p0}, Lcom/peel/i/cn;-><init>(Lcom/peel/i/cg;)V

    invoke-static {v0, v1, v2}, Lcom/peel/util/i;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/lang/Runnable;

    .line 618
    return-void
.end method

.method static synthetic a(Lcom/peel/i/cg;Landroid/app/ProgressDialog;)Landroid/app/ProgressDialog;
    .locals 0

    .prologue
    .line 59
    iput-object p1, p0, Lcom/peel/i/cg;->e:Landroid/app/ProgressDialog;

    return-object p1
.end method

.method static synthetic a(Lcom/peel/i/cg;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/peel/i/cg;->a:Ljava/lang/String;

    return-object v0
.end method

.method private a(Landroid/os/Bundle;Landroid/os/Bundle;Z)V
    .locals 9

    .prologue
    const/4 v1, 0x1

    const/4 v5, 0x0

    const/4 v3, 0x0

    .line 291
    iget-object v0, p0, Lcom/peel/i/cg;->c:Landroid/os/Bundle;

    const-string/jumbo v2, "content_room"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 292
    sget-object v0, Lcom/peel/content/a;->f:Lcom/peel/content/user/User;

    invoke-virtual {v0}, Lcom/peel/content/user/User;->l()[Lcom/peel/data/ContentRoom;

    move-result-object v4

    .line 294
    if-eqz v4, :cond_6

    .line 295
    array-length v6, v4

    move v2, v5

    move v0, v1

    :goto_0
    if-ge v2, v6, :cond_1

    aget-object v7, v4, v2

    .line 296
    invoke-virtual {v7}, Lcom/peel/data/ContentRoom;->b()I

    move-result v8

    if-lt v8, v0, :cond_0

    .line 297
    invoke-virtual {v7}, Lcom/peel/data/ContentRoom;->b()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    .line 295
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    move v4, v0

    .line 302
    :goto_1
    new-instance v6, Lcom/peel/control/RoomControl;

    iget-object v0, p0, Lcom/peel/i/cg;->c:Landroid/os/Bundle;

    const-string/jumbo v2, "room_name"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v6, v0}, Lcom/peel/control/RoomControl;-><init>(Ljava/lang/String;)V

    .line 303
    invoke-virtual {v6}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/peel/data/at;->a(I)V

    .line 304
    invoke-static {v5, v3}, Lcom/peel/control/o;->a(ILjava/lang/String;)Lcom/peel/control/o;

    move-result-object v0

    invoke-virtual {v6, v0}, Lcom/peel/control/RoomControl;->a(Lcom/peel/control/o;)V

    .line 313
    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v0}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v0

    if-nez v0, :cond_2

    .line 314
    invoke-virtual {v6}, Lcom/peel/control/RoomControl;->e()Z

    .line 317
    :cond_2
    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v0, v6}, Lcom/peel/control/am;->a(Lcom/peel/control/RoomControl;)V

    .line 318
    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v0, v6}, Lcom/peel/control/am;->b(Lcom/peel/control/RoomControl;)V

    .line 320
    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v0}, Lcom/peel/control/am;->c()[Lcom/peel/control/RoomControl;

    move-result-object v0

    array-length v0, v0

    if-ne v0, v1, :cond_3

    .line 321
    sget-object v0, Lcom/peel/content/a;->f:Lcom/peel/content/user/User;

    iget-object v2, p0, Lcom/peel/i/cg;->c:Landroid/os/Bundle;

    const-string/jumbo v7, "provider"

    invoke-virtual {v2, v7}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v2

    const-string/jumbo v7, "location"

    invoke-virtual {v2, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/peel/content/user/User;->a(Ljava/lang/String;)V

    .line 323
    sget-object v0, Lcom/peel/content/a;->f:Lcom/peel/content/user/User;

    invoke-virtual {v0}, Lcom/peel/content/user/User;->x()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v5, v3}, Lcom/peel/content/a/j;->a(Ljava/lang/String;ILcom/peel/util/t;)V

    .line 326
    sget-object v0, Lcom/peel/content/a;->f:Lcom/peel/content/user/User;

    invoke-virtual {v0}, Lcom/peel/content/user/User;->x()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v1, v3}, Lcom/peel/content/a/j;->a(Ljava/lang/String;ILcom/peel/util/t;)V

    .line 330
    iget-object v0, p0, Lcom/peel/i/cg;->a:Ljava/lang/String;

    const-string/jumbo v1, "add master genres"

    new-instance v2, Lcom/peel/i/ch;

    invoke-direct {v2, p0}, Lcom/peel/i/ch;-><init>(Lcom/peel/i/cg;)V

    invoke-static {v0, v1, v2}, Lcom/peel/util/i;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    .line 366
    :cond_3
    new-instance v0, Lcom/peel/data/ContentRoom;

    invoke-virtual {v6}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/data/at;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v2

    invoke-virtual {v2}, Lcom/peel/data/at;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v6}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v5

    invoke-virtual {v5}, Lcom/peel/data/at;->b()Ljava/lang/String;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, Lcom/peel/data/ContentRoom;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;ILjava/lang/String;)V

    .line 368
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 369
    const-string/jumbo v2, "path"

    const-string/jumbo v4, "rooms/add"

    invoke-virtual {v1, v2, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 370
    const-string/jumbo v2, "room"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 371
    sget-object v2, Lcom/peel/content/a;->f:Lcom/peel/content/user/User;

    invoke-virtual {v2, v1, v3}, Lcom/peel/content/user/User;->a(Landroid/os/Bundle;Lcom/peel/util/t;)V

    move-object v3, v0

    .line 376
    :goto_2
    new-instance v4, Lcom/peel/content/library/LiveLibrary;

    const-string/jumbo v0, "id"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v4, v0, p2}, Lcom/peel/content/library/LiveLibrary;-><init>(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 379
    sget-object v0, Lcom/peel/content/a;->f:Lcom/peel/content/user/User;

    invoke-virtual {v0}, Lcom/peel/content/user/User;->c()Landroid/os/Bundle;

    move-result-object v0

    .line 380
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3}, Lcom/peel/data/ContentRoom;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v4}, Lcom/peel/content/library/LiveLibrary;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 381
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3}, Lcom/peel/data/ContentRoom;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v4}, Lcom/peel/content/library/LiveLibrary;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "B"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 384
    :cond_4
    new-instance v5, Landroid/os/Bundle;

    invoke-direct {v5}, Landroid/os/Bundle;-><init>()V

    .line 385
    const-string/jumbo v1, "user"

    sget-object v2, Lcom/peel/content/a;->f:Lcom/peel/content/user/User;

    invoke-virtual {v2}, Lcom/peel/content/user/User;->x()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v5, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 386
    const-string/jumbo v1, "content_user"

    sget-object v2, Lcom/peel/content/a;->f:Lcom/peel/content/user/User;

    invoke-virtual {v5, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 387
    const-string/jumbo v1, "hdprefs"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3}, Lcom/peel/data/ContentRoom;->a()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v6, "/"

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v4}, Lcom/peel/content/library/LiveLibrary;->e()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v1, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 388
    const-string/jumbo v0, "path"

    const-string/jumbo v1, "lineup"

    invoke-virtual {v5, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 389
    const-string/jumbo v0, "room"

    invoke-virtual {v5, v0, v3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 390
    const-string/jumbo v0, "country"

    const-string/jumbo v1, "iso"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 391
    new-instance v0, Lcom/peel/i/ck;

    move-object v1, p0

    move-object v2, p1

    move-object v6, p2

    move v7, p3

    invoke-direct/range {v0 .. v7}, Lcom/peel/i/ck;-><init>(Lcom/peel/i/cg;Landroid/os/Bundle;Lcom/peel/data/ContentRoom;Lcom/peel/content/library/LiveLibrary;Landroid/os/Bundle;Landroid/os/Bundle;Z)V

    invoke-virtual {v4, v5, v0}, Lcom/peel/content/library/LiveLibrary;->a(Landroid/os/Bundle;Lcom/peel/util/t;)V

    .line 541
    return-void

    .line 373
    :cond_5
    iget-object v0, p0, Lcom/peel/i/cg;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "content_room"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/peel/data/ContentRoom;

    move-object v3, v0

    goto/16 :goto_2

    :cond_6
    move v4, v1

    goto/16 :goto_1
.end method

.method private a(Lcom/peel/data/ContentRoom;Lcom/peel/content/library/Library;)V
    .locals 4

    .prologue
    .line 544
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 545
    const-string/jumbo v1, "room"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 546
    const-string/jumbo v1, "library"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 547
    const-string/jumbo v1, "from_disambiguation"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 548
    iget-object v1, p0, Lcom/peel/i/cg;->c:Landroid/os/Bundle;

    const-string/jumbo v2, "goback_clazz"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string/jumbo v1, "goback_clazz"

    iget-object v2, p0, Lcom/peel/i/cg;->c:Landroid/os/Bundle;

    const-string/jumbo v3, "goback_clazz"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 549
    :cond_0
    invoke-virtual {p0}, Lcom/peel/i/cg;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    const-class v2, Lcom/peel/h/a/bb;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lcom/peel/d/e;->c(Landroid/support/v4/app/ae;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 550
    return-void
.end method

.method static synthetic a(Lcom/peel/i/cg;Lcom/peel/data/ContentRoom;Lcom/peel/content/library/Library;)V
    .locals 0

    .prologue
    .line 59
    invoke-direct {p0, p1, p2}, Lcom/peel/i/cg;->a(Lcom/peel/data/ContentRoom;Lcom/peel/content/library/Library;)V

    return-void
.end method

.method private a(Z)V
    .locals 3

    .prologue
    .line 209
    iget-object v0, p0, Lcom/peel/i/cg;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "country"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    .line 210
    invoke-virtual {p0}, Lcom/peel/i/cg;->c()V

    .line 213
    iget-object v1, p0, Lcom/peel/i/cg;->c:Landroid/os/Bundle;

    const-string/jumbo v2, "provider"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    invoke-direct {p0, v0, v1, p1}, Lcom/peel/i/cg;->a(Landroid/os/Bundle;Landroid/os/Bundle;Z)V

    .line 285
    return-void
.end method

.method static synthetic b(Lcom/peel/i/cg;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/peel/i/cg;->a:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic c(Lcom/peel/i/cg;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/peel/i/cg;->a:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic d(Lcom/peel/i/cg;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/peel/i/cg;->a:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic e(Lcom/peel/i/cg;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/peel/i/cg;->a:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic f(Lcom/peel/i/cg;)Landroid/content/SharedPreferences;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/peel/i/cg;->g:Landroid/content/SharedPreferences;

    return-object v0
.end method

.method static synthetic g(Lcom/peel/i/cg;)V
    .locals 0

    .prologue
    .line 59
    invoke-direct {p0}, Lcom/peel/i/cg;->T()V

    return-void
.end method

.method static synthetic h(Lcom/peel/i/cg;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/peel/i/cg;->f:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic i(Lcom/peel/i/cg;)Landroid/app/ProgressDialog;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/peel/i/cg;->e:Landroid/app/ProgressDialog;

    return-object v0
.end method


# virtual methods
.method public S()V
    .locals 3

    .prologue
    .line 685
    new-instance v0, Lcom/peel/i/cq;

    invoke-direct {v0, p0}, Lcom/peel/i/cq;-><init>(Lcom/peel/i/cg;)V

    .line 693
    invoke-static {}, Lcom/peel/util/i;->c()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 694
    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 698
    :goto_0
    return-void

    .line 696
    :cond_0
    const-class v1, Lcom/peel/i/cg;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "dismiss loading"

    invoke-static {v1, v2, v0}, Lcom/peel/util/i;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/lang/Runnable;

    goto :goto_0
.end method

.method public Z()V
    .locals 6

    .prologue
    .line 709
    iget-object v0, p0, Lcom/peel/i/cg;->d:Lcom/peel/d/a;

    if-nez v0, :cond_0

    .line 710
    new-instance v0, Lcom/peel/d/a;

    sget-object v1, Lcom/peel/d/d;->b:Lcom/peel/d/d;

    sget-object v2, Lcom/peel/d/b;->a:Lcom/peel/d/b;

    sget-object v3, Lcom/peel/d/c;->b:Lcom/peel/d/c;

    sget v4, Lcom/peel/ui/ft;->title_choose_correct_lineup:I

    invoke-virtual {p0, v4}, Lcom/peel/i/cg;->a(I)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, Lcom/peel/d/a;-><init>(Lcom/peel/d/d;Lcom/peel/d/b;Lcom/peel/d/c;Ljava/lang/String;Ljava/util/List;)V

    iput-object v0, p0, Lcom/peel/i/cg;->d:Lcom/peel/d/a;

    .line 712
    :cond_0
    iget-object v0, p0, Lcom/peel/i/cg;->b:Lcom/peel/d/i;

    iget-object v1, p0, Lcom/peel/i/cg;->d:Lcom/peel/d/a;

    invoke-virtual {v0, v1}, Lcom/peel/d/i;->a(Lcom/peel/d/a;)V

    .line 713
    return-void
.end method

.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    .line 71
    sget v0, Lcom/peel/ui/fq;->disambiguation:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 75
    return-object v0
.end method

.method public c()V
    .locals 3

    .prologue
    .line 660
    new-instance v0, Lcom/peel/i/cp;

    invoke-direct {v0, p0}, Lcom/peel/i/cp;-><init>(Lcom/peel/i/cg;)V

    .line 677
    invoke-static {}, Lcom/peel/util/i;->c()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 678
    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 682
    :goto_0
    return-void

    .line 680
    :cond_0
    const-class v1, Lcom/peel/i/cg;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "dismiss loading"

    invoke-static {v1, v2, v0}, Lcom/peel/util/i;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/lang/Runnable;

    goto :goto_0
.end method

.method public c(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 131
    sget-object v0, Lcom/peel/content/a;->h:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/peel/i/cg;->v()Landroid/view/View;

    move-result-object v0

    if-nez v0, :cond_1

    .line 158
    :cond_0
    :goto_0
    return-void

    .line 133
    :cond_1
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/peel/i/cg;->a(Z)V

    goto :goto_0
.end method

.method public d(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    .line 80
    invoke-super {p0, p1}, Lcom/peel/d/u;->d(Landroid/os/Bundle;)V

    .line 81
    invoke-virtual {p0}, Lcom/peel/i/cg;->Z()V

    .line 82
    invoke-virtual {p0}, Lcom/peel/i/cg;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/ae;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/i/cg;->f:Landroid/content/Context;

    .line 84
    iget-object v0, p0, Lcom/peel/i/cg;->f:Landroid/content/Context;

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/i/cg;->g:Landroid/content/SharedPreferences;

    .line 85
    if-eqz p1, :cond_0

    .line 86
    iget-object v0, p0, Lcom/peel/i/cg;->c:Landroid/os/Bundle;

    invoke-virtual {v0, p1}, Landroid/os/Bundle;->putAll(Landroid/os/Bundle;)V

    .line 89
    :cond_0
    iget-object v0, p0, Lcom/peel/i/cg;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "room_name"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 90
    iget-object v0, p0, Lcom/peel/i/cg;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "room_name"

    sget v2, Lcom/peel/ui/ft;->my_room:I

    invoke-virtual {p0, v2}, Lcom/peel/i/cg;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 93
    :cond_1
    iget-object v0, p0, Lcom/peel/i/cg;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "provider"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/peel/i/cg;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "content_library"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 94
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 95
    iget-object v0, p0, Lcom/peel/i/cg;->c:Landroid/os/Bundle;

    const-string/jumbo v2, "content_library"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/peel/content/library/Library;

    .line 96
    const-string/jumbo v2, "id"

    invoke-virtual {v0}, Lcom/peel/content/library/Library;->e()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 97
    instance-of v2, v0, Lcom/peel/content/library/LiveLibrary;

    if-eqz v2, :cond_2

    .line 98
    check-cast v0, Lcom/peel/content/library/LiveLibrary;

    .line 99
    const-string/jumbo v2, "location"

    invoke-virtual {v0}, Lcom/peel/content/library/LiveLibrary;->h()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 100
    const-string/jumbo v2, "mso"

    invoke-virtual {v0}, Lcom/peel/content/library/LiveLibrary;->j()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 101
    const-string/jumbo v2, "name"

    invoke-virtual {v0}, Lcom/peel/content/library/LiveLibrary;->d()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 102
    const-string/jumbo v2, "channeldifference"

    invoke-virtual {v0}, Lcom/peel/content/library/LiveLibrary;->m()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 103
    const-string/jumbo v2, "boxtype"

    invoke-virtual {v0}, Lcom/peel/content/library/LiveLibrary;->k()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 104
    const-string/jumbo v2, "lineupcount"

    invoke-virtual {v0}, Lcom/peel/content/library/LiveLibrary;->l()I

    move-result v0

    int-to-long v4, v0

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 106
    :cond_2
    iget-object v0, p0, Lcom/peel/i/cg;->c:Landroid/os/Bundle;

    const-string/jumbo v2, "provider"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 109
    :cond_3
    iget-object v0, p0, Lcom/peel/i/cg;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "country"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 111
    iget-object v0, p0, Lcom/peel/i/cg;->g:Landroid/content/SharedPreferences;

    const-string/jumbo v1, "config_legacy"

    const-string/jumbo v2, "US"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 112
    const-string/jumbo v1, "US"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 113
    const-string/jumbo v0, "name|USA|endpoint|usa|iso|US|type|5digitzip"

    invoke-static {v0}, Lcom/peel/util/bx;->b(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    .line 118
    :goto_0
    iget-object v1, p0, Lcom/peel/i/cg;->c:Landroid/os/Bundle;

    const-string/jumbo v2, "country"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 121
    :cond_4
    sget-object v0, Lcom/peel/content/a;->h:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/peel/i/cg;->c:Landroid/os/Bundle;

    invoke-virtual {p0, v0}, Lcom/peel/i/cg;->c(Landroid/os/Bundle;)V

    .line 122
    :cond_5
    return-void

    .line 115
    :cond_6
    invoke-static {v0}, Lcom/peel/util/bx;->b(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    goto :goto_0
.end method

.method public e(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 702
    iget-object v0, p0, Lcom/peel/i/cg;->c:Landroid/os/Bundle;

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->putAll(Landroid/os/Bundle;)V

    .line 703
    invoke-super {p0, p1}, Lcom/peel/d/u;->e(Landroid/os/Bundle;)V

    .line 704
    return-void
.end method

.method public h(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 126
    invoke-super {p0, p1}, Lcom/peel/d/u;->h(Landroid/os/Bundle;)V

    .line 127
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 622
    sget-object v0, Lcom/peel/content/a;->h:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/peel/i/cg;->b:Lcom/peel/d/i;

    if-nez v0, :cond_1

    .line 657
    :cond_0
    :goto_0
    return-void

    .line 624
    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    .line 625
    sget v1, Lcom/peel/ui/fp;->skip_btn:I

    if-ne v0, v1, :cond_0

    .line 626
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/peel/i/cg;->a(Z)V

    goto :goto_0
.end method
