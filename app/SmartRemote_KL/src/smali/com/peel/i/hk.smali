.class Lcom/peel/i/hk;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field final synthetic a:Lcom/peel/i/gm;


# direct methods
.method constructor <init>(Lcom/peel/i/gm;)V
    .locals 0

    .prologue
    .line 1483
    iput-object p1, p0, Lcom/peel/i/hk;->a:Lcom/peel/i/gm;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 0

    .prologue
    .line 1499
    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 1502
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 3

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 1486
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-nez v0, :cond_1

    .line 1487
    iget-object v0, p0, Lcom/peel/i/hk;->a:Lcom/peel/i/gm;

    invoke-static {v0}, Lcom/peel/i/gm;->an(Lcom/peel/i/gm;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1488
    iget-object v0, p0, Lcom/peel/i/hk;->a:Lcom/peel/i/gm;

    invoke-static {v0}, Lcom/peel/i/gm;->F(Lcom/peel/i/gm;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1494
    :goto_0
    iget-object v0, p0, Lcom/peel/i/hk;->a:Lcom/peel/i/gm;

    invoke-static {v0}, Lcom/peel/i/gm;->ao(Lcom/peel/i/gm;)Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lcom/peel/i/a/a;

    .line 1495
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/peel/i/a/a;->getFilter()Landroid/widget/Filter;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/widget/Filter;->filter(Ljava/lang/CharSequence;)V

    .line 1496
    :cond_0
    return-void

    .line 1490
    :cond_1
    iget-object v0, p0, Lcom/peel/i/hk;->a:Lcom/peel/i/gm;

    invoke-static {v0}, Lcom/peel/i/gm;->an(Lcom/peel/i/gm;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1491
    iget-object v0, p0, Lcom/peel/i/hk;->a:Lcom/peel/i/gm;

    invoke-static {v0}, Lcom/peel/i/gm;->F(Lcom/peel/i/gm;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0
.end method
