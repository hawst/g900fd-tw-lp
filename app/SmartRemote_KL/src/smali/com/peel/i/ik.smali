.class final Lcom/peel/i/ik;
.super Landroid/text/style/ClickableSpan;


# instance fields
.field final synthetic a:Lcom/peel/i/gm;

.field private b:I


# direct methods
.method private constructor <init>(Lcom/peel/i/gm;)V
    .locals 0

    .prologue
    .line 2390
    iput-object p1, p0, Lcom/peel/i/ik;->a:Lcom/peel/i/gm;

    invoke-direct {p0}, Landroid/text/style/ClickableSpan;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/peel/i/gm;Lcom/peel/i/gn;)V
    .locals 0

    .prologue
    .line 2390
    invoke-direct {p0, p1}, Lcom/peel/i/ik;-><init>(Lcom/peel/i/gm;)V

    return-void
.end method

.method static synthetic a(Lcom/peel/i/ik;I)I
    .locals 0

    .prologue
    .line 2390
    iput p1, p0, Lcom/peel/i/ik;->b:I

    return p1
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v4, 0x5

    const/4 v3, 0x0

    .line 2396
    iget v0, p0, Lcom/peel/i/ik;->b:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 2397
    iget-object v0, p0, Lcom/peel/i/ik;->a:Lcom/peel/i/gm;

    invoke-static {v0}, Lcom/peel/i/gm;->R(Lcom/peel/i/gm;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/i/ik;->a:Lcom/peel/i/gm;

    invoke-static {v1}, Lcom/peel/i/gm;->R(Lcom/peel/i/gm;)Landroid/os/Handler;

    move-result-object v1

    const/16 v2, 0x9

    invoke-static {v1, v3, v2, v4}, Landroid/os/Message;->obtain(Landroid/os/Handler;III)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 2400
    :goto_0
    iget-object v0, p0, Lcom/peel/i/ik;->a:Lcom/peel/i/gm;

    invoke-static {v0}, Lcom/peel/i/gm;->T(Lcom/peel/i/gm;)Lcom/peel/widget/TestBtnViewPager;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/peel/widget/TestBtnViewPager;->setEnabledSwipe(Z)V

    .line 2401
    iget-object v0, p0, Lcom/peel/i/ik;->a:Lcom/peel/i/gm;

    invoke-static {v0}, Lcom/peel/i/gm;->ab(Lcom/peel/i/gm;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setEnabled(Z)V

    .line 2402
    iget-object v0, p0, Lcom/peel/i/ik;->a:Lcom/peel/i/gm;

    invoke-static {v0}, Lcom/peel/i/gm;->ac(Lcom/peel/i/gm;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setEnabled(Z)V

    .line 2403
    return-void

    .line 2399
    :cond_0
    iget-object v0, p0, Lcom/peel/i/ik;->a:Lcom/peel/i/gm;

    invoke-static {v0}, Lcom/peel/i/gm;->R(Lcom/peel/i/gm;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/i/ik;->a:Lcom/peel/i/gm;

    invoke-static {v1}, Lcom/peel/i/gm;->R(Lcom/peel/i/gm;)Landroid/os/Handler;

    move-result-object v1

    const/4 v2, 0x4

    invoke-static {v1, v3, v2, v4}, Landroid/os/Message;->obtain(Landroid/os/Handler;III)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method public updateDrawState(Landroid/text/TextPaint;)V
    .locals 1

    .prologue
    .line 2407
    const-string/jumbo v0, "#21dcda"

    invoke-static {v0}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/text/TextPaint;->setColor(I)V

    .line 2408
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/text/TextPaint;->setUnderlineText(Z)V

    .line 2409
    return-void
.end method
