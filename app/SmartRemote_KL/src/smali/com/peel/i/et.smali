.class Lcom/peel/i/et;
.super Lcom/peel/util/t;


# instance fields
.field final synthetic a:Landroid/os/Bundle;

.field final synthetic b:Ljava/lang/String;

.field final synthetic c:Lcom/peel/i/ep;


# direct methods
.method constructor <init>(Lcom/peel/i/ep;ILandroid/os/Bundle;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 244
    iput-object p1, p0, Lcom/peel/i/et;->c:Lcom/peel/i/ep;

    iput-object p3, p0, Lcom/peel/i/et;->a:Landroid/os/Bundle;

    iput-object p4, p0, Lcom/peel/i/et;->b:Ljava/lang/String;

    invoke-direct {p0, p2}, Lcom/peel/util/t;-><init>(I)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .prologue
    const/16 v4, 0x8

    const/4 v5, 0x0

    const/4 v3, 0x1

    const/4 v6, 0x0

    .line 247
    iget-boolean v0, p0, Lcom/peel/i/et;->i:Z

    if-nez v0, :cond_1

    .line 248
    invoke-static {}, Lcom/peel/i/ep;->T()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Downloader.offline :: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-boolean v2, Lcom/peel/util/b/a;->a:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 249
    iget-object v0, p0, Lcom/peel/i/et;->c:Lcom/peel/i/ep;

    invoke-virtual {v0}, Lcom/peel/i/ep;->S()V

    .line 250
    sget-boolean v0, Lcom/peel/util/b/a;->a:Z

    if-nez v0, :cond_0

    .line 251
    iget-object v0, p0, Lcom/peel/i/et;->c:Lcom/peel/i/ep;

    invoke-virtual {v0}, Lcom/peel/i/ep;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    sget v1, Lcom/peel/ui/ft;->unable_get_lineups:I

    invoke-static {v0, v1, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 253
    :cond_0
    iget-object v0, p0, Lcom/peel/i/et;->c:Lcom/peel/i/ep;

    invoke-static {v0}, Lcom/peel/i/ep;->b(Lcom/peel/i/ep;)Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/widget/ListView;->setVisibility(I)V

    .line 254
    iget-object v0, p0, Lcom/peel/i/et;->c:Lcom/peel/i/ep;

    invoke-static {v0}, Lcom/peel/i/ep;->c(Lcom/peel/i/ep;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 361
    :goto_0
    return-void

    .line 257
    :cond_1
    iget-object v0, p0, Lcom/peel/i/et;->c:Lcom/peel/i/ep;

    invoke-static {v0}, Lcom/peel/i/ep;->b(Lcom/peel/i/ep;)Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/widget/ListView;->setVisibility(I)V

    .line 258
    iget-object v0, p0, Lcom/peel/i/et;->c:Lcom/peel/i/ep;

    invoke-static {v0}, Lcom/peel/i/ep;->c(Lcom/peel/i/ep;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    .line 260
    iget-object v0, p0, Lcom/peel/i/et;->j:Ljava/lang/Object;

    check-cast v0, [Landroid/os/Bundle;

    move-object v4, v0

    check-cast v4, [Landroid/os/Bundle;

    .line 261
    const-string/jumbo v0, "US"

    iget-object v1, p0, Lcom/peel/i/et;->a:Landroid/os/Bundle;

    const-string/jumbo v2, "iso"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 262
    invoke-static {}, Ljava/text/Collator;->getInstance()Ljava/text/Collator;

    move-result-object v0

    .line 263
    invoke-virtual {v0, v3}, Ljava/text/Collator;->setDecomposition(I)V

    .line 266
    new-instance v1, Lcom/peel/i/eu;

    invoke-direct {v1, p0, v0}, Lcom/peel/i/eu;-><init>(Lcom/peel/i/et;Ljava/text/Collator;)V

    invoke-static {v4, v1}, Ljava/util/Arrays;->sort([Ljava/lang/Object;Ljava/util/Comparator;)V

    .line 274
    :cond_2
    iget-object v0, p0, Lcom/peel/i/et;->c:Lcom/peel/i/ep;

    invoke-static {v0}, Lcom/peel/i/ep;->b(Lcom/peel/i/ep;)Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ListView;->getFooterViewsCount()I

    move-result v0

    if-lez v0, :cond_3

    .line 275
    iget-object v0, p0, Lcom/peel/i/et;->c:Lcom/peel/i/ep;

    invoke-static {v0}, Lcom/peel/i/ep;->b(Lcom/peel/i/ep;)Landroid/widget/ListView;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/i/et;->c:Lcom/peel/i/ep;

    invoke-static {v1}, Lcom/peel/i/ep;->d(Lcom/peel/i/ep;)Landroid/widget/LinearLayout;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->removeFooterView(Landroid/view/View;)Z

    .line 278
    :cond_3
    iget-object v1, p0, Lcom/peel/i/et;->c:Lcom/peel/i/ep;

    iget-object v0, p0, Lcom/peel/i/et;->c:Lcom/peel/i/ep;

    invoke-static {v0}, Lcom/peel/i/ep;->e(Lcom/peel/i/ep;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v2, Lcom/peel/ui/fq;->report_missing_service_provider_footer:I

    invoke-virtual {v0, v2, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    invoke-static {v1, v0}, Lcom/peel/i/ep;->a(Lcom/peel/i/ep;Landroid/widget/LinearLayout;)Landroid/widget/LinearLayout;

    .line 279
    iget-object v0, p0, Lcom/peel/i/et;->c:Lcom/peel/i/ep;

    invoke-static {v0}, Lcom/peel/i/ep;->d(Lcom/peel/i/ep;)Landroid/widget/LinearLayout;

    move-result-object v0

    sget v1, Lcom/peel/ui/fp;->report:I

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/peel/i/ev;

    invoke-direct {v1, p0}, Lcom/peel/i/ev;-><init>(Lcom/peel/i/et;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 314
    iget-object v0, p0, Lcom/peel/i/et;->c:Lcom/peel/i/ep;

    invoke-static {v0}, Lcom/peel/i/ep;->b(Lcom/peel/i/ep;)Landroid/widget/ListView;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/i/et;->c:Lcom/peel/i/ep;

    invoke-static {v1}, Lcom/peel/i/ep;->d(Lcom/peel/i/ep;)Landroid/widget/LinearLayout;

    move-result-object v1

    invoke-virtual {v0, v1, v5, v6}, Landroid/widget/ListView;->addFooterView(Landroid/view/View;Ljava/lang/Object;Z)V

    .line 316
    iget-object v0, p0, Lcom/peel/i/et;->c:Lcom/peel/i/ep;

    invoke-virtual {v0}, Lcom/peel/i/ep;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/peel/i/et;->c:Lcom/peel/i/ep;

    invoke-virtual {v0}, Lcom/peel/i/ep;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/ae;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v5

    .line 317
    :cond_4
    iget-object v0, p0, Lcom/peel/i/et;->c:Lcom/peel/i/ep;

    invoke-static {v0}, Lcom/peel/i/ep;->b(Lcom/peel/i/ep;)Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/ListView;->setItemsCanFocus(Z)V

    .line 318
    new-instance v0, Lcom/peel/i/ey;

    iget-object v1, p0, Lcom/peel/i/et;->c:Lcom/peel/i/ep;

    invoke-virtual {v1}, Lcom/peel/i/ep;->m()Landroid/support/v4/app/ae;

    move-result-object v2

    sget v3, Lcom/peel/ui/fq;->provider_row:I

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/peel/i/ey;-><init>(Lcom/peel/i/et;Landroid/content/Context;I[Landroid/os/Bundle;Ljava/lang/String;)V

    .line 334
    iget-object v1, p0, Lcom/peel/i/et;->c:Lcom/peel/i/ep;

    invoke-static {v1}, Lcom/peel/i/ep;->b(Lcom/peel/i/ep;)Landroid/widget/ListView;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 336
    iget-object v1, p0, Lcom/peel/i/et;->c:Lcom/peel/i/ep;

    invoke-static {v1}, Lcom/peel/i/ep;->b(Lcom/peel/i/ep;)Landroid/widget/ListView;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/widget/ListView;->setVisibility(I)V

    .line 337
    iget-object v1, p0, Lcom/peel/i/et;->c:Lcom/peel/i/ep;

    invoke-static {v1}, Lcom/peel/i/ep;->c(Lcom/peel/i/ep;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/view/View;->setVisibility(I)V

    .line 338
    iget-object v1, p0, Lcom/peel/i/et;->c:Lcom/peel/i/ep;

    invoke-static {v1}, Lcom/peel/i/ep;->h(Lcom/peel/i/ep;)Landroid/widget/TextView;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 339
    iget-object v1, p0, Lcom/peel/i/et;->c:Lcom/peel/i/ep;

    invoke-static {v1}, Lcom/peel/i/ep;->b(Lcom/peel/i/ep;)Landroid/widget/ListView;

    move-result-object v1

    new-instance v2, Lcom/peel/i/ez;

    invoke-direct {v2, p0, v0}, Lcom/peel/i/ez;-><init>(Lcom/peel/i/et;Landroid/widget/ArrayAdapter;)V

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 360
    iget-object v0, p0, Lcom/peel/i/et;->c:Lcom/peel/i/ep;

    invoke-virtual {v0}, Lcom/peel/i/ep;->S()V

    goto/16 :goto_0
.end method
