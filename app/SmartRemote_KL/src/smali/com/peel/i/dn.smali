.class Lcom/peel/i/dn;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/view/View$OnTouchListener;


# instance fields
.field final synthetic a:Lcom/peel/i/dm;


# direct methods
.method constructor <init>(Lcom/peel/i/dm;)V
    .locals 0

    .prologue
    .line 526
    iput-object p1, p0, Lcom/peel/i/dn;->a:Lcom/peel/i/dm;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 6

    .prologue
    .line 529
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_0

    .line 530
    iget-object v0, p0, Lcom/peel/i/dn;->a:Lcom/peel/i/dm;

    iget-object v0, v0, Lcom/peel/i/dm;->c:Lcom/peel/i/cr;

    iget-object v0, v0, Lcom/peel/i/cr;->f:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 532
    iget-object v0, p0, Lcom/peel/i/dn;->a:Lcom/peel/i/dm;

    iget-object v0, v0, Lcom/peel/i/dm;->c:Lcom/peel/i/cr;

    new-instance v1, Landroid/graphics/Rect;

    invoke-virtual {p1}, Landroid/view/View;->getLeft()I

    move-result v2

    invoke-virtual {p1}, Landroid/view/View;->getTop()I

    move-result v3

    invoke-virtual {p1}, Landroid/view/View;->getRight()I

    move-result v4

    invoke-virtual {p1}, Landroid/view/View;->getBottom()I

    move-result v5

    invoke-direct {v1, v2, v3, v4, v5}, Landroid/graphics/Rect;-><init>(IIII)V

    invoke-static {v0, v1}, Lcom/peel/i/cr;->a(Lcom/peel/i/cr;Landroid/graphics/Rect;)Landroid/graphics/Rect;

    .line 535
    :cond_0
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    .line 536
    iget-object v0, p0, Lcom/peel/i/dn;->a:Lcom/peel/i/dm;

    iget-object v0, v0, Lcom/peel/i/dm;->c:Lcom/peel/i/cr;

    invoke-static {v0}, Lcom/peel/i/cr;->n(Lcom/peel/i/cr;)Landroid/graphics/Rect;

    move-result-object v0

    invoke-virtual {p1}, Landroid/view/View;->getLeft()I

    move-result v1

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    float-to-int v2, v2

    add-int/2addr v1, v2

    invoke-virtual {p1}, Landroid/view/View;->getTop()I

    move-result v2

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    float-to-int v3, v3

    add-int/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Rect;->contains(II)Z

    move-result v0

    if-nez v0, :cond_1

    .line 537
    iget-object v0, p0, Lcom/peel/i/dn;->a:Lcom/peel/i/dm;

    iget-object v0, v0, Lcom/peel/i/dm;->c:Lcom/peel/i/cr;

    invoke-static {v0}, Lcom/peel/i/cr;->m(Lcom/peel/i/cr;)Lcom/peel/util/CustomSpinner;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/util/CustomSpinner;->onDetachedFromWindow()V

    .line 540
    :cond_1
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    .line 542
    iget-object v0, p0, Lcom/peel/i/dn;->a:Lcom/peel/i/dm;

    iget-object v0, v0, Lcom/peel/i/dm;->c:Lcom/peel/i/cr;

    iget-object v0, v0, Lcom/peel/i/cr;->f:Landroid/widget/TextView;

    sget v1, Lcom/peel/ui/fo;->region_list_stateful:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setBackgroundResource(I)V

    .line 544
    :cond_2
    const/4 v0, 0x0

    return v0
.end method
