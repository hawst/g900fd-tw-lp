.class Lcom/peel/i/iw;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/peel/i/iv;


# direct methods
.method constructor <init>(Lcom/peel/i/iv;)V
    .locals 0

    .prologue
    .line 95
    iput-object p1, p0, Lcom/peel/i/iw;->a:Lcom/peel/i/iv;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    const/4 v1, 0x1

    .line 99
    invoke-static {}, Lcom/peel/util/a/f;->a()Lcom/peel/util/a/f;

    move-result-object v2

    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v0}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v0

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    const/16 v3, 0x4bd

    iget-object v4, p0, Lcom/peel/i/iw;->a:Lcom/peel/i/iv;

    iget-object v4, v4, Lcom/peel/i/iv;->a:Lcom/peel/i/iu;

    iget-object v4, v4, Lcom/peel/i/iu;->b:Lcom/peel/i/it;

    iget-object v4, v4, Lcom/peel/i/it;->a:Lcom/peel/i/is;

    .line 100
    invoke-static {v4}, Lcom/peel/i/is;->c(Lcom/peel/i/is;)I

    move-result v4

    .line 99
    invoke-virtual {v2, v0, v3, v4}, Lcom/peel/util/a/f;->a(III)V

    .line 102
    iget-object v0, p0, Lcom/peel/i/iw;->a:Lcom/peel/i/iv;

    iget-object v0, v0, Lcom/peel/i/iv;->a:Lcom/peel/i/iu;

    iget-object v0, v0, Lcom/peel/i/iu;->b:Lcom/peel/i/it;

    iget-object v0, v0, Lcom/peel/i/it;->a:Lcom/peel/i/is;

    iget-object v0, v0, Lcom/peel/i/is;->c:Landroid/os/Bundle;

    const-string/jumbo v2, "goback_clazz"

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 103
    iget-object v0, p0, Lcom/peel/i/iw;->a:Lcom/peel/i/iv;

    iget-object v0, v0, Lcom/peel/i/iv;->a:Lcom/peel/i/iu;

    iget-object v0, v0, Lcom/peel/i/iu;->b:Lcom/peel/i/it;

    iget-object v0, v0, Lcom/peel/i/it;->a:Lcom/peel/i/is;

    iget-object v0, v0, Lcom/peel/i/is;->c:Landroid/os/Bundle;

    const-string/jumbo v3, "passback_bundle"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    new-instance v0, Landroid/os/Bundle;

    iget-object v3, p0, Lcom/peel/i/iw;->a:Lcom/peel/i/iv;

    iget-object v3, v3, Lcom/peel/i/iv;->a:Lcom/peel/i/iu;

    iget-object v3, v3, Lcom/peel/i/iu;->b:Lcom/peel/i/it;

    iget-object v3, v3, Lcom/peel/i/it;->a:Lcom/peel/i/is;

    iget-object v3, v3, Lcom/peel/i/is;->c:Landroid/os/Bundle;

    const-string/jumbo v4, "passback_bundle"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v3

    invoke-direct {v0, v3}, Landroid/os/Bundle;-><init>(Landroid/os/Bundle;)V

    .line 105
    :goto_1
    iget-object v3, p0, Lcom/peel/i/iw;->a:Lcom/peel/i/iv;

    iget-object v3, v3, Lcom/peel/i/iv;->a:Lcom/peel/i/iu;

    iget-object v3, v3, Lcom/peel/i/iu;->b:Lcom/peel/i/it;

    iget-object v3, v3, Lcom/peel/i/it;->a:Lcom/peel/i/is;

    invoke-virtual {v3}, Lcom/peel/i/is;->k()Landroid/support/v4/app/Fragment;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 106
    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3}, Landroid/content/Intent;-><init>()V

    .line 107
    const-string/jumbo v4, "loggedin"

    invoke-virtual {v3, v4, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 108
    iget-object v1, p0, Lcom/peel/i/iw;->a:Lcom/peel/i/iv;

    iget-object v1, v1, Lcom/peel/i/iv;->a:Lcom/peel/i/iu;

    iget-object v1, v1, Lcom/peel/i/iu;->b:Lcom/peel/i/it;

    iget-object v1, v1, Lcom/peel/i/it;->a:Lcom/peel/i/is;

    invoke-virtual {v1}, Lcom/peel/i/is;->k()Landroid/support/v4/app/Fragment;

    move-result-object v1

    iget-object v4, p0, Lcom/peel/i/iw;->a:Lcom/peel/i/iv;

    iget-object v4, v4, Lcom/peel/i/iv;->a:Lcom/peel/i/iu;

    iget-object v4, v4, Lcom/peel/i/iu;->b:Lcom/peel/i/it;

    iget-object v4, v4, Lcom/peel/i/it;->a:Lcom/peel/i/is;

    invoke-virtual {v4}, Lcom/peel/i/is;->l()I

    move-result v4

    const/4 v5, -0x1

    invoke-virtual {v1, v4, v5, v3}, Landroid/support/v4/app/Fragment;->a(IILandroid/content/Intent;)V

    .line 111
    :cond_0
    if-nez v2, :cond_3

    .line 112
    invoke-static {}, Lcom/peel/i/is;->c()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/i/iw;->a:Lcom/peel/i/iv;

    iget-object v1, v1, Lcom/peel/i/iv;->a:Lcom/peel/i/iu;

    iget-object v1, v1, Lcom/peel/i/iu;->b:Lcom/peel/i/it;

    iget-object v1, v1, Lcom/peel/i/it;->a:Lcom/peel/i/is;

    invoke-virtual {v1}, Lcom/peel/i/is;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/peel/d/e;->a(Ljava/lang/String;Landroid/support/v4/app/ae;)V

    .line 116
    :goto_2
    return-void

    .line 99
    :cond_1
    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v0}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/data/at;->f()I

    move-result v0

    goto/16 :goto_0

    .line 103
    :cond_2
    iget-object v0, p0, Lcom/peel/i/iw;->a:Lcom/peel/i/iv;

    iget-object v0, v0, Lcom/peel/i/iv;->a:Lcom/peel/i/iu;

    iget-object v0, v0, Lcom/peel/i/iu;->b:Lcom/peel/i/it;

    iget-object v0, v0, Lcom/peel/i/it;->a:Lcom/peel/i/is;

    iget-object v0, v0, Lcom/peel/i/is;->c:Landroid/os/Bundle;

    goto :goto_1

    .line 114
    :cond_3
    iget-object v1, p0, Lcom/peel/i/iw;->a:Lcom/peel/i/iv;

    iget-object v1, v1, Lcom/peel/i/iv;->a:Lcom/peel/i/iu;

    iget-object v1, v1, Lcom/peel/i/iu;->b:Lcom/peel/i/it;

    iget-object v1, v1, Lcom/peel/i/it;->a:Lcom/peel/i/is;

    invoke-virtual {v1}, Lcom/peel/i/is;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    invoke-static {v1, v2, v0}, Lcom/peel/d/e;->b(Landroid/support/v4/app/ae;Ljava/lang/String;Landroid/os/Bundle;)V

    goto :goto_2
.end method
