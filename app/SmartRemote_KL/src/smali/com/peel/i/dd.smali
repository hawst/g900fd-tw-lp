.class Lcom/peel/i/dd;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Landroid/os/Bundle;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Ljava/text/Collator;

.field final synthetic b:Lcom/peel/i/dc;


# direct methods
.method constructor <init>(Lcom/peel/i/dc;Ljava/text/Collator;)V
    .locals 0

    .prologue
    .line 332
    iput-object p1, p0, Lcom/peel/i/dd;->b:Lcom/peel/i/dc;

    iput-object p2, p0, Lcom/peel/i/dd;->a:Ljava/text/Collator;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/os/Bundle;Landroid/os/Bundle;)I
    .locals 3

    .prologue
    .line 335
    iget-object v0, p0, Lcom/peel/i/dd;->a:Ljava/text/Collator;

    const-string/jumbo v1, "mso"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "mso"

    invoke-virtual {p2, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/text/Collator;->compare(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 332
    check-cast p1, Landroid/os/Bundle;

    check-cast p2, Landroid/os/Bundle;

    invoke-virtual {p0, p1, p2}, Lcom/peel/i/dd;->a(Landroid/os/Bundle;Landroid/os/Bundle;)I

    move-result v0

    return v0
.end method
