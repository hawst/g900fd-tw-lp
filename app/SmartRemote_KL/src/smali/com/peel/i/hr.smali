.class Lcom/peel/i/hr;
.super Lcom/peel/util/t;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/peel/util/t",
        "<",
        "Ljava/util/Map",
        "<",
        "Ljava/lang/String;",
        "Ljava/util/Map",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Object;",
        ">;>;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/peel/i/gm;


# direct methods
.method constructor <init>(Lcom/peel/i/gm;I)V
    .locals 0

    .prologue
    .line 1578
    iput-object p1, p0, Lcom/peel/i/hr;->a:Lcom/peel/i/gm;

    invoke-direct {p0, p2}, Lcom/peel/util/t;-><init>(I)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 11

    .prologue
    const/4 v10, 0x1

    const/4 v0, 0x0

    const/4 v4, 0x0

    .line 1582
    invoke-static {}, Lcom/peel/i/gm;->c()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/peel/i/hr;->a:Lcom/peel/i/gm;

    invoke-virtual {v2}, Lcom/peel/i/gm;->m()Landroid/support/v4/app/ae;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lcom/peel/util/bx;->a(Ljava/lang/String;Landroid/app/Activity;Z)V

    .line 1583
    sget-boolean v1, Lcom/peel/util/b/a;->a:Z

    if-eqz v1, :cond_1

    .line 1620
    :cond_0
    :goto_0
    return-void

    .line 1586
    :cond_1
    iget-boolean v1, p0, Lcom/peel/i/hr;->i:Z

    if-nez v1, :cond_2

    .line 1587
    invoke-static {}, Lcom/peel/i/gm;->c()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "unable to get codes by codeset"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1588
    iget-object v0, p0, Lcom/peel/i/hr;->a:Lcom/peel/i/gm;

    invoke-virtual {v0}, Lcom/peel/i/gm;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/i/hr;->a:Lcom/peel/i/gm;

    iget-object v1, v1, Lcom/peel/i/gm;->b:Lcom/peel/d/i;

    iget-object v2, p0, Lcom/peel/i/hr;->a:Lcom/peel/i/gm;

    invoke-static {v2}, Lcom/peel/i/gm;->s(Lcom/peel/i/gm;)Lcom/peel/f/a;

    move-result-object v2

    invoke-virtual {v2}, Lcom/peel/f/a;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2, v4, v4}, Lcom/peel/util/bx;->a(Landroid/content/Context;Lcom/peel/d/i;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1592
    :cond_2
    iget-object v1, p0, Lcom/peel/i/hr;->j:Ljava/lang/Object;

    check-cast v1, Ljava/util/Map;

    .line 1593
    const-string/jumbo v2, "Play"

    invoke-interface {v1, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1595
    iget-object v9, p0, Lcom/peel/i/hr;->a:Lcom/peel/i/gm;

    const/16 v1, 0x14

    iget-object v2, p0, Lcom/peel/i/hr;->a:Lcom/peel/i/gm;

    invoke-static {v2}, Lcom/peel/i/gm;->s(Lcom/peel/i/gm;)Lcom/peel/f/a;

    move-result-object v2

    invoke-virtual {v2}, Lcom/peel/f/a;->b()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/peel/i/hr;->a:Lcom/peel/i/gm;

    invoke-static {v3}, Lcom/peel/i/gm;->w(Lcom/peel/i/gm;)Lcom/peel/control/h;

    move-result-object v3

    invoke-virtual {v3}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v3

    invoke-virtual {v3}, Lcom/peel/data/g;->g()Z

    move-result v3

    const/4 v5, -0x1

    move-object v6, v4

    move-object v7, v4

    move-object v8, v4

    invoke-static/range {v0 .. v8}, Lcom/peel/control/h;->a(IILjava/lang/String;ZLjava/lang/String;ILandroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;)Lcom/peel/control/h;

    move-result-object v1

    invoke-static {v9, v1}, Lcom/peel/i/gm;->b(Lcom/peel/i/gm;Lcom/peel/control/h;)Lcom/peel/control/h;

    .line 1597
    :cond_3
    iget-object v1, p0, Lcom/peel/i/hr;->a:Lcom/peel/i/gm;

    invoke-static {v1}, Lcom/peel/i/gm;->w(Lcom/peel/i/gm;)Lcom/peel/control/h;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v2

    iget-object v1, p0, Lcom/peel/i/hr;->a:Lcom/peel/i/gm;

    invoke-static {v1}, Lcom/peel/i/gm;->V(Lcom/peel/i/gm;)I

    move-result v3

    iget-object v1, p0, Lcom/peel/i/hr;->j:Ljava/lang/Object;

    check-cast v1, Ljava/util/Map;

    invoke-virtual {v2, v3, v1}, Lcom/peel/data/g;->a(ILjava/util/Map;)V

    .line 1598
    iget-object v1, p0, Lcom/peel/i/hr;->a:Lcom/peel/i/gm;

    invoke-static {v1}, Lcom/peel/i/gm;->w(Lcom/peel/i/gm;)Lcom/peel/control/h;

    move-result-object v1

    invoke-virtual {v1, v10}, Lcom/peel/control/h;->a(I)V

    .line 1603
    iget-object v1, p0, Lcom/peel/i/hr;->a:Lcom/peel/i/gm;

    invoke-static {v1}, Lcom/peel/i/gm;->e(Lcom/peel/i/gm;)Lcom/peel/control/a;

    move-result-object v1

    iget-object v2, p0, Lcom/peel/i/hr;->a:Lcom/peel/i/gm;

    invoke-static {v2}, Lcom/peel/i/gm;->w(Lcom/peel/i/gm;)Lcom/peel/control/h;

    move-result-object v2

    new-array v3, v10, [Ljava/lang/Integer;

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v0

    invoke-virtual {v1, v2, v4, v3}, Lcom/peel/control/a;->a(Lcom/peel/control/h;Ljava/lang/String;[Ljava/lang/Integer;)Z

    .line 1605
    iget-object v1, p0, Lcom/peel/i/hr;->a:Lcom/peel/i/gm;

    invoke-static {v1}, Lcom/peel/i/gm;->e(Lcom/peel/i/gm;)Lcom/peel/control/a;

    move-result-object v2

    iget-object v1, p0, Lcom/peel/i/hr;->a:Lcom/peel/i/gm;

    invoke-static {v1}, Lcom/peel/i/gm;->c(Lcom/peel/i/gm;)Lcom/peel/control/h;

    move-result-object v3

    iget-object v1, p0, Lcom/peel/i/hr;->a:Lcom/peel/i/gm;

    invoke-static {v1}, Lcom/peel/i/gm;->c(Lcom/peel/i/gm;)Lcom/peel/control/h;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/data/g;->d()I

    move-result v1

    const/16 v5, 0xa

    if-ne v1, v5, :cond_6

    move-object v1, v4

    :goto_1
    invoke-virtual {v2, v3, v4, v1}, Lcom/peel/control/a;->a(Lcom/peel/control/h;Ljava/lang/String;[Ljava/lang/Integer;)Z

    .line 1606
    iget-object v1, p0, Lcom/peel/i/hr;->a:Lcom/peel/i/gm;

    invoke-static {v1}, Lcom/peel/i/gm;->R(Lcom/peel/i/gm;)Landroid/os/Handler;

    move-result-object v1

    iget-object v2, p0, Lcom/peel/i/hr;->a:Lcom/peel/i/gm;

    invoke-static {v2}, Lcom/peel/i/gm;->R(Lcom/peel/i/gm;)Landroid/os/Handler;

    move-result-object v2

    const/16 v3, 0x9

    const/16 v5, 0x63

    invoke-static {v2, v0, v3, v5}, Landroid/os/Message;->obtain(Landroid/os/Handler;III)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 1608
    iget-object v1, p0, Lcom/peel/i/hr;->a:Lcom/peel/i/gm;

    invoke-static {v1}, Lcom/peel/i/gm;->b(Lcom/peel/i/gm;)Lcom/peel/control/RoomControl;

    move-result-object v1

    iget-object v2, p0, Lcom/peel/i/hr;->a:Lcom/peel/i/gm;

    invoke-static {v2}, Lcom/peel/i/gm;->e(Lcom/peel/i/gm;)Lcom/peel/control/a;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Lcom/peel/control/RoomControl;->a(Lcom/peel/control/a;I)Z

    .line 1610
    iget-object v1, p0, Lcom/peel/i/hr;->a:Lcom/peel/i/gm;

    invoke-virtual {v1}, Lcom/peel/i/gm;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/ae;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/peel/social/w;->f(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 1611
    new-instance v1, Lcom/peel/backup/c;

    iget-object v2, p0, Lcom/peel/i/hr;->a:Lcom/peel/i/gm;

    invoke-virtual {v2}, Lcom/peel/i/gm;->m()Landroid/support/v4/app/ae;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/ae;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/peel/backup/c;-><init>(Landroid/content/Context;)V

    .line 1612
    iget-object v2, p0, Lcom/peel/i/hr;->a:Lcom/peel/i/gm;

    invoke-static {v2}, Lcom/peel/i/gm;->d(Lcom/peel/i/gm;)Lcom/peel/content/library/LiveLibrary;

    move-result-object v2

    instance-of v2, v2, Lcom/peel/content/library/LiveLibrary;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/peel/i/hr;->a:Lcom/peel/i/gm;

    invoke-static {v2}, Lcom/peel/i/gm;->d(Lcom/peel/i/gm;)Lcom/peel/content/library/LiveLibrary;

    move-result-object v2

    invoke-virtual {v2}, Lcom/peel/content/library/LiveLibrary;->d()Ljava/lang/String;

    move-result-object v4

    .line 1613
    :cond_4
    iget-object v2, p0, Lcom/peel/i/hr;->a:Lcom/peel/i/gm;

    invoke-static {v2}, Lcom/peel/i/gm;->b(Lcom/peel/i/gm;)Lcom/peel/control/RoomControl;

    move-result-object v2

    iget-object v3, p0, Lcom/peel/i/hr;->a:Lcom/peel/i/gm;

    invoke-static {v3}, Lcom/peel/i/gm;->e(Lcom/peel/i/gm;)Lcom/peel/control/a;

    move-result-object v3

    iget-object v5, p0, Lcom/peel/i/hr;->a:Lcom/peel/i/gm;

    invoke-static {v5}, Lcom/peel/i/gm;->c(Lcom/peel/i/gm;)Lcom/peel/control/h;

    move-result-object v5

    invoke-virtual {v1, v2, v4, v3, v5}, Lcom/peel/backup/c;->a(Lcom/peel/control/RoomControl;Ljava/lang/String;Lcom/peel/control/a;Lcom/peel/control/h;)V

    .line 1614
    iget-object v2, p0, Lcom/peel/i/hr;->a:Lcom/peel/i/gm;

    invoke-static {v2}, Lcom/peel/i/gm;->b(Lcom/peel/i/gm;)Lcom/peel/control/RoomControl;

    move-result-object v2

    iget-object v3, p0, Lcom/peel/i/hr;->a:Lcom/peel/i/gm;

    invoke-static {v3}, Lcom/peel/i/gm;->e(Lcom/peel/i/gm;)Lcom/peel/control/a;

    move-result-object v3

    iget-object v5, p0, Lcom/peel/i/hr;->a:Lcom/peel/i/gm;

    invoke-static {v5}, Lcom/peel/i/gm;->w(Lcom/peel/i/gm;)Lcom/peel/control/h;

    move-result-object v5

    invoke-virtual {v1, v2, v4, v3, v5}, Lcom/peel/backup/c;->a(Lcom/peel/control/RoomControl;Ljava/lang/String;Lcom/peel/control/a;Lcom/peel/control/h;)V

    .line 1617
    :cond_5
    iget-object v1, p0, Lcom/peel/i/hr;->a:Lcom/peel/i/gm;

    iget-object v1, v1, Lcom/peel/i/gm;->c:Landroid/os/Bundle;

    const-string/jumbo v2, "from_tutorial"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1618
    const-string/jumbo v0, "remoteSetupCompletion"

    iget-object v1, p0, Lcom/peel/i/hr;->a:Lcom/peel/i/gm;

    invoke-virtual {v1}, Lcom/peel/i/gm;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/peel/util/a;->a(Ljava/lang/String;Landroid/content/Context;)V

    goto/16 :goto_0

    .line 1605
    :cond_6
    new-array v1, v10, [Ljava/lang/Integer;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v1, v0

    goto/16 :goto_1
.end method
