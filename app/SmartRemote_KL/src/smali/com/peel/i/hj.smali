.class Lcom/peel/i/hj;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field final synthetic a:Lcom/peel/i/gm;


# direct methods
.method constructor <init>(Lcom/peel/i/gm;)V
    .locals 0

    .prologue
    .line 1441
    iput-object p1, p0, Lcom/peel/i/hj;->a:Lcom/peel/i/gm;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 1444
    iget-object v0, p0, Lcom/peel/i/hj;->a:Lcom/peel/i/gm;

    invoke-static {v0, v1}, Lcom/peel/i/gm;->d(Lcom/peel/i/gm;Z)Z

    .line 1446
    iget-object v0, p0, Lcom/peel/i/hj;->a:Lcom/peel/i/gm;

    invoke-static {v0}, Lcom/peel/i/gm;->aj(Lcom/peel/i/gm;)Landroid/widget/ListView;

    move-result-object v0

    invoke-static {}, Lcom/peel/i/gm;->c()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/peel/util/bx;->a(Landroid/view/View;Ljava/lang/String;)V

    .line 1447
    iget-object v0, p0, Lcom/peel/i/hj;->a:Lcom/peel/i/gm;

    invoke-static {v0}, Lcom/peel/i/gm;->ai(Lcom/peel/i/gm;)Lcom/peel/i/a/a;

    move-result-object v0

    invoke-virtual {v0, p3}, Lcom/peel/i/a/a;->b(I)V

    .line 1449
    iget-object v3, p0, Lcom/peel/i/hj;->a:Lcom/peel/i/gm;

    iget-object v0, p0, Lcom/peel/i/hj;->a:Lcom/peel/i/gm;

    invoke-static {v0}, Lcom/peel/i/gm;->aj(Lcom/peel/i/gm;)Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ListView;->getCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-ne p3, v0, :cond_2

    move v0, v1

    :goto_0
    invoke-static {v3, v0}, Lcom/peel/i/gm;->e(Lcom/peel/i/gm;Z)Z

    .line 1450
    iget-object v0, p0, Lcom/peel/i/hj;->a:Lcom/peel/i/gm;

    iget-object v3, p0, Lcom/peel/i/hj;->a:Lcom/peel/i/gm;

    invoke-static {v3}, Lcom/peel/i/gm;->aj(Lcom/peel/i/gm;)Landroid/widget/ListView;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/ListView;->getCount()I

    move-result v3

    add-int/lit8 v3, v3, -0x2

    if-ne p3, v3, :cond_0

    move v2, v1

    :cond_0
    invoke-static {v0, v2}, Lcom/peel/i/gm;->f(Lcom/peel/i/gm;Z)Z

    .line 1451
    iget-object v2, p0, Lcom/peel/i/hj;->a:Lcom/peel/i/gm;

    iget-object v0, p0, Lcom/peel/i/hj;->a:Lcom/peel/i/gm;

    invoke-static {v0}, Lcom/peel/i/gm;->ak(Lcom/peel/i/gm;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/peel/i/hj;->a:Lcom/peel/i/gm;

    invoke-static {v0}, Lcom/peel/i/gm;->al(Lcom/peel/i/gm;)Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_1
    const/4 v0, 0x0

    :goto_1
    invoke-static {v2, v0}, Lcom/peel/i/gm;->b(Lcom/peel/i/gm;Lcom/peel/f/a;)Lcom/peel/f/a;

    .line 1452
    iget-object v0, p0, Lcom/peel/i/hj;->a:Lcom/peel/i/gm;

    iget-object v2, p0, Lcom/peel/i/hj;->a:Lcom/peel/i/gm;

    sget v3, Lcom/peel/ui/ft;->action_bar_title_stb:I

    invoke-virtual {v2, v3}, Lcom/peel/i/gm;->a(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/peel/i/hj;->a:Lcom/peel/i/gm;

    invoke-static {v3}, Lcom/peel/i/gm;->am(Lcom/peel/i/gm;)Z

    move-result v3

    invoke-static {v0, v2, v1, v3}, Lcom/peel/i/gm;->a(Lcom/peel/i/gm;Ljava/lang/String;ZZ)V

    .line 1453
    return-void

    :cond_2
    move v0, v2

    .line 1449
    goto :goto_0

    .line 1451
    :cond_3
    invoke-virtual {p1}, Landroid/widget/AdapterView;->getAdapter()Landroid/widget/Adapter;

    move-result-object v0

    invoke-interface {v0, p3}, Landroid/widget/Adapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/f/a;

    goto :goto_1
.end method
