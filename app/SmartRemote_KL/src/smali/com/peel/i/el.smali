.class Lcom/peel/i/el;
.super Landroid/widget/ArrayAdapter;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Landroid/os/Bundle;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic b:Lcom/peel/i/eh;


# direct methods
.method constructor <init>(Lcom/peel/i/eh;Landroid/content/Context;I[Landroid/os/Bundle;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 357
    iput-object p1, p0, Lcom/peel/i/el;->b:Lcom/peel/i/eh;

    iput-object p5, p0, Lcom/peel/i/el;->a:Ljava/lang/String;

    invoke-direct {p0, p2, p3, p4}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 5

    .prologue
    .line 360
    if-eqz p2, :cond_0

    .line 361
    :goto_0
    invoke-virtual {p0, p1}, Lcom/peel/i/el;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    .line 363
    sget v1, Lcom/peel/ui/fp;->name:I

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    const-string/jumbo v2, "mso"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/peel/i/el;->a:Ljava/lang/String;

    iget-object v4, p0, Lcom/peel/i/el;->b:Lcom/peel/i/eh;

    iget-object v4, v4, Lcom/peel/i/eh;->d:Lcom/peel/i/dx;

    invoke-virtual {v4}, Lcom/peel/i/dx;->n()Landroid/content/res/Resources;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/peel/util/bx;->a(Ljava/lang/String;Ljava/lang/String;Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 364
    sget v1, Lcom/peel/ui/fp;->alt_name:I

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    const-string/jumbo v1, "name"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v3, "mso"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/16 v1, 0x8

    :goto_1
    invoke-virtual {v2, v1}, Landroid/view/View;->setVisibility(I)V

    .line 365
    sget v1, Lcom/peel/ui/fp;->alt_name:I

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    const-string/jumbo v2, "name"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 367
    return-object p2

    .line 360
    :cond_0
    iget-object v0, p0, Lcom/peel/i/el;->b:Lcom/peel/i/eh;

    iget-object v0, v0, Lcom/peel/i/eh;->d:Lcom/peel/i/dx;

    invoke-static {v0}, Lcom/peel/i/dx;->e(Lcom/peel/i/dx;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lcom/peel/ui/fq;->provider_row:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    goto :goto_0

    .line 364
    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method
