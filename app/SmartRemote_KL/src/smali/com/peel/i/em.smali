.class Lcom/peel/i/em;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field final synthetic a:Lcom/peel/i/eh;


# direct methods
.method constructor <init>(Lcom/peel/i/eh;)V
    .locals 0

    .prologue
    .line 373
    iput-object p1, p0, Lcom/peel/i/em;->a:Lcom/peel/i/eh;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 376
    sget-boolean v0, Lcom/peel/util/b/a;->a:Z

    if-eqz v0, :cond_0

    .line 390
    :goto_0
    return-void

    .line 380
    :cond_0
    iget-object v0, p0, Lcom/peel/i/em;->a:Lcom/peel/i/eh;

    iget-object v0, v0, Lcom/peel/i/eh;->d:Lcom/peel/i/dx;

    invoke-static {v0}, Lcom/peel/i/dx;->h(Lcom/peel/i/dx;)Landroid/widget/ListView;

    move-result-object v0

    invoke-static {}, Lcom/peel/i/dx;->T()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/peel/util/bx;->a(Landroid/view/View;Ljava/lang/String;)V

    .line 382
    invoke-virtual {p1}, Landroid/widget/AdapterView;->getAdapter()Landroid/widget/Adapter;

    move-result-object v0

    invoke-interface {v0, p3}, Landroid/widget/Adapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Landroid/os/Bundle;

    .line 384
    invoke-static {}, Lcom/peel/util/a/f;->a()Lcom/peel/util/a/f;

    move-result-object v0

    sget-object v1, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v1}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v1

    if-nez v1, :cond_1

    const/4 v1, 0x1

    :goto_1
    const/16 v2, 0xbc6

    const/16 v3, 0x7d5

    const-string/jumbo v4, "mso"

    invoke-virtual {v8, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iget-object v6, p0, Lcom/peel/i/em;->a:Lcom/peel/i/eh;

    iget-object v6, v6, Lcom/peel/i/eh;->c:Ljava/lang/String;

    if-eqz v6, :cond_2

    iget-object v6, p0, Lcom/peel/i/em;->a:Lcom/peel/i/eh;

    iget-object v6, v6, Lcom/peel/i/eh;->c:Ljava/lang/String;

    :goto_2
    move v7, v5

    invoke-virtual/range {v0 .. v7}, Lcom/peel/util/a/f;->a(IIILjava/lang/String;ILjava/lang/String;I)V

    .line 387
    iget-object v0, p0, Lcom/peel/i/em;->a:Lcom/peel/i/eh;

    iget-object v0, v0, Lcom/peel/i/eh;->d:Lcom/peel/i/dx;

    iget-object v0, v0, Lcom/peel/i/dx;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "provider"

    invoke-virtual {v0, v1, v8}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 388
    iget-object v0, p0, Lcom/peel/i/em;->a:Lcom/peel/i/eh;

    iget-object v0, v0, Lcom/peel/i/eh;->d:Lcom/peel/i/dx;

    iget-object v0, v0, Lcom/peel/i/dx;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "provider"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    const-string/jumbo v2, "location"

    iget-object v0, p0, Lcom/peel/i/em;->a:Lcom/peel/i/eh;

    iget-object v0, v0, Lcom/peel/i/eh;->c:Ljava/lang/String;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/peel/i/em;->a:Lcom/peel/i/eh;

    iget-object v0, v0, Lcom/peel/i/eh;->c:Ljava/lang/String;

    :goto_3
    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 389
    iget-object v0, p0, Lcom/peel/i/em;->a:Lcom/peel/i/eh;

    iget-object v0, v0, Lcom/peel/i/eh;->d:Lcom/peel/i/dx;

    invoke-virtual {v0}, Lcom/peel/i/dx;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    const-class v1, Lcom/peel/i/cg;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/peel/i/em;->a:Lcom/peel/i/eh;

    iget-object v2, v2, Lcom/peel/i/eh;->d:Lcom/peel/i/dx;

    iget-object v2, v2, Lcom/peel/i/dx;->c:Landroid/os/Bundle;

    invoke-static {v0, v1, v2}, Lcom/peel/d/e;->c(Landroid/support/v4/app/ae;Ljava/lang/String;Landroid/os/Bundle;)V

    goto/16 :goto_0

    .line 384
    :cond_1
    sget-object v1, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v1}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/data/at;->f()I

    move-result v1

    goto :goto_1

    :cond_2
    iget-object v6, p0, Lcom/peel/i/em;->a:Lcom/peel/i/eh;

    iget-object v6, v6, Lcom/peel/i/eh;->b:Ljava/lang/String;

    goto :goto_2

    .line 388
    :cond_3
    iget-object v0, p0, Lcom/peel/i/em;->a:Lcom/peel/i/eh;

    iget-object v0, v0, Lcom/peel/i/eh;->b:Ljava/lang/String;

    goto :goto_3
.end method
