.class Lcom/peel/i/as;
.super Lcom/peel/util/t;


# instance fields
.field final synthetic a:Lcom/peel/i/ar;


# direct methods
.method constructor <init>(Lcom/peel/i/ar;)V
    .locals 0

    .prologue
    .line 153
    iput-object p1, p0, Lcom/peel/i/as;->a:Lcom/peel/i/ar;

    invoke-direct {p0}, Lcom/peel/util/t;-><init>()V

    return-void
.end method


# virtual methods
.method public a(ZLjava/lang/Object;Ljava/lang/String;)V
    .locals 11

    .prologue
    const/4 v0, 0x1

    .line 156
    iget-object v1, p0, Lcom/peel/i/as;->a:Lcom/peel/i/ar;

    iget-object v1, v1, Lcom/peel/i/ar;->a:Lcom/peel/i/al;

    invoke-static {v1}, Lcom/peel/i/al;->b(Lcom/peel/i/al;)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "onComplete from device discovery"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 157
    if-eqz p1, :cond_2

    .line 158
    check-cast p2, Ljava/util/ArrayList;

    .line 161
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_1

    .line 162
    iget-object v1, p0, Lcom/peel/i/as;->a:Lcom/peel/i/ar;

    iget-object v1, v1, Lcom/peel/i/ar;->a:Lcom/peel/i/al;

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v2

    new-array v2, v2, [Lcom/peel/control/h;

    invoke-static {v1, v2}, Lcom/peel/i/al;->a(Lcom/peel/i/al;[Lcom/peel/control/h;)[Lcom/peel/control/h;

    .line 163
    const/4 v1, 0x0

    move v9, v1

    :goto_0
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v1

    if-ge v9, v1, :cond_0

    .line 164
    iget-object v1, p0, Lcom/peel/i/as;->a:Lcom/peel/i/ar;

    iget-object v1, v1, Lcom/peel/i/ar;->a:Lcom/peel/i/al;

    invoke-static {v1}, Lcom/peel/i/al;->c(Lcom/peel/i/al;)[Lcom/peel/control/h;

    move-result-object v10

    const/4 v1, 0x2

    const-string/jumbo v2, "DIRECTV"

    invoke-interface {p2, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/peel/control/c/b;

    invoke-virtual {v3}, Lcom/peel/control/c/b;->b()Ljava/lang/String;

    move-result-object v4

    const/16 v5, 0x1f90

    invoke-interface {p2, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/peel/control/c/b;

    invoke-virtual {v3}, Lcom/peel/control/c/b;->a()Landroid/os/Bundle;

    move-result-object v6

    const-string/jumbo v7, "DirecTV"

    const-string/jumbo v8, ""

    move v3, v0

    invoke-static/range {v0 .. v8}, Lcom/peel/control/h;->a(IILjava/lang/String;ZLjava/lang/String;ILandroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;)Lcom/peel/control/h;

    move-result-object v1

    aput-object v1, v10, v9

    .line 163
    add-int/lit8 v1, v9, 0x1

    move v9, v1

    goto :goto_0

    .line 166
    :cond_0
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "getDtvLocations"

    new-instance v2, Lcom/peel/i/at;

    invoke-direct {v2, p0}, Lcom/peel/i/at;-><init>(Lcom/peel/i/as;)V

    const-wide/16 v4, 0x12c

    invoke-static {v0, v1, v2, v4, v5}, Lcom/peel/util/i;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;J)Ljava/lang/Runnable;

    .line 181
    :goto_1
    iget-object v0, p0, Lcom/peel/i/as;->a:Lcom/peel/i/ar;

    iget-object v0, v0, Lcom/peel/i/ar;->a:Lcom/peel/i/al;

    invoke-static {v0}, Lcom/peel/i/al;->j(Lcom/peel/i/al;)V

    .line 182
    return-void

    .line 173
    :cond_1
    iget-object v0, p0, Lcom/peel/i/as;->a:Lcom/peel/i/ar;

    iget-object v0, v0, Lcom/peel/i/ar;->a:Lcom/peel/i/al;

    invoke-static {v0}, Lcom/peel/i/al;->e(Lcom/peel/i/al;)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "no dtv found?"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 174
    iget-object v0, p0, Lcom/peel/i/as;->a:Lcom/peel/i/ar;

    iget-object v0, v0, Lcom/peel/i/ar;->a:Lcom/peel/i/al;

    invoke-static {v0}, Lcom/peel/i/al;->f(Lcom/peel/i/al;)V

    goto :goto_1

    .line 177
    :cond_2
    iget-object v0, p0, Lcom/peel/i/as;->a:Lcom/peel/i/ar;

    iget-object v0, v0, Lcom/peel/i/ar;->a:Lcom/peel/i/al;

    invoke-static {v0}, Lcom/peel/i/al;->g(Lcom/peel/i/al;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "error message from DeviceDiscovery.lookupDirecTv:\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 178
    iget-object v0, p0, Lcom/peel/i/as;->a:Lcom/peel/i/ar;

    iget-object v0, v0, Lcom/peel/i/ar;->a:Lcom/peel/i/al;

    invoke-static {v0}, Lcom/peel/i/al;->h(Lcom/peel/i/al;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/i/as;->a:Lcom/peel/i/ar;

    iget-object v1, v1, Lcom/peel/i/ar;->a:Lcom/peel/i/al;

    invoke-static {v1}, Lcom/peel/i/al;->i(Lcom/peel/i/al;)Ljava/lang/String;

    move-result-object v1

    check-cast p2, Ljava/io/IOException;

    invoke-static {v0, v1, p2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 179
    iget-object v0, p0, Lcom/peel/i/as;->a:Lcom/peel/i/ar;

    iget-object v0, v0, Lcom/peel/i/ar;->a:Lcom/peel/i/al;

    invoke-static {v0}, Lcom/peel/i/al;->f(Lcom/peel/i/al;)V

    goto :goto_1
.end method
