.class Lcom/peel/i/ee;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/view/View$OnTouchListener;


# instance fields
.field final synthetic a:Lcom/peel/i/ed;


# direct methods
.method constructor <init>(Lcom/peel/i/ed;)V
    .locals 0

    .prologue
    .line 220
    iput-object p1, p0, Lcom/peel/i/ee;->a:Lcom/peel/i/ed;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 6

    .prologue
    .line 223
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_0

    .line 224
    iget-object v0, p0, Lcom/peel/i/ee;->a:Lcom/peel/i/ed;

    iget-object v0, v0, Lcom/peel/i/ed;->c:Lcom/peel/i/dx;

    invoke-static {v0}, Lcom/peel/i/dx;->f(Lcom/peel/i/dx;)Landroid/widget/TextView;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 226
    iget-object v0, p0, Lcom/peel/i/ee;->a:Lcom/peel/i/ed;

    iget-object v0, v0, Lcom/peel/i/ed;->c:Lcom/peel/i/dx;

    new-instance v1, Landroid/graphics/Rect;

    invoke-virtual {p1}, Landroid/view/View;->getLeft()I

    move-result v2

    invoke-virtual {p1}, Landroid/view/View;->getTop()I

    move-result v3

    invoke-virtual {p1}, Landroid/view/View;->getRight()I

    move-result v4

    invoke-virtual {p1}, Landroid/view/View;->getBottom()I

    move-result v5

    invoke-direct {v1, v2, v3, v4, v5}, Landroid/graphics/Rect;-><init>(IIII)V

    invoke-static {v0, v1}, Lcom/peel/i/dx;->a(Lcom/peel/i/dx;Landroid/graphics/Rect;)Landroid/graphics/Rect;

    .line 229
    :cond_0
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    .line 230
    iget-object v0, p0, Lcom/peel/i/ee;->a:Lcom/peel/i/ed;

    iget-object v0, v0, Lcom/peel/i/ed;->c:Lcom/peel/i/dx;

    invoke-static {v0}, Lcom/peel/i/dx;->b(Lcom/peel/i/dx;)Landroid/graphics/Rect;

    move-result-object v0

    invoke-virtual {p1}, Landroid/view/View;->getLeft()I

    move-result v1

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    float-to-int v2, v2

    add-int/2addr v1, v2

    invoke-virtual {p1}, Landroid/view/View;->getTop()I

    move-result v2

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    float-to-int v3, v3

    add-int/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Rect;->contains(II)Z

    move-result v0

    if-nez v0, :cond_1

    .line 231
    iget-object v0, p0, Lcom/peel/i/ee;->a:Lcom/peel/i/ed;

    iget-object v0, v0, Lcom/peel/i/ed;->c:Lcom/peel/i/dx;

    invoke-static {v0}, Lcom/peel/i/dx;->g(Lcom/peel/i/dx;)Lcom/peel/util/CustomSpinner;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/util/CustomSpinner;->onDetachedFromWindow()V

    .line 234
    :cond_1
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    .line 236
    iget-object v0, p0, Lcom/peel/i/ee;->a:Lcom/peel/i/ed;

    iget-object v0, v0, Lcom/peel/i/ed;->c:Lcom/peel/i/dx;

    invoke-static {v0}, Lcom/peel/i/dx;->f(Lcom/peel/i/dx;)Landroid/widget/TextView;

    move-result-object v0

    sget v1, Lcom/peel/ui/fo;->region_list_stateful:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setBackgroundResource(I)V

    .line 238
    :cond_2
    const/4 v0, 0x0

    return v0
.end method
