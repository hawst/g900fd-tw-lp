.class Lcom/peel/i/ev;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/peel/i/et;


# direct methods
.method constructor <init>(Lcom/peel/i/et;)V
    .locals 0

    .prologue
    .line 279
    iput-object p1, p0, Lcom/peel/i/ev;->a:Lcom/peel/i/et;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 282
    iget-object v0, p0, Lcom/peel/i/ev;->a:Lcom/peel/i/et;

    iget-object v0, v0, Lcom/peel/i/et;->c:Lcom/peel/i/ep;

    new-instance v1, Lcom/peel/widget/ag;

    iget-object v2, p0, Lcom/peel/i/ev;->a:Lcom/peel/i/et;

    iget-object v2, v2, Lcom/peel/i/et;->c:Lcom/peel/i/ep;

    invoke-virtual {v2}, Lcom/peel/i/ep;->m()Landroid/support/v4/app/ae;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/peel/widget/ag;-><init>(Landroid/content/Context;)V

    invoke-static {v0, v1}, Lcom/peel/i/ep;->a(Lcom/peel/i/ep;Lcom/peel/widget/ag;)Lcom/peel/widget/ag;

    .line 283
    iget-object v0, p0, Lcom/peel/i/ev;->a:Lcom/peel/i/et;

    iget-object v0, v0, Lcom/peel/i/et;->a:Landroid/os/Bundle;

    const-string/jumbo v1, "type"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "zip"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 284
    iget-object v0, p0, Lcom/peel/i/ev;->a:Lcom/peel/i/et;

    iget-object v0, v0, Lcom/peel/i/et;->c:Lcom/peel/i/ep;

    invoke-static {v0}, Lcom/peel/i/ep;->f(Lcom/peel/i/ep;)Lcom/peel/widget/ag;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/i/ev;->a:Lcom/peel/i/et;

    iget-object v1, v1, Lcom/peel/i/et;->c:Lcom/peel/i/ep;

    invoke-virtual {v1}, Lcom/peel/i/ep;->n()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/peel/ui/ft;->zipcode_validation_msg_without_zipcode:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/peel/widget/ag;->b(Ljava/lang/CharSequence;)Lcom/peel/widget/ag;

    .line 288
    :goto_0
    iget-object v0, p0, Lcom/peel/i/ev;->a:Lcom/peel/i/et;

    iget-object v0, v0, Lcom/peel/i/et;->c:Lcom/peel/i/ep;

    invoke-static {v0}, Lcom/peel/i/ep;->f(Lcom/peel/i/ep;)Lcom/peel/widget/ag;

    move-result-object v0

    sget v1, Lcom/peel/ui/ft;->mytvservicemissing:I

    invoke-virtual {v0, v1}, Lcom/peel/widget/ag;->a(I)Lcom/peel/widget/ag;

    move-result-object v0

    sget v1, Lcom/peel/ui/ft;->label_report:I

    new-instance v2, Lcom/peel/i/ex;

    invoke-direct {v2, p0}, Lcom/peel/i/ex;-><init>(Lcom/peel/i/ev;)V

    invoke-virtual {v0, v1, v2}, Lcom/peel/widget/ag;->a(ILandroid/content/DialogInterface$OnClickListener;)Lcom/peel/widget/ag;

    move-result-object v0

    sget v1, Lcom/peel/ui/ft;->cancel:I

    new-instance v2, Lcom/peel/i/ew;

    invoke-direct {v2, p0}, Lcom/peel/i/ew;-><init>(Lcom/peel/i/ev;)V

    .line 303
    invoke-virtual {v0, v1, v2}, Lcom/peel/widget/ag;->c(ILandroid/content/DialogInterface$OnClickListener;)Lcom/peel/widget/ag;

    .line 309
    iget-object v0, p0, Lcom/peel/i/ev;->a:Lcom/peel/i/et;

    iget-object v0, v0, Lcom/peel/i/et;->c:Lcom/peel/i/ep;

    invoke-static {v0}, Lcom/peel/i/ep;->f(Lcom/peel/i/ep;)Lcom/peel/widget/ag;

    move-result-object v0

    invoke-virtual {v0, v5}, Lcom/peel/widget/ag;->setCanceledOnTouchOutside(Z)V

    .line 310
    iget-object v0, p0, Lcom/peel/i/ev;->a:Lcom/peel/i/et;

    iget-object v0, v0, Lcom/peel/i/et;->c:Lcom/peel/i/ep;

    invoke-static {v0}, Lcom/peel/i/ep;->f(Lcom/peel/i/ep;)Lcom/peel/widget/ag;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/i/ev;->a:Lcom/peel/i/et;

    iget-object v1, v1, Lcom/peel/i/et;->c:Lcom/peel/i/ep;

    invoke-static {v1}, Lcom/peel/i/ep;->f(Lcom/peel/i/ep;)Lcom/peel/widget/ag;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/peel/widget/ag;->a(Lcom/peel/widget/ag;)Lcom/peel/widget/ag;

    .line 311
    iget-object v0, p0, Lcom/peel/i/ev;->a:Lcom/peel/i/et;

    iget-object v0, v0, Lcom/peel/i/et;->c:Lcom/peel/i/ep;

    invoke-static {v0}, Lcom/peel/i/ep;->f(Lcom/peel/i/ep;)Lcom/peel/widget/ag;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/i/ev;->a:Lcom/peel/i/et;

    iget-object v1, v1, Lcom/peel/i/et;->c:Lcom/peel/i/ep;

    invoke-static {v1}, Lcom/peel/i/ep;->f(Lcom/peel/i/ep;)Lcom/peel/widget/ag;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/peel/widget/ag;->a(Lcom/peel/widget/ag;)Lcom/peel/widget/ag;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/widget/ag;->d()Lcom/peel/widget/ag;

    .line 312
    return-void

    .line 286
    :cond_0
    iget-object v0, p0, Lcom/peel/i/ev;->a:Lcom/peel/i/et;

    iget-object v0, v0, Lcom/peel/i/et;->c:Lcom/peel/i/ep;

    invoke-static {v0}, Lcom/peel/i/ep;->f(Lcom/peel/i/ep;)Lcom/peel/widget/ag;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/i/ev;->a:Lcom/peel/i/et;

    iget-object v1, v1, Lcom/peel/i/et;->c:Lcom/peel/i/ep;

    invoke-virtual {v1}, Lcom/peel/i/ep;->n()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/peel/ui/ft;->zipcode_validation_msg:I

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/peel/i/ev;->a:Lcom/peel/i/et;

    iget-object v4, v4, Lcom/peel/i/et;->b:Ljava/lang/String;

    aput-object v4, v3, v5

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/peel/widget/ag;->b(Ljava/lang/CharSequence;)Lcom/peel/widget/ag;

    goto :goto_0
.end method
