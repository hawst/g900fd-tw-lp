.class public Lcom/peel/i/fc;
.super Lcom/peel/d/u;

# interfaces
.implements Landroid/view/View$OnClickListener;


# static fields
.field private static final f:Ljava/lang/String;


# instance fields
.field private aj:Landroid/widget/ProgressBar;

.field private ak:Ljava/lang/String;

.field private al:Ljava/lang/String;

.field private am:I

.field private an:I

.field private ao:Lcom/peel/widget/ag;

.field public e:Lcom/peel/d/a;

.field private g:Landroid/widget/ViewFlipper;

.field private h:Ljava/lang/String;

.field private i:Landroid/view/LayoutInflater;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 59
    const-class v0, Lcom/peel/i/fc;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/peel/i/fc;->f:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 58
    invoke-direct {p0}, Lcom/peel/d/u;-><init>()V

    .line 64
    iput-object v0, p0, Lcom/peel/i/fc;->ak:Ljava/lang/String;

    .line 68
    iput-object v0, p0, Lcom/peel/i/fc;->ao:Lcom/peel/widget/ag;

    return-void
.end method

.method private S()V
    .locals 2

    .prologue
    .line 94
    invoke-virtual {p0}, Lcom/peel/i/fc;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    const-string/jumbo v1, "connectivity"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/ae;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 96
    if-eqz v0, :cond_0

    .line 97
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v0

    .line 99
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 100
    invoke-virtual {p0}, Lcom/peel/i/fc;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    const-string/jumbo v1, "wifi"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/ae;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiManager;

    .line 101
    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->getConnectionInfo()Landroid/net/wifi/WifiInfo;

    move-result-object v0

    .line 103
    if-eqz v0, :cond_0

    .line 104
    invoke-virtual {v0}, Landroid/net/wifi/WifiInfo;->getSSID()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/i/fc;->ak:Ljava/lang/String;

    .line 108
    :cond_0
    return-void
.end method

.method private T()V
    .locals 3

    .prologue
    .line 125
    sget-object v0, Lcom/peel/i/fc;->f:Ljava/lang/String;

    const-string/jumbo v1, "lookuproku"

    new-instance v2, Lcom/peel/i/fd;

    invoke-direct {v2, p0}, Lcom/peel/i/fd;-><init>(Lcom/peel/i/fc;)V

    invoke-static {v0, v1, v2}, Lcom/peel/util/i;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    .line 186
    return-void
.end method

.method private U()V
    .locals 5

    .prologue
    .line 202
    iget-object v0, p0, Lcom/peel/i/fc;->g:Landroid/widget/ViewFlipper;

    sget v1, Lcom/peel/ui/fp;->not_found_roku_label:I

    invoke-virtual {v0, v1}, Landroid/widget/ViewFlipper;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 204
    iget-object v1, p0, Lcom/peel/i/fc;->ak:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 205
    sget v1, Lcom/peel/ui/ft;->title_wifi_unavailable:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 210
    :goto_0
    iget-object v0, p0, Lcom/peel/i/fc;->g:Landroid/widget/ViewFlipper;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/widget/ViewFlipper;->setDisplayedChild(I)V

    .line 211
    invoke-virtual {p0}, Lcom/peel/i/fc;->Z()V

    .line 212
    iget-object v0, p0, Lcom/peel/i/fc;->g:Landroid/widget/ViewFlipper;

    sget v1, Lcom/peel/ui/fp;->add_btn:I

    invoke-virtual {v0, v1}, Landroid/widget/ViewFlipper;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 213
    iget-object v0, p0, Lcom/peel/i/fc;->g:Landroid/widget/ViewFlipper;

    sget v1, Lcom/peel/ui/fp;->setup_ir_btn:I

    invoke-virtual {v0, v1}, Landroid/widget/ViewFlipper;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 214
    return-void

    .line 207
    :cond_0
    sget v1, Lcom/peel/ui/ft;->not_found_roku:I

    invoke-virtual {p0, v1}, Lcom/peel/i/fc;->a(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/peel/i/fc;->ak:Ljava/lang/String;

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method static synthetic a(Lcom/peel/i/fc;)Landroid/widget/ProgressBar;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/peel/i/fc;->aj:Landroid/widget/ProgressBar;

    return-object v0
.end method

.method private a(Lcom/peel/control/h;)V
    .locals 13

    .prologue
    const/4 v12, 0x5

    const/4 v3, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 271
    .line 272
    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v0}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v7

    .line 275
    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v0, v7}, Lcom/peel/control/am;->c(Lcom/peel/control/RoomControl;)[Lcom/peel/control/h;

    move-result-object v8

    .line 277
    if-eqz v8, :cond_2

    .line 278
    array-length v9, v8

    move v6, v5

    move-object v0, v3

    move-object v2, v3

    :goto_0
    if-ge v6, v9, :cond_3

    aget-object v1, v8, v6

    .line 279
    invoke-virtual {v1}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v10

    invoke-virtual {v10}, Lcom/peel/data/g;->d()I

    move-result v10

    if-ne v10, v12, :cond_0

    .line 278
    :goto_1
    add-int/lit8 v2, v6, 0x1

    move v6, v2

    move-object v2, v1

    goto :goto_0

    .line 281
    :cond_0
    invoke-virtual {v1}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v10

    invoke-virtual {v10}, Lcom/peel/data/g;->d()I

    move-result v10

    if-eq v10, v4, :cond_1

    invoke-virtual {v1}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v10

    invoke-virtual {v10}, Lcom/peel/data/g;->d()I

    move-result v10

    const/16 v11, 0xa

    if-ne v10, v11, :cond_a

    :cond_1
    move-object v0, v1

    move-object v1, v2

    .line 282
    goto :goto_1

    :cond_2
    move-object v0, v3

    move-object v2, v3

    .line 288
    :cond_3
    invoke-virtual {p0}, Lcom/peel/i/fc;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    iget v6, p0, Lcom/peel/i/fc;->am:I

    invoke-static {v1, v6}, Lcom/peel/util/bx;->a(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/peel/control/a;->a(Ljava/lang/String;)Lcom/peel/control/a;

    move-result-object v1

    .line 291
    new-array v6, v4, [Ljava/lang/Integer;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v5

    invoke-virtual {v1, p1, v3, v6}, Lcom/peel/control/a;->a(Lcom/peel/control/h;Ljava/lang/String;[Ljava/lang/Integer;)Z

    .line 292
    invoke-virtual {v7, v1}, Lcom/peel/control/RoomControl;->a(Lcom/peel/control/a;)V

    .line 293
    invoke-virtual {v7}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v6

    invoke-virtual {v1}, Lcom/peel/control/a;->c()Lcom/peel/data/d;

    move-result-object v8

    invoke-virtual {v8}, Lcom/peel/data/d;->b()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, Lcom/peel/data/at;->c(Ljava/lang/String;)V

    .line 294
    if-eqz v2, :cond_8

    .line 295
    new-array v6, v4, [Ljava/lang/Integer;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v5

    invoke-virtual {v1, v2, v3, v6}, Lcom/peel/control/a;->a(Lcom/peel/control/h;Ljava/lang/String;[Ljava/lang/Integer;)Z

    .line 297
    if-eqz v0, :cond_4

    .line 298
    invoke-virtual {v1, v0, v3, v3}, Lcom/peel/control/a;->a(Lcom/peel/control/h;Ljava/lang/String;[Ljava/lang/Integer;)Z

    .line 301
    :cond_4
    :goto_2
    invoke-virtual {p0}, Lcom/peel/i/fc;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    const-string/jumbo v2, "social_accounts_setup"

    invoke-virtual {v0, v2, v5}, Landroid/support/v4/app/ae;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v2, "show social login"

    invoke-interface {v0, v2, v4}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 304
    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v0}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v2

    .line 305
    if-eqz v7, :cond_6

    if-eqz v2, :cond_6

    .line 306
    invoke-virtual {v7}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/data/at;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v6

    invoke-virtual {v6}, Lcom/peel/data/at;->b()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 307
    invoke-virtual {p0}, Lcom/peel/i/fc;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string/jumbo v6, "setup_type"

    invoke-interface {v0, v6, v5}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    if-ne v4, v0, :cond_9

    move v0, v4

    .line 308
    :goto_3
    if-nez v0, :cond_5

    if-nez v0, :cond_6

    iget v0, p0, Lcom/peel/i/fc;->am:I

    if-eq v0, v4, :cond_6

    iget v0, p0, Lcom/peel/i/fc;->am:I

    if-eq v0, v12, :cond_6

    .line 309
    :cond_5
    const-class v0, Lcom/peel/i/fc;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "start activity "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Lcom/peel/control/a;->c()Lcom/peel/data/d;

    move-result-object v5

    invoke-virtual {v5}, Lcom/peel/data/d;->a()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Lcom/peel/i/fn;

    invoke-direct {v5, p0, v2, v1}, Lcom/peel/i/fn;-><init>(Lcom/peel/i/fc;Lcom/peel/control/RoomControl;Lcom/peel/control/a;)V

    invoke-static {v0, v4, v5}, Lcom/peel/util/i;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/lang/Runnable;

    .line 320
    :cond_6
    invoke-virtual {p0}, Lcom/peel/i/fc;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/ae;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/peel/social/w;->f(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 321
    new-instance v0, Lcom/peel/backup/c;

    invoke-virtual {p0}, Lcom/peel/i/fc;->m()Landroid/support/v4/app/ae;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/peel/backup/c;-><init>(Landroid/content/Context;)V

    .line 322
    iget-object v2, p0, Lcom/peel/i/fc;->c:Landroid/os/Bundle;

    const-string/jumbo v4, "providername"

    invoke-virtual {v2, v4, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v7, v2, v1, p1}, Lcom/peel/backup/c;->a(Lcom/peel/control/RoomControl;Ljava/lang/String;Lcom/peel/control/a;Lcom/peel/control/h;)V

    .line 324
    :cond_7
    sget-object v0, Lcom/peel/i/fc;->f:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/peel/i/fc;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/peel/d/e;->a(Ljava/lang/String;Landroid/support/v4/app/ae;)V

    .line 325
    return-void

    .line 299
    :cond_8
    if-eqz v0, :cond_4

    .line 300
    new-array v2, v4, [Ljava/lang/Integer;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v2, v5

    invoke-virtual {v1, v0, v3, v2}, Lcom/peel/control/a;->a(Lcom/peel/control/h;Ljava/lang/String;[Ljava/lang/Integer;)Z

    goto/16 :goto_2

    :cond_9
    move v0, v5

    .line 307
    goto :goto_3

    :cond_a
    move-object v1, v2

    goto/16 :goto_1
.end method

.method static synthetic a(Lcom/peel/i/fc;Lcom/peel/control/h;)V
    .locals 0

    .prologue
    .line 58
    invoke-direct {p0, p1}, Lcom/peel/i/fc;->a(Lcom/peel/control/h;)V

    return-void
.end method

.method static synthetic a(Lcom/peel/i/fc;[Lcom/peel/control/h;)V
    .locals 0

    .prologue
    .line 58
    invoke-direct {p0, p1}, Lcom/peel/i/fc;->a([Lcom/peel/control/h;)V

    return-void
.end method

.method private a([Lcom/peel/control/h;)V
    .locals 5

    .prologue
    const/4 v2, 0x1

    .line 217
    iget-object v0, p0, Lcom/peel/i/fc;->g:Landroid/widget/ViewFlipper;

    invoke-virtual {v0, v2}, Landroid/widget/ViewFlipper;->setDisplayedChild(I)V

    .line 218
    iget-object v0, p0, Lcom/peel/i/fc;->g:Landroid/widget/ViewFlipper;

    sget v1, Lcom/peel/ui/fp;->found_roku_label:I

    invoke-virtual {v0, v1}, Landroid/widget/ViewFlipper;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 219
    sget v1, Lcom/peel/ui/ft;->found_roku:I

    invoke-virtual {p0, v1}, Lcom/peel/i/fc;->a(I)Ljava/lang/String;

    move-result-object v1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/peel/i/fc;->ak:Ljava/lang/String;

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 220
    iget-object v0, p0, Lcom/peel/i/fc;->g:Landroid/widget/ViewFlipper;

    sget v1, Lcom/peel/ui/fp;->roku_list:I

    invoke-virtual {v0, v1}, Landroid/widget/ViewFlipper;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    .line 221
    new-instance v1, Lcom/peel/i/fk;

    invoke-direct {v1, p0, p1}, Lcom/peel/i/fk;-><init>(Lcom/peel/i/fc;[Lcom/peel/control/h;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 268
    return-void
.end method

.method private a(Ljava/lang/String;)Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 366
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string/jumbo v1, "."

    invoke-virtual {p1, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 382
    :cond_0
    :goto_0
    return v0

    .line 370
    :cond_1
    const-string/jumbo v1, "\\."

    invoke-virtual {p1, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 372
    array-length v1, v2

    const/4 v3, 0x4

    if-ne v1, v3, :cond_0

    .line 375
    array-length v3, v2

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_2

    aget-object v4, v2, v1

    .line 376
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 375
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 382
    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method static synthetic b(Lcom/peel/i/fc;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/peel/i/fc;->h:Ljava/lang/String;

    return-object v0
.end method

.method private b(Lcom/peel/control/h;)V
    .locals 5

    .prologue
    .line 339
    invoke-virtual {p0}, Lcom/peel/i/fc;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string/jumbo v1, "country_ISO"

    const-string/jumbo v2, "US"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 340
    const-string/jumbo v1, "https://partners-ir.peel.com"

    iget v2, p0, Lcom/peel/i/fc;->an:I

    iget v3, p0, Lcom/peel/i/fc;->am:I

    new-instance v4, Lcom/peel/i/fo;

    invoke-direct {v4, p0, p1}, Lcom/peel/i/fo;-><init>(Lcom/peel/i/fc;Lcom/peel/control/h;)V

    invoke-static {v1, v2, v3, v0, v4}, Lcom/peel/control/aa;->a(Ljava/lang/String;IILjava/lang/String;Lcom/peel/util/t;)V

    .line 363
    return-void
.end method

.method static synthetic c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 58
    sget-object v0, Lcom/peel/i/fc;->f:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic c(Lcom/peel/i/fc;)V
    .locals 0

    .prologue
    .line 58
    invoke-direct {p0}, Lcom/peel/i/fc;->U()V

    return-void
.end method

.method static synthetic d(Lcom/peel/i/fc;)Landroid/view/LayoutInflater;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/peel/i/fc;->i:Landroid/view/LayoutInflater;

    return-object v0
.end method


# virtual methods
.method public Z()V
    .locals 6

    .prologue
    .line 329
    const/4 v5, 0x0

    .line 330
    iget-object v0, p0, Lcom/peel/i/fc;->g:Landroid/widget/ViewFlipper;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/peel/i/fc;->g:Landroid/widget/ViewFlipper;

    invoke-virtual {v0}, Landroid/widget/ViewFlipper;->getDisplayedChild()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 331
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 332
    sget v0, Lcom/peel/ui/fp;->menu_retry:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 334
    :cond_0
    new-instance v0, Lcom/peel/d/a;

    sget-object v1, Lcom/peel/d/d;->b:Lcom/peel/d/d;

    sget-object v2, Lcom/peel/d/b;->a:Lcom/peel/d/b;

    sget-object v3, Lcom/peel/d/c;->b:Lcom/peel/d/c;

    sget v4, Lcom/peel/ui/ft;->add_roku_title:I

    invoke-virtual {p0, v4}, Lcom/peel/i/fc;->a(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct/range {v0 .. v5}, Lcom/peel/d/a;-><init>(Lcom/peel/d/d;Lcom/peel/d/b;Lcom/peel/d/c;Ljava/lang/String;Ljava/util/List;)V

    iput-object v0, p0, Lcom/peel/i/fc;->e:Lcom/peel/d/a;

    .line 335
    iget-object v0, p0, Lcom/peel/i/fc;->b:Lcom/peel/d/i;

    iget-object v1, p0, Lcom/peel/i/fc;->e:Lcom/peel/d/a;

    invoke-virtual {v0, v1}, Lcom/peel/d/i;->a(Lcom/peel/d/a;)V

    .line 336
    return-void
.end method

.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    .line 112
    sget v0, Lcom/peel/ui/fq;->init_roku_screen:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ViewFlipper;

    iput-object v0, p0, Lcom/peel/i/fc;->g:Landroid/widget/ViewFlipper;

    .line 113
    iget-object v0, p0, Lcom/peel/i/fc;->g:Landroid/widget/ViewFlipper;

    sget v1, Lcom/peel/ui/fp;->roku_progress:I

    invoke-virtual {v0, v1}, Landroid/widget/ViewFlipper;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/peel/i/fc;->aj:Landroid/widget/ProgressBar;

    .line 114
    iput-object p1, p0, Lcom/peel/i/fc;->i:Landroid/view/LayoutInflater;

    .line 115
    iget-object v0, p0, Lcom/peel/i/fc;->g:Landroid/widget/ViewFlipper;

    return-object v0
.end method

.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lcom/peel/i/fc;->al:Ljava/lang/String;

    return-object v0
.end method

.method public a(Landroid/view/MenuItem;)Z
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 190
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    .line 191
    sget v1, Lcom/peel/ui/fp;->menu_retry:I

    if-ne v0, v1, :cond_0

    .line 192
    invoke-virtual {p0}, Lcom/peel/i/fc;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    const-string/jumbo v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/ae;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 193
    invoke-virtual {p0}, Lcom/peel/i/fc;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/ae;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 194
    iget-object v0, p0, Lcom/peel/i/fc;->g:Landroid/widget/ViewFlipper;

    invoke-virtual {v0, v2}, Landroid/widget/ViewFlipper;->setDisplayedChild(I)V

    .line 195
    invoke-virtual {p0}, Lcom/peel/i/fc;->Z()V

    .line 196
    invoke-direct {p0}, Lcom/peel/i/fc;->T()V

    .line 198
    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method public d(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    .line 72
    invoke-super {p0, p1}, Lcom/peel/d/u;->d(Landroid/os/Bundle;)V

    .line 73
    iget-object v0, p0, Lcom/peel/i/fc;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "brandName"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/i/fc;->h:Ljava/lang/String;

    .line 74
    iget-object v0, p0, Lcom/peel/i/fc;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "brandId"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/peel/i/fc;->an:I

    .line 75
    iget-object v0, p0, Lcom/peel/i/fc;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "device_type"

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/peel/i/fc;->am:I

    .line 76
    invoke-direct {p0}, Lcom/peel/i/fc;->S()V

    .line 77
    iget-object v0, p0, Lcom/peel/i/fc;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "back_to_clazz"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/peel/i/fc;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "back_to_clazz"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    iput-object v0, p0, Lcom/peel/i/fc;->al:Ljava/lang/String;

    .line 79
    iget-object v0, p0, Lcom/peel/i/fc;->ak:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 80
    iget-object v0, p0, Lcom/peel/i/fc;->g:Landroid/widget/ViewFlipper;

    sget v1, Lcom/peel/ui/fp;->roku_setup_label:I

    invoke-virtual {v0, v1}, Landroid/widget/ViewFlipper;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 81
    sget v1, Lcom/peel/ui/ft;->looking_roku:I

    invoke-virtual {p0, v1}, Lcom/peel/i/fc;->a(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/peel/i/fc;->ak:Ljava/lang/String;

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 82
    invoke-direct {p0}, Lcom/peel/i/fc;->T()V

    .line 86
    :goto_1
    return-void

    .line 77
    :cond_0
    const-class v0, Lcom/peel/h/a/fn;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 84
    :cond_1
    invoke-direct {p0}, Lcom/peel/i/fc;->U()V

    goto :goto_1
.end method

.method public f()V
    .locals 3

    .prologue
    .line 407
    invoke-super {p0}, Lcom/peel/d/u;->f()V

    .line 408
    invoke-virtual {p0}, Lcom/peel/i/fc;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    const-string/jumbo v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/ae;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 409
    invoke-virtual {p0}, Lcom/peel/i/fc;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/ae;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 410
    iget-object v0, p0, Lcom/peel/i/fc;->ao:Lcom/peel/widget/ag;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/peel/i/fc;->ao:Lcom/peel/widget/ag;

    invoke-virtual {v0}, Lcom/peel/widget/ag;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 411
    iget-object v0, p0, Lcom/peel/i/fc;->ao:Lcom/peel/widget/ag;

    invoke-virtual {v0}, Lcom/peel/widget/ag;->dismiss()V

    .line 413
    :cond_0
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 11

    .prologue
    const/4 v0, 0x1

    const/4 v6, 0x0

    .line 388
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    .line 389
    sget v2, Lcom/peel/ui/fp;->add_btn:I

    if-ne v1, v2, :cond_2

    .line 390
    iget-object v1, p0, Lcom/peel/i/fc;->g:Landroid/widget/ViewFlipper;

    sget v2, Lcom/peel/ui/fp;->add_roku_ip:I

    invoke-virtual {v1, v2}, Landroid/widget/ViewFlipper;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    .line 391
    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    .line 392
    invoke-direct {p0, v4}, Lcom/peel/i/fc;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 393
    const/4 v1, 0x6

    iget-object v2, p0, Lcom/peel/i/fc;->h:Ljava/lang/String;

    const/16 v5, 0x1f7c

    const-string/jumbo v7, "Roku"

    const-string/jumbo v8, ""

    move v3, v0

    invoke-static/range {v0 .. v8}, Lcom/peel/control/h;->a(IILjava/lang/String;ZLjava/lang/String;ILandroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;)Lcom/peel/control/h;

    move-result-object v0

    .line 394
    invoke-direct {p0, v0}, Lcom/peel/i/fc;->a(Lcom/peel/control/h;)V

    .line 403
    :cond_0
    :goto_0
    return-void

    .line 396
    :cond_1
    new-instance v0, Lcom/peel/widget/ag;

    invoke-virtual {p0}, Lcom/peel/i/fc;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/peel/widget/ag;-><init>(Landroid/content/Context;)V

    sget v1, Lcom/peel/ui/ft;->error_ip_enter:I

    invoke-virtual {v0, v1}, Lcom/peel/widget/ag;->b(I)Lcom/peel/widget/ag;

    move-result-object v0

    sget v1, Lcom/peel/ui/ft;->ok:I

    invoke-virtual {v0, v1, v6}, Lcom/peel/widget/ag;->c(ILandroid/content/DialogInterface$OnClickListener;)Lcom/peel/widget/ag;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/i/fc;->ao:Lcom/peel/widget/ag;

    .line 397
    iget-object v0, p0, Lcom/peel/i/fc;->ao:Lcom/peel/widget/ag;

    iget-object v1, p0, Lcom/peel/i/fc;->ao:Lcom/peel/widget/ag;

    invoke-virtual {v0, v1}, Lcom/peel/widget/ag;->a(Lcom/peel/widget/ag;)Lcom/peel/widget/ag;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/widget/ag;->d()Lcom/peel/widget/ag;

    goto :goto_0

    .line 399
    :cond_2
    sget v2, Lcom/peel/ui/fp;->setup_ir_btn:I

    if-ne v1, v2, :cond_0

    .line 400
    const/4 v2, 0x0

    iget v3, p0, Lcom/peel/i/fc;->am:I

    iget-object v4, p0, Lcom/peel/i/fc;->h:Ljava/lang/String;

    const/4 v7, -0x1

    const-string/jumbo v9, "Roku"

    move v5, v0

    move-object v8, v6

    move-object v10, v6

    invoke-static/range {v2 .. v10}, Lcom/peel/control/h;->a(IILjava/lang/String;ZLjava/lang/String;ILandroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;)Lcom/peel/control/h;

    move-result-object v0

    .line 401
    invoke-direct {p0, v0}, Lcom/peel/i/fc;->b(Lcom/peel/control/h;)V

    goto :goto_0
.end method

.method public w()V
    .locals 0

    .prologue
    .line 120
    invoke-super {p0}, Lcom/peel/d/u;->w()V

    .line 121
    invoke-virtual {p0}, Lcom/peel/i/fc;->Z()V

    .line 122
    return-void
.end method
