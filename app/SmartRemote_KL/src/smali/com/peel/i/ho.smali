.class Lcom/peel/i/ho;
.super Landroid/support/v4/view/av;


# instance fields
.field final synthetic a:Lcom/peel/i/gm;


# direct methods
.method constructor <init>(Lcom/peel/i/gm;)V
    .locals 0

    .prologue
    .line 606
    iput-object p1, p0, Lcom/peel/i/ho;->a:Lcom/peel/i/gm;

    invoke-direct {p0}, Landroid/support/v4/view/av;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/view/ViewGroup;I)Ljava/lang/Object;
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 610
    if-nez p2, :cond_1

    sget v0, Lcom/peel/ui/fq;->before_setup_step1:I

    .line 611
    :goto_0
    iget-object v1, p0, Lcom/peel/i/ho;->a:Lcom/peel/i/gm;

    invoke-static {v1}, Lcom/peel/i/gm;->x(Lcom/peel/i/gm;)Landroid/view/LayoutInflater;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 613
    if-ne p2, v3, :cond_0

    .line 614
    iget-object v2, p0, Lcom/peel/i/ho;->a:Lcom/peel/i/gm;

    sget v0, Lcom/peel/ui/fp;->tv_point_anim:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-static {v2, v0}, Lcom/peel/i/gm;->a(Lcom/peel/i/gm;Landroid/widget/ImageView;)Landroid/widget/ImageView;

    .line 615
    iget-object v0, p0, Lcom/peel/i/ho;->a:Lcom/peel/i/gm;

    invoke-static {v0}, Lcom/peel/i/gm;->y(Lcom/peel/i/gm;)Landroid/widget/ImageView;

    move-result-object v0

    sget v2, Lcom/peel/ui/fo;->setup_tv_signal_arrow_anim:I

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 616
    iget-object v2, p0, Lcom/peel/i/ho;->a:Lcom/peel/i/gm;

    iget-object v0, p0, Lcom/peel/i/ho;->a:Lcom/peel/i/gm;

    invoke-static {v0}, Lcom/peel/i/gm;->y(Lcom/peel/i/gm;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ImageView;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/AnimationDrawable;

    invoke-static {v2, v0}, Lcom/peel/i/gm;->a(Lcom/peel/i/gm;Landroid/graphics/drawable/AnimationDrawable;)Landroid/graphics/drawable/AnimationDrawable;

    .line 619
    :cond_0
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    .line 622
    if-nez v0, :cond_2

    if-ne p2, v3, :cond_2

    .line 623
    invoke-virtual {p1, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 628
    :goto_1
    return-object v1

    .line 610
    :cond_1
    sget v0, Lcom/peel/ui/fq;->before_setup_step2:I

    goto :goto_0

    .line 625
    :cond_2
    invoke-virtual {p1, v1, p2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;I)V

    goto :goto_1
.end method

.method public a(Landroid/view/ViewGroup;ILjava/lang/Object;)V
    .locals 0

    .prologue
    .line 644
    check-cast p3, Landroid/view/View;

    invoke-virtual {p1, p3}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 645
    return-void
.end method

.method public a(Landroid/view/View;Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 639
    if-ne p1, p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()I
    .locals 1

    .prologue
    .line 634
    const/4 v0, 0x2

    return v0
.end method
