.class Lcom/peel/i/gt;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field final synthetic a:Lcom/peel/i/gm;


# direct methods
.method constructor <init>(Lcom/peel/i/gm;)V
    .locals 0

    .prologue
    .line 951
    iput-object p1, p0, Lcom/peel/i/gt;->a:Lcom/peel/i/gm;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    .line 954
    iget-object v0, p0, Lcom/peel/i/gt;->a:Lcom/peel/i/gm;

    invoke-static {v0}, Lcom/peel/i/gm;->P(Lcom/peel/i/gm;)Lcom/peel/i/a/a;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 955
    iget-object v0, p0, Lcom/peel/i/gt;->a:Lcom/peel/i/gm;

    invoke-static {v0, v3}, Lcom/peel/i/gm;->c(Lcom/peel/i/gm;Z)Z

    .line 956
    iget-object v0, p0, Lcom/peel/i/gt;->a:Lcom/peel/i/gm;

    invoke-virtual {v0}, Lcom/peel/i/gm;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    const-string/jumbo v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/ae;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 957
    iget-object v1, p0, Lcom/peel/i/gt;->a:Lcom/peel/i/gm;

    invoke-static {v1}, Lcom/peel/i/gm;->I(Lcom/peel/i/gm;)Landroid/widget/AutoCompleteTextView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/AutoCompleteTextView;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 958
    iget-object v0, p0, Lcom/peel/i/gt;->a:Lcom/peel/i/gm;

    invoke-static {v0}, Lcom/peel/i/gm;->H(Lcom/peel/i/gm;)Landroid/widget/ListView;

    move-result-object v0

    invoke-static {}, Lcom/peel/i/gm;->c()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/peel/util/bx;->a(Landroid/view/View;Ljava/lang/String;)V

    .line 959
    iget-object v0, p0, Lcom/peel/i/gt;->a:Lcom/peel/i/gm;

    invoke-static {v0}, Lcom/peel/i/gm;->P(Lcom/peel/i/gm;)Lcom/peel/i/a/a;

    move-result-object v0

    invoke-virtual {v0, p3}, Lcom/peel/i/a/a;->b(I)V

    .line 960
    iget-object v0, p0, Lcom/peel/i/gt;->a:Lcom/peel/i/gm;

    invoke-static {v0, v3}, Lcom/peel/i/gm;->a(Lcom/peel/i/gm;Z)Z

    .line 961
    iget-object v1, p0, Lcom/peel/i/gt;->a:Lcom/peel/i/gm;

    invoke-virtual {p1}, Landroid/widget/AdapterView;->getAdapter()Landroid/widget/Adapter;

    move-result-object v0

    invoke-interface {v0, p3}, Landroid/widget/Adapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/f/a;

    invoke-static {v1, v0}, Lcom/peel/i/gm;->a(Lcom/peel/i/gm;Lcom/peel/f/a;)Lcom/peel/f/a;

    .line 962
    iget-object v0, p0, Lcom/peel/i/gt;->a:Lcom/peel/i/gm;

    iget-object v1, p0, Lcom/peel/i/gt;->a:Lcom/peel/i/gm;

    invoke-virtual {v1}, Lcom/peel/i/gm;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    iget-object v2, p0, Lcom/peel/i/gt;->a:Lcom/peel/i/gm;

    invoke-static {v2}, Lcom/peel/i/gm;->D(Lcom/peel/i/gm;)I

    move-result v2

    invoke-static {v1, v2}, Lcom/peel/util/bx;->c(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/peel/i/gt;->a:Lcom/peel/i/gm;

    invoke-static {v2}, Lcom/peel/i/gm;->K(Lcom/peel/i/gm;)Z

    move-result v2

    invoke-static {v0, v1, v3, v2}, Lcom/peel/i/gm;->a(Lcom/peel/i/gm;Ljava/lang/String;ZZ)V

    .line 970
    :cond_0
    return-void
.end method
