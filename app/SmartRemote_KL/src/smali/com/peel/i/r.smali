.class Lcom/peel/i/r;
.super Lcom/peel/util/t;


# instance fields
.field final synthetic a:Lcom/peel/i/a;


# direct methods
.method constructor <init>(Lcom/peel/i/a;I)V
    .locals 0

    .prologue
    .line 842
    iput-object p1, p0, Lcom/peel/i/r;->a:Lcom/peel/i/a;

    invoke-direct {p0, p2}, Lcom/peel/util/t;-><init>(I)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    const/16 v7, 0x8

    const/4 v2, 0x4

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 846
    iget-boolean v0, p0, Lcom/peel/i/r;->i:Z

    if-nez v0, :cond_2

    .line 847
    iget-object v0, p0, Lcom/peel/i/r;->k:Ljava/lang/String;

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/peel/i/a;->c()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/i/r;->k:Ljava/lang/String;

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 848
    :cond_0
    invoke-static {}, Lcom/peel/i/a;->c()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/i/r;->a:Lcom/peel/i/a;

    invoke-virtual {v1}, Lcom/peel/i/a;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    invoke-static {v0, v1, v5}, Lcom/peel/util/bx;->a(Ljava/lang/String;Landroid/app/Activity;Z)V

    .line 849
    sget-boolean v0, Lcom/peel/util/b/a;->a:Z

    if-eqz v0, :cond_1

    .line 850
    iget-object v0, p0, Lcom/peel/i/r;->a:Lcom/peel/i/a;

    invoke-static {v0}, Lcom/peel/i/a;->j(Lcom/peel/i/a;)V

    .line 953
    :goto_0
    return-void

    .line 852
    :cond_1
    iget-object v0, p0, Lcom/peel/i/r;->a:Lcom/peel/i/a;

    invoke-static {v0}, Lcom/peel/i/a;->k(Lcom/peel/i/a;)V

    goto :goto_0

    .line 854
    :cond_2
    invoke-static {}, Lcom/peel/i/a;->c()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/i/r;->a:Lcom/peel/i/a;

    invoke-virtual {v1}, Lcom/peel/i/a;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    invoke-static {v0, v1, v5}, Lcom/peel/util/bx;->a(Ljava/lang/String;Landroid/app/Activity;Z)V

    .line 855
    iget-object v1, p0, Lcom/peel/i/r;->a:Lcom/peel/i/a;

    iget-object v0, p0, Lcom/peel/i/r;->j:Ljava/lang/Object;

    check-cast v0, Ljava/util/ArrayList;

    invoke-static {v1, v0}, Lcom/peel/i/a;->a(Lcom/peel/i/a;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    .line 856
    iget-object v0, p0, Lcom/peel/i/r;->a:Lcom/peel/i/a;

    invoke-static {v0}, Lcom/peel/i/a;->x(Lcom/peel/i/a;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ne v0, v6, :cond_3

    .line 857
    iget-object v0, p0, Lcom/peel/i/r;->a:Lcom/peel/i/a;

    invoke-static {v0}, Lcom/peel/i/a;->I(Lcom/peel/i/a;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 858
    iget-object v0, p0, Lcom/peel/i/r;->a:Lcom/peel/i/a;

    invoke-static {v0}, Lcom/peel/i/a;->J(Lcom/peel/i/a;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 860
    :cond_3
    iget-object v0, p0, Lcom/peel/i/r;->a:Lcom/peel/i/a;

    invoke-static {v0, v5}, Lcom/peel/i/a;->c(Lcom/peel/i/a;I)I

    .line 861
    iget-object v1, p0, Lcom/peel/i/r;->a:Lcom/peel/i/a;

    iget-object v0, p0, Lcom/peel/i/r;->a:Lcom/peel/i/a;

    invoke-static {v0}, Lcom/peel/i/a;->x(Lcom/peel/i/a;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    const-string/jumbo v2, "codesetid"

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v1, v0}, Lcom/peel/i/a;->d(Lcom/peel/i/a;I)I

    .line 862
    iget-object v1, p0, Lcom/peel/i/r;->a:Lcom/peel/i/a;

    iget-object v0, p0, Lcom/peel/i/r;->a:Lcom/peel/i/a;

    invoke-static {v0}, Lcom/peel/i/a;->x(Lcom/peel/i/a;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    const-string/jumbo v2, "funName"

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v1, v0}, Lcom/peel/i/a;->b(Lcom/peel/i/a;Ljava/lang/String;)Ljava/lang/String;

    .line 863
    iget-object v0, p0, Lcom/peel/i/r;->a:Lcom/peel/i/a;

    invoke-static {v0}, Lcom/peel/i/a;->v(Lcom/peel/i/a;)Lcom/peel/control/h;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v1

    iget-object v0, p0, Lcom/peel/i/r;->a:Lcom/peel/i/a;

    invoke-static {v0}, Lcom/peel/i/a;->c(Lcom/peel/i/a;)Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Lcom/peel/i/r;->a:Lcom/peel/i/a;

    invoke-static {v0}, Lcom/peel/i/a;->x(Lcom/peel/i/a;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    invoke-virtual {v1, v2, v0}, Lcom/peel/data/g;->a(Ljava/lang/String;Ljava/util/Map;)V

    .line 865
    iget-object v0, p0, Lcom/peel/i/r;->a:Lcom/peel/i/a;

    invoke-static {v0}, Lcom/peel/i/a;->x(Lcom/peel/i/a;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    .line 866
    iget-object v0, p0, Lcom/peel/i/r;->a:Lcom/peel/i/a;

    new-instance v1, Lcom/peel/i/ad;

    iget-object v2, p0, Lcom/peel/i/r;->a:Lcom/peel/i/a;

    iget-object v3, p0, Lcom/peel/i/r;->a:Lcom/peel/i/a;

    invoke-static {v3}, Lcom/peel/i/a;->h(Lcom/peel/i/a;)I

    move-result v3

    iget-object v4, p0, Lcom/peel/i/r;->a:Lcom/peel/i/a;

    invoke-static {v4}, Lcom/peel/i/a;->x(Lcom/peel/i/a;)Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    invoke-direct {v1, v2, v3, v4}, Lcom/peel/i/ad;-><init>(Lcom/peel/i/a;II)V

    invoke-static {v0, v1}, Lcom/peel/i/a;->a(Lcom/peel/i/a;Lcom/peel/i/ad;)Lcom/peel/i/ad;

    .line 867
    iget-object v0, p0, Lcom/peel/i/r;->a:Lcom/peel/i/a;

    invoke-static {v0}, Lcom/peel/i/a;->y(Lcom/peel/i/a;)Lcom/peel/widget/TestBtnViewPager;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/i/r;->a:Lcom/peel/i/a;

    invoke-static {v1}, Lcom/peel/i/a;->K(Lcom/peel/i/a;)Lcom/peel/i/ad;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/peel/widget/TestBtnViewPager;->setAdapter(Landroid/support/v4/view/av;)V

    .line 868
    iget-object v0, p0, Lcom/peel/i/r;->a:Lcom/peel/i/a;

    invoke-static {v0}, Lcom/peel/i/a;->y(Lcom/peel/i/a;)Lcom/peel/widget/TestBtnViewPager;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/i/r;->a:Lcom/peel/i/a;

    invoke-virtual {v1}, Lcom/peel/i/a;->n()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/peel/ui/fn;->test_btn_pager_margin:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    mul-int/lit8 v1, v1, 0x2

    iget-object v2, p0, Lcom/peel/i/r;->a:Lcom/peel/i/a;

    invoke-virtual {v2}, Lcom/peel/i/a;->n()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/peel/ui/fn;->test_btn_pager_margin:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v2

    mul-int/lit8 v2, v2, 0x2

    invoke-virtual {v0, v1, v5, v2, v5}, Lcom/peel/widget/TestBtnViewPager;->setPadding(IIII)V

    .line 869
    iget-object v0, p0, Lcom/peel/i/r;->a:Lcom/peel/i/a;

    invoke-static {v0}, Lcom/peel/i/a;->y(Lcom/peel/i/a;)Lcom/peel/widget/TestBtnViewPager;

    move-result-object v0

    invoke-virtual {v0, v5}, Lcom/peel/widget/TestBtnViewPager;->setClipToPadding(Z)V

    .line 870
    iget-object v0, p0, Lcom/peel/i/r;->a:Lcom/peel/i/a;

    invoke-static {v0}, Lcom/peel/i/a;->y(Lcom/peel/i/a;)Lcom/peel/widget/TestBtnViewPager;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/i/r;->a:Lcom/peel/i/a;

    invoke-virtual {v1}, Lcom/peel/i/a;->n()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/peel/ui/fn;->test_btn_pager_margin:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/peel/widget/TestBtnViewPager;->setPageMargin(I)V

    .line 871
    iget-object v0, p0, Lcom/peel/i/r;->a:Lcom/peel/i/a;

    invoke-static {v0}, Lcom/peel/i/a;->y(Lcom/peel/i/a;)Lcom/peel/widget/TestBtnViewPager;

    move-result-object v0

    invoke-virtual {v0, v5}, Lcom/peel/widget/TestBtnViewPager;->setClipChildren(Z)V

    .line 872
    iget-object v0, p0, Lcom/peel/i/r;->a:Lcom/peel/i/a;

    invoke-static {v0}, Lcom/peel/i/a;->y(Lcom/peel/i/a;)Lcom/peel/widget/TestBtnViewPager;

    move-result-object v0

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/peel/widget/TestBtnViewPager;->setOffscreenPageLimit(I)V

    .line 873
    iget-object v0, p0, Lcom/peel/i/r;->a:Lcom/peel/i/a;

    invoke-static {v0}, Lcom/peel/i/a;->y(Lcom/peel/i/a;)Lcom/peel/widget/TestBtnViewPager;

    move-result-object v0

    invoke-virtual {v0, v5}, Lcom/peel/widget/TestBtnViewPager;->setVisibility(I)V

    .line 875
    iget-object v0, p0, Lcom/peel/i/r;->a:Lcom/peel/i/a;

    invoke-static {v0}, Lcom/peel/i/a;->I(Lcom/peel/i/a;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/widget/Button;->setEnabled(Z)V

    .line 876
    iget-object v0, p0, Lcom/peel/i/r;->a:Lcom/peel/i/a;

    invoke-static {v0}, Lcom/peel/i/a;->J(Lcom/peel/i/a;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/widget/Button;->setEnabled(Z)V

    .line 879
    :cond_4
    iget-object v0, p0, Lcom/peel/i/r;->a:Lcom/peel/i/a;

    invoke-static {v0}, Lcom/peel/i/a;->h(Lcom/peel/i/a;)I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_5

    .line 880
    iget-object v0, p0, Lcom/peel/i/r;->a:Lcom/peel/i/a;

    invoke-static {v0}, Lcom/peel/i/a;->y(Lcom/peel/i/a;)Lcom/peel/widget/TestBtnViewPager;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "btnView"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/peel/i/r;->a:Lcom/peel/i/a;

    invoke-static {v2}, Lcom/peel/i/a;->L(Lcom/peel/i/a;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/peel/widget/TestBtnViewPager;->findViewWithTag(Ljava/lang/Object;)Landroid/view/View;

    move-result-object v0

    sget v1, Lcom/peel/ui/fp;->test_other_btn_large_view:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    .line 881
    iget-object v0, p0, Lcom/peel/i/r;->a:Lcom/peel/i/a;

    invoke-static {v0}, Lcom/peel/i/a;->y(Lcom/peel/i/a;)Lcom/peel/widget/TestBtnViewPager;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "btnView"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/peel/i/r;->a:Lcom/peel/i/a;

    invoke-static {v2}, Lcom/peel/i/a;->L(Lcom/peel/i/a;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/peel/widget/TestBtnViewPager;->findViewWithTag(Ljava/lang/Object;)Landroid/view/View;

    move-result-object v0

    sget v1, Lcom/peel/ui/fp;->test_other_btn_small_view:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v7}, Landroid/view/View;->setVisibility(I)V

    .line 887
    :goto_1
    iget-object v0, p0, Lcom/peel/i/r;->a:Lcom/peel/i/a;

    invoke-static {v0}, Lcom/peel/i/a;->y(Lcom/peel/i/a;)Lcom/peel/widget/TestBtnViewPager;

    move-result-object v0

    new-instance v1, Lcom/peel/i/s;

    invoke-direct {v1, p0}, Lcom/peel/i/s;-><init>(Lcom/peel/i/r;)V

    invoke-virtual {v0, v1}, Lcom/peel/widget/TestBtnViewPager;->setOnPageChangeListener(Landroid/support/v4/view/cs;)V

    .line 951
    iget-object v0, p0, Lcom/peel/i/r;->a:Lcom/peel/i/a;

    invoke-static {v0}, Lcom/peel/i/a;->N(Lcom/peel/i/a;)V

    goto/16 :goto_0

    .line 883
    :cond_5
    iget-object v0, p0, Lcom/peel/i/r;->a:Lcom/peel/i/a;

    invoke-static {v0}, Lcom/peel/i/a;->y(Lcom/peel/i/a;)Lcom/peel/widget/TestBtnViewPager;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "btnView"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/peel/i/r;->a:Lcom/peel/i/a;

    invoke-static {v2}, Lcom/peel/i/a;->L(Lcom/peel/i/a;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/peel/widget/TestBtnViewPager;->findViewWithTag(Ljava/lang/Object;)Landroid/view/View;

    move-result-object v0

    sget v1, Lcom/peel/ui/fp;->test_pw_btn_large_view:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    .line 884
    iget-object v0, p0, Lcom/peel/i/r;->a:Lcom/peel/i/a;

    invoke-static {v0}, Lcom/peel/i/a;->y(Lcom/peel/i/a;)Lcom/peel/widget/TestBtnViewPager;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "btnView"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/peel/i/r;->a:Lcom/peel/i/a;

    invoke-static {v2}, Lcom/peel/i/a;->L(Lcom/peel/i/a;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/peel/widget/TestBtnViewPager;->findViewWithTag(Ljava/lang/Object;)Landroid/view/View;

    move-result-object v0

    sget v1, Lcom/peel/ui/fp;->test_pw_btn_small_view:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v7}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1
.end method
