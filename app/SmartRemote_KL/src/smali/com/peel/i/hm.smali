.class Lcom/peel/i/hm;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field final synthetic a:Lcom/peel/i/gm;


# direct methods
.method constructor <init>(Lcom/peel/i/gm;)V
    .locals 0

    .prologue
    .line 1518
    iput-object p1, p0, Lcom/peel/i/hm;->a:Lcom/peel/i/gm;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    .line 1521
    iget-object v0, p0, Lcom/peel/i/hm;->a:Lcom/peel/i/gm;

    invoke-static {v0, v3}, Lcom/peel/i/gm;->g(Lcom/peel/i/gm;Z)Z

    .line 1522
    iget-object v0, p0, Lcom/peel/i/hm;->a:Lcom/peel/i/gm;

    invoke-virtual {v0}, Lcom/peel/i/gm;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    const-string/jumbo v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/ae;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 1523
    iget-object v1, p0, Lcom/peel/i/hm;->a:Lcom/peel/i/gm;

    invoke-static {v1}, Lcom/peel/i/gm;->ap(Lcom/peel/i/gm;)Landroid/widget/AutoCompleteTextView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/AutoCompleteTextView;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 1525
    iget-object v0, p0, Lcom/peel/i/hm;->a:Lcom/peel/i/gm;

    invoke-static {v0}, Lcom/peel/i/gm;->ao(Lcom/peel/i/gm;)Landroid/widget/ListView;

    move-result-object v0

    invoke-static {}, Lcom/peel/i/gm;->c()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/peel/util/bx;->a(Landroid/view/View;Ljava/lang/String;)V

    .line 1526
    iget-object v1, p0, Lcom/peel/i/hm;->a:Lcom/peel/i/gm;

    invoke-virtual {p1}, Landroid/widget/AdapterView;->getAdapter()Landroid/widget/Adapter;

    move-result-object v0

    invoke-interface {v0, p3}, Landroid/widget/Adapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/f/a;

    invoke-static {v1, v0}, Lcom/peel/i/gm;->b(Lcom/peel/i/gm;Lcom/peel/f/a;)Lcom/peel/f/a;

    .line 1527
    iget-object v0, p0, Lcom/peel/i/hm;->a:Lcom/peel/i/gm;

    invoke-static {v0}, Lcom/peel/i/gm;->aq(Lcom/peel/i/gm;)Lcom/peel/i/a/a;

    move-result-object v0

    invoke-virtual {v0, p3}, Lcom/peel/i/a/a;->b(I)V

    .line 1528
    iget-object v0, p0, Lcom/peel/i/hm;->a:Lcom/peel/i/gm;

    iget-object v1, p0, Lcom/peel/i/hm;->a:Lcom/peel/i/gm;

    sget v2, Lcom/peel/ui/ft;->action_bar_title_stb:I

    invoke-virtual {v1, v2}, Lcom/peel/i/gm;->a(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/peel/i/hm;->a:Lcom/peel/i/gm;

    invoke-static {v2}, Lcom/peel/i/gm;->ar(Lcom/peel/i/gm;)Z

    move-result v2

    invoke-static {v0, v1, v3, v2}, Lcom/peel/i/gm;->a(Lcom/peel/i/gm;Ljava/lang/String;ZZ)V

    .line 1529
    return-void
.end method
