.class Lcom/peel/i/gn;
.super Landroid/os/Handler;


# instance fields
.field final synthetic a:Lcom/peel/i/gm;


# direct methods
.method constructor <init>(Lcom/peel/i/gm;)V
    .locals 0

    .prologue
    .line 174
    iput-object p1, p0, Lcom/peel/i/gn;->a:Lcom/peel/i/gm;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v1, 0x0

    const/4 v7, 0x0

    .line 181
    invoke-static {}, Lcom/peel/i/gm;->c()Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "what: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p1, Landroid/os/Message;->what:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " -- arg1: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p1, Landroid/os/Message;->arg1:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " -- arg2: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p1, Landroid/os/Message;->arg2:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 182
    iget v0, p1, Landroid/os/Message;->what:I

    if-nez v0, :cond_a

    .line 184
    iget v0, p1, Landroid/os/Message;->arg1:I

    const/16 v2, -0x9

    if-eq v0, v2, :cond_0

    iget v0, p1, Landroid/os/Message;->arg2:I

    const/4 v2, 0x5

    if-ne v0, v2, :cond_2

    .line 187
    :cond_0
    :goto_0
    iget v0, p1, Landroid/os/Message;->arg1:I

    packed-switch v0, :pswitch_data_0

    .line 408
    :cond_1
    :goto_1
    :pswitch_0
    return-void

    .line 186
    :cond_2
    iget-object v0, p0, Lcom/peel/i/gn;->a:Lcom/peel/i/gm;

    invoke-static {v0}, Lcom/peel/i/gm;->a(Lcom/peel/i/gm;)Ljava/util/Stack;

    move-result-object v0

    new-instance v2, Lcom/peel/i/im;

    iget-object v3, p0, Lcom/peel/i/gn;->a:Lcom/peel/i/gm;

    iget v4, p1, Landroid/os/Message;->arg1:I

    iget v5, p1, Landroid/os/Message;->arg2:I

    iget-object v6, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/peel/i/im;-><init>(Lcom/peel/i/gm;IILjava/lang/Object;)V

    invoke-virtual {v0, v2}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 191
    :pswitch_1
    invoke-static {}, Lcom/peel/i/gm;->c()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "SCREEN_ANY no op"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 201
    :pswitch_2
    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    iget-object v2, p0, Lcom/peel/i/gn;->a:Lcom/peel/i/gm;

    invoke-static {v2}, Lcom/peel/i/gm;->b(Lcom/peel/i/gm;)Lcom/peel/control/RoomControl;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/peel/control/am;->c(Lcom/peel/control/RoomControl;)[Lcom/peel/control/h;

    move-result-object v2

    .line 202
    if-eqz v2, :cond_3

    array-length v0, v2

    if-lez v0, :cond_3

    .line 203
    array-length v3, v2

    move v0, v1

    :goto_2
    if-ge v0, v3, :cond_3

    aget-object v4, v2, v0

    .line 204
    invoke-virtual {v4}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v5

    invoke-virtual {v5}, Lcom/peel/data/g;->d()I

    move-result v5

    if-ne v5, v8, :cond_4

    .line 205
    iget-object v0, p0, Lcom/peel/i/gn;->a:Lcom/peel/i/gm;

    invoke-static {v0, v4}, Lcom/peel/i/gm;->a(Lcom/peel/i/gm;Lcom/peel/control/h;)Lcom/peel/control/h;

    .line 211
    :cond_3
    iget-object v0, p0, Lcom/peel/i/gn;->a:Lcom/peel/i/gm;

    invoke-static {v0}, Lcom/peel/i/gm;->c(Lcom/peel/i/gm;)Lcom/peel/control/h;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 212
    iget-object v0, p0, Lcom/peel/i/gn;->a:Lcom/peel/i/gm;

    invoke-static {v0}, Lcom/peel/i/gm;->d(Lcom/peel/i/gm;)Lcom/peel/content/library/LiveLibrary;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/content/library/LiveLibrary;->j()Ljava/lang/String;

    move-result-object v0

    .line 213
    iget-object v2, p0, Lcom/peel/i/gn;->a:Lcom/peel/i/gm;

    invoke-static {v0}, Lcom/peel/control/a;->a(Ljava/lang/String;)Lcom/peel/control/a;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/peel/i/gm;->a(Lcom/peel/i/gm;Lcom/peel/control/a;)Lcom/peel/control/a;

    .line 214
    iget-object v0, p0, Lcom/peel/i/gn;->a:Lcom/peel/i/gm;

    invoke-static {v0}, Lcom/peel/i/gm;->b(Lcom/peel/i/gm;)Lcom/peel/control/RoomControl;

    move-result-object v0

    iget-object v2, p0, Lcom/peel/i/gn;->a:Lcom/peel/i/gm;

    invoke-static {v2}, Lcom/peel/i/gm;->e(Lcom/peel/i/gm;)Lcom/peel/control/a;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/peel/control/RoomControl;->a(Lcom/peel/control/a;)V

    .line 216
    iget-object v0, p0, Lcom/peel/i/gn;->a:Lcom/peel/i/gm;

    new-instance v2, Lcom/peel/f/a;

    iget-object v3, p0, Lcom/peel/i/gn;->a:Lcom/peel/i/gm;

    invoke-static {v3}, Lcom/peel/i/gm;->c(Lcom/peel/i/gm;)Lcom/peel/control/h;

    move-result-object v3

    invoke-virtual {v3}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v3

    invoke-virtual {v3}, Lcom/peel/data/g;->f()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v1, v3, v1}, Lcom/peel/f/a;-><init>(ILjava/lang/String;I)V

    invoke-static {v0, v2}, Lcom/peel/i/gm;->a(Lcom/peel/i/gm;Lcom/peel/f/a;)Lcom/peel/f/a;

    .line 217
    iget-object v0, p0, Lcom/peel/i/gn;->a:Lcom/peel/i/gm;

    invoke-static {v0}, Lcom/peel/i/gm;->f(Lcom/peel/i/gm;)V

    goto/16 :goto_1

    .line 203
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 219
    :cond_5
    sget-boolean v0, Lcom/peel/util/a;->l:Z

    if-eqz v0, :cond_6

    .line 220
    iget-object v0, p0, Lcom/peel/i/gn;->a:Lcom/peel/i/gm;

    invoke-static {v0}, Lcom/peel/i/gm;->g(Lcom/peel/i/gm;)V

    goto/16 :goto_1

    .line 222
    :cond_6
    iget-object v0, p0, Lcom/peel/i/gn;->a:Lcom/peel/i/gm;

    invoke-static {v0}, Lcom/peel/i/gm;->h(Lcom/peel/i/gm;)V

    goto/16 :goto_1

    .line 227
    :pswitch_3
    iget-object v0, p0, Lcom/peel/i/gn;->a:Lcom/peel/i/gm;

    invoke-static {v0}, Lcom/peel/i/gm;->g(Lcom/peel/i/gm;)V

    goto/16 :goto_1

    .line 230
    :pswitch_4
    iget v0, p1, Landroid/os/Message;->arg2:I

    packed-switch v0, :pswitch_data_1

    :pswitch_5
    goto/16 :goto_1

    .line 239
    :pswitch_6
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    sget v1, Lcom/peel/ui/fp;->other_tv_brand_btn:I

    if-ne v0, v1, :cond_7

    .line 240
    iget-object v0, p0, Lcom/peel/i/gn;->a:Lcom/peel/i/gm;

    invoke-static {v0}, Lcom/peel/i/gm;->k(Lcom/peel/i/gm;)V

    goto/16 :goto_1

    .line 232
    :pswitch_7
    iget-object v0, p0, Lcom/peel/i/gn;->a:Lcom/peel/i/gm;

    invoke-static {v0}, Lcom/peel/i/gm;->i(Lcom/peel/i/gm;)V

    goto/16 :goto_1

    .line 235
    :pswitch_8
    iget-object v0, p0, Lcom/peel/i/gn;->a:Lcom/peel/i/gm;

    invoke-static {v0}, Lcom/peel/i/gm;->a(Lcom/peel/i/gm;)Ljava/util/Stack;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/i/gn;->a:Lcom/peel/i/gm;

    invoke-static {v1}, Lcom/peel/i/gm;->a(Lcom/peel/i/gm;)Ljava/util/Stack;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Stack;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Ljava/util/Stack;->removeElementAt(I)V

    .line 236
    iget-object v0, p0, Lcom/peel/i/gn;->a:Lcom/peel/i/gm;

    invoke-static {v0}, Lcom/peel/i/gm;->j(Lcom/peel/i/gm;)V

    goto/16 :goto_1

    .line 242
    :cond_7
    iget-object v0, p0, Lcom/peel/i/gn;->a:Lcom/peel/i/gm;

    invoke-static {v0}, Lcom/peel/i/gm;->l(Lcom/peel/i/gm;)V

    goto/16 :goto_1

    .line 248
    :pswitch_9
    iget-object v0, p0, Lcom/peel/i/gn;->a:Lcom/peel/i/gm;

    invoke-static {v0}, Lcom/peel/i/gm;->i(Lcom/peel/i/gm;)V

    goto/16 :goto_1

    .line 251
    :pswitch_a
    iget v0, p1, Landroid/os/Message;->arg2:I

    packed-switch v0, :pswitch_data_2

    goto/16 :goto_1

    .line 253
    :pswitch_b
    iget-object v0, p0, Lcom/peel/i/gn;->a:Lcom/peel/i/gm;

    invoke-static {v0}, Lcom/peel/i/gm;->c(Lcom/peel/i/gm;)Lcom/peel/control/h;

    move-result-object v0

    if-eqz v0, :cond_9

    .line 255
    iget-object v0, p0, Lcom/peel/i/gn;->a:Lcom/peel/i/gm;

    invoke-static {v0}, Lcom/peel/i/gm;->e(Lcom/peel/i/gm;)Lcom/peel/control/a;

    move-result-object v0

    if-eqz v0, :cond_8

    .line 256
    iget-object v0, p0, Lcom/peel/i/gn;->a:Lcom/peel/i/gm;

    invoke-static {v0}, Lcom/peel/i/gm;->e(Lcom/peel/i/gm;)Lcom/peel/control/a;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/i/gn;->a:Lcom/peel/i/gm;

    invoke-static {v1}, Lcom/peel/i/gm;->c(Lcom/peel/i/gm;)Lcom/peel/control/h;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/peel/control/a;->c(Lcom/peel/control/h;)V

    .line 258
    :cond_8
    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    iget-object v1, p0, Lcom/peel/i/gn;->a:Lcom/peel/i/gm;

    invoke-static {v1}, Lcom/peel/i/gm;->c(Lcom/peel/i/gm;)Lcom/peel/control/h;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/peel/control/am;->b(Lcom/peel/control/h;)V

    .line 259
    iget-object v0, p0, Lcom/peel/i/gn;->a:Lcom/peel/i/gm;

    invoke-static {v0, v7}, Lcom/peel/i/gm;->a(Lcom/peel/i/gm;Lcom/peel/control/h;)Lcom/peel/control/h;

    .line 261
    :cond_9
    new-instance v5, Landroid/os/Bundle;

    invoke-direct {v5}, Landroid/os/Bundle;-><init>()V

    .line 262
    const-string/jumbo v0, "child_idx"

    const/4 v1, 0x2

    invoke-virtual {v5, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 263
    iget-object v0, p0, Lcom/peel/i/gn;->a:Lcom/peel/i/gm;

    invoke-virtual {v0}, Lcom/peel/i/gm;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/i/gn;->a:Lcom/peel/i/gm;

    iget-object v1, v1, Lcom/peel/i/gm;->b:Lcom/peel/d/i;

    iget-object v2, p0, Lcom/peel/i/gn;->a:Lcom/peel/i/gm;

    invoke-static {v2}, Lcom/peel/i/gm;->m(Lcom/peel/i/gm;)Lcom/peel/f/a;

    move-result-object v2

    invoke-virtual {v2}, Lcom/peel/f/a;->b()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/peel/i/gn;->a:Lcom/peel/i/gm;

    invoke-static {v3}, Lcom/peel/i/gm;->n(Lcom/peel/i/gm;)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/peel/i/gn;->a:Lcom/peel/i/gm;

    invoke-static {v4}, Lcom/peel/i/gm;->o(Lcom/peel/i/gm;)Ljava/lang/String;

    move-result-object v4

    invoke-static {}, Lcom/peel/i/gm;->c()Ljava/lang/String;

    move-result-object v6

    invoke-static/range {v0 .. v6}, Lcom/peel/util/bx;->a(Landroid/content/Context;Lcom/peel/d/i;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 266
    :pswitch_c
    iget-object v0, p0, Lcom/peel/i/gn;->a:Lcom/peel/i/gm;

    invoke-static {v0}, Lcom/peel/i/gm;->f(Lcom/peel/i/gm;)V

    goto/16 :goto_1

    .line 271
    :pswitch_d
    iget v0, p1, Landroid/os/Message;->arg2:I

    sparse-switch v0, :sswitch_data_0

    goto/16 :goto_1

    .line 276
    :sswitch_0
    iget-object v0, p0, Lcom/peel/i/gn;->a:Lcom/peel/i/gm;

    invoke-static {v0, v7}, Lcom/peel/i/gm;->a(Lcom/peel/i/gm;Ljava/lang/String;)Ljava/lang/String;

    .line 277
    iget-object v0, p0, Lcom/peel/i/gn;->a:Lcom/peel/i/gm;

    invoke-static {v0}, Lcom/peel/i/gm;->j(Lcom/peel/i/gm;)V

    goto/16 :goto_1

    .line 273
    :sswitch_1
    iget-object v0, p0, Lcom/peel/i/gn;->a:Lcom/peel/i/gm;

    invoke-static {v0}, Lcom/peel/i/gm;->p(Lcom/peel/i/gm;)V

    goto/16 :goto_1

    .line 282
    :pswitch_e
    iget v0, p1, Landroid/os/Message;->arg2:I

    packed-switch v0, :pswitch_data_3

    goto/16 :goto_1

    .line 287
    :pswitch_f
    iget-object v0, p0, Lcom/peel/i/gn;->a:Lcom/peel/i/gm;

    invoke-static {v0}, Lcom/peel/i/gm;->r(Lcom/peel/i/gm;)V

    goto/16 :goto_1

    .line 284
    :pswitch_10
    iget-object v0, p0, Lcom/peel/i/gn;->a:Lcom/peel/i/gm;

    invoke-static {v0}, Lcom/peel/i/gm;->q(Lcom/peel/i/gm;)V

    goto/16 :goto_1

    .line 292
    :pswitch_11
    iget-object v0, p0, Lcom/peel/i/gn;->a:Lcom/peel/i/gm;

    invoke-static {v0}, Lcom/peel/i/gm;->a(Lcom/peel/i/gm;)Ljava/util/Stack;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/i/gn;->a:Lcom/peel/i/gm;

    invoke-static {v1}, Lcom/peel/i/gm;->a(Lcom/peel/i/gm;)Ljava/util/Stack;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Stack;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Ljava/util/Stack;->removeElementAt(I)V

    .line 293
    iget-object v0, p0, Lcom/peel/i/gn;->a:Lcom/peel/i/gm;

    invoke-static {v0}, Lcom/peel/i/gm;->q(Lcom/peel/i/gm;)V

    goto/16 :goto_1

    .line 296
    :pswitch_12
    iget v0, p1, Landroid/os/Message;->arg2:I

    sparse-switch v0, :sswitch_data_1

    goto/16 :goto_1

    .line 298
    :sswitch_2
    new-instance v5, Landroid/os/Bundle;

    invoke-direct {v5}, Landroid/os/Bundle;-><init>()V

    .line 299
    const-string/jumbo v0, "child_idx"

    const/4 v1, 0x7

    invoke-virtual {v5, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 302
    iget-object v0, p0, Lcom/peel/i/gn;->a:Lcom/peel/i/gm;

    invoke-virtual {v0}, Lcom/peel/i/gm;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/i/gn;->a:Lcom/peel/i/gm;

    iget-object v1, v1, Lcom/peel/i/gm;->b:Lcom/peel/d/i;

    iget-object v2, p0, Lcom/peel/i/gn;->a:Lcom/peel/i/gm;

    invoke-static {v2}, Lcom/peel/i/gm;->s(Lcom/peel/i/gm;)Lcom/peel/f/a;

    move-result-object v2

    invoke-virtual {v2}, Lcom/peel/f/a;->b()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/peel/i/gn;->a:Lcom/peel/i/gm;

    invoke-static {v3}, Lcom/peel/i/gm;->t(Lcom/peel/i/gm;)Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v4, "Channel_Up"

    invoke-static {}, Lcom/peel/i/gm;->c()Ljava/lang/String;

    move-result-object v6

    invoke-static/range {v0 .. v6}, Lcom/peel/util/bx;->a(Landroid/content/Context;Lcom/peel/d/i;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 305
    :sswitch_3
    iget-object v0, p0, Lcom/peel/i/gn;->a:Lcom/peel/i/gm;

    invoke-static {v0}, Lcom/peel/i/gm;->p(Lcom/peel/i/gm;)V

    goto/16 :goto_1

    .line 310
    :cond_a
    iget v0, p1, Landroid/os/Message;->what:I

    if-ne v0, v8, :cond_1

    iget-object v0, p0, Lcom/peel/i/gn;->a:Lcom/peel/i/gm;

    invoke-static {v0}, Lcom/peel/i/gm;->a(Lcom/peel/i/gm;)Ljava/util/Stack;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Stack;->empty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 311
    iget-object v0, p0, Lcom/peel/i/gn;->a:Lcom/peel/i/gm;

    invoke-static {v0}, Lcom/peel/i/gm;->a(Lcom/peel/i/gm;)Ljava/util/Stack;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/i/im;

    iget v0, v0, Lcom/peel/i/im;->b:I

    const/16 v2, 0x63

    if-ne v0, v2, :cond_b

    .line 312
    invoke-static {}, Lcom/peel/i/gm;->c()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, ".... do nothing for SCREEN_PERSONALIZE_NO_THANKS"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 315
    :cond_b
    iget-object v0, p0, Lcom/peel/i/gn;->a:Lcom/peel/i/gm;

    invoke-static {v0}, Lcom/peel/i/gm;->a(Lcom/peel/i/gm;)Ljava/util/Stack;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/i/im;

    .line 317
    invoke-static {}, Lcom/peel/i/gm;->c()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "\n**** back flow from screen: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, v0, Lcom/peel/i/im;->a:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 319
    iget v0, v0, Lcom/peel/i/im;->a:I

    packed-switch v0, :pswitch_data_4

    :pswitch_13
    goto/16 :goto_1

    .line 325
    :pswitch_14
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "\n**** SCREEN ONE HERE, main.back()\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v0, p0, Lcom/peel/i/gn;->a:Lcom/peel/i/gm;

    invoke-static {v0}, Lcom/peel/i/gm;->e(Lcom/peel/i/gm;)Lcom/peel/control/a;

    move-result-object v0

    if-nez v0, :cond_c

    const-string/jumbo v0, "NULL TV ACTIVITY"

    :goto_3
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 326
    invoke-static {}, Lcom/peel/i/gm;->c()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 328
    invoke-static {}, Lcom/peel/i/gm;->c()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/i/gn;->a:Lcom/peel/i/gm;

    invoke-virtual {v1}, Lcom/peel/i/gm;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/peel/d/e;->a(Ljava/lang/String;Landroid/support/v4/app/ae;)V

    .line 330
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 331
    const-string/jumbo v1, "from_tv_setup"

    invoke-virtual {v0, v1, v8}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 332
    iget-object v1, p0, Lcom/peel/i/gn;->a:Lcom/peel/i/gm;

    invoke-virtual {v1}, Lcom/peel/i/gm;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    const-class v2, Lcom/peel/i/fv;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lcom/peel/d/e;->d(Landroid/support/v4/app/ae;Ljava/lang/String;Landroid/os/Bundle;)V

    goto/16 :goto_1

    .line 325
    :cond_c
    iget-object v0, p0, Lcom/peel/i/gn;->a:Lcom/peel/i/gm;

    invoke-static {v0}, Lcom/peel/i/gm;->e(Lcom/peel/i/gm;)Lcom/peel/control/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/control/a;->a()Ljava/lang/String;

    move-result-object v0

    goto :goto_3

    .line 335
    :pswitch_15
    iget-object v0, p0, Lcom/peel/i/gn;->a:Lcom/peel/i/gm;

    invoke-static {v0}, Lcom/peel/i/gm;->u(Lcom/peel/i/gm;)V

    goto/16 :goto_1

    .line 338
    :pswitch_16
    iget-object v0, p0, Lcom/peel/i/gn;->a:Lcom/peel/i/gm;

    invoke-static {v0}, Lcom/peel/i/gm;->h(Lcom/peel/i/gm;)V

    goto/16 :goto_1

    .line 341
    :pswitch_17
    iget-object v0, p0, Lcom/peel/i/gn;->a:Lcom/peel/i/gm;

    invoke-static {v0}, Lcom/peel/i/gm;->c(Lcom/peel/i/gm;)Lcom/peel/control/h;

    move-result-object v0

    if-eqz v0, :cond_e

    .line 343
    iget-object v0, p0, Lcom/peel/i/gn;->a:Lcom/peel/i/gm;

    invoke-static {v0}, Lcom/peel/i/gm;->e(Lcom/peel/i/gm;)Lcom/peel/control/a;

    move-result-object v0

    if-eqz v0, :cond_d

    .line 344
    iget-object v0, p0, Lcom/peel/i/gn;->a:Lcom/peel/i/gm;

    invoke-static {v0}, Lcom/peel/i/gm;->e(Lcom/peel/i/gm;)Lcom/peel/control/a;

    move-result-object v0

    iget-object v2, p0, Lcom/peel/i/gn;->a:Lcom/peel/i/gm;

    invoke-static {v2}, Lcom/peel/i/gm;->c(Lcom/peel/i/gm;)Lcom/peel/control/h;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/peel/control/a;->c(Lcom/peel/control/h;)V

    .line 346
    :cond_d
    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    iget-object v2, p0, Lcom/peel/i/gn;->a:Lcom/peel/i/gm;

    invoke-static {v2}, Lcom/peel/i/gm;->c(Lcom/peel/i/gm;)Lcom/peel/control/h;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/peel/control/am;->b(Lcom/peel/control/h;)V

    .line 347
    iget-object v0, p0, Lcom/peel/i/gn;->a:Lcom/peel/i/gm;

    invoke-static {v0, v7}, Lcom/peel/i/gm;->a(Lcom/peel/i/gm;Lcom/peel/control/h;)Lcom/peel/control/h;

    .line 349
    :cond_e
    iget-object v0, p0, Lcom/peel/i/gn;->a:Lcom/peel/i/gm;

    invoke-static {v0}, Lcom/peel/i/gm;->e(Lcom/peel/i/gm;)Lcom/peel/control/a;

    move-result-object v0

    if-eqz v0, :cond_f

    .line 351
    iget-object v0, p0, Lcom/peel/i/gn;->a:Lcom/peel/i/gm;

    invoke-static {v0}, Lcom/peel/i/gm;->b(Lcom/peel/i/gm;)Lcom/peel/control/RoomControl;

    move-result-object v0

    iget-object v2, p0, Lcom/peel/i/gn;->a:Lcom/peel/i/gm;

    invoke-static {v2}, Lcom/peel/i/gm;->e(Lcom/peel/i/gm;)Lcom/peel/control/a;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/peel/control/RoomControl;->b(Lcom/peel/control/a;)V

    .line 353
    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    iget-object v2, p0, Lcom/peel/i/gn;->a:Lcom/peel/i/gm;

    invoke-static {v2}, Lcom/peel/i/gm;->e(Lcom/peel/i/gm;)Lcom/peel/control/a;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/peel/control/am;->a(Lcom/peel/control/a;)V

    .line 354
    iget-object v0, p0, Lcom/peel/i/gn;->a:Lcom/peel/i/gm;

    invoke-static {v0, v7}, Lcom/peel/i/gm;->a(Lcom/peel/i/gm;Lcom/peel/control/a;)Lcom/peel/control/a;

    .line 356
    :cond_f
    iget-object v0, p0, Lcom/peel/i/gn;->a:Lcom/peel/i/gm;

    invoke-static {v0, v1}, Lcom/peel/i/gm;->a(Lcom/peel/i/gm;Z)Z

    .line 357
    iget-object v0, p0, Lcom/peel/i/gn;->a:Lcom/peel/i/gm;

    invoke-static {v0}, Lcom/peel/i/gm;->g(Lcom/peel/i/gm;)V

    goto/16 :goto_1

    .line 360
    :pswitch_18
    iget-object v0, p0, Lcom/peel/i/gn;->a:Lcom/peel/i/gm;

    invoke-static {v0}, Lcom/peel/i/gm;->e(Lcom/peel/i/gm;)Lcom/peel/control/a;

    move-result-object v0

    if-eqz v0, :cond_10

    .line 362
    iget-object v0, p0, Lcom/peel/i/gn;->a:Lcom/peel/i/gm;

    invoke-static {v0}, Lcom/peel/i/gm;->b(Lcom/peel/i/gm;)Lcom/peel/control/RoomControl;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/i/gn;->a:Lcom/peel/i/gm;

    invoke-static {v1}, Lcom/peel/i/gm;->e(Lcom/peel/i/gm;)Lcom/peel/control/a;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/peel/control/RoomControl;->b(Lcom/peel/control/a;)V

    .line 364
    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    iget-object v1, p0, Lcom/peel/i/gn;->a:Lcom/peel/i/gm;

    invoke-static {v1}, Lcom/peel/i/gm;->e(Lcom/peel/i/gm;)Lcom/peel/control/a;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/peel/control/am;->a(Lcom/peel/control/a;)V

    .line 365
    iget-object v0, p0, Lcom/peel/i/gn;->a:Lcom/peel/i/gm;

    invoke-static {v0, v7}, Lcom/peel/i/gm;->a(Lcom/peel/i/gm;Lcom/peel/control/a;)Lcom/peel/control/a;

    .line 367
    :cond_10
    iget-object v0, p0, Lcom/peel/i/gn;->a:Lcom/peel/i/gm;

    invoke-static {v0}, Lcom/peel/i/gm;->v(Lcom/peel/i/gm;)Z

    move-result v0

    if-nez v0, :cond_11

    .line 368
    iget-object v0, p0, Lcom/peel/i/gn;->a:Lcom/peel/i/gm;

    invoke-static {v0}, Lcom/peel/i/gm;->k(Lcom/peel/i/gm;)V

    goto/16 :goto_1

    .line 370
    :cond_11
    iget-object v0, p0, Lcom/peel/i/gn;->a:Lcom/peel/i/gm;

    invoke-static {v0}, Lcom/peel/i/gm;->l(Lcom/peel/i/gm;)V

    goto/16 :goto_1

    .line 374
    :pswitch_19
    iget-object v0, p0, Lcom/peel/i/gn;->a:Lcom/peel/i/gm;

    invoke-static {v0}, Lcom/peel/i/gm;->c(Lcom/peel/i/gm;)Lcom/peel/control/h;

    move-result-object v0

    if-eqz v0, :cond_13

    .line 376
    iget-object v0, p0, Lcom/peel/i/gn;->a:Lcom/peel/i/gm;

    invoke-static {v0}, Lcom/peel/i/gm;->e(Lcom/peel/i/gm;)Lcom/peel/control/a;

    move-result-object v0

    if-eqz v0, :cond_12

    .line 377
    iget-object v0, p0, Lcom/peel/i/gn;->a:Lcom/peel/i/gm;

    invoke-static {v0}, Lcom/peel/i/gm;->e(Lcom/peel/i/gm;)Lcom/peel/control/a;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/i/gn;->a:Lcom/peel/i/gm;

    invoke-static {v1}, Lcom/peel/i/gm;->c(Lcom/peel/i/gm;)Lcom/peel/control/h;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/peel/control/a;->c(Lcom/peel/control/h;)V

    .line 380
    :cond_12
    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    iget-object v1, p0, Lcom/peel/i/gn;->a:Lcom/peel/i/gm;

    invoke-static {v1}, Lcom/peel/i/gm;->c(Lcom/peel/i/gm;)Lcom/peel/control/h;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/peel/control/am;->b(Lcom/peel/control/h;)V

    .line 381
    iget-object v0, p0, Lcom/peel/i/gn;->a:Lcom/peel/i/gm;

    invoke-static {v0, v7}, Lcom/peel/i/gm;->a(Lcom/peel/i/gm;Lcom/peel/control/h;)Lcom/peel/control/h;

    .line 383
    :cond_13
    iget-object v0, p0, Lcom/peel/i/gn;->a:Lcom/peel/i/gm;

    invoke-static {v0}, Lcom/peel/i/gm;->e(Lcom/peel/i/gm;)Lcom/peel/control/a;

    move-result-object v0

    if-eqz v0, :cond_14

    .line 384
    iget-object v0, p0, Lcom/peel/i/gn;->a:Lcom/peel/i/gm;

    invoke-static {v0}, Lcom/peel/i/gm;->b(Lcom/peel/i/gm;)Lcom/peel/control/RoomControl;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/i/gn;->a:Lcom/peel/i/gm;

    invoke-static {v1}, Lcom/peel/i/gm;->e(Lcom/peel/i/gm;)Lcom/peel/control/a;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/peel/control/RoomControl;->b(Lcom/peel/control/a;)V

    .line 385
    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    iget-object v1, p0, Lcom/peel/i/gn;->a:Lcom/peel/i/gm;

    invoke-static {v1}, Lcom/peel/i/gm;->e(Lcom/peel/i/gm;)Lcom/peel/control/a;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/peel/control/am;->a(Lcom/peel/control/a;)V

    .line 386
    iget-object v0, p0, Lcom/peel/i/gn;->a:Lcom/peel/i/gm;

    invoke-static {v0, v7}, Lcom/peel/i/gm;->a(Lcom/peel/i/gm;Lcom/peel/control/a;)Lcom/peel/control/a;

    .line 388
    :cond_14
    iget-object v0, p0, Lcom/peel/i/gn;->a:Lcom/peel/i/gm;

    invoke-static {v0}, Lcom/peel/i/gm;->i(Lcom/peel/i/gm;)V

    goto/16 :goto_1

    .line 391
    :pswitch_1a
    iget-object v0, p0, Lcom/peel/i/gn;->a:Lcom/peel/i/gm;

    invoke-static {v0}, Lcom/peel/i/gm;->j(Lcom/peel/i/gm;)V

    goto/16 :goto_1

    .line 395
    :pswitch_1b
    iget-object v0, p0, Lcom/peel/i/gn;->a:Lcom/peel/i/gm;

    invoke-static {v0}, Lcom/peel/i/gm;->w(Lcom/peel/i/gm;)Lcom/peel/control/h;

    move-result-object v0

    if-eqz v0, :cond_15

    .line 396
    iget-object v0, p0, Lcom/peel/i/gn;->a:Lcom/peel/i/gm;

    invoke-static {v0, v7}, Lcom/peel/i/gm;->b(Lcom/peel/i/gm;Lcom/peel/control/h;)Lcom/peel/control/h;

    .line 397
    :cond_15
    iget-object v0, p0, Lcom/peel/i/gn;->a:Lcom/peel/i/gm;

    invoke-static {v0}, Lcom/peel/i/gm;->j(Lcom/peel/i/gm;)V

    goto/16 :goto_1

    .line 401
    :pswitch_1c
    iget-object v0, p0, Lcom/peel/i/gn;->a:Lcom/peel/i/gm;

    invoke-static {v0}, Lcom/peel/i/gm;->e(Lcom/peel/i/gm;)Lcom/peel/control/a;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/i/gn;->a:Lcom/peel/i/gm;

    invoke-static {v1}, Lcom/peel/i/gm;->w(Lcom/peel/i/gm;)Lcom/peel/control/h;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/peel/control/a;->c(Lcom/peel/control/h;)V

    .line 403
    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    iget-object v1, p0, Lcom/peel/i/gn;->a:Lcom/peel/i/gm;

    invoke-static {v1}, Lcom/peel/i/gm;->w(Lcom/peel/i/gm;)Lcom/peel/control/h;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/peel/control/am;->b(Lcom/peel/control/h;)V

    .line 404
    iget-object v0, p0, Lcom/peel/i/gn;->a:Lcom/peel/i/gm;

    invoke-static {v0}, Lcom/peel/i/gm;->q(Lcom/peel/i/gm;)V

    goto/16 :goto_1

    .line 187
    :pswitch_data_0
    .packed-switch -0x9
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_9
        :pswitch_a
        :pswitch_0
        :pswitch_d
        :pswitch_e
        :pswitch_11
        :pswitch_12
    .end packed-switch

    .line 230
    :pswitch_data_1
    .packed-switch 0x3
        :pswitch_6
        :pswitch_7
        :pswitch_5
        :pswitch_5
        :pswitch_8
    .end packed-switch

    .line 251
    :pswitch_data_2
    .packed-switch 0x5
        :pswitch_b
        :pswitch_c
    .end packed-switch

    .line 271
    :sswitch_data_0
    .sparse-switch
        0x7 -> :sswitch_0
        0x63 -> :sswitch_1
    .end sparse-switch

    .line 282
    :pswitch_data_3
    .packed-switch 0x8
        :pswitch_f
        :pswitch_10
    .end packed-switch

    .line 296
    :sswitch_data_1
    .sparse-switch
        0x5 -> :sswitch_2
        0x63 -> :sswitch_3
    .end sparse-switch

    .line 319
    :pswitch_data_4
    .packed-switch -0x1
        :pswitch_14
        :pswitch_15
        :pswitch_16
        :pswitch_17
        :pswitch_18
        :pswitch_19
        :pswitch_13
        :pswitch_1a
        :pswitch_1b
        :pswitch_1b
        :pswitch_1c
    .end packed-switch
.end method
