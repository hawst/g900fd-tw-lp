.class Lcom/peel/i/ih;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field final synthetic a:Lcom/peel/i/gm;


# direct methods
.method constructor <init>(Lcom/peel/i/gm;)V
    .locals 0

    .prologue
    .line 778
    iput-object p1, p0, Lcom/peel/i/ih;->a:Lcom/peel/i/gm;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 0

    .prologue
    .line 796
    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 798
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 3

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 782
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-nez v0, :cond_1

    .line 783
    iget-object v0, p0, Lcom/peel/i/ih;->a:Lcom/peel/i/gm;

    invoke-static {v0}, Lcom/peel/i/gm;->F(Lcom/peel/i/gm;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 784
    iget-object v0, p0, Lcom/peel/i/ih;->a:Lcom/peel/i/gm;

    invoke-static {v0}, Lcom/peel/i/gm;->G(Lcom/peel/i/gm;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 792
    :goto_0
    iget-object v0, p0, Lcom/peel/i/ih;->a:Lcom/peel/i/gm;

    invoke-static {v0}, Lcom/peel/i/gm;->H(Lcom/peel/i/gm;)Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lcom/peel/i/a/a;

    .line 793
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/peel/i/a/a;->getFilter()Landroid/widget/Filter;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/widget/Filter;->filter(Ljava/lang/CharSequence;)V

    .line 794
    :cond_0
    return-void

    .line 787
    :cond_1
    iget-object v0, p0, Lcom/peel/i/ih;->a:Lcom/peel/i/gm;

    invoke-static {v0}, Lcom/peel/i/gm;->G(Lcom/peel/i/gm;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 789
    iget-object v0, p0, Lcom/peel/i/ih;->a:Lcom/peel/i/gm;

    invoke-static {v0}, Lcom/peel/i/gm;->F(Lcom/peel/i/gm;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0
.end method
