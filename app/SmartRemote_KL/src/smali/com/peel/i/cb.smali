.class Lcom/peel/i/cb;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field final synthetic a:Lcom/peel/i/a/c;

.field final synthetic b:Lcom/peel/i/by;


# direct methods
.method constructor <init>(Lcom/peel/i/by;Lcom/peel/i/a/c;)V
    .locals 0

    .prologue
    .line 138
    iput-object p1, p0, Lcom/peel/i/cb;->b:Lcom/peel/i/by;

    iput-object p2, p0, Lcom/peel/i/cb;->a:Lcom/peel/i/a/c;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 140
    iget-object v0, p0, Lcom/peel/i/cb;->b:Lcom/peel/i/by;

    iget-object v0, v0, Lcom/peel/i/by;->b:Lcom/peel/d/i;

    if-nez v0, :cond_0

    .line 244
    :goto_0
    return-void

    .line 143
    :cond_0
    iget-object v0, p0, Lcom/peel/i/cb;->b:Lcom/peel/i/by;

    invoke-static {v0}, Lcom/peel/i/by;->c(Lcom/peel/i/by;)Landroid/widget/ListView;

    move-result-object v0

    invoke-static {}, Lcom/peel/i/by;->c()Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0x7d0

    invoke-static {v0, v1, v2}, Lcom/peel/util/bx;->a(Landroid/view/View;Ljava/lang/String;I)V

    .line 144
    invoke-static {}, Lcom/peel/i/by;->c()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/i/cb;->b:Lcom/peel/i/by;

    invoke-virtual {v1}, Lcom/peel/i/by;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    invoke-static {v0, v1, v4}, Lcom/peel/util/bx;->a(Ljava/lang/String;Landroid/app/Activity;Z)V

    .line 145
    iget-object v0, p0, Lcom/peel/i/cb;->b:Lcom/peel/i/by;

    invoke-virtual {v0}, Lcom/peel/i/by;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    const-string/jumbo v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/ae;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 146
    iget-object v1, p0, Lcom/peel/i/cb;->b:Lcom/peel/i/by;

    invoke-virtual {v1}, Lcom/peel/i/by;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/ae;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    invoke-virtual {v0, v1, v3}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 148
    iget-object v0, p0, Lcom/peel/i/cb;->b:Lcom/peel/i/by;

    invoke-static {v0}, Lcom/peel/i/by;->c(Lcom/peel/i/by;)Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0, p3, v4}, Landroid/widget/ListView;->setItemChecked(IZ)V

    .line 149
    iget-object v0, p0, Lcom/peel/i/cb;->b:Lcom/peel/i/by;

    iget-object v0, v0, Lcom/peel/i/by;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "selected_country"

    invoke-virtual {v0, v1, p3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 151
    iget-object v0, p0, Lcom/peel/i/cb;->a:Lcom/peel/i/a/c;

    invoke-virtual {v0, p3}, Lcom/peel/i/a/c;->a(I)Landroid/os/Bundle;

    move-result-object v1

    .line 153
    const-string/jumbo v0, "US"

    const-string/jumbo v2, "iso"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 154
    sput-boolean v3, Lcom/peel/content/a;->d:Z

    .line 161
    :goto_1
    sget-object v0, Lcom/peel/b/a;->a:Ljava/util/Map;

    const-string/jumbo v2, "endpoint"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    sput-object v0, Lcom/peel/b/a;->e:Ljava/lang/String;

    .line 162
    sget-object v0, Lcom/peel/b/a;->e:Ljava/lang/String;

    invoke-static {v0}, Lcom/peel/content/a/j;->a(Ljava/lang/String;)V

    .line 163
    const-string/jumbo v0, "iso"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/peel/content/a;->e:Ljava/lang/String;

    .line 164
    const-string/jumbo v0, "urloverride"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/peel/util/b/a;->b:Ljava/lang/String;

    .line 167
    iget-object v0, p0, Lcom/peel/i/cb;->b:Lcom/peel/i/by;

    invoke-virtual {v0}, Lcom/peel/i/by;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v2, "config_legacy"

    invoke-static {v1}, Lcom/peel/util/bx;->b(Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v2, "country"

    const-string/jumbo v3, "name"

    invoke-virtual {v1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v2, "country_ISO"

    const-string/jumbo v3, "iso"

    invoke-virtual {v1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 169
    const-string/jumbo v0, "XX"

    .line 170
    const-string/jumbo v2, "XX"

    const-string/jumbo v3, "iso"

    invoke-virtual {v1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 171
    const-string/jumbo v0, "XX"

    .line 186
    :cond_1
    :goto_2
    iget-object v2, p0, Lcom/peel/i/cb;->b:Lcom/peel/i/by;

    invoke-virtual {v2}, Lcom/peel/i/by;->m()Landroid/support/v4/app/ae;

    move-result-object v2

    sget-object v3, Lcom/peel/b/a;->b:Ljava/lang/String;

    invoke-static {v2, v0, v3}, Lcom/peel/util/eg;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 187
    sget-object v2, Lcom/peel/content/a;->e:Ljava/lang/String;

    const-string/jumbo v3, "Handset"

    sget-object v4, Landroid/os/Build;->MODEL:Ljava/lang/String;

    new-instance v5, Lcom/peel/i/cc;

    const/4 v6, 0x2

    invoke-direct {v5, p0, v6, v0, v1}, Lcom/peel/i/cc;-><init>(Lcom/peel/i/cb;ILjava/lang/String;Landroid/os/Bundle;)V

    invoke-static {v0, v2, v3, v4, v5}, Lcom/peel/content/a/j;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/peel/util/t;)V

    goto/16 :goto_0

    .line 155
    :cond_2
    const-string/jumbo v0, "CA"

    const-string/jumbo v2, "iso"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 156
    sput-boolean v3, Lcom/peel/content/a;->d:Z

    goto/16 :goto_1

    .line 158
    :cond_3
    sput-boolean v3, Lcom/peel/content/a;->d:Z

    goto/16 :goto_1

    .line 172
    :cond_4
    const-string/jumbo v2, "usa"

    const-string/jumbo v3, "endpoint"

    invoke-virtual {v1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 173
    const-string/jumbo v0, "US"

    goto :goto_2

    .line 174
    :cond_5
    const-string/jumbo v2, "asia"

    const-string/jumbo v3, "endpoint"

    invoke-virtual {v1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 175
    const-string/jumbo v0, "AS"

    goto :goto_2

    .line 176
    :cond_6
    const-string/jumbo v2, "europe"

    const-string/jumbo v3, "endpoint"

    invoke-virtual {v1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 177
    const-string/jumbo v0, "EU"

    goto :goto_2

    .line 178
    :cond_7
    const-string/jumbo v2, "australia"

    const-string/jumbo v3, "endpoint"

    invoke-virtual {v1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 179
    const-string/jumbo v0, "AU"

    goto/16 :goto_2

    .line 180
    :cond_8
    const-string/jumbo v2, "latin"

    const-string/jumbo v3, "endpoint"

    invoke-virtual {v1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 181
    const-string/jumbo v0, "LA"

    goto/16 :goto_2

    .line 182
    :cond_9
    const-string/jumbo v2, "bramex"

    const-string/jumbo v3, "endpoint"

    invoke-virtual {v1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 183
    const-string/jumbo v0, "BM"

    goto/16 :goto_2
.end method
