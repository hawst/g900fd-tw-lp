.class Lcom/peel/i/hg;
.super Lcom/peel/util/t;


# instance fields
.field final synthetic a:Lcom/peel/i/hf;


# direct methods
.method constructor <init>(Lcom/peel/i/hf;I)V
    .locals 0

    .prologue
    .line 1219
    iput-object p1, p0, Lcom/peel/i/hg;->a:Lcom/peel/i/hf;

    invoke-direct {p0, p2}, Lcom/peel/util/t;-><init>(I)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    const/4 v2, 0x4

    const/4 v3, 0x0

    .line 1222
    invoke-static {}, Lcom/peel/i/gm;->c()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/i/hg;->a:Lcom/peel/i/hf;

    iget-object v1, v1, Lcom/peel/i/hf;->b:Lcom/peel/i/gm;

    invoke-virtual {v1}, Lcom/peel/i/gm;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    invoke-static {v0, v1, v3}, Lcom/peel/util/bx;->a(Ljava/lang/String;Landroid/app/Activity;Z)V

    .line 1223
    sget-boolean v0, Lcom/peel/util/b/a;->a:Z

    if-eqz v0, :cond_0

    .line 1244
    :goto_0
    return-void

    .line 1228
    :cond_0
    iget-boolean v0, p0, Lcom/peel/i/hg;->i:Z

    if-nez v0, :cond_1

    .line 1229
    invoke-static {}, Lcom/peel/i/gm;->c()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "unable to get all possible ir by func name: Power for device type: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/peel/i/hg;->a:Lcom/peel/i/hf;

    iget-object v2, v2, Lcom/peel/i/hf;->b:Lcom/peel/i/gm;

    invoke-static {v2}, Lcom/peel/i/gm;->D(Lcom/peel/i/gm;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " -- brand: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/peel/i/hg;->a:Lcom/peel/i/hf;

    iget-object v2, v2, Lcom/peel/i/hf;->b:Lcom/peel/i/gm;

    invoke-static {v2}, Lcom/peel/i/gm;->m(Lcom/peel/i/gm;)Lcom/peel/f/a;

    move-result-object v2

    invoke-virtual {v2}, Lcom/peel/f/a;->a()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1230
    invoke-static {}, Lcom/peel/i/gm;->c()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/i/hg;->a:Lcom/peel/i/hf;

    iget-object v1, v1, Lcom/peel/i/hf;->b:Lcom/peel/i/gm;

    invoke-virtual {v1}, Lcom/peel/i/gm;->m()Landroid/support/v4/app/ae;

    move-result-object v1

    invoke-static {v0, v1, v3}, Lcom/peel/util/bx;->a(Ljava/lang/String;Landroid/app/Activity;Z)V

    .line 1231
    iget-object v0, p0, Lcom/peel/i/hg;->a:Lcom/peel/i/hf;

    iget-object v0, v0, Lcom/peel/i/hf;->b:Lcom/peel/i/gm;

    invoke-virtual {v0}, Lcom/peel/i/gm;->m()Landroid/support/v4/app/ae;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/i/hg;->a:Lcom/peel/i/hf;

    iget-object v1, v1, Lcom/peel/i/hf;->b:Lcom/peel/i/gm;

    iget-object v1, v1, Lcom/peel/i/gm;->b:Lcom/peel/d/i;

    iget-object v2, p0, Lcom/peel/i/hg;->a:Lcom/peel/i/hf;

    iget-object v2, v2, Lcom/peel/i/hf;->b:Lcom/peel/i/gm;

    invoke-static {v2}, Lcom/peel/i/gm;->s(Lcom/peel/i/gm;)Lcom/peel/f/a;

    move-result-object v2

    invoke-virtual {v2}, Lcom/peel/f/a;->b()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/peel/i/hg;->a:Lcom/peel/i/hf;

    iget-object v3, v3, Lcom/peel/i/hf;->b:Lcom/peel/i/gm;

    invoke-virtual {v3}, Lcom/peel/i/gm;->m()Landroid/support/v4/app/ae;

    move-result-object v3

    iget-object v4, p0, Lcom/peel/i/hg;->a:Lcom/peel/i/hf;

    iget-object v4, v4, Lcom/peel/i/hf;->b:Lcom/peel/i/gm;

    invoke-static {v4}, Lcom/peel/i/gm;->D(Lcom/peel/i/gm;)I

    move-result v4

    invoke-static {v3, v4}, Lcom/peel/util/bx;->a(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/peel/i/hg;->a:Lcom/peel/i/hf;

    iget-object v4, v4, Lcom/peel/i/hf;->b:Lcom/peel/i/gm;

    invoke-static {v4}, Lcom/peel/i/gm;->o(Lcom/peel/i/gm;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v1, v2, v3, v4}, Lcom/peel/util/bx;->a(Landroid/content/Context;Lcom/peel/d/i;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1235
    :cond_1
    iget-object v0, p0, Lcom/peel/i/hg;->a:Lcom/peel/i/hf;

    iget-object v1, v0, Lcom/peel/i/hf;->b:Lcom/peel/i/gm;

    iget-object v0, p0, Lcom/peel/i/hg;->j:Ljava/lang/Object;

    check-cast v0, Ljava/util/ArrayList;

    invoke-static {v1, v0}, Lcom/peel/i/gm;->a(Lcom/peel/i/gm;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    .line 1236
    iget-object v0, p0, Lcom/peel/i/hg;->a:Lcom/peel/i/hf;

    iget-object v0, v0, Lcom/peel/i/hf;->b:Lcom/peel/i/gm;

    invoke-static {v0, v3}, Lcom/peel/i/gm;->c(Lcom/peel/i/gm;I)I

    .line 1237
    iget-object v0, p0, Lcom/peel/i/hg;->a:Lcom/peel/i/hf;

    iget-object v0, v0, Lcom/peel/i/hf;->b:Lcom/peel/i/gm;

    invoke-static {v0}, Lcom/peel/i/gm;->S(Lcom/peel/i/gm;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    .line 1238
    iget-object v0, p0, Lcom/peel/i/hg;->a:Lcom/peel/i/hf;

    iget-object v0, v0, Lcom/peel/i/hf;->b:Lcom/peel/i/gm;

    invoke-static {v0}, Lcom/peel/i/gm;->ab(Lcom/peel/i/gm;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 1239
    iget-object v0, p0, Lcom/peel/i/hg;->a:Lcom/peel/i/hf;

    iget-object v0, v0, Lcom/peel/i/hf;->b:Lcom/peel/i/gm;

    invoke-static {v0}, Lcom/peel/i/gm;->ac(Lcom/peel/i/gm;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 1241
    :cond_2
    iget-object v0, p0, Lcom/peel/i/hg;->a:Lcom/peel/i/hf;

    iget-object v1, v0, Lcom/peel/i/hf;->b:Lcom/peel/i/gm;

    iget-object v0, p0, Lcom/peel/i/hg;->a:Lcom/peel/i/hf;

    iget-object v0, v0, Lcom/peel/i/hf;->b:Lcom/peel/i/gm;

    invoke-static {v0}, Lcom/peel/i/gm;->S(Lcom/peel/i/gm;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    const-string/jumbo v2, "codesetid"

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v1, v0}, Lcom/peel/i/gm;->d(Lcom/peel/i/gm;I)I

    .line 1242
    iget-object v0, p0, Lcom/peel/i/hg;->a:Lcom/peel/i/hf;

    iget-object v0, v0, Lcom/peel/i/hf;->b:Lcom/peel/i/gm;

    invoke-static {v0}, Lcom/peel/i/gm;->c(Lcom/peel/i/gm;)Lcom/peel/control/h;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v1

    iget-object v0, p0, Lcom/peel/i/hg;->a:Lcom/peel/i/hf;

    iget-object v0, v0, Lcom/peel/i/hf;->b:Lcom/peel/i/gm;

    invoke-static {v0}, Lcom/peel/i/gm;->o(Lcom/peel/i/gm;)Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Lcom/peel/i/hg;->a:Lcom/peel/i/hf;

    iget-object v0, v0, Lcom/peel/i/hf;->b:Lcom/peel/i/gm;

    invoke-static {v0}, Lcom/peel/i/gm;->S(Lcom/peel/i/gm;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    invoke-virtual {v1, v2, v0}, Lcom/peel/data/g;->a(Ljava/lang/String;Ljava/util/Map;)V

    .line 1243
    iget-object v0, p0, Lcom/peel/i/hg;->a:Lcom/peel/i/hf;

    iget-object v0, v0, Lcom/peel/i/hf;->b:Lcom/peel/i/gm;

    invoke-static {v0}, Lcom/peel/i/gm;->ad(Lcom/peel/i/gm;)V

    goto/16 :goto_0
.end method
