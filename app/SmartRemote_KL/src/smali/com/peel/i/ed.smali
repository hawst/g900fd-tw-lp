.class Lcom/peel/i/ed;
.super Lcom/peel/util/t;


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic b:Landroid/os/Bundle;

.field final synthetic c:Lcom/peel/i/dx;


# direct methods
.method constructor <init>(Lcom/peel/i/dx;ILjava/lang/String;Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 211
    iput-object p1, p0, Lcom/peel/i/ed;->c:Lcom/peel/i/dx;

    iput-object p3, p0, Lcom/peel/i/ed;->a:Ljava/lang/String;

    iput-object p4, p0, Lcom/peel/i/ed;->b:Landroid/os/Bundle;

    invoke-direct {p0, p2}, Lcom/peel/util/t;-><init>(I)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 215
    iget-boolean v0, p0, Lcom/peel/i/ed;->i:Z

    if-nez v0, :cond_0

    .line 216
    iget-object v0, p0, Lcom/peel/i/ed;->c:Lcom/peel/i/dx;

    iget-object v1, p0, Lcom/peel/i/ed;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/peel/i/dx;->a(Lcom/peel/i/dx;Ljava/lang/String;)Ljava/lang/String;

    .line 217
    iget-object v0, p0, Lcom/peel/i/ed;->c:Lcom/peel/i/dx;

    iget-object v1, p0, Lcom/peel/i/ed;->b:Landroid/os/Bundle;

    iget-object v2, p0, Lcom/peel/i/ed;->a:Ljava/lang/String;

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lcom/peel/i/dx;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;)V

    .line 284
    :goto_0
    return-void

    .line 220
    :cond_0
    iget-object v0, p0, Lcom/peel/i/ed;->c:Lcom/peel/i/dx;

    invoke-static {v0}, Lcom/peel/i/dx;->g(Lcom/peel/i/dx;)Lcom/peel/util/CustomSpinner;

    move-result-object v0

    new-instance v1, Lcom/peel/i/ee;

    invoke-direct {v1, p0}, Lcom/peel/i/ee;-><init>(Lcom/peel/i/ed;)V

    invoke-virtual {v0, v1}, Lcom/peel/util/CustomSpinner;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 242
    iget-object v0, p0, Lcom/peel/i/ed;->c:Lcom/peel/i/dx;

    invoke-static {v0}, Lcom/peel/i/dx;->g(Lcom/peel/i/dx;)Lcom/peel/util/CustomSpinner;

    move-result-object v0

    invoke-virtual {v0, v5}, Lcom/peel/util/CustomSpinner;->setVisibility(I)V

    .line 243
    iget-object v0, p0, Lcom/peel/i/ed;->c:Lcom/peel/i/dx;

    invoke-static {v0}, Lcom/peel/i/dx;->g(Lcom/peel/i/dx;)Lcom/peel/util/CustomSpinner;

    move-result-object v1

    new-instance v2, Lcom/peel/i/ef;

    iget-object v0, p0, Lcom/peel/i/ed;->c:Lcom/peel/i/dx;

    invoke-virtual {v0}, Lcom/peel/i/dx;->m()Landroid/support/v4/app/ae;

    move-result-object v3

    sget v4, Lcom/peel/ui/fq;->region_spinner_list_item:I

    iget-object v0, p0, Lcom/peel/i/ed;->j:Ljava/lang/Object;

    check-cast v0, Ljava/util/List;

    invoke-direct {v2, p0, v3, v4, v0}, Lcom/peel/i/ef;-><init>(Lcom/peel/i/ed;Landroid/content/Context;ILjava/util/List;)V

    invoke-virtual {v1, v2}, Lcom/peel/util/CustomSpinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 267
    iget-object v0, p0, Lcom/peel/i/ed;->c:Lcom/peel/i/dx;

    invoke-static {v0}, Lcom/peel/i/dx;->g(Lcom/peel/i/dx;)Lcom/peel/util/CustomSpinner;

    move-result-object v0

    new-instance v1, Lcom/peel/i/eg;

    invoke-direct {v1, p0}, Lcom/peel/i/eg;-><init>(Lcom/peel/i/ed;)V

    invoke-virtual {v0, v1}, Lcom/peel/util/CustomSpinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 283
    iget-object v0, p0, Lcom/peel/i/ed;->c:Lcom/peel/i/dx;

    invoke-static {v0}, Lcom/peel/i/dx;->g(Lcom/peel/i/dx;)Lcom/peel/util/CustomSpinner;

    move-result-object v0

    invoke-virtual {v0, v5}, Lcom/peel/util/CustomSpinner;->setSelection(I)V

    goto :goto_0
.end method
