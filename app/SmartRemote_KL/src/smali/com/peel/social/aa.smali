.class final Lcom/peel/social/aa;
.super Ljava/lang/Object;

# interfaces
.implements Lretrofit/ErrorHandler;


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public handleError(Lretrofit/RetrofitError;)Ljava/lang/Throwable;
    .locals 2

    .prologue
    .line 33
    :try_start_0
    new-instance v1, Lcom/peel/social/b;

    const-class v0, Lcom/peel/social/a;

    invoke-virtual {p1, v0}, Lretrofit/RetrofitError;->getBodyAs(Ljava/lang/reflect/Type;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/social/a;

    invoke-direct {v1, v0}, Lcom/peel/social/b;-><init>(Lcom/peel/social/a;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-object v0, v1

    .line 43
    :goto_0
    return-object v0

    .line 34
    :catch_0
    move-exception v0

    .line 35
    new-instance v1, Lcom/peel/social/a;

    invoke-direct {v1}, Lcom/peel/social/a;-><init>()V

    .line 36
    invoke-virtual {p1}, Lretrofit/RetrofitError;->getMessage()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/peel/social/a;->a:Ljava/lang/String;

    .line 37
    invoke-virtual {p1}, Lretrofit/RetrofitError;->getResponse()Lretrofit/client/Response;

    move-result-object v0

    .line 38
    if-eqz v0, :cond_0

    .line 39
    invoke-virtual {v0}, Lretrofit/client/Response;->getStatus()I

    move-result v0

    iput v0, v1, Lcom/peel/social/a;->b:I

    .line 43
    :goto_1
    new-instance v0, Lcom/peel/social/b;

    invoke-direct {v0, v1}, Lcom/peel/social/b;-><init>(Lcom/peel/social/a;)V

    goto :goto_0

    .line 41
    :cond_0
    const/4 v0, -0x1

    iput v0, v1, Lcom/peel/social/a;->b:I

    goto :goto_1
.end method
