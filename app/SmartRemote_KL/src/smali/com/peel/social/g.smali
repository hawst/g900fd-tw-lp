.class Lcom/peel/social/g;
.super Lcom/peel/util/t;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/peel/util/t",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/peel/social/f;


# direct methods
.method constructor <init>(Lcom/peel/social/f;)V
    .locals 0

    .prologue
    .line 49
    iput-object p1, p0, Lcom/peel/social/g;->a:Lcom/peel/social/f;

    invoke-direct {p0}, Lcom/peel/util/t;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    const/4 v5, 0x1

    const/4 v7, 0x0

    .line 52
    iget-boolean v0, p0, Lcom/peel/social/g;->i:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/peel/social/g;->j:Ljava/lang/Object;

    if-nez v0, :cond_2

    .line 53
    :cond_0
    sget-object v0, Lcom/peel/social/e;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "SocialLogin: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/peel/social/g;->k:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 54
    iget-object v0, p0, Lcom/peel/social/g;->a:Lcom/peel/social/f;

    iget-object v0, v0, Lcom/peel/social/f;->b:Lcom/peel/util/t;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Failed to create user with token: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/peel/social/g;->a:Lcom/peel/social/f;

    iget-object v2, v2, Lcom/peel/social/f;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/peel/social/g;->a:Lcom/peel/social/f;

    iget-object v2, v2, Lcom/peel/social/f;->c:Landroid/content/Context;

    sget v3, Lcom/peel/ui/ft;->error_login_with:I

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, v5, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/peel/social/g;->a:Lcom/peel/social/f;

    iget-object v4, v4, Lcom/peel/social/f;->c:Landroid/content/Context;

    sget v5, Lcom/peel/ui/ft;->facebook_label:I

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v7

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v7, v1, v2}, Lcom/peel/util/t;->a(ZLjava/lang/Object;Ljava/lang/String;)V

    .line 60
    :cond_1
    :goto_0
    return-void

    .line 56
    :cond_2
    iget-boolean v0, p0, Lcom/peel/social/g;->i:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/peel/social/g;->j:Ljava/lang/Object;

    if-eqz v0, :cond_1

    .line 57
    iget-object v0, p0, Lcom/peel/social/g;->a:Lcom/peel/social/f;

    iget-object v1, v0, Lcom/peel/social/f;->c:Landroid/content/Context;

    iget-object v0, p0, Lcom/peel/social/g;->j:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    iget-object v2, p0, Lcom/peel/social/g;->a:Lcom/peel/social/f;

    iget-object v2, v2, Lcom/peel/social/f;->b:Lcom/peel/util/t;

    iget-object v3, p0, Lcom/peel/social/g;->a:Lcom/peel/social/f;

    iget-object v3, v3, Lcom/peel/social/f;->c:Landroid/content/Context;

    sget v4, Lcom/peel/ui/ft;->logged_in_with:I

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-array v4, v5, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/peel/social/g;->a:Lcom/peel/social/f;

    iget-object v5, v5, Lcom/peel/social/f;->c:Landroid/content/Context;

    sget v6, Lcom/peel/ui/ft;->facebook_label:I

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v7

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v0, v2, v3}, Lcom/peel/social/e;->a(Landroid/content/Context;Ljava/lang/String;Lcom/peel/util/t;Ljava/lang/String;)V

    goto :goto_0
.end method
