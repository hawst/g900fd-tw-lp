.class public Lcom/peel/social/e;
.super Ljava/lang/Object;


# static fields
.field public static a:Ljava/lang/String;

.field public static b:Ljava/lang/String;

.field public static c:Ljava/lang/String;

.field public static d:Ljava/lang/String;

.field public static e:Lcom/peel/widget/ag;

.field public static f:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 36
    const-string/jumbo v0, "SOCIAL LOGIN HELPER"

    sput-object v0, Lcom/peel/social/e;->a:Ljava/lang/String;

    .line 37
    const-string/jumbo v0, "Facebook"

    sput-object v0, Lcom/peel/social/e;->b:Ljava/lang/String;

    .line 38
    const-string/jumbo v0, "Google"

    sput-object v0, Lcom/peel/social/e;->c:Ljava/lang/String;

    .line 39
    const-string/jumbo v0, "Samsung"

    sput-object v0, Lcom/peel/social/e;->d:Ljava/lang/String;

    .line 41
    const/4 v0, 0x0

    sput-boolean v0, Lcom/peel/social/e;->f:Z

    return-void
.end method

.method public static a(Landroid/app/Activity;IIZLcom/peel/util/t;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            "IIZ",
            "Lcom/peel/util/t",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    const/4 v1, 0x1

    const/16 v6, 0x8

    const/4 v2, 0x0

    .line 261
    const-string/jumbo v0, "layout_inflater"

    invoke-virtual {p0, v0}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 263
    new-instance v3, Lcom/peel/widget/ag;

    invoke-direct {v3, p0}, Lcom/peel/widget/ag;-><init>(Landroid/content/Context;)V

    sget v4, Lcom/peel/ui/ft;->login_dialog_title:I

    invoke-virtual {v3, v4}, Lcom/peel/widget/ag;->a(I)Lcom/peel/widget/ag;

    move-result-object v3

    sget v4, Lcom/peel/ui/ft;->cancel:I

    .line 264
    invoke-virtual {v3, v4, v5}, Lcom/peel/widget/ag;->a(ILandroid/content/DialogInterface$OnClickListener;)Lcom/peel/widget/ag;

    move-result-object v3

    sput-object v3, Lcom/peel/social/e;->e:Lcom/peel/widget/ag;

    .line 265
    sget v3, Lcom/peel/ui/fq;->login_view:I

    invoke-virtual {v0, v3, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v4

    .line 267
    invoke-static {p0}, Lcom/peel/util/bx;->c(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 268
    const-string/jumbo v3, "china"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    const-string/jumbo v3, "cn"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    move v3, v1

    .line 271
    :goto_0
    if-eqz v3, :cond_3

    .line 272
    sget v0, Lcom/peel/ui/fp;->img_facebook:I

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    .line 273
    sget v0, Lcom/peel/ui/fp;->img_google:I

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    .line 305
    :goto_1
    sget-object v0, Lcom/peel/c/a;->b:Lcom/peel/c/e;

    invoke-static {v0}, Lcom/peel/c/a;->a(Lcom/peel/c/e;)Ljava/lang/Object;

    move-result-object v0

    sget-object v5, Lcom/peel/c/b;->b:Lcom/peel/c/b;

    if-eq v0, v5, :cond_1

    sget-object v0, Lcom/peel/c/a;->b:Lcom/peel/c/e;

    .line 306
    invoke-static {v0}, Lcom/peel/c/a;->a(Lcom/peel/c/e;)Ljava/lang/Object;

    move-result-object v0

    sget-object v5, Lcom/peel/c/b;->a:Lcom/peel/c/b;

    if-ne v0, v5, :cond_4

    .line 307
    :cond_1
    invoke-static {p0}, Lcom/peel/util/bx;->h(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 309
    sget v0, Lcom/peel/ui/fp;->img_samsung:I

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 310
    sget v0, Lcom/peel/ui/fp;->img_samsung:I

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v2, Lcom/peel/social/h;

    invoke-direct {v2, p0, p3}, Lcom/peel/social/h;-><init>(Landroid/app/Activity;Z)V

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    move v0, v1

    .line 330
    :goto_2
    if-eqz v3, :cond_5

    if-nez v0, :cond_5

    .line 336
    :goto_3
    return-void

    :cond_2
    move v3, v2

    .line 268
    goto :goto_0

    .line 275
    :cond_3
    sget v0, Lcom/peel/ui/fp;->img_facebook:I

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v5, Lcom/peel/social/u;

    invoke-direct {v5, p0, p4, p1, p2}, Lcom/peel/social/u;-><init>(Landroid/app/Activity;Lcom/peel/util/t;II)V

    invoke-virtual {v0, v5}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 289
    sget v0, Lcom/peel/ui/fp;->img_google:I

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v5, Lcom/peel/social/v;

    invoke-direct {v5, p0, p3, p1, p2}, Lcom/peel/social/v;-><init>(Landroid/app/Activity;ZII)V

    invoke-virtual {v0, v5}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_1

    .line 326
    :cond_4
    sget v0, Lcom/peel/ui/fp;->img_samsung:I

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    move v0, v2

    goto :goto_2

    .line 333
    :cond_5
    sget-object v0, Lcom/peel/social/e;->e:Lcom/peel/widget/ag;

    invoke-virtual {v0, v4}, Lcom/peel/widget/ag;->a(Landroid/view/View;)Lcom/peel/widget/ag;

    .line 334
    sget-object v0, Lcom/peel/social/e;->e:Lcom/peel/widget/ag;

    invoke-virtual {v0}, Lcom/peel/widget/ag;->d()Lcom/peel/widget/ag;

    goto :goto_3
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Lcom/peel/util/t;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Lcom/peel/util/t",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 93
    invoke-static {p0}, Lcom/peel/social/e;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 94
    sget-object v0, Lcom/peel/social/e;->a:Ljava/lang/String;

    const-string/jumbo v1, "get facebook user"

    new-instance v2, Lcom/peel/social/k;

    invoke-direct {v2, p1, p0, p2}, Lcom/peel/social/k;-><init>(Ljava/lang/String;Landroid/content/Context;Lcom/peel/util/t;)V

    invoke-static {v0, v1, v2}, Lcom/peel/util/i;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    .line 115
    :goto_0
    return-void

    .line 112
    :cond_0
    const/4 v0, 0x0

    const-string/jumbo v1, ""

    sget v2, Lcom/peel/ui/ft;->no_internet:I

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2, v0, v1, v2}, Lcom/peel/util/t;->a(ZLjava/lang/Object;Ljava/lang/String;)V

    goto :goto_0
.end method

.method static synthetic a(Landroid/content/Context;Ljava/lang/String;Lcom/peel/util/t;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 34
    invoke-static {p0, p1, p2, p3}, Lcom/peel/social/e;->b(Landroid/content/Context;Ljava/lang/String;Lcom/peel/util/t;Ljava/lang/String;)V

    return-void
.end method

.method public static a(Landroid/content/Context;)Z
    .locals 1

    .prologue
    .line 252
    const-string/jumbo v0, "connectivity"

    .line 253
    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 254
    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v0

    .line 255
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(Landroid/content/Context;Ljava/lang/String;Lcom/peel/util/t;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Lcom/peel/util/t",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 119
    invoke-static {p0}, Lcom/peel/social/e;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 120
    sget-object v0, Lcom/peel/social/e;->a:Ljava/lang/String;

    const-string/jumbo v1, "get google user"

    new-instance v2, Lcom/peel/social/m;

    invoke-direct {v2, p1, p0, p2}, Lcom/peel/social/m;-><init>(Ljava/lang/String;Landroid/content/Context;Lcom/peel/util/t;)V

    invoke-static {v0, v1, v2}, Lcom/peel/util/i;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    .line 143
    :goto_0
    return-void

    .line 141
    :cond_0
    const/4 v0, 0x0

    const-string/jumbo v1, ""

    sget v2, Lcom/peel/ui/ft;->no_internet:I

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2, v0, v1, v2}, Lcom/peel/util/t;->a(ZLjava/lang/Object;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private static b(Landroid/content/Context;Ljava/lang/String;Lcom/peel/util/t;Ljava/lang/String;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Lcom/peel/util/t",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 200
    :try_start_0
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 201
    const-string/jumbo v1, "auth"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 202
    const-string/jumbo v2, "userID"

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 203
    const-string/jumbo v2, "social_accounts_setup"

    const/4 v3, 0x0

    invoke-virtual {p0, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    .line 204
    const-string/jumbo v3, "scalos_auth"

    invoke-interface {v2, v3, v1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 205
    const-string/jumbo v3, "scalos_userid"

    invoke-interface {v2, v3, v0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 206
    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 208
    sget-object v0, Lcom/peel/social/e;->a:Ljava/lang/String;

    const-string/jumbo v2, "get google user"

    new-instance v3, Lcom/peel/social/s;

    invoke-direct {v3, v1, p0, p2, p3}, Lcom/peel/social/s;-><init>(Ljava/lang/String;Landroid/content/Context;Lcom/peel/util/t;Ljava/lang/String;)V

    invoke-static {v0, v2, v3}, Lcom/peel/util/i;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/util/concurrent/Future;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 249
    :goto_0
    return-void

    .line 245
    :catch_0
    move-exception v0

    .line 246
    sget-object v1, Lcom/peel/social/e;->a:Ljava/lang/String;

    sget-object v2, Lcom/peel/social/e;->a:Ljava/lang/String;

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public static c(Landroid/content/Context;Ljava/lang/String;Lcom/peel/util/t;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Lcom/peel/util/t",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 172
    invoke-static {p0}, Lcom/peel/social/e;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 173
    sget-object v0, Lcom/peel/social/e;->a:Ljava/lang/String;

    const-string/jumbo v1, "get samsung user"

    new-instance v2, Lcom/peel/social/q;

    invoke-direct {v2, p1, p0, p2}, Lcom/peel/social/q;-><init>(Ljava/lang/String;Landroid/content/Context;Lcom/peel/util/t;)V

    invoke-static {v0, v1, v2}, Lcom/peel/util/i;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    .line 195
    :goto_0
    return-void

    .line 193
    :cond_0
    const/4 v0, 0x0

    const-string/jumbo v1, ""

    sget v2, Lcom/peel/ui/ft;->no_internet:I

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2, v0, v1, v2}, Lcom/peel/util/t;->a(ZLjava/lang/Object;Ljava/lang/String;)V

    goto :goto_0
.end method

.method static synthetic d(Landroid/content/Context;Ljava/lang/String;Lcom/peel/util/t;)V
    .locals 0

    .prologue
    .line 34
    invoke-static {p0, p1, p2}, Lcom/peel/social/e;->g(Landroid/content/Context;Ljava/lang/String;Lcom/peel/util/t;)V

    return-void
.end method

.method static synthetic e(Landroid/content/Context;Ljava/lang/String;Lcom/peel/util/t;)V
    .locals 0

    .prologue
    .line 34
    invoke-static {p0, p1, p2}, Lcom/peel/social/e;->h(Landroid/content/Context;Ljava/lang/String;Lcom/peel/util/t;)V

    return-void
.end method

.method static synthetic f(Landroid/content/Context;Ljava/lang/String;Lcom/peel/util/t;)V
    .locals 0

    .prologue
    .line 34
    invoke-static {p0, p1, p2}, Lcom/peel/social/e;->i(Landroid/content/Context;Ljava/lang/String;Lcom/peel/util/t;)V

    return-void
.end method

.method private static g(Landroid/content/Context;Ljava/lang/String;Lcom/peel/util/t;)V
    .locals 3

    .prologue
    .line 44
    sget-object v0, Lcom/peel/social/e;->a:Ljava/lang/String;

    const-string/jumbo v1, "create facebook user"

    new-instance v2, Lcom/peel/social/f;

    invoke-direct {v2, p1, p2, p0}, Lcom/peel/social/f;-><init>(Ljava/lang/String;Lcom/peel/util/t;Landroid/content/Context;)V

    invoke-static {v0, v1, v2}, Lcom/peel/util/i;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    .line 64
    return-void
.end method

.method private static h(Landroid/content/Context;Ljava/lang/String;Lcom/peel/util/t;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Lcom/peel/util/t",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 68
    sget-object v0, Lcom/peel/social/e;->a:Ljava/lang/String;

    const-string/jumbo v1, "create google user"

    new-instance v2, Lcom/peel/social/i;

    invoke-direct {v2, p1, p2, p0}, Lcom/peel/social/i;-><init>(Ljava/lang/String;Lcom/peel/util/t;Landroid/content/Context;)V

    invoke-static {v0, v1, v2}, Lcom/peel/util/i;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    .line 89
    return-void
.end method

.method private static i(Landroid/content/Context;Ljava/lang/String;Lcom/peel/util/t;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Lcom/peel/util/t",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 147
    sget-object v0, Lcom/peel/social/e;->a:Ljava/lang/String;

    const-string/jumbo v1, "create google user"

    new-instance v2, Lcom/peel/social/o;

    invoke-direct {v2, p0, p1, p2}, Lcom/peel/social/o;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/peel/util/t;)V

    invoke-static {v0, v1, v2}, Lcom/peel/util/i;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    .line 168
    return-void
.end method
