.class Lcom/peel/social/t;
.super Lcom/peel/util/t;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/peel/util/t",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/peel/social/s;


# direct methods
.method constructor <init>(Lcom/peel/social/s;)V
    .locals 0

    .prologue
    .line 212
    iput-object p1, p0, Lcom/peel/social/t;->a:Lcom/peel/social/s;

    invoke-direct {p0}, Lcom/peel/util/t;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 215
    iget-boolean v0, p0, Lcom/peel/social/t;->i:Z

    if-nez v0, :cond_1

    .line 240
    :cond_0
    :goto_0
    return-void

    .line 217
    :cond_1
    iget-boolean v0, p0, Lcom/peel/social/t;->i:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/peel/social/t;->j:Ljava/lang/Object;

    if-eqz v0, :cond_0

    .line 219
    :try_start_0
    new-instance v1, Lorg/json/JSONObject;

    iget-object v0, p0, Lcom/peel/social/t;->j:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    invoke-direct {v1, v0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 220
    iget-object v0, p0, Lcom/peel/social/t;->a:Lcom/peel/social/s;

    iget-object v0, v0, Lcom/peel/social/s;->b:Landroid/content/Context;

    const-string/jumbo v2, "social_accounts_setup"

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 222
    const-string/jumbo v2, "name"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 223
    const-string/jumbo v2, "name"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 224
    const-string/jumbo v3, "scalos_user_name"

    invoke-interface {v0, v3, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 227
    :cond_2
    const-string/jumbo v2, "picture"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 228
    const-string/jumbo v2, "picture"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 229
    const-string/jumbo v3, "scalos_pic_url"

    invoke-interface {v0, v3, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 232
    :cond_3
    const-string/jumbo v2, "email"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 233
    const-string/jumbo v2, "scalos_email"

    invoke-interface {v0, v2, v1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 234
    iget-object v0, p0, Lcom/peel/social/t;->a:Lcom/peel/social/s;

    iget-object v0, v0, Lcom/peel/social/s;->c:Lcom/peel/util/t;

    const/4 v1, 0x1

    const-string/jumbo v2, ""

    iget-object v3, p0, Lcom/peel/social/t;->a:Lcom/peel/social/s;

    iget-object v3, v3, Lcom/peel/social/s;->d:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3}, Lcom/peel/util/t;->a(ZLjava/lang/Object;Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 235
    :catch_0
    move-exception v0

    .line 236
    sget-object v1, Lcom/peel/social/e;->a:Ljava/lang/String;

    sget-object v2, Lcom/peel/social/e;->a:Ljava/lang/String;

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 237
    iget-object v0, p0, Lcom/peel/social/t;->a:Lcom/peel/social/s;

    iget-object v0, v0, Lcom/peel/social/s;->c:Lcom/peel/util/t;

    const-string/jumbo v1, ""

    iget-object v2, p0, Lcom/peel/social/t;->a:Lcom/peel/social/s;

    iget-object v2, v2, Lcom/peel/social/s;->d:Ljava/lang/String;

    invoke-virtual {v0, v4, v1, v2}, Lcom/peel/util/t;->a(ZLjava/lang/Object;Ljava/lang/String;)V

    goto/16 :goto_0
.end method
