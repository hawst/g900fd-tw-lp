.class public Lcom/peel/social/z;
.super Ljava/lang/Object;


# static fields
.field private static final a:Ljava/lang/String;

.field private static b:Lcom/peel/social/c;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 19
    const-class v0, Lcom/peel/social/z;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/peel/social/z;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lretrofit/client/Client;)Lcom/peel/social/c;
    .locals 4

    .prologue
    .line 25
    sget-object v0, Lcom/peel/social/z;->b:Lcom/peel/social/c;

    if-nez v0, :cond_0

    .line 26
    new-instance v0, Lretrofit/RestAdapter$Builder;

    invoke-direct {v0}, Lretrofit/RestAdapter$Builder;-><init>()V

    sget-object v1, Lcom/peel/b/a;->f:Ljava/lang/String;

    .line 27
    invoke-virtual {v0, v1}, Lretrofit/RestAdapter$Builder;->setServer(Ljava/lang/String;)Lretrofit/RestAdapter$Builder;

    move-result-object v0

    new-instance v1, Lcom/peel/social/d;

    const-string/jumbo v2, "c583c7c46eef455992a6846c81573f02"

    const-string/jumbo v3, "6f5d1afa224b4b2c93666d1405795725"

    invoke-direct {v1, p0, v2, v3}, Lcom/peel/social/d;-><init>(Lretrofit/client/Client;Ljava/lang/String;Ljava/lang/String;)V

    .line 28
    invoke-virtual {v0, v1}, Lretrofit/RestAdapter$Builder;->setClient(Lretrofit/client/Client;)Lretrofit/RestAdapter$Builder;

    move-result-object v0

    .line 30
    new-instance v1, Lcom/peel/social/aa;

    invoke-direct {v1}, Lcom/peel/social/aa;-><init>()V

    invoke-virtual {v0, v1}, Lretrofit/RestAdapter$Builder;->setErrorHandler(Lretrofit/ErrorHandler;)Lretrofit/RestAdapter$Builder;

    .line 51
    invoke-virtual {v0}, Lretrofit/RestAdapter$Builder;->build()Lretrofit/RestAdapter;

    move-result-object v0

    const-class v1, Lcom/peel/social/c;

    invoke-virtual {v0, v1}, Lretrofit/RestAdapter;->create(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/social/c;

    sput-object v0, Lcom/peel/social/z;->b:Lcom/peel/social/c;

    .line 52
    sget-object v0, Lcom/peel/social/z;->a:Ljava/lang/String;

    const-string/jumbo v1, "adapter built"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 54
    :cond_0
    sget-object v0, Lcom/peel/social/z;->b:Lcom/peel/social/c;

    return-object v0
.end method
