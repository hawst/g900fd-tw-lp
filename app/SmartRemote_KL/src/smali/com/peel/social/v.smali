.class final Lcom/peel/social/v;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Landroid/app/Activity;

.field final synthetic b:Z

.field final synthetic c:I

.field final synthetic d:I


# direct methods
.method constructor <init>(Landroid/app/Activity;ZII)V
    .locals 0

    .prologue
    .line 289
    iput-object p1, p0, Lcom/peel/social/v;->a:Landroid/app/Activity;

    iput-boolean p2, p0, Lcom/peel/social/v;->b:Z

    iput p3, p0, Lcom/peel/social/v;->c:I

    iput p4, p0, Lcom/peel/social/v;->d:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    .line 292
    sget-object v0, Lcom/peel/social/e;->e:Lcom/peel/widget/ag;

    invoke-virtual {v0}, Lcom/peel/widget/ag;->dismiss()V

    .line 293
    new-instance v2, Landroid/content/Intent;

    iget-object v0, p0, Lcom/peel/social/v;->a:Landroid/app/Activity;

    const-class v3, Lcom/peel/main/GoogleLoginActivity;

    invoke-direct {v2, v0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 294
    iget-object v3, p0, Lcom/peel/social/v;->a:Landroid/app/Activity;

    iget-boolean v0, p0, Lcom/peel/social/v;->b:Z

    if-eqz v0, :cond_0

    sget v0, Lcom/peel/social/w;->d:I

    :goto_0
    invoke-virtual {v3, v2, v0}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 296
    sput-boolean v1, Lcom/peel/social/e;->f:Z

    .line 297
    invoke-static {}, Lcom/peel/util/a/f;->a()Lcom/peel/util/a/f;

    move-result-object v2

    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    .line 298
    invoke-virtual {v0}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v0

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    iget v1, p0, Lcom/peel/social/v;->c:I

    iget v3, p0, Lcom/peel/social/v;->d:I

    sget-object v4, Lcom/peel/social/e;->c:Ljava/lang/String;

    .line 297
    invoke-virtual {v2, v0, v1, v3, v4}, Lcom/peel/util/a/f;->a(IIILjava/lang/String;)V

    .line 301
    return-void

    .line 294
    :cond_0
    sget v0, Lcom/peel/social/w;->b:I

    goto :goto_0

    .line 298
    :cond_1
    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v0}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/data/at;->f()I

    move-result v0

    goto :goto_1
.end method
