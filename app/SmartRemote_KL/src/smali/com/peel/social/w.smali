.class public Lcom/peel/social/w;
.super Ljava/lang/Object;


# static fields
.field public static a:I

.field public static b:I

.field public static c:I

.field public static d:I

.field public static e:Ljava/lang/String;

.field public static f:Ljava/lang/String;

.field public static g:Ljava/lang/String;

.field public static h:Ljava/lang/String;

.field public static i:Ljava/lang/String;

.field public static j:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 20
    const/16 v0, 0xd7d

    sput v0, Lcom/peel/social/w;->a:I

    .line 21
    const/16 v0, 0x26e1

    sput v0, Lcom/peel/social/w;->b:I

    .line 22
    const/16 v0, 0xd7e

    sput v0, Lcom/peel/social/w;->c:I

    .line 23
    const/16 v0, 0x26e2

    sput v0, Lcom/peel/social/w;->d:I

    .line 32
    const-string/jumbo v0, "access_token"

    sput-object v0, Lcom/peel/social/w;->e:Ljava/lang/String;

    .line 33
    const-string/jumbo v0, "error"

    sput-object v0, Lcom/peel/social/w;->f:Ljava/lang/String;

    .line 34
    const-string/jumbo v0, "exit"

    sput-object v0, Lcom/peel/social/w;->g:Ljava/lang/String;

    .line 35
    const-string/jumbo v0, "goog_access_token"

    sput-object v0, Lcom/peel/social/w;->h:Ljava/lang/String;

    .line 37
    const-string/jumbo v0, "started_google"

    sput-object v0, Lcom/peel/social/w;->i:Ljava/lang/String;

    .line 38
    const-string/jumbo v0, "user_cancel_google"

    sput-object v0, Lcom/peel/social/w;->j:Ljava/lang/String;

    return-void
.end method

.method public static a(Landroid/app/Activity;Lcom/peel/util/t;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            "Lcom/peel/util/t",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 41
    invoke-static {}, Lcom/peel/social/a/f;->a()Lcom/peel/social/a/f;

    move-result-object v0

    new-instance v1, Lcom/peel/social/x;

    invoke-direct {v1, p0, p1}, Lcom/peel/social/x;-><init>(Landroid/app/Activity;Lcom/peel/util/t;)V

    invoke-virtual {v0, v1}, Lcom/peel/social/a/f;->a(Lcom/peel/social/a/a/b;)V

    .line 66
    return-void
.end method

.method public static a(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 105
    invoke-static {p0}, Lcom/peel/social/w;->k(Landroid/content/Context;)V

    .line 106
    const-string/jumbo v0, "social_accounts_setup"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 107
    const-string/jumbo v1, "google_access_token"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 108
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 109
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/peel/main/GoogleLoginActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 110
    sget-object v1, Lcom/peel/main/GoogleLoginActivity;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 111
    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 112
    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/peel/util/t;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/peel/util/t",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 87
    const-string/jumbo v0, "social_accounts_setup"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 89
    const-string/jumbo v1, "samsungid"

    const/4 v2, 0x0

    .line 90
    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/peel/social/y;

    invoke-direct {v2, v0, p1}, Lcom/peel/social/y;-><init>(Landroid/content/SharedPreferences;Lcom/peel/util/t;)V

    .line 89
    invoke-static {p0, v1, v2}, Lcom/peel/social/e;->c(Landroid/content/Context;Ljava/lang/String;Lcom/peel/util/t;)V

    .line 102
    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Lcom/peel/util/t;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Lcom/peel/util/t",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 70
    invoke-static {p0, p1, p2}, Lcom/peel/social/e;->a(Landroid/content/Context;Ljava/lang/String;Lcom/peel/util/t;)V

    .line 71
    const-string/jumbo v0, "social_accounts_setup"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 72
    const-string/jumbo v1, "facebook_on"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 73
    const-string/jumbo v1, "has_facebook_login"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 74
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 75
    return-void
.end method

.method public static b(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 115
    const-string/jumbo v0, "social_accounts_setup"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 116
    const-string/jumbo v1, "has_facebook_login"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 117
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 118
    invoke-static {p0}, Lcom/peel/social/w;->k(Landroid/content/Context;)V

    .line 119
    invoke-static {p0}, Landroid/webkit/CookieSyncManager;->createInstance(Landroid/content/Context;)Landroid/webkit/CookieSyncManager;

    .line 120
    invoke-static {}, Landroid/webkit/CookieManager;->getInstance()Landroid/webkit/CookieManager;

    move-result-object v0

    .line 121
    invoke-virtual {v0}, Landroid/webkit/CookieManager;->removeAllCookie()V

    .line 122
    return-void
.end method

.method public static b(Landroid/content/Context;Ljava/lang/String;Lcom/peel/util/t;)V
    .locals 3

    .prologue
    .line 78
    invoke-static {p0, p1, p2}, Lcom/peel/social/e;->b(Landroid/content/Context;Ljava/lang/String;Lcom/peel/util/t;)V

    .line 79
    const-string/jumbo v0, "social_accounts_setup"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 80
    const-string/jumbo v1, "google_on"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 81
    const-string/jumbo v1, "google_access_token"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 82
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 83
    return-void
.end method

.method public static c(Landroid/content/Context;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 125
    invoke-static {p0}, Lcom/peel/social/w;->k(Landroid/content/Context;)V

    .line 126
    const-string/jumbo v0, "social_accounts_setup"

    invoke-virtual {p0, v0, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 127
    const-string/jumbo v1, "google_on"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 128
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 129
    return-void
.end method

.method public static d(Landroid/content/Context;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 132
    invoke-static {p0}, Lcom/peel/social/w;->k(Landroid/content/Context;)V

    .line 133
    invoke-static {p0}, Landroid/webkit/CookieSyncManager;->createInstance(Landroid/content/Context;)Landroid/webkit/CookieSyncManager;

    .line 134
    invoke-static {}, Landroid/webkit/CookieManager;->getInstance()Landroid/webkit/CookieManager;

    move-result-object v0

    .line 135
    invoke-virtual {v0}, Landroid/webkit/CookieManager;->removeAllCookie()V

    .line 136
    const-string/jumbo v0, "social_accounts_setup"

    invoke-virtual {p0, v0, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 137
    const-string/jumbo v1, "facebook_on"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 138
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 139
    return-void
.end method

.method public static e(Landroid/content/Context;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 142
    invoke-static {p0}, Lcom/peel/social/w;->k(Landroid/content/Context;)V

    .line 143
    const-string/jumbo v0, "social_accounts_setup"

    invoke-virtual {p0, v0, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 144
    const-string/jumbo v1, "samsung_on"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 145
    const-string/jumbo v1, "samsungid"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 146
    const-string/jumbo v1, "samsung_access_token"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 147
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 148
    return-void
.end method

.method public static f(Landroid/content/Context;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 160
    const-string/jumbo v1, "social_accounts_setup"

    invoke-virtual {p0, v1, v0}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 162
    const-string/jumbo v2, "scalos_auth"

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static g(Landroid/content/Context;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 176
    const-string/jumbo v1, "social_accounts_setup"

    invoke-virtual {p0, v1, v0}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 177
    const-string/jumbo v2, "google_access_token"

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    const-string/jumbo v2, "google_on"

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public static h(Landroid/content/Context;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 180
    const-string/jumbo v1, "social_accounts_setup"

    invoke-virtual {p0, v1, v0}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 181
    const-string/jumbo v2, "has_facebook_login"

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    const-string/jumbo v2, "facebook_on"

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public static i(Landroid/content/Context;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 185
    const-string/jumbo v1, "social_accounts_setup"

    invoke-virtual {p0, v1, v0}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 186
    const-string/jumbo v2, "samsungid"

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    const-string/jumbo v2, "samsung_on"

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public static j(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 190
    invoke-static {p0}, Lcom/peel/social/w;->h(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 191
    invoke-static {p0}, Lcom/peel/social/w;->b(Landroid/content/Context;)V

    .line 194
    :cond_0
    invoke-static {p0}, Lcom/peel/social/w;->g(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 195
    invoke-static {p0}, Lcom/peel/social/w;->a(Landroid/content/Context;)V

    .line 198
    :cond_1
    invoke-static {p0}, Lcom/peel/social/w;->i(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 199
    invoke-static {p0}, Lcom/peel/social/w;->e(Landroid/content/Context;)V

    .line 201
    :cond_2
    invoke-static {p0}, Lcom/peel/social/w;->k(Landroid/content/Context;)V

    .line 202
    return-void
.end method

.method private static k(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 166
    const-string/jumbo v0, "social_accounts_setup"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 167
    const-string/jumbo v1, "scalos_auth"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 168
    const-string/jumbo v1, "scalos_user_name"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 169
    const-string/jumbo v1, "scalos_userid"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 170
    const-string/jumbo v1, "scalos_pic_url"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 171
    const-string/jumbo v1, "device_profile_id"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 172
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 173
    return-void
.end method
