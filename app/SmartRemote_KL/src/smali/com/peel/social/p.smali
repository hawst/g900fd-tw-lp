.class Lcom/peel/social/p;
.super Lcom/peel/util/t;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/peel/util/t",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Landroid/content/SharedPreferences;

.field final synthetic b:Lcom/peel/social/o;


# direct methods
.method constructor <init>(Lcom/peel/social/o;Landroid/content/SharedPreferences;)V
    .locals 0

    .prologue
    .line 155
    iput-object p1, p0, Lcom/peel/social/p;->b:Lcom/peel/social/o;

    iput-object p2, p0, Lcom/peel/social/p;->a:Landroid/content/SharedPreferences;

    invoke-direct {p0}, Lcom/peel/util/t;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    const/4 v5, 0x1

    const/4 v7, 0x0

    .line 158
    iget-boolean v0, p0, Lcom/peel/social/p;->i:Z

    if-nez v0, :cond_1

    .line 159
    sget-object v0, Lcom/peel/social/e;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "SocialLogin: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/peel/social/p;->k:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 160
    iget-object v0, p0, Lcom/peel/social/p;->b:Lcom/peel/social/o;

    iget-object v0, v0, Lcom/peel/social/o;->c:Lcom/peel/util/t;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Failed to create user with token: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/peel/social/p;->b:Lcom/peel/social/o;

    iget-object v2, v2, Lcom/peel/social/o;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ", email : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/peel/social/p;->a:Landroid/content/SharedPreferences;

    const-string/jumbo v3, "samsung_account_email"

    const/4 v4, 0x0

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/peel/social/p;->b:Lcom/peel/social/o;

    iget-object v2, v2, Lcom/peel/social/o;->a:Landroid/content/Context;

    sget v3, Lcom/peel/ui/ft;->error_login_with:I

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, v5, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/peel/social/p;->b:Lcom/peel/social/o;

    iget-object v4, v4, Lcom/peel/social/o;->a:Landroid/content/Context;

    sget v5, Lcom/peel/ui/ft;->samsung_label:I

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v7

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v7, v1, v2}, Lcom/peel/util/t;->a(ZLjava/lang/Object;Ljava/lang/String;)V

    .line 164
    :cond_0
    :goto_0
    return-void

    .line 161
    :cond_1
    iget-boolean v0, p0, Lcom/peel/social/p;->i:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/peel/social/p;->j:Ljava/lang/Object;

    if-eqz v0, :cond_0

    .line 162
    iget-object v0, p0, Lcom/peel/social/p;->b:Lcom/peel/social/o;

    iget-object v1, v0, Lcom/peel/social/o;->a:Landroid/content/Context;

    iget-object v0, p0, Lcom/peel/social/p;->j:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    iget-object v2, p0, Lcom/peel/social/p;->b:Lcom/peel/social/o;

    iget-object v2, v2, Lcom/peel/social/o;->c:Lcom/peel/util/t;

    iget-object v3, p0, Lcom/peel/social/p;->b:Lcom/peel/social/o;

    iget-object v3, v3, Lcom/peel/social/o;->a:Landroid/content/Context;

    sget v4, Lcom/peel/ui/ft;->logged_in_with:I

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-array v4, v5, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/peel/social/p;->b:Lcom/peel/social/o;

    iget-object v5, v5, Lcom/peel/social/o;->a:Landroid/content/Context;

    sget v6, Lcom/peel/ui/ft;->samsung_label:I

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v7

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v0, v2, v3}, Lcom/peel/social/e;->a(Landroid/content/Context;Ljava/lang/String;Lcom/peel/util/t;Ljava/lang/String;)V

    goto :goto_0
.end method
