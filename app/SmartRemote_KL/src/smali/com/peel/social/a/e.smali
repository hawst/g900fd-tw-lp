.class public Lcom/peel/social/a/e;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/facebook/cq;


# instance fields
.field a:Lcom/peel/social/a/a/b;

.field b:Lcom/peel/social/a/a/c;

.field final synthetic c:Lcom/peel/social/a/c;

.field private d:Z

.field private e:Z

.field private f:Lcom/peel/social/a/a/d;


# direct methods
.method public constructor <init>(Lcom/peel/social/a/c;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 423
    iput-object p1, p0, Lcom/peel/social/a/e;->c:Lcom/peel/social/a/c;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 424
    iput-boolean v1, p0, Lcom/peel/social/a/e;->d:Z

    .line 425
    iput-boolean v1, p0, Lcom/peel/social/a/e;->e:Z

    .line 426
    iput-object v0, p0, Lcom/peel/social/a/e;->f:Lcom/peel/social/a/a/d;

    .line 427
    iput-object v0, p0, Lcom/peel/social/a/e;->a:Lcom/peel/social/a/a/b;

    .line 428
    iput-object v0, p0, Lcom/peel/social/a/e;->b:Lcom/peel/social/a/a/c;

    return-void
.end method

.method private a(Lcom/peel/social/a/b;)V
    .locals 1

    .prologue
    .line 552
    iget-object v0, p0, Lcom/peel/social/a/e;->a:Lcom/peel/social/a/a/b;

    if-eqz v0, :cond_0

    .line 553
    iget-object v0, p0, Lcom/peel/social/a/e;->a:Lcom/peel/social/a/a/b;

    invoke-interface {v0, p1}, Lcom/peel/social/a/a/b;->a(Lcom/peel/social/a/b;)V

    .line 555
    :cond_0
    return-void
.end method


# virtual methods
.method public a(Lcom/facebook/ca;Lcom/facebook/cv;Ljava/lang/Exception;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 436
    iget-object v0, p0, Lcom/peel/social/a/e;->c:Lcom/peel/social/a/c;

    invoke-virtual {v0}, Lcom/peel/social/a/c;->b()Ljava/util/List;

    move-result-object v0

    .line 437
    if-eqz p3, :cond_0

    .line 438
    instance-of v1, p3, Lcom/facebook/as;

    if-eqz v1, :cond_3

    sget-object v1, Lcom/facebook/cv;->e:Lcom/facebook/cv;

    invoke-virtual {v1, p2}, Lcom/facebook/cv;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 439
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_2

    .line 440
    sget-object v0, Lcom/peel/social/a/b;->b:Lcom/peel/social/a/b;

    invoke-direct {p0, v0}, Lcom/peel/social/a/e;->a(Lcom/peel/social/a/b;)V

    .line 453
    :cond_0
    :goto_0
    sget-object v0, Lcom/peel/social/a/d;->a:[I

    invoke-virtual {p2}, Lcom/facebook/cv;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 541
    :cond_1
    :goto_1
    :pswitch_0
    return-void

    .line 443
    :cond_2
    sget-object v0, Lcom/peel/social/a/b;->a:Lcom/peel/social/a/b;

    invoke-direct {p0, v0}, Lcom/peel/social/a/e;->a(Lcom/peel/social/a/b;)V

    goto :goto_0

    .line 447
    :cond_3
    iget-object v0, p0, Lcom/peel/social/a/e;->a:Lcom/peel/social/a/a/b;

    if-eqz v0, :cond_0

    .line 448
    iget-object v0, p0, Lcom/peel/social/a/e;->a:Lcom/peel/social/a/a/b;

    invoke-interface {v0, p3}, Lcom/peel/social/a/a/b;->a(Ljava/lang/Throwable;)V

    goto :goto_0

    .line 455
    :pswitch_1
    iget-object v0, p0, Lcom/peel/social/a/e;->b:Lcom/peel/social/a/a/c;

    if-eqz v0, :cond_1

    .line 456
    iget-object v0, p0, Lcom/peel/social/a/e;->b:Lcom/peel/social/a/a/c;

    invoke-interface {v0}, Lcom/peel/social/a/a/c;->a()V

    goto :goto_1

    .line 470
    :pswitch_2
    iget-object v0, p0, Lcom/peel/social/a/e;->a:Lcom/peel/social/a/a/b;

    if-eqz v0, :cond_1

    .line 471
    iget-object v0, p0, Lcom/peel/social/a/e;->a:Lcom/peel/social/a/a/b;

    invoke-interface {v0}, Lcom/peel/social/a/a/b;->b()V

    goto :goto_1

    .line 481
    :pswitch_3
    iget-object v0, p0, Lcom/peel/social/a/e;->f:Lcom/peel/social/a/a/d;

    if-eqz v0, :cond_4

    .line 482
    iget-object v0, p0, Lcom/peel/social/a/e;->f:Lcom/peel/social/a/a/d;

    sget-object v1, Lcom/peel/social/a/b;->a:Lcom/peel/social/a/b;

    invoke-interface {v0, v1}, Lcom/peel/social/a/a/d;->a(Lcom/peel/social/a/b;)V

    .line 483
    iput-object v3, p0, Lcom/peel/social/a/e;->f:Lcom/peel/social/a/a/d;

    goto :goto_1

    .line 491
    :cond_4
    iget-boolean v0, p0, Lcom/peel/social/a/e;->d:Z

    if-eqz v0, :cond_6

    invoke-virtual {p1}, Lcom/facebook/ca;->c()Lcom/facebook/cv;

    move-result-object v0

    sget-object v1, Lcom/facebook/cv;->d:Lcom/facebook/cv;

    invoke-virtual {v0, v1}, Lcom/facebook/cv;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 492
    iget-boolean v0, p0, Lcom/peel/social/a/e;->e:Z

    if-eqz v0, :cond_5

    .line 497
    iput-boolean v2, p0, Lcom/peel/social/a/e;->e:Z

    .line 498
    iget-object v0, p0, Lcom/peel/social/a/e;->a:Lcom/peel/social/a/a/b;

    invoke-interface {v0}, Lcom/peel/social/a/a/b;->a()V

    goto :goto_1

    .line 501
    :cond_5
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/peel/social/a/e;->e:Z

    .line 502
    iget-object v0, p0, Lcom/peel/social/a/e;->c:Lcom/peel/social/a/c;

    invoke-virtual {v0}, Lcom/peel/social/a/c;->d()V

    .line 503
    iput-boolean v2, p0, Lcom/peel/social/a/e;->d:Z

    goto :goto_1

    .line 507
    :cond_6
    iget-object v0, p0, Lcom/peel/social/a/e;->a:Lcom/peel/social/a/a/b;

    if-eqz v0, :cond_1

    .line 508
    iget-object v0, p0, Lcom/peel/social/a/e;->a:Lcom/peel/social/a/a/b;

    invoke-interface {v0}, Lcom/peel/social/a/a/b;->a()V

    goto :goto_1

    .line 519
    :pswitch_4
    iget-object v0, p0, Lcom/peel/social/a/e;->f:Lcom/peel/social/a/a/d;

    if-eqz v0, :cond_a

    .line 520
    if-eqz p3, :cond_7

    instance-of v0, p3, Lcom/facebook/as;

    if-nez v0, :cond_8

    :cond_7
    iget-object v0, p0, Lcom/peel/social/a/e;->c:Lcom/peel/social/a/c;

    invoke-virtual {v0}, Lcom/peel/social/a/c;->c()Z

    move-result v0

    if-nez v0, :cond_9

    .line 521
    :cond_8
    iget-object v0, p0, Lcom/peel/social/a/e;->f:Lcom/peel/social/a/a/d;

    sget-object v1, Lcom/peel/social/a/b;->a:Lcom/peel/social/a/b;

    invoke-interface {v0, v1}, Lcom/peel/social/a/a/d;->a(Lcom/peel/social/a/b;)V

    .line 526
    :goto_2
    iput-object v3, p0, Lcom/peel/social/a/e;->f:Lcom/peel/social/a/a/d;

    goto :goto_1

    .line 524
    :cond_9
    iget-object v0, p0, Lcom/peel/social/a/e;->f:Lcom/peel/social/a/a/d;

    invoke-interface {v0}, Lcom/peel/social/a/a/d;->a()V

    goto :goto_2

    .line 528
    :cond_a
    iget-boolean v0, p0, Lcom/peel/social/a/e;->e:Z

    if-eqz v0, :cond_1

    .line 529
    iput-boolean v2, p0, Lcom/peel/social/a/e;->e:Z

    .line 531
    iget-object v0, p0, Lcom/peel/social/a/e;->a:Lcom/peel/social/a/a/b;

    if-eqz v0, :cond_1

    .line 532
    iget-object v0, p0, Lcom/peel/social/a/e;->a:Lcom/peel/social/a/a/b;

    invoke-interface {v0}, Lcom/peel/social/a/a/b;->a()V

    goto/16 :goto_1

    .line 453
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public a(Z)V
    .locals 0

    .prologue
    .line 548
    iput-boolean p1, p0, Lcom/peel/social/a/e;->d:Z

    .line 549
    return-void
.end method
