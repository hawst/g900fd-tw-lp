.class public Lcom/peel/social/a/f;
.super Ljava/lang/Object;


# static fields
.field private static a:Lcom/peel/social/a/f;

.field private static b:Lcom/peel/social/a/g;

.field private static c:Landroid/app/Activity;

.field private static d:Lcom/peel/social/a/c;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 22
    sput-object v1, Lcom/peel/social/a/f;->a:Lcom/peel/social/a/f;

    .line 23
    new-instance v0, Lcom/peel/social/a/i;

    invoke-direct {v0}, Lcom/peel/social/a/i;-><init>()V

    invoke-virtual {v0}, Lcom/peel/social/a/i;->a()Lcom/peel/social/a/g;

    move-result-object v0

    sput-object v0, Lcom/peel/social/a/f;->b:Lcom/peel/social/a/g;

    .line 26
    sput-object v1, Lcom/peel/social/a/f;->d:Lcom/peel/social/a/c;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    return-void
.end method

.method public static a()Lcom/peel/social/a/f;
    .locals 1

    .prologue
    .line 88
    sget-object v0, Lcom/peel/social/a/f;->a:Lcom/peel/social/a/f;

    return-object v0
.end method

.method public static a(Landroid/app/Activity;)Lcom/peel/social/a/f;
    .locals 2

    .prologue
    .line 70
    sget-object v0, Lcom/peel/social/a/f;->a:Lcom/peel/social/a/f;

    if-nez v0, :cond_0

    .line 71
    new-instance v0, Lcom/peel/social/a/f;

    invoke-direct {v0}, Lcom/peel/social/a/f;-><init>()V

    sput-object v0, Lcom/peel/social/a/f;->a:Lcom/peel/social/a/f;

    .line 72
    new-instance v0, Lcom/peel/social/a/c;

    sget-object v1, Lcom/peel/social/a/f;->b:Lcom/peel/social/a/g;

    invoke-direct {v0, p0, v1}, Lcom/peel/social/a/c;-><init>(Landroid/app/Activity;Lcom/peel/social/a/g;)V

    sput-object v0, Lcom/peel/social/a/f;->d:Lcom/peel/social/a/c;

    .line 74
    :cond_0
    sput-object p0, Lcom/peel/social/a/f;->c:Landroid/app/Activity;

    .line 75
    sput-object p0, Lcom/peel/social/a/c;->a:Landroid/app/Activity;

    .line 76
    sget-object v0, Lcom/peel/social/a/f;->a:Lcom/peel/social/a/f;

    return-object v0
.end method

.method public static a(Lcom/peel/social/a/g;)V
    .locals 0

    .prologue
    .line 99
    sput-object p0, Lcom/peel/social/a/f;->b:Lcom/peel/social/a/g;

    .line 100
    sput-object p0, Lcom/peel/social/a/c;->b:Lcom/peel/social/a/g;

    .line 101
    return-void
.end method


# virtual methods
.method public a(Lcom/peel/social/a/a/b;)V
    .locals 1

    .prologue
    .line 118
    sget-object v0, Lcom/peel/social/a/f;->d:Lcom/peel/social/a/c;

    invoke-virtual {v0, p1}, Lcom/peel/social/a/c;->a(Lcom/peel/social/a/a/b;)V

    .line 119
    return-void
.end method

.method public a(Landroid/app/Activity;IILandroid/content/Intent;)Z
    .locals 1

    .prologue
    .line 223
    sget-object v0, Lcom/peel/social/a/f;->d:Lcom/peel/social/a/c;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/peel/social/a/c;->a(Landroid/app/Activity;IILandroid/content/Intent;)Z

    move-result v0

    return v0
.end method

.method public b()Lcom/facebook/ca;
    .locals 1

    .prologue
    .line 200
    sget-object v0, Lcom/peel/social/a/f;->d:Lcom/peel/social/a/c;

    invoke-virtual {v0}, Lcom/peel/social/a/c;->a()Lcom/facebook/ca;

    move-result-object v0

    return-object v0
.end method
