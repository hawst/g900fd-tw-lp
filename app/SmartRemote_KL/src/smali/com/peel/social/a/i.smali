.class public Lcom/peel/social/a/i;
.super Ljava/lang/Object;


# instance fields
.field private a:Ljava/lang/String;

.field private b:Ljava/lang/String;

.field private c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private e:Lcom/facebook/ct;

.field private f:Lcom/facebook/cu;

.field private g:Z

.field private h:Z

.field private i:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 168
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 158
    iput-object v1, p0, Lcom/peel/social/a/i;->a:Ljava/lang/String;

    .line 159
    iput-object v1, p0, Lcom/peel/social/a/i;->b:Ljava/lang/String;

    .line 160
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/peel/social/a/i;->c:Ljava/util/List;

    .line 161
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/peel/social/a/i;->d:Ljava/util/List;

    .line 162
    sget-object v0, Lcom/facebook/ct;->c:Lcom/facebook/ct;

    iput-object v0, p0, Lcom/peel/social/a/i;->e:Lcom/facebook/ct;

    .line 163
    sget-object v0, Lcom/facebook/cu;->a:Lcom/facebook/cu;

    iput-object v0, p0, Lcom/peel/social/a/i;->f:Lcom/facebook/cu;

    .line 164
    iput-boolean v2, p0, Lcom/peel/social/a/i;->g:Z

    .line 165
    iput-boolean v2, p0, Lcom/peel/social/a/i;->h:Z

    .line 166
    iput-object v1, p0, Lcom/peel/social/a/i;->i:Ljava/lang/String;

    .line 169
    return-void
.end method

.method static synthetic a(Lcom/peel/social/a/i;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 157
    iget-object v0, p0, Lcom/peel/social/a/i;->a:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic b(Lcom/peel/social/a/i;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 157
    iget-object v0, p0, Lcom/peel/social/a/i;->b:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic c(Lcom/peel/social/a/i;)Ljava/util/List;
    .locals 1

    .prologue
    .line 157
    iget-object v0, p0, Lcom/peel/social/a/i;->c:Ljava/util/List;

    return-object v0
.end method

.method static synthetic d(Lcom/peel/social/a/i;)Ljava/util/List;
    .locals 1

    .prologue
    .line 157
    iget-object v0, p0, Lcom/peel/social/a/i;->d:Ljava/util/List;

    return-object v0
.end method

.method static synthetic e(Lcom/peel/social/a/i;)Lcom/facebook/ct;
    .locals 1

    .prologue
    .line 157
    iget-object v0, p0, Lcom/peel/social/a/i;->e:Lcom/facebook/ct;

    return-object v0
.end method

.method static synthetic f(Lcom/peel/social/a/i;)Lcom/facebook/cu;
    .locals 1

    .prologue
    .line 157
    iget-object v0, p0, Lcom/peel/social/a/i;->f:Lcom/facebook/cu;

    return-object v0
.end method

.method static synthetic g(Lcom/peel/social/a/i;)Z
    .locals 1

    .prologue
    .line 157
    iget-boolean v0, p0, Lcom/peel/social/a/i;->g:Z

    return v0
.end method

.method static synthetic h(Lcom/peel/social/a/i;)Z
    .locals 1

    .prologue
    .line 157
    iget-boolean v0, p0, Lcom/peel/social/a/i;->h:Z

    return v0
.end method

.method static synthetic i(Lcom/peel/social/a/i;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 157
    iget-object v0, p0, Lcom/peel/social/a/i;->i:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public a()Lcom/peel/social/a/g;
    .locals 2

    .prologue
    .line 286
    new-instance v0, Lcom/peel/social/a/g;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/peel/social/a/g;-><init>(Lcom/peel/social/a/i;Lcom/peel/social/a/h;)V

    return-object v0
.end method

.method public a(Lcom/facebook/ct;)Lcom/peel/social/a/i;
    .locals 0

    .prologue
    .line 222
    iput-object p1, p0, Lcom/peel/social/a/i;->e:Lcom/facebook/ct;

    .line 223
    return-object p0
.end method

.method public a(Ljava/lang/String;)Lcom/peel/social/a/i;
    .locals 0

    .prologue
    .line 179
    iput-object p1, p0, Lcom/peel/social/a/i;->a:Ljava/lang/String;

    .line 180
    return-object p0
.end method

.method public a(Z)Lcom/peel/social/a/i;
    .locals 0

    .prologue
    .line 251
    iput-boolean p1, p0, Lcom/peel/social/a/i;->g:Z

    .line 252
    return-object p0
.end method

.method public a([Lcom/peel/social/a/a;)Lcom/peel/social/a/i;
    .locals 5

    .prologue
    .line 200
    array-length v1, p1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    aget-object v2, p1, v0

    .line 201
    sget-object v3, Lcom/peel/social/a/h;->a:[I

    invoke-virtual {v2}, Lcom/peel/social/a/a;->b()Lcom/facebook/b/bj;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/b/bj;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    .line 200
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 203
    :pswitch_0
    iget-object v3, p0, Lcom/peel/social/a/i;->c:Ljava/util/List;

    invoke-virtual {v2}, Lcom/peel/social/a/a;->a()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 206
    :pswitch_1
    iget-object v3, p0, Lcom/peel/social/a/i;->d:Ljava/util/List;

    invoke-virtual {v2}, Lcom/peel/social/a/a;->a()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 213
    :cond_0
    return-object p0

    .line 201
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public b(Ljava/lang/String;)Lcom/peel/social/a/i;
    .locals 0

    .prologue
    .line 190
    iput-object p1, p0, Lcom/peel/social/a/i;->b:Ljava/lang/String;

    .line 191
    return-object p0
.end method
