.class public Lcom/peel/social/a/g;
.super Ljava/lang/Object;


# instance fields
.field a:Z

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private f:Lcom/facebook/ct;

.field private g:Lcom/facebook/cu;

.field private h:Z

.field private i:Z

.field private j:Ljava/lang/String;


# direct methods
.method private constructor <init>(Lcom/peel/social/a/i;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    iput-object v0, p0, Lcom/peel/social/a/g;->d:Ljava/util/List;

    .line 14
    iput-object v0, p0, Lcom/peel/social/a/g;->e:Ljava/util/List;

    .line 15
    iput-object v0, p0, Lcom/peel/social/a/g;->f:Lcom/facebook/ct;

    .line 16
    iput-object v0, p0, Lcom/peel/social/a/g;->g:Lcom/facebook/cu;

    .line 17
    iput-boolean v1, p0, Lcom/peel/social/a/g;->h:Z

    .line 18
    iput-boolean v1, p0, Lcom/peel/social/a/g;->a:Z

    .line 19
    iput-boolean v1, p0, Lcom/peel/social/a/g;->i:Z

    .line 20
    iput-object v0, p0, Lcom/peel/social/a/g;->j:Ljava/lang/String;

    .line 23
    invoke-static {p1}, Lcom/peel/social/a/i;->a(Lcom/peel/social/a/i;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/social/a/g;->b:Ljava/lang/String;

    .line 24
    invoke-static {p1}, Lcom/peel/social/a/i;->b(Lcom/peel/social/a/i;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/social/a/g;->c:Ljava/lang/String;

    .line 25
    invoke-static {p1}, Lcom/peel/social/a/i;->c(Lcom/peel/social/a/i;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/social/a/g;->d:Ljava/util/List;

    .line 26
    invoke-static {p1}, Lcom/peel/social/a/i;->d(Lcom/peel/social/a/i;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/social/a/g;->e:Ljava/util/List;

    .line 27
    invoke-static {p1}, Lcom/peel/social/a/i;->e(Lcom/peel/social/a/i;)Lcom/facebook/ct;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/social/a/g;->f:Lcom/facebook/ct;

    .line 28
    invoke-static {p1}, Lcom/peel/social/a/i;->f(Lcom/peel/social/a/i;)Lcom/facebook/cu;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/social/a/g;->g:Lcom/facebook/cu;

    .line 29
    invoke-static {p1}, Lcom/peel/social/a/i;->g(Lcom/peel/social/a/i;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/peel/social/a/g;->a:Z

    .line 30
    invoke-static {p1}, Lcom/peel/social/a/i;->h(Lcom/peel/social/a/i;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/peel/social/a/g;->i:Z

    .line 31
    invoke-static {p1}, Lcom/peel/social/a/i;->i(Lcom/peel/social/a/i;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/social/a/g;->j:Ljava/lang/String;

    .line 33
    iget-object v0, p0, Lcom/peel/social/a/g;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 34
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/peel/social/a/g;->h:Z

    .line 36
    :cond_0
    return-void
.end method

.method synthetic constructor <init>(Lcom/peel/social/a/i;Lcom/peel/social/a/h;)V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0, p1}, Lcom/peel/social/a/g;-><init>(Lcom/peel/social/a/i;)V

    return-void
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/peel/social/a/g;->b:Ljava/lang/String;

    return-object v0
.end method

.method public b()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 62
    iget-object v0, p0, Lcom/peel/social/a/g;->d:Ljava/util/List;

    return-object v0
.end method

.method public c()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 71
    iget-object v0, p0, Lcom/peel/social/a/g;->e:Ljava/util/List;

    return-object v0
.end method

.method d()Z
    .locals 1

    .prologue
    .line 80
    iget-boolean v0, p0, Lcom/peel/social/a/g;->h:Z

    return v0
.end method

.method e()Lcom/facebook/cu;
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lcom/peel/social/a/g;->g:Lcom/facebook/cu;

    return-object v0
.end method

.method f()Lcom/facebook/ct;
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Lcom/peel/social/a/g;->f:Lcom/facebook/ct;

    return-object v0
.end method

.method g()Z
    .locals 1

    .prologue
    .line 126
    iget-boolean v0, p0, Lcom/peel/social/a/g;->a:Z

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 292
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 293
    const-string/jumbo v1, "[ "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "mAppId:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/peel/social/a/g;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "mNamespace:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/peel/social/a/g;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "mDefaultAudience:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/peel/social/a/g;->f:Lcom/facebook/ct;

    invoke-virtual {v2}, Lcom/facebook/ct;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ", "

    .line 294
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "mLoginBehavior:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/peel/social/a/g;->g:Lcom/facebook/cu;

    invoke-virtual {v2}, Lcom/facebook/cu;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "mReadPermissions:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/peel/social/a/g;->d:Ljava/util/List;

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "mPublishPermissions:"

    .line 295
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/peel/social/a/g;->e:Ljava/util/List;

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " ]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 296
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
