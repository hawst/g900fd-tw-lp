.class public final enum Lcom/peel/social/a/a;
.super Ljava/lang/Enum;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/peel/social/a/a;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum A:Lcom/peel/social/a/a;

.field public static final enum B:Lcom/peel/social/a/a;

.field public static final enum C:Lcom/peel/social/a/a;

.field public static final enum D:Lcom/peel/social/a/a;

.field public static final enum E:Lcom/peel/social/a/a;

.field public static final enum F:Lcom/peel/social/a/a;

.field public static final enum G:Lcom/peel/social/a/a;

.field public static final enum H:Lcom/peel/social/a/a;

.field public static final enum I:Lcom/peel/social/a/a;

.field private static final synthetic L:[Lcom/peel/social/a/a;

.field public static final enum a:Lcom/peel/social/a/a;

.field public static final enum b:Lcom/peel/social/a/a;

.field public static final enum c:Lcom/peel/social/a/a;

.field public static final enum d:Lcom/peel/social/a/a;

.field public static final enum e:Lcom/peel/social/a/a;

.field public static final enum f:Lcom/peel/social/a/a;

.field public static final enum g:Lcom/peel/social/a/a;

.field public static final enum h:Lcom/peel/social/a/a;

.field public static final enum i:Lcom/peel/social/a/a;

.field public static final enum j:Lcom/peel/social/a/a;

.field public static final enum k:Lcom/peel/social/a/a;

.field public static final enum l:Lcom/peel/social/a/a;

.field public static final enum m:Lcom/peel/social/a/a;

.field public static final enum n:Lcom/peel/social/a/a;

.field public static final enum o:Lcom/peel/social/a/a;

.field public static final enum p:Lcom/peel/social/a/a;

.field public static final enum q:Lcom/peel/social/a/a;

.field public static final enum r:Lcom/peel/social/a/a;

.field public static final enum s:Lcom/peel/social/a/a;

.field public static final enum t:Lcom/peel/social/a/a;

.field public static final enum u:Lcom/peel/social/a/a;

.field public static final enum v:Lcom/peel/social/a/a;

.field public static final enum w:Lcom/peel/social/a/a;

.field public static final enum x:Lcom/peel/social/a/a;

.field public static final enum y:Lcom/peel/social/a/a;

.field public static final enum z:Lcom/peel/social/a/a;


# instance fields
.field private J:Ljava/lang/String;

.field private K:Lcom/facebook/b/bj;


# direct methods
.method static constructor <clinit>()V
    .locals 10

    .prologue
    const/4 v9, 0x4

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 14
    new-instance v0, Lcom/peel/social/a/a;

    const-string/jumbo v1, "PUBLIC_PROFILE"

    const-string/jumbo v2, "public_profile"

    sget-object v3, Lcom/peel/social/a/b;->b:Lcom/peel/social/a/b;

    invoke-direct {v0, v1, v5, v2, v3}, Lcom/peel/social/a/a;-><init>(Ljava/lang/String;ILjava/lang/String;Lcom/peel/social/a/b;)V

    sput-object v0, Lcom/peel/social/a/a;->a:Lcom/peel/social/a/a;

    .line 15
    new-instance v0, Lcom/peel/social/a/a;

    const-string/jumbo v1, "USER_ABOUT_ME"

    const-string/jumbo v2, "user_about_me"

    sget-object v3, Lcom/peel/social/a/b;->b:Lcom/peel/social/a/b;

    invoke-direct {v0, v1, v6, v2, v3}, Lcom/peel/social/a/a;-><init>(Ljava/lang/String;ILjava/lang/String;Lcom/peel/social/a/b;)V

    sput-object v0, Lcom/peel/social/a/a;->b:Lcom/peel/social/a/a;

    .line 16
    new-instance v0, Lcom/peel/social/a/a;

    const-string/jumbo v1, "USER_ACTIONS_BOOKS"

    const-string/jumbo v2, "user_actions.books"

    sget-object v3, Lcom/peel/social/a/b;->b:Lcom/peel/social/a/b;

    invoke-direct {v0, v1, v7, v2, v3}, Lcom/peel/social/a/a;-><init>(Ljava/lang/String;ILjava/lang/String;Lcom/peel/social/a/b;)V

    sput-object v0, Lcom/peel/social/a/a;->c:Lcom/peel/social/a/a;

    .line 17
    new-instance v0, Lcom/peel/social/a/a;

    const-string/jumbo v1, "USER_ACTIONS_MUSIC"

    const-string/jumbo v2, "user_actions.music"

    sget-object v3, Lcom/peel/social/a/b;->b:Lcom/peel/social/a/b;

    invoke-direct {v0, v1, v8, v2, v3}, Lcom/peel/social/a/a;-><init>(Ljava/lang/String;ILjava/lang/String;Lcom/peel/social/a/b;)V

    sput-object v0, Lcom/peel/social/a/a;->d:Lcom/peel/social/a/a;

    .line 18
    new-instance v0, Lcom/peel/social/a/a;

    const-string/jumbo v1, "USER_ACTIONS_NEWS"

    const-string/jumbo v2, "user_actions.news"

    sget-object v3, Lcom/peel/social/a/b;->b:Lcom/peel/social/a/b;

    invoke-direct {v0, v1, v9, v2, v3}, Lcom/peel/social/a/a;-><init>(Ljava/lang/String;ILjava/lang/String;Lcom/peel/social/a/b;)V

    sput-object v0, Lcom/peel/social/a/a;->e:Lcom/peel/social/a/a;

    .line 19
    new-instance v0, Lcom/peel/social/a/a;

    const-string/jumbo v1, "USER_ACTIONS_VIDEO"

    const/4 v2, 0x5

    const-string/jumbo v3, "user_actions.video"

    sget-object v4, Lcom/peel/social/a/b;->b:Lcom/peel/social/a/b;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/peel/social/a/a;-><init>(Ljava/lang/String;ILjava/lang/String;Lcom/peel/social/a/b;)V

    sput-object v0, Lcom/peel/social/a/a;->f:Lcom/peel/social/a/a;

    .line 20
    new-instance v0, Lcom/peel/social/a/a;

    const-string/jumbo v1, "USER_ACTIVITIES"

    const/4 v2, 0x6

    const-string/jumbo v3, "user_activities"

    sget-object v4, Lcom/peel/social/a/b;->b:Lcom/peel/social/a/b;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/peel/social/a/a;-><init>(Ljava/lang/String;ILjava/lang/String;Lcom/peel/social/a/b;)V

    sput-object v0, Lcom/peel/social/a/a;->g:Lcom/peel/social/a/a;

    .line 21
    new-instance v0, Lcom/peel/social/a/a;

    const-string/jumbo v1, "USER_BIRTHDAY"

    const/4 v2, 0x7

    const-string/jumbo v3, "user_birthday"

    sget-object v4, Lcom/peel/social/a/b;->b:Lcom/peel/social/a/b;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/peel/social/a/a;-><init>(Ljava/lang/String;ILjava/lang/String;Lcom/peel/social/a/b;)V

    sput-object v0, Lcom/peel/social/a/a;->h:Lcom/peel/social/a/a;

    .line 22
    new-instance v0, Lcom/peel/social/a/a;

    const-string/jumbo v1, "USER_EDUCATION_HISTORY"

    const/16 v2, 0x8

    const-string/jumbo v3, "user_education_history"

    sget-object v4, Lcom/peel/social/a/b;->b:Lcom/peel/social/a/b;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/peel/social/a/a;-><init>(Ljava/lang/String;ILjava/lang/String;Lcom/peel/social/a/b;)V

    sput-object v0, Lcom/peel/social/a/a;->i:Lcom/peel/social/a/a;

    .line 23
    new-instance v0, Lcom/peel/social/a/a;

    const-string/jumbo v1, "USER_EVENTS"

    const/16 v2, 0x9

    const-string/jumbo v3, "user_events"

    sget-object v4, Lcom/peel/social/a/b;->b:Lcom/peel/social/a/b;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/peel/social/a/a;-><init>(Ljava/lang/String;ILjava/lang/String;Lcom/peel/social/a/b;)V

    sput-object v0, Lcom/peel/social/a/a;->j:Lcom/peel/social/a/a;

    .line 24
    new-instance v0, Lcom/peel/social/a/a;

    const-string/jumbo v1, "USER_FRIENDS"

    const/16 v2, 0xa

    const-string/jumbo v3, "user_friends"

    sget-object v4, Lcom/peel/social/a/b;->b:Lcom/peel/social/a/b;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/peel/social/a/a;-><init>(Ljava/lang/String;ILjava/lang/String;Lcom/peel/social/a/b;)V

    sput-object v0, Lcom/peel/social/a/a;->k:Lcom/peel/social/a/a;

    .line 25
    new-instance v0, Lcom/peel/social/a/a;

    const-string/jumbo v1, "USER_GAMES_ACTIVITY"

    const/16 v2, 0xb

    const-string/jumbo v3, "user_games_activity"

    sget-object v4, Lcom/peel/social/a/b;->b:Lcom/peel/social/a/b;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/peel/social/a/a;-><init>(Ljava/lang/String;ILjava/lang/String;Lcom/peel/social/a/b;)V

    sput-object v0, Lcom/peel/social/a/a;->l:Lcom/peel/social/a/a;

    .line 26
    new-instance v0, Lcom/peel/social/a/a;

    const-string/jumbo v1, "USER_GROUPS"

    const/16 v2, 0xc

    const-string/jumbo v3, "user_groups"

    sget-object v4, Lcom/peel/social/a/b;->b:Lcom/peel/social/a/b;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/peel/social/a/a;-><init>(Ljava/lang/String;ILjava/lang/String;Lcom/peel/social/a/b;)V

    sput-object v0, Lcom/peel/social/a/a;->m:Lcom/peel/social/a/a;

    .line 27
    new-instance v0, Lcom/peel/social/a/a;

    const-string/jumbo v1, "USER_HOMETOWN"

    const/16 v2, 0xd

    const-string/jumbo v3, "user_hometown"

    sget-object v4, Lcom/peel/social/a/b;->b:Lcom/peel/social/a/b;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/peel/social/a/a;-><init>(Ljava/lang/String;ILjava/lang/String;Lcom/peel/social/a/b;)V

    sput-object v0, Lcom/peel/social/a/a;->n:Lcom/peel/social/a/a;

    .line 28
    new-instance v0, Lcom/peel/social/a/a;

    const-string/jumbo v1, "USER_INTERESTS"

    const/16 v2, 0xe

    const-string/jumbo v3, "user_interests"

    sget-object v4, Lcom/peel/social/a/b;->b:Lcom/peel/social/a/b;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/peel/social/a/a;-><init>(Ljava/lang/String;ILjava/lang/String;Lcom/peel/social/a/b;)V

    sput-object v0, Lcom/peel/social/a/a;->o:Lcom/peel/social/a/a;

    .line 29
    new-instance v0, Lcom/peel/social/a/a;

    const-string/jumbo v1, "USER_LIKES"

    const/16 v2, 0xf

    const-string/jumbo v3, "user_likes"

    sget-object v4, Lcom/peel/social/a/b;->b:Lcom/peel/social/a/b;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/peel/social/a/a;-><init>(Ljava/lang/String;ILjava/lang/String;Lcom/peel/social/a/b;)V

    sput-object v0, Lcom/peel/social/a/a;->p:Lcom/peel/social/a/a;

    .line 30
    new-instance v0, Lcom/peel/social/a/a;

    const-string/jumbo v1, "USER_LOCATION"

    const/16 v2, 0x10

    const-string/jumbo v3, "user_location"

    sget-object v4, Lcom/peel/social/a/b;->b:Lcom/peel/social/a/b;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/peel/social/a/a;-><init>(Ljava/lang/String;ILjava/lang/String;Lcom/peel/social/a/b;)V

    sput-object v0, Lcom/peel/social/a/a;->q:Lcom/peel/social/a/a;

    .line 31
    new-instance v0, Lcom/peel/social/a/a;

    const-string/jumbo v1, "USER_PHOTOS"

    const/16 v2, 0x11

    const-string/jumbo v3, "user_photos"

    sget-object v4, Lcom/peel/social/a/b;->b:Lcom/peel/social/a/b;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/peel/social/a/a;-><init>(Ljava/lang/String;ILjava/lang/String;Lcom/peel/social/a/b;)V

    sput-object v0, Lcom/peel/social/a/a;->r:Lcom/peel/social/a/a;

    .line 32
    new-instance v0, Lcom/peel/social/a/a;

    const-string/jumbo v1, "USER_RELATIONSHIPS"

    const/16 v2, 0x12

    const-string/jumbo v3, "user_relationships"

    sget-object v4, Lcom/peel/social/a/b;->b:Lcom/peel/social/a/b;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/peel/social/a/a;-><init>(Ljava/lang/String;ILjava/lang/String;Lcom/peel/social/a/b;)V

    sput-object v0, Lcom/peel/social/a/a;->s:Lcom/peel/social/a/a;

    .line 33
    new-instance v0, Lcom/peel/social/a/a;

    const-string/jumbo v1, "USER_RELATIONSHIP_DETAILS"

    const/16 v2, 0x13

    const-string/jumbo v3, "user_relationship_details"

    sget-object v4, Lcom/peel/social/a/b;->b:Lcom/peel/social/a/b;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/peel/social/a/a;-><init>(Ljava/lang/String;ILjava/lang/String;Lcom/peel/social/a/b;)V

    sput-object v0, Lcom/peel/social/a/a;->t:Lcom/peel/social/a/a;

    .line 34
    new-instance v0, Lcom/peel/social/a/a;

    const-string/jumbo v1, "USER_RELIGION_POLITICS"

    const/16 v2, 0x14

    const-string/jumbo v3, "user_religion_politics"

    sget-object v4, Lcom/peel/social/a/b;->b:Lcom/peel/social/a/b;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/peel/social/a/a;-><init>(Ljava/lang/String;ILjava/lang/String;Lcom/peel/social/a/b;)V

    sput-object v0, Lcom/peel/social/a/a;->u:Lcom/peel/social/a/a;

    .line 35
    new-instance v0, Lcom/peel/social/a/a;

    const-string/jumbo v1, "USER_STATUS"

    const/16 v2, 0x15

    const-string/jumbo v3, "user_status"

    sget-object v4, Lcom/peel/social/a/b;->b:Lcom/peel/social/a/b;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/peel/social/a/a;-><init>(Ljava/lang/String;ILjava/lang/String;Lcom/peel/social/a/b;)V

    sput-object v0, Lcom/peel/social/a/a;->v:Lcom/peel/social/a/a;

    .line 36
    new-instance v0, Lcom/peel/social/a/a;

    const-string/jumbo v1, "USER_TAGGED_PLACES"

    const/16 v2, 0x16

    const-string/jumbo v3, "user_tagged_places"

    sget-object v4, Lcom/peel/social/a/b;->b:Lcom/peel/social/a/b;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/peel/social/a/a;-><init>(Ljava/lang/String;ILjava/lang/String;Lcom/peel/social/a/b;)V

    sput-object v0, Lcom/peel/social/a/a;->w:Lcom/peel/social/a/a;

    .line 37
    new-instance v0, Lcom/peel/social/a/a;

    const-string/jumbo v1, "USER_VIDEOS"

    const/16 v2, 0x17

    const-string/jumbo v3, "user_videos"

    sget-object v4, Lcom/peel/social/a/b;->b:Lcom/peel/social/a/b;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/peel/social/a/a;-><init>(Ljava/lang/String;ILjava/lang/String;Lcom/peel/social/a/b;)V

    sput-object v0, Lcom/peel/social/a/a;->x:Lcom/peel/social/a/a;

    .line 38
    new-instance v0, Lcom/peel/social/a/a;

    const-string/jumbo v1, "USER_WEBSITE"

    const/16 v2, 0x18

    const-string/jumbo v3, "user_website"

    sget-object v4, Lcom/peel/social/a/b;->b:Lcom/peel/social/a/b;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/peel/social/a/a;-><init>(Ljava/lang/String;ILjava/lang/String;Lcom/peel/social/a/b;)V

    sput-object v0, Lcom/peel/social/a/a;->y:Lcom/peel/social/a/a;

    .line 39
    new-instance v0, Lcom/peel/social/a/a;

    const-string/jumbo v1, "USER_WORK_HISTORY"

    const/16 v2, 0x19

    const-string/jumbo v3, "user_work_history"

    sget-object v4, Lcom/peel/social/a/b;->b:Lcom/peel/social/a/b;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/peel/social/a/a;-><init>(Ljava/lang/String;ILjava/lang/String;Lcom/peel/social/a/b;)V

    sput-object v0, Lcom/peel/social/a/a;->z:Lcom/peel/social/a/a;

    .line 40
    new-instance v0, Lcom/peel/social/a/a;

    const-string/jumbo v1, "READ_FRIENDLISTS"

    const/16 v2, 0x1a

    const-string/jumbo v3, "read_friendlists"

    sget-object v4, Lcom/peel/social/a/b;->b:Lcom/peel/social/a/b;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/peel/social/a/a;-><init>(Ljava/lang/String;ILjava/lang/String;Lcom/peel/social/a/b;)V

    sput-object v0, Lcom/peel/social/a/a;->A:Lcom/peel/social/a/a;

    .line 41
    new-instance v0, Lcom/peel/social/a/a;

    const-string/jumbo v1, "READ_INSIGHTS"

    const/16 v2, 0x1b

    const-string/jumbo v3, "read_insights"

    sget-object v4, Lcom/peel/social/a/b;->b:Lcom/peel/social/a/b;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/peel/social/a/a;-><init>(Ljava/lang/String;ILjava/lang/String;Lcom/peel/social/a/b;)V

    sput-object v0, Lcom/peel/social/a/a;->B:Lcom/peel/social/a/a;

    .line 42
    new-instance v0, Lcom/peel/social/a/a;

    const-string/jumbo v1, "READ_MAILBOX"

    const/16 v2, 0x1c

    const-string/jumbo v3, "read_mailbox"

    sget-object v4, Lcom/peel/social/a/b;->b:Lcom/peel/social/a/b;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/peel/social/a/a;-><init>(Ljava/lang/String;ILjava/lang/String;Lcom/peel/social/a/b;)V

    sput-object v0, Lcom/peel/social/a/a;->C:Lcom/peel/social/a/a;

    .line 43
    new-instance v0, Lcom/peel/social/a/a;

    const-string/jumbo v1, "READ_STREAM"

    const/16 v2, 0x1d

    const-string/jumbo v3, "read_stream"

    sget-object v4, Lcom/peel/social/a/b;->b:Lcom/peel/social/a/b;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/peel/social/a/a;-><init>(Ljava/lang/String;ILjava/lang/String;Lcom/peel/social/a/b;)V

    sput-object v0, Lcom/peel/social/a/a;->D:Lcom/peel/social/a/a;

    .line 44
    new-instance v0, Lcom/peel/social/a/a;

    const-string/jumbo v1, "EMAIL"

    const/16 v2, 0x1e

    const-string/jumbo v3, "email"

    sget-object v4, Lcom/peel/social/a/b;->b:Lcom/peel/social/a/b;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/peel/social/a/a;-><init>(Ljava/lang/String;ILjava/lang/String;Lcom/peel/social/a/b;)V

    sput-object v0, Lcom/peel/social/a/a;->E:Lcom/peel/social/a/a;

    .line 46
    new-instance v0, Lcom/peel/social/a/a;

    const-string/jumbo v1, "PUBLISH_ACTION"

    const/16 v2, 0x1f

    const-string/jumbo v3, "publish_actions"

    sget-object v4, Lcom/peel/social/a/b;->a:Lcom/peel/social/a/b;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/peel/social/a/a;-><init>(Ljava/lang/String;ILjava/lang/String;Lcom/peel/social/a/b;)V

    sput-object v0, Lcom/peel/social/a/a;->F:Lcom/peel/social/a/a;

    .line 47
    new-instance v0, Lcom/peel/social/a/a;

    const-string/jumbo v1, "RSVP_EVENT"

    const/16 v2, 0x20

    const-string/jumbo v3, "rsvp_event"

    sget-object v4, Lcom/peel/social/a/b;->a:Lcom/peel/social/a/b;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/peel/social/a/a;-><init>(Ljava/lang/String;ILjava/lang/String;Lcom/peel/social/a/b;)V

    sput-object v0, Lcom/peel/social/a/a;->G:Lcom/peel/social/a/a;

    .line 48
    new-instance v0, Lcom/peel/social/a/a;

    const-string/jumbo v1, "MANAGE_NOTIFICATIONS"

    const/16 v2, 0x21

    const-string/jumbo v3, "manage_notifications"

    sget-object v4, Lcom/peel/social/a/b;->a:Lcom/peel/social/a/b;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/peel/social/a/a;-><init>(Ljava/lang/String;ILjava/lang/String;Lcom/peel/social/a/b;)V

    sput-object v0, Lcom/peel/social/a/a;->H:Lcom/peel/social/a/a;

    .line 49
    new-instance v0, Lcom/peel/social/a/a;

    const-string/jumbo v1, "MANAGE_PAGES"

    const/16 v2, 0x22

    const-string/jumbo v3, "manage_pages"

    sget-object v4, Lcom/peel/social/a/b;->a:Lcom/peel/social/a/b;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/peel/social/a/a;-><init>(Ljava/lang/String;ILjava/lang/String;Lcom/peel/social/a/b;)V

    sput-object v0, Lcom/peel/social/a/a;->I:Lcom/peel/social/a/a;

    .line 12
    const/16 v0, 0x23

    new-array v0, v0, [Lcom/peel/social/a/a;

    sget-object v1, Lcom/peel/social/a/a;->a:Lcom/peel/social/a/a;

    aput-object v1, v0, v5

    sget-object v1, Lcom/peel/social/a/a;->b:Lcom/peel/social/a/a;

    aput-object v1, v0, v6

    sget-object v1, Lcom/peel/social/a/a;->c:Lcom/peel/social/a/a;

    aput-object v1, v0, v7

    sget-object v1, Lcom/peel/social/a/a;->d:Lcom/peel/social/a/a;

    aput-object v1, v0, v8

    sget-object v1, Lcom/peel/social/a/a;->e:Lcom/peel/social/a/a;

    aput-object v1, v0, v9

    const/4 v1, 0x5

    sget-object v2, Lcom/peel/social/a/a;->f:Lcom/peel/social/a/a;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/peel/social/a/a;->g:Lcom/peel/social/a/a;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/peel/social/a/a;->h:Lcom/peel/social/a/a;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/peel/social/a/a;->i:Lcom/peel/social/a/a;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/peel/social/a/a;->j:Lcom/peel/social/a/a;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/peel/social/a/a;->k:Lcom/peel/social/a/a;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/peel/social/a/a;->l:Lcom/peel/social/a/a;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/peel/social/a/a;->m:Lcom/peel/social/a/a;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/peel/social/a/a;->n:Lcom/peel/social/a/a;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/peel/social/a/a;->o:Lcom/peel/social/a/a;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/peel/social/a/a;->p:Lcom/peel/social/a/a;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/peel/social/a/a;->q:Lcom/peel/social/a/a;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/peel/social/a/a;->r:Lcom/peel/social/a/a;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/peel/social/a/a;->s:Lcom/peel/social/a/a;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lcom/peel/social/a/a;->t:Lcom/peel/social/a/a;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lcom/peel/social/a/a;->u:Lcom/peel/social/a/a;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Lcom/peel/social/a/a;->v:Lcom/peel/social/a/a;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, Lcom/peel/social/a/a;->w:Lcom/peel/social/a/a;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, Lcom/peel/social/a/a;->x:Lcom/peel/social/a/a;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, Lcom/peel/social/a/a;->y:Lcom/peel/social/a/a;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, Lcom/peel/social/a/a;->z:Lcom/peel/social/a/a;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, Lcom/peel/social/a/a;->A:Lcom/peel/social/a/a;

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    sget-object v2, Lcom/peel/social/a/a;->B:Lcom/peel/social/a/a;

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    sget-object v2, Lcom/peel/social/a/a;->C:Lcom/peel/social/a/a;

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    sget-object v2, Lcom/peel/social/a/a;->D:Lcom/peel/social/a/a;

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    sget-object v2, Lcom/peel/social/a/a;->E:Lcom/peel/social/a/a;

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    sget-object v2, Lcom/peel/social/a/a;->F:Lcom/peel/social/a/a;

    aput-object v2, v0, v1

    const/16 v1, 0x20

    sget-object v2, Lcom/peel/social/a/a;->G:Lcom/peel/social/a/a;

    aput-object v2, v0, v1

    const/16 v1, 0x21

    sget-object v2, Lcom/peel/social/a/a;->H:Lcom/peel/social/a/a;

    aput-object v2, v0, v1

    const/16 v1, 0x22

    sget-object v2, Lcom/peel/social/a/a;->I:Lcom/peel/social/a/a;

    aput-object v2, v0, v1

    sput-object v0, Lcom/peel/social/a/a;->L:[Lcom/peel/social/a/a;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;Lcom/peel/social/a/b;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/peel/social/a/b;",
            ")V"
        }
    .end annotation

    .prologue
    .line 101
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 102
    iput-object p3, p0, Lcom/peel/social/a/a;->J:Ljava/lang/String;

    .line 103
    invoke-static {p4}, Lcom/peel/social/a/b;->a(Lcom/peel/social/a/b;)Lcom/facebook/b/bj;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/social/a/a;->K:Lcom/facebook/b/bj;

    .line 104
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/peel/social/a/a;
    .locals 1

    .prologue
    .line 12
    const-class v0, Lcom/peel/social/a/a;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/peel/social/a/a;

    return-object v0
.end method

.method public static values()[Lcom/peel/social/a/a;
    .locals 1

    .prologue
    .line 12
    sget-object v0, Lcom/peel/social/a/a;->L:[Lcom/peel/social/a/a;

    invoke-virtual {v0}, [Lcom/peel/social/a/a;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/peel/social/a/a;

    return-object v0
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 107
    iget-object v0, p0, Lcom/peel/social/a/a;->J:Ljava/lang/String;

    return-object v0
.end method

.method public b()Lcom/facebook/b/bj;
    .locals 1

    .prologue
    .line 111
    iget-object v0, p0, Lcom/peel/social/a/a;->K:Lcom/facebook/b/bj;

    return-object v0
.end method
