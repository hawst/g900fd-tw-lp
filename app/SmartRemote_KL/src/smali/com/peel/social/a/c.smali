.class public Lcom/peel/social/a/c;
.super Ljava/lang/Object;


# static fields
.field static a:Landroid/app/Activity;

.field static b:Lcom/peel/social/a/g;

.field private static final c:Ljava/lang/String;


# instance fields
.field private final d:Lcom/peel/social/a/e;

.field private e:Lcom/facebook/db;

.field private f:Lcom/facebook/widget/b;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 26
    const-class v0, Lcom/peel/social/a/c;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/peel/social/a/c;->c:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/app/Activity;Lcom/peel/social/a/g;)V
    .locals 2

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    sput-object p1, Lcom/peel/social/a/c;->a:Landroid/app/Activity;

    .line 37
    sput-object p2, Lcom/peel/social/a/c;->b:Lcom/peel/social/a/g;

    .line 38
    new-instance v0, Lcom/peel/social/a/e;

    invoke-direct {v0, p0}, Lcom/peel/social/a/e;-><init>(Lcom/peel/social/a/c;)V

    iput-object v0, p0, Lcom/peel/social/a/c;->d:Lcom/peel/social/a/e;

    .line 39
    new-instance v0, Lcom/facebook/db;

    iget-object v1, p0, Lcom/peel/social/a/c;->d:Lcom/peel/social/a/e;

    invoke-direct {v0, p1, v1}, Lcom/facebook/db;-><init>(Landroid/app/Activity;Lcom/facebook/cq;)V

    iput-object v0, p0, Lcom/peel/social/a/c;->e:Lcom/facebook/db;

    .line 40
    return-void
.end method

.method private a(Lcom/facebook/ca;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 394
    sget-object v1, Lcom/peel/social/a/c;->a:Landroid/app/Activity;

    if-nez v1, :cond_1

    .line 395
    sget-object v1, Lcom/peel/social/a/c;->c:Ljava/lang/String;

    const-string/jumbo v2, "You must initialize the SimpleFacebook instance with you current Activity."

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 405
    :cond_0
    :goto_0
    return v0

    .line 399
    :cond_1
    sget-object v1, Lcom/facebook/cv;->b:Lcom/facebook/cv;

    invoke-virtual {p1}, Lcom/facebook/ca;->c()Lcom/facebook/cv;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/cv;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 400
    invoke-virtual {p0}, Lcom/peel/social/a/c;->b()Ljava/util/List;

    move-result-object v1

    .line 401
    sget-object v2, Lcom/peel/social/a/c;->b:Lcom/peel/social/a/g;

    invoke-virtual {v2}, Lcom/peel/social/a/g;->b()Ljava/util/List;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->containsAll(Ljava/util/Collection;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 402
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private b(Lcom/facebook/ca;)Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 410
    :try_start_0
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-string/jumbo v2, "pendingAuthorizationRequest"

    invoke-virtual {v0, v2}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v0

    .line 411
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    .line 412
    invoke-virtual {v0, p1}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/cg;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 413
    if-eqz v0, :cond_0

    move v0, v1

    .line 420
    :goto_0
    return v0

    .line 417
    :catch_0
    move-exception v0

    .line 420
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private f()Lcom/facebook/ca;
    .locals 2

    .prologue
    .line 375
    sget-object v0, Lcom/peel/social/a/c;->a:Landroid/app/Activity;

    if-nez v0, :cond_0

    .line 376
    sget-object v0, Lcom/peel/social/a/c;->c:Ljava/lang/String;

    const-string/jumbo v1, "You must initialize the SimpleFacebook instance with you current Activity."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 378
    const/4 v0, 0x0

    .line 385
    :goto_0
    return-object v0

    .line 381
    :cond_0
    invoke-virtual {p0}, Lcom/peel/social/a/c;->a()Lcom/facebook/ca;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/peel/social/a/c;->a()Lcom/facebook/ca;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ca;->b()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 382
    :cond_1
    new-instance v0, Lcom/facebook/cl;

    sget-object v1, Lcom/peel/social/a/c;->a:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/facebook/cl;-><init>(Landroid/content/Context;)V

    sget-object v1, Lcom/peel/social/a/c;->b:Lcom/peel/social/a/g;

    invoke-virtual {v1}, Lcom/peel/social/a/g;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/cl;->a(Ljava/lang/String;)Lcom/facebook/cl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/cl;->a()Lcom/facebook/ca;

    move-result-object v0

    .line 383
    invoke-static {v0}, Lcom/facebook/ca;->a(Lcom/facebook/ca;)V

    .line 385
    :cond_2
    invoke-virtual {p0}, Lcom/peel/social/a/c;->a()Lcom/facebook/ca;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public a()Lcom/facebook/ca;
    .locals 1

    .prologue
    .line 144
    invoke-static {}, Lcom/facebook/ca;->j()Lcom/facebook/ca;

    move-result-object v0

    return-object v0
.end method

.method public a(Lcom/facebook/ca;Z)V
    .locals 3

    .prologue
    .line 220
    new-instance v0, Lcom/facebook/cn;

    sget-object v1, Lcom/peel/social/a/c;->a:Landroid/app/Activity;

    invoke-direct {v0, v1}, Lcom/facebook/cn;-><init>(Landroid/app/Activity;)V

    .line 221
    if-eqz v0, :cond_1

    .line 222
    sget-object v1, Lcom/peel/social/a/c;->b:Lcom/peel/social/a/g;

    invoke-virtual {v1}, Lcom/peel/social/a/g;->f()Lcom/facebook/ct;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/cn;->b(Lcom/facebook/ct;)Lcom/facebook/cn;

    .line 223
    sget-object v1, Lcom/peel/social/a/c;->b:Lcom/peel/social/a/g;

    invoke-virtual {v1}, Lcom/peel/social/a/g;->e()Lcom/facebook/cu;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/cn;->b(Lcom/facebook/cu;)Lcom/facebook/cn;

    .line 225
    if-eqz p2, :cond_2

    .line 226
    sget-object v1, Lcom/peel/social/a/c;->b:Lcom/peel/social/a/g;

    invoke-virtual {v1}, Lcom/peel/social/a/g;->b()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/cn;->b(Ljava/util/List;)Lcom/facebook/cn;

    .line 233
    sget-object v1, Lcom/peel/social/a/c;->b:Lcom/peel/social/a/g;

    invoke-virtual {v1}, Lcom/peel/social/a/g;->d()Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v1, Lcom/peel/social/a/c;->b:Lcom/peel/social/a/g;

    invoke-virtual {v1}, Lcom/peel/social/a/g;->g()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 234
    iget-object v1, p0, Lcom/peel/social/a/c;->d:Lcom/peel/social/a/e;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/peel/social/a/e;->a(Z)V

    .line 238
    :cond_0
    invoke-virtual {p1, v0}, Lcom/facebook/ca;->a(Lcom/facebook/cn;)V

    .line 245
    :cond_1
    :goto_0
    return-void

    .line 241
    :cond_2
    sget-object v1, Lcom/peel/social/a/c;->b:Lcom/peel/social/a/g;

    invoke-virtual {v1}, Lcom/peel/social/a/g;->c()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/cn;->b(Ljava/util/List;)Lcom/facebook/cn;

    .line 242
    invoke-virtual {p1, v0}, Lcom/facebook/ca;->b(Lcom/facebook/cn;)V

    goto :goto_0
.end method

.method public a(Lcom/peel/social/a/a/b;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 48
    if-nez p1, :cond_1

    .line 49
    sget-object v0, Lcom/peel/social/a/c;->c:Ljava/lang/String;

    const-string/jumbo v1, "OnLoginListener can\'t be null in -> \'login(OnLoginListener onLoginListener)\' method."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 79
    :cond_0
    :goto_0
    return-void

    .line 54
    :cond_1
    sget-object v0, Lcom/peel/social/a/c;->a:Landroid/app/Activity;

    if-nez v0, :cond_2

    .line 55
    const-string/jumbo v0, "You must initialize the SimpleFacebook instance with you current Activity."

    invoke-interface {p1, v0}, Lcom/peel/social/a/a/b;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 58
    :cond_2
    invoke-virtual {p0, v2}, Lcom/peel/social/a/c;->a(Z)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 61
    invoke-interface {p1}, Lcom/peel/social/a/a/b;->a()V

    goto :goto_0

    .line 64
    :cond_3
    invoke-direct {p0}, Lcom/peel/social/a/c;->f()Lcom/facebook/ca;

    move-result-object v0

    .line 65
    invoke-direct {p0, v0}, Lcom/peel/social/a/c;->b(Lcom/facebook/ca;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 71
    iget-object v1, p0, Lcom/peel/social/a/c;->d:Lcom/peel/social/a/e;

    iput-object p1, v1, Lcom/peel/social/a/e;->a:Lcom/peel/social/a/a/b;

    .line 72
    iget-object v1, p0, Lcom/peel/social/a/c;->d:Lcom/peel/social/a/e;

    invoke-virtual {v0, v1}, Lcom/facebook/ca;->a(Lcom/facebook/cq;)V

    .line 73
    invoke-virtual {v0}, Lcom/facebook/ca;->a()Z

    move-result v1

    if-nez v1, :cond_4

    .line 74
    invoke-virtual {p0, v0, v2}, Lcom/peel/social/a/c;->a(Lcom/facebook/ca;Z)V

    goto :goto_0

    .line 77
    :cond_4
    invoke-interface {p1}, Lcom/peel/social/a/a/b;->a()V

    goto :goto_0
.end method

.method public a(Landroid/app/Activity;IILandroid/content/Intent;)Z
    .locals 2

    .prologue
    .line 355
    iget-object v0, p0, Lcom/peel/social/a/c;->e:Lcom/facebook/db;

    iget-object v1, p0, Lcom/peel/social/a/c;->f:Lcom/facebook/widget/b;

    invoke-virtual {v0, p2, p3, p4, v1}, Lcom/facebook/db;->a(IILandroid/content/Intent;Lcom/facebook/widget/b;)V

    .line 356
    const/4 v0, 0x1

    return v0
.end method

.method public a(Z)Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 115
    invoke-virtual {p0}, Lcom/peel/social/a/c;->a()Lcom/facebook/ca;

    move-result-object v0

    .line 116
    if-nez v0, :cond_1

    .line 117
    sget-object v0, Lcom/peel/social/a/c;->a:Landroid/app/Activity;

    if-nez v0, :cond_0

    move v0, v1

    .line 131
    :goto_0
    return v0

    .line 120
    :cond_0
    new-instance v0, Lcom/facebook/cl;

    sget-object v3, Lcom/peel/social/a/c;->a:Landroid/app/Activity;

    invoke-virtual {v3}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v0, v3}, Lcom/facebook/cl;-><init>(Landroid/content/Context;)V

    sget-object v3, Lcom/peel/social/a/c;->b:Lcom/peel/social/a/g;

    invoke-virtual {v3}, Lcom/peel/social/a/g;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/facebook/cl;->a(Ljava/lang/String;)Lcom/facebook/cl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/cl;->a()Lcom/facebook/ca;

    move-result-object v0

    .line 121
    invoke-static {v0}, Lcom/facebook/ca;->a(Lcom/facebook/ca;)V

    .line 123
    :cond_1
    invoke-virtual {v0}, Lcom/facebook/ca;->a()Z

    move-result v3

    if-eqz v3, :cond_2

    move v0, v2

    .line 124
    goto :goto_0

    .line 126
    :cond_2
    if-eqz p1, :cond_3

    invoke-direct {p0, v0}, Lcom/peel/social/a/c;->a(Lcom/facebook/ca;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 127
    invoke-virtual {p0}, Lcom/peel/social/a/c;->e()V

    move v0, v2

    .line 128
    goto :goto_0

    :cond_3
    move v0, v1

    .line 131
    goto :goto_0
.end method

.method public b()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 171
    invoke-virtual {p0}, Lcom/peel/social/a/c;->a()Lcom/facebook/ca;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ca;->g()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public c()Z
    .locals 2

    .prologue
    .line 197
    invoke-virtual {p0}, Lcom/peel/social/a/c;->b()Ljava/util/List;

    move-result-object v0

    sget-object v1, Lcom/peel/social/a/c;->b:Lcom/peel/social/a/g;

    invoke-virtual {v1}, Lcom/peel/social/a/g;->c()Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->containsAll(Ljava/util/Collection;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 198
    const/4 v0, 0x1

    .line 200
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public d()V
    .locals 4

    .prologue
    .line 209
    invoke-static {}, Lcom/facebook/ca;->j()Lcom/facebook/ca;

    move-result-object v0

    .line 210
    invoke-direct {p0, v0}, Lcom/peel/social/a/c;->b(Lcom/facebook/ca;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 214
    :cond_0
    new-instance v1, Lcom/facebook/cm;

    sget-object v2, Lcom/peel/social/a/c;->a:Landroid/app/Activity;

    sget-object v3, Lcom/peel/social/a/c;->b:Lcom/peel/social/a/g;

    invoke-virtual {v3}, Lcom/peel/social/a/g;->c()Ljava/util/List;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/facebook/cm;-><init>(Landroid/app/Activity;Ljava/util/List;)V

    .line 215
    iget-object v2, p0, Lcom/peel/social/a/c;->d:Lcom/peel/social/a/e;

    invoke-virtual {v0, v2}, Lcom/facebook/ca;->a(Lcom/facebook/cq;)V

    .line 216
    invoke-virtual {v0, v1}, Lcom/facebook/ca;->a(Lcom/facebook/cm;)V

    .line 217
    return-void
.end method

.method public e()V
    .locals 4

    .prologue
    .line 332
    invoke-static {}, Lcom/facebook/ca;->j()Lcom/facebook/ca;

    move-result-object v0

    .line 333
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/ca;->c()Lcom/facebook/cv;

    move-result-object v1

    sget-object v2, Lcom/facebook/cv;->b:Lcom/facebook/cv;

    invoke-virtual {v1, v2}, Lcom/facebook/cv;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 334
    invoke-virtual {v0}, Lcom/facebook/ca;->g()Ljava/util/List;

    move-result-object v1

    .line 335
    sget-object v2, Lcom/peel/social/a/c;->b:Lcom/peel/social/a/g;

    invoke-virtual {v2}, Lcom/peel/social/a/g;->c()Ljava/util/List;

    move-result-object v2

    .line 336
    if-eqz v2, :cond_1

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    if-lez v3, :cond_1

    invoke-interface {v1, v2}, Ljava/util/List;->containsAll(Ljava/util/Collection;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 337
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/peel/social/a/c;->a(Lcom/facebook/ca;Z)V

    .line 343
    :cond_0
    :goto_0
    return-void

    .line 339
    :cond_1
    sget-object v2, Lcom/peel/social/a/c;->b:Lcom/peel/social/a/g;

    invoke-virtual {v2}, Lcom/peel/social/a/g;->b()Ljava/util/List;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->containsAll(Ljava/util/Collection;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 340
    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/peel/social/a/c;->a(Lcom/facebook/ca;Z)V

    goto :goto_0
.end method
