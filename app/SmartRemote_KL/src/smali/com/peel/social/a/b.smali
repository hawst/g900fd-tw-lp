.class public final enum Lcom/peel/social/a/b;
.super Ljava/lang/Enum;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/peel/social/a/b;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/peel/social/a/b;

.field public static final enum b:Lcom/peel/social/a/b;

.field private static final synthetic d:[Lcom/peel/social/a/b;


# instance fields
.field private c:Lcom/facebook/b/bj;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 55
    new-instance v0, Lcom/peel/social/a/b;

    const-string/jumbo v1, "PUBLISH"

    sget-object v2, Lcom/facebook/b/bj;->b:Lcom/facebook/b/bj;

    invoke-direct {v0, v1, v3, v2}, Lcom/peel/social/a/b;-><init>(Ljava/lang/String;ILcom/facebook/b/bj;)V

    sput-object v0, Lcom/peel/social/a/b;->a:Lcom/peel/social/a/b;

    .line 56
    new-instance v0, Lcom/peel/social/a/b;

    const-string/jumbo v1, "READ"

    sget-object v2, Lcom/facebook/b/bj;->a:Lcom/facebook/b/bj;

    invoke-direct {v0, v1, v4, v2}, Lcom/peel/social/a/b;-><init>(Ljava/lang/String;ILcom/facebook/b/bj;)V

    sput-object v0, Lcom/peel/social/a/b;->b:Lcom/peel/social/a/b;

    .line 54
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/peel/social/a/b;

    sget-object v1, Lcom/peel/social/a/b;->a:Lcom/peel/social/a/b;

    aput-object v1, v0, v3

    sget-object v1, Lcom/peel/social/a/b;->b:Lcom/peel/social/a/b;

    aput-object v1, v0, v4

    sput-object v0, Lcom/peel/social/a/b;->d:[Lcom/peel/social/a/b;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILcom/facebook/b/bj;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/b/bj;",
            ")V"
        }
    .end annotation

    .prologue
    .line 60
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 61
    iput-object p3, p0, Lcom/peel/social/a/b;->c:Lcom/facebook/b/bj;

    .line 62
    return-void
.end method

.method static synthetic a(Lcom/peel/social/a/b;)Lcom/facebook/b/bj;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/peel/social/a/b;->c:Lcom/facebook/b/bj;

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/peel/social/a/b;
    .locals 1

    .prologue
    .line 54
    const-class v0, Lcom/peel/social/a/b;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/peel/social/a/b;

    return-object v0
.end method

.method public static values()[Lcom/peel/social/a/b;
    .locals 1

    .prologue
    .line 54
    sget-object v0, Lcom/peel/social/a/b;->d:[Lcom/peel/social/a/b;

    invoke-virtual {v0}, [Lcom/peel/social/a/b;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/peel/social/a/b;

    return-object v0
.end method
