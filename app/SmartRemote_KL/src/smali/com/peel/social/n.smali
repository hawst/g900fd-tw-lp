.class Lcom/peel/social/n;
.super Lcom/peel/util/t;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/peel/util/t",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/peel/social/m;


# direct methods
.method constructor <init>(Lcom/peel/social/m;)V
    .locals 0

    .prologue
    .line 125
    iput-object p1, p0, Lcom/peel/social/n;->a:Lcom/peel/social/m;

    invoke-direct {p0}, Lcom/peel/util/t;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    .line 128
    iget-boolean v0, p0, Lcom/peel/social/n;->i:Z

    if-nez v0, :cond_1

    .line 129
    sget-object v0, Lcom/peel/social/e;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "SocialLogin: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/peel/social/n;->k:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 130
    iget-object v0, p0, Lcom/peel/social/n;->a:Lcom/peel/social/m;

    iget-object v0, v0, Lcom/peel/social/m;->b:Landroid/content/Context;

    iget-object v1, p0, Lcom/peel/social/n;->a:Lcom/peel/social/m;

    iget-object v1, v1, Lcom/peel/social/m;->a:Ljava/lang/String;

    iget-object v2, p0, Lcom/peel/social/n;->a:Lcom/peel/social/m;

    iget-object v2, v2, Lcom/peel/social/m;->c:Lcom/peel/util/t;

    invoke-static {v0, v1, v2}, Lcom/peel/social/e;->e(Landroid/content/Context;Ljava/lang/String;Lcom/peel/util/t;)V

    .line 136
    :cond_0
    :goto_0
    return-void

    .line 131
    :cond_1
    iget-boolean v0, p0, Lcom/peel/social/n;->i:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/peel/social/n;->j:Ljava/lang/Object;

    if-eqz v0, :cond_0

    .line 132
    iget-object v0, p0, Lcom/peel/social/n;->a:Lcom/peel/social/m;

    iget-object v1, v0, Lcom/peel/social/m;->b:Landroid/content/Context;

    iget-object v0, p0, Lcom/peel/social/n;->j:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    iget-object v2, p0, Lcom/peel/social/n;->a:Lcom/peel/social/m;

    iget-object v2, v2, Lcom/peel/social/m;->c:Lcom/peel/util/t;

    iget-object v3, p0, Lcom/peel/social/n;->a:Lcom/peel/social/m;

    iget-object v3, v3, Lcom/peel/social/m;->b:Landroid/content/Context;

    sget v4, Lcom/peel/ui/ft;->logged_in_with:I

    .line 133
    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/peel/social/n;->a:Lcom/peel/social/m;

    iget-object v6, v6, Lcom/peel/social/m;->b:Landroid/content/Context;

    sget v7, Lcom/peel/ui/ft;->google_plus_label:I

    .line 134
    invoke-virtual {v6, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    .line 133
    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 132
    invoke-static {v1, v0, v2, v3}, Lcom/peel/social/e;->a(Landroid/content/Context;Ljava/lang/String;Lcom/peel/util/t;Ljava/lang/String;)V

    goto :goto_0
.end method
