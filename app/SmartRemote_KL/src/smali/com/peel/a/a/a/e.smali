.class public Lcom/peel/a/a/a/e;
.super Ljava/lang/Object;


# static fields
.field private static a:I


# instance fields
.field private b:Ljava/util/Stack;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Stack",
            "<",
            "Lcom/peel/a/a/a/b/b;",
            ">;"
        }
    .end annotation
.end field

.field private c:Lcom/peel/a/a/a/f;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 14
    const/4 v0, 0x0

    sput v0, Lcom/peel/a/a/a/e;->a:I

    return-void
.end method

.method public constructor <init>(Lcom/peel/a/a/a/f;)V
    .locals 1

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    new-instance v0, Ljava/util/Stack;

    invoke-direct {v0}, Ljava/util/Stack;-><init>()V

    iput-object v0, p0, Lcom/peel/a/a/a/e;->b:Ljava/util/Stack;

    .line 25
    iput-object p1, p0, Lcom/peel/a/a/a/e;->c:Lcom/peel/a/a/a/f;

    .line 26
    return-void
.end method


# virtual methods
.method public a(Lcom/peel/a/a/a/b/b;)V
    .locals 2

    .prologue
    .line 29
    iget-object v0, p0, Lcom/peel/a/a/a/e;->b:Ljava/util/Stack;

    invoke-virtual {v0, p1}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    .line 30
    sget v0, Lcom/peel/a/a/a/e;->a:I

    invoke-virtual {p1}, Lcom/peel/a/a/a/b/b;->a()I

    move-result v1

    add-int/2addr v0, v1

    sput v0, Lcom/peel/a/a/a/e;->a:I

    .line 31
    iget-object v0, p0, Lcom/peel/a/a/a/e;->b:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->size()I

    move-result v0

    const/16 v1, 0x19

    if-gt v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/peel/a/a/a/e;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 32
    :cond_0
    iget-object v0, p0, Lcom/peel/a/a/a/e;->c:Lcom/peel/a/a/a/f;

    invoke-interface {v0}, Lcom/peel/a/a/a/f;->a()V

    .line 34
    :cond_1
    return-void
.end method

.method public a()Z
    .locals 2

    .prologue
    .line 47
    sget v0, Lcom/peel/a/a/a/e;->a:I

    const/16 v1, 0x3200

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()[Lcom/peel/a/a/a/b/b;
    .locals 2

    .prologue
    .line 51
    iget-object v0, p0, Lcom/peel/a/a/a/e;->b:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->size()I

    move-result v0

    new-array v0, v0, [Lcom/peel/a/a/a/b/b;

    .line 52
    iget-object v1, p0, Lcom/peel/a/a/a/e;->b:Ljava/util/Stack;

    invoke-virtual {v1, v0}, Ljava/util/Stack;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 53
    iget-object v1, p0, Lcom/peel/a/a/a/e;->b:Ljava/util/Stack;

    invoke-virtual {v1}, Ljava/util/Stack;->clear()V

    .line 54
    const/4 v1, 0x0

    sput v1, Lcom/peel/a/a/a/e;->a:I

    .line 55
    return-object v0
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/peel/a/a/a/e;->b:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->size()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
