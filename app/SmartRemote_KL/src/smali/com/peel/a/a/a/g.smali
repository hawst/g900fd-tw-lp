.class public Lcom/peel/a/a/a/g;
.super Ljava/lang/Object;


# static fields
.field private static a:Lcom/peel/a/a/a/g;

.field private static b:Lcom/peel/a/a/a/a;

.field private static c:Ljava/lang/String;

.field private static d:Lcom/peel/a/a/a/a/a;

.field private static e:Ljava/util/logging/Logger;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 17
    const/4 v0, 0x0

    sput-object v0, Lcom/peel/a/a/a/g;->a:Lcom/peel/a/a/a/g;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getPackage()Ljava/lang/Package;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Package;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    move-result-object v0

    sput-object v0, Lcom/peel/a/a/a/g;->e:Ljava/util/logging/Logger;

    .line 47
    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/peel/a/a/a/a/a;)V
    .locals 2

    .prologue
    .line 32
    invoke-static {p0}, Lcom/peel/a/a/a/c/d;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/peel/a/a/a/g;->c:Ljava/lang/String;

    .line 33
    if-nez p5, :cond_0

    new-instance p5, Lcom/peel/a/a/a/a/a;

    invoke-direct {p5}, Lcom/peel/a/a/a/a/a;-><init>()V

    :cond_0
    sput-object p5, Lcom/peel/a/a/a/g;->d:Lcom/peel/a/a/a/a/a;

    .line 34
    sget-object v0, Lcom/peel/a/a/a/g;->d:Lcom/peel/a/a/a/a/a;

    invoke-virtual {v0, p1}, Lcom/peel/a/a/a/a/a;->a(Ljava/lang/String;)V

    .line 35
    sget-object v0, Lcom/peel/a/a/a/g;->d:Lcom/peel/a/a/a/a/a;

    invoke-virtual {v0, p2}, Lcom/peel/a/a/a/a/a;->b(Ljava/lang/String;)V

    .line 36
    sget-object v0, Lcom/peel/a/a/a/g;->d:Lcom/peel/a/a/a/a/a;

    invoke-virtual {v0, p3}, Lcom/peel/a/a/a/a/a;->d(Ljava/lang/String;)V

    .line 37
    sget-object v0, Lcom/peel/a/a/a/g;->d:Lcom/peel/a/a/a/a/a;

    invoke-virtual {v0, p4}, Lcom/peel/a/a/a/a/a;->c(Ljava/lang/String;)V

    .line 38
    sget-object v1, Lcom/peel/a/a/a/g;->d:Lcom/peel/a/a/a/a/a;

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v0

    iget v0, v0, Landroid/content/pm/ApplicationInfo;->flags:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Lcom/peel/a/a/a/a/a;->a(Z)V

    .line 40
    new-instance v0, Lcom/peel/a/a/a/a;

    sget-object v1, Lcom/peel/a/a/a/g;->d:Lcom/peel/a/a/a/a/a;

    invoke-direct {v0, v1}, Lcom/peel/a/a/a/a;-><init>(Lcom/peel/a/a/a/a/a;)V

    sput-object v0, Lcom/peel/a/a/a/g;->b:Lcom/peel/a/a/a/a;

    .line 43
    return-void

    .line 38
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b()Lcom/peel/a/a/a/g;
    .locals 1

    .prologue
    .line 58
    sget-object v0, Lcom/peel/a/a/a/g;->a:Lcom/peel/a/a/a/g;

    if-nez v0, :cond_0

    .line 59
    new-instance v0, Lcom/peel/a/a/a/g;

    invoke-direct {v0}, Lcom/peel/a/a/a/g;-><init>()V

    sput-object v0, Lcom/peel/a/a/a/g;->a:Lcom/peel/a/a/a/g;

    .line 61
    :cond_0
    sget-object v0, Lcom/peel/a/a/a/g;->a:Lcom/peel/a/a/a/g;

    return-object v0
.end method

.method public static c()V
    .locals 1

    .prologue
    .line 81
    sget-object v0, Lcom/peel/a/a/a/g;->b:Lcom/peel/a/a/a/a;

    invoke-virtual {v0}, Lcom/peel/a/a/a/a;->a()V

    .line 82
    return-void
.end method


# virtual methods
.method public a()Lcom/peel/a/a/a/a/a;
    .locals 1

    .prologue
    .line 54
    sget-object v0, Lcom/peel/a/a/a/g;->d:Lcom/peel/a/a/a/a/a;

    return-object v0
.end method

.method public a(Lcom/peel/a/a/a/b/b;)V
    .locals 1

    .prologue
    .line 65
    sget-object v0, Lcom/peel/a/a/a/g;->b:Lcom/peel/a/a/a/a;

    invoke-virtual {v0, p1}, Lcom/peel/a/a/a/a;->a(Lcom/peel/a/a/a/b/b;)V

    .line 66
    return-void
.end method
