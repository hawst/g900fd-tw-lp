.class public Lcom/peel/a/a/a/a;
.super Ljava/lang/Object;


# static fields
.field private static c:Lcom/peel/a/a/a/e;


# instance fields
.field private a:Ljava/util/concurrent/ScheduledExecutorService;

.field private b:Lcom/peel/a/a/a/a/a;

.field private d:Z

.field private e:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(Lcom/peel/a/a/a/a/a;)V
    .locals 2

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/peel/a/a/a/a;->d:Z

    .line 71
    new-instance v0, Lcom/peel/a/a/a/d;

    invoke-direct {v0, p0}, Lcom/peel/a/a/a/d;-><init>(Lcom/peel/a/a/a/a;)V

    iput-object v0, p0, Lcom/peel/a/a/a/a;->e:Ljava/lang/Runnable;

    .line 30
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/util/concurrent/Executors;->newScheduledThreadPool(I)Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/a/a/a/a;->a:Ljava/util/concurrent/ScheduledExecutorService;

    .line 31
    iput-object p1, p0, Lcom/peel/a/a/a/a;->b:Lcom/peel/a/a/a/a/a;

    .line 32
    new-instance v0, Lcom/peel/a/a/a/e;

    new-instance v1, Lcom/peel/a/a/a/b;

    invoke-direct {v1, p0}, Lcom/peel/a/a/a/b;-><init>(Lcom/peel/a/a/a/a;)V

    invoke-direct {v0, v1}, Lcom/peel/a/a/a/e;-><init>(Lcom/peel/a/a/a/f;)V

    sput-object v0, Lcom/peel/a/a/a/a;->c:Lcom/peel/a/a/a/e;

    .line 40
    return-void
.end method

.method static synthetic a(Lcom/peel/a/a/a/a;)V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Lcom/peel/a/a/a/a;->c()V

    return-void
.end method

.method static synthetic b()Lcom/peel/a/a/a/e;
    .locals 1

    .prologue
    .line 15
    sget-object v0, Lcom/peel/a/a/a/a;->c:Lcom/peel/a/a/a/e;

    return-object v0
.end method

.method static synthetic b(Lcom/peel/a/a/a/a;)Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 15
    iget-object v0, p0, Lcom/peel/a/a/a/a;->e:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic c(Lcom/peel/a/a/a/a;)Lcom/peel/a/a/a/a/a;
    .locals 1

    .prologue
    .line 15
    iget-object v0, p0, Lcom/peel/a/a/a/a;->b:Lcom/peel/a/a/a/a/a;

    return-object v0
.end method

.method private c()V
    .locals 4

    .prologue
    .line 90
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/peel/a/a/a/h;

    iget-object v2, p0, Lcom/peel/a/a/a/a;->b:Lcom/peel/a/a/a/a/a;

    sget-object v3, Lcom/peel/a/a/a/a;->c:Lcom/peel/a/a/a/e;

    invoke-virtual {v3}, Lcom/peel/a/a/a/e;->b()[Lcom/peel/a/a/a/b/b;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/peel/a/a/a/h;-><init>(Lcom/peel/a/a/a/a/a;[Lcom/peel/a/a/a/b/b;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 92
    return-void
.end method


# virtual methods
.method public a()V
    .locals 7

    .prologue
    .line 50
    iget-boolean v0, p0, Lcom/peel/a/a/a/a;->d:Z

    if-nez v0, :cond_0

    .line 51
    iget-object v0, p0, Lcom/peel/a/a/a/a;->a:Ljava/util/concurrent/ScheduledExecutorService;

    new-instance v1, Lcom/peel/a/a/a/c;

    invoke-direct {v1, p0}, Lcom/peel/a/a/a/c;-><init>(Lcom/peel/a/a/a/a;)V

    const-wide/16 v2, 0x3a98

    const-wide/16 v4, 0x7530

    sget-object v6, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface/range {v0 .. v6}, Ljava/util/concurrent/ScheduledExecutorService;->scheduleAtFixedRate(Ljava/lang/Runnable;JJLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    .line 66
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/peel/a/a/a/a;->d:Z

    .line 68
    return-void
.end method

.method public a(Lcom/peel/a/a/a/b/b;)V
    .locals 1

    .prologue
    .line 43
    sget-object v0, Lcom/peel/a/a/a/a;->c:Lcom/peel/a/a/a/e;

    invoke-virtual {v0, p1}, Lcom/peel/a/a/a/e;->a(Lcom/peel/a/a/a/b/b;)V

    .line 44
    return-void
.end method
