.class public Lcom/peel/a/a/a/a/a;
.super Ljava/lang/Object;


# static fields
.field private static l:Ljava/util/logging/Logger;

.field private static n:Z


# instance fields
.field private a:Ljava/lang/String;

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/String;

.field private g:Ljava/lang/String;

.field private h:Ljava/lang/String;

.field private i:Ljava/lang/String;

.field private j:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private k:Ljava/lang/String;

.field private m:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-object v0, p0, Lcom/peel/a/a/a/a/a;->c:Ljava/lang/String;

    .line 24
    iput-object v0, p0, Lcom/peel/a/a/a/a/a;->d:Ljava/lang/String;

    .line 25
    iput-object v0, p0, Lcom/peel/a/a/a/a/a;->e:Ljava/lang/String;

    .line 26
    iput-object v0, p0, Lcom/peel/a/a/a/a/a;->f:Ljava/lang/String;

    .line 27
    iput-object v0, p0, Lcom/peel/a/a/a/a/a;->g:Ljava/lang/String;

    .line 28
    iput-object v0, p0, Lcom/peel/a/a/a/a/a;->h:Ljava/lang/String;

    .line 29
    iput-object v0, p0, Lcom/peel/a/a/a/a/a;->i:Ljava/lang/String;

    .line 33
    iput-object v0, p0, Lcom/peel/a/a/a/a/a;->k:Ljava/lang/String;

    .line 45
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getPackage()Ljava/lang/Package;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Package;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    move-result-object v0

    sput-object v0, Lcom/peel/a/a/a/a/a;->l:Ljava/util/logging/Logger;

    .line 46
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/peel/a/a/a/a/a;->j:Ljava/util/HashMap;

    .line 47
    return-void
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/peel/a/a/a/a/a;->g:Ljava/lang/String;

    if-nez v0, :cond_0

    const-string/jumbo v0, "us-east-1"

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/peel/a/a/a/a/a;->g:Ljava/lang/String;

    goto :goto_0
.end method

.method public a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 124
    iput-object p1, p0, Lcom/peel/a/a/a/a/a;->a:Ljava/lang/String;

    .line 125
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Lcom/peel/a/a/a/a/a;->j:Ljava/util/HashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public a(Z)V
    .locals 0

    .prologue
    .line 147
    sput-boolean p1, Lcom/peel/a/a/a/a/a;->n:Z

    return-void
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/peel/a/a/a/a/a;->g:Ljava/lang/String;

    if-nez v0, :cond_0

    const-string/jumbo v0, "kinesis"

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/peel/a/a/a/a/a;->f:Ljava/lang/String;

    goto :goto_0
.end method

.method public b(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 128
    iput-object p1, p0, Lcom/peel/a/a/a/a/a;->b:Ljava/lang/String;

    .line 129
    return-void
.end method

.method public c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/peel/a/a/a/a/a;->g:Ljava/lang/String;

    if-nez v0, :cond_0

    const-string/jumbo v0, "application/x-amz-json-1.1"

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/peel/a/a/a/a/a;->h:Ljava/lang/String;

    goto :goto_0
.end method

.method public c(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 132
    iput-object p1, p0, Lcom/peel/a/a/a/a/a;->k:Ljava/lang/String;

    .line 133
    return-void
.end method

.method public d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/peel/a/a/a/a/a;->g:Ljava/lang/String;

    if-nez v0, :cond_0

    const-string/jumbo v0, "Kinesis_20131202.PutRecord"

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/peel/a/a/a/a/a;->d:Ljava/lang/String;

    goto :goto_0
.end method

.method public d(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 136
    iput-object p1, p0, Lcom/peel/a/a/a/a/a;->m:Ljava/lang/String;

    .line 137
    return-void
.end method

.method public e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/peel/a/a/a/a/a;->g:Ljava/lang/String;

    if-nez v0, :cond_0

    const-string/jumbo v0, "kinesis.us-east-1.amazonaws.com"

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/peel/a/a/a/a/a;->e:Ljava/lang/String;

    goto :goto_0
.end method

.method public f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lcom/peel/a/a/a/a/a;->g:Ljava/lang/String;

    if-nez v0, :cond_0

    const-string/jumbo v0, "https://kinesis.us-east-1.amazonaws.com"

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/peel/a/a/a/a/a;->c:Ljava/lang/String;

    goto :goto_0
.end method

.method public g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/peel/a/a/a/a/a;->i:Ljava/lang/String;

    if-nez v0, :cond_0

    const-string/jumbo v0, "events"

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/peel/a/a/a/a/a;->i:Ljava/lang/String;

    goto :goto_0
.end method

.method public h()Ljava/lang/String;
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lcom/peel/a/a/a/a/a;->a:Ljava/lang/String;

    return-object v0
.end method

.method public i()Ljava/lang/String;
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lcom/peel/a/a/a/a/a;->b:Ljava/lang/String;

    return-object v0
.end method

.method public j()Ljava/lang/String;
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lcom/peel/a/a/a/a/a;->k:Ljava/lang/String;

    return-object v0
.end method

.method public k()Ljava/lang/String;
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lcom/peel/a/a/a/a/a;->m:Ljava/lang/String;

    return-object v0
.end method

.method public l()Ljava/util/HashMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 89
    iget-object v0, p0, Lcom/peel/a/a/a/a/a;->j:Ljava/util/HashMap;

    return-object v0
.end method

.method public m()Z
    .locals 1

    .prologue
    .line 101
    sget-boolean v0, Lcom/peel/a/a/a/a/a;->n:Z

    return v0
.end method
