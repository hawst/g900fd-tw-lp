.class Lcom/peel/a/a/a/c;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/peel/a/a/a/a;

.field private final b:Ljava/util/concurrent/ExecutorService;

.field private c:Ljava/util/concurrent/Future;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/Future",
            "<*>;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/peel/a/a/a/a;)V
    .locals 1

    .prologue
    .line 51
    iput-object p1, p0, Lcom/peel/a/a/a/c;->a:Lcom/peel/a/a/a/a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    invoke-static {}, Ljava/util/concurrent/Executors;->newSingleThreadExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/a/a/a/c;->b:Ljava/util/concurrent/ExecutorService;

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 58
    iget-object v0, p0, Lcom/peel/a/a/a/c;->c:Ljava/util/concurrent/Future;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/peel/a/a/a/c;->c:Ljava/util/concurrent/Future;

    invoke-interface {v0}, Ljava/util/concurrent/Future;->isDone()Z

    move-result v0

    if-nez v0, :cond_0

    .line 62
    :goto_0
    return-void

    .line 61
    :cond_0
    iget-object v0, p0, Lcom/peel/a/a/a/c;->b:Ljava/util/concurrent/ExecutorService;

    iget-object v1, p0, Lcom/peel/a/a/a/c;->a:Lcom/peel/a/a/a/a;

    invoke-static {v1}, Lcom/peel/a/a/a/a;->b(Lcom/peel/a/a/a/a;)Ljava/lang/Runnable;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/a/a/a/c;->c:Ljava/util/concurrent/Future;

    goto :goto_0
.end method
