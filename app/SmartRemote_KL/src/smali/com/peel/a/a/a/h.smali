.class public Lcom/peel/a/a/a/h;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private a:Lcom/peel/a/a/a/a/a;

.field private b:[Lcom/peel/a/a/a/b/b;


# direct methods
.method public varargs constructor <init>(Lcom/peel/a/a/a/a/a;[Lcom/peel/a/a/a/b/b;)V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    iput-object p1, p0, Lcom/peel/a/a/a/h;->a:Lcom/peel/a/a/a/a/a;

    .line 40
    iput-object p2, p0, Lcom/peel/a/a/a/h;->b:[Lcom/peel/a/a/a/b/b;

    .line 41
    return-void
.end method

.method static synthetic a(Lcom/peel/a/a/a/h;)Lcom/peel/a/a/a/a/a;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/peel/a/a/a/h;->a:Lcom/peel/a/a/a/a/a;

    return-object v0
.end method

.method private a()Ljava/lang/String;
    .locals 9

    .prologue
    .line 44
    new-instance v3, Lorg/json/JSONObject;

    invoke-direct {v3}, Lorg/json/JSONObject;-><init>()V

    .line 46
    :try_start_0
    new-instance v2, Lorg/json/JSONArray;

    invoke-direct {v2}, Lorg/json/JSONArray;-><init>()V

    .line 47
    iget-object v4, p0, Lcom/peel/a/a/a/h;->b:[Lcom/peel/a/a/a/b/b;

    array-length v5, v4

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v5, :cond_0

    aget-object v6, v4, v1

    .line 48
    new-instance v7, Lorg/json/JSONObject;

    invoke-direct {v7}, Lorg/json/JSONObject;-><init>()V

    .line 49
    const-string/jumbo v8, "data"

    iget-object v6, v6, Lcom/peel/a/a/a/b/b;->a:Ljava/lang/String;

    invoke-virtual {v7, v8, v6}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 51
    invoke-virtual {v2, v7}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    .line 47
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 53
    :cond_0
    iget-object v1, p0, Lcom/peel/a/a/a/h;->a:Lcom/peel/a/a/a/a/a;

    invoke-virtual {v1}, Lcom/peel/a/a/a/a/a;->g()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 54
    iget-object v1, p0, Lcom/peel/a/a/a/h;->a:Lcom/peel/a/a/a/a/a;

    invoke-virtual {v1}, Lcom/peel/a/a/a/a/a;->l()Ljava/util/HashMap;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v0, v1

    check-cast v0, Ljava/util/Map$Entry;

    move-object v2, v0

    .line 56
    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v3, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 58
    :catch_0
    move-exception v1

    .line 59
    invoke-virtual {v1}, Lorg/json/JSONException;->printStackTrace()V

    .line 62
    :cond_1
    new-instance v1, Ljava/lang/String;

    invoke-virtual {v3}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->getBytes()[B

    move-result-object v2

    invoke-static {v2}, Lorg/apache/a/a/a/a;->a([B)[B

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/String;-><init>([B)V

    .line 63
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "{\"StreamName\":\""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/peel/a/a/a/h;->a:Lcom/peel/a/a/a/a/a;

    invoke-virtual {v3}, Lcom/peel/a/a/a/a/a;->k()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "\",\"Data\":\""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "\",\"PartitionKey\":\""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/peel/a/a/a/h;->a:Lcom/peel/a/a/a/a/a;

    invoke-virtual {v2}, Lcom/peel/a/a/a/a/a;->j()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "\"}"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method


# virtual methods
.method public run()V
    .locals 10

    .prologue
    .line 69
    new-instance v3, Ljava/util/Date;

    invoke-direct {v3}, Ljava/util/Date;-><init>()V

    .line 71
    new-instance v8, Lorg/apache/http/impl/client/DefaultHttpClient;

    invoke-direct {v8}, Lorg/apache/http/impl/client/DefaultHttpClient;-><init>()V

    .line 73
    new-instance v9, Lorg/apache/http/client/methods/HttpPost;

    iget-object v0, p0, Lcom/peel/a/a/a/h;->a:Lcom/peel/a/a/a/a/a;

    invoke-virtual {v0}, Lcom/peel/a/a/a/a/a;->f()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v9, v0}, Lorg/apache/http/client/methods/HttpPost;-><init>(Ljava/lang/String;)V

    .line 75
    invoke-direct {p0}, Lcom/peel/a/a/a/h;->a()Ljava/lang/String;

    move-result-object v1

    .line 77
    new-instance v2, Lcom/peel/a/a/a/b/a;

    invoke-direct {v2}, Lcom/peel/a/a/a/b/a;-><init>()V

    .line 78
    sget-object v0, Lcom/peel/a/a/a/b/a;->a:Ljava/lang/String;

    invoke-virtual {v2, v0}, Lcom/peel/a/a/a/b/a;->a(Ljava/lang/String;)V

    .line 79
    const-string/jumbo v0, "Host"

    iget-object v4, p0, Lcom/peel/a/a/a/h;->a:Lcom/peel/a/a/a/a/a;

    invoke-virtual {v4}, Lcom/peel/a/a/a/a/a;->e()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v0, v4}, Lcom/peel/a/a/a/b/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 80
    const-string/jumbo v0, "X-Amz-Date"

    invoke-static {v3}, Lcom/peel/a/a/a/c/a;->a(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v0, v4}, Lcom/peel/a/a/a/b/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 81
    const-string/jumbo v0, "X-Amz-Target"

    iget-object v4, p0, Lcom/peel/a/a/a/h;->a:Lcom/peel/a/a/a/a/a;

    invoke-virtual {v4}, Lcom/peel/a/a/a/a/a;->d()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v0, v4}, Lcom/peel/a/a/a/b/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 82
    const-string/jumbo v0, "Content-Type"

    iget-object v4, p0, Lcom/peel/a/a/a/h;->a:Lcom/peel/a/a/a/a/a;

    invoke-virtual {v4}, Lcom/peel/a/a/a/a/a;->c()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v0, v4}, Lcom/peel/a/a/a/b/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 84
    :try_start_0
    const-string/jumbo v0, "Content-Length"

    const-string/jumbo v4, "UTF-8"

    invoke-virtual {v1, v4}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v4

    array-length v4, v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v0, v4}, Lcom/peel/a/a/a/b/a;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    .line 89
    :goto_0
    const/4 v7, 0x0

    .line 91
    :try_start_1
    invoke-virtual {v2}, Lcom/peel/a/a/a/b/a;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v4, p0, Lcom/peel/a/a/a/h;->a:Lcom/peel/a/a/a/a/a;

    invoke-virtual {v4}, Lcom/peel/a/a/a/a/a;->a()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/peel/a/a/a/h;->a:Lcom/peel/a/a/a/a/a;

    invoke-virtual {v5}, Lcom/peel/a/a/a/a/a;->b()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/peel/a/a/a/h;->a:Lcom/peel/a/a/a/a/a;

    invoke-virtual {v6}, Lcom/peel/a/a/a/a/a;->i()Ljava/lang/String;

    move-result-object v6

    invoke-static/range {v0 .. v6}, Lcom/peel/a/a/a/c/a;->a(Ljava/lang/String;Ljava/lang/String;Lcom/peel/a/a/a/b/a;Ljava/util/Date;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v0

    .line 96
    :goto_1
    const-string/jumbo v4, "Authorization"

    iget-object v5, p0, Lcom/peel/a/a/a/h;->a:Lcom/peel/a/a/a/a/a;

    invoke-static {v5, v3, v2, v0}, Lcom/peel/a/a/a/c/a;->a(Lcom/peel/a/a/a/a/a;Ljava/util/Date;Lcom/peel/a/a/a/b/a;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v9, v4, v0}, Lorg/apache/http/client/methods/HttpPost;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 97
    const-string/jumbo v0, "X-Amz-Date"

    invoke-static {v3}, Lcom/peel/a/a/a/c/a;->a(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v9, v0, v2}, Lorg/apache/http/client/methods/HttpPost;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 98
    const-string/jumbo v0, "X-Amz-Target"

    iget-object v2, p0, Lcom/peel/a/a/a/h;->a:Lcom/peel/a/a/a/a/a;

    invoke-virtual {v2}, Lcom/peel/a/a/a/a/a;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v9, v0, v2}, Lorg/apache/http/client/methods/HttpPost;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 99
    const-string/jumbo v0, "Content-Type"

    iget-object v2, p0, Lcom/peel/a/a/a/h;->a:Lcom/peel/a/a/a/a/a;

    invoke-virtual {v2}, Lcom/peel/a/a/a/a/a;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v9, v0, v2}, Lorg/apache/http/client/methods/HttpPost;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 100
    const-string/jumbo v0, "Host"

    iget-object v2, p0, Lcom/peel/a/a/a/h;->a:Lcom/peel/a/a/a/a/a;

    invoke-virtual {v2}, Lcom/peel/a/a/a/a/a;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v9, v0, v2}, Lorg/apache/http/client/methods/HttpPost;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 102
    :try_start_2
    new-instance v0, Lorg/apache/http/entity/StringEntity;

    const-string/jumbo v2, "UTF-8"

    invoke-direct {v0, v1, v2}, Lorg/apache/http/entity/StringEntity;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v9, v0}, Lorg/apache/http/client/methods/HttpPost;->setEntity(Lorg/apache/http/HttpEntity;)V
    :try_end_2
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_2 .. :try_end_2} :catch_2

    .line 107
    :goto_2
    new-instance v0, Lcom/peel/a/a/a/i;

    invoke-direct {v0, p0}, Lcom/peel/a/a/a/i;-><init>(Lcom/peel/a/a/a/h;)V

    .line 131
    :try_start_3
    monitor-enter p0
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_3

    .line 132
    :try_start_4
    invoke-interface {v8, v9, v0}, Lorg/apache/http/client/HttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;Lorg/apache/http/client/ResponseHandler;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 133
    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Kinesis: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 134
    monitor-exit p0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 138
    :goto_3
    return-void

    .line 85
    :catch_0
    move-exception v0

    .line 86
    invoke-virtual {v0}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    goto/16 :goto_0

    .line 92
    :catch_1
    move-exception v0

    .line 93
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    move-object v0, v7

    goto :goto_1

    .line 103
    :catch_2
    move-exception v0

    .line 104
    invoke-virtual {v0}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    goto :goto_2

    .line 134
    :catchall_0
    move-exception v0

    :try_start_5
    monitor-exit p0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :try_start_6
    throw v0
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_3

    .line 135
    :catch_3
    move-exception v0

    .line 136
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_3
.end method
