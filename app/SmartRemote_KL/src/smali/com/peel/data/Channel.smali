.class public Lcom/peel/data/Channel;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/os/Parcelable;
.implements Lcom/peel/data/j;
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable;",
        "Lcom/peel/data/j;",
        "Ljava/lang/Comparable",
        "<",
        "Lcom/peel/data/Channel;",
        ">;"
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/peel/data/Channel;",
            ">;"
        }
    .end annotation
.end field

.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:Ljava/lang/String;

.field private final c:Ljava/lang/String;

.field private final d:Ljava/lang/String;

.field private final e:Ljava/lang/String;

.field private final f:Ljava/lang/String;

.field private final g:I

.field private final h:Ljava/lang/String;

.field private final i:Ljava/lang/String;

.field private final j:Ljava/lang/String;

.field private final k:Ljava/lang/String;

.field private l:Ljava/lang/String;

.field private m:Ljava/lang/String;

.field private n:Z

.field private o:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 16
    new-instance v0, Lcom/peel/data/a;

    invoke-direct {v0}, Lcom/peel/data/a;-><init>()V

    sput-object v0, Lcom/peel/data/Channel;->CREATOR:Landroid/os/Parcelable$Creator;

    .line 25
    const-class v0, Lcom/peel/data/Channel;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/peel/data/Channel;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    iput-object p1, p0, Lcom/peel/data/Channel;->b:Ljava/lang/String;

    .line 44
    iput-object p2, p0, Lcom/peel/data/Channel;->d:Ljava/lang/String;

    .line 45
    iput-object p3, p0, Lcom/peel/data/Channel;->c:Ljava/lang/String;

    .line 46
    if-nez p4, :cond_0

    iput-object p2, p0, Lcom/peel/data/Channel;->h:Ljava/lang/String;

    .line 48
    :goto_0
    const-string/jumbo v0, "^[0]*"

    const-string/jumbo v1, ""

    invoke-virtual {p5, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/data/Channel;->e:Ljava/lang/String;

    .line 49
    iput-object p6, p0, Lcom/peel/data/Channel;->f:Ljava/lang/String;

    .line 50
    iput p7, p0, Lcom/peel/data/Channel;->g:I

    .line 51
    iput-object p8, p0, Lcom/peel/data/Channel;->i:Ljava/lang/String;

    .line 52
    iput-object p9, p0, Lcom/peel/data/Channel;->j:Ljava/lang/String;

    .line 53
    iput-object p10, p0, Lcom/peel/data/Channel;->l:Ljava/lang/String;

    .line 54
    iput-object p11, p0, Lcom/peel/data/Channel;->k:Ljava/lang/String;

    .line 55
    return-void

    .line 47
    :cond_0
    iput-object p4, p0, Lcom/peel/data/Channel;->h:Ljava/lang/String;

    goto :goto_0
.end method

.method static synthetic a(Landroid/os/Parcel;)Lcom/peel/data/Channel;
    .locals 1

    .prologue
    .line 12
    invoke-static {p0}, Lcom/peel/data/Channel;->b(Landroid/os/Parcel;)Lcom/peel/data/Channel;

    move-result-object v0

    return-object v0
.end method

.method private static b(Landroid/os/Parcel;)Lcom/peel/data/Channel;
    .locals 12

    .prologue
    .line 58
    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 59
    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 60
    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    .line 61
    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v4

    .line 62
    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 63
    const-string/jumbo v5, "^[0]*"

    const-string/jumbo v6, ""

    invoke-virtual {v0, v5, v6}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 64
    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v6

    .line 65
    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    move-result v7

    .line 66
    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v8

    .line 67
    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v9

    .line 68
    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v11

    .line 70
    const/4 v10, 0x0

    .line 71
    new-instance v0, Lcom/peel/data/Channel;

    invoke-direct/range {v0 .. v11}, Lcom/peel/data/Channel;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public a(Lcom/peel/data/Channel;)I
    .locals 9

    .prologue
    const/16 v8, 0x30

    const/4 v7, 0x5

    const/4 v6, 0x2

    const/4 v0, 0x0

    .line 122
    :try_start_0
    invoke-virtual {p0}, Lcom/peel/data/Channel;->m()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "\\."

    invoke-virtual {v1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 123
    invoke-virtual {p1}, Lcom/peel/data/Channel;->m()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v3, "\\."

    invoke-virtual {v1, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 124
    array-length v1, v2

    if-lez v1, :cond_1

    const/4 v1, 0x0

    aget-object v1, v2, v1

    :goto_0
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    .line 125
    array-length v1, v4

    if-lez v1, :cond_2

    const/4 v1, 0x0

    aget-object v1, v4, v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    move v3, v1

    .line 126
    :goto_1
    array-length v1, v2

    if-ne v1, v6, :cond_3

    const/4 v1, 0x1

    aget-object v1, v2, v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    move v2, v1

    .line 127
    :goto_2
    array-length v1, v4

    if-ne v1, v6, :cond_4

    const/4 v0, 0x1

    aget-object v0, v4, v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    move v1, v0

    .line 128
    :goto_3
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/Integer;->compareTo(Ljava/lang/Integer;)I

    move-result v0

    .line 129
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/Integer;->compareTo(Ljava/lang/Integer;)I

    move-result v1

    .line 130
    if-nez v0, :cond_0

    if-nez v1, :cond_5

    .line 133
    :cond_0
    :goto_4
    return v0

    .line 124
    :cond_1
    invoke-virtual {p0}, Lcom/peel/data/Channel;->m()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    goto :goto_0

    :cond_2
    move v3, v0

    .line 125
    goto :goto_1

    :cond_3
    move v2, v0

    .line 126
    goto :goto_2

    :cond_4
    move v1, v0

    .line 127
    goto :goto_3

    :cond_5
    move v0, v1

    .line 130
    goto :goto_4

    .line 131
    :catch_0
    move-exception v0

    .line 133
    invoke-virtual {p0}, Lcom/peel/data/Channel;->m()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v7, v8}, Lcom/peel/util/eg;->a(Ljava/lang/String;IC)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/peel/data/Channel;->m()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v7, v8}, Lcom/peel/util/eg;->a(Ljava/lang/String;IC)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    goto :goto_4
.end method

.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lcom/peel/data/Channel;->b:Ljava/lang/String;

    return-object v0
.end method

.method public a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 112
    iput-object p1, p0, Lcom/peel/data/Channel;->m:Ljava/lang/String;

    return-void
.end method

.method public a(Lorg/codehaus/jackson/JsonGenerator;)V
    .locals 3

    .prologue
    .line 139
    :try_start_0
    invoke-virtual {p1}, Lorg/codehaus/jackson/JsonGenerator;->writeStartObject()V

    .line 142
    const-string/jumbo v0, "object_type"

    const-class v1, Lcom/peel/data/Channel;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lorg/codehaus/jackson/JsonGenerator;->writeStringField(Ljava/lang/String;Ljava/lang/String;)V

    .line 144
    const-string/jumbo v0, "id"

    iget-object v1, p0, Lcom/peel/data/Channel;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lorg/codehaus/jackson/JsonGenerator;->writeStringField(Ljava/lang/String;Ljava/lang/String;)V

    .line 145
    const-string/jumbo v0, "prgsvcid"

    iget-object v1, p0, Lcom/peel/data/Channel;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lorg/codehaus/jackson/JsonGenerator;->writeStringField(Ljava/lang/String;Ljava/lang/String;)V

    .line 146
    const-string/jumbo v0, "callsign"

    iget-object v1, p0, Lcom/peel/data/Channel;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lorg/codehaus/jackson/JsonGenerator;->writeStringField(Ljava/lang/String;Ljava/lang/String;)V

    .line 147
    const-string/jumbo v0, "number"

    iget-object v1, p0, Lcom/peel/data/Channel;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lorg/codehaus/jackson/JsonGenerator;->writeStringField(Ljava/lang/String;Ljava/lang/String;)V

    .line 148
    const-string/jumbo v0, "image"

    iget-object v1, p0, Lcom/peel/data/Channel;->f:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lorg/codehaus/jackson/JsonGenerator;->writeStringField(Ljava/lang/String;Ljava/lang/String;)V

    .line 149
    const-string/jumbo v0, "type"

    iget v1, p0, Lcom/peel/data/Channel;->g:I

    invoke-virtual {p1, v0, v1}, Lorg/codehaus/jackson/JsonGenerator;->writeNumberField(Ljava/lang/String;I)V

    .line 150
    const-string/jumbo v0, "name"

    iget-object v1, p0, Lcom/peel/data/Channel;->h:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lorg/codehaus/jackson/JsonGenerator;->writeStringField(Ljava/lang/String;Ljava/lang/String;)V

    .line 151
    const-string/jumbo v0, "language"

    iget-object v1, p0, Lcom/peel/data/Channel;->i:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lorg/codehaus/jackson/JsonGenerator;->writeStringField(Ljava/lang/String;Ljava/lang/String;)V

    .line 152
    const-string/jumbo v0, "tier"

    iget-object v1, p0, Lcom/peel/data/Channel;->j:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lorg/codehaus/jackson/JsonGenerator;->writeStringField(Ljava/lang/String;Ljava/lang/String;)V

    .line 153
    const-string/jumbo v0, "libraryId"

    iget-object v1, p0, Lcom/peel/data/Channel;->m:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lorg/codehaus/jackson/JsonGenerator;->writeStringField(Ljava/lang/String;Ljava/lang/String;)V

    .line 154
    const-string/jumbo v0, "favorite"

    iget-boolean v1, p0, Lcom/peel/data/Channel;->n:Z

    invoke-virtual {p1, v0, v1}, Lorg/codehaus/jackson/JsonGenerator;->writeBooleanField(Ljava/lang/String;Z)V

    .line 155
    const-string/jumbo v0, "cut"

    iget-boolean v1, p0, Lcom/peel/data/Channel;->o:Z

    invoke-virtual {p1, v0, v1}, Lorg/codehaus/jackson/JsonGenerator;->writeBooleanField(Ljava/lang/String;Z)V

    .line 156
    const-string/jumbo v0, "source"

    iget-object v1, p0, Lcom/peel/data/Channel;->k:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lorg/codehaus/jackson/JsonGenerator;->writeStringField(Ljava/lang/String;Ljava/lang/String;)V

    .line 158
    invoke-virtual {p1}, Lorg/codehaus/jackson/JsonGenerator;->writeEndObject()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 162
    :goto_0
    return-void

    .line 159
    :catch_0
    move-exception v0

    .line 160
    sget-object v1, Lcom/peel/data/Channel;->a:Ljava/lang/String;

    sget-object v2, Lcom/peel/data/Channel;->a:Ljava/lang/String;

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public a(Z)V
    .locals 0

    .prologue
    .line 97
    iput-boolean p1, p0, Lcom/peel/data/Channel;->n:Z

    .line 100
    return-void
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lcom/peel/data/Channel;->d:Ljava/lang/String;

    return-object v0
.end method

.method public b(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 118
    iput-object p1, p0, Lcom/peel/data/Channel;->l:Ljava/lang/String;

    return-void
.end method

.method public b(Z)V
    .locals 0

    .prologue
    .line 105
    iput-boolean p1, p0, Lcom/peel/data/Channel;->o:Z

    .line 108
    return-void
.end method

.method public c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lcom/peel/data/Channel;->c:Ljava/lang/String;

    return-object v0
.end method

.method public synthetic compareTo(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 12
    check-cast p1, Lcom/peel/data/Channel;

    invoke-virtual {p0, p1}, Lcom/peel/data/Channel;->a(Lcom/peel/data/Channel;)I

    move-result v0

    return v0
.end method

.method public d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lcom/peel/data/Channel;->h:Ljava/lang/String;

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 165
    const/4 v0, 0x0

    return v0
.end method

.method public e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/peel/data/Channel;->e:Ljava/lang/String;

    return-object v0
.end method

.method public f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lcom/peel/data/Channel;->f:Ljava/lang/String;

    return-object v0
.end method

.method public g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lcom/peel/data/Channel;->i:Ljava/lang/String;

    return-object v0
.end method

.method public h()Ljava/lang/String;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/peel/data/Channel;->j:Ljava/lang/String;

    return-object v0
.end method

.method public i()I
    .locals 1

    .prologue
    .line 90
    iget v0, p0, Lcom/peel/data/Channel;->g:I

    return v0
.end method

.method public j()Ljava/lang/String;
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lcom/peel/data/Channel;->k:Ljava/lang/String;

    return-object v0
.end method

.method public k()Z
    .locals 1

    .prologue
    .line 94
    iget-boolean v0, p0, Lcom/peel/data/Channel;->n:Z

    return v0
.end method

.method public l()Z
    .locals 1

    .prologue
    .line 102
    iget-boolean v0, p0, Lcom/peel/data/Channel;->o:Z

    return v0
.end method

.method public m()Ljava/lang/String;
    .locals 2

    .prologue
    .line 115
    iget-object v0, p0, Lcom/peel/data/Channel;->l:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/peel/data/Channel;->l:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x1

    if-ge v0, v1, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/peel/data/Channel;->e:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/peel/data/Channel;->l:Ljava/lang/String;

    goto :goto_0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 168
    iget-object v0, p0, Lcom/peel/data/Channel;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 169
    iget-object v0, p0, Lcom/peel/data/Channel;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 170
    iget-object v0, p0, Lcom/peel/data/Channel;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 171
    iget-object v0, p0, Lcom/peel/data/Channel;->h:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 172
    iget-object v0, p0, Lcom/peel/data/Channel;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 173
    iget-object v0, p0, Lcom/peel/data/Channel;->f:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 174
    iget v0, p0, Lcom/peel/data/Channel;->g:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 175
    iget-object v0, p0, Lcom/peel/data/Channel;->i:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 176
    iget-object v0, p0, Lcom/peel/data/Channel;->j:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 177
    iget-object v0, p0, Lcom/peel/data/Channel;->k:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 178
    return-void
.end method
