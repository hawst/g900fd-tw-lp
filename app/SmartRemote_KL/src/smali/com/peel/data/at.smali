.class public Lcom/peel/data/at;
.super Ljava/lang/Object;


# instance fields
.field private final a:Ljava/lang/String;

.field private b:Ljava/lang/String;

.field private c:[Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:I

.field private g:Ljava/lang/String;

.field private h:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Lcom/peel/data/f;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-object p1, p0, Lcom/peel/data/at;->b:Ljava/lang/String;

    .line 24
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/data/at;->a:Ljava/lang/String;

    .line 25
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    iput-object p1, p0, Lcom/peel/data/at;->b:Ljava/lang/String;

    .line 30
    iput-object p2, p0, Lcom/peel/data/at;->a:Ljava/lang/String;

    .line 31
    iput p3, p0, Lcom/peel/data/at;->f:I

    .line 32
    return-void
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/peel/data/at;->b:Ljava/lang/String;

    return-object v0
.end method

.method public a(I)V
    .locals 1

    .prologue
    .line 66
    iput p1, p0, Lcom/peel/data/at;->f:I

    .line 67
    invoke-static {}, Lcom/peel/data/m;->a()Lcom/peel/data/m;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/peel/data/m;->c(Lcom/peel/data/at;)V

    .line 68
    return-void
.end method

.method public a(Landroid/util/SparseArray;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/SparseArray",
            "<",
            "Lcom/peel/data/f;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 79
    iput-object p1, p0, Lcom/peel/data/at;->h:Landroid/util/SparseArray;

    .line 80
    return-void
.end method

.method public a(Lcom/peel/data/d;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 176
    iget-object v0, p0, Lcom/peel/data/at;->c:[Ljava/lang/String;

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    add-int/lit8 v0, v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    .line 177
    iget-object v2, p0, Lcom/peel/data/at;->c:[Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 178
    iget-object v2, p0, Lcom/peel/data/at;->c:[Ljava/lang/String;

    iget-object v3, p0, Lcom/peel/data/at;->c:[Ljava/lang/String;

    array-length v3, v3

    invoke-static {v2, v1, v0, v1, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 180
    :cond_0
    array-length v1, v0

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {p1}, Lcom/peel/data/d;->b()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    .line 181
    iput-object v0, p0, Lcom/peel/data/at;->c:[Ljava/lang/String;

    .line 183
    invoke-static {}, Lcom/peel/data/m;->a()Lcom/peel/data/m;

    move-result-object v0

    invoke-virtual {v0, p1, p0}, Lcom/peel/data/m;->a(Lcom/peel/data/d;Lcom/peel/data/at;)V

    .line 184
    return-void

    .line 176
    :cond_1
    iget-object v0, p0, Lcom/peel/data/at;->c:[Ljava/lang/String;

    array-length v0, v0

    goto :goto_0
.end method

.method public a(Lcom/peel/data/h;)V
    .locals 1

    .prologue
    .line 51
    invoke-virtual {p1}, Lcom/peel/data/h;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/peel/data/at;->b(Ljava/lang/String;)V

    .line 52
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 36
    iput-object p1, p0, Lcom/peel/data/at;->b:Ljava/lang/String;

    return-void
.end method

.method public a([Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 43
    iput-object p1, p0, Lcom/peel/data/at;->c:[Ljava/lang/String;

    .line 44
    return-void
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/peel/data/at;->a:Ljava/lang/String;

    return-object v0
.end method

.method public b(Lcom/peel/data/d;)V
    .locals 5

    .prologue
    const/4 v2, -0x1

    const/4 v1, 0x0

    .line 187
    move v0, v1

    .line 188
    :goto_0
    iget-object v3, p0, Lcom/peel/data/at;->c:[Ljava/lang/String;

    array-length v3, v3

    if-ge v0, v3, :cond_2

    .line 189
    iget-object v3, p0, Lcom/peel/data/at;->c:[Ljava/lang/String;

    aget-object v3, v3, v0

    invoke-virtual {p1}, Lcom/peel/data/d;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 195
    :goto_1
    if-ne v2, v0, :cond_1

    .line 203
    :goto_2
    return-void

    .line 188
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 197
    :cond_1
    iget-object v2, p0, Lcom/peel/data/at;->c:[Ljava/lang/String;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    new-array v2, v2, [Ljava/lang/String;

    .line 198
    iget-object v3, p0, Lcom/peel/data/at;->c:[Ljava/lang/String;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 199
    iget-object v1, p0, Lcom/peel/data/at;->c:[Ljava/lang/String;

    add-int/lit8 v3, v0, 0x1

    iget-object v4, p0, Lcom/peel/data/at;->c:[Ljava/lang/String;

    array-length v4, v4

    sub-int/2addr v4, v0

    add-int/lit8 v4, v4, -0x1

    invoke-static {v1, v3, v2, v0, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 200
    iput-object v2, p0, Lcom/peel/data/at;->c:[Ljava/lang/String;

    .line 202
    invoke-static {}, Lcom/peel/data/m;->a()Lcom/peel/data/m;

    move-result-object v0

    invoke-virtual {v0, p1, p0}, Lcom/peel/data/m;->b(Lcom/peel/data/d;Lcom/peel/data/at;)V

    goto :goto_2

    :cond_2
    move v0, v2

    goto :goto_1
.end method

.method public b(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 48
    iput-object p1, p0, Lcom/peel/data/at;->d:Ljava/lang/String;

    return-void
.end method

.method public c(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 59
    iput-object p1, p0, Lcom/peel/data/at;->e:Ljava/lang/String;

    .line 60
    invoke-static {}, Lcom/peel/data/m;->a()Lcom/peel/data/m;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/peel/data/m;->c(Lcom/peel/data/at;)V

    .line 61
    return-void
.end method

.method public c()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/peel/data/at;->c:[Ljava/lang/String;

    return-object v0
.end method

.method public d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/peel/data/at;->d:Ljava/lang/String;

    return-object v0
.end method

.method public d(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 73
    iput-object p1, p0, Lcom/peel/data/at;->g:Ljava/lang/String;

    .line 74
    return-void
.end method

.method public e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/peel/data/at;->e:Ljava/lang/String;

    return-object v0
.end method

.method public f()I
    .locals 1

    .prologue
    .line 63
    iget v0, p0, Lcom/peel/data/at;->f:I

    return v0
.end method

.method public g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lcom/peel/data/at;->g:Ljava/lang/String;

    return-object v0
.end method
