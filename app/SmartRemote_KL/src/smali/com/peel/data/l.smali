.class public final Lcom/peel/data/l;
.super Ljava/lang/Object;


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Ljava/lang/String;

.field private final c:Ljava/lang/String;

.field private final d:Ljava/lang/String;

.field private final e:Ljava/lang/String;

.field private final f:[Ljava/lang/String;

.field private final g:I

.field private final h:J

.field private final i:Ljava/lang/String;

.field private final j:Ljava/lang/String;

.field private final k:Ljava/lang/String;

.field private final l:Ljava/lang/String;

.field private final m:Landroid/os/Bundle;

.field private final n:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;IJLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    iput-object p1, p0, Lcom/peel/data/l;->a:Ljava/lang/String;

    .line 30
    iput-object p2, p0, Lcom/peel/data/l;->b:Ljava/lang/String;

    .line 31
    iput-object p3, p0, Lcom/peel/data/l;->c:Ljava/lang/String;

    .line 32
    iput-object p4, p0, Lcom/peel/data/l;->d:Ljava/lang/String;

    .line 33
    iput-object p5, p0, Lcom/peel/data/l;->e:Ljava/lang/String;

    .line 34
    iput-object p6, p0, Lcom/peel/data/l;->f:[Ljava/lang/String;

    .line 35
    iput p7, p0, Lcom/peel/data/l;->g:I

    .line 36
    iput-wide p8, p0, Lcom/peel/data/l;->h:J

    .line 37
    iput-object p12, p0, Lcom/peel/data/l;->k:Ljava/lang/String;

    .line 38
    iput-object p10, p0, Lcom/peel/data/l;->i:Ljava/lang/String;

    .line 39
    iput-object p11, p0, Lcom/peel/data/l;->j:Ljava/lang/String;

    .line 40
    iput-object p13, p0, Lcom/peel/data/l;->l:Ljava/lang/String;

    .line 41
    iput-object p14, p0, Lcom/peel/data/l;->n:Ljava/lang/String;

    .line 42
    iput-object p15, p0, Lcom/peel/data/l;->m:Landroid/os/Bundle;

    .line 43
    return-void
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/peel/data/l;->a:Ljava/lang/String;

    return-object v0
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/peel/data/l;->b:Ljava/lang/String;

    return-object v0
.end method

.method public c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/peel/data/l;->d:Ljava/lang/String;

    return-object v0
.end method

.method public d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/peel/data/l;->e:Ljava/lang/String;

    return-object v0
.end method

.method public e()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/peel/data/l;->f:[Ljava/lang/String;

    return-object v0
.end method

.method public f()I
    .locals 1

    .prologue
    .line 55
    iget v0, p0, Lcom/peel/data/l;->g:I

    return v0
.end method

.method public g()J
    .locals 2

    .prologue
    .line 57
    iget-wide v0, p0, Lcom/peel/data/l;->h:J

    return-wide v0
.end method

.method public h()Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/peel/data/l;->m:Landroid/os/Bundle;

    return-object v0
.end method

.method public i()Ljava/lang/String;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/peel/data/l;->c:Ljava/lang/String;

    return-object v0
.end method

.method public j()Ljava/lang/String;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/peel/data/l;->i:Ljava/lang/String;

    return-object v0
.end method

.method public k()Ljava/lang/String;
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lcom/peel/data/l;->j:Ljava/lang/String;

    return-object v0
.end method

.method public l()Ljava/lang/String;
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lcom/peel/data/l;->l:Ljava/lang/String;

    return-object v0
.end method

.method public m()Ljava/lang/String;
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lcom/peel/data/l;->k:Ljava/lang/String;

    return-object v0
.end method

.method public n()Ljava/lang/String;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/peel/data/l;->n:Ljava/lang/String;

    return-object v0
.end method
