.class public final Lcom/peel/data/a/b;
.super Lcom/peel/data/g;


# static fields
.field private static final a:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27
    const-class v0, Lcom/peel/data/a/b;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/peel/data/a/b;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(ILjava/lang/String;Ljava/lang/String;Z)V
    .locals 11

    .prologue
    const/4 v1, 0x0

    const/4 v6, 0x0

    .line 34
    move-object v0, p0

    move v2, p1

    move-object v3, p2

    move-object v4, p3

    move v5, p4

    move v7, v1

    move-object v8, v6

    move-object v9, v6

    move-object v10, v6

    invoke-direct/range {v0 .. v10}, Lcom/peel/data/g;-><init>(IILjava/lang/String;Ljava/lang/String;ZLjava/lang/String;ILandroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;)V

    .line 35
    return-void
.end method

.method public constructor <init>(ILjava/lang/String;Z)V
    .locals 1

    .prologue
    .line 30
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0, p2, p3}, Lcom/peel/data/a/b;-><init>(ILjava/lang/String;Ljava/lang/String;Z)V

    .line 31
    return-void
.end method


# virtual methods
.method public a(ILjava/util/Map;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 38
    invoke-super {p0, p1, p2}, Lcom/peel/data/g;->a(ILjava/util/Map;)V

    .line 41
    const-string/jumbo v0, "Power_Off"

    invoke-virtual {p0, v0}, Lcom/peel/data/a/b;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 42
    invoke-virtual {p0}, Lcom/peel/data/a/b;->a()Ljava/util/Map;

    move-result-object v0

    const-string/jumbo v1, "Power_Off"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    .line 43
    const-string/jumbo v1, "funName"

    const-string/jumbo v2, "PowerOff"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 44
    const-string/jumbo v1, "PowerOff"

    invoke-virtual {p0, v1, v0}, Lcom/peel/data/a/b;->a(Ljava/lang/String;Ljava/util/Map;)V

    .line 48
    :cond_0
    const-string/jumbo v0, "Dot_DASh"

    invoke-virtual {p0, v0}, Lcom/peel/data/a/b;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 49
    const-string/jumbo v0, "Dot_DASh"

    invoke-interface {p2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    .line 50
    const-string/jumbo v1, "Dot_DASh"

    invoke-virtual {p0, v1}, Lcom/peel/data/a/b;->b(Ljava/lang/String;)V

    .line 52
    const-string/jumbo v1, "funName"

    const-string/jumbo v2, "."

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 53
    const-string/jumbo v1, "."

    invoke-virtual {p0, v1, v0}, Lcom/peel/data/a/b;->a(Ljava/lang/String;Ljava/util/Map;)V

    .line 57
    :cond_1
    const-string/jumbo v0, "PlayPause"

    invoke-virtual {p0, v0}, Lcom/peel/data/a/b;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 58
    const-string/jumbo v0, "PlayPause"

    invoke-interface {p2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    .line 59
    invoke-virtual {p0}, Lcom/peel/data/a/b;->a()Ljava/util/Map;

    move-result-object v1

    const-string/jumbo v2, "PlayPause"

    invoke-interface {v1, v2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 60
    const-string/jumbo v1, "funName"

    const-string/jumbo v2, "Play"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 62
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1, v0}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    .line 63
    const-string/jumbo v2, "funName"

    const-string/jumbo v3, "Pause"

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 64
    const-string/jumbo v2, "Play"

    invoke-virtual {p0, v2, v0}, Lcom/peel/data/a/b;->a(Ljava/lang/String;Ljava/util/Map;)V

    .line 65
    const-string/jumbo v0, "Pause"

    invoke-virtual {p0, v0, v1}, Lcom/peel/data/a/b;->a(Ljava/lang/String;Ljava/util/Map;)V

    .line 68
    :cond_2
    const-string/jumbo v0, "Play/Pause"

    invoke-virtual {p0, v0}, Lcom/peel/data/a/b;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 69
    const-string/jumbo v0, "Play/Pause"

    invoke-interface {p2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    .line 70
    const-string/jumbo v1, "Play/Pause"

    invoke-virtual {p0, v1}, Lcom/peel/data/a/b;->b(Ljava/lang/String;)V

    .line 71
    const-string/jumbo v1, "funName"

    const-string/jumbo v2, "Play"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 73
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1, v0}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    .line 74
    const-string/jumbo v2, "funName"

    const-string/jumbo v3, "Pause"

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 75
    const-string/jumbo v2, "Play"

    invoke-virtual {p0, v2, v0}, Lcom/peel/data/a/b;->a(Ljava/lang/String;Ljava/util/Map;)V

    .line 76
    const-string/jumbo v0, "Pause"

    invoke-virtual {p0, v0, v1}, Lcom/peel/data/a/b;->a(Ljava/lang/String;Ljava/util/Map;)V

    .line 80
    :cond_3
    const-string/jumbo v0, "PopUpMenu"

    invoke-virtual {p0, v0}, Lcom/peel/data/a/b;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 81
    const-string/jumbo v0, "PopUpMenu"

    invoke-interface {p2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    .line 82
    const-string/jumbo v1, "PopUpMenu"

    invoke-virtual {p0, v1}, Lcom/peel/data/a/b;->b(Ljava/lang/String;)V

    .line 84
    const-string/jumbo v1, "funName"

    const-string/jumbo v2, "PopMenu"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 85
    const-string/jumbo v1, "PopMenu"

    invoke-virtual {p0, v1, v0}, Lcom/peel/data/a/b;->a(Ljava/lang/String;Ljava/util/Map;)V

    .line 89
    :cond_4
    const-string/jumbo v0, "Pop Up Menu"

    invoke-virtual {p0, v0}, Lcom/peel/data/a/b;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 90
    const-string/jumbo v0, "Pop Up Menu"

    invoke-interface {p2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    .line 91
    const-string/jumbo v1, "Pop Up Menu"

    invoke-virtual {p0, v1}, Lcom/peel/data/a/b;->b(Ljava/lang/String;)V

    .line 93
    const-string/jumbo v1, "funName"

    const-string/jumbo v2, "PopMenu"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 94
    const-string/jumbo v1, "PopMenu"

    invoke-virtual {p0, v1, v0}, Lcom/peel/data/a/b;->a(Ljava/lang/String;Ljava/util/Map;)V

    .line 98
    :cond_5
    const-string/jumbo v0, "DiscMenu"

    invoke-virtual {p0, v0}, Lcom/peel/data/a/b;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 99
    const-string/jumbo v0, "DiscMenu"

    invoke-interface {p2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    .line 100
    const-string/jumbo v1, "DiscMenu"

    invoke-virtual {p0, v1}, Lcom/peel/data/a/b;->b(Ljava/lang/String;)V

    .line 102
    const-string/jumbo v1, "funName"

    const-string/jumbo v2, "PopMenu"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 103
    const-string/jumbo v1, "PopMenu"

    invoke-virtual {p0, v1, v0}, Lcom/peel/data/a/b;->a(Ljava/lang/String;Ljava/util/Map;)V

    .line 106
    :cond_6
    sget-object v0, Lcom/peel/data/a/b;->a:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/peel/data/a/b;->a()Ljava/util/Map;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 107
    return-void
.end method
