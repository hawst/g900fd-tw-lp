.class public final Lcom/peel/data/Genre;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/os/Parcelable;
.implements Lcom/peel/data/j;
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable;",
        "Lcom/peel/data/j;",
        "Ljava/lang/Comparable",
        "<",
        "Lcom/peel/data/Genre;",
        ">;"
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/peel/data/Genre;",
            ">;"
        }
    .end annotation
.end field

.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:Ljava/lang/String;

.field private final c:Ljava/lang/String;

.field private final d:I

.field private e:I

.field private f:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 15
    new-instance v0, Lcom/peel/data/i;

    invoke-direct {v0}, Lcom/peel/data/i;-><init>()V

    sput-object v0, Lcom/peel/data/Genre;->CREATOR:Landroid/os/Parcelable$Creator;

    .line 24
    const-class v0, Lcom/peel/data/Genre;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/peel/data/Genre;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;IIZ)V
    .locals 1

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/peel/data/Genre;->f:Z

    .line 32
    iput-object p1, p0, Lcom/peel/data/Genre;->b:Ljava/lang/String;

    .line 33
    iput-object p2, p0, Lcom/peel/data/Genre;->c:Ljava/lang/String;

    .line 34
    iput p3, p0, Lcom/peel/data/Genre;->d:I

    .line 35
    iput p4, p0, Lcom/peel/data/Genre;->e:I

    .line 36
    iput-boolean p5, p0, Lcom/peel/data/Genre;->f:Z

    .line 37
    return-void
.end method

.method static synthetic a(Landroid/os/Parcel;)Lcom/peel/data/Genre;
    .locals 1

    .prologue
    .line 12
    invoke-static {p0}, Lcom/peel/data/Genre;->b(Landroid/os/Parcel;)Lcom/peel/data/Genre;

    move-result-object v0

    return-object v0
.end method

.method private static b(Landroid/os/Parcel;)Lcom/peel/data/Genre;
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 40
    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 41
    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 42
    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    move-result v3

    .line 43
    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    move-result v4

    .line 44
    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v5, v0, :cond_0

    .line 46
    :goto_0
    new-instance v0, Lcom/peel/data/Genre;

    invoke-direct/range {v0 .. v5}, Lcom/peel/data/Genre;-><init>(Ljava/lang/String;Ljava/lang/String;IIZ)V

    return-object v0

    .line 44
    :cond_0
    const/4 v5, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a(Lcom/peel/data/Genre;)I
    .locals 2

    .prologue
    .line 73
    iget v0, p0, Lcom/peel/data/Genre;->e:I

    iget v1, p1, Lcom/peel/data/Genre;->e:I

    sub-int/2addr v0, v1

    return v0
.end method

.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/peel/data/Genre;->b:Ljava/lang/String;

    return-object v0
.end method

.method public a(I)V
    .locals 0

    .prologue
    .line 58
    iput p1, p0, Lcom/peel/data/Genre;->e:I

    .line 60
    return-void
.end method

.method public a(Lorg/codehaus/jackson/JsonGenerator;)V
    .locals 3

    .prologue
    .line 79
    :try_start_0
    invoke-virtual {p1}, Lorg/codehaus/jackson/JsonGenerator;->writeStartObject()V

    .line 82
    const-string/jumbo v0, "object_type"

    const-class v1, Lcom/peel/data/Genre;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lorg/codehaus/jackson/JsonGenerator;->writeStringField(Ljava/lang/String;Ljava/lang/String;)V

    .line 84
    const-string/jumbo v0, "id"

    iget-object v1, p0, Lcom/peel/data/Genre;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lorg/codehaus/jackson/JsonGenerator;->writeStringField(Ljava/lang/String;Ljava/lang/String;)V

    .line 85
    const-string/jumbo v0, "name"

    iget-object v1, p0, Lcom/peel/data/Genre;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lorg/codehaus/jackson/JsonGenerator;->writeStringField(Ljava/lang/String;Ljava/lang/String;)V

    .line 86
    const-string/jumbo v0, "type"

    iget v1, p0, Lcom/peel/data/Genre;->d:I

    invoke-virtual {p1, v0, v1}, Lorg/codehaus/jackson/JsonGenerator;->writeNumberField(Ljava/lang/String;I)V

    .line 87
    const-string/jumbo v0, "rank"

    iget v1, p0, Lcom/peel/data/Genre;->e:I

    invoke-virtual {p1, v0, v1}, Lorg/codehaus/jackson/JsonGenerator;->writeNumberField(Ljava/lang/String;I)V

    .line 88
    const-string/jumbo v0, "cut"

    iget-boolean v1, p0, Lcom/peel/data/Genre;->f:Z

    invoke-virtual {p1, v0, v1}, Lorg/codehaus/jackson/JsonGenerator;->writeBooleanField(Ljava/lang/String;Z)V

    .line 90
    invoke-virtual {p1}, Lorg/codehaus/jackson/JsonGenerator;->writeEndObject()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 94
    :goto_0
    return-void

    .line 91
    :catch_0
    move-exception v0

    .line 92
    sget-object v1, Lcom/peel/data/Genre;->a:Ljava/lang/String;

    sget-object v2, Lcom/peel/data/Genre;->a:Ljava/lang/String;

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public a(Z)V
    .locals 0

    .prologue
    .line 65
    iput-boolean p1, p0, Lcom/peel/data/Genre;->f:Z

    .line 67
    return-void
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/peel/data/Genre;->c:Ljava/lang/String;

    return-object v0
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 62
    iget-boolean v0, p0, Lcom/peel/data/Genre;->f:Z

    return v0
.end method

.method public synthetic compareTo(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 12
    check-cast p1, Lcom/peel/data/Genre;

    invoke-virtual {p0, p1}, Lcom/peel/data/Genre;->a(Lcom/peel/data/Genre;)I

    move-result v0

    return v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 97
    const/4 v0, 0x0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lcom/peel/data/Genre;->b:Ljava/lang/String;

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 100
    iget-object v0, p0, Lcom/peel/data/Genre;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 101
    iget-object v0, p0, Lcom/peel/data/Genre;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 102
    iget v0, p0, Lcom/peel/data/Genre;->d:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 103
    iget v0, p0, Lcom/peel/data/Genre;->e:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 104
    iget-boolean v0, p0, Lcom/peel/data/Genre;->f:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 105
    return-void

    .line 104
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
