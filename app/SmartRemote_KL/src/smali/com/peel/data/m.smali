.class public Lcom/peel/data/m;
.super Ljava/lang/Object;


# static fields
.field public static final a:Lcom/peel/data/aq;

.field public static b:Ljava/lang/String;

.field private static final c:Ljava/lang/String;

.field private static d:Z

.field private static e:Lcom/peel/data/m;

.field private static f:Lcom/peel/e/c;

.field private static g:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 24
    new-instance v0, Lcom/peel/data/aq;

    invoke-direct {v0}, Lcom/peel/data/aq;-><init>()V

    sput-object v0, Lcom/peel/data/m;->a:Lcom/peel/data/aq;

    .line 25
    const-class v0, Lcom/peel/data/m;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/peel/data/m;->c:Ljava/lang/String;

    .line 26
    const-string/jumbo v0, "Peel"

    sput-object v0, Lcom/peel/data/m;->b:Ljava/lang/String;

    .line 27
    const/4 v0, 0x0

    sput-boolean v0, Lcom/peel/data/m;->d:Z

    .line 28
    sput-object v1, Lcom/peel/data/m;->e:Lcom/peel/data/m;

    .line 29
    sput-object v1, Lcom/peel/data/m;->f:Lcom/peel/e/c;

    .line 30
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/peel/data/m;->g:Ljava/util/HashMap;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 583
    return-void
.end method

.method public static a()Lcom/peel/data/m;
    .locals 1

    .prologue
    .line 33
    sget-object v0, Lcom/peel/data/m;->e:Lcom/peel/data/m;

    if-nez v0, :cond_0

    new-instance v0, Lcom/peel/data/m;

    invoke-direct {v0}, Lcom/peel/data/m;-><init>()V

    sput-object v0, Lcom/peel/data/m;->e:Lcom/peel/data/m;

    .line 34
    :cond_0
    sget-object v0, Lcom/peel/data/m;->e:Lcom/peel/data/m;

    return-object v0
.end method

.method public static a([Ljava/lang/String;)[Lcom/peel/data/k;
    .locals 1

    .prologue
    .line 39
    sget-object v0, Lcom/peel/data/m;->f:Lcom/peel/e/c;

    invoke-virtual {v0, p0}, Lcom/peel/e/c;->a([Ljava/lang/String;)[Lcom/peel/data/k;

    move-result-object v0

    return-object v0
.end method

.method static synthetic h()Lcom/peel/e/c;
    .locals 1

    .prologue
    .line 22
    sget-object v0, Lcom/peel/data/m;->f:Lcom/peel/e/c;

    return-object v0
.end method


# virtual methods
.method public a(ILcom/peel/util/t;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lcom/peel/util/t",
            "<",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 520
    sget-object v0, Lcom/peel/data/m;->c:Ljava/lang/String;

    const-string/jumbo v1, "get all shows"

    new-instance v2, Lcom/peel/data/aj;

    invoke-direct {v2, p0, p2, p1}, Lcom/peel/data/aj;-><init>(Lcom/peel/data/m;Lcom/peel/util/t;I)V

    invoke-static {v0, v1, v2}, Lcom/peel/util/i;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/lang/Runnable;

    .line 526
    return-void
.end method

.method public a(Landroid/content/Context;)V
    .locals 4

    .prologue
    .line 43
    sget-boolean v0, Lcom/peel/data/m;->d:Z

    if-eqz v0, :cond_0

    .line 57
    :goto_0
    return-void

    .line 44
    :cond_0
    const/4 v0, 0x1

    sput-boolean v0, Lcom/peel/data/m;->d:Z

    .line 46
    new-instance v0, Lcom/peel/e/c;

    invoke-direct {v0, p1}, Lcom/peel/e/c;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/peel/data/m;->f:Lcom/peel/e/c;

    .line 47
    sget-object v0, Lcom/peel/data/m;->f:Lcom/peel/e/c;

    invoke-virtual {v0}, Lcom/peel/e/c;->a()V

    .line 49
    invoke-virtual {p0, p1}, Lcom/peel/data/m;->c(Landroid/content/Context;)V

    .line 52
    :try_start_0
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 53
    const-string/jumbo v1, "app_name"

    const-string/jumbo v2, "string"

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/peel/data/m;->b:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 54
    :catch_0
    move-exception v0

    .line 55
    const-string/jumbo v0, "Peel"

    sput-object v0, Lcom/peel/data/m;->b:Ljava/lang/String;

    goto :goto_0
.end method

.method public a(Lcom/peel/data/at;)V
    .locals 3

    .prologue
    .line 102
    sget-object v0, Lcom/peel/data/m;->c:Ljava/lang/String;

    const-string/jumbo v1, "removeRoom"

    new-instance v2, Lcom/peel/data/al;

    invoke-direct {v2, p0, p1}, Lcom/peel/data/al;-><init>(Lcom/peel/data/m;Lcom/peel/data/at;)V

    invoke-static {v0, v1, v2}, Lcom/peel/util/i;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/lang/Runnable;

    .line 108
    return-void
.end method

.method public a(Lcom/peel/data/d;)V
    .locals 3

    .prologue
    .line 165
    sget-object v0, Lcom/peel/data/m;->c:Ljava/lang/String;

    const-string/jumbo v1, "removeActivity"

    new-instance v2, Lcom/peel/data/p;

    invoke-direct {v2, p0, p1}, Lcom/peel/data/p;-><init>(Lcom/peel/data/m;Lcom/peel/data/d;)V

    invoke-static {v0, v1, v2}, Lcom/peel/util/i;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/lang/Runnable;

    .line 169
    return-void
.end method

.method public a(Lcom/peel/data/d;Lcom/peel/data/at;)V
    .locals 5

    .prologue
    const/4 v0, 0x0

    const/16 v4, 0x14

    .line 144
    :try_start_0
    sget-object v1, Lcom/peel/data/m;->c:Ljava/lang/String;

    const-string/jumbo v2, "add Activity to Room"

    new-instance v3, Lcom/peel/data/ap;

    invoke-direct {v3, p0, p1, p2}, Lcom/peel/data/ap;-><init>(Lcom/peel/data/m;Lcom/peel/data/d;Lcom/peel/data/at;)V

    invoke-static {v1, v2, v3}, Lcom/peel/util/i;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/lang/Runnable;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 149
    sget-object v1, Lcom/peel/data/m;->a:Lcom/peel/data/aq;

    check-cast v0, [Ljava/lang/Object;

    invoke-virtual {v1, v4, p1, v0}, Lcom/peel/data/aq;->a(ILjava/lang/Object;[Ljava/lang/Object;)V

    .line 151
    return-void

    .line 149
    :catchall_0
    move-exception v1

    sget-object v2, Lcom/peel/data/m;->a:Lcom/peel/data/aq;

    check-cast v0, [Ljava/lang/Object;

    invoke-virtual {v2, v4, p1, v0}, Lcom/peel/data/aq;->a(ILjava/lang/Object;[Ljava/lang/Object;)V

    throw v1
.end method

.method public a(Lcom/peel/data/d;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 135
    sget-object v0, Lcom/peel/data/m;->c:Ljava/lang/String;

    const-string/jumbo v1, "add activity"

    new-instance v2, Lcom/peel/data/ao;

    invoke-direct {v2, p0, p1, p2}, Lcom/peel/data/ao;-><init>(Lcom/peel/data/m;Lcom/peel/data/d;Ljava/lang/String;)V

    invoke-static {v0, v1, v2}, Lcom/peel/util/i;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/lang/Runnable;

    .line 139
    return-void
.end method

.method public a(Lcom/peel/data/g;)V
    .locals 3

    .prologue
    .line 302
    sget-object v0, Lcom/peel/data/m;->c:Ljava/lang/String;

    const-string/jumbo v1, "addDevice"

    new-instance v2, Lcom/peel/data/s;

    invoke-direct {v2, p0, p1}, Lcom/peel/data/s;-><init>(Lcom/peel/data/m;Lcom/peel/data/g;)V

    invoke-static {v0, v1, v2}, Lcom/peel/util/i;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/lang/Runnable;

    .line 306
    return-void
.end method

.method public a(Lcom/peel/data/g;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 8

    .prologue
    const/4 v3, 0x1

    .line 185
    invoke-virtual {p1}, Lcom/peel/data/g;->a()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0, p3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 187
    invoke-virtual {p1}, Lcom/peel/data/g;->a()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0, p3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    const-string/jumbo v1, "UES"

    invoke-static {p6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 188
    invoke-virtual {p1}, Lcom/peel/data/g;->a()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0, p3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    const-string/jumbo v1, "frequency"

    invoke-interface {v0, v1, p4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 189
    invoke-virtual {p1}, Lcom/peel/data/g;->a()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0, p3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    const-string/jumbo v1, "ir"

    invoke-interface {v0, v1, p5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 213
    :goto_0
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v6

    const-string/jumbo v7, "updateLearnedIrCode"

    new-instance v0, Lcom/peel/data/q;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v5}, Lcom/peel/data/q;-><init>(Lcom/peel/data/m;Lcom/peel/data/g;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v6, v7, v0}, Lcom/peel/util/i;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/lang/Runnable;

    .line 217
    return-void

    .line 191
    :cond_0
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 192
    const-string/jumbo v1, "UES"

    invoke-static {p6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 193
    const-string/jumbo v1, "frequency"

    invoke-interface {v0, v1, p4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 194
    const-string/jumbo v1, "ir"

    invoke-interface {v0, v1, p5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 195
    const-string/jumbo v1, "funId"

    const/4 v2, -0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 196
    const-string/jumbo v1, "type"

    const-string/jumbo v2, "Full_Repeat"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 197
    const-string/jumbo v1, "repeatframe"

    const-string/jumbo v2, ""

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 198
    const-string/jumbo v1, "repeatcount"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 199
    const-string/jumbo v1, "mainframe"

    invoke-interface {v0, v1, p5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 200
    const-string/jumbo v1, "toggleframe1"

    const-string/jumbo v2, ""

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 201
    const-string/jumbo v1, "toggleframe2"

    const-string/jumbo v2, ""

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 202
    const-string/jumbo v1, "toggleframe3"

    const-string/jumbo v2, ""

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 203
    const-string/jumbo v1, "toggleframe4"

    const-string/jumbo v2, ""

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 204
    const-string/jumbo v1, "endframe"

    const-string/jumbo v2, ""

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 205
    const-string/jumbo v1, "rank"

    const/16 v2, 0x3e7

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 206
    const-string/jumbo v1, "is_input"

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 207
    const-string/jumbo v1, "is_custom"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 208
    const-string/jumbo v1, "funName"

    invoke-interface {v0, v1, p3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 209
    const-string/jumbo v1, "funDisplayName"

    invoke-interface {v0, v1, p3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 210
    invoke-virtual {p1}, Lcom/peel/data/g;->a()Ljava/util/Map;

    move-result-object v1

    invoke-interface {v1, p3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0
.end method

.method public a(Lcom/peel/data/g;Lcom/peel/data/d;)V
    .locals 5

    .prologue
    const/16 v4, 0x17

    const/4 v1, 0x0

    .line 349
    :try_start_0
    sget-object v0, Lcom/peel/data/m;->c:Ljava/lang/String;

    const-string/jumbo v2, "remove device"

    new-instance v3, Lcom/peel/data/x;

    invoke-direct {v3, p0, p1, p2}, Lcom/peel/data/x;-><init>(Lcom/peel/data/m;Lcom/peel/data/g;Lcom/peel/data/d;)V

    invoke-static {v0, v2, v3}, Lcom/peel/util/i;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/lang/Runnable;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 354
    sget-object v2, Lcom/peel/data/m;->a:Lcom/peel/data/aq;

    move-object v0, v1

    check-cast v0, [Ljava/lang/Object;

    invoke-virtual {v2, v4, v1, v0}, Lcom/peel/data/aq;->a(ILjava/lang/Object;[Ljava/lang/Object;)V

    .line 356
    return-void

    .line 354
    :catchall_0
    move-exception v0

    move-object v2, v0

    sget-object v3, Lcom/peel/data/m;->a:Lcom/peel/data/aq;

    move-object v0, v1

    check-cast v0, [Ljava/lang/Object;

    invoke-virtual {v3, v4, v1, v0}, Lcom/peel/data/aq;->a(ILjava/lang/Object;[Ljava/lang/Object;)V

    throw v2
.end method

.method public a(Lcom/peel/data/h;)V
    .locals 3

    .prologue
    .line 95
    sget-object v0, Lcom/peel/data/m;->c:Ljava/lang/String;

    const-string/jumbo v1, "peelDB.addFruit"

    new-instance v2, Lcom/peel/data/ah;

    invoke-direct {v2, p0, p1}, Lcom/peel/data/ah;-><init>(Lcom/peel/data/m;Lcom/peel/data/h;)V

    invoke-static {v0, v1, v2}, Lcom/peel/util/i;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/lang/Runnable;

    .line 99
    return-void
.end method

.method public a(Lcom/peel/data/k;)V
    .locals 3

    .prologue
    .line 365
    sget-object v0, Lcom/peel/data/m;->c:Ljava/lang/String;

    const-string/jumbo v1, "addLibrary"

    new-instance v2, Lcom/peel/data/y;

    invoke-direct {v2, p0, p1}, Lcom/peel/data/y;-><init>(Lcom/peel/data/m;Lcom/peel/data/k;)V

    invoke-static {v0, v1, v2}, Lcom/peel/util/i;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/lang/Runnable;

    .line 369
    return-void
.end method

.method public a(Lcom/peel/util/t;)V
    .locals 3

    .prologue
    .line 482
    sget-object v0, Lcom/peel/data/m;->c:Ljava/lang/String;

    const-string/jumbo v1, "addStopTags to database"

    new-instance v2, Lcom/peel/data/af;

    invoke-direct {v2, p0, p1}, Lcom/peel/data/af;-><init>(Lcom/peel/data/m;Lcom/peel/util/t;)V

    invoke-static {v0, v1, v2}, Lcom/peel/util/i;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/lang/Runnable;

    .line 489
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 529
    sget-object v0, Lcom/peel/data/m;->c:Ljava/lang/String;

    const-string/jumbo v1, "remove fav/dislike"

    new-instance v2, Lcom/peel/data/ak;

    invoke-direct {v2, p0, p1}, Lcom/peel/data/ak;-><init>(Lcom/peel/data/m;Ljava/lang/String;)V

    invoke-static {v0, v1, v2}, Lcom/peel/util/i;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/lang/Runnable;

    .line 535
    return-void
.end method

.method public a(Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 372
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 373
    invoke-virtual {v0, p2}, Landroid/os/Bundle;->putAll(Landroid/os/Bundle;)V

    .line 374
    sget-object v1, Lcom/peel/data/m;->c:Ljava/lang/String;

    const-string/jumbo v2, "peelDB.updateLibrary"

    new-instance v3, Lcom/peel/data/z;

    invoke-direct {v3, p0, p1, v0}, Lcom/peel/data/z;-><init>(Lcom/peel/data/m;Ljava/lang/String;Landroid/os/Bundle;)V

    invoke-static {v1, v2, v3}, Lcom/peel/util/i;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/lang/Runnable;

    .line 378
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 333
    sget-object v0, Lcom/peel/data/m;->c:Ljava/lang/String;

    const-string/jumbo v1, "update device"

    new-instance v2, Lcom/peel/data/v;

    invoke-direct {v2, p0, p1, p2}, Lcom/peel/data/v;-><init>(Lcom/peel/data/m;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0, v1, v2}, Lcom/peel/util/i;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/lang/Runnable;

    .line 337
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 3

    .prologue
    .line 510
    sget-object v0, Lcom/peel/data/m;->c:Ljava/lang/String;

    const-string/jumbo v1, " get user taste by show id"

    new-instance v2, Lcom/peel/data/ai;

    invoke-direct {v2, p0, p1, p2, p3}, Lcom/peel/data/ai;-><init>(Lcom/peel/data/m;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v0, v1, v2}, Lcom/peel/util/i;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/lang/Runnable;

    .line 516
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 452
    sget-object v0, Lcom/peel/data/m;->c:Ljava/lang/String;

    const-string/jumbo v1, "addUser to database"

    new-instance v2, Lcom/peel/data/ad;

    invoke-direct {v2, p0, p1, p2, p3}, Lcom/peel/data/ad;-><init>(Lcom/peel/data/m;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)V

    invoke-static {v0, v1, v2}, Lcom/peel/util/i;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/lang/Runnable;

    .line 458
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Integer;)V
    .locals 10

    .prologue
    const/16 v9, 0x1e

    const/4 v6, 0x0

    .line 323
    :try_start_0
    sget-object v7, Lcom/peel/data/m;->c:Ljava/lang/String;

    const-string/jumbo v8, "update device inputs"

    new-instance v0, Lcom/peel/data/u;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/peel/data/u;-><init>(Lcom/peel/data/m;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Integer;)V

    invoke-static {v7, v8, v0}, Lcom/peel/util/i;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/lang/Runnable;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 328
    sget-object v1, Lcom/peel/data/m;->a:Lcom/peel/data/aq;

    move-object v0, v6

    check-cast v0, [Ljava/lang/Object;

    invoke-virtual {v1, v9, v6, v0}, Lcom/peel/data/aq;->a(ILjava/lang/Object;[Ljava/lang/Object;)V

    .line 330
    return-void

    .line 328
    :catchall_0
    move-exception v0

    move-object v1, v0

    sget-object v2, Lcom/peel/data/m;->a:Lcom/peel/data/aq;

    move-object v0, v6

    check-cast v0, [Ljava/lang/Object;

    invoke-virtual {v2, v9, v6, v0}, Lcom/peel/data/aq;->a(ILjava/lang/Object;[Ljava/lang/Object;)V

    throw v1
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Integer;Ljava/lang/String;)V
    .locals 10

    .prologue
    const/16 v9, 0x16

    const/4 v6, 0x0

    .line 311
    :try_start_0
    sget-object v7, Lcom/peel/data/m;->c:Ljava/lang/String;

    const-string/jumbo v8, "addDeviceToActivity"

    new-instance v0, Lcom/peel/data/t;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/peel/data/t;-><init>(Lcom/peel/data/m;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Integer;Ljava/lang/String;)V

    invoke-static {v7, v8, v0}, Lcom/peel/util/i;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/lang/Runnable;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 316
    sget-object v1, Lcom/peel/data/m;->a:Lcom/peel/data/aq;

    move-object v0, v6

    check-cast v0, [Ljava/lang/Object;

    invoke-virtual {v1, v9, v6, v0}, Lcom/peel/data/aq;->a(ILjava/lang/Object;[Ljava/lang/Object;)V

    .line 318
    return-void

    .line 316
    :catchall_0
    move-exception v0

    move-object v1, v0

    sget-object v2, Lcom/peel/data/m;->a:Lcom/peel/data/aq;

    move-object v0, v6

    check-cast v0, [Ljava/lang/Object;

    invoke-virtual {v2, v9, v6, v0}, Lcom/peel/data/aq;->a(ILjava/lang/Object;[Ljava/lang/Object;)V

    throw v1
.end method

.method public a(Ljava/lang/String;Ljava/util/List;Z)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/peel/data/l;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 412
    sget-object v0, Lcom/peel/data/m;->c:Ljava/lang/String;

    const-string/jumbo v1, "addListingsToLibrary"

    new-instance v2, Lcom/peel/data/ab;

    invoke-direct {v2, p0, p1, p2, p3}, Lcom/peel/data/ab;-><init>(Lcom/peel/data/m;Ljava/lang/String;Ljava/util/List;Z)V

    invoke-static {v0, v1, v2}, Lcom/peel/util/i;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/lang/Runnable;

    .line 416
    return-void
.end method

.method public a(Ljava/lang/String;[Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 419
    sget-object v0, Lcom/peel/data/m;->c:Ljava/lang/String;

    const-string/jumbo v1, "removeListingsFromLibrary"

    new-instance v2, Lcom/peel/data/ac;

    invoke-direct {v2, p0, p1, p2}, Lcom/peel/data/ac;-><init>(Lcom/peel/data/m;Ljava/lang/String;[Ljava/lang/String;)V

    invoke-static {v0, v1, v2}, Lcom/peel/util/i;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/lang/Runnable;

    .line 423
    return-void
.end method

.method public a(Ljava/lang/String;[Ljava/lang/String;Lcom/peel/util/t;)V
    .locals 3

    .prologue
    .line 404
    sget-object v0, Lcom/peel/data/m;->c:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "getListingsFromLibrary "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/peel/data/aa;

    invoke-direct {v2, p0, p1, p2, p3}, Lcom/peel/data/aa;-><init>(Lcom/peel/data/m;Ljava/lang/String;[Ljava/lang/String;Lcom/peel/util/t;)V

    invoke-static {v0, v1, v2}, Lcom/peel/util/i;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/lang/Runnable;

    .line 408
    return-void
.end method

.method public a(Ljava/util/ArrayList;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/peel/data/av;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 492
    sget-object v0, Lcom/peel/data/m;->c:Ljava/lang/String;

    const-string/jumbo v1, "addStopTags to database"

    new-instance v2, Lcom/peel/data/ag;

    invoke-direct {v2, p0, p1}, Lcom/peel/data/ag;-><init>(Lcom/peel/data/m;Ljava/util/ArrayList;)V

    invoke-static {v0, v1, v2}, Lcom/peel/util/i;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/lang/Runnable;

    .line 498
    return-void
.end method

.method public b()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 60
    sget-object v0, Lcom/peel/data/m;->f:Lcom/peel/e/c;

    if-eqz v0, :cond_0

    .line 61
    sget-object v0, Lcom/peel/data/m;->c:Ljava/lang/String;

    const-string/jumbo v1, "close the database"

    new-instance v2, Lcom/peel/data/n;

    invoke-direct {v2, p0}, Lcom/peel/data/n;-><init>(Lcom/peel/data/m;)V

    invoke-static {v0, v1, v2}, Lcom/peel/util/i;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/lang/Runnable;

    .line 65
    sput-object v3, Lcom/peel/data/m;->f:Lcom/peel/e/c;

    .line 67
    :cond_0
    sput-object v3, Lcom/peel/data/m;->e:Lcom/peel/data/m;

    .line 69
    const/4 v0, 0x0

    sput-boolean v0, Lcom/peel/data/m;->d:Z

    .line 70
    return-void
.end method

.method public b(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 73
    sget-object v0, Lcom/peel/data/m;->c:Ljava/lang/String;

    const-string/jumbo v1, "resetDatabase"

    new-instance v2, Lcom/peel/data/r;

    invoke-direct {v2, p0}, Lcom/peel/data/r;-><init>(Lcom/peel/data/m;)V

    invoke-static {v0, v1, v2}, Lcom/peel/util/i;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/lang/Runnable;

    .line 77
    return-void
.end method

.method public b(Lcom/peel/data/at;)V
    .locals 3

    .prologue
    .line 117
    sget-object v0, Lcom/peel/data/m;->c:Ljava/lang/String;

    const-string/jumbo v1, "peelDB.addRoom"

    new-instance v2, Lcom/peel/data/am;

    invoke-direct {v2, p0, p1}, Lcom/peel/data/am;-><init>(Lcom/peel/data/m;Lcom/peel/data/at;)V

    invoke-static {v0, v1, v2}, Lcom/peel/util/i;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/lang/Runnable;

    .line 121
    return-void
.end method

.method public b(Lcom/peel/data/d;Lcom/peel/data/at;)V
    .locals 5

    .prologue
    const/4 v0, 0x0

    const/16 v4, 0x15

    .line 155
    :try_start_0
    sget-object v1, Lcom/peel/data/m;->c:Ljava/lang/String;

    const-string/jumbo v2, "remove Activity from Room"

    new-instance v3, Lcom/peel/data/o;

    invoke-direct {v3, p0, p1, p2}, Lcom/peel/data/o;-><init>(Lcom/peel/data/m;Lcom/peel/data/d;Lcom/peel/data/at;)V

    invoke-static {v1, v2, v3}, Lcom/peel/util/i;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/lang/Runnable;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 160
    sget-object v1, Lcom/peel/data/m;->a:Lcom/peel/data/aq;

    check-cast v0, [Ljava/lang/Object;

    invoke-virtual {v1, v4, p1, v0}, Lcom/peel/data/aq;->a(ILjava/lang/Object;[Ljava/lang/Object;)V

    .line 162
    return-void

    .line 160
    :catchall_0
    move-exception v1

    sget-object v2, Lcom/peel/data/m;->a:Lcom/peel/data/aq;

    check-cast v0, [Ljava/lang/Object;

    invoke-virtual {v2, v4, p1, v0}, Lcom/peel/data/aq;->a(ILjava/lang/Object;[Ljava/lang/Object;)V

    throw v1
.end method

.method public b(Lcom/peel/data/g;)V
    .locals 3

    .prologue
    .line 341
    sget-object v0, Lcom/peel/data/m;->c:Ljava/lang/String;

    const-string/jumbo v1, "remove device"

    new-instance v2, Lcom/peel/data/w;

    invoke-direct {v2, p0, p1}, Lcom/peel/data/w;-><init>(Lcom/peel/data/m;Lcom/peel/data/g;)V

    invoke-static {v0, v1, v2}, Lcom/peel/util/i;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/lang/Runnable;

    .line 345
    return-void
.end method

.method public b(Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 461
    sget-object v0, Lcom/peel/data/m;->c:Ljava/lang/String;

    const-string/jumbo v1, "updateUser to database"

    new-instance v2, Lcom/peel/data/ae;

    invoke-direct {v2, p0, p1, p2}, Lcom/peel/data/ae;-><init>(Lcom/peel/data/m;Ljava/lang/String;Landroid/os/Bundle;)V

    invoke-static {v0, v1, v2}, Lcom/peel/util/i;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/lang/Runnable;

    .line 467
    return-void
.end method

.method public c(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 548
    sget-object v0, Lcom/peel/data/m;->c:Ljava/lang/String;

    const-string/jumbo v1, "\n\n ######### Clean Up Bad Data ########\n\n"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 549
    invoke-static {p1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 550
    const-string/jumbo v1, "setup_type"

    const/16 v2, -0x3e7

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    .line 555
    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    .line 557
    sget-object v1, Lcom/peel/data/m;->c:Ljava/lang/String;

    const-string/jumbo v2, "\n\n######### Clean Up Bad Data: cleanupControlOnlyDanglingRoom() ########"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 558
    sget-object v1, Lcom/peel/data/m;->f:Lcom/peel/e/c;

    invoke-virtual {v1, v0}, Lcom/peel/e/c;->a(Landroid/content/SharedPreferences;)V

    .line 560
    sget-object v1, Lcom/peel/data/m;->c:Ljava/lang/String;

    const-string/jumbo v2, "\n\n######### Clean Up Bad Data: fixMissingRoom() ########"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 561
    sget-object v1, Lcom/peel/data/m;->f:Lcom/peel/e/c;

    invoke-virtual {v1, v0}, Lcom/peel/e/c;->e(Landroid/content/SharedPreferences;)V

    .line 579
    :cond_0
    :goto_0
    sget-object v1, Lcom/peel/data/m;->c:Ljava/lang/String;

    const-string/jumbo v2, "\n\n ######### restoreControlDevice ########\n\n"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 580
    sget-object v1, Lcom/peel/data/m;->f:Lcom/peel/e/c;

    invoke-virtual {v1, v0}, Lcom/peel/e/c;->f(Landroid/content/SharedPreferences;)V

    .line 581
    return-void

    .line 562
    :cond_1
    if-nez v1, :cond_0

    .line 565
    sget-object v1, Lcom/peel/data/m;->f:Lcom/peel/e/c;

    invoke-virtual {v1, v0}, Lcom/peel/e/c;->c(Landroid/content/SharedPreferences;)V

    .line 568
    sget-object v1, Lcom/peel/data/m;->f:Lcom/peel/e/c;

    invoke-virtual {v1, v0}, Lcom/peel/e/c;->b(Landroid/content/SharedPreferences;)V

    .line 575
    sget-object v1, Lcom/peel/data/m;->f:Lcom/peel/e/c;

    invoke-virtual {v1, v0}, Lcom/peel/e/c;->d(Landroid/content/SharedPreferences;)V

    goto :goto_0
.end method

.method public c(Lcom/peel/data/at;)V
    .locals 3

    .prologue
    .line 124
    sget-object v0, Lcom/peel/data/m;->c:Ljava/lang/String;

    const-string/jumbo v1, "peelDB.updateRoom"

    new-instance v2, Lcom/peel/data/an;

    invoke-direct {v2, p0, p1}, Lcom/peel/data/an;-><init>(Lcom/peel/data/m;Lcom/peel/data/at;)V

    invoke-static {v0, v1, v2}, Lcom/peel/util/i;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/lang/Runnable;

    .line 128
    return-void
.end method

.method public c()[Lcom/peel/data/h;
    .locals 1

    .prologue
    .line 88
    sget-object v0, Lcom/peel/data/m;->f:Lcom/peel/e/c;

    invoke-virtual {v0}, Lcom/peel/e/c;->b()[Lcom/peel/data/h;

    move-result-object v0

    .line 90
    if-nez v0, :cond_0

    const/4 v0, 0x0

    new-array v0, v0, [Lcom/peel/data/h;

    .line 91
    :cond_0
    return-object v0
.end method

.method public d()[Lcom/peel/data/at;
    .locals 2

    .prologue
    .line 111
    sget-object v0, Lcom/peel/data/m;->f:Lcom/peel/e/c;

    invoke-virtual {v0}, Lcom/peel/e/c;->c()[Lcom/peel/data/at;

    move-result-object v0

    .line 112
    if-eqz v0, :cond_0

    array-length v1, v0

    if-nez v1, :cond_1

    :cond_0
    const/4 v0, 0x0

    .line 113
    :cond_1
    return-object v0
.end method

.method public e()[Lcom/peel/data/d;
    .locals 1

    .prologue
    .line 131
    sget-object v0, Lcom/peel/data/m;->f:Lcom/peel/e/c;

    invoke-virtual {v0}, Lcom/peel/e/c;->d()[Lcom/peel/data/d;

    move-result-object v0

    return-object v0
.end method

.method public f()[Lcom/peel/data/g;
    .locals 1

    .prologue
    .line 359
    sget-object v0, Lcom/peel/data/m;->f:Lcom/peel/e/c;

    invoke-virtual {v0}, Lcom/peel/e/c;->e()[Lcom/peel/data/g;

    move-result-object v0

    return-object v0
.end method

.method public g()Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 470
    sget-object v0, Lcom/peel/data/m;->f:Lcom/peel/e/c;

    invoke-virtual {v0}, Lcom/peel/e/c;->g()Landroid/os/Bundle;

    move-result-object v0

    return-object v0
.end method
