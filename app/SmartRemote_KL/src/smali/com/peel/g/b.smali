.class public Lcom/peel/g/b;
.super Ljava/lang/Object;


# instance fields
.field private a:Ljava/lang/String;

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    iput-object v0, p0, Lcom/peel/g/b;->c:Ljava/lang/String;

    .line 37
    iput-object v0, p0, Lcom/peel/g/b;->d:Ljava/lang/String;

    .line 51
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 52
    :cond_0
    new-instance v0, Ljava/lang/Exception;

    const-string/jumbo v1, "null keys not allowed"

    invoke-direct {v0, v1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v0

    .line 54
    :cond_1
    iput-object p1, p0, Lcom/peel/g/b;->a:Ljava/lang/String;

    .line 55
    iput-object p2, p0, Lcom/peel/g/b;->b:Ljava/lang/String;

    .line 58
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/peel/g/b;->e:Ljava/lang/String;

    .line 59
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/peel/g/b;->f:Ljava/lang/String;

    .line 60
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;)Lcom/peel/g/b;
    .locals 0

    .prologue
    .line 69
    iput-object p1, p0, Lcom/peel/g/b;->c:Ljava/lang/String;

    .line 70
    return-object p0
.end method

.method public a()Ljava/lang/String;
    .locals 6

    .prologue
    .line 114
    iget-object v0, p0, Lcom/peel/g/b;->c:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 115
    new-instance v0, Ljava/lang/Exception;

    const-string/jumbo v1, "missing HttpMethod"

    invoke-direct {v0, v1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v0

    .line 117
    :cond_0
    iget-object v0, p0, Lcom/peel/g/b;->d:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 118
    new-instance v0, Ljava/lang/Exception;

    const-string/jumbo v1, "missing Resource"

    invoke-direct {v0, v1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v0

    .line 120
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "Peel "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/g/b;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/peel/g/b;->b:Ljava/lang/String;

    iget-object v2, p0, Lcom/peel/g/b;->c:Ljava/lang/String;

    iget-object v3, p0, Lcom/peel/g/b;->d:Ljava/lang/String;

    iget-object v4, p0, Lcom/peel/g/b;->e:Ljava/lang/String;

    iget-object v5, p0, Lcom/peel/g/b;->f:Ljava/lang/String;

    invoke-static {v1, v2, v3, v4, v5}, Lcom/peel/g/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public b(Ljava/lang/String;)Lcom/peel/g/b;
    .locals 0

    .prologue
    .line 80
    iput-object p1, p0, Lcom/peel/g/b;->d:Ljava/lang/String;

    .line 81
    return-object p0
.end method

.method public c(Ljava/lang/String;)Lcom/peel/g/b;
    .locals 0

    .prologue
    .line 91
    iput-object p1, p0, Lcom/peel/g/b;->e:Ljava/lang/String;

    .line 92
    return-object p0
.end method

.method public d(Ljava/lang/String;)Lcom/peel/g/b;
    .locals 0

    .prologue
    .line 102
    iput-object p1, p0, Lcom/peel/g/b;->f:Ljava/lang/String;

    .line 103
    return-object p0
.end method
