.class Lcom/facebook/dd;
.super Landroid/content/BroadcastReceiver;


# instance fields
.field final synthetic a:Lcom/facebook/db;


# direct methods
.method private constructor <init>(Lcom/facebook/db;)V
    .locals 0

    .prologue
    .line 261
    iput-object p1, p0, Lcom/facebook/dd;->a:Lcom/facebook/db;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/facebook/db;Lcom/facebook/dc;)V
    .locals 0

    .prologue
    .line 261
    invoke-direct {p0, p1}, Lcom/facebook/dd;-><init>(Lcom/facebook/db;)V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 264
    const-string/jumbo v0, "com.facebook.sdk.ACTIVE_SESSION_SET"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 265
    invoke-static {}, Lcom/facebook/ca;->j()Lcom/facebook/ca;

    move-result-object v0

    .line 266
    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/facebook/dd;->a:Lcom/facebook/db;

    invoke-static {v1}, Lcom/facebook/db;->a(Lcom/facebook/db;)Lcom/facebook/cq;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 267
    iget-object v1, p0, Lcom/facebook/dd;->a:Lcom/facebook/db;

    invoke-static {v1}, Lcom/facebook/db;->a(Lcom/facebook/db;)Lcom/facebook/cq;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/ca;->a(Lcom/facebook/cq;)V

    .line 275
    :cond_0
    :goto_0
    return-void

    .line 269
    :cond_1
    const-string/jumbo v0, "com.facebook.sdk.ACTIVE_SESSION_UNSET"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 270
    invoke-static {}, Lcom/facebook/ca;->j()Lcom/facebook/ca;

    move-result-object v0

    .line 271
    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/facebook/dd;->a:Lcom/facebook/db;

    invoke-static {v1}, Lcom/facebook/db;->a(Lcom/facebook/db;)Lcom/facebook/cq;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 272
    iget-object v1, p0, Lcom/facebook/dd;->a:Lcom/facebook/db;

    invoke-static {v1}, Lcom/facebook/db;->a(Lcom/facebook/db;)Lcom/facebook/cq;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/ca;->b(Lcom/facebook/cq;)V

    goto :goto_0
.end method
