.class Lcom/facebook/ay;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final a:Ljava/lang/String;

.field private static final b:[J


# instance fields
.field private c:Z

.field private d:Z

.field private e:J

.field private f:J

.field private g:J

.field private h:J

.field private i:I

.field private j:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 13
    const-class v0, Lcom/facebook/c;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/ay;->a:Ljava/lang/String;

    .line 22
    const/16 v0, 0x13

    new-array v0, v0, [J

    fill-array-data v0, :array_0

    sput-object v0, Lcom/facebook/ay;->b:[J

    return-void

    :array_0
    .array-data 8
        0x493e0
        0xdbba0
        0x1b7740
        0x36ee80
        0x1499700
        0x2932e00
        0x5265c00
        0xa4cb800
        0xf731400
        0x240c8400
        0x48190800
        0x6c258c00
        0x90321000L
        0x134fd9000L
        0x1cf7c5800L
        0x269fb2000L
        0x30479e800L
        0x39ef8b000L
        0x757b12c00L
    .end array-data
.end method

.method constructor <init>()V
    .locals 0

    .prologue
    .line 149
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 150
    invoke-direct {p0}, Lcom/facebook/ay;->a()V

    .line 151
    return-void
.end method

.method private static a(J)I
    .locals 4

    .prologue
    .line 270
    const/4 v0, 0x0

    .line 273
    :goto_0
    sget-object v1, Lcom/facebook/ay;->b:[J

    array-length v1, v1

    if-ge v0, v1, :cond_0

    sget-object v1, Lcom/facebook/ay;->b:[J

    aget-wide v2, v1, v0

    cmp-long v1, v2, p0

    if-gez v1, :cond_0

    .line 276
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 279
    :cond_0
    return v0
.end method

.method private a()V
    .locals 4

    .prologue
    const-wide/16 v2, -0x1

    const/4 v0, 0x0

    .line 283
    iput-boolean v0, p0, Lcom/facebook/ay;->d:Z

    .line 284
    iput-wide v2, p0, Lcom/facebook/ay;->f:J

    .line 285
    iput-wide v2, p0, Lcom/facebook/ay;->g:J

    .line 286
    iput v0, p0, Lcom/facebook/ay;->i:I

    .line 287
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/facebook/ay;->h:J

    .line 288
    return-void
.end method

.method private a(Lcom/facebook/c;J)V
    .locals 6

    .prologue
    .line 252
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 253
    const-string/jumbo v1, "fb_mobile_app_interruptions"

    iget v2, p0, Lcom/facebook/ay;->i:I

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 256
    const-string/jumbo v1, "fb_mobile_time_between_sessions"

    const-string/jumbo v2, "session_quanta_%d"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {p2, p3}, Lcom/facebook/ay;->a(J)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 259
    const-string/jumbo v1, "fb_mobile_launch_source"

    iget-object v2, p0, Lcom/facebook/ay;->j:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 262
    const-string/jumbo v1, "fb_mobile_deactivate_app"

    iget-wide v2, p0, Lcom/facebook/ay;->h:J

    const-wide/16 v4, 0x3e8

    div-long/2addr v2, v4

    long-to-double v2, v2

    invoke-virtual {p1, v1, v2, v3, v0}, Lcom/facebook/c;->a(Ljava/lang/String;DLandroid/os/Bundle;)V

    .line 266
    invoke-direct {p0}, Lcom/facebook/ay;->a()V

    .line 267
    return-void
.end method

.method private b()Z
    .locals 4

    .prologue
    .line 291
    iget-wide v0, p0, Lcom/facebook/ay;->g:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private c()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 297
    iget-boolean v0, p0, Lcom/facebook/ay;->c:Z

    if-nez v0, :cond_0

    move v0, v1

    .line 298
    :goto_0
    iput-boolean v1, p0, Lcom/facebook/ay;->c:Z

    .line 299
    return v0

    .line 297
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method a(Lcom/facebook/c;JLjava/lang/String;)V
    .locals 6

    .prologue
    const-wide/16 v0, 0x0

    .line 199
    .line 205
    invoke-direct {p0}, Lcom/facebook/ay;->c()Z

    move-result v2

    if-nez v2, :cond_0

    iget-wide v2, p0, Lcom/facebook/ay;->e:J

    sub-long v2, p2, v2

    const-wide/32 v4, 0x493e0

    cmp-long v2, v2, v4

    if-lez v2, :cond_1

    .line 207
    :cond_0
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 208
    const-string/jumbo v3, "fb_mobile_launch_source"

    invoke-virtual {v2, v3, p4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 211
    const-string/jumbo v3, "fb_mobile_activate_app"

    invoke-virtual {p1, v3, v2}, Lcom/facebook/c;->a(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 212
    iput-wide p2, p0, Lcom/facebook/ay;->e:J

    .line 217
    :cond_1
    iget-boolean v2, p0, Lcom/facebook/ay;->d:Z

    if-eqz v2, :cond_2

    .line 218
    sget-object v0, Lcom/facebook/bb;->e:Lcom/facebook/bb;

    sget-object v1, Lcom/facebook/ay;->a:Ljava/lang/String;

    const-string/jumbo v2, "Resume for active app"

    invoke-static {v0, v1, v2}, Lcom/facebook/b/ax;->a(Lcom/facebook/bb;Ljava/lang/String;Ljava/lang/String;)V

    .line 247
    :goto_0
    return-void

    .line 222
    :cond_2
    invoke-direct {p0}, Lcom/facebook/ay;->b()Z

    move-result v2

    if-eqz v2, :cond_5

    iget-wide v2, p0, Lcom/facebook/ay;->g:J

    sub-long v2, p2, v2

    .line 223
    :goto_1
    cmp-long v4, v2, v0

    if-gez v4, :cond_7

    .line 224
    sget-object v2, Lcom/facebook/bb;->e:Lcom/facebook/bb;

    sget-object v3, Lcom/facebook/ay;->a:Ljava/lang/String;

    const-string/jumbo v4, "Clock skew detected"

    invoke-static {v2, v3, v4}, Lcom/facebook/b/ax;->a(Lcom/facebook/bb;Ljava/lang/String;Ljava/lang/String;)V

    .line 230
    :goto_2
    const-wide/32 v2, 0xea60

    cmp-long v2, v0, v2

    if-lez v2, :cond_6

    .line 231
    invoke-direct {p0, p1, v0, v1}, Lcom/facebook/ay;->a(Lcom/facebook/c;J)V

    .line 241
    :cond_3
    :goto_3
    iget v0, p0, Lcom/facebook/ay;->i:I

    if-nez v0, :cond_4

    .line 242
    iput-object p4, p0, Lcom/facebook/ay;->j:Ljava/lang/String;

    .line 245
    :cond_4
    iput-wide p2, p0, Lcom/facebook/ay;->f:J

    .line 246
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/ay;->d:Z

    goto :goto_0

    :cond_5
    move-wide v2, v0

    .line 222
    goto :goto_1

    .line 235
    :cond_6
    const-wide/16 v2, 0x3e8

    cmp-long v0, v0, v2

    if-lez v0, :cond_3

    .line 236
    iget v0, p0, Lcom/facebook/ay;->i:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/facebook/ay;->i:I

    goto :goto_3

    :cond_7
    move-wide v0, v2

    goto :goto_2
.end method
