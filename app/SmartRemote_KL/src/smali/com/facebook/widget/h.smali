.class Lcom/facebook/widget/h;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/facebook/widget/LoginButton;


# direct methods
.method private constructor <init>(Lcom/facebook/widget/LoginButton;)V
    .locals 0

    .prologue
    .line 800
    iput-object p1, p0, Lcom/facebook/widget/h;->a:Lcom/facebook/widget/LoginButton;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/facebook/widget/LoginButton;Lcom/facebook/widget/d;)V
    .locals 0

    .prologue
    .line 800
    invoke-direct {p0, p1}, Lcom/facebook/widget/h;-><init>(Lcom/facebook/widget/LoginButton;)V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 10

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x0

    .line 804
    iget-object v0, p0, Lcom/facebook/widget/h;->a:Lcom/facebook/widget/LoginButton;

    invoke-virtual {v0}, Lcom/facebook/widget/LoginButton;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 805
    iget-object v1, p0, Lcom/facebook/widget/h;->a:Lcom/facebook/widget/LoginButton;

    invoke-static {v1}, Lcom/facebook/widget/LoginButton;->a(Lcom/facebook/widget/LoginButton;)Lcom/facebook/b/bk;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/b/bk;->b()Lcom/facebook/ca;

    move-result-object v6

    .line 807
    if-eqz v6, :cond_4

    .line 809
    iget-object v1, p0, Lcom/facebook/widget/h;->a:Lcom/facebook/widget/LoginButton;

    invoke-static {v1}, Lcom/facebook/widget/LoginButton;->d(Lcom/facebook/widget/LoginButton;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 811
    iget-object v1, p0, Lcom/facebook/widget/h;->a:Lcom/facebook/widget/LoginButton;

    invoke-virtual {v1}, Lcom/facebook/widget/LoginButton;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v4, Lcom/facebook/a/g;->com_facebook_loginview_log_out_action:I

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 812
    iget-object v1, p0, Lcom/facebook/widget/h;->a:Lcom/facebook/widget/LoginButton;

    invoke-virtual {v1}, Lcom/facebook/widget/LoginButton;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v7, Lcom/facebook/a/g;->com_facebook_loginview_cancel_action:I

    invoke-virtual {v1, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 814
    iget-object v1, p0, Lcom/facebook/widget/h;->a:Lcom/facebook/widget/LoginButton;

    invoke-static {v1}, Lcom/facebook/widget/LoginButton;->c(Lcom/facebook/widget/LoginButton;)Lcom/facebook/c/j;

    move-result-object v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/facebook/widget/h;->a:Lcom/facebook/widget/LoginButton;

    invoke-static {v1}, Lcom/facebook/widget/LoginButton;->c(Lcom/facebook/widget/LoginButton;)Lcom/facebook/c/j;

    move-result-object v1

    invoke-interface {v1}, Lcom/facebook/c/j;->d()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 815
    iget-object v1, p0, Lcom/facebook/widget/h;->a:Lcom/facebook/widget/LoginButton;

    invoke-virtual {v1}, Lcom/facebook/widget/LoginButton;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v8, Lcom/facebook/a/g;->com_facebook_loginview_logged_in_as:I

    invoke-virtual {v1, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-array v8, v3, [Ljava/lang/Object;

    iget-object v9, p0, Lcom/facebook/widget/h;->a:Lcom/facebook/widget/LoginButton;

    invoke-static {v9}, Lcom/facebook/widget/LoginButton;->c(Lcom/facebook/widget/LoginButton;)Lcom/facebook/c/j;

    move-result-object v9

    invoke-interface {v9}, Lcom/facebook/c/j;->d()Ljava/lang/String;

    move-result-object v9

    aput-object v9, v8, v2

    invoke-static {v1, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 819
    :goto_0
    new-instance v8, Landroid/app/AlertDialog$Builder;

    invoke-direct {v8, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 820
    invoke-virtual {v8, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v1, Lcom/facebook/widget/i;

    invoke-direct {v1, p0, v6}, Lcom/facebook/widget/i;-><init>(Lcom/facebook/widget/h;Lcom/facebook/ca;)V

    invoke-virtual {v0, v4, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, v7, v5}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 828
    invoke-virtual {v8}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 862
    :cond_0
    :goto_1
    iget-object v0, p0, Lcom/facebook/widget/h;->a:Lcom/facebook/widget/LoginButton;

    invoke-virtual {v0}, Lcom/facebook/widget/LoginButton;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/c;->a(Landroid/content/Context;)Lcom/facebook/c;

    move-result-object v1

    .line 864
    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    .line 865
    const-string/jumbo v7, "logging_in"

    if-eqz v6, :cond_9

    move v0, v2

    :goto_2
    invoke-virtual {v4, v7, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 867
    iget-object v0, p0, Lcom/facebook/widget/h;->a:Lcom/facebook/widget/LoginButton;

    invoke-static {v0}, Lcom/facebook/widget/LoginButton;->h(Lcom/facebook/widget/LoginButton;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0, v5, v4}, Lcom/facebook/c;->a(Ljava/lang/String;Ljava/lang/Double;Landroid/os/Bundle;)V

    .line 869
    iget-object v0, p0, Lcom/facebook/widget/h;->a:Lcom/facebook/widget/LoginButton;

    invoke-static {v0}, Lcom/facebook/widget/LoginButton;->i(Lcom/facebook/widget/LoginButton;)Landroid/view/View$OnClickListener;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 870
    iget-object v0, p0, Lcom/facebook/widget/h;->a:Lcom/facebook/widget/LoginButton;

    invoke-static {v0}, Lcom/facebook/widget/LoginButton;->i(Lcom/facebook/widget/LoginButton;)Landroid/view/View$OnClickListener;

    move-result-object v0

    invoke-interface {v0, p1}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    .line 872
    :cond_1
    return-void

    .line 817
    :cond_2
    iget-object v1, p0, Lcom/facebook/widget/h;->a:Lcom/facebook/widget/LoginButton;

    invoke-virtual {v1}, Lcom/facebook/widget/LoginButton;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v8, Lcom/facebook/a/g;->com_facebook_loginview_logged_in_using_facebook:I

    invoke-virtual {v1, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 830
    :cond_3
    invoke-virtual {v6}, Lcom/facebook/ca;->i()V

    goto :goto_1

    .line 833
    :cond_4
    iget-object v1, p0, Lcom/facebook/widget/h;->a:Lcom/facebook/widget/LoginButton;

    invoke-static {v1}, Lcom/facebook/widget/LoginButton;->a(Lcom/facebook/widget/LoginButton;)Lcom/facebook/b/bk;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/b/bk;->a()Lcom/facebook/ca;

    move-result-object v1

    .line 834
    if-eqz v1, :cond_5

    invoke-virtual {v1}, Lcom/facebook/ca;->c()Lcom/facebook/cv;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/cv;->b()Z

    move-result v4

    if-eqz v4, :cond_6

    .line 835
    :cond_5
    iget-object v1, p0, Lcom/facebook/widget/h;->a:Lcom/facebook/widget/LoginButton;

    invoke-static {v1}, Lcom/facebook/widget/LoginButton;->a(Lcom/facebook/widget/LoginButton;)Lcom/facebook/b/bk;

    move-result-object v1

    invoke-virtual {v1, v5}, Lcom/facebook/b/bk;->a(Lcom/facebook/ca;)V

    .line 836
    new-instance v1, Lcom/facebook/cl;

    invoke-direct {v1, v0}, Lcom/facebook/cl;-><init>(Landroid/content/Context;)V

    iget-object v4, p0, Lcom/facebook/widget/h;->a:Lcom/facebook/widget/LoginButton;

    invoke-static {v4}, Lcom/facebook/widget/LoginButton;->e(Lcom/facebook/widget/LoginButton;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/facebook/cl;->a(Ljava/lang/String;)Lcom/facebook/cl;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/cl;->a()Lcom/facebook/ca;

    move-result-object v1

    .line 837
    invoke-static {v1}, Lcom/facebook/ca;->a(Lcom/facebook/ca;)V

    .line 840
    :cond_6
    invoke-virtual {v1}, Lcom/facebook/ca;->a()Z

    move-result v4

    if-nez v4, :cond_0

    .line 842
    iget-object v4, p0, Lcom/facebook/widget/h;->a:Lcom/facebook/widget/LoginButton;

    invoke-static {v4}, Lcom/facebook/widget/LoginButton;->f(Lcom/facebook/widget/LoginButton;)Landroid/support/v4/app/Fragment;

    move-result-object v4

    if-eqz v4, :cond_7

    .line 843
    new-instance v0, Lcom/facebook/cn;

    iget-object v4, p0, Lcom/facebook/widget/h;->a:Lcom/facebook/widget/LoginButton;

    invoke-static {v4}, Lcom/facebook/widget/LoginButton;->f(Lcom/facebook/widget/LoginButton;)Landroid/support/v4/app/Fragment;

    move-result-object v4

    invoke-direct {v0, v4}, Lcom/facebook/cn;-><init>(Landroid/support/v4/app/Fragment;)V

    .line 848
    :goto_3
    if-eqz v0, :cond_0

    .line 849
    iget-object v4, p0, Lcom/facebook/widget/h;->a:Lcom/facebook/widget/LoginButton;

    invoke-static {v4}, Lcom/facebook/widget/LoginButton;->g(Lcom/facebook/widget/LoginButton;)Lcom/facebook/widget/g;

    move-result-object v4

    invoke-static {v4}, Lcom/facebook/widget/g;->a(Lcom/facebook/widget/g;)Lcom/facebook/ct;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/facebook/cn;->b(Lcom/facebook/ct;)Lcom/facebook/cn;

    .line 850
    iget-object v4, p0, Lcom/facebook/widget/h;->a:Lcom/facebook/widget/LoginButton;

    invoke-static {v4}, Lcom/facebook/widget/LoginButton;->g(Lcom/facebook/widget/LoginButton;)Lcom/facebook/widget/g;

    move-result-object v4

    invoke-static {v4}, Lcom/facebook/widget/g;->b(Lcom/facebook/widget/g;)Ljava/util/List;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/facebook/cn;->b(Ljava/util/List;)Lcom/facebook/cn;

    .line 851
    iget-object v4, p0, Lcom/facebook/widget/h;->a:Lcom/facebook/widget/LoginButton;

    invoke-static {v4}, Lcom/facebook/widget/LoginButton;->g(Lcom/facebook/widget/LoginButton;)Lcom/facebook/widget/g;

    move-result-object v4

    invoke-static {v4}, Lcom/facebook/widget/g;->c(Lcom/facebook/widget/g;)Lcom/facebook/cu;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/facebook/cn;->b(Lcom/facebook/cu;)Lcom/facebook/cn;

    .line 853
    sget-object v4, Lcom/facebook/b/bj;->b:Lcom/facebook/b/bj;

    iget-object v7, p0, Lcom/facebook/widget/h;->a:Lcom/facebook/widget/LoginButton;

    invoke-static {v7}, Lcom/facebook/widget/LoginButton;->g(Lcom/facebook/widget/LoginButton;)Lcom/facebook/widget/g;

    move-result-object v7

    invoke-static {v7}, Lcom/facebook/widget/g;->d(Lcom/facebook/widget/g;)Lcom/facebook/b/bj;

    move-result-object v7

    invoke-virtual {v4, v7}, Lcom/facebook/b/bj;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_8

    .line 854
    invoke-virtual {v1, v0}, Lcom/facebook/ca;->b(Lcom/facebook/cn;)V

    goto/16 :goto_1

    .line 844
    :cond_7
    instance-of v4, v0, Landroid/app/Activity;

    if-eqz v4, :cond_a

    .line 845
    new-instance v4, Lcom/facebook/cn;

    check-cast v0, Landroid/app/Activity;

    invoke-direct {v4, v0}, Lcom/facebook/cn;-><init>(Landroid/app/Activity;)V

    move-object v0, v4

    goto :goto_3

    .line 856
    :cond_8
    invoke-virtual {v1, v0}, Lcom/facebook/ca;->a(Lcom/facebook/cn;)V

    goto/16 :goto_1

    :cond_9
    move v0, v3

    .line 865
    goto/16 :goto_2

    :cond_a
    move-object v0, v5

    goto :goto_3
.end method
