.class public Lcom/facebook/widget/FacebookDialog;
.super Ljava/lang/Object;


# static fields
.field private static a:Lcom/facebook/be;


# direct methods
.method public static a(Landroid/content/Context;Lcom/facebook/widget/FacebookDialog$PendingCall;ILandroid/content/Intent;Lcom/facebook/widget/b;)Z
    .locals 2

    .prologue
    .line 355
    invoke-virtual {p1}, Lcom/facebook/widget/FacebookDialog$PendingCall;->c()I

    move-result v0

    if-eq p2, v0, :cond_0

    .line 356
    const/4 v0, 0x0

    .line 375
    :goto_0
    return v0

    .line 359
    :cond_0
    sget-object v0, Lcom/facebook/widget/FacebookDialog;->a:Lcom/facebook/be;

    if-eqz v0, :cond_1

    .line 360
    sget-object v0, Lcom/facebook/widget/FacebookDialog;->a:Lcom/facebook/be;

    invoke-virtual {p1}, Lcom/facebook/widget/FacebookDialog$PendingCall;->b()Ljava/util/UUID;

    move-result-object v1

    invoke-virtual {v0, p0, v1}, Lcom/facebook/be;->a(Landroid/content/Context;Ljava/util/UUID;)V

    .line 363
    :cond_1
    if-eqz p4, :cond_2

    .line 364
    invoke-static {p3}, Lcom/facebook/b/ay;->e(Landroid/content/Intent;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 365
    invoke-static {p3}, Lcom/facebook/b/ay;->f(Landroid/content/Intent;)Landroid/os/Bundle;

    move-result-object v0

    .line 366
    invoke-static {v0}, Lcom/facebook/b/ay;->a(Landroid/os/Bundle;)Ljava/lang/Exception;

    move-result-object v1

    .line 368
    invoke-interface {p4, p1, v1, v0}, Lcom/facebook/widget/b;->a(Lcom/facebook/widget/FacebookDialog$PendingCall;Ljava/lang/Exception;Landroid/os/Bundle;)V

    .line 375
    :cond_2
    :goto_1
    const/4 v0, 0x1

    goto :goto_0

    .line 370
    :cond_3
    invoke-static {p3}, Lcom/facebook/b/ay;->d(Landroid/content/Intent;)Landroid/os/Bundle;

    move-result-object v0

    .line 371
    invoke-interface {p4, p1, v0}, Lcom/facebook/widget/b;->a(Lcom/facebook/widget/FacebookDialog$PendingCall;Landroid/os/Bundle;)V

    goto :goto_1
.end method
