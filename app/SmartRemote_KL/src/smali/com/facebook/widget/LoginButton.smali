.class public Lcom/facebook/widget/LoginButton;
.super Landroid/widget/Button;


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private b:Ljava/lang/String;

.field private c:Lcom/facebook/b/bk;

.field private d:Lcom/facebook/c/j;

.field private e:Lcom/facebook/ca;

.field private f:Z

.field private g:Z

.field private h:Ljava/lang/String;

.field private i:Ljava/lang/String;

.field private j:Lcom/facebook/widget/l;

.field private k:Landroid/support/v4/app/Fragment;

.field private l:Lcom/facebook/widget/g;

.field private m:Ljava/lang/String;

.field private n:Landroid/view/View$OnClickListener;

.field private o:Z

.field private p:Lcom/facebook/widget/r;

.field private q:Lcom/facebook/widget/k;

.field private r:J

.field private s:Lcom/facebook/widget/m;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 80
    const-class v0, Lcom/facebook/widget/LoginButton;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/widget/LoginButton;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 218
    invoke-direct {p0, p1}, Landroid/widget/Button;-><init>(Landroid/content/Context;)V

    .line 81
    iput-object v0, p0, Lcom/facebook/widget/LoginButton;->b:Ljava/lang/String;

    .line 83
    iput-object v0, p0, Lcom/facebook/widget/LoginButton;->d:Lcom/facebook/c/j;

    .line 84
    iput-object v0, p0, Lcom/facebook/widget/LoginButton;->e:Lcom/facebook/ca;

    .line 91
    new-instance v0, Lcom/facebook/widget/g;

    invoke-direct {v0}, Lcom/facebook/widget/g;-><init>()V

    iput-object v0, p0, Lcom/facebook/widget/LoginButton;->l:Lcom/facebook/widget/g;

    .line 92
    const-string/jumbo v0, "fb_login_view_usage"

    iput-object v0, p0, Lcom/facebook/widget/LoginButton;->m:Ljava/lang/String;

    .line 95
    sget-object v0, Lcom/facebook/widget/r;->a:Lcom/facebook/widget/r;

    iput-object v0, p0, Lcom/facebook/widget/LoginButton;->p:Lcom/facebook/widget/r;

    .line 96
    sget-object v0, Lcom/facebook/widget/k;->a:Lcom/facebook/widget/k;

    iput-object v0, p0, Lcom/facebook/widget/LoginButton;->q:Lcom/facebook/widget/k;

    .line 97
    const-wide/16 v0, 0x1770

    iput-wide v0, p0, Lcom/facebook/widget/LoginButton;->r:J

    .line 219
    invoke-direct {p0, p1}, Lcom/facebook/widget/LoginButton;->a(Landroid/content/Context;)Z

    .line 221
    invoke-direct {p0}, Lcom/facebook/widget/LoginButton;->c()V

    .line 222
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 5

    .prologue
    const/4 v0, 0x0

    const/4 v2, 0x0

    .line 230
    invoke-direct {p0, p1, p2}, Landroid/widget/Button;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 81
    iput-object v0, p0, Lcom/facebook/widget/LoginButton;->b:Ljava/lang/String;

    .line 83
    iput-object v0, p0, Lcom/facebook/widget/LoginButton;->d:Lcom/facebook/c/j;

    .line 84
    iput-object v0, p0, Lcom/facebook/widget/LoginButton;->e:Lcom/facebook/ca;

    .line 91
    new-instance v0, Lcom/facebook/widget/g;

    invoke-direct {v0}, Lcom/facebook/widget/g;-><init>()V

    iput-object v0, p0, Lcom/facebook/widget/LoginButton;->l:Lcom/facebook/widget/g;

    .line 92
    const-string/jumbo v0, "fb_login_view_usage"

    iput-object v0, p0, Lcom/facebook/widget/LoginButton;->m:Ljava/lang/String;

    .line 95
    sget-object v0, Lcom/facebook/widget/r;->a:Lcom/facebook/widget/r;

    iput-object v0, p0, Lcom/facebook/widget/LoginButton;->p:Lcom/facebook/widget/r;

    .line 96
    sget-object v0, Lcom/facebook/widget/k;->a:Lcom/facebook/widget/k;

    iput-object v0, p0, Lcom/facebook/widget/LoginButton;->q:Lcom/facebook/widget/k;

    .line 97
    const-wide/16 v0, 0x1770

    iput-wide v0, p0, Lcom/facebook/widget/LoginButton;->r:J

    .line 232
    invoke-interface {p2}, Landroid/util/AttributeSet;->getStyleAttribute()I

    move-result v0

    if-nez v0, :cond_0

    .line 236
    const/16 v0, 0x11

    invoke-virtual {p0, v0}, Lcom/facebook/widget/LoginButton;->setGravity(I)V

    .line 237
    invoke-virtual {p0}, Lcom/facebook/widget/LoginButton;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/facebook/a/b;->com_facebook_loginview_text_color:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/facebook/widget/LoginButton;->setTextColor(I)V

    .line 238
    invoke-virtual {p0}, Lcom/facebook/widget/LoginButton;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/facebook/a/c;->com_facebook_loginview_text_size:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    invoke-virtual {p0, v2, v0}, Lcom/facebook/widget/LoginButton;->setTextSize(IF)V

    .line 240
    sget-object v0, Landroid/graphics/Typeface;->DEFAULT_BOLD:Landroid/graphics/Typeface;

    invoke-virtual {p0, v0}, Lcom/facebook/widget/LoginButton;->setTypeface(Landroid/graphics/Typeface;)V

    .line 241
    invoke-virtual {p0}, Lcom/facebook/widget/LoginButton;->isInEditMode()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 244
    invoke-virtual {p0}, Lcom/facebook/widget/LoginButton;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/facebook/a/b;->com_facebook_blue:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/facebook/widget/LoginButton;->setBackgroundColor(I)V

    .line 246
    const-string/jumbo v0, "Log in with Facebook"

    iput-object v0, p0, Lcom/facebook/widget/LoginButton;->h:Ljava/lang/String;

    .line 258
    :cond_0
    :goto_0
    invoke-direct {p0, p2}, Lcom/facebook/widget/LoginButton;->a(Landroid/util/AttributeSet;)V

    .line 259
    invoke-virtual {p0}, Lcom/facebook/widget/LoginButton;->isInEditMode()Z

    move-result v0

    if-nez v0, :cond_1

    .line 260
    invoke-direct {p0, p1}, Lcom/facebook/widget/LoginButton;->a(Landroid/content/Context;)Z

    .line 262
    :cond_1
    return-void

    .line 248
    :cond_2
    sget v0, Lcom/facebook/a/d;->com_facebook_button_blue:I

    invoke-virtual {p0, v0}, Lcom/facebook/widget/LoginButton;->setBackgroundResource(I)V

    .line 249
    sget v0, Lcom/facebook/a/d;->com_facebook_inverse_icon:I

    invoke-virtual {p0, v0, v2, v2, v2}, Lcom/facebook/widget/LoginButton;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    .line 250
    invoke-virtual {p0}, Lcom/facebook/widget/LoginButton;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/facebook/a/c;->com_facebook_loginview_compound_drawable_padding:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/facebook/widget/LoginButton;->setCompoundDrawablePadding(I)V

    .line 252
    invoke-virtual {p0}, Lcom/facebook/widget/LoginButton;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/facebook/a/c;->com_facebook_loginview_padding_left:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    invoke-virtual {p0}, Lcom/facebook/widget/LoginButton;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/facebook/a/c;->com_facebook_loginview_padding_top:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    invoke-virtual {p0}, Lcom/facebook/widget/LoginButton;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/facebook/a/c;->com_facebook_loginview_padding_right:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    invoke-virtual {p0}, Lcom/facebook/widget/LoginButton;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/facebook/a/c;->com_facebook_loginview_padding_bottom:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/facebook/widget/LoginButton;->setPadding(IIII)V

    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 270
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/Button;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 81
    iput-object v0, p0, Lcom/facebook/widget/LoginButton;->b:Ljava/lang/String;

    .line 83
    iput-object v0, p0, Lcom/facebook/widget/LoginButton;->d:Lcom/facebook/c/j;

    .line 84
    iput-object v0, p0, Lcom/facebook/widget/LoginButton;->e:Lcom/facebook/ca;

    .line 91
    new-instance v0, Lcom/facebook/widget/g;

    invoke-direct {v0}, Lcom/facebook/widget/g;-><init>()V

    iput-object v0, p0, Lcom/facebook/widget/LoginButton;->l:Lcom/facebook/widget/g;

    .line 92
    const-string/jumbo v0, "fb_login_view_usage"

    iput-object v0, p0, Lcom/facebook/widget/LoginButton;->m:Ljava/lang/String;

    .line 95
    sget-object v0, Lcom/facebook/widget/r;->a:Lcom/facebook/widget/r;

    iput-object v0, p0, Lcom/facebook/widget/LoginButton;->p:Lcom/facebook/widget/r;

    .line 96
    sget-object v0, Lcom/facebook/widget/k;->a:Lcom/facebook/widget/k;

    iput-object v0, p0, Lcom/facebook/widget/LoginButton;->q:Lcom/facebook/widget/k;

    .line 97
    const-wide/16 v0, 0x1770

    iput-wide v0, p0, Lcom/facebook/widget/LoginButton;->r:J

    .line 271
    invoke-direct {p0, p2}, Lcom/facebook/widget/LoginButton;->a(Landroid/util/AttributeSet;)V

    .line 272
    invoke-direct {p0, p1}, Lcom/facebook/widget/LoginButton;->a(Landroid/content/Context;)Z

    .line 273
    return-void
.end method

.method static synthetic a(Lcom/facebook/widget/LoginButton;)Lcom/facebook/b/bk;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/facebook/widget/LoginButton;->c:Lcom/facebook/b/bk;

    return-object v0
.end method

.method static synthetic a(Lcom/facebook/widget/LoginButton;Lcom/facebook/c/j;)Lcom/facebook/c/j;
    .locals 0

    .prologue
    .line 60
    iput-object p1, p0, Lcom/facebook/widget/LoginButton;->d:Lcom/facebook/c/j;

    return-object p1
.end method

.method private a(Landroid/util/AttributeSet;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 723
    invoke-virtual {p0}, Lcom/facebook/widget/LoginButton;->getContext()Landroid/content/Context;

    move-result-object v0

    sget-object v1, Lcom/facebook/a/h;->com_facebook_login_view:[I

    invoke-virtual {v0, p1, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 724
    const/4 v1, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Lcom/facebook/widget/LoginButton;->f:Z

    .line 725
    invoke-virtual {v0, v2, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Lcom/facebook/widget/LoginButton;->g:Z

    .line 726
    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/widget/LoginButton;->h:Ljava/lang/String;

    .line 727
    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/widget/LoginButton;->i:Ljava/lang/String;

    .line 728
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 729
    return-void
.end method

.method private a(Lcom/facebook/b/bs;)V
    .locals 1

    .prologue
    .line 654
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/facebook/b/bs;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/widget/LoginButton;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 655
    invoke-virtual {p1}, Lcom/facebook/b/bs;->c()Ljava/lang/String;

    move-result-object v0

    .line 656
    invoke-direct {p0, v0}, Lcom/facebook/widget/LoginButton;->a(Ljava/lang/String;)V

    .line 658
    :cond_0
    return-void
.end method

.method static synthetic a(Lcom/facebook/widget/LoginButton;Lcom/facebook/b/bs;)V
    .locals 0

    .prologue
    .line 60
    invoke-direct {p0, p1}, Lcom/facebook/widget/LoginButton;->a(Lcom/facebook/b/bs;)V

    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 661
    new-instance v0, Lcom/facebook/widget/m;

    invoke-direct {v0, p1, p0}, Lcom/facebook/widget/m;-><init>(Ljava/lang/String;Landroid/view/View;)V

    iput-object v0, p0, Lcom/facebook/widget/LoginButton;->s:Lcom/facebook/widget/m;

    .line 662
    iget-object v0, p0, Lcom/facebook/widget/LoginButton;->s:Lcom/facebook/widget/m;

    iget-object v1, p0, Lcom/facebook/widget/LoginButton;->p:Lcom/facebook/widget/r;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/m;->a(Lcom/facebook/widget/r;)V

    .line 663
    iget-object v0, p0, Lcom/facebook/widget/LoginButton;->s:Lcom/facebook/widget/m;

    iget-wide v2, p0, Lcom/facebook/widget/LoginButton;->r:J

    invoke-virtual {v0, v2, v3}, Lcom/facebook/widget/m;->a(J)V

    .line 664
    iget-object v0, p0, Lcom/facebook/widget/LoginButton;->s:Lcom/facebook/widget/m;

    invoke-virtual {v0}, Lcom/facebook/widget/m;->a()V

    .line 665
    return-void
.end method

.method private a(Landroid/content/Context;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 742
    if-nez p1, :cond_1

    .line 756
    :cond_0
    :goto_0
    return v0

    .line 746
    :cond_1
    invoke-static {}, Lcom/facebook/ca;->j()Lcom/facebook/ca;

    move-result-object v1

    .line 747
    if-eqz v1, :cond_2

    .line 748
    invoke-virtual {v1}, Lcom/facebook/ca;->a()Z

    move-result v0

    goto :goto_0

    .line 751
    :cond_2
    invoke-static {p1}, Lcom/facebook/b/bp;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 752
    if-eqz v1, :cond_0

    .line 756
    invoke-static {p1}, Lcom/facebook/ca;->a(Landroid/content/Context;)Lcom/facebook/ca;

    move-result-object v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method static synthetic b(Lcom/facebook/widget/LoginButton;)Lcom/facebook/widget/l;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/facebook/widget/LoginButton;->j:Lcom/facebook/widget/l;

    return-object v0
.end method

.method static synthetic b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 60
    sget-object v0, Lcom/facebook/widget/LoginButton;->a:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic c(Lcom/facebook/widget/LoginButton;)Lcom/facebook/c/j;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/facebook/widget/LoginButton;->d:Lcom/facebook/c/j;

    return-object v0
.end method

.method private c()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 613
    new-instance v0, Lcom/facebook/widget/h;

    invoke-direct {v0, p0, v4}, Lcom/facebook/widget/h;-><init>(Lcom/facebook/widget/LoginButton;Lcom/facebook/widget/d;)V

    invoke-super {p0, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 614
    invoke-direct {p0}, Lcom/facebook/widget/LoginButton;->e()V

    .line 615
    invoke-virtual {p0}, Lcom/facebook/widget/LoginButton;->isInEditMode()Z

    move-result v0

    if-nez v0, :cond_0

    .line 616
    new-instance v0, Lcom/facebook/b/bk;

    invoke-virtual {p0}, Lcom/facebook/widget/LoginButton;->getContext()Landroid/content/Context;

    move-result-object v1

    new-instance v2, Lcom/facebook/widget/f;

    invoke-direct {v2, p0, v4}, Lcom/facebook/widget/f;-><init>(Lcom/facebook/widget/LoginButton;Lcom/facebook/widget/d;)V

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v4, v3}, Lcom/facebook/b/bk;-><init>(Landroid/content/Context;Lcom/facebook/cq;Lcom/facebook/ca;Z)V

    iput-object v0, p0, Lcom/facebook/widget/LoginButton;->c:Lcom/facebook/b/bk;

    .line 617
    invoke-direct {p0}, Lcom/facebook/widget/LoginButton;->f()V

    .line 619
    :cond_0
    return-void
.end method

.method private d()V
    .locals 2

    .prologue
    .line 668
    iget-object v0, p0, Lcom/facebook/widget/LoginButton;->q:Lcom/facebook/widget/k;

    sget-object v1, Lcom/facebook/widget/k;->b:Lcom/facebook/widget/k;

    if-ne v0, v1, :cond_0

    .line 669
    invoke-virtual {p0}, Lcom/facebook/widget/LoginButton;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/facebook/a/g;->com_facebook_tooltip_default:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 670
    invoke-direct {p0, v0}, Lcom/facebook/widget/LoginButton;->a(Ljava/lang/String;)V

    .line 689
    :goto_0
    return-void

    .line 673
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/widget/LoginButton;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/b/bp;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 674
    new-instance v1, Lcom/facebook/widget/d;

    invoke-direct {v1, p0, v0}, Lcom/facebook/widget/d;-><init>(Lcom/facebook/widget/LoginButton;Ljava/lang/String;)V

    .line 686
    const/4 v0, 0x0

    check-cast v0, [Ljava/lang/Void;

    invoke-virtual {v1, v0}, Landroid/os/AsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method

.method static synthetic d(Lcom/facebook/widget/LoginButton;)Z
    .locals 1

    .prologue
    .line 60
    iget-boolean v0, p0, Lcom/facebook/widget/LoginButton;->f:Z

    return v0
.end method

.method static synthetic e(Lcom/facebook/widget/LoginButton;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/facebook/widget/LoginButton;->b:Ljava/lang/String;

    return-object v0
.end method

.method private e()V
    .locals 2

    .prologue
    .line 732
    iget-object v0, p0, Lcom/facebook/widget/LoginButton;->c:Lcom/facebook/b/bk;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/widget/LoginButton;->c:Lcom/facebook/b/bk;

    invoke-virtual {v0}, Lcom/facebook/b/bk;->b()Lcom/facebook/ca;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 733
    iget-object v0, p0, Lcom/facebook/widget/LoginButton;->i:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/widget/LoginButton;->i:Ljava/lang/String;

    :goto_0
    invoke-virtual {p0, v0}, Lcom/facebook/widget/LoginButton;->setText(Ljava/lang/CharSequence;)V

    .line 739
    :goto_1
    return-void

    .line 733
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/widget/LoginButton;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/facebook/a/g;->com_facebook_loginview_log_out_button:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 736
    :cond_1
    iget-object v0, p0, Lcom/facebook/widget/LoginButton;->h:Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/facebook/widget/LoginButton;->h:Ljava/lang/String;

    :goto_2
    invoke-virtual {p0, v0}, Lcom/facebook/widget/LoginButton;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    :cond_2
    invoke-virtual {p0}, Lcom/facebook/widget/LoginButton;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/facebook/a/g;->com_facebook_loginview_log_in_button:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_2
.end method

.method static synthetic f(Lcom/facebook/widget/LoginButton;)Landroid/support/v4/app/Fragment;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/facebook/widget/LoginButton;->k:Landroid/support/v4/app/Fragment;

    return-object v0
.end method

.method private f()V
    .locals 4

    .prologue
    .line 760
    iget-boolean v0, p0, Lcom/facebook/widget/LoginButton;->g:Z

    if-eqz v0, :cond_0

    .line 761
    iget-object v0, p0, Lcom/facebook/widget/LoginButton;->c:Lcom/facebook/b/bk;

    invoke-virtual {v0}, Lcom/facebook/b/bk;->b()Lcom/facebook/ca;

    move-result-object v0

    .line 762
    if-eqz v0, :cond_1

    .line 763
    iget-object v1, p0, Lcom/facebook/widget/LoginButton;->e:Lcom/facebook/ca;

    if-eq v0, v1, :cond_0

    .line 764
    new-instance v1, Lcom/facebook/widget/e;

    invoke-direct {v1, p0, v0}, Lcom/facebook/widget/e;-><init>(Lcom/facebook/widget/LoginButton;Lcom/facebook/ca;)V

    invoke-static {v0, v1}, Lcom/facebook/Request;->a(Lcom/facebook/ca;Lcom/facebook/bn;)Lcom/facebook/Request;

    move-result-object v1

    .line 778
    const/4 v2, 0x1

    new-array v2, v2, [Lcom/facebook/Request;

    const/4 v3, 0x0

    aput-object v1, v2, v3

    invoke-static {v2}, Lcom/facebook/Request;->b([Lcom/facebook/Request;)Lcom/facebook/bs;

    .line 779
    iput-object v0, p0, Lcom/facebook/widget/LoginButton;->e:Lcom/facebook/ca;

    .line 788
    :cond_0
    :goto_0
    return-void

    .line 782
    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/widget/LoginButton;->d:Lcom/facebook/c/j;

    .line 783
    iget-object v0, p0, Lcom/facebook/widget/LoginButton;->j:Lcom/facebook/widget/l;

    if-eqz v0, :cond_0

    .line 784
    iget-object v0, p0, Lcom/facebook/widget/LoginButton;->j:Lcom/facebook/widget/l;

    iget-object v1, p0, Lcom/facebook/widget/LoginButton;->d:Lcom/facebook/c/j;

    invoke-interface {v0, v1}, Lcom/facebook/widget/l;->a(Lcom/facebook/c/j;)V

    goto :goto_0
.end method

.method static synthetic g(Lcom/facebook/widget/LoginButton;)Lcom/facebook/widget/g;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/facebook/widget/LoginButton;->l:Lcom/facebook/widget/g;

    return-object v0
.end method

.method static synthetic h(Lcom/facebook/widget/LoginButton;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/facebook/widget/LoginButton;->m:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic i(Lcom/facebook/widget/LoginButton;)Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/facebook/widget/LoginButton;->n:Landroid/view/View$OnClickListener;

    return-object v0
.end method

.method static synthetic j(Lcom/facebook/widget/LoginButton;)V
    .locals 0

    .prologue
    .line 60
    invoke-direct {p0}, Lcom/facebook/widget/LoginButton;->f()V

    return-void
.end method

.method static synthetic k(Lcom/facebook/widget/LoginButton;)V
    .locals 0

    .prologue
    .line 60
    invoke-direct {p0}, Lcom/facebook/widget/LoginButton;->e()V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 550
    iget-object v0, p0, Lcom/facebook/widget/LoginButton;->s:Lcom/facebook/widget/m;

    if-eqz v0, :cond_0

    .line 551
    iget-object v0, p0, Lcom/facebook/widget/LoginButton;->s:Lcom/facebook/widget/m;

    invoke-virtual {v0}, Lcom/facebook/widget/m;->b()V

    .line 552
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/widget/LoginButton;->s:Lcom/facebook/widget/m;

    .line 554
    :cond_0
    return-void
.end method

.method a(Ljava/lang/Exception;)V
    .locals 2

    .prologue
    .line 893
    iget-object v0, p0, Lcom/facebook/widget/LoginButton;->l:Lcom/facebook/widget/g;

    invoke-static {v0}, Lcom/facebook/widget/g;->f(Lcom/facebook/widget/g;)Lcom/facebook/widget/j;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 894
    instance-of v0, p1, Lcom/facebook/aq;

    if-eqz v0, :cond_1

    .line 895
    iget-object v0, p0, Lcom/facebook/widget/LoginButton;->l:Lcom/facebook/widget/g;

    invoke-static {v0}, Lcom/facebook/widget/g;->f(Lcom/facebook/widget/g;)Lcom/facebook/widget/j;

    move-result-object v0

    check-cast p1, Lcom/facebook/aq;

    invoke-interface {v0, p1}, Lcom/facebook/widget/j;->a(Lcom/facebook/aq;)V

    .line 900
    :cond_0
    :goto_0
    return-void

    .line 897
    :cond_1
    iget-object v0, p0, Lcom/facebook/widget/LoginButton;->l:Lcom/facebook/widget/g;

    invoke-static {v0}, Lcom/facebook/widget/g;->f(Lcom/facebook/widget/g;)Lcom/facebook/widget/j;

    move-result-object v0

    new-instance v1, Lcom/facebook/aq;

    invoke-direct {v1, p1}, Lcom/facebook/aq;-><init>(Ljava/lang/Throwable;)V

    invoke-interface {v0, v1}, Lcom/facebook/widget/j;->a(Lcom/facebook/aq;)V

    goto :goto_0
.end method

.method public getDefaultAudience()Lcom/facebook/ct;
    .locals 1

    .prologue
    .line 313
    iget-object v0, p0, Lcom/facebook/widget/LoginButton;->l:Lcom/facebook/widget/g;

    invoke-virtual {v0}, Lcom/facebook/widget/g;->b()Lcom/facebook/ct;

    move-result-object v0

    return-object v0
.end method

.method public getLoginBehavior()Lcom/facebook/cu;
    .locals 1

    .prologue
    .line 450
    iget-object v0, p0, Lcom/facebook/widget/LoginButton;->l:Lcom/facebook/widget/g;

    invoke-virtual {v0}, Lcom/facebook/widget/g;->d()Lcom/facebook/cu;

    move-result-object v0

    return-object v0
.end method

.method public getOnErrorListener()Lcom/facebook/widget/j;
    .locals 1

    .prologue
    .line 291
    iget-object v0, p0, Lcom/facebook/widget/LoginButton;->l:Lcom/facebook/widget/g;

    invoke-virtual {v0}, Lcom/facebook/widget/g;->a()Lcom/facebook/widget/j;

    move-result-object v0

    return-object v0
.end method

.method getPermissions()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 711
    iget-object v0, p0, Lcom/facebook/widget/LoginButton;->l:Lcom/facebook/widget/g;

    invoke-virtual {v0}, Lcom/facebook/widget/g;->c()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getSessionStatusCallback()Lcom/facebook/cq;
    .locals 1

    .prologue
    .line 497
    iget-object v0, p0, Lcom/facebook/widget/LoginButton;->l:Lcom/facebook/widget/g;

    invoke-virtual {v0}, Lcom/facebook/widget/g;->e()Lcom/facebook/cq;

    move-result-object v0

    return-object v0
.end method

.method public getToolTipDisplayTime()J
    .locals 2

    .prologue
    .line 543
    iget-wide v0, p0, Lcom/facebook/widget/LoginButton;->r:J

    return-wide v0
.end method

.method public getToolTipMode()Lcom/facebook/widget/k;
    .locals 1

    .prologue
    .line 524
    iget-object v0, p0, Lcom/facebook/widget/LoginButton;->q:Lcom/facebook/widget/k;

    return-object v0
.end method

.method public getUserInfoChangedCallback()Lcom/facebook/widget/l;
    .locals 1

    .prologue
    .line 467
    iget-object v0, p0, Lcom/facebook/widget/LoginButton;->j:Lcom/facebook/widget/l;

    return-object v0
.end method

.method protected onAttachedToWindow()V
    .locals 1

    .prologue
    .line 635
    invoke-super {p0}, Landroid/widget/Button;->onAttachedToWindow()V

    .line 636
    iget-object v0, p0, Lcom/facebook/widget/LoginButton;->c:Lcom/facebook/b/bk;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/widget/LoginButton;->c:Lcom/facebook/b/bk;

    invoke-virtual {v0}, Lcom/facebook/b/bk;->e()Z

    move-result v0

    if-nez v0, :cond_0

    .line 637
    iget-object v0, p0, Lcom/facebook/widget/LoginButton;->c:Lcom/facebook/b/bk;

    invoke-virtual {v0}, Lcom/facebook/b/bk;->c()V

    .line 638
    invoke-direct {p0}, Lcom/facebook/widget/LoginButton;->f()V

    .line 639
    invoke-direct {p0}, Lcom/facebook/widget/LoginButton;->e()V

    .line 641
    :cond_0
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .prologue
    .line 693
    invoke-super {p0}, Landroid/widget/Button;->onDetachedFromWindow()V

    .line 694
    iget-object v0, p0, Lcom/facebook/widget/LoginButton;->c:Lcom/facebook/b/bk;

    if-eqz v0, :cond_0

    .line 695
    iget-object v0, p0, Lcom/facebook/widget/LoginButton;->c:Lcom/facebook/b/bk;

    invoke-virtual {v0}, Lcom/facebook/b/bk;->d()V

    .line 697
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/widget/LoginButton;->a()V

    .line 698
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 2

    .prologue
    .line 645
    invoke-super {p0, p1}, Landroid/widget/Button;->onDraw(Landroid/graphics/Canvas;)V

    .line 647
    iget-boolean v0, p0, Lcom/facebook/widget/LoginButton;->o:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/widget/LoginButton;->q:Lcom/facebook/widget/k;

    sget-object v1, Lcom/facebook/widget/k;->c:Lcom/facebook/widget/k;

    if-eq v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/facebook/widget/LoginButton;->isInEditMode()Z

    move-result v0

    if-nez v0, :cond_0

    .line 648
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/widget/LoginButton;->o:Z

    .line 649
    invoke-direct {p0}, Lcom/facebook/widget/LoginButton;->d()V

    .line 651
    :cond_0
    return-void
.end method

.method public onFinishInflate()V
    .locals 0

    .prologue
    .line 608
    invoke-super {p0}, Landroid/widget/Button;->onFinishInflate()V

    .line 609
    invoke-direct {p0}, Lcom/facebook/widget/LoginButton;->c()V

    .line 610
    return-void
.end method

.method protected onVisibilityChanged(Landroid/view/View;I)V
    .locals 0

    .prologue
    .line 702
    invoke-super {p0, p1, p2}, Landroid/widget/Button;->onVisibilityChanged(Landroid/view/View;I)V

    .line 704
    if-eqz p2, :cond_0

    .line 705
    invoke-virtual {p0}, Lcom/facebook/widget/LoginButton;->a()V

    .line 707
    :cond_0
    return-void
.end method

.method public setApplicationId(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 459
    iput-object p1, p0, Lcom/facebook/widget/LoginButton;->b:Ljava/lang/String;

    .line 460
    return-void
.end method

.method public setDefaultAudience(Lcom/facebook/ct;)V
    .locals 1

    .prologue
    .line 302
    iget-object v0, p0, Lcom/facebook/widget/LoginButton;->l:Lcom/facebook/widget/g;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/g;->a(Lcom/facebook/ct;)V

    .line 303
    return-void
.end method

.method public setFragment(Landroid/support/v4/app/Fragment;)V
    .locals 0

    .prologue
    .line 630
    iput-object p1, p0, Lcom/facebook/widget/LoginButton;->k:Landroid/support/v4/app/Fragment;

    .line 631
    return-void
.end method

.method public setLoginBehavior(Lcom/facebook/cu;)V
    .locals 1

    .prologue
    .line 437
    iget-object v0, p0, Lcom/facebook/widget/LoginButton;->l:Lcom/facebook/widget/g;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/g;->a(Lcom/facebook/cu;)V

    .line 438
    return-void
.end method

.method setLoginLogoutEventName(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 719
    iput-object p1, p0, Lcom/facebook/widget/LoginButton;->m:Ljava/lang/String;

    .line 720
    return-void
.end method

.method public setOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 0

    .prologue
    .line 797
    iput-object p1, p0, Lcom/facebook/widget/LoginButton;->n:Landroid/view/View$OnClickListener;

    .line 798
    return-void
.end method

.method public setOnErrorListener(Lcom/facebook/widget/j;)V
    .locals 1

    .prologue
    .line 282
    iget-object v0, p0, Lcom/facebook/widget/LoginButton;->l:Lcom/facebook/widget/g;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/g;->a(Lcom/facebook/widget/j;)V

    .line 283
    return-void
.end method

.method setProperties(Lcom/facebook/widget/g;)V
    .locals 0

    .prologue
    .line 715
    iput-object p1, p0, Lcom/facebook/widget/LoginButton;->l:Lcom/facebook/widget/g;

    .line 716
    return-void
.end method

.method public setPublishPermissions(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 390
    iget-object v0, p0, Lcom/facebook/widget/LoginButton;->l:Lcom/facebook/widget/g;

    iget-object v1, p0, Lcom/facebook/widget/LoginButton;->c:Lcom/facebook/b/bk;

    invoke-virtual {v1}, Lcom/facebook/b/bk;->a()Lcom/facebook/ca;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/facebook/widget/g;->b(Ljava/util/List;Lcom/facebook/ca;)V

    .line 391
    return-void
.end method

.method public varargs setPublishPermissions([Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 416
    iget-object v0, p0, Lcom/facebook/widget/LoginButton;->l:Lcom/facebook/widget/g;

    invoke-static {p1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/widget/LoginButton;->c:Lcom/facebook/b/bk;

    invoke-virtual {v2}, Lcom/facebook/b/bk;->a()Lcom/facebook/ca;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/widget/g;->b(Ljava/util/List;Lcom/facebook/ca;)V

    .line 417
    return-void
.end method

.method public setReadPermissions(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 338
    iget-object v0, p0, Lcom/facebook/widget/LoginButton;->l:Lcom/facebook/widget/g;

    iget-object v1, p0, Lcom/facebook/widget/LoginButton;->c:Lcom/facebook/b/bk;

    invoke-virtual {v1}, Lcom/facebook/b/bk;->a()Lcom/facebook/ca;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/facebook/widget/g;->a(Ljava/util/List;Lcom/facebook/ca;)V

    .line 339
    return-void
.end method

.method public varargs setReadPermissions([Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 363
    iget-object v0, p0, Lcom/facebook/widget/LoginButton;->l:Lcom/facebook/widget/g;

    invoke-static {p1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/widget/LoginButton;->c:Lcom/facebook/b/bk;

    invoke-virtual {v2}, Lcom/facebook/b/bk;->a()Lcom/facebook/ca;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/widget/g;->a(Ljava/util/List;Lcom/facebook/ca;)V

    .line 364
    return-void
.end method

.method public setSession(Lcom/facebook/ca;)V
    .locals 1

    .prologue
    .line 601
    iget-object v0, p0, Lcom/facebook/widget/LoginButton;->c:Lcom/facebook/b/bk;

    invoke-virtual {v0, p1}, Lcom/facebook/b/bk;->a(Lcom/facebook/ca;)V

    .line 602
    invoke-direct {p0}, Lcom/facebook/widget/LoginButton;->f()V

    .line 603
    invoke-direct {p0}, Lcom/facebook/widget/LoginButton;->e()V

    .line 604
    return-void
.end method

.method public setSessionStatusCallback(Lcom/facebook/cq;)V
    .locals 1

    .prologue
    .line 487
    iget-object v0, p0, Lcom/facebook/widget/LoginButton;->l:Lcom/facebook/widget/g;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/g;->a(Lcom/facebook/cq;)V

    .line 488
    return-void
.end method

.method public setToolTipDisplayTime(J)V
    .locals 1

    .prologue
    .line 535
    iput-wide p1, p0, Lcom/facebook/widget/LoginButton;->r:J

    .line 536
    return-void
.end method

.method public setToolTipMode(Lcom/facebook/widget/k;)V
    .locals 0

    .prologue
    .line 516
    iput-object p1, p0, Lcom/facebook/widget/LoginButton;->q:Lcom/facebook/widget/k;

    .line 517
    return-void
.end method

.method public setToolTipStyle(Lcom/facebook/widget/r;)V
    .locals 0

    .prologue
    .line 506
    iput-object p1, p0, Lcom/facebook/widget/LoginButton;->p:Lcom/facebook/widget/r;

    .line 507
    return-void
.end method

.method public setUserInfoChangedCallback(Lcom/facebook/widget/l;)V
    .locals 0

    .prologue
    .line 476
    iput-object p1, p0, Lcom/facebook/widget/LoginButton;->j:Lcom/facebook/widget/l;

    .line 477
    return-void
.end method
