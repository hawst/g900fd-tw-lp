.class public final enum Lcom/facebook/widget/r;
.super Ljava/lang/Enum;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/widget/r;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/facebook/widget/r;

.field public static final enum b:Lcom/facebook/widget/r;

.field private static final synthetic c:[Lcom/facebook/widget/r;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 40
    new-instance v0, Lcom/facebook/widget/r;

    const-string/jumbo v1, "BLUE"

    invoke-direct {v0, v1, v2}, Lcom/facebook/widget/r;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/widget/r;->a:Lcom/facebook/widget/r;

    .line 46
    new-instance v0, Lcom/facebook/widget/r;

    const-string/jumbo v1, "BLACK"

    invoke-direct {v0, v1, v3}, Lcom/facebook/widget/r;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/widget/r;->b:Lcom/facebook/widget/r;

    .line 35
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/facebook/widget/r;

    sget-object v1, Lcom/facebook/widget/r;->a:Lcom/facebook/widget/r;

    aput-object v1, v0, v2

    sget-object v1, Lcom/facebook/widget/r;->b:Lcom/facebook/widget/r;

    aput-object v1, v0, v3

    sput-object v0, Lcom/facebook/widget/r;->c:[Lcom/facebook/widget/r;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 35
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/widget/r;
    .locals 1

    .prologue
    .line 35
    const-class v0, Lcom/facebook/widget/r;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/r;

    return-object v0
.end method

.method public static values()[Lcom/facebook/widget/r;
    .locals 1

    .prologue
    .line 35
    sget-object v0, Lcom/facebook/widget/r;->c:[Lcom/facebook/widget/r;

    invoke-virtual {v0}, [Lcom/facebook/widget/r;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/widget/r;

    return-object v0
.end method
