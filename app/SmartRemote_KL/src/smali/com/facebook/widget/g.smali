.class Lcom/facebook/widget/g;
.super Ljava/lang/Object;


# instance fields
.field private a:Lcom/facebook/ct;

.field private b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private c:Lcom/facebook/b/bj;

.field private d:Lcom/facebook/widget/j;

.field private e:Lcom/facebook/cu;

.field private f:Lcom/facebook/cq;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 100
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 101
    sget-object v0, Lcom/facebook/ct;->c:Lcom/facebook/ct;

    iput-object v0, p0, Lcom/facebook/widget/g;->a:Lcom/facebook/ct;

    .line 102
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/widget/g;->b:Ljava/util/List;

    .line 103
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/widget/g;->c:Lcom/facebook/b/bj;

    .line 105
    sget-object v0, Lcom/facebook/cu;->a:Lcom/facebook/cu;

    iput-object v0, p0, Lcom/facebook/widget/g;->e:Lcom/facebook/cu;

    return-void
.end method

.method static synthetic a(Lcom/facebook/widget/g;)Lcom/facebook/ct;
    .locals 1

    .prologue
    .line 100
    iget-object v0, p0, Lcom/facebook/widget/g;->a:Lcom/facebook/ct;

    return-object v0
.end method

.method private a(Ljava/util/List;Lcom/facebook/b/bj;Lcom/facebook/ca;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/facebook/b/bj;",
            "Lcom/facebook/ca;",
            ")Z"
        }
    .end annotation

    .prologue
    .line 148
    sget-object v0, Lcom/facebook/b/bj;->b:Lcom/facebook/b/bj;

    invoke-virtual {v0, p2}, Lcom/facebook/b/bj;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 149
    invoke-static {p1}, Lcom/facebook/b/bp;->a(Ljava/util/Collection;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 150
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "Permissions for publish actions cannot be null or empty."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 153
    :cond_0
    if-eqz p3, :cond_1

    invoke-virtual {p3}, Lcom/facebook/ca;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 154
    invoke-virtual {p3}, Lcom/facebook/ca;->g()Ljava/util/List;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/b/bp;->a(Ljava/util/Collection;Ljava/util/Collection;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 155
    invoke-static {}, Lcom/facebook/widget/LoginButton;->b()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "Cannot set additional permissions when session is already open."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 156
    const/4 v0, 0x0

    .line 159
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method static synthetic b(Lcom/facebook/widget/g;)Ljava/util/List;
    .locals 1

    .prologue
    .line 100
    iget-object v0, p0, Lcom/facebook/widget/g;->b:Ljava/util/List;

    return-object v0
.end method

.method static synthetic c(Lcom/facebook/widget/g;)Lcom/facebook/cu;
    .locals 1

    .prologue
    .line 100
    iget-object v0, p0, Lcom/facebook/widget/g;->e:Lcom/facebook/cu;

    return-object v0
.end method

.method static synthetic d(Lcom/facebook/widget/g;)Lcom/facebook/b/bj;
    .locals 1

    .prologue
    .line 100
    iget-object v0, p0, Lcom/facebook/widget/g;->c:Lcom/facebook/b/bj;

    return-object v0
.end method

.method static synthetic e(Lcom/facebook/widget/g;)Lcom/facebook/cq;
    .locals 1

    .prologue
    .line 100
    iget-object v0, p0, Lcom/facebook/widget/g;->f:Lcom/facebook/cq;

    return-object v0
.end method

.method static synthetic f(Lcom/facebook/widget/g;)Lcom/facebook/widget/j;
    .locals 1

    .prologue
    .line 100
    iget-object v0, p0, Lcom/facebook/widget/g;->d:Lcom/facebook/widget/j;

    return-object v0
.end method


# virtual methods
.method public a()Lcom/facebook/widget/j;
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lcom/facebook/widget/g;->d:Lcom/facebook/widget/j;

    return-object v0
.end method

.method public a(Lcom/facebook/cq;)V
    .locals 0

    .prologue
    .line 180
    iput-object p1, p0, Lcom/facebook/widget/g;->f:Lcom/facebook/cq;

    .line 181
    return-void
.end method

.method public a(Lcom/facebook/ct;)V
    .locals 0

    .prologue
    .line 117
    iput-object p1, p0, Lcom/facebook/widget/g;->a:Lcom/facebook/ct;

    .line 118
    return-void
.end method

.method public a(Lcom/facebook/cu;)V
    .locals 0

    .prologue
    .line 172
    iput-object p1, p0, Lcom/facebook/widget/g;->e:Lcom/facebook/cu;

    .line 173
    return-void
.end method

.method public a(Lcom/facebook/widget/j;)V
    .locals 0

    .prologue
    .line 109
    iput-object p1, p0, Lcom/facebook/widget/g;->d:Lcom/facebook/widget/j;

    .line 110
    return-void
.end method

.method public a(Ljava/util/List;Lcom/facebook/ca;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/facebook/ca;",
            ")V"
        }
    .end annotation

    .prologue
    .line 125
    sget-object v0, Lcom/facebook/b/bj;->b:Lcom/facebook/b/bj;

    iget-object v1, p0, Lcom/facebook/widget/g;->c:Lcom/facebook/b/bj;

    invoke-virtual {v0, v1}, Lcom/facebook/b/bj;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 126
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string/jumbo v1, "Cannot call setReadPermissions after setPublishPermissions has been called."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 129
    :cond_0
    sget-object v0, Lcom/facebook/b/bj;->a:Lcom/facebook/b/bj;

    invoke-direct {p0, p1, v0, p2}, Lcom/facebook/widget/g;->a(Ljava/util/List;Lcom/facebook/b/bj;Lcom/facebook/ca;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 130
    iput-object p1, p0, Lcom/facebook/widget/g;->b:Ljava/util/List;

    .line 131
    sget-object v0, Lcom/facebook/b/bj;->a:Lcom/facebook/b/bj;

    iput-object v0, p0, Lcom/facebook/widget/g;->c:Lcom/facebook/b/bj;

    .line 133
    :cond_1
    return-void
.end method

.method public b()Lcom/facebook/ct;
    .locals 1

    .prologue
    .line 121
    iget-object v0, p0, Lcom/facebook/widget/g;->a:Lcom/facebook/ct;

    return-object v0
.end method

.method public b(Ljava/util/List;Lcom/facebook/ca;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/facebook/ca;",
            ")V"
        }
    .end annotation

    .prologue
    .line 136
    sget-object v0, Lcom/facebook/b/bj;->a:Lcom/facebook/b/bj;

    iget-object v1, p0, Lcom/facebook/widget/g;->c:Lcom/facebook/b/bj;

    invoke-virtual {v0, v1}, Lcom/facebook/b/bj;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 137
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string/jumbo v1, "Cannot call setPublishPermissions after setReadPermissions has been called."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 140
    :cond_0
    sget-object v0, Lcom/facebook/b/bj;->b:Lcom/facebook/b/bj;

    invoke-direct {p0, p1, v0, p2}, Lcom/facebook/widget/g;->a(Ljava/util/List;Lcom/facebook/b/bj;Lcom/facebook/ca;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 141
    iput-object p1, p0, Lcom/facebook/widget/g;->b:Ljava/util/List;

    .line 142
    sget-object v0, Lcom/facebook/b/bj;->b:Lcom/facebook/b/bj;

    iput-object v0, p0, Lcom/facebook/widget/g;->c:Lcom/facebook/b/bj;

    .line 144
    :cond_1
    return-void
.end method

.method c()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 163
    iget-object v0, p0, Lcom/facebook/widget/g;->b:Ljava/util/List;

    return-object v0
.end method

.method public d()Lcom/facebook/cu;
    .locals 1

    .prologue
    .line 176
    iget-object v0, p0, Lcom/facebook/widget/g;->e:Lcom/facebook/cu;

    return-object v0
.end method

.method public e()Lcom/facebook/cq;
    .locals 1

    .prologue
    .line 184
    iget-object v0, p0, Lcom/facebook/widget/g;->f:Lcom/facebook/cq;

    return-object v0
.end method
