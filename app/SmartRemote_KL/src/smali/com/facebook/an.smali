.class public Lcom/facebook/an;
.super Landroid/content/BroadcastReceiver;


# static fields
.field private static a:Lcom/facebook/an;


# instance fields
.field private b:Landroid/content/Context;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 20
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 21
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/an;->b:Landroid/content/Context;

    .line 22
    return-void
.end method

.method static a(Landroid/content/Context;)Lcom/facebook/an;
    .locals 1

    .prologue
    .line 35
    sget-object v0, Lcom/facebook/an;->a:Lcom/facebook/an;

    if-eqz v0, :cond_0

    .line 36
    sget-object v0, Lcom/facebook/an;->a:Lcom/facebook/an;

    .line 40
    :goto_0
    return-object v0

    .line 38
    :cond_0
    new-instance v0, Lcom/facebook/an;

    invoke-direct {v0, p0}, Lcom/facebook/an;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/facebook/an;->a:Lcom/facebook/an;

    .line 39
    sget-object v0, Lcom/facebook/an;->a:Lcom/facebook/an;

    invoke-direct {v0}, Lcom/facebook/an;->a()V

    .line 40
    sget-object v0, Lcom/facebook/an;->a:Lcom/facebook/an;

    goto :goto_0
.end method

.method private a()V
    .locals 3

    .prologue
    .line 25
    iget-object v0, p0, Lcom/facebook/an;->b:Landroid/content/Context;

    invoke-static {v0}, Landroid/support/v4/a/q;->a(Landroid/content/Context;)Landroid/support/v4/a/q;

    move-result-object v0

    .line 26
    new-instance v1, Landroid/content/IntentFilter;

    const-string/jumbo v2, "com.parse.bolts.measurement_event"

    invoke-direct {v1, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0, v1}, Landroid/support/v4/a/q;->a(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)V

    .line 27
    return-void
.end method

.method private b()V
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/facebook/an;->b:Landroid/content/Context;

    invoke-static {v0}, Landroid/support/v4/a/q;->a(Landroid/content/Context;)Landroid/support/v4/a/q;

    move-result-object v0

    .line 31
    invoke-virtual {v0, p0}, Landroid/support/v4/a/q;->a(Landroid/content/BroadcastReceiver;)V

    .line 32
    return-void
.end method


# virtual methods
.method protected finalize()V
    .locals 1

    .prologue
    .line 45
    :try_start_0
    invoke-direct {p0}, Lcom/facebook/an;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 47
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    .line 49
    return-void

    .line 47
    :catchall_0
    move-exception v0

    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    throw v0
.end method

.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 9

    .prologue
    .line 53
    invoke-static {p1}, Lcom/facebook/c;->a(Landroid/content/Context;)Lcom/facebook/c;

    move-result-object v1

    .line 54
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "bf_"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v2, "event_name"

    invoke-virtual {p2, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 55
    const-string/jumbo v0, "event_args"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getBundleExtra(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v3

    .line 56
    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    .line 57
    invoke-virtual {v3}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 58
    const-string/jumbo v6, "[^0-9a-zA-Z _-]"

    const-string/jumbo v7, "-"

    invoke-virtual {v0, v6, v7}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    const-string/jumbo v7, "^[ -]*"

    const-string/jumbo v8, ""

    invoke-virtual {v6, v7, v8}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    const-string/jumbo v7, "[ -]*$"

    const-string/jumbo v8, ""

    invoke-virtual {v6, v7, v8}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 59
    invoke-virtual {v3, v0}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v4, v6, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 61
    :cond_0
    invoke-virtual {v1, v2, v4}, Lcom/facebook/c;->a(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 62
    return-void
.end method
