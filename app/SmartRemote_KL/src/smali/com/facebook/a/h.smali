.class public final Lcom/facebook/a/h;
.super Ljava/lang/Object;


# static fields
.field public static final com_facebook_friend_picker_fragment:[I

.field public static final com_facebook_friend_picker_fragment_multi_select:I = 0x0

.field public static final com_facebook_like_view:[I

.field public static final com_facebook_like_view_auxiliary_view_position:I = 0x3

.field public static final com_facebook_like_view_foreground_color:I = 0x0

.field public static final com_facebook_like_view_horizontal_alignment:I = 0x4

.field public static final com_facebook_like_view_object_id:I = 0x1

.field public static final com_facebook_like_view_style:I = 0x2

.field public static final com_facebook_login_view:[I

.field public static final com_facebook_login_view_confirm_logout:I = 0x0

.field public static final com_facebook_login_view_fetch_user_info:I = 0x1

.field public static final com_facebook_login_view_login_text:I = 0x2

.field public static final com_facebook_login_view_logout_text:I = 0x3

.field public static final com_facebook_picker_fragment:[I

.field public static final com_facebook_picker_fragment_done_button_background:I = 0x6

.field public static final com_facebook_picker_fragment_done_button_text:I = 0x4

.field public static final com_facebook_picker_fragment_extra_fields:I = 0x1

.field public static final com_facebook_picker_fragment_show_pictures:I = 0x0

.field public static final com_facebook_picker_fragment_show_title_bar:I = 0x2

.field public static final com_facebook_picker_fragment_title_bar_background:I = 0x5

.field public static final com_facebook_picker_fragment_title_text:I = 0x3

.field public static final com_facebook_place_picker_fragment:[I

.field public static final com_facebook_place_picker_fragment_radius_in_meters:I = 0x0

.field public static final com_facebook_place_picker_fragment_results_limit:I = 0x1

.field public static final com_facebook_place_picker_fragment_search_text:I = 0x2

.field public static final com_facebook_place_picker_fragment_show_search_box:I = 0x3

.field public static final com_facebook_profile_picture_view:[I

.field public static final com_facebook_profile_picture_view_is_cropped:I = 0x1

.field public static final com_facebook_profile_picture_view_preset_size:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x4

    .line 222
    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v1, 0x0

    const v2, 0x7f010103

    aput v2, v0, v1

    sput-object v0, Lcom/facebook/a/h;->com_facebook_friend_picker_fragment:[I

    .line 224
    const/4 v0, 0x5

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/facebook/a/h;->com_facebook_like_view:[I

    .line 230
    new-array v0, v3, [I

    fill-array-data v0, :array_1

    sput-object v0, Lcom/facebook/a/h;->com_facebook_login_view:[I

    .line 235
    const/4 v0, 0x7

    new-array v0, v0, [I

    fill-array-data v0, :array_2

    sput-object v0, Lcom/facebook/a/h;->com_facebook_picker_fragment:[I

    .line 243
    new-array v0, v3, [I

    fill-array-data v0, :array_3

    sput-object v0, Lcom/facebook/a/h;->com_facebook_place_picker_fragment:[I

    .line 248
    const/4 v0, 0x2

    new-array v0, v0, [I

    fill-array-data v0, :array_4

    sput-object v0, Lcom/facebook/a/h;->com_facebook_profile_picture_view:[I

    return-void

    .line 224
    nop

    :array_0
    .array-data 4
        0x7f010104
        0x7f010105
        0x7f010106
        0x7f010107
        0x7f010108
    .end array-data

    .line 230
    :array_1
    .array-data 4
        0x7f010109
        0x7f01010a
        0x7f01010b
        0x7f01010c
    .end array-data

    .line 235
    :array_2
    .array-data 4
        0x7f01010d
        0x7f01010e
        0x7f01010f
        0x7f010110
        0x7f010111
        0x7f010112
        0x7f010113
    .end array-data

    .line 243
    :array_3
    .array-data 4
        0x7f010114
        0x7f010115
        0x7f010116
        0x7f010117
    .end array-data

    .line 248
    :array_4
    .array-data 4
        0x7f010118
        0x7f010119
    .end array-data
.end method
