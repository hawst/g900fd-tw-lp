.class public final Lcom/facebook/a/d;
.super Ljava/lang/Object;


# static fields
.field public static final com_facebook_button_blue:I = 0x7f0201eb

.field public static final com_facebook_button_blue_focused:I = 0x7f0201ec

.field public static final com_facebook_button_blue_normal:I = 0x7f0201ed

.field public static final com_facebook_button_blue_pressed:I = 0x7f0201ee

.field public static final com_facebook_button_check:I = 0x7f0201ef

.field public static final com_facebook_button_check_off:I = 0x7f0201f0

.field public static final com_facebook_button_check_on:I = 0x7f0201f1

.field public static final com_facebook_button_grey_focused:I = 0x7f0201f2

.field public static final com_facebook_button_grey_normal:I = 0x7f0201f3

.field public static final com_facebook_button_grey_pressed:I = 0x7f0201f4

.field public static final com_facebook_button_like:I = 0x7f0201f5

.field public static final com_facebook_button_like_background:I = 0x7f0201f6

.field public static final com_facebook_button_like_background_selected:I = 0x7f0201f7

.field public static final com_facebook_button_like_icon:I = 0x7f0201f8

.field public static final com_facebook_button_like_icon_selected:I = 0x7f0201f9

.field public static final com_facebook_button_like_pressed:I = 0x7f0201fa

.field public static final com_facebook_button_like_selected:I = 0x7f0201fb

.field public static final com_facebook_close:I = 0x7f0201fc

.field public static final com_facebook_inverse_icon:I = 0x7f0201fd

.field public static final com_facebook_list_divider:I = 0x7f0201fe

.field public static final com_facebook_list_section_header_background:I = 0x7f0201ff

.field public static final com_facebook_loginbutton_silver:I = 0x7f020200

.field public static final com_facebook_logo:I = 0x7f020201

.field public static final com_facebook_picker_default_separator_color:I = 0x7f020739

.field public static final com_facebook_picker_item_background:I = 0x7f020202

.field public static final com_facebook_picker_list_focused:I = 0x7f020203

.field public static final com_facebook_picker_list_longpressed:I = 0x7f020204

.field public static final com_facebook_picker_list_pressed:I = 0x7f020205

.field public static final com_facebook_picker_list_selector:I = 0x7f020206

.field public static final com_facebook_picker_list_selector_background_transition:I = 0x7f020207

.field public static final com_facebook_picker_list_selector_disabled:I = 0x7f020208

.field public static final com_facebook_picker_magnifier:I = 0x7f020209

.field public static final com_facebook_picker_top_button:I = 0x7f02020a

.field public static final com_facebook_place_default_icon:I = 0x7f02020b

.field public static final com_facebook_profile_default_icon:I = 0x7f02020c

.field public static final com_facebook_profile_picture_blank_portrait:I = 0x7f02020d

.field public static final com_facebook_profile_picture_blank_square:I = 0x7f02020e

.field public static final com_facebook_tooltip_black_background:I = 0x7f02020f

.field public static final com_facebook_tooltip_black_bottomnub:I = 0x7f020210

.field public static final com_facebook_tooltip_black_topnub:I = 0x7f020211

.field public static final com_facebook_tooltip_black_xout:I = 0x7f020212

.field public static final com_facebook_tooltip_blue_background:I = 0x7f020213

.field public static final com_facebook_tooltip_blue_bottomnub:I = 0x7f020214

.field public static final com_facebook_tooltip_blue_topnub:I = 0x7f020215

.field public static final com_facebook_tooltip_blue_xout:I = 0x7f020216

.field public static final com_facebook_top_background:I = 0x7f020217

.field public static final com_facebook_top_button:I = 0x7f020218

.field public static final com_facebook_usersettingsfragment_background_gradient:I = 0x7f020219
