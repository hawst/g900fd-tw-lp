.class public final Lcom/facebook/be;
.super Ljava/lang/Object;


# static fields
.field private static final a:Ljava/lang/String;

.field private static b:Ljava/io/File;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 45
    const-class v0, Lcom/facebook/be;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/be;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 154
    return-void
.end method


# virtual methods
.method a(Ljava/util/UUID;Z)Ljava/io/File;
    .locals 3

    .prologue
    .line 199
    sget-object v0, Lcom/facebook/be;->b:Ljava/io/File;

    if-nez v0, :cond_1

    .line 200
    const/4 v0, 0x0

    .line 207
    :cond_0
    :goto_0
    return-object v0

    .line 203
    :cond_1
    new-instance v0, Ljava/io/File;

    sget-object v1, Lcom/facebook/be;->b:Ljava/io/File;

    invoke-virtual {p1}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 204
    if-eqz p2, :cond_0

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_0

    .line 205
    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    goto :goto_0
.end method

.method public a(Landroid/content/Context;Ljava/util/UUID;)V
    .locals 1

    .prologue
    .line 165
    const/4 v0, 0x0

    invoke-virtual {p0, p2, v0}, Lcom/facebook/be;->a(Ljava/util/UUID;Z)Ljava/io/File;

    move-result-object v0

    .line 166
    invoke-static {v0}, Lcom/facebook/b/bp;->a(Ljava/io/File;)V

    .line 167
    return-void
.end method
