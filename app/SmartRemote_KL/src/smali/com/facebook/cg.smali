.class public Lcom/facebook/cg;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private final a:Lcom/facebook/cp;

.field private b:Lcom/facebook/cu;

.field private c:I

.field private d:Lcom/facebook/cq;

.field private e:Z

.field private f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private g:Lcom/facebook/ct;

.field private h:Ljava/lang/String;

.field private i:Ljava/lang/String;

.field private final j:Ljava/lang/String;

.field private final k:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Landroid/app/Activity;)V
    .locals 1

    .prologue
    .line 1898
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1887
    sget-object v0, Lcom/facebook/cu;->a:Lcom/facebook/cu;

    iput-object v0, p0, Lcom/facebook/cg;->b:Lcom/facebook/cu;

    .line 1888
    const v0, 0xface

    iput v0, p0, Lcom/facebook/cg;->c:I

    .line 1890
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/cg;->e:Z

    .line 1891
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/cg;->f:Ljava/util/List;

    .line 1892
    sget-object v0, Lcom/facebook/ct;->c:Lcom/facebook/ct;

    iput-object v0, p0, Lcom/facebook/cg;->g:Lcom/facebook/ct;

    .line 1895
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/cg;->j:Ljava/lang/String;

    .line 1896
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/facebook/cg;->k:Ljava/util/Map;

    .line 1899
    new-instance v0, Lcom/facebook/ch;

    invoke-direct {v0, p0, p1}, Lcom/facebook/ch;-><init>(Lcom/facebook/cg;Landroid/app/Activity;)V

    iput-object v0, p0, Lcom/facebook/cg;->a:Lcom/facebook/cp;

    .line 1910
    return-void
.end method

.method constructor <init>(Landroid/support/v4/app/Fragment;)V
    .locals 1

    .prologue
    .line 1912
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1887
    sget-object v0, Lcom/facebook/cu;->a:Lcom/facebook/cu;

    iput-object v0, p0, Lcom/facebook/cg;->b:Lcom/facebook/cu;

    .line 1888
    const v0, 0xface

    iput v0, p0, Lcom/facebook/cg;->c:I

    .line 1890
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/cg;->e:Z

    .line 1891
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/cg;->f:Ljava/util/List;

    .line 1892
    sget-object v0, Lcom/facebook/ct;->c:Lcom/facebook/ct;

    iput-object v0, p0, Lcom/facebook/cg;->g:Lcom/facebook/ct;

    .line 1895
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/cg;->j:Ljava/lang/String;

    .line 1896
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/facebook/cg;->k:Ljava/util/Map;

    .line 1913
    new-instance v0, Lcom/facebook/ci;

    invoke-direct {v0, p0, p1}, Lcom/facebook/ci;-><init>(Lcom/facebook/cg;Landroid/support/v4/app/Fragment;)V

    iput-object v0, p0, Lcom/facebook/cg;->a:Lcom/facebook/cp;

    .line 1924
    return-void
.end method

.method static synthetic a(Lcom/facebook/cg;)Ljava/util/Map;
    .locals 1

    .prologue
    .line 1882
    iget-object v0, p0, Lcom/facebook/cg;->k:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic b(Lcom/facebook/cg;)Z
    .locals 1

    .prologue
    .line 1882
    iget-boolean v0, p0, Lcom/facebook/cg;->e:Z

    return v0
.end method

.method static synthetic c(Lcom/facebook/cg;)Lcom/facebook/cu;
    .locals 1

    .prologue
    .line 1882
    iget-object v0, p0, Lcom/facebook/cg;->b:Lcom/facebook/cu;

    return-object v0
.end method

.method static synthetic d(Lcom/facebook/cg;)I
    .locals 1

    .prologue
    .line 1882
    iget v0, p0, Lcom/facebook/cg;->c:I

    return v0
.end method

.method static synthetic e(Lcom/facebook/cg;)Ljava/util/List;
    .locals 1

    .prologue
    .line 1882
    iget-object v0, p0, Lcom/facebook/cg;->f:Ljava/util/List;

    return-object v0
.end method

.method static synthetic f(Lcom/facebook/cg;)Lcom/facebook/ct;
    .locals 1

    .prologue
    .line 1882
    iget-object v0, p0, Lcom/facebook/cg;->g:Lcom/facebook/ct;

    return-object v0
.end method

.method static synthetic g(Lcom/facebook/cg;)Lcom/facebook/cp;
    .locals 1

    .prologue
    .line 1882
    iget-object v0, p0, Lcom/facebook/cg;->a:Lcom/facebook/cp;

    return-object v0
.end method


# virtual methods
.method a(Lcom/facebook/ct;)Lcom/facebook/cg;
    .locals 0

    .prologue
    .line 2014
    if-eqz p1, :cond_0

    .line 2015
    iput-object p1, p0, Lcom/facebook/cg;->g:Lcom/facebook/ct;

    .line 2017
    :cond_0
    return-object p0
.end method

.method a(Lcom/facebook/cu;)Lcom/facebook/cg;
    .locals 0

    .prologue
    .line 1977
    if-eqz p1, :cond_0

    .line 1978
    iput-object p1, p0, Lcom/facebook/cg;->b:Lcom/facebook/cu;

    .line 1980
    :cond_0
    return-object p0
.end method

.method a(Ljava/util/List;)Lcom/facebook/cg;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/facebook/cg;"
        }
    .end annotation

    .prologue
    .line 1999
    if-eqz p1, :cond_0

    .line 2000
    iput-object p1, p0, Lcom/facebook/cg;->f:Ljava/util/List;

    .line 2002
    :cond_0
    return-object p0
.end method

.method a()Lcom/facebook/cq;
    .locals 1

    .prologue
    .line 1973
    iget-object v0, p0, Lcom/facebook/cg;->d:Lcom/facebook/cq;

    return-object v0
.end method

.method a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2033
    iput-object p1, p0, Lcom/facebook/cg;->h:Ljava/lang/String;

    .line 2034
    return-void
.end method

.method b()Lcom/facebook/cu;
    .locals 1

    .prologue
    .line 1984
    iget-object v0, p0, Lcom/facebook/cg;->b:Lcom/facebook/cu;

    return-object v0
.end method

.method b(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2041
    iput-object p1, p0, Lcom/facebook/cg;->i:Ljava/lang/String;

    .line 2042
    return-void
.end method

.method c()I
    .locals 1

    .prologue
    .line 1995
    iget v0, p0, Lcom/facebook/cg;->c:I

    return v0
.end method

.method d()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2010
    iget-object v0, p0, Lcom/facebook/cg;->f:Ljava/util/List;

    return-object v0
.end method

.method e()Lcom/facebook/cp;
    .locals 1

    .prologue
    .line 2025
    iget-object v0, p0, Lcom/facebook/cg;->a:Lcom/facebook/cp;

    return-object v0
.end method

.method f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2045
    iget-object v0, p0, Lcom/facebook/cg;->j:Ljava/lang/String;

    return-object v0
.end method

.method g()Lcom/facebook/ab;
    .locals 10

    .prologue
    .line 2049
    new-instance v8, Lcom/facebook/cj;

    invoke-direct {v8, p0}, Lcom/facebook/cj;-><init>(Lcom/facebook/cg;)V

    .line 2060
    new-instance v0, Lcom/facebook/ab;

    iget-object v1, p0, Lcom/facebook/cg;->b:Lcom/facebook/cu;

    iget v2, p0, Lcom/facebook/cg;->c:I

    iget-boolean v3, p0, Lcom/facebook/cg;->e:Z

    iget-object v4, p0, Lcom/facebook/cg;->f:Ljava/util/List;

    iget-object v5, p0, Lcom/facebook/cg;->g:Lcom/facebook/ct;

    iget-object v6, p0, Lcom/facebook/cg;->h:Ljava/lang/String;

    iget-object v7, p0, Lcom/facebook/cg;->i:Ljava/lang/String;

    iget-object v9, p0, Lcom/facebook/cg;->j:Ljava/lang/String;

    invoke-direct/range {v0 .. v9}, Lcom/facebook/ab;-><init>(Lcom/facebook/cu;IZLjava/util/List;Lcom/facebook/ct;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/ak;Ljava/lang/String;)V

    return-object v0
.end method
