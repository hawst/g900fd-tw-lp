.class Lcom/facebook/ck;
.super Landroid/os/AsyncTask;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/facebook/ca;

.field private final b:Ljava/lang/String;

.field private final c:Landroid/content/Context;


# direct methods
.method public constructor <init>(Lcom/facebook/ca;Ljava/lang/String;Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1855
    iput-object p1, p0, Lcom/facebook/ck;->a:Lcom/facebook/ca;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 1856
    iput-object p2, p0, Lcom/facebook/ck;->b:Ljava/lang/String;

    .line 1857
    invoke-virtual {p3}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ck;->c:Landroid/content/Context;

    .line 1858
    return-void
.end method


# virtual methods
.method protected varargs a([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 3

    .prologue
    .line 1863
    :try_start_0
    iget-object v0, p0, Lcom/facebook/ck;->c:Landroid/content/Context;

    iget-object v1, p0, Lcom/facebook/ck;->b:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lcom/facebook/cx;->a(Landroid/content/Context;Ljava/lang/String;Z)Lcom/facebook/bz;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1867
    :goto_0
    const/4 v0, 0x0

    return-object v0

    .line 1864
    :catch_0
    move-exception v0

    .line 1865
    const-string/jumbo v1, "Facebook-publish"

    invoke-static {v1, v0}, Lcom/facebook/b/bp;->a(Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method protected a(Ljava/lang/Void;)V
    .locals 3

    .prologue
    .line 1873
    iget-object v1, p0, Lcom/facebook/ck;->a:Lcom/facebook/ca;

    monitor-enter v1

    .line 1874
    :try_start_0
    iget-object v0, p0, Lcom/facebook/ck;->a:Lcom/facebook/ca;

    const/4 v2, 0x0

    invoke-static {v0, v2}, Lcom/facebook/ca;->a(Lcom/facebook/ca;Lcom/facebook/ck;)Lcom/facebook/ck;

    .line 1875
    monitor-exit v1

    .line 1876
    return-void

    .line 1875
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1851
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/facebook/ck;->a([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1851
    check-cast p1, Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/facebook/ck;->a(Ljava/lang/Void;)V

    return-void
.end method
