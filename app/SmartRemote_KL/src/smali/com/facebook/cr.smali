.class Lcom/facebook/cr;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/content/ServiceConnection;


# instance fields
.field final a:Landroid/os/Messenger;

.field b:Landroid/os/Messenger;

.field final synthetic c:Lcom/facebook/ca;


# direct methods
.method constructor <init>(Lcom/facebook/ca;)V
    .locals 3

    .prologue
    .line 1648
    iput-object p1, p0, Lcom/facebook/cr;->c:Lcom/facebook/ca;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1650
    new-instance v0, Landroid/os/Messenger;

    new-instance v1, Lcom/facebook/cs;

    iget-object v2, p0, Lcom/facebook/cr;->c:Lcom/facebook/ca;

    invoke-direct {v1, v2, p0}, Lcom/facebook/cs;-><init>(Lcom/facebook/ca;Lcom/facebook/cr;)V

    invoke-direct {v0, v1}, Landroid/os/Messenger;-><init>(Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/facebook/cr;->a:Landroid/os/Messenger;

    .line 1653
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/cr;->b:Landroid/os/Messenger;

    return-void
.end method

.method static synthetic a(Lcom/facebook/cr;)V
    .locals 0

    .prologue
    .line 1648
    invoke-direct {p0}, Lcom/facebook/cr;->b()V

    return-void
.end method

.method private b()V
    .locals 2

    .prologue
    .line 1681
    iget-object v0, p0, Lcom/facebook/cr;->c:Lcom/facebook/ca;

    invoke-static {v0}, Lcom/facebook/ca;->d(Lcom/facebook/ca;)Lcom/facebook/cr;

    move-result-object v0

    if-ne v0, p0, :cond_0

    .line 1682
    iget-object v0, p0, Lcom/facebook/cr;->c:Lcom/facebook/ca;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/facebook/ca;->a(Lcom/facebook/ca;Lcom/facebook/cr;)Lcom/facebook/cr;

    .line 1684
    :cond_0
    return-void
.end method

.method private c()V
    .locals 3

    .prologue
    .line 1687
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 1688
    const-string/jumbo v1, "access_token"

    iget-object v2, p0, Lcom/facebook/cr;->c:Lcom/facebook/ca;

    invoke-virtual {v2}, Lcom/facebook/ca;->o()Lcom/facebook/a;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/a;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1690
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v1

    .line 1691
    invoke-virtual {v1, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 1692
    iget-object v0, p0, Lcom/facebook/cr;->a:Landroid/os/Messenger;

    iput-object v0, v1, Landroid/os/Message;->replyTo:Landroid/os/Messenger;

    .line 1695
    :try_start_0
    iget-object v0, p0, Lcom/facebook/cr;->b:Landroid/os/Messenger;

    invoke-virtual {v0, v1}, Landroid/os/Messenger;->send(Landroid/os/Message;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1699
    :goto_0
    return-void

    .line 1696
    :catch_0
    move-exception v0

    .line 1697
    invoke-direct {p0}, Lcom/facebook/cr;->b()V

    goto :goto_0
.end method


# virtual methods
.method public a()V
    .locals 3

    .prologue
    .line 1656
    invoke-static {}, Lcom/facebook/ca;->k()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/b/ay;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    .line 1657
    if-eqz v0, :cond_0

    invoke-static {}, Lcom/facebook/ca;->p()Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v0, p0, v2}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1659
    iget-object v0, p0, Lcom/facebook/cr;->c:Lcom/facebook/ca;

    new-instance v1, Ljava/util/Date;

    invoke-direct {v1}, Ljava/util/Date;-><init>()V

    invoke-virtual {v0, v1}, Lcom/facebook/ca;->a(Ljava/util/Date;)V

    .line 1663
    :goto_0
    return-void

    .line 1661
    :cond_0
    invoke-direct {p0}, Lcom/facebook/cr;->b()V

    goto :goto_0
.end method

.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 1

    .prologue
    .line 1667
    new-instance v0, Landroid/os/Messenger;

    invoke-direct {v0, p2}, Landroid/os/Messenger;-><init>(Landroid/os/IBinder;)V

    iput-object v0, p0, Lcom/facebook/cr;->b:Landroid/os/Messenger;

    .line 1668
    invoke-direct {p0}, Lcom/facebook/cr;->c()V

    .line 1669
    return-void
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 1

    .prologue
    .line 1673
    invoke-direct {p0}, Lcom/facebook/cr;->b()V

    .line 1677
    invoke-static {}, Lcom/facebook/ca;->p()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    .line 1678
    return-void
.end method
