.class public Lcom/facebook/db;
.super Ljava/lang/Object;


# instance fields
.field private final a:Landroid/app/Activity;

.field private final b:Lcom/facebook/cq;

.field private final c:Landroid/content/BroadcastReceiver;

.field private final d:Landroid/support/v4/a/q;

.field private e:Ljava/util/UUID;

.field private f:Lcom/facebook/b/be;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lcom/facebook/cq;)V
    .locals 2

    .prologue
    .line 70
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 71
    if-nez p1, :cond_0

    .line 72
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "activity cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 75
    :cond_0
    iput-object p1, p0, Lcom/facebook/db;->a:Landroid/app/Activity;

    .line 76
    iput-object p2, p0, Lcom/facebook/db;->b:Lcom/facebook/cq;

    .line 77
    new-instance v0, Lcom/facebook/dd;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/facebook/dd;-><init>(Lcom/facebook/db;Lcom/facebook/dc;)V

    iput-object v0, p0, Lcom/facebook/db;->c:Landroid/content/BroadcastReceiver;

    .line 78
    invoke-static {p1}, Landroid/support/v4/a/q;->a(Landroid/content/Context;)Landroid/support/v4/a/q;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/db;->d:Landroid/support/v4/a/q;

    .line 79
    invoke-static {}, Lcom/facebook/b/be;->a()Lcom/facebook/b/be;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/db;->f:Lcom/facebook/b/be;

    .line 82
    invoke-static {p1}, Lcom/facebook/cx;->a(Landroid/content/Context;)V

    .line 83
    return-void
.end method

.method static synthetic a(Lcom/facebook/db;)Lcom/facebook/cq;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/facebook/db;->b:Lcom/facebook/cq;

    return-object v0
.end method

.method private a()V
    .locals 2

    .prologue
    .line 344
    iget-object v0, p0, Lcom/facebook/db;->f:Lcom/facebook/b/be;

    iget-object v1, p0, Lcom/facebook/db;->e:Ljava/util/UUID;

    invoke-virtual {v0, v1}, Lcom/facebook/b/be;->a(Ljava/util/UUID;)V

    .line 345
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/db;->e:Ljava/util/UUID;

    .line 346
    return-void
.end method

.method private a(Lcom/facebook/widget/b;)V
    .locals 6

    .prologue
    .line 314
    iget-object v0, p0, Lcom/facebook/db;->e:Ljava/util/UUID;

    if-nez v0, :cond_1

    .line 341
    :cond_0
    :goto_0
    return-void

    .line 318
    :cond_1
    iget-object v0, p0, Lcom/facebook/db;->f:Lcom/facebook/b/be;

    iget-object v1, p0, Lcom/facebook/db;->e:Ljava/util/UUID;

    invoke-virtual {v0, v1}, Lcom/facebook/b/be;->b(Ljava/util/UUID;)Lcom/facebook/widget/FacebookDialog$PendingCall;

    move-result-object v0

    .line 320
    if-eqz v0, :cond_0

    .line 324
    if-eqz p1, :cond_2

    .line 325
    invoke-virtual {v0}, Lcom/facebook/widget/FacebookDialog$PendingCall;->a()Landroid/content/Intent;

    move-result-object v1

    .line 327
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    .line 328
    const-string/jumbo v3, "com.facebook.platform.protocol.CALL_ID"

    const-string/jumbo v4, "com.facebook.platform.protocol.CALL_ID"

    invoke-virtual {v1, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 330
    const-string/jumbo v3, "com.facebook.platform.protocol.PROTOCOL_ACTION"

    const-string/jumbo v4, "com.facebook.platform.protocol.PROTOCOL_ACTION"

    invoke-virtual {v1, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 332
    const-string/jumbo v3, "com.facebook.platform.protocol.PROTOCOL_VERSION"

    const-string/jumbo v4, "com.facebook.platform.protocol.PROTOCOL_VERSION"

    const/4 v5, 0x0

    invoke-virtual {v1, v4, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    invoke-virtual {v2, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 334
    const-string/jumbo v1, "com.facebook.platform.status.ERROR_TYPE"

    const-string/jumbo v3, "UnknownError"

    invoke-virtual {v2, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 336
    iget-object v1, p0, Lcom/facebook/db;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Lcom/facebook/widget/FacebookDialog$PendingCall;->c()I

    move-result v3

    invoke-static {v1, v0, v3, v2, p1}, Lcom/facebook/widget/FacebookDialog;->a(Landroid/content/Context;Lcom/facebook/widget/FacebookDialog$PendingCall;ILandroid/content/Intent;Lcom/facebook/widget/b;)Z

    .line 340
    :cond_2
    invoke-direct {p0}, Lcom/facebook/db;->a()V

    goto :goto_0
.end method

.method private b(IILandroid/content/Intent;Lcom/facebook/widget/b;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 280
    iget-object v2, p0, Lcom/facebook/db;->e:Ljava/util/UUID;

    if-nez v2, :cond_1

    .line 310
    :cond_0
    :goto_0
    return v0

    .line 283
    :cond_1
    iget-object v2, p0, Lcom/facebook/db;->f:Lcom/facebook/b/be;

    iget-object v3, p0, Lcom/facebook/db;->e:Ljava/util/UUID;

    invoke-virtual {v2, v3}, Lcom/facebook/b/be;->b(Ljava/util/UUID;)Lcom/facebook/widget/FacebookDialog$PendingCall;

    move-result-object v2

    .line 285
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Lcom/facebook/widget/FacebookDialog$PendingCall;->c()I

    move-result v3

    if-ne v3, p1, :cond_0

    .line 289
    if-nez p3, :cond_2

    .line 292
    invoke-direct {p0, p4}, Lcom/facebook/db;->a(Lcom/facebook/widget/b;)V

    move v0, v1

    .line 293
    goto :goto_0

    .line 296
    :cond_2
    invoke-static {p3}, Lcom/facebook/b/ay;->b(Landroid/content/Intent;)Ljava/util/UUID;

    move-result-object v0

    .line 299
    if-eqz v0, :cond_3

    iget-object v3, p0, Lcom/facebook/db;->e:Ljava/util/UUID;

    invoke-virtual {v3, v0}, Ljava/util/UUID;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 301
    iget-object v0, p0, Lcom/facebook/db;->a:Landroid/app/Activity;

    invoke-static {v0, v2, p1, p3, p4}, Lcom/facebook/widget/FacebookDialog;->a(Landroid/content/Context;Lcom/facebook/widget/FacebookDialog$PendingCall;ILandroid/content/Intent;Lcom/facebook/widget/b;)Z

    .line 309
    :goto_1
    invoke-direct {p0}, Lcom/facebook/db;->a()V

    move v0, v1

    .line 310
    goto :goto_0

    .line 306
    :cond_3
    invoke-direct {p0, p4}, Lcom/facebook/db;->a(Lcom/facebook/widget/b;)V

    goto :goto_1
.end method


# virtual methods
.method public a(IILandroid/content/Intent;Lcom/facebook/widget/b;)V
    .locals 2

    .prologue
    .line 156
    invoke-static {}, Lcom/facebook/ca;->j()Lcom/facebook/ca;

    move-result-object v0

    .line 157
    if-eqz v0, :cond_0

    .line 158
    iget-object v1, p0, Lcom/facebook/db;->a:Landroid/app/Activity;

    invoke-virtual {v0, v1, p1, p2, p3}, Lcom/facebook/ca;->a(Landroid/app/Activity;IILandroid/content/Intent;)Z

    .line 161
    :cond_0
    iget-object v0, p0, Lcom/facebook/db;->a:Landroid/app/Activity;

    invoke-static {v0, p1, p2, p3}, Lcom/facebook/b/aa;->a(Landroid/content/Context;IILandroid/content/Intent;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 166
    :goto_0
    return-void

    .line 165
    :cond_1
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/facebook/db;->b(IILandroid/content/Intent;Lcom/facebook/widget/b;)Z

    goto :goto_0
.end method
