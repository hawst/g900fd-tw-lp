.class Lcom/facebook/z;
.super Lcom/facebook/widget/w;


# instance fields
.field private a:Ljava/lang/String;

.field private b:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 907
    const-string/jumbo v0, "oauth"

    invoke-direct {p0, p1, p2, v0, p3}, Lcom/facebook/widget/w;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 908
    return-void
.end method


# virtual methods
.method public a()Lcom/facebook/widget/s;
    .locals 6

    .prologue
    .line 922
    invoke-virtual {p0}, Lcom/facebook/z;->e()Landroid/os/Bundle;

    move-result-object v3

    .line 923
    const-string/jumbo v0, "redirect_uri"

    const-string/jumbo v1, "fbconnect://success"

    invoke-virtual {v3, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 924
    const-string/jumbo v0, "client_id"

    invoke-virtual {p0}, Lcom/facebook/z;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 925
    const-string/jumbo v0, "e2e"

    iget-object v1, p0, Lcom/facebook/z;->a:Ljava/lang/String;

    invoke-virtual {v3, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 926
    const-string/jumbo v0, "response_type"

    const-string/jumbo v1, "token"

    invoke-virtual {v3, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 927
    const-string/jumbo v0, "return_scopes"

    const-string/jumbo v1, "true"

    invoke-virtual {v3, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 930
    iget-boolean v0, p0, Lcom/facebook/z;->b:Z

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/facebook/cx;->g()Z

    move-result v0

    if-nez v0, :cond_0

    .line 931
    const-string/jumbo v0, "auth_type"

    const-string/jumbo v1, "rerequest"

    invoke-virtual {v3, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 934
    :cond_0
    new-instance v0, Lcom/facebook/widget/s;

    invoke-virtual {p0}, Lcom/facebook/z;->c()Landroid/content/Context;

    move-result-object v1

    const-string/jumbo v2, "oauth"

    invoke-virtual {p0}, Lcom/facebook/z;->d()I

    move-result v4

    invoke-virtual {p0}, Lcom/facebook/z;->f()Lcom/facebook/widget/z;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, Lcom/facebook/widget/s;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;ILcom/facebook/widget/z;)V

    return-object v0
.end method

.method public a(Ljava/lang/String;)Lcom/facebook/z;
    .locals 0

    .prologue
    .line 911
    iput-object p1, p0, Lcom/facebook/z;->a:Ljava/lang/String;

    .line 912
    return-object p0
.end method

.method public a(Z)Lcom/facebook/z;
    .locals 0

    .prologue
    .line 916
    iput-boolean p1, p0, Lcom/facebook/z;->b:Z

    .line 917
    return-object p0
.end method
