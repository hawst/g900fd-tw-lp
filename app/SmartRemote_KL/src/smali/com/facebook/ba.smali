.class public final enum Lcom/facebook/ba;
.super Ljava/lang/Enum;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/ba;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/facebook/ba;

.field public static final enum b:Lcom/facebook/ba;

.field public static final enum c:Lcom/facebook/ba;

.field private static final synthetic d:[Lcom/facebook/ba;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 26
    new-instance v0, Lcom/facebook/ba;

    const-string/jumbo v1, "GET"

    invoke-direct {v0, v1, v2}, Lcom/facebook/ba;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/ba;->a:Lcom/facebook/ba;

    .line 31
    new-instance v0, Lcom/facebook/ba;

    const-string/jumbo v1, "POST"

    invoke-direct {v0, v1, v3}, Lcom/facebook/ba;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/ba;->b:Lcom/facebook/ba;

    .line 36
    new-instance v0, Lcom/facebook/ba;

    const-string/jumbo v1, "DELETE"

    invoke-direct {v0, v1, v4}, Lcom/facebook/ba;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/ba;->c:Lcom/facebook/ba;

    .line 22
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/facebook/ba;

    sget-object v1, Lcom/facebook/ba;->a:Lcom/facebook/ba;

    aput-object v1, v0, v2

    sget-object v1, Lcom/facebook/ba;->b:Lcom/facebook/ba;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/ba;->c:Lcom/facebook/ba;

    aput-object v1, v0, v4

    sput-object v0, Lcom/facebook/ba;->d:[Lcom/facebook/ba;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 22
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/ba;
    .locals 1

    .prologue
    .line 22
    const-class v0, Lcom/facebook/ba;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/ba;

    return-object v0
.end method

.method public static values()[Lcom/facebook/ba;
    .locals 1

    .prologue
    .line 22
    sget-object v0, Lcom/facebook/ba;->d:[Lcom/facebook/ba;

    invoke-virtual {v0}, [Lcom/facebook/ba;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/ba;

    return-object v0
.end method
