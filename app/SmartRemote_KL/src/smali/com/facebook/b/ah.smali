.class Lcom/facebook/b/ah;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/facebook/widget/b;


# instance fields
.field final synthetic a:Landroid/os/Bundle;

.field final synthetic b:Lcom/facebook/b/aa;


# direct methods
.method constructor <init>(Lcom/facebook/b/aa;Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 683
    iput-object p1, p0, Lcom/facebook/b/ah;->b:Lcom/facebook/b/aa;

    iput-object p2, p0, Lcom/facebook/b/ah;->a:Landroid/os/Bundle;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/facebook/widget/FacebookDialog$PendingCall;Landroid/os/Bundle;)V
    .locals 8

    .prologue
    .line 686
    const-string/jumbo v0, "object_is_liked"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 707
    :goto_0
    return-void

    .line 691
    :cond_0
    const-string/jumbo v0, "object_is_liked"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    .line 692
    const-string/jumbo v0, "like_count_string"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 693
    const-string/jumbo v0, "social_sentence"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 694
    const-string/jumbo v0, "unlike_token"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 696
    iget-object v0, p0, Lcom/facebook/b/ah;->a:Landroid/os/Bundle;

    if-nez v0, :cond_1

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 697
    :goto_1
    const-string/jumbo v3, "call_id"

    invoke-virtual {p1}, Lcom/facebook/widget/FacebookDialog$PendingCall;->b()Ljava/util/UUID;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v3, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 698
    iget-object v3, p0, Lcom/facebook/b/ah;->b:Lcom/facebook/b/aa;

    invoke-static {v3}, Lcom/facebook/b/aa;->b(Lcom/facebook/b/aa;)Lcom/facebook/c;

    move-result-object v3

    const-string/jumbo v5, "fb_like_control_dialog_did_succeed"

    const/4 v7, 0x0

    invoke-virtual {v3, v5, v7, v0}, Lcom/facebook/c;->a(Ljava/lang/String;Ljava/lang/Double;Landroid/os/Bundle;)V

    .line 700
    iget-object v0, p0, Lcom/facebook/b/ah;->b:Lcom/facebook/b/aa;

    move-object v3, v2

    move-object v5, v4

    invoke-static/range {v0 .. v6}, Lcom/facebook/b/aa;->a(Lcom/facebook/b/aa;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 696
    :cond_1
    iget-object v0, p0, Lcom/facebook/b/ah;->a:Landroid/os/Bundle;

    goto :goto_1
.end method

.method public a(Lcom/facebook/widget/FacebookDialog$PendingCall;Ljava/lang/Exception;Landroid/os/Bundle;)V
    .locals 5

    .prologue
    .line 711
    sget-object v0, Lcom/facebook/bb;->a:Lcom/facebook/bb;

    invoke-static {}, Lcom/facebook/b/aa;->g()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "Like Dialog failed with error : %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p2, v3, v4

    invoke-static {v0, v1, v2, v3}, Lcom/facebook/b/ax;->a(Lcom/facebook/bb;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 713
    iget-object v0, p0, Lcom/facebook/b/ah;->a:Landroid/os/Bundle;

    if-nez v0, :cond_0

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 714
    :goto_0
    const-string/jumbo v1, "call_id"

    invoke-virtual {p1}, Lcom/facebook/widget/FacebookDialog$PendingCall;->b()Ljava/util/UUID;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 717
    iget-object v1, p0, Lcom/facebook/b/ah;->b:Lcom/facebook/b/aa;

    const-string/jumbo v2, "present_dialog"

    invoke-static {v1, v2, v0}, Lcom/facebook/b/aa;->a(Lcom/facebook/b/aa;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 719
    iget-object v0, p0, Lcom/facebook/b/ah;->b:Lcom/facebook/b/aa;

    invoke-static {v0}, Lcom/facebook/b/aa;->c(Lcom/facebook/b/aa;)Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/b/ah;->b:Lcom/facebook/b/aa;

    const-string/jumbo v2, "com.facebook.sdk.LikeActionController.DID_ERROR"

    invoke-static {v0, v1, v2, p3}, Lcom/facebook/b/aa;->a(Landroid/content/Context;Lcom/facebook/b/aa;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 720
    return-void

    .line 713
    :cond_0
    iget-object v0, p0, Lcom/facebook/b/ah;->a:Landroid/os/Bundle;

    goto :goto_0
.end method
