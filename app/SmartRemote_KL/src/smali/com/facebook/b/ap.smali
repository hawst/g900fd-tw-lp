.class Lcom/facebook/b/ap;
.super Lcom/facebook/b/al;


# instance fields
.field d:Ljava/lang/String;

.field e:Ljava/lang/String;

.field f:Ljava/lang/String;

.field g:Ljava/lang/String;

.field final synthetic h:Lcom/facebook/b/aa;


# direct methods
.method constructor <init>(Lcom/facebook/b/aa;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 1212
    iput-object p1, p0, Lcom/facebook/b/ap;->h:Lcom/facebook/b/aa;

    .line 1213
    invoke-direct {p0, p1, p2}, Lcom/facebook/b/al;-><init>(Lcom/facebook/b/aa;Ljava/lang/String;)V

    .line 1215
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 1216
    const-string/jumbo v1, "fields"

    const-string/jumbo v2, "engagement.fields(count_string_with_like,count_string_without_like,social_sentence_with_like,social_sentence_without_like)"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1224
    new-instance v1, Lcom/facebook/Request;

    invoke-static {p1}, Lcom/facebook/b/aa;->f(Lcom/facebook/b/aa;)Lcom/facebook/ca;

    move-result-object v2

    sget-object v3, Lcom/facebook/ba;->a:Lcom/facebook/ba;

    invoke-direct {v1, v2, p2, v0, v3}, Lcom/facebook/Request;-><init>(Lcom/facebook/ca;Ljava/lang/String;Landroid/os/Bundle;Lcom/facebook/ba;)V

    invoke-virtual {p0, v1}, Lcom/facebook/b/ap;->a(Lcom/facebook/Request;)V

    .line 1225
    return-void
.end method


# virtual methods
.method protected a(Lcom/facebook/at;)V
    .locals 6

    .prologue
    .line 1240
    sget-object v0, Lcom/facebook/bb;->a:Lcom/facebook/bb;

    invoke-static {}, Lcom/facebook/b/aa;->g()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "Error fetching engagement for object \'%s\' : %s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/facebook/b/ap;->a:Ljava/lang/String;

    aput-object v5, v3, v4

    const/4 v4, 0x1

    aput-object p1, v3, v4

    invoke-static {v0, v1, v2, v3}, Lcom/facebook/b/ax;->a(Lcom/facebook/bb;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1243
    iget-object v0, p0, Lcom/facebook/b/ap;->h:Lcom/facebook/b/aa;

    const-string/jumbo v1, "get_engagement"

    invoke-static {v0, v1, p1}, Lcom/facebook/b/aa;->a(Lcom/facebook/b/aa;Ljava/lang/String;Lcom/facebook/at;)V

    .line 1244
    return-void
.end method

.method protected a(Lcom/facebook/bz;)V
    .locals 2

    .prologue
    .line 1229
    invoke-virtual {p1}, Lcom/facebook/bz;->b()Lcom/facebook/c/c;

    move-result-object v0

    const-string/jumbo v1, "engagement"

    invoke-static {v0, v1}, Lcom/facebook/b/bp;->a(Lcom/facebook/c/c;Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    .line 1230
    if-eqz v0, :cond_0

    .line 1231
    const-string/jumbo v1, "count_string_with_like"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/b/ap;->d:Ljava/lang/String;

    .line 1232
    const-string/jumbo v1, "count_string_without_like"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/b/ap;->e:Ljava/lang/String;

    .line 1233
    const-string/jumbo v1, "social_sentence_with_like"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/b/ap;->f:Ljava/lang/String;

    .line 1234
    const-string/jumbo v1, "social_sentence_without_like"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/b/ap;->g:Ljava/lang/String;

    .line 1236
    :cond_0
    return-void
.end method
