.class Lcom/facebook/b/bx;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/facebook/b/bw;


# static fields
.field static final synthetic a:Z


# instance fields
.field final synthetic b:Lcom/facebook/b/bu;

.field private final c:Ljava/lang/Runnable;

.field private d:Lcom/facebook/b/bx;

.field private e:Lcom/facebook/b/bx;

.field private f:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 126
    const-class v0, Lcom/facebook/b/bu;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/facebook/b/bx;->a:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>(Lcom/facebook/b/bu;Ljava/lang/Runnable;)V
    .locals 0

    .prologue
    .line 132
    iput-object p1, p0, Lcom/facebook/b/bx;->b:Lcom/facebook/b/bu;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 133
    iput-object p2, p0, Lcom/facebook/b/bx;->c:Ljava/lang/Runnable;

    .line 134
    return-void
.end method


# virtual methods
.method a(Lcom/facebook/b/bx;)Lcom/facebook/b/bx;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 191
    sget-boolean v1, Lcom/facebook/b/bx;->a:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/facebook/b/bx;->d:Lcom/facebook/b/bx;

    if-nez v1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 192
    :cond_0
    sget-boolean v1, Lcom/facebook/b/bx;->a:Z

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/facebook/b/bx;->e:Lcom/facebook/b/bx;

    if-nez v1, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 194
    :cond_1
    if-ne p1, p0, :cond_2

    .line 195
    iget-object v1, p0, Lcom/facebook/b/bx;->d:Lcom/facebook/b/bx;

    if-ne v1, p0, :cond_3

    move-object p1, v0

    .line 202
    :cond_2
    :goto_0
    iget-object v1, p0, Lcom/facebook/b/bx;->d:Lcom/facebook/b/bx;

    iget-object v2, p0, Lcom/facebook/b/bx;->e:Lcom/facebook/b/bx;

    iput-object v2, v1, Lcom/facebook/b/bx;->e:Lcom/facebook/b/bx;

    .line 203
    iget-object v1, p0, Lcom/facebook/b/bx;->e:Lcom/facebook/b/bx;

    iget-object v2, p0, Lcom/facebook/b/bx;->d:Lcom/facebook/b/bx;

    iput-object v2, v1, Lcom/facebook/b/bx;->d:Lcom/facebook/b/bx;

    .line 204
    iput-object v0, p0, Lcom/facebook/b/bx;->e:Lcom/facebook/b/bx;

    iput-object v0, p0, Lcom/facebook/b/bx;->d:Lcom/facebook/b/bx;

    .line 206
    return-object p1

    .line 198
    :cond_3
    iget-object p1, p0, Lcom/facebook/b/bx;->d:Lcom/facebook/b/bx;

    goto :goto_0
.end method

.method a(Lcom/facebook/b/bx;Z)Lcom/facebook/b/bx;
    .locals 2

    .prologue
    .line 176
    sget-boolean v0, Lcom/facebook/b/bx;->a:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/b/bx;->d:Lcom/facebook/b/bx;

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 177
    :cond_0
    sget-boolean v0, Lcom/facebook/b/bx;->a:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/facebook/b/bx;->e:Lcom/facebook/b/bx;

    if-eqz v0, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 179
    :cond_1
    if-nez p1, :cond_2

    .line 180
    iput-object p0, p0, Lcom/facebook/b/bx;->e:Lcom/facebook/b/bx;

    iput-object p0, p0, Lcom/facebook/b/bx;->d:Lcom/facebook/b/bx;

    move-object v0, p0

    .line 187
    :goto_0
    if-eqz p2, :cond_3

    :goto_1
    return-object p0

    .line 182
    :cond_2
    iput-object p1, p0, Lcom/facebook/b/bx;->d:Lcom/facebook/b/bx;

    .line 183
    iget-object v0, p1, Lcom/facebook/b/bx;->e:Lcom/facebook/b/bx;

    iput-object v0, p0, Lcom/facebook/b/bx;->e:Lcom/facebook/b/bx;

    .line 184
    iget-object v0, p0, Lcom/facebook/b/bx;->d:Lcom/facebook/b/bx;

    iget-object v1, p0, Lcom/facebook/b/bx;->e:Lcom/facebook/b/bx;

    iput-object p0, v1, Lcom/facebook/b/bx;->d:Lcom/facebook/b/bx;

    iput-object p0, v0, Lcom/facebook/b/bx;->e:Lcom/facebook/b/bx;

    move-object v0, p1

    goto :goto_0

    :cond_3
    move-object p0, v0

    .line 187
    goto :goto_1
.end method

.method a()Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 164
    iget-object v0, p0, Lcom/facebook/b/bx;->c:Ljava/lang/Runnable;

    return-object v0
.end method

.method a(Z)V
    .locals 0

    .prologue
    .line 172
    iput-boolean p1, p0, Lcom/facebook/b/bx;->f:Z

    .line 173
    return-void
.end method
