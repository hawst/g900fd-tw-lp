.class Lcom/facebook/b/ac;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/facebook/bu;


# instance fields
.field final synthetic a:Lcom/facebook/b/aq;

.field final synthetic b:Lcom/facebook/b/as;

.field final synthetic c:Lcom/facebook/b/au;

.field final synthetic d:Lcom/facebook/b/aa;


# direct methods
.method constructor <init>(Lcom/facebook/b/aa;Lcom/facebook/b/aq;Lcom/facebook/b/as;Lcom/facebook/b/au;)V
    .locals 0

    .prologue
    .line 968
    iput-object p1, p0, Lcom/facebook/b/ac;->d:Lcom/facebook/b/aa;

    iput-object p2, p0, Lcom/facebook/b/ac;->a:Lcom/facebook/b/aq;

    iput-object p3, p0, Lcom/facebook/b/ac;->b:Lcom/facebook/b/as;

    iput-object p4, p0, Lcom/facebook/b/ac;->c:Lcom/facebook/b/au;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/facebook/bt;)V
    .locals 6

    .prologue
    .line 971
    iget-object v0, p0, Lcom/facebook/b/ac;->d:Lcom/facebook/b/aa;

    iget-object v1, p0, Lcom/facebook/b/ac;->a:Lcom/facebook/b/aq;

    iget-object v1, v1, Lcom/facebook/b/aq;->d:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/facebook/b/aa;->a(Lcom/facebook/b/aa;Ljava/lang/String;)Ljava/lang/String;

    .line 972
    iget-object v0, p0, Lcom/facebook/b/ac;->d:Lcom/facebook/b/aa;

    invoke-static {v0}, Lcom/facebook/b/aa;->d(Lcom/facebook/b/aa;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/b/bp;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 973
    iget-object v0, p0, Lcom/facebook/b/ac;->d:Lcom/facebook/b/aa;

    iget-object v1, p0, Lcom/facebook/b/ac;->b:Lcom/facebook/b/as;

    iget-object v1, v1, Lcom/facebook/b/as;->d:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/facebook/b/aa;->a(Lcom/facebook/b/aa;Ljava/lang/String;)Ljava/lang/String;

    .line 974
    iget-object v0, p0, Lcom/facebook/b/ac;->d:Lcom/facebook/b/aa;

    iget-object v1, p0, Lcom/facebook/b/ac;->b:Lcom/facebook/b/as;

    iget-boolean v1, v1, Lcom/facebook/b/as;->e:Z

    invoke-static {v0, v1}, Lcom/facebook/b/aa;->a(Lcom/facebook/b/aa;Z)Z

    .line 977
    :cond_0
    iget-object v0, p0, Lcom/facebook/b/ac;->d:Lcom/facebook/b/aa;

    invoke-static {v0}, Lcom/facebook/b/aa;->d(Lcom/facebook/b/aa;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/b/bp;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 978
    sget-object v0, Lcom/facebook/bb;->f:Lcom/facebook/bb;

    invoke-static {}, Lcom/facebook/b/aa;->g()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "Unable to verify the FB id for \'%s\'. Verify that it is a valid FB object or page"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/facebook/b/ac;->d:Lcom/facebook/b/aa;

    invoke-static {v5}, Lcom/facebook/b/aa;->e(Lcom/facebook/b/aa;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v0, v1, v2, v3}, Lcom/facebook/b/ax;->a(Lcom/facebook/bb;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 981
    iget-object v1, p0, Lcom/facebook/b/ac;->d:Lcom/facebook/b/aa;

    const-string/jumbo v2, "get_verified_id"

    iget-object v0, p0, Lcom/facebook/b/ac;->b:Lcom/facebook/b/as;

    iget-object v0, v0, Lcom/facebook/b/as;->b:Lcom/facebook/at;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/facebook/b/ac;->b:Lcom/facebook/b/as;

    iget-object v0, v0, Lcom/facebook/b/as;->b:Lcom/facebook/at;

    :goto_0
    invoke-static {v1, v2, v0}, Lcom/facebook/b/aa;->a(Lcom/facebook/b/aa;Ljava/lang/String;Lcom/facebook/at;)V

    .line 985
    :cond_1
    iget-object v0, p0, Lcom/facebook/b/ac;->c:Lcom/facebook/b/au;

    if-eqz v0, :cond_2

    .line 986
    iget-object v0, p0, Lcom/facebook/b/ac;->c:Lcom/facebook/b/au;

    invoke-interface {v0}, Lcom/facebook/b/au;->a()V

    .line 988
    :cond_2
    return-void

    .line 981
    :cond_3
    iget-object v0, p0, Lcom/facebook/b/ac;->a:Lcom/facebook/b/aq;

    iget-object v0, v0, Lcom/facebook/b/aq;->b:Lcom/facebook/at;

    goto :goto_0
.end method
