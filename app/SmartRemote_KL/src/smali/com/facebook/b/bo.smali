.class Lcom/facebook/b/bo;
.super Ljava/lang/Object;


# static fields
.field static final a:Ljava/lang/String;

.field private static final b:Ljava/lang/String;

.field private static volatile c:Lcom/facebook/b/l;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 31
    const-class v0, Lcom/facebook/b/bo;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/b/bo;->a:Ljava/lang/String;

    .line 32
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/facebook/b/bo;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "_Redirect"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/b/bo;->b:Ljava/lang/String;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static declared-synchronized a(Landroid/content/Context;)Lcom/facebook/b/l;
    .locals 5

    .prologue
    .line 37
    const-class v1, Lcom/facebook/b/bo;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/facebook/b/bo;->c:Lcom/facebook/b/l;

    if-nez v0, :cond_0

    .line 38
    new-instance v0, Lcom/facebook/b/l;

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    sget-object v3, Lcom/facebook/b/bo;->a:Ljava/lang/String;

    new-instance v4, Lcom/facebook/b/u;

    invoke-direct {v4}, Lcom/facebook/b/u;-><init>()V

    invoke-direct {v0, v2, v3, v4}, Lcom/facebook/b/l;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/facebook/b/u;)V

    sput-object v0, Lcom/facebook/b/bo;->c:Lcom/facebook/b/l;

    .line 40
    :cond_0
    sget-object v0, Lcom/facebook/b/bo;->c:Lcom/facebook/b/l;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 37
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method static b(Landroid/content/Context;)V
    .locals 6

    .prologue
    .line 103
    :try_start_0
    invoke-static {p0}, Lcom/facebook/b/bo;->a(Landroid/content/Context;)Lcom/facebook/b/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/b/l;->a()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 107
    :goto_0
    return-void

    .line 104
    :catch_0
    move-exception v0

    .line 105
    sget-object v1, Lcom/facebook/bb;->d:Lcom/facebook/bb;

    const/4 v2, 0x5

    sget-object v3, Lcom/facebook/b/bo;->a:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "clearCache failed "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v2, v3, v0}, Lcom/facebook/b/ax;->a(Lcom/facebook/bb;ILjava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
