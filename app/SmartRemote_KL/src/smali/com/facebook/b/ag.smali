.class Lcom/facebook/b/ag;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Z

.field final synthetic b:Landroid/content/Context;

.field final synthetic c:Lcom/facebook/b/af;


# direct methods
.method constructor <init>(Lcom/facebook/b/af;ZLandroid/content/Context;)V
    .locals 0

    .prologue
    .line 292
    iput-object p1, p0, Lcom/facebook/b/ag;->c:Lcom/facebook/b/af;

    iput-boolean p2, p0, Lcom/facebook/b/ag;->a:Z

    iput-object p3, p0, Lcom/facebook/b/ag;->b:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 301
    iget-boolean v0, p0, Lcom/facebook/b/ag;->a:Z

    if-eqz v0, :cond_0

    .line 302
    invoke-static {}, Lcom/facebook/b/aa;->c()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    rem-int/lit16 v0, v0, 0x3e8

    invoke-static {v0}, Lcom/facebook/b/aa;->a(I)I

    .line 303
    iget-object v0, p0, Lcom/facebook/b/ag;->b:Landroid/content/Context;

    const-string/jumbo v1, "com.facebook.LikeActionController.CONTROLLER_STORE_KEY"

    invoke-virtual {v0, v1, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v1, "OBJECT_SUFFIX"

    invoke-static {}, Lcom/facebook/b/aa;->c()I

    move-result v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 310
    invoke-static {}, Lcom/facebook/b/aa;->d()Ljava/util/concurrent/ConcurrentHashMap;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->clear()V

    .line 311
    invoke-static {}, Lcom/facebook/b/aa;->e()Lcom/facebook/b/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/b/l;->a()V

    .line 314
    :cond_0
    iget-object v0, p0, Lcom/facebook/b/ag;->b:Landroid/content/Context;

    const/4 v1, 0x0

    const-string/jumbo v2, "com.facebook.sdk.LikeActionController.DID_RESET"

    invoke-static {v0, v1, v2}, Lcom/facebook/b/aa;->a(Landroid/content/Context;Lcom/facebook/b/aa;Ljava/lang/String;)V

    .line 315
    invoke-static {v3}, Lcom/facebook/b/aa;->a(Z)Z

    .line 316
    return-void
.end method
