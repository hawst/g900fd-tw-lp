.class public final Lcom/facebook/b/ay;
.super Ljava/lang/Object;


# static fields
.field private static final a:Lcom/facebook/b/bc;

.field private static b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/b/bc;",
            ">;"
        }
    .end annotation
.end field

.field private static c:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/b/bc;",
            ">;>;"
        }
    .end annotation
.end field

.field private static final d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 309
    new-instance v0, Lcom/facebook/b/ba;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/facebook/b/ba;-><init>(Lcom/facebook/b/az;)V

    sput-object v0, Lcom/facebook/b/ay;->a:Lcom/facebook/b/bc;

    .line 310
    invoke-static {}, Lcom/facebook/b/ay;->b()Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcom/facebook/b/ay;->b:Ljava/util/List;

    .line 311
    invoke-static {}, Lcom/facebook/b/ay;->c()Ljava/util/Map;

    move-result-object v0

    sput-object v0, Lcom/facebook/b/ay;->c:Ljava/util/Map;

    .line 429
    const/16 v0, 0x8

    new-array v0, v0, [Ljava/lang/Integer;

    const/4 v1, 0x0

    const v2, 0x13353c9

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const v2, 0x133529d

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const v2, 0x1335124

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const v2, 0x13350ac

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const v2, 0x1332d23

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const v2, 0x1332b3a

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const v2, 0x1332ac6

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const v2, 0x133060d

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcom/facebook/b/ay;->d:Ljava/util/List;

    return-void
.end method

.method public static final a()I
    .locals 2

    .prologue
    .line 425
    sget-object v0, Lcom/facebook/b/ay;->d:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method public static a(Landroid/content/Context;I)I
    .locals 3

    .prologue
    .line 608
    sget-object v0, Lcom/facebook/b/ay;->b:Ljava/util/List;

    const/4 v1, 0x1

    new-array v1, v1, [I

    const/4 v2, 0x0

    aput p1, v1, v2

    invoke-static {p0, v0, v1}, Lcom/facebook/b/ay;->a(Landroid/content/Context;Ljava/util/List;[I)I

    move-result v0

    return v0
.end method

.method private static a(Landroid/content/Context;Lcom/facebook/b/bc;[I)I
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v6, -0x1

    .line 637
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 639
    const/4 v1, 0x1

    new-array v2, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string/jumbo v3, "version"

    aput-object v3, v2, v1

    .line 640
    invoke-static {p1}, Lcom/facebook/b/ay;->a(Lcom/facebook/b/bc;)Landroid/net/Uri;

    move-result-object v1

    .line 643
    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    :try_start_0
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v1

    .line 644
    if-nez v1, :cond_1

    .line 686
    if-eqz v1, :cond_0

    .line 687
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 691
    :cond_0
    :goto_0
    return v6

    .line 648
    :cond_1
    :try_start_1
    new-instance v0, Ljava/util/TreeSet;

    invoke-direct {v0}, Ljava/util/TreeSet;-><init>()V

    .line 649
    :goto_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 650
    const-string/jumbo v2, "version"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    .line 651
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/TreeSet;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 686
    :catchall_0
    move-exception v0

    :goto_2
    if-eqz v1, :cond_2

    .line 687
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v0

    .line 656
    :cond_3
    :try_start_2
    array-length v2, p2

    add-int/lit8 v2, v2, -0x1

    .line 657
    invoke-virtual {v0}, Ljava/util/TreeSet;->descendingIterator()Ljava/util/Iterator;

    move-result-object v3

    .line 658
    invoke-static {}, Lcom/facebook/b/ay;->a()I

    move-result v4

    .line 659
    :goto_3
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 660
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v5

    move v0, v2

    .line 663
    :goto_4
    if-ltz v0, :cond_4

    aget v2, p2, v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    if-le v2, v5, :cond_4

    .line 664
    add-int/lit8 v0, v0, -0x1

    goto :goto_4

    .line 667
    :cond_4
    if-gez v0, :cond_5

    .line 686
    if-eqz v1, :cond_0

    .line 687
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 676
    :cond_5
    :try_start_3
    aget v2, p2, v0

    if-ne v2, v5, :cond_8

    .line 678
    rem-int/lit8 v0, v0, 0x2

    if-nez v0, :cond_7

    invoke-static {v5, v4}, Ljava/lang/Math;->min(II)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result v0

    .line 686
    :goto_5
    if-eqz v1, :cond_6

    .line 687
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_6
    move v6, v0

    goto :goto_0

    :cond_7
    move v0, v6

    .line 678
    goto :goto_5

    :cond_8
    move v2, v0

    .line 684
    goto :goto_3

    .line 686
    :cond_9
    if-eqz v1, :cond_0

    .line 687
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 686
    :catchall_1
    move-exception v0

    move-object v1, v7

    goto :goto_2
.end method

.method private static a(Landroid/content/Context;Ljava/util/List;[I)I
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/b/bc;",
            ">;[I)I"
        }
    .end annotation

    .prologue
    const/4 v1, -0x1

    .line 618
    if-nez p1, :cond_0

    move v0, v1

    .line 630
    :goto_0
    return v0

    .line 623
    :cond_0
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/b/bc;

    .line 624
    invoke-static {p0, v0, p2}, Lcom/facebook/b/ay;->a(Landroid/content/Context;Lcom/facebook/b/bc;[I)I

    move-result v0

    .line 625
    if-eq v0, v1, :cond_1

    goto :goto_0

    :cond_2
    move v0, v1

    .line 630
    goto :goto_0
.end method

.method public static a(Landroid/content/Intent;)I
    .locals 2

    .prologue
    .line 517
    const-string/jumbo v0, "com.facebook.platform.protocol.PROTOCOL_VERSION"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public static a(Landroid/content/Context;)Landroid/content/Intent;
    .locals 5

    .prologue
    .line 411
    sget-object v0, Lcom/facebook/b/ay;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/b/bc;

    .line 412
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    invoke-virtual {v0}, Lcom/facebook/b/bc;->a()Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v4, "com.facebook.katana.platform.TokenRefreshService"

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    .line 415
    invoke-static {p0, v2, v0}, Lcom/facebook/b/ay;->b(Landroid/content/Context;Landroid/content/Intent;Lcom/facebook/b/bc;)Landroid/content/Intent;

    move-result-object v0

    .line 417
    if-eqz v0, :cond_0

    .line 421
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static a(Landroid/content/Context;Landroid/content/Intent;Lcom/facebook/b/bc;)Landroid/content/Intent;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 340
    if-nez p1, :cond_1

    move-object p1, v0

    .line 353
    :cond_0
    :goto_0
    return-object p1

    .line 344
    :cond_1
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, p1, v2}, Landroid/content/pm/PackageManager;->resolveActivity(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;

    move-result-object v1

    .line 345
    if-nez v1, :cond_2

    move-object p1, v0

    .line 346
    goto :goto_0

    .line 349
    :cond_2
    iget-object v1, v1, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v1, v1, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    invoke-virtual {p2, p0, v1}, Lcom/facebook/b/bc;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    move-object p1, v0

    .line 350
    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;ZLcom/facebook/ct;)Landroid/content/Intent;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Z",
            "Lcom/facebook/ct;",
            ")",
            "Landroid/content/Intent;"
        }
    .end annotation

    .prologue
    .line 375
    sget-object v0, Lcom/facebook/b/ay;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/b/bc;

    .line 376
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    invoke-virtual {v0}, Lcom/facebook/b/bc;->a()Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v4, "com.facebook.katana.ProxyAuth"

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    const-string/jumbo v3, "client_id"

    invoke-virtual {v2, v3, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    .line 380
    invoke-static {p2}, Lcom/facebook/b/bp;->a(Ljava/util/Collection;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 381
    const-string/jumbo v3, "scope"

    const-string/jumbo v4, ","

    invoke-static {v4, p2}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 383
    :cond_1
    invoke-static {p3}, Lcom/facebook/b/bp;->a(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 384
    const-string/jumbo v3, "e2e"

    invoke-virtual {v2, v3, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 387
    :cond_2
    const-string/jumbo v3, "response_type"

    const-string/jumbo v4, "token"

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 388
    const-string/jumbo v3, "return_scopes"

    const-string/jumbo v4, "true"

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 389
    const-string/jumbo v3, "default_audience"

    invoke-virtual {p5}, Lcom/facebook/ct;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 391
    invoke-static {}, Lcom/facebook/cx;->g()Z

    move-result v3

    if-nez v3, :cond_3

    .line 393
    const-string/jumbo v3, "legacy_override"

    const-string/jumbo v4, "v2.1"

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 396
    if-eqz p4, :cond_3

    .line 397
    const-string/jumbo v3, "auth_type"

    const-string/jumbo v4, "rerequest"

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 401
    :cond_3
    invoke-static {p0, v2, v0}, Lcom/facebook/b/ay;->a(Landroid/content/Context;Landroid/content/Intent;Lcom/facebook/b/bc;)Landroid/content/Intent;

    move-result-object v0

    .line 403
    if-eqz v0, :cond_0

    .line 407
    :goto_0
    return-object v0

    :cond_4
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a(Lcom/facebook/b/bc;)Landroid/net/Uri;
    .locals 2

    .prologue
    .line 695
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "content://"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/facebook/b/bc;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ".provider.PlatformProvider/versions"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/os/Bundle;)Ljava/lang/Exception;
    .locals 3

    .prologue
    .line 589
    if-nez p0, :cond_0

    .line 590
    const/4 v0, 0x0

    .line 603
    :goto_0
    return-object v0

    .line 595
    :cond_0
    const-string/jumbo v0, "com.facebook.platform.status.ERROR_TYPE"

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 596
    const-string/jumbo v1, "com.facebook.platform.status.ERROR_DESCRIPTION"

    invoke-virtual {p0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 598
    if-eqz v0, :cond_1

    const-string/jumbo v2, "UserCanceled"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 599
    new-instance v0, Lcom/facebook/as;

    invoke-direct {v0, v1}, Lcom/facebook/as;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 603
    :cond_1
    new-instance v0, Lcom/facebook/aq;

    invoke-direct {v0, v1}, Lcom/facebook/aq;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static a(I)Z
    .locals 2

    .prologue
    .line 463
    sget-object v0, Lcom/facebook/b/ay;->d:Ljava/util/List;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x133529d

    if-lt p0, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(Landroid/content/Context;)Landroid/content/Intent;
    .locals 4

    .prologue
    .line 504
    sget-object v0, Lcom/facebook/b/ay;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/b/bc;

    .line 505
    new-instance v2, Landroid/content/Intent;

    const-string/jumbo v3, "com.facebook.platform.PLATFORM_SERVICE"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/facebook/b/bc;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    const-string/jumbo v3, "android.intent.category.DEFAULT"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    .line 508
    invoke-static {p0, v2, v0}, Lcom/facebook/b/ay;->b(Landroid/content/Context;Landroid/content/Intent;Lcom/facebook/b/bc;)Landroid/content/Intent;

    move-result-object v0

    .line 509
    if-eqz v0, :cond_0

    .line 513
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static b(Landroid/content/Context;Landroid/content/Intent;Lcom/facebook/b/bc;)Landroid/content/Intent;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 357
    if-nez p1, :cond_1

    move-object p1, v0

    .line 370
    :cond_0
    :goto_0
    return-object p1

    .line 361
    :cond_1
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, p1, v2}, Landroid/content/pm/PackageManager;->resolveService(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;

    move-result-object v1

    .line 362
    if-nez v1, :cond_2

    move-object p1, v0

    .line 363
    goto :goto_0

    .line 366
    :cond_2
    iget-object v1, v1, Landroid/content/pm/ResolveInfo;->serviceInfo:Landroid/content/pm/ServiceInfo;

    iget-object v1, v1, Landroid/content/pm/ServiceInfo;->packageName:Ljava/lang/String;

    invoke-virtual {p2, p0, v1}, Lcom/facebook/b/bc;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    move-object p1, v0

    .line 367
    goto :goto_0
.end method

.method private static b()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/b/bc;",
            ">;"
        }
    .end annotation

    .prologue
    .line 314
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 317
    sget-object v1, Lcom/facebook/b/ay;->a:Lcom/facebook/b/bc;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 318
    new-instance v1, Lcom/facebook/b/bd;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/facebook/b/bd;-><init>(Lcom/facebook/b/az;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 320
    return-object v0
.end method

.method public static b(Landroid/content/Intent;)Ljava/util/UUID;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 521
    if-nez p0, :cond_1

    .line 542
    :cond_0
    :goto_0
    return-object v1

    .line 524
    :cond_1
    invoke-static {p0}, Lcom/facebook/b/ay;->a(Landroid/content/Intent;)I

    move-result v0

    .line 526
    invoke-static {v0}, Lcom/facebook/b/ay;->a(I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 527
    const-string/jumbo v0, "com.facebook.platform.protocol.BRIDGE_ARGS"

    invoke-virtual {p0, v0}, Landroid/content/Intent;->getBundleExtra(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    .line 528
    if-eqz v0, :cond_3

    .line 529
    const-string/jumbo v2, "action_id"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 536
    :goto_1
    if-eqz v0, :cond_0

    .line 538
    :try_start_0
    invoke-static {v0}, Ljava/util/UUID;->fromString(Ljava/lang/String;)Ljava/util/UUID;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    goto :goto_0

    .line 532
    :cond_2
    const-string/jumbo v0, "com.facebook.platform.protocol.CALL_ID"

    invoke-virtual {p0, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 539
    :catch_0
    move-exception v0

    goto :goto_0

    :cond_3
    move-object v0, v1

    goto :goto_1
.end method

.method public static c(Landroid/content/Intent;)Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 546
    invoke-static {p0}, Lcom/facebook/b/ay;->a(Landroid/content/Intent;)I

    move-result v0

    .line 547
    invoke-static {v0}, Lcom/facebook/b/ay;->a(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 548
    const/4 v0, 0x0

    .line 551
    :goto_0
    return-object v0

    :cond_0
    const-string/jumbo v0, "com.facebook.platform.protocol.BRIDGE_ARGS"

    invoke-virtual {p0, v0}, Landroid/content/Intent;->getBundleExtra(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    goto :goto_0
.end method

.method private static c()Ljava/util/Map;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/b/bc;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 324
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 326
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 327
    new-instance v2, Lcom/facebook/b/bb;

    const/4 v3, 0x0

    invoke-direct {v2, v3}, Lcom/facebook/b/bb;-><init>(Lcom/facebook/b/az;)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 330
    const-string/jumbo v2, "com.facebook.platform.action.request.OGACTIONPUBLISH_DIALOG"

    sget-object v3, Lcom/facebook/b/ay;->b:Ljava/util/List;

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 331
    const-string/jumbo v2, "com.facebook.platform.action.request.FEED_DIALOG"

    sget-object v3, Lcom/facebook/b/ay;->b:Ljava/util/List;

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 332
    const-string/jumbo v2, "com.facebook.platform.action.request.LIKE_DIALOG"

    sget-object v3, Lcom/facebook/b/ay;->b:Ljava/util/List;

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 333
    const-string/jumbo v2, "com.facebook.platform.action.request.MESSAGE_DIALOG"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 334
    const-string/jumbo v2, "com.facebook.platform.action.request.OGMESSAGEPUBLISH_DIALOG"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 336
    return-object v0
.end method

.method public static d(Landroid/content/Intent;)Landroid/os/Bundle;
    .locals 2

    .prologue
    .line 555
    invoke-static {p0}, Lcom/facebook/b/ay;->a(Landroid/content/Intent;)I

    move-result v1

    .line 556
    invoke-virtual {p0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 557
    invoke-static {v1}, Lcom/facebook/b/ay;->a(I)Z

    move-result v1

    if-eqz v1, :cond_0

    if-nez v0, :cond_1

    .line 561
    :cond_0
    :goto_0
    return-object v0

    :cond_1
    const-string/jumbo v1, "com.facebook.platform.protocol.RESULT_ARGS"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    goto :goto_0
.end method

.method public static e(Landroid/content/Intent;)Z
    .locals 2

    .prologue
    .line 565
    invoke-static {p0}, Lcom/facebook/b/ay;->c(Landroid/content/Intent;)Landroid/os/Bundle;

    move-result-object v0

    .line 566
    if-eqz v0, :cond_0

    .line 567
    const-string/jumbo v1, "error"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    .line 569
    :goto_0
    return v0

    :cond_0
    const-string/jumbo v0, "com.facebook.platform.status.ERROR_TYPE"

    invoke-virtual {p0, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    goto :goto_0
.end method

.method public static f(Landroid/content/Intent;)Landroid/os/Bundle;
    .locals 2

    .prologue
    .line 574
    invoke-static {p0}, Lcom/facebook/b/ay;->e(Landroid/content/Intent;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 575
    const/4 v0, 0x0

    .line 585
    :goto_0
    return-object v0

    .line 578
    :cond_0
    invoke-static {p0}, Lcom/facebook/b/ay;->c(Landroid/content/Intent;)Landroid/os/Bundle;

    move-result-object v0

    .line 579
    if-eqz v0, :cond_1

    .line 580
    const-string/jumbo v1, "error"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    goto :goto_0

    .line 585
    :cond_1
    invoke-virtual {p0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    goto :goto_0
.end method
