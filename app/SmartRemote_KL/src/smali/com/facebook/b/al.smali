.class abstract Lcom/facebook/b/al;
.super Ljava/lang/Object;


# instance fields
.field protected a:Ljava/lang/String;

.field b:Lcom/facebook/at;

.field final synthetic c:Lcom/facebook/b/aa;

.field private d:Lcom/facebook/Request;


# direct methods
.method protected constructor <init>(Lcom/facebook/b/aa;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1253
    iput-object p1, p0, Lcom/facebook/b/al;->c:Lcom/facebook/b/aa;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1254
    iput-object p2, p0, Lcom/facebook/b/al;->a:Ljava/lang/String;

    .line 1255
    return-void
.end method


# virtual methods
.method protected a(Lcom/facebook/Request;)V
    .locals 1

    .prologue
    .line 1262
    iput-object p1, p0, Lcom/facebook/b/al;->d:Lcom/facebook/Request;

    .line 1264
    const-string/jumbo v0, "v2.1"

    invoke-virtual {p1, v0}, Lcom/facebook/Request;->a(Ljava/lang/String;)V

    .line 1265
    new-instance v0, Lcom/facebook/b/am;

    invoke-direct {v0, p0}, Lcom/facebook/b/am;-><init>(Lcom/facebook/b/al;)V

    invoke-virtual {p1, v0}, Lcom/facebook/Request;->a(Lcom/facebook/bm;)V

    .line 1276
    return-void
.end method

.method protected a(Lcom/facebook/at;)V
    .locals 6

    .prologue
    .line 1279
    sget-object v0, Lcom/facebook/bb;->a:Lcom/facebook/bb;

    invoke-static {}, Lcom/facebook/b/aa;->g()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "Error running request for object \'%s\' : %s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/facebook/b/al;->a:Ljava/lang/String;

    aput-object v5, v3, v4

    const/4 v4, 0x1

    aput-object p1, v3, v4

    invoke-static {v0, v1, v2, v3}, Lcom/facebook/b/ax;->a(Lcom/facebook/bb;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1282
    return-void
.end method

.method a(Lcom/facebook/bt;)V
    .locals 1

    .prologue
    .line 1258
    iget-object v0, p0, Lcom/facebook/b/al;->d:Lcom/facebook/Request;

    invoke-virtual {p1, v0}, Lcom/facebook/bt;->a(Lcom/facebook/Request;)Z

    .line 1259
    return-void
.end method

.method protected abstract a(Lcom/facebook/bz;)V
.end method
