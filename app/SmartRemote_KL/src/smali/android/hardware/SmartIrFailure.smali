.class public Landroid/hardware/SmartIrFailure;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<*>;"
        }
    .end annotation
.end field

.field public static final MSG_RECEIVE_CANCELED:Ljava/lang/String; = "Receiving canceled"

.field public static final MSG_RECEIVE_CORRUPT_DATA:Ljava/lang/String; = "Receiving corrupt data"

.field public static final MSG_RECEIVE_HAL_PROBLEM:Ljava/lang/String; = "Receiving HAL problem"

.field public static final MSG_RECEIVE_TIMEOUT:Ljava/lang/String; = "Receiving timeout"

.field public static final MSG_TRANSMIT_CANCELED:Ljava/lang/String; = "Transmit canceled"

.field public static final MSG_TRANSMIT_HAL_PROBLEM:Ljava/lang/String; = "Transmit HAL problem"

.field public static final MSG_TRANSMIT_MODE_UNSUPPORTED:Ljava/lang/String; = "Transmit mode unsupported"

.field public static final STATUS_RECEIVE_CANCELED:I = 0x68

.field public static final STATUS_RECEIVE_CORRUPT_DATA:I = 0x67

.field public static final STATUS_RECEIVE_HAL_PROBLEM:I = 0x65

.field public static final STATUS_RECEIVE_TIMEOUT:I = 0x66

.field public static final STATUS_TRANSMIT_CANCELED:I = 0x3

.field public static final STATUS_TRANSMIT_HAL_PROBLEM:I = 0x1

.field public static final STATUS_TRANSMIT_MODE_UNSUPPORTED:I = 0x2


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 16
    const/4 v0, 0x0

    sput-object v0, Landroid/hardware/SmartIrFailure;->CREATOR:Landroid/os/Parcelable$Creator;

    .line 30
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public constructor <init>(ILjava/lang/String;)V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 9
    const/4 v0, 0x0

    return v0
.end method

.method public getMessage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 10
    const/4 v0, 0x0

    return-object v0
.end method

.method public getStatusCode()I
    .locals 1

    .prologue
    .line 11
    const/4 v0, 0x0

    return v0
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .locals 0

    .prologue
    .line 12
    return-void
.end method

.method public setMessage(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 13
    return-void
.end method

.method public setStatusCode(I)V
    .locals 0

    .prologue
    .line 14
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .prologue
    .line 15
    return-void
.end method
