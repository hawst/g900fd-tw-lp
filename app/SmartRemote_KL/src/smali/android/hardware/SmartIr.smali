.class public Landroid/hardware/SmartIr;
.super Ljava/lang/Object;


# static fields
.field public static final RESPONSE_ALL_OK:I = 0x0

.field public static final RESPONSE_INIT_FAILURE:I = 0x1

.field public static final RESPONSE_NULL_CALLBACK:I = 0x6a

.field public static final RESPONSE_READ_MODE_FAILURE:I = 0x3

.field public static final RESPONSE_READ_MSG_FAILURE:I = 0x5

.field public static final RESPONSE_RECEIVE_ALREADY_CANCELED:I = 0x69

.field public static final RESPONSE_RECEIVE_FAILURE:I = 0x67

.field public static final RESPONSE_RECEIVE_IN_PROGRESS:I = 0x68

.field public static final RESPONSE_RECEIVE_NOT_ENOUGH_MEMORY:I = 0x66

.field public static final RESPONSE_TRANSMIT_ALREADY_CANCELED:I = 0x65

.field public static final RESPONSE_TRANSMIT_FAILURE:I = 0x7

.field public static final RESPONSE_TRANSMIT_IN_PROGRESS:I = 0x64

.field public static final RESPONSE_WRITE_BITS_FAILURE:I = 0x2

.field public static final RESPONSE_WRITE_MODE_FAILURE:I = 0x4

.field public static final RESPONSE_WRITE_MSG_FAILURE:I = 0x6


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public cancelContinuousTransmit()I
    .locals 1

    .prologue
    .line 36
    const/4 v0, 0x0

    return v0
.end method

.method public cancelReceiving()I
    .locals 1

    .prologue
    .line 41
    const/4 v0, 0x0

    return v0
.end method

.method public hasIrEmitter()Z
    .locals 1

    .prologue
    .line 33
    const/4 v0, 0x0

    return v0
.end method

.method public receive(I)I
    .locals 1

    .prologue
    .line 40
    const/4 v0, 0x0

    return v0
.end method

.method public registerReceiveCallback(Landroid/hardware/SmartIr$ReceiveCallback;Landroid/os/Handler;)I
    .locals 1

    .prologue
    .line 42
    const/4 v0, 0x0

    return v0
.end method

.method public registerTransmitCallback(Landroid/hardware/SmartIr$TransmitCallback;Landroid/os/Handler;)I
    .locals 1

    .prologue
    .line 37
    const/4 v0, 0x0

    return v0
.end method

.method public transmit(ILjava/lang/String;Ljava/lang/String;III)I
    .locals 1

    .prologue
    .line 35
    const/4 v0, 0x0

    return v0
.end method

.method public unregisterReceiveCallback(Landroid/hardware/SmartIr$ReceiveCallback;)I
    .locals 1

    .prologue
    .line 43
    const/4 v0, 0x0

    return v0
.end method

.method public unregisterTransmitCallback(Landroid/hardware/SmartIr$TransmitCallback;)I
    .locals 1

    .prologue
    .line 38
    const/4 v0, 0x0

    return v0
.end method
