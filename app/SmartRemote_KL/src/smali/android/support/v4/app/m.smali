.class public Landroid/support/v4/app/m;
.super Landroid/support/v4/a/c;


# direct methods
.method private static a(Landroid/support/v4/app/dh;)Landroid/support/v4/app/p;
    .locals 1

    .prologue
    .line 210
    const/4 v0, 0x0

    .line 211
    if-eqz p0, :cond_0

    .line 212
    new-instance v0, Landroid/support/v4/app/n;

    invoke-direct {v0, p0}, Landroid/support/v4/app/n;-><init>(Landroid/support/v4/app/dh;)V

    .line 214
    :cond_0
    return-object v0
.end method

.method public static a(Landroid/app/Activity;)V
    .locals 2

    .prologue
    .line 141
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_0

    .line 142
    invoke-static {p0}, Landroid/support/v4/app/s;->a(Landroid/app/Activity;)V

    .line 146
    :goto_0
    return-void

    .line 144
    :cond_0
    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    goto :goto_0
.end method

.method public static a(Landroid/app/Activity;Landroid/support/v4/app/dh;)V
    .locals 2

    .prologue
    .line 175
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_0

    .line 176
    invoke-static {p1}, Landroid/support/v4/app/m;->a(Landroid/support/v4/app/dh;)Landroid/support/v4/app/p;

    move-result-object v0

    invoke-static {p0, v0}, Landroid/support/v4/app/o;->a(Landroid/app/Activity;Landroid/support/v4/app/p;)V

    .line 178
    :cond_0
    return-void
.end method

.method public static b(Landroid/app/Activity;)V
    .locals 2

    .prologue
    .line 158
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_0

    .line 159
    invoke-static {p0}, Landroid/support/v4/app/o;->a(Landroid/app/Activity;)V

    .line 163
    :goto_0
    return-void

    .line 161
    :cond_0
    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    goto :goto_0
.end method

.method public static b(Landroid/app/Activity;Landroid/support/v4/app/dh;)V
    .locals 2

    .prologue
    .line 191
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_0

    .line 192
    invoke-static {p1}, Landroid/support/v4/app/m;->a(Landroid/support/v4/app/dh;)Landroid/support/v4/app/p;

    move-result-object v0

    invoke-static {p0, v0}, Landroid/support/v4/app/o;->b(Landroid/app/Activity;Landroid/support/v4/app/p;)V

    .line 194
    :cond_0
    return-void
.end method

.method public static c(Landroid/app/Activity;)V
    .locals 2

    .prologue
    .line 197
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_0

    .line 198
    invoke-static {p0}, Landroid/support/v4/app/o;->b(Landroid/app/Activity;)V

    .line 200
    :cond_0
    return-void
.end method

.method public static d(Landroid/app/Activity;)V
    .locals 2

    .prologue
    .line 203
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_0

    .line 204
    invoke-static {p0}, Landroid/support/v4/app/o;->c(Landroid/app/Activity;)V

    .line 206
    :cond_0
    return-void
.end method
