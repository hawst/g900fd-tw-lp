.class Landroid/support/v4/app/n;
.super Landroid/support/v4/app/p;


# instance fields
.field private a:Landroid/support/v4/app/dh;


# direct methods
.method public constructor <init>(Landroid/support/v4/app/dh;)V
    .locals 0

    .prologue
    .line 222
    invoke-direct {p0}, Landroid/support/v4/app/p;-><init>()V

    .line 223
    iput-object p1, p0, Landroid/support/v4/app/n;->a:Landroid/support/v4/app/dh;

    .line 224
    return-void
.end method


# virtual methods
.method public a(Landroid/view/View;Landroid/graphics/Matrix;Landroid/graphics/RectF;)Landroid/os/Parcelable;
    .locals 1

    .prologue
    .line 253
    iget-object v0, p0, Landroid/support/v4/app/n;->a:Landroid/support/v4/app/dh;

    invoke-virtual {v0, p1, p2, p3}, Landroid/support/v4/app/dh;->a(Landroid/view/View;Landroid/graphics/Matrix;Landroid/graphics/RectF;)Landroid/os/Parcelable;

    move-result-object v0

    return-object v0
.end method

.method public a(Landroid/content/Context;Landroid/os/Parcelable;)Landroid/view/View;
    .locals 1

    .prologue
    .line 259
    iget-object v0, p0, Landroid/support/v4/app/n;->a:Landroid/support/v4/app/dh;

    invoke-virtual {v0, p1, p2}, Landroid/support/v4/app/dh;->a(Landroid/content/Context;Landroid/os/Parcelable;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 242
    iget-object v0, p0, Landroid/support/v4/app/n;->a:Landroid/support/v4/app/dh;

    invoke-virtual {v0, p1}, Landroid/support/v4/app/dh;->a(Ljava/util/List;)V

    .line 243
    return-void
.end method

.method public a(Ljava/util/List;Ljava/util/List;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;",
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 229
    iget-object v0, p0, Landroid/support/v4/app/n;->a:Landroid/support/v4/app/dh;

    invoke-virtual {v0, p1, p2, p3}, Landroid/support/v4/app/dh;->a(Ljava/util/List;Ljava/util/List;Ljava/util/List;)V

    .line 231
    return-void
.end method

.method public a(Ljava/util/List;Ljava/util/Map;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Landroid/view/View;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 247
    iget-object v0, p0, Landroid/support/v4/app/n;->a:Landroid/support/v4/app/dh;

    invoke-virtual {v0, p1, p2}, Landroid/support/v4/app/dh;->a(Ljava/util/List;Ljava/util/Map;)V

    .line 248
    return-void
.end method

.method public b(Ljava/util/List;Ljava/util/List;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;",
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 236
    iget-object v0, p0, Landroid/support/v4/app/n;->a:Landroid/support/v4/app/dh;

    invoke-virtual {v0, p1, p2, p3}, Landroid/support/v4/app/dh;->b(Ljava/util/List;Ljava/util/List;Ljava/util/List;)V

    .line 238
    return-void
.end method
