.class public interface abstract Landroid/support/v4/c/a/b;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/view/MenuItem;


# virtual methods
.method public abstract collapseActionView()Z
.end method

.method public abstract expandActionView()Z
.end method

.method public abstract getActionView()Landroid/view/View;
.end method

.method public abstract getSupportActionProvider()Landroid/support/v4/view/n;
.end method

.method public abstract isActionViewExpanded()Z
.end method

.method public abstract setActionView(I)Landroid/view/MenuItem;
.end method

.method public abstract setActionView(Landroid/view/View;)Landroid/view/MenuItem;
.end method

.method public abstract setShowAsAction(I)V
.end method

.method public abstract setShowAsActionFlags(I)Landroid/view/MenuItem;
.end method

.method public abstract setSupportActionProvider(Landroid/support/v4/view/n;)Landroid/support/v4/c/a/b;
.end method

.method public abstract setSupportOnActionExpandListener(Landroid/support/v4/view/am;)Landroid/support/v4/c/a/b;
.end method
