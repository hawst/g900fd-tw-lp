.class Landroid/support/v4/view/bm;
.super Ljava/lang/Object;


# direct methods
.method public static a(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 33
    invoke-virtual {p0}, Landroid/view/View;->requestApplyInsets()V

    .line 34
    return-void
.end method

.method public static a(Landroid/view/View;F)V
    .locals 0

    .prologue
    .line 37
    invoke-virtual {p0, p1}, Landroid/view/View;->setElevation(F)V

    .line 38
    return-void
.end method

.method public static a(Landroid/view/View;Landroid/support/v4/view/au;)V
    .locals 1

    .prologue
    .line 54
    new-instance v0, Landroid/support/v4/view/bn;

    invoke-direct {v0, p1}, Landroid/support/v4/view/bn;-><init>(Landroid/support/v4/view/au;)V

    invoke-virtual {p0, v0}, Landroid/view/View;->setOnApplyWindowInsetsListener(Landroid/view/View$OnApplyWindowInsetsListener;)V

    .line 65
    return-void
.end method

.method public static b(Landroid/view/View;)F
    .locals 1

    .prologue
    .line 41
    invoke-virtual {p0}, Landroid/view/View;->getElevation()F

    move-result v0

    return v0
.end method
