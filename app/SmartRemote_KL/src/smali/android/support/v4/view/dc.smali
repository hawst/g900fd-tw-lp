.class Landroid/support/v4/view/dc;
.super Landroid/support/v4/view/da;


# instance fields
.field b:Ljava/util/WeakHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/WeakHashMap",
            "<",
            "Landroid/view/View;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 335
    invoke-direct {p0}, Landroid/support/v4/view/da;-><init>()V

    .line 336
    const/4 v0, 0x0

    iput-object v0, p0, Landroid/support/v4/view/dc;->b:Ljava/util/WeakHashMap;

    .line 497
    return-void
.end method


# virtual methods
.method public a(Landroid/support/v4/view/cy;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 435
    invoke-static {p2}, Landroid/support/v4/view/di;->a(Landroid/view/View;)V

    .line 436
    return-void
.end method

.method public a(Landroid/support/v4/view/cy;Landroid/view/View;F)V
    .locals 0

    .prologue
    .line 345
    invoke-static {p2, p3}, Landroid/support/v4/view/di;->a(Landroid/view/View;F)V

    .line 346
    return-void
.end method

.method public a(Landroid/support/v4/view/cy;Landroid/view/View;J)V
    .locals 1

    .prologue
    .line 340
    invoke-static {p2, p3, p4}, Landroid/support/v4/view/di;->a(Landroid/view/View;J)V

    .line 341
    return-void
.end method

.method public a(Landroid/support/v4/view/cy;Landroid/view/View;Landroid/support/v4/view/do;)V
    .locals 1

    .prologue
    .line 475
    const/high16 v0, 0x7e000000

    invoke-virtual {p2, v0, p3}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 476
    new-instance v0, Landroid/support/v4/view/dd;

    invoke-direct {v0, p1}, Landroid/support/v4/view/dd;-><init>(Landroid/support/v4/view/cy;)V

    invoke-static {p2, v0}, Landroid/support/v4/view/di;->a(Landroid/view/View;Landroid/support/v4/view/do;)V

    .line 477
    return-void
.end method

.method public a(Landroid/support/v4/view/cy;Landroid/view/View;Landroid/view/animation/Interpolator;)V
    .locals 0

    .prologue
    .line 365
    invoke-static {p2, p3}, Landroid/support/v4/view/di;->a(Landroid/view/View;Landroid/view/animation/Interpolator;)V

    .line 366
    return-void
.end method

.method public b(Landroid/support/v4/view/cy;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 470
    invoke-static {p2}, Landroid/support/v4/view/di;->b(Landroid/view/View;)V

    .line 471
    return-void
.end method

.method public b(Landroid/support/v4/view/cy;Landroid/view/View;F)V
    .locals 0

    .prologue
    .line 350
    invoke-static {p2, p3}, Landroid/support/v4/view/di;->b(Landroid/view/View;F)V

    .line 351
    return-void
.end method

.method public c(Landroid/support/v4/view/cy;Landroid/view/View;F)V
    .locals 0

    .prologue
    .line 355
    invoke-static {p2, p3}, Landroid/support/v4/view/di;->c(Landroid/view/View;F)V

    .line 356
    return-void
.end method

.method public d(Landroid/support/v4/view/cy;Landroid/view/View;F)V
    .locals 0

    .prologue
    .line 425
    invoke-static {p2, p3}, Landroid/support/v4/view/di;->d(Landroid/view/View;F)V

    .line 426
    return-void
.end method
