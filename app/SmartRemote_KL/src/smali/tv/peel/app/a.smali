.class public Ltv/peel/app/a;
.super Ljava/lang/Object;


# static fields
.field private static final b:Ljava/lang/String;


# instance fields
.field protected a:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21
    const-class v0, Ltv/peel/app/a;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Ltv/peel/app/a;->b:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 0

    .prologue
    .line 55
    return-void
.end method

.method public a(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 32
    iput-object p1, p0, Ltv/peel/app/a;->a:Landroid/content/Context;

    .line 33
    sget-object v0, Lcom/peel/c/a;->a:Lcom/peel/c/e;

    invoke-static {v0, p1}, Lcom/peel/c/a;->a(Lcom/peel/c/e;Ljava/lang/Object;)V

    .line 34
    sget-object v0, Lcom/peel/c/a;->b:Lcom/peel/c/e;

    invoke-virtual {p0}, Ltv/peel/app/a;->b()Lcom/peel/c/b;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/peel/c/a;->a(Lcom/peel/c/e;Ljava/lang/Object;)V

    .line 35
    sget-object v0, Lcom/peel/c/a;->c:Lcom/peel/c/e;

    sget-object v1, Lcom/peel/ui/c;->b:Lcom/peel/c/d;

    invoke-static {v0, v1}, Lcom/peel/c/a;->a(Lcom/peel/c/e;Ljava/lang/Object;)V

    .line 36
    sget-object v0, Lcom/peel/c/a;->d:Lcom/peel/c/e;

    invoke-virtual {p0}, Ltv/peel/app/a;->d()Lcom/peel/ui/ni;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/peel/c/a;->a(Lcom/peel/c/e;Ljava/lang/Object;)V

    .line 37
    sget-object v0, Lcom/peel/c/a;->e:Lcom/peel/c/e;

    invoke-virtual {p0}, Ltv/peel/app/a;->e()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/peel/c/a;->a(Lcom/peel/c/e;Ljava/lang/Object;)V

    .line 38
    sget-object v0, Lcom/peel/c/a;->f:Lcom/peel/c/e;

    invoke-virtual {p0}, Ltv/peel/app/a;->c()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/peel/c/a;->a(Lcom/peel/c/e;Ljava/lang/Object;)V

    .line 40
    new-instance v0, Landroid/webkit/WebView;

    invoke-direct {v0, p1}, Landroid/webkit/WebView;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    invoke-virtual {v0}, Landroid/webkit/WebSettings;->getUserAgentString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/peel/util/b/a;->c:Ljava/lang/String;

    .line 43
    :try_start_0
    new-instance v0, Lcom/squareup/a/r;

    invoke-direct {v0}, Lcom/squareup/a/r;-><init>()V

    invoke-static {v0}, Ljava/net/URL;->setURLStreamHandlerFactory(Ljava/net/URLStreamHandlerFactory;)V
    :try_end_0
    .catch Ljava/lang/Error; {:try_start_0 .. :try_end_0} :catch_0

    .line 46
    :goto_0
    const-string/jumbo v0, "http://countries-partners.epg.peel.com/"

    const-string/jumbo v1, "http://countries-partners.epg.peelchina.com/"

    invoke-static {v0, v1}, Lcom/peel/content/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 47
    invoke-virtual {p0}, Ltv/peel/app/a;->f()V

    .line 48
    return-void

    .line 44
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method protected b()Lcom/peel/c/b;
    .locals 1

    .prologue
    .line 58
    sget-object v0, Lcom/peel/c/b;->c:Lcom/peel/c/b;

    return-object v0
.end method

.method protected c()I
    .locals 1

    .prologue
    .line 62
    const/4 v0, 0x6

    return v0
.end method

.method protected d()Lcom/peel/ui/ni;
    .locals 1

    .prologue
    .line 66
    new-instance v0, Lcom/peel/ui/ni;

    invoke-direct {v0}, Lcom/peel/ui/ni;-><init>()V

    return-object v0
.end method

.method protected e()Z
    .locals 1

    .prologue
    .line 70
    const/4 v0, 0x1

    return v0
.end method

.method protected f()V
    .locals 6

    .prologue
    .line 74
    const-string/jumbo v0, "https://insights.peel-prod.com"

    sput-object v0, Lcom/peel/util/a/c;->a:Ljava/lang/String;

    .line 75
    iget-object v0, p0, Ltv/peel/app/a;->a:Landroid/content/Context;

    const-string/jumbo v1, "AKIAJSJI6NCNBCVZ4GZQ"

    const-string/jumbo v2, "b5iWoTI+KxBuGAF8IW7HCiGG35+EUAc6M5Un47BL"

    const-string/jumbo v3, "peel_insights"

    const-string/jumbo v4, ""

    const/4 v5, 0x0

    invoke-static/range {v0 .. v5}, Lcom/peel/a/a/a/g;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/peel/a/a/a/a/a;)V

    .line 78
    invoke-static {}, Lcom/peel/util/a/f;->a()Lcom/peel/util/a/f;

    move-result-object v0

    iget-object v1, p0, Ltv/peel/app/a;->a:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/peel/util/a/f;->a(Landroid/content/Context;)V

    .line 79
    return-void
.end method
