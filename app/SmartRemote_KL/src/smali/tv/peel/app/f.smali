.class Ltv/peel/app/f;
.super Lcom/peel/util/s;


# instance fields
.field final synthetic a:Ltv/peel/app/c;


# direct methods
.method constructor <init>(Ltv/peel/app/c;)V
    .locals 0

    .prologue
    .line 136
    iput-object p1, p0, Ltv/peel/app/f;->a:Ltv/peel/app/c;

    invoke-direct {p0}, Lcom/peel/util/s;-><init>()V

    return-void
.end method


# virtual methods
.method public final varargs a(ILjava/lang/Object;[Ljava/lang/Object;)V
    .locals 11

    .prologue
    const/4 v0, 0x0

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 139
    if-ne v2, p1, :cond_0

    .line 140
    invoke-static {}, Ltv/peel/app/c;->b()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v4, "ContentEvents.LOADED received"

    invoke-static {v1, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 141
    invoke-static {}, Lcom/peel/util/bx;->c()Ljava/lang/String;

    move-result-object v1

    .line 143
    if-eqz v1, :cond_1

    iget-object v4, p0, Ltv/peel/app/f;->a:Ltv/peel/app/c;

    const-string/jumbo v5, "widget_pref"

    invoke-virtual {v4, v5, v3}, Ltv/peel/app/c;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v4

    const-string/jumbo v5, "notification"

    invoke-interface {v4, v5, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 144
    sget-object v4, Lcom/peel/util/v;->a:Lcom/peel/util/v;

    invoke-virtual {v4, v1}, Lcom/peel/util/v;->a(Ljava/lang/String;)V

    .line 149
    :goto_0
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    .line 151
    :try_start_0
    sget-object v4, Lcom/peel/content/a;->f:Lcom/peel/content/user/User;

    if-nez v4, :cond_2

    if-eqz v1, :cond_2

    .line 153
    iget-object v0, p0, Ltv/peel/app/f;->a:Ltv/peel/app/c;

    const/4 v1, 0x1

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Ltv/peel/app/c;->a(Ltv/peel/app/c;ZZ)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 231
    :cond_0
    :goto_1
    return-void

    .line 146
    :cond_1
    sget-object v1, Lcom/peel/util/v;->a:Lcom/peel/util/v;

    invoke-virtual {v1}, Lcom/peel/util/v;->a()V

    goto :goto_0

    .line 158
    :cond_2
    :try_start_1
    invoke-static {}, Lcom/peel/util/a/f;->a()Lcom/peel/util/a/f;

    move-result-object v4

    sget-object v5, Lcom/peel/content/a;->f:Lcom/peel/content/user/User;

    invoke-virtual {v5}, Lcom/peel/content/user/User;->x()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/peel/util/a/f;->a(Ljava/lang/String;)V

    .line 160
    invoke-static {}, Lcom/peel/content/a;->a()Lcom/peel/data/ContentRoom;

    move-result-object v6

    .line 161
    if-nez v6, :cond_3

    move-object v5, v0

    .line 162
    :goto_2
    if-nez v6, :cond_4

    .line 164
    :goto_3
    iget-object v4, p0, Ltv/peel/app/f;->a:Ltv/peel/app/c;

    invoke-static {v4}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v7

    .line 166
    if-nez v0, :cond_6

    .line 168
    const-string/jumbo v0, "is_setup_complete"

    const/4 v1, 0x0

    invoke-interface {v7, v0, v1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_5

    .line 169
    iget-object v0, p0, Ltv/peel/app/f;->a:Ltv/peel/app/c;

    const/4 v1, 0x1

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Ltv/peel/app/c;->a(Ltv/peel/app/c;ZZ)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    .line 226
    :catch_0
    move-exception v0

    .line 227
    invoke-static {}, Ltv/peel/app/c;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {}, Ltv/peel/app/c;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 228
    iget-object v0, p0, Ltv/peel/app/f;->a:Ltv/peel/app/c;

    invoke-static {v0}, Ltv/peel/app/c;->a(Ltv/peel/app/c;)V

    goto :goto_1

    .line 161
    :cond_3
    :try_start_2
    sget-object v4, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v6}, Lcom/peel/data/ContentRoom;->d()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/peel/control/am;->a(Ljava/lang/String;)Lcom/peel/control/RoomControl;

    move-result-object v4

    move-object v5, v4

    goto :goto_2

    .line 162
    :cond_4
    invoke-virtual {v6}, Lcom/peel/data/ContentRoom;->f()Ljava/util/List;

    move-result-object v0

    goto :goto_3

    .line 173
    :cond_5
    new-instance v0, Ljava/lang/RuntimeException;

    const-string/jumbo v1, "corrupted database"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 177
    :cond_6
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_7
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 178
    invoke-static {v0}, Lcom/peel/content/a;->b(Ljava/lang/String;)Lcom/peel/content/library/Library;

    move-result-object v0

    if-nez v0, :cond_7

    .line 180
    new-instance v0, Ljava/lang/RuntimeException;

    const-string/jumbo v1, "corrupted database"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 184
    :cond_8
    const-string/jumbo v0, "current_room"

    const/4 v4, 0x0

    invoke-interface {v7, v0, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 186
    if-eqz v0, :cond_9

    invoke-virtual {v6}, Lcom/peel/data/ContentRoom;->d()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_a

    .line 188
    :cond_9
    invoke-interface {v7}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v4, "last_activity"

    invoke-interface {v0, v4}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 191
    :cond_a
    if-eqz v5, :cond_b

    .line 193
    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v0, v5}, Lcom/peel/control/am;->b(Lcom/peel/control/RoomControl;)V

    .line 196
    :cond_b
    const-string/jumbo v0, "last_activity"

    const/4 v4, 0x0

    invoke-interface {v7, v0, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 197
    invoke-virtual {v5}, Lcom/peel/control/RoomControl;->d()Lcom/peel/control/a;

    move-result-object v0

    .line 198
    if-nez v0, :cond_11

    .line 199
    invoke-virtual {v5}, Lcom/peel/control/RoomControl;->c()[Lcom/peel/control/a;

    move-result-object v8

    .line 201
    if-eqz v8, :cond_11

    array-length v4, v8

    if-lez v4, :cond_11

    .line 202
    if-eqz v6, :cond_c

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v4

    if-lez v4, :cond_c

    .line 203
    array-length v9, v8

    move v4, v3

    :goto_4
    if-ge v4, v9, :cond_c

    aget-object v10, v8, v4

    .line 204
    invoke-virtual {v10}, Lcom/peel/control/a;->b()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v10, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_e

    .line 205
    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v0, v6}, Lcom/peel/control/am;->e(Ljava/lang/String;)Lcom/peel/control/a;

    move-result-object v0

    .line 211
    :cond_c
    if-nez v0, :cond_11

    .line 212
    const/4 v0, 0x0

    aget-object v0, v8, v0

    move-object v4, v0

    .line 216
    :goto_5
    const/4 v0, 0x0

    invoke-virtual {v5, v4, v0}, Lcom/peel/control/RoomControl;->a(Lcom/peel/control/a;I)Z

    .line 217
    invoke-virtual {v5, v4}, Lcom/peel/control/RoomControl;->c(Lcom/peel/control/a;)V

    .line 218
    const-string/jumbo v0, "setup_type"

    const/4 v6, 0x0

    invoke-interface {v7, v0, v6}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    if-ne v2, v0, :cond_f

    move v0, v2

    .line 220
    :goto_6
    if-nez v0, :cond_10

    if-eqz v4, :cond_d

    invoke-virtual {v5}, Lcom/peel/control/RoomControl;->c()[Lcom/peel/control/a;

    move-result-object v0

    array-length v0, v0

    if-nez v0, :cond_10

    :cond_d
    move v0, v2

    .line 223
    :goto_7
    if-eqz v0, :cond_0

    .line 224
    iget-object v0, p0, Ltv/peel/app/f;->a:Ltv/peel/app/c;

    const/4 v1, 0x1

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Ltv/peel/app/c;->a(Ltv/peel/app/c;ZZ)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    goto/16 :goto_1

    .line 203
    :cond_e
    add-int/lit8 v4, v4, 0x1

    goto :goto_4

    :cond_f
    move v0, v3

    .line 218
    goto :goto_6

    :cond_10
    move v0, v1

    goto :goto_7

    :cond_11
    move-object v4, v0

    goto :goto_5
.end method
