.class Ltv/peel/app/d;
.super Lcom/peel/util/s;


# instance fields
.field final synthetic a:Ltv/peel/app/c;


# direct methods
.method constructor <init>(Ltv/peel/app/c;)V
    .locals 0

    .prologue
    .line 51
    iput-object p1, p0, Ltv/peel/app/d;->a:Ltv/peel/app/c;

    invoke-direct {p0}, Lcom/peel/util/s;-><init>()V

    return-void
.end method


# virtual methods
.method public final varargs a(ILjava/lang/Object;[Ljava/lang/Object;)V
    .locals 9

    .prologue
    const/16 v6, 0x15

    const/4 v1, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 54
    sparse-switch p1, :sswitch_data_0

    .line 133
    :cond_0
    :goto_0
    return-void

    .line 56
    :sswitch_0
    invoke-static {}, Ltv/peel/app/c;->b()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v2, "ControlEvents.LOADED received"

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 60
    :try_start_0
    iget-object v0, p0, Ltv/peel/app/d;->a:Ltv/peel/app/c;

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 61
    const-string/jumbo v2, "current_room"

    const/4 v3, 0x0

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 63
    if-nez v0, :cond_1

    .line 64
    :try_start_1
    sget-object v1, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v1}, Lcom/peel/control/am;->c()[Lcom/peel/control/RoomControl;

    move-result-object v1

    .line 65
    if-eqz v1, :cond_1

    array-length v2, v1

    if-lez v2, :cond_1

    .line 66
    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-virtual {v1}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/data/at;->b()Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v0

    .line 72
    :cond_1
    invoke-static {v0}, Lcom/peel/content/a;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 69
    :catch_0
    move-exception v0

    .line 70
    :goto_1
    :try_start_2
    invoke-static {}, Ltv/peel/app/c;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {}, Ltv/peel/app/c;->b()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 72
    invoke-static {v1}, Lcom/peel/content/a;->a(Ljava/lang/String;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    :goto_2
    invoke-static {v1}, Lcom/peel/content/a;->a(Ljava/lang/String;)V

    throw v0

    .line 77
    :sswitch_1
    iget-object v0, p0, Ltv/peel/app/d;->a:Ltv/peel/app/c;

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 78
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v1, "last_activity"

    check-cast p2, Lcom/peel/control/a;

    invoke-virtual {p2}, Lcom/peel/control/a;->b()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 79
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-ge v0, v6, :cond_2

    .line 80
    iget-object v0, p0, Ltv/peel/app/d;->a:Ltv/peel/app/c;

    invoke-virtual {v0}, Ltv/peel/app/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/peel/util/bb;->a(Landroid/content/Context;)V

    .line 82
    :cond_2
    iget-object v0, p0, Ltv/peel/app/d;->a:Ltv/peel/app/c;

    const-string/jumbo v1, "widget_pref"

    invoke-virtual {v0, v1, v3}, Ltv/peel/app/c;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string/jumbo v1, "notification"

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 83
    new-instance v1, Landroid/content/Intent;

    sget-object v0, Lcom/peel/c/a;->b:Lcom/peel/c/e;

    invoke-static {v0}, Lcom/peel/c/a;->a(Lcom/peel/c/e;)Ljava/lang/Object;

    move-result-object v0

    sget-object v2, Lcom/peel/c/b;->c:Lcom/peel/c/b;

    if-ne v0, v2, :cond_3

    const-string/jumbo v0, "tv.peel.notification.EXPANDED"

    :goto_3
    invoke-direct {v1, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 84
    iget-object v0, p0, Ltv/peel/app/d;->a:Ltv/peel/app/c;

    invoke-virtual {v0, v1}, Ltv/peel/app/c;->sendBroadcast(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 83
    :cond_3
    const-string/jumbo v0, "tv.peel.samsung.notification.EXPANDED"

    goto :goto_3

    .line 89
    :sswitch_2
    iget-object v0, p0, Ltv/peel/app/d;->a:Ltv/peel/app/c;

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 90
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v4, "current_room"

    check-cast p2, Lcom/peel/control/RoomControl;

    invoke-virtual {p2}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v5

    invoke-virtual {v5}, Lcom/peel/data/at;->b()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v0, v4, v5}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 92
    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v0}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/control/RoomControl;->g()[Ljava/lang/String;

    move-result-object v0

    .line 93
    if-eqz v0, :cond_8

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    const-string/jumbo v4, "live"

    invoke-interface {v0, v4}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 94
    iget-object v0, p0, Ltv/peel/app/d;->a:Ltv/peel/app/c;

    invoke-virtual {v0}, Ltv/peel/app/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, v2}, Lcom/peel/util/bx;->a(Landroid/content/Context;Z)V

    .line 99
    :goto_4
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-ge v0, v6, :cond_4

    .line 100
    iget-object v0, p0, Ltv/peel/app/d;->a:Ltv/peel/app/c;

    invoke-virtual {v0}, Ltv/peel/app/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/peel/util/bb;->a(Landroid/content/Context;)V

    .line 103
    :cond_4
    iget-object v0, p0, Ltv/peel/app/d;->a:Ltv/peel/app/c;

    const-string/jumbo v4, "widget_pref"

    invoke-virtual {v0, v4, v3}, Ltv/peel/app/c;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string/jumbo v4, "notification"

    invoke-interface {v0, v4, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 104
    new-instance v4, Landroid/content/Intent;

    sget-object v0, Lcom/peel/c/a;->b:Lcom/peel/c/e;

    invoke-static {v0}, Lcom/peel/c/a;->a(Lcom/peel/c/e;)Ljava/lang/Object;

    move-result-object v0

    sget-object v5, Lcom/peel/c/b;->c:Lcom/peel/c/b;

    if-ne v0, v5, :cond_9

    const-string/jumbo v0, "tv.peel.notification.EXPANDED"

    :goto_5
    invoke-direct {v4, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 105
    iget-object v0, p0, Ltv/peel/app/d;->a:Ltv/peel/app/c;

    invoke-virtual {v0, v4}, Ltv/peel/app/c;->sendBroadcast(Landroid/content/Intent;)V

    .line 109
    :cond_5
    iget-object v0, p0, Ltv/peel/app/d;->a:Ltv/peel/app/c;

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string/jumbo v4, "country_ISO"

    invoke-interface {v0, v4, v1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 110
    const-string/jumbo v4, "US"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 112
    const-string/jumbo v0, "live"

    invoke-static {v0}, Lcom/peel/content/a;->c(Ljava/lang/String;)Lcom/peel/content/library/Library;

    move-result-object v0

    .line 113
    if-eqz v0, :cond_6

    .line 114
    invoke-virtual {v0}, Lcom/peel/content/library/Library;->e()Ljava/lang/String;

    move-result-object v0

    new-instance v4, Ltv/peel/app/e;

    invoke-direct {v4, p0}, Ltv/peel/app/e;-><init>(Ltv/peel/app/d;)V

    invoke-static {v0, v4}, Lcom/peel/util/al;->a(Ljava/lang/String;Lcom/peel/util/t;)V

    .line 124
    :cond_6
    :try_start_3
    invoke-static {}, Lcom/peel/d/i;->a()Lcom/peel/d/i;

    move-result-object v0

    const-string/jumbo v4, "headset"

    invoke-virtual {v0, v4}, Lcom/peel/d/i;->c(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v5

    .line 125
    if-nez v5, :cond_a

    move-object v0, v1

    .line 126
    :goto_6
    if-nez v0, :cond_b

    move-object v4, v1

    .line 127
    :goto_7
    if-eqz v4, :cond_7

    const-string/jumbo v0, "state"

    const/4 v1, 0x0

    invoke-virtual {v5, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    if-ne v2, v0, :cond_c

    move v1, v2

    :goto_8
    const-string/jumbo v0, "name"

    const-string/jumbo v6, "headset"

    invoke-virtual {v5, v0, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    const-string/jumbo v0, "microphone"

    const/4 v7, 0x0

    invoke-virtual {v5, v0, v7}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    if-ne v2, v0, :cond_d

    move v0, v2

    :goto_9
    invoke-virtual {v4, v1, v6, v0}, Lcom/peel/control/o;->a(ZLjava/lang/String;Z)V

    .line 128
    :cond_7
    invoke-static {}, Lcom/peel/d/k;->c()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    goto/16 :goto_0

    .line 129
    :catch_1
    move-exception v0

    goto/16 :goto_0

    .line 96
    :cond_8
    iget-object v0, p0, Ltv/peel/app/d;->a:Ltv/peel/app/c;

    invoke-virtual {v0}, Ltv/peel/app/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, v3}, Lcom/peel/util/bx;->a(Landroid/content/Context;Z)V

    goto/16 :goto_4

    .line 104
    :cond_9
    const-string/jumbo v0, "tv.peel.samsung.notification.EXPANDED"

    goto :goto_5

    .line 125
    :cond_a
    :try_start_4
    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v0}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v0

    goto :goto_6

    .line 126
    :cond_b
    invoke-virtual {v0}, Lcom/peel/control/RoomControl;->b()Lcom/peel/control/o;
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1

    move-result-object v1

    move-object v4, v1

    goto :goto_7

    :cond_c
    move v1, v3

    .line 127
    goto :goto_8

    :cond_d
    move v0, v3

    goto :goto_9

    .line 72
    :catchall_1
    move-exception v1

    move-object v8, v1

    move-object v1, v0

    move-object v0, v8

    goto/16 :goto_2

    .line 69
    :catch_2
    move-exception v1

    move-object v8, v1

    move-object v1, v0

    move-object v0, v8

    goto/16 :goto_1

    .line 54
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0xa -> :sswitch_2
        0x14 -> :sswitch_1
    .end sparse-switch
.end method
