.class public Ltv/peel/app/c;
.super Landroid/app/Application;


# static fields
.field private static final b:Ljava/lang/String;


# instance fields
.field public a:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/peel/main/Home;",
            ">;"
        }
    .end annotation
.end field

.field private c:Landroid/content/Intent;

.field private final d:Lcom/peel/util/s;

.field private final e:Lcom/peel/util/s;

.field private final f:Ltv/peel/app/a;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 45
    const-class v0, Ltv/peel/app/c;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Ltv/peel/app/c;->b:Ljava/lang/String;

    return-void
.end method

.method protected constructor <init>(Ltv/peel/app/a;)V
    .locals 1

    .prologue
    .line 236
    invoke-direct {p0}, Landroid/app/Application;-><init>()V

    .line 51
    new-instance v0, Ltv/peel/app/d;

    invoke-direct {v0, p0}, Ltv/peel/app/d;-><init>(Ltv/peel/app/c;)V

    iput-object v0, p0, Ltv/peel/app/c;->d:Lcom/peel/util/s;

    .line 136
    new-instance v0, Ltv/peel/app/f;

    invoke-direct {v0, p0}, Ltv/peel/app/f;-><init>(Ltv/peel/app/c;)V

    iput-object v0, p0, Ltv/peel/app/c;->e:Lcom/peel/util/s;

    .line 237
    iput-object p1, p0, Ltv/peel/app/c;->f:Ltv/peel/app/a;

    .line 238
    return-void
.end method

.method static synthetic a(Ltv/peel/app/c;)V
    .locals 0

    .prologue
    .line 44
    invoke-direct {p0}, Ltv/peel/app/c;->c()V

    return-void
.end method

.method static synthetic a(Ltv/peel/app/c;ZZ)V
    .locals 0

    .prologue
    .line 44
    invoke-direct {p0, p1, p2}, Ltv/peel/app/c;->a(ZZ)V

    return-void
.end method

.method private final a(ZZ)V
    .locals 3

    .prologue
    .line 296
    invoke-static {}, Lcom/peel/util/i;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 297
    iget-object v0, p0, Ltv/peel/app/c;->a:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ltv/peel/app/c;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Ltv/peel/app/c;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/main/Home;

    invoke-virtual {v0, p1, p2}, Lcom/peel/main/Home;->a(ZZ)V

    .line 306
    :cond_0
    :goto_0
    return-void

    .line 299
    :cond_1
    const-class v0, Ltv/peel/app/c;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "load ui from app"

    new-instance v2, Ltv/peel/app/g;

    invoke-direct {v2, p0, p1, p2}, Ltv/peel/app/g;-><init>(Ltv/peel/app/c;ZZ)V

    invoke-static {v0, v1, v2}, Lcom/peel/util/i;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/lang/Runnable;

    goto :goto_0
.end method

.method static synthetic b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 44
    sget-object v0, Ltv/peel/app/c;->b:Ljava/lang/String;

    return-object v0
.end method

.method private final c()V
    .locals 3

    .prologue
    .line 309
    invoke-static {}, Lcom/peel/util/i;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 310
    iget-object v0, p0, Ltv/peel/app/c;->a:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ltv/peel/app/c;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Ltv/peel/app/c;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/main/Home;

    invoke-virtual {v0}, Lcom/peel/main/Home;->a()V

    .line 319
    :cond_0
    :goto_0
    return-void

    .line 312
    :cond_1
    const-class v0, Ltv/peel/app/c;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "load ui from app"

    new-instance v2, Ltv/peel/app/h;

    invoke-direct {v2, p0}, Ltv/peel/app/h;-><init>(Ltv/peel/app/c;)V

    invoke-static {v0, v1, v2}, Lcom/peel/util/i;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/lang/Runnable;

    goto :goto_0
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 279
    new-instance v0, Landroid/content/Intent;

    const-class v1, Ltv/peel/widget/service/WidgetTimerService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iput-object v0, p0, Ltv/peel/app/c;->c:Landroid/content/Intent;

    .line 280
    iget-object v0, p0, Ltv/peel/app/c;->c:Landroid/content/Intent;

    invoke-virtual {p0, v0}, Ltv/peel/app/c;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 281
    return-void
.end method

.method public onCreate()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 243
    invoke-super {p0}, Landroid/app/Application;->onCreate()V

    .line 244
    iget-object v0, p0, Ltv/peel/app/c;->f:Ltv/peel/app/a;

    invoke-virtual {p0}, Ltv/peel/app/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Ltv/peel/app/a;->a(Landroid/content/Context;)V

    .line 246
    invoke-static {}, Lcom/peel/util/i;->a()V

    .line 247
    invoke-static {p0}, Lcom/peel/control/am;->a(Landroid/content/Context;)V

    .line 248
    sget-object v0, Lcom/peel/content/a;->b:Lcom/peel/content/i;

    iget-object v1, p0, Ltv/peel/app/c;->e:Lcom/peel/util/s;

    invoke-virtual {v0, v1}, Lcom/peel/content/i;->a(Ljava/lang/Object;)V

    .line 249
    sget-object v0, Lcom/peel/control/am;->a:Lcom/peel/control/aq;

    iget-object v1, p0, Ltv/peel/app/c;->d:Lcom/peel/util/s;

    invoke-virtual {v0, v1}, Lcom/peel/control/aq;->a(Ljava/lang/Object;)V

    .line 251
    const-string/jumbo v0, "connectivity"

    invoke-virtual {p0, v0}, Ltv/peel/app/c;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 252
    const-string/jumbo v1, "network_setup"

    invoke-virtual {p0, v1, v6}, Ltv/peel/app/c;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    .line 254
    invoke-static {p0}, Lcom/peel/util/bx;->d(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    .line 256
    const/4 v1, 0x0

    .line 257
    if-eqz v0, :cond_4

    .line 258
    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v0

    .line 261
    :goto_0
    if-eqz v0, :cond_3

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getType()I

    move-result v1

    if-nez v1, :cond_3

    .line 262
    const-string/jumbo v1, "mobile_network"

    invoke-interface {v2, v1, v6}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    .line 263
    const-string/jumbo v3, "roaming_network"

    invoke-interface {v2, v3, v6}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    .line 264
    const-string/jumbo v4, "network_dialog"

    invoke-interface {v2, v4, v6}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    .line 265
    const-string/jumbo v5, "roaming_dialog"

    invoke-interface {v2, v5, v6}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    .line 267
    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isRoaming()Z

    move-result v5

    if-eqz v5, :cond_0

    if-eqz v2, :cond_0

    if-nez v3, :cond_1

    :cond_0
    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isRoaming()Z

    move-result v0

    if-nez v0, :cond_2

    if-eqz v4, :cond_2

    if-eqz v1, :cond_2

    .line 268
    :cond_1
    invoke-virtual {p0}, Ltv/peel/app/c;->a()V

    .line 276
    :cond_2
    :goto_1
    return-void

    .line 270
    :cond_3
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getType()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    .line 272
    const-string/jumbo v0, "china"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 273
    invoke-virtual {p0}, Ltv/peel/app/c;->a()V

    goto :goto_1

    :cond_4
    move-object v0, v1

    goto :goto_0
.end method

.method public onTerminate()V
    .locals 2

    .prologue
    .line 285
    iget-object v0, p0, Ltv/peel/app/c;->c:Landroid/content/Intent;

    invoke-virtual {p0, v0}, Ltv/peel/app/c;->stopService(Landroid/content/Intent;)Z

    .line 286
    iget-object v0, p0, Ltv/peel/app/c;->f:Ltv/peel/app/a;

    invoke-virtual {v0}, Ltv/peel/app/a;->a()V

    .line 287
    invoke-static {}, Lcom/peel/control/am;->a()V

    .line 288
    invoke-static {}, Lcom/peel/util/i;->b()V

    .line 289
    sget-object v0, Lcom/peel/control/am;->a:Lcom/peel/control/aq;

    iget-object v1, p0, Ltv/peel/app/c;->d:Lcom/peel/util/s;

    invoke-virtual {v0, v1}, Lcom/peel/control/aq;->b(Ljava/lang/Object;)V

    .line 290
    sget-object v0, Lcom/peel/content/a;->b:Lcom/peel/content/i;

    iget-object v1, p0, Ltv/peel/app/c;->e:Lcom/peel/util/s;

    invoke-virtual {v0, v1}, Lcom/peel/content/i;->b(Ljava/lang/Object;)V

    .line 292
    invoke-super {p0}, Landroid/app/Application;->onTerminate()V

    .line 293
    return-void
.end method
