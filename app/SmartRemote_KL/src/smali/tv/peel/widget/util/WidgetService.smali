.class public Ltv/peel/widget/util/WidgetService;
.super Landroid/app/Service;

# interfaces
.implements Ltv/peel/widget/a/g;


# static fields
.field public static a:Z

.field public static b:Z

.field public static c:Z

.field public static d:Z

.field public static e:Z

.field private static final f:Ljava/lang/String;


# instance fields
.field private g:Z

.field private h:Ltv/peel/widget/b;

.field private i:Ljava/lang/String;

.field private j:[Ljava/lang/String;

.field private k:Ltv/peel/widget/util/c;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 39
    const-class v0, Ltv/peel/widget/WidgetService;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Ltv/peel/widget/util/WidgetService;->f:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 38
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 78
    iput-boolean v2, p0, Ltv/peel/widget/util/WidgetService;->g:Z

    .line 80
    const-string/jumbo v0, "Play"

    iput-object v0, p0, Ltv/peel/widget/util/WidgetService;->i:Ljava/lang/String;

    .line 81
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const-string/jumbo v1, "PowerOn"

    aput-object v1, v0, v2

    const/4 v1, 0x1

    const-string/jumbo v2, "PowerOn"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string/jumbo v2, "PowerOn"

    aput-object v2, v0, v1

    iput-object v0, p0, Ltv/peel/widget/util/WidgetService;->j:[Ljava/lang/String;

    .line 83
    new-instance v0, Ltv/peel/widget/util/k;

    invoke-direct {v0, p0}, Ltv/peel/widget/util/k;-><init>(Ltv/peel/widget/util/WidgetService;)V

    iput-object v0, p0, Ltv/peel/widget/util/WidgetService;->k:Ltv/peel/widget/util/c;

    return-void
.end method

.method static synthetic a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 38
    sget-object v0, Ltv/peel/widget/util/WidgetService;->f:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic a(Ltv/peel/widget/util/WidgetService;Ltv/peel/widget/b;)Ltv/peel/widget/b;
    .locals 0

    .prologue
    .line 38
    iput-object p1, p0, Ltv/peel/widget/util/WidgetService;->h:Ltv/peel/widget/b;

    return-object p1
.end method

.method private a(I)V
    .locals 11

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 138
    :try_start_0
    invoke-static {p0}, Lcom/peel/util/a/a;->a(Landroid/content/Context;)Lcom/peel/util/a/a;

    move-result-object v3

    const-string/jumbo v4, "lockpanelused"

    invoke-virtual {v3, v4}, Lcom/peel/util/a/a;->a(Ljava/lang/String;)V

    .line 139
    iget-object v3, p0, Ltv/peel/widget/util/WidgetService;->h:Ltv/peel/widget/b;

    invoke-virtual {v3}, Ltv/peel/widget/b;->f()[Lcom/peel/control/a;

    move-result-object v5

    .line 140
    invoke-static {p0}, Ltv/peel/widget/util/j;->a(Landroid/content/Context;)Ltv/peel/widget/util/j;

    move-result-object v6

    .line 141
    sget-object v3, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v3}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v3

    invoke-virtual {v3}, Lcom/peel/control/RoomControl;->d()Lcom/peel/control/a;

    move-result-object v7

    .line 143
    sget v3, Lcom/peel/ui/fp;->widget_device_select_next:I

    if-eq p1, v3, :cond_0

    const/16 v3, 0x75

    if-ne p1, v3, :cond_6

    .line 146
    :cond_0
    array-length v8, v5

    move v4, v0

    :goto_0
    if-ge v4, v8, :cond_c

    aget-object v3, v5, v4

    .line 147
    invoke-virtual {v7}, Lcom/peel/control/a;->b()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v3}, Lcom/peel/control/a;->b()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    move v0, v1

    .line 146
    :cond_1
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    goto :goto_0

    .line 151
    :cond_2
    if-eqz v0, :cond_1

    move-object v0, v3

    .line 156
    :goto_1
    if-nez v0, :cond_3

    .line 159
    const/4 v0, 0x0

    aget-object v0, v5, v0

    .line 161
    :cond_3
    if-eqz v0, :cond_5

    .line 162
    iget-object v1, p0, Ltv/peel/widget/util/WidgetService;->h:Ltv/peel/widget/b;

    invoke-virtual {v1, v0}, Ltv/peel/widget/b;->b(Lcom/peel/control/a;)V

    .line 163
    const/4 v1, 0x0

    invoke-virtual {v6, v1}, Ltv/peel/widget/util/j;->a(Z)V

    .line 164
    invoke-virtual {v6, v0}, Ltv/peel/widget/util/j;->a(Lcom/peel/control/a;)V

    .line 165
    invoke-virtual {v0}, Lcom/peel/control/a;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6, v1}, Ltv/peel/widget/util/j;->a(Ljava/lang/String;)V

    .line 166
    invoke-virtual {v0}, Lcom/peel/control/a;->d()[Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lcom/peel/control/a;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v6, v1, v2}, Ltv/peel/widget/util/j;->a([Ljava/lang/String;Ljava/lang/String;)V

    .line 167
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/peel/control/a;->a(I)Lcom/peel/control/h;

    move-result-object v0

    .line 169
    if-eqz v0, :cond_4

    .line 170
    invoke-virtual {v6, v0}, Ltv/peel/widget/util/j;->a(Lcom/peel/control/h;)V

    .line 172
    :cond_4
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 173
    const-string/jumbo v1, "moving"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 174
    const-string/jumbo v1, "tv.peel.widget.action.UPDATE_WIDGETS"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 175
    invoke-virtual {p0, v0}, Ltv/peel/widget/util/WidgetService;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 213
    :cond_5
    :goto_2
    return-void

    .line 177
    :cond_6
    sget v1, Lcom/peel/ui/fp;->widget_device_select_prev:I

    if-eq p1, v1, :cond_7

    const/16 v1, 0x76

    if-ne p1, v1, :cond_5

    .line 179
    :cond_7
    array-length v3, v5

    move v1, v0

    move-object v0, v2

    :goto_3
    if-ge v1, v3, :cond_8

    aget-object v2, v5, v1

    .line 180
    invoke-virtual {v7}, Lcom/peel/control/a;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2}, Lcom/peel/control/a;->b()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v4, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_b

    .line 185
    :cond_8
    if-nez v0, :cond_9

    .line 189
    array-length v0, v5

    add-int/lit8 v0, v0, -0x1

    aget-object v0, v5, v0

    .line 191
    :cond_9
    if-eqz v0, :cond_5

    .line 193
    iget-object v1, p0, Ltv/peel/widget/util/WidgetService;->h:Ltv/peel/widget/b;

    invoke-virtual {v1, v0}, Ltv/peel/widget/b;->b(Lcom/peel/control/a;)V

    .line 194
    invoke-virtual {v6, v0}, Ltv/peel/widget/util/j;->a(Lcom/peel/control/a;)V

    .line 195
    invoke-virtual {v0}, Lcom/peel/control/a;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6, v1}, Ltv/peel/widget/util/j;->a(Ljava/lang/String;)V

    .line 196
    invoke-virtual {v0}, Lcom/peel/control/a;->d()[Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lcom/peel/control/a;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v6, v1, v2}, Ltv/peel/widget/util/j;->a([Ljava/lang/String;Ljava/lang/String;)V

    .line 198
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/peel/control/a;->a(I)Lcom/peel/control/h;

    move-result-object v0

    .line 200
    if-eqz v0, :cond_a

    .line 201
    invoke-virtual {v6, v0}, Ltv/peel/widget/util/j;->a(Lcom/peel/control/h;)V

    .line 202
    :cond_a
    const/4 v0, 0x0

    invoke-virtual {v6, v0}, Ltv/peel/widget/util/j;->a(Z)V

    .line 203
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 204
    const-string/jumbo v1, "moving"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 205
    const-string/jumbo v1, "tv.peel.widget.action.UPDATE_WIDGETS"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 206
    invoke-virtual {p0, v0}, Ltv/peel/widget/util/WidgetService;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    .line 210
    :catch_0
    move-exception v0

    .line 211
    sget-object v1, Ltv/peel/widget/util/WidgetService;->f:Ljava/lang/String;

    const-string/jumbo v2, "NPE, stop changing device"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_2

    .line 179
    :cond_b
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    move-object v0, v2

    goto :goto_3

    :cond_c
    move-object v0, v2

    goto/16 :goto_1
.end method

.method private a(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 451
    new-instance v0, Landroid/content/Intent;

    const-class v1, Ltv/peel/widget/WidgetService;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Ltv/peel/widget/util/WidgetService;->stopService(Landroid/content/Intent;)Z

    .line 452
    return-void
.end method

.method private a(Landroid/content/Intent;)V
    .locals 11

    .prologue
    const/4 v2, 0x1

    const/4 v7, 0x0

    const/4 v3, 0x0

    .line 234
    invoke-direct {p0}, Ltv/peel/widget/util/WidgetService;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 235
    sget-object v0, Ltv/peel/widget/util/WidgetService;->f:Ljava/lang/String;

    const-string/jumbo v1, "need to wait"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 239
    :cond_0
    invoke-static {p0}, Ltv/peel/widget/util/j;->a(Landroid/content/Context;)Ltv/peel/widget/util/j;

    move-result-object v8

    .line 240
    iget-object v0, p0, Ltv/peel/widget/util/WidgetService;->h:Ltv/peel/widget/b;

    invoke-virtual {v0}, Ltv/peel/widget/b;->k()Z

    move-result v4

    .line 241
    iget-object v0, p0, Ltv/peel/widget/util/WidgetService;->h:Ltv/peel/widget/b;

    invoke-virtual {v0}, Ltv/peel/widget/b;->j()Z

    move-result v5

    .line 243
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string/jumbo v1, "always_remote_widget_enabled"

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Ltv/peel/widget/util/WidgetService;->a:Z

    .line 245
    sget-boolean v0, Ltv/peel/widget/util/WidgetService;->a:Z

    if-eqz v0, :cond_2

    .line 246
    invoke-static {p0}, Ltv/peel/widget/a/a;->a(Ltv/peel/widget/a/g;)V

    .line 251
    :goto_0
    invoke-static {p0}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v0

    .line 252
    new-instance v1, Landroid/content/ComponentName;

    const-class v6, Ltv/peel/widget/f;

    invoke-direct {v1, p0, v6}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v0, v1}, Landroid/appwidget/AppWidgetManager;->getAppWidgetIds(Landroid/content/ComponentName;)[I

    move-result-object v9

    .line 253
    new-instance v1, Landroid/content/ComponentName;

    const-class v6, Ltv/peel/widget/a;

    invoke-direct {v1, p0, v6}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v0, v1}, Landroid/appwidget/AppWidgetManager;->getAppWidgetIds(Landroid/content/ComponentName;)[I

    move-result-object v1

    .line 255
    array-length v0, v1

    if-lez v0, :cond_3

    move v0, v2

    :goto_1
    sput-boolean v0, Ltv/peel/widget/util/WidgetService;->b:Z

    .line 256
    array-length v0, v9

    if-lez v0, :cond_4

    move v0, v2

    :goto_2
    sput-boolean v0, Ltv/peel/widget/util/WidgetService;->c:Z

    .line 258
    if-eqz v5, :cond_17

    .line 266
    :try_start_0
    iget-object v0, p0, Ltv/peel/widget/util/WidgetService;->h:Ltv/peel/widget/b;

    invoke-virtual {v0}, Ltv/peel/widget/b;->f()[Lcom/peel/control/a;

    move-result-object v0

    .line 267
    invoke-virtual {v8, v0}, Ltv/peel/widget/util/j;->a([Lcom/peel/control/a;)V

    .line 268
    if-nez v0, :cond_5

    move v5, v3

    .line 270
    :goto_3
    const-string/jumbo v0, "moving"

    const/4 v4, 0x0

    invoke-virtual {p1, v0, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-virtual {v8}, Ltv/peel/widget/util/j;->e()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 271
    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v8}, Ltv/peel/widget/util/j;->e()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/peel/control/am;->e(Ljava/lang/String;)Lcom/peel/control/a;

    move-result-object v0

    .line 276
    :goto_4
    if-eqz v0, :cond_1

    .line 277
    const/4 v4, 0x1

    invoke-virtual {v0, v4}, Lcom/peel/control/a;->a(I)Lcom/peel/control/h;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    .line 278
    if-nez v4, :cond_7

    .line 361
    :cond_1
    :goto_5
    return-void

    .line 248
    :cond_2
    invoke-static {}, Ltv/peel/widget/a/a;->a()V

    goto :goto_0

    :cond_3
    move v0, v3

    .line 255
    goto :goto_1

    :cond_4
    move v0, v3

    .line 256
    goto :goto_2

    .line 268
    :cond_5
    :try_start_1
    array-length v5, v0

    goto :goto_3

    .line 273
    :cond_6
    iget-object v0, p0, Ltv/peel/widget/util/WidgetService;->h:Ltv/peel/widget/b;

    invoke-virtual {v0}, Ltv/peel/widget/b;->g()Lcom/peel/control/a;

    move-result-object v0

    goto :goto_4

    .line 280
    :cond_7
    invoke-virtual {v8, v0}, Ltv/peel/widget/util/j;->a(Lcom/peel/control/a;)V

    .line 281
    iget-object v6, p0, Ltv/peel/widget/util/WidgetService;->h:Ltv/peel/widget/b;

    invoke-virtual {v6, v0}, Ltv/peel/widget/b;->a(Lcom/peel/control/a;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v8, v6}, Ltv/peel/widget/util/j;->a(Ljava/lang/String;)V

    .line 282
    invoke-virtual {v0}, Lcom/peel/control/a;->d()[Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0}, Lcom/peel/control/a;->b()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v6, v10}, Ltv/peel/widget/util/j;->a([Ljava/lang/String;Ljava/lang/String;)V

    .line 283
    const-string/jumbo v6, "tv.peel.widget.lockscreen.height"

    const/4 v10, 0x0

    invoke-virtual {p1, v6, v10}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v6

    .line 284
    if-lez v6, :cond_8

    .line 285
    invoke-virtual {v8, v6}, Ltv/peel/widget/util/j;->a(I)V

    .line 287
    :cond_8
    invoke-virtual {v8, v4}, Ltv/peel/widget/util/j;->a(Lcom/peel/control/h;)V

    .line 289
    iget-object v4, p0, Ltv/peel/widget/util/WidgetService;->h:Ltv/peel/widget/b;

    invoke-virtual {v4, v0}, Ltv/peel/widget/b;->a(Lcom/peel/control/a;)Ljava/lang/String;

    move-result-object v4

    sput-object v4, Ltv/peel/widget/a/j;->a:Ljava/lang/String;

    .line 290
    iget-object v4, p0, Ltv/peel/widget/util/WidgetService;->h:Ltv/peel/widget/b;

    invoke-virtual {v4, v0}, Ltv/peel/widget/b;->a(Lcom/peel/control/a;)Ljava/lang/String;

    move-result-object v4

    sput-object v4, Ltv/peel/widget/a/i;->a:Ljava/lang/String;

    .line 291
    sget-boolean v4, Ltv/peel/widget/util/WidgetService;->a:Z

    if-eqz v4, :cond_9

    .line 292
    iget-object v4, p0, Ltv/peel/widget/util/WidgetService;->h:Ltv/peel/widget/b;

    invoke-virtual {v4, v0}, Ltv/peel/widget/b;->a(Lcom/peel/control/a;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Ltv/peel/widget/a/a;->a:Ljava/lang/String;

    .line 295
    :cond_9
    iget-object v0, p0, Ltv/peel/widget/util/WidgetService;->h:Ltv/peel/widget/b;

    invoke-virtual {v0}, Ltv/peel/widget/b;->n()[Ltv/peel/widget/service/DeviceParcelable;

    move-result-object v0

    invoke-virtual {v8, v0}, Ltv/peel/widget/util/j;->a([Ltv/peel/widget/service/DeviceParcelable;)V
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_0

    .line 316
    invoke-virtual {v8}, Ltv/peel/widget/util/j;->e()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v8, v0}, Ltv/peel/widget/util/j;->b(Ljava/lang/String;)Ljava/util/Set;

    move-result-object v0

    .line 318
    if-eqz v0, :cond_b

    .line 319
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_a
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_b

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 320
    const-string/jumbo v6, "live"

    invoke-virtual {v0, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a

    move v3, v2

    .line 327
    :cond_b
    sget-boolean v0, Ltv/peel/widget/util/WidgetService;->b:Z

    if-eqz v0, :cond_c

    .line 328
    invoke-virtual {v8}, Ltv/peel/widget/util/j;->e()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_f

    move-object v2, v7

    :goto_6
    if-eqz v3, :cond_10

    iget-object v0, p0, Ltv/peel/widget/util/WidgetService;->h:Ltv/peel/widget/b;

    invoke-virtual {v0}, Ltv/peel/widget/b;->l()Ljava/util/List;

    move-result-object v4

    :goto_7
    invoke-virtual {v8}, Ltv/peel/widget/util/j;->f()I

    move-result v6

    move-object v0, p0

    invoke-static/range {v0 .. v6}, Ltv/peel/widget/a/i;->a(Landroid/content/Context;[I[Lcom/peel/control/h;ZLjava/util/List;II)V

    .line 330
    :cond_c
    sget-boolean v0, Ltv/peel/widget/util/WidgetService;->c:Z

    if-eqz v0, :cond_d

    .line 331
    invoke-virtual {v8}, Ltv/peel/widget/util/j;->e()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_11

    move-object v2, v7

    :goto_8
    if-eqz v3, :cond_12

    iget-object v0, p0, Ltv/peel/widget/util/WidgetService;->h:Ltv/peel/widget/b;

    invoke-virtual {v0}, Ltv/peel/widget/b;->l()Ljava/util/List;

    move-result-object v4

    :goto_9
    invoke-virtual {v8}, Ltv/peel/widget/util/j;->f()I

    move-result v6

    move-object v0, p0

    move-object v1, v9

    invoke-static/range {v0 .. v6}, Ltv/peel/widget/a/j;->a(Landroid/content/Context;[I[Lcom/peel/control/h;ZLjava/util/List;II)V

    .line 334
    :cond_d
    sget-boolean v0, Ltv/peel/widget/util/WidgetService;->a:Z

    if-eqz v0, :cond_16

    sget-boolean v0, Ltv/peel/widget/util/WidgetService;->e:Z

    if-nez v0, :cond_16

    .line 335
    sget-boolean v0, Ltv/peel/widget/util/WidgetService;->d:Z

    if-eqz v0, :cond_14

    .line 336
    invoke-virtual {v8}, Ltv/peel/widget/util/j;->e()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_13

    :goto_a
    invoke-static {p0, v3}, Ltv/peel/widget/a/a;->a(Landroid/content/Context;Z)I

    move-result v0

    invoke-static {p0, v7, v0, v5}, Ltv/peel/widget/a/a;->a(Landroid/content/Context;[Lcom/peel/control/h;II)V

    .line 344
    :goto_b
    sget-boolean v0, Ltv/peel/widget/util/WidgetService;->b:Z

    if-nez v0, :cond_1

    sget-boolean v0, Ltv/peel/widget/util/WidgetService;->c:Z

    if-nez v0, :cond_1

    sget-boolean v0, Ltv/peel/widget/util/WidgetService;->a:Z

    if-nez v0, :cond_1

    .line 346
    invoke-direct {p0, p0}, Ltv/peel/widget/util/WidgetService;->a(Landroid/content/Context;)V

    goto/16 :goto_5

    .line 299
    :catch_0
    move-exception v0

    .line 300
    sget-object v1, Ltv/peel/widget/util/WidgetService;->f:Ljava/lang/String;

    const-string/jumbo v2, "NPE, retrying..."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 301
    iput-boolean v3, p0, Ltv/peel/widget/util/WidgetService;->g:Z

    .line 302
    iget-object v0, p0, Ltv/peel/widget/util/WidgetService;->h:Ltv/peel/widget/b;

    if-eqz v0, :cond_e

    .line 303
    iget-object v0, p0, Ltv/peel/widget/util/WidgetService;->h:Ltv/peel/widget/b;

    invoke-virtual {v0}, Ltv/peel/widget/b;->h()V

    .line 304
    iput-object v7, p0, Ltv/peel/widget/util/WidgetService;->h:Ltv/peel/widget/b;

    .line 306
    :cond_e
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, Ltv/peel/widget/util/l;

    invoke-direct {v1, p0}, Ltv/peel/widget/util/l;-><init>(Ltv/peel/widget/util/WidgetService;)V

    const-wide/16 v2, 0x5dc

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/16 :goto_5

    .line 328
    :cond_f
    iget-object v0, p0, Ltv/peel/widget/util/WidgetService;->h:Ltv/peel/widget/b;

    invoke-virtual {v0}, Ltv/peel/widget/b;->o()[Lcom/peel/control/h;

    move-result-object v2

    goto/16 :goto_6

    :cond_10
    move-object v4, v7

    goto :goto_7

    .line 331
    :cond_11
    iget-object v0, p0, Ltv/peel/widget/util/WidgetService;->h:Ltv/peel/widget/b;

    invoke-virtual {v0}, Ltv/peel/widget/b;->o()[Lcom/peel/control/h;

    move-result-object v2

    goto :goto_8

    :cond_12
    move-object v4, v7

    goto :goto_9

    .line 336
    :cond_13
    iget-object v0, p0, Ltv/peel/widget/util/WidgetService;->h:Ltv/peel/widget/b;

    invoke-virtual {v0}, Ltv/peel/widget/b;->o()[Lcom/peel/control/h;

    move-result-object v7

    goto :goto_a

    .line 338
    :cond_14
    invoke-virtual {v8}, Ltv/peel/widget/util/j;->e()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_15

    :goto_c
    invoke-static {p0, v7, v3, v5}, Ltv/peel/widget/a/a;->a(Landroid/content/Context;[Lcom/peel/control/h;ZI)V

    goto :goto_b

    :cond_15
    iget-object v0, p0, Ltv/peel/widget/util/WidgetService;->h:Ltv/peel/widget/b;

    invoke-virtual {v0}, Ltv/peel/widget/b;->o()[Lcom/peel/control/h;

    move-result-object v7

    goto :goto_c

    .line 341
    :cond_16
    invoke-static {}, Ltv/peel/widget/a/a;->a()V

    goto :goto_b

    .line 351
    :cond_17
    sget-object v0, Ltv/peel/widget/util/WidgetService;->f:Ljava/lang/String;

    const-string/jumbo v1, "Controller is not setup yet"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 353
    invoke-virtual {v8}, Ltv/peel/widget/util/j;->d()V

    .line 354
    sget-boolean v0, Ltv/peel/widget/util/WidgetService;->b:Z

    if-eqz v0, :cond_18

    .line 355
    invoke-static {p0, v4}, Ltv/peel/widget/a/i;->a(Landroid/content/Context;Z)V

    .line 357
    :cond_18
    sget-boolean v0, Ltv/peel/widget/util/WidgetService;->c:Z

    if-eqz v0, :cond_1

    .line 358
    invoke-static {p0, v4}, Ltv/peel/widget/a/j;->a(Landroid/content/Context;Z)V

    goto/16 :goto_5
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 4

    .prologue
    .line 407
    invoke-direct {p0}, Ltv/peel/widget/util/WidgetService;->e()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 448
    :cond_0
    :goto_0
    return-void

    .line 411
    :cond_1
    sget-object v0, Ltv/peel/widget/util/WidgetService;->f:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {p1, v2}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " button is pressed"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 414
    :try_start_0
    invoke-static {p0}, Lcom/peel/util/a/a;->a(Landroid/content/Context;)Lcom/peel/util/a/a;

    move-result-object v0

    const-string/jumbo v1, "lockpanelused"

    invoke-virtual {v0, v1}, Lcom/peel/util/a/a;->a(Ljava/lang/String;)V

    .line 415
    invoke-static {p0}, Ltv/peel/widget/util/j;->a(Landroid/content/Context;)Ltv/peel/widget/util/j;

    move-result-object v0

    .line 416
    const-string/jumbo v1, "Volume_Down"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    const-string/jumbo v1, "Volume_Up"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    const-string/jumbo v1, "Mute"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 417
    :cond_2
    iget-object v0, p0, Ltv/peel/widget/util/WidgetService;->h:Ltv/peel/widget/b;

    invoke-virtual {v0}, Ltv/peel/widget/b;->n()[Ltv/peel/widget/service/DeviceParcelable;

    move-result-object v0

    .line 418
    if-eqz v0, :cond_0

    array-length v1, v0

    if-lez v1, :cond_0

    .line 419
    iget-object v1, p0, Ltv/peel/widget/util/WidgetService;->h:Ltv/peel/widget/b;

    const/4 v2, 0x0

    aget-object v0, v0, v2

    invoke-virtual {v0}, Ltv/peel/widget/service/DeviceParcelable;->a()Ljava/lang/String;

    move-result-object v0

    const/16 v2, 0x1b58

    invoke-virtual {v1, p1, v0, v2}, Ltv/peel/widget/b;->a(Ljava/lang/String;Ljava/lang/String;I)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 444
    :catch_0
    move-exception v0

    .line 445
    sget-object v1, Ltv/peel/widget/util/WidgetService;->f:Ljava/lang/String;

    const-string/jumbo v2, "NPE while trying to send IR command"

    invoke-static {v1, v2}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)I

    .line 446
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_0

    .line 421
    :cond_3
    :try_start_1
    const-string/jumbo v1, "Play|Pause"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 422
    invoke-virtual {v0}, Ltv/peel/widget/util/j;->a()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "Apple"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 423
    iget-object v1, p0, Ltv/peel/widget/util/WidgetService;->h:Ltv/peel/widget/b;

    const-string/jumbo v2, "Select"

    invoke-virtual {v0}, Ltv/peel/widget/util/j;->b()Ljava/lang/String;

    move-result-object v0

    const/16 v3, 0x1b58

    invoke-virtual {v1, v2, v0, v3}, Ltv/peel/widget/b;->a(Ljava/lang/String;Ljava/lang/String;I)V

    goto/16 :goto_0

    .line 425
    :cond_4
    iget-object v1, p0, Ltv/peel/widget/util/WidgetService;->h:Ltv/peel/widget/b;

    iget-object v2, p0, Ltv/peel/widget/util/WidgetService;->i:Ljava/lang/String;

    invoke-virtual {v0}, Ltv/peel/widget/util/j;->b()Ljava/lang/String;

    move-result-object v0

    const/16 v3, 0x1b58

    invoke-virtual {v1, v2, v0, v3}, Ltv/peel/widget/b;->a(Ljava/lang/String;Ljava/lang/String;I)V

    .line 426
    const-string/jumbo v0, "Play"

    iget-object v1, p0, Ltv/peel/widget/util/WidgetService;->i:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    const-string/jumbo v0, "Pause"

    :goto_1
    iput-object v0, p0, Ltv/peel/widget/util/WidgetService;->i:Ljava/lang/String;

    goto/16 :goto_0

    :cond_5
    const-string/jumbo v0, "Play"

    goto :goto_1

    .line 428
    :cond_6
    const-string/jumbo v1, "channel"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 429
    iget-object v1, p0, Ltv/peel/widget/util/WidgetService;->h:Ltv/peel/widget/b;

    invoke-virtual {v0}, Ltv/peel/widget/util/j;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0, p2}, Ltv/peel/widget/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 430
    :cond_7
    const-string/jumbo v1, "Power"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 431
    iget-object v0, p0, Ltv/peel/widget/util/WidgetService;->h:Ltv/peel/widget/b;

    const-string/jumbo v1, "Power"

    const/16 v2, 0x1b58

    invoke-virtual {v0, v1, p2, v2}, Ltv/peel/widget/b;->a(Ljava/lang/String;Ljava/lang/String;I)V

    goto/16 :goto_0

    .line 432
    :cond_8
    const-string/jumbo v1, "PowerOn|PowerOff"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 433
    iget-object v0, p0, Ltv/peel/widget/util/WidgetService;->j:[Ljava/lang/String;

    aget-object v0, v0, p3

    .line 434
    iget-object v1, p0, Ltv/peel/widget/util/WidgetService;->h:Ltv/peel/widget/b;

    const/16 v2, 0x1b58

    invoke-virtual {v1, v0, p2, v2}, Ltv/peel/widget/b;->a(Ljava/lang/String;Ljava/lang/String;I)V

    .line 435
    iget-object v1, p0, Ltv/peel/widget/util/WidgetService;->j:[Ljava/lang/String;

    const-string/jumbo v2, "PowerOn"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    const-string/jumbo v0, "PowerOff"

    :goto_2
    aput-object v0, v1, p3

    goto/16 :goto_0

    :cond_9
    const-string/jumbo v0, "PowerOn"

    goto :goto_2

    .line 437
    :cond_a
    iget-object v1, p0, Ltv/peel/widget/util/WidgetService;->h:Ltv/peel/widget/b;

    invoke-virtual {v0}, Ltv/peel/widget/util/j;->b()Ljava/lang/String;

    move-result-object v0

    const/16 v2, 0x1b58

    invoke-virtual {v1, p1, v0, v2}, Ltv/peel/widget/b;->a(Ljava/lang/String;Ljava/lang/String;I)V

    .line 438
    const-string/jumbo v0, "MODE"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_b

    const-string/jumbo v0, "Down"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_b

    const-string/jumbo v0, "UP"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 439
    :cond_b
    new-instance v0, Landroid/content/Intent;

    const-class v1, Ltv/peel/widget/WidgetService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 440
    const-string/jumbo v1, "tv.peel.widget.action.UPDATE_WIDGETS"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 441
    invoke-virtual {p0, v0}, Ltv/peel/widget/util/WidgetService;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0
.end method

.method static synthetic a(Ltv/peel/widget/util/WidgetService;)Z
    .locals 1

    .prologue
    .line 38
    iget-boolean v0, p0, Ltv/peel/widget/util/WidgetService;->g:Z

    return v0
.end method

.method static synthetic a(Ltv/peel/widget/util/WidgetService;Z)Z
    .locals 0

    .prologue
    .line 38
    iput-boolean p1, p0, Ltv/peel/widget/util/WidgetService;->g:Z

    return p1
.end method

.method static synthetic b(Ltv/peel/widget/util/WidgetService;)Ltv/peel/widget/util/c;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Ltv/peel/widget/util/WidgetService;->k:Ltv/peel/widget/util/c;

    return-object v0
.end method

.method private b(I)V
    .locals 3

    .prologue
    .line 216
    invoke-static {p0}, Ltv/peel/widget/util/j;->a(Landroid/content/Context;)Ltv/peel/widget/util/j;

    move-result-object v1

    .line 217
    invoke-virtual {v1}, Ltv/peel/widget/util/j;->f()I

    move-result v0

    .line 218
    sget v2, Lcom/peel/ui/fp;->widget_channel_prev:I

    if-ne p1, v2, :cond_1

    .line 219
    add-int/lit8 v0, v0, -0x1

    .line 224
    :cond_0
    :goto_0
    invoke-virtual {v1, v0}, Ltv/peel/widget/util/j;->b(I)V

    .line 225
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 226
    const-string/jumbo v1, "tv.peel.widget.action.UPDATE_WIDGETS"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 227
    invoke-virtual {p0, v0}, Ltv/peel/widget/util/WidgetService;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 228
    return-void

    .line 220
    :cond_1
    sget v2, Lcom/peel/ui/fp;->widget_channel_next:I

    if-ne p1, v2, :cond_0

    .line 221
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private e()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 364
    iget-object v1, p0, Ltv/peel/widget/util/WidgetService;->h:Ltv/peel/widget/b;

    if-eqz v1, :cond_0

    iget-object v1, p0, Ltv/peel/widget/util/WidgetService;->h:Ltv/peel/widget/b;

    invoke-virtual {v1}, Ltv/peel/widget/b;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 375
    :goto_0
    return v0

    .line 368
    :cond_0
    iput-boolean v0, p0, Ltv/peel/widget/util/WidgetService;->g:Z

    .line 370
    iget-object v0, p0, Ltv/peel/widget/util/WidgetService;->h:Ltv/peel/widget/b;

    if-nez v0, :cond_1

    .line 371
    sget-object v0, Ltv/peel/widget/util/WidgetService;->f:Ljava/lang/String;

    const-string/jumbo v1, "The helper is null!!!"

    invoke-static {v0, v1}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)I

    .line 372
    new-instance v0, Ltv/peel/widget/b;

    iget-object v1, p0, Ltv/peel/widget/util/WidgetService;->k:Ltv/peel/widget/util/c;

    invoke-direct {v0, p0, v1}, Ltv/peel/widget/b;-><init>(Landroid/content/Context;Ltv/peel/widget/util/c;)V

    iput-object v0, p0, Ltv/peel/widget/util/WidgetService;->h:Ltv/peel/widget/b;

    .line 375
    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public b()V
    .locals 2

    .prologue
    .line 456
    sget-object v0, Ltv/peel/widget/util/WidgetService;->f:Ljava/lang/String;

    const-string/jumbo v1, "Always On Remote widget created"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 457
    const/4 v0, 0x1

    sput-boolean v0, Ltv/peel/widget/util/WidgetService;->d:Z

    .line 458
    return-void
.end method

.method public c()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 462
    sget-object v0, Ltv/peel/widget/util/WidgetService;->f:Ljava/lang/String;

    const-string/jumbo v1, "Always On Remote widget removed"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 463
    sput-boolean v2, Ltv/peel/widget/util/WidgetService;->a:Z

    .line 464
    sput-boolean v2, Ltv/peel/widget/util/WidgetService;->d:Z

    .line 465
    return-void
.end method

.method public d()V
    .locals 2

    .prologue
    .line 469
    sget-object v0, Ltv/peel/widget/util/WidgetService;->f:Ljava/lang/String;

    const-string/jumbo v1, "Always On Remote widget updated"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 470
    return-void
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1

    .prologue
    .line 391
    const/4 v0, 0x0

    return-object v0
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 380
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 381
    iget-object v0, p0, Ltv/peel/widget/util/WidgetService;->h:Ltv/peel/widget/b;

    if-eqz v0, :cond_0

    .line 382
    iget-object v0, p0, Ltv/peel/widget/util/WidgetService;->h:Ltv/peel/widget/b;

    invoke-virtual {v0}, Ltv/peel/widget/b;->h()V

    .line 383
    const/4 v0, 0x0

    iput-object v0, p0, Ltv/peel/widget/util/WidgetService;->h:Ltv/peel/widget/b;

    .line 385
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Ltv/peel/widget/util/WidgetService;->g:Z

    .line 386
    invoke-static {}, Ltv/peel/widget/a/a;->a()V

    .line 387
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, -0x1

    .line 106
    if-eqz p1, :cond_1

    .line 107
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 108
    iget-object v1, p0, Ltv/peel/widget/util/WidgetService;->h:Ltv/peel/widget/b;

    if-nez v1, :cond_0

    .line 109
    iput-boolean v4, p0, Ltv/peel/widget/util/WidgetService;->g:Z

    .line 110
    new-instance v1, Ltv/peel/widget/b;

    iget-object v2, p0, Ltv/peel/widget/util/WidgetService;->k:Ltv/peel/widget/util/c;

    invoke-direct {v1, p0, v2}, Ltv/peel/widget/b;-><init>(Landroid/content/Context;Ltv/peel/widget/util/c;)V

    iput-object v1, p0, Ltv/peel/widget/util/WidgetService;->h:Ltv/peel/widget/b;

    .line 111
    iget-object v1, p0, Ltv/peel/widget/util/WidgetService;->h:Ltv/peel/widget/b;

    sput-object v1, Ltv/peel/widget/a/j;->b:Ltv/peel/widget/b;

    .line 112
    sget-object v1, Lcom/peel/c/a;->b:Lcom/peel/c/e;

    invoke-static {v1}, Lcom/peel/c/a;->a(Lcom/peel/c/e;)Ljava/lang/Object;

    move-result-object v1

    sget-object v2, Lcom/peel/c/b;->c:Lcom/peel/c/b;

    if-ne v1, v2, :cond_0

    .line 113
    iget-object v1, p0, Ltv/peel/widget/util/WidgetService;->h:Ltv/peel/widget/b;

    sput-object v1, Ltv/peel/widget/a/a;->b:Ltv/peel/widget/b;

    .line 114
    iget-object v1, p0, Ltv/peel/widget/util/WidgetService;->h:Ltv/peel/widget/b;

    sput-object v1, Ltv/peel/widget/a/i;->b:Ltv/peel/widget/b;

    .line 118
    :cond_0
    const-string/jumbo v1, "tv.peel.widget.action.UPDATE_WIDGETS"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 119
    invoke-direct {p0, p1}, Ltv/peel/widget/util/WidgetService;->a(Landroid/content/Intent;)V

    .line 132
    :cond_1
    :goto_0
    const/4 v0, 0x1

    return v0

    .line 120
    :cond_2
    const-string/jumbo v1, "tv.peel.widget.action.BUTTON_PRESSED"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 121
    const-string/jumbo v0, "tv.peel.widget.extra.COMMAND"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "tv.peel.widget.extra.field"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "tv.peel.widget.extra.index"

    invoke-virtual {p1, v2, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    invoke-direct {p0, v0, v1, v2}, Ltv/peel/widget/util/WidgetService;->a(Ljava/lang/String;Ljava/lang/String;I)V

    goto :goto_0

    .line 122
    :cond_3
    const-string/jumbo v1, "tv.peel.widget.action.FAV_IDX_CHANGE"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 123
    const-string/jumbo v0, "tv.peel.widget.extra.COMMAND"

    invoke-virtual {p1, v0, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    invoke-direct {p0, v0}, Ltv/peel/widget/util/WidgetService;->b(I)V

    goto :goto_0

    .line 124
    :cond_4
    const-string/jumbo v1, "tv.peel.widget.action.CHANGE_DEVICE"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 125
    const-string/jumbo v0, "tv.peel.widget.extra.COMMAND"

    invoke-virtual {p1, v0, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    invoke-direct {p0, v0}, Ltv/peel/widget/util/WidgetService;->a(I)V

    goto :goto_0

    .line 126
    :cond_5
    const-string/jumbo v1, "tv.peel.settings.REMOTE_WIDGET_RESET"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 127
    invoke-static {}, Ltv/peel/widget/a/a;->a()V

    .line 128
    invoke-direct {p0, p1}, Ltv/peel/widget/util/WidgetService;->a(Landroid/content/Intent;)V

    goto :goto_0
.end method
