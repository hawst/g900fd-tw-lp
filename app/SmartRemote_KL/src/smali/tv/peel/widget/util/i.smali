.class Ltv/peel/widget/util/i;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/squareup/picasso/Target;


# instance fields
.field final synthetic a:Landroid/widget/RemoteViews;

.field final synthetic b:I

.field final synthetic c:Lcom/peel/util/t;

.field final synthetic d:Ltv/peel/widget/util/d;


# direct methods
.method constructor <init>(Ltv/peel/widget/util/d;Landroid/widget/RemoteViews;ILcom/peel/util/t;)V
    .locals 0

    .prologue
    .line 263
    iput-object p1, p0, Ltv/peel/widget/util/i;->d:Ltv/peel/widget/util/d;

    iput-object p2, p0, Ltv/peel/widget/util/i;->a:Landroid/widget/RemoteViews;

    iput p3, p0, Ltv/peel/widget/util/i;->b:I

    iput-object p4, p0, Ltv/peel/widget/util/i;->c:Lcom/peel/util/t;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onBitmapFailed(Landroid/graphics/drawable/Drawable;)V
    .locals 0

    .prologue
    .line 273
    return-void
.end method

.method public onBitmapLoaded(Landroid/graphics/Bitmap;Lcom/squareup/picasso/Picasso$LoadedFrom;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 266
    iget-object v0, p0, Ltv/peel/widget/util/i;->a:Landroid/widget/RemoteViews;

    iget v1, p0, Ltv/peel/widget/util/i;->b:I

    invoke-virtual {v0, v1, p1}, Landroid/widget/RemoteViews;->setImageViewBitmap(ILandroid/graphics/Bitmap;)V

    .line 267
    iget-object v0, p0, Ltv/peel/widget/util/i;->c:Lcom/peel/util/t;

    const/4 v1, 0x1

    invoke-virtual {v0, v1, v2, v2}, Lcom/peel/util/t;->a(ZLjava/lang/Object;Ljava/lang/String;)V

    .line 268
    return-void
.end method

.method public onPrepareLoad(Landroid/graphics/drawable/Drawable;)V
    .locals 0

    .prologue
    .line 278
    return-void
.end method
