.class Ltv/peel/widget/util/h;
.super Lcom/peel/util/t;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/peel/util/t",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Ltv/peel/widget/util/g;


# direct methods
.method constructor <init>(Ltv/peel/widget/util/g;I)V
    .locals 0

    .prologue
    .line 162
    iput-object p1, p0, Ltv/peel/widget/util/h;->a:Ltv/peel/widget/util/g;

    invoke-direct {p0, p2}, Lcom/peel/util/t;-><init>(I)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 13

    .prologue
    const/4 v12, 0x3

    .line 165
    iget-boolean v0, p0, Ltv/peel/widget/util/h;->i:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Ltv/peel/widget/util/h;->j:Ljava/lang/Object;

    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 168
    :try_start_0
    new-instance v1, Lorg/json/JSONObject;

    iget-object v0, p0, Ltv/peel/widget/util/h;->j:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    invoke-direct {v1, v0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 169
    const-string/jumbo v0, "programs"

    invoke-virtual {v1, v0}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v9

    .line 170
    new-instance v10, Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v0, -0x1

    invoke-direct {v10, v0}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    .line 171
    new-instance v11, Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v0, 0x3

    invoke-direct {v11, v0}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    .line 172
    const/4 v0, 0x0

    move v8, v0

    .line 173
    :goto_0
    invoke-virtual {v10}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    move-result v0

    if-ge v0, v12, :cond_0

    .line 174
    invoke-virtual {v10}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v0

    invoke-virtual {v9, v0}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v0

    const-string/jumbo v1, "1"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 175
    invoke-virtual {v10}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v0

    invoke-virtual {v9, v0}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v0

    const-string/jumbo v1, "24"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v1

    .line 177
    iget-object v0, p0, Ltv/peel/widget/util/h;->a:Ltv/peel/widget/util/g;

    iget-object v0, v0, Ltv/peel/widget/util/g;->b:Ljava/util/HashMap;

    invoke-virtual {v10}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Ltv/peel/widget/util/h;->a:Ltv/peel/widget/util/g;

    iget-object v0, v0, Ltv/peel/widget/util/g;->b:Ljava/util/HashMap;

    invoke-virtual {v10}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 178
    add-int/lit8 v0, v8, 0x1

    .line 180
    invoke-virtual {v11}, Ljava/util/concurrent/atomic/AtomicInteger;->decrementAndGet()I

    move-result v1

    .line 181
    if-nez v1, :cond_1

    if-ge v0, v12, :cond_1

    .line 182
    iget-object v0, p0, Ltv/peel/widget/util/h;->a:Ltv/peel/widget/util/g;

    iget-object v0, v0, Ltv/peel/widget/util/g;->c:Lcom/peel/util/t;

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lcom/peel/util/t;->a(ZLjava/lang/Object;Ljava/lang/String;)V

    .line 216
    :cond_0
    :goto_1
    return-void

    .line 184
    :cond_1
    if-nez v1, :cond_2

    if-eq v0, v12, :cond_0

    :cond_2
    move v8, v0

    goto :goto_0

    .line 190
    :cond_3
    iget-object v0, p0, Ltv/peel/widget/util/h;->a:Ltv/peel/widget/util/g;

    iget-object v0, v0, Ltv/peel/widget/util/g;->g:Ltv/peel/widget/util/d;

    invoke-static {v0}, Ltv/peel/widget/util/d;->b(Ltv/peel/widget/util/d;)Ljava/util/HashSet;

    move-result-object v0

    if-nez v0, :cond_4

    .line 191
    iget-object v0, p0, Ltv/peel/widget/util/h;->a:Ltv/peel/widget/util/g;

    iget-object v0, v0, Ltv/peel/widget/util/g;->g:Ltv/peel/widget/util/d;

    new-instance v3, Ljava/util/HashSet;

    invoke-direct {v3}, Ljava/util/HashSet;-><init>()V

    invoke-static {v0, v3}, Ltv/peel/widget/util/d;->a(Ltv/peel/widget/util/d;Ljava/util/HashSet;)Ljava/util/HashSet;

    .line 193
    :cond_4
    iget-object v0, p0, Ltv/peel/widget/util/h;->a:Ltv/peel/widget/util/g;

    iget-object v0, v0, Ltv/peel/widget/util/g;->b:Ljava/util/HashMap;

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->containsValue(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 194
    iget-object v0, p0, Ltv/peel/widget/util/h;->a:Ltv/peel/widget/util/g;

    iget-object v0, v0, Ltv/peel/widget/util/g;->b:Ljava/util/HashMap;

    invoke-virtual {v10}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 197
    :cond_5
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 198
    iget-object v3, p0, Ltv/peel/widget/util/h;->a:Ltv/peel/widget/util/g;

    iget-object v3, v3, Ltv/peel/widget/util/g;->g:Ltv/peel/widget/util/d;

    invoke-static {v3, v1}, Ltv/peel/widget/util/d;->a(Ltv/peel/widget/util/d;Lorg/json/JSONArray;)Ljava/lang/String;

    move-result-object v7

    .line 199
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v10}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v3, "|"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v3, "|"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v3, p0, Ltv/peel/widget/util/h;->a:Ltv/peel/widget/util/g;

    iget-object v3, v3, Ltv/peel/widget/util/g;->d:Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v3, "|"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 200
    iget-object v1, p0, Ltv/peel/widget/util/h;->a:Ltv/peel/widget/util/g;

    iget-object v1, v1, Ltv/peel/widget/util/g;->b:Ljava/util/HashMap;

    invoke-virtual {v10}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v3, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 202
    invoke-virtual {v11}, Ljava/util/concurrent/atomic/AtomicInteger;->decrementAndGet()I

    move-result v0

    if-nez v0, :cond_7

    .line 203
    iget-object v0, p0, Ltv/peel/widget/util/h;->a:Ltv/peel/widget/util/g;

    iget-object v0, v0, Ltv/peel/widget/util/g;->g:Ltv/peel/widget/util/d;

    invoke-static {v0}, Ltv/peel/widget/util/d;->b(Ltv/peel/widget/util/d;)Ljava/util/HashSet;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/HashSet;->clear()V

    .line 204
    iget-object v0, p0, Ltv/peel/widget/util/h;->a:Ltv/peel/widget/util/g;

    iget-object v0, v0, Ltv/peel/widget/util/g;->b:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 205
    iget-object v3, p0, Ltv/peel/widget/util/h;->a:Ltv/peel/widget/util/g;

    iget-object v3, v3, Ltv/peel/widget/util/g;->g:Ltv/peel/widget/util/d;

    invoke-static {v3}, Ltv/peel/widget/util/d;->b(Ltv/peel/widget/util/d;)Ljava/util/HashSet;

    move-result-object v3

    iget-object v4, p0, Ltv/peel/widget/util/h;->a:Ltv/peel/widget/util/g;

    iget-object v4, v4, Ltv/peel/widget/util/g;->b:Ljava/util/HashMap;

    invoke-virtual {v4, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    .line 211
    :catch_0
    move-exception v0

    .line 213
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto/16 :goto_1

    .line 207
    :cond_6
    :try_start_1
    iget-object v0, p0, Ltv/peel/widget/util/h;->a:Ltv/peel/widget/util/g;

    iget-object v0, v0, Ltv/peel/widget/util/g;->e:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v1, "popular"

    iget-object v3, p0, Ltv/peel/widget/util/h;->a:Ltv/peel/widget/util/g;

    iget-object v3, v3, Ltv/peel/widget/util/g;->g:Ltv/peel/widget/util/d;

    invoke-static {v3}, Ltv/peel/widget/util/d;->b(Ltv/peel/widget/util/d;)Ljava/util/HashSet;

    move-result-object v3

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences$Editor;->putStringSet(Ljava/lang/String;Ljava/util/Set;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 209
    :cond_7
    iget-object v0, p0, Ltv/peel/widget/util/h;->a:Ltv/peel/widget/util/g;

    iget-object v0, v0, Ltv/peel/widget/util/g;->g:Ltv/peel/widget/util/d;

    invoke-virtual {v10}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v1

    invoke-virtual {v11}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v3

    iget-object v4, p0, Ltv/peel/widget/util/h;->a:Ltv/peel/widget/util/g;

    iget-object v4, v4, Ltv/peel/widget/util/g;->c:Lcom/peel/util/t;

    iget-object v5, p0, Ltv/peel/widget/util/h;->a:Ltv/peel/widget/util/g;

    iget-object v5, v5, Ltv/peel/widget/util/g;->f:Landroid/widget/RemoteViews;

    iget-object v6, p0, Ltv/peel/widget/util/h;->a:Ltv/peel/widget/util/g;

    iget-object v6, v6, Ltv/peel/widget/util/g;->d:Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static/range {v0 .. v7}, Ltv/peel/widget/util/d;->a(Ltv/peel/widget/util/d;ILjava/lang/String;ILcom/peel/util/t;Landroid/widget/RemoteViews;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0
.end method
