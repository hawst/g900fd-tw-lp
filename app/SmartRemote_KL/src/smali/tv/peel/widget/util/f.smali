.class Ltv/peel/widget/util/f;
.super Lcom/peel/util/t;


# instance fields
.field final synthetic a:Ljava/lang/StringBuilder;

.field final synthetic b:Ljava/lang/String;

.field final synthetic c:Ltv/peel/widget/util/e;


# direct methods
.method constructor <init>(Ltv/peel/widget/util/e;ILjava/lang/StringBuilder;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 90
    iput-object p1, p0, Ltv/peel/widget/util/f;->c:Ltv/peel/widget/util/e;

    iput-object p3, p0, Ltv/peel/widget/util/f;->a:Ljava/lang/StringBuilder;

    iput-object p4, p0, Ltv/peel/widget/util/f;->b:Ljava/lang/String;

    invoke-direct {p0, p2}, Lcom/peel/util/t;-><init>(I)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .prologue
    .line 93
    :try_start_0
    iget-boolean v0, p0, Ltv/peel/widget/util/f;->i:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Ltv/peel/widget/util/f;->j:Ljava/lang/Object;

    if-nez v0, :cond_1

    .line 94
    :cond_0
    iget-object v0, p0, Ltv/peel/widget/util/f;->a:Ljava/lang/StringBuilder;

    const-string/jumbo v1, "http://popular.%ssamsung.peel.com/epg/schedules/popular?country=%s&limit=5"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string/jumbo v4, ""

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-object v4, p0, Ltv/peel/widget/util/f;->b:Ljava/lang/String;

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 95
    iget-object v0, p0, Ltv/peel/widget/util/f;->c:Ltv/peel/widget/util/e;

    iget-object v0, v0, Ltv/peel/widget/util/e;->f:Ltv/peel/widget/util/d;

    iget-object v1, p0, Ltv/peel/widget/util/f;->a:Ljava/lang/StringBuilder;

    iget-object v2, p0, Ltv/peel/widget/util/f;->c:Ltv/peel/widget/util/e;

    iget-object v2, v2, Ltv/peel/widget/util/e;->a:Lcom/peel/util/t;

    iget-object v3, p0, Ltv/peel/widget/util/f;->c:Ltv/peel/widget/util/e;

    iget-object v3, v3, Ltv/peel/widget/util/e;->b:Landroid/content/SharedPreferences;

    iget-object v4, p0, Ltv/peel/widget/util/f;->c:Ltv/peel/widget/util/e;

    iget-object v4, v4, Ltv/peel/widget/util/e;->c:Ljava/util/HashMap;

    iget-object v5, p0, Ltv/peel/widget/util/f;->c:Ltv/peel/widget/util/e;

    iget-object v5, v5, Ltv/peel/widget/util/e;->d:Ljava/lang/StringBuilder;

    iget-object v6, p0, Ltv/peel/widget/util/f;->c:Ltv/peel/widget/util/e;

    iget-object v6, v6, Ltv/peel/widget/util/e;->e:Landroid/widget/RemoteViews;

    invoke-static/range {v0 .. v6}, Ltv/peel/widget/util/d;->a(Ltv/peel/widget/util/d;Ljava/lang/StringBuilder;Lcom/peel/util/t;Landroid/content/SharedPreferences;Ljava/util/HashMap;Ljava/lang/StringBuilder;Landroid/widget/RemoteViews;)V

    .line 120
    :goto_0
    return-void

    .line 99
    :cond_1
    iget-object v0, p0, Ltv/peel/widget/util/f;->j:Ljava/lang/Object;

    check-cast v0, Ljava/util/ArrayList;

    .line 101
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    .line 102
    iget-object v2, p0, Ltv/peel/widget/util/f;->b:Ljava/lang/String;

    const-string/jumbo v3, "iso"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 103
    const-string/jumbo v1, "endpoint"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 104
    const-string/jumbo v1, "europe"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 105
    iget-object v0, p0, Ltv/peel/widget/util/f;->c:Ltv/peel/widget/util/e;

    iget-object v0, v0, Ltv/peel/widget/util/e;->d:Ljava/lang/StringBuilder;

    const-string/jumbo v1, "eu."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 106
    iget-object v0, p0, Ltv/peel/widget/util/f;->a:Ljava/lang/StringBuilder;

    const-string/jumbo v1, "http://popular.%ssamsung.peel.com/epg/schedules/popular?country=%s&limit=5"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string/jumbo v4, "eu."

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-object v4, p0, Ltv/peel/widget/util/f;->b:Ljava/lang/String;

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 116
    :cond_3
    :goto_1
    iget-object v0, p0, Ltv/peel/widget/util/f;->c:Ltv/peel/widget/util/e;

    iget-object v0, v0, Ltv/peel/widget/util/e;->f:Ltv/peel/widget/util/d;

    iget-object v1, p0, Ltv/peel/widget/util/f;->a:Ljava/lang/StringBuilder;

    iget-object v2, p0, Ltv/peel/widget/util/f;->c:Ltv/peel/widget/util/e;

    iget-object v2, v2, Ltv/peel/widget/util/e;->a:Lcom/peel/util/t;

    iget-object v3, p0, Ltv/peel/widget/util/f;->c:Ltv/peel/widget/util/e;

    iget-object v3, v3, Ltv/peel/widget/util/e;->b:Landroid/content/SharedPreferences;

    iget-object v4, p0, Ltv/peel/widget/util/f;->c:Ltv/peel/widget/util/e;

    iget-object v4, v4, Ltv/peel/widget/util/e;->c:Ljava/util/HashMap;

    iget-object v5, p0, Ltv/peel/widget/util/f;->c:Ltv/peel/widget/util/e;

    iget-object v5, v5, Ltv/peel/widget/util/e;->d:Ljava/lang/StringBuilder;

    iget-object v6, p0, Ltv/peel/widget/util/f;->c:Ltv/peel/widget/util/e;

    iget-object v6, v6, Ltv/peel/widget/util/e;->e:Landroid/widget/RemoteViews;

    invoke-static/range {v0 .. v6}, Ltv/peel/widget/util/d;->a(Ltv/peel/widget/util/d;Ljava/lang/StringBuilder;Lcom/peel/util/t;Landroid/content/SharedPreferences;Ljava/util/HashMap;Ljava/lang/StringBuilder;Landroid/widget/RemoteViews;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 117
    :catch_0
    move-exception v0

    .line 118
    invoke-static {}, Ltv/peel/widget/util/d;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {}, Ltv/peel/widget/util/d;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_0

    .line 107
    :cond_4
    :try_start_1
    const-string/jumbo v1, "asia"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 108
    iget-object v0, p0, Ltv/peel/widget/util/f;->c:Ltv/peel/widget/util/e;

    iget-object v0, v0, Ltv/peel/widget/util/e;->d:Ljava/lang/StringBuilder;

    const-string/jumbo v1, "asia."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 109
    iget-object v0, p0, Ltv/peel/widget/util/f;->a:Ljava/lang/StringBuilder;

    const-string/jumbo v1, "http://popular.%ssamsung.peel.com/epg/schedules/popular?country=%s&limit=5"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string/jumbo v4, "asia."

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-object v4, p0, Ltv/peel/widget/util/f;->b:Ljava/lang/String;

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 111
    :cond_5
    iget-object v0, p0, Ltv/peel/widget/util/f;->a:Ljava/lang/StringBuilder;

    const-string/jumbo v1, "http://popular.%ssamsung.peel.com/epg/schedules/popular?country=%s&limit=5"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string/jumbo v4, ""

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-object v4, p0, Ltv/peel/widget/util/f;->b:Ljava/lang/String;

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1
.end method
