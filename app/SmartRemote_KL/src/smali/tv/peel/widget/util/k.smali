.class Ltv/peel/widget/util/k;
.super Ljava/lang/Object;

# interfaces
.implements Ltv/peel/widget/util/c;


# instance fields
.field final synthetic a:Ltv/peel/widget/util/WidgetService;


# direct methods
.method constructor <init>(Ltv/peel/widget/util/WidgetService;)V
    .locals 0

    .prologue
    .line 83
    iput-object p1, p0, Ltv/peel/widget/util/k;->a:Ltv/peel/widget/util/WidgetService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/content/Context;I)V
    .locals 2

    .prologue
    .line 86
    iget-object v0, p0, Ltv/peel/widget/util/k;->a:Ltv/peel/widget/util/WidgetService;

    invoke-static {v0}, Ltv/peel/widget/util/WidgetService;->a(Ltv/peel/widget/util/WidgetService;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 87
    invoke-static {}, Ltv/peel/widget/util/WidgetService;->a()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "The service is on"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 88
    iget-object v0, p0, Ltv/peel/widget/util/k;->a:Ltv/peel/widget/util/WidgetService;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Ltv/peel/widget/util/WidgetService;->a(Ltv/peel/widget/util/WidgetService;Z)Z

    .line 97
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 98
    const-string/jumbo v1, "tv.peel.widget.action.UPDATE_WIDGETS"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 99
    invoke-virtual {p1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 101
    :cond_0
    return-void
.end method
