.class public Ltv/peel/widget/util/d;
.super Ljava/lang/Object;


# static fields
.field private static final a:Ljava/lang/String;

.field private static d:I


# instance fields
.field private b:Landroid/content/Context;

.field private c:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 33
    const-class v0, Ltv/peel/widget/util/d;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Ltv/peel/widget/util/d;->a:Ljava/lang/String;

    .line 37
    const/16 v0, 0xa0

    sput v0, Ltv/peel/widget/util/d;->d:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    iput-object p1, p0, Ltv/peel/widget/util/d;->b:Landroid/content/Context;

    .line 41
    return-void
.end method

.method static synthetic a(Ltv/peel/widget/util/d;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Ltv/peel/widget/util/d;->b:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 32
    sget-object v0, Ltv/peel/widget/util/d;->a:Ljava/lang/String;

    return-object v0
.end method

.method private a(Lorg/json/JSONArray;)Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 227
    const/4 v1, 0x0

    move v5, v0

    move-object v0, v1

    move v1, v5

    .line 229
    :goto_0
    invoke-virtual {p1}, Lorg/json/JSONArray;->length()I

    move-result v2

    if-ge v1, v2, :cond_1

    .line 231
    :try_start_0
    invoke-virtual {p1, v1}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "x"

    invoke-virtual {v2, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 232
    const/4 v3, 0x0

    aget-object v3, v2, v3

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    sget v4, Ltv/peel/widget/util/d;->d:I

    if-lt v3, v4, :cond_2

    .line 233
    if-nez v0, :cond_0

    .line 234
    invoke-virtual {p1, v1}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 236
    :cond_0
    const/4 v3, 0x0

    aget-object v3, v2, v3

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    const/4 v4, 0x1

    aget-object v2, v2, v4

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    if-le v3, v2, :cond_2

    .line 237
    invoke-virtual {p1, v1}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 246
    :cond_1
    return-object v0

    .line 240
    :catch_0
    move-exception v2

    .line 242
    invoke-virtual {v2}, Lorg/json/JSONException;->printStackTrace()V

    .line 229
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method static synthetic a(Ltv/peel/widget/util/d;Lorg/json/JSONArray;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 32
    invoke-direct {p0, p1}, Ltv/peel/widget/util/d;->a(Lorg/json/JSONArray;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Ltv/peel/widget/util/d;Ljava/util/HashSet;)Ljava/util/HashSet;
    .locals 0

    .prologue
    .line 32
    iput-object p1, p0, Ltv/peel/widget/util/d;->c:Ljava/util/HashSet;

    return-object p1
.end method

.method private a(ILjava/lang/String;ILcom/peel/util/t;Landroid/widget/RemoteViews;Ljava/lang/String;Ljava/lang/String;)V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    .line 250
    const-string/jumbo v1, "http://popular.%ssamsung.peel.com/epg/schedules/pictures/%s/%s"

    .line 252
    if-nez p1, :cond_0

    .line 253
    sget v0, Lcom/peel/ui/fp;->img1:I

    .line 261
    :goto_0
    iget-object v2, p0, Ltv/peel/widget/util/d;->b:Landroid/content/Context;

    invoke-static {v2}, Lcom/peel/util/b/e;->a(Landroid/content/Context;)Lcom/squareup/picasso/Picasso;

    move-result-object v2

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p6, v3, v4

    aput-object p2, v3, v5

    aput-object p7, v3, v6

    .line 262
    invoke-static {v1, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Lcom/squareup/picasso/Picasso;->load(Ljava/lang/String;)Lcom/squareup/picasso/RequestCreator;

    move-result-object v1

    new-instance v2, Ltv/peel/widget/util/i;

    invoke-direct {v2, p0, p5, v0, p4}, Ltv/peel/widget/util/i;-><init>(Ltv/peel/widget/util/d;Landroid/widget/RemoteViews;ILcom/peel/util/t;)V

    .line 263
    invoke-virtual {v1, v2}, Lcom/squareup/picasso/RequestCreator;->into(Lcom/squareup/picasso/Target;)V

    .line 280
    return-void

    .line 254
    :cond_0
    if-ne p1, v5, :cond_1

    .line 255
    sget v0, Lcom/peel/ui/fp;->img2:I

    goto :goto_0

    .line 256
    :cond_1
    if-ne p1, v6, :cond_2

    .line 257
    sget v0, Lcom/peel/ui/fp;->img3:I

    goto :goto_0

    .line 259
    :cond_2
    const/4 v0, -0x1

    goto :goto_0
.end method

.method private a(Ljava/lang/StringBuilder;Lcom/peel/util/t;Landroid/content/SharedPreferences;Ljava/util/HashMap;Ljava/lang/StringBuilder;Landroid/widget/RemoteViews;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/StringBuilder;",
            "Lcom/peel/util/t;",
            "Landroid/content/SharedPreferences;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/StringBuilder;",
            "Landroid/widget/RemoteViews;",
            ")V"
        }
    .end annotation

    .prologue
    .line 157
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 158
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v8

    const-string/jumbo v9, "processPopularStuff"

    new-instance v0, Ltv/peel/widget/util/g;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p4

    move-object v4, p2

    move-object v5, p5

    move-object v6, p3

    move-object/from16 v7, p6

    invoke-direct/range {v0 .. v7}, Ltv/peel/widget/util/g;-><init>(Ltv/peel/widget/util/d;Ljava/lang/StringBuilder;Ljava/util/HashMap;Lcom/peel/util/t;Ljava/lang/StringBuilder;Landroid/content/SharedPreferences;Landroid/widget/RemoteViews;)V

    invoke-static {v8, v9, v0}, Lcom/peel/util/i;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    .line 224
    :goto_0
    return-void

    .line 222
    :cond_0
    const/4 v0, 0x1

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {p2, v0, v1, v2}, Lcom/peel/util/t;->a(ZLjava/lang/Object;Ljava/lang/String;)V

    goto :goto_0
.end method

.method static synthetic a(Ltv/peel/widget/util/d;ILjava/lang/String;ILcom/peel/util/t;Landroid/widget/RemoteViews;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 32
    invoke-direct/range {p0 .. p7}, Ltv/peel/widget/util/d;->a(ILjava/lang/String;ILcom/peel/util/t;Landroid/widget/RemoteViews;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic a(Ltv/peel/widget/util/d;Ljava/lang/StringBuilder;Lcom/peel/util/t;Landroid/content/SharedPreferences;Ljava/util/HashMap;Ljava/lang/StringBuilder;Landroid/widget/RemoteViews;)V
    .locals 0

    .prologue
    .line 32
    invoke-direct/range {p0 .. p6}, Ltv/peel/widget/util/d;->a(Ljava/lang/StringBuilder;Lcom/peel/util/t;Landroid/content/SharedPreferences;Ljava/util/HashMap;Ljava/lang/StringBuilder;Landroid/widget/RemoteViews;)V

    return-void
.end method

.method static synthetic b(Ltv/peel/widget/util/d;)Ljava/util/HashSet;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Ltv/peel/widget/util/d;->c:Ljava/util/HashSet;

    return-object v0
.end method


# virtual methods
.method public a(Lcom/peel/util/t;Landroid/widget/RemoteViews;)V
    .locals 13

    .prologue
    .line 44
    new-instance v8, Ljava/util/HashMap;

    invoke-direct {v8}, Ljava/util/HashMap;-><init>()V

    .line 45
    iget-object v0, p0, Ltv/peel/widget/util/d;->b:Landroid/content/Context;

    const-string/jumbo v1, "widget_pref"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v9

    .line 46
    const-string/jumbo v0, "popular"

    const/4 v1, 0x0

    invoke-interface {v9, v0, v1}, Landroid/content/SharedPreferences;->getStringSet(Ljava/lang/String;Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    check-cast v0, Ljava/util/HashSet;

    iput-object v0, p0, Ltv/peel/widget/util/d;->c:Ljava/util/HashSet;

    .line 47
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    .line 49
    iget-object v0, p0, Ltv/peel/widget/util/d;->c:Ljava/util/HashSet;

    if-eqz v0, :cond_1

    .line 50
    iget-object v0, p0, Ltv/peel/widget/util/d;->c:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 51
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 52
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 53
    const-string/jumbo v2, "\\|"

    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 56
    const/4 v2, 0x0

    :try_start_0
    aget-object v2, v0, v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v4, 0x1

    aget-object v4, v0, v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "|"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const/4 v4, 0x2

    aget-object v4, v0, v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "|"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const/4 v4, 0x3

    aget-object v0, v0, v4

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v8, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 57
    :catch_0
    move-exception v0

    goto :goto_0

    .line 61
    :cond_0
    new-instance v11, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v8}, Ljava/util/HashMap;->size()I

    move-result v0

    invoke-direct {v11, v0}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    .line 63
    invoke-virtual {v8}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    .line 64
    if-eqz v0, :cond_1

    .line 65
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v12

    .line 66
    :goto_1
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 67
    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 68
    invoke-virtual {v8, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 70
    const-string/jumbo v2, "\\|"

    invoke-virtual {v1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 71
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    const/4 v0, 0x0

    aget-object v2, v4, v0

    invoke-virtual {v11}, Ljava/util/concurrent/atomic/AtomicInteger;->decrementAndGet()I

    move-result v3

    const/4 v0, 0x1

    aget-object v6, v4, v0

    array-length v0, v4

    add-int/lit8 v0, v0, -0x1

    aget-object v7, v4, v0

    move-object v0, p0

    move-object v4, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v7}, Ltv/peel/widget/util/d;->a(ILjava/lang/String;ILcom/peel/util/t;Landroid/widget/RemoteViews;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 76
    :cond_1
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v7

    const-string/jumbo v11, "get data from api"

    new-instance v0, Ltv/peel/widget/util/e;

    move-object v1, p0

    move-object v2, p1

    move-object v3, v9

    move-object v4, v8

    move-object v5, v10

    move-object v6, p2

    invoke-direct/range {v0 .. v6}, Ltv/peel/widget/util/e;-><init>(Ltv/peel/widget/util/d;Lcom/peel/util/t;Landroid/content/SharedPreferences;Ljava/util/HashMap;Ljava/lang/StringBuilder;Landroid/widget/RemoteViews;)V

    invoke-static {v7, v11, v0}, Lcom/peel/util/i;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    .line 153
    return-void
.end method
