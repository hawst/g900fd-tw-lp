.class Ltv/peel/widget/a/h;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private a:Landroid/content/Context;

.field private b:I

.field private c:Landroid/app/PendingIntent;

.field private d:Landroid/content/Intent;

.field private e:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;ILandroid/app/PendingIntent;Landroid/content/Intent;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 457
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 458
    iput-object p1, p0, Ltv/peel/widget/a/h;->a:Landroid/content/Context;

    .line 459
    iput p2, p0, Ltv/peel/widget/a/h;->b:I

    .line 460
    iput-object p3, p0, Ltv/peel/widget/a/h;->c:Landroid/app/PendingIntent;

    .line 461
    iput-object p4, p0, Ltv/peel/widget/a/h;->d:Landroid/content/Intent;

    .line 462
    iput-object p5, p0, Ltv/peel/widget/a/h;->e:Ljava/lang/String;

    .line 463
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    .line 469
    :try_start_0
    iget v0, p0, Ltv/peel/widget/a/h;->b:I

    const/16 v1, 0x64

    if-ne v0, v1, :cond_0

    .line 470
    const/4 v0, 0x1

    sput-boolean v0, Ltv/peel/widget/WidgetService;->e:Z

    .line 471
    invoke-static {}, Ltv/peel/widget/a/a;->a()V

    .line 473
    :cond_0
    iget-object v0, p0, Ltv/peel/widget/a/h;->c:Landroid/app/PendingIntent;

    iget-object v1, p0, Ltv/peel/widget/a/h;->a:Landroid/content/Context;

    iget v2, p0, Ltv/peel/widget/a/h;->b:I

    iget-object v3, p0, Ltv/peel/widget/a/h;->d:Landroid/content/Intent;

    invoke-virtual {v0, v1, v2, v3}, Landroid/app/PendingIntent;->send(Landroid/content/Context;ILandroid/content/Intent;)V
    :try_end_0
    .catch Landroid/app/PendingIntent$CanceledException; {:try_start_0 .. :try_end_0} :catch_0

    .line 478
    :goto_0
    return-void

    .line 475
    :catch_0
    move-exception v0

    .line 476
    invoke-static {}, Ltv/peel/widget/a/a;->m()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Ltv/peel/widget/a/h;->e:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " button sending intent failed."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
