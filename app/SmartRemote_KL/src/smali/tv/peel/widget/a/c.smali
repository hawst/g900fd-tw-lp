.class final Ltv/peel/widget/a/c;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/view/View$OnTouchListener;


# instance fields
.field a:I

.field b:I

.field c:F

.field d:F

.field e:F

.field f:F

.field final synthetic g:Landroid/view/GestureDetector;

.field final synthetic h:Landroid/util/DisplayMetrics;


# direct methods
.method constructor <init>(Landroid/view/GestureDetector;Landroid/util/DisplayMetrics;)V
    .locals 0

    .prologue
    .line 151
    iput-object p1, p0, Ltv/peel/widget/a/c;->g:Landroid/view/GestureDetector;

    iput-object p2, p0, Ltv/peel/widget/a/c;->h:Landroid/util/DisplayMetrics;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 160
    iget-object v2, p0, Ltv/peel/widget/a/c;->g:Landroid/view/GestureDetector;

    invoke-virtual {v2, p2}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 161
    invoke-static {}, Ltv/peel/widget/a/a;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 162
    invoke-static {}, Ltv/peel/widget/a/a;->c()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    iget v0, v0, Landroid/view/WindowManager$LayoutParams;->x:I

    int-to-float v0, v0

    iput v0, p0, Ltv/peel/widget/a/c;->e:F

    .line 163
    invoke-static {}, Ltv/peel/widget/a/a;->c()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    iget v0, v0, Landroid/view/WindowManager$LayoutParams;->y:I

    int-to-float v0, v0

    iput v0, p0, Ltv/peel/widget/a/c;->f:F

    .line 164
    invoke-static {}, Ltv/peel/widget/a/a;->c()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    invoke-static {}, Ltv/peel/widget/a/a;->d()I

    move-result v2

    iput v2, v0, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 165
    invoke-static {}, Ltv/peel/widget/a/a;->c()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    invoke-static {}, Ltv/peel/widget/a/a;->e()I

    move-result v2

    iput v2, v0, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 166
    invoke-static {}, Ltv/peel/widget/a/a;->f()V

    .line 167
    invoke-static {}, Ltv/peel/widget/a/a;->h()Landroid/view/WindowManager;

    move-result-object v0

    invoke-static {}, Ltv/peel/widget/a/a;->g()Landroid/view/ViewGroup;

    move-result-object v2

    invoke-static {}, Ltv/peel/widget/a/a;->c()Landroid/view/WindowManager$LayoutParams;

    move-result-object v3

    invoke-interface {v0, v2, v3}, Landroid/view/WindowManager;->updateViewLayout(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    :goto_0
    move v0, v1

    .line 201
    :goto_1
    return v0

    .line 170
    :cond_0
    invoke-static {}, Ltv/peel/widget/a/a;->i()V

    .line 171
    invoke-static {}, Ltv/peel/widget/a/a;->c()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    iget v2, p0, Ltv/peel/widget/a/c;->e:F

    float-to-int v2, v2

    iput v2, v0, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 172
    invoke-static {}, Ltv/peel/widget/a/a;->c()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    iget v2, p0, Ltv/peel/widget/a/c;->f:F

    float-to-int v2, v2

    iput v2, v0, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 173
    invoke-static {}, Ltv/peel/widget/a/a;->h()Landroid/view/WindowManager;

    move-result-object v0

    invoke-static {}, Ltv/peel/widget/a/a;->g()Landroid/view/ViewGroup;

    move-result-object v2

    invoke-static {}, Ltv/peel/widget/a/a;->c()Landroid/view/WindowManager$LayoutParams;

    move-result-object v3

    invoke-interface {v0, v2, v3}, Landroid/view/WindowManager;->updateViewLayout(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0

    .line 177
    :cond_1
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    goto :goto_1

    .line 179
    :pswitch_0
    invoke-static {}, Ltv/peel/widget/a/a;->c()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    iget v0, v0, Landroid/view/WindowManager$LayoutParams;->x:I

    iput v0, p0, Ltv/peel/widget/a/c;->a:I

    .line 180
    invoke-static {}, Ltv/peel/widget/a/a;->c()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    iget v0, v0, Landroid/view/WindowManager$LayoutParams;->y:I

    iput v0, p0, Ltv/peel/widget/a/c;->b:I

    .line 181
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawX()F

    move-result v0

    iput v0, p0, Ltv/peel/widget/a/c;->c:F

    .line 182
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawY()F

    move-result v0

    iput v0, p0, Ltv/peel/widget/a/c;->d:F

    move v0, v1

    .line 183
    goto :goto_1

    .line 185
    :pswitch_1
    invoke-static {}, Ltv/peel/widget/a/a;->b()Z

    move-result v0

    if-nez v0, :cond_2

    .line 186
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawY()F

    move-result v0

    iget v2, p0, Ltv/peel/widget/a/c;->d:F

    sub-float/2addr v0, v2

    float-to-int v0, v0

    .line 187
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawX()F

    move-result v2

    iget v3, p0, Ltv/peel/widget/a/c;->c:F

    sub-float/2addr v2, v3

    float-to-int v2, v2

    .line 188
    invoke-static {}, Ltv/peel/widget/a/a;->c()Landroid/view/WindowManager$LayoutParams;

    move-result-object v3

    iget v4, p0, Ltv/peel/widget/a/c;->b:I

    add-int/2addr v0, v4

    iput v0, v3, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 189
    invoke-static {}, Ltv/peel/widget/a/a;->c()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    iget v3, p0, Ltv/peel/widget/a/c;->a:I

    add-int/2addr v2, v3

    iput v2, v0, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 190
    invoke-static {}, Ltv/peel/widget/a/a;->h()Landroid/view/WindowManager;

    move-result-object v0

    invoke-static {}, Ltv/peel/widget/a/a;->g()Landroid/view/ViewGroup;

    move-result-object v2

    invoke-static {}, Ltv/peel/widget/a/a;->c()Landroid/view/WindowManager$LayoutParams;

    move-result-object v3

    invoke-interface {v0, v2, v3}, Landroid/view/WindowManager;->updateViewLayout(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    :cond_2
    move v0, v1

    .line 192
    goto/16 :goto_1

    .line 194
    :pswitch_2
    invoke-static {}, Ltv/peel/widget/a/a;->b()Z

    move-result v2

    if-nez v2, :cond_4

    .line 195
    invoke-static {}, Ltv/peel/widget/a/a;->c()Landroid/view/WindowManager$LayoutParams;

    move-result-object v2

    invoke-static {}, Ltv/peel/widget/a/a;->c()Landroid/view/WindowManager$LayoutParams;

    move-result-object v3

    iget v3, v3, Landroid/view/WindowManager$LayoutParams;->x:I

    iget-object v4, p0, Ltv/peel/widget/a/c;->h:Landroid/util/DisplayMetrics;

    iget v4, v4, Landroid/util/DisplayMetrics;->widthPixels:I

    div-int/lit8 v4, v4, 0x2

    if-lt v3, v4, :cond_3

    invoke-static {}, Ltv/peel/widget/a/a;->d()I

    move-result v0

    :cond_3
    iput v0, v2, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 196
    invoke-static {}, Ltv/peel/widget/a/a;->h()Landroid/view/WindowManager;

    move-result-object v0

    invoke-static {}, Ltv/peel/widget/a/a;->g()Landroid/view/ViewGroup;

    move-result-object v2

    invoke-static {}, Ltv/peel/widget/a/a;->c()Landroid/view/WindowManager$LayoutParams;

    move-result-object v3

    invoke-interface {v0, v2, v3}, Landroid/view/WindowManager;->updateViewLayout(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    :cond_4
    move v0, v1

    .line 198
    goto/16 :goto_1

    .line 177
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method
