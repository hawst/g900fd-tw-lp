.class public Ltv/peel/widget/a/i;
.super Ljava/lang/Object;


# static fields
.field public static a:Ljava/lang/String;

.field public static b:Ltv/peel/widget/b;

.field private static final c:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 40
    const-class v0, Ltv/peel/widget/a/i;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Ltv/peel/widget/a/i;->c:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Landroid/content/Context;ILjava/lang/String;Landroid/widget/RemoteViews;ZI)V
    .locals 3

    .prologue
    .line 162
    new-instance v0, Landroid/content/Intent;

    const-class v1, Ltv/peel/widget/WidgetService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 163
    const-string/jumbo v1, "tv.peel.widget.action.BUTTON_PRESSED"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 165
    if-eqz p4, :cond_0

    .line 166
    const-string/jumbo v1, "tv.peel.widget.extra.index"

    invoke-virtual {v0, v1, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 167
    const-string/jumbo v1, "tv.peel.widget.extra.COMMAND"

    const-string/jumbo v2, "PowerOn|PowerOff"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 171
    :goto_0
    const-string/jumbo v1, "tv.peel.widget.extra.field"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 172
    const-string/jumbo v1, "tv.peel.widget.extra.WIDGET_TYPE"

    const/16 v2, 0x1f40

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 174
    const/high16 v1, 0x8000000

    invoke-static {p0, p1, v0, v1}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    invoke-virtual {p3, p1, v0}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 175
    return-void

    .line 169
    :cond_0
    const-string/jumbo v1, "tv.peel.widget.extra.COMMAND"

    const-string/jumbo v2, "Power"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_0
.end method

.method private static a(Landroid/content/Context;Landroid/widget/RemoteViews;)V
    .locals 6

    .prologue
    const/high16 v5, 0x8000000

    const/high16 v4, 0x200000

    const/4 v3, 0x3

    .line 288
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->getLaunchIntentForPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 289
    const-string/jumbo v1, "context_id"

    const/16 v2, 0x1f40

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 290
    const/high16 v1, 0x30000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 291
    const-string/jumbo v1, "from"

    const-string/jumbo v2, "HOMESCREEN"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 292
    sget v1, Lcom/peel/ui/fp;->btn_peel_tv:I

    invoke-static {p0, v3, v0, v4}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v2

    invoke-virtual {p1, v1, v2}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 293
    sget v1, Lcom/peel/ui/fp;->btn_peel_tv1:I

    invoke-static {p0, v3, v0, v4}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 296
    new-instance v0, Landroid/content/Intent;

    const-class v1, Ltv/peel/widget/WidgetService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 297
    const-string/jumbo v1, "tv.peel.widget.action.CHANGE_DEVICE"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 300
    const-string/jumbo v1, "tv.peel.widget.extra.COMMAND"

    sget v2, Lcom/peel/ui/fp;->widget_device_select_prev:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 301
    sget v1, Lcom/peel/ui/fp;->widget_device_select_prev:I

    sget v2, Lcom/peel/ui/fp;->widget_device_select_prev:I

    invoke-static {p0, v2, v0, v5}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v2

    invoke-virtual {p1, v1, v2}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 303
    const-string/jumbo v1, "tv.peel.widget.extra.COMMAND"

    sget v2, Lcom/peel/ui/fp;->widget_device_select_next:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 304
    sget v1, Lcom/peel/ui/fp;->widget_device_select_next:I

    sget v2, Lcom/peel/ui/fp;->widget_device_select_next:I

    invoke-static {p0, v2, v0, v5}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 305
    return-void
.end method

.method private static a(Landroid/content/Context;Landroid/widget/RemoteViews;ILjava/lang/String;)V
    .locals 2

    .prologue
    .line 178
    new-instance v0, Landroid/content/Intent;

    const-class v1, Ltv/peel/widget/WidgetService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 179
    const-string/jumbo v1, "tv.peel.widget.action.BUTTON_PRESSED"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 180
    const-string/jumbo v1, "tv.peel.widget.extra.COMMAND"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 181
    const/high16 v1, 0x8000000

    invoke-static {p0, p2, v0, v1}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    invoke-virtual {p1, p2, v0}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 182
    return-void
.end method

.method private static a(Landroid/content/Context;Landroid/widget/RemoteViews;IZ)V
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 186
    sget v1, Lcom/peel/ui/fp;->btn8:I

    if-eqz p3, :cond_3

    const-string/jumbo v0, "Select"

    :goto_0
    invoke-static {p0, p1, v1, v0}, Ltv/peel/widget/a/i;->a(Landroid/content/Context;Landroid/widget/RemoteViews;ILjava/lang/String;)V

    .line 187
    sget v0, Lcom/peel/ui/fp;->btn9:I

    const-string/jumbo v1, "Mute"

    invoke-static {p0, p1, v0, v1}, Ltv/peel/widget/a/i;->a(Landroid/content/Context;Landroid/widget/RemoteViews;ILjava/lang/String;)V

    .line 189
    sget v0, Lcom/peel/ui/fp;->btn_peel_tv:I

    sget v1, Lcom/peel/ui/fo;->widget_expand_icon:I

    invoke-virtual {p1, v0, v1}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    .line 190
    sget v0, Lcom/peel/ui/fp;->btn_peel_tv:I

    const-string/jumbo v1, "setBackgroundResource"

    sget v2, Lcom/peel/ui/fo;->lockscreen_power_button_stateful:I

    invoke-virtual {p1, v0, v1, v2}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    .line 191
    sget v0, Lcom/peel/ui/fp;->textview1:I

    sget v1, Lcom/peel/ui/ft;->lock_btn_vol:I

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 192
    sget v0, Lcom/peel/ui/fp;->textview2:I

    sget v1, Lcom/peel/ui/ft;->lock_btn_ch:I

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 194
    sget v0, Lcom/peel/ui/fp;->btn1:I

    const-string/jumbo v1, "setBackgroundResource"

    sget v2, Lcom/peel/ui/fo;->lockscreen_power_button_stateful:I

    invoke-virtual {p1, v0, v1, v2}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    .line 195
    sget v0, Lcom/peel/ui/fp;->btn2:I

    const-string/jumbo v1, "setBackgroundResource"

    sget v2, Lcom/peel/ui/fo;->lockscreen_power_button_stateful:I

    invoke-virtual {p1, v0, v1, v2}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    .line 196
    sget v0, Lcom/peel/ui/fp;->btn3:I

    const-string/jumbo v1, "setBackgroundResource"

    sget v2, Lcom/peel/ui/fo;->lockscreen_power_button_stateful:I

    invoke-virtual {p1, v0, v1, v2}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    .line 197
    sget v1, Lcom/peel/ui/fp;->btn1:I

    sget v2, Lcom/peel/ui/fo;->lock_qw_icon:I

    move-object v0, p1

    move v4, v3

    move v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/widget/RemoteViews;->setTextViewCompoundDrawables(IIIII)V

    .line 198
    sget v1, Lcom/peel/ui/fp;->btn2:I

    sget v2, Lcom/peel/ui/fo;->lock_qw_icon:I

    move-object v0, p1

    move v4, v3

    move v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/widget/RemoteViews;->setTextViewCompoundDrawables(IIIII)V

    .line 199
    sget v1, Lcom/peel/ui/fp;->btn3:I

    sget v2, Lcom/peel/ui/fo;->lock_qw_icon:I

    move-object v0, p1

    move v4, v3

    move v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/widget/RemoteViews;->setTextViewCompoundDrawables(IIIII)V

    .line 202
    sget v0, Lcom/peel/ui/fp;->btn4:I

    sget v1, Lcom/peel/ui/fo;->lockscreen_vol_up_states:I

    invoke-virtual {p1, v0, v1}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    .line 203
    sget v0, Lcom/peel/ui/fp;->btn5:I

    sget v1, Lcom/peel/ui/fo;->lockscreen_vol_dn_states:I

    invoke-virtual {p1, v0, v1}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    .line 204
    sget v0, Lcom/peel/ui/fp;->btn6:I

    sget v1, Lcom/peel/ui/fo;->lockscreen_ch_up_states:I

    invoke-virtual {p1, v0, v1}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    .line 205
    sget v0, Lcom/peel/ui/fp;->btn7:I

    sget v1, Lcom/peel/ui/fo;->lockscreen_ch_dn_states:I

    invoke-virtual {p1, v0, v1}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    .line 207
    sget v0, Lcom/peel/ui/fp;->btn8:I

    const-string/jumbo v1, "setBackgroundResource"

    sget v2, Lcom/peel/ui/fo;->lockscreen_power_button_stateful:I

    invoke-virtual {p1, v0, v1, v2}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    .line 208
    sget v0, Lcom/peel/ui/fp;->btn8:I

    sget v1, Lcom/peel/ui/fo;->widget_play_pause_icon:I

    invoke-virtual {p1, v0, v1}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    .line 209
    sget v0, Lcom/peel/ui/fp;->btn9:I

    const-string/jumbo v1, "setBackgroundResource"

    sget v2, Lcom/peel/ui/fo;->lockscreen_power_button_stateful:I

    invoke-virtual {p1, v0, v1, v2}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    .line 210
    sget v0, Lcom/peel/ui/fp;->btn9:I

    sget v1, Lcom/peel/ui/fo;->widget_mute_icon:I

    invoke-virtual {p1, v0, v1}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    .line 213
    sget v0, Lcom/peel/ui/fq;->homescreen_placeholder1:I

    if-ne p2, v0, :cond_0

    .line 214
    sget v0, Lcom/peel/ui/fp;->btn4:I

    const-string/jumbo v1, "Volume_Up"

    invoke-static {p0, p1, v0, v1}, Ltv/peel/widget/a/i;->a(Landroid/content/Context;Landroid/widget/RemoteViews;ILjava/lang/String;)V

    .line 215
    sget v0, Lcom/peel/ui/fp;->btn5:I

    const-string/jumbo v1, "Volume_Down"

    invoke-static {p0, p1, v0, v1}, Ltv/peel/widget/a/i;->a(Landroid/content/Context;Landroid/widget/RemoteViews;ILjava/lang/String;)V

    .line 216
    sget v0, Lcom/peel/ui/fp;->btn6:I

    const-string/jumbo v1, "Channel_Up"

    invoke-static {p0, p1, v0, v1}, Ltv/peel/widget/a/i;->a(Landroid/content/Context;Landroid/widget/RemoteViews;ILjava/lang/String;)V

    .line 217
    sget v0, Lcom/peel/ui/fp;->btn7:I

    const-string/jumbo v1, "Channel_Down"

    invoke-static {p0, p1, v0, v1}, Ltv/peel/widget/a/i;->a(Landroid/content/Context;Landroid/widget/RemoteViews;ILjava/lang/String;)V

    .line 218
    sget v0, Lcom/peel/ui/fp;->btn10:I

    const-string/jumbo v1, "Fast_Forward"

    invoke-static {p0, p1, v0, v1}, Ltv/peel/widget/a/i;->a(Landroid/content/Context;Landroid/widget/RemoteViews;ILjava/lang/String;)V

    .line 219
    sget v0, Lcom/peel/ui/fp;->btn11:I

    const-string/jumbo v1, "Channel_Up"

    invoke-static {p0, p1, v0, v1}, Ltv/peel/widget/a/i;->a(Landroid/content/Context;Landroid/widget/RemoteViews;ILjava/lang/String;)V

    .line 220
    sget v0, Lcom/peel/ui/fp;->btn12:I

    const-string/jumbo v1, "Channel_Down"

    invoke-static {p0, p1, v0, v1}, Ltv/peel/widget/a/i;->a(Landroid/content/Context;Landroid/widget/RemoteViews;ILjava/lang/String;)V

    .line 222
    sget v0, Lcom/peel/ui/fp;->btn10:I

    const-string/jumbo v1, "setBackgroundResource"

    sget v2, Lcom/peel/ui/fo;->lockscreen_power_button_stateful:I

    invoke-virtual {p1, v0, v1, v2}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    .line 223
    sget v0, Lcom/peel/ui/fp;->btn10:I

    sget v1, Lcom/peel/ui/fo;->widget_ff_icon:I

    invoke-virtual {p1, v0, v1}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    .line 224
    sget v0, Lcom/peel/ui/fp;->btn11:I

    const-string/jumbo v1, "setBackgroundResource"

    sget v2, Lcom/peel/ui/fo;->lockscreen_power_button_stateful:I

    invoke-virtual {p1, v0, v1, v2}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    .line 225
    sget v0, Lcom/peel/ui/fp;->btn11:I

    sget v1, Lcom/peel/ui/fo;->widget_ch_up_icon:I

    invoke-virtual {p1, v0, v1}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    .line 226
    sget v0, Lcom/peel/ui/fp;->btn12:I

    const-string/jumbo v1, "setBackgroundResource"

    sget v2, Lcom/peel/ui/fo;->lockscreen_power_button_stateful:I

    invoke-virtual {p1, v0, v1, v2}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    .line 227
    sget v0, Lcom/peel/ui/fp;->btn12:I

    sget v1, Lcom/peel/ui/fo;->widget_ch_down_icon:I

    invoke-virtual {p1, v0, v1}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    .line 230
    :cond_0
    sget v0, Lcom/peel/ui/fq;->homescreen_placeholder2:I

    if-ne p2, v0, :cond_1

    .line 231
    sget v0, Lcom/peel/ui/fp;->btn4:I

    const-string/jumbo v1, "Volume_Up"

    invoke-static {p0, p1, v0, v1}, Ltv/peel/widget/a/i;->a(Landroid/content/Context;Landroid/widget/RemoteViews;ILjava/lang/String;)V

    .line 232
    sget v0, Lcom/peel/ui/fp;->btn5:I

    const-string/jumbo v1, "Volume_Down"

    invoke-static {p0, p1, v0, v1}, Ltv/peel/widget/a/i;->a(Landroid/content/Context;Landroid/widget/RemoteViews;ILjava/lang/String;)V

    .line 233
    sget v0, Lcom/peel/ui/fp;->btn13:I

    const-string/jumbo v1, "Rewind"

    invoke-static {p0, p1, v0, v1}, Ltv/peel/widget/a/i;->a(Landroid/content/Context;Landroid/widget/RemoteViews;ILjava/lang/String;)V

    .line 234
    sget v0, Lcom/peel/ui/fp;->btn14:I

    const-string/jumbo v1, "Fast_Forward"

    invoke-static {p0, p1, v0, v1}, Ltv/peel/widget/a/i;->a(Landroid/content/Context;Landroid/widget/RemoteViews;ILjava/lang/String;)V

    .line 235
    sget v1, Lcom/peel/ui/fp;->btn15:I

    if-eqz p3, :cond_4

    const-string/jumbo v0, "Select"

    :goto_1
    invoke-static {p0, p1, v1, v0}, Ltv/peel/widget/a/i;->a(Landroid/content/Context;Landroid/widget/RemoteViews;ILjava/lang/String;)V

    .line 237
    sget v0, Lcom/peel/ui/fp;->btn13:I

    const-string/jumbo v1, "setBackgroundResource"

    sget v2, Lcom/peel/ui/fo;->lockscreen_power_button_stateful:I

    invoke-virtual {p1, v0, v1, v2}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    .line 238
    sget v0, Lcom/peel/ui/fp;->btn13:I

    sget v1, Lcom/peel/ui/fo;->widget_rewind_icon:I

    invoke-virtual {p1, v0, v1}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    .line 239
    sget v0, Lcom/peel/ui/fp;->btn14:I

    const-string/jumbo v1, "setBackgroundResource"

    sget v2, Lcom/peel/ui/fo;->lockscreen_power_button_stateful:I

    invoke-virtual {p1, v0, v1, v2}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    .line 240
    sget v0, Lcom/peel/ui/fp;->btn14:I

    sget v1, Lcom/peel/ui/fo;->widget_ff_icon:I

    invoke-virtual {p1, v0, v1}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    .line 241
    sget v0, Lcom/peel/ui/fp;->btn15:I

    const-string/jumbo v1, "setBackgroundResource"

    sget v2, Lcom/peel/ui/fo;->lockscreen_power_button_stateful:I

    invoke-virtual {p1, v0, v1, v2}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    .line 242
    sget v0, Lcom/peel/ui/fp;->btn15:I

    sget v1, Lcom/peel/ui/fo;->widget_play_pause_icon:I

    invoke-virtual {p1, v0, v1}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    .line 245
    :cond_1
    sget v0, Lcom/peel/ui/fq;->homescreen_placeholder3:I

    if-ne p2, v0, :cond_2

    .line 246
    sget v0, Lcom/peel/ui/fp;->textview1:I

    sget v1, Lcom/peel/ui/ft;->lockscreen_temp:I

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 247
    sget v0, Lcom/peel/ui/fp;->textview2:I

    sget v1, Lcom/peel/ui/ft;->lockscreen_fan_speed:I

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 249
    sget v1, Lcom/peel/ui/fp;->btn17:I

    sget v2, Lcom/peel/ui/fo;->ac_mode_icon:I

    move-object v0, p1

    move v4, v3

    move v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/widget/RemoteViews;->setTextViewCompoundDrawables(IIIII)V

    .line 250
    sget v0, Lcom/peel/ui/fp;->btn17:I

    const-string/jumbo v1, "setBackgroundResource"

    sget v2, Lcom/peel/ui/fo;->lockscreen_power_button_stateful:I

    invoke-virtual {p1, v0, v1, v2}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    .line 251
    sget v0, Lcom/peel/ui/fp;->btn4:I

    const-string/jumbo v1, "UP"

    invoke-static {p0, p1, v0, v1}, Ltv/peel/widget/a/i;->a(Landroid/content/Context;Landroid/widget/RemoteViews;ILjava/lang/String;)V

    .line 252
    sget v0, Lcom/peel/ui/fp;->btn5:I

    const-string/jumbo v1, "Down"

    invoke-static {p0, p1, v0, v1}, Ltv/peel/widget/a/i;->a(Landroid/content/Context;Landroid/widget/RemoteViews;ILjava/lang/String;)V

    .line 253
    sget v0, Lcom/peel/ui/fp;->btn6:I

    const-string/jumbo v1, "FAN_HIGH"

    invoke-static {p0, p1, v0, v1}, Ltv/peel/widget/a/i;->a(Landroid/content/Context;Landroid/widget/RemoteViews;ILjava/lang/String;)V

    .line 254
    sget v0, Lcom/peel/ui/fp;->btn7:I

    const-string/jumbo v1, "FAN_LOW"

    invoke-static {p0, p1, v0, v1}, Ltv/peel/widget/a/i;->a(Landroid/content/Context;Landroid/widget/RemoteViews;ILjava/lang/String;)V

    .line 255
    sget v0, Lcom/peel/ui/fp;->btn17:I

    const-string/jumbo v1, "MODE"

    invoke-static {p0, p1, v0, v1}, Ltv/peel/widget/a/i;->a(Landroid/content/Context;Landroid/widget/RemoteViews;ILjava/lang/String;)V

    .line 257
    :cond_2
    return-void

    .line 186
    :cond_3
    const-string/jumbo v0, "Play|Pause"

    goto/16 :goto_0

    .line 235
    :cond_4
    const-string/jumbo v0, "Play|Pause"

    goto/16 :goto_1
.end method

.method public static final a(Landroid/content/Context;Z)V
    .locals 8

    .prologue
    const/high16 v7, 0x30000000

    const/high16 v6, 0x8000000

    const/16 v5, 0x1f40

    .line 262
    new-instance v1, Landroid/widget/RemoteViews;

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    sget v2, Lcom/peel/ui/fq;->widget_layout_home_setup:I

    invoke-direct {v1, v0, v2}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    .line 263
    invoke-static {p0}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v2

    .line 266
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string/jumbo v3, "current_room"

    const/4 v4, 0x0

    invoke-interface {v0, v3, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    if-eqz p1, :cond_1

    .line 267
    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/content/pm/PackageManager;->getLaunchIntentForPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 268
    const-string/jumbo v3, "context_id"

    invoke-virtual {v0, v3, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 269
    invoke-virtual {v0, v7}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 270
    const-string/jumbo v3, "from"

    const-string/jumbo v4, "HOMESCREEN"

    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 271
    const/4 v3, 0x3

    invoke-static {p0, v3, v0, v6}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    .line 281
    :goto_0
    sget v3, Lcom/peel/ui/fp;->widget_home_setup_layout:I

    invoke-virtual {v1, v3, v0}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 282
    sget v3, Lcom/peel/ui/fp;->widget_home_setup_tap:I

    invoke-virtual {v1, v3, v0}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 283
    new-instance v0, Landroid/content/ComponentName;

    const-class v3, Ltv/peel/widget/a;

    invoke-direct {v0, p0, v3}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v2, v0, v1}, Landroid/appwidget/AppWidgetManager;->updateAppWidget(Landroid/content/ComponentName;Landroid/widget/RemoteViews;)V

    .line 284
    return-void

    .line 273
    :cond_1
    new-instance v0, Landroid/content/Intent;

    const-class v3, Lcom/peel/main/Home;

    invoke-direct {v0, p0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 274
    const-string/jumbo v3, "context_id"

    invoke-virtual {v0, v3, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 275
    invoke-virtual {v0, v7}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 276
    const-string/jumbo v3, "from"

    const-string/jumbo v4, "HOMESCREEN"

    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 277
    const-string/jumbo v3, "peel://remote/"

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 278
    const/4 v3, 0x4

    invoke-static {p0, v3, v0, v6}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;[I[Lcom/peel/control/h;ZLjava/util/List;II)V
    .locals 15
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "[I[",
            "Lcom/peel/control/h;",
            "Z",
            "Ljava/util/List",
            "<",
            "Ltv/peel/widget/service/ChannelParcelable;",
            ">;II)V"
        }
    .end annotation

    .prologue
    .line 56
    .line 57
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    const-string/jumbo v2, "is_setup_complete"

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_c

    .line 58
    move/from16 v0, p3

    invoke-static {p0, v0}, Ltv/peel/widget/a/i;->b(Landroid/content/Context;Z)I

    move-result v9

    .line 60
    new-instance v4, Landroid/widget/RemoteViews;

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v4, v1, v9}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    .line 61
    invoke-static {p0}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v10

    .line 62
    const/4 v7, 0x0

    .line 64
    move-object/from16 v0, p1

    array-length v11, v0

    const/4 v1, 0x0

    move v8, v1

    :goto_0
    if-ge v8, v11, :cond_c

    aget v12, p1, v8

    .line 65
    const-string/jumbo v1, "layout_inflater"

    invoke-virtual {p0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/LayoutInflater;

    .line 66
    const/4 v2, 0x0

    invoke-virtual {v1, v9, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    .line 67
    invoke-static {p0}, Ltv/peel/widget/util/j;->a(Landroid/content/Context;)Ltv/peel/widget/util/j;

    .line 68
    new-instance v1, Landroid/content/Intent;

    const-class v2, Ltv/peel/widget/WidgetService;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 69
    const-string/jumbo v2, "tv.peel.widget.action.BUTTON_PRESSED"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 70
    new-instance v13, Ljava/util/ArrayList;

    invoke-direct {v13}, Ljava/util/ArrayList;-><init>()V

    .line 72
    const/4 v2, 0x0

    .line 73
    move-object/from16 v0, p2

    array-length v5, v0

    const/4 v1, 0x0

    move v3, v1

    move v1, v2

    :goto_1
    if-ge v3, v5, :cond_5

    aget-object v2, p2, v3

    .line 74
    invoke-virtual {v2}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v6

    invoke-virtual {v6}, Lcom/peel/data/g;->d()I

    move-result v6

    .line 76
    const/4 v14, 0x6

    if-ne v6, v14, :cond_0

    .line 77
    invoke-virtual {v2}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v2

    invoke-virtual {v2}, Lcom/peel/data/g;->f()Ljava/lang/String;

    move-result-object v2

    .line 78
    if-eqz v2, :cond_2

    const-string/jumbo v6, "Apple"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 79
    const/4 v2, 0x1

    .line 73
    :goto_2
    add-int/lit8 v3, v3, 0x1

    move v7, v2

    goto :goto_1

    .line 83
    :cond_0
    const/16 v14, 0x12

    if-ne v6, v14, :cond_3

    .line 84
    sget-object v6, Ltv/peel/widget/a/i;->b:Ltv/peel/widget/b;

    if-eqz v6, :cond_1

    sget-object v6, Ltv/peel/widget/a/i;->b:Ltv/peel/widget/b;

    invoke-virtual {v6}, Ltv/peel/widget/b;->b()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 85
    sget v6, Lcom/peel/ui/fp;->btn17:I

    const/4 v14, 0x0

    invoke-virtual {v4, v6, v14}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 86
    sget v6, Lcom/peel/ui/fp;->btn17:I

    sget-object v14, Ltv/peel/widget/a/i;->b:Ltv/peel/widget/b;

    invoke-virtual {v14}, Ltv/peel/widget/b;->d()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v4, v6, v14}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 91
    :cond_1
    :goto_3
    invoke-interface {v13, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_2
    move v2, v7

    goto :goto_2

    .line 88
    :cond_3
    const/4 v14, 0x2

    if-eq v6, v14, :cond_4

    const/16 v14, 0x14

    if-ne v6, v14, :cond_1

    .line 89
    :cond_4
    const/4 v1, 0x1

    goto :goto_3

    .line 94
    :cond_5
    const/4 v2, 0x2

    move/from16 v0, p5

    if-ge v0, v2, :cond_6

    .line 95
    sget v2, Lcom/peel/ui/fp;->widget_device_select_prev:I

    const/4 v3, 0x4

    invoke-virtual {v4, v2, v3}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 96
    sget v2, Lcom/peel/ui/fp;->widget_device_select_next:I

    const/4 v3, 0x4

    invoke-virtual {v4, v2, v3}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 98
    :cond_6
    invoke-static {p0, v4}, Ltv/peel/widget/a/i;->a(Landroid/content/Context;Landroid/widget/RemoteViews;)V

    .line 99
    sget v2, Lcom/peel/ui/fp;->widget_device_name:I

    sget-object v3, Ltv/peel/widget/a/i;->a:Ljava/lang/String;

    invoke-virtual {v4, v2, v3}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 100
    invoke-interface {v13}, Ljava/util/List;->size()I

    move-result v2

    .line 102
    if-eqz v1, :cond_7

    sget v3, Lcom/peel/ui/fq;->homescreen_placeholder1:I

    if-ne v9, v3, :cond_7

    .line 103
    sget v1, Lcom/peel/ui/fp;->command_holder1:I

    const/16 v3, 0x8

    invoke-virtual {v4, v1, v3}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 104
    sget v1, Lcom/peel/ui/fp;->command_holder2:I

    const/4 v3, 0x0

    invoke-virtual {v4, v1, v3}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 105
    sget v1, Lcom/peel/ui/fp;->btn8:I

    const/4 v3, 0x0

    invoke-virtual {v4, v1, v3}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 114
    :goto_4
    if-nez v2, :cond_9

    .line 115
    sget v1, Lcom/peel/ui/fp;->btn1:I

    const/16 v2, 0x8

    invoke-virtual {v4, v1, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 116
    sget v1, Lcom/peel/ui/fp;->btn2:I

    const/16 v2, 0x8

    invoke-virtual {v4, v1, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 117
    sget v1, Lcom/peel/ui/fp;->btn3:I

    const/16 v2, 0x8

    invoke-virtual {v4, v1, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 144
    :goto_5
    invoke-static {p0, v4, v9, v7}, Ltv/peel/widget/a/i;->a(Landroid/content/Context;Landroid/widget/RemoteViews;IZ)V

    .line 145
    invoke-virtual {v10, v12, v4}, Landroid/appwidget/AppWidgetManager;->updateAppWidget(ILandroid/widget/RemoteViews;)V

    .line 64
    add-int/lit8 v1, v8, 0x1

    move v8, v1

    goto/16 :goto_0

    .line 106
    :cond_7
    if-nez v1, :cond_8

    sget v1, Lcom/peel/ui/fq;->homescreen_placeholder1:I

    if-ne v9, v1, :cond_8

    .line 107
    sget v1, Lcom/peel/ui/fp;->command_holder1:I

    const/4 v3, 0x0

    invoke-virtual {v4, v1, v3}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 108
    sget v1, Lcom/peel/ui/fp;->command_holder2:I

    const/16 v3, 0x8

    invoke-virtual {v4, v1, v3}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 109
    sget v1, Lcom/peel/ui/fp;->btn8:I

    const/4 v3, 0x4

    invoke-virtual {v4, v1, v3}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    goto :goto_4

    .line 111
    :cond_8
    sget v1, Lcom/peel/ui/fp;->btn8:I

    const/4 v3, 0x4

    invoke-virtual {v4, v1, v3}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    goto :goto_4

    .line 118
    :cond_9
    const/4 v1, 0x1

    if-ne v2, v1, :cond_a

    .line 119
    sget v1, Lcom/peel/ui/fp;->btn1:I

    const/4 v2, 0x4

    invoke-virtual {v4, v1, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 120
    sget v1, Lcom/peel/ui/fp;->btn2:I

    const/4 v2, 0x0

    invoke-virtual {v4, v1, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 121
    sget v1, Lcom/peel/ui/fp;->btn3:I

    const/4 v2, 0x4

    invoke-virtual {v4, v1, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 122
    sget v2, Lcom/peel/ui/fp;->btn2:I

    const/4 v1, 0x0

    invoke-interface {v13, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/peel/control/h;

    invoke-virtual {v1}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/data/g;->d()I

    move-result v1

    invoke-static {p0, v1}, Lcom/peel/util/bx;->b(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v2, v1}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 123
    sget v2, Lcom/peel/ui/fp;->btn2:I

    const/4 v1, 0x0

    invoke-interface {v13, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/peel/control/h;

    invoke-virtual {v1}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/data/g;->b()Ljava/lang/String;

    move-result-object v3

    const/4 v1, 0x0

    invoke-interface {v13, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/peel/control/h;

    invoke-static {v1}, Ltv/peel/widget/a/i;->a(Lcom/peel/control/h;)Z

    move-result v5

    const/4 v6, 0x0

    move-object v1, p0

    invoke-static/range {v1 .. v6}, Ltv/peel/widget/a/i;->a(Landroid/content/Context;ILjava/lang/String;Landroid/widget/RemoteViews;ZI)V

    goto :goto_5

    .line 124
    :cond_a
    const/4 v1, 0x2

    if-ne v2, v1, :cond_b

    .line 125
    sget v1, Lcom/peel/ui/fp;->btn1:I

    const/4 v2, 0x0

    invoke-virtual {v4, v1, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 126
    sget v1, Lcom/peel/ui/fp;->btn2:I

    const/4 v2, 0x4

    invoke-virtual {v4, v1, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 127
    sget v1, Lcom/peel/ui/fp;->btn3:I

    const/4 v2, 0x0

    invoke-virtual {v4, v1, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 128
    sget v2, Lcom/peel/ui/fp;->btn1:I

    const/4 v1, 0x0

    invoke-interface {v13, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/peel/control/h;

    invoke-virtual {v1}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/data/g;->d()I

    move-result v1

    invoke-static {p0, v1}, Lcom/peel/util/bx;->b(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v2, v1}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 129
    sget v2, Lcom/peel/ui/fp;->btn3:I

    const/4 v1, 0x1

    invoke-interface {v13, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/peel/control/h;

    invoke-virtual {v1}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/data/g;->d()I

    move-result v1

    invoke-static {p0, v1}, Lcom/peel/util/bx;->b(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v2, v1}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 130
    sget v2, Lcom/peel/ui/fp;->btn1:I

    const/4 v1, 0x0

    invoke-interface {v13, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/peel/control/h;

    invoke-virtual {v1}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/data/g;->b()Ljava/lang/String;

    move-result-object v3

    const/4 v1, 0x0

    invoke-interface {v13, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/peel/control/h;

    invoke-static {v1}, Ltv/peel/widget/a/i;->a(Lcom/peel/control/h;)Z

    move-result v5

    const/4 v6, 0x0

    move-object v1, p0

    invoke-static/range {v1 .. v6}, Ltv/peel/widget/a/i;->a(Landroid/content/Context;ILjava/lang/String;Landroid/widget/RemoteViews;ZI)V

    .line 131
    sget v2, Lcom/peel/ui/fp;->btn3:I

    const/4 v1, 0x1

    invoke-interface {v13, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/peel/control/h;

    invoke-virtual {v1}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/data/g;->b()Ljava/lang/String;

    move-result-object v3

    const/4 v1, 0x1

    invoke-interface {v13, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/peel/control/h;

    invoke-static {v1}, Ltv/peel/widget/a/i;->a(Lcom/peel/control/h;)Z

    move-result v5

    const/4 v6, 0x1

    move-object v1, p0

    invoke-static/range {v1 .. v6}, Ltv/peel/widget/a/i;->a(Landroid/content/Context;ILjava/lang/String;Landroid/widget/RemoteViews;ZI)V

    goto/16 :goto_5

    .line 133
    :cond_b
    sget v1, Lcom/peel/ui/fp;->btn1:I

    const/4 v2, 0x0

    invoke-virtual {v4, v1, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 134
    sget v1, Lcom/peel/ui/fp;->btn2:I

    const/4 v2, 0x0

    invoke-virtual {v4, v1, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 135
    sget v1, Lcom/peel/ui/fp;->btn3:I

    const/4 v2, 0x0

    invoke-virtual {v4, v1, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 136
    sget v2, Lcom/peel/ui/fp;->btn1:I

    const/4 v1, 0x0

    invoke-interface {v13, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/peel/control/h;

    invoke-virtual {v1}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/data/g;->d()I

    move-result v1

    invoke-static {p0, v1}, Lcom/peel/util/bx;->b(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v2, v1}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 137
    sget v2, Lcom/peel/ui/fp;->btn2:I

    const/4 v1, 0x1

    invoke-interface {v13, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/peel/control/h;

    invoke-virtual {v1}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/data/g;->d()I

    move-result v1

    invoke-static {p0, v1}, Lcom/peel/util/bx;->b(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v2, v1}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 138
    sget v2, Lcom/peel/ui/fp;->btn3:I

    const/4 v1, 0x2

    invoke-interface {v13, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/peel/control/h;

    invoke-virtual {v1}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/data/g;->d()I

    move-result v1

    invoke-static {p0, v1}, Lcom/peel/util/bx;->b(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v2, v1}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 139
    sget v2, Lcom/peel/ui/fp;->btn1:I

    const/4 v1, 0x0

    invoke-interface {v13, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/peel/control/h;

    invoke-virtual {v1}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/data/g;->b()Ljava/lang/String;

    move-result-object v3

    const/4 v1, 0x0

    invoke-interface {v13, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/peel/control/h;

    invoke-static {v1}, Ltv/peel/widget/a/i;->a(Lcom/peel/control/h;)Z

    move-result v5

    const/4 v6, 0x0

    move-object v1, p0

    invoke-static/range {v1 .. v6}, Ltv/peel/widget/a/i;->a(Landroid/content/Context;ILjava/lang/String;Landroid/widget/RemoteViews;ZI)V

    .line 140
    sget v2, Lcom/peel/ui/fp;->btn2:I

    const/4 v1, 0x1

    invoke-interface {v13, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/peel/control/h;

    invoke-virtual {v1}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/data/g;->b()Ljava/lang/String;

    move-result-object v3

    const/4 v1, 0x1

    invoke-interface {v13, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/peel/control/h;

    invoke-static {v1}, Ltv/peel/widget/a/i;->a(Lcom/peel/control/h;)Z

    move-result v5

    const/4 v6, 0x1

    move-object v1, p0

    invoke-static/range {v1 .. v6}, Ltv/peel/widget/a/i;->a(Landroid/content/Context;ILjava/lang/String;Landroid/widget/RemoteViews;ZI)V

    .line 141
    sget v2, Lcom/peel/ui/fp;->btn3:I

    const/4 v1, 0x2

    invoke-interface {v13, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/peel/control/h;

    invoke-virtual {v1}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/data/g;->b()Ljava/lang/String;

    move-result-object v3

    const/4 v1, 0x2

    invoke-interface {v13, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/peel/control/h;

    invoke-static {v1}, Ltv/peel/widget/a/i;->a(Lcom/peel/control/h;)Z

    move-result v5

    const/4 v6, 0x2

    move-object v1, p0

    invoke-static/range {v1 .. v6}, Ltv/peel/widget/a/i;->a(Landroid/content/Context;ILjava/lang/String;Landroid/widget/RemoteViews;ZI)V

    goto/16 :goto_5

    .line 149
    :cond_c
    return-void
.end method

.method private static a(Lcom/peel/control/h;)Z
    .locals 2

    .prologue
    .line 152
    invoke-virtual {p0}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/data/g;->a()Ljava/util/Map;

    move-result-object v0

    .line 154
    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v1

    if-lez v1, :cond_0

    const-string/jumbo v1, "Power"

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 155
    const/4 v0, 0x0

    .line 157
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private static b(Landroid/content/Context;Z)I
    .locals 1

    .prologue
    .line 308
    invoke-static {p0}, Ltv/peel/widget/util/j;->a(Landroid/content/Context;)Ltv/peel/widget/util/j;

    move-result-object v0

    .line 309
    invoke-virtual {v0}, Ltv/peel/widget/util/j;->c()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 315
    if-eqz p1, :cond_0

    .line 316
    sget v0, Lcom/peel/ui/fq;->homescreen_placeholder1:I

    .line 318
    :goto_0
    return v0

    .line 311
    :sswitch_0
    sget v0, Lcom/peel/ui/fq;->homescreen_placeholder1:I

    goto :goto_0

    .line 313
    :sswitch_1
    sget v0, Lcom/peel/ui/fq;->homescreen_placeholder3:I

    goto :goto_0

    .line 318
    :cond_0
    sget v0, Lcom/peel/ui/fq;->homescreen_placeholder2:I

    goto :goto_0

    .line 309
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x12 -> :sswitch_1
    .end sparse-switch
.end method
