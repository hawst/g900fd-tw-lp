.class public Ltv/peel/widget/g;
.super Ljava/lang/Object;


# static fields
.field private static final i:Ljava/lang/String;


# instance fields
.field public a:Z

.field public b:[Ltv/peel/widget/service/DeviceParcelable;

.field public c:Z

.field public d:Z

.field public e:[Lcom/peel/control/a;

.field public f:Lcom/peel/control/a;

.field public g:Z

.field public h:I

.field private j:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 19
    const-class v0, Ltv/peel/widget/g;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Ltv/peel/widget/g;->i:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v0, 0x0

    const/4 v2, 0x0

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-boolean v2, p0, Ltv/peel/widget/g;->a:Z

    .line 23
    iput-object v0, p0, Ltv/peel/widget/g;->b:[Ltv/peel/widget/service/DeviceParcelable;

    .line 27
    iput-boolean v2, p0, Ltv/peel/widget/g;->c:Z

    .line 29
    iput-boolean v2, p0, Ltv/peel/widget/g;->d:Z

    .line 31
    iput-object v0, p0, Ltv/peel/widget/g;->e:[Lcom/peel/control/a;

    .line 33
    iput-object v0, p0, Ltv/peel/widget/g;->f:Lcom/peel/control/a;

    .line 35
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Ltv/peel/widget/g;->j:J

    .line 37
    iput-boolean v2, p0, Ltv/peel/widget/g;->g:Z

    .line 45
    iput v2, p0, Ltv/peel/widget/g;->h:I

    return-void
.end method

.method private a(Ljava/lang/String;Lcom/peel/control/a;Ltv/peel/widget/b;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 157
    const-string/jumbo v0, ""

    .line 158
    if-eqz p1, :cond_1

    if-eqz p2, :cond_1

    .line 160
    const-string/jumbo v1, "Volume_Down"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string/jumbo v1, "Volume_Up"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string/jumbo v1, "Mute"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 161
    :cond_0
    invoke-virtual {p0, p3}, Ltv/peel/widget/g;->b(Ltv/peel/widget/b;)Ljava/lang/String;

    move-result-object v0

    .line 171
    :cond_1
    :goto_0
    return-object v0

    .line 165
    :cond_2
    const/4 v1, 0x1

    invoke-virtual {p2, v1}, Lcom/peel/control/a;->a(I)Lcom/peel/control/h;

    move-result-object v1

    .line 167
    if-eqz v1, :cond_1

    .line 168
    invoke-virtual {v1}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/data/g;->b()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Ltv/peel/widget/b;)Ltv/peel/widget/g;
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/4 v4, 0x1

    .line 48
    .line 50
    if-eqz p0, :cond_4

    invoke-virtual {p0}, Ltv/peel/widget/b;->a()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 51
    new-instance v0, Ltv/peel/widget/g;

    invoke-direct {v0}, Ltv/peel/widget/g;-><init>()V

    .line 54
    :try_start_0
    sget-object v2, Lcom/peel/c/a;->b:Lcom/peel/c/e;

    invoke-static {v2}, Lcom/peel/c/a;->a(Lcom/peel/c/e;)Ljava/lang/Object;

    move-result-object v2

    sget-object v3, Lcom/peel/c/b;->a:Lcom/peel/c/b;

    if-ne v2, v3, :cond_2

    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x15

    if-ge v2, v3, :cond_2

    .line 55
    invoke-virtual {p0}, Ltv/peel/widget/b;->j()Z

    move-result v2

    iput-boolean v2, v0, Ltv/peel/widget/g;->a:Z

    .line 64
    :goto_0
    invoke-virtual {p0}, Ltv/peel/widget/b;->m()Z

    move-result v2

    iput-boolean v2, v0, Ltv/peel/widget/g;->d:Z

    .line 65
    invoke-virtual {p0}, Ltv/peel/widget/b;->k()Z

    move-result v2

    iput-boolean v2, v0, Ltv/peel/widget/g;->g:Z

    .line 67
    iget-boolean v2, v0, Ltv/peel/widget/g;->a:Z

    if-eq v2, v4, :cond_0

    iget-boolean v2, v0, Ltv/peel/widget/g;->g:Z

    if-eqz v2, :cond_1

    .line 68
    :cond_0
    invoke-virtual {p0}, Ltv/peel/widget/b;->e()[Ltv/peel/widget/service/DeviceParcelable;

    move-result-object v2

    iput-object v2, v0, Ltv/peel/widget/g;->b:[Ltv/peel/widget/service/DeviceParcelable;

    .line 69
    invoke-virtual {p0}, Ltv/peel/widget/b;->i()Z

    move-result v2

    iput-boolean v2, v0, Ltv/peel/widget/g;->c:Z

    .line 70
    invoke-virtual {p0}, Ltv/peel/widget/b;->f()[Lcom/peel/control/a;

    move-result-object v2

    iput-object v2, v0, Ltv/peel/widget/g;->e:[Lcom/peel/control/a;

    .line 71
    invoke-virtual {p0}, Ltv/peel/widget/b;->g()Lcom/peel/control/a;

    move-result-object v2

    iput-object v2, v0, Ltv/peel/widget/g;->f:Lcom/peel/control/a;

    .line 73
    iget-object v2, v0, Ltv/peel/widget/g;->e:[Lcom/peel/control/a;

    if-eqz v2, :cond_1

    .line 74
    iget-object v2, v0, Ltv/peel/widget/g;->e:[Lcom/peel/control/a;

    invoke-static {v2}, Ltv/peel/widget/g;->a([Lcom/peel/control/a;)[Lcom/peel/control/a;

    move-result-object v2

    iput-object v2, v0, Ltv/peel/widget/g;->e:[Lcom/peel/control/a;

    .line 82
    :cond_1
    :goto_1
    return-object v0

    .line 57
    :cond_2
    sget-object v2, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v2}, Lcom/peel/control/am;->e()[Lcom/peel/control/h;

    move-result-object v2

    .line 58
    if-eqz v2, :cond_3

    array-length v2, v2

    if-nez v2, :cond_5

    .line 59
    :cond_3
    const/4 v2, 0x0

    iput-boolean v2, v0, Ltv/peel/widget/g;->a:Z
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 77
    :catch_0
    move-exception v0

    .line 79
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    :cond_4
    move-object v0, v1

    goto :goto_1

    .line 61
    :cond_5
    const/4 v2, 0x1

    :try_start_1
    iput-boolean v2, v0, Ltv/peel/widget/g;->a:Z
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method

.method private static a([Lcom/peel/control/a;)[Lcom/peel/control/a;
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x7

    .line 194
    if-eqz p0, :cond_0

    array-length v0, p0

    if-le v0, v1, :cond_0

    .line 195
    new-array v0, v1, [Lcom/peel/control/a;

    .line 196
    invoke-static {p0, v2, v0, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    move-object p0, v0

    .line 201
    :cond_0
    return-object p0
.end method


# virtual methods
.method public a(Lcom/peel/control/a;)I
    .locals 8

    .prologue
    const/4 v1, 0x1

    .line 91
    const/4 v0, -0x1

    .line 93
    if-eqz p1, :cond_1

    .line 94
    invoke-virtual {p1, v1}, Lcom/peel/control/a;->a(I)Lcom/peel/control/h;

    move-result-object v3

    .line 95
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0x15

    if-ge v2, v4, :cond_0

    sget-object v2, Lcom/peel/c/a;->b:Lcom/peel/c/e;

    invoke-static {v2}, Lcom/peel/c/a;->a(Lcom/peel/c/e;)Ljava/lang/Object;

    move-result-object v2

    sget-object v4, Lcom/peel/c/b;->a:Lcom/peel/c/b;

    if-eq v2, v4, :cond_3

    .line 96
    :cond_0
    invoke-virtual {p1}, Lcom/peel/control/a;->d()[Ljava/lang/String;

    move-result-object v4

    .line 97
    if-eqz v4, :cond_3

    .line 98
    array-length v5, v4

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v5, :cond_3

    aget-object v6, v4, v2

    .line 99
    const-string/jumbo v7, "live"

    invoke-virtual {v7, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    move v0, v1

    .line 109
    :cond_1
    :goto_1
    return v0

    .line 98
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 106
    :cond_3
    if-eqz v3, :cond_1

    invoke-virtual {v3}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/data/g;->d()I

    move-result v0

    goto :goto_1
.end method

.method public a(Ltv/peel/widget/b;Ljava/lang/String;Lcom/peel/control/a;)V
    .locals 3

    .prologue
    .line 139
    if-nez p3, :cond_1

    .line 154
    :cond_0
    :goto_0
    return-void

    .line 142
    :cond_1
    const-string/jumbo v0, "Power"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 143
    invoke-virtual {p3}, Lcom/peel/control/a;->e()[Lcom/peel/control/h;

    move-result-object v0

    .line 144
    if-eqz v0, :cond_2

    array-length v1, v0

    const/4 v2, 0x1

    if-le v1, v2, :cond_2

    .line 145
    iget v1, p0, Ltv/peel/widget/g;->h:I

    aget-object v0, v0, v1

    invoke-virtual {v0}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/data/g;->b()Ljava/lang/String;

    move-result-object v0

    .line 153
    :goto_1
    if-eqz v0, :cond_0

    invoke-virtual {p0, p1, p2, v0}, Ltv/peel/widget/g;->a(Ltv/peel/widget/b;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 147
    :cond_2
    invoke-direct {p0, p2, p3, p1}, Ltv/peel/widget/g;->a(Ljava/lang/String;Lcom/peel/control/a;Ltv/peel/widget/b;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 150
    :cond_3
    invoke-direct {p0, p2, p3, p1}, Ltv/peel/widget/g;->a(Ljava/lang/String;Lcom/peel/control/a;Ltv/peel/widget/b;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method public a(Ltv/peel/widget/b;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 184
    :try_start_0
    sget-object v0, Ltv/peel/widget/g;->i:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Send IR command "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 185
    const/16 v0, 0x1770

    invoke-virtual {p1, p2, p3, v0}, Ltv/peel/widget/b;->a(Ljava/lang/String;Ljava/lang/String;I)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 189
    :goto_0
    return-void

    .line 186
    :catch_0
    move-exception v0

    .line 187
    sget-object v1, Ltv/peel/widget/g;->i:Ljava/lang/String;

    const-string/jumbo v2, "Failed to send IR command"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public b(Ltv/peel/widget/b;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 86
    invoke-virtual {p1}, Ltv/peel/widget/b;->n()[Ltv/peel/widget/service/DeviceParcelable;

    move-result-object v0

    .line 87
    if-eqz v0, :cond_0

    array-length v1, v0

    if-nez v1, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_1
    const/4 v1, 0x0

    aget-object v0, v0, v1

    invoke-virtual {v0}, Ltv/peel/widget/service/DeviceParcelable;->a()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public b(Lcom/peel/control/a;)Z
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 122
    const/4 v1, 0x0

    .line 124
    invoke-virtual {p1, v0}, Lcom/peel/control/a;->a(I)Lcom/peel/control/h;

    move-result-object v2

    .line 125
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v3

    invoke-virtual {v3}, Lcom/peel/data/g;->d()I

    move-result v3

    const/4 v4, 0x6

    if-ne v3, v4, :cond_0

    .line 126
    invoke-virtual {v2}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v2

    invoke-virtual {v2}, Lcom/peel/data/g;->f()Ljava/lang/String;

    move-result-object v2

    .line 127
    if-eqz v2, :cond_0

    const-string/jumbo v3, "Apple"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 132
    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 6

    .prologue
    .line 206
    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    .line 207
    const-string/jumbo v0, "******** Remote control settings *********\n"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 208
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "mIsExpanded = "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v2, p0, Ltv/peel/widget/g;->d:Z

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v2, "\n"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 209
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "mIsSetupFinished = "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v2, p0, Ltv/peel/widget/g;->a:Z

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v2, "\n"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 210
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "mIsMultipleSource = "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v2, p0, Ltv/peel/widget/g;->c:Z

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v2, "\n"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 211
    const-string/jumbo v0, "-- mCurrentDevicesList:"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 212
    iget-object v0, p0, Ltv/peel/widget/g;->e:[Lcom/peel/control/a;

    if-nez v0, :cond_1

    .line 213
    const-string/jumbo v0, " is NULL\n"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 222
    :cond_0
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 215
    :cond_1
    const-string/jumbo v0, "\n"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 216
    iget-object v2, p0, Ltv/peel/widget/g;->e:[Lcom/peel/control/a;

    array-length v3, v2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_0

    aget-object v4, v2, v0

    .line 217
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 216
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method
