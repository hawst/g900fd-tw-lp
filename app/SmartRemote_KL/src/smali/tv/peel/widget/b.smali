.class public Ltv/peel/widget/b;
.super Ljava/lang/Object;


# static fields
.field private static final a:Ljava/lang/String;

.field private static f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static j:I

.field private static k:I

.field private static l:I


# instance fields
.field private b:Ltv/peel/widget/service/e;

.field private c:Landroid/content/Context;

.field private d:Ltv/peel/widget/util/c;

.field private e:I

.field private i:Landroid/content/SharedPreferences;

.field private m:Z

.field private n:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private o:Ljava/lang/String;

.field private p:Ljava/lang/String;

.field private q:Z

.field private r:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private s:Landroid/content/ServiceConnection;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 48
    const-class v0, Ltv/peel/widget/b;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Ltv/peel/widget/b;->a:Ljava/lang/String;

    .line 56
    sput v1, Ltv/peel/widget/b;->j:I

    sput v1, Ltv/peel/widget/b;->k:I

    sput v1, Ltv/peel/widget/b;->l:I

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 63
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    const/4 v0, 0x0

    iput-object v0, p0, Ltv/peel/widget/b;->b:Ltv/peel/widget/service/e;

    .line 53
    const/4 v0, -0x1

    iput v0, p0, Ltv/peel/widget/b;->e:I

    .line 57
    iput-boolean v1, p0, Ltv/peel/widget/b;->m:Z

    .line 61
    iput-boolean v1, p0, Ltv/peel/widget/b;->q:Z

    .line 304
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Ltv/peel/widget/b;->r:Ljava/util/Map;

    .line 514
    new-instance v0, Ltv/peel/widget/c;

    invoke-direct {v0, p0}, Ltv/peel/widget/c;-><init>(Ltv/peel/widget/b;)V

    iput-object v0, p0, Ltv/peel/widget/b;->s:Landroid/content/ServiceConnection;

    .line 63
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ltv/peel/widget/util/c;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 69
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    const/4 v0, 0x0

    iput-object v0, p0, Ltv/peel/widget/b;->b:Ltv/peel/widget/service/e;

    .line 53
    const/4 v0, -0x1

    iput v0, p0, Ltv/peel/widget/b;->e:I

    .line 57
    iput-boolean v1, p0, Ltv/peel/widget/b;->m:Z

    .line 61
    iput-boolean v1, p0, Ltv/peel/widget/b;->q:Z

    .line 304
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Ltv/peel/widget/b;->r:Ljava/util/Map;

    .line 514
    new-instance v0, Ltv/peel/widget/c;

    invoke-direct {v0, p0}, Ltv/peel/widget/c;-><init>(Ltv/peel/widget/b;)V

    iput-object v0, p0, Ltv/peel/widget/b;->s:Landroid/content/ServiceConnection;

    .line 70
    iput-object p1, p0, Ltv/peel/widget/b;->c:Landroid/content/Context;

    .line 71
    iput-object p2, p0, Ltv/peel/widget/b;->d:Ltv/peel/widget/util/c;

    .line 73
    invoke-static {p1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Ltv/peel/widget/b;->i:Landroid/content/SharedPreferences;

    .line 75
    iget-object v0, p0, Ltv/peel/widget/b;->b:Ltv/peel/widget/service/e;

    if-nez v0, :cond_0

    .line 76
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "tv.peel.widget.service.IPeelService"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 77
    iget-object v1, p0, Ltv/peel/widget/b;->s:Landroid/content/ServiceConnection;

    if-eqz v1, :cond_1

    .line 78
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Ltv/peel/widget/b;->s:Landroid/content/ServiceConnection;

    const/4 v3, 0x1

    invoke-virtual {v1, v0, v2, v3}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    .line 83
    :cond_0
    :goto_0
    return-void

    .line 80
    :cond_1
    sget-object v0, Ltv/peel/widget/b;->a:Ljava/lang/String;

    const-string/jumbo v1, "ServiceConnection is null"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method static synthetic a(Ltv/peel/widget/b;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Ltv/peel/widget/b;->c:Landroid/content/Context;

    return-object v0
.end method

.method private a(Ljava/lang/String;)Lcom/peel/control/a;
    .locals 11

    .prologue
    const/4 v0, 0x0

    const/4 v3, 0x0

    .line 451
    sget-object v1, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v1, p1}, Lcom/peel/control/am;->a(Ljava/lang/String;)Lcom/peel/control/RoomControl;

    move-result-object v2

    .line 452
    invoke-virtual {v2}, Lcom/peel/control/RoomControl;->c()[Lcom/peel/control/a;

    move-result-object v5

    .line 453
    invoke-virtual {v2}, Lcom/peel/control/RoomControl;->d()Lcom/peel/control/a;

    move-result-object v1

    .line 454
    if-eqz v1, :cond_1

    const/4 v4, 0x2

    invoke-virtual {v1}, Lcom/peel/control/a;->f()I

    move-result v6

    if-ne v4, v6, :cond_1

    move-object v0, v1

    .line 484
    :cond_0
    :goto_0
    return-object v0

    .line 458
    :cond_1
    iget-object v1, p0, Ltv/peel/widget/b;->c:Landroid/content/Context;

    invoke-static {v1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    const-string/jumbo v4, "last_activity"

    invoke-interface {v1, v4, v0}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 459
    if-eqz v4, :cond_3

    .line 462
    :try_start_0
    invoke-virtual {v2}, Lcom/peel/control/RoomControl;->c()[Lcom/peel/control/a;

    move-result-object v6

    array-length v7, v6

    move v2, v3

    :goto_1
    if-ge v2, v7, :cond_3

    aget-object v1, v6, v2

    .line 463
    invoke-virtual {v1}, Lcom/peel/control/a;->b()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v8

    if-eqz v8, :cond_2

    move-object v0, v1

    .line 464
    goto :goto_0

    .line 462
    :cond_2
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_1

    .line 467
    :catch_0
    move-exception v1

    .line 472
    :cond_3
    if-eqz v5, :cond_7

    .line 473
    array-length v6, v5

    move v4, v3

    :goto_2
    if-ge v4, v6, :cond_7

    aget-object v1, v5, v4

    .line 474
    invoke-virtual {v1}, Lcom/peel/control/a;->d()[Ljava/lang/String;

    move-result-object v7

    .line 475
    if-nez v7, :cond_5

    .line 473
    :cond_4
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    goto :goto_2

    .line 477
    :cond_5
    array-length v8, v7

    move v2, v3

    :goto_3
    if-ge v2, v8, :cond_4

    aget-object v9, v7, v2

    .line 478
    const-string/jumbo v10, "live"

    invoke-virtual {v10, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_6

    move-object v0, v1

    .line 479
    goto :goto_0

    .line 477
    :cond_6
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 484
    :cond_7
    if-eqz v5, :cond_0

    aget-object v0, v5, v3

    goto :goto_0
.end method

.method private a(Lcom/peel/control/h;Ljava/lang/String;)Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v4, 0x0

    .line 188
    const-string/jumbo v0, "UP"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 189
    sget-object v0, Ltv/peel/widget/b;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_2

    .line 191
    sget-object v0, Ltv/peel/widget/b;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "No Temperature commands for AC "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v2

    invoke-virtual {v2}, Lcom/peel/data/g;->f()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " -- code set: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v2

    invoke-virtual {v2}, Lcom/peel/data/g;->h()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, p2

    .line 207
    :goto_0
    const-string/jumbo v1, "T"

    iput-object v1, p0, Ltv/peel/widget/b;->p:Ljava/lang/String;

    move-object p2, v0

    .line 281
    :cond_0
    :goto_1
    invoke-virtual {p1}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v0

    const-string/jumbo v1, "VANE"

    invoke-virtual {v0, v1}, Lcom/peel/data/g;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_14

    .line 282
    invoke-virtual {p1}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/data/g;->a()Ljava/util/Map;

    move-result-object v0

    const-string/jumbo v1, "VANE"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    const-string/jumbo v1, "type"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Ltv/peel/widget/b;->o:Ljava/lang/String;

    .line 288
    :goto_2
    iget-object v0, p0, Ltv/peel/widget/b;->o:Ljava/lang/String;

    const-string/jumbo v1, "+"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    iput-boolean v0, p0, Ltv/peel/widget/b;->m:Z

    .line 289
    sget-object v0, Ltv/peel/widget/b;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "\ncombo code rule: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Ltv/peel/widget/b;->o:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " -- use combo codes: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Ltv/peel/widget/b;->m:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 290
    iget-boolean v0, p0, Ltv/peel/widget/b;->m:Z

    if-eqz v0, :cond_16

    .line 291
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Ltv/peel/widget/b;->n:Ljava/util/Map;

    .line 292
    iget-object v0, p0, Ltv/peel/widget/b;->p:Ljava/lang/String;

    invoke-direct {p0, p2, v0, p1}, Ltv/peel/widget/b;->a(Ljava/lang/String;Ljava/lang/String;Lcom/peel/control/h;)V

    .line 301
    :cond_1
    :goto_3
    return-object p2

    .line 192
    :cond_2
    sget-object v0, Ltv/peel/widget/b;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ne v0, v1, :cond_3

    .line 193
    sget-object v0, Ltv/peel/widget/b;->f:Ljava/util/List;

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 194
    sget-object v2, Ltv/peel/widget/b;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "sending the only 1 temperature command: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v1, Ltv/peel/widget/b;->f:Ljava/util/List;

    invoke-interface {v1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 196
    :cond_3
    sget v0, Ltv/peel/widget/b;->j:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Ltv/peel/widget/b;->j:I

    .line 197
    sget v0, Ltv/peel/widget/b;->j:I

    sget-object v1, Ltv/peel/widget/b;->f:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lt v0, v1, :cond_4

    .line 198
    sget-object v0, Ltv/peel/widget/b;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    sput v0, Ltv/peel/widget/b;->j:I

    .line 199
    :cond_4
    sget-object v0, Ltv/peel/widget/b;->f:Ljava/util/List;

    sget v1, Ltv/peel/widget/b;->j:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 201
    :try_start_0
    iget-object v1, p0, Ltv/peel/widget/b;->i:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v3}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v3

    invoke-virtual {v3}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v3

    invoke-virtual {v3}, Lcom/peel/data/at;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "_"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "ac_last_temp_idx"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    sget v3, Ltv/peel/widget/b;->j:I

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->apply()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 205
    :goto_4
    sget-object v2, Ltv/peel/widget/b;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "sending the Temperature command (idx: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget v3, Ltv/peel/widget/b;->j:I

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v3, "): "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v1, Ltv/peel/widget/b;->f:Ljava/util/List;

    sget v4, Ltv/peel/widget/b;->j:I

    invoke-interface {v1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 202
    :catch_0
    move-exception v1

    .line 203
    sget-object v2, Ltv/peel/widget/b;->a:Ljava/lang/String;

    sget-object v3, Ltv/peel/widget/b;->a:Ljava/lang/String;

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_4

    .line 208
    :cond_5
    const-string/jumbo v0, "Down"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 209
    sget-object v0, Ltv/peel/widget/b;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_6

    .line 211
    sget-object v0, Ltv/peel/widget/b;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "No Temperature commands for AC "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v2

    invoke-virtual {v2}, Lcom/peel/data/g;->f()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " -- code set: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v2

    invoke-virtual {v2}, Lcom/peel/data/g;->h()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, p2

    .line 227
    :goto_5
    const-string/jumbo v1, "T"

    iput-object v1, p0, Ltv/peel/widget/b;->p:Ljava/lang/String;

    move-object p2, v0

    goto/16 :goto_1

    .line 212
    :cond_6
    sget-object v0, Ltv/peel/widget/b;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ne v0, v1, :cond_7

    .line 213
    sget-object v0, Ltv/peel/widget/b;->f:Ljava/util/List;

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 214
    sget-object v2, Ltv/peel/widget/b;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "sending the only 1 temperature command: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v1, Ltv/peel/widget/b;->f:Ljava/util/List;

    invoke-interface {v1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_5

    .line 216
    :cond_7
    sget v0, Ltv/peel/widget/b;->j:I

    add-int/lit8 v0, v0, -0x1

    sput v0, Ltv/peel/widget/b;->j:I

    .line 217
    sget v0, Ltv/peel/widget/b;->j:I

    if-gez v0, :cond_8

    .line 218
    sput v4, Ltv/peel/widget/b;->j:I

    .line 219
    :cond_8
    sget-object v0, Ltv/peel/widget/b;->f:Ljava/util/List;

    sget v1, Ltv/peel/widget/b;->j:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 221
    :try_start_1
    iget-object v1, p0, Ltv/peel/widget/b;->i:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v3}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v3

    invoke-virtual {v3}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v3

    invoke-virtual {v3}, Lcom/peel/data/at;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "_"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "ac_last_temp_idx"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    sget v3, Ltv/peel/widget/b;->j:I

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->apply()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 225
    :goto_6
    sget-object v2, Ltv/peel/widget/b;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "sending the Temperature command (idx: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget v3, Ltv/peel/widget/b;->j:I

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v3, "): "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v1, Ltv/peel/widget/b;->f:Ljava/util/List;

    sget v4, Ltv/peel/widget/b;->j:I

    invoke-interface {v1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_5

    .line 222
    :catch_1
    move-exception v1

    .line 223
    sget-object v2, Ltv/peel/widget/b;->a:Ljava/lang/String;

    sget-object v3, Ltv/peel/widget/b;->a:Ljava/lang/String;

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_6

    .line 228
    :cond_9
    const-string/jumbo v0, "FAN_HIGH"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 229
    sget-object v0, Ltv/peel/widget/b;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_a

    .line 231
    sget-object v0, Ltv/peel/widget/b;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "No Fan Speed commands for AC: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v2

    invoke-virtual {v2}, Lcom/peel/data/g;->f()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " -- code set: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v2

    invoke-virtual {v2}, Lcom/peel/data/g;->h()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, p2

    .line 243
    :goto_7
    const-string/jumbo v1, "F"

    iput-object v1, p0, Ltv/peel/widget/b;->p:Ljava/lang/String;

    move-object p2, v0

    goto/16 :goto_1

    .line 232
    :cond_a
    sget-object v0, Ltv/peel/widget/b;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ne v0, v1, :cond_b

    .line 233
    sget-object v0, Ltv/peel/widget/b;->g:Ljava/util/List;

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 234
    sget-object v2, Ltv/peel/widget/b;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "sending the only 1 Fan command: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v1, Ltv/peel/widget/b;->g:Ljava/util/List;

    invoke-interface {v1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_7

    .line 236
    :cond_b
    sget v0, Ltv/peel/widget/b;->k:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Ltv/peel/widget/b;->k:I

    .line 237
    sget v0, Ltv/peel/widget/b;->k:I

    sget-object v1, Ltv/peel/widget/b;->g:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lt v0, v1, :cond_c

    .line 238
    sget-object v0, Ltv/peel/widget/b;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    sput v0, Ltv/peel/widget/b;->k:I

    .line 239
    :cond_c
    sget-object v0, Ltv/peel/widget/b;->g:Ljava/util/List;

    sget v1, Ltv/peel/widget/b;->k:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 240
    iget-object v1, p0, Ltv/peel/widget/b;->i:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v3}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v3

    invoke-virtual {v3}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v3

    invoke-virtual {v3}, Lcom/peel/data/at;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "_"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "ac_last_fanspeed_idx"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    sget v3, Ltv/peel/widget/b;->k:I

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 241
    sget-object v2, Ltv/peel/widget/b;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "sending the Fan command (idx: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget v3, Ltv/peel/widget/b;->k:I

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v3, "): "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v1, Ltv/peel/widget/b;->g:Ljava/util/List;

    sget v4, Ltv/peel/widget/b;->k:I

    invoke-interface {v1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_7

    .line 244
    :cond_d
    const-string/jumbo v0, "FAN_LOW"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_11

    .line 245
    sget-object v0, Ltv/peel/widget/b;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_e

    .line 247
    sget-object v0, Ltv/peel/widget/b;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "No Fan Speed commands for AC: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v2

    invoke-virtual {v2}, Lcom/peel/data/g;->f()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " -- code set: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v2

    invoke-virtual {v2}, Lcom/peel/data/g;->h()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, p2

    .line 259
    :goto_8
    const-string/jumbo v1, "F"

    iput-object v1, p0, Ltv/peel/widget/b;->p:Ljava/lang/String;

    move-object p2, v0

    goto/16 :goto_1

    .line 248
    :cond_e
    sget-object v0, Ltv/peel/widget/b;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ne v0, v1, :cond_f

    .line 249
    sget-object v0, Ltv/peel/widget/b;->g:Ljava/util/List;

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 250
    sget-object v2, Ltv/peel/widget/b;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "sending the only 1 Fan command: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v1, Ltv/peel/widget/b;->g:Ljava/util/List;

    invoke-interface {v1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_8

    .line 252
    :cond_f
    sget v0, Ltv/peel/widget/b;->k:I

    add-int/lit8 v0, v0, -0x1

    sput v0, Ltv/peel/widget/b;->k:I

    .line 253
    sget v0, Ltv/peel/widget/b;->k:I

    if-gez v0, :cond_10

    .line 254
    sput v4, Ltv/peel/widget/b;->k:I

    .line 255
    :cond_10
    sget-object v0, Ltv/peel/widget/b;->g:Ljava/util/List;

    sget v1, Ltv/peel/widget/b;->k:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 256
    iget-object v1, p0, Ltv/peel/widget/b;->i:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v3}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v3

    invoke-virtual {v3}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v3

    invoke-virtual {v3}, Lcom/peel/data/at;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "_"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "ac_last_fanspeed_idx"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    sget v3, Ltv/peel/widget/b;->k:I

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 257
    sget-object v2, Ltv/peel/widget/b;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "sending the Fan command (idx: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget v3, Ltv/peel/widget/b;->k:I

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v3, "): "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v1, Ltv/peel/widget/b;->g:Ljava/util/List;

    sget v4, Ltv/peel/widget/b;->k:I

    invoke-interface {v1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_8

    .line 260
    :cond_11
    const-string/jumbo v0, "MODE"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 264
    sget-object v0, Ltv/peel/widget/b;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_12

    .line 266
    sget-object v0, Ltv/peel/widget/b;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "No Mode commands for AC: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v2

    invoke-virtual {v2}, Lcom/peel/data/g;->f()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " -- code set: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v2

    invoke-virtual {v2}, Lcom/peel/data/g;->h()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, p2

    .line 278
    :goto_9
    const-string/jumbo v1, "M"

    iput-object v1, p0, Ltv/peel/widget/b;->p:Ljava/lang/String;

    move-object p2, v0

    goto/16 :goto_1

    .line 267
    :cond_12
    sget-object v0, Ltv/peel/widget/b;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ne v0, v1, :cond_13

    .line 268
    sget-object v0, Ltv/peel/widget/b;->h:Ljava/util/List;

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 269
    sget-object v2, Ltv/peel/widget/b;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "sending the only 1 direction command: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v1, Ltv/peel/widget/b;->h:Ljava/util/List;

    invoke-interface {v1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_9

    .line 271
    :cond_13
    sget v0, Ltv/peel/widget/b;->l:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Ltv/peel/widget/b;->l:I

    .line 272
    sget v0, Ltv/peel/widget/b;->l:I

    sget-object v1, Ltv/peel/widget/b;->h:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    rem-int/2addr v0, v1

    sput v0, Ltv/peel/widget/b;->l:I

    .line 273
    sget-object v0, Ltv/peel/widget/b;->h:Ljava/util/List;

    sget v1, Ltv/peel/widget/b;->l:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 274
    iget-object v1, p0, Ltv/peel/widget/b;->i:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v3}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v3

    invoke-virtual {v3}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v3

    invoke-virtual {v3}, Lcom/peel/data/at;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "_"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "ac_last_mode_idx"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    sget v3, Ltv/peel/widget/b;->l:I

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 275
    sget-object v2, Ltv/peel/widget/b;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "sending the direction command (idx: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget v3, Ltv/peel/widget/b;->l:I

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v3, "): "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v1, Ltv/peel/widget/b;->h:Ljava/util/List;

    sget v4, Ltv/peel/widget/b;->l:I

    invoke-interface {v1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_9

    .line 283
    :cond_14
    invoke-virtual {p1}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v0

    const-string/jumbo v1, "T_22"

    invoke-virtual {v0, v1}, Lcom/peel/data/g;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_15

    .line 284
    invoke-virtual {p1}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/data/g;->a()Ljava/util/Map;

    move-result-object v0

    const-string/jumbo v1, "T_22"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    const-string/jumbo v1, "type"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Ltv/peel/widget/b;->o:Ljava/lang/String;

    goto/16 :goto_2

    .line 286
    :cond_15
    const-string/jumbo v0, ""

    iput-object v0, p0, Ltv/peel/widget/b;->o:Ljava/lang/String;

    goto/16 :goto_2

    .line 293
    :cond_16
    iget-boolean v0, p0, Ltv/peel/widget/b;->q:Z

    if-eqz v0, :cond_1

    .line 294
    iget-object v0, p0, Ltv/peel/widget/b;->n:Ljava/util/Map;

    if-nez v0, :cond_17

    .line 295
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Ltv/peel/widget/b;->n:Ljava/util/Map;

    .line 297
    :cond_17
    iget-object v0, p0, Ltv/peel/widget/b;->n:Ljava/util/Map;

    iget-object v1, p0, Ltv/peel/widget/b;->p:Ljava/lang/String;

    invoke-interface {v0, v1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 298
    iget-object v0, p0, Ltv/peel/widget/b;->n:Ljava/util/Map;

    invoke-static {v0}, Lcom/peel/util/ay;->a(Ljava/util/Map;)Ljava/lang/String;

    move-result-object p2

    goto/16 :goto_3
.end method

.method static synthetic a(Ltv/peel/widget/b;Ltv/peel/widget/service/e;)Ltv/peel/widget/service/e;
    .locals 0

    .prologue
    .line 47
    iput-object p1, p0, Ltv/peel/widget/b;->b:Ltv/peel/widget/service/e;

    return-object p1
.end method

.method private a(ILjava/lang/String;Lcom/peel/control/h;)V
    .locals 8

    .prologue
    const/16 v2, 0x1388

    const/4 v5, -0x1

    .line 617
    if-eqz p3, :cond_0

    .line 618
    invoke-static {}, Lcom/peel/util/a/f;->a()Lcom/peel/util/a/f;

    move-result-object v0

    sget-object v1, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v1}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/data/at;->f()I

    move-result v1

    invoke-virtual {p3}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v3

    invoke-virtual {v3}, Lcom/peel/data/g;->h()I

    move-result v5

    invoke-virtual {p3}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v3

    invoke-virtual {v3}, Lcom/peel/data/g;->f()Ljava/lang/String;

    move-result-object v6

    .line 619
    invoke-virtual {p3}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v3

    invoke-virtual {v3}, Lcom/peel/data/g;->d()I

    move-result v7

    move v3, p1

    move-object v4, p2

    .line 618
    invoke-virtual/range {v0 .. v7}, Lcom/peel/util/a/f;->a(IIILjava/lang/String;ILjava/lang/String;I)V

    .line 623
    :goto_0
    return-void

    .line 621
    :cond_0
    invoke-static {}, Lcom/peel/util/a/f;->a()Lcom/peel/util/a/f;

    move-result-object v0

    sget-object v1, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v1}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/data/at;->f()I

    move-result v1

    const-string/jumbo v6, "no device"

    move v3, p1

    move-object v4, p2

    move v7, v5

    invoke-virtual/range {v0 .. v7}, Lcom/peel/util/a/f;->a(IIILjava/lang/String;ILjava/lang/String;I)V

    goto :goto_0
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Lcom/peel/control/h;)V
    .locals 7

    .prologue
    .line 307
    iget-boolean v0, p0, Ltv/peel/widget/b;->m:Z

    if-eqz v0, :cond_2

    .line 311
    invoke-virtual {p3}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/data/g;->a()Ljava/util/Map;

    move-result-object v3

    .line 313
    invoke-interface {v3, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    .line 317
    const-string/jumbo v1, "PowerOn"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    const-string/jumbo v1, "PowerOff"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 318
    iget-object v1, p0, Ltv/peel/widget/b;->n:Ljava/util/Map;

    invoke-interface {v1, p2, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 321
    iget-object v1, p0, Ltv/peel/widget/b;->r:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 322
    iget-object v1, p0, Ltv/peel/widget/b;->r:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 323
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v3, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map;

    .line 324
    const-string/jumbo v5, "ir"

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v6

    invoke-interface {v2, v5, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 325
    invoke-virtual {p3}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v5

    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v5, v1, v2}, Lcom/peel/data/g;->a(Ljava/lang/String;Ljava/util/Map;)V

    goto :goto_0

    .line 329
    :cond_0
    iget-object v1, p0, Ltv/peel/widget/b;->r:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 331
    iget-object v1, p0, Ltv/peel/widget/b;->r:Ljava/util/Map;

    const-string/jumbo v2, "ir"

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, p1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 337
    :cond_1
    iget-object v1, p0, Ltv/peel/widget/b;->o:Ljava/lang/String;

    iget-object v2, p0, Ltv/peel/widget/b;->n:Ljava/util/Map;

    invoke-static {v1, v3, v2}, Lcom/peel/util/ay;->b(Ljava/lang/String;Ljava/util/Map;Ljava/util/Map;)Ljava/lang/String;

    move-result-object v1

    .line 339
    const-string/jumbo v2, "ir"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 341
    invoke-virtual {p3}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v1

    invoke-virtual {v1, p1, v0}, Lcom/peel/data/g;->a(Ljava/lang/String;Ljava/util/Map;)V

    .line 344
    :cond_2
    return-void
.end method

.method static synthetic b(Ltv/peel/widget/b;)I
    .locals 1

    .prologue
    .line 47
    iget v0, p0, Ltv/peel/widget/b;->e:I

    return v0
.end method

.method static synthetic c(Ltv/peel/widget/b;)Ltv/peel/widget/util/c;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Ltv/peel/widget/b;->d:Ltv/peel/widget/util/c;

    return-object v0
.end method


# virtual methods
.method public a(Lcom/peel/control/a;)Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 395
    const/4 v0, 0x0

    .line 396
    if-eqz p1, :cond_0

    .line 397
    invoke-virtual {p1, v4}, Lcom/peel/control/a;->a(I)Lcom/peel/control/h;

    move-result-object v0

    .line 399
    if-eqz v0, :cond_2

    .line 400
    invoke-virtual {v0}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/data/g;->f()Ljava/lang/String;

    move-result-object v1

    .line 402
    invoke-virtual {v0}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/data/g;->d()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 420
    iget-object v0, p0, Ltv/peel/widget/b;->c:Landroid/content/Context;

    sget v1, Lcom/peel/ui/ft;->watching:I

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-array v1, v4, [Ljava/lang/Object;

    invoke-virtual {p1}, Lcom/peel/control/a;->c()Lcom/peel/data/d;

    move-result-object v2

    invoke-virtual {v2}, Lcom/peel/data/d;->a()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v5

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 428
    :cond_0
    :goto_0
    return-object v0

    .line 404
    :sswitch_0
    iget-object v0, p0, Ltv/peel/widget/b;->c:Landroid/content/Context;

    sget v2, Lcom/peel/ui/ft;->watching:I

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-array v2, v4, [Ljava/lang/Object;

    invoke-virtual {p1}, Lcom/peel/control/a;->c()Lcom/peel/data/d;

    move-result-object v3

    invoke-virtual {v3}, Lcom/peel/data/d;->a()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-static {v0, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 405
    const-string/jumbo v2, "directv"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    const-string/jumbo v2, "tivo"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 406
    :cond_1
    iget-object v0, p0, Ltv/peel/widget/b;->c:Landroid/content/Context;

    sget v2, Lcom/peel/ui/ft;->watching:I

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-array v2, v4, [Ljava/lang/Object;

    aput-object v1, v2, v5

    invoke-static {v0, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 410
    :sswitch_1
    iget-object v0, p0, Ltv/peel/widget/b;->c:Landroid/content/Context;

    sget v2, Lcom/peel/ui/ft;->watching:I

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-array v2, v4, [Ljava/lang/Object;

    aput-object v1, v2, v5

    invoke-static {v0, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 414
    :sswitch_2
    invoke-virtual {p1}, Lcom/peel/control/a;->a()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 424
    :cond_2
    iget-object v0, p0, Ltv/peel/widget/b;->c:Landroid/content/Context;

    sget v1, Lcom/peel/ui/ft;->watching:I

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-array v1, v4, [Ljava/lang/Object;

    invoke-virtual {p1}, Lcom/peel/control/a;->c()Lcom/peel/data/d;

    move-result-object v2

    invoke-virtual {v2}, Lcom/peel/data/d;->a()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v5

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 402
    nop

    :sswitch_data_0
    .sparse-switch
        0x5 -> :sswitch_2
        0x6 -> :sswitch_1
        0x12 -> :sswitch_2
        0x14 -> :sswitch_0
    .end sparse-switch
.end method

.method public a(Lcom/peel/control/h;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 90
    invoke-virtual {p1}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/data/g;->a()Ljava/util/Map;

    move-result-object v1

    .line 92
    const-string/jumbo v0, "16_F_A_C"

    invoke-interface {v1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 93
    const/4 v0, 0x1

    iput-boolean v0, p0, Ltv/peel/widget/b;->q:Z

    .line 96
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Ltv/peel/widget/b;->f:Ljava/util/List;

    .line 97
    const/16 v0, 0x10

    :goto_0
    const/16 v2, 0x1e

    if-gt v0, v2, :cond_3

    .line 98
    iget-boolean v2, p0, Ltv/peel/widget/b;->q:Z

    if-nez v2, :cond_1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "T_"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 99
    :cond_1
    sget-object v2, Ltv/peel/widget/b;->f:Ljava/util/List;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "T_"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 97
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 105
    :cond_3
    sget-object v0, Ltv/peel/widget/b;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_5

    .line 106
    const-string/jumbo v0, "UP"

    invoke-interface {v1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 107
    sget-object v0, Ltv/peel/widget/b;->f:Ljava/util/List;

    const-string/jumbo v2, "UP"

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 108
    :cond_4
    const-string/jumbo v0, "Down"

    invoke-interface {v1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 109
    sget-object v0, Ltv/peel/widget/b;->f:Ljava/util/List;

    const-string/jumbo v2, "Down"

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 113
    :cond_5
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Ltv/peel/widget/b;->g:Ljava/util/List;

    .line 114
    iget-boolean v0, p0, Ltv/peel/widget/b;->q:Z

    if-nez v0, :cond_6

    const-string/jumbo v0, "FAN_LOW"

    invoke-interface {v1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 115
    :cond_6
    sget-object v0, Ltv/peel/widget/b;->g:Ljava/util/List;

    const-string/jumbo v2, "FAN_LOW"

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 116
    :cond_7
    iget-boolean v0, p0, Ltv/peel/widget/b;->q:Z

    if-nez v0, :cond_8

    const-string/jumbo v0, "FAN_MED"

    invoke-interface {v1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 117
    :cond_8
    sget-object v0, Ltv/peel/widget/b;->g:Ljava/util/List;

    const-string/jumbo v2, "FAN_MED"

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 118
    :cond_9
    iget-boolean v0, p0, Ltv/peel/widget/b;->q:Z

    if-nez v0, :cond_a

    const-string/jumbo v0, "FAN_HIGH"

    invoke-interface {v1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 119
    :cond_a
    sget-object v0, Ltv/peel/widget/b;->g:Ljava/util/List;

    const-string/jumbo v2, "FAN_HIGH"

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 121
    :cond_b
    iget-boolean v0, p0, Ltv/peel/widget/b;->q:Z

    if-eqz v0, :cond_c

    sget-object v0, Ltv/peel/widget/b;->g:Ljava/util/List;

    const-string/jumbo v2, "FAN_AUTO"

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 124
    :cond_c
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Ltv/peel/widget/b;->h:Ljava/util/List;

    .line 125
    invoke-interface {v1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    .line 126
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_d
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_e

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 127
    const-string/jumbo v2, "Mode_"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_d

    .line 128
    sget-object v2, Ltv/peel/widget/b;->h:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 130
    :cond_e
    iget-boolean v0, p0, Ltv/peel/widget/b;->q:Z

    if-eqz v0, :cond_f

    .line 131
    sget-object v0, Ltv/peel/widget/b;->h:Ljava/util/List;

    const-string/jumbo v1, "Mode_Cool"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 132
    sget-object v0, Ltv/peel/widget/b;->h:Ljava/util/List;

    const-string/jumbo v1, "Mode_Heat"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 135
    :cond_f
    iget-object v0, p0, Ltv/peel/widget/b;->i:Landroid/content/SharedPreferences;

    if-nez v0, :cond_13

    const-string/jumbo v0, "prefs null"

    .line 136
    :goto_2
    sget-object v1, Ltv/peel/widget/b;->a:Ljava/lang/String;

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 137
    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    if-nez v0, :cond_14

    const-string/jumbo v0, "PeelControl.control null"

    .line 138
    :goto_3
    sget-object v1, Ltv/peel/widget/b;->a:Ljava/lang/String;

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 139
    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v0}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v0

    if-nez v0, :cond_15

    const-string/jumbo v0, "PeelControl.control.getCurrentRoom() null"

    .line 140
    :goto_4
    sget-object v1, Ltv/peel/widget/b;->a:Ljava/lang/String;

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 142
    iget-object v0, p0, Ltv/peel/widget/b;->i:Landroid/content/SharedPreferences;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v2}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v2

    invoke-virtual {v2}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v2

    invoke-virtual {v2}, Lcom/peel/data/at;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "ac_last_temp_idx"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, v5}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    sput v0, Ltv/peel/widget/b;->j:I

    .line 145
    sget v0, Ltv/peel/widget/b;->j:I

    sget-object v1, Ltv/peel/widget/b;->f:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lt v0, v1, :cond_10

    .line 146
    sput v5, Ltv/peel/widget/b;->j:I

    .line 148
    :cond_10
    iget-object v0, p0, Ltv/peel/widget/b;->i:Landroid/content/SharedPreferences;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v2}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v2

    invoke-virtual {v2}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v2

    invoke-virtual {v2}, Lcom/peel/data/at;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "ac_last_fanspeed_idx"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, v5}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    sput v0, Ltv/peel/widget/b;->k:I

    .line 150
    sget v0, Ltv/peel/widget/b;->k:I

    sget-object v1, Ltv/peel/widget/b;->g:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lt v0, v1, :cond_11

    .line 151
    sput v5, Ltv/peel/widget/b;->k:I

    .line 153
    :cond_11
    iget-object v0, p0, Ltv/peel/widget/b;->i:Landroid/content/SharedPreferences;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v2}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v2

    invoke-virtual {v2}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v2

    invoke-virtual {v2}, Lcom/peel/data/at;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "ac_last_mode_idx"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, v5}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    sput v0, Ltv/peel/widget/b;->l:I

    .line 155
    sget v0, Ltv/peel/widget/b;->l:I

    sget-object v1, Ltv/peel/widget/b;->h:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lt v0, v1, :cond_12

    .line 156
    sput v5, Ltv/peel/widget/b;->l:I

    .line 158
    :cond_12
    return-void

    .line 135
    :cond_13
    const-string/jumbo v0, "prefs not null"

    goto/16 :goto_2

    .line 137
    :cond_14
    const-string/jumbo v0, "PeelControl.control not null"

    goto/16 :goto_3

    .line 139
    :cond_15
    const-string/jumbo v0, "PeelControl.control.getCurrentRoom() not null"

    goto/16 :goto_4
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 665
    if-eqz p1, :cond_0

    .line 666
    :try_start_0
    iget-object v0, p0, Ltv/peel/widget/b;->b:Ltv/peel/widget/service/e;

    invoke-interface {v0, p1, p2}, Ltv/peel/widget/service/e;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 670
    :cond_0
    :goto_0
    return-void

    .line 667
    :catch_0
    move-exception v0

    .line 668
    sget-object v1, Ltv/peel/widget/b;->a:Ljava/lang/String;

    sget-object v2, Ltv/peel/widget/b;->a:Ljava/lang/String;

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 4

    .prologue
    .line 627
    if-nez p2, :cond_1

    .line 652
    :cond_0
    :goto_0
    return-void

    .line 630
    :cond_1
    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v0, p2}, Lcom/peel/control/am;->c(Ljava/lang/String;)Lcom/peel/control/h;

    move-result-object v0

    .line 632
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/data/g;->d()I

    move-result v1

    const/16 v2, 0x12

    if-ne v1, v2, :cond_2

    .line 633
    const-string/jumbo v1, "PowerOn"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    const-string/jumbo v1, "PowerOff"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 634
    invoke-direct {p0, v0, p1}, Ltv/peel/widget/b;->a(Lcom/peel/control/h;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 637
    :cond_2
    if-eqz v0, :cond_0

    .line 639
    invoke-static {}, Lcom/peel/util/i;->c()Z

    move-result v1

    if-nez v1, :cond_3

    .line 640
    invoke-virtual {v0, p1}, Lcom/peel/control/h;->c(Ljava/lang/String;)Z

    .line 650
    :goto_1
    invoke-direct {p0, p3, p1, v0}, Ltv/peel/widget/b;->a(ILjava/lang/String;Lcom/peel/control/h;)V

    goto :goto_0

    .line 642
    :cond_3
    const-class v1, Ltv/peel/widget/b;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "sendCommand"

    new-instance v3, Ltv/peel/widget/d;

    invoke-direct {v3, p0, v0, p1}, Ltv/peel/widget/d;-><init>(Ltv/peel/widget/b;Lcom/peel/control/h;Ljava/lang/String;)V

    invoke-static {v1, v2, v3}, Lcom/peel/util/i;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/lang/Runnable;

    goto :goto_1
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Ltv/peel/widget/b;->b:Ltv/peel/widget/service/e;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public a([Lcom/peel/control/a;)[Ljava/lang/String;
    .locals 3

    .prologue
    .line 382
    const/4 v0, 0x0

    .line 383
    if-eqz p1, :cond_1

    array-length v1, p1

    if-lez v1, :cond_1

    .line 384
    array-length v0, p1

    new-array v1, v0, [Ljava/lang/String;

    .line 386
    const/4 v0, 0x0

    :goto_0
    array-length v2, p1

    if-ge v0, v2, :cond_0

    .line 387
    aget-object v2, p1, v0

    invoke-virtual {p0, v2}, Ltv/peel/widget/b;->a(Lcom/peel/control/a;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    .line 386
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    move-object v0, v1

    .line 391
    :cond_1
    return-object v0
.end method

.method public b(Lcom/peel/control/a;)V
    .locals 3

    .prologue
    .line 682
    const-class v0, Ltv/peel/widget/b;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "started activity"

    new-instance v2, Ltv/peel/widget/e;

    invoke-direct {v2, p0, p1}, Ltv/peel/widget/e;-><init>(Ltv/peel/widget/b;Lcom/peel/control/a;)V

    invoke-static {v0, v1, v2}, Lcom/peel/util/i;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/lang/Runnable;

    .line 695
    return-void
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 161
    sget-object v0, Ltv/peel/widget/b;->h:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-object v0, Ltv/peel/widget/b;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 162
    const/4 v0, 0x1

    .line 165
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c()Ljava/lang/String;
    .locals 3

    .prologue
    .line 169
    sget-object v0, Ltv/peel/widget/b;->f:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-object v0, Ltv/peel/widget/b;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 170
    sget-object v0, Ltv/peel/widget/b;->f:Ljava/util/List;

    sget v1, Ltv/peel/widget/b;->j:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string/jumbo v1, "_"

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 171
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v2, 0x1

    aget-object v0, v0, v2

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0xb0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "C"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 173
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public d()Ljava/lang/String;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 177
    sget-object v0, Ltv/peel/widget/b;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_0

    move-object v0, v1

    .line 184
    :goto_0
    return-object v0

    .line 179
    :cond_0
    sget-object v0, Ltv/peel/widget/b;->h:Ljava/util/List;

    sget v2, Ltv/peel/widget/b;->l:I

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 181
    if-eqz v0, :cond_1

    .line 182
    const/16 v1, 0x5f

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    move-object v0, v1

    .line 184
    goto :goto_0
.end method

.method public e()[Ltv/peel/widget/service/DeviceParcelable;
    .locals 3

    .prologue
    .line 348
    :try_start_0
    iget-object v0, p0, Ltv/peel/widget/b;->b:Ltv/peel/widget/service/e;

    invoke-interface {v0}, Ltv/peel/widget/service/e;->b()[Ltv/peel/widget/service/DeviceParcelable;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 353
    :goto_0
    return-object v0

    .line 349
    :catch_0
    move-exception v0

    .line 350
    sget-object v1, Ltv/peel/widget/b;->a:Ljava/lang/String;

    sget-object v2, Ltv/peel/widget/b;->a:Ljava/lang/String;

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 353
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public f()[Lcom/peel/control/a;
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 357
    sget-object v1, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v1}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v1

    .line 359
    if-nez v1, :cond_1

    .line 378
    :cond_0
    :goto_0
    return-object v0

    .line 363
    :cond_1
    invoke-virtual {v1}, Lcom/peel/control/RoomControl;->c()[Lcom/peel/control/a;

    move-result-object v1

    .line 364
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 366
    if-eqz v1, :cond_0

    .line 367
    array-length v3, v1

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v3, :cond_3

    aget-object v4, v1, v0

    .line 368
    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Lcom/peel/control/a;->a(I)Lcom/peel/control/h;

    move-result-object v5

    .line 369
    invoke-virtual {p0}, Ltv/peel/widget/b;->k()Z

    move-result v6

    if-nez v6, :cond_2

    if-eqz v5, :cond_2

    invoke-virtual {v5}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v5

    invoke-virtual {v5}, Lcom/peel/data/g;->d()I

    move-result v5

    const/4 v6, 0x5

    if-ne v5, v6, :cond_2

    .line 367
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 372
    :cond_2
    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 374
    :cond_3
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [Lcom/peel/control/a;

    .line 375
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    goto :goto_0
.end method

.method public g()Lcom/peel/control/a;
    .locals 4

    .prologue
    .line 432
    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v0}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v1

    .line 433
    const/4 v0, 0x0

    .line 435
    if-eqz v1, :cond_1

    .line 436
    invoke-virtual {v1}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/data/at;->b()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Ltv/peel/widget/b;->a(Ljava/lang/String;)Lcom/peel/control/a;

    move-result-object v0

    .line 437
    if-eqz v0, :cond_0

    .line 438
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/peel/control/a;->a(I)Lcom/peel/control/h;

    move-result-object v1

    .line 440
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v2

    invoke-virtual {v2}, Lcom/peel/data/g;->d()I

    move-result v2

    const/16 v3, 0x12

    if-ne v2, v3, :cond_0

    .line 441
    invoke-virtual {p0, v1}, Ltv/peel/widget/b;->a(Lcom/peel/control/h;)V

    .line 447
    :cond_0
    :goto_0
    return-object v0

    .line 445
    :cond_1
    sget-object v1, Ltv/peel/widget/b;->a:Ljava/lang/String;

    const-string/jumbo v2, "room is null"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public h()V
    .locals 2

    .prologue
    .line 508
    iget-object v0, p0, Ltv/peel/widget/b;->b:Ltv/peel/widget/service/e;

    if-eqz v0, :cond_0

    .line 509
    iget-object v0, p0, Ltv/peel/widget/b;->c:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Ltv/peel/widget/b;->s:Landroid/content/ServiceConnection;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    .line 510
    const/4 v0, 0x0

    iput-object v0, p0, Ltv/peel/widget/b;->b:Ltv/peel/widget/service/e;

    .line 512
    :cond_0
    return-void
.end method

.method public i()Z
    .locals 3

    .prologue
    .line 541
    :try_start_0
    iget-object v0, p0, Ltv/peel/widget/b;->b:Ltv/peel/widget/service/e;

    invoke-interface {v0}, Ltv/peel/widget/service/e;->d()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 546
    :goto_0
    return v0

    .line 542
    :catch_0
    move-exception v0

    .line 543
    sget-object v1, Ltv/peel/widget/b;->a:Ljava/lang/String;

    sget-object v2, Ltv/peel/widget/b;->a:Ljava/lang/String;

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 546
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public j()Z
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 550
    iget-object v0, p0, Ltv/peel/widget/b;->c:Landroid/content/Context;

    const-string/jumbo v1, "widget_pref"

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string/jumbo v1, "is_setup_complete"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public k()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 554
    iget-object v2, p0, Ltv/peel/widget/b;->c:Landroid/content/Context;

    invoke-static {v2}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v2

    const-string/jumbo v3, "setup_type"

    invoke-interface {v2, v3, v1}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v2

    .line 555
    if-ne v2, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public l()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ltv/peel/widget/service/ChannelParcelable;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 656
    :try_start_0
    iget-object v1, p0, Ltv/peel/widget/b;->b:Ltv/peel/widget/service/e;

    if-nez v1, :cond_0

    .line 660
    :goto_0
    return-object v0

    .line 656
    :cond_0
    iget-object v1, p0, Ltv/peel/widget/b;->b:Ltv/peel/widget/service/e;

    invoke-interface {v1}, Ltv/peel/widget/service/e;->g()Ljava/util/List;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 657
    :catch_0
    move-exception v1

    .line 658
    sget-object v2, Ltv/peel/widget/b;->a:Ljava/lang/String;

    sget-object v3, Ltv/peel/widget/b;->a:Ljava/lang/String;

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public m()Z
    .locals 3

    .prologue
    .line 674
    :try_start_0
    iget-object v0, p0, Ltv/peel/widget/b;->b:Ltv/peel/widget/service/e;

    invoke-interface {v0}, Ltv/peel/widget/service/e;->f()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 678
    :goto_0
    return v0

    .line 675
    :catch_0
    move-exception v0

    .line 676
    sget-object v1, Ltv/peel/widget/b;->a:Ljava/lang/String;

    sget-object v2, Ltv/peel/widget/b;->a:Ljava/lang/String;

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 678
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public n()[Ltv/peel/widget/service/DeviceParcelable;
    .locals 12

    .prologue
    const/4 v11, 0x1

    const/4 v5, 0x0

    const/4 v7, 0x0

    .line 698
    invoke-static {}, Lcom/peel/content/a;->a()Lcom/peel/data/ContentRoom;

    move-result-object v0

    .line 701
    if-eqz v0, :cond_3

    .line 702
    invoke-virtual {v0}, Lcom/peel/data/ContentRoom;->a()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Ltv/peel/widget/b;->a(Ljava/lang/String;)Lcom/peel/control/a;

    move-result-object v0

    .line 703
    invoke-virtual {v0, v7}, Lcom/peel/control/a;->a(I)Lcom/peel/control/h;

    move-result-object v4

    .line 704
    if-nez v4, :cond_4

    .line 705
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 707
    invoke-virtual {v0}, Lcom/peel/control/a;->e()[Lcom/peel/control/h;

    move-result-object v9

    array-length v10, v9

    move v6, v7

    :goto_0
    if-ge v6, v10, :cond_2

    aget-object v4, v9, v6

    .line 708
    const/16 v0, 0xd

    invoke-virtual {v4}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/data/g;->d()I

    move-result v1

    if-eq v0, v1, :cond_0

    const/4 v0, 0x5

    invoke-virtual {v4}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/data/g;->d()I

    move-result v1

    if-eq v0, v1, :cond_0

    const/16 v0, 0xe

    invoke-virtual {v4}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/data/g;->d()I

    move-result v1

    if-eq v0, v1, :cond_0

    .line 709
    invoke-virtual {v4}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/data/g;->d()I

    move-result v0

    if-ne v11, v0, :cond_1

    .line 710
    :cond_0
    new-instance v0, Ltv/peel/widget/service/DeviceParcelable;

    invoke-virtual {v4}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/data/g;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v2

    invoke-virtual {v2}, Lcom/peel/data/g;->d()I

    move-result v2

    invoke-virtual {v4}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v3

    invoke-virtual {v3}, Lcom/peel/data/g;->d()I

    move-result v3

    invoke-static {v3}, Lcom/peel/util/bx;->a(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v4

    invoke-virtual {v4}, Lcom/peel/data/g;->f()Ljava/lang/String;

    move-result-object v4

    invoke-direct/range {v0 .. v5}, Ltv/peel/widget/service/DeviceParcelable;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 707
    :cond_1
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto :goto_0

    .line 713
    :cond_2
    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v5, v0, [Ltv/peel/widget/service/DeviceParcelable;

    .line 714
    invoke-virtual {v8, v5}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 720
    :cond_3
    :goto_1
    return-object v5

    .line 716
    :cond_4
    new-array v6, v11, [Ltv/peel/widget/service/DeviceParcelable;

    .line 717
    new-instance v0, Ltv/peel/widget/service/DeviceParcelable;

    invoke-virtual {v4}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/data/g;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v2

    invoke-virtual {v2}, Lcom/peel/data/g;->d()I

    move-result v2

    invoke-virtual {v4}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v3

    invoke-virtual {v3}, Lcom/peel/data/g;->d()I

    move-result v3

    invoke-static {v3}, Lcom/peel/util/bx;->a(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v4

    invoke-virtual {v4}, Lcom/peel/data/g;->f()Ljava/lang/String;

    move-result-object v4

    invoke-direct/range {v0 .. v5}, Ltv/peel/widget/service/DeviceParcelable;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v0, v6, v7

    move-object v5, v6

    goto :goto_1
.end method

.method public o()[Lcom/peel/control/h;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 724
    sget-object v1, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v1}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v1

    .line 727
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/data/at;->b()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Ltv/peel/widget/b;->a(Ljava/lang/String;)Lcom/peel/control/a;

    move-result-object v1

    .line 729
    :goto_0
    if-nez v1, :cond_0

    .line 730
    :goto_1
    return-object v0

    :cond_0
    invoke-virtual {v1}, Lcom/peel/control/a;->e()[Lcom/peel/control/h;

    move-result-object v0

    goto :goto_1

    :cond_1
    move-object v1, v0

    goto :goto_0
.end method
